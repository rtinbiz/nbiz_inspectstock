/** 
@addtogroup	Client_Socket_Library
@{
*/ 

/**
@file		InterServerInterface.h
@brief		Client Socket DLL 을 이용하여 Visual Station 64 Server의 기능을 이용 할 수 있게한다.

@remark		
-			본 Lib 는 UNICODE 문자열을 기본으로 사용하였다.
-			UNICODE 를 사용하지 않을시 컴파일 옵션을 변경하고 다시 컴파일 해주야 한다.
@author		김용태
@date		2007.05.08
*/
#pragma once

/**
@file		InterServerInterface.h
@brief		Client Socket DLL 과의 연결 기능을 제공한다.

@remark		Client Application 에서 본 헤더 파일을 Include 하고 Link 옵션에 ClientSockDll.lib 파일을 연결하여서 사용한다.
@author		김용태
@date		2006.
*/

#include <Windows.h>
#include "..\Constant\CommonConstant.h"
#include "..\Structure\Define_Message.h"
#include "..\Class\MemPooler.h"
#include "..\Class\MessageQueue.h"
#include "..\ImportClientDll.h"



//static CMemPooler<UCHAR[MAX_CLIENT_MESSAGE_BUFFER]>* MemoryPool;

static CMemPooler<UCHAR[MAX_CLIENT_MESSAGE_BUFFER]> MemoryPool(300);	///< Application 으로 전달될 메모리를 제공하는 메모리풀					
static HWND	st_hWnd;													///< 메시지 처리자가 존재하는 윈도우 핸들
static UINT	st_nUserMessage;											///< Application 에서 VS 64 메시지를 Window 이벤트로 받을 경우에 사용되는 사용자 메시지 번호
static CMessageQueue<char*>	MessageQueue;								///< Application 에서 VS 64 메시지를 직접 Peek 할때 사용되는 Message Queue

//#pragma warning( disable : 4996 )

/**
@fn			g_fnAppCallBackFunc
@brief		Client Socket DLL 과의 연결 기능을 제공한다.

@return		정상 종료 : OKAY, 이상 종료 : 그외
@remark		Client DLL 에서 부터 응답 받을 Callback function
@warning	Application 내에서는 VS64 메시지를 처리하고 나서 MemoryPool.Free() 함수를 이용하여 자원을 해지 해줘야 한다. 
@author		김용태
@date		2006.
*/
static int	g_fnAppCallBackFunc(CMDMSG *pCmdMsg,	///< Client Dll 에서 넘어온 메시지 구조체 포인터
								UCHAR *cBuff)		///< Client Dll 에서 넘어온 Data 포인터
{
	CMDMSG* cmdMsg	= NULL;
	UCHAR*	buff	= NULL;

	//cmdMsg = new CMDMSG;
	//buff = new UCHAR[cmdMsg->uMsgSize];

	if(NULL == pCmdMsg && NULL == cBuff)
	{
		cmdMsg	= (CMDMSG *)-1;		//QuitMessage....
		buff	= (UCHAR*)-1;
	}
	else
	{
		cmdMsg	= (CMDMSG*)MemoryPool.Alloc();
		buff	= (UCHAR*)MemoryPool.Alloc();

		memcpy(cmdMsg, pCmdMsg, sizeof(CMDMSG));
		memcpy(buff, cBuff, cmdMsg->uMsgSize);

		cmdMsg->cMsgBuf = buff;
	}

	if(NULL != st_hWnd && NULL != st_nUserMessage)
	{
		if(NULL == pCmdMsg && NULL == cBuff)
		{
			PostMessageW(st_hWnd, WM_QUIT, NULL, NULL);
		}
		else
		{
			PostMessageW(st_hWnd, st_nUserMessage, NULL, (LPARAM)cmdMsg);
		}
	}
	else
	{
		MessageQueue.m_fnPushMessage((char*)cmdMsg);
	}

	return OKAY;
}

/** 
@class		CInterServerInterface
@brief		InterServerInterface

@remark		
-			Application 에 Client DLL 의 기능을 제공한다.
-			Client로 메시지 전달 방법은 2가지방식으로 제공한다. 
-			생성자에 파라메터가 입력이 되면 Window Message 를 이용한 VS 64 메시지 전달이 되고, 아니면 메시지 큐를 이용한 전달이 된다.
@warning	Application 내에서는 VS64 메시지를 처리하고 나서 MemoryPool.Free() 함수를 이용하여 자원을 해지 해줘야 한다. 
@author		김용태
@date		2006.
*/ 
class CInterServerInterface
{
public:
	USHORT m_uTaskNo;					///< 자신의 Task NO 를 가진다.

private:
	//Log Task Number
	USHORT	m_uLogTaskNo;				///< Log Task 번호. 로그 기록 기능 호출시 사용된다.
	USHORT	m_uLogBlockLevel;			///< Log 를 기록할 Log Level 을 가진다. 이것을 보고 Log Task 로 메시지를 보낼지 결정한다.
	USHORT	m_uLogFileID;				///< Log 기록 할 File ID , File ID List 는 Log Task 에 정의 되어 있다.
	bool	m_bAutoExitFlag;			///< Server 와의 연결이 끊기면 자동으로 Process 가 종료할지 여부. Default 는 true 이다.

public:
	/** 
	@brief		생성자

	@return		없음
	@remark		Class 생성시 파라메터가 입력이되면 Window Message 를 이용한 VS 64 메시지 전달이 되고, 아니면 메시지 큐를 이용한 전달이 된다.
	@author		김용태
	@date		2006.
	*/
	CInterServerInterface(
		HWND hWnd = NULL,				///< 메시지를 받을 윈도우 Handle
		UINT nUserMessageNo = NULL,		///< 메시지 전달에 사용될 사용자 메시지 번호
		bool bAutoExitFlag = true		///< 서버가 종료 될때 Application 을 자동 종료 시킬지를 가지는 Flag
		):
	m_uLogTaskNo(DEFAULT_LOG_TASK),
		m_uLogBlockLevel(LOG_LEVEL_4),
		m_uLogFileID(0),
		m_bAutoExitFlag(bAutoExitFlag)
	{
		st_hWnd			= hWnd;
		st_nUserMessage	= nUserMessageNo;

		//MemoryPool		= new CMemPooler<UCHAR[MAX_CLIENT_MESSAGE_BUFFER]>(300);
	}

	/** 
	@brief		소멸자

	@return		없음
	@remark		
	@author		김용태
	@date		2006.
	*/
	~CInterServerInterface(void)
	{
		//delete MemoryPool;
	}

	/** 
	@brief		VS64 메시지를 전달 받을 Window handle 을 등록한다.

	@return		없음
	@remark		생성자에서도 같은 기능을 제공하지만 따로 호출되어 등록 할 수 있다.
	@author		김용태
	@date		2006.
	*/
	void m_fnSetAppWindowHandle(
		HWND hWnd						///< 메시지를 전달 받을 Window Handle
		)
	{
		st_hWnd = hWnd;
	}

	/** 
	@brief		사용자 메시지를 등록한다.

	@return		없음
	@remark		생성자에서도 같은 기능을 제공하지만 따로 호출되어 등록 할 수 있다.
	@author		김용태
	@date		2006.
	*/
	void m_fnSetAppUserMessage(
		UINT	nUserMessage			///< 메시지로 전달 받을시 사용될 메시지 번호
		)
	{
		st_nUserMessage = nUserMessage;
	}

	/** 
	@brief		VS64 메시지 전달을 위해 Application 에 제공한 메모리를 회수한다.

	@return		없음
	@remark		Application 에서는 메시지 처리가 끝난 후 본 함수를 사용하여 자원을 반환해 주어야 한다.
	@author		김용태
	@date		2006.
	*/
	int m_fnFreeMemory(
		void* pMemory					///< 자원을 회수할 메모리 주소
		)
	{ 
		return MemoryPool.Free((UCHAR(*)[MAX_CLIENT_MESSAGE_BUFFER])pMemory)==true?1:0;
	}

	/** 
	@brief		서버에서 전달된 메시지를 큐에서 하나 꺼내어 Application 에 전달 한다.

	@return		VS64 메시지
	@remark		꺼내진 메시지는 Application 내에서 사용 후 반환 되어야 한다.
	@author		김용태
	@date		2006.
	*/
	char* m_fnPeekMessage()
	{	
		char*	pMsg = NULL; 

		while(TRUE)
		{
			pMsg = MessageQueue.m_fnPopMessage();

			if((char*)-1 == pMsg)
			{
				if(m_bAutoExitFlag)
				{				
					exit(1);
				}
				else
				{
					//자동 종료를 하지 않는 경우 Application 에서 처리가 되어 지게 한다.
					break;
				}
			}
			else if(NULL == pMsg)
			{
				//메시지 큐에서 메시지를 Pop 했지만 없을 경우 대기 모드로 진입한다.
				::WaitForSingleObject(MessageQueue.m_hEventMessageExist, INFINITE);
				continue;
			}
			break;
		}

		return pMsg;
	}

	/** 
	@brief		서버에서 전달된 메시지를 큐에서 하나 꺼내어 Application 에 전달 한다. Non Block Mode

	@return		VS64 메시지
	@remark		꺼내진 메시지는 Application 내에서 사용 후 반환 되어야 한다. Blocking 이 되지 않는다.
	@author		김용태
	@date		2006.
	*/
	char* m_fnPeekMessageForNonBlock()
	{	
		char*	pMsg = NULL; 


		pMsg = MessageQueue.m_fnPopMessage();

		if((char*)-1 == pMsg)
		{
			if(m_bAutoExitFlag)
			{				
				exit(1);
			}

		}
		else if(pMsg != NULL)
		{
			pMsg = pMsg;
		}

		return pMsg;
	}
	/** 
	@brief		서버에서 전달된 메시지를 큐에서 하나 꺼내어 Application 에 전달 한다. Non Block Mode

	@return		VS64 메시지
	@remark		꺼내진 메시지는 Application 내에서 사용 후 반환 되어야 한다. Blocking 이 되지 않는다.
	@author		김용태
	@date		2006.
	*/
	char* m_fnPeekMessageForNonBlockExitCode()
	{	
		char*	pMsg = NULL; 


		pMsg = MessageQueue.m_fnPopMessage();

		if((char*)-1 == pMsg)
		{
			if(m_bAutoExitFlag)
			{				
				//exit(1);
				return (char*)-1;
			}

		}
		else if(pMsg != NULL)
		{
			pMsg = pMsg;
		}

		return pMsg;
	}
	/** 
	@brief		서버에 등록한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		등록 할 서버가 같은 PC 내에 있지 않다면 IP 주소와 Port 번호를 입력받아야 한다.
	@author		김용태
	@date		2006.
	*/
	int m_fnRegisterTask(
		USHORT uTaskNo,						///< 서버로 등록시 사용할 Task 번호
		char* pszServerIp = "127.0.0.1",	///< 등록 할 서버의 IP 주소
		WORD wPortNo = 5000					///< 등록 할 서버의 Port No
		)
	{
		int	nRet;

		m_uTaskNo = uTaskNo;

		nRet = g_fnRegisterTask(uTaskNo, g_fnAppCallBackFunc, pszServerIp, wPortNo);

		return nRet;
	}

	/** 
	@brief		서버에 등록 해지한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int m_fnUnRegisterTask()
	{
		int	nRet;

		nRet = g_fnUnRegisterTask();

		return nRet;
	}

	/** 
	@brief		다른 Task 로 메시지를 전달한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnSendCommand(
		CMDMSG* sCmdMsg						///< 전송할 정보를 담은 CMDMSG 구조체 포인터
		)
	{
		int		nResultCode;

		nResultCode = g_fnCommandSend(sCmdMsg);

		return nResultCode;//OK;
	}

	/** 
	@brief		다른 Task 로 메시지를 전달한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnSendCommand(
		USHORT uTask_Dest,					///< 목적지 Task 번호
		USHORT uFunID,						///< 호출 할 목적지의 Funtion ID
		USHORT uSeqID,						///< 호출 할 목적지의 Sequence ID
		USHORT uUnitID						///< 호출 할 목적지의 Unit ID
		)
	{
		int		nResultCode;
		CMDMSG	sCmdMsg;

		ZeroMemory((void*)&sCmdMsg,sizeof(CMDMSG));

		sCmdMsg.uStation_Src	= 0;
		sCmdMsg.uStation_Dest	= sCmdMsg.uStation_Src;
		sCmdMsg.uTask_Src		= m_uTaskNo;
		sCmdMsg.uTask_Dest		= uTask_Dest;
		sCmdMsg.uFunID_Dest		= uFunID;
		sCmdMsg.uSeqID_Dest		= uSeqID;
		sCmdMsg.uUnitID_Dest	= uUnitID;
		//sCmdMsg.uRetStatus=0;
		sCmdMsg.uFunID_Src		= 0;
		sCmdMsg.uSeqID_Src		= 0;
		sCmdMsg.uUnitID_Src		= 0;
		sCmdMsg.uMsgType		= CMD_TYPE_RES;
		sCmdMsg.uMsgOrigin		= CMD_TYPE_NOREPLY;

		nResultCode = g_fnCommandSend(&sCmdMsg);

		return nResultCode;//OK;
	}

	/** 
	@brief		다른 Task 로 메시지를 전달한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnSendCommand(
		USHORT uTask_Dest,						///< 목적지 Task 번호
		USHORT uFunID_Dest,						///< 호출 할 목적지의 Funtion ID
		USHORT uSeqID_Dest,						///< 호출 할 목적지의 Sequence ID
		USHORT uUnitID_Dest,					///< 호출 할 목적지의 Unit ID
		USHORT uMsgSize,						///< 전달할 메시지의 Size
		UCHAR *cMsgBuf,							///< 전달할 메시지
		USHORT uFunID_Src = 0,					///< 근원지의 Function ID
		USHORT uSeqID_Src = 0,					///< 근원지의 Sequence ID
		USHORT uUnitID_Src = 0,					///< 근원지의 Unit ID
		USHORT uMsgType = CMD_TYPE_RES,		///< 메시지가 Response 가 필요한지 여부, CMD_TYPE_NORES/CMD_TYPE_RES
		USHORT uMsgOrigin = CMD_TYPE_NOREPLY	///< 메시지가 처리에 대한 응답인지 여부, CMD_TYPE_REPLY/CMD_TYPE_NOREPLY
		)
	{
		int		nResultCode;
		CMDMSG	sCmdMsg;

		ZeroMemory((void*)&sCmdMsg,sizeof(CMDMSG));

		sCmdMsg.uStation_Src	= 0;
		sCmdMsg.uStation_Dest	= sCmdMsg.uStation_Src;
		sCmdMsg.uTask_Src		= m_uTaskNo;
		sCmdMsg.uTask_Dest		= uTask_Dest;
		sCmdMsg.uFunID_Dest		= uFunID_Dest;
		sCmdMsg.uSeqID_Dest		= uSeqID_Dest;
		sCmdMsg.uUnitID_Dest	= uUnitID_Dest;
		sCmdMsg.uFunID_Src		= uFunID_Src;
		sCmdMsg.uSeqID_Src		= uSeqID_Src;
		sCmdMsg.uUnitID_Src		= uUnitID_Src;
		//sCmdMsg.uRetStatus=0;
		sCmdMsg.uMsgType		= uMsgType;
		sCmdMsg.uMsgOrigin		= uMsgOrigin;
		sCmdMsg.uMsgSize		= uMsgSize;
		sCmdMsg.cMsgBuf			= cMsgBuf;

		nResultCode = g_fnCommandSend(&sCmdMsg);

		return nResultCode;//OK;
	}

//////////////////////////////////////////////////////////////////////////
	/** 
	@brief		다른 Task 로 메시지를 전달한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnSendCommand_Nrs(
		USHORT uTask_Dest,					///< 목적지 Task 번호
		USHORT uFunID,						///< 호출 할 목적지의 Funtion ID
		USHORT uSeqID,						///< 호출 할 목적지의 Sequence ID
		USHORT uUnitID						///< 호출 할 목적지의 Unit ID
		)
	{
		int		nResultCode;
		CMDMSG	sCmdMsg;

		ZeroMemory((void*)&sCmdMsg,sizeof(CMDMSG));

		sCmdMsg.uStation_Src	= 0;
		sCmdMsg.uStation_Dest	= sCmdMsg.uStation_Src;
		sCmdMsg.uTask_Src		= m_uTaskNo;
		sCmdMsg.uTask_Dest		= uTask_Dest;
		sCmdMsg.uFunID_Dest		= uFunID;
		sCmdMsg.uSeqID_Dest		= uSeqID;
		sCmdMsg.uUnitID_Dest	= uUnitID;
		//sCmdMsg.uRetStatus=0;
		sCmdMsg.uFunID_Src		= 0;
		sCmdMsg.uSeqID_Src		= 0;
		sCmdMsg.uUnitID_Src		= 0;
		sCmdMsg.uMsgType		= CMD_TYPE_NORES;
		sCmdMsg.uMsgOrigin		= CMD_TYPE_NOREPLY;


		nResultCode = g_fnCommandSend(&sCmdMsg);

		return nResultCode;//OK;
	}
	
	/** 
	@brief		다른 Task 로 메시지를 전달한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnSendCommand_Nrs(
		USHORT uTask_Dest,						///< 목적지 Task 번호
		USHORT uFunID_Dest,						///< 호출 할 목적지의 Funtion ID
		USHORT uSeqID_Dest,						///< 호출 할 목적지의 Sequence ID
		USHORT uUnitID_Dest,					///< 호출 할 목적지의 Unit ID
		USHORT uMsgSize,						///< 전달할 메시지의 Size
		UCHAR *cMsgBuf,							///< 전달할 메시지
		USHORT uFunID_Src = 0,					///< 근원지의 Function ID
		USHORT uSeqID_Src = 0,					///< 근원지의 Sequence ID
		USHORT uUnitID_Src = 0,					///< 근원지의 Unit ID
		USHORT uMsgType = CMD_TYPE_NORES,		///< 메시지가 Response 가 필요한지 여부, CMD_TYPE_NORES/CMD_TYPE_RES
		USHORT uMsgOrigin = CMD_TYPE_NOREPLY	///< 메시지가 처리에 대한 응답인지 여부, CMD_TYPE_REPLY/CMD_TYPE_NOREPLY
		)
	{
		int		nResultCode;
		CMDMSG	sCmdMsg;

		ZeroMemory((void*)&sCmdMsg,sizeof(CMDMSG));

		sCmdMsg.uStation_Src	= 0;
		sCmdMsg.uStation_Dest	= sCmdMsg.uStation_Src;
		sCmdMsg.uTask_Src		= m_uTaskNo;
		sCmdMsg.uTask_Dest		= uTask_Dest;
		sCmdMsg.uFunID_Dest		= uFunID_Dest;
		sCmdMsg.uSeqID_Dest		= uSeqID_Dest;
		sCmdMsg.uUnitID_Dest	= uUnitID_Dest;
		sCmdMsg.uFunID_Src		= uFunID_Src;
		sCmdMsg.uSeqID_Src		= uSeqID_Src;
		sCmdMsg.uUnitID_Src		= uUnitID_Src;
		//sCmdMsg.uRetStatus=0;
		sCmdMsg.uMsgType		= uMsgType;
		sCmdMsg.uMsgOrigin		= uMsgOrigin;
		sCmdMsg.uMsgSize		= uMsgSize;
		sCmdMsg.cMsgBuf			= cMsgBuf;

		nResultCode = g_fnCommandSend(&sCmdMsg);

		return nResultCode;//OK;
	}

	/** 
	@brief		받은 Message에 대한 Response를 보낸다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	USHORT m_fnSendResponse(UCHAR *cMsgBuf, UCHAR * pResponMsge = NULL, USHORT SizeByte = 0)
	{
		CMDMSG *pCmdMsg = (CMDMSG *)cMsgBuf;
		
		CMDMSG cmdMsg;
		int nRetVal;

		ZeroMemory(&cmdMsg, sizeof(CMDMSG));

		cmdMsg.uStation_Src		= pCmdMsg->uStation_Dest;
		cmdMsg.uStation_Dest	= pCmdMsg->uStation_Src;
		cmdMsg.uTask_Src		= pCmdMsg->uTask_Dest;
		cmdMsg.uTask_Dest		= pCmdMsg->uTask_Src;
		cmdMsg.uFunID_Src		= pCmdMsg->uFunID_Dest;
		cmdMsg.uFunID_Dest		= pCmdMsg->uFunID_Src;
		cmdMsg.uSeqID_Src		= pCmdMsg->uSeqID_Dest;
		cmdMsg.uSeqID_Dest		= pCmdMsg->uSeqID_Src;
		cmdMsg.uUnitID_Src		= pCmdMsg->uUnitID_Dest;
		cmdMsg.uUnitID_Dest		= pCmdMsg->uUnitID_Src;

		if(SizeByte>0 && pResponMsge != NULL)
		{
			cmdMsg.uMsgSize			= SizeByte;
			cmdMsg.cMsgBuf			= pCmdMsg->cMsgBuf;
		}
		else
		{
			cmdMsg.uMsgSize			= 0;
			cmdMsg.cMsgBuf			= NULL;
		}

		cmdMsg.uRetStatus		= pCmdMsg->uRetStatus;
		cmdMsg.uMsgType			= CMD_TYPE_NORES;
		cmdMsg.uMsgOrigin		= CMD_TYPE_REPLY;
		cmdMsg.uTimeOut			= CMD_TIMEOUT;


		nRetVal = g_fnCommandSend(&cmdMsg);

		return nRetVal;
	}
//////////////////////////////////////////////////////////////////////////
	/** 
	@brief		공유메모리에 Data를 기록한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnWriteSMPage(
		SMPMSG* sWriteSM						///< 기록할 정보를 담은 SMPMSG 구조체 포인터
		)
	{
		int		nResultCode = 0;	

		nResultCode = g_fnSMPWrite(sWriteSM);

		return nResultCode;
	}

	/** 
	@brief		공유메모리에 Data를 기록한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnWriteSMPage(
		USHORT uStation,						///< 접근할 Station No
		USHORT uTaskNo,							///< 자신의 Task No
		USHORT uFileNo,							///< 접근할 SMP File No
		char* cAccessID,						///< 접근자
		UCHAR* cSMPBuf							///< 기록 할 실제 Data
		)
	{
		int			nResultCode = 0;
		SMPMSG		WriteSMData;

		ZeroMemory((void*)&WriteSMData,sizeof(SMPMSG));
		CopyMemory((void*)WriteSMData.cAccessID, cAccessID, strlen(cAccessID));	

		WriteSMData.uStation	= uStation;
		WriteSMData.uTaskNo		= uTaskNo;
		WriteSMData.uFuncNo		= 0;		
		WriteSMData.uFileNo		= uFileNo;
		WriteSMData.uAccessType	= VS_MEMORY_ACCESS_ID;	
		WriteSMData.uStartPos	= 0;
		WriteSMData.uLength		= 0;	
		WriteSMData.cSMPBuf		= cSMPBuf;

		nResultCode = g_fnSMPWrite(&WriteSMData);

		return nResultCode;
	}

	/** 
	@brief		공유메모리에 Data를 기록한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnWriteSMPage(
		USHORT uStation,						///< 접근할 Station No
		USHORT uTaskNo,							///< 자신의 Task No
		USHORT uFileNo,							///< 접근할 SMP File No
		char* cAccessID,						///< 접근자
		USHORT uLength,							///< 접근자를 기준으로 접근 할 길이
		UCHAR* cSMPBuf							///< 기록 할 실제 Data
		)
	{
		int			nResultCode = 0;
		SMPMSG		WriteSMData;

		ZeroMemory((void*)&WriteSMData,sizeof(SMPMSG));
		CopyMemory((void*)WriteSMData.cAccessID, cAccessID, strlen(cAccessID));	

		WriteSMData.uStation	= uStation;
		WriteSMData.uTaskNo		= uTaskNo;
		WriteSMData.uFuncNo		= 0;		
		WriteSMData.uFileNo		= uFileNo;
		WriteSMData.uAccessType	= VS_MEMORY_ACCESS_ID;	
		WriteSMData.uStartPos	= 0;
		WriteSMData.uLength		= uLength;	
		WriteSMData.cSMPBuf		= cSMPBuf;

		nResultCode = g_fnSMPWrite(&WriteSMData);

		return nResultCode;
	}
	/** 
	@brief		공유메모리에 Data를 기록한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnWriteSMPage(
		USHORT uTaskNo,							///< 자신의 Task No
		USHORT uFileNo,							///< 접근할 SMP File No
		char* cAccessID,						///< 접근자
		USHORT uLength,							///< 접근자를 기준으로 접근 할 길이
		UCHAR* cSMPBuf							///< 기록 할 실제 Data
		)
	{
		int			nResultCode = 0;
		SMPMSG		WriteSMData;

		ZeroMemory((void*)&WriteSMData,sizeof(SMPMSG));
		CopyMemory((void*)WriteSMData.cAccessID, cAccessID, strlen(cAccessID));	

		WriteSMData.uStation	= 0;
		WriteSMData.uTaskNo		= uTaskNo;
		WriteSMData.uFuncNo		= 0;		
		WriteSMData.uFileNo		= uFileNo;
		WriteSMData.uAccessType	= VS_MEMORY_ACCESS_ID;	
		WriteSMData.uStartPos	= 0;
		WriteSMData.uLength		= uLength;	
		WriteSMData.cSMPBuf		= cSMPBuf;

		nResultCode = g_fnSMPWrite(&WriteSMData);

		return nResultCode;
	}
	/** 
	@brief		공유메모리에 Data를 기록한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnWriteSMPage(
		USHORT uStation,						///< 접근할 Station No
		USHORT uTaskNo,							///< 자신의 Task No
		USHORT uFileNo,							///< 접근할 SMP File No
		USHORT uStartPos,						///< 접근할 SMP File 내의 시작 Address
		USHORT uLength,							///< 시작 Address 부터 길이
		UCHAR* cSMPBuf							///< 기록 할 실제 Data
		)
	{
		int		nResultCode;
		SMPMSG	WriteSMData;

		ZeroMemory((void*)&WriteSMData,sizeof(SMPMSG));

		WriteSMData.uStation		= uStation;
		WriteSMData.uTaskNo			= uTaskNo;
		WriteSMData.uFuncNo			= 0;		
		WriteSMData.uFileNo			= uFileNo;
		WriteSMData.uAccessType		= VS_MEMORY_ACCESS_DIRECT;	
		WriteSMData.uStartPos		= uStartPos;
		WriteSMData.uLength			= uLength;	
		WriteSMData.cSMPBuf			= cSMPBuf;

		nResultCode = g_fnSMPWrite(&WriteSMData);
		return nResultCode;
	}

	/** 
	@brief		공유메모리에서 Data를 가져온다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnReadSMPage(
		SMPMSG* sReadSM							///< SMP 접근 정보를 담은 SMPMSG 구조체 포인터
		)
	{
		int		nResultCode = 0;

		nResultCode = g_fnSMPRead(sReadSM);

		return nResultCode;
	}

	/** 
	@brief		공유메모리에서 Data를 가져온다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnReadSMPage(
		USHORT uStation,						///< 접근할 Station No
		USHORT uTaskNo,							///< 자신의 Task No
		USHORT uFileNo,							///< 접근할 SMP File No
		char* cAccessID,						///< 접근자
		UCHAR* cSMPBuf							///< 응답 받을 메모리 공간
		)
	{
		int		nResultCode;
		SMPMSG		ReadSMData;

		ZeroMemory((void*)&ReadSMData,sizeof(SMPMSG));
		CopyMemory((void*)ReadSMData.cAccessID, cAccessID, strlen(cAccessID));	

		ReadSMData.uStation		= uStation;
		ReadSMData.uTaskNo		= uTaskNo;
		ReadSMData.uFuncNo		= 0;		
		ReadSMData.uFileNo		= uFileNo;
		ReadSMData.uAccessType	= VS_MEMORY_ACCESS_ID;	
		ReadSMData.uStartPos	= 0;
		ReadSMData.uLength		= 0;	
		ReadSMData.cSMPBuf		= cSMPBuf;

		nResultCode = g_fnSMPRead(&ReadSMData);
		return nResultCode;
	}

	/** 
	@brief		공유메모리에서 Data를 가져온다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		
	@author		김용태
	@date		2006.
	*/
	int	m_fnReadSMPage(
		USHORT uStation,						///< 접근할 Station No
		USHORT uTaskNo,							///< 자신의 Task No
		USHORT uFileNo,							///< 접근할 SMP File No
		USHORT uStartPos,						///< File 내의 시작 Address
		USHORT uLength,							///< 시작 Address 부터 길이
		UCHAR* cSMPBuf							///< 응답 받을 메모리 공간
		)
	{
		int		nResultCode;
		SMPMSG	ReadSMData;
		ZeroMemory((void*)&ReadSMData,sizeof(SMPMSG));

		ReadSMData.uStation			= uStation;
		ReadSMData.uTaskNo			= uTaskNo;
		ReadSMData.uFuncNo			= 0;		
		ReadSMData.uFileNo			= uFileNo;
		ReadSMData.uAccessType		= VS_MEMORY_ACCESS_DIRECT;	
		ReadSMData.uStartPos		= uStartPos;
		ReadSMData.uLength			= uLength;	
		ReadSMData.cSMPBuf			= cSMPBuf;

		nResultCode = g_fnSMPRead(&ReadSMData);
		return nResultCode;
	}

	/** 
	@brief		로그 기록을 위해 Log Task 정보를 설정한다.

	@return		없음
	@remark		
	@author		최지형
	@date		2006.
	*/
	void m_fnSetLogTaskInfo(
		USHORT LogTaskNumber = DEFAULT_LOG_TASK,	///< Log Task 번호
		USHORT BlockingLevel = LOG_LEVEL_4,			///< Log Block Level 을 설정한다.
		USHORT LogFileID = 0						///< Default로 사용 할 Log File ID
		)
	{
		m_uLogTaskNo		= LogTaskNumber;
		m_uLogBlockLevel	= BlockingLevel;
		m_uLogFileID			= LogFileID;
	}
	/** 
	@brief		로그를 기록한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		Log Task 로 Log 기록 메시지를 전달한다.
	@author		최지형
	@date		2006.
	*/
	int m_fnPrintLog(
		USHORT LogLevel,				///< 기록 할 Log 의 Log Level, 1차로 함수 내에서  Block을 결정하고, 2차로 Log Task 에서 Block 여부가 결정된다.
		const char* pszLogData,			///< 실제 기록 할 Log 내용
		...								///< 가변 인자
		)
	{
		int				nResultCode;
		WRITE_LOG_MSG	SendLogData;

		if(LogLevel > m_uLogBlockLevel)//NetWork부하를 줄이기 위해서 Log Data자체를 보내지 않는다. 
		{
			return OKAY;
		}

		ZeroMemory((void*)&SendLogData,sizeof(WRITE_LOG_MSG));
		SendLogData.LogLevel		= LogLevel;

		/////////////////////////////////////////////////////////////////////////////////////////////
		{
			char LogTime[30/*STRING_LENGTHE_LOG_TIME*/+1] = {0,};
			SYSTEMTIME SystemTime;
			GetLocalTime(&SystemTime); 
			//Format ex)"2006/10/20-16/48/52-102"
			sprintf_s(LogTime, "%02d:%02d:%02d:%03d", 
				SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, SystemTime.wMilliseconds);

			if(strlen(LogTime) <= 30/*STRING_LENGTHE_LOG_TIME*/)
				memcpy((char*)SendLogData.LogTime, LogTime, strlen(LogTime));
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		{
			char cLogBuffer[MAX_LOG_BUFF] ={0,};
			va_list vaList;
			va_start(vaList, pszLogData);
			vsprintf_s(cLogBuffer, pszLogData, (va_list)vaList);
			va_end(vaList);
			int LogCopySize  = (int) ((strlen(cLogBuffer)>500) ? 500 : strlen(cLogBuffer));
			memcpy((char*)SendLogData.LogData, (char*)cLogBuffer, LogCopySize);
		}
		/////////////////////////////////////////////////////////////////////////////////////////////

		nResultCode = m_fnSendCommand(m_uLogTaskNo, 60, 1, m_uLogFileID, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData);

		return nResultCode;//OK;
	}

	/** 
	@brief		로그를 기록한다. 유니코드(한글) 대응 로그 기록 함수이다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		유니코드 문자열을 가지는 Log 를 기록하게 한다. 
	@author		김용태
	@date		2006.
	*/
	int m_fnPrintLog(
		USHORT LogLevel,					///< 기록 할 Log 의 Log Level, 1차로 함수 내에서  Block을 결정하고, 2차로 Log Task 에서 Block 여부가 결정된다.
		const wchar_t* pswzLogData,			///< 실제 기록 할 Log 내용
		...									///< 가변 인자
		)
	{
		int				nResultCode;
		WRITE_LOG_MSG	SendLogData;

		if(LogLevel > m_uLogBlockLevel)//NetWork부하를 줄이기 위해서 Log Data자체를 보내지 않는다. 
		{
			return OKAY;
		}

		ZeroMemory((void*)&SendLogData,sizeof(WRITE_LOG_MSG));
		SendLogData.LogLevel		= LogLevel;

		/////////////////////////////////////////////////////////////////////////////////////////////
		{
			char LogTime[30/*STRING_LENGTHE_LOG_TIME*/+1] = {0,};
			SYSTEMTIME SystemTime;
			GetLocalTime(&SystemTime); 
			//Format ex)"2006/10/20-16/48/52-102"
			sprintf_s(LogTime, "%02d:%02d:%02d:%03d", 
				SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, SystemTime.wMilliseconds);

			if(strlen(LogTime) <= 30/*STRING_LENGTHE_LOG_TIME*/)
				memcpy((char*)SendLogData.LogTime, LogTime, strlen(LogTime));
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		{
			char szLogData[MAX_LOG_BUFF] = {0,};
			wchar_t cLogBuffer[MAX_LOG_BUFF] ={0,};

			va_list vaList;
			va_start(vaList, pswzLogData);
			vswprintf_s(cLogBuffer, pswzLogData, (va_list)vaList);
			va_end(vaList);

			WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)cLogBuffer, -1, (LPSTR)szLogData, 
				(int)wcslen((LPCWSTR)cLogBuffer)*2 +1,NULL,NULL);


			int LogCopySize  = (int) ((strlen(szLogData)>500) ? 99 : strlen(szLogData));
			memcpy((char*)SendLogData.LogData, (char*)szLogData, LogCopySize);
		}
		/////////////////////////////////////////////////////////////////////////////////////////////

		nResultCode = m_fnSendCommand(m_uLogTaskNo, 60, 1, m_uLogFileID, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData);

		return nResultCode;//OK;
	}

	/** 
	@brief		로그를 기록한다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		Log Task 로 Log 기록 메시지를 전달한다.
	@author		최지형
	@date		2006.
	*/
	int m_fnPrintLog(
		UINT LogFileID,					///< 기록 할 Log File ID, Log Task 에서 관리하는 File ID 이다.
		USHORT LogLevel,				///< 기록 할 Log 의 Log Level, 1차로 함수 내에서  Block을 결정하고, 2차로 Log Task 에서 Block 여부가 결정된다.
		const char* pszLogData,			///< 실제 기록 할 Log 내용
		...								///< 가변 인자
		)
	{
		int				nResultCode;
		WRITE_LOG_MSG	SendLogData;

		if(LogLevel > m_uLogBlockLevel)//NetWork부하를 줄이기 위해서 Log Data자체를 보내지 않는다. 
		{
			return OKAY;
		}

		ZeroMemory((void*)&SendLogData,sizeof(WRITE_LOG_MSG));
		SendLogData.LogLevel		= LogLevel;

		/////////////////////////////////////////////////////////////////////////////////////////////
		{
			char LogTime[30/*STRING_LENGTHE_LOG_TIME*/+1] = {0,};
			SYSTEMTIME SystemTime;
			GetLocalTime(&SystemTime); 
			//Format ex)"2006/10/20-16/48/52-102"
			sprintf_s(LogTime, "%02d:%02d:%02d:%03d", 
				SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, SystemTime.wMilliseconds);

			if(strlen(LogTime) <= 30/*STRING_LENGTHE_LOG_TIME*/)
				memcpy((char*)SendLogData.LogTime, LogTime, strlen(LogTime));
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		{
			char cLogBuffer[MAX_LOG_BUFF] ={0,};
			va_list vaList;
			va_start(vaList, pszLogData);
			vsprintf_s(cLogBuffer, pszLogData, (va_list)vaList);
			va_end(vaList);
			int LogCopySize  = (int) ((strlen(cLogBuffer)>500) ? 500 : strlen(cLogBuffer));
			memcpy((char*)SendLogData.LogData, (char*)cLogBuffer, LogCopySize);
		}
		/////////////////////////////////////////////////////////////////////////////////////////////

		nResultCode = m_fnSendCommand(m_uLogTaskNo, 60, 1, LogFileID, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData);

		return nResultCode;//OK;
	}


	/** 
	@brief		로그를 기록한다. 유니코드(한글) 대응 로그 기록 함수이다.

	@return		정상 종료 : 0, 이상 종료 : 그외
	@remark		유니코드 문자열을 가지는 Log 를 기록하게 한다. 
	@author		김용태
	@date		2006.
	*/
	int m_fnPrintLog(
		UINT LogFileID,						///< 기록 할 Log File ID, Log Task 에서 관리하는 File ID 이다.
		USHORT LogLevel,					///< 기록 할 Log 의 Log Level, 1차로 함수 내에서  Block을 결정하고, 2차로 Log Task 에서 Block 여부가 결정된다.
		const wchar_t* pswzLogData,			///< 실제 기록 할 Log 내용
		...									///< 가변 인자
		)
	{
		int				nResultCode;
		WRITE_LOG_MSG	SendLogData;

		if(LogLevel > m_uLogBlockLevel)//NetWork부하를 줄이기 위해서 Log Data자체를 보내지 않는다. 
		{
			return OKAY;
		}

		ZeroMemory((void*)&SendLogData,sizeof(WRITE_LOG_MSG));
		SendLogData.LogLevel		= LogLevel;

		/////////////////////////////////////////////////////////////////////////////////////////////
		{
			char LogTime[30/*STRING_LENGTHE_LOG_TIME*/+1] = {0,};
			SYSTEMTIME SystemTime;
			GetLocalTime(&SystemTime); 
			//Format ex)"2006/10/20-16/48/52-102"
			sprintf_s(LogTime, "%02d:%02d:%02d:%03d", 
				SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, SystemTime.wMilliseconds);

			if(strlen(LogTime) <= 30/*STRING_LENGTHE_LOG_TIME*/)
				memcpy((char*)SendLogData.LogTime, LogTime, strlen(LogTime));
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		{
			char szLogData[MAX_LOG_BUFF] = {0,};
			wchar_t cLogBuffer[MAX_LOG_BUFF] ={0,};

			va_list vaList;
			va_start(vaList, pswzLogData);
			vswprintf_s(cLogBuffer, pswzLogData, (va_list)vaList);
			va_end(vaList);

			WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)cLogBuffer, -1, (LPSTR)szLogData, 
				(int)wcslen((LPCWSTR)cLogBuffer)*2 +1,NULL,NULL);


			int LogCopySize  = (int) ((strlen(szLogData)>500) ? 99 : strlen(szLogData));
			memcpy((char*)SendLogData.LogData, (char*)szLogData, LogCopySize);
		}
		/////////////////////////////////////////////////////////////////////////////////////////////

		nResultCode = m_fnSendCommand(m_uLogTaskNo, 60, 1, LogFileID, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData);

		return nResultCode;//OK;
	}

};

/** 
@}
*/ 