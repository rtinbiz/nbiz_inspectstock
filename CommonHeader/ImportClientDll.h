/** 
@addtogroup	Client_Socket_Library
@{
*/ 

/**
@file		ImportClientDll.h
@brief		Client Socket DLL 의 기능을 제공한다.

@remark		
@author		김용태
@date		2006
*/

#pragma once

typedef int (*AppCallBackFunc)(CMDMSG* pCmdMsg, UCHAR* pBuff);			///< client Application 의 메시지 처리자의 함수형을 정의 한다.

extern "C++" __declspec(dllimport) int g_fnRegisterTask(
	USHORT uTaskNo, AppCallBackFunc g_CallBackFunc, 
	char* pszServerIp = "127.0.0.1", WORD wPortNo = 5000);				///< Task를 Visual Station Internal Server 에 등록 한다.


extern "C++" __declspec(dllimport) int g_fnUnRegisterTask();			///< Visual Station에서 등록 해지 한다.

extern "C++" __declspec(dllimport) int g_fnCommandSend(CMDMSG* pCmdMsg);///< 타 Cleint 로 메시지를 전송한다.

extern "C++" __declspec(dllimport) int g_fnSMPRead(SMPMSG* pSmpMsg);	///< 공유메모리 읽기 기능을 지원한다.

extern "C++" __declspec(dllimport) int g_fnSMPWrite(SMPMSG* pSmpMsg);	///< 공유메모리 쓰기 기능을 지원한다.

/** 
@}
*/ 