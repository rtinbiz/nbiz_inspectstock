#pragma once

//Error Code Define Start=========================================================
#define		BASIC_ERROR_NUM					20000					//Seq Task 의 기본이 되는 Error No이다.
//#define		SUCCESS_CODE					00001
#define		ERR_DAT_FILE_OPEN				BASIC_ERROR_NUM
#define		ERR_COMMAND_FILE_OPEN			BASIC_ERROR_NUM + 1
#define		ERR_STATUS_FILE_OPEN			BASIC_ERROR_NUM + 2
#define		ERR_MATCH_ID					BASIC_ERROR_NUM + 3


//가공 관련 Error code
#define		ERR_CONDITION_LOADING			BASIC_ERROR_NUM + 9000
#define		ERR_REPAIR_PROCESS				BASIC_ERROR_NUM + 9001
//Error Code Define End=================================================

