#pragma once

#define		VS61LOGMCC_INIT_FILE_PATH			"D:\\nBiz_InspectStock\\EXE\\Config\\APP_T61_Log.DAT"
#define MCC_COMMON_INI_SAVE_PATH		"D:\\nBiz_InspectStock\\Data\\Task_INI\\VS61MCCCommonParam.ini"
#define MAX_MCC_LOG_BUFF   50

#define MCC_SECTION  "MCC_COMMON"
#define T_MODULE_ID  "MODULE_ID"
#define T_LOGTYPE    "LOGTYPE"
#define T_STEPID     "STEPID"
#define T_STICKID    "STICKID" 
#define T_BOXID      "BOXID"
#define T_HOSTPPID   "HOSTPPID"
#define T_EQPPPID	 "EQPPPID"

//////////////////////////////////////////////////////////////////////////
/// 위치 변경 주의! String Table 과 Enum
const CString STR_MCC_COMMON_TABLE[] = 
{
	T_MODULE_ID,
	T_LOGTYPE,  
	T_STEPID,   
	T_STICKID,  
	T_BOXID,   
	T_HOSTPPID,
	T_EQPPPID,
};

enum
{
	N_MODULE_ID = 0,
	N_LOGTYPE,
	N_STEPID,
	N_STICKID,
	N_BOXID,
	N_HOSTPPID,
	N_EQPPPID,
};

/** 
@brief		SMD A3 MCC 를 위해 추가.

@remark		Log Type 구분을 위한 List
@author		최재원
@date		2014.12.15
*/
enum MCC_LOG_LIST
{	
	MCC_ACCTION_LOG = 0,
	MCC_SIGNAL_LOG,
	MCC_INFORMATION_LOG,
	MCC_EVENT_LOG,
	MCC_ERROR_LOG,
	MCC_WARNING_LOG,

};

#define LOGTYPE_ACTION			"A"
#define LOGTYPE_SIGNAL			"S"
#define LOGTYPE_INFORMATION		"I"
#define LOGTYPE_EVENT			"V"
#define LOGTYPE_ERROR			"E"
#define LOGTYPE_WARNING			"W"

const CString STR_MCC_LOGTYPE_TABLE[] = 
{
	LOGTYPE_ACTION,
	LOGTYPE_SIGNAL,		
	LOGTYPE_INFORMATION,
	LOGTYPE_EVENT,		
	LOGTYPE_ERROR,		
	LOGTYPE_WARNING,	
};
/// 위치 변경 주의! String Table 과 Enum
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
/// 위치 변경 주의! 
struct STRU_MCC_COMM_READ
{
	//SYSTEMTIME	tEventTime;					  // Event Time		ex) 1017_1532_59.799 10월 17일 15시 32분 59.799초
	char	chModuleID[MAX_MCC_LOG_BUFF];		  // ModuleID
	char	chLogType[MAX_MCC_LOG_BUFF];		  // LogType		ex) A,S,I,V,E,W (Action, Signal, Information, eVent, Error, Warning)
	char	chStepID[MAX_MCC_LOG_BUFF];			  // StepID
	char	chStickID[MAX_MCC_LOG_BUFF];		  // StickID		ex) 사양서에는 GlassID로 나와있음
	char	chBoxID[MAX_MCC_LOG_BUFF];		      // BoxID			ex) 사양서에는 TrayID로 나와있음
	char	chHostPPID[MAX_MCC_LOG_BUFF];	      // Host PPID      
	char	chEqpPPID[MAX_MCC_LOG_BUFF];	      // Eqp PPID      

	STRU_MCC_COMM_READ()
	{
		memset(this, 0x00, sizeof(STRU_MCC_COMM_READ));
	}
};

struct STRU_MCC_EVENT_INPUT
{
	int			nLogType;
	//SYSTEMTIME	tEventTime;	
	char	chModuleID[MAX_MCC_LOG_BUFF];		  // ModuleID
	char	chStickID[MAX_MCC_LOG_BUFF];		  // StickID		ex) 사양서에는 GlassID로 나와있음

	char		chEvent_1[MAX_MCC_LOG_BUFF];		  // X_LD_Servo_On  ex) Action Name
	char		chEvent_2[MAX_MCC_LOG_BUFF];          // SM01_UD01      ex) From
	char		chEvent_3[MAX_MCC_LOG_BUFF];          // SM02_UD02		ex) ToSTRU_MCC_COMM_READ()
	char		chEvent_4[MAX_MCC_LOG_BUFF];	      // Start or End{
	char		chEvent_5[MAX_MCC_LOG_BUFF];	      // Reserve	memset(this, 0x00, sizeof(STRU_MCC_COMM_READ));
	char		chEvent_6[MAX_MCC_LOG_BUFF];	      // Reserve}
	
	STRU_MCC_EVENT_INPUT()
	{
		memset(this, 0x00, sizeof(STRU_MCC_EVENT_INPUT));
	}
};

struct STRU_MCC_LOG
{
	//SYSTEMTIME	tEventTime;					  // Event Time		ex) 1017_1532_59.799 10월 17일 15시 32분 59.799초
	char	chModuleID[MAX_MCC_LOG_BUFF];		  // ModuleID
	char	chLogType[MAX_MCC_LOG_BUFF];		  // LogType		ex) A,S,I,V,E,W (Action, Signal, Information, eVent, Error, Warning)
	char	chStepID[MAX_MCC_LOG_BUFF];			  // StepID
	char	chStickID[MAX_MCC_LOG_BUFF];		  // StickID		ex) 사양서에는 GlassID로 나와있음
	char	chBoxID[MAX_MCC_LOG_BUFF];		      // BoxID			ex) 사양서에는 TrayID로 나와있음
	char	chHostPPID[MAX_MCC_LOG_BUFF];	      // Host PPID      
	char	chEqpPPID[MAX_MCC_LOG_BUFF];		  // Eqp PPID		
	char	chEvent_1[MAX_MCC_LOG_BUFF];		  // X_LD_Servo_On  ex) Action Name
	char	chEvent_2[MAX_MCC_LOG_BUFF];          // SM01_UD01      ex) From
	char	chEvent_3[MAX_MCC_LOG_BUFF];          // SM02_UD02		ex) To
	char	chEvent_4[MAX_MCC_LOG_BUFF];	      // Start or End
	char	chEvent_5[MAX_MCC_LOG_BUFF];	      // Reserve
	char	chEvent_6[MAX_MCC_LOG_BUFF];	      // Reserve

	STRU_MCC_LOG()
	{
		memset(this, 0x00, sizeof(STRU_MCC_LOG));
	}
	void ResetEvent()
	{
		memset(chEvent_1, 0x00, MAX_MCC_LOG_BUFF * 6);
	}
};

/// 위치 변경 주의! 
//////////////////////////////////////////////////////////////////////////


// LOG Foramt
////////////////////////////////////////////////////////////////////////// 
#define MODULEID_SIZE 20


#define MODULE_BC01_BT01			"BC01_BT01"
#define MODULE_BC01_TM01			"BC01_TM01"
#define MODULE_BC01_LT01			"BC01_LT01"
#define MODULE_BC01_BO01			"BC01_BO01"
#define MODULE_BC01_UT01			"BC01_BC01"

#define MODULE_SC01_BT01			"SC01_TM02"
#define MODULE_SC01_TM01			"SC01_UT02"
#define MODULE_SC01_LT01			"SC01_TT01"
#define MODULE_SC01_BO01			"SC01_AM01"

#define LAYER_BT01					"BT01"
#define LAYER_TM01					"TM01"
#define LAYER_LT01					"LT01"
#define LAYER_BO01					"BO01"
#define LAYER_BC01					"BC01"

#define LAYER_TM02					"TM02"
#define LAYER_UT02					"UT02"
#define LAYER_TT01					"TT01"
#define LAYER_AM01					"AM01"


////////////////////////////////////////////////////////////////////////// 
// ACTION LOG START
struct STRU_ACTION_LOG
{
	int nIndex;
	char *pChActionName;
	char *pChModuleID;
	char *pchFrom;
	char *pchTo;	
};

enum enum_ACTION_NAME
{
	INIT_LIFT_ORIGIN,
	LOAD_POS_MOVE01,
	LOAD_POS_MOVE02,
	LOAD_POS_MOVE03,
	LOAD_POS_MOVE04,
	LOAD_POS_MOVE05,
	LOAD_POS_MOVE06,
	LOAD_POS_MOVE07,
};

static STRU_ACTION_LOG G_ActionLog_Format[] = {
	{INIT_LIFT_ORIGIN, "INIT_LIFT_ORIGIN", MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{LOAD_POS_MOVE01,   "LOAD_POS_MOVE01", MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{LOAD_POS_MOVE02,   "LOAD_POS_MOVE02", MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{LOAD_POS_MOVE03,   "LOAD_POS_MOVE03", MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{LOAD_POS_MOVE04,   "LOAD_POS_MOVE04", MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{LOAD_POS_MOVE05,   "LOAD_POS_MOVE05", MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{LOAD_POS_MOVE06,   "LOAD_POS_MOVE06", MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{LOAD_POS_MOVE07,   "LOAD_POS_MOVE07", MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},	
};

// SIGNAL LOG START
enum
{
	SIGNAL_TYPE_INPUT,
	SIGNAL_TYPE_OUTPUT,
	SIGNAL_TYPE_PIO,
};

struct STRU_SIGNAL_LOG
{
	int nIndex;
	char *pChSignalName;
	int nType;     // Input, Output, PIO	
	char *pChModuleID;
	char *pchFrom;
	char *pchTo;	
};

enum
{
	STEP1_BOX_DETECT_SENSOR1_1,
	STEP1_BOX_DETECT_SENSOR1_2,
	STEP1_BOX_DETECT_SENSOR1_3,
	STEP2_BOX_DETECT_SENSOR2_1,
	STEP2_BOX_DETECT_SENSOR2_2,
	STEP2_BOX_DETECT_SENSOR2_3,
	STEP3_BOX_DETECT_SENSOR3_1,
	STEP3_BOX_DETECT_SENSOR3_2,
	STEP3_BOX_DETECT_SENSOR3_3,
	STEP4_BOX_DETECT_SENSOR4_1,
	STEP4_BOX_DETECT_SENSOR4_2,
	STEP4_BOX_DETECT_SENSOR4_3,
	STEP5_BOX_DETECT_SENSOR5_1,
	STEP5_BOX_DETECT_SENSOR5_2,
	STEP5_BOX_DETECT_SENSOR5_3,
	STEP6_BOX_DETECT_SENSOR6_1,
	STEP6_BOX_DETECT_SENSOR6_2,
	STEP6_BOX_DETECT_SENSOR6_3,
	STEP7_BOX_DETECT_SENSOR7_1,
	STEP7_BOX_DETECT_SENSOR7_2,
	STEP7_BOX_DETECT_SENSOR7_3,
	STEP8_BOX_DETECT_SENSOR8_1,
	STEP8_BOX_DETECT_SENSOR8_2,
	STEP8_BOX_DETECT_SENSOR8_3,
};

static STRU_SIGNAL_LOG G_SignalLog_Format[] = {
	{STEP1_BOX_DETECT_SENSOR1_1, "STEP1_BOX_DETECT_SENSOR1_1", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP1_BOX_DETECT_SENSOR1_2, "STEP1_BOX_DETECT_SENSOR1_2", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP1_BOX_DETECT_SENSOR1_3, "STEP1_BOX_DETECT_SENSOR1_3", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP2_BOX_DETECT_SENSOR2_1, "STEP2_BOX_DETECT_SENSOR2_1", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP2_BOX_DETECT_SENSOR2_2, "STEP2_BOX_DETECT_SENSOR2_2", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP2_BOX_DETECT_SENSOR2_3, "STEP2_BOX_DETECT_SENSOR2_3", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP3_BOX_DETECT_SENSOR3_1, "STEP3_BOX_DETECT_SENSOR3_1", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP3_BOX_DETECT_SENSOR3_2, "STEP3_BOX_DETECT_SENSOR3_2", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP3_BOX_DETECT_SENSOR3_3, "STEP3_BOX_DETECT_SENSOR3_3", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP4_BOX_DETECT_SENSOR4_1, "STEP4_BOX_DETECT_SENSOR4_1", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP4_BOX_DETECT_SENSOR4_2, "STEP4_BOX_DETECT_SENSOR4_2", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP4_BOX_DETECT_SENSOR4_3, "STEP4_BOX_DETECT_SENSOR4_3", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP5_BOX_DETECT_SENSOR5_1, "STEP5_BOX_DETECT_SENSOR5_1", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP5_BOX_DETECT_SENSOR5_2, "STEP5_BOX_DETECT_SENSOR5_2", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP5_BOX_DETECT_SENSOR5_3, "STEP5_BOX_DETECT_SENSOR5_3", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP6_BOX_DETECT_SENSOR6_1, "STEP6_BOX_DETECT_SENSOR6_1", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP6_BOX_DETECT_SENSOR6_2, "STEP6_BOX_DETECT_SENSOR6_2", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP6_BOX_DETECT_SENSOR6_3, "STEP6_BOX_DETECT_SENSOR6_3", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP7_BOX_DETECT_SENSOR7_1, "STEP7_BOX_DETECT_SENSOR7_1", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP7_BOX_DETECT_SENSOR7_2, "STEP7_BOX_DETECT_SENSOR7_2", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP7_BOX_DETECT_SENSOR7_3, "STEP7_BOX_DETECT_SENSOR7_3", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP8_BOX_DETECT_SENSOR8_1, "STEP8_BOX_DETECT_SENSOR8_1", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP8_BOX_DETECT_SENSOR8_2, "STEP8_BOX_DETECT_SENSOR8_2", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{STEP8_BOX_DETECT_SENSOR8_3, "STEP8_BOX_DETECT_SENSOR8_3", SIGNAL_TYPE_INPUT, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
};

// INFORMATION LOG START
struct STRU_INFORMATION_LOG
{
	int nIndex;
	char *pChParameter;
	char *pChUnit;     // ex mm/s, °, °c, %
	char *pChModuleID;
	char *pchFrom;
	char *pchTo;
};

#define UNIT_TEMP		 "°c"
#define UNIT_ANGLE		 "°"
#define UNIT_PERCENT     "%"
#define UNIT_MMSEC		 "mm/sec"
#define UNIT_MM			 "mm"
#define UNIT_HOUR		 "Hour"
#define UNIT_A			 "A"
#define UNIT_V			 "V"
#define UNIT_KPA		 "Kpa"
#define UNIT_TORQUE		 "N.m"

enum
{
	SET_SERVO_UPDOWN_SPD,
	SERVO_UPDOWN_CUR_SPD,
	SET_SERVO_UPDOWN_POS,
	SERVO_UPDOWN_CUR_POS,
	SERVO_UPDOWN_TOQ,	
};

static STRU_INFORMATION_LOG G_InformationLog_Format[] = {
	{SET_SERVO_UPDOWN_SPD, "SET_SERVO_UPDOWN_SPD", UNIT_MMSEC, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{SERVO_UPDOWN_CUR_SPD, "SERVO_UPDOWN_CUR_SPD", UNIT_MMSEC, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{SET_SERVO_UPDOWN_POS, "SET_SERVO_UPDOWN_POS", UNIT_MM, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{SERVO_UPDOWN_CUR_POS, "SERVO_UPDOWN_CUR_POS", UNIT_MM, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
	{SERVO_UPDOWN_TOQ,	   "SERVO_UPDOWN_TOQ",	   UNIT_TORQUE, MODULE_BC01_BT01, LAYER_BT01, LAYER_BT01},
};

static int G_InfoSize = sizeof(G_InformationLog_Format) / sizeof(STRU_INFORMATION_LOG);

struct STRU_InfoValue
{	
	int   nIndex;
	float nfValue;

	STRU_InfoValue()
	{
		nIndex = 0;
		nfValue = 0;				
	}
	
};
static STRU_InfoValue G_InfoValue;



