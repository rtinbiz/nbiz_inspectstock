#include "StdAfx.h"
#include "InspectPadDataQ.h"

/** 
@brief		Inspect Data Queue Node 생성자.

@remark
-			 생성자
-			
@author		최지형
@date		2012.02.00
*/ 
InspectPadDataQ::InspectPadDataQ()
{
	//	AfxMessageBox("InspectDataQ");
	InitializeCriticalSection(&m_CrtSection);

	EnterCriticalSection(&m_CrtSection);
	//Node Link 정보.
	m_pFrontNode				= nullptr;
	m_pNextNode					= nullptr;

	m_pPADAreaImg				= nullptr;

	//m_MaxDefectCnt	= 0;
	//m_ReviewSizeW	= 0;
	//m_ReviewSizeH	= 0;

	m_PadInScanPos.left			= -1;
	m_PadInScanPos.top			= -1;
	m_PadInScanPos.right		= -1;
	m_PadInScanPos.bottom		= -1;

	m_pSearchRetList			= nullptr;
	m_pCoordinateRetList		= nullptr;
	//m_ppDefectImgList			= nullptr;

	m_ImgFragmentatSizeH		= 0;
	//Inspect Buffer Create 변경
	m_pBufferData				= nullptr;

	LeaveCriticalSection(&m_CrtSection);
}
InspectPadDataQ::InspectPadDataQ(int PadPosIndex, int ImgWidth, int ImgHeight, int ImgFragmentatSizeH, int MaxDefectCnt)//, int ReviewSizeW, int ReviewSizeH)
{
	//	AfxMessageBox("InspectDataQ");
	InitializeCriticalSection(&m_CrtSection);

	EnterCriticalSection(&m_CrtSection);
	//Node Link 정보.
	
	m_pFrontNode				= nullptr;
	m_pNextNode					= nullptr;

	m_pBufferData				= nullptr;

	m_pImgFragmentatStep		= nullptr;
	m_ImgFragmentatSizeH		= ImgFragmentatSizeH;

	m_FragmentRect.x			= 0;
	m_FragmentRect.y			= 0;
	m_FragmentRect.width		= ImgWidth;
	m_FragmentRect.height		= ImgFragmentatSizeH;

	m_PADImageSize				= ImgWidth*ImgFragmentatSizeH;


	m_pImgFragmentat			= cvCreateImage( cvSize(ImgWidth, ImgFragmentatSizeH), IPL_DEPTH_8U, 1);
	cvZero(m_pImgFragmentat);
	//memset(m_pImgFragmentat->imageData, 0xFF, m_pImgFragmentat->imageSize);

	m_PadInScanPos.left			= -1;
	m_PadInScanPos.top			= -1;
	m_PadInScanPos.right		= -1;
	m_PadInScanPos.bottom	= -1;

	m_pPADAreaImg				= cvCreateImage( cvSize(ImgWidth, ImgHeight), IPL_DEPTH_8U, 1);
	//cvZero(m_pPADAreaImg);
	memset(m_pPADAreaImg->imageData, 0xFF, m_pPADAreaImg->imageSize);


	
	m_PadPosIndex   = PadPosIndex;


	m_MaxDefectCnt	= MaxDefectCnt;
	//m_ReviewSizeW	= ReviewSizeW;
	//m_ReviewSizeH	= ReviewSizeH;

	m_pSearchRetList			= new DEFECT_DATA[m_MaxDefectCnt];
	m_pCoordinateRetList		= new COORD_DINFO[m_MaxDefectCnt];
	//m_ppDefectImgList		= new BYTE*[m_MaxDefectCnt];

	//Defect 정보 저장소를 Setting한다.
	memset(m_pSearchRetList, 0x00, sizeof(DEFECT_DATA)*m_MaxDefectCnt);
	memset(m_pCoordinateRetList, 0x00, sizeof(DEFECT_DATA)*m_MaxDefectCnt);

	//for(int CrStep=0; CrStep < m_MaxDefectCnt ; CrStep++)
	//{
	//	m_ppDefectImgList[CrStep] = new BYTE[ReviewSizeW*ReviewSizeH];
	//	memset(m_ppDefectImgList[CrStep], 0x00, sizeof(BYTE)*(ReviewSizeW*ReviewSizeH));
	//}
#ifdef USE_STATIC_BUFFER
	m_pBufferData				= cvCreateImage( cvSize(ImgWidth, ImgHeight), IPL_DEPTH_8U, 1);
	cvZero(m_pBufferData);
	//memset(m_pBufferData->imageData, 0xFF, m_pBufferData->imageSize);
#endif
	LeaveCriticalSection(&m_CrtSection);
}

/** 
@brief		Inspect Data Queue Node 소멸자.

@remark
-			 소멸자
-			
@author		최지형
@date		2012.02.00
*/ 
InspectPadDataQ::~InspectPadDataQ(void)
{
	//AfxMessageBox("~InspectDataQ");

	EnterCriticalSection(&m_CrtSection);

	if(m_pPADAreaImg != nullptr)
		cvReleaseImage(&m_pPADAreaImg);
	m_pPADAreaImg = nullptr;

	if(m_pImgFragmentat != nullptr)
		cvReleaseImage(&m_pImgFragmentat);
	m_pImgFragmentat = nullptr;

	
	delete [] m_pSearchRetList;
	m_pSearchRetList = nullptr;
	delete [] m_pCoordinateRetList ;
	m_pCoordinateRetList = nullptr;
	////Defect 정보를 제거 한다.
	//for(int CrStep=0; CrStep<m_MaxDefectCnt; CrStep++)
	//{
	//	delete [] m_ppDefectImgList[CrStep];
	//	m_ppDefectImgList[CrStep] = nullptr;
	//}
	//delete [] m_ppDefectImgList;
	//m_ppDefectImgList = nullptr;

	//Inspect Buffer Create 변경
	if(m_pBufferData != nullptr)
	{
#ifdef USE_STATIC_BUFFER
		cvReleaseImage(&m_pBufferData);
#else
		delete [] m_pBufferData;
#endif
	}
	LeaveCriticalSection(&m_CrtSection);
	DeleteCriticalSection(&m_CrtSection);
}
void InspectPadDataQ::m_fnQCleanData()
{
	EnterCriticalSection(&m_CrtSection);
	cvZero(m_pPADAreaImg);
	//memset(m_pPADAreaImg->imageData, 0xFF, m_pPADAreaImg->imageSize);

	memset(m_pSearchRetList, 0x00, sizeof(DEFECT_DATA)*m_MaxDefectCnt);
	memset(m_pCoordinateRetList, 0x00, sizeof(DEFECT_DATA)*m_MaxDefectCnt);

	//for(int CrStep=0; CrStep < m_MaxDefectCnt ; CrStep++)
	//{
	//	memset(m_ppDefectImgList[CrStep], 0x00, sizeof(BYTE)*(m_ReviewSizeW*m_ReviewSizeH));
	//}

	LeaveCriticalSection(&m_CrtSection);
}
void InspectPadDataQ::m_fnQSetPadMPosition( ComRect *pNewArea)
{
		m_PadInScanPos = *pNewArea;
}

/** 
@brief		Get Grab Buffer  Data.

@remark
-			 Grab Buffer Pointer를 전달 한다.
-			
@author		최지형
@date		2012.02.00
*/ 
BYTE* InspectPadDataQ::m_fnQGetPADImage()
{
	//단편화 Size만큼 보낸다.
	//EnterCriticalSection(&m_CrtSection);
	m_FragmentRect.y = m_ImgFragmentatSizeH*(*m_pImgFragmentatStep);
	cvSetImageROI(m_pPADAreaImg, m_FragmentRect);
	cvCopyImage(m_pPADAreaImg, m_pImgFragmentat);
	cvResetImageROI(m_pPADAreaImg);

	//{
	//	char TestSaveImage[256];
	//	sprintf_s(TestSaveImage, "D:\\nBiz_InspectStock\\Data\\PADTestImg\\PADImg_%d.jpg",::GetTickCount());
	//	cvSaveImage(TestSaveImage, m_pImgFragmentat);
	//}
	//LeaveCriticalSection(&m_CrtSection);
	return (BYTE*)m_pImgFragmentat->imageData;
}
/** 
@brief		Get Grab Buffer  Data.

@remark
-			 Grab Buffer Pointer를 전달 한다.
-			
@author		최지형
@date		2010.03.00
*/ 
void InspectPadDataQ::m_fnQSetBufferData(BYTE * pNewData)
{
	//EnterCriticalSection(&m_CrtSection);
	//Inspect Buffer Create 변경
#ifdef USE_STATIC_BUFFER
	memcpy(m_pBufferData->imageData, pNewData, m_PADImageSize);
#else
	m_pBufferData = new BYTE[m_PADImageSize];
	memcpy(m_pBufferData, pNewData, m_PADImageSize);
#endif
	//LeaveCriticalSection(&m_CrtSection);
}
/** 
@brief		Get Grab Buffer  Data.

@remark
-			 Grab Buffer Pointer를 전달 한다.
-			
@author		최지형
@date		2010.03.00
*/ 
BYTE* InspectPadDataQ::m_fnQGetBufferData()
{
	//Inspect Buffer Create 변경
#ifdef USE_STATIC_BUFFER
	return (BYTE*)m_pBufferData->imageData;
#else
	return m_pBufferData;
#endif
}
/** 
@brief		Defect Data Set

@remark
-			Inspector에서 Active Search 결과를 저장 한다.
-			
@author		최지형
@date		2012.02.00
*/ 
BOOL InspectPadDataQ::m_fnQSetDefectAllBuffer(int DefectNodeCount, DEFECT_DATA *pNewData)
{
	//EnterCriticalSection(&m_CrtSection);
	if(DefectNodeCount > 0 && DefectNodeCount <= m_MaxDefectCnt )
	{
		m_DefectListCnt = DefectNodeCount;
		memcpy(m_pSearchRetList, pNewData, sizeof(DEFECT_DATA)*m_DefectListCnt);
	}
	else
	{
		m_DefectListCnt = 0;
		memset(m_pSearchRetList, 0x00, sizeof(DEFECT_DATA)*m_MaxDefectCnt);
	}
	m_fnQSetProcessEnd(QDATA_PROC_END_PADINSPECT);
	//Inspect Buffer Create 변경
#ifdef USE_STATIC_BUFFER

#else
	if(m_pBufferData != nullptr)
	{
		delete [] m_pBufferData;
		m_pBufferData = nullptr;
	}
#endif
	//LeaveCriticalSection(&m_CrtSection)
	return TRUE;
}
void InspectPadDataQ::QDataCpy(IplImage *pOrg, CvRect *pOrgPos, CvRect *pDestPos)
{
	EnterCriticalSection(&m_CrtSection);
	cvSetImageROI(pOrg, *pOrgPos);
	cvSetImageROI(m_pPADAreaImg, *pDestPos);
	cvCopyImage(pOrg, m_pPADAreaImg);
	cvResetImageROI(pOrg);
	cvResetImageROI(m_pPADAreaImg);
	LeaveCriticalSection(&m_CrtSection);
}

void InspectPadDataQ::m_fnQSetProcessEnd(int NewState)
{
	if(NewState == QDATA_PROC_END_SORT)
		m_NodeProcState		= QDATA_PROC_END_NONE;//모든 작업 완료.
	else
		m_NodeProcState = NewState;
}
