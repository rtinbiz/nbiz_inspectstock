
#pragma once
//AOI State Flag
// 1)	Idle             0x00：0x01
// 2)	Ready            0x00：0x02
// 3)	Busy             0x00：0x04
// 4)	Alarm            0xXX：0x8N
// 									(N는 Aram 이외 상태 Code, 
// 									XX는 Alarm Code로 별도 정의가 필요 ）
// 5)	Maintenance      0x00：0x08
#define nBiz_AOI_STATE_ERROR				0	//0x0000
#define nBiz_AOI_STATE_IDLE					1	//0x0001
#define nBiz_AOI_STATE_READY				2	//0x0002
#define nBiz_AOI_STATE_BUSY					3	//0x0004
#define nBiz_AOI_STATE_ALARM				4	//0x0080

#define nBiz_ST_CH_OK						1
#define nBiz_ST_CH_ERROR_ALARM_TO_OTHER		2 //		ALARM	==> IDLE Only OK
#define nBiz_ST_CH_ERROR_BUSY_TO_OTHER		3//		BUSY		==> IDLE,				ALARM
#define nBiz_ST_CH_ERROR_READY_TO_OTHER		4//		READY	==> BUSY, IDLE,		ALARM
#define nBiz_ST_CH_ERROR_IDLE_TO_OTHER		5//		IDLE		==> READY,			ALARM
#define nBiz_ST_CH_ERROR_STATE_ERROR		6//		State Change Error
//////////////////////////////////////////////////////////////////////////
#define  PROCESS_STATE_NONE					0
#define  PROCESS_STATE_RUN					1
#define  PROCESS_STATE_STOP					2
#define  PROCESS_STATE_IDLE					3


// Data State
#define QDATA_PROC_END_NONE					0//0x0000
#define QDATA_PROC_END_GRAB					1//0x0001
#define QDATA_PROC_END_CUTTER				2//0x0004
#define QDATA_PROC_END_INSPECT				3//0x0002
#define QDATA_PROC_END_RESULT				4//0x0008
#define QDATA_PROC_END_PADINPUT				5//0x0008
#define QDATA_PROC_END_PADINSPECT			6//0x0008
//#define QDATA_PROC_END_PADRET				7//0x0008
#define QDATA_PROC_END_SORT					8//0x0010

#define RESOLUTION_TYPE_1					1 //고배율 : 1.5, 2.0, 
#define RESOLUTION_TYPE_2					2 //저배율 : 3.0 3.5, 4.0
//Inspector Memory Buffer를 정적으로 생성할지 동적으로 생성할지 결정
//#define USE_STATIC_BUFFER

static const int	NOR_DEFECT		= 11;
static const int	CD_DEFECT		= 12;
//CRect와 혼용.
struct ComRect
{
	LONG    left;
	LONG    top;
	LONG    right;
	LONG    bottom;
};

/** 
@brief		검출 Defect정보를 저장 한다.

@remark
-			 Defect 정보를 처리 하기 위한 기본 Data구조 이다.
-			
@author		최지형
@date		2010.03.00
*/ 
typedef struct DEFECT_DATA_TAG
{
	int		DefectType;
	int		StartX;
	int		StartY;
	int		EndX;
	int		EndY;

	int		nDefectCount;
	int		nDValueMin;
	int		nDValueMax;
	int		nDValueAvg;

	DEFECT_DATA_TAG()
	{
		memset(this,0x00, sizeof(DEFECT_DATA_TAG));
	}

}DEFECT_DATA;


/** 
@brief		좌표계 설정.

@remark
-			 계산된 좌표계에 Defect Image 위치를 지정 한다.
-			
@author		최지형
@date		2010.03.00
*/ 
typedef struct COORDINATE_DEFECT_POSITION_TAG
{
	int		DefectType;
	int		CoodStartX;
	int		CoodStartY;
	int		CoodWidth;
	int		CoodHeight;


	int		nDefectCount;
	int		nDValueMin;
	int		nDValueMax;
	int		nDValueAvg;

	COORDINATE_DEFECT_POSITION_TAG()
	{
		memset(this,0x00, sizeof(COORDINATE_DEFECT_POSITION_TAG));
	}

}COORD_DINFO;

