
#ifndef		ANAIMAOI_INTERFACE_STRUCT
#define		ANAIMAOI_INTERFACE_STRUCT
/**
@struct	SMASK_AOI_IMAGEINFO
@brief	마스크 데이터
@remark	
@author	고정진
@date	2010/4/28  16:47
*/
//struct SAOI_IMAGE_INFO
//{	
//	int		nScaleX1;						///<마스크 패턴의 단위
//	int		nScaleY1;						///<마스크 패턴의 단위
//	int		nScaleX2;						///<마스크 패턴의 단위
//	int		nScaleY2;						///<마스크 패턴의 단위
//	int		nScaleX3;						///<마스크 패턴의 단위
//	int		nScaleY3;						///<마스크 패턴의 단위
//	int		nMinRange;						///<검출 허용치 Default : 60
//	int		nMaxRange;					///<검출 허용치 Default : 60
//	int		nRangeReserve1;				///<검출 허용치 Default : 60
//	int		nOutLineLeft;					///<제거할 테두리 두께. Default : 1
//	int		nOutLineRight;					///<제거할 테두리 두께. Default : 1
//	int		nOutLineUp;					///<제거할 테두리 두께. Default : 1
//	int		nOutLineDown;				///<제거할 테두리 두께. Default : 1
//	int		nBlockCount;					///<_ACC Block, Default : 128
//	int		nThreadCount;					///<_ACC Thread, Default : 64
//	int		nMinimumSizeX;				///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
//	int		nMinimumSizeY;				///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.	
//	int		nMaximumSizeX;				///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
//	int		nMaximumSizeY;				///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.	
//	//char	ch_ACCResultPath[_MAX_PATH];///<_ACC Result Image 를 저장할 경로
//
//	SAOI_IMAGE_INFO()
//	{
//		memset(this, 0x00, sizeof(SAOI_IMAGE_INFO));
//	}
//};
//struct SAOI_IMAGE_INFO
//{         
//	int                  nScaleX1;                                                  ///<마스크 패턴의 단위
//	int                  nScaleY1;                                                   ///<마스크 패턴의 단위
//	int                  nScaleX2;                                                  ///<마스크 패턴의 단위
//	int                  nScaleY2;                                                   ///<마스크 패턴의 단위
//	int                  nScaleX3;                                                  ///<마스크 패턴의 단위
//	int                  nScaleY3;                                                   ///<마스크 패턴의 단위
//
//	int                  nRangeStep1;                                             ///<검출 허용치 Default : 10
//	int                  nRangeStep2;                                             ///<검출 허용치 Default : 20
//	int                  nRangeStep3;                                             ///<검출 허용치 Default : 30
//	int                  nRangeZone1;                                            ///<그레이 레벨 예 : 100
//	int                  nRangeZone2;                                            ///<그레이 레벨 예 : 200
//
//	int                  nOutLineLeft;                                              ///<제거할 테두리 두께. Default : 1
//	int                  nOutLineRight;                                            ///<제거할 테두리 두께. Default : 1
//	int                  nOutLineUp;                                                ///<제거할 테두리 두께. Default : 1
//	int                  nOutLineDown;                                            ///<제거할 테두리 두께. Default : 1
//	int                  nBlockCount;                                              ///<_ACC Block, Default : 128
//	int                  nThreadCount;                                            ///<_ACC Thread, Default : 64
//	int                  nMinimumSizeX;                                          ///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
//	int                  nMinimumSizeY;                                          ///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.
//	int                  nMaximumSizeX;                                         ///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
//	int                  nMaximumSizeY;                                         ///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.
//	SAOI_IMAGE_INFO()
//	{
//		memset(this, 0x00, sizeof(SAOI_IMAGE_INFO));
//	}
//};
struct SAOI_IMAGE_INFO
{          
	int                  nScaleX1;                                                               ///<마스크 패턴의 단위(Active)
	int                  nScaleY1;                                                               ///<마스크 패턴의 단위(Active)
	int                  nScaleX2;                                                               ///<마스크 패턴의 단위(Left Pad)
	int                  nScaleY2;                                                               ///<마스크 패턴의 단위(Left Pad)
	int                  nScaleX3;                                                               ///<마스크 패턴의 단위(Right Pad)
	int                  nScaleY3;                                                               ///<마스크 패턴의 단위(Right Pad)
	int                  nMinRangeMain;                                           ///<검출 허용치 Default : 60(Active)
	int                  nMaxRangeMain;                                ///<검출 허용치 Default : 60(Active)
	int                  nMinRangeLPad;                                 ///<검출 허용치 Default : 60(Left Pad)
	int                  nMaxRangeLPad;                                 ///<검출 허용치 Default : 60(Left Pad)
	int                  nMinRangeRPad;                                 ///<검출 허용치 Default : 60(Right Pad)
	int                  nMaxRangeRPad;                                ///<검출 허용치 Default : 60(Right Pad)
	int                  nOutLineLeft;                                                ///<제거할 테두리 두께. Default : 1
	int                  nOutLineRight;                                              ///<제거할 테두리 두께. Default : 1
	int                  nOutLineUp;                                                 ///<제거할 테두리 두께. Default : 1
	int                  nOutLineDown;                                  ///<제거할 테두리 두께. Default : 1
	int                  nBlockCount;                                                ///<_ACC Block, Default : 128
	int                  nThreadCount;                                              ///<_ACC Thread, Default : 64
	int                  nMinimumSizeX;                                 ///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
	int                  nMinimumSizeY;                                  ///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.
	int                  nMaximumSizeX;                                 ///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
	int                  nMaximumSizeY;                                 ///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.
	SAOI_IMAGE_INFO()
	{
		memset(this, 0x00, sizeof(SAOI_IMAGE_INFO));
	}
};
/**
@struct	SCOMMON_AOI_PARA
@brief	공용 파라미터 구조체
@remark	
-		
@author	고정진
@date	2010/4/29  11:15
*/
struct SAOI_COMMON_PARA
{	
	int		nDefectDistance;		///<동일 결함으로 간주할 거리.				Default 100 Pixel	
	int		nImageSizeX;			///<이미지의 X축 길이
	int		nImageSizeY;			///<이미지의 Y축 길이
	int		nMaskType;				 ///<마스크의 사이즈       1 : 1, 2 : 5, 3 : 9

	int		nZoneStep[5];
	int		nZoneOffset[5];
	int		bImageLogUsed;			///<이미지 로그 생성 유무					true or false

	SAOI_COMMON_PARA()
	{
	//	int SizeofTyup = sizeof(SAOI_COMMON_PARA);
		memset(this, 0x00, sizeof(SAOI_COMMON_PARA));
		nMaskType = 100;
	}
};

/**
@struct	SAOI_PAD_POS
@brief	공용 파라미터 구조체
@remark	
-		
@author	고정진
@date	2012/5/2  11:15
*/
struct SAOI_PAD_POS
{	
	int		nStartX;		
	int		nStartY;			
	int		nEndX;			
	int		nEndY;

	SAOI_PAD_POS()
	{
		nStartX	=-1;		
		nStartY	=-1;			
		nEndX	=-1;		
		nEndY	=-1;
	}
};


/**
@struct	SAOI_PAD_AREA
@brief	공용 파라미터 구조체
@remark	
-		
@author	고정진
@date	2012/5/2  11:15
*/
struct SAOI_PAD_AREA
{	
	SAOI_PAD_POS stTop;
	SAOI_PAD_POS stDown;
	SAOI_PAD_POS stLeft;
	SAOI_PAD_POS stRight;
};

#endif