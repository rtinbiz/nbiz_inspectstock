
#include "StdAfx.h"
#include "CoordinatesProc.h"

void FillRectDraw(IplImage *pTargetImg, CRect *pDrawData, CvScalar DrawColor)
{
	CvPoint imgb_rectangle_pts[4] = {	cvPoint(pDrawData->left, pDrawData->top),
													cvPoint(pDrawData->right, pDrawData->top), 
													cvPoint(pDrawData->right, pDrawData->bottom),
													cvPoint(pDrawData->left, pDrawData->bottom)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pTargetImg , PointArray, PolyVertexNumber, PolyNumber, DrawColor);
}


CoordinatesProc::CoordinatesProc()
{
	m_TargetTaskNum = 0;
	memset(&m_HWParam,		0x00,		sizeof(HW_PARAM));
	memset(&m_GlassParam ,	0x00,		sizeof(GLASS_PARAM));
	m_nBlockCnt		= 0;
	m_pBlockList		= nullptr;
	m_nCellCnt			= 0;
	m_pCellList			= nullptr;
	m_nPadCellCnt		= 0;
	m_pPadCellList		= nullptr;
	m_nScanImgCnt	= 0;
	m_pScanImgList	= nullptr;

	m_TotalScnaLineCnt		= 0;
	m_OneLineScnaImgCnt	= 0;

	cvInitFont (&m_BlockNameFont, CV_FONT_HERSHEY_TRIPLEX , 0.6f, 0.7f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	cvInitFont (&m_CellNameFont, CV_FONT_HERSHEY_COMPLEX , 0.4f, 0.5f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX


	m_AXIS_Y_OVER_STEP = 600;

	m_pMainMpaDrawObj  =  cvCreateImage(cvSize(850, 1050), 8, 3);

	m_ResolutionType = RESOLUTION_TYPE_1;

	m_RealScanType = -1;
	m_RealCellCnt = 0;
	m_pRealCellPosList = nullptr;
}
CoordinatesProc::~CoordinatesProc()
{
	if(m_pBlockList	 != nullptr)
		delete [] m_pBlockList;
	m_pBlockList = nullptr;

	if(m_pCellList != nullptr)
		delete [] m_pCellList;
	m_pCellList = nullptr;

	if(m_pPadCellList != nullptr)
		delete [] m_pPadCellList;
	m_pPadCellList = nullptr;

	if(m_pScanImgList != nullptr)
		delete [] m_pScanImgList;
	m_pScanImgList = nullptr;

	if(m_pMainMpaDrawObj != nullptr)
		cvReleaseImage(&m_pMainMpaDrawObj);
	m_pMainMpaDrawObj = nullptr;

	if(m_pRealCellPosList != nullptr)
		delete [] m_pRealCellPosList;
	m_pRealCellPosList = nullptr;

}
/*
*	Module Name		:	SetWorkZeroPos
*	Parameter		:	 Glass Info
*	Return			:	TRUE/FALSE
*	Function		:	Work 좌표와, Class내의 미리 연사가능 변수의 초기화.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool	CoordinatesProc::m_fnSetRecipeData(int TargetTaskNum, GLASS_PARAM *	pGlassParam)
{
	m_TargetTaskNum = TargetTaskNum;
	//if(pHWParam == nullptr)
	//	return false;
	//memcpy(&m_HWParam, pHWParam, sizeof(HW_PARAM));

	if(pGlassParam == nullptr)
		return false;

	memcpy(&m_GlassParam, pGlassParam, sizeof(GLASS_PARAM));

	//Glass Block/Cell정보를 계산한다.(Real Pos Data생성 여부, 입력 Glass Info와 Total Cnt의 동일 여부.)
	if(m_pRealCellPosList == nullptr && m_RealCellCnt != (pGlassParam->GLASS_CELL_COUNT_X * pGlassParam->GLASS_CELL_COUNT_Y))
		m_fnGlassDataCalculate();
	else
		m_fnGlassDataCalculate(m_RealCellCnt, m_pRealCellPosList);

	m_fnSetRealCellPos(0,0, nullptr, nullptr);//전송 받은 Real Cell Data입력이 끝났음으로 Clear 한다.
	//H/W정보를 이용하여 Scan 위치를 계산 한다.
	m_fnGlassScanCalculate();

	//Glass정보와 Scan정보를 이용 Pad부 정보를 갱신한다.
	m_fnGlassPADAreaCalculate();

	return true;
}
bool	CoordinatesProc::m_fnSetRealCellPos(int RealCellCntX, int RealCellCntY, int *pDisListX, int *pDisListY )
{
	m_RealCellCnt = 0;

	if(m_pRealCellPosList != nullptr)
		delete [] m_pRealCellPosList;
	m_pRealCellPosList = nullptr;

	if(RealCellCntX>0 && RealCellCntY>0)
	{
		m_RealCellCnt = RealCellCntX * RealCellCntY;
		m_pRealCellPosList = new RealCellPos[m_RealCellCnt];
		int InputIndex = 0;
		for(int CellYStep = 0; CellYStep< RealCellCntY; CellYStep++)
		{
			for(int CellXStep = 0; CellXStep< RealCellCntX; CellXStep++)
			{
				m_pRealCellPosList[InputIndex].PosStartX = *(pDisListX+(CellXStep*2));
				m_pRealCellPosList[InputIndex].PosStartY = *(pDisListY+(CellYStep*2));
				m_pRealCellPosList[InputIndex].PosEndX = *(pDisListX+(CellXStep*2)+1);
				m_pRealCellPosList[InputIndex].PosEndY = *(pDisListY+(CellYStep*2)+1);;

				InputIndex++;
			}
		}
	}
	else
	{
		m_RealCellCnt = 0;
		m_pRealCellPosList = nullptr;
		return false;
	}
	return true;
}
bool	CoordinatesProc::m_fnSetRealCellPos(int RealCellCntX, int RealCellCntY,CvRect *pDataList )
{
	m_RealCellCnt = 0;
	if(m_pRealCellPosList != nullptr)
		delete [] m_pRealCellPosList;
	m_pRealCellPosList = nullptr;

	if(RealCellCntX>0 && RealCellCntY>0)
	{
		m_RealCellCnt = RealCellCntX * RealCellCntY;
		m_pRealCellPosList = new RealCellPos[m_RealCellCnt];
		int InputIndex = 0;
		for(int CellYStep = 0; CellYStep< RealCellCntY; CellYStep++)
		{
			for(int CellXStep = 0; CellXStep< RealCellCntX; CellXStep++)
			{
				CvRect *pRectData =  (pDataList+CellXStep+(CellYStep*RealCellCntX));
				m_pRealCellPosList[InputIndex].PosStartX = pRectData->x;
				m_pRealCellPosList[InputIndex].PosStartY = pRectData->y;
				m_pRealCellPosList[InputIndex].PosEndX =  pRectData->x+pRectData->width;
				m_pRealCellPosList[InputIndex].PosEndY =  pRectData->y+pRectData->height;

				InputIndex++;
			}
		}
	}
	else
	{
		m_RealCellCnt = 0;
		m_pRealCellPosList = nullptr;
		return false;
	}
	return true;
}
bool CoordinatesProc::m_fnActivePadAreaLRFind(int ScnaNum, int *pLeftStartLX, int *pLeftEndX, int *pRightStartX, int *pRightEndX)
{
	if(m_pCellList == nullptr || m_pScanImgList == nullptr)
		return false;
	SCANIMGINFO* pCoodArea = nullptr;

	CRect LeftPadTotalArea[4]={CRect(-1, -1, -1, -1),CRect(-1, -1, -1, -1), CRect(-1, -1, -1, -1), CRect(-1, -1, -1, -1)};
	CRect RightPadTotalArea[4]={CRect(-1, -1, -1, -1),CRect(-1, -1, -1, -1), CRect(-1, -1, -1, -1), CRect(-1, -1, -1, -1)};
	for(int ImgIndex = 1; ImgIndex < m_OneLineScnaImgCnt ; ImgIndex++)
	{
		pCoodArea = m_fnGetCalculateAreaInImg(ScnaNum, ImgIndex);
		if(pCoodArea != nullptr)
		{
			//if(pCoodArea->m_findSubRecCnt[3]>0 && pCoodArea->m_findSubRecCnt[4])
			{
				for(int CutStep =0; CutStep<pCoodArea->m_findSubRecCnt[3]; CutStep++)
				{//좌
					LeftPadTotalArea[CutStep] = pCoodArea->m_InImgPositionRect[3][CutStep];
				}
				for(int CutStep =0; CutStep<pCoodArea->m_findSubRecCnt[4]; CutStep++)
				{//우
					RightPadTotalArea[CutStep] = pCoodArea->m_InImgPositionRect[4][CutStep];
				}

				CRect MergeLeftRect = LeftPadTotalArea[0];
				for(int CutStep =1; CutStep<pCoodArea->m_findSubRecCnt[3]; CutStep++)
				{//좌
					MergeLeftRect.UnionRect(MergeLeftRect, LeftPadTotalArea[CutStep]);
				}

				CRect MergeRightRect = RightPadTotalArea[0];
				for(int CutStep =1; CutStep<pCoodArea->m_findSubRecCnt[4]; CutStep++)
				{//좌
					MergeRightRect.UnionRect(MergeRightRect, RightPadTotalArea[CutStep]);
				}

				//좌.
				*pLeftStartLX	= MergeLeftRect.left;
				*pLeftEndX		= MergeLeftRect.right;
				//우
				*pRightStartX	=  MergeRightRect.left;
				*pRightEndX		= MergeRightRect.right;
				//break;
			}
		}
	}
	return true;
}

bool CoordinatesProc::m_fnUpDnPadAlignCoordCalc(int ScnaNum, int UpDnIndex, PAlignCoordInfo *pAlignCoordData)
{

	if(m_pCellList == nullptr || m_pScanImgList == nullptr)
		return false;
	SCANIMGINFO* pCoodArea = nullptr;

	CRect UpPadTotalArea[4]={CRect(-1, -1, -1, -1),CRect(-1, -1, -1, -1), CRect(-1, -1, -1, -1), CRect(-1, -1, -1, -1)};

	CRect BigRect, SmallRect;
	BigRect.left = -1;
	BigRect.right = -1;
	BigRect.top =-1;
	BigRect.bottom =-1;

	pAlignCoordData->m_BigAreaStartX = -1;
	pAlignCoordData->m_BigAreaEndX  = -1;
	for(int ImgIndex = 1; ImgIndex < m_OneLineScnaImgCnt ; ImgIndex++)
	{
		pCoodArea = m_fnGetCalculateAreaInImg(ScnaNum, ImgIndex);
		if(pCoodArea != nullptr)
		{

			if(pCoodArea->m_findSubRecCnt[UpDnIndex]>0)
			{
				for(int CutStep =0; CutStep<pCoodArea->m_findSubRecCnt[UpDnIndex]; CutStep++)
				{//
					UpPadTotalArea[CutStep] = pCoodArea->m_InImgPositionRect[UpDnIndex][CutStep];
				}

				for(int CutStep =0; CutStep<pCoodArea->m_findSubRecCnt[UpDnIndex]; CutStep++)
				{//
					
					if(BigRect.Width() < UpPadTotalArea[CutStep].Width())
						BigRect = UpPadTotalArea[CutStep];
				}

				pAlignCoordData->m_BigAreaStartX = BigRect.left;
				pAlignCoordData->m_BigAreaEndX  = BigRect.right;


				break;
			}
		}
	}
	return true;
}
/*
*	Module Name		:	SetWorkZeroPos
*	Parameter		:	 Glass Info
*	Return			:	TRUE/FALSE
*	Function		:	Work 좌표와, Class내의 미리 연사가능 변수의 초기화.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CoordinatesProc::m_fnGlassDataCalculate(int RealCellCnt, RealCellPos *pInputRealCellPosList)
{
	//////////////////////////////////////////////////////////////////////////
	//1) 이전 항목이 있다면 Clear
	if(m_pBlockList	 != nullptr)
		delete [] m_pBlockList;
	m_nBlockCnt		= m_GlassParam.GLASS_BLOCK_COUNT_X*m_GlassParam.GLASS_BLOCK_COUNT_Y;
	m_pBlockList = new BLOCKINFO[m_nBlockCnt];
	
	if(m_pCellList != nullptr)
		delete [] m_pCellList;
	m_nCellCnt = m_GlassParam.GLASS_CELL_COUNT_X*m_GlassParam.GLASS_CELL_COUNT_Y;
	m_pCellList = new CELLINFO[m_nCellCnt];

	if(m_pPadCellList != nullptr)
		delete [] m_pPadCellList;
	m_nPadCellCnt = m_GlassParam.GLASS_CELL_COUNT_X*m_GlassParam.GLASS_CELL_COUNT_Y;
	m_pPadCellList = new PADINFO[m_nPadCellCnt];

	//////////////////////////////////////////////////////////////////////////
	//2) Block Position Set.
	BLOCKINFO *pBlockBuf		= nullptr;
	CELLINFO	*pCellBuf		= nullptr;
	PADINFO		*pPadCellBuff	= nullptr;
#ifdef VIEW_CREATE_MAP
	IplImage *pViewMap = cvCreateImage(cvSize(850, 1050), 8, 3);
	cvNamedWindow("MapViewer");
#endif
	int BlockDataSetIndex = 0;
	int CellDataSetIndex  =0;

	int CellCntInBlockX = m_GlassParam.GLASS_CELL_COUNT_X/m_GlassParam.GLASS_BLOCK_COUNT_X;
	int CellCntInBlockY = m_GlassParam.GLASS_CELL_COUNT_Y/m_GlassParam.GLASS_BLOCK_COUNT_Y;

	int CellStartPosX = 0;
	int CellStartPosY = 0;
	
	RealCellPos *pRealCellPosList = nullptr;
	if( RealCellCnt == m_nCellCnt)
		pRealCellPosList = pInputRealCellPosList;
	
	for(int BlockYStep = 0; BlockYStep<m_GlassParam.GLASS_BLOCK_COUNT_Y; BlockYStep++)
	{
		for(int BlockXStep = 0; BlockXStep<m_GlassParam.GLASS_BLOCK_COUNT_X; BlockXStep++)
		{
			pBlockBuf = &m_pBlockList[BlockDataSetIndex++];

			pBlockBuf->m_BlockPos.left		= m_GlassParam.CenterPointX + (m_GlassParam.CELL_EDGE_X			+ (BlockXStep*m_GlassParam.BLOCK_DISTANCE_X));
			pBlockBuf->m_BlockPos.top		= m_GlassParam.CenterPointY + (m_GlassParam.CELL_EDGE_Y*(-1)	+ (BlockYStep*m_GlassParam.BLOCK_DISTANCE_Y));

			pBlockBuf->m_BlockPos.right		= pBlockBuf->m_BlockPos.left + (((CellCntInBlockX-1)*m_GlassParam.CELL_DISTANCE_X))+ m_GlassParam.CELL_SIZE_X;
			pBlockBuf->m_BlockPos.bottom	= pBlockBuf->m_BlockPos.top + (((CellCntInBlockY-1)*m_GlassParam.CELL_DISTANCE_Y))+ m_GlassParam.CELL_SIZE_Y;

			sprintf_s(pBlockBuf->m_BlockName,"Block %c%d", 'A'+BlockXStep, BlockYStep+1);
#ifdef VIEW_CREATE_MAP
			cvDrawRect(pViewMap, cvPoint(pBlockBuf->m_BlockPos.left/1000, pBlockBuf->m_BlockPos.top/1000),
				cvPoint(pBlockBuf->m_BlockPos.right/1000, pBlockBuf->m_BlockPos.bottom/1000), CV_RGB(255, 0, 0));
			cvShowImage("MapViewer", pViewMap);
			cvWaitKey(200);
#endif
			//3)Cell Position Set.
			CellStartPosX = pBlockBuf->m_BlockPos.left;
			CellStartPosY = pBlockBuf->m_BlockPos.top;
			for(int CellYStep = 0; CellYStep<CellCntInBlockY; CellYStep++)
			{
				for(int CellXStep = 0; CellXStep<CellCntInBlockX; CellXStep++)
				{
					pCellBuf			= &m_pCellList[CellDataSetIndex];
					pPadCellBuff	= &m_pPadCellList[CellDataSetIndex];

					if(pRealCellPosList != nullptr)
					{
						pCellBuf->m_CellPos.left		= m_GlassParam.CenterPointX + (m_GlassParam.CELL_EDGE_X)			+ pRealCellPosList[CellDataSetIndex].PosStartX;
						pCellBuf->m_CellPos.top		= m_GlassParam.CenterPointY + (m_GlassParam.CELL_EDGE_Y*(-1))	+ pRealCellPosList[CellDataSetIndex].PosStartY;
						pCellBuf->m_CellPos.right		= m_GlassParam.CenterPointX + (m_GlassParam.CELL_EDGE_X)			+ pRealCellPosList[CellDataSetIndex].PosEndX;
						pCellBuf->m_CellPos.bottom	= m_GlassParam.CenterPointY + (m_GlassParam.CELL_EDGE_Y*(-1))	+ pRealCellPosList[CellDataSetIndex].PosEndY;
					}
					else
					{
						pCellBuf->m_CellPos.left		= CellStartPosX + (CellXStep*m_GlassParam.CELL_DISTANCE_X);
						pCellBuf->m_CellPos.top		= CellStartPosY + (CellYStep*m_GlassParam.CELL_DISTANCE_Y);
						pCellBuf->m_CellPos.right		= pCellBuf->m_CellPos.left + m_GlassParam.CELL_SIZE_X;
						pCellBuf->m_CellPos.bottom	= pCellBuf->m_CellPos.top + m_GlassParam.CELL_SIZE_Y;
					}
					CellDataSetIndex++;


					sprintf_s(pCellBuf->m_CellName,"A%c%02d", 'A'+CellXStep+(BlockXStep*CellCntInBlockX), CellYStep + (BlockYStep*CellCntInBlockY)+1);

					//////////////////////////////////////////////////////////////////////////
					//Pad부 좌표. 잡기.
					memcpy(pPadCellBuff->m_CellName, pCellBuf->m_CellName, NAME_BUF_CNT);
					//1:상하/양쪽검사시 Pad부 자르기.
					if(m_HWParam.PadInspectAreaType == INSPACT_PAD_UPDOWN || m_HWParam.PadInspectAreaType == INSPACT_PAD_BOTH)
					{
						//<<<Cell의 상단 Pad부
						pPadCellBuff->m_CellPos_Top.left			= pCellBuf->m_CellPos.left			+ m_GlassParam.ActiveDeadZoneSize;//- m_GlassParam.PadStartDistanceX;
						pPadCellBuff->m_CellPos_Top.top			= pCellBuf->m_CellPos.top			- m_GlassParam.PadStartDistanceY;
						pPadCellBuff->m_CellPos_Top.right			= pCellBuf->m_CellPos.right		- m_GlassParam.ActiveDeadZoneSize;//+ m_GlassParam.PadEndDistanceX;
						pPadCellBuff->m_CellPos_Top.bottom		= pCellBuf->m_CellPos.top;
						//<<<Cell의 하단 Pad부
						pPadCellBuff->m_CellPos_Bottom.left		= pCellBuf->m_CellPos.left			+ m_GlassParam.ActiveDeadZoneSize;//- m_GlassParam.PadStartDistanceX;
						pPadCellBuff->m_CellPos_Bottom.top		= pCellBuf->m_CellPos.bottom;
						pPadCellBuff->m_CellPos_Bottom.right		= pCellBuf->m_CellPos.right		- m_GlassParam.ActiveDeadZoneSize;//+ m_GlassParam.PadEndDistanceX;
						pPadCellBuff->m_CellPos_Bottom.bottom	= pCellBuf->m_CellPos.bottom		+ m_GlassParam.PadEndDistanceY;

						{//Y Axis Align을 위해 ActivePadDeadZone/2 가량 추가 한다.
							
							pPadCellBuff->m_CellPos_Top.top			-= m_GlassParam.ActiveDeadZoneSize;
							pPadCellBuff->m_CellPos_Top.bottom		+= (m_GlassParam.ActiveDeadZoneSize*2);

							pPadCellBuff->m_CellPos_Bottom.top		-= (m_GlassParam.ActiveDeadZoneSize*2);
							pPadCellBuff->m_CellPos_Bottom.bottom	+= m_GlassParam.ActiveDeadZoneSize;

						}

						//<<<Cell의 왼쪽단 Pad부
						pPadCellBuff->m_CellPos_Left.left			= pCellBuf->m_CellPos.left			- m_GlassParam.PadStartDistanceX;
						pPadCellBuff->m_CellPos_Left.top			= pCellBuf->m_CellPos.top;
						pPadCellBuff->m_CellPos_Left.right			= pCellBuf->m_CellPos.left;
						pPadCellBuff->m_CellPos_Left.bottom		= pCellBuf->m_CellPos.bottom;
						//<<<Cell의 오른쪽단 Pad부
						pPadCellBuff->m_CellPos_Right.left			= pCellBuf->m_CellPos.right;			
						pPadCellBuff->m_CellPos_Right.top			= pCellBuf->m_CellPos.top;
						pPadCellBuff->m_CellPos_Right.right			= pCellBuf->m_CellPos.right		+  m_GlassParam.PadEndDistanceX;
						pPadCellBuff->m_CellPos_Right.bottom		= pCellBuf->m_CellPos.bottom;


						{
							//<<<Cell의 왼쪽단 Pad부
							pPadCellBuff->m_CellPos_Left.top			+= m_GlassParam.ActivePadDeadZone;
							pPadCellBuff->m_CellPos_Left.bottom		-= m_GlassParam.ActivePadDeadZone;
							//<<<Cell의 오른쪽단 Pad부
							pPadCellBuff->m_CellPos_Right.top			+= m_GlassParam.ActivePadDeadZone;
							pPadCellBuff->m_CellPos_Right.bottom		-= m_GlassParam.ActivePadDeadZone;
						}

					}
					else if(m_HWParam.PadInspectAreaType == INSPACT_PAD_LEFTRIGHT)
					{
						//<<<Cell의 상단 Pad부
						pPadCellBuff->m_CellPos_Top.left			= pCellBuf->m_CellPos.left;
						pPadCellBuff->m_CellPos_Top.top			= pCellBuf->m_CellPos.top			- m_GlassParam.PadStartDistanceY;
						pPadCellBuff->m_CellPos_Top.right			= pCellBuf->m_CellPos.right;
						pPadCellBuff->m_CellPos_Top.bottom		= pCellBuf->m_CellPos.top;
						//<<<Cell의 하단 Pad부
						pPadCellBuff->m_CellPos_Bottom.left		= pCellBuf->m_CellPos.left;
						pPadCellBuff->m_CellPos_Bottom.top		= pCellBuf->m_CellPos.bottom;
						pPadCellBuff->m_CellPos_Bottom.right		= pCellBuf->m_CellPos.right;
						pPadCellBuff->m_CellPos_Bottom.bottom	= pCellBuf->m_CellPos.bottom		+ m_GlassParam.PadEndDistanceY;
						//<<<Cell의 왼쪽단 Pad부
						pPadCellBuff->m_CellPos_Left.left			= pCellBuf->m_CellPos.left			- m_GlassParam.PadStartDistanceX;
						pPadCellBuff->m_CellPos_Left.top			= pCellBuf->m_CellPos.top			- m_GlassParam.PadStartDistanceY;
						pPadCellBuff->m_CellPos_Left.right			= pCellBuf->m_CellPos.left;
						pPadCellBuff->m_CellPos_Left.bottom		= pCellBuf->m_CellPos.bottom		+ m_GlassParam.PadEndDistanceY;
						//<<<Cell의 오른쪽단 Pad부
						pPadCellBuff->m_CellPos_Right.left			= pCellBuf->m_CellPos.right;			
						pPadCellBuff->m_CellPos_Right.top			= pCellBuf->m_CellPos.top			- m_GlassParam.PadStartDistanceY;
						pPadCellBuff->m_CellPos_Right.right			= pCellBuf->m_CellPos.right		+ m_GlassParam.PadEndDistanceX;
						pPadCellBuff->m_CellPos_Right.bottom		= pCellBuf->m_CellPos.bottom		+ m_GlassParam.PadEndDistanceY;
					}

					//Acive Dead Zone 설정.
					pCellBuf->m_CellPos.left		+= m_GlassParam.ActiveDeadZoneSize;
					pCellBuf->m_CellPos.top		+= m_GlassParam.ActiveDeadZoneSize;
					pCellBuf->m_CellPos.right		-= m_GlassParam.ActiveDeadZoneSize;
					pCellBuf->m_CellPos.bottom	-= m_GlassParam.ActiveDeadZoneSize;

#ifdef VIEW_CREATE_MAP
					cvDrawRect(pViewMap, cvPoint(pCellBuf->m_CellPos.left/1000, pCellBuf->m_CellPos.top/1000),
						cvPoint(pCellBuf->m_CellPos.right/1000, pCellBuf->m_CellPos.bottom/1000), CV_RGB(0, 255, 0));
					cvShowImage("MapViewer", pViewMap);
					cvWaitKey(20);
#endif
				}
			}
		}
	}

#ifdef VIEW_CREATE_MAP
	cvShowImage("MapViewer", pViewMap);
	cvWaitKey(500);
	cvDestroyAllWindows();
	cvReleaseImage(&pViewMap);
#endif
	return true;
}
/*
*	Module Name		:	SetScanArea
*	Parameter		:	 HW 정보와 Glass정보로 Scan Area를 계산 한다.
*	Return			:	TRUE/FALSE
*	Function		:	
*	Create			:	2012.00.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool 	CoordinatesProc::m_fnGlassScanCalculate()
{
	int InspectorIndex  = -1;
	switch (m_TargetTaskNum)
	{
	case TASK_21_Inspector: InspectorIndex =0;
		break;
	//case TN_INSPECTOR1: InspectorIndex =0;
	//	break;
	//case TN_INSPECTOR2: InspectorIndex =1;
	//	break;
	//case TN_INSPECTOR3: InspectorIndex =2;
	//	break;
	//case TN_INSPECTOR4: InspectorIndex =3;
	//	break;
	default:
		return false;
	}
	int	  OverLapYAxis		= (int)(m_AXIS_Y_OVER_STEP*m_GlassParam.m_ScanImgResolution_H);
	float OverLapSize		= (m_GlassParam.m_ScanImgResolution_W * (float)m_HWParam.m_ProcImageW) - (float)m_GlassParam.m_ScanLineShiftDistance;

	//float LineImageCntW		= (float)m_HWParam.m_INSPECTION_SCAN_WIDTH/((m_GlassParam.m_ScanImgResolution_W * (float)m_HWParam.m_ProcImageW)-OverLapSize);
	float LineImageCntW		= (float)m_GlassParam.STICK_WIDTH/((m_GlassParam.m_ScanImgResolution_W * (float)m_HWParam.m_ProcImageW)-OverLapSize);
	float LineImageCntH		= (float)m_HWParam.m_INSPECTION_SCAN_HEIGHT/(m_GlassParam.m_ScanImgResolution_H * (float)m_HWParam.m_ProcImageH);

	//정수로 나누어 떨어지지 않는다면(소수점 존재) +1회의 스켄을 수행한다.
	float TotalLineByCam	= LineImageCntW/ ((float)m_HWParam.m_INSPECTION_CAM_COUNT);

	m_AOIOneCam_ScanLineCnt	= (TotalLineByCam -(int)TotalLineByCam) != 0 ? (int)TotalLineByCam+1:(int)TotalLineByCam;
	m_TotalScnaLineCnt		= m_AOIOneCam_ScanLineCnt *m_HWParam.m_INSPECTION_CAM_COUNT;
	m_OneLineScnaImgCnt		= (LineImageCntH -(int)LineImageCntH) != 0 ? (int)LineImageCntH+1:(int)LineImageCntH;


	//////////////////////////////////////////////////////////////////////////
	//Scan Area Calculate.
	if(m_pScanImgList != nullptr)
		delete [] m_pScanImgList;
	m_nScanImgCnt = m_AOIOneCam_ScanLineCnt*m_OneLineScnaImgCnt;
	m_pScanImgList =  new SCANIMGINFO[m_nScanImgCnt];

	int ImageIndex  = 0;

	int ImageSizeWum = (int)(m_GlassParam.m_ScanImgResolution_W * (float)m_HWParam.m_ProcImageW);
	int ImageSizeHum = (int)(m_GlassParam.m_ScanImgResolution_H * (float)m_HWParam.m_ProcImageH);

	int CamBlockStartPosX = m_HWParam.m_AOI_BLOCK_SCAN_START_X + m_HWParam.m_AOI_CAM_DISTANCE_X;
	int CamBlockStartPosY = m_HWParam.m_AOI_BLOCK_SCAN_START_Y + m_HWParam.m_AOI_CAM_DISTANCE_Y;

	//int CamBlockStartPosYEven = (CamBlockStartPosY + m_HWParam.m_INSPECTION_SCAN_HEIGHT) - ImageSizeHum;
	int CamBlockStartPosYEven = (CamBlockStartPosY + (ImageSizeHum*m_OneLineScnaImgCnt)) - ImageSizeHum;
#ifdef VIEW_CREATE_SCAN_PAD
	IplImage *pViewMap = cvCreateImage(cvSize(850, 1050), 8, 3);
	cvNamedWindow("ScnaMapViewer");
#endif
	SCANIMGINFO * pScanImgBuf = nullptr;
	for(int LineStep = 0; LineStep<m_AOIOneCam_ScanLineCnt; LineStep++)
	{
		for(int ImgStep = 0; ImgStep<m_OneLineScnaImgCnt; ImgStep++)
		{
			pScanImgBuf = &m_pScanImgList[ImageIndex++];

			//pScanImgBuf->m_LineTotalCnt = m_AOIOneCam_ScanLineCnt;
			//pScanImgBuf->m_LineTotalImageCnt = m_OneLineScnaImgCnt;
			pScanImgBuf->m_ScanLineNum	= LineStep +1 /*(InspectorIndex*m_AOIOneCam_ScanLineCnt)*/ ;
			pScanImgBuf->m_ScanImgIndex	= ImgStep +1;

			if((LineStep+1)%2 == 1)
			{
				//////////////////////////////////////////////////////////////////////////
				//1)홀수 Scan시.
				pScanImgBuf->m_ScanImgPos.left		= (CamBlockStartPosX - (ImageSizeWum/2)) + LineStep*m_GlassParam.m_ScanLineShiftDistance;
				pScanImgBuf->m_ScanImgPos.top		= CamBlockStartPosY + (ImageSizeHum*ImgStep) + m_HWParam.m_AOI_SCAN_START_OFFSET_ODD;

				pScanImgBuf->m_ScanImgPos.right		= pScanImgBuf->m_ScanImgPos.left + ImageSizeWum;
				pScanImgBuf->m_ScanImgPos.bottom		= pScanImgBuf->m_ScanImgPos.top + ImageSizeHum;

				//pScanImgBuf->m_ScanImgPos.right		= pScanImgBuf->m_ScanImgPos.left + (ImageSizeWum-1);
				//pScanImgBuf->m_ScanImgPos.bottom		= pScanImgBuf->m_ScanImgPos.top + (ImageSizeHum-1);

				pScanImgBuf->m_ScanImgPos.top		-= OverLapYAxis;
				pScanImgBuf->m_ScanImgPos.bottom		+= OverLapYAxis;
			}
			else
			{
				//////////////////////////////////////////////////////////////////////////
				//2)짝수 Scan시.
				pScanImgBuf->m_ScanImgPos.left		= (CamBlockStartPosX - (ImageSizeWum/2))+ LineStep*m_GlassParam.m_ScanLineShiftDistance;
				pScanImgBuf->m_ScanImgPos.top		= CamBlockStartPosYEven - (ImageSizeHum*ImgStep) + m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN;

				pScanImgBuf->m_ScanImgPos.right		= pScanImgBuf->m_ScanImgPos.left + ImageSizeWum;
				pScanImgBuf->m_ScanImgPos.bottom		= pScanImgBuf->m_ScanImgPos.top + ImageSizeHum;

				//pScanImgBuf->m_ScanImgPos.right		= pScanImgBuf->m_ScanImgPos.left + (ImageSizeWum-1);
				//pScanImgBuf->m_ScanImgPos.bottom		= pScanImgBuf->m_ScanImgPos.top + (ImageSizeHum-1);

				pScanImgBuf->m_ScanImgPos.top		-= OverLapYAxis;
				pScanImgBuf->m_ScanImgPos.bottom		+= OverLapYAxis;
			}
			//////////////////////////////////////////////////////////////////////////
			//어느Cell의 Pad부 에 걸리는지 찾는다.
#ifndef VIEW_CREATE_SCAN_PAD
			m_fnFindIntersectScanPos(pScanImgBuf);
#else
			m_fnFindIntersectScanPos(pScanImgBuf, pViewMap);
#endif

#ifdef VIEW_CREATE_SCAN_PAD
			cvDrawRect(pViewMap, cvPoint(pScanImgBuf->m_ScanImgPos.left/1000, pScanImgBuf->m_ScanImgPos.top/1000),
				cvPoint(pScanImgBuf->m_ScanImgPos.right/1000, pScanImgBuf->m_ScanImgPos.bottom/1000), CV_RGB(255, 0, 0));
			cvShowImage("ScnaMapViewer", pViewMap);
			cvWaitKey(10);
#endif
		}
	}
#ifdef VIEW_CREATE_SCAN_PAD
	cvShowImage("ScnaMapViewer", pViewMap);
	//cvWaitKey();
	//cvDestroyAllWindows();
	cvReleaseImage(&pViewMap);
#endif
	return true;
}
bool CoordinatesProc::m_fnFindIntersectScanPos(SCANIMGINFO * pScanImgBuf, IplImage *pScanView)
{
	BLOCKINFO *pBlockBuf		= nullptr;
	CELLINFO	*pCellBuf		= nullptr;
	PADINFO		*pPadCellBuf	= nullptr;

	int BlockDataSetIndex = 0;
	int CellDataSetIndex  =0;
	for(int BlockYStep = 0; BlockYStep<m_GlassParam.GLASS_BLOCK_COUNT_Y; BlockYStep++)
	{
		for(int BlockXStep = 0; BlockXStep<m_GlassParam.GLASS_BLOCK_COUNT_X; BlockXStep++)
		{
			pBlockBuf = &m_pBlockList[BlockDataSetIndex++];
			int CellCntInBlockX = m_GlassParam.GLASS_CELL_COUNT_X/m_GlassParam.GLASS_BLOCK_COUNT_X;
			int CellCntInBlockY = m_GlassParam.GLASS_CELL_COUNT_Y/m_GlassParam.GLASS_BLOCK_COUNT_Y;

			int BlockIndex = BlockXStep+(BlockYStep*(CellCntInBlockX*CellCntInBlockY) );
			//3)Cell Position Set.
			for(int CellYStep = 0; CellYStep<CellCntInBlockY; CellYStep++)
			{
				for(int CellXStep = 0; CellXStep<CellCntInBlockX; CellXStep++)
				{
					pCellBuf			=  &m_pCellList[CellDataSetIndex];
					pPadCellBuf		=  &m_pPadCellList[CellDataSetIndex++];
					CRect SubRectBuf;
					int PosIndex = 0;
					int TypeIndex = 0;
					if(SubRectBuf.IntersectRect(&pScanImgBuf->m_ScanImgPos, &pCellBuf->m_CellPos)  == TRUE)
					{
						PosIndex = pScanImgBuf->m_findSubRecCnt[TypeIndex];
						pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex] = SubRectBuf;

						pScanImgBuf->m_ORGTargetRect[TypeIndex][PosIndex] = pCellBuf->m_CellPos;
						//Image 좌표계로 변환
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].left		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].top			- pScanImgBuf->m_ScanImgPos.top;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].right		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].bottom		- pScanImgBuf->m_ScanImgPos.top;

						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left	/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right	/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom	/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_findSubRecCnt[TypeIndex]++;

					}
					TypeIndex++;
					if(SubRectBuf.IntersectRect(&pScanImgBuf->m_ScanImgPos, &pPadCellBuf->m_CellPos_Top) == TRUE)
					{
						PosIndex = pScanImgBuf->m_findSubRecCnt[TypeIndex];
						pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex] = SubRectBuf;

#ifdef VIEW_CREATE_SCAN_PAD
						cvRectangle(pScanView, cvPoint(SubRectBuf.left/1000, SubRectBuf.top/1000),
							cvPoint(SubRectBuf.right/1000, SubRectBuf.bottom/1000), CV_RGB(255, 255, 0),CV_FILLED);
#endif
						pScanImgBuf->m_ORGTargetRect[TypeIndex][PosIndex] = pPadCellBuf->m_CellPos_Top;
						//Image 좌표계로 변환
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].left		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].top			- pScanImgBuf->m_ScanImgPos.top;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].right		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].bottom		- pScanImgBuf->m_ScanImgPos.top;

						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		=(int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		=(int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		=(int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right	/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		=(int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom	/ m_GlassParam.m_ScanImgResolution_H);

						pScanImgBuf->m_findSubRecCnt[TypeIndex]++;
					}
					TypeIndex++;
					if(SubRectBuf.IntersectRect(&pScanImgBuf->m_ScanImgPos, &pPadCellBuf->m_CellPos_Bottom)  == TRUE)
					{
						PosIndex = pScanImgBuf->m_findSubRecCnt[TypeIndex];
						pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex] = SubRectBuf;

#ifdef VIEW_CREATE_SCAN_PAD
						cvRectangle(pScanView, cvPoint(SubRectBuf.left/1000, SubRectBuf.top/1000),
							cvPoint(SubRectBuf.right/1000, SubRectBuf.bottom/1000), CV_RGB(255, 255, 0),CV_FILLED);
#endif

						pScanImgBuf->m_ORGTargetRect[TypeIndex][PosIndex] = pPadCellBuf->m_CellPos_Bottom;
						//Image 좌표계로 변환
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].left		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].top			- pScanImgBuf->m_ScanImgPos.top;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].right		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].bottom		- pScanImgBuf->m_ScanImgPos.top;

						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left	/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right	/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom	/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_findSubRecCnt[TypeIndex]++;
					}

					TypeIndex++;
					if(SubRectBuf.IntersectRect(&pScanImgBuf->m_ScanImgPos, &pPadCellBuf->m_CellPos_Left)  == TRUE)
					{
						PosIndex = pScanImgBuf->m_findSubRecCnt[TypeIndex];
						pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex] = SubRectBuf;

						pScanImgBuf->m_ORGTargetRect[TypeIndex][PosIndex] = pPadCellBuf->m_CellPos_Left;
						//Image 좌표계로 변환
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].left		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].top			- pScanImgBuf->m_ScanImgPos.top;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].right		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].bottom		- pScanImgBuf->m_ScanImgPos.top;

						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left	/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right	/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom	/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_findSubRecCnt[TypeIndex]++;
					}
					TypeIndex++;
					if(SubRectBuf.IntersectRect(&pScanImgBuf->m_ScanImgPos, &pPadCellBuf->m_CellPos_Right)  == TRUE)
					{
						PosIndex = pScanImgBuf->m_findSubRecCnt[TypeIndex];
						pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex] = SubRectBuf;
#ifdef VIEW_CREATE_SCAN_PAD
						cvRectangle(pScanView, cvPoint(SubRectBuf.left/1000, SubRectBuf.top/1000),
							cvPoint(SubRectBuf.right/1000, SubRectBuf.bottom/1000), CV_RGB(255, 255, 0),CV_FILLED);
#endif

						pScanImgBuf->m_ORGTargetRect[TypeIndex][PosIndex] = pPadCellBuf->m_CellPos_Right;
						//Image 좌표계로 변환
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].left		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].top			- pScanImgBuf->m_ScanImgPos.top;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].right		- pScanImgBuf->m_ScanImgPos.left;
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= pScanImgBuf->m_IspectAreaSubtractRect[TypeIndex][PosIndex].bottom		- pScanImgBuf->m_ScanImgPos.top;

						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].left		/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].top		/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].right	/ m_GlassParam.m_ScanImgResolution_W);
						pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom		= (int)((float)pScanImgBuf->m_InImgPositionRect[TypeIndex][PosIndex].bottom	/ m_GlassParam.m_ScanImgResolution_H);
						pScanImgBuf->m_findSubRecCnt[TypeIndex]++;
					}
				}
			}
		}
	}
	return false;
}
bool CoordinatesProc::m_fnGlassPADAreaCalculate()
{
	return true;
}
void 	CoordinatesProc::m_fnDrawGlassInspectInfo()
{
	if(m_pMainMpaDrawObj == nullptr)
		return;

	cvZero(m_pMainMpaDrawObj);
	BLOCKINFO *pBlockBuf		= nullptr;
	CELLINFO	*pCellBuf		= nullptr;
	PADINFO		*pPadCellBuf	= nullptr;
	CvPoint cvNamePos;
	CPoint NamePos;
	//cvCircle(pViewMap, cvPoint(m_GlassParam.CenterPointX/1000, m_GlassParam.CenterPointY/1000), 10,CV_RGB(0,0,255), 5);
	cvLine(m_pMainMpaDrawObj,cvPoint(m_GlassParam.CenterPointX/1000 -10, m_GlassParam.CenterPointY/1000), cvPoint(m_GlassParam.CenterPointX/1000 + 10, m_GlassParam.CenterPointY/1000), CV_RGB(255,0,255),3);
	cvLine(m_pMainMpaDrawObj,cvPoint(m_GlassParam.CenterPointX/1000 , m_GlassParam.CenterPointY/1000 -10), cvPoint(m_GlassParam.CenterPointX/1000, m_GlassParam.CenterPointY/1000+10), CV_RGB(255,0,255),3);

	int BlockDataSetIndex = 0;
	int CellDataSetIndex  =0;
	for(int BlockYStep = 0; BlockYStep<m_GlassParam.GLASS_BLOCK_COUNT_Y; BlockYStep++)
	{
		for(int BlockXStep = 0; BlockXStep<m_GlassParam.GLASS_BLOCK_COUNT_X; BlockXStep++)
		{
			pBlockBuf = &m_pBlockList[BlockDataSetIndex++];
	
			int CellCntInBlockX = m_GlassParam.GLASS_CELL_COUNT_X/m_GlassParam.GLASS_BLOCK_COUNT_X;
			int CellCntInBlockY = m_GlassParam.GLASS_CELL_COUNT_Y/m_GlassParam.GLASS_BLOCK_COUNT_Y;
	
			NamePos  = 	pBlockBuf->m_BlockPos.CenterPoint();
			cvNamePos.x = pBlockBuf->m_BlockPos.left/1000;
			cvNamePos.y = pBlockBuf->m_BlockPos.top/1000 -10;
			cvPutText(m_pMainMpaDrawObj, pBlockBuf->m_BlockName, cvNamePos, &m_BlockNameFont, CV_RGB(255,255,255));

			cvDrawRect(m_pMainMpaDrawObj, cvPoint(pBlockBuf->m_BlockPos.left/1000, pBlockBuf->m_BlockPos.top/1000),
											cvPoint(pBlockBuf->m_BlockPos.right/1000, pBlockBuf->m_BlockPos.bottom/1000), CV_RGB(255, 0, 0),1);
			int BlockIndex = BlockXStep+(BlockYStep*(CellCntInBlockX*CellCntInBlockY) );
			//3)Cell Position Set.
			for(int CellYStep = 0; CellYStep<CellCntInBlockY; CellYStep++)
			{
				for(int CellXStep = 0; CellXStep<CellCntInBlockX; CellXStep++)
				{
					pCellBuf			=  &m_pCellList[CellDataSetIndex];
					pPadCellBuf		=  &m_pPadCellList[CellDataSetIndex++];

					cvDrawRect(m_pMainMpaDrawObj, cvPoint(pCellBuf->m_CellPos.left/1000, pCellBuf->m_CellPos.top/1000),
						cvPoint(pCellBuf->m_CellPos.right/1000, pCellBuf->m_CellPos.bottom/1000), CV_RGB(0, 255, 0));

					////Draw Pad Area
					//cvDrawRect(m_pMainMpaDrawObj, cvPoint(pPadCellBuf->m_CellPos_Top.left/1000, pPadCellBuf->m_CellPos_Top.top/1000),
					//	cvPoint(pPadCellBuf->m_CellPos_Top.right/1000, pPadCellBuf->m_CellPos_Top.bottom/1000), CV_RGB(255, 0, 0));

					//cvDrawRect(m_pMainMpaDrawObj, cvPoint(pPadCellBuf->m_CellPos_Bottom.left/1000, pPadCellBuf->m_CellPos_Bottom.top/1000),
					//	cvPoint(pPadCellBuf->m_CellPos_Bottom.right/1000, pPadCellBuf->m_CellPos_Bottom.bottom/1000), CV_RGB(0, 255, 0));

					//cvDrawRect(m_pMainMpaDrawObj, cvPoint(pPadCellBuf->m_CellPos_Left.left/1000, pPadCellBuf->m_CellPos_Left.top/1000),
					//	cvPoint(pPadCellBuf->m_CellPos_Left.right/1000, pPadCellBuf->m_CellPos_Left.bottom/1000), CV_RGB(0, 0, 255));

					//cvDrawRect(m_pMainMpaDrawObj, cvPoint(pPadCellBuf->m_CellPos_Right.left/1000, pPadCellBuf->m_CellPos_Right.top/1000),
					//	cvPoint(pPadCellBuf->m_CellPos_Right.right/1000, pPadCellBuf->m_CellPos_Right.bottom/1000), CV_RGB(255, 255, 255));


					NamePos  = 	pCellBuf->m_CellPos.CenterPoint();
					cvNamePos.x = NamePos.x/1000 - 25;
					cvNamePos.y = pCellBuf->m_CellPos.top/1000+20;
					cvPutText(m_pMainMpaDrawObj, pCellBuf->m_CellName, cvNamePos, &m_CellNameFont, CV_RGB(255,255,255));
				}
			}
		}
	}
	CvScalar PadAreaColor[5] = {CV_RGB(255,0,0),CV_RGB(0,255,0),CV_RGB(0,0,255),CV_RGB(255,255,0),CV_RGB(255,255,255)};
	CRect MapDrawRect;

	int ImageIndex  = 0;
	SCANIMGINFO * pScanImgBuf = nullptr;
	
	CvScalar ScanAreaColor;
	
	for(int LineStep = 0; LineStep<m_AOIOneCam_ScanLineCnt; LineStep++)
	{
		for(int ImgStep = 0; ImgStep<m_OneLineScnaImgCnt; ImgStep++)
		{
			pScanImgBuf = &m_pScanImgList[ImageIndex++];

			switch (m_TargetTaskNum)
			{
			case  TASK_21_Inspector: ScanAreaColor=  CV_RGB(255,255,255);
				break;
			//case TN_INSPECTOR1: ScanAreaColor=  CV_RGB(255,255,255);
			//	break;
			//case TN_INSPECTOR2:	ScanAreaColor=  CV_RGB(0,255,255);
			//	break;
			//case TN_INSPECTOR3:	ScanAreaColor=  CV_RGB(255,0,255);
			//	break;
			//case TN_INSPECTOR4:	ScanAreaColor=  CV_RGB(255,255,0);
			//	break;
			default:
				return ;
			}
			cvDrawRect(m_pMainMpaDrawObj, cvPoint(pScanImgBuf->m_ScanImgPos.left/1000, pScanImgBuf->m_ScanImgPos.top/1000),
				cvPoint(pScanImgBuf->m_ScanImgPos.right/1000, pScanImgBuf->m_ScanImgPos.bottom/1000),ScanAreaColor);

			//for(int DrawStep = 0; DrawStep<5; DrawStep++)
			//{
			//	for(int InerStep = 0; InerStep<pScanImgBuf->m_findSubRecCnt[DrawStep]; InerStep++)
			//	{
			//		MapDrawRect = pScanImgBuf->m_IspectAreaSubtractRect[DrawStep][InerStep];
			//		MapDrawRect.left /=1000;
			//		MapDrawRect.top /=1000;
			//		MapDrawRect.right /=1000;
			//		MapDrawRect.bottom/=1000;
			//		FillRectDraw(m_pMainMpaDrawObj, &MapDrawRect, PadAreaColor[DrawStep]);
			//	}				
			//}
		}
	}
}

SCANIMGINFO* CoordinatesProc::m_fnGetCalculateAreaInImg(int ScnaNum, int ImgIndex)
{
	SCANIMGINFO* pRetData = nullptr;
	if(m_pScanImgList != nullptr && ScnaNum>0 && ImgIndex > 0)
	{
		int ImageIndex = ((ScnaNum-1)*m_OneLineScnaImgCnt)+(ImgIndex-1);//배열은 0 Index이다.
		//한번더 위치를 확인 하는 것이다.
		if(m_pScanImgList[ImageIndex].m_ScanLineNum  == ScnaNum && m_pScanImgList[ImageIndex].m_ScanImgIndex  == ImgIndex)
			pRetData = &m_pScanImgList[ImageIndex];
	}
	return pRetData;
}
/*
*	Module Name		:	m_fnFindFirstCellPadInfo
*	Parameter		:	Pad 부
*	Return			:	Machine Coordinate
*	Function		:	지정 역역에 겹침 되는 영역 List를 반환한다.(최대 4개.)
						pFindArea의 지정영역 안에 들어가는 Pa부를 모두 선택 한다.
*	Create			:	2012.11.00
*	Author			:	최지형
*	Version			:	1.0
*/
void CoordinatesProc::m_fnFindRectCellPadInfo(int PadIndex, CRect *pFindArea, int *pResultCnt, CRect *pResultList)
{
	int InsertRectCnt = 0;
	CRect CheckRect;
	CRect *pPadArea = nullptr;
	if(m_pPadCellList != nullptr)
	{
		for(int CellCntStep = 0; CellCntStep<m_nPadCellCnt; CellCntStep++)
		{
			switch(PadIndex)
			{
			case 0://상
				pPadArea = &m_pPadCellList[CellCntStep].m_CellPos_Top;
				break;
			case 1://하
				pPadArea = &m_pPadCellList[CellCntStep].m_CellPos_Bottom;
				break;
			case 2://좌
				pPadArea = &m_pPadCellList[CellCntStep].m_CellPos_Left;
				break;
			case 3://우
				pPadArea = &m_pPadCellList[CellCntStep].m_CellPos_Right;
				break;
			}


			if(CheckRect.IntersectRect(pPadArea, pFindArea) == TRUE)
			{
				*(pResultList+InsertRectCnt) = *pPadArea;
				InsertRectCnt++;
				if(InsertRectCnt>4)
					return;
			}
		}
		*pResultCnt = InsertRectCnt;
	}
}

/*
*	Module Name		:	MachinPos_GetCellRect
*	Parameter		:	Cell Y, X  Index
*	Return			:	Machine Coordinate
*	Function		:	지정 index의 Cell의 Machine좌표를 Return한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//CRect CoordinatesProc::MachinPos_GetCellRect(int *pCellIndex_YAxis, int *pCellIndex_XAxis)
//{
//
//	CRect ReturnRect;//Machine Coordinate이다.
//	//ReturnRect.left = Y_Axis_Start
//	ReturnRect.left		= m_pRecipeParam->m_CELL_START_POS_Y
//								+((*pCellIndex_YAxis-1) * m_pRecipeParam->m_CELL_SIZE_Y)
//								+((*pCellIndex_YAxis-1) * m_pRecipeParam->m_CELL_SPACE_SIZE_Y);
//
//	//ReturnRect.right = Y_Axis_End
//	ReturnRect.right	= ReturnRect.left 
//							+ m_pRecipeParam->m_CELL_SIZE_Y;
//
//		//ReturnRect.top = 	X_Axis_Start
//	ReturnRect.top		=  m_pRecipeParam->m_CELL_START_POS_X
//								+((*pCellIndex_XAxis-1) * m_pRecipeParam->m_CELL_SIZE_X)
//								+((*pCellIndex_XAxis-1) * m_pRecipeParam->m_CELL_SPACE_SIZE_X);
//
//	//ReturnRect.bottom =  X_Axis_End
//	ReturnRect.bottom	=   ReturnRect.top  
//								+ m_pRecipeParam->m_CELL_SIZE_X;
//
//	return ReturnRect;
//}
/*
*	Module Name		:	MachinPos_GetROIRect
*	Parameter		:	Cell Y, X  Index
*	Return			:	Machine Coordinate
*	Function		:	지정 index의 Cell ROI영역의 Machine좌표를 Return한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//CRect CoordinatesProc::MachinPos_GetROIRect(int *pCellIndex_YAxis, int *pCellIndex_XAxis)
//{
//	
//	CRect ReturnRect;//Machine Coordinate이다.
//	ReturnRect				= MachinPos_GetCellRect( pCellIndex_YAxis, pCellIndex_XAxis);
//	//ReturnRect.left = Y_Axis_Start
//	ReturnRect.left		= ReturnRect.left	- m_pRecipeParam->m_CELL_ROI_OVERSIZE_XY;
//	//ReturnRect.right = Y_Axis_End
//	ReturnRect.right		= ReturnRect.right	+ m_pRecipeParam->m_ROI_OverValueXYx2;
//	//ReturnRect.top = 	X_Axis_Start
//	ReturnRect.top		= ReturnRect.top	- m_pRecipeParam->m_CELL_ROI_OVERSIZE_XY;
//	//ReturnRect.bottom =  X_Axis_End
//	ReturnRect.bottom	= ReturnRect.bottom + m_pRecipeParam->m_ROI_OverValueXYx2;
//
//	return ReturnRect;
//}
/*
*	Module Name		:	MachinPos_GetROIAndCellWorkRect
*	Parameter		:	Cell Y, X  Index & Return Data Pointer Buffer
*	Return			:	Machine Coordinate
*	Function		:	지정 index의 Cell의 ROI영역과 Work 좌표계를 Return한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//BOOL CoordinatesProc::MachinPos_GetROIAndCellWorkRect(int *pCellIndex_YAxis, int *pCellIndex_XAxis,  CRect * pROIRect, CRect * pCellRectWork)
//{
//	if( pROIRect == nullptr || pCellRectWork == nullptr)//Machine Coordinate이다.
//		return FALSE;
//	*pCellRectWork = MachinPos_GetCellRect( pCellIndex_YAxis, pCellIndex_XAxis);
//
//	if(m_pRecipeParam->m_bUSE_ROICheck != 0)
//	{//ROI를 사용 한다.
//		//ReturnRect.left = Y_Axis_Start
//		pROIRect->left			= pCellRectWork->left	- m_pRecipeParam->m_CELL_ROI_OVERSIZE_XY;
//		//ReturnRect.right = Y_Axis_End
//		pROIRect->right		= pCellRectWork->right	+ m_pRecipeParam->m_ROI_OverValueXYx2;
//		//ReturnRect.top = 	X_Axis_Start
//		pROIRect->top			= pCellRectWork->top	- m_pRecipeParam->m_CELL_ROI_OVERSIZE_XY;
//		//ReturnRect.bottom =  X_Axis_End
//		pROIRect->bottom	= pCellRectWork->bottom	+ m_pRecipeParam->m_ROI_OverValueXYx2;
//	}
//	else
//	{
//		//ROI를 사용 하지 않으면 Defect의 위치정보를 Cell의 위치로 표기하기 위해. 
//		//ROI영역을 Cell영역으로 한다.
//		*pROIRect = *pCellRectWork;
//	}
//	COODPOINT WorkPosXY;
//	WorkPosXY = MachinPosToWorkPos(&COODPOINT( pCellRectWork->top, pCellRectWork->left));
//	pCellRectWork->left = WorkPosXY.COOD_Y;
//	pCellRectWork->top	= WorkPosXY.COOD_X;
//
//	WorkPosXY = MachinPosToWorkPos(&COODPOINT(pCellRectWork->bottom, pCellRectWork->right));
//	pCellRectWork->right = WorkPosXY.COOD_Y;
//	pCellRectWork->bottom= WorkPosXY.COOD_X;
//	return TRUE;
//}



//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
/*
*	Module Name		:	MachinPos_GetImageXYPos
*	Parameter		:	Scan Line Index, Scan Image Index, In Image Point.
*	Return			:	Machine Coordinate
*	Function		:	Image 상의 한점의 Machine좌표를 반환다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//COODPOINT CoordinatesProc::MachinPos_GetImageXYPos(int *pScanLineIndex, int *pScanImageIndex, CPoint *pinImagePosXY)
//{
//	COODPOINT ReturnMachinePos;
//	//pInMachinePosYX->x = Y_Axis
//	ReturnMachinePos.COOD_Y = (LONG)(m_pHWParam->m_AxisY_MachineStdPos_Y 
//											- m_pHWParam->m_ScanImageRealHalfW
//											+ ((*pScanLineIndex-1) * m_pHWParam->m_SCAN_LINE_SHIFT_OFFSET));
//
//	//Index방향에 따라 Start위치 가 바뀐다.
//	if(*pScanLineIndex %2)//홀수 Scan Line Index.
//	{
//		//pInMachinePosYX->y = X_Axis
//		ReturnMachinePos.COOD_X = (LONG)(m_pHWParam->m_AxisY_MachineStdPos_X /*m_pHWParam->m_AOI_BLOCK_SCAN_START_X*/ 
//												+ ((*pScanImageIndex-1) * m_pHWParam->m_ScanImageRealH))
//												+ m_pHWParam->m_AOI_SCAN_START_OFFSET_ODD;
//	}
//	else //짝수Scan Line Index 
//	{
//		pinImagePosXY->y = m_pHWParam->m_SCAN_IMAGE_H_PIXELS - pinImagePosXY->y;//이미지 좌표 역계산.
//		//pInMachinePosYX->y = X_Axis
//		ReturnMachinePos.COOD_X = (LONG)(m_pHWParam->m_AxisX_MachineStdPosEven
//											- ((*pScanImageIndex) * m_pHWParam->m_ScanImageRealH))
//											+ m_pHWParam->m_AOI_SCAN_START_OFFSET_EVEN;
//							//(ScanImageIndex-1)에서 Index -1 이 없는건 하단 좌표 이동 계산식 이다.
//	}
//	//Image의 X,Y 좌표가 Stage의 XY좌표 계로 변환된다.
//	//Image 상의 X위치는 Stage상의 Y축과 대응 된다.
//	//ReturnMachinePos.x = (LONG)(ReturnMachinePos.x + (inImagePosXY.x * m_pHWParam->m_SCAN_IMAGE_W_RESOLUTION));
//	//ReturnMachinePos.y = (LONG)(ReturnMachinePos.y + (inImagePosXY.y * m_pHWParam->m_SCAN_IMAGE_H_RESOLUTION));
//	ReturnMachinePos.COOD_Y += (LONG)(pinImagePosXY->x * m_pHWParam->m_SCAN_IMAGE_W_RESOLUTION);
//	ReturnMachinePos.COOD_X += (LONG)(pinImagePosXY->y * m_pHWParam->m_SCAN_IMAGE_H_RESOLUTION);
//	return ReturnMachinePos;
//}
/*
*	Module Name		:	MachinPos_GetScanImageRect
*	Parameter		:	Scan Line Index, Scan Image Index, Return Rect Buffer Pointer
*	Return			:	TRUE/FALSE
*	Function		:	Grab Image의 Machine 좌표를 구한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//BOOL CoordinatesProc::MachinPos_GetScanImageRect(int *pScanLineIndex, int *pScanImageIndex, CRect *pGrabImageMachinePos)
//{
//	if( pGrabImageMachinePos == nullptr)
//		return FALSE;
//	COODPOINT MachinePos;
//	if(*pScanLineIndex %2)//홀수 Scan Line Index.
//		MachinePos = MachinPos_GetImageXYPos(pScanLineIndex, pScanImageIndex, &CPoint(0,0));
//	else
//		MachinePos = MachinPos_GetImageXYPos(pScanLineIndex, pScanImageIndex, &CPoint(0,m_pHWParam->m_SCAN_IMAGE_H_PIXELS));
//
//	//*pGrabImageMachinePos->left = Y_Axis
//	pGrabImageMachinePos->left			= MachinePos.COOD_Y;
//	pGrabImageMachinePos->right		= (LONG)(pGrabImageMachinePos->left + m_pHWParam->m_ScanImageRealW);
//	//Index방향에 따라 Start위치 가 바뀐다.
//	//pGrabImageMachinePos->top = X_Axis
//	pGrabImageMachinePos->top			= MachinePos.COOD_X;
//	pGrabImageMachinePos->bottom		= (LONG)(pGrabImageMachinePos->top + m_pHWParam->m_ScanImageRealH);
//
//	return TRUE;
//}
/*
*	Module Name		:	WorkPos_GetDefectPos
*	Parameter		:	Scan Line Index, Scan Image Index, In Image Point.
*	Return			:	Work Coordinates Pos
*	Function		:	이미지 상의 Defect 위치를  Work 좌표계로 변환다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//COODPOINT CoordinatesProc::WorkPos_GetDefectPos(int *pScanLineIndex, int *pScanImageIndex, CPoint *pInImageDefectXY)
//{
//	COODPOINT MachinePos = MachinPos_GetImageXYPos( pScanLineIndex, pScanImageIndex, pInImageDefectXY);
//	return MachinPosToWorkPos(&MachinePos);//.COOD_Y, MachinePos.COOD_X,(int*)&MachinePos.x,(int*)&MachinePos.y);
//}
/*
*	Module Name		:	WM_GetDefectPosWorkAndMachine
*	Parameter		:	Scan Line Index, Scan Image Index, Return Buffer Pointers
*	Return			:	Work Coordinates Pos & Machine Coordinates Pos
*	Function		:	이미지 상의 Defect 위치를  Work 좌표와 Machine좌표로 변환한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//BOOL CoordinatesProc::WM_GetDefectPosWorkAndMachine(int *pScanLineIndex, int *pScanImageIndex, CPoint *pInImageDefectXY,  COODPOINT*pMachinPos, COODPOINT *pWorkPos)
//{
//	if( pMachinPos == nullptr || pWorkPos == nullptr)
//		return FALSE;
//
//	*pMachinPos = MachinPos_GetImageXYPos( pScanLineIndex, pScanImageIndex, pInImageDefectXY);
//	*pWorkPos = MachinPosToWorkPos(pMachinPos);
//	return  TRUE;
//}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
/*
*	Module Name		:	MachinPosToWorkPos
*	Parameter		:	Machine Pos X, Y, Return Buffer Pointer
*	Return			:	Work Coordinate
*	Function		:	Machine 좌표를 Work좌표로 변환 한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

//COODPOINT	CoordinatesProc::MachinPosToWorkPos(COODPOINT *pMachinePosXY)
//{
//	COODPOINT ReturnValue;
//	ReturnValue.COOD_Y = pMachinePosXY->COOD_Y -  m_WorkZeroPosInMC.COOD_Y;
//	ReturnValue.COOD_X = (pMachinePosXY->COOD_X -  m_WorkZeroPosInMC.COOD_X) * (-1);//부호 변경. 상단이 + 하단이 -를 갖는 좌표계이다.
//	return ReturnValue;
//}

/*
*	Module Name		:	WorkPosToMachinPos
*	Parameter		:	Work Pos X, Y, Return Buffer Pointer
*	Return			:	Machine Coordinate
*	Function		:	Work 좌표를 Machine 좌표로 변환 한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

//COODPOINT	CoordinatesProc::WorkPosToMachinPos(COODPOINT *pWorkPosXY)
//{
//	COODPOINT ReturnValue;
//	ReturnValue.COOD_Y = m_WorkZeroPosInMC.COOD_X + pWorkPosXY->COOD_Y;
//	ReturnValue.COOD_X = (m_WorkZeroPosInMC.COOD_Y - pWorkPosXY->COOD_X );
//	return ReturnValue;
//}