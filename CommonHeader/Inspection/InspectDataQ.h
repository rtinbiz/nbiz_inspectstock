#pragma once
#pragma pack(1)
#include "DefineStaticValue.h"





class InspectDataQ
{
public:
	InspectDataQ(int ImageW, int ImageH, int MaxDefectCnt, int ReviewSizeW, int ReviewSizeH);
	~InspectDataQ(void);

private: 
	CRITICAL_SECTION		m_CrtSection;
	int						m_GrabImageSize;
public:
	int						m_MaxDefectCnt;
	int						m_ReviewSizeW;
	int						m_ReviewSizeH;
public:
	DWORD 					m_nOneTick;
	//Node 간 연결점으로 사용 된다.
	InspectDataQ	*		m_pFrontNode;
	InspectDataQ	*		m_pNextNode;
	
	//Node의 기본정보.
	int						m_ScanLine;													//<<<Scan Block중 몇번째 인지
	int						m_ScanImageIndex;											//<<<Grab된 Image의 Index

	IplImage		*		m_pGrabImage;												//<<<Grab된 Image.	
	IplImage		*		m_pGrabOrgImage;												//<<<Grab된 Image.
	
	//////////////////////////////////////////////////////////////////////////
	int						m_DefectListCnt;												//<<<검출 Defect의 갯수.
	DEFECT_DATA		*		m_pSearchRetList;											//<<<Image상의 Defect위치
	COORD_DINFO		*		m_pCoordinateRetList;										//<<<좌표계 상의 Defect위치
	//////////////////////////////////////////////////////////////////////////
	
	//void				*		m_StrDefect;													//<<<Result Process에서 string Data로 기록 된다.
	//////////////////////////////////////////////////////////////////////////
   	BYTE			**		m_ppDefectImgList;											//<<<지정 Size만큼의 Defect Image.
//	int							m_ActivePADArea[4*4];										//<<<(StartX, StartY, EndX, EndY)*4 ==>상하좌우 4개의 영역이 있을수 있음으로 최대치를 사용 한다.
public:	
	//////////////////////////////////////////////////////////////////////////
	//Result Process에서 처리 완료 Setting.
	int						m_NodeProcState;											//<<<현재 Node의 Process 진행 상태를 알려준다.
	void			m_fnQSetProcessEnd(int NewState);								//<<<각각의 Process에서 호출되어져야 한다..
								
	//////////////////////////////////////////////////////////////////////////
	// Inspect DLL에서 사용할 Function.
	BYTE *		m_fnQGetGrabImage();					//저장된 Grab Image를 가져온다.
	//int *		m_fnQGetActivePadArea();					//Pad부이나 Active연산을 수행할 영역을 가져 온다.
	BOOL			m_fnQSetDefectAllBuffer(int DefectNodeCount, DEFECT_DATA *pNewData);

	void			m_fnQSetBufferData(BYTE * pNewData);					//<<< Buffer Data를 기록한다.
	BYTE *		m_fnQGetBufferData();											//<<<저장된 Buffer Data를 가져온다.
private:
	//Inspect Buffer Create 변경
#ifdef USE_STATIC_BUFFER
	IplImage		*		m_pBufferData;										//<<<Grab된 Image.	
#else
	BYTE			*		m_pBufferData;
#endif
	
	//////////////////////////////////////////////////////////////////////////
public:
	void			*		m_pCoordAraeInImg;// 내부 좌표 계와 연결 하여 사용 (SCANIMGINFO	*)(void * 로 변환 하여 가지고 있는다.)
	
};

 #pragma pack()