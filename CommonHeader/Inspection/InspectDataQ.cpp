#include "StdAfx.h"
#include "InspectDataQ.h"
int CountProc = 0;
/** 
@brief		Inspect Data Queue Node 생성자.

@remark
-			 생성자
-			
@author		최지형
@date		2010.03.00
*/ 
InspectDataQ::InspectDataQ(int ImageW, int ImageH, int MaxDefectCnt, int ReviewSizeW, int ReviewSizeH)
{
//	AfxMessageBox("InspectDataQ");
	InitializeCriticalSection(&m_CrtSection);

	EnterCriticalSection(&m_CrtSection);
	m_nOneTick			= 0;
	//Node Link 정보.
	m_pFrontNode		= nullptr;
	m_pNextNode			= nullptr;
//	m_StrDefect 		= nullptr;
	//Grab Image 정보
	m_ScanLine			= -1;
	m_ScanImageIndex	= -1;
	m_pGrabImage		= nullptr;
	m_pGrabOrgImage		= nullptr;
	m_pBufferData		= nullptr;

	m_NodeProcState		= QDATA_PROC_END_NONE;
	//Grabber에서 들어오는 Data를 저장한다.
	m_pGrabImage		= cvCreateImage( cvSize(ImageW, ImageH), IPL_DEPTH_8U, 1);
	m_pGrabOrgImage		= cvCreateImage( cvSize(ImageW, ImageH), IPL_DEPTH_8U, 1);
	m_GrabImageSize		= ImageW*ImageH;
	//cvZero(m_pGrabImage);
	//cvZero(m_pGrabOrgImage);
	memset(m_pGrabImage->imageData, 0xFF, m_pGrabImage->imageSize);
	memset(m_pGrabOrgImage->imageData, 0xFF, m_pGrabOrgImage->imageSize);
	//Inspect Buffer Create 변경
#ifdef USE_STATIC_BUFFER
	m_pBufferData			= cvCreateImage( cvSize(ImageW, ImageH), IPL_DEPTH_8U, 1);
	//cvZero(m_pBufferData);
	memset(m_pBufferData->imageData, 0xFF, m_pBufferData->imageSize);
#endif
	m_MaxDefectCnt	= MaxDefectCnt;
	m_ReviewSizeW	= ReviewSizeW;
	m_ReviewSizeH	= ReviewSizeH;

	m_pSearchRetList			= new DEFECT_DATA[m_MaxDefectCnt];
	m_pCoordinateRetList	= new COORD_DINFO[m_MaxDefectCnt];
	m_ppDefectImgList		= new BYTE*[m_MaxDefectCnt];

	//Defect 정보 저장소를 Setting한다.
	memset(m_pSearchRetList, 0x00, sizeof(DEFECT_DATA)*m_MaxDefectCnt);
	memset(m_pCoordinateRetList, 0x00, sizeof(DEFECT_DATA)*m_MaxDefectCnt);
	
	for(int CrStep=0; CrStep < m_MaxDefectCnt ; CrStep++)
	{
		m_ppDefectImgList[CrStep] = new BYTE[ReviewSizeW*ReviewSizeH];
		memset(m_ppDefectImgList[CrStep], 0x00, sizeof(BYTE)*(ReviewSizeW*ReviewSizeH));
	}

	//Void Pointer로 가지고 있다가 필요 Process에서 변환 하여 사용 한다.
	m_pCoordAraeInImg	= nullptr;

	//for(int InitStep =0; InitStep<(4*4); InitStep++)
	//	m_ActivePADArea[InitStep] = -1;

	LeaveCriticalSection(&m_CrtSection);
}

/** 
@brief		Inspect Data Queue Node 소멸자.

@remark
-			 소멸자
-			
@author		최지형
@date		2010.03.00
*/ 
InspectDataQ::~InspectDataQ(void)
{
	//AfxMessageBox("~InspectDataQ");
	
	EnterCriticalSection(&m_CrtSection);

	//생성된 Image가 있으면 제거 한다.
	if(m_pGrabImage != nullptr)
		cvReleaseImage(&m_pGrabImage);
	m_pGrabImage = nullptr;
	if(m_pGrabOrgImage != nullptr)
		cvReleaseImage(&m_pGrabOrgImage);
	m_pGrabOrgImage = nullptr;
	
	//Inspect Buffer Create 변경
	if(m_pBufferData != nullptr)
	{
#ifdef USE_STATIC_BUFFER
		cvReleaseImage(&m_pBufferData);
#else
		delete [] m_pBufferData;
#endif
	}
	m_pBufferData = nullptr;

	delete [] m_pSearchRetList;
	m_pSearchRetList = nullptr;
	delete [] m_pCoordinateRetList ;
	m_pCoordinateRetList = nullptr;
	//Defect 정보를 제거 한다.
	for(int CrStep=0; CrStep<m_MaxDefectCnt; CrStep++)
	{
		delete [] m_ppDefectImgList[CrStep];
		m_ppDefectImgList[CrStep] = nullptr;
	}
	delete [] m_ppDefectImgList;
	m_ppDefectImgList = nullptr;

	LeaveCriticalSection(&m_CrtSection);

	DeleteCriticalSection(&m_CrtSection);
}

void InspectDataQ::m_fnQSetProcessEnd(int NewState)
{
	EnterCriticalSection(&m_CrtSection);
	if(NewState == QDATA_PROC_END_SORT)
	{
		m_ScanLine				= -1;//기본 정보 Clear
		m_ScanImageIndex	= -1;//기본 정보 Clear
		m_NodeProcState		= QDATA_PROC_END_NONE;//모든 작업 완료.
	}
	else
		m_NodeProcState = NewState;
	LeaveCriticalSection(&m_CrtSection);
}

/** 
@brief		Get Grab Image  Data.

@remark
-			 Grab Image Pointer를 전달 한다.
-			
@author		최지형
@date		2010.03.00
*/ 
BYTE * InspectDataQ::m_fnQGetGrabImage()
{
	BYTE *pretValue = nullptr;
	if(m_pGrabImage != nullptr)
		pretValue = (BYTE*)m_pGrabImage->imageData;
	return pretValue;
}
//int *InspectDataQ::m_fnQGetActivePadArea()
//{
//	return &m_ActivePADArea[0];
//}
/** 
@brief		Get Grab Buffer  Data.

@remark
-			 Grab Buffer Pointer를 전달 한다.
-			
@author		최지형
@date		2010.03.00
*/ 
void InspectDataQ::m_fnQSetBufferData(BYTE * pNewData)
{
	//EnterCriticalSection(&m_CrtSection);
	//Inspect Buffer Create 변경
#ifdef USE_STATIC_BUFFER
	memcpy(m_pBufferData->imageData, pNewData, m_GrabImageSize);
#else
	m_pBufferData = new BYTE[m_GrabImageSize];
	memcpy(m_pBufferData, pNewData, m_GrabImageSize);
#endif
	//LeaveCriticalSection(&m_CrtSection);
}
/** 
@brief		Get Grab Buffer  Data.

@remark
-			 Grab Buffer Pointer를 전달 한다.
-			
@author		최지형
@date		2010.03.00
*/ 
BYTE* InspectDataQ::m_fnQGetBufferData()
{
	//Inspect Buffer Create 변경
#ifdef USE_STATIC_BUFFER
	return (BYTE*)m_pBufferData->imageData;
#else
	return m_pBufferData;
#endif
}
/** 
@brief		Defect Data Set

@remark
-			Inspector에서 Active Search 결과를 저장 한다.
-			
@author		최지형
@date		2010.03.00
*/ 

BOOL InspectDataQ::m_fnQSetDefectAllBuffer(int DefectNodeCount, DEFECT_DATA *pNewData)
{
	EnterCriticalSection(&m_CrtSection);
	
	if(DefectNodeCount > 0 && DefectNodeCount <= m_MaxDefectCnt )
	{
		m_DefectListCnt = DefectNodeCount;
		memcpy(m_pSearchRetList, pNewData, sizeof(DEFECT_DATA)*m_DefectListCnt);
	}
	else
	{
		m_DefectListCnt = 0;
		memset(m_pSearchRetList, 0x00, sizeof(DEFECT_DATA)*m_MaxDefectCnt);
	}
	//Inspect Buffer Create 변경
#ifdef USE_STATIC_BUFFER

#else
	if(m_pBufferData != nullptr)
	{
		delete [] m_pBufferData;
		m_pBufferData = nullptr;
	}
#endif
	m_fnQSetProcessEnd(QDATA_PROC_END_INSPECT);
	CountProc++;
	LeaveCriticalSection(&m_CrtSection);
	return TRUE;
}
