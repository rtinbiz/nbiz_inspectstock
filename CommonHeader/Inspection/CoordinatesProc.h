#pragma once
#include "CalculateCoordinateDefine.h"


//CPoint Type의 x = Axis Y와 연결 되며
//CPoint Type의 y = Axis X와 연결 된다. 좌표를 볼때는 일상적 좌상단 을 원점으로 보는 X, Y축으로 보면 된다.
//내부 사용 방식 이다.
//ReturnRect.left = Y_Axis_Start
//ReturnRect.right = Y_Axis_End
//ReturnRect.top = 	X_Axis_Start
//ReturnRect.bottom =  X_Axis_End
class CoordinatesProc
{
public:
	CoordinatesProc();
	~CoordinatesProc();
private:
	CvFont					m_BlockNameFont;
	CvFont					m_CellNameFont;
public:
	IplImage		*		m_pMainMpaDrawObj;
public:
	int						m_AOIOneCam_ScanLineCnt;			//<<<해당 Task의 Camera당 Scan 횟수.
	int						m_TotalScnaLineCnt;				//<<< Map전체 Scan Line Count.
	int						m_OneLineScnaImgCnt;				//<<< One Line Scan Image Count;

	int						m_TargetTaskNum;
	HW_PARAM				m_HWParam;//HW Setting Value.
	SAOI_IMAGE_INFO			m_InspectActiveParam;//==>>> Cutter에서 사용 하기 위해
	SAOI_COMMON_PARA		m_InspectParam;//==>>> Inspection Dll에 직접 넘겨 줘야 하는 값으로 따로 관리 한다.(Lens Offset 정보만 취한다.)

	SAOI_IMAGE_INFO			m_InspectPADMaskImageInfo[4];//0:상, 1:하, 2:좌, 3:우
	SAOI_COMMON_PARA		m_InspectPADCommonPara[4];	

	SAOI_PAD_AREA			m_stPadArea;

	int						m_ResolutionType;

	int						m_AXIS_Y_OVER_STEP;
	GLASS_PARAM				m_GlassParam;
	CString					m_ImageUpdatePath;
	CString					m_ResultUpdatePath;
	//Recipe Calculate Data List
	int						m_nBlockCnt;
	BLOCKINFO		*		m_pBlockList;
	int						m_nCellCnt;
	CELLINFO		*		m_pCellList;
	int						m_nPadCellCnt;
	PADINFO			*		m_pPadCellList;			//<<Pad부영역
	int						m_nScanImgCnt;
	SCANIMGINFO		*		m_pScanImgList;

	//Scan 방향에 대한 Pad부 즉 Active에 속한 Pad부처리를 위한것이다.(좌우 Pad부가 Active에 속할경우)
	//PAD부 방향이 바뀌는 것이 들어오면 따로 만들어줘야 한다.)
	bool m_fnActivePadAreaLRFind(int ScnaNum, int *pLeftStartLX, int *pLeftEndX, int *pRightStartX, int *pRightEndX);
	bool m_fnUpDnPadAlignCoordCalc(int ScnaNum, int UpDnIndex, PAlignCoordInfo *pAlignCoordData);
	void m_fnFindRectCellPadInfo(int PadIndex, CRect *pFindArea, int *pResultCnt, CRect *pResultList);
private:
	int m_RealCellCnt;
	RealCellPos *m_pRealCellPosList;

	bool	m_fnGlassDataCalculate(int RealCellCnt = 0, RealCellPos *pInputRealCellPosList = nullptr);
	bool	m_fnGlassScanCalculate();

	bool	m_fnFindIntersectScanPos(SCANIMGINFO * pScanImgBuf, IplImage *pScanView = nullptr);
	bool	m_fnGlassPADAreaCalculate();
public:
	void 	m_fnDrawGlassInspectInfo();
	//함수의 인자 순서는 가로축, 세로축 기준으로 입력 된다. (축의 X,Y기호일뿐이고 . 가로축 세로축을 기준으로 한다.)
	bool	m_fnSetRecipeData(int TaskNum, GLASS_PARAM *	pGlassParam);
	int m_RealScanType;
	bool	m_fnSetRealCellPos(int RealCellCntX, int RealCellCntY, int *pDisListX, int *pDisListY );
	bool	m_fnSetRealCellPos(int RealCellCntX, int RealCellCntY, CvRect *pDataList );

public:
	//
	
	SCANIMGINFO* m_fnGetCalculateAreaInImg(int ScnaNum, int ImgIndex);

	//CRect	MachinPos_GetCellRect(				int *pCellIndex_YAxis, int *pCellIndex_XAxis);
	//CRect	MachinPos_GetROIRect(				int *pCellIndex_YAxis, int *pCellIndex_XAxis);
	//BOOL	MachinPos_GetROIAndCellWorkRect(	int *pCellIndex_YAxis, int *pCellIndex_XAxis,  
	//	CRect * pROIRect, CRect * pCellRectWork);

	////////////////////////////////////////////////////////////////////////////
	//BOOL		MachinPos_GetScanImageRect(		int *pScanLineIndex, int *pScanImageIndex, CRect  *pGrabImageMachinePos);
	//COODPOINT	MachinPos_GetImageXYPos(		int *pScanLineIndex, int *pScanImageIndex, CPoint *pinImagePosXY);
	//COODPOINT	WorkPos_GetDefectPos(			int *pScanLineIndex, int *pScanImageIndex, CPoint *pInImageDefectXY);
	//BOOL		WM_GetDefectPosWorkAndMachine(	int *pScanLineIndex, int *pScanImageIndex, CPoint *pInImageDefectXY,
	//	COODPOINT*pMachinPos, COODPOINT *pWorkPos);

	////////////////////////////////////////////////////////////////////////////
	//COODPOINT	MachinPosToWorkPos(COODPOINT *pMachinePosXY);
	//COODPOINT	WorkPosToMachinPos(COODPOINT *pWorkPosXY);	
};