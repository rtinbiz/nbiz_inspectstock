#pragma once
#include "AnaimAOI_Struct.h"
#include "DefineStaticValue.h"

//////////////////////////////////////////////////////////////////////////
/** 
@brief		Glass 내의 Block 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
typedef struct BLOCK_INFO_TAG
{
	char	 m_BlockName[NAME_BUF_CNT];
	CRect m_BlockPos;

	BLOCK_INFO_TAG()
	{
		memset(this,0x00, sizeof(BLOCK_INFO_TAG));
	}
} BLOCKINFO;
//////////////////////////////////////////////////////////////////////////
/** 
@brief		Glass 내의 Cell 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
typedef struct CELLINFO_TAG
{
	char	 m_CellName[NAME_BUF_CNT];
	CRect m_CellPos;
	CELLINFO_TAG()
	{
		memset(this,0x00, sizeof(CELLINFO_TAG));
	}
} CELLINFO;
//////////////////////////////////////////////////////////////////////////
/** 
@brief		Glass 내의 Cell 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
typedef struct SCANIMGINFO_TAG
{
	int			m_ScanLineNum;
	int			m_ScanImgIndex;
	CRect		m_ScanImgPos;							//<<< Scna Image의 물리 좌표이다.


	//////////////////////////////////////////////////////////////////////////
	//Active, Pad 부의 영역을 계산 하기 위해서.
	int			m_findSubRecCnt[5];					//<<< Scan Image에 대한 Cell의 Active, Pad부에 대해 몇개의 겹침영역이 생기는지 Count 한다.
	CRect		m_IspectAreaSubtractRect[5][4];	//<<< 겹침 영역이 생기면 계속 추가 한다. (Active, Top, Bottom, Left, Right에대한 최대 4개의 겹침 영역을 의미한다.
	CRect		m_InImgPositionRect[5][4];	//<<< Image 좌표계로 변환 된 Data를 저장 한다.

	CRect		m_ORGTargetRect[5][4];	//<<< 대상이미지에 겹침이 발생 하는 좌표.
	SCANIMGINFO_TAG()
	{
		memset(this,0x00, sizeof(SCANIMGINFO_TAG));
	}
} SCANIMGINFO;
//////////////////////////////////////////////////////////////////////////
/** 
@brief		Glass 내의 Cell Pad부 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
typedef struct PADINFO_TAG
{
	char	 m_CellName[NAME_BUF_CNT];
	CRect m_CellPos_Top;			//<<<Cell의 상단 Pad부
	CRect m_CellPos_Bottom;		//<<<Cell의 하단 Pad부
	CRect m_CellPos_Left;			//<<<Cell의 왼쪽단 Pad부
	CRect m_CellPos_Right;			//<<<Cell의 오른쪽단 Pad부
	PADINFO_TAG()
	{
		memset(this,0x00, sizeof(PADINFO_TAG));
	}
} PADINFO;


//Pad부 검사 방향설정.
#define  INSPACT_PAD_SKIP				0
#define  INSPACT_PAD_UPDOWN			1
#define  INSPACT_PAD_LEFTRIGHT		2
#define  INSPACT_PAD_BOTH				3

//////////////////////////////////////////////////////////////////////////
/** 
@brief		Stage X, Y의 좌표계 구조체.

@remark
-			위치는 Axis 기준 Y, X로 분리 된다.
-			
@author		최지형
@date		2010.00.00
*/
//typedef struct COORDINTE_POINT_TAG
//{
//	int COOD_Y;
//	int COOD_X;
//
//	COORDINTE_POINT_TAG()
//	{
//		memset(this,0x00, sizeof(COORDINTE_POINT_TAG));
//	}
//	COORDINTE_POINT_TAG(int CoodX, int CoodY)
//	{
//		COOD_X = CoodX;
//		COOD_Y = CoodY;
//	}
//} COODPOINT;





/** 
@brief		Inspector에 전달되는 Parameter

@remark
-			결함 검출에 필요한 필터 및 부속 데이터 구조체.
-			
@author		최지형
@date		2010.03.00
*/ 
typedef struct HW_PARAM_TRG
{
	int		m_VSB_Size;

	int		m_ProcImageW;
	int		m_ProcImageH;

	int		m_DefectMaxCntInImg;						//<<< Process Image당 Max Defect의 갯수.
	int		m_DefectReviewSizeW;						//<<< Defect당 Review용 이미지의 넓이
	int		m_DefectReviewSizeH;						//<<< Defect당 Review용 이미지의 높이

	int		m_INSPECTION_CAM_COUNT;						//<<< System에 적용될 AOI Camera Count

	int		m_INSPECTION_SCAN_WIDTH;					//<<< Scan할 Total 넓이.(um단위)
	int		m_INSPECTION_SCAN_HEIGHT;					//<<< Scan할  Total 높이.(um단위)

	int		m_AOI_BLOCK_SCAN_START_X;					//<<<기준점.기준의 Block Scan이 시작되는 위치.(um단위)
	int		m_AOI_BLOCK_SCAN_START_Y;					//<<<기준점. Cam기준의 Block Scan이 시작되는 위치.(um단위)
	
	//////////////////////////////////////////////////////////////////////////
	//Inspect Camera 별.
	int		m_AOI_CAM_DISTANCE_X;						//<<< 기준점. Cam Center에서 AOI Cam과의 거리.(um단위)
	int		m_AOI_CAM_DISTANCE_Y;						//<<< 기준점. Cam Center에서 AOI Cam과의 거리.(um단위)

	int		m_AOI_SCAN_START_OFFSET_ODD;				//<<Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(홀수 Scan시 적용)(um단위)
	int		m_AOI_SCAN_START_OFFSET_EVEN;				//<<Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(짝수 Scan시 적용)(um단위)

	//////////////////////////////////////////////////////////////////////////
	int		m_OnlyGrabImageSave;
	int		m_SaveDefectOrgImage;
	int		DefectSaveFileType;

	int		PadInspectAreaType;							//<<1: 상하, 2:좌우. 3:양쪽다.

	int		CamScanDirectionPort;						//Camera Scan Direction을 바꾸기위한 Serial Port
	//////////////////////////////////////////////////////////////////////////
	char strResultPath[MAX_PATH];
	char strOrgImgPath[MAX_PATH];

	float m_LensCurvature_Type1;
	float m_LensCurvature_Type2;
	HW_PARAM_TRG()
	{
		memset(this,0x00, sizeof(HW_PARAM_TRG));
	}
} HW_PARAM, *LPHW_PARAM;

/** 
@brief		Recipe 정보 저장 구조체

@remark
-			Recipe정보를 갖는다 (입력은 um단위로 한다.)
-			
@author		최지형
@date		2010.03.00
*/ 
typedef struct GLASS_PARAM_TRG
{

	//////////////////////////////////////////////////////////////////////////
	int		m_ScanLineShiftDistance;			//<<< Scna Line Shift 거리.(um단위)검사 Resolution에 따라 변경된다.
	float	m_ScanImgResolution_W;				//<<검사 Pixel Size.(um단위)검사 Resolution이 된다.
	float	m_ScanImgResolution_H;				//<<검사 Pixel Size.(um단위)

	//Glass Center Position으로 Machine좌표를 입력 한다.
	int CenterPointX;							//<<um단위 이다.
	int CenterPointY;							//<<um단위 이다.

	int PadStartDistanceX;						//<<CELL_EDGE_X에서 거리.(Pad부는 Active 보다 항상 크다.)
	int PadStartDistanceY;						//<<CELL_EDGE_Y에서 거리.(Pad부는 Active 보다 항상 크다.)
	int PadEndDistanceX;						//
	int PadEndDistanceY;						//

	int ActiveDeadZoneSize;						//<<Acive 영역의 Dead Zone을 설정한다. (상, 하, 좌, 우 모든 구간에 대하여 적용 된다.(um단위)

	int PadStartDeadZoneDisX;					//<<Pad 영역의 Dead Zone을 설정한다. : 좌 Dead Zone.
	int PadStartDeadZoneDisY;					//<<Pad 영역의 Dead Zone을 설정한다. : 상 Dead Zone.
	int PadEndDeadZoneDisX;						//<<Pad 영역의 Dead Zone을 설정한다. : 우 Dead Zone.
	int PadEndDeadZoneDisY;						//<<Pad 영역의 Dead Zone을 설정한다. : 하 Dead Zone.

														//<<하부 Pad부의 좌우 하단에 동일하게 적용 된다.(Cell의 Mark 설정이 되는 부분을 제거 한다.)
	int PadDnDeadZoneWidth;						//<<하부 Pad 영역의 Dead Zone을 설정한다. 넓이.
	int PadDnDeadZoneHeight;					//<<하부 Pad 영역의 Dead Zone을 설정한다. 높이.

	int ActivePadDeadZone;						//<<Acive 영역의 Pad Dead Zon(Left, Right 상하 Dead Zone이다.)
	//////////////////////////////////////////////////////////////////////////
	int STICK_WIDTH;//Stick 검사기 신규 추가.
	int STICK_EDGE_DIS_WIDTH;
	int STICK_EDGE_DIS_HEIGHT;
	//Center Point (0,0)을 기준으로 거리 Offset이다.(설계치는 mm단위이고 입력은 um로 한다.)
	int CELL_EDGE_X;							//<<um단위 이다.
	int CELL_EDGE_Y;							//<<um단위 이다.
	int CELL_DISTANCE_X;						//<<um단위 이다.
	int CELL_DISTANCE_Y;						//<<um단위 이다.
	int CELL_SIZE_X;							//<<um단위 이다.
	int CELL_SIZE_Y;							//<<um단위 이다.

	//축 기준으로 몇개의 Cell이 있는지 나타 낸다.
	int GLASS_CELL_COUNT_X;				
	int GLASS_CELL_COUNT_Y;				

	//Block 갯수를 나타낸다.
	int BLOCK_DISTANCE_X;						//<<um단위 이다.
	int BLOCK_DISTANCE_Y;						//<<um단위 이다.

	//축 기준으로 몇개의 Block이 있는지 나타 낸다.
	int GLASS_BLOCK_COUNT_X;				
	int GLASS_BLOCK_COUNT_Y;				

	//////////////////////////////////////////////////////////////////////////
	char m_GlassName[128];						//<<< Glass Name(Last Name 저장 폴더를 지정)

	GLASS_PARAM_TRG()
	{
		//int SizeofTyup = sizeof(GLASS_PARAM_TRG);
		memset(this,0x00, sizeof(GLASS_PARAM_TRG));
	}
} GLASS_PARAM, *LPGLASS_PARAM;



/** 
@brief		Cell위치의 Real Position

@remark
-			Cell의 좌표를 받는다. 기준점을 위치로 Distance로 표현된다.
-			
@author		최지형
@date		2012.00.00
*/
typedef struct Real_Cell_Pos_TAG
{
	int PosStartX;
	int PosStartY;

	int PosEndX;
	int PosEndY;

	Real_Cell_Pos_TAG()
	{
		memset(this,0x00, sizeof(Real_Cell_Pos_TAG));
	}
} RealCellPos;


typedef struct PadAlignCoordInfo_Tag
{
	//int								 m_PadAlignAreaStartX;
	//int								 m_PadAlignAreaEndX;

	int								 m_BigAreaStartX;
	int								 m_BigAreaEndX;

	//int								 m_SmallAreaStartX;
	//int								 m_SmallAreaEndX;
	PadAlignCoordInfo_Tag()
	{
		memset(this, 0x00, sizeof(PadAlignCoordInfo_Tag));
	}
}PAlignCoordInfo;

 #pragma pack()