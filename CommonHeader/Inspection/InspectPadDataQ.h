#pragma once
#pragma pack(1)
#include "DefineStaticValue.h"


class InspectPadDataQ
{
public:
	InspectPadDataQ();
	InspectPadDataQ(int PadPosIndex, int ImgWidth, int ImgHeight, int ImgFragmentatSizeH, int MaxDefectCnt);//, int ReviewSizeW, int ReviewSizeH);
	~InspectPadDataQ(void);

private: 
	CRITICAL_SECTION	m_CrtSection;
	int					m_PADImageSize;
public:
	//Node 간 연결점으로 사용 된다.
	InspectPadDataQ	*	m_pFrontNode;
	InspectPadDataQ	*	m_pNextNode;
public:
	int					m_PadPosIndex;//ex) Up Type에서 몇번째 인지 지정 한다.
	int					m_DeadZoneSize;
	int					m_DNDeadZoneW;
	int					m_DNDeadZoneH;

	int					m_MaxDefectCnt;

public:
	ComRect				m_PadInScanPos;

	IplImage		*	m_pPADAreaImg;

	int				*	m_pImgFragmentatStep;
	int					m_ImgFragmentatSizeH;
	CvRect				m_FragmentRect;
	IplImage		*	m_pImgFragmentat;
	//////////////////////////////////////////////////////////////////////////
	int					m_DefectListCnt;												//<<<검출 Defect의 갯수.
	DEFECT_DATA		*	m_pSearchRetList;											//<<<Image상의 Defect위치
	COORD_DINFO		*	m_pCoordinateRetList;										//<<<좌표계 상의 Defect위치

	//////////////////////////////////////////////////////////////////////////
	//BYTE				**		m_ppDefectImgList;											//<<<지정 Size만큼의 Defect Image.

#ifdef USE_STATIC_BUFFER
	IplImage		*	m_pBufferData;										//<<<Grab된 Image.	
#else
	BYTE			*	m_pBufferData;
#endif

public:
	void m_fnQSetPadMPosition(ComRect *pNewArea);
	//////////////////////////////////////////////////////////////////////////
	// Inspect DLL에서 사용할 Function.
	BYTE *		m_fnQGetPADImage();					//저장된 Grab Image를 가져온다.
	BOOL			m_fnQSetDefectAllBuffer(int DefectNodeCount, DEFECT_DATA *pNewData);

	void			m_fnQSetBufferData(BYTE * pNewData);					//<<<저장된 Buffer Data를 가져온다.
	BYTE *		m_fnQGetBufferData();											//<<<저장된 Buffer Data를 가져온다.

	void		m_fnQCleanData();					//저장된 Data를 Clear 한다.

	void QDataCpy(IplImage *pOrg, CvRect *pOrgPos, CvRect *pDestPos);


	//////////////////////////////////////////////////////////////////////////
	//Result Process에서 처리 완료 Setting.
	int							m_NodeProcState;											//<<<현재 Node의 Process 진행 상태를 알려준다.
	void			m_fnQSetProcessEnd(int NewState);								//<<<각각의 Process에서 호출되어져야 한다..
};

#pragma pack()