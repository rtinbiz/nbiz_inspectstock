
#include "AnaimAOI_Struct.h"

#ifndef		ANAIMAOI_INTERFACE_H
#define		ANAIMAOI_INTERFACE_H

#define  AOI_DLL_PROCESS_CNT_MAX	8
#define  AOI_DLL_PROCESS_CNT		4

#define  AOI_DLLPROCESS_STATE_NONE	0
#define  AOI_DLLPROCESS_STATE_RUN	1
#define  AOI_DLLPROCESS_STATE_IDLE	2


static	const enum	ERROR_CODE{	ERR_SOURCE_FILE = 1001,
								ERR_ALIGN_FILE,
								ERR_MASK_FILE,
								ERR_THREAD_COUNT,
								ERR_THREAD_NOT_CREATE};


static const int    DEFECT_SEARCH_MODE = 0;

static const int    DOGBON_SEARCH_MODE = 1;

static const int	DEFAULT_THREAD_COUNT	= 2;

//static const int	OKAY					= 1;

extern "C" __declspec(dllimport) int  _AOI_Act_Defect_Inspection_Start( int nThread = DEFAULT_THREAD_COUNT, 
	UINT *pProcStateArea = NULL);
extern "C" __declspec(dllimport) int  _AOI_Act_Defect_Inspection_Stop();

extern "C" __declspec(dllimport) int  _AOI_Act_SearchParameterSet( SAOI_IMAGE_INFO	sMaskImageInfo,
	SAOI_COMMON_PARA		sCommonPara);

extern "C" __declspec(dllimport) int  _AOI_Act_SearchScaleSet( SAOI_PAD_AREA stPadArea );

extern "C" __declspec(dllimport)  void  _AOI_Act_PushItem( InspectDataQ* cInspectDataQ );

extern "C" __declspec(dllimport) int  _AOI_Pad_Defect_Inspection_Start( int nThread = DEFAULT_THREAD_COUNT, 
	UINT *pProcStateArea = NULL);

extern "C" __declspec(dllimport) int  _AOI_Pad_SearchParameterSet( SAOI_IMAGE_INFO	sMaskImageInfo,
	SAOI_COMMON_PARA		sCommonPara, int nPosition);

extern "C" __declspec(dllimport)  void  _AOI_Pad_PushItem( InspectPadDataQ* cInspectDataQ );

extern "C" __declspec(dllimport) int  _AOI_Pad_Process_Start( int nPosition, int nPositionCount );
extern "C" __declspec(dllimport) int  _AOI_Pad_Defect_Inspection_Stop();

#endif