#pragma once

#pragma pack(1)

//Speed
#undef  B9600
#define B9600 9600
#undef  B19200
#define B19200 19200
#undef  B38400
#define B38400 38400
#undef  B57600
#define B57600 57600
#undef  B115200
#define B115200 115200


// Stop bits
#define	RS_STOP1	0
#define	RS_STOP1_5	1
#define	RS_STOP2	2

// Parity bits
#define	RS_PARITY_NONE	0	// 'N'
//#define NONE 0
#define	RS_PARITY_ODD	1	// 'O'
#define	RS_PARITY_EVEN	2	// 'E'
#define	RS_PARITY_MARK	3	// 'M'
#define	RS_PARITY_SPACE	4	// 'S'


//Client To Server Send RS232 Port List
#define SPR_PORT_None		0
#define SPR_PORT_LIST_REQ 1
#define SPR_PORT_OPEN		2
#define SPR_PORT_CLOSE		3
#define SPR_PORT_DSEND		4
#define SPR_PORT_PDSEND	5

#define SPR_PORT_RESULT_NG		0
#define SPR_PORT_RESULT_OK		1




#define SPR_PORT_OPEN_ERROR1		-1 //해당 Link에서 이전 Open Port와 다른 포트 열기를 시도 했다.
#define SPR_PORT_OPEN_ERROR2		-2 //해당 Port를 Open 할 수 없습니다.

#define SPR_PORT_SEND_ERROR1		-3 //해당 Port를 Send 할 수 없습니다.(Open되어 있지 않음)
#define SPR_PORT_SEND_ERROR2		-4 //해당 Port에 이상이 생겼다(Data 전송 불DataSize

#define SPR_PORT_TIMEOUT		-500 //명령수행중 시간 초과.


#define SPR_CMD_PROC_NONE	10
#define SPR_CMD_PROC_RUN		11
#define SPR_CMD_PROC_END		12



typedef struct RecvCheckTag
{
	BYTE ASCII_EndFlag;//ASCII Mode일때 Receive End Check
	BYTE B_StartFlag;//Binary Mode일때 Receive End Check And Size
	int		B_DataSize;
	RecvCheckTag()
	{
		ASCII_EndFlag = (BYTE)'\n';
		B_StartFlag = 0;
		B_DataSize = 0;
	}
}RecvCheck;

typedef struct  PORT_OPEN_Info_AGT
{
	char OPortName[30];
	//////////////////////////////////////////////////////////////////////////
	//SPR_PORT_OPEN
	DWORD nRate;
	BYTE nParity;
	BYTE nSize;
	BYTE nStop;

	RecvCheck ComEndCheck;

	PORT_OPEN_Info_AGT()
	{
		memset(OPortName, 0x00, 30);
		nRate = B9600;
		nParity = RS_PARITY_NONE;
		nSize = 8;
		nStop = RS_STOP1;
		memset(&ComEndCheck, 0x00, sizeof(RecvCheck));
	}
}OPEN_Info;


typedef struct  SPR_CMD_AGT
{
	int SPRCommand;
	int SPRCommandRet;
	int DataSize;
	SPR_CMD_AGT()
	{
		memset(this, 0x00, sizeof(SPR_CMD_AGT));
	}
}SPR_CMD;








#pragma pack()