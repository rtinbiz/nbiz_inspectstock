#pragma once

#define SQ_STEP_ERROR				-2
#define SQ_STEP_AUTO_INC			-1
#define SQ_STEP_IDLE					0
#define SQ_STEP_RUN					1

//Seq진행 상태를 알려준다.
typedef int (*pNowStepSq)(void *pRecvObj, int NowStep);

typedef struct SEQ_PROCESS_DATA_TAG
{
	bool					m_bProcessRun;
	CWinThread			*m_pRunnerThread;

	void					*pCtrlObj;

	//////////////////////////////////////////////////////////////////////////
	//진행 상태를 알려준다.
	void					*pRecvObj;
	pNowStepSq		 pfunction;

	SEQ_PROCESS_DATA_TAG()
	{
		m_bProcessRun				= false;
		m_pRunnerThread				= nullptr;
		pCtrlObj							= nullptr;

		pCtrlObj							= nullptr;
		pRecvObj						= nullptr;
	}
}SEQ_PROC_DATA;



class CSeqBase
{
public:
	CSeqBase(void);
	~CSeqBase(void);
public:
	int m_OldStep;
	int m_NowStep;
	
	SEQ_PROC_DATA m_SeqProcData;
	
public:
	int fnSeqInit(void *pRecvObj = nullptr, pNowStepSq pfunction= nullptr);

	int fnSeqSetNextStep(int StepIndex = SQ_STEP_AUTO_INC);

	bool m_SeqStop;
	int fnSeqErrorReset();
	int fnSeqSetStop();

	int fnSeqGetNowStep(){return m_NowStep;};
	int fnSeqGetOldStep(){return m_OldStep;};
	virtual int fnSeqFlowProc(int ProcStep){return fnSeqGetNowStep();};
	virtual int fnSeqErrorStepProc(){return 0;};
	
};
UINT th_SeqRunProcess(LPVOID pParam);
