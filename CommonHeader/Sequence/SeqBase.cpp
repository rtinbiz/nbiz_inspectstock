#include "StdAfx.h"
#include "SeqBase.h"

UINT th_SeqRunProcess(LPVOID pParam)
{
	SEQ_PROC_DATA *pData =(SEQ_PROC_DATA *)pParam;
	CSeqBase *pFlowObj = (CSeqBase*)pData->pCtrlObj;
	
	int NowStep = SQ_STEP_IDLE;
	int ProcStep = SQ_STEP_IDLE;

	while(pData->m_bProcessRun== true)
	{
		NowStep = pFlowObj->fnSeqGetNowStep();
		ProcStep = pFlowObj->fnSeqFlowProc(NowStep);
		
		if(pFlowObj->m_SeqStop == true)
		{
			ProcStep = SQ_STEP_IDLE;
			pFlowObj->fnSeqSetNextStep(SQ_STEP_IDLE);
			pFlowObj->m_SeqStop = false;
		}

		if(NowStep != ProcStep)
		{//CallBack 함수를 호출하여 현상태를 알린다.
			if(pData->pfunction != nullptr && pData->pRecvObj != nullptr)
			{
				NowStep = pFlowObj->fnSeqGetNowStep();
				pData->pfunction(pData->pRecvObj, NowStep);
			}

			//Error가 발생 했을 때 처리할것을 호출.
			if(ProcStep == SQ_STEP_ERROR)
				pFlowObj->fnSeqErrorStepProc();
		}

		Sleep(10);
	}
	pData->m_pRunnerThread = nullptr;
 	return 0;
}
CSeqBase::CSeqBase(void)
{
	m_OldStep = SQ_STEP_IDLE;
	m_NowStep = SQ_STEP_IDLE;
	m_SeqStop = false;
}

CSeqBase::~CSeqBase(void)
{
	m_NowStep = SQ_STEP_IDLE;
	m_SeqProcData.m_bProcessRun = false;
	while(m_SeqProcData.m_pRunnerThread != nullptr)
	{
		Sleep(20);
	}
}

int CSeqBase::fnSeqInit(void *pRecvObj, pNowStepSq pfunction)
{
	if(m_SeqProcData.m_pRunnerThread!=nullptr)
		return m_NowStep;

	m_SeqProcData.pRecvObj					= pRecvObj;
	m_SeqProcData.pfunction					= pfunction;
	m_SeqProcData.pCtrlObj					= this;
	m_SeqProcData.m_bProcessRun			= true;
	m_SeqProcData.m_pRunnerThread		= ::AfxBeginThread(th_SeqRunProcess, &m_SeqProcData, THREAD_PRIORITY_NORMAL);

	return m_NowStep;
}
void CSeqBase::fnSeqExit()
{
	m_NowStep = SQ_STEP_IDLE;
	m_SeqProcData.pRecvObj	= nullptr;
	m_SeqProcData.pfunction	 = nullptr;	
	m_SeqProcData.pCtrlObj	 = nullptr;	

	m_SeqProcData.m_bProcessRun = false;
	while(m_SeqProcData.m_pRunnerThread != nullptr)
	{
		Sleep(20);
	}
}
int CSeqBase::fnSeqErrorReset()
{
	if(m_NowStep == SQ_STEP_ERROR)
	{
		m_OldStep = SQ_STEP_ERROR;
		m_NowStep = SQ_STEP_IDLE;
//		m_SeqStop = false;
		//상태 전의를 알린다.
		if(m_SeqProcData.pfunction != nullptr && m_SeqProcData.pRecvObj != nullptr)
			m_SeqProcData.pfunction(m_SeqProcData.pRecvObj, m_NowStep);
	}
	return m_NowStep;
}
int CSeqBase::fnSeqSetNextStep(int StepIndex)
{
	//if(m_SeqStop == true)
	//{
	//	m_OldStep = SQ_STEP_IDLE;
	//	m_NowStep = SQ_STEP_IDLE;
	//	m_SeqStop = false;
	//	return m_NowStep;
	//}
	if(m_NowStep != SQ_STEP_ERROR )
	{
		if(SQ_STEP_AUTO_INC == StepIndex)
		{
			m_NowStep += 1;
			m_OldStep = m_NowStep;
		}
		else
		{
			m_NowStep = StepIndex;
			m_OldStep = m_NowStep;
		}
	}
	
	return m_NowStep;
}

int CSeqBase::fnSeqSetStop()
{
	
	//현재 진행 스탭은 끝내야 한다.
	//m_SeqStop =  true;
	//pNowStepSq		 pbfunction = m_SeqProcData.pfunction;
	//m_SeqProcData.m_bProcessRun = false;
	//m_SeqProcData.pfunction		   = nullptr;
	//Sleep(10);
	//int CntInit = 100;
	//while(m_SeqProcData.m_pRunnerThread != nullptr)
	//{
	//	Sleep(10);
	//	//CntInit--;
	//	//if(CntInit == 0)
	//	//{

	//	//	::TerminateThread(m_SeqProcData.m_pRunnerThread, 0);
	//	//	m_SeqProcData.m_pRunnerThread = nullptr;
	//	//}
	//}
	//m_SeqProcData.m_pRunnerThread = nullptr;
	//m_SeqStop = false;
	////////////////////////////////////////////////////////////////////////////
	//m_OldStep = SQ_STEP_IDLE;
	//m_NowStep = SQ_STEP_IDLE;
	////신규 Step을 만든다.
	//m_SeqProcData.pfunction					= pbfunction;
	//m_SeqProcData.pCtrlObj					= this;
	//m_SeqProcData.m_bProcessRun			= true;
	//m_SeqProcData.m_pRunnerThread		= ::AfxBeginThread(th_SeqRunProcess, &m_SeqProcData, THREAD_PRIORITY_NORMAL);


	m_SeqStop =  true;

	////////////////////////////////////////////////////////////////////////////
	//m_OldStep = SQ_STEP_IDLE;
	//m_NowStep = SQ_STEP_IDLE;
	//fnSeqErrorReset();
	return m_NowStep;
}