
#pragma once
//////////////////////////////////////////////////////////////////////////
//Opecv 2.4.9
//
//Depth Link변경을 위해 절대 경로를 사용함.
//////////////////////////////////////////////////////////////////////////
#ifdef WIN64
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL64/opencv_core249.lib")
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL64/opencv_imgproc249.lib")
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL64/opencv_highgui249.lib")

	#ifdef _DEBUG
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL64/tbb_debug.lib")
	#else
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL64/tbb.lib")
	#endif
#else
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL32/opencv_core249.lib")
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL32/opencv_imgproc249.lib")
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL32/opencv_highgui249.lib")

	#ifdef _DEBUG
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL32/tbb_debug.lib")
	#else
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL32/tbb.lib")
	#endif
#endif
#define cvCopyImage( src, dst )         cvCopy( src, dst, 0 )
#define cvQueryHistValue_1D( hist, idx0 ) \
	((float)cvGetReal1D( (hist)->bins, (idx0)))
#include "ImgProc/OpenCV249/opencv2/opencv.hpp"