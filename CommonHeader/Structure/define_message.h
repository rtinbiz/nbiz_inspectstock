/** 
@addtogroup	VS64_COMMON
@{
*/ 

/**
@file		ClientStructure.h
@brief		모든 Module 에서 사용하는 공용 구조체를 정의 한다.

@remark
@author		김용태
@date		2006.
*/

#pragma once

/** 
@brief		Client 간 메시지 전송에 사용되는 구조체이다.

@remark		
@author		김용태
@date		2006.
*/ 
struct CMDMSG
{
	USHORT	uStation_Src;		///< 근원지 Station No
	USHORT	uStation_Dest;		///< 목적지 Station No
	USHORT	uTask_Src;			///< 근원지 Task
	USHORT	uTask_Dest;			///< 목적지 Task
	USHORT	uFunID_Src;			///< 근원지 Function No
	USHORT	uSeqID_Src;			///< 근원지 Sequence No
	USHORT	uUnitID_Src;		///< 근원지 Unit No
	USHORT	uFunID_Dest;		///< 목적지 Function No
	USHORT	uSeqID_Dest;		///< 목적지 Sequence No
	USHORT	uUnitID_Dest;		///< 목적지 Unit No
	USHORT	uRetStatus;			///< 반환시 수행 결과 값, 정상 : SEQ_OK, 비정상 : Error Code
	USHORT	uMsgType;			///< CMD_TYPE_RES, CMD_TYPE_NORES, 현재 메시지가 응답이 필요한지 유무를 나타낸다.
	USHORT	uMsgOrigin;			///< CMD_TYPE_REPLY, CMD_TYPE_NOREPLY, 현재 메시지가 Respons 인지 COmmand 인지를 나타낸다.
	USHORT	uTimeOut;			///< 메시지 Time out (현재 미구현)
	USHORT	uMsgSize;			///< 전송되는 메시지 Buffer Size
	UCHAR	*cMsgBuf;			///< 메시지 Buffer
	UCHAR	*cDummy;			///< 기능 없음

	CMDMSG()					///< 구조체 생성자, 초기 생성과 동시에 메모리를 초기화 한다.
	{
		memset(this, 0x00, sizeof(CMDMSG));
	}
};

//Basic Task Flame 내로 분리 되어야 할 ITEM... Work Item!!!!!!!!!!!!!!!!!!
/** 
@brief		Basic Sequence Flame 에서 사용하는 메시지 버퍼, 구조체 배열로 선언해서 사용한다.

@remark		구조체 배열로 선언하여 Basic Sequence Flame 내에서 Sequence Message buffer 로 사용한다.
@author		김용태
@date		2006.
*/ 
struct CMDMSG_BUF
{
	USHORT	uUseFlag;								///< 현재 Buffer 가 사용 중인지 여부를 가진다.
	CMDMSG	sCmdMsg;								///< 다른 Task 에서 전달 받은 메시지의 정보를 가진다.
	UCHAR	cRecvBuf[MAX_CLIENT_MESSAGE_BUFFER];	///< 메시지 Data

	CMDMSG_BUF()									///< 구조체 생성자, 초기 생성과 동시에 메모리를 초기화 한다.
	{
		memset(this, 0x00, sizeof(CMDMSG_BUF));
	}
};

/** 
@brief		공유 메모리에 접근하기 위해 사용되는 구조체이다.

@remark	
@author		김용태
@date		2006.
*/ 
struct SMPMSG						
{
	USHORT	uStation;										///< 근원지 Station No (0 으로 설정하면 자동으로 자신의 Station No 로 입력된다.)
	USHORT	uTaskNo;										///< 근원지 Task No
	USHORT	uFuncNo;										///< 근원지 Function No, Server의 처리에는 필요하지 않다.. 참고 값으로 사용한다.
	USHORT	uFileNo;										///< 접근 할 File No
	USHORT	uAccessType;									///< 메모리 번지 직접 접근, ID를 통한 접근인가의 구분자
	USHORT	uStartPos;										///< 직접 접근시의 메모리 시작 지점
	USHORT	uLength;										///< 직접 접근시의 시작 지점으로 부터의 길이
	char	cAccessID[MAX_SHARED_MEMORY_ACCESS_ID_NAME];	///< ID 접근시의 ID name
	UCHAR*	cSMPBuf;										///< Parameter Buffer

	SMPMSG()												///< 구조체 생성자, 초기 생성과 동시에 메모리를 초기화 한다.
	{
		memset(this, 0x00, sizeof(SMPMSG));
	}
};

/** 
@brief		Client 에서 VS60LOGT로 LogData를 보내는 Log Level

@remark		Log Level 을 보고 Log 기록 Message 전송 전, Log Task 메시지 기록전 메시지의 처리를 차단 하게된다.
@author		최지형
@date		2006.10.23
*/ 
enum LOG_LEVEL
{
	LOG_LEVEL_ALWAYS = 0,	//MAX Log Level.
	LOG_LEVEL_1,
	LOG_LEVEL_2,
	LOG_LEVEL_3,
	LOG_LEVEL_4,			//MIN Log Level.
	LOG_LEVEL_NONE,
};

/** 
@brief		Log Task들의 List 를 가진다.

@remark		여러 Log Task 가 존재 하게 될 경우 필요하다.
@author		최지형
@date		2006.10.23
*/ 
enum LOG_TASK_LIST
{	
	DEFAULT_LOG_TASK = 60,
	APPEND_LOG_TASK_1,
	APPEND_LOG_TASK_2,
	APPEND_LOG_TASK_3,
	APPEND_LOG_TASK_4,
};

//InternalServer Interface 와 Log Task 에서만 사용되므로 다른 파일로 옮겨야 한다. Work Item !!!!!!!!!!!!!!!!!!!!!
/** 
@brief		Client 에서 Log Task 로 LogData를 보내는 structure

@remark		
@author		최지형
@date		2006.10.20
*/ 
struct WRITE_LOG_MSG
{
	USHORT	LogLevel;				///< Log Level.
	UCHAR	LogTime[30];			///< Log outbreak Time
	UCHAR	LogData[MAX_LOG_BUFF];	///< Log 내용

	///////////////////////
	WRITE_LOG_MSG()				///< 구조체 생성자, 초기 생성과 동시에 메모리를 초기화 한다.
	{
		memset(this, 0x00, sizeof(WRITE_LOG_MSG));
	}
};

/** 
@}
*/ 