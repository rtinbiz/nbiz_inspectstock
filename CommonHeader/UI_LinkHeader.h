#pragma once
//////////////////////////////////////////////////////////////////////////
//Button/Static Window UI
//
//
//////////////////////////////////////////////////////////////////////////
#define		WM_USER_BTUPDN_EVN							WM_USER + 1000
#define  USER_BT_DOWN		1
#define  USER_BT_UP			2
#include "./UI/ClassButton/RoundButton2.h"
#include "./UI/ClassButton/RoundButtonStyle.h"
#include "./UI/StaticSt/ColorStaticST.h"
#include "./UI/Progress/TextProgressCtrl.h"
#ifdef WIN64
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
#include "./UI/Grid64/GridCtrl.h"
#include "./UI/Grid64/NewCellTypes/GridURLCell.h"
#include "./UI/Grid64/NewCellTypes/GridCellCombo.h"
#include "./UI/Grid64/NewCellTypes/GridCellCheck.h"
#include "./UI/Grid64/NewCellTypes/GridCellNumeric.h"
#include "./UI/Grid64/NewCellTypes/GridCellDateTime.h"
#else
	#include "./UI/Grid/GridCtrl.h"
#endif



static const int	LIST_ALARM_COUNT = 200;
enum ListColor
{
	Log_Normal		= 0,
	Log_Check		= 1,
	Log_Worrying	,
	Log_Error		,
	Log_Info			,
	Log_Exception	
};
#include "./UI/ListBox/XListBox.h"
inline void CXListBoxAddString(CXListBox*pWriter, ListColor nType, CString strData)
{
	switch(nType)
	{
	case Log_Normal:
		pWriter->AddLine((CXListBox::Color)1, 	(CXListBox::Color)0, strData);
		break;
	case Log_Check:
		pWriter->AddLine((CXListBox::Color)1, 	(CXListBox::Color)3, 	strData);
		break;
	case Log_Worrying:
		pWriter->AddLine((CXListBox::Color)0, 	(CXListBox::Color)12, strData);
		break;
	case Log_Error:
		pWriter->AddLine((CXListBox::Color)1, (CXListBox::Color)10, strData);
		break;
	case Log_Info:
		pWriter->AddLine((CXListBox::Color)0, (CXListBox::Color)1, strData);
		break;
	case Log_Exception:
		pWriter->AddLine((CXListBox::Color)1, (CXListBox::Color)13, strData);
		break;
	default:
		pWriter->AddLine((CXListBox::Color)1, (CXListBox::Color)0, strData);
		break;
	}
}
inline TASK_State G_TaskStateChange(CWnd* pParent, CColorStaticST *pStateCtrl, TASK_State newTaskState )
{
	if(pStateCtrl == nullptr)
		return newTaskState;

	switch(newTaskState)
	{
	case TASK_STATE_NONE:
		{
			pStateCtrl->SetWindowText("NONE");
			// Set text blinking colors
			pStateCtrl->SetBlinkTextColors(RGB(255,255,255), RGB(0,0,0));
			// Set background blinking colors
			pStateCtrl->SetBlinkBkColors(RGB(128, 128, 128), RGB(255,255,255));
			//pStateCtrl->SetBkColor(G_Color_Off);
		}break;

	case TASK_STATE_INIT:
		{
			pStateCtrl->SetWindowText("INIT");
			// Set text blinking colors
			pStateCtrl->SetBlinkTextColors(RGB(0,0,255), RGB(0,0,0));
			// Set background blinking colors
			pStateCtrl->SetBlinkBkColors(RGB(255, 255, 0), RGB(0,255,0));
			//pStateCtrl->SetBkColor(G_Color_Off);
		}break;
	case TASK_STATE_IDLE:
		{
			pStateCtrl->SetWindowText("IDLE");
			// Set text blinking colors
			pStateCtrl->SetBlinkTextColors(RGB(0,0,255), RGB(0,0,0));
			// Set background blinking colors
			pStateCtrl->SetBlinkBkColors(RGB(0, 255, 0), RGB(0,255,0));
		}break;
	case TASK_STATE_RUN:
		{
			pStateCtrl->SetWindowText("RUN");
			// Set text blinking colors
			pStateCtrl->SetBlinkTextColors(RGB(255,0,0), RGB(0,0,255));
			// Set background blinking colors
			pStateCtrl->SetBlinkBkColors(RGB(0, 255, 0), RGB(0,255,0));
		}break;
	case TASK_STATE_ERROR:
		{
			pStateCtrl->SetWindowText("ERROR");
			// Set text blinking colors
			pStateCtrl->SetBlinkTextColors(RGB(255,0,0), RGB(0,0,255));
			// Set background blinking colors
			pStateCtrl->SetBlinkBkColors(RGB(0, 0, 255), RGB(255,0,0));
		}break;
	case TASK_STATE_PM:
		{
			pStateCtrl->SetWindowText("PM");
			// Set text blinking colors
			pStateCtrl->SetBlinkTextColors(RGB(0,0,255), RGB(0,0,0));
			// Set background blinking colors
			pStateCtrl->SetBlinkBkColors(RGB(255, 0, 0), RGB(255,0,0));
			//pStateCtrl->SetBkColor(G_Color_Off);
		}break;
	default:
		break;
	}
	// Start background blinking
	pStateCtrl->StartBkBlink(TRUE, CColorStaticST::ST_FLS_FAST);	
	// Start text blinking
	pStateCtrl->StartTextBlink(TRUE, CColorStaticST::ST_FLS_FAST);
	// We want be notified when the control blinks
	pStateCtrl->EnableNotify(pParent, WM_USER + 10);
	return newTaskState;
}



#pragma warning(disable : 4996)
#include <direct.h>
/*
*	Module Name		:	G_fnCheckDirAndCreate
*	Parameter		:	Dir Check Path.
*	Return			:	TRUE/FALSE
*	Function		:	Drive상에 경로 존재 여부와 경로생성을 하게 된다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
static bool G_fnCheckDirAndCreate(char * pCheckPath)
{
	char CreateDir[512] = {0,};
	memcpy(CreateDir, pCheckPath, strlen(pCheckPath));
	//뒤에서부터 '\'를 체크한다.(대표이름 자르기)
	for(size_t DirPos= strlen(pCheckPath);  DirPos > 0 && CreateDir[DirPos] != '\\';DirPos--)
	{
		CreateDir[(int)DirPos] = 0;
	}

	DWORD Error = 0;
	::SetLastError(0);

	//앞에서 부터 '\'를체크하여 Directiory를 만든다.
	for(size_t i = 0; CreateDir[(int)i] != 0 ; i++)
	{
		if(CreateDir[(int)i] == '\\')
		{
			char CreateDirBuff[512] = {0,};
			memcpy(CreateDirBuff, CreateDir, i);
			::CreateDirectory(CreateDirBuff, NULL);
			Error = ::GetLastError();
			if(ERROR_INVALID_NAME == Error)//Name error
				return false;
			//ERROR_ALREADY_EXISTS no error
		}
	}
	return true;
}
static bool G_ClearFolder(CString ClearFolderPath)
{
	CFileFind ff;
	CString path = ClearFolderPath;

	if(path.Right(1) != "\\")
		path += "\\";

	path += "*.*";

	BOOL res = ff.FindFile(path);

	while(res)
	{
		res = ff.FindNextFile();
		if (!ff.IsDots() && !ff.IsDirectory())
			DeleteFile(ff.GetFilePath());
		else if (ff.IsDirectory()&&!ff.IsDots())
		{
			path = ff.GetFilePath();
			G_ClearFolder(path);
			RemoveDirectory(path);
		}
	}
	return true;
}
SHFILEOPSTRUCT G_saveFO ;
static BYTE FolderCopyFromAToB(CString fromPath, CString ToPath) 
{
	int ret=0;
	fromPath.Format("%s", fromPath.TrimRight("\\"));
	ToPath.Format("%s", ToPath.TrimRight("\\"));
	//AOI_Forlder Clear
	G_ClearFolder(ToPath);
	//New Data Move.(Backup)
	char strBufFrom[256];
	char strBufTo[256];
	memset(strBufFrom, 0x00, 256);
	memset(strBufTo, 0x00, 256);
	strcpy(strBufFrom, fromPath);
	strcpy(strBufTo, ToPath);

	::ZeroMemory ( &G_saveFO, sizeof (SHFILEOPSTRUCT ) ) ;
	G_saveFO.hwnd = NULL;
	G_saveFO.wFunc = FO_COPY;
	G_saveFO.fFlags =  FOF_SILENT |  FOF_NOCONFIRMATION | FOF_MULTIDESTFILES| FOF_NOERRORUI;
	G_saveFO.fAnyOperationsAborted = FALSE;
	G_saveFO.lpszProgressTitle = _T("AOI Grab File Backup!");
	G_saveFO.pFrom = (LPCTSTR)strBufFrom;
	G_saveFO.pTo   = (LPCTSTR)strBufTo;
	ret= ::SHFileOperation(&G_saveFO);

	//AOI_Forlder Clear
	G_ClearFolder(fromPath);
	return TRUE;
}

static bool FileCopyFromAToB_InFolder(CString fromPath, CString ToPath, CString strFileOption)
{
	//if(strFileOption.GetLength()<=0)
	//	strFileOption.Format("*.*");
	CFileFind finder;
	CString strFind;
	CString strFindPath;

	CString ToFullName;
	strFindPath.Format("%s*.*", fromPath);
	BOOL bWorking = finder.FindFile(strFindPath);
	int nCount = 0;
	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		strFind = (LPCTSTR)finder.GetFilePath();

		if(bWorking == TRUE || strFind !="")
		{
			if(finder.IsDots() == FALSE && finder.IsDirectory() == FALSE && finder.IsHidden() == FALSE )
			{
				if(strFileOption.GetLength()>0)
				{
					if(strFind.Find(strFileOption) > 0)
					{//선택적 Copy
						ToFullName.Format("%s%s", ToPath, finder.GetFileName());
						if(::CopyFile(strFind, ToFullName,  FALSE) == FALSE)
							return false;

					}
				}
				else
				{//모두 Copy
					ToFullName.Format("%s%s", ToPath, finder.GetFileName());
					if(::CopyFile(strFind, ToFullName,  FALSE) == FALSE)
						return false;
				}
			}
		}
	}	
	return true;
}