#pragma once

////Master System에서 자기 Inspection PC에 전달 하기 위한 SMP 정의 Name
#define  NAME_BUF_CNT			10


//////////////////////////////////////////////////////////////////////////
//Project Task Number Define.
#define	TASK_10_Master		10
#define	TASK_11_Indexer		11

#define	TASK_13_AutoFocus		13

#define	TASK_21_Inspector	21
#define	TASK_24_Motion		24

#define	TASK_31_Mesure3D	31
#define	TASK_32_Mesure2D	32
#define	TASK_33_SerialPort	33

#define	TASK_60_Log			60

#define	DDMP_TASK_V61		61
#define	ALIGN_TASK_AOI76	76

#define	TASK_90_TaskLoader 90
//Test Helper Task
#define TASK_99_Sample 99


enum TASK_State
{
	TASK_STATE_NONE = 0,
	TASK_STATE_INIT = 1,
	TASK_STATE_IDLE,
	TASK_STATE_RUN,
	TASK_STATE_ERROR,
	TASK_STATE_PM
};
static LPCTSTR G_strTASK_State[] = {
	"NONE",
	"INIT",
	"IDLE",
	"RUN",
	"ERROR",
	"PM",
};

//////////////////////////////////////////////////////////////////////////
#define  MSG_TASK_INI_FullPath_TN10 "\\Config\\APP_T10_Master.DAT"
#define  MSG_TASK_INI_FullPath_TN11 "\\Config\\APP_T11_Indexer.DAT"
#define  MSG_TASK_INI_FullPath_TN13 "\\Config\\APP_T13_AutoFocus.DAT"

#define  MSG_TASK_INI_FullPath_TN21 "\\Config\\APP_T21_Inspector.DAT"
#define  MSG_TASK_INI_FullPath_TN24 "\\Config\\APP_T24_Motion.DAT"

#define  MSG_TASK_INI_FullPath_TN31 "\\Config\\APP_T31_Mesure3D.DAT"
#define  MSG_TASK_INI_FullPath_TN32 "\\Config\\APP_T32_Mesure2D.DAT"
#define  MSG_TASK_INI_FullPath_TN33 "\\Config\\APP_T33_SerialPort.DAT"

#define  MSG_TASK_INI_FullPath_TN60 "\\Config\\APP_T60_Log.DAT"

#define  MSG_TASK_INI_FullPath_TN75 "\\Config\\APP_T75_InspectSimul.DAT"

#define  MSG_TASK_INI_FullPath_TN90 "\\Config\\APP_T90_TLoader.DAT"

#define  MSG_TASK_INI_FullPath_TN99 "\\Config\\APP_T99_Sample.DAT"
//////////////////////////////////////////////////////////////////////////
//Motion Task Data Struct
#pragma pack(push, 1) 
//운용상의 Speed만 정의 한다 Home Speed는 Umac PLC에서 정한다.
enum AXIS_SPEED_MODE
{
	Speed_Normal = 0,
	Speed_Pickup = 1,
	Speed_Teach,//Motion Task에서 관리한다.(기타에서 오는것은 무시 한다.)
	Speed_ModCnt
};
typedef struct VS24MotData_Tag
{
	//Motion
	enum MotAct
	{
		Act_None =0,
		Act_Home = 1,
		Act_Move,
		Act_Stop,
	}nAction;

	//Motion/Sol
	enum MotRet
	{
		Ret_None =0,
		Ret_Ok = 1,
		Ret_Error,
		Ret_TimeOut,
		Ret_NowRun,//명령이 이미 실행중이다.

	}nResult;

	//int nAction;
	int newPosition1;
	int newPosition2;
	int newPosition3;
	int newPosition4;

	int SelectPoint;//Gripper, AxisLink의 축을 선택 하기 위해서.(Position은 4개 까지.)
	int ActionTime;//유지 시간
	int TimeOut;
	int SpeedMode;

	VS24MotData_Tag()
	{
		memset(this, 0x00, sizeof(VS24MotData_Tag));
	}
	void Reset()
	{
		memset(this, 0x00, sizeof(VS24MotData_Tag));
	}

}VS24MotData;
#pragma pack(pop) 


//////////////////////////////////////////////////////////////////////////
//Command
#define nBiz_Func_System				1
#define nBiz_Seq_Alive_Question		1
#define nBiz_Seq_Alive_Response	2
#define nBiz_Seq_Mot_All_Stop    100

//////////////////////////////////////////////////////////////////////////
//VS24Motion Command

#define nBiz_Unit_Zero					0
//Motion
#define nBiz_Func_MotionAct			2
	//TT01
#define nBiz_Seq_TT01_TableDrive				1
#define nBiz_Seq_TT01_ThetaAlign				2
#define nBiz_Seq_TT01_TableTurn				3
	//BT01
#define nBiz_Seq_BT01_CassetteUpDown		4	
	//LT01
#define nBiz_Seq_LT01_BoxLoadPush			5
	//BO01
#define nBiz_Seq_BO01_BoxRotate				6					
#define nBiz_Seq_BO01_BoxUpDn				7							
#define nBiz_Seq_BO01_BoxDrive				8				

	//UT01
#define nBiz_Seq_UT01_BoxUnLoadPush		9
		
	//TM01
#define nBiz_Seq_TM01_ForwardBackward	10

	//TM02
#define nBiz_Seq_TM02_PickerDrive				11				
#define nBiz_Seq_TM02_PickerUpDown			12			
#define nBiz_Seq_TM02_PickerShift				13				
#define nBiz_Seq_TM02_PickerRotate			14			

//AM01
#define nBiz_Seq_AM01_ScanDrive				15				
#define nBiz_Seq_AM01_ScanShift				16		
#define nBiz_Seq_AM01_ScanUpDn				17				
#define nBiz_Seq_AM01_SpaceSnUpDn			18		

#define nBiz_Seq_AM01_ScopeDrive			19				
#define nBiz_Seq_AM01_ScopeShift				20			
#define nBiz_Seq_AM01_ScopeUpDn			21				
#define nBiz_Seq_AM01_AirBlowLeftRight		22		

#define nBiz_Seq_AM01_TensionUpDn			23	
#define nBiz_Seq_AM01_LTension1				24	
#define nBiz_Seq_AM01_LTension2				25	
#define nBiz_Seq_AM01_LTension3				26		
#define nBiz_Seq_AM01_LTension4				27		
#define nBiz_Seq_AM01_RTension1				28		
#define nBiz_Seq_AM01_RTension2				29		
#define nBiz_Seq_AM01_RTension3				30		
#define nBiz_Seq_AM01_RTension4				31		

#define nBiz_Seq_AM01_MultiLTension			32		
#define nBiz_Seq_AM01_MultiRTension			33		
#define nBiz_Seq_AM01_TensionStart			34

#define nBiz_Seq_AM01_MultiMoveScan			35		
#define nBiz_Seq_AM01_MultiMoveScofe			36		
#define nBiz_Seq_AM01_SyncMoveScanScofe	37	

#define nBiz_Seq_AM01_SpaceMeasurStart		38

#define nBiz_Seq_AM01_ReviewLensOffset		39
#define nBiz_Seq_AM01_ScopeLensOffset			40

#define nBiz_Seq_AM01_SetTrigger_Inspect		41
#define nBiz_Seq_AM01_SetTrigger_AreaScan	42

#define nBiz_Seq_AM01_JigInout						43

#define nBiz_Seq_AM01_SyncMoveScopeSpaceZ		44


//Sol Cylinder 동작.
#define nBiz_Func_CylinderAct			3
#define nBiz_Seq_TM01_FORK_UP							1
#define nBiz_Seq_TM01_FORK_DN							2

#define nBiz_Seq_LT01_GUIDE_FWD						3	
#define nBiz_Seq_LT01_GUIDE_BWD						4

#define nBiz_Seq_UT01_GUIDE_FWD						5
#define nBiz_Seq_UT01_GUIDE_BWD						6

#define nBiz_Seq_BO01_PICKER_FWD						7
#define nBiz_Seq_BO01_PICKER_BWD						8

#define nBiz_Seq_TM02_LR_PAPER_PICKER_UP			9
#define nBiz_Seq_TM02_LR_PAPER_PICKER_DN			10
#define nBiz_Seq_TM02_CNT_PAPER_PICKER_UP		11
#define nBiz_Seq_TM02_CNT_PAPER_PICKER_DN		12

#define nBiz_Seq_AM01_OPEN_TEN_GRIPPER				13
#define nBiz_Seq_AM01_CLOSE_TEN_GRIPPER			14

#define nBiz_Seq_AM01_TENTION_FWD					15
#define nBiz_Seq_AM01_TENTION_BWD					16

#define nBiz_Seq_UT02_TABLE_UP							17
#define nBiz_Seq_UT02_TABLE_DN							18

#define nBiz_Seq_Door_Release_Front						19
#define nBiz_Seq_Door_Release_Side						20
#define nBiz_Seq_Door_Release_Back						21
#define nBiz_Seq_Door_Lock_Front							22
#define nBiz_Seq_Door_Lock_Side							23
#define nBiz_Seq_Door_Lock_Back							24

#define nBiz_Seq_SetSignalTower							25

#define nBiz_Seq_AM01_RingLight_UP						26
#define nBiz_Seq_AM01_RingLight_DN						27
#define nBiz_Seq_AM01_Review_UP							28
#define nBiz_Seq_AM01_Review_DN							29

#define nBiz_Seq_AM01_Doontuk_ON						30
#define nBiz_Seq_AM01_Doontuk_OFF						31


//Acc 동작.
#define nBiz_Func_ACC			4
#define nBiz_Seq_Light_Scan									1 //SelectPoint 0x01:Ring, 0x02:Back, 0x04:Reflect, 0x08:Aligh
#define nBiz_Seq_Light_Align									2 //SelectPoint 0x01:Aligh1_1, 0x02:Aligh1_2, 0x04:Aligh2_1, 0x08:Aligh2_2
#define nBiz_Seq_LoadCell_L_ZeroSet						3 //SelectPoint Left 0x01:1번, 0x02:2번, 0x04:3번, 0x08:4번
#define nBiz_Seq_LoadCell_R_ZeroSet						4 //SelectPoint Left 0x01:1번, 0x02:2번, 0x04:3번, 0x08:4번
#define nBiz_Seq_LoadCell_L_Start							5 //SelectPoint Left 0x01:1번, 0x02:2번, 0x04:3번, 0x08:4번//Motion Task PLC에서 구동
#define nBiz_Seq_LoadCell_L_Stop							6 //SelectPoint Left 0x01:1번, 0x02:2번, 0x04:3번, 0x08:4번//Motion Task PLC에서 구동
#define nBiz_Seq_LoadCell_R_Start							7 //SelectPoint Right 0x01:1번, 0x02:2번, 0x04:3번, 0x08:4번(아직 사용 계획없다.)
#define nBiz_Seq_LoadCell_R_Stop							8 //SelectPoint Right 0x01:1번, 0x02:2번, 0x04:3번, 0x08:4번(아직 사용 계획없다.-디바이스도 없다.)
#define nBiz_Seq_Read_QR									9//VS24MotData구조체 뒤로 Data가 추가된다.(전체 (nMessSize-VS24MotData)가 QRCode Size 이다)
#define nBiz_Seq_SpaceSensorStart						10//Motion Task PLC에서 구동 로직을 수행 한다. 값을 가져 오지는 않는다.
#define nBiz_Seq_SpaceSensorStop							11//Motion Task PLC에서 구동 로직을 수행 한다. 값을 가져 오지는 않는다.
#define nBiz_Seq_Read_AirN2									12

#define nBiz_Seq_Buzzer2_On		13
#define nBiz_Seq_Buzzer2_Off		14

//Vacuum 동작.
#define nBiz_Func_Vac			5
//TM01
#define nBiz_Seq_TM01_Vac_ON								1
#define nBiz_Seq_TM01_Vac_OFF							2
#define nBiz_Seq_TM01_Purge								3
//LT01
#define nBiz_Seq_LT01_Vac_ON								11
#define nBiz_Seq_LT01_Vac_OFF							12

//m_stLT01_Mess.nAction = m_stLT01_Mess.Act_None;
//m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;	
//m_stLT01_Mess.newPosition1 = nPurgeTime;  //500ms
//m_stLT01_Mess.TimeOut_IO = NORMAL_TIMEOUT;
#define nBiz_Seq_LT01_Purge									13
//BO01
#define nBiz_Seq_BO01_Vac_ON								21
#define nBiz_Seq_BO01_Vac_OFF							22
#define nBiz_Seq_BO01_Purge								23
//UT01
#define nBiz_Seq_UT01_Vac_ON								31
#define nBiz_Seq_UT01_Vac_OFF							32
#define nBiz_Seq_UT01_Purge								33
//TM02
#define nBiz_Seq_TM02_ST_Vac_ON							41
#define nBiz_Seq_TM02_ST_Vac_OFF							42
#define nBiz_Seq_TM02_ST_Purge								43
#define nBiz_Seq_TM02_PP_Vac_ON							44
#define nBiz_Seq_TM02_PP_Vac_OFF							45
#define nBiz_Seq_TM02_PP_Purge								46
//TT01
#define nBiz_Seq_TT01_UP_Vac_ON							51
#define nBiz_Seq_TT01_UP_Vac_OFF							52
#define nBiz_Seq_TT01_UP_Purge								53
#define nBiz_Seq_TT01_DN_Vac_ON							54
#define nBiz_Seq_TT01_DN_Vac_OFF							55
#define nBiz_Seq_TT01_DN_Purge								56

//UT02
#define nBiz_Seq_UT02_Vac_ON								61
#define nBiz_Seq_UT02_Vac_OFF							62
#define nBiz_Seq_UT02_Purge								63


//Status 동작.
#define nBiz_Func_Status			10

#define nBiz_Seq_BT01								1
#define nBiz_Seq_TM01								2
#define nBiz_Seq_LT01								3
#define nBiz_Seq_BO01								4
#define nBiz_Seq_UT01								5
#define nBiz_Seq_TM02								6
#define nBiz_Seq_TT01								7
#define nBiz_Seq_UT02								8
#define nBiz_Seq_AM01								9
#define nBiz_Seq_ALL								10

//Indexer AM01 간 통신.
//Indexer to AM01
//-->Loading Sequence
#define nBiz_Func_IndexerToMaster		11
#define nBiz_Seq_Recipe_Change			1  //공정 전체 Recipe
#define nBiz_Seq_Stick_Information			2  //Stick Size, Load Offset
#define nBiz_Seq_Stick_Load_Ready		3 //턴테이블 이동 준비 완료.
#define nBiz_Seq_Theta_Align_Start		4
#define nBiz_Seq_Stick_Align_Value_Ack	5
#define nBiz_Seq_TT01_VAC_OFF			6
#define nBiz_Seq_Load_Vac_Off_Ack		7
#define nBiz_Seq_Load_Vac_On_Ack		8

//-->UnLoading Sequence
#define nBiz_Seq_Stick_Result_ACK			  11
#define nBiz_Seq_Stick_Unload_Ready_OK  12
#define nBiz_Seq_TT01_VAC_ON				  13

//-->Position Sequence
#define nBiz_Seq_TT01_Pos_AM01			20
#define nBiz_Seq_TT01_Pos_Turn			21
#define nBiz_Seq_TT01_Pos_UT02			22



//AM01 to Indexer
//-->Loading Sequence
#define nBiz_Func_MasterToIndexer		12
#define nBiz_Seq_Recipe_Change_Ack				1  //공정 전체 Recipe
#define nBiz_Seq_Stick_Information_Ack			2  //Stick Size, Load Offset
#define nBiz_Seq_Stick_Load_Ready_OK			3  //
#define nBiz_Seq_Stick_Align_Value					4
#define nBiz_Seq_Stick_Align_End					5
#define nBiz_Seq_Stick_Load_Standby				6  //Gripper Close, TT01 Vac Off
#define nBiz_Seq_Stick_Load_Comp				7  //모터 후진
#define nBiz_Seq_Load_Vac_Off					8
#define nBiz_Seq_Load_Vac_On					9
//-->UnLoading Sequence
#define nBiz_Seq_Stick_Result					  11
#define nBiz_Seq_Stick_Unload_Ready			  12
#define nBiz_Seq_Stick_Unload_Standby		  13  //모터 전진, TT01 Vac On
#define nBiz_Seq_Stick_Unload_Comp			  14  //Gripper Open

#define nBiz_Seq_TT01_Pos_Req					15

//AM01 to Indexer
//-->Status Sequence
#define nBiz_Func_AM01_Process		13
#define nBiz_Seq_Reset									1
#define nBiz_Seq_Sick_Loading_Start				2
#define nBiz_Seq_Sick_Loading_End					3
#define nBiz_Seq_3D_Scan_Start					4
#define nBiz_Seq_3D_Scan_End						5
#define nBiz_Seq_Defect_Inspect_Start			6
#define nBiz_Seq_Defect_Inspect_End				7
#define nBiz_Seq_CD_TP_Start						8
#define nBiz_Seq_CD_TP_End							9
#define nBiz_Seq_3D_Scope_Start					10
#define nBiz_Seq_3D_Scope_End					11

//////////////////////////////////////////////////////////////////////////
//Inspector 
//////////////////////////////////////////////////////////////////////////
//Command
#define nBiz_AOI_System		110
//Master to Inspector
#define nBiz_Send_UnitInitialize					1
#define nBiz_Send_AlarmReset					2

#define nBiz_Send_SetSysTime					3
#define nBiz_Send_CamCommand				4

#define nBiz_Send_DebugScreen				60
#define nBiz_Send_DebugSysReset			61
//Inspector To Master
#define nBiz_Recv_CamCommand			10
#define nBiz_Recv_Status						11
#define nBiz_Recv_FileBackUp_Start		12
#define nBiz_Recv_FileBackUp_End			13

#define nBiz_Recv_Alarm						40

#define nBiz_AOI_Parameter		111
//Master to Inspector
#define nBiz_Send_ActiveAreaParam			1
#define nBiz_Send_PadAreaParam				2
#define nBiz_Send_GlassParamSet				3

#define nBiz_Send_RealCellPos					4
//Inspector To Master
#define nBiz_Recv_CalculateInfo					11
#define nBiz_Recv_BlockDrawInfo				12
#define nBiz_Recv_CellDrawInfo					13
#define nBiz_Recv_PADDrawInfo					14
#define nBiz_Recv_HWParam						15
#define nBiz_Recv_ScanImgDrawInfo			16



#define nBiz_AOI_Inspection		112
//Master to Inspector
#define nBiz_Send_ScanStart					1
#define nBiz_Send_ScanStop					2

#define nBiz_Send_IllumValueReset			5
#define nBiz_Send_IllumRunRealTime		6
#define nBiz_Send_IllValueStartEndIndex	7


//Inspector To Master
#define nBiz_Recv_ScanReady				11
#define nBiz_Recv_ScanStop					12

#define nBiz_Recv_InspectRetInfo			13
#define nBiz_Recv_InspectPADRetInfo		14
#define nBiz_Recv_ScanActEnd				15
#define nBiz_Recv_ScanPADEnd				16
#define nBiz_Recv_BlockEnd					17

#define nBiz_Send_BigInspect				200
#define nBiz_Send_BigSaveImg				201
#define nBiz_Recv_BigInspectResult			202

#define nBiz_Recv_IlluminationValue			30
#define nBiz_Recv_IllValueRealTime			31

#define nBiz_AOI_FileAccess		113
//Inspector To Master
#define nBiz_Recv_DefectWriteStart	11
#define nBiz_Recv_DefectWriteEnd		12

#define nBiz_Recv_BackupStart			13
#define nBiz_Recv_BackupEnd			14

#define nBiz_AOI_AlignCam		114
//Align Task To Master
#define nBiz_Recv_MoveStageXY			11
//Align Task to Master
#define nBiz_Recv_NowStageXY				13
//Master to Align Task
#define nBiz_Send_StagePosXY				14

//AlignTask To Inspector
#define nBiz_Send_AlignGrabStart			15
#define nBiz_Send_AlignGrabAbort			16
#define nBiz_Send_AlignImgSave				17
#define nBiz_Recv_AlignImgEnd				18

//////////////////////////////////////////////////////////////////////////
//Command
#define nBiz_DMP_System		115
//Master to DDMP
#define nBiz_Send_LotStart					1
#define nBiz_Send_LoadTDData				2

//DDMP To Master
#define nBiz_Recv_Mask_Error				10
#define nBiz_Recv_Common_Error			11

#define nBiz_Recv_TDLoad_Error				12
#define nBiz_Recv_Alive						40



//////////////////////////////////////////////////////////////////////////
//3D검사Unit 

//Command
#define nBiz_3DMeasure_System			210
//Master to 3DMeasur
#define nBiz_3DSend_UnitInitialize				1
#define nBiz_3DSend_AlarmReset				2

#define nBiz_3DSend_AutoFocus					3
#define nBiz_3DSend_StartMeasure				4
#define nBiz_3DSend_StopMeasure				5

#define nBiz_3DSend_ManualModeON			6
#define nBiz_3DSend_ManualModeOFF			7

#define nBiz_3DSend_SelectLens					8
#define nBiz_3DSend_AlignGrabImg				9
//3DMeasur To Master
#define nBiz_Recv_AutoFocusEnd				20
#define nBiz_Recv_StartMeasureEnd			21
#define nBiz_Recv_AlignGrabEnd					22
//3DMeasur To Master
#define nBiz_3DMeasure_Status					211
#define nBiz_3DRecv_Status						1
#define nBiz_3DRecv_ActionStatus				2

//Micro Epsilon
#define nBiz_3DScan_System		220  //Function
//Master To Task
#define nBiz_3DScan_UnitInitialize		1
#define nBiz_3DScan_AlarmReset		2
#define nBiz_3DScan_ProcessStart		3
#define nBiz_3DScan_ScanStart			4
#define nBiz_3DScan_ScanEnd			5
#define nBiz_3DScan_ProcessEnd		6
//Task To Master
#define nBiz_3DScan_FileBackupStart		7
#define nBiz_3DScan_FileBackupEnd		8




//Auto Focus
#define nBiz_Func_ReviewLens		300
#define nBiz_Seq_Lens1				1
#define nBiz_Seq_Lens2				2
#define nBiz_Seq_Lens3				3
#define nBiz_Seq_Lens4				4

#define nBiz_Func_AF					301
#define nBiz_Seq_Init					1
#define nBiz_Seq_AF						2
#define nBiz_Seq_AFNowPos				3//AF가 성공 하면 전달

#define nBiz_Func_Light				302
#define nBiz_Seq_SetReviewLight	1



// MCC Command
#define nBiz_MCC_Func						61
#define nBiz_MCC_SeqSendCommand			 4
// Master에서 Mcc Task 로 보냄	
#define nBiz_MCC_Unit_Param			 1

// Mcc Task에서 Master로 보냄
#define nBiz_MCC_File_Write_End		 2