#include "stdafx.h"
#include "MCC_Command.h"
//////////////////////////////////////////////////////////////////////////

void CMCC_Command::m_fnReadMCC_Param(int *pRefreshTime, int* pFileMakeTime, int* pWriteOnOff)
{
	CStdioFile cstFile;

	if (!cstFile.Open(VS61LOGMCC_INIT_FILE_PATH, CFile::modeRead|CFile::typeText, NULL))
	{
		//G_AddLog(3, "Data File Open Error %s", GLASS_ALARM_DAT_PATH);
		return;
	}

	CStringArray strArrayRead;
	CString strRead;
	CString strWriteData;
	CString strValue;

	while(cstFile.ReadString(strRead))
	{
		if(strRead.Find(";") >= 0)
		{
			continue;
		}

		if(strRead.Find("RefreshTime") >= 0)
		{
			strValue = strRead.Mid(strRead.Find("=")+1);
			strValue = strValue.Trim();	
			*pRefreshTime = atoi(strValue);			
		}
		else if(strRead.Find("Record Time") >= 0)
		{
			strValue = strRead.Mid(strRead.Find(":")+1);
			strValue = strValue.Trim();			
			*pFileMakeTime = atoi(strValue);
		}
		else if(strRead.Find("Write OnOff") >= 0)
		{
			strValue = strRead.Mid(strRead.Find("=")+1);
			strValue = strValue.Trim();		
			*pWriteOnOff = atoi(strValue);
		}
	}

	cstFile.Close();
}
void CMCC_Command::m_fnSendMCC_Parm(int RefreshTime, int FileMakeTime, int WriteOnOff)
{
	CString strNewData;
	strNewData.Format("%d,%d,%d",FileMakeTime, RefreshTime, WriteOnOff);
	m_fnSendCommand_Nrs(TASK_61_MCCLog, FUN_ID_LOG_61, SEQ_ID_MCC_COMMAND, MCC_Unit_Param, 
		strNewData.GetLength(), (UCHAR*)strNewData.GetBuffer(strNewData.GetLength()));
}
// 	/// MCC Start
// 	// MCC 공통 Data File 을 Ini 에 기록한다.
BOOL CMCC_Command::m_fnMCC_WriteInfo(int nStringTable, CString strValue)
{
	CString strItem;
	strItem = STR_MCC_COMMON_TABLE[nStringTable];
		
	return WritePrivateProfileString(MCC_SECTION, strItem, strValue, MCC_COMMON_INI_SAVE_PATH);		
}

	// MCC 공통 Data File 모두 기록했음을 알린다.
BOOL CMCC_Command::m_fnMCC_WriteInfoEnd()
{
	BOOL nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog,  61, 1, 1); // SEQ_ID_MCC_WRITE_END 1

	return nResultCode;//OK;
}
//
/*
BOOL CMCC_Command::m_fnMCC_ActionLog(BOOL bStartEnd, CString strModuleID, CString strStickID, CString strActionName, CString strFromPos, CString strToPos)
{
	BOOL nResultCode = FALSE;

	if(
		strModuleID.GetLength() > MAX_MCC_LOG_BUFF
		|| strStickID.GetLength() > MAX_MCC_LOG_BUFF
		|| strActionName.GetLength() > MAX_MCC_LOG_BUFF 
		|| strFromPos.GetLength() > MAX_MCC_LOG_BUFF
		|| strToPos.GetLength() > MAX_MCC_LOG_BUFF)
		return FALSE;

	STRU_MCC_EVENT_INPUT struMCCInput;
		
	struMCCInput.nLogType = 0;
//	struMCCInput.tEventTime = tTime;	
		
	memcpy(struMCCInput.chModuleID, strModuleID, strModuleID.GetLength());

	memcpy(struMCCInput.chStickID, strStickID, strStickID.GetLength());

	// Action Name
	memcpy(struMCCInput.chEvent_1, strActionName, strActionName.GetLength());

	// From Position
	memcpy(struMCCInput.chEvent_2, strFromPos, strFromPos.GetLength());

	// To Position
	memcpy(struMCCInput.chEvent_3, strToPos, strToPos.GetLength());

	// START or END
	if(bStartEnd == TRUE)
		sprintf_s(struMCCInput.chEvent_4, "%s", "START");
	else
		sprintf_s(struMCCInput.chEvent_4, "%s", "END");		

	WRITE_LOG_MSG SendLogData;

	memcpy(SendLogData.LogData, &struMCCInput, sizeof(STRU_MCC_EVENT_INPUT));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 2, 1, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}
*/
BOOL CMCC_Command::m_fnMCC_ActionLog(int nIndex, BOOL bStartEnd, CString strStickID)
{
	BOOL nResultCode = FALSE;

	if(strStickID.GetLength() > MAX_MCC_LOG_BUFF)
		return FALSE;

	STRU_MCC_EVENT_INPUT struMCCInput;

	struMCCInput.nLogType = 0;	

	sprintf_s(struMCCInput.chModuleID, "%s", G_ActionLog_Format[nIndex].pChModuleID);
// 
 	memcpy(struMCCInput.chStickID, strStickID, strStickID.GetLength());
 
// 	// Action Name
 	sprintf_s(struMCCInput.chEvent_1, "%s", G_ActionLog_Format[nIndex].pChActionName);
// 
// 	// From Position
 	sprintf_s(struMCCInput.chEvent_2, "%s", G_ActionLog_Format[nIndex].pchFrom);
// 
// 	// To Position
 	sprintf_s(struMCCInput.chEvent_3, "%s", G_ActionLog_Format[nIndex].pchTo);

	// START or END
	if(bStartEnd == TRUE)
		sprintf_s(struMCCInput.chEvent_4, "%s", "START");
	else
		sprintf_s(struMCCInput.chEvent_4, "%s", "END");		

	WRITE_LOG_MSG SendLogData;

	memcpy(SendLogData.LogData, &struMCCInput, sizeof(STRU_MCC_EVENT_INPUT));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 2, 1, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}

BOOL CMCC_Command::m_fnMCC_SignalLog(int nIndex, BOOL bStartEnd, CString strStickID)	
{
	BOOL nResultCode = FALSE;

	if(	strStickID.GetLength() > MAX_MCC_LOG_BUFF)
		return FALSE;	


	STRU_MCC_EVENT_INPUT struMCCInput;

	struMCCInput.nLogType = 1;

	sprintf_s(struMCCInput.chModuleID, "%s", G_SignalLog_Format[nIndex].pChModuleID);
	
	memcpy(struMCCInput.chStickID, strStickID, strStickID.GetLength());

	CString strSignal;
	// Signal Name
	strSignal = G_SignalLog_Format->pChSignalName;

	int nType = 0;
	nType = G_SignalLog_Format[nIndex].nType;

	if(nType == SIGNAL_TYPE_OUTPUT)  
	{
		strSignal = "Y_" + strSignal;
	}
	else if(nType == SIGNAL_TYPE_INPUT)
	{
		strSignal = "X_" + strSignal;
	}

	sprintf_s(struMCCInput.chEvent_1, "%s", strSignal);

	// From Position
	sprintf_s(struMCCInput.chEvent_2, "%s", G_SignalLog_Format[nIndex].pchFrom);

	// To Position
	sprintf_s(struMCCInput.chEvent_3, "%s", G_SignalLog_Format[nIndex].pchTo);

	// START or END
	if(bStartEnd == TRUE)
		sprintf_s(struMCCInput.chEvent_4, "%s", "START");
	else
		sprintf_s(struMCCInput.chEvent_4, "%s", "END");		

	WRITE_LOG_MSG SendLogData;

	memcpy(SendLogData.LogData, &struMCCInput, sizeof(STRU_MCC_EVENT_INPUT));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 2, 1, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}

BOOL CMCC_Command::m_fnMCC_EventLog_CompInOut(BOOL bInOut, CString strModuleID, CString strStickID, CString strFromPos, CString strToPos)	
{
	BOOL nResultCode = FALSE;

	if(
		strModuleID.GetLength() > MAX_MCC_LOG_BUFF
		|| strStickID.GetLength() > MAX_MCC_LOG_BUFF						
		|| strFromPos.GetLength() > MAX_MCC_LOG_BUFF
		|| strToPos.GetLength() > MAX_MCC_LOG_BUFF)
		return FALSE;

	STRU_MCC_EVENT_INPUT struMCCInput;

	struMCCInput.nLogType = 3;
//	struMCCInput.tEventTime = tTime;

	memcpy(struMCCInput.chModuleID, strModuleID, strModuleID.GetLength());

	memcpy(struMCCInput.chStickID, strStickID, strStickID.GetLength());		

	// From Position
	memcpy(struMCCInput.chEvent_3, strFromPos, strFromPos.GetLength());

	// To Position
	memcpy(struMCCInput.chEvent_4, strToPos, strToPos.GetLength());

	// START or END
	if(bInOut == TRUE)
	{			
		CString strEventID = "CEID_16";
		memcpy(struMCCInput.chEvent_1, strEventID, strEventID.GetLength());

		sprintf_s(struMCCInput.chEvent_2, "%s", "COMPONENT_IN");
	}
	else
	{
		CString strEventID = "CEID_17";
		memcpy(struMCCInput.chEvent_1, strEventID, strEventID.GetLength());

		sprintf_s(struMCCInput.chEvent_2, "%s", "COMPONENT_OUT");		
	}

	WRITE_LOG_MSG SendLogData;

	memcpy(SendLogData.LogData, &struMCCInput, sizeof(STRU_MCC_EVENT_INPUT));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 2, 1, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}

BOOL CMCC_Command::m_fnMCC_EventLog_ChangeProcessState(CString strModuleID, CString strStickID, CString strOldState, CString strNewState)	
{
	BOOL nResultCode = FALSE;

	if(
		strModuleID.GetLength() > MAX_MCC_LOG_BUFF
		|| strStickID.GetLength() > MAX_MCC_LOG_BUFF						
		|| strOldState.GetLength() > MAX_MCC_LOG_BUFF
		|| strNewState.GetLength() > MAX_MCC_LOG_BUFF)
		return FALSE;

	STRU_MCC_EVENT_INPUT struMCCInput;

	struMCCInput.nLogType = 3;
//	struMCCInput.tEventTime = tTime;

	memcpy(struMCCInput.chModuleID, strModuleID, strModuleID.GetLength());

	memcpy(struMCCInput.chStickID, strStickID, strStickID.GetLength());

	// Signal Name
	CString strEventID = "CEID_51";
	memcpy(struMCCInput.chEvent_1, strEventID, strEventID.GetLength());

	// From Position
	memcpy(struMCCInput.chEvent_2, strOldState, strOldState.GetLength());

	// To Position
	memcpy(struMCCInput.chEvent_3, strNewState, strNewState.GetLength());


	WRITE_LOG_MSG SendLogData;

	memcpy(SendLogData.LogData, &struMCCInput, sizeof(STRU_MCC_EVENT_INPUT));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 2, 1, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}

BOOL CMCC_Command::m_fnMCC_EventLog_ChangeState(CString strModuleID, CString strStickID, CString strOldState, CString strNewState, CString strAlarmID, CString strAlarmText)	
{
	BOOL nResultCode = FALSE;

	if(
		strModuleID.GetLength() > MAX_MCC_LOG_BUFF
		|| strStickID.GetLength() > MAX_MCC_LOG_BUFF						
		|| strOldState.GetLength() > MAX_MCC_LOG_BUFF
		|| strNewState.GetLength() > MAX_MCC_LOG_BUFF
		|| strAlarmID.GetLength() > MAX_MCC_LOG_BUFF
		|| strAlarmText.GetLength() > MAX_MCC_LOG_BUFF)
			return FALSE;

	STRU_MCC_EVENT_INPUT struMCCInput;

	struMCCInput.nLogType = 3;
//	struMCCInput.tEventTime = tTime;

	memcpy(struMCCInput.chModuleID, strModuleID, strModuleID.GetLength());

	memcpy(struMCCInput.chStickID, strStickID, strStickID.GetLength());

	// Signal Name
	CString strEventID = "CEID_53";
	memcpy(struMCCInput.chEvent_1, strEventID, strEventID.GetLength());

	// From Position
	memcpy(struMCCInput.chEvent_2, strOldState, strOldState.GetLength());

	// To Position
	memcpy(struMCCInput.chEvent_3, strNewState, strNewState.GetLength());

	if(strAlarmID.GetLength() != 0)
		memcpy(struMCCInput.chEvent_4, strAlarmID, strAlarmID.GetLength());
	if(strAlarmText.GetLength() != 0)
		memcpy(struMCCInput.chEvent_5, strAlarmText, strAlarmText.GetLength());
		
	WRITE_LOG_MSG SendLogData;

	memcpy(SendLogData.LogData, &struMCCInput, sizeof(STRU_MCC_EVENT_INPUT));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 2, 1, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}

BOOL CMCC_Command::m_fnMCC_EventLog_ChangePPID(CString strModuleID, CString strStickID, CString strOldPPID, CString strNewPPID)	
{
	BOOL nResultCode = FALSE;

	if(
		strModuleID.GetLength() > MAX_MCC_LOG_BUFF
		|| strStickID.GetLength() > MAX_MCC_LOG_BUFF						
		|| strOldPPID.GetLength() > MAX_MCC_LOG_BUFF
		|| strNewPPID.GetLength() > MAX_MCC_LOG_BUFF)
		return FALSE;

	STRU_MCC_EVENT_INPUT struMCCInput;

	struMCCInput.nLogType = 3;
	//struMCCInput.tEventTime = tTime;

	memcpy(struMCCInput.chModuleID, strModuleID, strModuleID.GetLength());

	memcpy(struMCCInput.chStickID, strStickID, strStickID.GetLength());

	// Signal Name
	CString strEventID = "CEID_131";
	memcpy(struMCCInput.chEvent_1, strEventID, strEventID.GetLength());

	// From Position
	memcpy(struMCCInput.chEvent_2, strOldPPID, strOldPPID.GetLength());

	// To Position
	memcpy(struMCCInput.chEvent_3, strNewPPID, strNewPPID.GetLength());	

	WRITE_LOG_MSG SendLogData;

	memcpy(SendLogData.LogData, &struMCCInput, sizeof(STRU_MCC_EVENT_INPUT));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 2, 1, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}

BOOL CMCC_Command::m_fnMCC_EventLog_Error_WarningLog(BOOL bTrueisErrorFalseisWarning, BOOL bSetReset, CString strModuleID, CString strStickID, CString strAlarmID, CString strAlarmText)	
{
	BOOL nResultCode = FALSE;
	if(
		strModuleID.GetLength() > MAX_MCC_LOG_BUFF
		|| strStickID.GetLength() > MAX_MCC_LOG_BUFF						
		|| strAlarmID.GetLength() > MAX_MCC_LOG_BUFF
		|| strAlarmText.GetLength() > MAX_MCC_LOG_BUFF)
		return FALSE;

	STRU_MCC_EVENT_INPUT struMCCInput;

	if(bTrueisErrorFalseisWarning == TRUE)     // Error
	{
		struMCCInput.nLogType = 5;
	}
	else
	{
		struMCCInput.nLogType = 4;
	}
		
	//struMCCInput.tEventTime = tTime;

	memcpy(struMCCInput.chModuleID, strModuleID, strModuleID.GetLength());

	memcpy(struMCCInput.chStickID, strStickID, strStickID.GetLength());

	if(bSetReset == TRUE) // SET		
		sprintf_s(struMCCInput.chEvent_1, "%s", "SET");
	else
		sprintf_s(struMCCInput.chEvent_1, "%s", "RESET");
		
	memcpy(struMCCInput.chEvent_2, strAlarmID, strAlarmID.GetLength());
		
	memcpy(struMCCInput.chEvent_3, strAlarmText, strAlarmText.GetLength());	

	WRITE_LOG_MSG SendLogData;

	memcpy(SendLogData.LogData, &struMCCInput, sizeof(STRU_MCC_EVENT_INPUT));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 2, 1, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}

BOOL CMCC_Command::m_fnMCC_InfoInputValue(int nIndex, float nfValue)
{
	BOOL nResultCode = FALSE;	

	CString strDataValue;
	CString strTemp;

	G_InfoValue.nfValue = nfValue;
	G_InfoValue.nIndex = nIndex;

	WRITE_LOG_MSG SendLogData;
	
	memcpy(SendLogData.LogData, &G_InfoValue, sizeof(STRU_InfoValue));		

	nResultCode = m_fnSendCommand_Nrs(TASK_61_MCCLog, 61, 3, 2, sizeof(WRITE_LOG_MSG), (UCHAR*)&SendLogData); 

	return nResultCode;//OK;
}