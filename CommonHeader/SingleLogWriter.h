#pragma once

//////////////////////////////////////////////////////////////////////////
#define  LOG_TEXT_MAX_SIZE   1024

typedef struct SearchParam_Tag
{
	COleDateTime tSearchStart;
	COleDateTime tSearchEnd;

	CString SearchKeyWord;

}SearchParam, * LPSearchParam;

extern "C" __declspec(dllimport) void	_stdcall  Ns_Init_WriteLogProc(char *pstrBasePath, int KeepPeriod_Day, int KeepSize_MB);
//extern "C" __declspec(dllimport) void	_stdcall  Ns_Init_WriteReportProc(char *pstrBasePath, int KeepPeriod_Day, int KeepSize_MB);
extern "C" __declspec(dllimport) void	_stdcall  Ns_Init_CloseWriterAll();
//Project 어디서나 Log를 기록 할 수 있도록 
extern "C" __declspec(dllimport) void	_stdcall  Ns_WriteLogProc(const char* pWritLog, ...);
//extern "C" __declspec(dllimport) void	_stdcall  Ns_WriteLogProc_Img(IplImage *pTargetImg, const char* pWritLog, ...);

extern "C" __declspec(dllimport) void	_stdcall  Ns_WriteReportProc(const char* pWritLog, ...);
//extern "C" __declspec(dllimport) void	_stdcall  Ns_WriteReportProc_Img(IplImage *pTargetImg, const char* pWritLog, ...);

//////////////////////////////////////////////////////////////////////////
typedef void (*pfSEndCallback)(CStringArray *strpFileStrList,void*pHandleObj);
extern "C" __declspec(dllimport) bool	_stdcall  Ns_Search_LogProc(SearchParam SearchData, pfSEndCallback fn_SearchEnd, void*pHandleObj);
//extern "C" __declspec(dllimport) bool	_stdcall  Ns_Search_ReportProc(SearchParam SearchData, pfSEndCallback fn_SearchEnd, void*pHandleObj);

