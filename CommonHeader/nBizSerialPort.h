#pragma once

//Speed
#undef  B9600
#define B9600 9600
#undef  B19200
#define B19200 19200
#undef  B38400
#define B38400 38400
#undef  B57600
#define B57600 57600
#undef  B115200
#define B115200 115200


// Stop bits
#define	RS_STOP1	0
#define	RS_STOP1_5	1
#define	RS_STOP2	2

// Parity bits
#define	RS_PARITY_NONE	0	// 'N'
//#define NONE 0
#define	RS_PARITY_ODD	1	// 'O'
#define	RS_PARITY_EVEN	2	// 'E'
#define	RS_PARITY_MARK	3	// 'M'
#define	RS_PARITY_SPACE	4	// 'S'

typedef struct RecvCheckTag
{
	BYTE ASCII_EndFlag;//ASCII Mode�϶� Receive End Check
	BYTE B_StartFlag;//Binary Mode�϶� Receive End Check And Size
	int		B_DataSize;
	RecvCheckTag()
	{
		ASCII_EndFlag = (BYTE)'\n';
		B_StartFlag = 0;
		B_DataSize = 0;
	}
}RecvCheck;
typedef struct SerialDataTag
{
	DWORD				mLength;
	LPVOID				mpData;
	SerialDataTag()
	{
		mLength = 0;
		mpData  = NULL;
	}
}SERIAL_DATA;

typedef void (*pReceiveFunction)(LPVOID, LPVOID, DWORD);

typedef struct PortOption_TAG
{
	DWORD nRate;
	BYTE nParity;
	BYTE nSize;
	BYTE nStop;

	PortOption_TAG()
	{
		nRate = B9600;
		nParity = RS_PARITY_NONE;
		nSize = 8;
		nStop = RS_STOP1;
	}
}PortOption;

//////////////////////////////////////////////////////////////////////////
extern "C" __declspec(dllimport) int  _stdcall nBizM_FindSerialPorts(char *pstrPortNames);
extern "C" __declspec(dllimport) int  _stdcall nBizM_InitSerialPortList();
extern "C" __declspec(dllimport) BOOL  _stdcall nBizM_SPortSetParam(char *pstrPortNames, PortOption *pPortParam);
extern "C" __declspec(dllimport) BOOL  _stdcall nBizM_SPortOpen(CString strPortNames, RecvCheck *pRCheck = NULL);
extern "C" __declspec(dllimport) BOOL  _stdcall nBizM_SPortSend(CString strPortNames, SERIAL_DATA*pSendData);
extern "C" __declspec(dllimport) void  _stdcall nBizM_SPortClose(CString strPortNames);
extern "C" __declspec(dllimport) void  _stdcall nBizM_SPortInterface(CString strPortNames, LPVOID CtrlObj, pReceiveFunction pRecvFn);
extern "C" __declspec(dllimport) BOOL  _stdcall nBizM_SHSPortSend(CString strPort, SERIAL_DATA*pSendData, SERIAL_DATA*pRecvCheckData);

extern "C" __declspec(dllimport) BOOL  _stdcall nBizM_SPortGetPortFindded(CString strPort);
