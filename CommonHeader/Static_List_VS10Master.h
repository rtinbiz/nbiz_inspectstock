#pragma once

struct AlarmInfo
{
	bool bSetAlarm;
	//int nAlarmIndex;
	int nAlarmID;
	int nAlarmCode;
	int nAlarmType;
	char *strAlarmText;
};



enum
{
	AL_1001 = 1001,
	AL_1002 = 1002,
	AL_1003 = 1003,
	AL_1004 = 1004,
	AL_1005 = 1005,
	AL_1006 = 1006,
	AL_1007 = 1007,
	AL_1008 = 1008,
	AL_1009 = 1009,
	AL_1010 = 1010,
	AL_1011 = 1011,
	AL_1012 = 1012,
	AL_1013 = 1013,
	AL_1014 = 1014,
	AL_1015 = 1015,
	AL_1016 = 1016,
	AL_1017 = 1017,
	AL_1018 = 1018,
	AL_1019 = 1019,
	AL_1020 = 1020,
	AL_1021 = 1021,
	AL_1022 = 1022,
	AL_1023 = 1023,
	AL_1024 = 1024,
	AL_1025 = 1025,
	AL_1101 = 1101,
	AL_1102 = 1102,
	AL_1103 = 1103,
	AL_1104 = 1104,
	AL_1105 = 1105,
	AL_1106 = 1106,
	AL_1107 = 1107,
	AL_1108 = 1108,
	AL_1109 = 1109,
	AL_1110 = 1110,
	AL_1111 = 1111,
	AL_1112 = 1112,
	AL_1113 = 1113,
	AL_1114 = 1114,
	AL_1115 = 1115,
	AL_1116 = 1116,
	AL_1117 = 1117,
	AL_1118 = 1118,
	AL_1119 = 1119,
	AL_1120 = 1120,
	AL_1121 = 1121,
	AL_1122 = 1122,
	AL_2001 = 2001,
	AL_2002 = 2002,
	AL_2003 = 2003,
	AL_2004 = 2004,
	AL_2005 = 2005,
	AL_2006 = 2006,
	AL_2007 = 2007,
	AL_2008 = 2008,
	AL_2009 = 2009,
	AL_2010 = 2010,
	AL_2011 = 2011,
	AL_2012 = 2012,
	AL_2101 = 2101,
	AL_2102 = 2102,
	AL_2103 = 2103,
	AL_2104 = 2104,
	AL_2105 = 2105,
	AL_2106 = 2106,
	AL_2107 = 2107,
	AL_2108 = 2108,
	AL_2109 = 2109,
	AL_2110 = 2110,
	AL_2111 = 2111,
	AL_2112 = 2112,
	AL_2113 = 2113,
	AL_2114 = 2114,
	AL_2115 = 2115,
	AL_2116 = 2116,
	AL_2117 = 2117,
	AL_2118 = 2118,
	AL_2119 = 2119,
	AL_2120 = 2120,
	AL_2121 = 2121,
	AL_2122 = 2122,
	AL_2123 = 2123,
	AL_2124 = 2124,
	AL_2125 = 2125,
	AL_2126 = 2126,
	AL_2127 = 2127,
	AL_2128 = 2128,
	AL_2129 = 2129,
	AL_2130 = 2130,
	AL_2131 = 2131,
	AL_2132 = 2132,
	AL_2133 = 2133,
	AL_2134 = 2134,
};



struct ECIDInfo
{
	int nECID;
	char strECName[128];
	int nECSLL;
	int nECWLL;
	int nECDEF;
	int nECWUL;
	int nECSUL;
};



struct SVIDInfo
{
	int nSVIDIndex;
	int nSVID;
	char *strSVName;
	char strValue[32];
};


struct PPIDInfo
{
	int nPPIDIndex;
	int nPPID;
	int nSubPPID;
	char *strParamName;

};


struct EOIDInfo
{
	int nEOID;
	char *strEOIDDescrip;
	int nEOMD;
	char *strnEOMDDescrip;
	int nEOV;
	char *strnEOVDescrip;
};
