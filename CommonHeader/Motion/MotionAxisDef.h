#pragma once

static LPCTSTR G_strAxisName[] = {
	//TT01
	"TT01_TableDrive",	//Cruiser Axis #1
	"TT01_ThetaAlign",//ML3 #18
	"TT01_TableTurn",//ML3 #19

	//BT01
	"BT01_CassetteUpDown",//ML3 #22

	//LT01
	"LT01_BoxLoadPush",//ML3 #14

	//BO01
	"BO01_BoxRotate",//ML3 #10, #11 //좌우 동기축.
	"BO01_BoxUpDn",//ML3 #12, #13 //좌우 동기축.
	"BO01_BoxDrive",//ML3 #16

	//UT01
	"UT01_BoxUnLoadPush",//ML3 #15

	//TM01
	"TM01_ForwardBackward", //ML3 #17

	//TM02
	"TM02_PickerDrive",//Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
	"TM02_PickerUpDown",//ML3 #20
	"TM02_PickerShift",//ML3 #21
	"TM02_PickerRotate",//AxisLink_A Y

	//AM01
	"AM01_ScanDrive",//Cruiser Axis #6
	"AM01_ScanShift",//Cruiser Axis #2
	"AM01_ScanUpDn",//AxisLink_D X
	"AM01_SpaceSnUpDn",//AxisLink_D Y

	"AM01_ScopeDrive",//Cruiser Axis #5
	"AM01_ScopeShift",//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
	"AM01_ScopeUpDn",//AxisLink_D Z
	"AM01_AirBlowLeftRight",//AxisLink_D U

	"AM01_TensionLeftUpDn",//AxisLink_A Z
	"AM01_TensionRightUpDn",//AxisLink_A U
	"AM01_LTension1",//AxisLink_B X
	"AM01_LTension2",//AxisLink_B Y
	"AM01_LTension3",//AxisLink_B Z
	"AM01_LTension4",//AxisLink_B U
	"AM01_RTension1",//AxisLink_C X
	"AM01_RTension2",//AxisLink_C Y
	"AM01_RTension3",//AxisLink_C Z
	"AM01_RTension4",//AxisLink_C U
	"AM01_JigInOut"//ML3 #23
};
//Axis Define(이정의는 제어 관점에서의 축정의다 (겐트리와 동기 축은 1개로 본다))
enum
{
	//TT01
	AXIS_TT01_TableDrive = 0,			//Cruiser Axis #1
	AXIS_TT01_ThetaAlign = 1,			//ML3 #18
	AXIS_TT01_TableTurn,					//ML3 #19

	//BT01
	AXIS_BT01_CassetteUpDown,			//ML3 #22

	//LT01
	AXIS_LT01_BoxLoadPush,				//ML3 #14

	//BO01
	AXIS_BO01_BoxRotate,					//ML3 #10, #11 //좌우 동기축.
	AXIS_BO01_BoxUpDn,					//ML3 #12, #13 //좌우 동기축.
	AXIS_BO01_BoxDrive,					//ML3 #16

	//UT01
	AXIS_UT01_BoxUnLoadPush,			//ML3 #15

	//TM01
	AXIS_TM01_ForwardBackward,		//ML3 #17

	//TM02
	AXIS_TM02_PickerDrive,				//Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
	AXIS_TM02_PickerUpDown,			//ML3 #20
	AXIS_TM02_PickerShift,					//ML3 #21
	AXIS_TM02_PickerRotate,				//AxisLink_A Y

	//AM01
	AXIS_AM01_ScanDrive,					//Cruiser Axis #6
	AXIS_AM01_ScanShift,					//Cruiser Axis #2
	AXIS_AM01_ScanUpDn,					//AxisLink_D X
	AXIS_AM01_SpaceSnUpDn,			//AxisLink_D Y

	AXIS_AM01_ScopeDrive,				//Cruiser Axis #5
	AXIS_AM01_ScopeShift,				//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
	AXIS_AM01_ScopeUpDn,				//AxisLink_D Z
	AXIS_AM01_AirBlowLeftRight,			//AxisLink_D U

	AXIS_AM01_TensionLeftUpDn,		//AxisLink_A Z
	AXIS_AM01_TensionRightUpDn,		//AxisLink_A U
	AXIS_AM01_LTension1,					//AxisLink_B X
	AXIS_AM01_LTension2,					//AxisLink_B Y
	AXIS_AM01_LTension3,					//AxisLink_B Z
	AXIS_AM01_LTension4,					//AxisLink_B U
	AXIS_AM01_RTension1,					//AxisLink_C X
	AXIS_AM01_RTension2,					//AxisLink_C Y
	AXIS_AM01_RTension3,					//AxisLink_C Z
	AXIS_AM01_RTension4,					//AxisLink_C U

	AXIS_AM01_JigInOut,					//ML3 #23
	AXIS_Cnt,
};

//축의 위치가 3um오차 헝용.(동작 완료 체크시에 사용 한다.)
#define AXIS_INPOS_CTS		30	 //CTS단위
#define AXIS_INPOS_um		3	 //CTS단위

#define CHECK_AXIS_ALL(AxisNum) if(AxisNum<0||AxisNum> AXIS_AM01_RTension4) return false; 
#define CHECK_AXIS_TT01(AxisNum) if(AxisNum<0||AxisNum> AXIS_TT01_TableTurn) return false; 
#define CHECK_AXIS_LT01(AxisNum) if(AxisNum != AXIS_LT01_BoxLoadPush) return false; 
#define CHECK_AXIS_BT01(AxisNum) if(AxisNum != AXIS_BT01_CassetteUpDown) return false; 
#define CHECK_AXIS_BO01(AxisNum) if(AxisNum<AXIS_BO01_BoxRotate||AxisNum> AXIS_BO01_BoxDrive) return false; 
#define CHECK_AXIS_UT01(AxisNum) if(AxisNum != AXIS_UT01_BoxUnLoadPush) return false; 
#define CHECK_AXIS_TM01(AxisNum) if(AxisNum != AXIS_TM01_ForwardBackward) return false; 
#define CHECK_AXIS_TM02(AxisNum) if(AxisNum<AXIS_TM02_PickerDrive||AxisNum> AXIS_TM02_PickerRotate) return false; 
#define CHECK_AXIS_AM01(AxisNum) if(AxisNum<AXIS_AM01_ScanDrive||AxisNum> AXIS_AM01_JigInOut) return false; 


typedef union AXIS_STATUS_A_Tag
{
	struct
	{  
		DWORD		St00_Reserve						:1;
		DWORD		St01_Reserve						:1;
		DWORD		St02_Reserve						:1;
		DWORD		St03_Reserve						:1;
		DWORD		St04_Reserve			  			:1;
		DWORD		St05_Reserve		 				:1;
		DWORD		St06_Reserve						:1;
		DWORD		St07_Reserve		 				:1;
		DWORD		St08_Reserve						:1;
		DWORD		St09_Reserve						:1;
		DWORD		St10_HomeSearchActive		:1;
		DWORD		St11_BlockRequest				:1;
		DWORD		St12_AbortDeceleration			:1;
		DWORD		St13_DesiredVelocityZero		:1;
		DWORD		St14_DataBlockError				:1;
		DWORD		St15_DwellInProgress			:1;
		DWORD		St16_IntergrationMode			:1;
		DWORD		St17_RunningProgram			:1;
		DWORD		St18_OpenLoopMode				:1;
		DWORD		St19_PhasedMotor				:1;
		DWORD		St20_HandwheelEnabled		:1;
		DWORD		St21_PositiveEndLimitSet		:1;
		DWORD		St22_NegativeEndLimitSet		:1;
		DWORD		St23_MotorActivated			:1;
		DWORD		St24_Reserve						:1;
		DWORD		St25_Reserve						:1;
		DWORD		St26_Reserve						:1;
		DWORD		St27_Reserve						:1;
		DWORD		St28_Reserve						:1;
		DWORD		St29_Reserve						:1;
		DWORD		St30_Reserve						:1;
		DWORD		St31_Reserve						:1;
	};
	DWORD		MotStatus;

	AXIS_STATUS_A_Tag()
	{
		MotStatus = 0;
	}
}AXIS_STATUS_A;

typedef union AXIS_STATUS_B_Tag
{
	struct
	{  
		DWORD	St00_InPosition							:1;
		DWORD	St01_WarningFollowingError			:1;
		DWORD	St02_FatalFollowingError				:1;
		DWORD	St03_AmpFaultError						:1;
		DWORD	St04_BacklashDirectionFlag			:1;
		DWORD	St05_I2TAmpFaultError		 			:1;
		DWORD	St06_IntegratedFatalFollowError		:1;
		DWORD	St07_TriggerMove		 				:1;
		DWORD	St08_PhasingSearchError				:1;
		DWORD	St09_Reserved								:1;
		DWORD	St10_HomeComplete						:1;
		DWORD	St11_StoppedPositionLimit				:1;
		DWORD	St12_Reserved								:1;
		DWORD	St13_Reserved								:1;
		DWORD	St14_AmplifierEnabled					:1;
		DWORD	St15_Reserved								:1;
		DWORD	St16_Reserved								:1;
		DWORD	St17_Reserved								:1;
		DWORD	St18_Reserved								:1;
		DWORD	St19_Reserved								:1;
		DWORD	St20_Reserved								:1;
		DWORD	St21_Reserved								:1;
		DWORD	St22_Reserved								:1;
		DWORD	St23_Reserved								:1;
		DWORD	St24_Reserved								:1;
		DWORD	St25_Reserved								:1;
		DWORD	St26_Reserved								:1;
		DWORD	St27_Reserved								:1;
		DWORD	St28_Reserved								:1;
		DWORD	St29_Reserved								:1;
		DWORD	St30_Reserved								:1;
		DWORD	St31_Reserved								:1;
	};

	DWORD	MotStatus;	

	AXIS_STATUS_B_Tag()
	{
		MotStatus = 0;
	}
}AXIS_STATUS_B;

typedef struct LOCAL_AXIS_Tag
{
	AXIS_STATUS_A m_AxisStA;
	AXIS_STATUS_B m_AxisStB;
	LOCAL_AXIS_Tag()
	{
		m_AxisStA.MotStatus = 0;
		m_AxisStB.MotStatus = 0;
	}
}AxisStateCruiser;


typedef struct ML3_AXIS_Tag
{
	AXIS_STATUS_A m_AxisStA;
	AXIS_STATUS_B m_AxisStB;
	ML3_AXIS_Tag()
	{
		m_AxisStA.MotStatus = 0;
		m_AxisStB.MotStatus = 0;
	}
}AxisStateML3;

typedef union AL_Axis_StatusTag
{
	struct
	{  
		unsigned short	St00_SRUN					:1;//	펄스 출력중
		unsigned short	St01_SEND					:1;//펄스 출력종료
		unsigned short	St02_SERR					:1;//에러 발생시 1 로 됩 상태 읽어야 해지
		unsigned short	St03_SALM					:1;//알람 입력시
		unsigned short	St04_SRDY					:1;//Servo Drive Ready 신호 입력상태
		unsigned short	St05_SVON		 			:1;//SERVO ON 출력 신호 상태
		unsigned short	St06_SINP					:1;//INP 입력상태
		unsigned short	St07_SPEL		 			:1;//양방향 리미트시
		unsigned short	St08_SMEL					:1;//음방향 리미트시		
		unsigned short	St09_SORG					:1;//원점 입력		
		unsigned short	St10_UI0					:1;//범용입력 0			
		unsigned short	St11_UI1					:1;//범용입력 1		
		unsigned short	St12_UI2					:1;//범용입력 2 
		unsigned short	St13_UO0					:1;//범용출력 0
		unsigned short	St14_UO1					:1;//범용출력 1
		unsigned short	St15_UO2					:1;//범용출력 2
	};
	unsigned short	MotStatus;	

	AL_Axis_StatusTag()
	{
		MotStatus = 0;
	}
}AxisStateALink;


//Speed Table
typedef struct AxisSpeed_Tag
{
	float m_MaxAccValue;

	int m_AccTime;
	int m_DecTime;
	int m_MoveSpeed;//Jog Pos Speed

	AxisSpeed_Tag()
	{
		m_MaxAccValue = 0.1f;

		m_AccTime = 10;//msec
		m_DecTime = 10;//msec
		m_MoveSpeed = 100;//Jog Pos Speed
	}
}AxisSpeed;


//Cylinder의 상태를 표시 한다.
enum CY_STATE
{
	SOL_ERROR_BOTH_OFF = -4,
	SOL_ERROR_BOTH_ON = -3,
	SOL_ERROR_TIMEOUT = -2,
	SOL_ERROR_SENSOR = -1,
	SOL_NONE = 0,//상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
	SOL_UP = 1,
	SOL_DOWN = 2,
};