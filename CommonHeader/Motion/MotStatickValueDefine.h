#pragma once
//////////////////////////////////////////////////////////////////////////
//PLC 20번
//////////////////////////////////////////////////////////////////////////
//Mot Axis Link IO M Value Define(Input)

//Input I/O가 변하였을때 해당 위치를 알려준다.(PC쪽에서 읽지 않는다.)
//PLC에서는 UpDate값이 0일때 변화 감지를 하여 Flag Setting을 한다.
#define AL_INPUT_CheckValeu_A									0x0001
#define AL_INPUT_CheckValeu_B										0x0002
#define AL_INPUT_CheckValeu_C									0x0004
#define AL_INPUT_CheckValeu_D									0x0008
#define AL_INPUT_CheckValeu_E										0x0100
#define AL_INPUT_CheckValeu_F										0x0200
#define AL_INPUT_CheckValeu_G									0x0400
#define AL_INPUT_CheckValeu_H									0x0800
#define AL_INPUT_CheckValeu_I										0x0800  //H 갱신시에 같이 한다.

//#define AL_INPUT_CheckValeu_A    $0001
//#define AL_INPUT_CheckValeu_B    $0002
//#define AL_INPUT_CheckValeu_C    $0004
//#define AL_INPUT_CheckValeu_D    $0008
//#define AL_INPUT_CheckValeu_E    $0100
//#define AL_INPUT_CheckValeu_F    $0200
//#define AL_INPUT_CheckValeu_G    $0400
//#define AL_INPUT_CheckValeu_H    $0800
//#define AL_INPUT_CheckValeu_I    $0800

#define AL_IO_INPUT_UpDateBuf										"M4209" //Flag Setting을 위한  Buffer(PLC내부에서만 쓴다.)
#define AL_IO_INPUT_UpDate											"M4210" 
#define AL_IO_INPUT_UpDateReset									"M4210=0" 




//PLC Program에서 I/O 변화 감지를 위해서 변화 값 저장용(PC쪽에서 읽지 않는다. 초기 셋팅으로 )
#define AL_IO_INPUT_OLD_A1										"M4211=$FFFF"
#define AL_IO_INPUT_OLD_A2										"M4212=$FFFF"
#define AL_IO_INPUT_OLD_B1										"M4213=$FFFF"
#define AL_IO_INPUT_OLD_B2										"M4214=$FFFF"
#define AL_IO_INPUT_OLD_C1										"M4215=$FFFF"
#define AL_IO_INPUT_OLD_C2										"M4216=$FFFF"
#define AL_IO_INPUT_OLD_D1										"M4217=$FFFF"
#define AL_IO_INPUT_OLD_D2										"M4218=$FFFF"
#define AL_IO_INPUT_OLD_E1										"M4219=$FFFF"
#define AL_IO_INPUT_OLD_E2										"M4220=$FFFF"
#define AL_IO_INPUT_OLD_F1										"M4221=$FFFF"
#define AL_IO_INPUT_OLD_F2										"M4222=$FFFF"
#define AL_IO_INPUT_OLD_G1										"M4223=$FFFF"
#define AL_IO_INPUT_OLD_G2										"M4224=$FFFF"
#define AL_IO_INPUT_OLD_H1										"M4225=$FFFF"
#define AL_IO_INPUT_OLD_H2										"M4226=$FFFF"
#define AL_IO_INPUT_OLD_I1											"M4227=$FFFF"
#define AL_IO_INPUT_OLD_I2											"M4228=$FFFF"

//Mot Axis Link IO M Value Define(Input)
#define AL_IO_INPUT_A1												"M4231"
#define AL_IO_INPUT_A2												"M4232"
#define AL_IO_INPUT_B1												"M4233"
#define AL_IO_INPUT_B2												"M4234"
#define AL_IO_INPUT_C1												"M4235"
#define AL_IO_INPUT_C2												"M4236"
#define AL_IO_INPUT_D1												"M4237"
#define AL_IO_INPUT_D2												"M4238"
#define AL_IO_INPUT_E1												"M4239"
#define AL_IO_INPUT_E2												"M4240"
#define AL_IO_INPUT_F1												"M4241"
#define AL_IO_INPUT_F2												"M4242"
#define AL_IO_INPUT_G1												"M4243"
#define AL_IO_INPUT_G2												"M4244"
#define AL_IO_INPUT_H1												"M4245"
#define AL_IO_INPUT_H2												"M4246"
#define AL_IO_INPUT_I1												"M4247"
#define AL_IO_INPUT_I2												"M4248"
//AL_IO_INPUT_A1->Y:$6C082,8,16	//SLAVE 1 IN
//AL_IO_INPUT_A2->Y:$6C083,8,16
//AL_IO_INPUT_B1->Y:$6C084,8,16	//SLAVE 2 IN
//AL_IO_INPUT_B2->Y:$6C085,8,16
//AL_IO_INPUT_C1->Y:$6C086,8,16	//SLAVE 3 IN
//AL_IO_INPUT_C2->Y:$6C087,8,16
//AL_IO_INPUT_D1->Y:$6C088,8,16	//SLAVE 4 IN
//AL_IO_INPUT_D2->Y:$6C089,8,16
//AL_IO_INPUT_E1->Y:$6C08A,8,16	//SLAVE 5 IN
//AL_IO_INPUT_E2->Y:$6C08B,8,16
//AL_IO_INPUT_F1->Y:$6C08C,8,16	//SLAVE 6 IN
//AL_IO_INPUT_F2->Y:$6C08D,8,16
//AL_IO_INPUT_G1->Y:$6C08E,8,16	//SLAVE 7 IN
//AL_IO_INPUT_G2->Y:$6C08F,8,16
//AL_IO_INPUT_H1->Y:$6C090,8,16	//SLAVE 8 IN
//AL_IO_INPUT_H2->Y:$6C091,8,16
//AL_IO_INPUT_I1->Y:$6C092,8,16	//SLAVE 9 IN
//AL_IO_INPUT_I2->Y:$6C093,8,16

//Mot Axis Link IO M Value Define(Output)
#define AL_IO_OUTPUT_A1											"M4251"
#define AL_IO_OUTPUT_A2											"M4252"
#define AL_IO_OUTPUT_B1											"M4253"
#define AL_IO_OUTPUT_B2											"M4254"
#define AL_IO_OUTPUT_C1											"M4255"
#define AL_IO_OUTPUT_C2											"M4256"
#define AL_IO_OUTPUT_D1											"M4257"
#define AL_IO_OUTPUT_D2											"M4258"
#define AL_IO_OUTPUT_E1											"M4259"
#define AL_IO_OUTPUT_E2											"M4260"
#define AL_IO_OUTPUT_F1											"M4261"
#define AL_IO_OUTPUT_F2											"M4262"
#define AL_IO_OUTPUT_G1											"M4263"
#define AL_IO_OUTPUT_G2											"M4264"
#define AL_IO_OUTPUT_H1											"M4265"
#define AL_IO_OUTPUT_H2											"M4266"
#define AL_IO_OUTPUT_I1												"M4267"
#define AL_IO_OUTPUT_I2												"M4268"

//AL_IO_OUTPUT_A1->Y:$6C042,8,16//SLAVE 1 OUT
//AL_IO_OUTPUT_A2->Y:$6C043,8,16
//AL_IO_OUTPUT_B1->Y:$6C044,8,16	//SLAVE 2 OUT
//AL_IO_OUTPUT_B2->Y:$6C045,8,16
//AL_IO_OUTPUT_C1->Y:$6C046,8,16//SLAVE 3 OUT
//AL_IO_OUTPUT_C2->Y:$6C047,8,16
//AL_IO_OUTPUT_D1->Y:$6C048,8,16//SLAVE 4 OUT
//AL_IO_OUTPUT_D2->Y:$6C049,8,16
//AL_IO_OUTPUT_E1->Y:$6C04A,8,16//SLAVE 5 OUT
//AL_IO_OUTPUT_E2->Y:$6C04B,8,16
//AL_IO_OUTPUT_F1->Y:$6C04C,8,16//SLAVE 6 OUT
//AL_IO_OUTPUT_F2->Y:$6C04D,8,16
//AL_IO_OUTPUT_G1->Y:$6C04E,8,16//SLAVE 7 OUT
//AL_IO_OUTPUT_G2->Y:$6C04F,8,16
//AL_IO_OUTPUT_H1->Y:$6C050,8,16//SLAVE 8 OUT
//AL_IO_OUTPUT_H2->Y:$6C051,8,16
//AL_IO_OUTPUT_I1->Y:$6C052,8,16	//SLAVE 9 OUT
//AL_IO_OUTPUT_I2->Y:$6C053,8,16

//////////////////////////////////////////////////////////////////////////
//PLC21_28
//////////////////////////////////////////////////////////////////////////
//Axis Link Motion
//절대 위치 이송명령
#define AL_MOT_BLOCK_A_X_NewPOS								"P3201"
#define AL_MOT_BLOCK_A_Y_NewPOS								"P3202"
#define AL_MOT_BLOCK_A_Z_NewPOS								"P3203"
#define AL_MOT_BLOCK_A_U_NewPOS								"P3204"
#define AL_MOT_BLOCK_A_SelectAxis								"M4271"

#define AL_MOT_BLOCK_B_X_NewPOS								"P3205"
#define AL_MOT_BLOCK_B_Y_NewPOS								"P3206"
#define AL_MOT_BLOCK_B_Z_NewPOS								"P3207"
#define AL_MOT_BLOCK_B_U_NewPOS								"P3208"
#define AL_MOT_BLOCK_B_SelectAxis								"M4272"

#define AL_MOT_BLOCK_C_X_NewPOS								"P3209"
#define AL_MOT_BLOCK_C_Y_NewPOS								"P3210"
#define AL_MOT_BLOCK_C_Z_NewPOS								"P3211"
#define AL_MOT_BLOCK_C_U_NewPOS								"P3213"
#define AL_MOT_BLOCK_C_SelectAxis								"M4273"

#define AL_MOT_BLOCK_D_X_NewPOS								"P3214"
#define AL_MOT_BLOCK_D_Y_NewPOS								"P3215"
#define AL_MOT_BLOCK_D_Z_NewPOS								"P3216"
#define AL_MOT_BLOCK_D_U_NewPOS								"P3217"
#define AL_MOT_BLOCK_D_SelectAxis								"M4274"

//#define AL_MOT_BLOCK_CONECTIN									"P3218"

//Axis Link Mot All Stop And Block Mot Stop Flag(Stop 1, Normal 0)
#define AL_MOT_BLOCK_ALL_ESTOP								"M4281" //Axis Link A,B,C,D Block이 모두 비상정지 한다.
#define AL_MOT_BLOCK_A_SSTOP									"M4282"
#define AL_MOT_BLOCK_B_SSTOP									"M4283"
#define AL_MOT_BLOCK_C_SSTOP									"M4284"
#define AL_MOT_BLOCK_D_SSTOP									"M4285"

#define AL_MOT_BLOCK_ALL_STANDBY							"M4286"
#define AL_MOT_BLOCK_A_STANDBY								"M4287"
#define AL_MOT_BLOCK_B_STANDBY								"M4288"
#define AL_MOT_BLOCK_C_STANDBY								"M4289"
#define AL_MOT_BLOCK_D_STANDBY								"M4290"

#define AL_MOT_BLOCK_ALL_IDLE									"M4301"
#define AL_MOT_BLOCK_A_IDLE										"M4302"
#define AL_MOT_BLOCK_B_IDLE										"M4303"
#define AL_MOT_BLOCK_C_IDLE										"M4304"
#define AL_MOT_BLOCK_D_IDLE										"M4305"

//PLC 구동 상태 표시
#define PLC_00_ACTIVE												"M4310"//Axis Link Motion 구동.
#define PLC_01_ACTIVE												"M4311"
#define PLC_02_ACTIVE												"M4312"
#define PLC_03_ACTIVE												"M4313"
#define PLC_04_STICK_TENSION										"M4314"
#define PLC_05_ACTIVE												"M4315"
#define PLC_06_SPACE_MEASURE												"M4316"
#define PLC_07_GantryHome_30										"M4317"
#define PLC_08_GantryHome_31										"M4318"
#define PLC_09_ACTIVE												"M4319"
#define PLC_10_AxisLink_A_Home									"M4320"
#define PLC_11_AxisLink_B_Home									"M4321"
#define PLC_12_AxisLink_C_Home									"M4322"
#define PLC_13_AxisLink_D_Home									"M4323"
#define PLC_14_AxisLineServoOnOff									"M4324"//All Active
#define PLC_15_AxisLink_A_Move									"M4325"
#define PLC_16_AxisLink_B_Move									"M4326"
#define PLC_17_AxisLink_C_Move									"M4327"
#define PLC_18_AxisLink_D_Move									"M4328"
#define PLC_19_AxisLineStop											"M4329"//All Active
#define PLC_20_AxisLineInOutCheck								"M4330"//All Active
#define PLC_21_TT01_Home											"M4331"//TT01 Cruiser Axis #1, #18, #19
#define PLC_22_BT01_Home											"M4332"//BT01 ML3 #22
#define PLC_23_BO01_Home											"M4333"//BO01 ML3 #15, #16 (#10, #11 좌우 동기축),(#12, #13 좌우 동기축), 
#define PLC_24_TM01_Home											"M4334"//TM01 ML3 #17
#define PLC_25_TM02_Home											"M4335"//TM02 Cruiser Axis  #20, #21 (Gantry구동 PLC 8) 
#define PLC_26_AM01_Home											"M4336"//AM01 Cruiser Axis #6, #2, #5, (좌우 Gantry구동 PLC 7)
#define PLC_27_LT01_Home											"M4337"//LT01 ML3 #14,
#define PLC_28_UT01_Home											"M4338"//UT01 ML3 #15,
#define PLC_29_ACTIVE												"M4339"
#define PLC_30_ACTIVE												"M4340"//Axis Link Motion 구동
#define PLC_31_ACTIVE												"M4341"//Axis Link Motion 구동

//PLC_00_ACTIVE->Y:$003100,20,4
//PLC_01_ACTIVE->Y:$003101,20,4
//PLC_02_ACTIVE->Y:$003102,20,4
//PLC_03_ACTIVE->Y:$003103,20,4
//PLC_04_ACTIVE->Y:$003104,20,4
//PLC_05_ACTIVE->Y:$003105,20,4
//PLC_06_ACTIVE->Y:$003106,20,4
//PLC_07_ACTIVE->Y:$003107,20,4
//PLC_08_ACTIVE->Y:$003108,20,4
//PLC_09_ACTIVE->Y:$003109,20,4
//PLC_10_ACTIVE->Y:$00310A,20,4
//PLC_11_ACTIVE->Y:$00310B,20,4
//PLC_12_ACTIVE->Y:$00310C,20,4
//PLC_13_ACTIVE->Y:$00310D,20,4
//PLC_14_ACTIVE->Y:$00310E,20,4
//PLC_15_ACTIVE->Y:$00310F,20,4
//PLC_16_ACTIVE->Y:$003110,20,4
//PLC_17_ACTIVE->Y:$003111,20,4
//PLC_18_ACTIVE->Y:$003112,20,4
//PLC_19_ACTIVE->Y:$003113,20,4
//PLC_20_ACTIVE->Y:$003114,20,4
//PLC_21_ACTIVE->Y:$003115,20,4
//PLC_22_ACTIVE->Y:$003116,20,4
//PLC_23_ACTIVE->Y:$003117,20,4
//PLC_24_ACTIVE->Y:$003118,20,4
//PLC_25_ACTIVE->Y:$003119,20,4
//PLC_26_ACTIVE->Y:$00311A,20,4
//PLC_27_ACTIVE->Y:$00311B,20,4
//PLC_28_ACTIVE->Y:$00311C,20,4
//PLC_29_ACTIVE->Y:$00311D,20,4
//PLC_30_ACTIVE->Y:$00311E,20,4
//PLC_31_ACTIVE->Y:$00311F,20,4

//Module Home PLC Call Flag.
#define AXIS_HOME_FLAG_TT01										"M4351"  //Cruiser Axis #1, ML3 #18, ML3 #19
#define AXIS_HOME_FLAG_LT01										"M4352"  //#14, 
#define AXIS_HOME_FLAG_BT01										"M4353"  //ML3 #22
#define AXIS_HOME_FLAG_BO01										"M4354"  //ML3 (#10, #11 좌우 동기축),(#12, #13 좌우 동기축), #16
#define AXIS_HOME_FLAG_UT01										"M4355"  //#15,
#define AXIS_HOME_FLAG_TM01										"M4356"  //ML3 #17
#define AXIS_HOME_FLAG_TM02										"M4357"  //Cruiser Axis #20, #21 (#7+#8=>#31 Gantry구동)  
#define AXIS_HOME_FLAG_AM01										"M4358"  //Cruiser Axis #6, #2, #5, (#3+#4=>#30 좌우 Gantry구동)

//Homing 진행 과정을 표시 한다.(0~5까지-동기구동은 0~7)
#define AXIS_HOMING_RUN_TT01									"M4361"
#define AXIS_HOMING_RUN_LT01									"M4362"
#define AXIS_HOMING_RUN_BT01									"M4363"
#define AXIS_HOMING_RUN_BO01									"M4364"
#define AXIS_HOMING_RUN_UT01									"M4365"
#define AXIS_HOMING_RUN_TM01									"M4366"
#define AXIS_HOMING_RUN_TM02									"M4367"
#define AXIS_HOMING_RUN_AM01									"M4368"

//Gentry PLC 구동 위치 표시
//////////////////////////////////////////////////////////////////////////
//PLC7
//////////////////////////////////////////////////////////////////////////
#define AXIS_GANTRY_PLC7											"M4371"
//////////////////////////////////////////////////////////////////////////
//PLC8
//////////////////////////////////////////////////////////////////////////
#define AXIS_GANTRY_PLC8											"M4372"


//고정위치에서 Memory 할당을 하자. M변수 Memory 위치
#define AXIS_CRUISER_STATUS_A1									"M4380"
#define AXIS_CRUISER_STATUS_B1									"M4381"
#define AXIS_CRUISER_STATUS_A2									"M4382"
#define AXIS_CRUISER_STATUS_B2									"M4383"
#define AXIS_CRUISER_STATUS_A3									"M4384"
#define AXIS_CRUISER_STATUS_B3									"M4385"
#define AXIS_CRUISER_STATUS_A4									"M4386"
#define AXIS_CRUISER_STATUS_B4									"M4387"
#define AXIS_CRUISER_STATUS_A5									"M4388"
#define AXIS_CRUISER_STATUS_B5									"M4389"
#define AXIS_CRUISER_STATUS_A6									"M4390"
#define AXIS_CRUISER_STATUS_B6									"M4391"
#define AXIS_CRUISER_STATUS_A7									"M4392"
#define AXIS_CRUISER_STATUS_B7									"M4393"
#define AXIS_CRUISER_STATUS_A8									"M4394"
#define AXIS_CRUISER_STATUS_B8									"M4395"

#define AXIS_CRUISER_STATUS_A30								"M4396"
#define AXIS_CRUISER_STATUS_B30								"M4397"
#define AXIS_CRUISER_STATUS_A31								"M4398"
#define AXIS_CRUISER_STATUS_B31								"M4399"

#define AXIS_ML3_STATUS_A01										"M4400"
#define AXIS_ML3_STATUS_B01										"M4401"
#define AXIS_ML3_STATUS_A02										"M4402"
#define AXIS_ML3_STATUS_B02										"M4403"
#define AXIS_ML3_STATUS_A03										"M4404"
#define AXIS_ML3_STATUS_B03										"M4405"
#define AXIS_ML3_STATUS_A04										"M4406"
#define AXIS_ML3_STATUS_B04										"M4407"
#define AXIS_ML3_STATUS_A05										"M4408"
#define AXIS_ML3_STATUS_B05										"M4409"
#define AXIS_ML3_STATUS_A06										"M4410"
#define AXIS_ML3_STATUS_B06										"M4411"
#define AXIS_ML3_STATUS_A07										"M4412"
#define AXIS_ML3_STATUS_B07										"M4413"
#define AXIS_ML3_STATUS_A08										"M4414"
#define AXIS_ML3_STATUS_B08										"M4415"
#define AXIS_ML3_STATUS_A09										"M4416"
#define AXIS_ML3_STATUS_B09										"M4417"
#define AXIS_ML3_STATUS_A10										"M4418"
#define AXIS_ML3_STATUS_B10										"M4419"
#define AXIS_ML3_STATUS_A11										"M4420"
#define AXIS_ML3_STATUS_B11										"M4421"
#define AXIS_ML3_STATUS_A12										"M4422"
#define AXIS_ML3_STATUS_B12										"M4423"
#define AXIS_ML3_STATUS_A13										"M4424"
#define AXIS_ML3_STATUS_B13										"M4425"

#define AXIS_ML3_STATUS_A14										"M4426"
#define AXIS_ML3_STATUS_B14										"M4427"
//////////////////////////////////////////////////////////////////////////
//PLC 4번 사용 변수.(인장기.)
//LEFT_LOADCELL_VALUE1가 0이되면 PLC는 해당 값을 멈춘다.
//부호를 주어야 당길지 밀지를 결정 한다.
//정확히 0가 될 수 없음으로 0에 근사값이 되면 0으로 기입 한다.(PC에서)
//ex 범위가 +-3이라면 -3~+3의 값이 들어오면 해당.LEFT_LOADCELL_VALUE1값을 0으로 만든다.
//선택 축의 Load Cell Value가 모두 0이 되면 끝난다.(PLC4번이)
#define LEFT_LOADCELL_VALUE1									"P3221="//PC==> PLC5로 보내는 LoadCell 1번 Data.(Target과의 차이 값을 보낸다.)
#define LEFT_LOADCELL_VALUE2									"P3222="//PC==> PLC5로 보내는 LoadCell 2번 Data.(Target과의 차이 값을 보낸다.)
#define LEFT_LOADCELL_VALUE3									"P3223="//PC==> PLC5로 보내는 LoadCell 3번 Data.(Target과의 차이 값을 보낸다.)
#define LEFT_LOADCELL_VALUE4									"P3224="//PC==> PLC5로 보내는 LoadCell 4번 Data.(Target과의 차이 값을 보낸다.)

#define LEFT_LOADCELL_RESPONSE_TIME						"M4431="//Command 응답시간.Mot 와 Sync를 맞춘다.
#define AXIS_LINK_B_LOADCELL_SELECT						"M4432"//적용 축. 0이 되면 PLC는 종료 한다.
//Enable PLC를 하기전 축을 선택 하고 Enable PLC4를 하자.
//////////////////////////////////////////////////////////////////////////
//PLC5번 사용 변수.(인장기.)
//LEFT_LOADCELL_VALUE1가 0이되면 PLC는 해당 값을 멈춘다.
//부호를 주어야 당길지 밀지를 결정 한다.
//정확히 0가 될 수 없음으로 0에 근사값이 되면 0으로 기입 한다.(PC에서)
//ex 범위가 +-3이라면 -3~+3의 값이 들어오면 해당.LEFT_LOADCELL_VALUE1값을 0으로 만든다.
//선택 축의 Load Cell Value가 모두 0이 되면 끝난다.(PLC5번이)
#define RIGHT_LOADCELL_VALUE1								"P3225="//PC==> PLC5로 보내는 LoadCell 1번 Data.(Target과의 차이 값을 보낸다.)
#define RIGHT_LOADCELL_VALUE2								"P3226="//PC==> PLC5로 보내는 LoadCell 2번 Data.(Target과의 차이 값을 보낸다.)
#define RIGHT_LOADCELL_VALUE3								"P3227="//PC==> PLC5로 보내는 LoadCell 3번 Data.(Target과의 차이 값을 보낸다.)
#define RIGHT_LOADCELL_VALUE4								"P3228="//PC==> PLC5로 보내는 LoadCell 4번 Data.(Target과의 차이 값을 보낸다.)
#define RIGHT_LOADCELL_RESPONSE_TIME					"M4433="//Command 응답시간..Mot 와 Sync를 맞춘다.
#define AXIS_LINK_C_LOADCELL_SELECT						"M4434"//적용 축. 0이 되면 PLC는 종료 한다.
//Enable PLC를 하기전 축을 선택 하고 Enable PLC5를 하자.
//////////////////////////////////////////////////////////////////////////
//PLC 6번 사용 변수.(Space Sensor)
#define SPACE_SENSER_VALUE1										"P3229="//PC==> PLC6로 보내는 Space Senser Out1 Data.
#define SPACE_SENSER_VALUE2										"P3230="//PC==> PLC6로 보내는  Space Senser Out1 Data.
#define SPACE_SENSER_RESPONSE_TIME							"M4435="//Command 응답시간..Mot 와 Sync를 맞춘다.


#define AXIS_BUF_HOMEJOG_ACC_TT01							"P3240"
#define AXIS_BUF_HOMEJOG_RUN_TT01							"P3241"
#define AXIS_BUF_HOMEJOG_ACC_LT01							"P3242"
#define AXIS_BUF_HOMEJOG_RUN_LT01							"P3243"
#define AXIS_BUF_HOMEJOG_ACC_BT01							"P3244"
#define AXIS_BUF_HOMEJOG_RUN_BT01							"P3245"
#define AXIS_BUF_HOMEJOG_ACC_BO01							"P3246"
#define AXIS_BUF_HOMEJOG_RUN_BO01							"P3247"
#define AXIS_BUF_HOMEJOG_ACC_UT01							"P3248"
#define AXIS_BUF_HOMEJOG_RUN_UT01							"P3249"
#define AXIS_BUF_HOMEJOG_ACC_TM01							"P3250"
#define AXIS_BUF_HOMEJOG_RUN_TM01							"P3251"
#define AXIS_BUF_HOMEJOG_ACC_TM02							"P3252"
#define AXIS_BUF_HOMEJOG_RUN_TM02							"P3253"
#define AXIS_BUF_HOMEJOG_ACC_AM01							"P3254"
#define AXIS_BUF_HOMEJOG_RUN_AM01							"P3255"

#define AXIS_PICKER_HOME_SETUP_OFFSET1						"P3333"
#define AXIS_PICKER_HOME_SETUP_OFFSET2						"P3334"


#define LEFT_LOADCELL_STEP_VALUE1								"P3341="
#define LEFT_LOADCELL_STEP_VALUE2								"P3342="
#define LEFT_LOADCELL_STEP_VALUE3								"P3343="
#define LEFT_LOADCELL_STEP_VALUE4								"P3344="

#define RIGHT_LOADCELL_STEP_VALUE1								"P3345="
#define RIGHT_LOADCELL_STEP_VALUE2								"P3346="
#define RIGHT_LOADCELL_STEP_VALUE3								"P3347="
#define RIGHT_LOADCELL_STEP_VALUE4								"P3348="



#define AL_MOT_BLOCK_A_SPEED_PROFILE						"M4441"
#define AL_MOT_BLOCK_B_SPEED_PROFILE						"M4442"
#define AL_MOT_BLOCK_C_SPEED_PROFILE						"M4443"
#define AL_MOT_BLOCK_D_SPEED_PROFILE						"M4444"

//Scope의 움직임이 간섭이 없다는것을 보장 한다 
#define INTERLOCK_SCOPE_BWD_SENSER							"M4428"



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//이하 P변수는이미 고정된것이다.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define P_B00_A_X_STS	"P7536"		//X Axis Status             
#define P_B00_A_Y_STS	"P7537"	    //Y Axis Status             
#define P_B00_A_Z_STS	"P7538"	    //Z Axis Status             
#define P_B00_A_U_STS	"P7539"	    //U Axis Status 

#define P_B00_A_X_INCUN		"P7540"  // X Axis Internal Counter   
#define P_B00_A_Y_INCUN		"P7541"  // Y Axis Internal Counter   
#define P_B00_A_Z_INCUN		"P7542"  // Z Axis Internal Counter   
#define P_B00_A_U_INCUN		"P7543"  // U Axis Internal Counter   
#define P_B00_A_X_EXCUN	"P7544"  // X Axis External Counter   
#define P_B00_A_Y_EXCUN	"P7545"  // Y Axis External Counter   
#define P_B00_A_Z_EXCUN	"P7546"  // Z Axis External Counter   
#define P_B00_A_U_EXCUN	"P7547"  // U Axis External Counter  

#define P_B00_B_X_STS	"P7576"	
#define P_B00_B_Y_STS	"P7577"	
#define P_B00_B_Z_STS	"P7578"	
#define P_B00_B_U_STS	"P7579"

#define P_B00_B_X_INCUN		"P7580"
#define P_B00_B_Y_INCUN		"P7581"
#define P_B00_B_Z_INCUN		"P7582"
#define P_B00_B_U_INCUN		"P7583"
#define P_B00_B_X_EXCUN	"P7584"
#define P_B00_B_Y_EXCUN	"P7585"
#define P_B00_B_Z_EXCUN	"P7586"
#define P_B00_B_U_EXCUN	"P7587"

#define P_B00_C_X_STS	 "P7616"	
#define P_B00_C_Y_STS	 "P7617"	
#define P_B00_C_Z_STS	 "P7618"	
#define P_B00_C_U_STS	 "P7619"	

#define P_B00_C_X_INCUN		"P7620"
#define P_B00_C_Y_INCUN		"P7621"
#define P_B00_C_Z_INCUN		"P7622"
#define P_B00_C_U_INCUN		"P7623"
#define P_B00_C_X_EXCUN	"P7624"
#define P_B00_C_Y_EXCUN	"P7625"
#define P_B00_C_Z_EXCUN	"P7626"
#define P_B00_C_U_EXCUN	"P7627"

#define P_B00_D_X_STS	"P7656"	
#define P_B00_D_Y_STS	"P7657"	
#define P_B00_D_Z_STS	"P7658"	
#define P_B00_D_U_STS	"P7659"	

#define P_B00_D_X_INCUN		"P7660"
#define P_B00_D_Y_INCUN		"P7661"
#define P_B00_D_Z_INCUN		"P7662"
#define P_B00_D_U_INCUN		"P7663"
#define P_B00_D_X_EXCUN	"P7664"
#define P_B00_D_Y_EXCUN	"P7665"
#define P_B00_D_Z_EXCUN	"P7666"
#define P_B00_D_U_EXCUN	"P7667"



//이하 M변수는 고정 수치 이다.
//Home Complet확인용.
//TT01
#define HOME_COMPLET_TT01_TableDrive					"M145"//Cruiser Axis #1
#define HOME_COMPLET_TT01_ThetaAlign					"M1845"//ML3 #18
#define HOME_COMPLET_TT01_TableTurn						"M1945"//ML3 #19

//BT01
#define HOME_COMPLET_BT01_CassetteUpDown			"M2245"//ML3 #22

//LT01
#define HOME_COMPLET_LT01_BoxLoadPush					"M1445"//ML3 #14

//BO01
#define HOME_COMPLET_BO01_BoxRotateL					"M1045"//ML3 #10, #11 //좌우 동기축.
#define HOME_COMPLET_BO01_BoxRotateR					"M1145"
#define HOME_COMPLET_BO01_BoxUpDnL						"M1245"//ML3 #12, #13 //좌우 동기축.
#define HOME_COMPLET_BO01_BoxUpDnR						"M1345"
#define HOME_COMPLET_BO01_BoxDrive						"M1645"//ML3 #16

//UT01
#define HOME_COMPLET_UT01_BoxUnLoadPush				"M1545"//ML3 #15

//TM01
#define HOME_COMPLET_TM01_ForwardBackward			"M1745"//ML3 #17

//TM02
#define HOME_COMPLET_TM02_PickerDrive					"M3145"//Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
#define HOME_COMPLET_TM02_PickerUpDown				"M2045"//ML3 #20
#define HOME_COMPLET_TM02_PickerShift					"M2145"//ML3 #21
//#define HOME_COMPLET_TM02_PickerRotate					"M"//AxisLink_A Y

//AM01
#define HOME_COMPLET_AM01_ScanDrive					"M645"//Cruiser Axis #6
#define HOME_COMPLET_AM01_ScanShift						"M245"//Cruiser Axis #2
//#define HOME_COMPLET_AM01_ScanUpDn					"M"//AxisLink_D X
//#define HOME_COMPLET_AM01_SpaceSnUpDn				"M"//AxisLink_D Y

#define HOME_COMPLET_AM01_ScopeDrive					"M545"//Cruiser Axis #5
#define HOME_COMPLET_AM01_ScopeShift					"M3045"//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
//#define HOME_COMPLET_AM01_ScopeUpDn				"M"//AxisLink_D Z
//#define HOME_COMPLET_AM01_AirBlowLeftRight			"M"//AxisLink_D U

//#define HOME_COMPLET_AM01_TensionLeftUpDn			"M"//AxisLink_A Z
//#define HOME_COMPLET_AM01_TensionRightUpDn		"M"//AxisLink_A U
//#define HOME_COMPLET_AM01_LTension1					"M"//AxisLink_B X
//#define HOME_COMPLET_AM01_LTension2					"M"//AxisLink_B Y
//#define HOME_COMPLET_AM01_LTension3					"M"//AxisLink_B Z
//#define HOME_COMPLET_AM01_LTension4					"M"//AxisLink_B U
//#define HOME_COMPLET_AM01_RTension1					"M"//AxisLink_C X
//#define HOME_COMPLET_AM01_RTension2					"M"//AxisLink_C Y
//#define HOME_COMPLET_AM01_RTension3					"M"//AxisLink_C Z
//#define HOME_COMPLET_AM01_RTension4					"M"//AxisLink_C U

#define HOME_COMPLET_AM01_JigInOut						"M2345"//ML3 #23
//Open Loop 확인용.
//TT01
#define OPEN_LOOP_TT01_TableDrive						"M138"//Cruiser Axis #1
#define OPEN_LOOP_TT01_ThetaAlign						"M1838"//ML3 #18
#define OPEN_LOOP_TT01_TableTurn						"M1938"//ML3 #19
//BT01
#define OPEN_LOOP_BT01_CassetteUpDown				"M2238"//ML3 #22
//LT01
#define OPEN_LOOP_LT01_BoxLoadPush					"M1438"//ML3 #14
//BO01
#define OPEN_LOOP_BO01_BoxRotateL						"M1038"//ML3 #10, #11 //좌우 동기축.
#define OPEN_LOOP_BO01_BoxRotateR						"M1138"
#define OPEN_LOOP_BO01_BoxUpDnL						"M1238"//ML3 #12, #13 //좌우 동기축.
#define OPEN_LOOP_BO01_BoxUpDnR						"M1338"
#define OPEN_LOOP_BO01_BoxDrive							"M1638"//ML3 #16
//UT01
#define OPEN_LOOP_UT01_BoxUnLoadPush				"M1538"//ML3 #15
//TM01
#define OPEN_LOOP_TM01_ForwardBackward			"M1738"//ML3 #17
//TM02
#define OPEN_LOOP_TM02_PickerDrive						"M3138"//Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
#define OPEN_LOOP_TM02_PickerUpDown					"M2038"//ML3 #20
#define OPEN_LOOP_TM02_PickerShift						"M2138"//ML3 #21
//#define OPEN_LOOP_TM02_PickerRotate				"M"//AxisLink_A Y
//AM01
#define OPEN_LOOP_AM01_ScanDrive						"M638"//Cruiser Axis #6
#define OPEN_LOOP_AM01_ScanShift						"M238"//Cruiser Axis #2
//#define OPEN_LOOP_AM01_ScanUpDn					"M"//AxisLink_D X
//#define OPEN_LOOP_AM01_SpaceSnUpDn				"M"//AxisLink_D Y
#define OPEN_LOOP_AM01_ScopeDrive						"M538"//Cruiser Axis #5
#define OPEN_LOOP_AM01_ScopeShift						"M3038"//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
//#define OPEN_LOOP_AM01_ScopeUpDn					"M"//AxisLink_D Z
//#define OPEN_LOOP_AM01_AirBlowLeftRight			"M"//AxisLink_D U
//#define OPEN_LOOP_AM01_TensionLeftUpDn			"M"//AxisLink_A Z
//#define OPEN_LOOP_AM01_TensionRightUpDn			"M"//AxisLink_A U
//#define OPEN_LOOP_AM01_LTension1					"M"//AxisLink_B X
//#define OPEN_LOOP_AM01_LTension2					"M"//AxisLink_B Y
//#define OPEN_LOOP_AM01_LTension3					"M"//AxisLink_B Z
//#define OPEN_LOOP_AM01_LTension4					"M"//AxisLink_B U
//#define OPEN_LOOP_AM01_RTension1					"M"//AxisLink_C X
//#define OPEN_LOOP_AM01_RTension2					"M"//AxisLink_C Y
//#define OPEN_LOOP_AM01_RTension3					"M"//AxisLink_C Z
//#define OPEN_LOOP_AM01_RTension4					"M"//AxisLink_C U


//AMP ENABLE 확인용.
//TT01
#define AMP_ENABLE_TT01_TableDrive						"M139"//Cruiser Axis #1
#define AMP_ENABLE_TT01_ThetaAlign					"M1839"//ML3 #18
#define AMP_ENABLE_TT01_TableTurn						"M1939"//ML3 #19
//BT01
#define AMP_ENABLE_BT01_CassetteUpDown			"M2239"//ML3 #22
//LT01
#define AMP_ENABLE_LT01_BoxLoadPush					"M1439"//ML3 #14
//BO01
#define AMP_ENABLE_BO01_BoxRotateL					"M1039"//ML3 #10, #11 //좌우 동기축.
#define AMP_ENABLE_BO01_BoxRotateR					"M1139"
#define AMP_ENABLE_BO01_BoxUpDnL						"M1239"//ML3 #12, #13 //좌우 동기축.
#define AMP_ENABLE_BO01_BoxUpDnR						"M1339"
#define AMP_ENABLE_BO01_BoxDrive						"M1639"//ML3 #16
//UT01
#define AMP_ENABLE_UT01_BoxUnLoadPush				"M1539"//ML3 #15
//TM01
#define AMP_ENABLE_TM01_ForwardBackward			"M1739"//ML3 #17
//TM02
#define AMP_ENABLE_TM02_PickerDrive					"M3139"//Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
#define AMP_ENABLE_TM02_PickerUpDown				"M2039"//ML3 #20
#define AMP_ENABLE_TM02_PickerShift					"M2139"//ML3 #21
//#define AMP_ENABLE_TM02_PickerRotate				"M"//AxisLink_A Y
//AM01
#define AMP_ENABLE_AM01_ScanDrive						"M639"//Cruiser Axis #6
#define AMP_ENABLE_AM01_ScanShift						"M239"//Cruiser Axis #2
//#define AMP_ENABLE_AM01_ScanUpDn					"M"//AxisLink_D X
//#define AMP_ENABLE_AM01_SpaceSnUpDn				"M"//AxisLink_D Y
#define AMP_ENABLE_AM01_ScopeDrive					"M539"//Cruiser Axis #5
#define AMP_ENABLE_AM01_ScopeShift					"M3039"//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
//#define AMP_ENABLE_AM01_ScopeUpDn					"M"//AxisLink_D Z
//#define AMP_ENABLE_AM01_AirBlowLeftRight			"M"//AxisLink_D U
//#define AMP_ENABLE_AM01_TensionLeftUpDn			"M"//AxisLink_A Z
//#define AMP_ENABLE_AM01_TensionRightUpDn		"M"//AxisLink_A U
//#define AMP_ENABLE_AM01_LTension1					"M"//AxisLink_B X
//#define AMP_ENABLE_AM01_LTension2					"M"//AxisLink_B Y
//#define AMP_ENABLE_AM01_LTension3					"M"//AxisLink_B Z
//#define AMP_ENABLE_AM01_LTension4					"M"//AxisLink_B U
//#define AMP_ENABLE_AM01_RTension1					"M"//AxisLink_C X
//#define AMP_ENABLE_AM01_RTension2					"M"//AxisLink_C Y
//#define AMP_ENABLE_AM01_RTension3					"M"//AxisLink_C Z
//#define AMP_ENABLE_AM01_RTension4					"M"//AxisLink_C U