#pragma once

enum STWOER_MODE
{
	STWOER_MODE0_NONE = 0,		
	STWOER_MODE2_IDLE1 = 1,		//설비가 정상(Glass가 없을때	)
	STWOER_MODE6_CST,				//설비는 정상 상태(CST 배출 요청)
	STWOER_MODE1_RUN,				//설비가 정상(생산 상태)
	STWOER_MODE7_PM,				//설비는 PM 상태
	STWOER_MODE3_IDLE2,			//설비가 정상(투입 중지 상태 일때)
	STWOER_MODE8_OPCall,			//Operator Call 상태( CIM, Loader, EQ 표시)
	STWOER_MODE4_ALARM,			//설비는 기동 상태(경알람)		
	STWOER_MODE5_FAULT,			//설비는 정지 상태(생산 불가능 상태)
	STWOER_MODE9_MachineDown, //설비 구동 정지 상태.
	//////////////////////////////////////////////////////////////////////////
	STWOER_MODE_Cnt
};

typedef union AL_A_IN_TAG
{
	struct
	{  
		DWORD		In00_EMO1										:1;	
		DWORD		In01_EMO2										:1;
		DWORD		In02_EMO3										:1;
		DWORD		In03_Sn_IDXFrontDoorOpenClose			:1;
		DWORD		In04_Sn_IDXFrontDoorLock				:1;
		DWORD		In05_Sn_BT01CouplingLeft					:1;
		DWORD		In06_Sn_BT01CouplingRight				:1;
		DWORD		In07_Sn_IDXSideDoorLeftOpenClose		:1;
		DWORD		In08_Sn_IDXSideDoorLeftLock				:1;
		DWORD		In09_Sn_IDXSideDoorRightOpenClose	:1;
		DWORD		In10_Sn_IDXSideDoorRightLock			:1;
		DWORD		In11_Sn_InspSideDoorLeftOpenClose	:1;
		DWORD		In12_Sn_InspSideDoorLeftLock			:1;
		DWORD		In13_Sn_InspSideDoorRightOpenClose	:1;
		DWORD		In14_Sn_InspSideDoorRightLock			:1;
		DWORD		In15_Sn_InspBackDoorLeftOpenClose	:1;
		DWORD		In16_Sn_InspBackDoorLeftLock			:1;	
		DWORD		In17_Sn_InspBackDoorRightOpenClose	:1;
		DWORD		In18_Sn_InspBackDoorRightLock			:1;
		DWORD		In19_AutoTeachMode						:1;
		DWORD		In20_ElectronicBoxTempAlarm1			:1;
		DWORD		In21_ElectronicBoxTempAlarm2			:1;
		DWORD		In22_PCRackTempAlarm1					:1;
		DWORD		In23_PCRackTempAlarm2					:1;
		DWORD		In24_SystemOn								:1;
		DWORD		In25_BuzzerReset								:1;
		DWORD		In26_Sn_CassetteLoadingPosSensor		:1;
		DWORD		In27_EQ_Lamp									:1;
		DWORD		In28_EMO4										:1;
		DWORD		In29_TeachMoveEnable						:1;
		DWORD		In30_TwoPaperAbsorbInterlock1			:1;
		DWORD		In31_TwoPaperAbsorbInterlock2			:1;
	};
	DWORD	 shAL_IN;

	AL_A_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_A_IN_TAG));
	}
}ALIO_A_In;

typedef union AL_A_Out_TAG
{
	struct
	{  
		DWORD		Out00_IndexerFrontDoorRelease		:1;	
		DWORD		Out01_SideDoorAllRelease				:1;
		DWORD		Out02_InspectorBackDoorRelease	:1;
		DWORD		Out03_TeachKeyUnlock					:1;
		DWORD		Out04_Reserve							:1;
		DWORD		Out05_Reserve							:1;
		DWORD		Out06_Reserve							:1;
		DWORD		Out07_Reserve							:1;
		DWORD		Out08_SignalTower_Red				:1;		
		DWORD		Out09_SignalTower_Yellow				:1;		
		DWORD		Out10_SignalTower_Green				:1;			
		DWORD		Out11_Buzzer1								:1;			
		DWORD		Out12_Buzzer2								:1;
		DWORD		Out13_Reserve		:1;
		DWORD		Out14_Reserve		:1;
		DWORD		Out15_Reserve		:1;

		DWORD		Out16_Reserve		:1;	
		DWORD		Out17_Reserve		:1;
		DWORD		Out18_Reserve		:1;
		DWORD		Out19_Reserve		:1;
		DWORD		Out20_Reserve		:1;
		DWORD		Out21_Reserve		:1;
		DWORD		Out22_Reserve		:1;
		DWORD		Out23_Reserve		:1;
		DWORD		Out24_Reserve		:1;		
		DWORD		Out25_Reserve		:1;		
		DWORD		Out26_Reserve		:1;			
		DWORD		Out27_Reserve		:1;			
		DWORD		Out28_Reserve		:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve								:1;
		DWORD		Out31_Space_Sensor_LaserOnOff		:1;
	};
	DWORD	 shAL_Out;

	AL_A_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_A_Out_TAG));
	}
}ALIO_A_Out;

typedef union AL_B_IN_TAG
{
	struct
	{  
		DWORD		In00_Sn_BoxTable1_100mm			:1;	
		DWORD		In01_Sn_BoxTable1_200mm			:1;
		DWORD		In02_Sn_BoxTable1_440mm			:1;
		DWORD		In03_Sn_BoxTable2_100mm			:1;
		DWORD		In04_Sn_BoxTable2_200mm			:1;
		DWORD		In05_Sn_BoxTable2_440mm			:1;
		DWORD		In06_Sn_BoxTable3_100mm			:1;
		DWORD		In07_Sn_BoxTable3_200mm			:1;
		DWORD		In08_Sn_BoxTable3_440mm			:1;		
		DWORD		In09_Sn_BoxTable4_100mm			:1;		
		DWORD		In10_Sn_BoxTable4_200mm			:1;			
		DWORD		In11_Sn_BoxTable4_440mm			:1;			
		DWORD		In12_Sn_BoxTable5_100mm			:1;
		DWORD		In13_Sn_BoxTable5_200mm			:1;
		DWORD		In14_Sn_BoxTable5_440mm			:1;
		DWORD		In15_Sn_BoxTable6_100mm			:1;
		DWORD		In16_Sn_BoxTable6_200mm			:1;	
		DWORD		In17_Sn_BoxTable6_440mm			:1;
		DWORD		In18_Sn_BoxTable7_100mm			:1;
		DWORD		In19_Sn_BoxTable7_200mm			:1;
		DWORD		In20_Sn_BoxTable7_440mm			:1;
		DWORD		In21_Sn_BoxTableF_100mm			:1;
		DWORD		In22_Sn_BoxTableF_200mm			:1;
		DWORD		In23_Sn_BoxTableF_440mm			:1;
		DWORD		In24_Reserve		:1;		
		DWORD		In25_Reserve		:1;		
		DWORD		In26_Reserve		:1;			
		DWORD		In27_Reserve		:1;			
		DWORD		In28_Reserve		:1;
		DWORD		In29_Reserve		:1;
		DWORD		In30_Reserve		:1;
		DWORD		In31_Reserve		:1;
	};
	DWORD	 shAL_IN;

	AL_B_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_B_IN_TAG));
	}
}ALIO_B_In;

typedef union AL_B_Out_TAG
{
	struct
	{  
		DWORD		Out00_Reserve		:1;	
		DWORD		Out01_Reserve		:1;
		DWORD		Out02_Reserve		:1;
		DWORD		Out03_Reserve		:1;
		DWORD		Out04_Reserve		:1;
		DWORD		Out05_Reserve		:1;
		DWORD		Out06_Reserve		:1;
		DWORD		Out07_Reserve		:1;
		DWORD		Out08_Reserve		:1;		
		DWORD		Out09_Reserve		:1;		
		DWORD		Out10_Reserve		:1;			
		DWORD		Out11_Reserve		:1;			
		DWORD		Out12_Reserve		:1;
		DWORD		Out13_Reserve		:1;
		DWORD		Out14_Reserve		:1;
		DWORD		Out15_Reserve		:1;

		DWORD		Out16_Reserve		:1;	
		DWORD		Out17_Reserve		:1;
		DWORD		Out18_Reserve		:1;
		DWORD		Out19_Reserve		:1;
		DWORD		Out20_Reserve		:1;
		DWORD		Out21_Reserve		:1;
		DWORD		Out22_Reserve		:1;
		DWORD		Out23_Reserve		:1;
		DWORD		Out24_Reserve		:1;		
		DWORD		Out25_Reserve		:1;		
		DWORD		Out26_Reserve		:1;			
		DWORD		Out27_Reserve		:1;			
		DWORD		Out28_Reserve		:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve		:1;
		DWORD		Out31_Reserve		:1;
	};
	DWORD	 shAL_Out;

	AL_B_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_B_Out_TAG));
	}
}ALIO_B_Out;

typedef union AL_C_IN_TAG
{
	struct
	{  
		DWORD		In00_Sn_BoxLeftCylinderForward				:1;	
		DWORD		In01_Sn_BoxLeftCylinderBackward			:1;
		DWORD		In02_Sn_BoxDetection							:1;
		DWORD		In03_Sn_BoxLeftVacuum						:1;
		DWORD		In04_Sn_BoxRightCylinderForward			:1;
		DWORD		In05_Sn_BoxRightCylinderBackward			:1;
		DWORD		In06_Sn_BoxRightVacuum						:1;
		DWORD		In07_Sn_LoadBoxSmallDetection				:1;
		DWORD		In08_Sn_LoadBoxMediumDetection			:1;		
		DWORD		In09_Sn_LoadBoxLargeDetection				:1;		
		DWORD		In10_Sn_LoadBoxVacuum						:1;			
		DWORD		In11_Sn_LoadLeftAlignForward				:1;			
		DWORD		In12_Sn_LoadLeftAlignBackward				:1;
		DWORD		In13_Sn_LoadRightAlignForward				:1;
		DWORD		In14_Sn_LoadRightAlignBackward			:1;
		DWORD		In15_Sn_UnloadBoxDetection					:1;
		DWORD		In16_Sn_UnloadBoxVacuum					:1;	
		DWORD		In17_Sn_UnloadBoxLeftAlignForward			:1;
		DWORD		In18_Sn_UnloadBoxLeftAlignBackward		:1;
		DWORD		In19_Sn_UnloadBoxRightAlignForward		:1;
		DWORD		In20_Sn_UnloadBoxRightAlignBackward		:1;
		DWORD		In21_Sn_ForkBoxDetection						:1;
		DWORD		In22_Sn_ForkBoxVacuum						:1;
		DWORD		In23_Sn_ForkFrontUp							:1;
		DWORD		In24_Sn_ForkFrontDown						:1;		
		DWORD		In25_Sn_ForkRearUp								:1;		
		DWORD		In26_Sn_ForkRearDown							:1;			
		DWORD		In27_Sn_IndexerMainCDA						:1;			
		DWORD		In28_Sn_InspectMainCDA						:1;
		DWORD		In29_Sn_MainN2									:1;
		DWORD		In30_Sn_IndexerMainVacuum					:1;
		DWORD		In31_Reserve		:1;
	};
	DWORD	 shAL_IN;

	AL_C_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_C_IN_TAG));
	}
}ALIO_C_In;

typedef union AL_C_Out_TAG
{
	struct
	{  
		DWORD		Out00_BoxLeftCylinderForward				:1;	
		DWORD		Out01_BoxLeftCylinderBackward				:1;
		DWORD		Out02_BoxLeftVacuum							:1;
		DWORD		Out03_BoxLeftPurge								:1;
		DWORD		Out04_BoxRightCylinderForward				:1;
		DWORD		Out05_BoxRightCylinderBackward				:1;
		DWORD		Out06_BoxRightVacuum							:1;
		DWORD		Out07_BoxRightPurge							:1;
		DWORD		Out08_LoadBoxVacuum							:1;		
		DWORD		Out09_LoadBoxPurge								:1;		
		DWORD		Out10_LoadBoxAlignCylinderForward			:1;			
		DWORD		Out11_LoadBoxAlignCylinderBackward		:1;			
		DWORD		Out12_UnloadBoxVacuum						:1;
		DWORD		Out13_UnloadBoxPurge							:1;
		DWORD		Out14_UnloadBoxAlignCylinderForward		:1;
		DWORD		Out15_UnloadBoxAlignCylinderBackward	:1;
		DWORD		Out16_ForkVacuumSol							:1;	
		DWORD		Out17_ForkPurge									:1;
		DWORD		Out18_ForkUpSol									:1;
		DWORD		Out19_ForkDownSol								:1;
		DWORD		Out20_N2Blowing		:1;
		DWORD		Out21_Reserve		:1;
		DWORD		Out22_Reserve		:1;
		DWORD		Out23_Reserve		:1;
		DWORD		Out24_Reserve		:1;		
		DWORD		Out25_Reserve		:1;		
		DWORD		Out26_Reserve		:1;			
		DWORD		Out27_Reserve		:1;			
		DWORD		Out28_Reserve		:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve		:1;
		DWORD		Out31_Reserve		:1;
	};
	DWORD	 shAL_Out;

	AL_C_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_C_Out_TAG));
	}
}ALIO_C_Out;

typedef union AL_D_IN_TAG
{
	struct
	{  
		DWORD		In00_Sn_LeftOpenGripper1						:1;	
		DWORD		In01_Sn_LeftCloseGripper1						:1;
		DWORD		In02_Sn_LeftOpenGripper2						:1;
		DWORD		In03_Sn_LeftCloseGripper2						:1;
		DWORD		In04_Sn_LeftOpenGripper3						:1;
		DWORD		In05_Sn_LeftCloseGripper3						:1;
		DWORD		In06_Sn_LeftOpenGripper4						:1;
		DWORD		In07_Sn_LeftCloseGripper4						:1;
		DWORD		In08_Sn_LeftTensionCylinderForward			:1;		
		DWORD		In09_Sn_LeftTensionCylinderBackward			:1;		
		DWORD		In10_Sn_LeftTensionStickDetection				:1;			
		DWORD		In11_Sn_LeftTensionPressure						:1;			
		DWORD		In12_Sn_RightOpenGripper1						:1;
		DWORD		In13_Sn_RightCloseGripper1						:1;
		DWORD		In14_Sn_RightOpenGripper2						:1;
		DWORD		In15_Sn_RightCloseGripper2						:1;
		DWORD		In16_Sn_RightOpenGripper3						:1;	
		DWORD		In17_Sn_RightCloseGripper3						:1;
		DWORD		In18_Sn_RightOpenGripper4						:1;
		DWORD		In19_Sn_RightCloseGripper4						:1;
		DWORD		In20_Sn_RightTensionCylinderForward			:1;
		DWORD		In21_Sn_RightTensionCylinderBackward		:1;
		DWORD		In22_Sn_RightTensionStickDetection			:1;
		DWORD		In23_Sn_RightTensionPressure					:1;
		DWORD		In24_Reserve		:1;		
		DWORD		In25_Reserve		:1;		
		DWORD		In26_Reserve		:1;			
		DWORD		In27_Reserve		:1;			
		DWORD		In28_Reserve		:1;
		DWORD		In29_Reserve		:1;
		DWORD		In30_Reserve		:1;
		DWORD		In31_Reserve		:1;
	};
	DWORD	 shAL_IN;

	AL_D_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_D_IN_TAG));
	}
}ALIO_D_In;

typedef union AL_D_Out_TAG
{
	struct
	{  
		DWORD		Out00_LeftOpenGripper1						:1;	
		DWORD		Out01_LeftCloseGripper1						:1;
		DWORD		Out02_LeftOpenGripper2						:1;
		DWORD		Out03_LeftCloseGripper2						:1;
		DWORD		Out04_LeftOpenGripper3						:1;
		DWORD		Out05_LeftCloseGripper3						:1;
		DWORD		Out06_LeftOpenGripper4						:1;
		DWORD		Out07_LeftCloseGripper4						:1;
		DWORD		Out08_LeftTensionCylinderForward			:1;		
		DWORD		Out09_LeftTensionCylinderBackward		:1;		
		DWORD		Out10_RightOpenGripper1						:1;			
		DWORD		Out11_RightCloseGripper1						:1;			
		DWORD		Out12_RightOpenGripper2						:1;
		DWORD		Out13_RightCloseGripper2						:1;
		DWORD		Out14_RightOpenGripper3						:1;
		DWORD		Out15_RightCloseGripper3						:1;
		DWORD		Out16_RightOpenGripper4						:1;	
		DWORD		Out17_RightCloseGripper4						:1;
		DWORD		Out18_RightTensionCylinderForward			:1;
		DWORD		Out19_RightTensionCylinderBackward		:1;
		DWORD		Out20_Reserve		:1;
		DWORD		Out21_Reserve		:1;
		DWORD		Out22_Reserve		:1;
		DWORD		Out23_Reserve		:1;
		DWORD		Out24_Reserve		:1;		
		DWORD		Out25_Reserve		:1;		
		DWORD		Out26_Reserve		:1;			
		DWORD		Out27_Reserve		:1;			
		DWORD		Out28_Reserve		:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve		:1;
		DWORD		Out31_Reserve		:1;
	};
	DWORD	 shAL_Out;

	AL_D_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_D_Out_TAG));
	}
}ALIO_D_Out;

typedef union AL_E_IN_TAG
{
	struct
	{  
		DWORD		In00_Sn_PickerFrontStickVacuum			:1;	
		DWORD		In01_Sn_PickerLeftInputStickDetect		:1;
		DWORD		In02_Sn_PickerLeftInputPaperDetect		:1;
		DWORD		In03_Sn_PickerLeftOutputStickDetect		:1;
		DWORD		In04_Sn_PickerLeftOutputPaperDetect		:1;
		DWORD		In05_Sn_PickerRightInputStickDetect		:1;
		DWORD		In06_Sn_PickerRightInputPaperDetect		:1;
		DWORD		In07_Sn_PickerRightOutputStickDetect		:1;
		DWORD		In08_Sn_PickerRightOutputPaperDetect	:1;		
		DWORD		In09_Sn_PickerFrontPaperVacuum			:1;		
		DWORD		In10_Sn_PickerBackStickVacuum				:1;			
		DWORD		In11_Sn_PickerBackPaperVacuum			:1;			
		DWORD		In12_Sn_PickerZAxisClashPrevent			:1;
		DWORD		In13_Reserve		:1;
		DWORD		In14_Reserve		:1;
		DWORD		In15_Reserve		:1;

		DWORD		In16_Reserve		:1;	
		DWORD		In17_Reserve		:1;
		DWORD		In18_Reserve		:1;
		DWORD		In19_Reserve		:1;
		DWORD		In20_Reserve		:1;
		DWORD		In21_Reserve		:1;
		DWORD		In22_Reserve		:1;
		DWORD		In23_Reserve		:1;
		DWORD		In24_Reserve		:1;		
		DWORD		In25_Reserve		:1;		
		DWORD		In26_Reserve		:1;			
		DWORD		In27_Reserve		:1;			
		DWORD		In28_Reserve		:1;
		DWORD		In29_Reserve		:1;
		DWORD		In30_Reserve		:1;
		DWORD		In31_Reserve		:1;
	};
	DWORD	 shAL_IN;

	AL_E_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_E_IN_TAG));
	}
}ALIO_E_In;

typedef union AL_E_Out_TAG
{
	struct
	{  
		DWORD		Out00_PickerStickAbsorb01_100mm		:1;	
		DWORD		Out01_PickerStickAbsorb02_100mm		:1;
		DWORD		Out02_PickerStickAbsorb03_100mm		:1;
		DWORD		Out03_PickerStickAbsorb04_200mm		:1;
		DWORD		Out04_PickerStickAbsorb05_200mm		:1;
		DWORD		Out05_PickerStickAbsorb06_440mm		:1;
		DWORD		Out06_PickerStickAbsorb07_440mm		:1;
		DWORD		Out07_PickerStickAbsorb08_440mm		:1;
		DWORD		Out08_PickerStickAbsorb09_440mm		:1;		
		DWORD		Out09_PickerStickAbsorb10_440mm		:1;		
		DWORD		Out10_PickerStickPurge01_100mm		:1;			
		DWORD		Out11_PickerStickPurge02_100mm		:1;			
		DWORD		Out12_PickerStickPurge03_100mm		:1;
		DWORD		Out13_PickerStickPurge04_200mm		:1;
		DWORD		Out14_PickerStickPurge05_200mm		:1;
		DWORD		Out15_PickerStickPurge06_440mm		:1;
		DWORD		Out16_PickerStickPurge07_440mm		:1;	
		DWORD		Out17_PickerStickPurge08_440mm		:1;
		DWORD		Out18_PickerStickPurge09_440mm		:1;
		DWORD		Out19_PickerStickPurge10_440mm		:1;
		DWORD		Out20_Reserve		:1;
		DWORD		Out21_Reserve		:1;
		DWORD		Out22_Reserve		:1;
		DWORD		Out23_Reserve		:1;
		DWORD		Out24_Reserve		:1;		
		DWORD		Out25_Reserve		:1;		
		DWORD		Out26_Reserve		:1;			
		DWORD		Out27_Reserve		:1;			
		DWORD		Out28_Reserve		:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve		:1;
		DWORD		Out31_Reserve		:1;
	};
	DWORD	 shAL_Out;

	AL_E_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_E_Out_TAG));
	}
}ALIO_E_Out;

typedef union AL_F_IN_TAG
{
	struct
	{  
		DWORD		In00_Sn_PickerPaperLeftUp				:1;	
		DWORD		In01_Sn_PickerPaperLeftDown			:1;
		DWORD		In02_Sn_PickerPaperRightUp				:1;
		DWORD		In03_Sn_PickerPaperRightDown			:1;
		DWORD		In04_Sn_PickerPaperCenterUp				:1;
		DWORD		In05_Sn_PickerPaperCenterDown			:1;
		DWORD		In06_Reserve		:1;
		DWORD		In07_Reserve		:1;
		DWORD		In08_Reserve		:1;		
		DWORD		In09_Reserve		:1;		
		DWORD		In10_Reserve		:1;			
		DWORD		In11_Reserve		:1;			
		DWORD		In12_Reserve		:1;
		DWORD		In13_Reserve		:1;
		DWORD		In14_Reserve		:1;
		DWORD		In15_Reserve		:1;

		DWORD		In16_Reserve		:1;	
		DWORD		In17_Reserve		:1;
		DWORD		In18_Reserve		:1;
		DWORD		In19_Reserve		:1;
		DWORD		In20_Reserve		:1;
		DWORD		In21_Reserve		:1;
		DWORD		In22_Reserve		:1;
		DWORD		In23_Reserve		:1;
		DWORD		In24_Reserve		:1;		
		DWORD		In25_Reserve		:1;		
		DWORD		In26_Reserve		:1;			
		DWORD		In27_Reserve		:1;			
		DWORD		In28_Reserve		:1;
		DWORD		In29_Reserve		:1;
		DWORD		In30_Reserve		:1;
		DWORD		In31_Reserve		:1;
	};
	DWORD	 shAL_IN;

	AL_F_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_F_IN_TAG));
	}
}ALIO_F_In;

typedef union AL_F_Out_TAG
{
	struct
	{  
		DWORD		Out00_PickerPaperAbsorb01_100mm		:1;	
		DWORD		Out01_PickerPaperAbsorb02_100mm		:1;
		DWORD		Out02_PickerPaperAbsorb03_100mm		:1;
		DWORD		Out03_PickerPaperAbsorb04_200mm		:1;
		DWORD		Out04_PickerPaperAbsorb05_200mm		:1;
		DWORD		Out05_PickerPaperAbsorb06_440mm		:1;
		DWORD		Out06_PickerPaperAbsorb07_440mm		:1;
		DWORD		Out07_PickerPaperAbsorb08_440mm		:1;
		DWORD		Out08_PickerPaperAbsorb09_440mm		:1;		
		DWORD		Out09_PickerPaperAbsorb10_440mm		:1;		
		DWORD		Out10_PickerPaperPurge01_100mm		:1;			
		DWORD		Out11_PickerPaperPurge02_100mm		:1;			
		DWORD		Out12_PickerPaperPurge03_100mm		:1;
		DWORD		Out13_PickerPaperPurge04_200mm		:1;
		DWORD		Out14_PickerPaperPurge05_200mm		:1;
		DWORD		Out15_PickerPaperPurge06_440mm		:1;
		DWORD		Out16_PickerPaperPurge07_440mm		:1;	
		DWORD		Out17_PickerPaperPurge08_440mm		:1;
		DWORD		Out18_PickerPaperPurge09_440mm		:1;
		DWORD		Out19_PickerPaperPurge10_440mm		:1;
		DWORD		Out20_PickerPaperLeftRightUpSol		:1;
		DWORD		Out21_PickerPaperLeftRightDownSol		:1;
		DWORD		Out22_PickerPaperCenterUpSol			:1;
		DWORD		Out23_PickerPaperCenterDownSol		:1;


		DWORD		Out24_Reserve		:1;		
		DWORD		Out25_Reserve		:1;		
		DWORD		Out26_Reserve		:1;			
		DWORD		Out27_Reserve		:1;			
		DWORD		Out28_Reserve		:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve		:1;
		DWORD		Out31_Reserve		:1;
	};
	DWORD	 shAL_Out;

	AL_F_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_F_Out_TAG));
	}
}ALIO_F_Out;

typedef union AL_G_IN_TAG
{
	struct
	{  
		DWORD		In00_Sn_TurnTableUpperVacuum					:1;	
		DWORD		In01_Sn_TurnTableUpperLeftStickDetect		:1;
		DWORD		In02_Sn_TurnTableUpperRightStickDetect		:1;
		DWORD		In03_Sn_TurnTableLowerLeftStickDetect		:1;
		DWORD		In04_Sn_TurnTableLowerRightStickDetect		:1;
		DWORD		In05_Sn_TurnTableUpperPosition					:1;
		DWORD		In06_Sn_TurnTableLowerPosition					:1;
		DWORD		In07_Sn_TurnTableDownVacuum					:1;
		DWORD		In08_Reserve		:1;		
		DWORD		In09_Reserve		:1;		
		DWORD		In10_Sn_ClashPrevent_TurnTableAndCCD		:1;	//반전T과 하부CCD축 충돌방지 센서		
		DWORD		In11_Reserve		:1;			
		DWORD		In12_Reserve		:1;
		DWORD		In13_Reserve		:1;
		DWORD		In14_Reserve		:1;
		DWORD		In15_Reserve		:1;

		DWORD		In16_Reserve		:1;	
		DWORD		In17_Reserve		:1;
		DWORD		In18_Reserve		:1;
		DWORD		In19_Reserve		:1;
		DWORD		In20_Reserve		:1;
		DWORD		In21_Reserve		:1;
		DWORD		In22_Reserve		:1;
		DWORD		In23_Reserve		:1;
		DWORD		In24_Reserve		:1;		
		DWORD		In25_Reserve		:1;		
		DWORD		In26_Reserve		:1;			
		DWORD		In27_Reserve		:1;			
		DWORD		In28_Reserve		:1;
		DWORD		In29_Reserve		:1;
		DWORD		In30_Reserve		:1;
		DWORD		In31_Reserve		:1;
	};
	DWORD	 shAL_IN;

	AL_G_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_G_IN_TAG));
	}
}ALIO_G_In;

typedef union AL_G_Out_TAG
{
	struct
	{  
		DWORD		Out00_TurnTableUpperStickAbsorb01_100mm			:1;	
		DWORD		Out01_TurnTableUpperStickAbsorb02_100mm			:1;
		DWORD		Out02_TurnTableUpperStickAbsorb03_100mm			:1;
		DWORD		Out03_TurnTableUpperStickAbsorb04_200mm			:1;
		DWORD		Out04_TurnTableUpperStickAbsorb05_200mm			:1;
		DWORD		Out05_TurnTableUpperStickAbsorb06_440mm			:1;
		DWORD		Out06_TurnTableUpperStickAbsorb07_440mm			:1;
		DWORD		Out07_TurnTableUpperStickAbsorb08_440mm			:1;
		DWORD		Out08_TurnTableUpperStickAbsorb09_440mm			:1;		
		DWORD		Out09_TurnTableUpperStickAbsorb10_440mm			:1;		
		DWORD		Out10_TurnTableUpperStickPurge01_100mm				:1;			
		DWORD		Out11_TurnTableUpperStickPurge02_100mm				:1;			
		DWORD		Out12_TurnTableUpperStickPurge03_100mm				:1;
		DWORD		Out13_TurnTableUpperStickPurge04_200mm				:1;
		DWORD		Out14_TurnTableUpperStickPurge05_200mm				:1;
		DWORD		Out15_TurnTableUpperStickPurge06_440mm				:1;
		DWORD		Out16_TurnTableUpperStickPurge07_440mm				:1;	
		DWORD		Out17_TurnTableUpperStickPurge08_440mm				:1;
		DWORD		Out18_TurnTableUpperStickPurge09_440mm				:1;
		DWORD		Out19_TurnTableUpperStickPurge10_440mm				:1;
		DWORD		Out20_Reserve		:1;
		DWORD		Out21_Reserve		:1;
		DWORD		Out22_Reserve		:1;
		DWORD		Out23_Reserve		:1;
		DWORD		Out24_Reserve		:1;		
		DWORD		Out25_Reserve		:1;		
		DWORD		Out26_Reserve		:1;			
		DWORD		Out27_Reserve		:1;			
		DWORD		Out28_Reserve		:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve		:1;
		DWORD		Out31_Reserve		:1;
	};
	DWORD	 shAL_Out;

	AL_G_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_G_Out_TAG));
	}
}ALIO_G_Out;

typedef union AL_H_IN_TAG
{
	struct
	{  
		DWORD		In00_Reserve		:1;	
		DWORD		In01_Reserve		:1;
		DWORD		In02_Reserve		:1;
		DWORD		In03_Reserve		:1;
		DWORD		In04_Reserve		:1;
		DWORD		In05_Reserve		:1;
		DWORD		In06_Reserve		:1;
		DWORD		In07_Reserve		:1;
		DWORD		In08_Reserve		:1;		
		DWORD		In09_Reserve		:1;		
		DWORD		In10_Reserve		:1;			
		DWORD		In11_Reserve		:1;			
		DWORD		In12_Reserve		:1;
		DWORD		In13_Reserve		:1;
		DWORD		In14_Reserve		:1;
		DWORD		In15_Reserve		:1;

		DWORD		In16_Reserve		:1;	
		DWORD		In17_Reserve		:1;
		DWORD		In18_Reserve		:1;
		DWORD		In19_Reserve		:1;
		DWORD		In20_Reserve		:1;
		DWORD		In21_Reserve		:1;
		DWORD		In22_Reserve		:1;
		DWORD		In23_Reserve		:1;
		DWORD		In24_Reserve		:1;		
		DWORD		In25_Reserve		:1;		
		DWORD		In26_Reserve		:1;			
		DWORD		In27_Reserve		:1;			
		DWORD		In28_Reserve		:1;
		DWORD		In29_Reserve		:1;
		DWORD		In30_Reserve		:1;
		DWORD		In31_Reserve		:1;
	};
	DWORD	 shAL_IN;

	AL_H_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_H_IN_TAG));
	}
}ALIO_H_In;

typedef union AL_H_Out_TAG
{
	struct
	{  
		DWORD		Out00_TurnTableLowerStickAbsorb01_100mm			:1;	
		DWORD		Out01_TurnTableLowerStickAbsorb02_100mm			:1;
		DWORD		Out02_TurnTableLowerStickAbsorb03_100mm			:1;
		DWORD		Out03_TurnTableLowerStickAbsorb04_200mm			:1;
		DWORD		Out04_TurnTableLowerStickAbsorb05_200mm			:1;
		DWORD		Out05_TurnTableLowerStickAbsorb06_440mm			:1;
		DWORD		Out06_TurnTableLowerStickAbsorb07_440mm			:1;
		DWORD		Out07_TurnTableLowerStickAbsorb08_440mm			:1;
		DWORD		Out08_TurnTableLowerStickAbsorb09_440mm			:1;		
		DWORD		Out09_TurnTableLowerStickAbsorb10_440mm			:1;		
		DWORD		Out10_TurnTableLowerStickPurge01_100mm				:1;			
		DWORD		Out11_TurnTableLowerStickPurge02_100mm				:1;			
		DWORD		Out12_TurnTableLowerStickPurge03_100mm				:1;
		DWORD		Out13_TurnTableLowerStickPurge04_200mm				:1;
		DWORD		Out14_TurnTableLowerStickPurge05_200mm				:1;
		DWORD		Out15_TurnTableLowerStickPurge06_440mm				:1;
		DWORD		Out16_TurnTableLowerStickPurge07_440mm				:1;	
		DWORD		Out17_TurnTableLowerStickPurge08_440mm				:1;
		DWORD		Out18_TurnTableLowerStickPurge09_440mm				:1;
		DWORD		Out19_TurnTableLowerStickPurge10_440mm				:1;
		DWORD		Out20_Reserve		:1;
		DWORD		Out21_Reserve		:1;
		DWORD		Out22_Reserve		:1;
		DWORD		Out23_Reserve		:1;
		DWORD		Out24_Reserve		:1;		
		DWORD		Out25_Reserve		:1;		
		DWORD		Out26_Reserve		:1;			
		DWORD		Out27_Reserve		:1;			
		DWORD		Out28_Reserve		:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve		:1;
		DWORD		Out31_Reserve		:1;
	};
	DWORD	 shAL_Out;

	AL_H_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_H_Out_TAG));
	}
}ALIO_H_Out;

typedef union AL_I_IN_TAG
{
	struct
	{  
		DWORD		In00_Sn_OutputTableLeftStickDetect	:1;	
		DWORD		In01_Sn_OutputTableRightStickDetect	:1;
		DWORD		In02_Sn_OutputLTableCylinderUp		:1;
		DWORD		In03_Sn_OutputLTableCylinderDown		:1;
		DWORD		In04_Sn_OutputTableVacuum				:1;
		DWORD		In05_Sn_OutputRTableCylinderUp		:1;
		DWORD		In06_Sn_OutputRTableCylinderDown	:1;
		DWORD		In07_Reserve									:1;
		DWORD		In08_MC_On_Off								:1;	//MC Off ==> bit On 되면 Alarm	
		DWORD		In09_Sn_ForkStandbyPos					:1;		
		DWORD		In10_Sn_TurnTableRotateEnablePos		:1;			
		DWORD		In11_Sn_RingLightCylinderUp				:1;			
		DWORD		In12_Sn_RingLightCylinderDown			:1;
		DWORD		In13_Sn_ReviewCylinderUp					:1;
		DWORD		In14_Sn_ReviewCylinderDown				:1;
		DWORD		In15_Sn_MaskJigBWD						:1;

		DWORD		In16_Reserve		:1;	
		DWORD		In17_Reserve		:1;
		DWORD		In18_Reserve		:1;
		DWORD		In19_Reserve		:1;
		DWORD		In20_Reserve		:1;
		DWORD		In21_Reserve		:1;
		DWORD		In22_Reserve		:1;
		DWORD		In23_Reserve		:1;
		DWORD		In24_Reserve		:1;		
		DWORD		In25_Reserve		:1;		
		DWORD		In26_Reserve		:1;			
		DWORD		In27_Reserve		:1;			
		DWORD		In28_Reserve		:1;
		DWORD		In29_Reserve		:1;
		DWORD		In30_Reserve		:1;
		DWORD		In31_Reserve		:1;
	};
	DWORD	 shAL_IN;

	AL_I_IN_TAG()
	{		
		memset(this, 0x00, sizeof(AL_I_IN_TAG));
	}
}ALIO_I_In;

typedef union AL_I_Out_TAG
{
	struct
	{  
		DWORD		Out00_OutPutStickAbsorb01_100mm			:1;	
		DWORD		Out01_OutPutStickAbsorb02_100mm			:1;
		DWORD		Out02_OutPutStickAbsorb03_100mm			:1;
		DWORD		Out03_OutPutStickAbsorb04_200mm			:1;
		DWORD		Out04_OutPutStickAbsorb05_200mm			:1;
		DWORD		Out05_OutPutStickAbsorb06_440mm			:1;
		DWORD		Out06_OutPutStickAbsorb07_440mm			:1;
		DWORD		Out07_OutPutStickAbsorb08_440mm			:1;
		DWORD		Out08_OutPutStickAbsorb09_440mm			:1;		
		DWORD		Out09_OutPutStickAbsorb10_440mm			:1;		
		DWORD		Out10_OutPutStickPurge01_100mm				:1;			
		DWORD		Out11_OutPutStickPurge02_100mm				:1;			
		DWORD		Out12_OutPutStickPurge03_100mm				:1;
		DWORD		Out13_OutPutStickPurge04_200mm				:1;
		DWORD		Out14_OutPutStickPurge05_200mm				:1;
		DWORD		Out15_OutPutStickPurge06_440mm				:1;
		DWORD		Out16_OutPutStickPurge07_440mm				:1;	
		DWORD		Out17_OutPutStickPurge08_440mm				:1;
		DWORD		Out18_OutPutStickPurge09_440mm				:1;
		DWORD		Out19_OutPutStickPurge10_440mm				:1;
		DWORD		Out20_OutputTableCylinderUpSol					:1;
		DWORD		Out21_OutputTableCylinderDownSol				:1;
		DWORD		Out22_Reserve		:1;
		DWORD		Out23_Reserve		:1;
		DWORD		Out24_OutputRingLightCylinderUpSol			:1;		
		DWORD		Out25_OutputRingLightCylinderDownSol		:1;		
		DWORD		Out26_OutputReviewCylinderUpSol				:1;			
		DWORD		Out27_OutputReviewCylinderDownSol			:1;			
		DWORD		Out28_Output3DElectromagnetOn				:1;
		DWORD		Out29_Reserve		:1;
		DWORD		Out30_Reserve		:1;
		DWORD		Out31_Reserve		:1;
	};
	DWORD	 shAL_Out;

	AL_I_Out_TAG()
	{		
		memset(this, 0x00, sizeof(AL_I_Out_TAG));
	}
}ALIO_I_Out;