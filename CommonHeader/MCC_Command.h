#pragma once

#include "MCCDataStruct.h"
#include "Class\InterServerInterface.h"
#define TASK_61_MCCLog			61
#define		FUN_ID_LOG_61						61
#define		SEQ_ID_MCC_COMMAND					4
#define      MCC_Unit_Param					1
//////////////////////////////////////////////////////////////////////////
class CMCC_Command : public CInterServerInterface
{
	// 생성입니다.
public:
	BOOL m_fnMCC_WriteInfo(int nStringTable, CString strValue);
	BOOL m_fnMCC_WriteInfoEnd();

	// Action	
	BOOL m_fnMCC_ActionLog(int nIndex, BOOL bStartEnd, CString strStickID);

	// Signal
	BOOL m_fnMCC_SignalLog(int nIndex, BOOL bStartEnd, CString strStickID);

	// eVent
	BOOL m_fnMCC_EventLog_CompInOut(BOOL bInOut, CString strModuleID, CString strStickID, CString strFromPos, CString strToPos);	
	BOOL m_fnMCC_EventLog_ChangeProcessState(CString strModuleID, CString strStickID, CString strOldState, CString strNewState);	
	BOOL m_fnMCC_EventLog_ChangeState(CString strModuleID, CString strStickID, CString strOldState, CString strNewState, CString strAlarmID="", CString strAlarmText="");	
	BOOL m_fnMCC_EventLog_ChangePPID(CString strModuleID, CString strStickID, CString strOldPPID, CString strNewPPID);

	// Error, Warning
	BOOL m_fnMCC_EventLog_Error_WarningLog(BOOL bTrueisErrorFalseisWarning, BOOL bSetReset, CString strModuleID, CString strStickID, CString strAlarmID, CString strAlarmText); 	

	// Information       
 	BOOL m_fnMCC_InfoInputValue(int nIndex, float nfValue);

	void m_fnReadMCC_Param(int *pRefreshTime, int* pFileMakeTime, int* pWriteOnOff);
	void m_fnSendMCC_Parm(int RefreshTime, int FileMakeTime, int WriteOnOff);
};

