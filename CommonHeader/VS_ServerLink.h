#pragma once
//////////////////////////////////////////////////////////////////////////
//VS Server Link 32Bit 64Bit
//////////////////////////////////////////////////////////////////////////
#ifdef WIN64
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL64/clientsockdll64.lib")
#else
	#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL32/clientsockdll.lib")
#endif
#pragma warning(disable : 4995)
#pragma warning(disable : 4996)
#include ".\Class\VS64Exceptions.h"
#include ".\Class\InterServerInterface.h"
#include ".\Inspection\DefineStaticValue.h"
#include ".\ProcInitial.h"




//Project Task 정의 
#include ".\TaskDefine.h"

//Client DLL 에서 Callback 되어질 때 사용되는 이벤트
#define  WM_VS64USER_CALLBACK				 (WM_USER + 1000)