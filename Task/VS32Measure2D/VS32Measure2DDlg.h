
// VS32Measure2DDlg.h : 헤더 파일
//

#pragma once

typedef struct tag_3D_Scan_Recipe
{
	int		nScanCount; //1~n
	int		nTriggerStep; //um
	int		nScanDistance; //um
	int		nDummy; //Dummy


	tag_3D_Scan_Recipe()
	{
		memset(this, 0x00, sizeof(tag_3D_Scan_Recipe));
	}
}ST_SCAN_RECIPE;


// CVS32Measure2DDlg 대화 상자
class CVS32Measure2DDlg : public CDialogEx, public CProcInitial//VS64 Interface
{
// 생성입니다.
public:
	CVS32Measure2DDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~CVS32Measure2DDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VS90SAMPLE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	IplImage *m_pSampleView;
	HDC			m_HDCView;
	CRect			m_RectiewArea;
	void DrawImageView();
	ST_SCAN_RECIPE m_stScanRecipe;

public:
	CInterServerInterface  m_ServerInterface;
	void m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...);
	void Sys_fnInitVS64Interface();
	int				Sys_fnAnalyzeMsg(CMDMSG* cmdMsg);//Message Process Function.
	LRESULT		Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam);
};
