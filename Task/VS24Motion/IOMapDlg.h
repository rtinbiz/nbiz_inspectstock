#pragma once

#define IO_READ_TIME		101
// CIOMapDlg 대화 상자입니다.

class CIOMapDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CIOMapDlg)

public:
	CIOMapDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CIOMapDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_IO_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	void m_fnInit(/*CDioReadThread *pDioReadThread*/);
	void m_fnDeInit();	
	COLORREF m_BitOnColor;
	COLORREF m_BitOffColor;
	
	COLORREF m_BitBlockColor1;
	COLORREF m_BitBlockColor2;

	void InitCtrl_UI(void);
	CFont m_GridFont;
	CFont m_Font;
	/*CDioReadThread *m_pDioReadThread;*/
public:
	COLORREF m_btBackColor;
	virtual BOOL OnInitDialog();

	CGridCtrl m_GridInputList;
	CGridCtrl m_GridOutputList;

	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btnExit;

	CRoundButton2 m_btInputSelect;
	CRoundButton2 m_btOutputSelect;

	CColorStaticST m_stSelectIO;

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnIoInExit();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnTagGridClickOuput(NMHDR* pNMHDR, LRESULT* pResult);


	afx_msg void OnBnClickedBtnIoInSelect();
	afx_msg void OnBnClickedBtnIoOutSelect();
	virtual void PostNcDestroy();
};
