#pragma once

#include "Net232Port.h"

#define TENTION_GET_VALUE				"ID01P"//지령장비의 현재값 전송
#define TENTION_SET_HOLD				"ID01H"//지령장비의 HOLD 동작
#define TENTION_RESET_HOLD			"ID01R"//지령장비의 HOLD 해제
#define TENTION_SET_ZERO				"ID01Z"//지령장비의 현재값을 ZERO로 동작

class CTensionCtrl:public CNet232Port
{
public:
	CTensionCtrl(void);
	~CTensionCtrl(void);
public:
	LinkUpdate m_LinkUpdateObj;
	CString m_IPAddress;
	CString m_PortName;

	int m_CtrlIndex;
	HWND m_ParentUpdate;
	bool m_bStreamMode;

	float m_fTargetTention;
	float m_fInposValue;

	//int m_nReadJumpCnt;
	int WriteSVID_Data;

	DWORD m_CmdReadTimeCalBuf;
	DWORD m_CmdReadTime;
public:
	void SetEndChar(char EndChar = '\n', BYTE StartFlag = 0, int EndSize = 0);
	void SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj);
	bool ServerConnect();
	bool ServerDisconnect();
	bool OpenPort();
	void ClosePort();
	void static RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen);

	//////////////////////////////////////////////////////////////////////////
	bool LinkOpen();//Stream Mode일경우 연결과 동시에 Data가 들어 온다.

	//CMD Mode에서동작 할경우.
	bool ReadPos();
	bool SetZeroPos();
	bool SetStreamMod(bool OnOff);
	bool HoldValue(bool OnOff);


	bool SetTentionValue(float fTargetTention, float fInposValue);


	
};

