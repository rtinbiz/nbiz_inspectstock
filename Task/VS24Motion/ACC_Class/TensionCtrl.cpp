#include "StdAfx.h"
#include "TensionCtrl.h"

enum TentionsStepValue
{
	SPEED_1 = 1,
	SPEED_2 = 2,
	SPEED_3 = 10,
	SPEED_4 = 35,
	SPEED_5 = 100,
};

CTensionCtrl::CTensionCtrl(void)
{
	m_DataEndCheck.ASCII_EndFlag = '\n';
	m_DataEndCheck.B_StartFlag = 0;
	m_DataEndCheck.B_DataSize = 0;

	m_ParentUpdate = 0;
	m_CtrlIndex = 0;

	m_fTargetTention = 0.0f;
	m_fInposValue =  0.0f;

	m_bStreamMode = false;
	
	//m_nReadJumpCnt = 0;

	m_CmdReadTime = 100;//기본 Respons Time은 100ms으로 한다.
	m_CmdReadTimeCalBuf = m_CmdReadTime;

	WriteSVID_Data = 0;
}
CTensionCtrl::~CTensionCtrl(void)
{
}
void CTensionCtrl::SetEndChar(char EndChar, BYTE StartFlag, int EndSize)
{
	m_DataEndCheck.ASCII_EndFlag = EndChar;
	m_DataEndCheck.B_StartFlag = StartFlag;
	m_DataEndCheck.B_DataSize = EndSize;
}
void CTensionCtrl::SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj)
{
	m_LinkUpdateObj.pSPRServerTree = UpdateObj.pSPRServerTree;
	m_LinkUpdateObj.CtrlItem = UpdateObj.CtrlItem;
	m_LinkUpdateObj.IPItem = UpdateObj.IPItem;
	m_LinkUpdateObj.PortItem = UpdateObj.PortItem;
	m_IPAddress.Format("%s", IPAddress);
	m_PortName.Format("%s", PortName);
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 6,6);
}
bool CTensionCtrl::ServerConnect()
{
	if(m_isOpen == true)
		return true;

	bool bConnect  = false;
	bConnect = Net232Connect(inet_addr(m_IPAddress.GetBuffer(m_IPAddress.GetLength())), SPR_SERVER_PORT);
	if(bConnect == true)
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 6,6);
	else
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 7,7);
	
	return bConnect;
}
bool CTensionCtrl::ServerDisconnect()
{
	Net232DisConnect();
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 7,7);
	return true;
}
bool CTensionCtrl::OpenPort()
{
	if(fnPortOpen(m_PortName, &m_DataEndCheck,(LPVOID)this, RecvCallBack) != SPR_PORT_RESULT_OK)
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
		return false;
	}
	else
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 4,4);
	}
	return true;
}
void CTensionCtrl::ClosePort()
{
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
	fnPortClose();
}

void CTensionCtrl::RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen)
{
	CTensionCtrl *pCtrlObj = (CTensionCtrl *)CtrlObj;

	//ex) Stream Mode "ST,NT,+00000.0"
	//1) BYTE1, BYTE2 
	//. DATA 안정		: S T   	 . DATA 비안정	: U S 
	//. DATA OVERFLOW	: O L  		 . DATA UNDERFLOW: U L 
	//2) BYTE3 ~ BYTE6  	: 고정문자(, N T ,)
	//3) BYTE7 ~ BYTE14 	: DATA 8 BYTE(+/- 포함)

	//ex) CMD Mode "ID001,+00000.0"

	pCtrlObj->m_LinkUpdateObj.pSPRServerTree->SetItemImage(pCtrlObj->m_LinkUpdateObj.PortItem, 9,9);
	LPARAM pSendData;
	char RecvMSG[128];
	memset(RecvMSG, 0x00, 128);
	recvLen = (recvLen>127?127:recvLen);
	memcpy(RecvMSG, (char*)pBuf, recvLen);
	float fReadValue=0.0;
	if(RecvMSG[0]=='I')//ID Mode
	{
		fReadValue = (float)atof(&RecvMSG[7]);
		if(RecvMSG[6] =='-')
			fReadValue*=(-1);	

		//CMD Stream으로 구현 할경우.
		if(pCtrlObj->m_bStreamMode == true)
			pCtrlObj->ReadPos();
	}
	else//Stream Mode
	{
		fReadValue = (float)atof(&RecvMSG[7]);
		if(RecvMSG[6] =='-')
			fReadValue*=(-1);	
	}
	//////////////////////////////////////////////////////////////////////////
	//UMAC PLC 5번에 Data전송

	CString strSetData;
	float fSendValue= 0.0f;
	//부호를 같이 넘겨 준다.
	fSendValue = (fReadValue - pCtrlObj->m_fTargetTention);
	//범위 안에 들어오면 0으로 설정 한다.
	if(pCtrlObj->m_fInposValue>abs(fSendValue))
		fSendValue = 0.0f;
	if(abs(fReadValue)>3000.0f)
	{
		strSetData.Format("dis plc 4");
		G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
		strSetData.Format("dis plc 5");
		G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
		fSendValue = 0.0f;
		G_WriteInfo(Log_Error, "Tension Value Over!!!(Index %d)", pCtrlObj->m_CtrlIndex);
	}

	//////////////////////////////////////////////////////////////////////////
	memcpy(&pSendData, &fReadValue, sizeof(float));
	if(pCtrlObj->m_ParentUpdate != 0)
		::SendMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_LOADCELL_UPDATE, pCtrlObj->m_CtrlIndex, pSendData);

	//if(abs(fSendValue)<50.0f && pCtrlObj->m_nReadJumpCnt>0)
	{
		//pCtrlObj->m_nReadJumpCnt--;

		if(pCtrlObj->m_CtrlIndex>0 && pCtrlObj->m_CtrlIndex<=4)
		{
			strSetData.Format("%s%d", LEFT_LOADCELL_RESPONSE_TIME, 0);
			G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
		}
		else if(pCtrlObj->m_CtrlIndex>4 && pCtrlObj->m_CtrlIndex<=8)
		{
			strSetData.Format("%s%d", RIGHT_LOADCELL_RESPONSE_TIME, 0);
			G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
		}
		//return;

	}
	//pCtrlObj->m_nReadJumpCnt = (int)((50.0f-abs(fSendValue))*0.3f);
	//if(pCtrlObj->m_nReadJumpCnt<0)
	//	pCtrlObj->m_nReadJumpCnt =5;
	int MoveStep = SPEED_1;//부호 없는 Step값이다.

	int DistValue = (int)abs(fSendValue);

	if(DistValue<150)
		MoveStep = SPEED_1;
	else if(DistValue<300)
		MoveStep = SPEED_2;
	else if(DistValue<450)
		MoveStep = SPEED_3;
	else if(DistValue<600)
		MoveStep = SPEED_4;
	else 
		MoveStep = SPEED_5;


	switch(pCtrlObj->m_CtrlIndex)
	{
	case 1:
		{//LEFT_LOADCELL_VALUE1
			strSetData.Format("%s%d", LEFT_LOADCELL_STEP_VALUE1, MoveStep);

			CString strNewValue;
			if(pCtrlObj->WriteSVID_Data == 1)
			{
				strNewValue.Format("%d", (int)fSendValue);
				WritePrivateProfileString(Section_SVID, Key_STICK_TENSION_GRIP, strNewValue, SVID_LIST_PATH);
			}
			else if(pCtrlObj->WriteSVID_Data == 2)
			{
				strNewValue.Format("%d", (int)fSendValue);
				WritePrivateProfileString(Section_SVID, Key_STICK_TENSION_FINE, strNewValue, SVID_LIST_PATH);
			}
		}break;
	case 2:
		{//LEFT_LOADCELL_VALUE2
			strSetData.Format("%s%d", LEFT_LOADCELL_STEP_VALUE2, MoveStep);
		}break;
	case 3:
		{//LEFT_LOADCELL_VALUE3
			strSetData.Format("%s%d", LEFT_LOADCELL_STEP_VALUE3, MoveStep);
		}break;
	case 4:
		{//LEFT_LOADCELL_VALUE4
			strSetData.Format("%s%d", LEFT_LOADCELL_STEP_VALUE4, MoveStep);
		}break;
	case 5:
		{//RIGHT_LOADCELL_VALUE1
			strSetData.Format("%s%d", RIGHT_LOADCELL_STEP_VALUE1, MoveStep);
		}break;
	case 6:
		{//RIGHT_LOADCELL_VALUE2
			strSetData.Format("%s%d", RIGHT_LOADCELL_STEP_VALUE2, MoveStep);
		}break;
	case 7:
		{//RIGHT_LOADCELL_VALUE3
			strSetData.Format("%s%d", RIGHT_LOADCELL_STEP_VALUE3, MoveStep);
		}break;
	case 8:
		{//RIGHT_LOADCELL_VALUE4
			strSetData.Format("%s%d", RIGHT_LOADCELL_STEP_VALUE4, MoveStep);
		}break;
	}
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	switch(pCtrlObj->m_CtrlIndex)
	{
	case 1:
		{//LEFT_LOADCELL_VALUE1
			strSetData.Format("%s%f", LEFT_LOADCELL_VALUE1, fSendValue);
		}break;
	case 2:
		{//LEFT_LOADCELL_VALUE2
			strSetData.Format("%s%f", LEFT_LOADCELL_VALUE2, fSendValue);
		}break;
	case 3:
		{//LEFT_LOADCELL_VALUE3
			strSetData.Format("%s%f", LEFT_LOADCELL_VALUE3, fSendValue);
		}break;
	case 4:
		{//LEFT_LOADCELL_VALUE4
			strSetData.Format("%s%f", LEFT_LOADCELL_VALUE4, fSendValue);
		}break;
	case 5:
		{//RIGHT_LOADCELL_VALUE1
			strSetData.Format("%s%f", RIGHT_LOADCELL_VALUE1, fSendValue);
		}break;
	case 6:
		{//RIGHT_LOADCELL_VALUE2
			strSetData.Format("%s%f", RIGHT_LOADCELL_VALUE2, fSendValue);
		}break;
	case 7:
		{//RIGHT_LOADCELL_VALUE3
			strSetData.Format("%s%f", RIGHT_LOADCELL_VALUE3, fSendValue);
		}break;
	case 8:
		{//RIGHT_LOADCELL_VALUE4
			strSetData.Format("%s%f", RIGHT_LOADCELL_VALUE4, fSendValue);
		}break;
	}
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);

	pCtrlObj->m_CmdReadTime = GetTickCount() - pCtrlObj->m_CmdReadTimeCalBuf;
	if(pCtrlObj->m_CmdReadTime <=0)
		pCtrlObj->m_CmdReadTime = 1;

	if(pCtrlObj->m_CtrlIndex>0 && pCtrlObj->m_CtrlIndex<=4)
	{
		strSetData.Format("%s%d", LEFT_LOADCELL_RESPONSE_TIME, pCtrlObj->m_CmdReadTime);
		G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	}
	else if(pCtrlObj->m_CtrlIndex>4 && pCtrlObj->m_CtrlIndex<=8)
	{
		strSetData.Format("%s%d", RIGHT_LOADCELL_RESPONSE_TIME, pCtrlObj->m_CmdReadTime);
		G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	}
	//else
	//{
	//	strSetData.Format("%s%d", LEFT_LOADCELL_RESPONSE_TIME, pCtrlObj->m_CmdReadTime);
	//	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	//	strSetData.Format("%s%d", RIGHT_LOADCELL_RESPONSE_TIME, pCtrlObj->m_CmdReadTime);
	//	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	//}

	
	


	
}

//////////////////////////////////////////////////////////////////////////

bool CTensionCtrl::LinkOpen()
{
	if(ServerConnect() == true)
	{
		if(OpenPort()== false)
			return false;
	}
	else
		return false;

	return true;
}
bool CTensionCtrl::ReadPos()
{
	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r\n", TENTION_GET_VALUE);

	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);
	m_CmdReadTimeCalBuf = GetTickCount();
	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
		return true;
	return false;
}
bool CTensionCtrl::SetZeroPos()
{

	SetStreamMod(false);
	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r\n", TENTION_SET_ZERO);

	//m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);

	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
		return true;
	return false;
}
bool CTensionCtrl::SetStreamMod(bool OnOff)
{

	if(OnOff == true && m_bStreamMode == false)
	{//Tention 시작시.
		WriteSVID_Data = 1;
	}
	if(OnOff == false && m_bStreamMode == true)
	{//Tention 끝날때

		WriteSVID_Data = 2;
		if(m_isOpen == true)
			ReadPos();
	}

	m_bStreamMode = OnOff;
	if(m_isOpen == true&& OnOff== true)
		ReadPos();
	return m_bStreamMode;
}
bool CTensionCtrl::HoldValue(bool OnOff)
{
	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r\n", (OnOff?TENTION_SET_HOLD:TENTION_RESET_HOLD));
	
	//m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);

	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
		return true;
	return false;
}

bool CTensionCtrl::SetTentionValue(float fTargetTention, float fInposValue)
{
	m_fTargetTention = fTargetTention;
	if(fInposValue<0.0f)//기본값 투입.
		fInposValue=3.0f;
	m_fInposValue = abs(fInposValue);//0보다 큰값이 들어와야 한다.

	return true;
}