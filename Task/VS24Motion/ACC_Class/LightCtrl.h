#pragma once

#include "Net232Port.h"
enum LIGHT_CH
{
	LIGHT_CH_None = 0,
	LIGHT_CH_1 = 1,
	LIGHT_CH_2 = 2,
	LIGHT_CH_3,
	LIGHT_CH_4,
};
class CLightCtrl :public CNet232Port
{
public:
	CLightCtrl(void);
	~CLightCtrl(void);
public:
	LinkUpdate m_LinkUpdateObj;
	CString m_IPAddress;
	CString m_PortName;

	BYTE m_LightValue[4];

	int m_CtrlIndex;
	int m_SelectCh;
	HWND m_ParentUpdate;
public:
	void SetEndChar(char EndChar = '\n', BYTE StartFlag = 0, int EndSize = 0);
	void SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj);
	bool ServerConnect();
	bool ServerDisconnect();
	bool OpenPort();
	void ClosePort();
	void static RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen);

	//////////////////////////////////////////////////////////////////////////
	bool LinkOpen();
	bool SetLightValue(LIGHT_CH ChIndex, int nValue);
	void GetLightValue(LIGHT_CH ChIndex);
};

