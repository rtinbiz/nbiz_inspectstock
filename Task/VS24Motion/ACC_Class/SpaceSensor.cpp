#include "StdAfx.h"
#include "SpaceSensor.h"

CSpaceSensor::CSpaceSensor(void)
{
	m_DataEndCheck.ASCII_EndFlag = '\r';// '\n';
	m_DataEndCheck.B_StartFlag = 0;
	m_DataEndCheck.B_DataSize = 0;
	m_NowValue[0] = 0;//Air
	m_NowValue[1] = 0;//N2
	m_ParentUpdate = 0;
	m_bStreamMode = false;


	m_fTargetDisOut1 = 0.0f;
	m_fTargetDisOut2 = 0.0f;
	m_fInposValue = 0.0f;

	m_CmdReadTime = 100;//기본 Respons Time은 100ms으로 한다.
	m_CmdReadTimeCalBuf = m_CmdReadTime;
}


CSpaceSensor::~CSpaceSensor(void)
{
}
void CSpaceSensor::SetEndChar(char EndChar, BYTE StartFlag, int EndSize)
{
	m_DataEndCheck.ASCII_EndFlag = EndChar;
	m_DataEndCheck.B_StartFlag = StartFlag;
	m_DataEndCheck.B_DataSize = EndSize;
}
void CSpaceSensor::SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj)
{
	m_LinkUpdateObj.pSPRServerTree = UpdateObj.pSPRServerTree;
	m_LinkUpdateObj.CtrlItem = UpdateObj.CtrlItem;
	m_LinkUpdateObj.IPItem = UpdateObj.IPItem;
	m_LinkUpdateObj.PortItem = UpdateObj.PortItem;
	m_IPAddress.Format("%s", IPAddress);
	m_PortName.Format("%s", PortName);
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 6,6);
}
bool CSpaceSensor::ServerConnect()
{
	if(m_isOpen == true)
		return true;

	bool bConnect  = false;
	bConnect = Net232Connect(inet_addr(m_IPAddress.GetBuffer(m_IPAddress.GetLength())), SPR_SERVER_PORT);
	if(bConnect == true)
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 6,6);
	else
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 7,7);

	return bConnect;
}
bool CSpaceSensor::ServerDisconnect()
{
	Net232DisConnect();
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 7,7);
	return true;
}
bool CSpaceSensor::OpenPort()
{
	if(fnPortOpen(m_PortName, &m_DataEndCheck,(LPVOID)this, RecvCallBack) != SPR_PORT_RESULT_OK)
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
		return false;
	}
	else
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 4,4);
	}
	return true;
}
void CSpaceSensor::ClosePort()
{
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
	fnPortClose();
}
void CSpaceSensor::RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen)
{
	CSpaceSensor *pCtrlObj = (CSpaceSensor *)CtrlObj;

	WPARAM pSendOut1Data;
	LPARAM  pSendOut2Data;
	char RecvMSG[128];
	memset(RecvMSG, 0x00, 128);
	recvLen = (recvLen>127?127:recvLen);
	memcpy(RecvMSG, (char*)pBuf, recvLen);
	float fReadOut1Value=0.0;
	float fReadOut2Value=0.0;
	if(RecvMSG[0]=='M' &&RecvMSG[1] == '0')
	{
		CString strTData;
		strTData.Format("%s", RecvMSG);

		CStringArray strReadValues;
		CString resToken; 
		int curPos= 0; 
		resToken= strTData.Tokenize(",",curPos); 
		while (resToken != "")
		{ 
			strReadValues.Add(resToken);
			resToken= strTData.Tokenize(",",curPos); 
		}

		fReadOut1Value = (float)atof(strReadValues.GetAt(1));
		fReadOut2Value = (float)atof(strReadValues.GetAt(2));
		
		memcpy(&pSendOut1Data, &fReadOut1Value, sizeof(float));
		memcpy(&pSendOut2Data, &fReadOut2Value, sizeof(float));

		pCtrlObj->m_NowValue[0] = fReadOut1Value;//Out 1
		pCtrlObj->m_NowValue[1] = fReadOut2Value;//Out 2

		CString strSetData;
		float fSendValueOut1= 0.0f;
		float fSendValueOut2= 0.0f;
		//부호를 같이 넘겨 준다.
		fSendValueOut1 = (pCtrlObj->m_NowValue[0] - pCtrlObj->m_fTargetDisOut1);
		fSendValueOut2 = (pCtrlObj->m_NowValue[1] - pCtrlObj->m_fTargetDisOut2);
		//범위 안에 들어오면 0으로 설정 한다.
		if(pCtrlObj->m_fInposValue>abs(fSendValueOut1))
			fSendValueOut1 = 0.0f;
		strSetData.Format("%s%f", SPACE_SENSER_VALUE1, fSendValueOut1);
		G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);

		if(pCtrlObj->m_fInposValue>abs(fSendValueOut2))
			fSendValueOut2= 0.0f;
		strSetData.Format("%s%f", SPACE_SENSER_VALUE2, fSendValueOut2);
		G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
		
		pCtrlObj->m_CmdReadTime = GetTickCount() - pCtrlObj->m_CmdReadTimeCalBuf;
		strSetData.Format("%s%d", SPACE_SENSER_RESPONSE_TIME, pCtrlObj->m_CmdReadTime);
		G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);

		if(pCtrlObj->m_ParentUpdate != 0)
			::SendMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_SpaceSensor_UPDATE, pSendOut1Data, pSendOut2Data);

		strReadValues.RemoveAll();
		//CMD StreamA¸·I ±¸Co CO°æ¿i.
		if(pCtrlObj->m_bStreamMode == true)
			pCtrlObj->ReadOutAll();
	}
	
}
//////////////////////////////////////////////////////////////////////////
bool CSpaceSensor::LinkOpen()
{
	if(ServerConnect() == true)
	{
		if(OpenPort()== false)
			return false;
	}
	else
		return false;

	return true;
}
bool CSpaceSensor::ReadOutAll()
{
	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r", SPSensor_ReadOutAll);

	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);

	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
		return true;
	return false;
}
bool CSpaceSensor::SetStreamMod(bool OnOff)
{


	m_bStreamMode = OnOff;
	//Measure Laser On
	G_MotCtrlObj.m_fnSpaceSensorLaserOnOff(m_bStreamMode);
	Sleep(250);

	if(m_isOpen == true&& OnOff== true)
		ReadOutAll();
	return m_bStreamMode;
}
bool CSpaceSensor::SetAutoZeroOnOff(bool OnOff)
{
	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r", (OnOff==true?SPSensor_AutoZeroOnAll:SPSensor_AutoZeroOffAll));

	//m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);

	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
		return true;
	return false;
}
bool CSpaceSensor::SetMeasureOnOff(bool OnOff)
{

	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r", (OnOff==true?SPSensor_MeasurOn:SPSensor_MeasurOff));

	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
		return true;
	return false;
}
bool CSpaceSensor::SetTargetSpaceValue(float fTargetSpace, float fInposValue)
{
	m_fTargetDisOut1 = fTargetSpace;
	m_fTargetDisOut2 = fTargetSpace;
	if(fInposValue<0.0f)//기본값 투입.
		fInposValue=3.0f;
	m_fInposValue = abs(fInposValue);//0보다 큰값이 들어와야 한다.

	return true;
}