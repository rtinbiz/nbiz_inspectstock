#pragma once

#include "Net232Port.h"
class CAirPressCtrl :public CNet232Port
{
public:
	CAirPressCtrl(void);
	~CAirPressCtrl(void);
public:
	LinkUpdate m_LinkUpdateObj;
	CString m_IPAddress;
	CString m_PortName;
	int m_NowValue[2];
	HWND m_ParentUpdate;
public:
	void SetEndChar(char EndChar = '\n', BYTE StartFlag = 0, int EndSize = 0);
	void SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj);
	bool ServerConnect();
	bool ServerDisconnect();
	bool OpenPort();
	void ClosePort();
	void static RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen);

	//////////////////////////////////////////////////////////////////////////
	bool LinkOpen();
	bool ReadOutAll();
};

