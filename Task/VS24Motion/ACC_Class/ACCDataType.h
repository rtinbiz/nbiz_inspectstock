#pragma once
#include "../../../CommonHeader/SPRDataType.h"
#define  SPR_SERVER_PORT 5100
//////////////////////////////////////////////////////////////////////////
#define SYSTEM_PARAM_INI_PATH "D:\\nBiz_InspectStock\\Data\\Task_INI\\VS33ACC_Sys_Param.ini"
#define  Section_SYS_PORT		"SYS_SERVER_PORT"
#define  Key_LIGHT_CTRL_SCAN				"LIGHT_CTRL_SCAN"
#define  Key_LIGHT_CTRL_ALIGN				"LIGHT_CTRL_ALIGN"

#define  Key_TENSION_CELL1				"TENSION_CELL1"
#define  Key_TENSION_CELL2				"TENSION_CELL2"
#define  Key_TENSION_CELL3				"TENSION_CELL3"
#define  Key_TENSION_CELL4				"TENSION_CELL4"

#define  Key_TENSION_CELL5				"TENSION_CELL5"
#define  Key_TENSION_CELL6				"TENSION_CELL6"
#define  Key_TENSION_CELL7				"TENSION_CELL7"
#define  Key_TENSION_CELL8				"TENSION_CELL8"

#define  Key_QR_READER					"QR_READER"
#define  Key_BCR_READER					"BCR_READER"
#define  Key_AIR_PRESS					"AIR_PRESS"

#define  Key_3D_REVOLVER				"3D_REVOLVER"

#define  Key_SPACE_SENSOR				"SPACE_SENSOR"
//////////////////////////////////////////////////////////////////////////
//ACC Light Value
#define  Section_LightValue_Scan		"LightValue_Scan"
#define  Key_LV_ch1_ScanRing			"ch1_ScanRing"
#define  Key_LV_ch2_ScanBack			"ch2_ScanBack"
#define  Key_LV_ch3_ScanReflect		"ch3_ScanReflect"
#define  Key_LV_ch4_Reserve			"ch4_Reserve"

#define  Section_LightValue_Align		"LightValue_Align"
#define  Key_LV_ch1_Align1_1		"ch1_Align1_1"
#define  Key_LV_ch2_Align1_2		"ch2_Align1_2"
#define  Key_LV_ch3_Align2_1		"ch3_Align2_1"
#define  Key_LV_ch4_Align2_2		"ch4_Align2_2"



