#pragma once

#include "./ACCDataType.h"

typedef struct LinkStateUpdate_TAG
{
	CTreeCtrl *pSPRServerTree;
	HTREEITEM CtrlItem;
	HTREEITEM IPItem;
	HTREEITEM PortItem;

	LinkStateUpdate_TAG()
	{
		pSPRServerTree = nullptr;
		CtrlItem = 0;
		IPItem = 0;
		PortItem = 0;
	}
}LinkUpdate;

typedef void (*pReceiveFunction)(LPVOID, LPVOID, DWORD);

class CNet232Port
{
public:
	CNet232Port();
	~CNet232Port(void);
public:
	SOCKET m_Net232Sockfd; 
	char m_Net232PortList[1024];
	char m_NowOpenPort[30];
	bool m_isOpen;
private:
	double m_SendDelay;
public:
	bool		m_RunThread;
	CWinThread	*m_pHandleThread;

	int m_CMDListReq;
	int m_CMDOpen;
	int m_CMDClose;
	//int m_CMDSend;

	int m_CMDListReqRet;
	int m_CMDOpenRet;
	int m_CMDCloseRet;
	//int m_CMDSendRet;
public:
	RecvCheck m_DataEndCheck;

	LPVOID m_CallObj;
	pReceiveFunction m_pfnRecvData;
	pReceiveFunction m_pfnRecvList;
	//void static RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen);

	bool Net232Connect(DWORD	 ServerIPAdd, WORD	ServerPort);
	void Net232DisConnect();
public:
	int fnPortListUpDate(LPVOID CallObj, pReceiveFunction pfnRecvList);
	int fnPortOpen(CString strPort,  RecvCheck *pRCheck, LPVOID CallObj, pReceiveFunction m_pfnRecvData, DWORD nRate = B9600, BYTE nParity = RS_PARITY_NONE, BYTE nSize = 8, BYTE nStop= RS_STOP1);
	int fnPortClose();
	int fnDataSend(int DSize, BYTE *pDBuf);
};

UINT Thread_Net232CMDProc(LPVOID pParam);