#pragma once

#include "Net232Port.h"


#define SPSensor_ReadOutAll					"M0,0"// Out1, Out2의 측정치 Read
#define SPSensor_ReadOutAll_P				"M0,1"// Out1, Out2의 측정치 + 판정치 Read
//추가 내용은 확인 바람
#define SPSensor_AutoZeroOnAll					"V0"// Out1, Out2에 Auto Zero를 설정 한다.
#define SPSensor_AutoZeroOffAll				"W0"// Out1, Out2에 Auto Zero를 해지 한다.


#define SPSensor_MeasurOn				"11"// 
#define SPSensor_MeasurOff				"00"// 
class CSpaceSensor:public CNet232Port
{
public:
	CSpaceSensor(void);
	~CSpaceSensor(void);
public:
	LinkUpdate m_LinkUpdateObj;
	CString m_IPAddress;
	CString m_PortName;
	float m_NowValue[2];
	HWND m_ParentUpdate;
	bool m_bStreamMode;

	float m_fTargetDisOut1;
	float m_fTargetDisOut2;
	float m_fInposValue;

	DWORD m_CmdReadTimeCalBuf;
	DWORD m_CmdReadTime;
public:
	void SetEndChar(char EndChar = '\n', BYTE StartFlag = 0, int EndSize = 0);
	void SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj);
	bool ServerConnect();
	bool ServerDisconnect();
	bool OpenPort();
	void ClosePort();
	void static RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen);

	//////////////////////////////////////////////////////////////////////////
	bool LinkOpen();
	bool ReadOutAll();
	bool SetStreamMod(bool OnOff);
	bool SetAutoZeroOnOff(bool OnOff);
	bool SetMeasureOnOff(bool OnOff);
	bool SetTargetSpaceValue(float fTargetSpace, float fInposValue);

};

