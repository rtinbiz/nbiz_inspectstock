#include "StdAfx.h"
#include "LightCtrl.h"
//CR = 캐리지 리턴 = 보통 C#에서 '\r'
//LF = 라인 피드 = 보통 C#에서  '\n'

CLightCtrl::CLightCtrl(void)
{
	m_DataEndCheck.ASCII_EndFlag = 0x0A;
	m_DataEndCheck.B_StartFlag = 0;
	m_DataEndCheck.B_DataSize = 0;

	m_LightValue[0] = 0;
	m_LightValue[1] = 0;
	m_LightValue[2] = 0;
	m_LightValue[3] = 0;

	m_CtrlIndex = -1;
	m_ParentUpdate = 0;
	m_SelectCh = 0;

}


CLightCtrl::~CLightCtrl(void)
{
}
void CLightCtrl::SetEndChar(char EndChar, BYTE StartFlag, int EndSize)
{
	m_DataEndCheck.ASCII_EndFlag = EndChar;
	m_DataEndCheck.B_StartFlag = StartFlag;
	m_DataEndCheck.B_DataSize = EndSize;
}
void CLightCtrl::SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj)
{
	m_LinkUpdateObj.pSPRServerTree = UpdateObj.pSPRServerTree;
	m_LinkUpdateObj.CtrlItem = UpdateObj.CtrlItem;
	m_LinkUpdateObj.IPItem = UpdateObj.IPItem;
	m_LinkUpdateObj.PortItem = UpdateObj.PortItem;

	m_IPAddress.Format("%s", IPAddress);
	m_PortName.Format("%s", PortName);
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 6,6);
}
bool CLightCtrl::ServerConnect()
{
	if(m_isOpen == true)
		return true;

	bool bConnect  = false;
	bConnect = Net232Connect(inet_addr(m_IPAddress.GetBuffer(m_IPAddress.GetLength())), SPR_SERVER_PORT);
	if(bConnect == true)
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 6,6);
	else
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 7,7);

	return bConnect;
}
bool CLightCtrl::ServerDisconnect()
{
	Net232DisConnect();
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 7,7);
	return true;
}
bool CLightCtrl::OpenPort()
{
	if(fnPortOpen(m_PortName, &m_DataEndCheck,(LPVOID)this, RecvCallBack) != SPR_PORT_RESULT_OK)
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
		return false;
	}
	else
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 4,4);
	}
	return true;
}
void CLightCtrl::ClosePort()
{
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
	fnPortClose();
}

void CLightCtrl::RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen)
{
	//CLightCtrl*pCtrlObj = (CLightCtrl*)CtrlObj;
	//pCtrlObj->m_LinkUpdateObj.pSPRServerTree->SetItemImage(pCtrlObj->m_LinkUpdateObj.PortItem, 9,9);
	//char RecvMSG[128];
	//memset(RecvMSG, 0x00, 128);
	//recvLen = (recvLen>127?127:recvLen);
	//memcpy(RecvMSG, (char*)pBuf, recvLen);
	//int LightValue = atoi(RecvMSG);

	//pCtrlObj->m_LightValue[pCtrlObj->m_SelectCh-1] = LightValue;

	//G_WriteInfo(Log_Check, "Light Setvalue Ret(%d)==>%d", pCtrlObj->m_SelectCh, LightValue);
	//if(pCtrlObj->m_ParentUpdate != 0)
	//{
	//	if(pCtrlObj->m_CtrlIndex == 0)
	//		//::SendMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_LIGHT_UPDATE, pCtrlObj->m_SelectCh+10, LightValue);
	//		::PostMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_LIGHT_UPDATE, pCtrlObj->m_SelectCh+10, LightValue);

	//	if(pCtrlObj->m_CtrlIndex == 1)
	//		//::SendMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_LIGHT_UPDATE, pCtrlObj->m_SelectCh+20, LightValue);
	//		::PostMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_LIGHT_UPDATE, pCtrlObj->m_SelectCh+20, LightValue);
	//}
}

//////////////////////////////////////////////////////////////////////////
bool CLightCtrl::LinkOpen()
{
	if(ServerConnect() == true)
	{
		if(OpenPort()== false)
			return false;
	}
	else
		return false;

	return true;
}
bool CLightCtrl::SetLightValue(LIGHT_CH ChIndex, int nValue)
{
	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "L%d%03d\r\n", ChIndex, nValue);
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);
	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
	{
		//Sleep(300);
		//GetLightValue(ChIndex);

		m_LightValue[ChIndex-1] = nValue;
		if(m_CtrlIndex == 0)
			::PostMessage(m_ParentUpdate, WM_USER_ACC_LIGHT_UPDATE, m_SelectCh+10, nValue);
		else
			::PostMessage(m_ParentUpdate, WM_USER_ACC_LIGHT_UPDATE, m_SelectCh+20, nValue);
		return true;
	}
	return false;
}
void CLightCtrl::GetLightValue(LIGHT_CH ChIndex)
{
	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	//sprintf_s(SendMSG, ":R%d$0D$0A\r\n", ChIndex);
	sprintf_s(SendMSG, "R%d\r\n", ChIndex);
	m_SelectCh = ChIndex;
	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
		return;
}