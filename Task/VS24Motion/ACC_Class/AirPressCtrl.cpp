#include "StdAfx.h"
#include "AirPressCtrl.h"
//모듈 셋팅(통신요)
//속도 9600, 7bit, None, stop 1
//Type Ascii 모드 6번
//C30=> 1(Air), 2(N2) 국번 설정
//C31=> 02 ==>9600
//C32=> 06 ==>ASCII 통신 모드
//C33=> 01 ==>ASCII 모드
//////////////////////////////////////////////////////////////////////////
//RTC Type
//010304b8000504dc
//020304b8000504ef
//
//ASCII Type
//Send => :010307D1000222
//Recv => :01030400000000F8
//
//04B8==> Instantaneous flow rate (1028)
//Send => :010304B800053B
//Recv => :0183027A ==>Address Error
//Send => :020304B800053A
//Recv => :02830279 ==>Address Error
//
//: 01 83 02 7A
//
//
//Send => :0103057A000578
//Recv => :01030A000000000000014100C0F0
//Send => :0203057A000577
//Recv => :02030A0000000000000051004060
//Recv => :02030A0029000000000051004037
//Recv => :02030A000D00000000005100C0D3 ==>유량 13
//
//:02030A0010 00000000005100C0D0  ==>유량 16 양수
//:02030A0010 00000000005100C0D0  ==>유량 16 음수
//////////////////////////////////////////////////////////////////////////


//void hex_to_ascii(unsigned char* buf, unsigned char number)
//{
//	int i;
//	sprintf(  buf, "%03d", number );
//	printf("\nNumber in Ascii: ");
//	for( i=0; i< 3; i++)
//		printf("%#04x ", *(buf+i));
//	printf("\n");
//	return;
//}
void ascii_to_hex(char *in, size_t len, BYTE *out)
{
	unsigned int i, t, hn, ln;
	for (t = 0,i = 0; i < len; i+=2,++t) 
	{
		hn = in[i] > '9' ? (in[i]|32) - 'a' + 10 : in[i] - '0';
		ln = in[i+1] > '9' ? (in[i+1]|32) - 'a' + 10 : in[i+1] - '0';
		out[t] = (hn << 4 ) | ln;
		//printf("%s",out[t]);
	}
}
CAirPressCtrl::CAirPressCtrl(void)
{
	m_DataEndCheck.ASCII_EndFlag = '\n';
	m_DataEndCheck.B_StartFlag = 0;
	m_DataEndCheck.B_DataSize = 0;

	m_NowValue[0] = 0;//Air
	m_NowValue[1] = 0;//N2
	m_ParentUpdate = 0;
}
unsigned short CRC_16Modbus(int bufLen, BYTE *pbuf)
{
	unsigned short dt_16= 0;
	unsigned short next = 0;
	unsigned short carry= 0;
	
	dt_16 = 0xFFFF;
	while(bufLen>0)
	{
		next =(unsigned short)*pbuf;
		dt_16^=next;
		for(int i= 0; i<8;i++)
		{
			carry = (unsigned short)(dt_16&0x0001);
			dt_16>>=1;
			if(carry != 0)
				dt_16^=0xA001;
		}
		pbuf++;
		bufLen--;
	}
	return dt_16;
}

BYTE LRC_8Modbus(int wLength, BYTE *nData)
{
	int nLRC = 0 ;
	while (wLength--)
		nLRC += *nData++;
	return (BYTE)(-nLRC);
}

CAirPressCtrl::~CAirPressCtrl(void)
{
}
void CAirPressCtrl::SetEndChar(char EndChar, BYTE StartFlag, int EndSize)
{
	m_DataEndCheck.ASCII_EndFlag = EndChar;
	m_DataEndCheck.B_StartFlag = StartFlag;
	m_DataEndCheck.B_DataSize = EndSize;
}
void CAirPressCtrl::SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj)
{
	m_LinkUpdateObj.pSPRServerTree = UpdateObj.pSPRServerTree;
	m_LinkUpdateObj.CtrlItem = UpdateObj.CtrlItem;
	m_LinkUpdateObj.IPItem = UpdateObj.IPItem;
	m_LinkUpdateObj.PortItem = UpdateObj.PortItem;
	m_IPAddress.Format("%s", IPAddress);
	m_PortName.Format("%s", PortName);


	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 6,6);
}
bool CAirPressCtrl::ServerConnect()
{
	if(m_isOpen == true)
		return true;

	bool bConnect  = false;
	bConnect = Net232Connect(inet_addr(m_IPAddress.GetBuffer(m_IPAddress.GetLength())), SPR_SERVER_PORT);
	if(bConnect == true)
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 6,6);
	else
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 7,7);

	return bConnect;
}
bool CAirPressCtrl::ServerDisconnect()
{
	Net232DisConnect();
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 7,7);
	return true;
}
bool CAirPressCtrl::OpenPort()
{
	if(fnPortOpen(m_PortName, &m_DataEndCheck,(LPVOID)this, RecvCallBack,
		B9600,RS_PARITY_NONE,7,RS_STOP1) != SPR_PORT_RESULT_OK)
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
		return false;
	}
	else
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 4,4);
	}
	return true;
}
void CAirPressCtrl::ClosePort()
{
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
	fnPortClose();
}
void CAirPressCtrl::RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen)
{
	CAirPressCtrl *pCtrlObj = (CAirPressCtrl *)CtrlObj;
	pCtrlObj->m_LinkUpdateObj.pSPRServerTree->SetItemImage(pCtrlObj->m_LinkUpdateObj.PortItem, 9,9);
	BYTE RecvMSG[128];
	memset(RecvMSG, 0x00, 128);
	recvLen = (recvLen>127?127:recvLen);
	memcpy(RecvMSG, (BYTE*)pBuf, recvLen);

	USHORT readValue = 0;
	BYTE BufValue[2];
	if(recvLen>(7+4))
	{
		if(RecvMSG[3] =='8')//Error Code Cmd(0x03)에 0x80가 더해저서 온다.
		{
			if(pCtrlObj->m_ParentUpdate != 0)
				::SendMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_AIR_UPDATE, 0, 0);//오류 발생
		}
		else
		{
			//first High Low값만 취한다.
			ascii_to_hex((char*)&RecvMSG[7], 4, (BYTE*)BufValue);
			readValue =((BufValue[0]<<16)&0xFF00)|(BufValue[1]&0x00FF);

			if(RecvMSG[2] == '1')//Air
				pCtrlObj->m_NowValue[0] = readValue;
			if(RecvMSG[2] == '2')//N2
				pCtrlObj->m_NowValue[1] = readValue;

			if(pCtrlObj->m_ParentUpdate != 0)
				::SendMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_AIR_UPDATE, (WPARAM)RecvMSG[2], (LPARAM)readValue);
		}
	}
	else
	{
		if(pCtrlObj->m_ParentUpdate != 0)
			::SendMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_AIR_UPDATE, 0, 0);//오류 발생
	}
	
	
}

//////////////////////////////////////////////////////////////////////////
bool CAirPressCtrl::LinkOpen()
{
	if(ServerConnect() == true)
	{
		if(OpenPort()== false)
			return false;
	}
	else
		return false;

	return true;
}
bool CAirPressCtrl::ReadOutAll()
{
	//BYTE SendMSG[10];
	//memset(SendMSG, 0x00, 10);
	//RTC Mode
	//SendMSG[0] = 0x01;
	//SendMSG[1] = 0x03;
	//SendMSG[2] = 0x04;
	//SendMSG[3] = 0xB8;
	//SendMSG[4] = 0x00;
	//SendMSG[5] = 0x05;
	//unsigned short CRC_16 =0;
	//CRC_16 = CRC_16Modbus(6, SendMSG);
	//SendMSG[6] = CRC_16&0x00FF;
	//SendMSG[7] = (CRC_16&0xFF00)>>8;
	//SendMSG[8] = 0x00;
	//ASCII Mode
	////Station 01
	//SendMSG[0] = 0x01;
	//SendMSG[1] = 0x03;
	//SendMSG[2] = 0x07;
	//SendMSG[3] = 0xD1;
	//SendMSG[4] = 0x00;
	//SendMSG[5] = 0x02;
	//BYTE LRC_8 = LRC_8Modbus(6, (BYTE*)SendMSG);
	////Station 02
	//SendMSG[0] = 0x02;
	//SendMSG[1] = 0x03;
	//SendMSG[2] = 0x04;
	//SendMSG[3] = 0xb8;
	//SendMSG[4] = 0x00;
	//SendMSG[5] = 0x05;
	//BYTE LRC_8 = LRC_8Modbus(6, (BYTE*)SendMSG);

	//신규 명령 처리시에 계산 해서 만들어 준다.
	//char *pstrSend1 =":010304B800053B\r\n";//Station 01
	//char *pstrSend2 =":020304B800053A\r\n";//Station 02

	//BYTE SendMSG[10];
	//memset(SendMSG, 0x00, 10);
	////Station 01
	//SendMSG[0] = 0x01;
	//SendMSG[1] = 0x03;
	//SendMSG[2] = 0x05;
	//SendMSG[3] = 0x7A;
	//SendMSG[4] = 0x00;
	//SendMSG[5] = 0x05;
	//BYTE LRC_81 = LRC_8Modbus(6, (BYTE*)SendMSG);
	////Station 02
	//SendMSG[0] = 0x02;
	//SendMSG[1] = 0x03;
	//SendMSG[2] = 0x05;
	//SendMSG[3] = 0x7A;
	//SendMSG[4] = 0x00;
	//SendMSG[5] = 0x05;
	//BYTE LRC_82 = LRC_8Modbus(6, (BYTE*)SendMSG);
	//읽기 전에 Reset
	m_NowValue[0] = -1;//Air 값은 음수가 없다.(Flow 방향이 필요하면 읽어야 한다.)
	m_NowValue[1] = -1;
	char *pstrSend1 =":0103057A000578\r\n";//Station 01 유량 구하는 것.
	char *pstrSend2 =":0203057A000577\r\n";//Station 02 유량 구하는 것.
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);

	if(fnDataSend(strlen(pstrSend1), (BYTE*)pstrSend1) == SPR_PORT_RESULT_OK &&
		fnDataSend(strlen(pstrSend1), (BYTE*)pstrSend2) == SPR_PORT_RESULT_OK)
		return true;

	return false;
}