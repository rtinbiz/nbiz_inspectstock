
#include "StdAfx.h"
#include "QRCodeCtrl.h"



VOID CALLBACK TimerRoutine(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
	if (lpParam == NULL)
	{
		//printf("TimerRoutine lpParam is NULL\n");
	}
	else
	{
		CQRCodeCtrl *pCallObj =(CQRCodeCtrl *) lpParam;
		pCallObj->SetTriggerOnOff(false);
		if(pCallObj->m_ParentUpdate != 0)
		{
			pCallObj->m_LastReadCode.Format("READ_TIME_OUT");
			::SendMessage(pCallObj->m_ParentUpdate, WM_USER_ACC_CODE_UPDATE, 1, NULL);//LParam이 NULL이면 Time Out
		}
		if(TimerOrWaitFired)
		{
			//printf("The wait timed out.\n");
		}
		else
		{
			//printf("The wait event was signaled.\n");
		}
	}
}

//

CQRCodeCtrl::CQRCodeCtrl(void)
{
	m_DataEndCheck.ASCII_EndFlag = '\n';
	m_DataEndCheck.B_StartFlag = 0;
	m_DataEndCheck.B_DataSize = 0;

	m_ParentUpdate = 0;

	bTimerRun = false;
	hTimer = NULL;
	hTimerQueue = NULL;
	// Create the timer queue.
	hTimerQueue = CreateTimerQueue();
	if (NULL == hTimerQueue)
	{
		//printf("CreateTimerQueue failed (%d)\n", GetLastError());
	}
}


CQRCodeCtrl::~CQRCodeCtrl(void)
{
	if (!DeleteTimerQueue(hTimerQueue))
	{
		//printf("DeleteTimerQueue failed (%d)\n", GetLastError());
	}
	hTimerQueue = NULL;
}


void CQRCodeCtrl::SetEndChar(char EndChar, BYTE StartFlag, int EndSize)
{
	m_DataEndCheck.ASCII_EndFlag = EndChar;
	m_DataEndCheck.B_StartFlag = StartFlag;
	m_DataEndCheck.B_DataSize = EndSize;
}
void CQRCodeCtrl::SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj)
{
	m_LinkUpdateObj.pSPRServerTree = UpdateObj.pSPRServerTree;
	m_LinkUpdateObj.CtrlItem = UpdateObj.CtrlItem;
	m_LinkUpdateObj.IPItem = UpdateObj.IPItem;
	m_LinkUpdateObj.PortItem = UpdateObj.PortItem;
	m_IPAddress.Format("%s", IPAddress);
	m_PortName.Format("%s", PortName);
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 6,6);
}
bool CQRCodeCtrl::ServerConnect()
{
	if(m_isOpen == true)
		return true;

	bool bConnect  = false;
	bConnect = Net232Connect(inet_addr(m_IPAddress.GetBuffer(m_IPAddress.GetLength())), SPR_SERVER_PORT);
	if(bConnect == true)
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 6,6);
	else
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.IPItem, 7,7);

	return bConnect;
}
bool CQRCodeCtrl::ServerDisconnect()
{
	Net232DisConnect();
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.CtrlItem, 7,7);
	return true;
}
bool CQRCodeCtrl::OpenPort()
{
	if(fnPortOpen(m_PortName, &m_DataEndCheck,(LPVOID)this, RecvCallBack) != SPR_PORT_RESULT_OK)
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
		return false;
	}
	else
	{
		m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 4,4);
	}
	return true;
}
void CQRCodeCtrl::ClosePort()
{
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 5,5);
	fnPortClose();
}
bool CQRCodeCtrl::LinkOpen()
{
	if(ServerConnect() == true)
	{
		if(OpenPort()== false)
			return false;
	}
	else
		return false;

	return true;
}
void CQRCodeCtrl::RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen)
{
	CQRCodeCtrl *pCtrlObj = (CQRCodeCtrl *)CtrlObj;
	pCtrlObj->TimerStop();//Data가 들어왔음으로 Time Out을 종료 한다.

	pCtrlObj->m_LinkUpdateObj.pSPRServerTree->SetItemImage(pCtrlObj->m_LinkUpdateObj.PortItem, 9,9);
	char RecvMSG[128];
	memset(RecvMSG, 0x00, 128);
	recvLen = (recvLen>127?127:recvLen);
	memcpy(RecvMSG, (char*)pBuf, recvLen);

	pCtrlObj->m_LastReadCode.Format("%s", RecvMSG);
	if(pCtrlObj->m_ParentUpdate != 0)
		::SendMessage(pCtrlObj->m_ParentUpdate, WM_USER_ACC_CODE_UPDATE, 1, (LPARAM)RecvMSG);

	pCtrlObj->SetLightAimerOnOff(false);
}

int CQRCodeCtrl::TimerStop()
{
	if(bTimerRun == true)
		CancelTimerQueueTimer(hTimerQueue, hTimer);

	bTimerRun = false;
	return 0;
}
int CQRCodeCtrl::TimerRun()
{

	TimerStop();//기존에 돌고 있던것이 있으면 취소.
	// Set a timer to call the timer routine in 10 seconds.
	if (!CreateTimerQueueTimer( &hTimer, hTimerQueue, 
		(WAITORTIMERCALLBACK)TimerRoutine, (LPVOID)this  , 3000, 0, 0))
	{
		printf("CreateTimerQueueTimer failed (%d)\n", GetLastError());
		return 3;
	}
	bTimerRun = true;
	return 0;
}
//////////////////////////////////////////////////////////////////////////
bool CQRCodeCtrl::SetTriggerOnOff(bool OnOff)//Data Read를 할때 On을 한다.
{
	
	if(OnOff==true)
	{
		SetLightAimerOnOff(true);
		TimerRun();//Data Read Time Out을 시작 한다.
	}
	else
	{
		SetLightAimerOnOff(false);
	}

	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r\n", (OnOff?QRCODE_TRIGGER_ON:QRCODE_TRIGGER_OFF));
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);
	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
	{
		return true;
	}
	return false;
}
bool CQRCodeCtrl::SetLightAimerOnOff(bool OnOff)//Code위치를 지정 할때 켠다.
{
	if(OnOff == true)
		m_LastReadCode.Format("");

	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r\n", (OnOff?QRCODE_LIGHT_AIMER_ON:QRCODE_LIGHT_AIMER_OFF));
	m_LinkUpdateObj.pSPRServerTree->SetItemImage(m_LinkUpdateObj.PortItem, 8,8);
	if(fnDataSend(strlen(SendMSG), (BYTE*)&SendMSG[0]) == SPR_PORT_RESULT_OK)
	{
		return true;
	}
	return false;
}