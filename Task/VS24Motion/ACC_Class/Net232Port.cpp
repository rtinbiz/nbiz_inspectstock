#include "StdAfx.h"
#include "Net232Port.h"


CNet232Port::CNet232Port()
{
	//////////////////////////////////////////////////////////////////////////
	m_Net232Sockfd= INVALID_SOCKET; 

	m_CMDListReq = SPR_CMD_PROC_NONE;
	m_CMDOpen = SPR_CMD_PROC_NONE;
	m_CMDClose = SPR_CMD_PROC_NONE;
	//m_CMDSend = SPR_CMD_PROC_NONE;

	m_CMDListReqRet = SPR_PORT_RESULT_OK;
	m_CMDOpenRet = SPR_PORT_RESULT_OK;
	m_CMDCloseRet = SPR_PORT_RESULT_OK;
	//m_CMDSendRet = SPR_PORT_RESULT_OK;


	m_RunThread	= false;
	m_pHandleThread = nullptr;

	m_CallObj = nullptr;
	m_pfnRecvData = nullptr;
	m_pfnRecvList = nullptr;

	memset(m_Net232PortList, 0x00, 1024);
	memset(m_NowOpenPort, 0x00, 30);
	m_isOpen = false;

	m_SendDelay = 0.0;

}


CNet232Port::~CNet232Port(void)
{
	if(m_Net232Sockfd != INVALID_SOCKET)
		closesocket(m_Net232Sockfd);
	m_Net232Sockfd = INVALID_SOCKET;

	WSACleanup(); 

	m_RunThread = false;

	while(m_pHandleThread != nullptr)
	{
		Sleep(10);
	}
}
//#include <fcntl.h>
//int ConnectWait(int sockfd, struct sockaddr *saddr, int addrsize, int sec) 
//{ 
//	int newSockStat; 
//	int orgSockStat; 
//	int res, n; 
//	fd_set  rset, wset; 
//	struct timeval tval; 
//
//	int error = 0; 
//	int esize; 
//
//	if ( (newSockStat = fcntl(sockfd, F_GETFL, NULL)) < 0 )  
//	{ 
//		perror("F_GETFL error"); 
//		return -1; 
//	} 
//
//	orgSockStat = newSockStat; 
//	newSockStat |= O_NONBLOCK; 
//
//	// Non blocking 상태로 만든다.  
//	if(fcntl(sockfd, F_SETFL, newSockStat) < 0) 
//	{ 
//		perror("F_SETLF error"); 
//		return -1; 
//	} 
//
//	// 연결을 기다린다. 
//	// Non blocking 상태이므로 바로 리턴한다. 
//	if((res = connect(sockfd, saddr, addrsize)) < 0) 
//	{ 
//		if (errno != EINPROGRESS) 
//			return -1; 
//	} 
//
//	printf("RES : %d\n", res); 
//	// 즉시 연결이 성공했을 경우 소켓을 원래 상태로 되돌리고 리턴한다.  
//	if (res == 0) 
//	{ 
//		printf("Connect Success\n"); 
//		fcntl(sockfd, F_SETFL, orgSockStat); 
//		return 1; 
//	} 
//
//	FD_ZERO(&rset); 
//	FD_SET(sockfd, &rset); 
//	wset = rset; 
//
//	tval.tv_sec        = sec;     
//	tval.tv_usec    = 0; 
//
//	if ( (n = select(sockfd+1, &rset, &wset, NULL, NULL)) == 0) 
//	{ 
//		// timeout 
//		errno = ETIMEDOUT;     
//		return -1; 
//	} 
//
//	// 읽거나 쓴 데이터가 있는지 검사한다.  
//	if (FD_ISSET(sockfd, &rset) || FD_ISSET(sockfd, &wset) ) 
//	{ 
//		printf("Read data\n"); 
//		esize = sizeof(int); 
//		if ((n = getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, (socklen_t *)&esize)) < 0) 
//			return -1; 
//	} 
//	else 
//	{ 
//		perror("Socket Not Set"); 
//		return -1; 
//	} 
//
//
//	fcntl(sockfd, F_SETFL, orgSockStat); 
//	if(error) 
//	{ 
//		errno = error; 
//		perror("Socket"); 
//		return -1; 
//	} 
//
//	return 1; 
//} 

//bool connect(char *host,int port, int timeout)
//{
//	TIMEVAL Timeout;
//	Timeout.tv_sec = timeout;
//	Timeout.tv_usec = 0;
//	struct sockaddr_in address;  /* the libc network address data structure */   
//
//	SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
//
//	address.sin_addr.s_addr = inet_addr(host); /* assign the address */
//	address.sin_port = htons(port);            /* translate int2port num */	
//	address.sin_family = AF_INET;
//
//	//set the socket in non-blocking
//	unsigned long iMode = 1;
//	int iResult = ioctlsocket(sock, FIONBIO, &iMode);
//	if (iResult != NO_ERROR)
//	{	
//		printf("ioctlsocket failed with error: %ld\n", iResult);
//	}
//
//	if(connect(sock,(struct sockaddr *)&address,sizeof(address))==false)
//	{	
//		return false;
//	}	
//
//	// restart the socket mode
//	iMode = 0;
//	iResult = ioctlsocket(sock, FIONBIO, &iMode);
//	if (iResult != NO_ERROR)
//	{	
//		printf("ioctlsocket failed with error: %ld\n", iResult);
//	}
//
//	fd_set Write, Err;
//	FD_ZERO(&Write);
//	FD_ZERO(&Err);
//	FD_SET(sock, &Write);
//	FD_SET(sock, &Err);
//
//	// check if the socket is ready
//	select(0,NULL,&Write,&Err,&Timeout);			
//	if(FD_ISSET(sock, &Write)) 
//	{	
//		return true;
//	}
//
//	return false;
//}
bool CNet232Port::Net232Connect(DWORD ServerIPAdd, WORD ServerPort)
{
	if(m_Net232Sockfd != INVALID_SOCKET)
		closesocket(m_Net232Sockfd);
	m_Net232Sockfd = INVALID_SOCKET;


	WSADATA SendData; 

	struct sockaddr_in addrTClient; 

	memset((void *)&addrTClient, 0x00, sizeof(addrTClient)); 
	addrTClient.sin_family = AF_INET; 
	addrTClient.sin_addr.s_addr = ServerIPAdd; 
	addrTClient.sin_port = htons(ServerPort);

	//////////////////////////////////////////////////////////////////////////
	UINT SConnectCount =0;
	UINT RConnectCount =0;
	//Socke Time Out Setting
	//struct timeval tv_timeo = { 2, 500000 };  /* 3.5 second */
	int Optval = 100;//Time Out
	int TestInc = 0;

	int SendLen =0;
	int RecvLen =0;
	COleDateTime TargetCheckTime = COleDateTime::GetCurrentTime();

	//통신 Clear 
	if(WSAStartup(MAKEWORD(2,2), &SendData) != NO_ERROR) 
	{ 
		return false;
	} 

	//Winsocket Create(Client Sock)
	if((m_Net232Sockfd = socket(AF_INET,SOCK_STREAM, IPPROTO_HOPOPTS)) == INVALID_SOCKET)  
	{ 
		return false;
	} 

	//접속 시도는 Blocking Mode로 하고 접속 후에는 None Blocking으로 전환 하자.
	ULONG nonBlk = TRUE;
	struct  timeval     timevalue;

	ioctlsocket( m_Net232Sockfd, FIONBIO, &nonBlk );
	connect ( m_Net232Sockfd,  (struct sockaddr *)&addrTClient, sizeof(addrTClient));
	fd_set fdset;
	FD_ZERO( &fdset );
	FD_SET( m_Net232Sockfd, &fdset );
	timevalue.tv_sec = 4;
	timevalue.tv_usec = 0;

	::select( m_Net232Sockfd+1, NULL, &fdset, NULL, &timevalue );
	if ( !FD_ISSET( m_Net232Sockfd, &fdset ) )
	{
		closesocket( m_Net232Sockfd );
		WSACleanup();
		m_Net232Sockfd = INVALID_SOCKET;
		return false;
	}
	nonBlk = FALSE;
	ioctlsocket( m_Net232Sockfd, FIONBIO, &nonBlk );//None Blocking 에서 Blocking으로 변환 한다.
	if(m_pHandleThread == nullptr)
	{
		m_RunThread =  true;
		m_pHandleThread = ::AfxBeginThread(Thread_Net232CMDProc, this,	 THREAD_PRIORITY_HIGHEST);
	}
	//////////////////////////////////////////////////////////////////////////

	return true;
}
void CNet232Port::Net232DisConnect()
{
	if(m_Net232Sockfd != INVALID_SOCKET)
		closesocket(m_Net232Sockfd);
	m_Net232Sockfd = INVALID_SOCKET;
	m_RunThread =  false;
	while(m_pHandleThread != nullptr)
		Sleep(10);
}
int CNet232Port::fnPortListUpDate(LPVOID CallObj, pReceiveFunction pfnRecvList)
{
	if(m_Net232Sockfd != INVALID_SOCKET&& m_CMDListReq != SPR_CMD_PROC_RUN)
	{
		int nReturn = sizeof(SPR_CMD);
		SPR_CMD SendCmd;
		SendCmd.SPRCommand = SPR_PORT_LIST_REQ;
		SendCmd.SPRCommandRet = SPR_PORT_RESULT_OK;
		SendCmd.DataSize = 0;

		m_CMDListReq = SPR_CMD_PROC_RUN;
		m_CMDListReqRet = SPR_PORT_RESULT_OK;

		m_CallObj = CallObj;
		m_pfnRecvList = pfnRecvList;
		if(SOCKET_ERROR == send(m_Net232Sockfd, (char*)&SendCmd, nReturn, 0))
		{
			m_CMDListReq = SPR_CMD_PROC_NONE;
			m_CMDListReqRet = SPR_PORT_RESULT_NG;
			return SPR_PORT_RESULT_NG;
		}
		int CMDTimer=200;
		//MSG msg;
		//BOOL bRet; 
		while(m_CMDListReq != SPR_CMD_PROC_END)
		{

			//bRet = PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
			//if (bRet != -1)
			//{
			//	TranslateMessage(&msg); 
			//	DispatchMessage(&msg); 
			//}
			CMDTimer--;
			if(CMDTimer<=0)
			{
				return  SPR_PORT_TIMEOUT;
			}
			Sleep(10);
		}
	}
	else
	{
		m_CMDListReq = SPR_CMD_PROC_NONE;
		m_CMDListReqRet = SPR_PORT_RESULT_NG;
		return SPR_PORT_RESULT_NG;
	}

	return m_CMDListReqRet;
}
int CNet232Port::fnPortOpen(CString strPort,  RecvCheck *pRCheck, LPVOID CallObj, pReceiveFunction pfnRecvData, DWORD nRate, BYTE nParity, BYTE nSize, BYTE nStop)
{
	if(m_isOpen == true)
		return SPR_PORT_RESULT_OK;
	if(m_Net232Sockfd != INVALID_SOCKET && m_CMDOpen != SPR_CMD_PROC_RUN)
	{
		char SendDataBuf[sizeof(SPR_CMD)+sizeof(OPEN_Info)];
		OPEN_Info PortOpenInfo;
		sprintf_s(PortOpenInfo.OPortName, 29,"%s", strPort.GetBuffer(strPort.GetLength()));
		PortOpenInfo.nRate = nRate;
		PortOpenInfo.nParity = nParity;
		PortOpenInfo.nSize = nSize;
		PortOpenInfo.nStop = nStop;


		m_SendDelay  = (PortOpenInfo.nRate/8.0)/1000.0;
		memcpy(&PortOpenInfo.ComEndCheck, pRCheck, sizeof(RecvCheck));

		int nReturn = sizeof(SPR_CMD)+sizeof(OPEN_Info);
		SPR_CMD SendCmd;
		SendCmd.SPRCommand = SPR_PORT_OPEN;
		SendCmd.SPRCommandRet = SPR_PORT_RESULT_OK;
		SendCmd.DataSize = 0;

		m_CMDOpen = SPR_CMD_PROC_RUN;
		m_CMDOpenRet = SPR_PORT_RESULT_OK;

		m_CallObj = CallObj;
		m_pfnRecvData = pfnRecvData;

		memcpy(SendDataBuf,&SendCmd, sizeof(SPR_CMD) );
		memcpy((SendDataBuf+sizeof(SPR_CMD)),(char*)&PortOpenInfo, sizeof(OPEN_Info) );
		if(SOCKET_ERROR == send(m_Net232Sockfd, SendDataBuf, nReturn, 0))
		{
			m_CMDOpen = SPR_CMD_PROC_NONE;
			m_CMDOpenRet = SPR_PORT_RESULT_NG;
			return SPR_PORT_RESULT_NG;
		}
		int CMDTimer=200;
		//MSG msg;
		//BOOL bRet; 
		while(m_CMDOpen != SPR_CMD_PROC_END)
		{

			//bRet = PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
			//if (bRet != -1)
			//{
			//	TranslateMessage(&msg); 
			//	DispatchMessage(&msg); 
			//}
			CMDTimer--;
			if(CMDTimer<=0)
			{
				return  SPR_PORT_TIMEOUT;
			}

			Sleep(10);
		}


		sprintf_s(m_NowOpenPort, 29,"%s", strPort.GetBuffer(strPort.GetLength()));
	}
	else
	{
		m_pfnRecvData = nullptr;
		m_CallObj = nullptr;
		memset(m_NowOpenPort, 0x00, 30);
		m_isOpen = false;
		m_CMDOpen = SPR_CMD_PROC_NONE;
		m_CMDOpenRet = SPR_PORT_RESULT_NG;
		return SPR_PORT_RESULT_NG;
	}
	return m_CMDOpenRet;
}
int CNet232Port::fnPortClose()
{
	if(m_Net232Sockfd != INVALID_SOCKET&& m_CMDClose != SPR_CMD_PROC_RUN && m_isOpen == true)
	{
		int nReturn = sizeof(SPR_CMD);
		SPR_CMD SendCmd;
		SendCmd.SPRCommand = SPR_PORT_CLOSE;
		SendCmd.SPRCommandRet = SPR_PORT_RESULT_OK;
		SendCmd.DataSize = 0;

		m_CMDClose = SPR_CMD_PROC_RUN;
		m_CMDCloseRet = SPR_PORT_RESULT_OK;
		if(SOCKET_ERROR == send(m_Net232Sockfd, (char*)&SendCmd, nReturn, 0))
		{
			m_CMDClose = SPR_CMD_PROC_NONE;
			m_CMDCloseRet = SPR_PORT_RESULT_NG;
			return SPR_PORT_RESULT_NG;
		}

		int CMDTimer=200;
		//MSG msg;
		//BOOL bRet; 
		while(m_CMDClose != SPR_CMD_PROC_END)
		{

			//bRet = PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
			//if (bRet != -1)
			//{
			//	TranslateMessage(&msg); 
			//	DispatchMessage(&msg); 
			//}
			CMDTimer--;
			if(CMDTimer<=0)
			{
				return  SPR_PORT_TIMEOUT;
			}

			Sleep(10);
		}
	}
	else
	{
		m_CMDClose = SPR_CMD_PROC_NONE;
		m_CMDCloseRet = SPR_PORT_RESULT_NG;
		return SPR_PORT_RESULT_NG;
	}

	return m_CMDCloseRet;
}
int CNet232Port::fnDataSend(int DSize, BYTE *pDBuf)
{
	if(m_Net232Sockfd != INVALID_SOCKET && DSize>0 && pDBuf!=nullptr && m_isOpen == true)// && m_CMDSend != SPR_CMD_PROC_RUN)
	{
		int nReturn = sizeof(SPR_CMD)+DSize;
		char *pSendDataBuf = new char[nReturn];

		SPR_CMD SendCmd;
		SendCmd.SPRCommand = SPR_PORT_DSEND;
		SendCmd.SPRCommandRet = SPR_PORT_RESULT_OK;
		SendCmd.DataSize = DSize;

		//m_CMDSend = SPR_CMD_PROC_RUN;
		//m_CMDSendRet = SPR_PORT_RESULT_OK;

		memcpy(pSendDataBuf,&SendCmd, sizeof(SPR_CMD) );
		memcpy((pSendDataBuf+sizeof(SPR_CMD)),(char*)pDBuf, DSize );
		if(SOCKET_ERROR == send(m_Net232Sockfd, pSendDataBuf, nReturn, 0))
		{
			//m_CMDSend = SPR_CMD_PROC_NONE;
			//m_CMDSendRet = SPR_PORT_RESULT_NG;
			delete []pSendDataBuf;
			pSendDataBuf = nullptr;

			closesocket(m_Net232Sockfd);
			m_Net232Sockfd = INVALID_SOCKET;
			m_RunThread =  false;
			while(m_pHandleThread != nullptr)
				Sleep(10);

			m_isOpen = false;

			return SPR_PORT_RESULT_NG;
		}

		int SleepTime = (int)((DSize>0?DSize:1/m_SendDelay)+0.5);
		Sleep(SleepTime);
		//extern int SendCnt ;
		//SendCnt++;
		//int CMDTimer=1000;
		//MSG msg;
		//BOOL bRet; 
		//while(m_CMDSend != SPR_CMD_PROC_END)
		//{

		//	bRet = GetMessage( &msg, NULL, 0, 0 );//bRet = PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
		//	if (bRet != -1)
		//	{
		//		TranslateMessage(&msg); 
		//		DispatchMessage(&msg); 
		//	}
		//	CMDTimer--;
		//	if(CMDTimer<=0)
		//	{
		//		delete []pSendDataBuf;
		//		pSendDataBuf = nullptr;
		//		return  SPR_PORT_TIMEOUT;
		//	}
		//	Sleep(10);
		//}

		//통신 속도에 따른 전송 지연 시간을 갖는다.


		delete []pSendDataBuf;
		pSendDataBuf = nullptr;
	}
	else
	{
		return SPR_PORT_RESULT_NG;
	}

	return SPR_PORT_RESULT_OK;
}
int RecvCount = 0;
int UnKnowCount = 0;
UINT Thread_Net232CMDProc(LPVOID pParam)
{
	CNet232Port *pPortObj = (CNet232Port*)pParam;

	const int buflen = 2048*5;
	char buf[buflen] = {0};
	int nReturn = 0;

	int TokenPacket = 0;
	char *pBufPos = nullptr;
	SPR_CMD RecvCMD;
	while(pPortObj->m_RunThread == true)
	{
		memset(buf, 0x00, buflen);
		nReturn = recv(pPortObj->m_Net232Sockfd, buf, buflen-1, 0);
		if (nReturn == 0 || nReturn == SOCKET_ERROR)
		{
			pPortObj->m_RunThread  = false;
			break;
		}

		TokenPacket = 0;
		if(nReturn>=sizeof(SPR_CMD))
		{
			while(TokenPacket<nReturn)
			{
				RecvCount++;
				pBufPos = (buf+TokenPacket);
				memcpy(&RecvCMD, pBufPos, sizeof(SPR_CMD));

				switch(RecvCMD.SPRCommand)
				{
				case SPR_PORT_None		:
					{
					}break;
				case SPR_PORT_LIST_REQ :
					{
						if(pPortObj->m_CMDListReq == SPR_CMD_PROC_RUN)
						{
							if(RecvCMD.SPRCommandRet == SPR_PORT_RESULT_OK && RecvCMD.DataSize>0)
								sprintf_s(pPortObj->m_Net232PortList, "%s", (pBufPos + sizeof(SPR_CMD)));

							pPortObj->m_CMDListReqRet = RecvCMD.SPRCommandRet;
							pPortObj->m_CMDListReq = SPR_CMD_PROC_END;

							if(pPortObj->m_pfnRecvList != nullptr)
							{
								//Port List는 요청시 한번만 한다.
								pPortObj->m_pfnRecvList((LPVOID)pPortObj->m_CallObj,(LPVOID)(pBufPos+sizeof(SPR_CMD)), RecvCMD.DataSize);
								pPortObj->m_pfnRecvList = nullptr;
							}
						}
					}break;
				case SPR_PORT_OPEN		:
					{
						if(pPortObj->m_CMDOpen == SPR_CMD_PROC_RUN)
						{
							pPortObj->m_CMDOpenRet = RecvCMD.SPRCommandRet;
							pPortObj->m_CMDOpen = SPR_CMD_PROC_END;

							if( RecvCMD.SPRCommandRet == SPR_PORT_RESULT_OK)
								pPortObj->m_isOpen = true;
						}
					}break;
				case SPR_PORT_CLOSE		:
					{
						if(pPortObj->m_CMDClose == SPR_CMD_PROC_RUN)
						{
							pPortObj->m_CMDCloseRet = RecvCMD.SPRCommandRet;
							pPortObj->m_CMDClose = SPR_CMD_PROC_END;

							if( RecvCMD.SPRCommandRet == SPR_PORT_RESULT_OK)
								pPortObj->m_isOpen = false;
						}
					}break;
				case SPR_PORT_DSEND		://Send Error 발생시 
					{
						//if(pPortObj->m_CMDSend == SPR_CMD_PROC_RUN && RecvCMD.DataSize == 0)
						//{//Send CMD에대한 Return
						//	pPortObj->m_CMDSendRet = RecvCMD.SPRCommandRet;
						//	pPortObj->m_CMDSend = SPR_CMD_PROC_END;
						//}
						//else
						//{//Port 입력 Data
						//	if(pPortObj->m_pfnRecvData!=nullptr)
						//	{
						//		pPortObj->m_pfnRecvData((LPVOID)pPortObj->m_CallObj,(LPVOID)(pBufPos+sizeof(SPR_CMD)), RecvCMD.DataSize);
						//	}
						//}
					}break;
				case SPR_PORT_PDSEND		:
					{
						//if(pPortObj->m_CMDSend == SPR_CMD_PROC_RUN && RecvCMD.DataSize == 0)
						//{//Send CMD에대한 Return
						//	pPortObj->m_CMDSendRet = RecvCMD.SPRCommandRet;
						//	pPortObj->m_CMDSend = SPR_CMD_PROC_END;
						//}
						//else
						{//Port 입력 Data
							if(pPortObj->m_pfnRecvData!=nullptr)
							{
								pPortObj->m_pfnRecvData((LPVOID)pPortObj->m_CallObj,(LPVOID)(pBufPos+sizeof(SPR_CMD)), RecvCMD.DataSize);
							}
							else
								G_WriteInfo(Log_Worrying, "xxxxxx");
						}
					}break;
				default:
					G_WriteInfo(Log_Worrying, "UnKnowCount");
					UnKnowCount++;
					break;
				}
				TokenPacket+= sizeof(SPR_CMD)+RecvCMD.DataSize;
			}
		}
		Sleep(10);
	}
	pPortObj->m_pHandleThread = nullptr;
	return 0;
}