#pragma once

#include "Net232Port.h"
//#define QRCODE_GET_TRIGGER_MODE		"||>GET TRIGGER"
#define QRCODE_TRIGGER_ON				"||>TRIGGER ON"
#define QRCODE_TRIGGER_OFF				"||>TRIGGER OFF"
//#define QRCODE_GET_LIGHT_AIMER			"||>GET LIGHT.AIMER"
#define QRCODE_LIGHT_AIMER_ON			"||>SET LIGHT.AIMER 1"
#define QRCODE_LIGHT_AIMER_OFF			"||>SET LIGHT.AIMER 0"

class CQRCodeCtrl:public CNet232Port
{
public:
	CQRCodeCtrl(void);
	~CQRCodeCtrl(void);
public:
	LinkUpdate m_LinkUpdateObj;
	CString m_IPAddress;
	CString m_PortName;

	HWND m_ParentUpdate;

	CString m_LastReadCode;
	//Read Time Out을 측정 하기 위해서.
	bool bTimerRun;
	HANDLE hTimer;
	HANDLE hTimerQueue;
	int TimerRun();
	int TimerStop();
public:
	void SetEndChar(char EndChar = '\n', BYTE StartFlag = 0, int EndSize = 0);
	void SetConnectInfo(CString IPAddress, CString PortName, LinkUpdate UpdateObj);
	bool ServerConnect();
	bool ServerDisconnect();
	bool OpenPort();
	void ClosePort();
	void static RecvCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen);

	//////////////////////////////////////////////////////////////////////////
	bool LinkOpen();
	bool SetTriggerOnOff(bool OnOff);//Data Read를 할때 On을 한다.
	bool SetLightAimerOnOff(bool OnOff);//Code위치를 지정 할때 켠다.

};

