#include "StdAfx.h"
#include "MotCtrl.h"
#include ".\MotionLink\Runtime.h"
UINT DataUpdateThread(LPVOID pParam)
{
	MotThreadData*pThObj = (MotThreadData*)pParam;

	CMotCtrl *pMotObj = (CMotCtrl *)pThObj->m_pCallObj;
	DWORD NowCnt = 0;
	while(pThObj->m_RunIOThread)
	{
		NowCnt = ::GetTickCount();
		//////////////////////////////////////////////////////////////////////////
		pMotObj->m_fnUpdateIO();
		pMotObj->m_fnUpdateLocalMotState();
		pMotObj->m_fnUpdateML3MotState();
		pMotObj->m_fnUpdateAxisLinkMotState();

		pMotObj->m_fnUpdateAxisPosition();

		pMotObj->m_fnLensOffsetApplyCheck();
		Sleep(20);
		//////////////////////////////////////////////////////////////////////////
		pThObj->m_ThCyclesTime = ::GetTickCount() - NowCnt;
	}
	pThObj->m_pThreadObj = nullptr;
	return 0;
}

UINT IOScenarioProcThread(LPVOID pParam)
{
	MotThreadData*pThObj = (MotThreadData*)pParam;

	CMotCtrl *pMotObj = (CMotCtrl *)pThObj->m_pCallObj;
	DWORD NowCnt = 0;
	while(pThObj->m_RunIOThread)
	{
		NowCnt = ::GetTickCount();
		//////////////////////////////////////////////////////////////////////////
		pMotObj->m_fnIOScenarioProc();
		pMotObj->m_fnIOMCCUpdateProc();
		Sleep(20);
		//////////////////////////////////////////////////////////////////////////
		pThObj->m_ThCyclesTime = ::GetTickCount() - NowCnt;
	}
	pThObj->m_pThreadObj = nullptr;
	return 0;
}


CMotCtrl::CMotCtrl(void)
{
	// Class 생성자에서 변수 초기화.
	m_hPmacLib		= NULL;	
	m_bDriverOpen		= FALSE;		
	m_dwDevice		= 0;	

	m_bBuzzerOFF		= false;

	m_SignalTowerMode = STWOER_MODE0_NONE;//처음 구동시에는 동작 Off상태로 기동.

	//동기화 축에 좌우의 편차를 보정 하기 위한 값.(초기화에서 ??)
	m_BO01_BoxRotateOffset = 0;
	m_BO01_BoxUpDnOffset = 0;
	m_AM01_TensionUpDnOffset = 0;

	m_NowAllSpeedMode = Speed_Normal;

	m_bReviewLensOffsetApply = false;
	m_ReviewLensOffsetShiftApply_Value = 0;
	m_ReviewLensOffsetDriveApply_Value = 0;
	m_ReviewLensOffsetShift = 0;
	m_ReviewLensOffsetDrive = 0;

	m_bScopeLensOffsetApply = false;
	m_ScopeLensOffsetShiftApply_Value = 0;
	m_ScopeLensOffsetDriveApply_Value = 0;
	m_ScopeLensOffsetShift = 0;
	m_ScopeLensOffsetDrive = 0;
}


CMotCtrl::~CMotCtrl(void)
{
	//Thread 정리.
	m_fnStopProcThreadAll();

	if ( m_bDriverOpen == TRUE )
	{
		DeviceClose(m_dwDevice);	// 해당 Device를 Close한다.
		m_bDriverOpen = FALSE;
	}

	if ( m_hPmacLib )
	{
		CloseRuntimeLink();		// Runtime Link 닫아주기.
		m_hPmacLib = NULL;		// 변수 다시 초기화.
	}
}
bool CMotCtrl::InitMotDevice()
{
	// Pmac OpenRuntimeLink의 핸들을 얻어온다.
	if(!m_hPmacLib)
		m_hPmacLib = OpenRuntimeLink();

	if(m_hPmacLib == NULL)
		return false;

	m_dwDevice = 0;	// 0번 Device를 사용하겠다.
	 m_bDriverOpen = DeviceOpen(m_dwDevice);

	if(m_bDriverOpen == TRUE)
	{
		//State, IO RW Thread Run
		m_fnRunStateReadThread();

		//IO Scenario Run
		m_fnIOProcessThread();
	}
	else
		return false;

	return true;
}

CString CMotCtrl::m_fnUmacOnlineCommand(CString strCommand)
{
	if(m_bDriverOpen == FALSE)
		return "";

	//G_WriteInfo(Log_Error, "===>%s", strCommand);
	CString strMessage;
	char chResponse[1024];
	memset(chResponse, 0x00, 1024);
	DeviceGetResponse(m_dwDevice, chResponse, 1024, strCommand.GetBuffer(strCommand.GetLength()));
	strMessage.Format("%s", chResponse);
	strMessage.Replace("\n", "");
	strMessage.Replace("\r", "");
	return strMessage;
}
void CMotCtrl::m_fnMotionPerInit()
{
	CString UmacCMD;
	UmacCMD.Format("%s=0",AXIS_HOME_FLAG_TT01);
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=0",AXIS_HOME_FLAG_LT01);
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=0",AXIS_HOME_FLAG_BT01);
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=0",AXIS_HOME_FLAG_BO01);
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=0",AXIS_HOME_FLAG_UT01);
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=0",AXIS_HOME_FLAG_TM01);
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=0",AXIS_HOME_FLAG_TM02);
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=0",AXIS_HOME_FLAG_AM01);
	m_fnUmacOnlineCommand(UmacCMD);

	//내부 In/Out Buffer 를 모두 초기화 시켜서  무조건 읽게 한다.
	memset(&m_OldIO_InA.shAL_IN, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_InB.shAL_IN, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_InC.shAL_IN, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_InD.shAL_IN, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_InE.shAL_IN, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_InF.shAL_IN, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_InG.shAL_IN, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_InH.shAL_IN, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_InI.shAL_IN, 0xFF, sizeof(DWORD));
	G_WriteInfo(Log_Normal, "1) Internal Input Buffer Clear Ok");
	//내부 In/Out Buffer 를 모두 초기화 시켜서  무조건 갱신시킨다.
	memset(&m_OldIO_OutA.shAL_Out, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_OutB.shAL_Out, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_OutC.shAL_Out, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_OutD.shAL_Out, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_OutE.shAL_Out, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_OutF.shAL_Out, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_OutG.shAL_Out, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_OutH.shAL_Out, 0xFF, sizeof(DWORD));
	memset(&m_OldIO_OutI.shAL_Out, 0xFF, sizeof(DWORD));
	G_WriteInfo(Log_Normal, "2) Internal Output Buffer Clear Ok");
	//초기 UMAC BUFFER를 리셋 하여 최초에는 무조건 읽게 한다.
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_A1);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_A2);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_B1);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_B2);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_C1);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_C2);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_D1);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_D2);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_E1);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_E2);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_F1);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_F2);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_G1);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_G2);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_H1);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_H2);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_I1	);
	m_fnUmacOnlineCommand(AL_IO_INPUT_OLD_I2	);
	m_fnUmacOnlineCommand(AL_IO_INPUT_UpDateReset);//함부로 하는거 아니다.
	G_WriteInfo(Log_Normal, "3) External(UMAC PLC) Output Buffer Clear Ok");

	//기존 출력 값을 읽어서 현재 기본값에 셋하자.(Out Put 유지)
	m_IO_OutA.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_A1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_A2))<<16)&0xFFFF0000);		
	m_IO_OutB.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_B1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_B2))<<16)&0xFFFF0000);		
	m_IO_OutC.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_C1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_C2))<<16)&0xFFFF0000);		
	m_IO_OutD.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_D1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_D2))<<16)&0xFFFF0000);		
	m_IO_OutE.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_E1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_E2))<<16)&0xFFFF0000);		
	m_IO_OutF.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_F1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_F2))<<16)&0xFFFF0000);		
	m_IO_OutG.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_G1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_G2))<<16)&0xFFFF0000);		
	m_IO_OutH.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_H1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_H2))<<16)&0xFFFF0000);		
	m_IO_OutI.shAL_Out = (atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_I1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_OUTPUT_I2))<<16)&0xFFFF0000);		
	G_WriteInfo(Log_Normal, "4) Out put Data Restoration OK");

	m_fnUmacOnlineCommand("ena plc 20");//IO Plc를 구동 한다.
	G_WriteInfo(Log_Normal, "5) IO R/W PLC Start");

	m_fnUmacOnlineCommand("ena plc 19");//IO Plc를 구동 한다.
	G_WriteInfo(Log_Normal, "6) Axis Link Mot Stop Plc Run");

	m_fnUmacOnlineCommand("ena plcc 2");//IO Plc를 구동 한다.
	G_WriteInfo(Log_Normal, "7) PLCC 2 Start");
	
	//ML3 Axis Link Error시 재 접속 시도를 10초간 진행 한다.
	G_WriteInfo(Log_Normal, "8) ML3 Motion Link Check Start");
	int TimeOutML3Link =1000;//10sec
	if((atoi(m_fnUmacOnlineCommand("M8130")) != atoi(m_fnUmacOnlineCommand("M8134"))) ||
		(atoi(m_fnUmacOnlineCommand("M8123")) != 4))
	{
		m_fnUmacOnlineCommand("M8123 = 8");
		while(atoi(m_fnUmacOnlineCommand("M8123")) != 4)
		{
			Sleep(10);
			if(TimeOutML3Link <= 0)
			{
				G_WriteInfo(Log_Error, "ML3 Link Error %d", m_fnUmacOnlineCommand("M8123"));
				break;
			}
			TimeOutML3Link--;
		}
	}
	G_WriteInfo(Log_Normal, "ML3 Motion Link Check END");

	//Speed Mode Set
	m_fnSetMotSpeed(Speed_Normal);
}

int CMotCtrl::m_fnSpaceSensorLaserOnOff(bool OnOff)
{
	//Normal On임으로 IO출력으로 꺼준다.
	m_IO_OutA.Out31_Space_Sensor_LaserOnOff = (OnOff==true?0:1);// (OnOff==true?1:0);
	return m_IO_OutA.Out31_Space_Sensor_LaserOnOff;
}
void CMotCtrl::m_fnRunStateReadThread()
{
	//초기 UMAC Buff를 Reset한다.
	m_fnMotionPerInit();
	m_MotStateThData.m_RunIOThread		= true;
	m_MotStateThData.m_pCallObj			= (void*)this;
	m_MotStateThData.m_pThreadObj		=  ::AfxBeginThread(DataUpdateThread, (LPVOID)&m_MotStateThData ,THREAD_PRIORITY_HIGHEST);

	G_WriteInfo(Log_Normal, "State Data Read Thread Run OK");
}


void CMotCtrl::m_fnIOProcessThread()
{
	m_IOProcessThData.m_RunIOThread	= true;
	m_IOProcessThData.m_pCallObj			= (void*)this;
	m_IOProcessThData.m_pThreadObj		=  ::AfxBeginThread(IOScenarioProcThread, (LPVOID)&m_IOProcessThData ,THREAD_PRIORITY_HIGHEST);
}

void CMotCtrl::m_fnStopProcThreadAll()
{
	m_MotStateThData.m_RunIOThread			= false;
	m_IOProcessThData.m_RunIOThread		= false;
	int TimeOutKillThread = 500;//5Sec 대기
	while(m_MotStateThData.m_pThreadObj	!= nullptr || m_IOProcessThData.m_pThreadObj != nullptr)
	{
		TimeOutKillThread--;
		if(TimeOutKillThread == 0)
		{
			break;
		}
		Sleep( 10 );
	}
	m_MotStateThData.m_pCallObj			= nullptr;
	m_IOProcessThData.m_pCallObj			= nullptr;

}
bool CMotCtrl::m_fnUpdateIO()
{
	DWORD UpdateTimeBuf =  ::GetTickCount();

	if(m_bDriverOpen == FALSE)
		return false;
	CString strCommand;

	//////////////////////////////////////////////////////////////////////////
	//Input Read
	//m_InputCheck.shAL_InChange = (unsigned short)(atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_UpDate))&0x0000FFFF);
	//short CheckIOValue = (short)atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_UpDate));
	//if(CheckIOValue!=0)//변경이 있을경우
	//{
	//	//strCommand.GetAt(0) == '9';
	//	if(CheckIOValue & AL_INPUT_CheckValeu_A)
	//		m_IO_InA.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_A1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_A2))<<16)&0xFFFF0000);	
	//	if(CheckIOValue & AL_INPUT_CheckValeu_B)
	//		m_IO_InB.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_B1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_B2))<<16)&0xFFFF0000);	
	//	if(CheckIOValue & AL_INPUT_CheckValeu_C)
	//		m_IO_InC.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_C1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_C2))<<16)&0xFFFF0000);	
	//	if(CheckIOValue & AL_INPUT_CheckValeu_D)
	//		m_IO_InD.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_D1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_D2))<<16)&0xFFFF0000);		
	//	if(CheckIOValue & AL_INPUT_CheckValeu_E)
	//		m_IO_InE.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_E1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_E2))<<16)&0xFFFF0000);	

	//	//strCommand.GetAt(6) == '.';

	//	if(CheckIOValue & AL_INPUT_CheckValeu_F)
	//		m_IO_InF.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_F1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_F2))<<16)&0xFFFF0000);		
	//	if(CheckIOValue & AL_INPUT_CheckValeu_G)
	//		m_IO_InG.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_G1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_G2))<<16)&0xFFFF0000);	
	//	if(CheckIOValue & AL_INPUT_CheckValeu_H)
	//		m_IO_InH.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_H1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_H2))<<16)&0xFFFF0000);	
	//	if(CheckIOValue & AL_INPUT_CheckValeu_I)
	//		m_IO_InI.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_I1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_I2))<<16)&0xFFFF0000);	

	//	m_fnUmacOnlineCommand(AL_IO_INPUT_UpDateReset);
	//}
	m_bScopeSafetyPos = (atoi(m_fnUmacOnlineCommand(INTERLOCK_SCOPE_BWD_SENSER))>0?true:false);

	m_IO_InA.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_A1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_A2))<<16)&0xFFFF0000);	
	m_IO_InB.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_B1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_B2))<<16)&0xFFFF0000);	
	m_IO_InC.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_C1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_C2))<<16)&0xFFFF0000);	
	m_IO_InD.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_D1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_D2))<<16)&0xFFFF0000);		
	m_IO_InE.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_E1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_E2))<<16)&0xFFFF0000);	
	m_IO_InF.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_F1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_F2))<<16)&0xFFFF0000);		
	m_IO_InG.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_G1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_G2))<<16)&0xFFFF0000);	
	m_IO_InH.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_H1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_H2))<<16)&0xFFFF0000);	
	m_IO_InI.shAL_IN = (atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_I1))&0x0000FFFF) |  ((atoi(m_fnUmacOnlineCommand(AL_IO_INPUT_I2))<<16)&0xFFFF0000);	

	//Master 공유 메모리에 Update하자.
	if(G_MotInfo.m_pMotState != nullptr)
	{
		G_MotInfo.m_pMotState->stNormalStatus.shAL_IN		= m_IO_InA.shAL_IN; //Door, Temperature, 장비 일반 사항.
		G_MotInfo.m_pMotState->stBoxTableSensor.shAL_IN		= m_IO_InB.shAL_IN; //Box Table, Box Detect Sensor
		G_MotInfo.m_pMotState->stModuleStatus.shAL_IN		= m_IO_InC.shAL_IN; //TM01, LT01, BO01, UT01
		G_MotInfo.m_pMotState->stGripperStatus.shAL_IN		= m_IO_InD.shAL_IN; //Gripper
		G_MotInfo.m_pMotState->stPickerSensor.shAL_IN		= m_IO_InE.shAL_IN; //TM02
		G_MotInfo.m_pMotState->stPickerCylinder.shAL_IN		= m_IO_InF.shAL_IN; //TM02 Cylinder
		G_MotInfo.m_pMotState->stTurnTableSensor.shAL_IN		= m_IO_InG.shAL_IN; //TT01 
		G_MotInfo.m_pMotState->stReserve.shAL_IN				= m_IO_InH.shAL_IN;
		G_MotInfo.m_pMotState->stEmissionTable.shAL_IN		= m_IO_InI.shAL_IN ; //UT02 Status


		G_MotInfo.m_pMotState->m_IO_OutA.shAL_Out	= m_IO_OutA.shAL_Out; //Door, Temperature, 장비 일반 사항.
		G_MotInfo.m_pMotState->m_IO_OutB.shAL_Out	= m_IO_OutB.shAL_Out; //Box Table, Box Detect Sensor
		G_MotInfo.m_pMotState->m_IO_OutC.shAL_Out	= m_IO_OutC.shAL_Out; //TM01, LT01, BO01, UT01
		G_MotInfo.m_pMotState->m_IO_OutD.shAL_Out	= m_IO_OutD.shAL_Out; //Gripper
		G_MotInfo.m_pMotState->m_IO_OutE.shAL_Out	= m_IO_OutE.shAL_Out; //TM02
		G_MotInfo.m_pMotState->m_IO_OutF.shAL_Out	= m_IO_OutF.shAL_Out; //TM02 Cylinder
		G_MotInfo.m_pMotState->m_IO_OutG.shAL_Out	= m_IO_OutG.shAL_Out; //TT01 
		G_MotInfo.m_pMotState->m_IO_OutH.shAL_Out	= m_IO_OutH.shAL_Out;
		G_MotInfo.m_pMotState->m_IO_OutI.shAL_Out	= m_IO_OutI.shAL_Out ; //UT02 Status

		G_MotInfo.m_pMotState->m_bScopeSafetyPos		= m_bScopeSafetyPos;
	}
	//////////////////////////////////////////////////////////////////////////
	//Output Set
	if(m_OldIO_OutA.shAL_Out != m_IO_OutA.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_A1, m_IO_OutA.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_A2, (m_IO_OutA.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutA.shAL_Out = m_IO_OutA.shAL_Out;
	}
	if(m_OldIO_OutB.shAL_Out != m_IO_OutB.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_B1, m_IO_OutB.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_B2, (m_IO_OutB.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutB.shAL_Out = m_IO_OutB.shAL_Out;
	}
	if(m_OldIO_OutC.shAL_Out != m_IO_OutC.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_C1, m_IO_OutC.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_C2, (m_IO_OutC.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutC.shAL_Out = m_IO_OutC.shAL_Out;
	}
	if(m_OldIO_OutD.shAL_Out != m_IO_OutD.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_D1, m_IO_OutD.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_D2, (m_IO_OutD.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutD.shAL_Out = m_IO_OutD.shAL_Out;
	}
	if(m_OldIO_OutE.shAL_Out != m_IO_OutE.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_E1, m_IO_OutE.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_E2, (m_IO_OutE.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutE.shAL_Out = m_IO_OutE.shAL_Out;
	}
	if(m_OldIO_OutF.shAL_Out != m_IO_OutF.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_F1, m_IO_OutF.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_F2, (m_IO_OutF.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutF.shAL_Out = m_IO_OutF.shAL_Out;
	}
	if(m_OldIO_OutG.shAL_Out != m_IO_OutG.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_G1, m_IO_OutG.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_G2, (m_IO_OutG.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutG.shAL_Out = m_IO_OutG.shAL_Out;
	}
	if(m_OldIO_OutH.shAL_Out != m_IO_OutH.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_H1, m_IO_OutH.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_H2, (m_IO_OutH.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutH.shAL_Out = m_IO_OutH.shAL_Out;
	}
	if(m_OldIO_OutI.shAL_Out != m_IO_OutI.shAL_Out)
	{
		strCommand.Format("%s=%d", AL_IO_OUTPUT_I1, m_IO_OutI.shAL_Out&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		strCommand.Format("%s=%d", AL_IO_OUTPUT_I2, (m_IO_OutI.shAL_Out>>16)&0x0000FFFF);
		m_fnUmacOnlineCommand(strCommand);
		m_OldIO_OutI.shAL_Out = m_IO_OutI.shAL_Out;
	}
	//////////////////////////////////////////////////////////////////////////
	m_UpdateTime_IO = ::GetTickCount() -UpdateTimeBuf;
	return true;
}
void CMotCtrl::GetInputData(DWORD *pBlock1)
{
	pBlock1[0] = m_IO_InA.shAL_IN;
	pBlock1[1] = m_IO_InB.shAL_IN;
	pBlock1[2] = m_IO_InC.shAL_IN;
	pBlock1[3] = m_IO_InD.shAL_IN;
	pBlock1[4] = m_IO_InE.shAL_IN;
	pBlock1[5] = m_IO_InF.shAL_IN;
	pBlock1[6] = m_IO_InG.shAL_IN;
	pBlock1[7] = m_IO_InH.shAL_IN;
	pBlock1[8] = m_IO_InI.shAL_IN;
}
void CMotCtrl::GetOutputData(DWORD *pBlock1)
{
	pBlock1[0] = m_IO_OutA.shAL_Out;
	pBlock1[1] = m_IO_OutB.shAL_Out;
	pBlock1[2] = m_IO_OutC.shAL_Out;
	pBlock1[3] = m_IO_OutD.shAL_Out;
	pBlock1[4] = m_IO_OutE.shAL_Out;
	pBlock1[5] = m_IO_OutF.shAL_Out;
	pBlock1[6] = m_IO_OutG.shAL_Out;
	pBlock1[7] = m_IO_OutH.shAL_Out;
	pBlock1[8] = m_IO_OutI.shAL_Out;
}
void CMotCtrl::SetOutputData(DWORD *pBlock1)
{
	m_IO_OutA.shAL_Out	=	pBlock1[0];
	m_IO_OutB.shAL_Out	=	pBlock1[1];
	m_IO_OutC.shAL_Out	=	pBlock1[2];
	m_IO_OutD.shAL_Out	=	pBlock1[3];
	m_IO_OutE.shAL_Out	=	pBlock1[4];
	m_IO_OutF.shAL_Out	=	pBlock1[5];
	m_IO_OutG.shAL_Out	=	pBlock1[6];
	m_IO_OutH.shAL_Out	=	pBlock1[7];
	m_IO_OutI.shAL_Out		=	pBlock1[8];
}
bool CMotCtrl::m_fnUpdateLocalMotState()
{
	//int ReadIndex = 0;
	//for(int SStep = 0; SStep<8; SStep++)
	//{
	//	strMValue.Format("%c%c%c%02d", AXIS_CRUISER_STATUS_A1[0], AXIS_CRUISER_STATUS_A1[1], AXIS_CRUISER_STATUS_A1[2], ReadIndex++);
	//	m_LocalAxisST_Old[SStep].m_AxisStA.MotStatus = m_LocalAxisST[SStep].m_AxisStA.MotStatus;
	//	strStatus = m_fnUmacOnlineCommand(strMValue);	
	//	m_LocalAxisST[SStep].m_AxisStA.MotStatus = (DWORD)atoi(strStatus);

	//	strMValue.Format("%c%c%c%02d", AXIS_CRUISER_STATUS_A1[0], AXIS_CRUISER_STATUS_A1[1], AXIS_CRUISER_STATUS_A1[2], ReadIndex++);
	//	m_LocalAxisST_Old[SStep].m_AxisStB.MotStatus = m_LocalAxisST[SStep].m_AxisStB.MotStatus;
	//	strStatus = m_fnUmacOnlineCommand(strMValue);
	//	m_LocalAxisST[SStep].m_AxisStB.MotStatus = (DWORD)atoi(strStatus);
	//}

	//CString strSendMotState;
	//strSendMotState.Format("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s ",
	//	AXIS_CRUISER_STATUS_A1,
	//	AXIS_CRUISER_STATUS_B1,	
	//	AXIS_CRUISER_STATUS_A2,	
	//	AXIS_CRUISER_STATUS_B2,	
	//	AXIS_CRUISER_STATUS_A3,	
	//	AXIS_CRUISER_STATUS_B3,	
	//	AXIS_CRUISER_STATUS_A4,	
	//	AXIS_CRUISER_STATUS_B4,	
	//	AXIS_CRUISER_STATUS_A5,	
	//	AXIS_CRUISER_STATUS_B5,	
	//	AXIS_CRUISER_STATUS_A6,	
	//	AXIS_CRUISER_STATUS_B6,	
	//	AXIS_CRUISER_STATUS_A7,	
	//	AXIS_CRUISER_STATUS_B7,	
	//	AXIS_CRUISER_STATUS_A8,	
	//	AXIS_CRUISER_STATUS_B8);
	//CString strRetMotState;
	//CStringArray strNewReadValue;
	//strRetMotState.Format("%s", m_fnUmacOnlineCommand(strSendMotState));

	//CString resToken; 
	//int curPos= 0; 
	//resToken= strRetMotState.Tokenize(" ",curPos); 
	//for(int PStep = 0; resToken != "" ; PStep++)
	//{ 
	//	strNewReadValue.Add(resToken);
	//	resToken= strRetMotState.Tokenize(" ",curPos); 
	//}
	////Block으로 읽으면 ㅡ,.ㅡ. 구분 없이 블럭으로 들어온다 .....
	//	strRetMotState	"86508162568650816256871636825687163682568650816256871635425626214402621440"
	DWORD UpdateTimeBuf =  ::GetTickCount();
	
	int MotStep = 0;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A1));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B1));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A2));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B2));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A3));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B3));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A4));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B4));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A5));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B5));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A6));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B6));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A7));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B7));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A8));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B8));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A30));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B30));
	MotStep++;
	m_LocalAxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_A31));
	m_LocalAxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_CRUISER_STATUS_B31));
	MotStep++;
	m_UpdateTime_MotState = ::GetTickCount() - UpdateTimeBuf;
	return true;
}

bool CMotCtrl::m_fnUpdateML3MotState()
{

	//int ReadIndex = 0;
	//for(int SStep = 0; SStep<13; SStep++)
	//{
	//	strMValue.Format("%c%c%c%02d", AXIS_ML3_STATUS_A01[0], AXIS_ML3_STATUS_A01[1], AXIS_ML3_STATUS_A01[2], ReadIndex++);
	//	m_ML3AxisST_Old[SStep].m_AxisStA.MotStatus = m_ML3AxisST[SStep].m_AxisStA.MotStatus;
	//	strStatus = m_fnUmacOnlineCommand(strMValue);	
	//	m_ML3AxisST[SStep].m_AxisStA.MotStatus = (DWORD)atoi(strStatus);

	//	strMValue.Format("%c%c%c%02d", AXIS_ML3_STATUS_A01[0], AXIS_ML3_STATUS_A01[1], AXIS_ML3_STATUS_A01[2], ReadIndex++);
	//	m_ML3AxisST_Old[SStep].m_AxisStB.MotStatus = m_ML3AxisST[SStep].m_AxisStB.MotStatus;
	//	strStatus = m_fnUmacOnlineCommand(strMValue);
	//	m_ML3AxisST[SStep].m_AxisStB.MotStatus = (DWORD)atoi(strStatus);
	//}
	DWORD UpdateTimeBuf =  ::GetTickCount();
	int MotStep = 0;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A01));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B01));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A02));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B02));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A03));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B03));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A04));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B04));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A05));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B05));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A06));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B06));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A07));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B07));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A08));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B08));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A09));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B09));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A10));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B10));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A11));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B11));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A12));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B12));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A13));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B13));
	MotStep++;
	m_ML3AxisST[MotStep].m_AxisStA.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_A14));
	m_ML3AxisST[MotStep].m_AxisStB.MotStatus = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_ML3_STATUS_B14));
	MotStep++;

	//case AXIS_BT01_CassetteUpDown://ML3 #22
	if(G_MotCtrlObj.m_ML3AxisST[12].m_AxisStB.St00_InPosition == 0 &&//구동중.
		G_MotCtrlObj.m_ML3AxisST[12].m_AxisStA.St18_OpenLoopMode != 1)//Open Loop가 아닐때
	{
		//BT01 Coupling Check
		if(abs(m_BT01CouplingCnt[0]-m_BT01CouplingCnt[1])>BT01_Coupling_OverCnt_Limit)
		{//Error
			G_WriteInfo(Log_Error, "BT01 Coupling Error (L:%d, R:%d)", m_BT01CouplingCnt[0], m_BT01CouplingCnt[1]);
			m_BT01CouplingCnt[0] = m_BT01CouplingCnt[1] =0;
		}
	}
	m_UpdateTime_ML3State =  ::GetTickCount() - UpdateTimeBuf;
	return true;
}
bool CMotCtrl::m_fnUpdateAxisLinkMotState()
{
	DWORD UpdateTimeBuf =  ::GetTickCount();
	CString strMValue;
	DWORD ReadValue = 0;
	int StepIndex  = 0;
	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_A_X_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_A_Y_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_A_Z_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_A_U_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	//////////////////////////////////////////////////////////////////////////
	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_B_X_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_B_Y_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_B_Z_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_B_U_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;


	//////////////////////////////////////////////////////////////////////////
	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_C_X_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_C_Y_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;


	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_C_Z_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_C_U_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	//////////////////////////////////////////////////////////////////////////
	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_D_X_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_D_Y_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;


	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_D_Z_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;


	ReadValue = atoi(m_fnUmacOnlineCommand(P_B00_D_U_STS));
	m_ALinkAxisST[StepIndex].MotStatus = (unsigned short)(ReadValue & 0x0000FFFF);
	StepIndex++;

	m_UpdateTime_ALinkState =  ::GetTickCount() - UpdateTimeBuf;
	return true;
}
bool CMotCtrl::m_fnUpdateAxisPosition()
{
	DWORD UpdateTimeBuf =  ::GetTickCount();
	m_AxisNowPos[AXIS_TT01_TableDrive]					=	(int)(atoi(m_fnUmacOnlineCommand("#1P"))/Cruiser_Encoder_DivValue1);	//Cruiser Axis #1
	//m_AxisNowPos[AXIS_TT01_TableDrive]					=	atoi(m_fnUmacOnlineCommand("#1P"));	//Cruiser Axis #1


	m_AxisNowPos[AXIS_TT01_ThetaAlign]				=	atoi(m_fnUmacOnlineCommand("#18P"));	//ML3 #18
	m_AxisNowPos[AXIS_TT01_TableTurn]					=	atoi(m_fnUmacOnlineCommand("#19P"));	//ML3 #19

	//BT01
	m_AxisNowPos[AXIS_BT01_CassetteUpDown]		=	atoi(m_fnUmacOnlineCommand("#22P"));	//ML3 #22
	//LT01
	m_AxisNowPos[AXIS_LT01_BoxLoadPush]				=	atoi(m_fnUmacOnlineCommand("#14P"));	//ML3 #14
	//BO01
	//기준 축만 표시
	m_AxisNowPos[AXIS_BO01_BoxRotate]					=	atoi(m_fnUmacOnlineCommand("#10P"));	//ML3 #10, #11 //좌우 동기축.
	//m_AxisNowPos[AXIS_BO01_BoxRotate]					=	atoi(m_fnUmacOnlineCommand("#11P"));
	//기준 축만 표시
	m_AxisNowPos[AXIS_BO01_BoxUpDn]					=	atoi(m_fnUmacOnlineCommand("#12P"));	//ML3 #12, #13 //좌우 동기축.
	//m_AxisNowPos[AXIS_BO01_BoxUpDn]					=	atoi(m_fnUmacOnlineCommand("#13P"));
	m_AxisNowPos[AXIS_BO01_BoxDrive]				=	atoi(m_fnUmacOnlineCommand("#16P"));	//ML3 #16
	//UT01
	m_AxisNowPos[AXIS_UT01_BoxUnLoadPush]			=	atoi(m_fnUmacOnlineCommand("#15P"));	//ML3 #15
	//TM01
	m_AxisNowPos[AXIS_TM01_ForwardBackward]		=	atoi(m_fnUmacOnlineCommand("#17P"));	//ML3 #17

	//TM02
	////m_AxisNowPos[AXIS_TM02_PickerDrive]				=	(int)(atoi(m_fnUmacOnlineCommand("#31P"))/Cruiser_Encoder_DivValue);	//좌우 Gantry구동 #31가상축
	//m_AxisNowPos[AXIS_TM02_PickerDrive]				=	(int)(atoi(m_fnUmacOnlineCommand("#7P"))/Cruiser_Encoder_DivValue);	//Cruiser Axis #7, #8 //좌우 Gantry구동 #31
	////m_AxisNowPos[AXIS_TM02_PickerDrive]				=	(int)(atoi(m_fnUmacOnlineCommand("#8P"))/Cruiser_Encoder_DivValue);;

	m_AxisNowPos[AXIS_TM02_PickerDrive]				=	atoi(m_fnUmacOnlineCommand("#31P"));	//좌우 Gantry구동 #31가상축
	//m_AxisNowPos[AXIS_TM02_PickerDrive]				=	atoi(m_fnUmacOnlineCommand("#7P"));	//Cruiser Axis #7, #8 //좌우 Gantry구동 #31
	//m_AxisNowPos[AXIS_TM02_PickerDrive]				=	atoi(m_fnUmacOnlineCommand("#8P"));


	m_AxisNowPos[AXIS_TM02_PickerUpDown]			=	atoi(m_fnUmacOnlineCommand("#20P"));	//ML3 #20
	m_AxisNowPos[AXIS_TM02_PickerShift]				=	atoi(m_fnUmacOnlineCommand("#21P"));	//ML3 #21
	m_AxisNowPos[AXIS_TM02_PickerRotate]				=	atoi(m_fnUmacOnlineCommand(P_B00_A_Y_INCUN));	//AxisLink_A Y

	//AM01
	m_AxisNowPos[AXIS_AM01_ScanDrive]					=	(int)(atoi(m_fnUmacOnlineCommand("#6P"))/Cruiser_Encoder_DivValue2)-(m_ReviewLensOffsetDrive);	//Cruiser Axis #6
	m_AxisNowPos[AXIS_AM01_ScanShift]					=	(int)(atoi(m_fnUmacOnlineCommand("#2P"))/Cruiser_Encoder_DivValue1)-(m_ReviewLensOffsetShift);	//Cruiser Axis #2
	m_AxisNowPos[AXIS_AM01_ScanUpDn]					=	atoi(m_fnUmacOnlineCommand(P_B00_D_X_INCUN));	//AxisLink_D X
	m_AxisNowPos[AXIS_AM01_SpaceSnUpDn]			=	atoi(m_fnUmacOnlineCommand(P_B00_D_Y_INCUN));	//AxisLink_D Y

	m_AxisNowPos[AXIS_AM01_ScopeDrive]   			=	(int)(atoi(m_fnUmacOnlineCommand("#5P"))/Cruiser_Encoder_DivValue2)-(m_ScopeLensOffsetDrive);	//Cruiser Axis #5


	m_AxisNowPos[AXIS_AM01_ScopeShift]				=	(int)(atoi(m_fnUmacOnlineCommand("#30P"))/Cruiser_Encoder_DivValue1)-(m_ScopeLensOffsetShift);;	//좌우 Gantry구동 #30가상축
	//m_AxisNowPos[AXIS_AM01_ScopeShift]				=	(int)(atoi(m_fnUmacOnlineCommand("#3P"))/Cruiser_Encoder_DivValue1)-(m_ScopeLensOffsetShift);	//Cruiser Axis #3, #4 //좌우 Gantry구동
	//m_AxisNowPos[AXIS_AM01_ScopeShift]				=	(int)(atoi(m_fnUmacOnlineCommand("#4P"))/Cruiser_Encoder_DivValue);

	m_AxisNowPos[AXIS_AM01_ScopeUpDn]				=	atoi(m_fnUmacOnlineCommand(P_B00_D_Z_INCUN));	//AxisLink_D Z
	m_AxisNowPos[AXIS_AM01_AirBlowLeftRight]			=	atoi(m_fnUmacOnlineCommand(P_B00_D_U_INCUN));	//AxisLink_D U

	m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]		=	atoi(m_fnUmacOnlineCommand(P_B00_A_Z_INCUN));	//AxisLink_A Z
	m_AxisNowPos[AXIS_AM01_TensionRightUpDn]		=	atoi(m_fnUmacOnlineCommand(P_B00_A_U_INCUN));	//AxisLink_A U
	m_AxisNowPos[AXIS_AM01_LTension1]					=	atoi(m_fnUmacOnlineCommand(P_B00_B_X_INCUN));	//AxisLink_B X
	m_AxisNowPos[AXIS_AM01_LTension2]					=	atoi(m_fnUmacOnlineCommand(P_B00_B_Y_INCUN));	//AxisLink_B Y
	m_AxisNowPos[AXIS_AM01_LTension3]					=	atoi(m_fnUmacOnlineCommand(P_B00_B_Z_INCUN));	//AxisLink_B Z
	m_AxisNowPos[AXIS_AM01_LTension4]					=	atoi(m_fnUmacOnlineCommand(P_B00_B_U_INCUN));	//AxisLink_B U
	m_AxisNowPos[AXIS_AM01_RTension1]					=	atoi(m_fnUmacOnlineCommand(P_B00_C_X_INCUN));	//AxisLink_C X
	m_AxisNowPos[AXIS_AM01_RTension2]					=	atoi(m_fnUmacOnlineCommand(P_B00_C_Y_INCUN));	//AxisLink_C Y
	m_AxisNowPos[AXIS_AM01_RTension3]					=	atoi(m_fnUmacOnlineCommand(P_B00_C_Z_INCUN));	//AxisLink_C Z
	m_AxisNowPos[AXIS_AM01_RTension4]					=	atoi(m_fnUmacOnlineCommand(P_B00_C_U_INCUN));	//AxisLink_C U
#ifdef MOT_USE_UMAC_JIG_23AXIS
	m_AxisNowPos[AXIS_AM01_JigInOut]		=	atoi(m_fnUmacOnlineCommand("#23P"));	//ML3 #23
#else
	m_AxisNowPos[AXIS_AM01_JigInOut]		= 0;
#endif
	//Master 공유 메모리에 Update하자.
	if(G_MotInfo.m_pMotState != nullptr)
	{
		memcpy(&G_MotInfo.m_pMotState->nMotionPos[0], &m_AxisNowPos[0], sizeof(int)*AXIS_Cnt);
	}

	CString strStatus;
	CString strMValue;
	int ReadIndex = 10;
	for(int SStep = 0; SStep<32; SStep++)
	{
		strMValue.Format("%c%c%c%02d", PLC_00_ACTIVE[0], PLC_00_ACTIVE[1], PLC_00_ACTIVE[2], ReadIndex++);
		strStatus = m_fnUmacOnlineCommand(strMValue);	
		m_PLCProActive[SStep] =  ((DWORD)atoi(strStatus)&0x04) == 0? true : false; //PLC 값이 0이면 활성 화 상태 이다.
	}

	m_UpdateTime_AxisPos = ::GetTickCount() -UpdateTimeBuf;
	return true;
}

void CMotCtrl::m_fnLensOffsetApplyCheck()
{
	CString UmacCMD;

	if(m_bReviewLensOffsetApply == true)
	{
		if((G_MotCtrlObj.m_LocalAxisST[1].m_AxisStA.St18_OpenLoopMode == 0 && //AXIS_AM01_ScanShift //Cruiser Axis #2
			G_MotCtrlObj.m_LocalAxisST[1].m_AxisStB.St10_HomeComplete ==1) &&
			(G_MotCtrlObj.m_LocalAxisST[5].m_AxisStA.St18_OpenLoopMode == 0 && //AXIS_AM01_ScanDrive://Cruiser Axis #6
			G_MotCtrlObj.m_LocalAxisST[5].m_AxisStB.St10_HomeComplete ==1))
		{//Home완료.
			if(G_MotCtrlObj.m_LocalAxisST[1].m_AxisStB.St00_InPosition ==1 &&
				G_MotCtrlObj.m_LocalAxisST[5].m_AxisStB.St00_InPosition ==1)//정지 상태.
			{
				Sleep(10);
				//Apply Value는 Distance 속성임으로 바로 적용 한다.
				int ScanShiftPos = atoi(m_fnUmacOnlineCommand("#2P")) + (int)((double)(m_ReviewLensOffsetShift  - m_ReviewLensOffsetShiftApply_Value)*Cruiser_Encoder_DivValue1);
				int ScanDrivePos = atoi(m_fnUmacOnlineCommand("#6P")) + (m_ReviewLensOffsetDrive - m_ReviewLensOffsetDriveApply_Value)*Cruiser_Encoder_DivValue2;
				UmacCMD.Format("#2J=%d #6J=%d", ScanShiftPos, ScanDrivePos);
				m_fnUmacOnlineCommand(UmacCMD);
				m_ReviewLensOffsetShiftApply_Value = 0;
				m_ReviewLensOffsetDriveApply_Value = 0;
				m_bReviewLensOffsetApply = false;
				G_WriteInfo(Log_Normal, "Scan Lens Offset Apply(%d, %d)", m_ReviewLensOffsetShift, m_ReviewLensOffsetDrive);
			}
			else
			{//구동 정지까지 기다린다.

			}
		}
		else
		{//구동 가능 상태가 아님으로 무시 한다.
			m_ReviewLensOffsetShiftApply_Value = 0;
			m_ReviewLensOffsetDriveApply_Value = 0;
			m_bReviewLensOffsetApply = false;
		}
	}

	if(m_bScopeLensOffsetApply == true)
	{
		if((G_MotCtrlObj.m_LocalAxisST[8].m_AxisStA.St18_OpenLoopMode == 0 && //Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
			G_MotCtrlObj.m_LocalAxisST[8].m_AxisStB.St10_HomeComplete ==1) &&
			(G_MotCtrlObj.m_LocalAxisST[4].m_AxisStA.St18_OpenLoopMode == 0 && //Cruiser Axis #5
			G_MotCtrlObj.m_LocalAxisST[4].m_AxisStB.St10_HomeComplete ==1))
		{//Home완료.
			if(G_MotCtrlObj.m_LocalAxisST[1].m_AxisStB.St00_InPosition ==1 &&
				G_MotCtrlObj.m_LocalAxisST[5].m_AxisStB.St00_InPosition ==1)//정지 상태.
			{
				Sleep(10);
				//Apply Value는 Distance 속성임으로 바로 적용 한다.
				int ScanShiftPos = atoi(m_fnUmacOnlineCommand("#3P")) + (int)((double)(m_ScopeLensOffsetShift  - m_ScopeLensOffsetShiftApply_Value)*Cruiser_Encoder_DivValue1); //Cruiser Axis #3, #4 //좌우 Gantry구동#30가상축
				int ScanDrivePos = atoi(m_fnUmacOnlineCommand("#5P")) + (m_ScopeLensOffsetDrive - m_ScopeLensOffsetDriveApply_Value)*Cruiser_Encoder_DivValue2;//Cruiser Axis #5
				UmacCMD.Format("#30J=%d #5J=%d", ScanShiftPos, ScanDrivePos);//Cruiser Axis #3, #4 //좌우 Gantry구동#30가상축
				m_fnUmacOnlineCommand(UmacCMD);
				m_ScopeLensOffsetShiftApply_Value = 0;
				m_ScopeLensOffsetDriveApply_Value = 0;
				m_bScopeLensOffsetApply = false;

				G_WriteInfo(Log_Normal, "Scope Lens Offset Apply(%d, %d)", m_ScopeLensOffsetShift, m_ScopeLensOffsetDrive);
			}
			else
			{//구동 정지까지 기다린다.

			}
		}
		else
		{//구동 가능 상태가 아님으로 무시 한다.
			m_ScopeLensOffsetShiftApply_Value = 0;
			m_ScopeLensOffsetDriveApply_Value = 0;
			m_bScopeLensOffsetApply = false;
		}
	}
}
bool CMotCtrl::m_fnIOTeachKeyUnlockEnableCheck()//Teach => Auto Mode 전환시 체크.항목.
{
	if((m_IO_InA.In00_EMO1 ==0 && m_IO_InA.In01_EMO2 ==0 && m_IO_InA.In02_EMO3 ==0 && m_IO_InA.In28_EMO4 ==0)&&
		(m_IO_InA.In03_Sn_IDXFrontDoorOpenClose==0 &&
		m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose ==0 &&
		m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose ==0 &&
		m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose ==0 &&
		m_IO_InA.In13_Sn_InspSideDoorRightOpenClose ==0 &&
		m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose ==0 &&
		m_IO_InA.In17_Sn_InspBackDoorRightOpenClose ==0 )&&

		(m_IO_InA.In04_Sn_IDXFrontDoorLock==0 &&
		m_IO_InA.In08_Sn_IDXSideDoorLeftLock ==0 &&
		m_IO_InA.In10_Sn_IDXSideDoorRightLock ==0 &&
		m_IO_InA.In12_Sn_InspSideDoorLeftLock ==0 &&
		m_IO_InA.In14_Sn_InspSideDoorRightLock ==0 &&
		m_IO_InA.In16_Sn_InspBackDoorLeftLock ==0 &&
		m_IO_InA.In18_Sn_InspBackDoorRightLock ==0))
	{
		return true;//전환 가능.(true)
	}
	return false;//전환 불가(false)
}
bool CMotCtrl::m_fnIOMCCUpdateProc()
{
	if(m_OldIO_InA.shAL_IN != m_IO_InA.shAL_IN)//1
	{
	}
	if(m_OldIO_InB.shAL_IN != m_IO_InB.shAL_IN)//2
	{
	}
	if(m_OldIO_InC.shAL_IN != m_IO_InC.shAL_IN)//3
	{
	}
	if(m_OldIO_InD.shAL_IN != m_IO_InD.shAL_IN)//4
	{
	}
	if(m_OldIO_InE.shAL_IN != m_IO_InE.shAL_IN)//5
	{
	}
	if(m_OldIO_InF.shAL_IN != m_IO_InF.shAL_IN)//6
	{
	}
	if(m_OldIO_InG.shAL_IN != m_IO_InG.shAL_IN)//7
	{
	}
	if(m_OldIO_InH.shAL_IN != m_IO_InH.shAL_IN)//8
	{
	}
	if(m_OldIO_InI.shAL_IN != m_IO_InI.shAL_IN)//9
	{
	}
	return true;
}
bool CMotCtrl::m_fnIOScenarioProc()
{
	if(m_IO_InA.In19_AutoTeachMode == 0)//Teach Mode에서.
	{
		if(m_fnIOTeachKeyUnlockEnableCheck() == true)
		{//1)EMO 체크, 2)door Close 체크 3)door Lock 체크(모두 정상이면 Teach Key Unlock을 준다.)
			m_IO_OutA.Out03_TeachKeyUnlock = 1;
			//G_WriteInfo(Log_Normal, "Change Enable (Teach=>Auto Mode)");
		}
		else 
			m_IO_OutA.Out03_TeachKeyUnlock = 0;
	}

	if(m_OldIO_InA.shAL_IN != m_IO_InA.shAL_IN)
	{
		if(m_OldIO_InA.In00_EMO1^m_IO_InA.In00_EMO1)//변화가 생기면
		{
			if(m_IO_InA.In00_EMO1 == 1  )//EMO스위치 눌리면 ON
			{//IO On 
			}
			else if(m_IO_InA.In00_EMO1 == 0)
			{//IO Off 

			}
			else
			{//변화 없음

			}
		}

		if(m_OldIO_InA.In00_EMO1^m_IO_InA.In00_EMO1 || 
			m_OldIO_InA.In01_EMO2^m_IO_InA.In01_EMO2 || 
			m_OldIO_InA.In02_EMO3^m_IO_InA.In02_EMO3 || 
			m_OldIO_InA.In28_EMO4^m_IO_InA.In28_EMO4)
		{
			if(m_IO_InA.In00_EMO1 == 1 || m_IO_InA.In01_EMO2 == 1 || m_IO_InA.In02_EMO3 == 1 || m_IO_InA.In28_EMO4 == 1 )//EMO스위치 눌리면 ON
			{//IO On 시점(하나라도 On되면.
			}
			else if(m_IO_InA.In00_EMO1 == 0 && m_IO_InA.In01_EMO2 == 0 && m_IO_InA.In02_EMO3 == 0 && m_IO_InA.In28_EMO4 == 0)
			{//IO Off 전부 Off되면

				//Teach Mode 에서 감지되면.
				if(m_IO_InA.In19_AutoTeachMode == 0)//Teach Mode에서.
				{
					if(m_fnIOTeachKeyUnlockEnableCheck() == true)
					{//1)EMO 체크, 2)door Close 체크 3)door Lock 체크(모두 정상이면 Teach Key Unlock을 준다.)
						m_IO_OutA.Out03_TeachKeyUnlock = 1;
						G_WriteInfo(Log_Normal, "Change Enable (Teach=>Auto Mode)");
					}
				}
			}
			else
			{//변화 없음

			}
		}
		if((m_OldIO_InA.In04_Sn_IDXFrontDoorLock^m_IO_InA.In04_Sn_IDXFrontDoorLock)||
			(m_OldIO_InA.In08_Sn_IDXSideDoorLeftLock^m_IO_InA.In08_Sn_IDXSideDoorLeftLock))//변화가 생기면
		{
			if(m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 0 && m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 0 && m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 0)//EMO스위치 눌리면 ON
			{//IO On 전부 잠김 설정되면.
				//Teach Mode 에서 감지되면.
				if(m_IO_InA.In19_AutoTeachMode == 0)//Teach Mode에서.
				{
					if(m_fnIOTeachKeyUnlockEnableCheck() == true)
					{//1)EMO 체크, 2)door Close 체크 3)door Lock 체크(모두 정상이면 Teach Key Unlock을 준다.)
						m_IO_OutA.Out03_TeachKeyUnlock = 1;
						G_WriteInfo(Log_Normal, "Change Enable (Teach=>Auto Mode)");
					}
				}
			}
			else if(m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 1 || m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 1 || m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 1)
			{//IO Off //하나라도 잠김 해지되면

			}
			else
			{//변화 없음

			}
		}

		if((m_OldIO_InA.In03_Sn_IDXFrontDoorOpenClose^m_IO_InA.In03_Sn_IDXFrontDoorOpenClose)||
			(m_OldIO_InA.In07_Sn_IDXSideDoorLeftOpenClose^m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose)||
			(m_OldIO_InA.In09_Sn_IDXSideDoorRightOpenClose^m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose)||
			(m_OldIO_InA.In11_Sn_InspSideDoorLeftOpenClose^m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose)||
			(m_OldIO_InA.In13_Sn_InspSideDoorRightOpenClose^m_IO_InA.In13_Sn_InspSideDoorRightOpenClose)||
			(m_OldIO_InA.In15_Sn_InspBackDoorLeftOpenClose^m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose)||
			(m_OldIO_InA.In17_Sn_InspBackDoorRightOpenClose^m_IO_InA.In17_Sn_InspBackDoorRightOpenClose))//변화가 생기면
		{
			if(m_IO_InA.In03_Sn_IDXFrontDoorOpenClose == 0 && 
				m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose == 0&&
				m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose == 0 && 
				m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose == 0 && 
				m_IO_InA.In13_Sn_InspSideDoorRightOpenClose == 0 &&
				m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose == 0 && 
				m_IO_InA.In17_Sn_InspBackDoorRightOpenClose == 0 )//EMO스위치 눌리면 ON
			{//IO On 전부 닫히면 
				//Teach Mode 에서 감지되면.
				if(m_IO_InA.In19_AutoTeachMode == 0)//Teach Mode에서.
				{
					if(m_fnIOTeachKeyUnlockEnableCheck() == true)
					{//1)EMO 체크, 2)door Close 체크 3)door Lock 체크(모두 정상이면 Teach Key Unlock을 준다.)
						m_IO_OutA.Out03_TeachKeyUnlock = 1;
						G_WriteInfo(Log_Normal, "Change Enable (Teach=>Auto Mode)");
					}
				}
			}
			else if(m_IO_InA.In03_Sn_IDXFrontDoorOpenClose == 1 || m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose == 0 ||
				m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose == 1 || m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose == 1 || m_IO_InA.In13_Sn_InspSideDoorRightOpenClose == 0 ||
				m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose == 1 || m_IO_InA.In17_Sn_InspBackDoorRightOpenClose == 1 )
			{//IO Off //하나라도 열리면

			}
			else
			{//변화 없음

			}
		}

		if(m_OldIO_InA.In19_AutoTeachMode^m_IO_InA.In19_AutoTeachMode)
		{
			if(m_IO_InA.In19_AutoTeachMode == 1)//Auto Mode로 진입.
			{//IO On 시점
				//Unlock Enable을 꺼준다 전환 되면.
				m_IO_OutA.Out03_TeachKeyUnlock = 0;
				G_WriteInfo(Log_Normal, "Change (Teach=>Auto Mode)");
				//1)Speed Down.(100mm/sec) 
				m_fnSetMotSpeed(Speed_Normal);
				
			}
			else if(m_IO_InA.In19_AutoTeachMode == 0)//Teach Mode로 진입.
			{//IO Off 시점(
				G_WriteInfo(Log_Normal, "Change (Auto=>Teach Mode)");
			//1) Door Open 가능 

			//2)구동중이던 Mot Stop,
				m_fnMotAllStop();
			//3)Speed Down.(100mm/sec) 
				m_fnSetMotSpeed(Speed_Teach);

			}
			else
			{//변화 없음

			}
		}
		if(m_OldIO_InA.In29_TeachMoveEnable^m_IO_InA.In29_TeachMoveEnable)//변화가 생기면
		{
			if(m_IO_InA.In29_TeachMoveEnable == 1  )//Motion 이동가능.
			{//IO On 
			}
			else if(m_IO_InA.In29_TeachMoveEnable == 0 && m_IO_InA.In19_AutoTeachMode == 0)//Motion 이동 불가. All Stop
			{//IO Off 
				//1)구동중이던 Mot Stop,
				m_fnMotAllStop();
			}
			else
			{//변화 없음

			}
		}
		//////////////////////////////////////////////////////////////////////////
		////Door 동작. Front Left Right가 닫히면 Release를 0로 만들어 잠김 상태로 바꾼다.(2개)
		//if((m_OldIO_InA.In04_Sn_IDXFrontDoorLeftLock^m_IO_InA.In04_Sn_IDXFrontDoorLeftLock)||
		//	(m_OldIO_InA.In06_Sn_IDXFrontDoorRightLock^m_IO_InA.In06_Sn_IDXFrontDoorRightLock))
		//{
		//	if(m_IO_InA.In04_Sn_IDXFrontDoorLeftLock == 1 && m_IO_InA.In06_Sn_IDXFrontDoorRightLock == 1)
		//	{//IO On 시점
		//		m_IO_OutA.Out00_IndexerFrontDoorRelease = 0;
		//	}
		//	//Release On은 UI에서 수행 한다.
		//}
		////Door 동작. Side Left Right가 닫히면 Release를 0로 만들어 잠김 상태로 바꾼다.(4개)
		//if((m_OldIO_InA.In08_Sn_IDXSideDoorLeftLock^m_IO_InA.In08_Sn_IDXSideDoorLeftLock)||
		//	(m_OldIO_InA.In10_Sn_IDXSideDoorRightLock^m_IO_InA.In10_Sn_IDXSideDoorRightLock)||
		//	(m_OldIO_InA.In12_Sn_InspSideDoorLeftLock^m_IO_InA.In12_Sn_InspSideDoorLeftLock)||
		//	(m_OldIO_InA.In14_Sn_InspSideDoorRightLock^m_IO_InA.In14_Sn_InspSideDoorRightLock))
		//{
		//	if(m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 1 && 
		//		m_IO_InA.In10_Sn_IDXSideDoorRightLock == 1 &&
		//		m_IO_InA.In12_Sn_InspSideDoorLeftLock == 1 &&
		//		m_IO_InA.In14_Sn_InspSideDoorRightLock == 1)
		//	{//IO On 시점
		//		m_IO_OutA.Out01_SideDoorAllRelease = 0;
		//	}
		//	//Release On은 UI에서 수행 한다.
		//}

		////Door 동작. Back Left Right가 닫히면 Release를 0로 만들어 잠김 상태로 바꾼다.(2개)
		//if((m_OldIO_InA.In16_Sn_InspBackDoorLeftLock^m_IO_InA.In16_Sn_InspBackDoorLeftLock)||
		//	(m_OldIO_InA.In18_Sn_InspBackDoorRightLock^m_IO_InA.In18_Sn_InspBackDoorRightLock))
		//{
		//	if(m_IO_InA.In16_Sn_InspBackDoorLeftLock == 1 && m_IO_InA.In18_Sn_InspBackDoorRightLock == 1)
		//	{//IO On 시점
		//		m_IO_OutA.Out02_InspectorBackDoorRelease = 0;
		//	}
		//	//Release On은 UI에서 수행 한다.
		//}
		if(m_OldIO_InA.In05_Sn_BT01CouplingLeft^m_IO_InA.In05_Sn_BT01CouplingLeft)//변화가 생기면
		{
			if(m_IO_InA.In05_Sn_BT01CouplingLeft == 1  )//Motion 이동가능.
			{//IO On 
				m_BT01CouplingCnt[0]++;
			}
			else if(m_IO_InA.In05_Sn_BT01CouplingLeft == 0 )//Motion 이동 불가. All Stop
			{//IO Off 
			}
			else
			{//변화 없음

			}
		}

		if(m_OldIO_InA.In06_Sn_BT01CouplingRight^m_IO_InA.In06_Sn_BT01CouplingRight)//변화가 생기면
		{
			if(m_IO_InA.In06_Sn_BT01CouplingRight == 1  )//Motion 이동가능.
			{//IO On 
				m_BT01CouplingCnt[1]++;
			}
			else if(m_IO_InA.In06_Sn_BT01CouplingRight == 0 )//Motion 이동 불가. All Stop
			{//IO Off 
			}
			else
			{//변화 없음

			}
		}
	}
	if(m_OldIO_InB.shAL_IN != m_IO_InB.shAL_IN)
	{

	}
	if(m_OldIO_InC.shAL_IN != m_IO_InC.shAL_IN)
	{

	}
	if(m_OldIO_InD.shAL_IN != m_IO_InD.shAL_IN)
	{

	}
	if(m_OldIO_InE.shAL_IN != m_IO_InE.shAL_IN)
	{

		if(m_OldIO_InE.In12_Sn_PickerZAxisClashPrevent^m_IO_InE.In12_Sn_PickerZAxisClashPrevent)//변화가 생기면
		{
			if(m_IO_InE.In12_Sn_PickerZAxisClashPrevent == 1  )//
			{//IO On 
				//정상
			}
			else if(m_IO_InE.In12_Sn_PickerZAxisClashPrevent == 0)
			{//IO Off 
				//축정지
				CString strStopCmd;
				strStopCmd.Format("#20J/ #31J/");
				m_fnUmacOnlineCommand(strStopCmd);
			}
			else
			{//변화 없음

			}
		}



	}
	if(m_OldIO_InF.shAL_IN != m_IO_InF.shAL_IN)
	{

	}
	if(m_OldIO_InG.shAL_IN != m_IO_InG.shAL_IN)
	{

	}
	if(m_OldIO_InH.shAL_IN != m_IO_InH.shAL_IN)
	{

	}
	if(m_OldIO_InI.shAL_IN != m_IO_InI.shAL_IN)
	{

	}
	//New IO Data Old로 BackUp
	m_OldIO_InA.shAL_IN = m_IO_InA.shAL_IN;
	m_OldIO_InB.shAL_IN = m_IO_InB.shAL_IN;
	m_OldIO_InC.shAL_IN = m_IO_InC.shAL_IN;
	m_OldIO_InD.shAL_IN = m_IO_InD.shAL_IN;
	m_OldIO_InE.shAL_IN = m_IO_InE.shAL_IN;
	m_OldIO_InF.shAL_IN = m_IO_InF.shAL_IN;
	m_OldIO_InG.shAL_IN = m_IO_InG.shAL_IN;
	m_OldIO_InH.shAL_IN = m_IO_InH.shAL_IN;
	m_OldIO_InI.shAL_IN = m_IO_InI.shAL_IN;




	//if(m_OldIO_OutA.shAL_Out != m_IO_OutA.shAL_Out)
	//{
	//	if(m_OldIO_OutA.Out00_IndexerFrontDoorRelease^m_IO_OutA.Out00_IndexerFrontDoorRelease)//변화가 생기면
	//	{
	//		if(m_IO_OutA.Out00_IndexerFrontDoorRelease == 1  )//EMO스위치 눌리면 ON
	//		{//Ouput On (Door unlock)
	//		}
	//		else if(m_IO_OutA.Out00_IndexerFrontDoorRelease == 0)
	//		{//Ouput Off 

	//		}
	//		else
	//		{//변화 없음

	//		}
	//	}

	//	if(m_OldIO_OutA.Out01_SideDoorAllRelease^m_IO_OutA.Out01_SideDoorAllRelease)//변화가 생기면
	//	{
	//		if(m_IO_OutA.Out01_SideDoorAllRelease == 1  )//EMO스위치 눌리면 ON
	//		{//Ouput On (Door unlock)
	//		}
	//		else if(m_IO_OutA.Out01_SideDoorAllRelease == 0)
	//		{//Ouput Off 

	//		}
	//		else
	//		{//변화 없음

	//		}
	//	}

	//	if(m_OldIO_OutA.Out02_InspectorBackDoorRelease^m_IO_OutA.Out02_InspectorBackDoorRelease)//변화가 생기면
	//	{
	//		if(m_IO_OutA.Out02_InspectorBackDoorRelease == 1  )//EMO스위치 눌리면 ON
	//		{//Ouput On (Door unlock)
	//		}
	//		else if(m_IO_OutA.Out02_InspectorBackDoorRelease == 0)
	//		{//Ouput Off 

	//		}
	//		else
	//		{//변화 없음

	//		}
	//	}
	//}




	return true;
}

//Signal Tower
int CMotCtrl::m_fnSignalTowerSet(STWOER_MODE STMode)
{
	
	if(m_SignalTowerMode != STMode)
	{
		if(m_bBuzzerOFF == true)//Mode 전환시 Buzzer Off를 Reset한다.
			m_bBuzzerOFF = false;

		//우선순위.(9>5>4>8>3>7>1>6>2)
		if(STMode>m_SignalTowerMode)//우선순위가 높은것으로 유지.
		{
			m_SignalTowerMode = STMode;
			m_fnSignalTModeCheck();
			return 1;
		}
		return 2;
	}
	return 0;
}
void CMotCtrl::m_fnSignalTModeCheck()//Timer에서 1초단위로 호출 한다.
{

	//우선순위.(9>5>4>8>3>7>1>6>2)
	switch(m_SignalTowerMode)
	{
	case STWOER_MODE0_NONE:
		{
			
			//if(m_IO_OutA.Out08_SignalTower_Red		== 1)
			//{
			//	m_IO_OutA.Out08_SignalTower_Red		= 0;
			//	m_IO_OutA.Out09_SignalTower_Yellow		= 1;
			//	m_IO_OutA.Out10_SignalTower_Green		= 0;
			//}
		 //   else if(m_IO_OutA.Out09_SignalTower_Yellow		== 1)
			//{
			//	m_IO_OutA.Out08_SignalTower_Red		= 0;
			//	m_IO_OutA.Out09_SignalTower_Yellow		= 0;
			//	m_IO_OutA.Out10_SignalTower_Green		= 1;
			//}
			// else if(m_IO_OutA.Out10_SignalTower_Green		== 1)
			//{
			//	m_IO_OutA.Out08_SignalTower_Red		= 1;
			//	m_IO_OutA.Out09_SignalTower_Yellow		= 0;
			//	m_IO_OutA.Out10_SignalTower_Green		= 0;
			//}

			// else if(m_IO_OutA.Out08_SignalTower_Red		== 0 &&
			//	m_IO_OutA.Out09_SignalTower_Yellow		== 0 &&
			//	m_IO_OutA.Out10_SignalTower_Green		== 0)
			//{
			//	m_IO_OutA.Out08_SignalTower_Red		= 1;
			//	m_IO_OutA.Out09_SignalTower_Yellow		= 0;
			//	m_IO_OutA.Out10_SignalTower_Green		= 0;
			//}
			//m_IO_OutA.Out08_SignalTower_Red		= 1;
			//m_IO_OutA.Out09_SignalTower_Yellow		= 1;
			//m_IO_OutA.Out10_SignalTower_Green		= 1;


			if(m_IO_OutA.Out09_SignalTower_Yellow		== 0)
			{
				m_IO_OutA.Out08_SignalTower_Red		= 0;
				m_IO_OutA.Out09_SignalTower_Yellow		= 1;
				m_IO_OutA.Out10_SignalTower_Green		= 0;
			}
			else
			{
				m_IO_OutA.Out08_SignalTower_Red		= 1;
				m_IO_OutA.Out09_SignalTower_Yellow		= 0;
				m_IO_OutA.Out10_SignalTower_Green		= 0;
			}
			m_IO_OutA.Out11_Buzzer1						= 0;
			m_IO_OutA.Out12_Buzzer2						= 0;
		}break;
	case STWOER_MODE1_RUN: //설비가 정상(생산 상태) 
		{
			m_IO_OutA.Out08_SignalTower_Red		= 0;				
			m_IO_OutA.Out09_SignalTower_Yellow		= 0;					
			m_IO_OutA.Out10_SignalTower_Green		= 1;						
			m_IO_OutA.Out11_Buzzer1						= 0;			
			m_IO_OutA.Out12_Buzzer2						= 0;	

		}break;
	case STWOER_MODE2_IDLE1:		//설비가 정상(Glass가 없을때	)
		{
			m_IO_OutA.Out08_SignalTower_Red		= 0;				
			m_IO_OutA.Out09_SignalTower_Yellow		= 1;					
			m_IO_OutA.Out10_SignalTower_Green		= 0;						
			m_IO_OutA.Out11_Buzzer1						= 0;			
			m_IO_OutA.Out12_Buzzer2						= 0;	
		}break;
	case STWOER_MODE3_IDLE2:		//설비가 정상(투입 중지 상태 일때)
		{
			m_IO_OutA.Out08_SignalTower_Red		= 0;				
			m_IO_OutA.Out09_SignalTower_Yellow		= 1;					
			m_IO_OutA.Out10_SignalTower_Green		= 1;						
			m_IO_OutA.Out11_Buzzer1						= 0;			
			m_IO_OutA.Out12_Buzzer2						= 0;	
		}break;
	case STWOER_MODE4_ALARM:	//설비는 기동 상태(경알람)	
		{
			if(m_IO_OutA.Out08_SignalTower_Red == 1)//점멸
				m_IO_OutA.Out08_SignalTower_Red = 0;
			else
				m_IO_OutA.Out08_SignalTower_Red = 1;

			m_IO_OutA.Out09_SignalTower_Yellow		= 0;
			m_IO_OutA.Out10_SignalTower_Green		= 1;

			if(m_bBuzzerOFF == true)
			{
				m_IO_OutA.Out11_Buzzer1						= 0;
				m_IO_OutA.Out12_Buzzer2						= 0;
			}
			else
			{
				if(m_IO_OutA.Out11_Buzzer1 == 1)//점멸
					m_IO_OutA.Out11_Buzzer1 = 0;
				else
					m_IO_OutA.Out11_Buzzer1 = 1;
				m_IO_OutA.Out12_Buzzer2						= 0;
			}
		}break;
	case STWOER_MODE5_FAULT:		//설비는 정지 상태(생산 불가능 상태)
		{
			m_IO_OutA.Out08_SignalTower_Red		= 1;
			m_IO_OutA.Out09_SignalTower_Yellow		= 0;
			m_IO_OutA.Out10_SignalTower_Green		= 0;

			if(m_bBuzzerOFF == true)
			{
				m_IO_OutA.Out11_Buzzer1						= 0;
				m_IO_OutA.Out12_Buzzer2						= 0;
			}
			else
			{
				m_IO_OutA.Out11_Buzzer1						= 1;
				m_IO_OutA.Out12_Buzzer2						= 0;
			}
		}break;
	case STWOER_MODE6_CST:	//설비는 정상 상태(CST 배출 요청)
		{
			m_IO_OutA.Out08_SignalTower_Red		= 0;
			if(m_IO_OutA.Out09_SignalTower_Yellow == 1)//점멸
				m_IO_OutA.Out09_SignalTower_Yellow = 0;
			else
				m_IO_OutA.Out09_SignalTower_Yellow = 1;

			m_IO_OutA.Out10_SignalTower_Green		= 1;
			m_IO_OutA.Out11_Buzzer1						= 0;
			m_IO_OutA.Out12_Buzzer2						= 0;
		}break;
	case STWOER_MODE7_PM:			//설비는 PM 상태
		{
			m_IO_OutA.Out08_SignalTower_Red		= 1;
			m_IO_OutA.Out09_SignalTower_Yellow		= 1;
			m_IO_OutA.Out10_SignalTower_Green		= 1;
			m_IO_OutA.Out11_Buzzer1						= 0;
			m_IO_OutA.Out12_Buzzer2						= 0;
		}break;
	case STWOER_MODE8_OPCall: 	//Operator Call 상태( CIM, Loader, EQ 표시)
		{
			if(m_IO_OutA.Out08_SignalTower_Red == 1)//점멸
			{
				m_IO_OutA.Out08_SignalTower_Red		= 0;
				m_IO_OutA.Out09_SignalTower_Yellow		= 0;
				m_IO_OutA.Out10_SignalTower_Green		= 0;
			}
			else
			{
				m_IO_OutA.Out08_SignalTower_Red		= 1;
				m_IO_OutA.Out09_SignalTower_Yellow		= 1;
				m_IO_OutA.Out10_SignalTower_Green		= 1;
			}

			if(m_bBuzzerOFF == true)
			{
				m_IO_OutA.Out11_Buzzer1						= 0;
				m_IO_OutA.Out12_Buzzer2						= 0;
			}
			else
			{
				m_IO_OutA.Out11_Buzzer1						= 1;
				m_IO_OutA.Out12_Buzzer2						= 0;
			}
			//m_IO_OutA.Out11_Buzzer1						= 0;
			//m_IO_OutA.Out12_Buzzer2						= 0;
		}break;
	case STWOER_MODE9_MachineDown: //설비 구동 정지 상태.
		{
			m_IO_OutA.Out08_SignalTower_Red		= 0;
			m_IO_OutA.Out09_SignalTower_Yellow		= 0;
			m_IO_OutA.Out10_SignalTower_Green		= 0;

			if(m_bBuzzerOFF == true)
			{
				m_IO_OutA.Out11_Buzzer1						= 0;
				m_IO_OutA.Out12_Buzzer2						= 0;
			}
			else
			{
				m_IO_OutA.Out11_Buzzer1						= 1;
				m_IO_OutA.Out12_Buzzer2						= 0;
			}
		}break;
		//STWOER_MODE_Cnt;
	default:
		{

		}break;
		//////////////////////////////////////////////////////////////////////////
		
	}
}
void CMotCtrl::m_fnSignalTModeReset(STWOER_MODE STMode)
{
	if(m_SignalTowerMode != STMode)
	{
		if(m_bBuzzerOFF == true)//Mode 전환시 Buzzer Off를 Reset한다.
			m_bBuzzerOFF = false;

			m_SignalTowerMode = STMode;//모드 우선순위와 상관없이 변경.
			m_fnSignalTModeCheck();
	}
}

void  CMotCtrl::m_fnSignalTowerBuzzerOn()
{
	m_bBuzzerOFF = false;
}
void  CMotCtrl::m_fnSignalTowerBuzzerOff()
{
	m_bBuzzerOFF = true;
}
//////////////////////////////////////////////////////////////////////////
//Motion Ctrl (Homing Inter Lock)
bool CMotCtrl::m_fnMotHomeInterlockCheck(int AxisNum)
{
	//구동 가능한 상태 이다.(fals:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.

	CHECK_AXIS_ALL(AxisNum);
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
	case AXIS_TT01_ThetaAlign:
	case AXIS_TT01_TableTurn:
		{
			return m_fnTT01HomeInterlockCheck(AxisNum);
		}break;
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
			return m_fnLT01HomeInterlockCheck(AxisNum);
		}break;
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			return m_fnBT01HomeInterlockCheck(AxisNum);
		}break;
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
	case AXIS_BO01_BoxUpDn://좌우 동기축.
	case AXIS_BO01_BoxDrive:
		{
			return m_fnBO01HomeInterlockCheck(AxisNum);
		}break;
		//UT01
	case AXIS_UT01_BoxUnLoadPush:
		{
			return m_fnUT01HomeInterlockCheck(AxisNum);
		}break;

		//TM01
	case AXIS_TM01_ForwardBackward:
		{
			return m_fnTM01HomeInterlockCheck(AxisNum);
		}break;
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
	case AXIS_TM02_PickerUpDown:
	case AXIS_TM02_PickerShift:
	case AXIS_TM02_PickerRotate:
		{
			return m_fnTM02HomeInterlockCheck(AxisNum);
		}break;
		//AM01
	case AXIS_AM01_ScanDrive:
	case AXIS_AM01_ScanShift:
	case AXIS_AM01_ScanUpDn:
	case AXIS_AM01_SpaceSnUpDn:
	case AXIS_AM01_ScopeDrive:
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
	case AXIS_AM01_ScopeUpDn:
	case AXIS_AM01_AirBlowLeftRight:
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
	case AXIS_AM01_JigInOut://ML3 #23
		{
			return m_fnAM01HomeInterlockCheck(AxisNum);
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//TT01
bool CMotCtrl::m_fnTT01HomeInterlockCheck(int AxisNum)
{
	CHECK_AXIS_TT01(AxisNum);

	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;

	//단축 Home Command  처리시에는 
	//해당 모듈의 해당 축은 한번에 한축만 Home 한다.
	CString strPLCRun;
	strPLCRun.Format("%s", m_fnUmacOnlineCommand(PLC_21_TT01_Home));
	if(strPLCRun == '1')
		return false; 
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
		{
			if(m_IO_InG.In05_Sn_TurnTableUpperPosition == 0 && m_IO_InG.In06_Sn_TurnTableLowerPosition == 0)
			{
				G_WriteInfo(Log_Check,"Turn Table Up/Down Position Sensor Error!!");
				return true;
			}
		}break;
	case AXIS_TT01_ThetaAlign:
		{

		}break;
	case AXIS_TT01_TableTurn:
		{
			//Home기능이 좌표만 읽는것이라서 Interlock 필요 없음
			//if(m_IO_InG.In10_Sn_TurnTableRotateEnablePos == 0)
			//{
			//	G_WriteInfo(Log_Check,"Sensor Error!!");
			//	return true;
			//}
		}break;
	default: 
		return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
//BT01
bool CMotCtrl::m_fnBT01HomeInterlockCheck(int AxisNum)
{
	CHECK_AXIS_BT01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;

	//단축 Home Command  처리시에는 
	//해당 모듈의 해당 축은 한번에 한축만 Home 한다.
	CString strPLCRun;
	strPLCRun.Format("%s", m_fnUmacOnlineCommand(PLC_22_BT01_Home));
	if(strPLCRun == '1')
		return true; 
	switch(AxisNum)
	{
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			if(m_IO_InI.In09_Sn_ForkStandbyPos == 0)
			{
				G_WriteInfo(Log_Check,"BT01 Home Fork Standby Pos Error!!");
				return true;
			}
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//LT01
bool CMotCtrl::m_fnLT01HomeInterlockCheck(int AxisNum)
{
	//구동 가능한 상태 이다.(fals:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
	CHECK_AXIS_LT01(AxisNum);

	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;

	//단축 Home Command  처리시에는 
	//해당 모듈의 해당 축은 한번에 한축만 Home 한다.
	CString strPLCRun;
	strPLCRun.Format("%s", m_fnUmacOnlineCommand(PLC_27_LT01_Home));
	if(strPLCRun == '1')
		return true; 
	switch(AxisNum)
	{
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
		}break;
	default: 
		return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
//UT01
bool CMotCtrl::m_fnUT01HomeInterlockCheck(int AxisNum)
{
	CHECK_AXIS_UT01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;

	//단축 Home Command  처리시에는 
	//해당 모듈의 해당 축은 한번에 한축만 Home 한다.
	CString strPLCRun;
	strPLCRun.Format("%s", m_fnUmacOnlineCommand(PLC_28_UT01_Home));
	if(strPLCRun == '1')
		return true; 

	switch(AxisNum)
	{
		//UT01
	case AXIS_UT01_BoxUnLoadPush:
		{
		}break;
	default: 
		return true;
	}
	return false;
}
	
	

//////////////////////////////////////////////////////////////////////////
//BO01
bool CMotCtrl::m_fnBO01HomeInterlockCheck(int AxisNum)
{
	CHECK_AXIS_BO01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	//단축 Home Command  처리시에는 
	//해당 모듈의 해당 축은 한번에 한축만 Home 한다.
	CString strPLCRun;
	strPLCRun.Format("%s", m_fnUmacOnlineCommand(PLC_23_BO01_Home));
	if(strPLCRun == '1')
		return true; 
	switch(AxisNum)
	{
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
		{

		}break;
	case AXIS_BO01_BoxUpDn://좌우 동기축.
		{
		}break;

	case AXIS_BO01_BoxDrive:
		{
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//TM01
bool CMotCtrl::m_fnTM01HomeInterlockCheck(int AxisNum)
{
	CHECK_AXIS_TM01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	//단축 Home Command  처리시에는 
	//해당 모듈의 해당 축은 한번에 한축만 Home 한다.
	CString strPLCRun;
	strPLCRun.Format("%s", m_fnUmacOnlineCommand(PLC_24_TM01_Home));
	if(strPLCRun == '1')
		return true; 

	switch(AxisNum)
	{
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//TM02
bool CMotCtrl::m_fnTM02HomeInterlockCheck(int AxisNum)
{
	CHECK_AXIS_TM02(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	//단축 Home Command  처리시에는 
	//해당 모듈의 해당 축은 한번에 한축만 Home 한다.
	CString strPLCRun;
	strPLCRun.Format("%s", m_fnUmacOnlineCommand(PLC_25_TM02_Home));
	if(strPLCRun == '1')
		return true; 

	switch(AxisNum)
	{
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
		{
		}break;
	case AXIS_TM02_PickerUpDown:
		{
		}break;
	case AXIS_TM02_PickerShift:
		{
		}break;
	case AXIS_TM02_PickerRotate:
		{

		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//AM01
bool CMotCtrl::m_fnAM01HomeInterlockCheck(int AxisNum)
{
	CHECK_AXIS_AM01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	//단축 Home Command  처리시에는 
	//해당 모듈의 해당 축은 한번에 한축만 Home 한다.
	CString strPLCRun;
	strPLCRun.Format("%s", m_fnUmacOnlineCommand(PLC_26_AM01_Home));
	if(strPLCRun == '1')
		return true; 
	switch(AxisNum)
	{
		//AM01
	case AXIS_AM01_ScanDrive:
		{
		}break;
	case AXIS_AM01_ScanShift:
		{
		}break;
	case AXIS_AM01_ScanUpDn:
		{
		}break;
	case AXIS_AM01_SpaceSnUpDn:
		{
		}break;
	case AXIS_AM01_ScopeDrive:
		{
			//Scope Z축이 안전 위치에 있어야 한다.
			if(m_bScopeSafetyPos != true)
			{
				G_WriteInfo(Log_Check,"Home Action : AXIS_AM01_ScopeDrive Scope가 안전 위치에 있지 않습니다.");
				return true;
			}

		}break;
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
		{
			//Scope Z축이 안전 위치에 있어야 한다.
			if(m_bScopeSafetyPos != true)
			{
				G_WriteInfo(Log_Check,"Home Action : AXIS_AM01_ScopeDrive Scope가 안전 위치에 있지 않습니다.");
				return true;
			}
		}break;
	case AXIS_AM01_ScopeUpDn:
		{
		}break;
	case AXIS_AM01_AirBlowLeftRight:
		{
		}break;
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
		{
			//인장기는 Stick이 감지 되어 있으면 Home을 할 수가 없다.)
			if(m_IO_InD.In10_Sn_LeftTensionStickDetection == 1 || m_IO_InD.In22_Sn_RightTensionStickDetection == 1)
			{
				G_WriteInfo(Log_Check,"Home Action :  AXIS_AM01_Tension Stick이 감지 되어 있으면 Home을 할 수가 없다.");
				return true;
			}
		}break;
	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
		{
			//인장기는 Stick이 감지 되어 있으면 Home을 할 수가 없다.)
			if(m_IO_InD.In10_Sn_LeftTensionStickDetection == 1 || m_IO_InD.In22_Sn_RightTensionStickDetection == 1)
			{
				G_WriteInfo(Log_Check,"Home Action :  AXIS_AM01_Tension Stick이 감지 되어 있으면 Home을 할 수가 없다.");
				return true;
			}
		}break;
	case AXIS_AM01_JigInOut://ML3 #23
		{
			//Scope Z Up Limit
			//Scope의 Z축이 대기 위치에서 벗어나 있는경우 Scope Drive 범위는 제한된다.
			//int ScopeUpDownStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			////if(abs(m_AxisNowPos[AXIS_AM01_ScopeUpDn]-ScopeUpDownStandby) > AXIS_INPOS_CTS)
			//if(m_AxisNowPos[AXIS_AM01_ScopeUpDn] > ScopeUpDownStandby)
			//{
			//	G_WriteInfo(Log_Check,"Home Action AXIS_AM01_JigInOut : AXIS_AM01_SCOPEUPDOWN STASNDBY POS [%d]에 있지 않습니다.", ScopeUpDownStandby);
			//	return true;
			//}
			//Scope Z축이 안전 위치에 있어야 한다.
			if(m_bScopeSafetyPos != true)
			{
				G_WriteInfo(Log_Check,"Home Action : AXIS_AM01_ScopeDrive Scope가 안전 위치에 있지 않습니다.");
				return true;
			}

			int ScanReviewStandBy = GetPrivateProfileInt(Section_ScanPos, Key_Scan_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			if(m_AxisNowPos[AXIS_AM01_ScanUpDn] > ScanReviewStandBy)
			{
				G_WriteInfo(Log_Check,"Home Action AXIS_AM01_JigInOut : AXIS_AM01_SCANUPDOWN STASNDBY POS [%d]에 있지 않습니다.", ScanReviewStandBy);
				return true;
			}

			if(m_AxisNowPos[AXIS_TT01_TableDrive]>0) // 대기위치가 -값을 갖고 있다.
			{
				G_WriteInfo(Log_Check,"Home Action AXIS_AM01_JigInOut : AXIS_TT01_Drive 가  간섭 범위[%d] 내에 있습니다.", m_AxisNowPos[AXIS_TT01_TableDrive]);
				return true;			
			}

			int SpaceStandbyZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			if(m_AxisNowPos[AXIS_AM01_SpaceSnUpDn] > SpaceStandbyZPos)
			{
				G_WriteInfo(Log_Check,"Home Action AXIS_AM01_JigInOut : AXIS_AM01_SpaceSnUpDn 간섭 범위[%d/%d] 내에 있습니다.", SpaceStandbyZPos, m_AxisNowPos[AXIS_AM01_SpaceSnUpDn]);
				return true;
			}

			
			//if(m_IO_InD.In10_Sn_LeftTensionStickDetection == 1 || m_IO_InD.In22_Sn_RightTensionStickDetection == 1)
			//{
			//	int TentionStandbyZPos = GetPrivateProfileInt(Section_GripperPos, Key_Gripper_ZPos_TT01Out, 0, VS10Master_PARAM_INI_PATH);
			//	//if(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]!= TentionStandbyZPos)
			//	if(abs(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]-TentionStandbyZPos) > AXIS_INPOS_CTS)
			//	{
			//		G_WriteInfo(Log_Check,"Home Action AXIS_AM01_JigInOut : AXIS_AM01_GripperPos 간섭 범위[%d/%d] 내에 있습니다.", TentionStandbyZPos, m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]);
			//		return true;
			//	}
			//}


			if(m_fnGetAM01State_CySol_Review() != SOL_DOWN)
			{//Review Up Move Limit Check

				G_WriteInfo(Log_Check,"Home Action AXIS_AM01_JigInOut: CySol_Review Down 위치에 있지 않습니다.");
				return true;
			}

			if(	m_fnGetAM01State_CySol_RingLight() != SOL_DOWN)
			{
				G_WriteInfo(Log_Check,"Home Action AXIS_AM01_JigInOut : CySol_RingLight Down 위치에 있지 않습니다.");
				return true;	
			}
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
//Motion Ctrl (Homing)
//////////////////////////////////////////////////////////////////////////
//Motion Ctrl (Homing)
bool CMotCtrl::m_fnMotHoming(int AxisNum)
{
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
	case AXIS_TT01_ThetaAlign:
	case AXIS_TT01_TableTurn:
		{
			return m_fnTT01Homing(AxisNum);
		}break;
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			return m_fnBT01Homing(AxisNum);
		}break;
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
	case AXIS_BO01_BoxUpDn://좌우 동기축.
	case AXIS_BO01_BoxDrive:
		{
			return m_fnBO01Homing(AxisNum);
		}break;
	case AXIS_LT01_BoxLoadPush:
		{
			return m_fnLT01Homing(AxisNum);
		}break;
	case AXIS_UT01_BoxUnLoadPush:
		{
			return m_fnUT01Homing(AxisNum);
		}break;
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
			return m_fnTM01Homing(AxisNum);
		}break;
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
	case AXIS_TM02_PickerUpDown:
	case AXIS_TM02_PickerShift:
	case AXIS_TM02_PickerRotate:
		{
			return m_fnTM02Homing(AxisNum);
		}break;
		//AM01
	case AXIS_AM01_ScanDrive:
	case AXIS_AM01_ScanShift:
	case AXIS_AM01_ScanUpDn:
	case AXIS_AM01_SpaceSnUpDn:
	case AXIS_AM01_ScopeDrive:
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
	case AXIS_AM01_ScopeUpDn:
	case AXIS_AM01_AirBlowLeftRight:
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
	case AXIS_AM01_JigInOut:
		{
			return m_fnAM01Homing(AxisNum);
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//TT01
bool CMotCtrl::m_fnTT01Homing(int AxisNum)
{
	if(m_fnTT01HomeInterlockCheck(AxisNum) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive://Cruiser Axis #1
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_TT01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnTT01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 21");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_TT01, 0x01);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 21");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;

		}break;
	case AXIS_TT01_ThetaAlign://ML3 #18
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_TT01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnTT01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 21");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_TT01, 0x02);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 21");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	case AXIS_TT01_TableTurn://ML3 #19
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_TT01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnTT01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 21");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_TT01, 0x04);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 21");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//BT01
bool CMotCtrl::m_fnBT01Homing(int AxisNum)
{
	if(m_fnBT01HomeInterlockCheck(AxisNum) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BT01
	case AXIS_BT01_CassetteUpDown://ML3 #22
		{
			m_BT01CouplingCnt[0] = 0;
			m_BT01CouplingCnt[1] = 0;

			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_BT01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnBT01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 22");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_BT01, 0x01);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 22");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//BT01
bool CMotCtrl::m_fnLT01Homing(int AxisNum)
{
	if(m_fnLT01HomeInterlockCheck(AxisNum) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//LT01
	case AXIS_LT01_BoxLoadPush://ML3 #14
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_LT01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnLT01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 27");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_LT01, 0x01);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 27");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//UT01
bool CMotCtrl::m_fnUT01Homing(int AxisNum)
{
	if(m_fnUT01HomeInterlockCheck(AxisNum) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//UT01
	case AXIS_UT01_BoxUnLoadPush://ML3 #15
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_UT01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnUT01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 28");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_UT01, 0x01);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 28");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//BO01
bool CMotCtrl::m_fnBO01Homing(int AxisNum)
{
	if(m_fnBO01HomeInterlockCheck(AxisNum) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BO01
	case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_BO01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnBO01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 23");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_BO01, 0x02);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 23");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;

		}break;
	case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_BO01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnBO01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 23");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_BO01, 0x04);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 23");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;


	case AXIS_BO01_BoxDrive://ML3 #16
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_BO01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnBO01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 23");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_BO01, 0x01);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 23");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//TM01
bool CMotCtrl::m_fnTM01Homing(int AxisNum)
{
	if(m_fnTM01HomeInterlockCheck(AxisNum) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM01
	case AXIS_TM01_ForwardBackward://ML3 #17
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_TM01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnTM01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 24");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_TM01, 0x01);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 24");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				return true;
			}
			else
				return false;
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//TM02
bool CMotCtrl::m_fnTM02Homing(int AxisNum)
{
	if(m_fnTM02HomeInterlockCheck(AxisNum) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM02
	case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_TM02));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnTM02Standby(AXIS_TM02_PickerDrive) == true)
				{
					//UmacCMD.Format("dis plc 25");
					//m_fnUmacOnlineCommand(UmacCMD);

// 					UmacCMD.Format("dis plc 8");
// 					m_fnUmacOnlineCommand(UmacCMD);  //2014.12.15 GJJ

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_TM02, 0x04);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 25");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	case AXIS_TM02_PickerUpDown: //ML3 #20
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_TM02));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnTM02Standby(AXIS_TM02_PickerUpDown) == true)
				{
					//UmacCMD.Format("dis plc 25");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_TM02, 0x01);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 25");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;

		}break;
	case AXIS_TM02_PickerShift: //ML3 #21
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_TM02));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnTM02Standby(AXIS_TM02_PickerShift) == true)
				{
					//UmacCMD.Format("dis plc 25");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_TM02, 0x02);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 25");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;

		}break;
	case AXIS_TM02_PickerRotate: //AxisLink_A Y
		{
			if(m_fnTM02Standby(AXIS_TM02_PickerRotate) == true)
			{
				//UmacCMD.Format("dis plc 10");
				//m_fnUmacOnlineCommand(UmacCMD);

				UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x02);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("ena plc 10");
				m_fnUmacOnlineCommand(UmacCMD);
			}
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//AM01
bool CMotCtrl::m_fnAM01Homing(int AxisNum)
{
	if(m_fnAM01HomeInterlockCheck(AxisNum) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//AM01
	case AXIS_AM01_ScanDrive://Cruiser Axis #6
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_AM01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnAM01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 26");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_AM01, 0x01);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 26");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;

		}break;
	case AXIS_AM01_ScanShift://Cruiser Axis #2
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_AM01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnAM01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 26");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_AM01, 0x02);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 26");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	case AXIS_AM01_ScanUpDn://AxisLink_D X
		{
			if(m_fnAM01Standby(AxisNum) == true)
			{
				//UmacCMD.Format("dis plc 13");
				//m_fnUmacOnlineCommand(UmacCMD);

				UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x01);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("ena plc 13");
				m_fnUmacOnlineCommand(UmacCMD);
			}
			else
				return false;

		}break;
	case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
		{
			if(m_fnAM01Standby(AxisNum) == true)
			{
				//UmacCMD.Format("dis plc 13");
				//m_fnUmacOnlineCommand(UmacCMD);

				UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x02);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("ena plc 13");
				m_fnUmacOnlineCommand(UmacCMD);
			}
			else
				return false;
		}break;
	case AXIS_AM01_ScopeDrive://Cruiser Axis #5
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_AM01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnAM01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 26");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_AM01, 0x04);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 26");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 
		{
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_AM01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnAM01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 26");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_AM01, 0x08);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 26");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
		}break;
	case AXIS_AM01_ScopeUpDn://AxisLink_D Z
		{
			if(m_fnAM01Standby(AxisNum) == true)
			{
				//UmacCMD.Format("dis plc 13");
				//m_fnUmacOnlineCommand(UmacCMD);

				UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x04);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("ena plc 13");
				m_fnUmacOnlineCommand(UmacCMD);
			}
			else
				return false;
		}break;
	case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
		{
			if(m_fnAM01Standby(AxisNum) == true)
			{
				//UmacCMD.Format("dis plc 13");
				//m_fnUmacOnlineCommand(UmacCMD);

				UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x08);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("ena plc 13");
				m_fnUmacOnlineCommand(UmacCMD);
			}
			else
				return false;
		}break;
		//////////////////////////////////////////////////////////////////////////
		//Axis Link Block A
	case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
	case AXIS_AM01_TensionRightUpDn://AxisLink_A U
		{//초기화는 동시에 진행 한다.
			if( m_fnAM01Standby(AXIS_AM01_TensionLeftUpDn) == true && 
				m_fnAM01Standby(AXIS_AM01_TensionRightUpDn) == true )
			{
				//UmacCMD.Format("dis plc 10");
				//m_fnUmacOnlineCommand(UmacCMD);

				UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, (0x4|0x8));
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("ena plc 10");
				m_fnUmacOnlineCommand(UmacCMD);
			}
			else
				return false;
		}break;

		//////////////////////////////////////////////////////////////////////////
		//Axis Link Block B
		//Block B는 대표 축 X가 Home이 오면 전체 Home을 한다.
	case AXIS_AM01_LTension1://AxisLink_B X
	//case AXIS_AM01_LTension2://AxisLink_B Y
	//case AXIS_AM01_LTension3://AxisLink_B Z
	//case AXIS_AM01_LTension4://AxisLink_B U
		{//초기화는 동시에 진행 한다.
			//UmacCMD.Format("dis plc 11");
			//m_fnUmacOnlineCommand(UmacCMD);

			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0xF);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 11");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
		//////////////////////////////////////////////////////////////////////////
		//Axis Link Block C
		//Block C는 대표 축 X가 Home이 오면 전체 Home을 한다.
	case AXIS_AM01_RTension1://AxisLink_C X
	//case AXIS_AM01_RTension2://AxisLink_C Y
	//case AXIS_AM01_RTension3://AxisLink_C Z
	//case AXIS_AM01_RTension4://AxisLink_C U
		{//초기화는 동시에 진행 한다.
			//UmacCMD.Format("dis plc 12");
			//m_fnUmacOnlineCommand(UmacCMD);

			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0xF);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 12");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;

	case AXIS_AM01_JigInOut: //ML3 #23
		{
#ifdef MOT_USE_UMAC_JIG_23AXIS
			HomeFlag = (DWORD)atoi(m_fnUmacOnlineCommand(AXIS_HOME_FLAG_AM01));
			if(HomeFlag == 0)//구동중이라면(구동 완료 되면 PLC에서 0 Set 한다.)
			{
				if(m_fnAM01Standby(AxisNum) == true)
				{
					//UmacCMD.Format("dis plc 26");
					//m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("%s=%d", AXIS_HOME_FLAG_AM01, 0x10);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("ena plc 26");
					m_fnUmacOnlineCommand(UmacCMD);
				}
				else
					return false;
			}
			else
				return false;
#else
			return false;
#endif
		}break;
	default: 
		return false;
	}
	return true;
}
//bool CMotCtrl::m_fnAM01ScanMultiHomingXY()
//{
//	if( m_fnAM01HomeInterlockCheck(AXIS_AM01_ScanDrive) == true ||
//		m_fnAM01HomeInterlockCheck(AXIS_AM01_ScanShift) == true)
//	{
//		return false;
//	}
//
//	return true;
//}
//bool CMotCtrl::m_fnAM01ScopeMultiHomingXY()
//{
//	if( m_fnAM01HomeInterlockCheck(AXIS_AM01_ScopeDrive) == true ||
//		m_fnAM01HomeInterlockCheck(AXIS_AM01_ScopeShift) == true)
//	{
//		return false;
//	}
//	
//	return true;
//}

//////////////////////////////////////////////////////////////////////////
//Motion Ctrl(Inter lock)
bool CMotCtrl::m_fnMotInterlockCheck(int AxisNum, long newPosition)
{
	//구동 가능한 상태 이다.(fals:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
	CHECK_AXIS_ALL(AxisNum);
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
	case AXIS_TT01_ThetaAlign:
	case AXIS_TT01_TableTurn:
		{
			return m_fnTT01InterlockCheck(AxisNum, newPosition);
		}break;
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			return m_fnBT01InterlockCheck(AxisNum, newPosition);
		}break;
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
			return m_fnLT01InterlockCheck(AxisNum, newPosition);
		}break;
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
	case AXIS_BO01_BoxUpDn://좌우 동기축.
	case AXIS_BO01_BoxDrive:
		{
			return m_fnBO01InterlockCheck(AxisNum, newPosition);
		}break;
	case AXIS_UT01_BoxUnLoadPush:
		{
			return m_fnUT01InterlockCheck(AxisNum, newPosition);
		}break;
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
			return m_fnTM01InterlockCheck(AxisNum, newPosition);
		}break;
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
	case AXIS_TM02_PickerUpDown:
	case AXIS_TM02_PickerShift:
	case AXIS_TM02_PickerRotate:
		{
			return m_fnTM02InterlockCheck(AxisNum, newPosition);
		}break;
		//AM01
	case AXIS_AM01_ScanDrive:
	case AXIS_AM01_ScanShift:
	case AXIS_AM01_ScanUpDn:
	case AXIS_AM01_SpaceSnUpDn:
	case AXIS_AM01_ScopeDrive:
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
	case AXIS_AM01_ScopeUpDn:
	case AXIS_AM01_AirBlowLeftRight:
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
	case AXIS_AM01_JigInOut:
		{
			return m_fnAM01InterlockCheck(AxisNum, newPosition);
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//TT01
bool CMotCtrl::m_fnTT01InterlockCheck(int AxisNum, long newPosition)
{
	//구동 가능한 상태 이다.(fals:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
	CHECK_AXIS_TT01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;

	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
		{
			if(m_IO_InG.In05_Sn_TurnTableUpperPosition == 0 && m_IO_InG.In06_Sn_TurnTableLowerPosition == 0)
			{
				G_WriteInfo(Log_Check,"Turn Table Up/Down Position Sensor Error!!");
				return true;
			}
			//지그위치가 뒤쪽으로 명확하지 않으면 움직일수 없다.
			if(m_IO_InI.In15_Sn_MaskJigBWD == 0)//
			{
				G_WriteInfo(Log_Check,"Turn Table Move Jig BWD Sensor Error!!");
				return true;
			}

		}break;
	case AXIS_TT01_ThetaAlign:
		{

		}break;
	case AXIS_TT01_TableTurn:
		{
			if(m_IO_InI.In10_Sn_TurnTableRotateEnablePos == 0)
			{
				G_WriteInfo(Log_Check,"TT01 Turn Position Sensor Error!!");
				return true;
			}
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//BT01
bool CMotCtrl::m_fnBT01InterlockCheck(int AxisNum, long newPosition)
{
	CHECK_AXIS_BT01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	switch(AxisNum)
	{
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{

			//Fork가 정위치에 있지 않으면 움직이지 않는다.
			if(m_IO_InI.In09_Sn_ForkStandbyPos == 0)
			{
				G_WriteInfo(Log_Check,"BT01 Move Fork Standby Pos Error!!");
				return true;
			}

			//Picker Drive가 구동중이면 BT01은 움직이지 않는다.
			//AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
			if( G_MotCtrlObj.m_LocalAxisST[9].m_AxisStB.St00_InPosition == 0 ||//#31
				G_MotCtrlObj.m_LocalAxisST[6].m_AxisStB.St00_InPosition == 0 ||//#7
				G_MotCtrlObj.m_LocalAxisST[7].m_AxisStB.St00_InPosition == 0)  //#8 이 움직이는 중이면 구동X
			{
				G_WriteInfo(Log_Check,"AXIS_BT01_CassetteUpDown : AXIS_TM02_PickerDrive Move Interlock");
				return true;
			}
			////Picker Drive가 지정위치보다 작은쪽에 있어야 한다.
			//int PickerBT01InterlockPos = GetPrivateProfileInt(Section_BT01, Key_UpDn_TM02DriveLimit, 1525000, Interlock_PARAM_INI_PATH);
			//if(G_MotCtrlObj.m_AxisNowPos[AXIS_TM02_PickerDrive] < PickerBT01InterlockPos)
			//{
			//	G_WriteInfo(Log_Check,"AXIS_BT01_CassetteUpDown : AXIS_TM02_PickerDrive Position Move Interlock(Over Limit Pos)");
			//	return true;
			//}

		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//LT01
bool CMotCtrl::m_fnLT01InterlockCheck(int AxisNum, long newPosition)
{
	//구동 가능한 상태 이다.(fals:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
	CHECK_AXIS_LT01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	switch(AxisNum)
	{
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//UT01
bool CMotCtrl::m_fnUT01InterlockCheck(int AxisNum, long newPosition)
{
	CHECK_AXIS_UT01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	switch(AxisNum)
	{
		//UT01
	case AXIS_UT01_BoxUnLoadPush:
		{
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//BO01
bool CMotCtrl::m_fnBO01InterlockCheck(int AxisNum, long newPosition)
{
	CHECK_AXIS_BO01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	switch(AxisNum)
	{
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
		{

		}break;
	case AXIS_BO01_BoxUpDn://좌우 동기축.
		{
		}break;
	case AXIS_BO01_BoxDrive:
		{
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//TM01
bool CMotCtrl::m_fnTM01InterlockCheck(int AxisNum, long newPosition)
{
	CHECK_AXIS_TM01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	switch(AxisNum)
	{
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//TM02
bool CMotCtrl::m_fnTM02InterlockCheck(int AxisNum, long newPosition)
{
	CHECK_AXIS_TM02(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
	{
		if(m_IO_InA.In19_AutoTeachMode == 0)
		{
			G_WriteInfo(Log_Check, "Check Teach-Key!");
		}

		return true;
	}

		
	switch(AxisNum)
	{
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
		{

			if(m_IO_InE.In12_Sn_PickerZAxisClashPrevent == 0)
			{
				G_WriteInfo(Log_Check,"AXIS_TM02_PickerDrive : In12_Sn_PickerZAxisClashPrevent Zero Interlock");
				return true;
			}
			//TM02_PickerUpDown://ML3 #20
			if(G_MotCtrlObj.m_ML3AxisST[10].m_AxisStB.St00_InPosition == 0)//Up/down이 움직이는 중이면 구동X
			{
				G_WriteInfo(Log_Check,"AXIS_TM02_PickerDrive : TM02_PickerUpDown Move Interlock");
				return true;
			}

			//BT01 Cassette가 정위치에 있지 않다.
			if(m_IO_InA.In26_Sn_CassetteLoadingPosSensor == 0 )
			{
				G_WriteInfo(Log_Check,"AXIS_TM02_PickerDrive : 카셋 배출위치 센서  Move Interlock");
				return true;
			}

			//AXIS_BT01_CassetteUpDown://ML3 #22
			if(G_MotCtrlObj.m_ML3AxisST[12].m_AxisStB.St00_InPosition == 0)//Up/down이 움직이는 중이면 구동X
			{
				G_WriteInfo(Log_Check,"AXIS_TM02_PickerDrive : AXIS_BT01_CassetteUpDown Move Interlock");
				return true;
			}
		}break;
	case AXIS_TM02_PickerUpDown:
		{
			if(m_IO_InE.In12_Sn_PickerZAxisClashPrevent == 0)
			{
				if(newPosition >= m_AxisNowPos[AXIS_TM02_PickerUpDown])
				{
					G_WriteInfo(Log_Check,"AXIS_TM02_PickerUpDown : In12_Sn_PickerZAxisClashPrevent Pos( %d==>%d) Interlock",
						m_AxisNowPos[AXIS_TM02_PickerUpDown], newPosition);
					return true;
				}
			}

			//#7//#8//#31
			if(G_MotCtrlObj.m_LocalAxisST[6].m_AxisStB.St00_InPosition ==0 ||//PickerDrive이 움직이는 중이면 구동X
			G_MotCtrlObj.m_LocalAxisST[7].m_AxisStB.St00_InPosition ==0||
			G_MotCtrlObj.m_LocalAxisST[9].m_AxisStB.St00_InPosition ==0)
			{
				G_WriteInfo(Log_Check,"AXIS_TM02_PickerUpDown : AXIS_TM02_PickerDrive Move Interlock");
				return true;
			}
		}break;
	case AXIS_TM02_PickerShift:
		{
		}break;
	case AXIS_TM02_PickerRotate:
		{
		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//AM01
bool CMotCtrl::m_fnAM01InterlockCheck(int AxisNum, long newPosition)
{
	CHECK_AXIS_AM01(AxisNum);
	//Teach Mode에서는 In29_TeachMoveEnable 가 1이어야 이동 가능 하다.
	if(m_IO_InA.In19_AutoTeachMode == 0 && m_IO_InA.In29_TeachMoveEnable != 1)
		return true;
	switch(AxisNum)
	{
		//AM01
	case AXIS_AM01_ScanDrive:
		{
			//Space Sensore의 Z축이 Standby위치가 아니면 움직이면 안됨.
			int SpaceSensorZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			if(abs(m_AxisNowPos[AXIS_AM01_SpaceSnUpDn]-SpaceSensorZPos)>AXIS_INPOS_CTS)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScanDrive : 단차센서 Position Interlock");
				return true;
			}

			int ScanDriveMin = GetPrivateProfileInt(Section_SCAN, Key_Drive_Min, 50000, Interlock_PARAM_INI_PATH);
			int ScanDriveMax = GetPrivateProfileInt(Section_SCAN, Key_Drive_Max, 930000, Interlock_PARAM_INI_PATH);
			if(newPosition<ScanDriveMin ||newPosition>ScanDriveMax)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScanDrive : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", ScanDriveMin, newPosition, ScanDriveMax);
				return true;
			}


			if(m_IO_InI.In15_Sn_MaskJigBWD == 1)
			{
				if(	m_fnGetAM01State_CySol_RingLight() != SOL_DOWN)
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_ScanDrive : In15_Sn_MaskJigBWD And RingLight Interlock");
					return true;
				}
				if(m_fnGetAM01State_CySol_Review() != SOL_DOWN)
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_ScanDrive : In15_Sn_MaskJigBWD And Review Interlock");
					return true;
				}
				//TT01이 들어와 있을때 인터락.
				if(m_AxisNowPos[AXIS_TT01_TableDrive]>0)//TT01 Drive는 음수 위치가 대기 위치 이다.
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_ScanShift : TT01_TableDrive(%d) 안전 위치 범위 내에 있지 않습니다.", m_AxisNowPos[AXIS_TT01_TableDrive]);
					return true;
				}
			}
			else
			{//측정위치에 있을때는 구간내에서 움직일수 있어야 한다.

			}
		}break;
	case AXIS_AM01_ScanShift:
		{
			//Space Sensore의 Z축이 Standby위치가 아니면 움직이면 안됨.
			int SpaceSensorZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			if(abs(m_AxisNowPos[AXIS_AM01_SpaceSnUpDn]-SpaceSensorZPos)>AXIS_INPOS_CTS)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScanShift : 단차센서 Position Interlock(Standby:%d, NowPos(%d))", SpaceSensorZPos, m_AxisNowPos[AXIS_AM01_SpaceSnUpDn]);
				return true;
			}
			int ScanShiftMin = GetPrivateProfileInt(Section_SCAN, Key_Shift_Min, -45600, Interlock_PARAM_INI_PATH);
			int ScanShiftMax = GetPrivateProfileInt(Section_SCAN, Key_Shift_Max, 980000, Interlock_PARAM_INI_PATH);
			if(newPosition<ScanShiftMin ||newPosition>ScanShiftMax)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScanShift : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", ScanShiftMin, newPosition, ScanShiftMax);
				return true;
			}
			//TT01이 들어와 있을때 인터락.
			if(m_AxisNowPos[AXIS_TT01_TableDrive]>0)//TT01 Drive는 음수 위치가 대기 위치 이다.
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScanShift : TT01_TableDrive(%d) 안전 위치 범위 내에 있지 않습니다.", m_AxisNowPos[AXIS_TT01_TableDrive]);
				return true;
			}
			
			if(m_fnGetAM01State_CySol_Review() != SOL_DOWN)
			{//Review Up Move Limit Check

				int ScanShiftMin = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_ReviewSol_Scan_Shift_Min, 311894, Interlock_PARAM_INI_PATH);
				int ScanShiftMax = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_ReviewSol_Scan_Shift_Max, 412756, Interlock_PARAM_INI_PATH);
				if(newPosition<(ScanShiftMin-AXIS_INPOS_um) || newPosition>(ScanShiftMax+AXIS_INPOS_um))
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_ScanShift : [Min(%d)<New(%d)<Max(%d)] CySol_Review 범위 내에 있지 않습니다.", 
						ScanShiftMin, newPosition, ScanShiftMax);
					return true;
				}
			}
			if(	m_fnGetAM01State_CySol_RingLight() != SOL_DOWN)
			{
				int ScanShiftMin = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_RingLightSol_Scan_Shift_Min, 301894, Interlock_PARAM_INI_PATH);
				int ScanShiftMax = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_RingLightSol_Scan_Shift_Max, 422756, Interlock_PARAM_INI_PATH);
				int LineScanSiftOffset = GetPrivateProfileInt(Section_ReviewCamToObjDis, Key_LineScanDisX, 0, VS10Master_PARAM_INI_PATH);
				ScanShiftMin+=(LineScanSiftOffset);
				ScanShiftMax+=(LineScanSiftOffset);
				if(newPosition<(ScanShiftMin-AXIS_INPOS_um) || newPosition>(ScanShiftMax+AXIS_INPOS_um))
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_ScanShift : [Min(%d)<New(%d)<Max(%d)] CySol_RingLight 범위 내에 있지 않습니다.", 
						ScanShiftMin, newPosition, ScanShiftMax);
					return true;
				}
			}
			//if(	m_fnGetAM01State_CySol_RingLight() != SOL_DOWN || m_fnGetAM01State_CySol_Review() != SOL_DOWN)
			//{
			//	G_WriteInfo(Log_Check,"AXIS_AM01_ScanShift : Ring Light Or Review 실린더가 다운상태가 아닙니다.");
			//	return true;
			//}
		}break;
	case AXIS_AM01_ScanUpDn:
		{
			//Space Up/Down과 같이 동작 할수 없다.(스켄축이 커지는 동작에 한 해서만.
			int SpaceStandbyZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			if(m_AxisNowPos[AXIS_AM01_SpaceSnUpDn] > SpaceStandbyZPos && newPosition>m_AxisNowPos[AXIS_AM01_ScanUpDn])
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScanUpDn : Space Up/Down과 같이 동작 할수 없다.(Now(%d), Standby(%d)", m_AxisNowPos[AXIS_AM01_SpaceSnUpDn], SpaceStandbyZPos);
				return true;
			}
		}break;
	case AXIS_AM01_SpaceSnUpDn:
		{
			int SpaceStandbyZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			if(newPosition>m_AxisNowPos[AXIS_AM01_SpaceSnUpDn] && newPosition>SpaceStandbyZPos)
			{//Z축이 올라가는것에만(증가 하는것에만 Interlock이 해당 된다.(Standby Pos 내에서는 상관 없다.)
				//Gripper Close 상태 체크
				if(	m_fnGetAM01State_CySol_Review() != SOL_DOWN ||
					m_fnGetAM01State_CySol_RingLight() != SOL_DOWN)
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_SpaceSnUpDn : Review, RingLight  Cylinder Interlock");
					return true;
				}

				int ScanReviewStandBy = GetPrivateProfileInt(Section_ScanPos, Key_Scan_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
				if(abs(m_AxisNowPos[AXIS_AM01_ScanUpDn]-ScanReviewStandBy)>AXIS_INPOS_CTS && m_fnGetAM01State_CySol_Review() != SOL_DOWN)
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_SpaceSnUpDn : AXIS_AM01_ScanUpDn STASNDBY POS [%d]에 있지 않습니다.", ScanReviewStandBy);
					return true;
				}
			
				
				if(newPosition != SpaceStandbyZPos)
				{
					//Gripper의 Z축이 Measur Pos가 아니면 움직이지 않는다.
					int GripperMeasureZPos = GetPrivateProfileInt(Section_GripperPos, Key_Gripper_ZPos_Measure, 0, VS10Master_PARAM_INI_PATH);
					if(abs(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]-GripperMeasureZPos)>AXIS_INPOS_CTS)
					{
						G_WriteInfo(Log_Check,"AXIS_AM01_SpaceSnUpDn : Gripper Position Interlock-GripperMeasureZPos");
						return true;
					}
				}

				int SpaceUpDn_ScanShiftMin = GetPrivateProfileInt(Section_SCAN, Key_Space_Shift_Min, 460000, Interlock_PARAM_INI_PATH);
				int SpaceUpDn_ScanShiftMax = GetPrivateProfileInt(Section_SCAN, Key_Space_Shift_Max, 590000, Interlock_PARAM_INI_PATH);
				if(m_AxisNowPos[AXIS_AM01_ScanShift]<SpaceUpDn_ScanShiftMin || SpaceUpDn_ScanShiftMax < m_AxisNowPos[AXIS_AM01_ScanShift])
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_SpaceSnUpDn : Scan Shift Axis [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", SpaceUpDn_ScanShiftMin, m_AxisNowPos[AXIS_AM01_ScanShift], SpaceUpDn_ScanShiftMax);
					return true;
				}
			}
		}break;
	case AXIS_AM01_ScopeDrive:
		{
			//Gripper Close 상태 체크
			if(	m_fnGetAM01State_CySol_TEN_GRIPPER1() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER2() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER3() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER4() != SOL_DOWN)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScopeDrive : Gripper Cylinder Interlock");
				return true;
			}

			////Gripper의 Z축이 Grip Pos보다 위에 있으면 간섭 발생이 가능 함으로 움직이지 않는다.
			//int GripperGripZPos = GetPrivateProfileInt(Section_GripperPos, Key_Gripper_ZPos_Grip, 0, VS10Master_PARAM_INI_PATH);
			////if(abs(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]-GripperGripZPos)>AXIS_INPOS_CTS)
			//if(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]>=GripperGripZPos)
			//{
			//	G_WriteInfo(Log_Check,"AXIS_AM01_ScopeDrive : Gripper Position Interlock");
			//	return true;
			//}
			//Scope Z Up Limit
			int ScopeDriveMin = GetPrivateProfileInt(Section_SCOPE, Key_Drive_Min, 65000, Interlock_PARAM_INI_PATH);
			int ScopeDriveMax = GetPrivateProfileInt(Section_SCOPE, Key_Drive_Max, 1340000, Interlock_PARAM_INI_PATH);
			if(newPosition<ScopeDriveMin ||newPosition>ScopeDriveMax)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScopeDrive : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", ScopeDriveMin, newPosition, ScopeDriveMax);
				return true;
			}

			//Scope의 Z축이 대기 위치에서 벗어나 있는경우 Scope Drive 범위는 제한된다.
			int ScopeUpDownStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			//int ScopeUpDownScan3D = GetPrivateProfileInt(Section_ScopePos, Key_Scope_ZPos_Area3DMeasure, 0, VS10Master_PARAM_INI_PATH);
			//if(abs(m_AxisNowPos[AXIS_AM01_ScopeUpDn]-ScopeUpDownStandby) > AXIS_INPOS_CTS)
			if(m_bScopeSafetyPos != true)
			{
				if(m_AxisNowPos[AXIS_AM01_ScopeUpDn]>ScopeUpDownStandby )
				{
					//Scope Z축이 안전 위치에 있어야 한다.
					//if(abs(m_AxisNowPos[AXIS_AM01_ScopeUpDn]-ScopeUpDownScan3D) > AXIS_INPOS_CTS)
					//{
					//}
					int ScopeDriveMin = GetPrivateProfileInt(Section_SCOPE, Key_Z_Down_Drive_Min, 340000, Interlock_PARAM_INI_PATH);
					int ScopeDriveMax = GetPrivateProfileInt(Section_SCOPE, Key_Z_Down_Drive_Max, 1320000, Interlock_PARAM_INI_PATH);
					if(newPosition<ScopeDriveMin ||newPosition>ScopeDriveMax)
					{
						G_WriteInfo(Log_Check,"AXIS_AM01_ScopeDrive : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", ScopeDriveMin, newPosition, ScopeDriveMax);
						return true;
					}
				}
			}
			
		}break;
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
		{
			//Gripper Close 상태 체크
			if(	m_fnGetAM01State_CySol_TEN_GRIPPER1() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER2() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER3() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER4() != SOL_DOWN)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScopeShift : Gripper Cylinder Interlock");
				return true;
			}

			//최대 영역일때
			int ScopeShiftMin = GetPrivateProfileInt(Section_SCOPE, Key_Shift_Min, -48000, Interlock_PARAM_INI_PATH);
			int ScopeShiftMax = GetPrivateProfileInt(Section_SCOPE, Key_Shift_Max, 532000, Interlock_PARAM_INI_PATH);
			if(newPosition<ScopeShiftMin ||newPosition>ScopeShiftMax)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScopeShift : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", ScopeShiftMin, newPosition, ScopeShiftMax);
				return true;
			}

			if(m_bScopeSafetyPos != true)
			{
				int ScopeUpDownStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
				if(m_AxisNowPos[AXIS_AM01_ScopeUpDn]>ScopeUpDownStandby )
				{
					//Scope Z축이 안전 위치에 있어야 한다.
					//if(abs(m_AxisNowPos[AXIS_AM01_ScopeUpDn]-ScopeUpDownScan3D) > AXIS_INPOS_CTS)
					//{
					//}
					int ScopeShiftMin = GetPrivateProfileInt(Section_SCOPE, Key_Z_Down_Shift_Min, 340000, Interlock_PARAM_INI_PATH);
					int ScopeShiftMax = GetPrivateProfileInt(Section_SCOPE, Key_Z_Down_Shift_Max, 1320000, Interlock_PARAM_INI_PATH);
					if(newPosition<ScopeShiftMin ||newPosition>ScopeShiftMax)
					{
						G_WriteInfo(Log_Check,"AXIS_AM01_ScopeDrive : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", ScopeShiftMin, newPosition, ScopeShiftMax);
						return true;
					}
				}
			}

			////Gripper의 Z축이 Grip Pos보다 위에 있으면 간섭 발생이 가능 함으로 움직이지 않는다.
			//int GripperGripZPos = GetPrivateProfileInt(Section_GripperPos, Key_Gripper_ZPos_Grip, 0, VS10Master_PARAM_INI_PATH);
			////if(abs(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]-GripperGripZPos)>AXIS_INPOS_CTS)
			//if(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]>=GripperGripZPos)
			//{
			//	G_WriteInfo(Log_Check,"AXIS_AM01_ScopeShift : Gripper Position Interlock");
			//	return true;
			//}
		}break;
	case AXIS_AM01_ScopeUpDn:
		{
			//Gripper Close 상태 체크
			if(	m_fnGetAM01State_CySol_TEN_GRIPPER1() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER2() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER3() != SOL_DOWN ||
				m_fnGetAM01State_CySol_TEN_GRIPPER4() != SOL_DOWN)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScopeUpDn : Gripper Cylinder Interlock");
				return true;
			}

			//스틱 감지상태에서만 스코프가 내려간다.(올라가는것은 막지 말자)-대기위치까지는 상관없음
			if(m_IO_InD.In10_Sn_LeftTensionStickDetection == 0 && m_IO_InD.In22_Sn_RightTensionStickDetection == 0)
			{
				int ScopeUpDownStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);

				if(newPosition > ScopeUpDownStandby)
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_ScopeUpDn : 스틱이 없는 상태에서 스코프가 내려갈 이유는 없다.");
					return true;
				}
			}
			int ScopeZDownMax = GetPrivateProfileInt(Section_SCOPE, Key_Z_Down_Max, 230000, Interlock_PARAM_INI_PATH);
			if(newPosition>ScopeZDownMax)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_ScopeUpDn : [New(%d)<Max(%d)] Z S/W Limit을 넘었습니다.", newPosition, ScopeZDownMax);
				return true;
			}
		}break;
	case AXIS_AM01_AirBlowLeftRight:
		{
		}break;
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
		{
			//Scope의 X, Y축이 대기 위치에서 벗어나 있는경우 Scope Shift, Drive 범위는 제한된다.
			int ScopeShiftStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_Standby_Shift, 0, VS10Master_PARAM_INI_PATH);
			int ScopeDriveStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_Standby_Drive, 0, VS10Master_PARAM_INI_PATH);
			if((abs(m_AxisNowPos[AXIS_AM01_ScopeShift]-ScopeShiftStandby) > AXIS_INPOS_um) ||
				(abs(m_AxisNowPos[AXIS_AM01_ScopeDrive]-ScopeDriveStandby) > AXIS_INPOS_um) )
			{
				G_WriteInfo(Log_Check,"Tension Axis : AXIS_AM01_ScopeShift, AXIS_AM01_ScopeDrive가");
				G_WriteInfo(Log_Check,"STASNDBY POS [S:%d][D:%d]에 있지 않습니다.", ScopeShiftStandby, ScopeDriveStandby);
				return true;
			}

			//Scan의 X, Y축이 대기 위치에서 벗어나 있는경우 Scan Shift, Drive 범위는 제한된다.
			int ScanShiftStandby = GetPrivateProfileInt(Section_ScanPos, Key_Scan_Standby_Shift, 0, VS10Master_PARAM_INI_PATH);
			int ScanDriveStandby = GetPrivateProfileInt(Section_ScanPos, Key_Scan_Standby_Drive, 0, VS10Master_PARAM_INI_PATH);
			if((abs(m_AxisNowPos[AXIS_AM01_ScanShift]-ScanShiftStandby) > AXIS_INPOS_um) ||
				(abs(m_AxisNowPos[AXIS_AM01_ScanDrive]-ScanDriveStandby) > AXIS_INPOS_um) )
			{
				G_WriteInfo(Log_Check,"Tension Axis : AXIS_AM01_ScanShift, AXIS_AM01_ScanDrive가");
				G_WriteInfo(Log_Check,"STASNDBY POS [S:%d][D:%d]에 있지 않습니다.", ScanShiftStandby, ScanDriveStandby);
				return true;
			}
		}break;

	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
		{
			//Scope의 X, Y축이 대기 위치에서 벗어나 있는경우 Scope Shift, Drive 범위는 제한된다.
			int ScopeShiftStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_Standby_Shift, 0, VS10Master_PARAM_INI_PATH);
			int ScopeDriveStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_Standby_Drive, 0, VS10Master_PARAM_INI_PATH);
			if((abs(m_AxisNowPos[AXIS_AM01_ScopeShift]-ScopeShiftStandby) > AXIS_INPOS_um) ||
				(abs(m_AxisNowPos[AXIS_AM01_ScopeDrive]-ScopeDriveStandby) > AXIS_INPOS_um) )
			{
				G_WriteInfo(Log_Check,"Tension Axis : AXIS_AM01_ScopeShift, AXIS_AM01_ScopeDrive가");
				G_WriteInfo(Log_Check,"STASNDBY POS [S:%d][D:%d]에 있지 않습니다.", ScopeShiftStandby, ScopeDriveStandby);
				return true;
			}

			//Scan의 X, Y축이 대기 위치에서 벗어나 있는경우 Scan Shift, Drive 범위는 제한된다.
			int ScanShiftStandby = GetPrivateProfileInt(Section_ScanPos, Key_Scan_Standby_Shift, 0, VS10Master_PARAM_INI_PATH);
			int ScanDriveStandby = GetPrivateProfileInt(Section_ScanPos, Key_Scan_Standby_Drive, 0, VS10Master_PARAM_INI_PATH);
			if((abs(m_AxisNowPos[AXIS_AM01_ScanShift]-ScanShiftStandby) > AXIS_INPOS_um) ||
				(abs(m_AxisNowPos[AXIS_AM01_ScanDrive]-ScanDriveStandby) > AXIS_INPOS_um) )
			{
				G_WriteInfo(Log_Check,"Tension Axis : AXIS_AM01_ScanShift, AXIS_AM01_ScanDrive가");
				G_WriteInfo(Log_Check,"STASNDBY POS [S:%d][D:%d]에 있지 않습니다.", ScanShiftStandby, ScanDriveStandby);
				return true;
			}

		}break;

	case AXIS_AM01_JigInOut:
		{
			//1)스틱이 감지상태이면 Gripper Z는 TT01_Out위치이어야 한다.
			if(m_IO_InD.In10_Sn_LeftTensionStickDetection == 1 || m_IO_InD.In22_Sn_RightTensionStickDetection == 1)
			{
				int TentionStandbyZPos = GetPrivateProfileInt(Section_GripperPos, Key_Gripper_ZPos_TT01Out, 0, VS10Master_PARAM_INI_PATH);
				//if(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]!= TentionStandbyZPos)
				if(abs(m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]-TentionStandbyZPos) > AXIS_INPOS_CTS)
				{
					G_WriteInfo(Log_Check,"AXIS_AM01_JigInOut : AXIS_AM01_GripperPos 간섭 범위[%d/%d] 내에 있습니다.", TentionStandbyZPos, m_AxisNowPos[AXIS_AM01_TensionLeftUpDn]);
					return true;
				}
			}
			//2)스틱이 비감지상태이면 최소한 Gripper Sol이 BWD인지 체크 하자.

			//3)TT01위치는 UT02이어야 한다.
			if(m_AxisNowPos[AXIS_TT01_TableDrive]>0) // 수정 필요
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_JigInOut : AXIS_TT01_Drive 가  간섭 범위[%d] 내에 있습니다.", m_AxisNowPos[AXIS_TT01_TableDrive]);
				return true;			
			}
			//4)Review Sol Down
			if(m_fnGetAM01State_CySol_Review() != SOL_DOWN)
			{//Review Up Move Limit Check

				G_WriteInfo(Log_Check,"AXIS_AM01_JigInOut: CySol_Review Down 위치에 있지 않습니다.");
					return true;
			}
			//5)Ring Light Sol Down
			if(	m_fnGetAM01State_CySol_RingLight() != SOL_DOWN)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_JigInOut : CySol_RingLight Down 위치에 있지 않습니다.");
				return true;	
			}
			//6)Review(Inspect) Z대기 위치.
			int ScanReviewStandBy = GetPrivateProfileInt(Section_ScanPos, Key_Scan_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			if(abs(m_AxisNowPos[AXIS_AM01_ScanUpDn]-ScanReviewStandBy)>AXIS_INPOS_CTS)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_JigInOut : AXIS_AM01_SCANUPDOWN STASNDBY POS [%d]에 있지 않습니다.", ScanReviewStandBy);
				return true;
			}
			//7)Space Sensor Z대기 위치.
			int SpaceStandbyZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			if(m_AxisNowPos[AXIS_AM01_SpaceSnUpDn] > SpaceStandbyZPos)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_JigInOut : AXIS_AM01_SpaceSnUpDn 간섭 범위[%d/%d] 내에 있습니다.", SpaceStandbyZPos,m_AxisNowPos[AXIS_AM01_SpaceSnUpDn]);
				return true;
			}
			//8)Scope Z대기 위치.
			////Scope Z Up Limit
			////Scope의 Z축이 대기 위치에서 벗어나 있는경우 Scope Drive 범위는 제한된다.
			//int ScopeUpDownStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
			////if(abs(m_AxisNowPos[AXIS_AM01_ScopeUpDn]-ScopeUpDownStandby) > AXIS_INPOS_CTS)

			//if(m_AxisNowPos[AXIS_AM01_ScopeUpDn]>ScopeUpDownStandby)
			//{
			//	G_WriteInfo(Log_Check,"AXIS_AM01_JigInOut : AXIS_AM01_SCOPEUPDOWN STASNDBY POS [%d]에 있지 않습니다.", ScopeUpDownStandby);
			//	return true;
			//}
			if(m_bScopeSafetyPos != true)
			{
				G_WriteInfo(Log_Check,"AXIS_AM01_JigInOut : AXIS_AM01_SCOPEUPDOWN Scope가 안전 위치에 있지 않습니다.");
				return true;
			}
			//9)Scan Drive, Shift대기 위치.
			//10)Scope Drive, Shift대기 위치.

		}break;
	default: 
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
//Motion Ctrl (ABS Move)
bool CMotCtrl::m_fnMotAbsMove(int AxisNum, long newPosition)
{
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
	case AXIS_TT01_ThetaAlign:
	case AXIS_TT01_TableTurn:
		{
			return m_fnTT01AbsMove( AxisNum, newPosition);
		}break;
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			return m_fnBT01AbsMove( AxisNum, newPosition);
		}break;
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
	case AXIS_BO01_BoxUpDn://좌우 동기축.
	case AXIS_BO01_BoxDrive:
		{
			return m_fnBO01AbsMove( AxisNum, newPosition);
		}break;
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
			return m_fnLT01AbsMove( AxisNum, newPosition);
		}break;
		//UT01
	case AXIS_UT01_BoxUnLoadPush:
		{
			return m_fnUT01AbsMove( AxisNum, newPosition);
		}break;
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
			return m_fnTM01AbsMove( AxisNum, newPosition);
		}break;
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
	case AXIS_TM02_PickerUpDown:
	case AXIS_TM02_PickerShift:
	case AXIS_TM02_PickerRotate:
		{
			return m_fnTM02AbsMove( AxisNum, newPosition);
		}break;
		//AM01
	case AXIS_AM01_ScanDrive:
	case AXIS_AM01_ScanShift:
	case AXIS_AM01_ScanUpDn:
	case AXIS_AM01_SpaceSnUpDn:
	case AXIS_AM01_ScopeDrive:
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
	case AXIS_AM01_ScopeUpDn:
	case AXIS_AM01_AirBlowLeftRight:
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
	case AXIS_AM01_JigInOut:
		{
			return m_fnAM01AbsMove( AxisNum, newPosition);
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnMotStop(int AxisNum)
{
	//if(m_fnMotInterlockCheck(AxisNum) == false)
	//{
	//	return false;
	//}
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
	case AXIS_TT01_ThetaAlign:
	case AXIS_TT01_TableTurn:
		{
			return m_fnTT01Stop( AxisNum); 
		}break;
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			return m_fnBT01Stop( AxisNum); 
		}break;
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
	case AXIS_BO01_BoxUpDn://좌우 동기축.
	case AXIS_BO01_BoxDrive:
		{
			return m_fnTT01Stop( AxisNum); 
		}break;
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
			return m_fnLT01Stop( AxisNum); 
		}break;
		//UT01
	case AXIS_UT01_BoxUnLoadPush:
		{
			return m_fnUT01Stop( AxisNum); 
		}break;
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
			return m_fnTM01Stop( AxisNum); 
		}break;
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
	case AXIS_TM02_PickerUpDown:
	case AXIS_TM02_PickerShift:
	case AXIS_TM02_PickerRotate:
		{
			return m_fnTM02Stop( AxisNum); 
		}break;
		//AM01
	case AXIS_AM01_ScanDrive:
	case AXIS_AM01_ScanShift:
	case AXIS_AM01_ScanUpDn:
	case AXIS_AM01_SpaceSnUpDn:
	case AXIS_AM01_ScopeDrive:
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
	case AXIS_AM01_ScopeUpDn:
	case AXIS_AM01_AirBlowLeftRight:
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
	case AXIS_AM01_JigInOut:
		{
			return m_fnAM01Stop( AxisNum); 
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnMotAllStop()
{
	//return true;
	CString UmacCMD;
	//1)Motion Program 정지.

	//2)PLC프로그램 정지.
	UmacCMD.Format("dis plc 7");//#30 Gentry Home Stop
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 8");//#31 Gentry Home Stop
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 10");//10번  Axis Link Block A Homing 명령
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 11");//11번  Axis Link Block B Homing 명령
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 12");//12번  Axis Link Block C Homing 명령
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 13");//13번  Axis Link Block D Homing 명령
	m_fnUmacOnlineCommand(UmacCMD);

	UmacCMD.Format("dis plc 21");//21번 TT01 Cruiser Axis #1, #18, #19
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 22");//22번 BT01 ML3 #22
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 23");//23번 BO01 ML3 #15, #16 (#10, #11 좌우 동기축),(#12, #13 좌우 동기축), 
											 //(#10, #11은 동시에 초기화 진행)(#12, #13은 동시에 초기화 진행, ) 
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 24");//24번 TM01 ML3 #17
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 25");//25번 TM02 Cruiser Axis  #20, #21 (Gantry구동 PLC 8) 
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 26");//26번 AM01 Cruiser Axis #6, #2, #5, (좌우 Gantry구동 PLC 7)
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 27");//27번 LT01 ML3 #14,
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("dis plc 28");//28번 UT01 ML3 #15,
	m_fnUmacOnlineCommand(UmacCMD);


	//3)Motion정지.
	UmacCMD.Format("#1J/ #2J/");
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("#30J/");//#3, #4
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("#5J/ #6J/");
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("#31J/");//#7, #8
	m_fnUmacOnlineCommand(UmacCMD);

	UmacCMD.Format("#10J/ #11J/");
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("#12J/ #13J/");
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("#14J/ #15J/");
	m_fnUmacOnlineCommand(UmacCMD);

	UmacCMD.Format("#16J/ #17J/");
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("#18J/ #19J/");
	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("#20J/ #21J/ #22J/");
	m_fnUmacOnlineCommand(UmacCMD);
	
	//Axis Link A,B,C,D Block이 모두 비상정지 한다.
	UmacCMD.Format("%s=%d", AL_MOT_BLOCK_ALL_ESTOP, 0x1);
	m_fnUmacOnlineCommand(UmacCMD);

	//UmacCMD.Format("ena plc 19");//Axis link 비상 정지
	//m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
//////////////////////////////////////////////////////////////////////////
//TT01
bool CMotCtrl::m_fnTT01AbsMove(int AxisNum, long newPosition)
{
	if(m_fnTT01InterlockCheck(AxisNum, newPosition) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive://Cruiser Axis #1
		{
			//UmacCMD.Format("#1J=%d", newPosition);
			UmacCMD.Format("#1J=%d", (int) (newPosition*Cruiser_Encoder_DivValue1));
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TT01_ThetaAlign://ML3 #18
		{
			UmacCMD.Format("#18J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TT01_TableTurn://ML3 #19
		{
			UmacCMD.Format("#19J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnTT01Stop(int AxisNum)
{
	//if(m_fnTT01InterlockCheck(AxisNum) == true)
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive://Cruiser Axis #1
		{
			UmacCMD.Format("#1J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TT01_ThetaAlign://ML3 #18
		{
			UmacCMD.Format("#18J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TT01_TableTurn://ML3 #19
		{
			UmacCMD.Format("#19J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//BT01
bool CMotCtrl::m_fnBT01AbsMove(int AxisNum, long newPosition)
{
	
	if(m_fnBT01InterlockCheck(AxisNum, newPosition) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BT01
	case AXIS_BT01_CassetteUpDown://ML3 #22
		{
			m_BT01CouplingCnt[0] = 0;
			m_BT01CouplingCnt[1] = 0;
			UmacCMD.Format("#22J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnBT01Stop(int AxisNum)
{
	//if(m_fnBT01InterlockCheck(AxisNum) == true)
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BT01
	case AXIS_BT01_CassetteUpDown://ML3 #22
		{
			UmacCMD.Format("#22J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//LT01
bool CMotCtrl::m_fnLT01AbsMove(int AxisNum, long newPosition)
{

	if(m_fnLT01InterlockCheck(AxisNum, newPosition) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//LT01
	case AXIS_LT01_BoxLoadPush://ML3 #14
		{
			UmacCMD.Format("#14J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnLT01Stop(int AxisNum)
{
	//if(m_fnLT01InterlockCheck(AxisNum) == true)
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//LT01
	case AXIS_LT01_BoxLoadPush://ML3 #14
		{
			UmacCMD.Format("#14J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//UT01
bool CMotCtrl::m_fnUT01AbsMove(int AxisNum, long newPosition)
{

	if(m_fnUT01InterlockCheck(AxisNum, newPosition) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//UT01
	case AXIS_UT01_BoxUnLoadPush://ML3 #15
		{
			UmacCMD.Format("#15J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnUT01Stop(int AxisNum)
{
	//if(m_fnUT01InterlockCheck(AxisNum) == true)
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
	case AXIS_UT01_BoxUnLoadPush://ML3 #15
		{
			UmacCMD.Format("#15J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//BO01
bool CMotCtrl::m_fnBO01AbsMove(int AxisNum, long newPosition)
{
	if(m_fnBO01InterlockCheck(AxisNum, newPosition) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BO01
	case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
		{
			UmacCMD.Format("#10J=%d #11J=%d", newPosition, newPosition+m_BO01_BoxRotateOffset);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
		{
			UmacCMD.Format("#12J=%d #13J=%d", newPosition, newPosition+m_BO01_BoxUpDnOffset);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_BO01_BoxDrive://ML3 #16
		{
			UmacCMD.Format("#16J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnBO01Stop(int AxisNum)
{
	//if(m_fnBO01InterlockCheck(AxisNum) == true)
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BO01
	case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
		{
			UmacCMD.Format("#10J/ #11J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
		{
			UmacCMD.Format("#12J/ #13J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_BO01_BoxDrive://ML3 #16
		{
			UmacCMD.Format("#16J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//TM01
bool CMotCtrl::m_fnTM01AbsMove(int AxisNum, long newPosition)
{
	if(m_fnTM01InterlockCheck(AxisNum, newPosition) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM01
	case AXIS_TM01_ForwardBackward://ML3 #17
		{
			UmacCMD.Format("#17J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnTM01Stop(int AxisNum)
{
	//if(m_fnTM01InterlockCheck(AxisNum) == true)
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM01
	case AXIS_TM01_ForwardBackward://ML3 #17
		{
			UmacCMD.Format("#17J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//TM02
bool CMotCtrl::m_fnTM02AbsMove(int AxisNum, long newPosition)
{
	if(m_fnTM02InterlockCheck(AxisNum, newPosition) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM02
	case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
		{
			//UmacCMD.Format("#31J=%d", (newPosition*Cruiser_Encoder_DivValue1));
			UmacCMD.Format("#31J=%d", (newPosition));  //2014.12.15 GJJ
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerUpDown://ML3 #20
		{
			UmacCMD.Format("#20J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerShift://ML3 #21
		{
			UmacCMD.Format("#21J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerRotate://AxisLink_A Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_Y_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 15");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnTM02Stop(int AxisNum)
{
	//if(m_fnTM02InterlockCheck(AxisNum) == true)
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM02
	case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
		{
			UmacCMD.Format("#31J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerUpDown://ML3 #20
		{
			UmacCMD.Format("#20J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerShift://ML3 #21
		{
			UmacCMD.Format("#21J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerRotate://AxisLink_A Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//AM01
bool CMotCtrl::m_fnAM01AbsMove(int AxisNum, long newPosition)
{
	if(m_fnAM01InterlockCheck(AxisNum, newPosition) == true)
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//AM01
	case AXIS_AM01_ScanDrive://Cruiser Axis #6
		{
			int DriveAxisPos =  (newPosition*Cruiser_Encoder_DivValue2)+(m_ReviewLensOffsetDrive*Cruiser_Encoder_DivValue2);
			UmacCMD.Format("#6J=%d", DriveAxisPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScanShift://Cruiser Axis #2
		{
			int ShiftAxisPos =  (int) (newPosition*Cruiser_Encoder_DivValue1)+ (int)(m_ReviewLensOffsetShift*Cruiser_Encoder_DivValue1);
			UmacCMD.Format("#2J=%d", ShiftAxisPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScanUpDn://AxisLink_D X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_X_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 18");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_Y_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 18");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeDrive://Cruiser Axis #5
		{
			int DriveAxisPos =  (newPosition*Cruiser_Encoder_DivValue2)+(m_ScopeLensOffsetDrive*Cruiser_Encoder_DivValue2);
			UmacCMD.Format("#5J=%d", DriveAxisPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
		{
			int ShiftAxisPos =   (int)(newPosition*Cruiser_Encoder_DivValue1)+ (int)(m_ScopeLensOffsetShift*Cruiser_Encoder_DivValue1);
			UmacCMD.Format("#30J=%d", ShiftAxisPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeUpDn://AxisLink_D Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_Z_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 18");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_U_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 18");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_Z_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_U_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x4|0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 15");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_TensionRightUpDn://AxisLink_A U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_U_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 15");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
		//////////////////////////////////////////////////////////////////////////
		//Axis Link Block B
	case AXIS_AM01_LTension1://AxisLink_B X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_X_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 16");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension2://AxisLink_B Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_Y_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 16");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension3://AxisLink_B Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_Z_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 16");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension4://AxisLink_B U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_U_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 16");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
		//////////////////////////////////////////////////////////////////////////
		//Axis Link Block C
	case AXIS_AM01_RTension1://AxisLink_C X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_X_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 17");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension2://AxisLink_C Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_Y_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 17");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension3://AxisLink_C Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_Z_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 17");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension4://AxisLink_C U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_U_NewPOS, newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 17");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;

	case AXIS_AM01_JigInOut://ML3 #23 
		{
#ifdef MOT_USE_UMAC_JIG_23AXIS
			UmacCMD.Format("#23J=%d", newPosition);
			m_fnUmacOnlineCommand(UmacCMD);
#endif
		}break;
	default: 
		return false;
	}
	return true;
}
bool CMotCtrl::m_fnAM01Stop(int AxisNum)
{

	//if(m_fnAM01InterlockCheck(AxisNum) == true)
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//AM01
	case AXIS_AM01_ScanDrive://Cruiser Axis #6
		{
			UmacCMD.Format("#6J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScanShift://Cruiser Axis #2
		{
			UmacCMD.Format("#2J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScanUpDn://AxisLink_D X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeDrive://Cruiser Axis #5
		{
			UmacCMD.Format("#5J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
		{
			UmacCMD.Format("#30J/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeUpDn://AxisLink_D Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;

	case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x4|0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	//case AXIS_AM01_TensionRightUpDn://AxisLink_A U
	//	{
	//		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x8);
	//		m_fnUmacOnlineCommand(UmacCMD);
	//		UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_SSTOP);
	//		m_fnUmacOnlineCommand(UmacCMD);
	//		//UmacCMD.Format("ena plc 19");
	//		//m_fnUmacOnlineCommand(UmacCMD);
	//	}break;

	case AXIS_AM01_LTension1://AxisLink_B X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);

		}break;
	case AXIS_AM01_LTension2://AxisLink_B Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension3://AxisLink_B Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension4://AxisLink_B U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;


	case AXIS_AM01_RTension1://AxisLink_C X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension2://AxisLink_C Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension3://AxisLink_C Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension4://AxisLink_C U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_SSTOP);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 19");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_JigInOut://ML3 #23
		{
#ifdef MOT_USE_UMAC_JIG_23AXIS
			UmacCMD.Format("#23J/");
			m_fnUmacOnlineCommand(UmacCMD);
#endif
		}break;
	default: 
		return false;
	}
	return true;
}

bool CMotCtrl::m_fnAM01ScanMultiMoveXY(long newPosShift, long newPosDirve)
{
	if( m_fnAM01InterlockCheck(AXIS_AM01_ScanDrive, newPosDirve) == true ||//Cruiser Axis #6
		m_fnAM01InterlockCheck(AXIS_AM01_ScanShift, newPosShift) == true)//Cruiser Axis #2
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;

	int ShiftAxisPos =   (int)(newPosShift*Cruiser_Encoder_DivValue1)+ (int)(m_ReviewLensOffsetShift*Cruiser_Encoder_DivValue1);
	int DriveAxisPos =  (newPosDirve*Cruiser_Encoder_DivValue2)+(m_ReviewLensOffsetDrive*Cruiser_Encoder_DivValue2);

	UmacCMD.Format("#2J=%d #6J=%d", ShiftAxisPos, DriveAxisPos);
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
bool CMotCtrl::m_fnAM01ScanMultiStopXY()
{
	//if( m_fnAM01InterlockCheck(AXIS_AM01_ScanDrive) == true ||//Cruiser Axis #6
	//	m_fnAM01InterlockCheck(AXIS_AM01_ScanShift) == true)//Cruiser Axis #2
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	UmacCMD.Format("#6J/ #2J/");
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}

bool CMotCtrl::m_fnAM01ScopeMultiMoveXY(long newPosShift, long newPosDirve)
{
	if( m_fnAM01InterlockCheck(AXIS_AM01_ScopeDrive, newPosDirve) == true ||//Cruiser Axis #5
		m_fnAM01InterlockCheck(AXIS_AM01_ScopeShift, newPosShift) == true)//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;

	int ShiftAxisPos =   (int)(newPosShift*Cruiser_Encoder_DivValue1)+ (int)(m_ScopeLensOffsetShift*Cruiser_Encoder_DivValue1);
	int DriveAxisPos =  (newPosDirve*Cruiser_Encoder_DivValue2)+(m_ScopeLensOffsetDrive*Cruiser_Encoder_DivValue2);

	UmacCMD.Format("#30J=%d #5J=%d", ShiftAxisPos , DriveAxisPos);
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
bool CMotCtrl::m_fnAM01ScopeMultiStopXY()
{
	//if( m_fnAM01InterlockCheck(AXIS_AM01_ScopeDrive) == true ||//Cruiser Axis #5
	//	m_fnAM01InterlockCheck(AXIS_AM01_ScopeShift) == true)//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	UmacCMD.Format("#5J/ #30J/");
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
bool CMotCtrl::m_fnAM01SyncMoveXY(long newScanPosShift, long newScanPosDirve, long newScopePosShift, long newScopePosDirve)
{
	if( m_fnAM01InterlockCheck(AXIS_AM01_ScanShift, newScanPosShift) == true ||//Cruiser Axis #2
		m_fnAM01InterlockCheck(AXIS_AM01_ScanDrive, newScanPosDirve) == true ||//Cruiser Axis #6
		m_fnAM01InterlockCheck(AXIS_AM01_ScopeDrive, newScopePosDirve) == true ||//Cruiser Axis #5
		m_fnAM01InterlockCheck(AXIS_AM01_ScopeShift, newScopePosShift) == true)//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
	{
		return false;
	}
	CString UmacCMD;
	DWORD HomeFlag = 0;

	int ScanShiftAxisPos =   (int)(newScanPosShift*Cruiser_Encoder_DivValue1)+ (int)(m_ReviewLensOffsetShift*Cruiser_Encoder_DivValue1);
	int ScanDriveAxisPos =  (newScanPosDirve*Cruiser_Encoder_DivValue2)+(m_ReviewLensOffsetDrive*Cruiser_Encoder_DivValue2);
	int ScopeShiftAxisPos =   (int)(newScopePosShift*Cruiser_Encoder_DivValue1)+ (int)(m_ScopeLensOffsetShift*Cruiser_Encoder_DivValue1);
	int ScopeDriveAxisPos =  (newScopePosDirve*Cruiser_Encoder_DivValue2)+(m_ScopeLensOffsetDrive*Cruiser_Encoder_DivValue2);

	UmacCMD.Format("#2J=%d #6J=%d #30J=%d #5J=%d",
		ScanShiftAxisPos, ScanDriveAxisPos,
		ScopeShiftAxisPos,ScopeDriveAxisPos);
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
bool CMotCtrl::m_fnAM01SyncStopXY()
{
	//if( m_fnAM01InterlockCheck(AXIS_AM01_ScopeDrive) == true ||//Cruiser Axis #5
	//	m_fnAM01InterlockCheck(AXIS_AM01_ScopeShift) == true)//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
	//{
	//	return false;
	//}
	CString UmacCMD;
	DWORD HomeFlag = 0;
	UmacCMD.Format("#6J/ #2J/ #5J/ #30J/");
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
bool CMotCtrl::m_fnAM01TensionUpDn(long newPosition)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	if(m_fnAM01InterlockCheck(AXIS_AM01_TensionLeftUpDn, newPosition) == true ||
		m_fnAM01InterlockCheck(AXIS_AM01_TensionRightUpDn, newPosition) == true)
	{
		return false;
	}
	else
	{
		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_Z_NewPOS, newPosition);
		m_fnUmacOnlineCommand(UmacCMD);
		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_U_NewPOS, newPosition+m_AM01_TensionUpDnOffset);
		m_fnUmacOnlineCommand(UmacCMD);

		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, (0x4|0x8));//Block A에 3, 4번축
		m_fnUmacOnlineCommand(UmacCMD);

		UmacCMD.Format("ena plc 15");
		m_fnUmacOnlineCommand(UmacCMD);
	}
	return true;
}
bool CMotCtrl::m_fnAM01TensionStart(int SelectAxis, float fTargetValue)
{
	if(SelectAxis ==0)
		return false;

	CString UmacCMD;
	DWORD HomeFlag = 0;
	bool TentionLinkOpen =  true;
	if((SelectAxis&0x01) > 0)
	{
		//Tention Sensor Open
		if(G_TesionCtlrObj[0].LinkOpen() == false)
		{
			if(G_TesionCtlrObj[0].LinkOpen() == false)
			{
				TentionLinkOpen= false;
			}
		}

		G_TesionCtlrObj[0].SetTentionValue(fTargetValue, 1.0f);
		G_TesionCtlrObj[0].SetStreamMod(true);
	}

	if((SelectAxis&0x02) > 0)
	{
		//Tention Sensor Open
		if(G_TesionCtlrObj[1].LinkOpen() == false)
		{
			if(G_TesionCtlrObj[1].LinkOpen() == false)
			{
				TentionLinkOpen= false;
			}
		}

		G_TesionCtlrObj[1].SetTentionValue(fTargetValue, 1.0f);
		G_TesionCtlrObj[1].SetStreamMod(true);
	}

	if((SelectAxis&0x04) > 0)
	{
		//Tention Sensor Open
		if(G_TesionCtlrObj[2].LinkOpen() == false)
		{
			if(G_TesionCtlrObj[2].LinkOpen() == false)
			{
				TentionLinkOpen= false;
			}
		}
		G_TesionCtlrObj[2].SetTentionValue(fTargetValue, 1.0f);
		G_TesionCtlrObj[2].SetStreamMod(true);
	}

	if((SelectAxis&0x08) > 0)
	{
		//Tention Sensor Open
		if(G_TesionCtlrObj[3].LinkOpen() == false)
		{
			if(G_TesionCtlrObj[3].LinkOpen() == false)
			{
				TentionLinkOpen= false;
			}
		}
		G_TesionCtlrObj[3].SetTentionValue(fTargetValue, 1.0f);
		G_TesionCtlrObj[3].SetStreamMod(true);
	}

	if(TentionLinkOpen== false)
	{
		G_TesionCtlrObj[0].SetStreamMod(false);
		G_TesionCtlrObj[1].SetStreamMod(false);
		G_TesionCtlrObj[2].SetStreamMod(false);
		G_TesionCtlrObj[3].SetStreamMod(false);
		return false;
	}
	CString strSetData;
	//////////////////////////////////////////////////////////////////////////
	//기본 인장 Step 값 전달.
	strSetData.Format("%s%d ", LEFT_LOADCELL_STEP_VALUE1, 1);
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	strSetData.Format("%s%d ", LEFT_LOADCELL_STEP_VALUE2, 1);
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	strSetData.Format("%s%d ", LEFT_LOADCELL_STEP_VALUE3, 1);
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	strSetData.Format("%s%d ", LEFT_LOADCELL_STEP_VALUE4, 1);
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	//////////////////////////////////////////////////////////////////////////
	strSetData.Format("%s%f ", LEFT_LOADCELL_VALUE1, 0.0f);
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	strSetData.Format("%s%f", LEFT_LOADCELL_VALUE2, 0.0f);
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	strSetData.Format("%s%f", LEFT_LOADCELL_VALUE3, 0.0f);
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	strSetData.Format("%s%f", LEFT_LOADCELL_VALUE4, 0.0f);
	G_MotCtrlObj.m_fnUmacOnlineCommand(strSetData);
	Sleep(150);//Process준비 Delay
	UmacCMD.Format("%s=%d", AXIS_LINK_B_LOADCELL_SELECT, SelectAxis);//Block 
	m_fnUmacOnlineCommand(UmacCMD);

	UmacCMD.Format("ena plc 4");
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
bool CMotCtrl::m_fnAM01TensionStop()
{
	CString UmacCMD;
	DWORD HomeFlag = 0;

	G_TesionCtlrObj[0].SetStreamMod(false);
	G_TesionCtlrObj[1].SetStreamMod(false);
	G_TesionCtlrObj[2].SetStreamMod(false);
	G_TesionCtlrObj[3].SetStreamMod(false);

	UmacCMD.Format("dis plc 4");
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}

bool CMotCtrl::m_fnAM01SpaceMeasureStart(float fSpaceValue)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	
	
	bool TentionLinkOpen =  true;

	//Tention Sensor Open
	if(G_SpaceCtrObj.LinkOpen() == false)
	{
		if(G_SpaceCtrObj.LinkOpen() == false)
		{
			TentionLinkOpen= false;
		}
	}

	G_SpaceCtrObj.SetTargetSpaceValue(fSpaceValue, 0.1f);
	G_SpaceCtrObj.SetMeasureOnOff(true);
	G_SpaceCtrObj.SetStreamMod(true);

	if(TentionLinkOpen== false)
	{
		G_SpaceCtrObj.SetMeasureOnOff(false);
		G_SpaceCtrObj.SetStreamMod(false);
		return false;
	}


	UmacCMD.Format("ena plc 6");
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
bool CMotCtrl::m_fnAM01SpaceMeasureStop()
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	G_SpaceCtrObj.SetMeasureOnOff(false);
	G_SpaceCtrObj.SetStreamMod(false);
	UmacCMD.Format("dis plc 6");
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}
bool CMotCtrl::m_fnAM01TenstionLMultiAbsMoveInterlockCheck()
{
	return false;
}
bool CMotCtrl::m_fnAM01TenstionLMultiAbsMove(int SelectAxis, int AxisXPos, int AxisYPos, int AxisZPos, int AxisUPos )
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	if(m_fnAM01TenstionLMultiAbsMoveInterlockCheck() == true || SelectAxis<=0)
	{
		return false;
	}
	else
	{
		if((SelectAxis &0x01)>0)
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_X_NewPOS, AxisXPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}
		if((SelectAxis &0x02)>0)
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_Y_NewPOS, AxisYPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}
		if((SelectAxis &0x04)>0)
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_Z_NewPOS, AxisZPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}
		if((SelectAxis &0x08)>0)
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_U_NewPOS, AxisUPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}
		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, (0x00FF & SelectAxis));//Block B에 선택축 
		m_fnUmacOnlineCommand(UmacCMD);
		UmacCMD.Format("ena plc 16");
		m_fnUmacOnlineCommand(UmacCMD);
	}
	return true;
}

bool CMotCtrl::m_fnAM01TenstionLMultiAbsStop(bool AllEStop, int SelectAxis)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	 if(AllEStop == true)
	 {
		 UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x0F);
	 }
	 else
	 {
		 //if(SelectAxis<= 0)
			// return false;
		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x0F);
	 }

	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_SSTOP);
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}

bool CMotCtrl::m_fnAM01TenstionRMultiAbsMoveInterlockCheck()
{
	return false;
}
bool CMotCtrl::m_fnAM01TenstionRMultiAbsMove(int SelectAxis, int AxisXPos, int AxisYPos, int AxisZPos, int AxisUPos )
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	if(m_fnAM01TenstionRMultiAbsMoveInterlockCheck() == true || SelectAxis<=0)
	{
		return false;
	}
	else
	{
		if((SelectAxis &0x01)>0)
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_X_NewPOS, AxisXPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}
		if((SelectAxis &0x02)>0)
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_Y_NewPOS, AxisYPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}
		if((SelectAxis &0x04)>0)
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_Z_NewPOS, AxisZPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}
		if((SelectAxis &0x08)>0)
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_U_NewPOS, AxisUPos);
			m_fnUmacOnlineCommand(UmacCMD);
		}
		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, (0x00FF & SelectAxis));//Block B에 선택축 
		m_fnUmacOnlineCommand(UmacCMD);
		UmacCMD.Format("ena plc 17");
		m_fnUmacOnlineCommand(UmacCMD);
	}
	return true;
}
bool CMotCtrl::m_fnAM01TenstionRMultiAbsStop(bool AllEStop, int SelectAxis)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	if(AllEStop == true)
	{
		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x0F);//Block B에 선택축 
	}
	else
	{
		//if(SelectAxis<= 0)
		//	return false;
		UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x0F);//Block B에 선택축 
	}

	m_fnUmacOnlineCommand(UmacCMD);
	UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_SSTOP);
	m_fnUmacOnlineCommand(UmacCMD);
	return true;
}

bool CMotCtrl::fn_SetTrigger_Inspection(float TriggerSize_um)
{
	//M608=M601-50
	//M609=M601+50
	//M610=100
	//M612=0
	//M611=1
	int nTrOpt = (int)((double)TriggerSize_um/0.05);
	int nEdge = nTrOpt/4;

	int nValue_M601 = atoi(m_fnUmacOnlineCommand("M601"));
	int nValue_M608=nValue_M601-nEdge;
	int nValue_M609=nValue_M601+nEdge;
	int nValue_M610=nTrOpt;
	int nValue_M612=0;
	int nValue_M611=1;

	CString strWriteCMD;
	strWriteCMD.Format("M608=%d", nValue_M608);
	m_fnUmacOnlineCommand(strWriteCMD);
	strWriteCMD.Format("M609=%d", nValue_M609);
	m_fnUmacOnlineCommand(strWriteCMD);
	strWriteCMD.Format("M610=%d", nValue_M610);
	m_fnUmacOnlineCommand(strWriteCMD);
	strWriteCMD.Format("M612=%d", nValue_M612);
	m_fnUmacOnlineCommand(strWriteCMD);
	strWriteCMD.Format("M611=%d", nValue_M611);
	m_fnUmacOnlineCommand(strWriteCMD);
	return true;
}
bool CMotCtrl::fn_SetTrigger_AreaScan(float TriggerSize_um)
{
	//M508=M501-500
	//M509=M501+500
	//M510=2000
	//M512=0
	//M511=1

	int nTrOpt = (int)((double)TriggerSize_um/0.05);
	int nEdge = nTrOpt/4;

	int nValue_M501 = atoi(m_fnUmacOnlineCommand("M501"));
	int nValue_M508=nValue_M501-nEdge;
	int nValue_M509=nValue_M501+nEdge;
	int nValue_M510=nTrOpt;
	int nValue_M512=0;
	int nValue_M511=1;

	CString strWriteCMD;
	strWriteCMD.Format("M508=%d", nValue_M508);
	m_fnUmacOnlineCommand(strWriteCMD);
	strWriteCMD.Format("M509=%d", nValue_M509);
	m_fnUmacOnlineCommand(strWriteCMD);
	strWriteCMD.Format("M510=%d", nValue_M510);
	m_fnUmacOnlineCommand(strWriteCMD);
	strWriteCMD.Format("M512=%d", nValue_M512);
	m_fnUmacOnlineCommand(strWriteCMD);
	strWriteCMD.Format("M511=%d", nValue_M511);
	m_fnUmacOnlineCommand(strWriteCMD);

	return true;
}
//////////////////////////////////////////////////////////////////////////
//Motion Ctrl (Idling)
bool CMotCtrl::m_fnMotStandby(int AxisNum)
{
	CHECK_AXIS_ALL(AxisNum);
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
	case AXIS_TT01_ThetaAlign:
	case AXIS_TT01_TableTurn:
		{
			return m_fnTT01Standby(AxisNum);
		}break;
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			return m_fnBT01Idling(AxisNum);
		}break;
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
			return m_fnLT01Idling(AxisNum);
		}break;
		//UT01
	case AXIS_UT01_BoxUnLoadPush:
		{
			return m_fnUT01Idling(AxisNum);
		}break;
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
	case AXIS_BO01_BoxUpDn://좌우 동기축.
	case AXIS_BO01_BoxDrive:
		{
			return m_fnBO01Standby(AxisNum);
		}break;
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
			return m_fnTM01Standby(AxisNum);
		}break;
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
	case AXIS_TM02_PickerUpDown:
	case AXIS_TM02_PickerShift:
	case AXIS_TM02_PickerRotate:
		{
			return m_fnTM02Standby(AxisNum);
		}break;
		//AM01
	case AXIS_AM01_ScanDrive:
	case AXIS_AM01_ScanShift:
	case AXIS_AM01_ScanUpDn:
	case AXIS_AM01_SpaceSnUpDn:
	case AXIS_AM01_ScopeDrive:
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
	case AXIS_AM01_ScopeUpDn:
	case AXIS_AM01_AirBlowLeftRight:
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
	case AXIS_AM01_JigInOut:
		{
			return m_fnAM01Standby(AxisNum);
		}break;
	default: 
		return false;
	}
}
//////////////////////////////////////////////////////////////////////////
//TT01
bool CMotCtrl::m_fnTT01Standby(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive://Cruiser Axis #1
		{
			UmacCMD.Format("#1$");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TT01_ThetaAlign://ML3 #18
		{
			UmacCMD.Format("#18$");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TT01_TableTurn://ML3 #19
		{
			UmacCMD.Format("#19$");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//BT01
bool CMotCtrl::m_fnBT01Standby(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BT01
	case AXIS_BT01_CassetteUpDown://ML3 #22
		{
			UmacCMD.Format("#22j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//LT01
bool CMotCtrl::m_fnLT01Standby(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//LT01
	case AXIS_LT01_BoxLoadPush://ML3 #14
		{
			UmacCMD.Format("#14j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//UT01
bool CMotCtrl::m_fnUT01Standby(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//UT01
	case AXIS_UT01_BoxUnLoadPush://ML3 #15
		{
			UmacCMD.Format("#15j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//BO01
bool CMotCtrl::m_fnBO01Standby(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BO01
	case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
		{
			UmacCMD.Format("#10j/ #11j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
		{
			UmacCMD.Format("#12j/ #13j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_BO01_BoxDrive://ML3 #16
		{
			UmacCMD.Format("#16j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//TM01
bool CMotCtrl::m_fnTM01Standby(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM01
	case AXIS_TM01_ForwardBackward://ML3 #17
		{
			UmacCMD.Format("#17j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//TM02
bool CMotCtrl::m_fnTM02Standby(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM02
	case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
		{
			UmacCMD.Format("#31$");
			m_fnUmacOnlineCommand(UmacCMD);

			UmacCMD.Format("#7$ #8$");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerUpDown://ML3 #20
		{
			UmacCMD.Format("#20j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerShift://ML3 #21
		{
			UmacCMD.Format("#21j/");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerRotate://AxisLink_A Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//AM01
bool CMotCtrl::m_fnAM01Standby(int AxisNum)
{
	CString UmacCMD;
	switch(AxisNum)
	{
		//AM01
	case AXIS_AM01_ScanDrive://Cruiser Axis #6
		{
			UmacCMD.Format("#6$");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScanShift://Cruiser Axis #2
		{
			UmacCMD.Format("#2$");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScanUpDn://AxisLink_D X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeDrive://Cruiser Axis #5
		{
			UmacCMD.Format("#5$");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
		{
			UmacCMD.Format("#30$");
			m_fnUmacOnlineCommand(UmacCMD);

			UmacCMD.Format("#3$ #4$");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeUpDn://AxisLink_D Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_TensionRightUpDn://AxisLink_A U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension1://AxisLink_B X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension2://AxisLink_B Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension3://AxisLink_B Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension4://AxisLink_B U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension1://AxisLink_C X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension2://AxisLink_C Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension3://AxisLink_C Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension4://AxisLink_C U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_STANDBY);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_JigInOut://ML3 #23
		{
#ifdef MOT_USE_UMAC_JIG_23AXIS
			UmacCMD.Format("#23j/");
			m_fnUmacOnlineCommand(UmacCMD);
#endif
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//Motion Ctrl (Idling)
bool CMotCtrl::m_fnMotIdling(int AxisNum)
{
	CHECK_AXIS_ALL(AxisNum);
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
	case AXIS_TT01_ThetaAlign:
	case AXIS_TT01_TableTurn:
		{
			return m_fnTT01Idling(AxisNum);
		}break;
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			return m_fnBT01Idling(AxisNum);
		}break;
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
			return m_fnLT01Idling(AxisNum);
		}break;
		//UT01
	case AXIS_UT01_BoxUnLoadPush:
		{
			return m_fnUT01Idling(AxisNum);
		}break;

		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
	case AXIS_BO01_BoxUpDn://좌우 동기축.
	case AXIS_BO01_BoxDrive:
		{
			return m_fnBO01Idling(AxisNum);
		}break;
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
			return m_fnTM01Idling(AxisNum);
		}break;
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
	case AXIS_TM02_PickerUpDown:
	case AXIS_TM02_PickerShift:
	case AXIS_TM02_PickerRotate:
		{
			return m_fnTM02Idling(AxisNum);
		}break;
		//AM01
	case AXIS_AM01_ScanDrive:
	case AXIS_AM01_ScanShift:
	case AXIS_AM01_ScanUpDn:
	case AXIS_AM01_SpaceSnUpDn:
	case AXIS_AM01_ScopeDrive:
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
	case AXIS_AM01_ScopeUpDn:
	case AXIS_AM01_AirBlowLeftRight:
	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
	case AXIS_AM01_LTension1:
	case AXIS_AM01_LTension2:
	case AXIS_AM01_LTension3:
	case AXIS_AM01_LTension4:
	case AXIS_AM01_RTension1:
	case AXIS_AM01_RTension2:
	case AXIS_AM01_RTension3:
	case AXIS_AM01_RTension4:
	case AXIS_AM01_JigInOut:
		{
			return m_fnAM01Idling(AxisNum);
		}break;
	default: 
		return false;
	}
}
bool CMotCtrl::m_fnSetMotSpeed(int SpeedMode, int SelectAxis)
{
	//return true;//Home PLC구동 완료 되면 .

	AxisSpeed ReadSpeedTable;
	CString SpeedSection;
	CString SpeedAxisKey;
	switch(SpeedMode)
	{
	case Speed_Normal:
		SpeedSection.Format("%s", Section_SPEED_NORMAL);
		G_WriteInfo(Log_Normal, "Speed Mode Setting(%s)", Section_SPEED_NORMAL);
		break;
	case Speed_Pickup:
		SpeedSection.Format("%s", Section_SPEED_PICKUP);
		G_WriteInfo(Log_Normal, "Speed Mode Setting(%s)", Section_SPEED_PICKUP);
		break;
	case Speed_Teach:
		SpeedSection.Format("%s", Section_SPEED_TEACH);
		G_WriteInfo(Log_Normal, "Speed Mode Setting(%s)", Section_SPEED_TEACH);
		break;
	default:
		SpeedSection.Format("%s", Section_SPEED_NORMAL);
		G_WriteInfo(Log_Normal, "Speed Mode Setting(%s)", Section_SPEED_NORMAL);
		break;
	}
	char strReadData[256];
	if(SelectAxis<AXIS_TT01_TableDrive)
	{
		//전체 모드 변경시 갱신
		m_NowAllSpeedMode = SpeedMode;

		for(int RStepH= AXIS_TT01_TableDrive; RStepH<AXIS_Cnt; RStepH++)
		{

			memset(strReadData, 0x00, 256);
			SpeedAxisKey.Format("%s%s", G_strAxisName[RStepH], Key_MaxAccValue);
			GetPrivateProfileString(SpeedSection, SpeedAxisKey, "0.1", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
			//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
			ReadSpeedTable.m_MaxAccValue = (float)atof(strReadData);

			memset(strReadData, 0x00, 256);
			SpeedAxisKey.Format("%s%s", G_strAxisName[RStepH], Key_AccTime);
			GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
			//WritePrivateProfileString(SpeedSection, SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
			ReadSpeedTable.m_AccTime = atoi(strReadData);

			memset(strReadData, 0x00, 256);
			SpeedAxisKey.Format("%s%s", G_strAxisName[RStepH], Key_DecTime);
			GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
			//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
			ReadSpeedTable.m_DecTime = atoi(strReadData);

			memset(strReadData, 0x00, 256);
			SpeedAxisKey.Format("%s%s", G_strAxisName[RStepH], Key_MoveSpeed);
			GetPrivateProfileString(SpeedSection, SpeedAxisKey, "50", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
			//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
			ReadSpeedTable.m_MoveSpeed = atoi(strReadData);
		

			G_WriteInfo(Log_Info, "Max:%0.3f, AccT:%03d, DecT:%03d, Sp:%03d", 
				ReadSpeedTable.m_MaxAccValue,
				ReadSpeedTable.m_AccTime, 
				ReadSpeedTable.m_DecTime, 
				ReadSpeedTable.m_MoveSpeed);

			CString UmacCMD;
			//Read Value Setting
			switch(RStepH)
			{
			case AXIS_TT01_TableDrive:	//Cruiser Axis #1
				{
					UmacCMD.Format("i119 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i120 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i121 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i122 = %d", (int)(ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue1));
					//UmacCMD.Format("i122 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			case AXIS_TT01_ThetaAlign://ML3 #18
				{
					UmacCMD.Format("i1819 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1820 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1821 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1822 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			case AXIS_TT01_TableTurn://ML3 #19
				{
					UmacCMD.Format("i1919 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1920 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1921 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1922 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			//BT01
			case AXIS_BT01_CassetteUpDown://ML3 #22
				{
					UmacCMD.Format("i2219 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2220 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2221 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2222 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			//LT01
			case AXIS_LT01_BoxLoadPush://ML3 #14
				{
					UmacCMD.Format("i1419 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1420 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1421 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1422 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			//BO01
			case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
				{
					UmacCMD.Format("i1019 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1020 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1021 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1022 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("i1119 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1120 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1121 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1122 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
				{
					UmacCMD.Format("i1219 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1220 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1221 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1222 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);

					UmacCMD.Format("i1319 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1320 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1321 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1322 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			case AXIS_BO01_BoxDrive://ML3 #16
				{
					UmacCMD.Format("i1619 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1620 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1621 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1622 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			//UT01
			case AXIS_UT01_BoxUnLoadPush://ML3 #15
				{
					UmacCMD.Format("i1519 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1520 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1521 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1522 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			//TM01
			case AXIS_TM01_ForwardBackward://ML3 #17
				{
					UmacCMD.Format("i1719 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1720 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1721 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i1722 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			//TM02
			case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
				{
					UmacCMD.Format("i3119 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i3120 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i3121 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					//UmacCMD.Format("i3122 = %d", (ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue));
					UmacCMD.Format("i3122 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			case AXIS_TM02_PickerUpDown://ML3 #20
				{
					UmacCMD.Format("i2019 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2020 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2021 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2022 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			case AXIS_TM02_PickerShift://ML3 #21
				{
					UmacCMD.Format("i2119 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2120 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2121 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2122 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
		
			//AM01
			case AXIS_AM01_ScanDrive://Cruiser Axis #6
				{
					UmacCMD.Format("i619 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i620 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i621 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i622 = %d",  (float) (ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue2));
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			case AXIS_AM01_ScanShift://Cruiser Axis #2
				{
					UmacCMD.Format("i219 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i220 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i221 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i222 = %d",  (int) (ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue1));
					m_fnUmacOnlineCommand(UmacCMD);
				}break;


			case AXIS_AM01_ScopeDrive://Cruiser Axis #5
				{
					UmacCMD.Format("i519 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i520 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i521 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i522 = %d", (int) (ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue2));
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
				{
					UmacCMD.Format("i3019 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i3020 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i3021 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i3022 = %d",  (int) (ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue1));
					m_fnUmacOnlineCommand(UmacCMD);
				}break;
			////case AXIS_NONE://AxisLink_A X
			//case AXIS_TM02_PickerRotate://AxisLink_A Y
			////case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
			////case AXIS_AM01_TensionRightUpDn://AxisLink_A U
			//	{
			//		//값을 선택 해놓으면 해당 축이 구동 할때 적용 된다.

			//		UmacCMD.Format("%s = d", AL_MOT_BLOCK_A_SPEED_PROFILE, m_NowAllSpeedMode+1);
			//		m_fnUmacOnlineCommand(UmacCMD);
			//	}break;
			//case AXIS_AM01_LTension1://AxisLink_B X
			////case AXIS_AM01_LTension2://AxisLink_B Y
			////case AXIS_AM01_LTension3://AxisLink_B Z
			////case AXIS_AM01_LTension4://AxisLink_B U
			//	{
			//		//값을 선택 해놓으면 해당 축이 구동 할때 적용 된다.
			//		UmacCMD.Format("%s = %d", AL_MOT_BLOCK_B_SPEED_PROFILE, m_NowAllSpeedMode+1);
			//		m_fnUmacOnlineCommand(UmacCMD);
			//	}break;
			//case AXIS_AM01_RTension1://AxisLink_C X
			////case AXIS_AM01_RTension2://AxisLink_C Y
			////case AXIS_AM01_RTension3://AxisLink_C Z
			////case AXIS_AM01_RTension4://AxisLink_C U
			//	{
			//		//값을 선택 해놓으면 해당 축이 구동 할때 적용 된다.
			//		UmacCMD.Format("%s = %d", AL_MOT_BLOCK_C_SPEED_PROFILE, m_NowAllSpeedMode+1);
			//		m_fnUmacOnlineCommand(UmacCMD);
			//	}break;
			//case AXIS_AM01_ScanUpDn://AxisLink_D X
			////case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
			////case AXIS_AM01_ScopeUpDn://AxisLink_D Z
			////case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
			//	{
			//		//값을 선택 해놓으면 해당 축이 구동 할때 적용 된다.
			//		UmacCMD.Format("%s = %d", AL_MOT_BLOCK_D_SPEED_PROFILE, m_NowAllSpeedMode+1);
			//		m_fnUmacOnlineCommand(UmacCMD);
			//	}break;
			case AXIS_AM01_JigInOut://ML3 #23
				{
#ifdef MOT_USE_UMAC_JIG_23AXIS
					UmacCMD.Format("i2319 = %f", ReadSpeedTable.m_MaxAccValue);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2320 = %d", ReadSpeedTable.m_AccTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2321 = %d", ReadSpeedTable.m_DecTime);
					m_fnUmacOnlineCommand(UmacCMD);
					UmacCMD.Format("i2322 = %d", ReadSpeedTable.m_MoveSpeed);
					m_fnUmacOnlineCommand(UmacCMD);
#endif
				}break;
			default: 
				return false;
			}
		}
	}
	else
	{
		//if(m_NowAllSpeedMode == Speed_Teach)
		//	return false;// 전체 teach  Mode에서는 속도를 변경 할 수 없다.
		////설정 범위 내의 것이 아니면 Return 한다.
		if(!(Speed_Normal<=SpeedMode&&SpeedMode<Speed_ModCnt))//개별 Speed Setting은 Teach를 사용 할수 없다.
			return false;

		memset(strReadData, 0x00, 256);
		SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_MaxAccValue);
		GetPrivateProfileString(SpeedSection, SpeedAxisKey, "0.1", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
		//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
		ReadSpeedTable.m_MaxAccValue = (float)atof(strReadData);

		memset(strReadData, 0x00, 256);
		SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_AccTime);
		GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
		//WritePrivateProfileString(SpeedSection, SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
		ReadSpeedTable.m_AccTime = atoi(strReadData);

		memset(strReadData, 0x00, 256);
		SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_DecTime);
		GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
		//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
		ReadSpeedTable.m_DecTime = atoi(strReadData);

		memset(strReadData, 0x00, 256);
		SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_MoveSpeed);
		GetPrivateProfileString(SpeedSection, SpeedAxisKey, "50", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
		//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
		ReadSpeedTable.m_MoveSpeed = atoi(strReadData);


		G_WriteInfo(Log_Info, "%s)Max:%0.3f, AccT:%03d, DecT:%03d, Sp:%03d", G_strAxisName[SelectAxis],
			ReadSpeedTable.m_MaxAccValue,
			ReadSpeedTable.m_AccTime, 
			ReadSpeedTable.m_DecTime, 
			ReadSpeedTable.m_MoveSpeed);

		CString UmacCMD;
		//Read Value Setting
		switch(SelectAxis)
		{
		case AXIS_TT01_TableDrive:	//Cruiser Axis #1
			{
				UmacCMD.Format("i119 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i120 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i121 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				//UmacCMD.Format("i122 = %d", ReadSpeedTable.m_MoveSpeed);
				UmacCMD.Format("i122 = %d", (int)(ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue1));
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		case AXIS_TT01_ThetaAlign://ML3 #18
			{
				UmacCMD.Format("i1819 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1820 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1821 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1822 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		case AXIS_TT01_TableTurn://ML3 #19
			{
				UmacCMD.Format("i1919 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1920 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1921 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1922 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
			//BT01
		case AXIS_BT01_CassetteUpDown://ML3 #22
			{
				UmacCMD.Format("i2219 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2220 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2221 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2222 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
			//LT01
		case AXIS_LT01_BoxLoadPush://ML3 #14
			{
				UmacCMD.Format("i1419 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1420 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1421 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1422 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
			//BO01
		case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
			{
				UmacCMD.Format("i1019 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1020 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1021 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1022 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);

				UmacCMD.Format("i1119 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1120 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1121 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1122 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
			{
				UmacCMD.Format("i1219 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1220 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1221 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1222 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);

				UmacCMD.Format("i1319 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1320 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1321 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1322 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		case AXIS_BO01_BoxDrive://ML3 #16
			{
				UmacCMD.Format("i1619 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1620 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1621 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1622 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
			//UT01
		case AXIS_UT01_BoxUnLoadPush://ML3 #15
			{
				UmacCMD.Format("i1519 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1520 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1521 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1522 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
			//TM01
		case AXIS_TM01_ForwardBackward://ML3 #17
			{
				UmacCMD.Format("i1719 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1720 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1721 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i1722 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
			//TM02
		case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
			{
				UmacCMD.Format("i3119 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i3120 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i3121 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i3122 = %d",  ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		case AXIS_TM02_PickerUpDown://ML3 #20
			{
				UmacCMD.Format("i2019 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2020 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2021 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2022 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		case AXIS_TM02_PickerShift://ML3 #21
			{
				UmacCMD.Format("i2119 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2120 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2121 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2122 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
			}break;

			//AM01
		case AXIS_AM01_ScanDrive://Cruiser Axis #6
			{
				UmacCMD.Format("i619 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i620 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i621 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i622 = %d",  (ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue2));
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		case AXIS_AM01_ScanShift://Cruiser Axis #2
			{
				UmacCMD.Format("i219 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i220 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i221 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i222 = %d", (int)(ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue1));
				m_fnUmacOnlineCommand(UmacCMD);
			}break;


		case AXIS_AM01_ScopeDrive://Cruiser Axis #5
			{
				UmacCMD.Format("i519 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i520 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i521 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i522 = %d",  (ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue2));
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
			{
				UmacCMD.Format("i3019 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i3020 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i3021 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i3022 = %d",  (int)(ReadSpeedTable.m_MoveSpeed*Cruiser_Encoder_DivValue1));
				m_fnUmacOnlineCommand(UmacCMD);
			}break;
		////case AXIS_NONE://AxisLink_A X
		//case AXIS_TM02_PickerRotate://AxisLink_A Y
		//case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
		//case AXIS_AM01_TensionRightUpDn://AxisLink_A U
		//	{
		//		//값을 선택 해놓으면 해당 축이 구동 할때 적용 된다.
		//		UmacCMD.Format("%s = %d", AL_MOT_BLOCK_A_SPEED_PROFILE, m_NowAllSpeedMode+1);
		//		m_fnUmacOnlineCommand(UmacCMD);
		//	}break;
		//case AXIS_AM01_LTension1://AxisLink_B X
		//case AXIS_AM01_LTension2://AxisLink_B Y
		//case AXIS_AM01_LTension3://AxisLink_B Z
		//case AXIS_AM01_LTension4://AxisLink_B U
		//	{
		//		//값을 선택 해놓으면 해당 축이 구동 할때 적용 된다.
		//		UmacCMD.Format("%s = %d", AL_MOT_BLOCK_B_SPEED_PROFILE, m_NowAllSpeedMode+1);
		//		m_fnUmacOnlineCommand(UmacCMD);
		//	}break;
		//case AXIS_AM01_RTension1://AxisLink_C X
		//case AXIS_AM01_RTension2://AxisLink_C Y
		//case AXIS_AM01_RTension3://AxisLink_C Z
		//case AXIS_AM01_RTension4://AxisLink_C U
		//	{
		//		//값을 선택 해놓으면 해당 축이 구동 할때 적용 된다.
		//		UmacCMD.Format("%s = %d", AL_MOT_BLOCK_C_SPEED_PROFILE, m_NowAllSpeedMode+1);
		//		m_fnUmacOnlineCommand(UmacCMD);
		//	}break;
		//case AXIS_AM01_ScanUpDn://AxisLink_D X
		//case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
		//case AXIS_AM01_ScopeUpDn://AxisLink_D Z
		//case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
		//	{
		//		//값을 선택 해놓으면 해당 축이 구동 할때 적용 된다.
		//		UmacCMD.Format("%s = %d", AL_MOT_BLOCK_D_SPEED_PROFILE, m_NowAllSpeedMode+1);
		//		m_fnUmacOnlineCommand(UmacCMD);
		//	}break;
		case AXIS_AM01_JigInOut://ML3 #23
			{
#ifdef MOT_USE_UMAC_JIG_23AXIS
				UmacCMD.Format("i2319 = %f", ReadSpeedTable.m_MaxAccValue);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2320 = %d", ReadSpeedTable.m_AccTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2321 = %d", ReadSpeedTable.m_DecTime);
				m_fnUmacOnlineCommand(UmacCMD);
				UmacCMD.Format("i2322 = %d", ReadSpeedTable.m_MoveSpeed);
				m_fnUmacOnlineCommand(UmacCMD);
#endif
			}break;
		default: 
			return false;
		}
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//TT01
bool CMotCtrl::m_fnTT01Idling(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive://Cruiser Axis #1
		{
			UmacCMD.Format("#1k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TT01_ThetaAlign://ML3 #18
		{
			UmacCMD.Format("#18k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TT01_TableTurn://ML3 #19
		{
			UmacCMD.Format("#19k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//BT01
bool CMotCtrl::m_fnBT01Idling(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BT01
	case AXIS_BT01_CassetteUpDown://ML3 #22
		{
			UmacCMD.Format("#22k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//LT01
bool CMotCtrl::m_fnLT01Idling(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//LT01
	case AXIS_LT01_BoxLoadPush://ML3 #14
		{
			UmacCMD.Format("#14k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//UT01
bool CMotCtrl::m_fnUT01Idling(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//UT01
	case AXIS_UT01_BoxUnLoadPush://ML3 #15
		{
			UmacCMD.Format("#15k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//BO01
bool CMotCtrl::m_fnBO01Idling(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//BO01
	case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
		{
			UmacCMD.Format("#10k #11k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
		{
			UmacCMD.Format("#12k #13k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_BO01_BoxDrive://ML3 #16
		{
			UmacCMD.Format("#16k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//TM01
bool CMotCtrl::m_fnTM01Idling(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM01
	case AXIS_TM01_ForwardBackward://ML3 #17
		{
			UmacCMD.Format("#17k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//TM02
bool CMotCtrl::m_fnTM02Idling(int AxisNum)
{
	CString UmacCMD;
	DWORD HomeFlag = 0;
	switch(AxisNum)
	{
		//TM02
	case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
		{
			UmacCMD.Format("#7k #8k #31k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerUpDown://ML3 #20
		{
			UmacCMD.Format("#20k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerShift://ML3 #21
		{
			UmacCMD.Format("#21k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_TM02_PickerRotate://AxisLink_A Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	default: 
		return false;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
//AM01
bool CMotCtrl::m_fnAM01Idling(int AxisNum)
{
	CString UmacCMD;
	switch(AxisNum)
	{
		//AM01
	case AXIS_AM01_ScanDrive://Cruiser Axis #6
		{
			UmacCMD.Format("#6k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScanShift://Cruiser Axis #2
		{
			UmacCMD.Format("#2k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScanUpDn://AxisLink_D X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeDrive://Cruiser Axis #5
		{
			UmacCMD.Format("#5k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
		{
			UmacCMD.Format("#3k #4k #31k");
			m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_ScopeUpDn://AxisLink_D Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_TensionRightUpDn://AxisLink_A U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_A_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_A_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension1://AxisLink_B X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension2://AxisLink_B Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension3://AxisLink_B Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_LTension4://AxisLink_B U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_B_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_B_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension1://AxisLink_C X
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x1);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension2://AxisLink_C Y
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x2);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension3://AxisLink_C Z
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x4);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;
	case AXIS_AM01_RTension4://AxisLink_C U
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_C_SelectAxis, 0x8);
			m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_C_IDLE);
			m_fnUmacOnlineCommand(UmacCMD);
			//UmacCMD.Format("ena plc 14");
			//m_fnUmacOnlineCommand(UmacCMD);
		}break;

	case AXIS_AM01_JigInOut://ML3 #23
		{
#ifdef MOT_USE_UMAC_JIG_23AXIS
			UmacCMD.Format("#23k");
			m_fnUmacOnlineCommand(UmacCMD);
#endif
		}break;
	default: 
		return false;
	}
	return true;
}



//////////////////////////////////////////////////////////////////////////
//
bool CMotCtrl::m_fnTM01SolInterLockCheck_FORK_UP()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnTM01SolInterLockCheck_FORK_DN()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnLT01SolInterLockCheck_GUIDE_FWD()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnLT01SolInterLockCheck_GUIDE_BWD()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnUT01SolInterLockCheck_GUIDE_FWD()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnUT01SolInterLockCheck_GUIDE_BWD()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnBO01SolInterLockCheck_PICKER_FWD()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnBO01SolInterLockCheck_PICKER_BWD()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
//bool CMotCtrl::m_fnBO01SolInterLockCheck_RIGHT_PICKER_FWD()
//{
//return false;
//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
//Interlock이 걸린것을 의미 함.
//}
//bool CMotCtrl::m_fnBO01SolInterLockCheck_RIGHT_PICKER_BWD()
//{
//return false;
//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
//Interlock이 걸린것을 의미 함.
//}
bool CMotCtrl::m_fnTM02SolInterLockCheck_LR_PAPER_PICKER_UP()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnTM02SolInterLockCheck_LR_PAPER_PICKER_DN()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnTM02SolInterLockCheck_CNT_PAPER_PICKER_UP()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnTM02SolInterLockCheck_CNT_PAPER_PICKER_DN()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnAM01SolInterLockCheck_OPEN_TEN_GRIPPER()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnAM01SolInterLockCheck_CLOSE_TEN_GRIPPER()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}


bool CMotCtrl::m_fnAM01SolInterLockCheck_TENTION_FWD()
{

	//구동시에는 항상 열려 있어야 한다.
	//down(close), Up(open)
	if(m_fnGetAM01State_CySol_TEN_GRIPPER1() == SOL_DOWN ||
		m_fnGetAM01State_CySol_TEN_GRIPPER2() == SOL_DOWN ||
		m_fnGetAM01State_CySol_TEN_GRIPPER3() == SOL_DOWN ||
		m_fnGetAM01State_CySol_TEN_GRIPPER4() == SOL_DOWN)
		return true;//Interlock에 걸린것.

	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnAM01SolInterLockCheck_TENTION_BWD()
{
	//Stick 물려 있다면 후진 하면 찢어진다 ㅡ,.ㅡ;;;
	if(m_IO_InD.In10_Sn_LeftTensionStickDetection == 1 || m_IO_InD.In22_Sn_RightTensionStickDetection == 1)
	{
		//down(close), Up(open)
		if(m_fnGetAM01State_CySol_TEN_GRIPPER1() == SOL_DOWN ||
			m_fnGetAM01State_CySol_TEN_GRIPPER2() == SOL_DOWN ||
			m_fnGetAM01State_CySol_TEN_GRIPPER3() == SOL_DOWN ||
			m_fnGetAM01State_CySol_TEN_GRIPPER4() == SOL_DOWN)
		return true;//Interlock에 걸린것.
	}
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}

bool CMotCtrl::m_fnAM01SolInterLockCheck_RingLight_UP()
{
	//Review cylinder Interlock
	if(m_fnGetAM01State_CySol_Review() != SOL_DOWN)
	{
		G_WriteInfo(Log_Check,"Review Cylinder가 Down 상태이어야 합니다.");
		return true;
	}
	//Space Sensor가 대기 위치 이어야 한다.
	int SpaceStandbyZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
	if(m_AxisNowPos[AXIS_AM01_SpaceSnUpDn] > SpaceStandbyZPos)
	{
		G_WriteInfo(Log_Check,"RingLight_UP : Space Up/Down과 같이 동작 할수 없다.");
		return true;
	}

	//위치 Interlock.
	int nowPosition = m_AxisNowPos[AXIS_AM01_ScanShift];
	int ScanShiftMin = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_RingLightSol_Scan_Shift_Min, 301894, Interlock_PARAM_INI_PATH);
	int ScanShiftMax = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_RingLightSol_Scan_Shift_Max, 422756, Interlock_PARAM_INI_PATH);
	int LineScanSiftOffset = GetPrivateProfileInt(Section_ReviewCamToObjDis, Key_LineScanDisX, 0, VS10Master_PARAM_INI_PATH);
	ScanShiftMin+=(LineScanSiftOffset);
	ScanShiftMax+=(LineScanSiftOffset);
	if(nowPosition<(ScanShiftMin-AXIS_INPOS_um) || nowPosition>(ScanShiftMax+AXIS_INPOS_um))
	{
		G_WriteInfo(Log_Check,"AM01SolInterLockCheck_RingLight_UP : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", 
			ScanShiftMin, nowPosition, ScanShiftMax);
		return true;
	}
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnAM01SolInterLockCheck_RingLight_DN()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}

bool CMotCtrl::m_fnAM01SolInterLockCheck_Review_UP()
{
	//Ring Light cylinder Interlock
	if(m_fnGetAM01State_CySol_RingLight() != SOL_DOWN)
	{
		G_WriteInfo(Log_Check,"Ring Light Cylinder가 Down 상태이어야 합니다.");
		return true;
	}
	//Space Sensor가 대기 위치 이어야 한다.
	int SpaceStandbyZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
	if(m_AxisNowPos[AXIS_AM01_SpaceSnUpDn] > SpaceStandbyZPos)
	{
		G_WriteInfo(Log_Check,"RingLight_UP : Space Up/Down과 같이 동작 할수 없다.");
		return true;
	}
	//위치 Interlock.
	int nowPosition = m_AxisNowPos[AXIS_AM01_ScanShift];
	int ScanShiftMin = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_ReviewSol_Scan_Shift_Min, 311894, Interlock_PARAM_INI_PATH);
	int ScanShiftMax = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_ReviewSol_Scan_Shift_Max, 412756, Interlock_PARAM_INI_PATH);
	if(nowPosition<(ScanShiftMin-AXIS_INPOS_um) || nowPosition>(ScanShiftMax+AXIS_INPOS_um))
	{
		G_WriteInfo(Log_Check,"AM01SolInterLockCheck_Review_UP : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.", 
			ScanShiftMin, nowPosition, ScanShiftMax);
		return true;
	}
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnAM01SolInterLockCheck_Review_DN()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}

bool CMotCtrl::m_fnUT02SolInterLockCheck_TABLE_UP()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}
bool CMotCtrl::m_fnUT02SolInterLockCheck_TABLE_DN()
{
	return false;
	//구동 가능한 상태 이다.(false:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
}

int CMotCtrl::m_fnGetTM01State_CySol_FORK()
{
	int SolStateFront = SOL_NONE;
	int SolStateRear = SOL_NONE;

	if((m_IO_InC.In23_Sn_ForkFrontUp == 1 && m_IO_InC.In24_Sn_ForkFrontDown == 0) )
		SolStateFront = SOL_UP;
	else if((m_IO_InC.In23_Sn_ForkFrontUp == 0 && m_IO_InC.In24_Sn_ForkFrontDown == 1))
		SolStateFront = SOL_DOWN;
	else if((m_IO_InC.In23_Sn_ForkFrontUp == 1 && m_IO_InC.In24_Sn_ForkFrontDown == 1))
		SolStateFront = SOL_ERROR_BOTH_ON;
	else if((m_IO_InC.In23_Sn_ForkFrontUp == 0 && m_IO_InC.In24_Sn_ForkFrontDown == 0))
		SolStateFront = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateFront = SOL_NONE;
	}

	if((m_IO_InC.In25_Sn_ForkRearUp == 1 && m_IO_InC.In26_Sn_ForkRearDown == 0) )
		SolStateRear = SOL_UP;
	else if((m_IO_InC.In25_Sn_ForkRearUp == 0 && m_IO_InC.In26_Sn_ForkRearDown == 1))
		SolStateRear = SOL_DOWN;
	else if((m_IO_InC.In25_Sn_ForkRearUp == 1 && m_IO_InC.In26_Sn_ForkRearDown == 1))
		SolStateRear = SOL_ERROR_BOTH_ON;
	else if((m_IO_InC.In25_Sn_ForkRearUp == 0 && m_IO_InC.In26_Sn_ForkRearDown == 0))
		SolStateRear = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRear = SOL_NONE;
	}
	if(SolStateFront != SolStateRear)
	{
		SolStateFront = SOL_ERROR_SENSOR;
	}
	return SolStateFront;
}
int CMotCtrl::m_fnGetLT01State_CySol_GUIDE()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InC.In11_Sn_LoadLeftAlignForward == 1 && m_IO_InC.In12_Sn_LoadLeftAlignBackward == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InC.In11_Sn_LoadLeftAlignForward == 0 && m_IO_InC.In12_Sn_LoadLeftAlignBackward == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InC.In11_Sn_LoadLeftAlignForward == 1 && m_IO_InC.In12_Sn_LoadLeftAlignBackward == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InC.In11_Sn_LoadLeftAlignForward == 0 && m_IO_InC.In12_Sn_LoadLeftAlignBackward == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InC.In13_Sn_LoadRightAlignForward == 1 && m_IO_InC.In14_Sn_LoadRightAlignBackward == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InC.In13_Sn_LoadRightAlignForward == 0 && m_IO_InC.In14_Sn_LoadRightAlignBackward == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InC.In13_Sn_LoadRightAlignForward == 1 && m_IO_InC.In14_Sn_LoadRightAlignBackward == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InC.In13_Sn_LoadRightAlignForward == 0 && m_IO_InC.In14_Sn_LoadRightAlignBackward == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}
int CMotCtrl::m_fnGetUT01State_CySol_GUIDE()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InC.In17_Sn_UnloadBoxLeftAlignForward == 1 && m_IO_InC.In18_Sn_UnloadBoxLeftAlignBackward == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InC.In17_Sn_UnloadBoxLeftAlignForward == 0 && m_IO_InC.In18_Sn_UnloadBoxLeftAlignBackward == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InC.In17_Sn_UnloadBoxLeftAlignForward == 1 && m_IO_InC.In18_Sn_UnloadBoxLeftAlignBackward == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InC.In17_Sn_UnloadBoxLeftAlignForward == 0 && m_IO_InC.In18_Sn_UnloadBoxLeftAlignBackward == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InC.In19_Sn_UnloadBoxRightAlignForward == 1 && m_IO_InC.In20_Sn_UnloadBoxRightAlignBackward == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InC.In19_Sn_UnloadBoxRightAlignForward == 0 && m_IO_InC.In20_Sn_UnloadBoxRightAlignBackward == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InC.In19_Sn_UnloadBoxRightAlignForward == 1 && m_IO_InC.In20_Sn_UnloadBoxRightAlignBackward == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InC.In19_Sn_UnloadBoxRightAlignForward == 0 && m_IO_InC.In20_Sn_UnloadBoxRightAlignBackward == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}
int CMotCtrl::m_fnGetBO01State_CySol_PICKER()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InC.In00_Sn_BoxLeftCylinderForward == 1 && m_IO_InC.In01_Sn_BoxLeftCylinderBackward == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InC.In00_Sn_BoxLeftCylinderForward == 0 && m_IO_InC.In01_Sn_BoxLeftCylinderBackward == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InC.In00_Sn_BoxLeftCylinderForward == 1 && m_IO_InC.In01_Sn_BoxLeftCylinderBackward == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InC.In00_Sn_BoxLeftCylinderForward == 0 && m_IO_InC.In01_Sn_BoxLeftCylinderBackward == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InC.In04_Sn_BoxRightCylinderForward == 1 && m_IO_InC.In05_Sn_BoxRightCylinderBackward == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InC.In04_Sn_BoxRightCylinderForward == 0 && m_IO_InC.In05_Sn_BoxRightCylinderBackward == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InC.In04_Sn_BoxRightCylinderForward == 1 && m_IO_InC.In05_Sn_BoxRightCylinderBackward == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InC.In04_Sn_BoxRightCylinderForward == 0 && m_IO_InC.In05_Sn_BoxRightCylinderBackward == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}
//int CMotCtrl::m_fnGetBO01State_CySol_RIGHT_PICKER()
//{
//	int SolState = SOL_NONE;
//	if((m_IO_InC.In04_Sn_BoxRightCylinderForward == 1 && m_IO_InC.In05_Sn_BoxRightCylinderBackward == 0) )
//		SolState = SOL_UP;
//	else if((m_IO_InC.In04_Sn_BoxRightCylinderForward == 0 && m_IO_InC.In05_Sn_BoxRightCylinderBackward == 1))
//		SolState = SOL_DOWN;
//	else if((m_IO_InC.In04_Sn_BoxRightCylinderForward == 1 && m_IO_InC.In05_Sn_BoxRightCylinderBackward == 1))
//		SolState = SOL_ERROR_BOTH_ON;
//	else if((m_IO_InC.In04_Sn_BoxRightCylinderForward == 0 && m_IO_InC.In05_Sn_BoxRightCylinderBackward == 0))
//		SolState = SOL_ERROR_BOTH_OFF;
//	else
//	{
//		SolState = SOL_NONE;
//	}
//	return SolState;
//}
int CMotCtrl::m_fnGetTM02State_CySol_LR_PAPER_PICKER()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InF.In00_Sn_PickerPaperLeftUp == 1 && m_IO_InF.In01_Sn_PickerPaperLeftDown == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InF.In00_Sn_PickerPaperLeftUp == 0 && m_IO_InF.In01_Sn_PickerPaperLeftDown == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InF.In00_Sn_PickerPaperLeftUp == 1 && m_IO_InF.In01_Sn_PickerPaperLeftDown == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InF.In00_Sn_PickerPaperLeftUp == 0 && m_IO_InF.In01_Sn_PickerPaperLeftDown == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InF.In02_Sn_PickerPaperRightUp == 1 && m_IO_InF.In03_Sn_PickerPaperRightDown == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InF.In02_Sn_PickerPaperRightUp == 0 && m_IO_InF.In03_Sn_PickerPaperRightDown == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InF.In02_Sn_PickerPaperRightUp == 1 && m_IO_InF.In03_Sn_PickerPaperRightDown == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InF.In02_Sn_PickerPaperRightUp == 0 && m_IO_InF.In03_Sn_PickerPaperRightDown == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}
int CMotCtrl::m_fnGetTM02State_CySol_CNT_PAPER_PICKER()
{
	int SolState = SOL_NONE;
	if((m_IO_InF.In04_Sn_PickerPaperCenterUp == 1 && m_IO_InF.In05_Sn_PickerPaperCenterDown == 0) )
		SolState = SOL_UP;
	else if((m_IO_InF.In04_Sn_PickerPaperCenterUp == 0 && m_IO_InF.In05_Sn_PickerPaperCenterDown == 1))
		SolState = SOL_DOWN;
	else if((m_IO_InF.In04_Sn_PickerPaperCenterUp == 1 && m_IO_InF.In05_Sn_PickerPaperCenterDown == 1))
		SolState = SOL_ERROR_BOTH_ON;
	else if((m_IO_InF.In04_Sn_PickerPaperCenterUp == 0 && m_IO_InF.In05_Sn_PickerPaperCenterDown == 0))
		SolState = SOL_ERROR_BOTH_OFF;
	else
	{
		SolState = SOL_NONE;
	}
	return SolState;


}
int CMotCtrl::m_fnGetAM01State_CySol_TEN_GRIPPER1()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InD.In00_Sn_LeftOpenGripper1 == 1 && m_IO_InD.In01_Sn_LeftCloseGripper1 == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InD.In00_Sn_LeftOpenGripper1 == 0 && m_IO_InD.In01_Sn_LeftCloseGripper1 == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InD.In00_Sn_LeftOpenGripper1 == 1 && m_IO_InD.In01_Sn_LeftCloseGripper1 == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In00_Sn_LeftOpenGripper1 == 0 && m_IO_InD.In01_Sn_LeftCloseGripper1 == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InD.In12_Sn_RightOpenGripper1 == 1 && m_IO_InD.In13_Sn_RightCloseGripper1 == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InD.In12_Sn_RightOpenGripper1 == 0 && m_IO_InD.In13_Sn_RightCloseGripper1 == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InD.In12_Sn_RightOpenGripper1 == 1 && m_IO_InD.In13_Sn_RightCloseGripper1 == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In12_Sn_RightOpenGripper1 == 0 && m_IO_InD.In13_Sn_RightCloseGripper1 == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}
int CMotCtrl::m_fnGetAM01State_CySol_TEN_GRIPPER2()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InD.In02_Sn_LeftOpenGripper2 == 1 && m_IO_InD.In03_Sn_LeftCloseGripper2 == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InD.In02_Sn_LeftOpenGripper2 == 0 && m_IO_InD.In03_Sn_LeftCloseGripper2 == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InD.In02_Sn_LeftOpenGripper2 == 1 && m_IO_InD.In03_Sn_LeftCloseGripper2 == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In02_Sn_LeftOpenGripper2 == 0 && m_IO_InD.In03_Sn_LeftCloseGripper2 == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InD.In14_Sn_RightOpenGripper2 == 1 && m_IO_InD.In15_Sn_RightCloseGripper2 == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InD.In14_Sn_RightOpenGripper2 == 0 && m_IO_InD.In15_Sn_RightCloseGripper2 == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InD.In14_Sn_RightOpenGripper2 == 1 && m_IO_InD.In15_Sn_RightCloseGripper2 == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In14_Sn_RightOpenGripper2 == 0 && m_IO_InD.In15_Sn_RightCloseGripper2 == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;

}
int CMotCtrl::m_fnGetAM01State_CySol_TEN_GRIPPER3()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InD.In04_Sn_LeftOpenGripper3 == 1 && m_IO_InD.In05_Sn_LeftCloseGripper3 == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InD.In04_Sn_LeftOpenGripper3 == 0 && m_IO_InD.In05_Sn_LeftCloseGripper3 == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InD.In04_Sn_LeftOpenGripper3 == 1 && m_IO_InD.In05_Sn_LeftCloseGripper3 == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In04_Sn_LeftOpenGripper3 == 0 && m_IO_InD.In05_Sn_LeftCloseGripper3 == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InD.In16_Sn_RightOpenGripper3 == 1 && m_IO_InD.In17_Sn_RightCloseGripper3 == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InD.In16_Sn_RightOpenGripper3 == 0 && m_IO_InD.In17_Sn_RightCloseGripper3 == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InD.In16_Sn_RightOpenGripper3 == 1 && m_IO_InD.In17_Sn_RightCloseGripper3 == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In16_Sn_RightOpenGripper3 == 0 && m_IO_InD.In17_Sn_RightCloseGripper3 == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;

}
int CMotCtrl::m_fnGetAM01State_CySol_TEN_GRIPPER4()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InD.In06_Sn_LeftOpenGripper4 == 1 && m_IO_InD.In07_Sn_LeftCloseGripper4 == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InD.In06_Sn_LeftOpenGripper4 == 0 && m_IO_InD.In07_Sn_LeftCloseGripper4 == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InD.In06_Sn_LeftOpenGripper4 == 1 && m_IO_InD.In07_Sn_LeftCloseGripper4 == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In06_Sn_LeftOpenGripper4 == 0 && m_IO_InD.In07_Sn_LeftCloseGripper4 == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InD.In18_Sn_RightOpenGripper4 == 1 && m_IO_InD.In19_Sn_RightCloseGripper4 == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InD.In18_Sn_RightOpenGripper4 == 0 && m_IO_InD.In19_Sn_RightCloseGripper4 == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InD.In18_Sn_RightOpenGripper4 == 1 && m_IO_InD.In19_Sn_RightCloseGripper4 == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In18_Sn_RightOpenGripper4 == 0 && m_IO_InD.In19_Sn_RightCloseGripper4 == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}

int CMotCtrl::m_fnGetAM01State_CySol_TENTION()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InD.In08_Sn_LeftTensionCylinderForward == 1 && m_IO_InD.In09_Sn_LeftTensionCylinderBackward == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InD.In08_Sn_LeftTensionCylinderForward == 0 && m_IO_InD.In09_Sn_LeftTensionCylinderBackward == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InD.In08_Sn_LeftTensionCylinderForward == 1 && m_IO_InD.In09_Sn_LeftTensionCylinderBackward == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In08_Sn_LeftTensionCylinderForward == 0 && m_IO_InD.In09_Sn_LeftTensionCylinderBackward == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InD.In20_Sn_RightTensionCylinderForward == 1 && m_IO_InD.In21_Sn_RightTensionCylinderBackward == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InD.In20_Sn_RightTensionCylinderForward == 0 && m_IO_InD.In21_Sn_RightTensionCylinderBackward == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InD.In20_Sn_RightTensionCylinderForward == 1 && m_IO_InD.In21_Sn_RightTensionCylinderBackward == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InD.In20_Sn_RightTensionCylinderForward == 0 && m_IO_InD.In21_Sn_RightTensionCylinderBackward == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}
int CMotCtrl::m_fnGetAM01State_DoonTuk_Laser_OnOff()
{
	if (m_IO_OutA.Out31_Space_Sensor_LaserOnOff==1) return 1;
	else return 0;
}
int CMotCtrl::m_fnGetAM01State_CySol_RingLight()
{
	int SolState = SOL_NONE;
	if((m_IO_InI.In11_Sn_RingLightCylinderUp == 1 && m_IO_InI.In12_Sn_RingLightCylinderDown == 0) )
		SolState = SOL_UP;
	else if((m_IO_InI.In11_Sn_RingLightCylinderUp == 0 && m_IO_InI.In12_Sn_RingLightCylinderDown == 1))
		SolState = SOL_DOWN;
	else if((m_IO_InI.In11_Sn_RingLightCylinderUp == 1 && m_IO_InI.In12_Sn_RingLightCylinderDown == 1))
		SolState = SOL_ERROR_BOTH_ON;
	else if((m_IO_InI.In11_Sn_RingLightCylinderUp == 0 && m_IO_InI.In12_Sn_RingLightCylinderDown == 0))
		SolState = SOL_ERROR_BOTH_OFF;
	else
	{
		SolState = SOL_NONE;
	}
	return SolState;
}
int CMotCtrl::m_fnGetAM01State_CySol_Review()
{
	int SolState = SOL_NONE;
	if((m_IO_InI.In13_Sn_ReviewCylinderUp == 1 && m_IO_InI.In14_Sn_ReviewCylinderDown == 0) )
		SolState = SOL_UP;
	else if((m_IO_InI.In13_Sn_ReviewCylinderUp == 0 && m_IO_InI.In14_Sn_ReviewCylinderDown == 1))
		SolState = SOL_DOWN;
	else if((m_IO_InI.In13_Sn_ReviewCylinderUp == 1 && m_IO_InI.In14_Sn_ReviewCylinderDown == 1))
		SolState = SOL_ERROR_BOTH_ON;
	else if((m_IO_InI.In13_Sn_ReviewCylinderUp == 0 && m_IO_InI.In14_Sn_ReviewCylinderDown == 0))
		SolState = SOL_ERROR_BOTH_OFF;
	else
	{
		SolState = SOL_NONE;
	}
	return SolState;
}
int CMotCtrl::m_fnGetUT02State_CySol_TABLE()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((m_IO_InI.In02_Sn_OutputLTableCylinderUp == 1 && m_IO_InI.In03_Sn_OutputLTableCylinderDown == 0) )
		SolStateLeft = SOL_UP;
	else if((m_IO_InI.In02_Sn_OutputLTableCylinderUp == 0 && m_IO_InI.In03_Sn_OutputLTableCylinderDown == 1))
		SolStateLeft = SOL_DOWN;
	else if((m_IO_InI.In02_Sn_OutputLTableCylinderUp == 1 && m_IO_InI.In03_Sn_OutputLTableCylinderDown == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((m_IO_InI.In02_Sn_OutputLTableCylinderUp == 0 && m_IO_InI.In03_Sn_OutputLTableCylinderDown == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((m_IO_InI.In05_Sn_OutputRTableCylinderUp == 1 && m_IO_InI.In06_Sn_OutputRTableCylinderDown == 0) )
		SolStateRight = SOL_UP;
	else if((m_IO_InI.In05_Sn_OutputRTableCylinderUp == 0 && m_IO_InI.In06_Sn_OutputRTableCylinderDown == 1))
		SolStateRight = SOL_DOWN;
	else if((m_IO_InI.In05_Sn_OutputRTableCylinderUp == 1 && m_IO_InI.In06_Sn_OutputRTableCylinderDown == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((m_IO_InI.In05_Sn_OutputRTableCylinderUp == 0 && m_IO_InI.In06_Sn_OutputRTableCylinderDown == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}

bool CMotCtrl::m_fnGetDoorReleaseStateFront()
{
	return (m_IO_OutA.Out00_IndexerFrontDoorRelease ==1)?true:false;
}
bool CMotCtrl::m_fnGetDoorReleaseStateSide()
{
	return (m_IO_OutA.Out01_SideDoorAllRelease ==1)?true:false;
}
bool CMotCtrl::m_fnGetDoorReleaseStateBack()
{
	return (m_IO_OutA.Out02_InspectorBackDoorRelease ==1)?true:false;
}

void CMotCtrl::m_fnSetDoorReleaseFront(bool OnOff)
{
	m_IO_OutA.Out00_IndexerFrontDoorRelease = (OnOff == true)?1:0;
}
void CMotCtrl::m_fnSetDoorReleaseSide(bool OnOff)
{
	m_IO_OutA.Out01_SideDoorAllRelease = (OnOff == true)?1:0;
}
void CMotCtrl::m_fnSetDoorReleaseBack(bool OnOff)
{
	m_IO_OutA.Out02_InspectorBackDoorRelease = (OnOff == true)?1:0;
}

