#pragma once

#include "MotInitSeq.h"
#include "afxwin.h"
#include "afxcmn.h"
// CInitSyDlg 대화 상자입니다.

class CInitSyDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CInitSyDlg)

public:
	CInitSyDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CInitSyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SYS_INIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	int					m_NowStep;
	bool				m_bInitEnd;

	CMotInitSeq	m_SeqSysInitObj;
	static int SeqFlowStep(void *pObj, int NowFlowStep);
public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	
	CProgressCtrl m_pgInistStep;
	afx_msg void OnBnClickedBtStartInit();

	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btSysInit;
	CRoundButton2 m_btStopInit;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	
	afx_msg void OnBnClickedBtStopInit();
};
