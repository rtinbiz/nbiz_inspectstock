// CtrlSolPanel.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS24Motion.h"
#include "CtrlSolPanel.h"
#include "afxdialogex.h"


// CCtrlSolPanel 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCtrlSolPanel, CDialogEx)

CCtrlSolPanel::CCtrlSolPanel(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCtrlSolPanel::IDD, pParent)
{

}

CCtrlSolPanel::~CCtrlSolPanel()
{
}

void CCtrlSolPanel::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BT_CHANGE_ACC_VIEW, m_btChangeACC);

	DDX_Control(pDX, IDC_BT_TM01_FORK_UP, m_btCylinderAction[0]);
	DDX_Control(pDX, IDC_BT_TM01_FORK_DN, m_btCylinderAction[1]);

	DDX_Control(pDX, IDC_BT_LT01_GUIDE_FWD, m_btCylinderAction[2]);
	DDX_Control(pDX, IDC_BT_LT01_GUIDE_BWD, m_btCylinderAction[3]);

	DDX_Control(pDX, IDC_BT_UT01_GUIDE_FWD, m_btCylinderAction[4]);
	DDX_Control(pDX, IDC_BT_UT01_GUIDE_BWD, m_btCylinderAction[5]);

	DDX_Control(pDX, IDC_BT_UT02_TABLE_UP, m_btCylinderAction[6]);
	DDX_Control(pDX, IDC_BT_UT02_TABLE_DN, m_btCylinderAction[7]);

	DDX_Control(pDX, IDC_BT_BO01_PICKER_FWD, m_btCylinderAction[8]);
	DDX_Control(pDX, IDC_BT_BO01_PICKER_BWD, m_btCylinderAction[9]);

	DDX_Control(pDX, IDC_BT_AM01_OPEN_TEN_GRIPPER1, m_btCylinderAction[10]);
	DDX_Control(pDX, IDC_BT_AM01_OPEN_TEN_GRIPPER2, m_btCylinderAction[11]);
	DDX_Control(pDX, IDC_BT_AM01_OPEN_TEN_GRIPPER3, m_btCylinderAction[12]);
	DDX_Control(pDX, IDC_BT_AM01_OPEN_TEN_GRIPPER4, m_btCylinderAction[13]);

	DDX_Control(pDX, IDC_BT_AM01_CLOSE_TEN_GRIPPER1, m_btCylinderAction[14]);
	DDX_Control(pDX, IDC_BT_AM01_CLOSE_TEN_GRIPPER2, m_btCylinderAction[15]);
	DDX_Control(pDX, IDC_BT_AM01_CLOSE_TEN_GRIPPER3, m_btCylinderAction[16]);
	DDX_Control(pDX, IDC_BT_AM01_CLOSE_TEN_GRIPPER4, m_btCylinderAction[17]);

	DDX_Control(pDX, IDC_BT_AM01_GRIPPER_OPEN, m_btCylinderAction[18]);
	DDX_Control(pDX, IDC_BT_AM01_GRIPPER_CLOSE, m_btCylinderAction[19]);

	DDX_Control(pDX, IDC_BT_AM01_TENTION_FWD, m_btCylinderAction[20]);
	DDX_Control(pDX, IDC_BT_AM01_TENTION_BWD, m_btCylinderAction[21]);

	DDX_Control(pDX, IDC_BT_TM02_LEFT_RIGHT_PAPER_PICKER_UP, m_btCylinderAction[22]);
	DDX_Control(pDX, IDC_BT_TM02_LEFT_RIGHT_PAPER_PICKER_DN, m_btCylinderAction[23]);

	DDX_Control(pDX, IDC_BT_TM02_CENTER_PAPER_PICKER_UP, m_btCylinderAction[24]);
	DDX_Control(pDX, IDC_BT_TM02_CENTER_PAPER_PICKER_DN, m_btCylinderAction[25]);

	DDX_Control(pDX, IDC_BT_AM01_RINGLIGHT_UP, m_btCylinderAction[26]);
	DDX_Control(pDX, IDC_BT_AM01_RINGLIGHT_DN, m_btCylinderAction[27]);
	DDX_Control(pDX, IDC_BT_AM01_REVIEW_UP, m_btCylinderAction[28]);
	DDX_Control(pDX, IDC_BT_AM01_REVIEW_DN, m_btCylinderAction[29]);

	DDX_Control(pDX, IDC_BT_DOOR_FRONT, m_btUnlockFrontDoor);
	DDX_Control(pDX, IDC_BT_DOOR_EQ_SIDE, m_btUnlockSideDoor);
	DDX_Control(pDX, IDC_BT_DOOR_BACK, m_btUnlockBackDoor);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_FRONT_LEFT,				m_stOpenCloseDoor[0]);
	DDX_Control(pDX, IDC_ST_FRONT_LEFT_LOCK,						m_stLockCheckDoor[0]);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_FRONT_RIGHT,			m_stOpenCloseDoor[1]);
	DDX_Control(pDX, IDC_ST_FRONT_RIGHT_LOCK,					m_stLockCheckDoor[1]);

	DDX_Control(pDX, IDC_ST_OPENCLOSE_INDEX_SID_LEFT,		m_stOpenCloseDoor[2]);
	DDX_Control(pDX, IDC_ST_SIDE_LEFT_INDEX_LOCK,				m_stLockCheckDoor[2]);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_INDEX_SID_RIGHT,		m_stOpenCloseDoor[3]);
	DDX_Control(pDX, IDC_ST_SIDE_RIGHT_INDEX_LOCK,				m_stLockCheckDoor[3]);

	DDX_Control(pDX, IDC_ST_OPENCLOSE_INSPECT_SID_LEFT,	m_stOpenCloseDoor[4]);
	DDX_Control(pDX, IDC_ST_SIDE_LEFT_INSPECT_LOCK,			m_stLockCheckDoor[4]);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_INSPECT_SID_RIGHT,	m_stOpenCloseDoor[5]);
	DDX_Control(pDX, IDC_ST_SIDE_RIGHT_INSPECT_LOCK,			m_stLockCheckDoor[5]);

	DDX_Control(pDX, IDC_ST_OPENCLOSE_BACK_LEFT,				m_stOpenCloseDoor[6]);
	DDX_Control(pDX, IDC_ST_BACK_LEFT_LOCK,						m_stLockCheckDoor[6]);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_BACK_RIGHT,				m_stOpenCloseDoor[7]);
	DDX_Control(pDX, IDC_ST_BACK_RIGHT_LOCK,						m_stLockCheckDoor[7]);

}


BEGIN_MESSAGE_MAP(CCtrlSolPanel, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BT_CHANGE_ACC_VIEW, &CCtrlSolPanel::OnBnClickedBtChangeAccView)

	ON_BN_CLICKED(IDC_BT_TM01_FORK_UP, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_TM01_FORK_DN, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_LT01_GUIDE_FWD, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_LT01_GUIDE_BWD, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_UT01_GUIDE_FWD, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_UT01_GUIDE_BWD, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_UT02_TABLE_UP, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_UT02_TABLE_DN, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_BO01_PICKER_FWD, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_BO01_PICKER_BWD, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_AM01_OPEN_TEN_GRIPPER1, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_OPEN_TEN_GRIPPER2, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_OPEN_TEN_GRIPPER3, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_OPEN_TEN_GRIPPER4, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_AM01_CLOSE_TEN_GRIPPER1, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_CLOSE_TEN_GRIPPER2, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_CLOSE_TEN_GRIPPER3, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_CLOSE_TEN_GRIPPER4, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_AM01_GRIPPER_OPEN, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_GRIPPER_CLOSE, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_AM01_TENTION_FWD, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_TENTION_BWD, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_TM02_LEFT_RIGHT_PAPER_PICKER_UP, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_TM02_LEFT_RIGHT_PAPER_PICKER_DN, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_BN_CLICKED(IDC_BT_TM02_CENTER_PAPER_PICKER_UP, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_TM02_CENTER_PAPER_PICKER_DN, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)


	ON_BN_CLICKED(IDC_BT_AM01_RINGLIGHT_UP, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_RINGLIGHT_DN, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_REVIEW_UP, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)
	ON_BN_CLICKED(IDC_BT_AM01_REVIEW_DN, &CCtrlSolPanel::OnBnClickedBtCylinenderAction)

	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_DOOR_FRONT, &CCtrlSolPanel::OnBnClickedBtDoorFront)
	ON_BN_CLICKED(IDC_BT_DOOR_EQ_SIDE, &CCtrlSolPanel::OnBnClickedBtDoorEqSide)
	ON_BN_CLICKED(IDC_BT_DOOR_BACK, &CCtrlSolPanel::OnBnClickedBtDoorBack)

END_MESSAGE_MAP()


// CCtrlSolPanel 메시지 처리기입니다.


BOOL CCtrlSolPanel::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();

	SetTimer(TIMER_CYLINDER_STATE_UPDATE, 250, 0);//초당 4번정도?
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CCtrlSolPanel::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btChangeACC.GetTextColor(&tColor);
		m_btChangeACC.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}
		m_btChangeACC.SetRoundButtonStyle(&m_tButtonStyle);
		m_btChangeACC.SetTextColor(&tColor);
		m_btChangeACC.SetCheckButton(false);
		m_btChangeACC.SetFont(&tFont);

		for(int BStep = 0; BStep<26+4; BStep++)
		{
			m_btCylinderAction[BStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btCylinderAction[BStep].SetTextColor(&tColor);
			m_btCylinderAction[BStep].SetCheckButton(true, true);
			m_btCylinderAction[BStep].SetFont(&tFont);
		}
		m_btUnlockFrontDoor.SetRoundButtonStyle(&m_tButtonStyle);
		m_btUnlockFrontDoor.SetTextColor(&tColor);
		m_btUnlockFrontDoor.SetCheckButton(true, true);
		m_btUnlockFrontDoor.SetFont(&tFont);

		m_btUnlockSideDoor.SetRoundButtonStyle(&m_tButtonStyle);
		m_btUnlockSideDoor.SetTextColor(&tColor);
		m_btUnlockSideDoor.SetCheckButton(true, true);
		m_btUnlockSideDoor.SetFont(&tFont);

		m_btUnlockBackDoor.SetRoundButtonStyle(&m_tButtonStyle);
		m_btUnlockBackDoor.SetTextColor(&tColor);
		m_btUnlockBackDoor.SetCheckButton(true, true);
		m_btUnlockBackDoor.SetFont(&tFont);


		//extern COLORREF G_Color_Lock;
		//extern COLORREF G_Color_Unlock;
		//extern COLORREF G_Color_Open;
		//extern COLORREF G_Color_Close;

		for(int StStep =0; StStep<8; StStep++)
		{
			m_stOpenCloseDoor[StStep].SetFont(&G_PLCStateFont);
			m_stOpenCloseDoor[StStep].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[StStep].SetBkColor(G_Color_WinBack);
		}

		for(int StStep =0; StStep<8; StStep++)
		{
			m_stLockCheckDoor[StStep].SetFont(&G_PLCStateFont);
			m_stLockCheckDoor[StStep].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[StStep].SetBkColor(G_Color_WinBack);
		}

	}
}

void CCtrlSolPanel::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CDialogEx::PostNcDestroy();
	delete this;
}


BOOL CCtrlSolPanel::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


HBRUSH CCtrlSolPanel::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(DLG_ID_Number == IDC_TREE_PORT_LIST ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_FRONT_LEFT ||
		DLG_ID_Number == IDC_ST_FRONT_LEFT_LOCK ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_FRONT_RIGHT ||
		DLG_ID_Number == IDC_ST_FRONT_RIGHT_LOCK ||

		DLG_ID_Number == IDC_ST_OPENCLOSE_INDEX_SID_LEFT ||
		DLG_ID_Number == IDC_ST_SIDE_LEFT_INDEX_LOCK ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_INDEX_SID_RIGHT ||
		DLG_ID_Number == IDC_ST_SIDE_RIGHT_INDEX_LOCK ||

		DLG_ID_Number == IDC_ST_OPENCLOSE_INSPECT_SID_LEFT ||
		DLG_ID_Number == IDC_ST_SIDE_LEFT_INSPECT_LOCK ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_INSPECT_SID_RIGHT ||
		DLG_ID_Number == IDC_ST_SIDE_RIGHT_INSPECT_LOCK ||

		DLG_ID_Number == IDC_ST_OPENCLOSE_BACK_LEFT ||
		DLG_ID_Number == IDC_ST_BACK_LEFT_LOCK ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_BACK_RIGHT ||
		DLG_ID_Number == IDC_ST_BACK_RIGHT_LOCK)
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


#include "VS24MotionDlg.h"
void CCtrlSolPanel::OnBnClickedBtChangeAccView()
{
	CVS24MotionDlg* pMainWnd = (CVS24MotionDlg*)AfxGetApp()->m_pMainWnd;
	pMainWnd->ChangeView_ACC_SOL(1);
}



void CCtrlSolPanel::OnBnClickedBtCylinenderAction()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();

	int SelectCMD = 0;
	int SelectPoint = 0;//Gripper에서 쓴다.

	switch(wID)
	{
	case IDC_BT_TM01_FORK_UP:
		{
			SelectCMD = nBiz_Seq_TM01_FORK_UP;
		}break;
	case IDC_BT_TM01_FORK_DN:
		{
			SelectCMD = nBiz_Seq_TM01_FORK_DN;
		}break;

	case IDC_BT_LT01_GUIDE_FWD:
		{
			SelectCMD = nBiz_Seq_LT01_GUIDE_FWD;
		}break;
	case IDC_BT_LT01_GUIDE_BWD:
		{
			SelectCMD = nBiz_Seq_LT01_GUIDE_BWD;
		}break;

	case IDC_BT_UT01_GUIDE_FWD:
		{
			SelectCMD = nBiz_Seq_UT01_GUIDE_FWD;
		}break;
	case IDC_BT_UT01_GUIDE_BWD:
		{
			SelectCMD = nBiz_Seq_UT01_GUIDE_BWD;
		}break;

	case IDC_BT_UT02_TABLE_UP:
		{
			SelectCMD = nBiz_Seq_UT02_TABLE_UP;
		}break;
	case IDC_BT_UT02_TABLE_DN:
		{
			SelectCMD = nBiz_Seq_UT02_TABLE_DN;
		}break;

	case IDC_BT_BO01_PICKER_FWD:
		{
			SelectCMD = nBiz_Seq_BO01_PICKER_FWD;
		}break;
	case IDC_BT_BO01_PICKER_BWD:
		{
			SelectCMD = nBiz_Seq_BO01_PICKER_BWD;
		}break;

	case IDC_BT_AM01_OPEN_TEN_GRIPPER1:
		{
			SelectPoint = 0x01;
			SelectCMD = nBiz_Seq_AM01_OPEN_TEN_GRIPPER;
		}break;
	case IDC_BT_AM01_OPEN_TEN_GRIPPER2:
		{
			SelectPoint = 0x02;
			SelectCMD = nBiz_Seq_AM01_OPEN_TEN_GRIPPER;
		}break;
	case IDC_BT_AM01_OPEN_TEN_GRIPPER3:
		{
			SelectPoint = 0x04;
			SelectCMD = nBiz_Seq_AM01_OPEN_TEN_GRIPPER;
		}break;
	case IDC_BT_AM01_OPEN_TEN_GRIPPER4:
		{
			SelectPoint = 0x08;
			SelectCMD = nBiz_Seq_AM01_OPEN_TEN_GRIPPER;
		}break;

	case IDC_BT_AM01_CLOSE_TEN_GRIPPER1:
		{
			SelectPoint = 0x01;
			SelectCMD = nBiz_Seq_AM01_CLOSE_TEN_GRIPPER;
		}break;
	case IDC_BT_AM01_CLOSE_TEN_GRIPPER2:
		{
			SelectPoint = 0x02;
			SelectCMD = nBiz_Seq_AM01_CLOSE_TEN_GRIPPER;
		}break;
	case IDC_BT_AM01_CLOSE_TEN_GRIPPER3:
		{
			SelectPoint = 0x04;
			SelectCMD = nBiz_Seq_AM01_CLOSE_TEN_GRIPPER;
		}break;
	case IDC_BT_AM01_CLOSE_TEN_GRIPPER4:
		{
			SelectPoint = 0x08;
			SelectCMD = nBiz_Seq_AM01_CLOSE_TEN_GRIPPER;
		}break;

	case IDC_BT_AM01_GRIPPER_OPEN:
		{
			SelectPoint = 0;
			if(IsDlgButtonChecked(IDC_CH_AM01_GRIP_1) != 0)
				SelectPoint |= 0x01;
			if(IsDlgButtonChecked(IDC_CH_AM01_GRIP_2) != 0)
				SelectPoint |= 0x02;
			if(IsDlgButtonChecked(IDC_CH_AM01_GRIP_3) != 0)
				SelectPoint |= 0x04;
			if(IsDlgButtonChecked(IDC_CH_AM01_GRIP_4) != 0)
				SelectPoint |= 0x08;


			SelectCMD = nBiz_Seq_AM01_OPEN_TEN_GRIPPER;
		}break;
	case IDC_BT_AM01_GRIPPER_CLOSE:
		{
			SelectPoint = 0;
			if(IsDlgButtonChecked(IDC_CH_AM01_GRIP_1) != 0)
				SelectPoint |= 0x01;
			if(IsDlgButtonChecked(IDC_CH_AM01_GRIP_2) != 0)
				SelectPoint |= 0x02;
			if(IsDlgButtonChecked(IDC_CH_AM01_GRIP_3) != 0)
				SelectPoint |= 0x04;
			if(IsDlgButtonChecked(IDC_CH_AM01_GRIP_4) != 0)
				SelectPoint |= 0x08;

			SelectCMD = nBiz_Seq_AM01_CLOSE_TEN_GRIPPER;
		}break;

	case IDC_BT_AM01_TENTION_FWD:
		{
			SelectCMD = nBiz_Seq_AM01_TENTION_FWD;
		}break;
	case IDC_BT_AM01_TENTION_BWD:
		{
			SelectCMD = nBiz_Seq_AM01_TENTION_BWD;
		}break;

	case IDC_BT_TM02_LEFT_RIGHT_PAPER_PICKER_UP:
		{
			SelectCMD = nBiz_Seq_TM02_LR_PAPER_PICKER_UP;
		}break;
	case IDC_BT_TM02_LEFT_RIGHT_PAPER_PICKER_DN:
		{
			SelectCMD = nBiz_Seq_TM02_LR_PAPER_PICKER_DN;
		}break;

	case IDC_BT_TM02_CENTER_PAPER_PICKER_UP:
		{
			SelectCMD = nBiz_Seq_TM02_CNT_PAPER_PICKER_UP;
		}break;
	case IDC_BT_TM02_CENTER_PAPER_PICKER_DN:
		{
			SelectCMD = nBiz_Seq_TM02_CNT_PAPER_PICKER_DN;
		}break;


	case IDC_BT_AM01_RINGLIGHT_UP:
		{
			SelectCMD = nBiz_Seq_AM01_RingLight_UP;
		}break;
	case IDC_BT_AM01_RINGLIGHT_DN:
		{
			SelectCMD = nBiz_Seq_AM01_RingLight_DN;
		}break;
	case IDC_BT_AM01_REVIEW_UP:
		{
			SelectCMD = nBiz_Seq_AM01_Review_UP;
		}break;
	case IDC_BT_AM01_REVIEW_DN:
		{
			SelectCMD = nBiz_Seq_AM01_Review_DN;
		}break;
	default:
		break;
	}

	if(SelectCMD>0)
		::SendMessage(this->GetParent()->m_hWnd, WM_USER_CYLINDER_INTERNAL_CMD, (WPARAM)SelectCMD, (LPARAM)SelectPoint );

}


void CCtrlSolPanel::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(TIMER_CYLINDER_STATE_UPDATE == nIDEvent)
	{
		//Cylinder Sol 상태
		int SolState = G_MotCtrlObj.m_fnGetTM01State_CySol_FORK();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[0].GetCheck() == false)
			{
				m_btCylinderAction[0].SetCheck(true);
				m_btCylinderAction[1].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[1].GetCheck() == false)
			{
				m_btCylinderAction[0].SetCheck(false);
				m_btCylinderAction[1].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[0].GetCheck() != false)
				m_btCylinderAction[0].SetCheck(false);
			if(m_btCylinderAction[1].GetCheck() != false)
				m_btCylinderAction[1].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetLT01State_CySol_GUIDE();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[2].GetCheck() == false)
			{
				m_btCylinderAction[2].SetCheck(true);
				m_btCylinderAction[3].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[3].GetCheck() == false)
			{
				m_btCylinderAction[2].SetCheck(false);
				m_btCylinderAction[3].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[2].GetCheck() != false)
				m_btCylinderAction[2].SetCheck(false);
			if(m_btCylinderAction[3].GetCheck() != false)
				m_btCylinderAction[3].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetUT01State_CySol_GUIDE();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[4].GetCheck() == false)
			{
				m_btCylinderAction[4].SetCheck(true);
				m_btCylinderAction[5].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[5].GetCheck() == false)
			{
				m_btCylinderAction[4].SetCheck(false);
				m_btCylinderAction[5].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[4].GetCheck() != false)
				m_btCylinderAction[4].SetCheck(false);
			if(m_btCylinderAction[5].GetCheck() != false)
				m_btCylinderAction[5].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetUT02State_CySol_TABLE();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[6].GetCheck() == false)
			{
				m_btCylinderAction[6].SetCheck(true);
				m_btCylinderAction[7].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[7].GetCheck() == false)
			{
				m_btCylinderAction[6].SetCheck(false);
				m_btCylinderAction[7].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[6].GetCheck() != false)
				m_btCylinderAction[6].SetCheck(false);
			if(m_btCylinderAction[7].GetCheck() != false)
				m_btCylinderAction[7].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetBO01State_CySol_PICKER();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[8].GetCheck() == false)
			{
				m_btCylinderAction[8].SetCheck(true);
				m_btCylinderAction[9].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[9].GetCheck() == false)
			{
				m_btCylinderAction[8].SetCheck(false);
				m_btCylinderAction[9].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[8].GetCheck() != false)
				m_btCylinderAction[8].SetCheck(false);
			if(m_btCylinderAction[9].GetCheck() != false)
				m_btCylinderAction[9].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER1();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[10].GetCheck() == false)
			{
				m_btCylinderAction[10].SetCheck(true);
				m_btCylinderAction[14].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[14].GetCheck() == false)
			{
				m_btCylinderAction[10].SetCheck(false);
				m_btCylinderAction[14].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[10].GetCheck() != false)
				m_btCylinderAction[10].SetCheck(false);
			if(m_btCylinderAction[14].GetCheck() != false)
				m_btCylinderAction[14].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER2();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[11].GetCheck() == false)
			{
				m_btCylinderAction[11].SetCheck(true);
				m_btCylinderAction[15].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[15].GetCheck() == false)
			{
				m_btCylinderAction[11].SetCheck(false);
				m_btCylinderAction[15].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[11].GetCheck() != false)
				m_btCylinderAction[11].SetCheck(false);
			if(m_btCylinderAction[15].GetCheck() != false)
				m_btCylinderAction[15].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER3();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[12].GetCheck() == false)
			{
				m_btCylinderAction[12].SetCheck(true);
				m_btCylinderAction[16].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[16].GetCheck() == false)
			{
				m_btCylinderAction[12].SetCheck(false);
				m_btCylinderAction[16].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[12].GetCheck() != false)
				m_btCylinderAction[12].SetCheck(false);
			if(m_btCylinderAction[16].GetCheck() != false)
				m_btCylinderAction[16].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER4();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[13].GetCheck() == false)
			{
				m_btCylinderAction[13].SetCheck(true);
				m_btCylinderAction[17].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[17].GetCheck() == false)
			{
				m_btCylinderAction[13].SetCheck(false);
				m_btCylinderAction[17].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[13].GetCheck() != false)
				m_btCylinderAction[13].SetCheck(false);
			if(m_btCylinderAction[17].GetCheck() != false)
				m_btCylinderAction[17].SetCheck(false);
		}

		//if(m_btCylinderAction[18].GetCheck() != false)
		//	m_btCylinderAction[18].SetCheck(false);
		//if(m_btCylinderAction[19].GetCheck() != false)
		//	m_btCylinderAction[19].SetCheck(false);

		SolState = G_MotCtrlObj.m_fnGetAM01State_CySol_TENTION();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[20].GetCheck() == false)
			{
				m_btCylinderAction[20].SetCheck(true);
				m_btCylinderAction[21].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[21].GetCheck() == false)
			{
				m_btCylinderAction[20].SetCheck(false);
				m_btCylinderAction[21].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[20].GetCheck() != false)
				m_btCylinderAction[20].SetCheck(false);
			if(m_btCylinderAction[21].GetCheck() != false)
				m_btCylinderAction[21].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetTM02State_CySol_LR_PAPER_PICKER();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[22].GetCheck() == false)
			{
				m_btCylinderAction[22].SetCheck(true);
				m_btCylinderAction[23].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[23].GetCheck() == false)
			{
				m_btCylinderAction[22].SetCheck(false);
				m_btCylinderAction[23].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[22].GetCheck() != false)
				m_btCylinderAction[22].SetCheck(false);
			if(m_btCylinderAction[23].GetCheck() != false)
				m_btCylinderAction[23].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetTM02State_CySol_CNT_PAPER_PICKER();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[24].GetCheck() == false)
			{
				m_btCylinderAction[24].SetCheck(true);
				m_btCylinderAction[25].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[25].GetCheck() == false)
			{
				m_btCylinderAction[24].SetCheck(false);
				m_btCylinderAction[25].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[24].GetCheck() != false)
				m_btCylinderAction[24].SetCheck(false);
			if(m_btCylinderAction[25].GetCheck() != false)
				m_btCylinderAction[25].SetCheck(false);
		}

		SolState = G_MotCtrlObj.m_fnGetAM01State_CySol_RingLight();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[26].GetCheck() == false)
			{
				m_btCylinderAction[26].SetCheck(true);
				m_btCylinderAction[27].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[27].GetCheck() == false)
			{
				m_btCylinderAction[26].SetCheck(false);
				m_btCylinderAction[27].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[26].GetCheck() != false)
				m_btCylinderAction[26].SetCheck(false);
			if(m_btCylinderAction[27].GetCheck() != false)
				m_btCylinderAction[27].SetCheck(false);
		}
		SolState = G_MotCtrlObj.m_fnGetAM01State_CySol_Review();
		if(SolState == SOL_UP)
		{
			if(m_btCylinderAction[28].GetCheck() == false)
			{
				m_btCylinderAction[28].SetCheck(true);
				m_btCylinderAction[29].SetCheck(false);
			}
		}
		else if(SolState == SOL_DOWN)
		{
			if(m_btCylinderAction[29].GetCheck() == false)
			{
				m_btCylinderAction[28].SetCheck(false);
				m_btCylinderAction[29].SetCheck(true);
			}
		}
		else
		{
			if(m_btCylinderAction[28].GetCheck() != false)
				m_btCylinderAction[28].SetCheck(false);
			if(m_btCylinderAction[29].GetCheck() != false)
				m_btCylinderAction[29].SetCheck(false);
		}
		//////////////////////////////////////////////////////////////////////////
		//Door Lock 감지.
		//////////////////////////////////////////////////////////////////////////
		if(G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose == 0 &&
			(m_stOpenCloseDoor[0].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose||
			m_stOpenCloseDoor[1].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose))// && m_stOpenCloseDoor[0].GetBkColor() != G_Color_Close )//On 되면 Close 된것 입니다.
		{
			m_stOpenCloseDoor[0].SetWindowText("Close");
			m_stOpenCloseDoor[0].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[0].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[1].SetWindowText("Close");
			m_stOpenCloseDoor[1].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[1].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[0].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose;
			m_stOpenCloseDoor[1].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose;
		}
		else if(G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose == 1 &&
			(m_stOpenCloseDoor[0].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose||
			m_stOpenCloseDoor[1].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose))// && m_stOpenCloseDoor[0].GetBkColor() != G_Color_Open)
		{
			m_stOpenCloseDoor[0].SetWindowText("Open");
			m_stOpenCloseDoor[0].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[0].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[1].SetWindowText("Open");
			m_stOpenCloseDoor[1].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[1].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[0].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose;
			m_stOpenCloseDoor[1].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In03_Sn_IDXFrontDoorOpenClose;
		}
		if(G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock == 0 &&
			(m_stLockCheckDoor[0].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock||
			m_stLockCheckDoor[1].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock))// && m_stLockCheckDoor[0].GetBkColor() != G_Color_Lock)//On 되면 Lock 된것 입니다.
		{
			m_stLockCheckDoor[0].SetWindowText("Lock");
			m_stLockCheckDoor[0].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[0].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[1].SetWindowText("Lock");
			m_stLockCheckDoor[1].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[1].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[0].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock;
			m_stLockCheckDoor[1].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock;
		}
		else if(G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock == 1 &&
			(m_stLockCheckDoor[0].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock||
			m_stLockCheckDoor[1].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock) )//&&m_stLockCheckDoor[0].GetBkColor() != G_Color_Unlock)
		{
			m_stLockCheckDoor[0].SetWindowText("Unlock");
			m_stLockCheckDoor[0].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[0].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[1].SetWindowText("Unlock");
			m_stLockCheckDoor[1].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[1].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[0].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock;
			m_stLockCheckDoor[1].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock;
		}

		//////////////////////////////////////////////////////////////////////////
		if(G_MotCtrlObj.m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose == 0 &&
			m_stOpenCloseDoor[2].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose)// && m_stOpenCloseDoor[2].GetBkColor() != G_Color_Close)//On 되면 Close 된것 입니다.
		{
			m_stOpenCloseDoor[2].SetWindowText("Close");
			m_stOpenCloseDoor[2].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[2].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[2].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose;
		}
		else if(G_MotCtrlObj.m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose == 1 &&
			m_stOpenCloseDoor[2].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose)//&&  m_stOpenCloseDoor[2].GetBkColor() != G_Color_Open)
		{
			m_stOpenCloseDoor[2].SetWindowText("Open");
			m_stOpenCloseDoor[2].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[2].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[2].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In07_Sn_IDXSideDoorLeftOpenClose;
		}
		if(G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 0 &&
			m_stLockCheckDoor[2].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock)//&& m_stLockCheckDoor[2].GetBkColor() != G_Color_Lock)//On 되면 Lock 된것 입니다.
		{
			m_stLockCheckDoor[2].SetWindowText("Lock");
			m_stLockCheckDoor[2].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[2].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[2].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock;
		}
		else  if(G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 1 &&
			m_stLockCheckDoor[2].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock)//&&m_stLockCheckDoor[2].GetBkColor() != G_Color_Unlock)
		{
			m_stLockCheckDoor[2].SetWindowText("Unlock");
			m_stLockCheckDoor[2].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[2].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[2].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock;
		}
		//////////////////////////////////////////////////////////////////////////
		if(G_MotCtrlObj.m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose == 0  &&
			m_stOpenCloseDoor[3].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose)//&& m_stOpenCloseDoor[3].GetBkColor() != G_Color_Close)//On 되면 Close 된것 입니다.
		{
			m_stOpenCloseDoor[3].SetWindowText("Close");
			m_stOpenCloseDoor[3].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[3].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[3].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose;
		}
		else if(G_MotCtrlObj.m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose == 1 &&
			m_stOpenCloseDoor[3].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose)// &&m_stOpenCloseDoor[3].GetBkColor() != G_Color_Open)
		{
			m_stOpenCloseDoor[3].SetWindowText("Open");
			m_stOpenCloseDoor[3].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[3].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[3].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In09_Sn_IDXSideDoorRightOpenClose;
		}
		if(G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock == 0 &&
			m_stLockCheckDoor[3].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock)//&& m_stLockCheckDoor[3].GetBkColor() != G_Color_Lock)//On 되면 Lock 된것 입니다.
		{
			m_stLockCheckDoor[3].SetWindowText("Lock");
			m_stLockCheckDoor[3].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[3].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[3].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock;
		}
		else if(G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock == 1 &&
			m_stLockCheckDoor[3].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock)//&& m_stLockCheckDoor[3].GetBkColor() != G_Color_Unlock)
		{
			m_stLockCheckDoor[3].SetWindowText("Unlock");
			m_stLockCheckDoor[3].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[3].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[3].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock;
		}
		//////////////////////////////////////////////////////////////////////////
		if(G_MotCtrlObj.m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose == 0&&
			m_stOpenCloseDoor[4].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose)// && m_stOpenCloseDoor[4].GetBkColor() != G_Color_Close)//On 되면 Close 된것 입니다.
		{
			m_stOpenCloseDoor[4].SetWindowText("Close");
			m_stOpenCloseDoor[4].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[4].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[4].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose;
		}
		else if(G_MotCtrlObj.m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose == 1&&
			m_stOpenCloseDoor[4].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose)// &&m_stOpenCloseDoor[4].GetBkColor() != G_Color_Open)
		{
			m_stOpenCloseDoor[4].SetWindowText("Open");
			m_stOpenCloseDoor[4].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[4].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[4].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In11_Sn_InspSideDoorLeftOpenClose;
		}
		if(G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock == 0 &&
			m_stLockCheckDoor[4].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock)//&& m_stLockCheckDoor[4].GetBkColor() != G_Color_Lock)//On 되면 Lock 된것 입니다.
		{
			m_stLockCheckDoor[4].SetWindowText("Lock");
			m_stLockCheckDoor[4].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[4].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[4].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock;
		}
		else if(G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock == 1 &&
			m_stLockCheckDoor[4].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock)//&&m_stOpenCloseDoor[4].GetBkColor() != G_Color_Unlock)
		{
			m_stLockCheckDoor[4].SetWindowText("Unlock");
			m_stLockCheckDoor[4].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[4].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[4].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock;
		}
		//////////////////////////////////////////////////////////////////////////
		if(G_MotCtrlObj.m_IO_InA.In13_Sn_InspSideDoorRightOpenClose == 0 &&
			m_stOpenCloseDoor[5].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In13_Sn_InspSideDoorRightOpenClose)//&& m_stLockCheckDoor[5].GetBkColor() != G_Color_Close)//On 되면 Close 된것 입니다.
		{
			m_stOpenCloseDoor[5].SetWindowText("Close");
			m_stOpenCloseDoor[5].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[5].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[5].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In13_Sn_InspSideDoorRightOpenClose;
		}
		else if(G_MotCtrlObj.m_IO_InA.In13_Sn_InspSideDoorRightOpenClose == 1 &&
			m_stOpenCloseDoor[5].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In13_Sn_InspSideDoorRightOpenClose)//&&m_stLockCheckDoor[5].GetBkColor() != G_Color_Open)
		{
			m_stOpenCloseDoor[5].SetWindowText("Open");
			m_stOpenCloseDoor[5].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[5].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[5].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In13_Sn_InspSideDoorRightOpenClose;
		}
		if(G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock == 0 &&
			m_stLockCheckDoor[5].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock)//&& m_stLockCheckDoor[5].GetBkColor() != G_Color_Lock)//On 되면 Lock 된것 입니다.
		{
			m_stLockCheckDoor[5].SetWindowText("Lock");
			m_stLockCheckDoor[5].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[5].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[5].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock;
		}
		else if(G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock == 1 &&
			m_stLockCheckDoor[5].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock)//&&m_stLockCheckDoor[5].GetBkColor() == G_Color_Unlock)
		{
			m_stLockCheckDoor[5].SetWindowText("Unlock");
			m_stLockCheckDoor[5].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[5].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[5].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock;
		}
		//////////////////////////////////////////////////////////////////////////
		if(G_MotCtrlObj.m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose == 0 &&
			m_stOpenCloseDoor[6].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose)//&& m_stLockCheckDoor[6].GetBkColor() != G_Color_Close)//On 되면 Close 된것 입니다.
		{
			m_stOpenCloseDoor[6].SetWindowText("Close");
			m_stOpenCloseDoor[6].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[6].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[6].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose;
		}
		else if(G_MotCtrlObj.m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose == 1 &&
			m_stOpenCloseDoor[6].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose)//&&m_stOpenCloseDoor[6].GetBkColor() != G_Color_Open)
		{
			m_stOpenCloseDoor[6].SetWindowText("Open");
			m_stOpenCloseDoor[6].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[6].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[6].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In15_Sn_InspBackDoorLeftOpenClose;
		}
		if(G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock == 0 &&
			m_stLockCheckDoor[6].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock)//&& m_stLockCheckDoor[6].GetBkColor() != G_Color_Lock)//On 되면 Lock 된것 입니다.
		{
			m_stLockCheckDoor[6].SetWindowText("Lock");
			m_stLockCheckDoor[6].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[6].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[6].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock;
		}
		else if(G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock == 1 &&
			m_stLockCheckDoor[6].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock)//&&m_stLockCheckDoor[6].GetBkColor() != G_Color_Unlock)
		{
			m_stLockCheckDoor[6].SetWindowText("Unlock");
			m_stLockCheckDoor[6].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[6].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[6].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock;
		}
		//////////////////////////////////////////////////////////////////////////

		if(G_MotCtrlObj.m_IO_InA.In17_Sn_InspBackDoorRightOpenClose == 0 &&
			m_stOpenCloseDoor[7].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In17_Sn_InspBackDoorRightOpenClose)//&& m_stOpenCloseDoor[7].GetBkColor() != G_Color_Close)//On 되면 Close 된것 입니다.
		{
			m_stOpenCloseDoor[7].SetWindowText("Close");
			m_stOpenCloseDoor[7].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[7].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[7].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In17_Sn_InspBackDoorRightOpenClose;
		}
		else if(G_MotCtrlObj.m_IO_InA.In17_Sn_InspBackDoorRightOpenClose ==1 &&
			m_stOpenCloseDoor[7].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In17_Sn_InspBackDoorRightOpenClose)//&& m_stOpenCloseDoor[7].GetBkColor() != G_Color_Open)
		{
			m_stOpenCloseDoor[7].SetWindowText("Open");
			m_stOpenCloseDoor[7].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[7].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[7].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In17_Sn_InspBackDoorRightOpenClose;
		}
		if(G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock ==0  &&
			m_stLockCheckDoor[7].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock)//&& m_stLockCheckDoor[7].GetBkColor() != G_Color_Lock)//On 되면 Lock 된것 입니다.
		{
			m_stLockCheckDoor[7].SetWindowText("Lock");
			m_stLockCheckDoor[7].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[7].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[7].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock;
		}
		else if(G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock == 1 &&
			m_stLockCheckDoor[7].m_SettingFlag != G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock)//&& m_stLockCheckDoor[7].GetBkColor() != G_Color_Unlock)
		{
			m_stLockCheckDoor[7].SetWindowText("Unlock");
			m_stLockCheckDoor[7].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[7].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[7].m_SettingFlag = G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock;
		}

		if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 1)//Auto Mode일때는 Door Open 을 막는다.
		{
			if(m_btUnlockFrontDoor.IsWindowEnabled() == TRUE)
				m_btUnlockFrontDoor.EnableWindow(FALSE);

			if(m_btUnlockSideDoor.IsWindowEnabled() == TRUE)
				m_btUnlockSideDoor.EnableWindow(FALSE);

			if(m_btUnlockBackDoor.IsWindowEnabled() == TRUE)
				m_btUnlockBackDoor.EnableWindow(FALSE);
		}
		else
		{
			if(m_btUnlockFrontDoor.IsWindowEnabled() == FALSE)
				m_btUnlockFrontDoor.EnableWindow(TRUE);

			if(m_btUnlockSideDoor.IsWindowEnabled() == FALSE)
				m_btUnlockSideDoor.EnableWindow(TRUE);

			if(m_btUnlockBackDoor.IsWindowEnabled() == FALSE)
				m_btUnlockBackDoor.EnableWindow(TRUE);
		}

		if(G_MotCtrlObj.m_IO_OutA.Out00_IndexerFrontDoorRelease == 1 && m_btUnlockFrontDoor.GetCheck() == false)
		{
			m_btUnlockFrontDoor.SetWindowText("Front Door Lock");
			m_btUnlockFrontDoor.SetCheck(true);
		}
		else if(G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock == 0 && 
			m_btUnlockFrontDoor.GetCheck() == true)
		{
			m_btUnlockFrontDoor.SetWindowText("Front Door Unlock");
			m_btUnlockFrontDoor.SetCheck(false);
		}
		if(G_MotCtrlObj.m_IO_OutA.Out01_SideDoorAllRelease == 1 && m_btUnlockSideDoor.GetCheck() == false)
		{
			m_btUnlockSideDoor.SetWindowText("Side Door Lock");
			m_btUnlockSideDoor.SetCheck(true);
		}
		else if(G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 0 && 
			G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock == 0  && 
			G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock == 0  && 
			G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock == 0  && 
			m_btUnlockSideDoor.GetCheck() == true)
		{
			m_btUnlockSideDoor.SetWindowText("Side Door Unlock");
			m_btUnlockSideDoor.SetCheck(false);
		}

		if(G_MotCtrlObj.m_IO_OutA.Out02_InspectorBackDoorRelease == 1 && m_btUnlockBackDoor.GetCheck() == false) 
		{
			m_btUnlockBackDoor.SetWindowText("Back Door Lock");
			m_btUnlockBackDoor.SetCheck(true);
		}
		else if(G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock == 0 && 
			G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock == 0  && 
			m_btUnlockBackDoor.GetCheck() == true)
		{
			m_btUnlockBackDoor.SetWindowText("Back Door Unlock");
			m_btUnlockBackDoor.SetCheck(false);
		}

		//UI Test
		//G_MotCtrlObj.m_IO_InA.shAL_IN = rand();

		//G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode= 0;
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CCtrlSolPanel::OnBnClickedBtDoorFront()
{
	if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
	{
		if(m_btUnlockFrontDoor.GetCheck() == true)//Lock상태.
		{
			G_MotCtrlObj.m_IO_OutA.Out00_IndexerFrontDoorRelease =0;
			G_WriteInfo(Log_Normal, "Index Front Door Lock");
		}
		else
		{
			G_MotCtrlObj.m_IO_OutA.Out00_IndexerFrontDoorRelease =1;
			G_WriteInfo(Log_Normal, "Index Front Door Unlock");
		}
	}
	else
	{
		G_WriteInfo(Log_Worrying, "Now Auto Mode(Change Teach Mode)");
	}
}


void CCtrlSolPanel::OnBnClickedBtDoorEqSide()
{
	if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
	{

		if(m_btUnlockSideDoor.GetCheck() == true)//Lock상태.
		{
			G_MotCtrlObj.m_IO_OutA.Out01_SideDoorAllRelease =0;
			G_WriteInfo(Log_Normal, "Side Door Lock");
		}
		else
		{
			G_MotCtrlObj.m_IO_OutA.Out01_SideDoorAllRelease =1;
			G_WriteInfo(Log_Normal, "Side Door Unlock");
		}
	}
	else
	{
		G_WriteInfo(Log_Worrying, "Now Auto Mode(Change Teach Mode)");
	}
}

void CCtrlSolPanel::OnBnClickedBtDoorBack()
{
	if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
	{
		if(m_btUnlockBackDoor.GetCheck() == true)//Lock상태.
		{
			G_MotCtrlObj.m_IO_OutA.Out02_InspectorBackDoorRelease =0;
			G_WriteInfo(Log_Normal, "Back Door Lock");
		}
		else
		{
			G_MotCtrlObj.m_IO_OutA.Out02_InspectorBackDoorRelease =1;
			G_WriteInfo(Log_Normal, "Back Door Unlock");
		}
	}
	else
	{
		G_WriteInfo(Log_Worrying, "Now Auto Mode(Change Teach Mode)");
	}
}


