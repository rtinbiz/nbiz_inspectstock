// AccCtrlDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS24Motion.h"
#include "AccCtrlDlg.h"
#include "afxdialogex.h"

UINT Thread_IPPORTCheck(LPVOID pParam)
{
	CAccCtrlDlg *pObj = (CAccCtrlDlg*)pParam;


	while(pObj->m_RunThread == true)
	{
		pObj->SPR_ServerLinkCheck();
		Sleep(2000);
	}
	pObj->m_pHandleThread = nullptr;
	return 0;
}
// CAccCtrlDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAccCtrlDlg, CDialogEx)

CAccCtrlDlg::CAccCtrlDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAccCtrlDlg::IDD, pParent)
{
	m_RunThread =  false;
	m_pHandleThread = nullptr;
	m_ServerUpdateTime = 0;
}

CAccCtrlDlg::~CAccCtrlDlg()
{
}

void CAccCtrlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_PORT_LIST, m_SPRServerTree);


	//DDX_Control(pDX, IDC_BT_3D_LENS_ID1, m_bt3DScofLens[0]);
	//DDX_Control(pDX, IDC_BT_3D_LENS_ID2, m_bt3DScofLens[1]);
	//DDX_Control(pDX, IDC_BT_3D_LENS_ID3, m_bt3DScofLens[2]);
	//DDX_Control(pDX, IDC_BT_3D_LENS_ID4, m_bt3DScofLens[3]);
	//DDX_Control(pDX, IDC_BT_3D_LENS_ID5, m_bt3DScofLens[4]);

	DDX_Control(pDX, IDC_BT_SET_SCAN_RING,			m_btSetLightValue[0][0]);
	DDX_Control(pDX, IDC_BT_SET_SCAN_BACK,			m_btSetLightValue[0][1]);
	DDX_Control(pDX, IDC_BT_SET_SCAN_REFLECT,	m_btSetLightValue[0][2]);
	DDX_Control(pDX, IDC_BT_SET_SCAN_RESERVE,	m_btSetLightValue[0][3]);

	DDX_Control(pDX, IDC_BT_SET_ALIGN_1_1,			m_btSetLightValue[1][0]);
	DDX_Control(pDX, IDC_BT_SET_ALIGN_1_2,			m_btSetLightValue[1][1]);
	DDX_Control(pDX, IDC_BT_SET_ALIGN_2_1,			m_btSetLightValue[1][2]);
	DDX_Control(pDX, IDC_BT_SET_ALIGN_2_2,			m_btSetLightValue[1][3]);

	DDX_Control(pDX, IDC_ED_SCAN_RING_VALUE, m_edLightValue[0][0]);
	DDX_Control(pDX, IDC_ED_SCAN_BACK_VALUE, m_edLightValue[0][1]);
	DDX_Control(pDX, IDC_ED_SCAN_REFLECT_VALUE, m_edLightValue[0][2]);
	DDX_Control(pDX, IDC_ED_SCAN_RESERVE_VALUE, m_edLightValue[0][3]);

	DDX_Control(pDX, IDC_ED_SCAN_ALIGN_1_1_VALUE, m_edLightValue[1][0]);
	DDX_Control(pDX, IDC_ED_SCAN_ALIGN_1_2_VALUE, m_edLightValue[1][1]);
	DDX_Control(pDX, IDC_ED_SCAN_ALIGN_2_1_VALUE, m_edLightValue[1][2]);
	DDX_Control(pDX, IDC_ED_SCAN_ALIGN_2_2_VALUE, m_edLightValue[1][3]);


	DDX_Control(pDX, IDC_BT_START_LOADCELL1, m_btStartTention[0]);
	DDX_Control(pDX, IDC_BT_START_LOADCELL2, m_btStartTention[1]);
	DDX_Control(pDX, IDC_BT_START_LOADCELL3, m_btStartTention[2]);
	DDX_Control(pDX, IDC_BT_START_LOADCELL4, m_btStartTention[3]);
	DDX_Control(pDX, IDC_BT_START_LOADCELL5, m_btStartTention[4]);
	DDX_Control(pDX, IDC_BT_START_LOADCELL6, m_btStartTention[5]);
	DDX_Control(pDX, IDC_BT_START_LOADCELL7, m_btStartTention[6]);
	DDX_Control(pDX, IDC_BT_START_LOADCELL8, m_btStartTention[7]);

	DDX_Control(pDX, IDC_BT_P_REFRESH_AIR, m_btRefleshAir);
	DDX_Control(pDX, IDC_BT_CHANGE_SOL_VIEW, m_btChangeSOL);
	DDX_Control(pDX, IDC_BT_READ_QRCODE, m_btQRCodeRead);
	DDX_Control(pDX, IDC_BT_SPACE_SENSER_READ, m_btSpeceSnRead);

	DDX_Control(pDX, IDC_BT_OPEN_SETTING_FILE, m_btOpenSetFile);
}

BEGIN_MESSAGE_MAP(CAccCtrlDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_DESTROY()

	ON_MESSAGE(WM_USER_ACC_LIGHT_UPDATE, &CAccCtrlDlg::OnUserUpdateLight)
	ON_MESSAGE(WM_USER_ACC_LOADCELL_UPDATE, &CAccCtrlDlg::OnUserUpdateLoadCell)
	ON_MESSAGE(WM_USER_ACC_SpaceSensor_UPDATE, &CAccCtrlDlg::OnUserUpdateSpaceSensor)
	ON_MESSAGE(WM_USER_ACC_AIR_UPDATE, &CAccCtrlDlg::OnUserUpdateAir)
	ON_MESSAGE(WM_USER_ACC_CODE_UPDATE, &CAccCtrlDlg::OnUserUpdateCode)
	
	ON_WM_SYSCOMMAND()
	ON_WM_QUERYDRAGICON()
	//ON_BN_CLICKED(IDC_BT_3D_LENS_ID1, &CAccCtrlDlg::OnBnClickedBt3dLens)
	//ON_BN_CLICKED(IDC_BT_3D_LENS_ID2, &CAccCtrlDlg::OnBnClickedBt3dLens)
	//ON_BN_CLICKED(IDC_BT_3D_LENS_ID3, &CAccCtrlDlg::OnBnClickedBt3dLens)
	//ON_BN_CLICKED(IDC_BT_3D_LENS_ID4, &CAccCtrlDlg::OnBnClickedBt3dLens)
	//ON_BN_CLICKED(IDC_BT_3D_LENS_ID5, &CAccCtrlDlg::OnBnClickedBt3dLens)

	ON_BN_CLICKED(IDC_BT_SET_SCAN_RING, &CAccCtrlDlg::OnBnClickedBtSet)
	ON_BN_CLICKED(IDC_BT_SET_SCAN_BACK, &CAccCtrlDlg::OnBnClickedBtSet)
	ON_BN_CLICKED(IDC_BT_SET_SCAN_REFLECT, &CAccCtrlDlg::OnBnClickedBtSet)
	ON_BN_CLICKED(IDC_BT_SET_SCAN_RESERVE, &CAccCtrlDlg::OnBnClickedBtSet)
	ON_BN_CLICKED(IDC_BT_SET_ALIGN_1_1, &CAccCtrlDlg::OnBnClickedBtSet)
	ON_BN_CLICKED(IDC_BT_SET_ALIGN_1_2, &CAccCtrlDlg::OnBnClickedBtSet)
	ON_BN_CLICKED(IDC_BT_SET_ALIGN_2_1, &CAccCtrlDlg::OnBnClickedBtSet)
	ON_BN_CLICKED(IDC_BT_SET_ALIGN_2_2, &CAccCtrlDlg::OnBnClickedBtSet)

	ON_BN_CLICKED(IDC_BT_START_LOADCELL1, &CAccCtrlDlg::OnBnClickedBtStartLoadcell)
	ON_BN_CLICKED(IDC_BT_START_LOADCELL2, &CAccCtrlDlg::OnBnClickedBtStartLoadcell)
	ON_BN_CLICKED(IDC_BT_START_LOADCELL3, &CAccCtrlDlg::OnBnClickedBtStartLoadcell)
	ON_BN_CLICKED(IDC_BT_START_LOADCELL4, &CAccCtrlDlg::OnBnClickedBtStartLoadcell)
	ON_BN_CLICKED(IDC_BT_START_LOADCELL5, &CAccCtrlDlg::OnBnClickedBtStartLoadcell)
	ON_BN_CLICKED(IDC_BT_START_LOADCELL6, &CAccCtrlDlg::OnBnClickedBtStartLoadcell)
	ON_BN_CLICKED(IDC_BT_START_LOADCELL7, &CAccCtrlDlg::OnBnClickedBtStartLoadcell)
	ON_BN_CLICKED(IDC_BT_START_LOADCELL8, &CAccCtrlDlg::OnBnClickedBtStartLoadcell)

	ON_BN_CLICKED(IDC_BT_P_REFRESH_AIR, &CAccCtrlDlg::OnBnClickedBtPRefreshAir)
	ON_BN_CLICKED(IDC_BT_CHANGE_SOL_VIEW, &CAccCtrlDlg::OnBnClickedBtChangeSolView)
	ON_BN_CLICKED(IDC_BT_READ_QRCODE, &CAccCtrlDlg::OnBnClickedBtReadQrcode)

	ON_BN_CLICKED(IDC_BT_SPACE_SENSER_READ, &CAccCtrlDlg::OnBnClickedBtSpaceSenserRead)
	//ON_EN_CHANGE(IDC_ED_SCAN_RING_VALUE, &CAccCtrlDlg::OnEnChangeEdValue)
	//ON_EN_CHANGE(IDC_ED_SCAN_BACK_VALUE, &CAccCtrlDlg::OnEnChangeEdValue)
	//ON_EN_CHANGE(IDC_ED_SCAN_REFLECT_VALUE, &CAccCtrlDlg::OnEnChangeEdValue)
	//ON_EN_CHANGE(IDC_ED_SCAN_RESERVE_VALUE, &CAccCtrlDlg::OnEnChangeEdValue)

	//ON_EN_CHANGE(IDC_ED_SCAN_ALIGN_1_1_VALUE, &CAccCtrlDlg::OnEnChangeEdValue)
	//ON_EN_CHANGE(IDC_ED_SCAN_ALIGN_1_2_VALUE, &CAccCtrlDlg::OnEnChangeEdValue)
	//ON_EN_CHANGE(IDC_ED_SCAN_ALIGN_2_1_VALUE, &CAccCtrlDlg::OnEnChangeEdValue)
	//ON_EN_CHANGE(IDC_ED_SCAN_ALIGN_2_2_VALUE, &CAccCtrlDlg::OnEnChangeEdValue)
	ON_BN_CLICKED(IDC_BT_OPEN_SETTING_FILE, &CAccCtrlDlg::OnBnClickedBtOpenSettingFile)
END_MESSAGE_MAP()


// CAccCtrlDlg 메시지 처리기입니다.


BOOL CAccCtrlDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CAccCtrlDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	//2) ACC Data Read
	LoadSPRServerList();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CAccCtrlDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btSetLightValue[0][0].GetTextColor(&tColor);
		m_btSetLightValue[0][0].GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		int BTIndex = 0;
		for(int BTStep = 0; BTStep<4; BTStep++)
		{
			m_btSetLightValue[0][BTStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btSetLightValue[0][BTStep].SetTextColor(&tColor);
			m_btSetLightValue[0][BTStep].SetCheckButton(true, true);
			m_btSetLightValue[0][BTStep].SetFont(&tFont);
		}
		for(int BTStep = 0; BTStep<4; BTStep++)
		{
			m_btSetLightValue[1][BTStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btSetLightValue[1][BTStep].SetTextColor(&tColor);
			m_btSetLightValue[1][BTStep].SetCheckButton(true, true);
			m_btSetLightValue[1][BTStep].SetFont(&tFont);
		}
		for(int BTStep = 0; BTStep<8; BTStep++)
		{
			m_btStartTention[BTStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btStartTention[BTStep].SetTextColor(&tColor);
			m_btStartTention[BTStep].SetCheckButton(true, true);
			m_btStartTention[BTStep].SetFont(&tFont);
		}

		//for(int BTStep = 0; BTStep<5; BTStep++)
		//{
		//	m_bt3DScofLens[BTStep].SetRoundButtonStyle(&m_tButtonStyle);
		//	m_bt3DScofLens[BTStep].SetTextColor(&tColor);
		//	m_bt3DScofLens[BTStep].SetCheckButton(true, true);
		//	m_bt3DScofLens[BTStep].SetFont(&tFont);
		//}


		m_btRefleshAir.SetRoundButtonStyle(&m_tButtonStyle);
		m_btRefleshAir.SetTextColor(&tColor);
		m_btRefleshAir.SetCheckButton(true, true);
		m_btRefleshAir.SetFont(&tFont);


		m_btQRCodeRead.SetRoundButtonStyle(&m_tButtonStyle);
		m_btQRCodeRead.SetTextColor(&tColor);
		m_btQRCodeRead.SetCheckButton(false);
		m_btQRCodeRead.SetFont(&tFont);

		m_btSpeceSnRead.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSpeceSnRead.SetTextColor(&tColor);
		m_btSpeceSnRead.SetCheckButton(false);
		m_btSpeceSnRead.SetFont(&tFont);

		m_btChangeSOL.SetRoundButtonStyle(&m_tButtonStyle);
		m_btChangeSOL.SetTextColor(&tColor);
		m_btChangeSOL.SetCheckButton(false);
		m_btChangeSOL.SetFont(&tFont);

		m_btOpenSetFile.SetRoundButtonStyle(&m_tButtonStyle);
		m_btOpenSetFile.SetTextColor(&tColor);
		m_btOpenSetFile.SetCheckButton(false);
		m_btOpenSetFile.SetFont(&tFont);
		
	}

	m_EditFont.CreateFont(20, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	GetDlgItem(IDC_ED_SCAN_RING_VALUE)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_SCAN_BACK_VALUE)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_SCAN_REFLECT_VALUE)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_SCAN_RESERVE_VALUE)->SetFont(&m_EditFont);
	
	GetDlgItem(IDC_ED_SCAN_ALIGN_1_1_VALUE)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_SCAN_ALIGN_1_2_VALUE)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_SCAN_ALIGN_2_1_VALUE)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_SCAN_ALIGN_2_2_VALUE)->SetFont(&m_EditFont);


	GetDlgItem(IDC_ED_LOAD_CELL_VALUE1)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE2)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE3)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE4)->SetFont(&m_EditFont);

	GetDlgItem(IDC_ED_LOAD_CELL_VALUE5)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE6)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE7)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE8)->SetFont(&m_EditFont);

	GetDlgItem(IDC_ED_SPACE_SENSER_OUT1)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_SPACE_SENSER_OUT2)->SetFont(&m_EditFont);

	GetDlgItem(IDC_ED_PRESSURE_AIR)->SetFont(&m_EditFont);
	GetDlgItem(IDC_ED_PRESSURE_N2)->SetFont(&m_EditFont);

	GetDlgItem(IDC_ED_QR_CODE_VALUE)->SetFont(&m_EditFont);
}

void CAccCtrlDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CDialogEx::PostNcDestroy();
	delete this;
}

HBRUSH CAccCtrlDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(DLG_ID_Number == IDC_TREE_PORT_LIST ||
		DLG_ID_Number == IDC_ED_SCAN_RING_VALUE||
		DLG_ID_Number == IDC_ED_SCAN_BACK_VALUE||
		DLG_ID_Number == IDC_ED_SCAN_REFLECT_VALUE||
		DLG_ID_Number == IDC_ED_SCAN_RESERVE_VALUE||
		
		DLG_ID_Number == IDC_ED_SCAN_ALIGN_1_1_VALUE||
		DLG_ID_Number == IDC_ED_SCAN_ALIGN_1_2_VALUE||
		DLG_ID_Number == IDC_ED_SCAN_ALIGN_2_1_VALUE||
		DLG_ID_Number == IDC_ED_SCAN_ALIGN_2_2_VALUE)
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CAccCtrlDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(SERVER_CHECK_TIMER == nIDEvent)
	{
		CString strServerUpdateTime;
		strServerUpdateTime.Format("%d", m_ServerUpdateTime++);
		SetDlgItemText(IDC_ST_LIST_UPDATE_TIME, strServerUpdateTime);
	}
	if(TIMER_ACC_UPDATE == nIDEvent)
	{

		
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CAccCtrlDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	int ExitTimeOut = 300;
	
	m_RunThread = false;

	while(m_pHandleThread != nullptr)
	{
		ExitTimeOut--;
		if(ExitTimeOut<0)
		{
			break;
		}
		Sleep(10);
	}

	//내부 정리 작업
	CNet232Port *pDeleteObj = nullptr;
	HTREEITEM Root1Item = m_SPRServerTree.GetRootItem();

	while(Root1Item != NULL)
	{
		pDeleteObj = (CNet232Port *)m_SPRServerTree.GetItemData(Root1Item);
		if(pDeleteObj != nullptr)
			delete pDeleteObj;
		pDeleteObj = nullptr;
		m_SPRServerTree.DeleteItem(Root1Item);
		Root1Item = m_SPRServerTree.GetRootItem();
	}

}
void CAccCtrlDlg::LoadSPRServerList()
{
	//////////////////////////////////////////////////////////////////////////
	G_LightCtlrObj.m_ParentUpdate = this->GetSafeHwnd();		G_LightCtlrObj.m_CtrlIndex = 0;
	//G_LightCtlrObj[1].m_ParentUpdate = this->GetSafeHwnd();		G_LightCtlrObj[1].m_CtrlIndex = 1;
	G_AirCtlrObj.m_ParentUpdate = this->GetSafeHwnd();
	//m_LensCtlrObj.m_ParentUpdate = this->GetSafeHwnd();
	G_QRCtlrObj.m_ParentUpdate = this->GetSafeHwnd();
#ifdef BCR_USE_LINK
	G_BCRCtrlObj.m_ParentUpdate = this->GetSafeHwnd();
#endif

	G_TesionCtlrObj[0].m_ParentUpdate = this->GetSafeHwnd();	G_TesionCtlrObj[0].m_CtrlIndex = 1;
	G_TesionCtlrObj[1].m_ParentUpdate = this->GetSafeHwnd();	G_TesionCtlrObj[1].m_CtrlIndex = 2;
	G_TesionCtlrObj[2].m_ParentUpdate = this->GetSafeHwnd();	G_TesionCtlrObj[2].m_CtrlIndex = 3;
	G_TesionCtlrObj[3].m_ParentUpdate = this->GetSafeHwnd();	G_TesionCtlrObj[3].m_CtrlIndex = 4;

	G_TesionCtlrObj[4].m_ParentUpdate = this->GetSafeHwnd();	G_TesionCtlrObj[4].m_CtrlIndex = 5;
	G_TesionCtlrObj[5].m_ParentUpdate = this->GetSafeHwnd();	G_TesionCtlrObj[5].m_CtrlIndex = 6;
	G_TesionCtlrObj[6].m_ParentUpdate = this->GetSafeHwnd();	G_TesionCtlrObj[6].m_CtrlIndex = 7;
	G_TesionCtlrObj[7].m_ParentUpdate = this->GetSafeHwnd();	G_TesionCtlrObj[7].m_CtrlIndex = 8;

#ifndef TENTION_CELL_USE_5_8
	m_btStartTention[4].EnableWindow(FALSE);
	m_btStartTention[5].EnableWindow(FALSE);
	m_btStartTention[6].EnableWindow(FALSE);
	m_btStartTention[7].EnableWindow(FALSE);

	GetDlgItem(IDC_ED_LOAD_CELL_VALUE5)->EnableWindow(FALSE);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE6)->EnableWindow(FALSE);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE7)->EnableWindow(FALSE);
	GetDlgItem(IDC_ED_LOAD_CELL_VALUE8)->EnableWindow(FALSE);
#endif

	G_SpaceCtrObj.m_ParentUpdate = this->GetSafeHwnd();
	//////////////////////////////////////////////////////////////////////////
	m_ImageList.Create(MAKEINTRESOURCE(IDB_ITEM_IMAGE), 16, 1, RGB(255,255,255));
	m_SPRServerTree.SetImageList(&m_ImageList,TVSIL_NORMAL);
	m_SPRServerTree.DeleteAllItems();
	HTREEITEM Root1Item;

	Root1Item = m_SPRServerTree.InsertItem("Setting", 4, 4, NULL);

	int IndexIP=0;
	int IPSetImgIndex = 5;
	int PtSetImgIndex = 5;
	CString strIPList[15];
	CString strIP;
	CString strPort;
	char strReadData[256];

	LinkUpdate nowSetItem;
	nowSetItem.pSPRServerTree = &m_SPRServerTree;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SYS_PORT, Key_LIGHT_CTRL_SCAN, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	strIPList[IndexIP].Format("%s", strReadData); 
	strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_LIGHT_CTRL_SCAN, 7, 7, Root1Item);
	nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	G_LightCtlrObj.SetConnectInfo(strIP, strPort, nowSetItem);
	//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	strIPList[IndexIP].Format("%s", strIP); 
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_LightValue_Scan, Key_LV_ch1_ScanRing, "100", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	G_LightCtlrObj.m_LightValue[0] = atoi(strReadData);
	SetDlgItemText(IDC_ED_SCAN_RING_VALUE, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_LightValue_Scan, Key_LV_ch2_ScanBack, "100", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	G_LightCtlrObj.m_LightValue[1] = atoi(strReadData);
	SetDlgItemText(IDC_ED_SCAN_BACK_VALUE, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_LightValue_Scan, Key_LV_ch3_ScanReflect, "100", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	G_LightCtlrObj.m_LightValue[2] = atoi(strReadData);
	SetDlgItemText(IDC_ED_SCAN_REFLECT_VALUE, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_LightValue_Scan, Key_LV_ch4_Reserve, "100", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	G_LightCtlrObj.m_LightValue[3] = atoi(strReadData);
	SetDlgItemText(IDC_ED_SCAN_RESERVE_VALUE, strReadData);

	//////////////////////////////////////////////////////////////////////////
	//IndexIP++;
	//GetPrivateProfileString(Section_SYS_PORT, Key_LIGHT_CTRL_ALIGN, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	//strIPList[IndexIP].Format("%s", strReadData); 
	//strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	//strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	//nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_LIGHT_CTRL_ALIGN, 7, 7, Root1Item);
	//nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	//nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	//G_LightCtlrObj[1].SetConnectInfo(strIP, strPort, nowSetItem);
	////m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	//strIPList[IndexIP].Format("%s", strIP); 

	//GetPrivateProfileString(Section_LightValue_Align, Key_LV_ch1_Align1_1, "100", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	//G_LightCtlrObj[1].m_LightValue[0] = atoi(strReadData);
	//SetDlgItemText(IDC_ED_SCAN_ALIGN_1_1_VALUE, strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_LightValue_Align, Key_LV_ch2_Align1_2, "100", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	//G_LightCtlrObj[1].m_LightValue[1] = atoi(strReadData);
	//SetDlgItemText(IDC_ED_SCAN_ALIGN_1_2_VALUE, strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_LightValue_Align, Key_LV_ch3_Align2_1, "100", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	//G_LightCtlrObj[1].m_LightValue[2] = atoi(strReadData);
	//SetDlgItemText(IDC_ED_SCAN_ALIGN_2_1_VALUE, strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_LightValue_Align, Key_LV_ch4_Align2_2, "100", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	//G_LightCtlrObj[1].m_LightValue[3] = atoi(strReadData);
	//SetDlgItemText(IDC_ED_SCAN_ALIGN_2_2_VALUE, strReadData);
	//////////////////////////////////////////////////////////////////////////

	IndexIP++;
	GetPrivateProfileString(Section_SYS_PORT, Key_TENSION_CELL1, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	strIPList[IndexIP].Format("%s", strReadData); 
	strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_TENSION_CELL1, 7, 7, Root1Item);
	nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	G_TesionCtlrObj[0].SetConnectInfo(strIP, strPort, nowSetItem);
	//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	strIPList[IndexIP].Format("%s", strIP);
	IndexIP++;
	GetPrivateProfileString(Section_SYS_PORT, Key_TENSION_CELL2, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	strIPList[IndexIP].Format("%s", strReadData); 
	strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_TENSION_CELL2, 7, 7, Root1Item);
	nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	G_TesionCtlrObj[1].SetConnectInfo(strIP, strPort, nowSetItem);
	//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	strIPList[IndexIP].Format("%s", strIP);
	IndexIP++;
	GetPrivateProfileString(Section_SYS_PORT, Key_TENSION_CELL3, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	strIPList[IndexIP].Format("%s", strReadData); 
	strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_TENSION_CELL3, 7, 7, Root1Item);
	nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	G_TesionCtlrObj[2].SetConnectInfo(strIP, strPort, nowSetItem);
	//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	strIPList[IndexIP].Format("%s", strIP);
	IndexIP++;
	GetPrivateProfileString(Section_SYS_PORT, Key_TENSION_CELL4, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	strIPList[IndexIP].Format("%s", strReadData); 
	strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_TENSION_CELL4, 7, 7, Root1Item);
	nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	G_TesionCtlrObj[3].SetConnectInfo(strIP, strPort, nowSetItem);
	//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	strIPList[IndexIP].Format("%s", strIP);
	IndexIP++;
	//////////////////////////////////////////////////////////////////////////
#ifdef TENTION_CELL_USE_5_8
		GetPrivateProfileString(Section_SYS_PORT, Key_TENSION_CELL5, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
		strIPList[IndexIP].Format("%s", strReadData); 
		strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
		strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
		nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_TENSION_CELL5, 7, 7, Root1Item);
		nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
		nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
		G_TesionCtlrObj[4].SetConnectInfo(strIP, strPort, nowSetItem);
		//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
		strIPList[IndexIP].Format("%s", strIP);
		IndexIP++;

		GetPrivateProfileString(Section_SYS_PORT, Key_TENSION_CELL6, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
		strIPList[IndexIP].Format("%s", strReadData); 
		strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
		strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
		nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_TENSION_CELL6, 7, 7, Root1Item);
		nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
		nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
		G_TesionCtlrObj[5].SetConnectInfo(strIP, strPort, nowSetItem);
		//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
		strIPList[IndexIP].Format("%s", strIP);
		IndexIP++;

		GetPrivateProfileString(Section_SYS_PORT, Key_TENSION_CELL7, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
		strIPList[IndexIP].Format("%s", strReadData); 
		strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
		strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
		nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_TENSION_CELL7, 7, 7, Root1Item);
		nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
		nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
		G_TesionCtlrObj[6].SetConnectInfo(strIP, strPort, nowSetItem);
		//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
		strIPList[IndexIP].Format("%s", strIP);
		IndexIP++;

		GetPrivateProfileString(Section_SYS_PORT, Key_TENSION_CELL8, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
		strIPList[IndexIP].Format("%s", strReadData); 
		strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
		strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
		nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_TENSION_CELL8, 7, 7, Root1Item);
		nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
		nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
		G_TesionCtlrObj[7].SetConnectInfo(strIP, strPort, nowSetItem);
		//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
		strIPList[IndexIP].Format("%s", strIP);
		IndexIP++;
#endif
	GetPrivateProfileString(Section_SYS_PORT, Key_QR_READER, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	strIPList[IndexIP].Format("%s", strReadData); 
	strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_QR_READER, 7, 7, Root1Item);
	nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	G_QRCtlrObj.SetConnectInfo(strIP, strPort, nowSetItem);
	//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	strIPList[IndexIP].Format("%s", strIP);
	IndexIP++;
	//////////////////////////////////////////////////////////////////////////
	GetPrivateProfileString(Section_SYS_PORT, Key_AIR_PRESS, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	strIPList[IndexIP].Format("%s", strReadData); 
	strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_AIR_PRESS, 7, 7, Root1Item);
	nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	G_AirCtlrObj.SetConnectInfo(strIP, strPort, nowSetItem);
	//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	strIPList[IndexIP].Format("%s", strIP);
	IndexIP++;
	//////////////////////////////////////////////////////////////////////////
	//GetPrivateProfileString(Section_SYS_PORT, Key_3D_REVOLVER, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	//strIPList[IndexIP].Format("%s", strReadData); 
	//strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	//strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	//nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_3D_REVOLVER, 7, 7, Root1Item);
	//nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	//nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	//m_LensCtlrObj.SetConnectInfo(strIP, strPort, nowSetItem);
	////m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	//strIPList[IndexIP].Format("%s", strIP);
	//IndexIP++;
	//////////////////////////////////////////////////////////////////////////
	GetPrivateProfileString(Section_SYS_PORT, Key_SPACE_SENSOR, "", &strReadData[0], 256,  SYSTEM_PARAM_INI_PATH);
	strIPList[IndexIP].Format("%s", strReadData); 
	strIP.Format("%s", strIPList[IndexIP].Mid(0,strIPList[IndexIP].Find(':')));
	strPort.Format("%s", strIPList[IndexIP].Mid(strIPList[IndexIP].Find(':')+1,strIPList[IndexIP].GetLength()));
	nowSetItem.CtrlItem  = m_SPRServerTree.InsertItem(Key_SPACE_SENSOR, 7, 7, Root1Item);
	nowSetItem.IPItem    = m_SPRServerTree.InsertItem(strIP, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	nowSetItem.PortItem = m_SPRServerTree.InsertItem(strPort, IPSetImgIndex, PtSetImgIndex, nowSetItem.CtrlItem);
	G_SpaceCtrObj.SetConnectInfo(strIP, strPort, nowSetItem);
	//m_SPRServerTree.Expand(nowSetItem.CtrlItem, TVE_EXPAND);
	strIPList[IndexIP].Format("%s", strIP);
	IndexIP++;




	m_SPRServerTree.Expand(Root1Item, TVE_EXPAND);
	//Link용 Server정보만 취득 한다.
	for(int IPStep= 0; IPStep<IndexIP; IPStep++)
	{
		strIPList[IPStep].Replace(" ", "");
	}
	for(int IPStep= 0; IPStep<IndexIP; IPStep++)
	{
		if(strIPList[IPStep].GetLength()>5)
		{
			for(int DelStep= IPStep+1; DelStep<IndexIP; DelStep++)
			{
				if(strIPList[DelStep].GetLength()>5)
				{
					if(strIPList[IPStep].CompareNoCase(strIPList[DelStep]) == 0)
						strIPList[DelStep].Format("");
				}
			}
		}
	}


	for(int IPStep= 0; IPStep<IndexIP; IPStep++)
	{
		if(strIPList[IPStep].GetLength()>5)
		{
			Root1Item = m_SPRServerTree.InsertItem(strIPList[IPStep], 1, 1, NULL);
			m_SPRServerTree.SetItemData(Root1Item, (DWORD_PTR)new CNet232Port() );
		}
	}

	WSADATA SendData; 
	//통신 Clear 
	if(WSAStartup(MAKEWORD(2,2), &SendData) != NO_ERROR) 
	{ 
		return;
	} 
	//SPR_ServerLinkCheck();

	m_RunThread =  true;
	m_pHandleThread = ::AfxBeginThread(Thread_IPPORTCheck, this,	 THREAD_PRIORITY_NORMAL);

	SetTimer(SERVER_CHECK_TIMER, 1000, 0);
	SetTimer(TIMER_ACC_UPDATE, 400, 0);
}
void CAccCtrlDlg::SPR_ServerLinkCheck()
{
	HTREEITEM Root1Item = m_SPRServerTree.GetRootItem();
	CNet232Port *pChkObj = nullptr;
	CString strConnectIPAdd;
	DWORD IPValue = 0;

	while(Root1Item != NULL)
	{
		pChkObj = (CNet232Port *)m_SPRServerTree.GetItemData(Root1Item);

		if(pChkObj != nullptr)
		{
			if(pChkObj->m_Net232Sockfd == INVALID_SOCKET)
			{//Socket 연결이 되어 있지 않으면 Link check
				strConnectIPAdd.Format("%s", m_SPRServerTree.GetItemText(Root1Item));
				IPValue = inet_addr(strConnectIPAdd.GetBuffer(strConnectIPAdd.GetLength()));
				if(pChkObj->Net232Connect(IPValue, SPR_SERVER_PORT) == true)
				{
					m_SPRServerTree.SetItemImage(Root1Item, 0,0);
					////연결 되면 List요청.
					//ListCBData *pNewCBData = new ListCBData();
					//pNewCBData->pCallObj = (LPVOID)this;
					//pNewCBData->Root1Item = Root1Item;
					//if(pChkObj->fnPortListUpDate((LPVOID)pNewCBData, PortListCallBack) != SPR_PORT_RESULT_OK)
					//{
					//	delete pNewCBData;
					//	pNewCBData = nullptr;
					//	m_SPRServerTree.SetItemImage(Root1Item, 1,1);
					//}
				}
				else
					m_SPRServerTree.SetItemImage(Root1Item, 1,1);
			}
			else
			{//연결 되어 있다면 List요청.
				if(pChkObj->m_pfnRecvList == nullptr)//Null이 아니라면 지금 처리중이다.
				{
					ListCBData *pNewCBData = new ListCBData();
					pNewCBData->pCallObj = (LPVOID)this;
					pNewCBData->Root1Item = Root1Item;
					if(pChkObj->fnPortListUpDate((LPVOID)pNewCBData, PortListCallBack) != SPR_PORT_RESULT_OK)
					{
						delete pNewCBData;
						pNewCBData = nullptr;
						m_SPRServerTree.SetItemImage(Root1Item, 1,1);
						HTREEITEM PortItem = NULL;
						PortItem = m_SPRServerTree.GetChildItem(Root1Item);
						while(PortItem != NULL)
						{
							m_SPRServerTree.DeleteItem(PortItem);
							PortItem = m_SPRServerTree.GetChildItem(Root1Item);
						}
						pChkObj->Net232DisConnect();
					}
				}
			}
		}
		Root1Item = m_SPRServerTree.GetNextItem(Root1Item, TVGN_NEXT);
	}
	m_ServerUpdateTime = 0;
}

void CAccCtrlDlg::PortListCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen)
{
	ListCBData *pCallData = (ListCBData *)CtrlObj;

	CAccCtrlDlg *pCallObj =(CAccCtrlDlg *)pCallData->pCallObj;
	HTREEITEM Root1Item = pCallData->Root1Item;

	HTREEITEM PortItem = NULL;
	PortItem = pCallObj->m_SPRServerTree.GetChildItem(Root1Item);
	while(PortItem != NULL)
	{
		pCallObj->m_SPRServerTree.DeleteItem(PortItem);
		PortItem = pCallObj->m_SPRServerTree.GetChildItem(Root1Item);
	}

	char strPortList[1024];
	memset(strPortList, 0x00, 1024);
	int FindPortNum = 0;
	CStringArray strPortListArray;
	int PCPortCnt = 0;

	CString str; 
	str.Format("%s", (char*)pBuf);
	CString resToken; 
	int curPos= 0; 
	resToken= str.Tokenize("/",curPos); 
	for(int PStep = 0; resToken != "" ; PStep++)
	{ 
		if(resToken.GetAt(0)=='X')//사용중
		{
			resToken.Remove('X');
			pCallObj->m_SPRServerTree.InsertItem(resToken.GetBuffer(resToken.GetLength()), 3, 3, Root1Item);
		}
		else
		{
			pCallObj->m_SPRServerTree.InsertItem(resToken.GetBuffer(resToken.GetLength()), 2, 2, Root1Item);
		}

		resToken= str.Tokenize("/",curPos); 
		PCPortCnt++;
	}
	delete pCallData;
	pCallData = nullptr;
}
void CAccCtrlDlg::OnBnClickedBtOpenSettingFile()
{
	CString strCmd;
	strCmd.Format("notePad %s", SYSTEM_PARAM_INI_PATH);
	::WinExec(strCmd.GetBuffer(strCmd.GetLength()), 1);
}
//void CAccCtrlDlg::OnBnClickedBt3dLens()
//{
//	CWnd *pwnd = GetFocus(); 
//	int wID = pwnd->GetDlgCtrlID();
//	switch(wID)
//	{
//	case IDC_BT_3D_LENS_ID1:
//		{
//
//		}break;
//	case IDC_BT_3D_LENS_ID2:
//		{
//
//		}break;
//	case IDC_BT_3D_LENS_ID3:
//		{
//
//		}break;
//	case IDC_BT_3D_LENS_ID4:
//		{
//
//		}break;
//	default:
//		break;
//	}
//}

void CAccCtrlDlg::OnBnClickedBtSet()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();

	int ctrlNum= -1;
	LIGHT_CH ChNum = LIGHT_CH_None;
	switch(wID)
	{
	case IDC_BT_SET_SCAN_RING:
		{
			ctrlNum = 0;
			ChNum = LIGHT_CH_1;
		}break;
	case IDC_BT_SET_SCAN_BACK:
		{
			ctrlNum = 0;
			ChNum = LIGHT_CH_2;
		}break;
	case IDC_BT_SET_SCAN_REFLECT:
		{
			ctrlNum = 0;
			ChNum = LIGHT_CH_3;
		}break;
	case IDC_BT_SET_SCAN_RESERVE:
		{
			ctrlNum = 0;
			ChNum = LIGHT_CH_4;
		}break;
	//case IDC_BT_SET_ALIGN_1_1:
	//	{
	//		ctrlNum = 1;
	//		ChNum = LIGHT_CH_1;
	//	}break;
	//case IDC_BT_SET_ALIGN_1_2:
	//	{
	//		ctrlNum = 1;
	//		ChNum = LIGHT_CH_2;
	//	}break;
	//case IDC_BT_SET_ALIGN_2_1:
	//	{
	//		ctrlNum = 1;
	//		ChNum = LIGHT_CH_3;
	//	}break;
	//case IDC_BT_SET_ALIGN_2_2:
	//	{
	//		ctrlNum = 1;
	//		ChNum = LIGHT_CH_4;
	//	}break;
	default:
		break;
	}
	CString strNewValue;
	if(ctrlNum>=0 &&  ChNum != LIGHT_CH_None)
	{
		if(G_LightCtlrObj.m_isOpen == true)
		{
			m_btSetLightValue[ctrlNum][ChNum-1].SetCheck(false);
			m_edLightValue[ctrlNum][ChNum-1].GetWindowText(strNewValue);
			G_LightCtlrObj.SetLightValue(ChNum, atoi(strNewValue) );
		}
		else
		{
			if(G_LightCtlrObj.LinkOpen()== false)
			{
				m_btSetLightValue[ctrlNum][0].SetCheck(false);
				m_btSetLightValue[ctrlNum][1].SetCheck(false);
				m_btSetLightValue[ctrlNum][2].SetCheck(false);
				m_btSetLightValue[ctrlNum][3].SetCheck(false);
				m_btSetLightValue[ctrlNum][0].SetWindowText("Connect");
				m_btSetLightValue[ctrlNum][1].SetWindowText("Connect");
				m_btSetLightValue[ctrlNum][2].SetWindowText("Connect");
				m_btSetLightValue[ctrlNum][3].SetWindowText("Connect");

			}
			else
			{
				m_btSetLightValue[ctrlNum][0].SetCheck(true);
				m_btSetLightValue[ctrlNum][1].SetCheck(true);
				m_btSetLightValue[ctrlNum][2].SetCheck(true);
				m_btSetLightValue[ctrlNum][3].SetCheck(true);

				m_btSetLightValue[ctrlNum][0].SetWindowText("Set");
				m_btSetLightValue[ctrlNum][1].SetWindowText("Set");
				m_btSetLightValue[ctrlNum][2].SetWindowText("Set");
				m_btSetLightValue[ctrlNum][3].SetWindowText("Set");
			}			
		}
	}
}

void CAccCtrlDlg::OnBnClickedBtStartLoadcell()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();
	int BtIndex = -1;
	switch(wID)
	{
	case IDC_BT_START_LOADCELL1: BtIndex = 0; break;
	case IDC_BT_START_LOADCELL2: BtIndex = 1; break;
	case IDC_BT_START_LOADCELL3: BtIndex = 2; break;
	case IDC_BT_START_LOADCELL4: BtIndex = 3; break;
	case IDC_BT_START_LOADCELL5: BtIndex = 4; break;
	case IDC_BT_START_LOADCELL6: BtIndex = 5; break;
	case IDC_BT_START_LOADCELL7: BtIndex = 6; break;
	case IDC_BT_START_LOADCELL8: BtIndex = 7; break;
	default:
		break;
	}
	if(BtIndex>=0)
	{
		if(G_TesionCtlrObj[BtIndex].m_isOpen == true)
		{
			if(m_btStartTention[BtIndex].GetCheck() == false)
			{
				m_btStartTention[BtIndex].SetCheck(true);
				m_btStartTention[BtIndex].SetWindowText("Stop");
				G_TesionCtlrObj[BtIndex].SetStreamMod(true);
				//G_TesionCtlrObj[BtIndex].ReadPos();
			}
			else
			{
				m_btStartTention[BtIndex].SetCheck(false);
				m_btStartTention[BtIndex].SetWindowText("Start");
				G_TesionCtlrObj[BtIndex].SetStreamMod( false);
			}	
		}
		else
		{
			if(G_TesionCtlrObj[BtIndex].LinkOpen()== false)
			{
				m_btStartTention[BtIndex].SetCheck(false);
				m_btStartTention[BtIndex].SetWindowText("Connect");
				G_TesionCtlrObj[BtIndex].SetStreamMod(false);
			}
			else
			{
				m_btStartTention[BtIndex].SetCheck(false);
				m_btStartTention[BtIndex].SetWindowText("Start");
				G_TesionCtlrObj[BtIndex].SetStreamMod(false);
			}			
		}
	}
}


void CAccCtrlDlg::OnBnClickedBtPRefreshAir()
{
	if(G_AirCtlrObj.m_isOpen == true)
	{
		m_btRefleshAir.SetCheck(false);
		G_AirCtlrObj.ReadOutAll();
	}
	else
	{
		if(G_AirCtlrObj.LinkOpen()== false)
		{
			m_btRefleshAir.SetCheck(false);
			m_btRefleshAir.SetWindowText("Connect");
		}
		else
		{
			m_btRefleshAir.SetCheck(true);
			m_btRefleshAir.SetWindowText("Read");
		}
	}
}
void CAccCtrlDlg::OnBnClickedBtReadQrcode()
{

#ifdef BCR_USE_LINK
	if(G_BCRCtrlObj.m_BCRSockfd  != INVALID_SOCKET)
	{
		//m_btQRCodeRead.SetCheck(false);
		G_BCRCtrlObj.SetTriggerOnOff(true);
	}
	else
	{
		if(G_BCRCtrlObj.LinkOpen()== false)
		{
			//m_btQRCodeRead.SetCheck(false);
			m_btQRCodeRead.SetWindowText("Connect");
		}
		else
		{
			//m_btQRCodeRead.SetCheck(true);
			m_btQRCodeRead.SetWindowText("Read");
		}			
	}
#else
	if(G_QRCtlrObj.m_isOpen == true)
	{
		m_btQRCodeRead.SetCheck(false);
		G_QRCtlrObj.SetTriggerOnOff(true);
	}
	else
	{
		if(G_QRCtlrObj.LinkOpen()== false)
		{
			m_btQRCodeRead.SetCheck(false);
			m_btQRCodeRead.SetWindowText("Connect");
		}
		else
		{
			m_btQRCodeRead.SetCheck(true);
			m_btQRCodeRead.SetWindowText("Read");
		}			
	}
#endif
}

void CAccCtrlDlg::OnBnClickedBtSpaceSenserRead()
{

	if(G_SpaceCtrObj.m_isOpen == true)
	{
		if(m_btSpeceSnRead.GetCheck() == false)
		{
			m_btSpeceSnRead.SetCheck(true);
			m_btSpeceSnRead.SetWindowText("Stop");
			G_SpaceCtrObj.SetStreamMod(true);
		}
		else
		{
			m_btSpeceSnRead.SetCheck(false);
			m_btSpeceSnRead.SetWindowText("Start");
			G_SpaceCtrObj.SetStreamMod( false);
		}	
	}
	else
	{
		if(G_SpaceCtrObj.LinkOpen()== false)
		{
			m_btSpeceSnRead.SetCheck(false);
			m_btSpeceSnRead.SetWindowText("Connect");
			G_SpaceCtrObj.SetStreamMod(false);
		}
		else
		{
			m_btSpeceSnRead.SetCheck(false);
			m_btSpeceSnRead.SetWindowText("Start");
			G_SpaceCtrObj.SetStreamMod(false);
		}			
	}
}

LRESULT CAccCtrlDlg::OnUserUpdateLight(WPARAM wParam, LPARAM lParam)
{

	int LightCh = (int)wParam;
	int LightValue = (int)lParam;
	CString strNewValue;
	strNewValue.Format("%d", LightValue);
	switch(LightCh)
	{
	case 11:
			SetDlgItemText(IDC_ED_SCAN_RING_VALUE, strNewValue);
			WritePrivateProfileString(Section_LightValue_Scan, Key_LV_ch1_ScanRing, strNewValue,SYSTEM_PARAM_INI_PATH);
			m_btSetLightValue[0][0].SetCheck(true);
		break;
	case 12:
			SetDlgItemText(IDC_ED_SCAN_BACK_VALUE, strNewValue);
			WritePrivateProfileString(Section_LightValue_Scan, Key_LV_ch2_ScanBack, strNewValue,SYSTEM_PARAM_INI_PATH);
			m_btSetLightValue[0][1].SetCheck(true);
		break;
	case 13:
			SetDlgItemText(IDC_ED_SCAN_REFLECT_VALUE, strNewValue);
			WritePrivateProfileString(Section_LightValue_Scan, Key_LV_ch3_ScanReflect, strNewValue,SYSTEM_PARAM_INI_PATH);
			m_btSetLightValue[0][2].SetCheck(true);
		break;
	case 14:
			SetDlgItemText(IDC_ED_SCAN_RESERVE_VALUE, strNewValue);
			WritePrivateProfileString(Section_LightValue_Scan, Key_LV_ch4_Reserve, strNewValue,SYSTEM_PARAM_INI_PATH);
			m_btSetLightValue[0][3].SetCheck(true);
		break;
	case 21:
			SetDlgItemText(IDC_ED_SCAN_ALIGN_1_1_VALUE, strNewValue);
			WritePrivateProfileString(Section_LightValue_Align, Key_LV_ch1_Align1_1, strNewValue,SYSTEM_PARAM_INI_PATH);
			m_btSetLightValue[1][0].SetCheck(true);
		break;
	case 22:
			SetDlgItemText(IDC_ED_SCAN_ALIGN_1_2_VALUE, strNewValue);
			WritePrivateProfileString(Section_LightValue_Align, Key_LV_ch2_Align1_2, strNewValue,SYSTEM_PARAM_INI_PATH);
			m_btSetLightValue[1][1].SetCheck(true);
		break;
	case 23:
			SetDlgItemText(IDC_ED_SCAN_ALIGN_2_1_VALUE, strNewValue);
			WritePrivateProfileString(Section_LightValue_Align, Key_LV_ch3_Align2_1, strNewValue,SYSTEM_PARAM_INI_PATH);
			m_btSetLightValue[1][2].SetCheck(true);
		break;
	case 24:
			SetDlgItemText(IDC_ED_SCAN_ALIGN_2_2_VALUE, strNewValue);
			WritePrivateProfileString(Section_LightValue_Align, Key_LV_ch4_Align2_2, strNewValue,SYSTEM_PARAM_INI_PATH);
			m_btSetLightValue[1][3].SetCheck(true);
		break;
	}
	return 0;
}

LRESULT CAccCtrlDlg::OnUserUpdateLoadCell(WPARAM wParam, LPARAM lParam)
{
	int LoadCellID = (int)wParam;
	float fValue = (float)lParam;
	memcpy(&fValue, &lParam, sizeof(float));
	CString strNewValue;
	strNewValue.Format("%0.4f", fValue);
	switch(LoadCellID)
	{
	case 1:
		SetDlgItemText(IDC_ED_LOAD_CELL_VALUE1, strNewValue);
		break;
	case 2:
		SetDlgItemText(IDC_ED_LOAD_CELL_VALUE2, strNewValue);
		break;
	case 3:
		SetDlgItemText(IDC_ED_LOAD_CELL_VALUE3, strNewValue);
		break;
	case 4:
		SetDlgItemText(IDC_ED_LOAD_CELL_VALUE4, strNewValue);
		break;
	case 5:
		SetDlgItemText(IDC_ED_LOAD_CELL_VALUE5, strNewValue);
		break;
	case 6:
		SetDlgItemText(IDC_ED_LOAD_CELL_VALUE6, strNewValue);
		break;
	case 7:
		SetDlgItemText(IDC_ED_LOAD_CELL_VALUE7, strNewValue);
		break;
	case 8:
		SetDlgItemText(IDC_ED_LOAD_CELL_VALUE8, strNewValue);
		break;
	}
	return 0;
}
LRESULT CAccCtrlDlg::OnUserUpdateSpaceSensor(WPARAM wParam, LPARAM lParam)
{
	if(wParam == 9999999 && lParam== 9999999)
	{//Time Out
		CString strNewValue;
		strNewValue.Format("READ_TIME_OUT");
		SetDlgItemText(IDC_ED_SPACE_SENSER_OUT1, strNewValue);
		SetDlgItemText(IDC_ED_SPACE_SENSER_OUT2, strNewValue);
	}
	else
	{
		float fValueOut1 = 0.0f;
		float fValueOut2 = 0.0f;
		CString strNewValue;
		memcpy(&fValueOut1, &wParam, sizeof(float));
		strNewValue.Format("%0.4f", fValueOut1);
		SetDlgItemText(IDC_ED_SPACE_SENSER_OUT1, strNewValue);
		memcpy(&fValueOut2, &lParam, sizeof(float));
		strNewValue.Format("%0.4f", fValueOut2);
		SetDlgItemText(IDC_ED_SPACE_SENSER_OUT2, strNewValue);
	}
	return 0;
}
LRESULT CAccCtrlDlg::OnUserUpdateAir(WPARAM wParam, LPARAM lParam)
{
	CString strNewValue;
	strNewValue.Format("%d", (USHORT)lParam);
	switch((char)wParam)
	{
	case 0:
		strNewValue.Format("Read Error");
		SetDlgItemText(IDC_ED_PRESSURE_AIR, strNewValue);
		SetDlgItemText(IDC_ED_PRESSURE_N2, strNewValue);
		break;
	case '1':
		SetDlgItemText(IDC_ED_PRESSURE_AIR, strNewValue);
		break;
	case '2':
		SetDlgItemText(IDC_ED_PRESSURE_N2, strNewValue);
		break;
	}	
	return 0;
}
LRESULT CAccCtrlDlg::OnUserUpdateCode(WPARAM wParam, LPARAM lParam)
{
	CString strNewValue;
	
	switch(wParam)
	{
	case 1://QRCode Read
		{
			m_btQRCodeRead.SetCheck(true);
			if(lParam != 0)
			{
				strNewValue.Format("%s", (char*)lParam);
			}
			else//Time Out
			{
				strNewValue.Format("READ_TIME_OUT");
			}
			SetDlgItemText(IDC_ED_QR_CODE_VALUE, strNewValue);
		}break;
	case 2://Bar Code Read
		{
			if(lParam != 0)
			{
				strNewValue.Format("%s", (char*)lParam);
			}
			else//Time Out
			{
				strNewValue.Format("READ_TIME_OUT");
			}
			SetDlgItemText(IDC_ED_QR_CODE_VALUE, strNewValue);
		}break;
	default:
		break;
	}
	return 0;
}

#include "VS24MotionDlg.h"
void CAccCtrlDlg::OnBnClickedBtChangeSolView()
{
	CVS24MotionDlg* pMainWnd = (CVS24MotionDlg*)AfxGetApp()->m_pMainWnd;
	pMainWnd->ChangeView_ACC_SOL(2);
}





//void CAccCtrlDlg::OnEnChangeEdValue()
//{
//	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
//	// CDialogEx::OnInitDialog() 함수를 재지정 
//	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
//	// 이 알림 메시지를 보내지 않습니다.
//
//	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
//
//	CWnd *pwnd = GetFocus(); 
//	int wID = pwnd->GetDlgCtrlID();
//
//	switch(wID)
//	{
//	case IDC_ED_LOAD_CELL_VALUE1:
//		break;
//	case IDC_ED_LOAD_CELL_VALUE2:
//		break;
//	case IDC_ED_LOAD_CELL_VALUE3:
//		break;
//	case IDC_ED_LOAD_CELL_VALUE4:
//		break;
//	case IDC_ED_LOAD_CELL_VALUE5:
//		break;
//	case IDC_ED_LOAD_CELL_VALUE6:
//		break;
//	case IDC_ED_LOAD_CELL_VALUE7:
//		break;
//	case IDC_ED_LOAD_CELL_VALUE8:
//		break;
//	}
//}



