#include "StdAfx.h"
#include "MotionProcMsgQ.h"


//Motion Ctrl Obj Link
//extern CMotCtrl G_MotCtrlObj;
//////////////////////////////////////////////////////////////////////////
//Motion Action
//////////////////////////////////////////////////////////////////////////
int CMDFunList::fn_Seq_TT01_TableDrive(bool *pBreakProc, VS24MotData*pProcData)//Cruiser Axis #1
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnTT01Homing(AXIS_TT01_TableDrive);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_TT01_TableDrive);
			MotRunRet = G_MotCtrlObj.m_fnTT01AbsMove(AXIS_TT01_TableDrive,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnTT01Stop(AXIS_TT01_TableDrive);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.

		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"TT01_TableDrive Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_TT01_TableDrive] - pProcData->newPosition1);
				//AXIS_TT01_TableDrive://Cruiser Axis #1
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_LocalAxisST[0].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_TableDrive Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TT01_TableDrive Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TT01_TableDrive Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_21_TT01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_TT01_TableDrive));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_TableDrive Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TT01_TableDrive Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TT01_TableDrive Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"TT01_TableDrive Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_TT01_ThetaAlign(bool *pBreakProc, VS24MotData*pProcData)//ML3 #18
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnTT01Homing(AXIS_TT01_ThetaAlign);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_TT01_ThetaAlign);
			MotRunRet = G_MotCtrlObj.m_fnTT01AbsMove(AXIS_TT01_ThetaAlign,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnTT01Stop(AXIS_TT01_ThetaAlign);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"TT01_ThetaAlign Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_TT01_ThetaAlign] - pProcData->newPosition1);
				//AXIS_TT01_ThetaAlign://ML3 #18
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[8].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_ThetaAlign Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TT01_ThetaAlign Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TT01_ThetaAlign Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_21_TT01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_TT01_ThetaAlign));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_ThetaAlign Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TT01_ThetaAlign Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TT01_ThetaAlign Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"TT01_ThetaAlign Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_TT01_TableTurn(bool *pBreakProc, VS24MotData*pProcData)//ML3 #19
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnTT01Homing(AXIS_TT01_TableTurn);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_TT01_TableTurn);
			MotRunRet = G_MotCtrlObj.m_fnTT01AbsMove(AXIS_TT01_TableTurn,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnTT01Stop(AXIS_TT01_TableTurn);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"TT01_TableTurn Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_TT01_TableTurn] - pProcData->newPosition1);
				// AXIS_TT01_TableTurn://ML3 #19
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[9].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_TableTurn Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TT01_TableTurn Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TT01_TableTurn Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_21_TT01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_TT01_TableTurn));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_TableTurn Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TT01_TableTurn Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TT01_TableTurn Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"TT01_TableTurn Send CMD Error");
	}
	return 1;
}
//BT01
int CMDFunList::fn_Seq_BT01_CassetteUpDown(bool *pBreakProc, VS24MotData*pProcData)//ML3 #22
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnBT01Homing(AXIS_BT01_CassetteUpDown);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_BT01_CassetteUpDown);
			MotRunRet = G_MotCtrlObj.m_fnBT01AbsMove(AXIS_BT01_CassetteUpDown,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnBT01Stop(AXIS_BT01_CassetteUpDown);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"BT01_CassetteUpDown Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_BT01_CassetteUpDown] - pProcData->newPosition1);
				//AXIS_BT01_CassetteUpDown://ML3 #22
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[12].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BT01_CassetteUpDown Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"BT01_CassetteUpDown Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"BT01_CassetteUpDown Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_22_BT01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_BT01_CassetteUpDown));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BT01_CassetteUpDown Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"BT01_CassetteUpDown Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"BT01_CassetteUpDown Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"BT01_CassetteUpDown Send CMD Error");
	}
	return 1;
}
//LT01
int CMDFunList::fn_Seq_LT01_BoxLoadPush(bool *pBreakProc, VS24MotData*pProcData)//ML3 #14
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnLT01Homing(AXIS_LT01_BoxLoadPush);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_LT01_BoxLoadPush);
			MotRunRet = G_MotCtrlObj.m_fnLT01AbsMove(AXIS_LT01_BoxLoadPush,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnLT01Stop(AXIS_LT01_BoxLoadPush);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"LT01_BoxLoadPush Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_LT01_BoxLoadPush] - pProcData->newPosition1);
				//AXIS_LT01_BoxLoadPush://ML3 #14
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[4].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"LT01_BoxLoadPush Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"LT01_BoxLoadPush Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"LT01_BoxLoadPush Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_27_LT01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_LT01_BoxLoadPush));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"LT01_BoxLoadPush Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"LT01_BoxLoadPush Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"LT01_BoxLoadPush Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"LT01_BoxLoadPush Send CMD Error");
	}
	return 1;
}
//BO01
int CMDFunList::fn_Seq_BO01_BoxRotate(bool *pBreakProc, VS24MotData*pProcData)//ML3 #10, #11 //좌우 동기축.
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnBO01Homing(AXIS_BO01_BoxRotate);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_BO01_BoxRotate);
			MotRunRet = G_MotCtrlObj.m_fnBO01AbsMove(AXIS_BO01_BoxRotate,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnBO01Stop(AXIS_BO01_BoxRotate);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"BO01_BoxRotate Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_BO01_BoxRotate] - pProcData->newPosition1);
				//AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
				if(InpositionValue<=AXIS_INPOS_CTS && 
					G_MotCtrlObj.m_ML3AxisST[0].m_AxisStB.St00_InPosition == 1 &&
					G_MotCtrlObj.m_ML3AxisST[1].m_AxisStB.St00_InPosition == 1 )
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BO01_BoxRotate Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"BO01_BoxRotate Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"BO01_BoxRotate Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotCompletL, strMotCompletR;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_23_BO01_Home));
				strMotCompletL.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_BO01_BoxRotateL));
				strMotCompletR.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_BO01_BoxRotateR));
				if(strPLCRun == '4' && strMotCompletL == '1' && strMotCompletR == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BO01_BoxRotate Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"BO01_BoxRotate Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"BO01_BoxRotate Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"BO01_BoxRotate Send CMD Error");
	}
	return 1;
}	

int CMDFunList::fn_Seq_BO01_BoxUpDn(bool *pBreakProc, VS24MotData*pProcData)//ML3 #12, #13 //좌우 동기축.
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnBO01Homing(AXIS_BO01_BoxUpDn);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_BO01_BoxUpDn);
			MotRunRet = G_MotCtrlObj.m_fnBO01AbsMove(AXIS_BO01_BoxUpDn,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnBO01Stop(AXIS_BO01_BoxUpDn);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"BO01_BoxUpDn Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_BO01_BoxUpDn] - pProcData->newPosition1);
				//AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
				if(InpositionValue<=AXIS_INPOS_CTS && 
					G_MotCtrlObj.m_ML3AxisST[2].m_AxisStB.St00_InPosition == 1 &&
					G_MotCtrlObj.m_ML3AxisST[3].m_AxisStB.St00_InPosition == 1 )
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BO01_BoxUpDn Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"BO01_BoxUpDn Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"BO01_BoxUpDn Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotCompletL, strMotCompletR;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_23_BO01_Home));
				strMotCompletL.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_BO01_BoxUpDnL));
				strMotCompletR.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_BO01_BoxUpDnR));
				if(strPLCRun == '4' && strMotCompletL == '1' && strMotCompletR == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BO01_BoxUpDn Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"BO01_BoxUpDn Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"BO01_BoxUpDn Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"BO01_BoxUpDn Send CMD Error");
	}
	return 1;
}			
int CMDFunList::fn_Seq_BO01_BoxDrive(bool *pBreakProc, VS24MotData*pProcData)//ML3 #16
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnBO01Homing(AXIS_BO01_BoxDrive);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_BO01_BoxDrive);
			MotRunRet = G_MotCtrlObj.m_fnBO01AbsMove(AXIS_BO01_BoxDrive,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnBO01Stop(AXIS_BO01_BoxDrive);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"BO01_BoxDrive Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_BO01_BoxDrive] - pProcData->newPosition1);
				//AXIS_BO01_BoxDrive://ML3 #16
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[6].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BO01_BoxDrive Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"BO01_BoxDrive Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"BO01_BoxDrive Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_23_BO01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_BO01_BoxDrive));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BO01_BoxDrive Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"BO01_BoxDrive Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"BO01_BoxDrive Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"BO01_BoxDrive Send CMD Error");
	}
	return 1;
}		
//UT01
int CMDFunList::fn_Seq_UT01_BoxUnLoadPush(bool *pBreakProc, VS24MotData*pProcData)//ML3 #15
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnUT01Homing(AXIS_UT01_BoxUnLoadPush);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_UT01_BoxUnLoadPush);
			MotRunRet = G_MotCtrlObj.m_fnUT01AbsMove(AXIS_UT01_BoxUnLoadPush,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnUT01Stop(AXIS_UT01_BoxUnLoadPush);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"UT01_BoxUnLoadPush Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_UT01_BoxUnLoadPush] - pProcData->newPosition1);
				//AXIS_UT01_BoxUnLoadPush://ML3 #15
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[5].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"UT01_BoxUnLoadPush Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"UT01_BoxUnLoadPush Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"UT01_BoxUnLoadPush Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_28_UT01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_UT01_BoxUnLoadPush));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"UT01_BoxUnLoadPush Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"UT01_BoxUnLoadPush Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"UT01_BoxUnLoadPush Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"UT01_BoxUnLoadPush Send CMD Error");
	}
	return 1;
}
//TM01
int CMDFunList::fn_Seq_TM01_ForwardBackward(bool *pBreakProc, VS24MotData*pProcData)//ML3 #17
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM01Homing(AXIS_TM01_ForwardBackward);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_TM01_ForwardBackward);
			MotRunRet = G_MotCtrlObj.m_fnTM01AbsMove(AXIS_TM01_ForwardBackward,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM01Stop(AXIS_TM01_ForwardBackward);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"TM01_ForwardBackward Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_TM01_ForwardBackward] - pProcData->newPosition1);
				//AXIS_TM01_ForwardBackward://ML3 #17
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[7].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM01_ForwardBackward Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM01_ForwardBackward Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM01_ForwardBackward Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_24_TM01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_TM01_ForwardBackward));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM01_ForwardBackward Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM01_ForwardBackward Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM01_ForwardBackward Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"TM01_ForwardBackward Send CMD Error");
	}
	return 1;
}

//TM02
int CMDFunList::fn_Seq_TM02_PickerDrive(bool *pBreakProc, VS24MotData*pProcData)//Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM02Homing(AXIS_TM02_PickerDrive);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_TM02_PickerDrive);
			MotRunRet = G_MotCtrlObj.m_fnTM02AbsMove(AXIS_TM02_PickerDrive,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM02Stop(AXIS_TM02_PickerDrive);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"TM02_PickerDrive Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 1000*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_TM02_PickerDrive] - pProcData->newPosition1);

				//AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
				if(InpositionValue<=AXIS_INPOS_CTS && 
					G_MotCtrlObj.m_LocalAxisST[6].m_AxisStB.St00_InPosition == 1 &&
					G_MotCtrlObj.m_LocalAxisST[7].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PickerDrive Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM02_PickerDrive Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM02_PickerDrive Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 100*10;//100Sec
			CString strPLCRun, strMotComplet, strCheckPLC;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_25_TM02_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_TM02_PickerDrive));
				strCheckPLC.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(AXIS_GANTRY_PLC8));

				if(strPLCRun == '4' )//&& strMotComplet == '1' )
				{//정상 완료.
					//if(atoi(strCheckPLC) == 1234)
					//{
					//	pProcData->nResult = VS24MotData::Ret_Ok;
					//	G_WriteInfo(Log_Info,"TM02_PickerDrive Act_Home Ok");
					//	break;
					//}
					//else
					//{
					//	pProcData->nResult = VS24MotData::Ret_Ok;
					//	G_WriteInfo(Log_Worrying,"TM02_PickerDrive Act_Home Fail");
					//}
					G_WriteInfo(Log_Info,"TM02_PickerDrive Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM02_PickerDrive Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM02_PickerDrive Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"TM02_PickerDrive Send CMD Error");
	}
	return 1;
}	
int CMDFunList::fn_Seq_TM02_PickerUpDown(bool *pBreakProc, VS24MotData*pProcData)//ML3 #20
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM02Homing(AXIS_TM02_PickerUpDown);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_TM02_PickerUpDown);
			MotRunRet = G_MotCtrlObj.m_fnTM02AbsMove(AXIS_TM02_PickerUpDown,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM02Stop(AXIS_TM02_PickerUpDown);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"TM02_PickerUpDown Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 30*100;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_TM02_PickerUpDown] - pProcData->newPosition1);
				//AXIS_TM02_PickerUpDown://ML3 #20
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[10].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PickerUpDown Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM02_PickerUpDown Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM02_PickerUpDown Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_25_TM02_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_TM02_PickerUpDown));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PickerUpDown Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM02_PickerUpDown Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM02_PickerUpDown Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"TM02_PickerUpDown Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM02_PickerShift(bool *pBreakProc, VS24MotData*pProcData)//ML3 #21
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM02Homing(AXIS_TM02_PickerShift);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_TM02_PickerShift);
			MotRunRet = G_MotCtrlObj.m_fnTM02AbsMove(AXIS_TM02_PickerShift,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM02Stop(AXIS_TM02_PickerShift);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"TM02_PickerShift Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_TM02_PickerShift] - pProcData->newPosition1);
				//AXIS_TM02_PickerShift://ML3 #21
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[11].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PickerShift Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM02_PickerShift Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM02_PickerShift Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_25_TM02_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_TM02_PickerShift));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PickerShift Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM02_PickerShift Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM02_PickerShift Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"TM02_PickerShift Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM02_PickerRotate(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_A Y
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM02Homing(AXIS_TM02_PickerRotate);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_TM02_PickerRotate);
			MotRunRet = G_MotCtrlObj.m_fnTM02AbsMove(AXIS_TM02_PickerRotate,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnTM02Stop(AXIS_TM02_PickerRotate);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"TM02_PickerRotate Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_TM02_PickerRotate] - pProcData->newPosition1);
				//AXIS_TM02_PickerRotate://AxisLink_A Y
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[1].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PickerRotate Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM02_PickerRotate Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM02_PickerRotate Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_25_TM02_Home));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PickerRotate Act_Home Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"TM02_PickerRotate Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"TM02_PickerRotate Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"TM02_PickerRotate Send CMD Error");
	}
	return 1;
}

//AM01
int CMDFunList::fn_Seq_AM01_ScanDrive(bool *pBreakProc, VS24MotData*pProcData)//Cruiser Axis #6
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_ScanDrive);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScanDrive);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_ScanDrive,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_ScanDrive);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_ScanDrive Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScanDrive] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_LocalAxisST[5].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScanDrive Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScanDrive Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScanDrive Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_AM01_ScanDrive));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScanDrive Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScanDrive Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScanDrive Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_ScanDrive Send CMD Error");
	}
	return 1;
}		
int CMDFunList::fn_Seq_AM01_ScanShift(bool *pBreakProc, VS24MotData*pProcData)//Cruiser Axis #2
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_ScanShift);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScanShift);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_ScanShift,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_ScanShift);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_ScanShift Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScanShift] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_LocalAxisST[1].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScanShift Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScanShift Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScanShift Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_AM01_ScanShift));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScanShift Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScanShift Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScanShift Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_ScanShift Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_ScanUpDn(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_D X
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_ScanUpDn);
		}break;
	case VS24MotData::Act_Move:
		{
			int LensChtimer = 500;
			while(G_MotCtrlObj.m_bReviewLensOffsetApply == true)
			{
				LensChtimer--;
				if(LensChtimer <=0)
					break;
				Sleep(10);
			}
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScanUpDn);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_ScanUpDn,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_ScanUpDn);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_ScanUpDn Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScanUpDn] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[12].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScanUpDn Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScanUpDn Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScanUpDn Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScanUpDn Act_Home Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScanUpDn Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScanUpDn Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_ScanUpDn Send CMD Error");
	}
	return 1;
}	
int CMDFunList::fn_Seq_AM01_SpaceSnUpDn(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_D Y
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_SpaceSnUpDn);
		}break;
	case VS24MotData::Act_Move:
		{
			int LensChtimer = 500;
			while(G_MotCtrlObj.m_bReviewLensOffsetApply == true)
			{
				LensChtimer--;
				if(LensChtimer <=0)
					break;
				Sleep(10);
			}
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_SpaceSnUpDn);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_SpaceSnUpDn,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_SpaceSnUpDn);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_SpaceSnUpDn Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_SpaceSnUpDn] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[13].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_SpaceSnUpDn Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_SpaceSnUpDn Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_SpaceSnUpDn Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_SpaceSnUpDn Act_Home Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_SpaceSnUpDn Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_SpaceSnUpDn Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_SpaceSnUpDn Send CMD Error");
	}
	return 1;
}

int CMDFunList::fn_Seq_AM01_ScopeDrive(bool *pBreakProc, VS24MotData*pProcData)//Cruiser Axis #5
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_ScopeDrive);
		}break;
	case VS24MotData::Act_Move:
		{
			int LensChtimer = 500;
			while(G_MotCtrlObj.m_bScopeLensOffsetApply == true)
			{
				LensChtimer--;
				if(LensChtimer <=0)
					break;
				Sleep(10);
			}
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScopeDrive);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_ScopeDrive,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_ScopeDrive);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_ScopeDrive Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScopeDrive] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_LocalAxisST[4].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScopeDrive Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScopeDrive Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScopeDrive Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_AM01_ScopeDrive));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScopeDrive Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScopeDrive Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScopeDrive Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_ScopeDrive Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_ScopeShift(bool *pBreakProc, VS24MotData*pProcData)//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_ScopeShift);
		}break;
	case VS24MotData::Act_Move:
		{
			int LensChtimer = 500;
			while(G_MotCtrlObj.m_bScopeLensOffsetApply == true)
			{
				LensChtimer--;
				if(LensChtimer <=0)
					break;
				Sleep(10);
			}
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScopeShift);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_ScopeShift,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_ScopeShift);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_ScopeShift Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScopeShift] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && 
					G_MotCtrlObj.m_LocalAxisST[2].m_AxisStB.St00_InPosition == 1 &&
					G_MotCtrlObj.m_LocalAxisST[3].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScopeShift Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScopeShift Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScopeShift Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet, strCheckPLC;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_AM01_ScopeShift));
				strCheckPLC.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(AXIS_GANTRY_PLC7));

				if(strPLCRun == '4' && strMotComplet == '1' )
				{//정상 완료.
					if(atoi(strCheckPLC) == 1234)
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						G_WriteInfo(Log_Info,"AM01_ScopeShift Act_Home Ok");
						break;
					}
					else
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						G_WriteInfo(Log_Info,"AM01_ScopeShift Act_Home Fail");
					}

					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScopeShift Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScopeShift Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_ScopeShift Send CMD Error");
	}
	return 1;
}	
int CMDFunList::fn_Seq_AM01_ScopeUpDn(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_D Z
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_ScopeUpDn);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScopeUpDn);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_ScopeUpDn,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_ScopeUpDn);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_ScopeUpDn Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScopeUpDn] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[14].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScopeUpDn Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScopeUpDn Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScopeUpDn Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_ScopeUpDn Act_Home Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_ScopeUpDn Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_ScopeUpDn Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_ScopeUpDn Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_AirBlowLeftRight(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_D U
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_AirBlowLeftRight);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_AirBlowLeftRight);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_AirBlowLeftRight,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_AirBlowLeftRight);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_AirBlowLeftRight Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_AirBlowLeftRight] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[15].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_AirBlowLeftRight Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_AirBlowLeftRight Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_AirBlowLeftRight Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_AirBlowLeftRight Act_Home Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_AirBlowLeftRight Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_AirBlowLeftRight Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_AirBlowLeftRight Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_ScanMultiMove(bool *pBreakProc, VS24MotData*pProcData)//Cruiser Axis #6//Cruiser Axis #2
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
		}break;
	case VS24MotData::Act_Move:
		{
			int LensChtimer = 500;
			while(G_MotCtrlObj.m_bReviewLensOffsetApply == true)
			{
				LensChtimer--;
				if(LensChtimer <=0)
					break;
				Sleep(10);
			}
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScanDrive);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScanShift);
			MotRunRet = G_MotCtrlObj.m_fnAM01ScanMultiMoveXY((long)pProcData->newPosition1, (long)pProcData->newPosition2);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01ScanMultiStopXY();
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"fn_Seq_AM01_ScanMultiMove Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue1 = AXIS_INPOS_CTS;
			int InpositionValue2 = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue1 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScanShift] - pProcData->newPosition1);
				InpositionValue2 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScanDrive] - pProcData->newPosition2);
				if(InpositionValue1<=AXIS_INPOS_CTS && InpositionValue2<=AXIS_INPOS_CTS &&
					G_MotCtrlObj.m_LocalAxisST[1].m_AxisStB.St00_InPosition == 1 &&
					G_MotCtrlObj.m_LocalAxisST[5].m_AxisStB.St00_InPosition == 1 )
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"fn_Seq_AM01_ScanMultiMove Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"fn_Seq_AM01_ScanMultiMove Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"fn_Seq_AM01_ScanMultiMove Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"fn_Seq_AM01_ScanMultiMove Send CMD Error(Act_Home)");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"fn_Seq_AM01_ScanMultiMove Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_ScopeMultiMove(bool *pBreakProc, VS24MotData*pProcData)//Cruiser Axis #5//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
		}break;
	case VS24MotData::Act_Move:
		{
			int LensChtimer = 500;
			while(G_MotCtrlObj.m_bScopeLensOffsetApply == true)
			{
				LensChtimer--;
				if(LensChtimer <=0)
					break;
				Sleep(10);
			}

			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScopeShift);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScopeDrive);
			MotRunRet = G_MotCtrlObj.m_fnAM01ScopeMultiMoveXY((long)pProcData->newPosition1, (long)pProcData->newPosition2);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01ScopeMultiStopXY();
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"fn_Seq_AM01_ScopeMultiMove Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;
			//30Sec
			int InpositionValue1 = AXIS_INPOS_CTS;
			int InpositionValue2 = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue1 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScopeShift] - pProcData->newPosition1);
				InpositionValue2 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScopeDrive] - pProcData->newPosition2);
				if(InpositionValue1<=AXIS_INPOS_CTS && InpositionValue2<=AXIS_INPOS_CTS &&
					G_MotCtrlObj.m_LocalAxisST[2].m_AxisStB.St00_InPosition == 1 &&
					G_MotCtrlObj.m_LocalAxisST[3].m_AxisStB.St00_InPosition == 1 &&
					G_MotCtrlObj.m_LocalAxisST[4].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"fn_Seq_AM01_ScopeMultiMove Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"fn_Seq_AM01_ScopeMultiMove Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"fn_Seq_AM01_ScopeMultiMove Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"fn_Seq_AM01_ScopeMultiMove Send CMD Error(Act_Home)");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"fn_Seq_AM01_ScopeMultiMove Send CMD Error");
	}
	return 1;
}

int CMDFunList::fn_Seq_AM01_SetReviewLensOffset(bool *pBreakProc, VS24MotData*pProcData)
{
	bool MotRunRet = false;
	//Offset 적용 체크용
	long OffsetSettingPosShift = 0;
	long OffsetSettingPosDrive = 0;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
		}break;
	case VS24MotData::Act_Move:
		{
			G_WriteInfo(Log_Check,"SetReviewLensOffset Old:%d, %d ==>New:%d, %d ",
				G_MotCtrlObj.m_ReviewLensOffsetShift, G_MotCtrlObj.m_ReviewLensOffsetDrive,
				pProcData->newPosition1, pProcData->newPosition2);

			//이전 Offset을 빼서 기준 위치로 가고
			//3, 4는 현재 위치에서 적용 위치가 들어온다.
			G_MotCtrlObj.m_ReviewLensOffsetShiftApply_Value = G_MotCtrlObj.m_ReviewLensOffsetShift;
			G_MotCtrlObj.m_ReviewLensOffsetDriveApply_Value = G_MotCtrlObj.m_ReviewLensOffsetDrive;
			//1, 2는 Offset값이 들어오고 
			G_MotCtrlObj.m_ReviewLensOffsetShift = pProcData->newPosition1;
			G_MotCtrlObj.m_ReviewLensOffsetDrive = pProcData->newPosition2;
			G_MotCtrlObj.m_bReviewLensOffsetApply = true;//Thread에서 이것을 읽고 모션 중지 상태에서 Apply Value를 적용 한다.
			MotRunRet = true;
		}break;
	case VS24MotData::Act_Stop:
		{
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		if(pProcData->nAction == VS24MotData::Act_Move)
		{
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"SetReviewLensOffset Act_Move Ok");
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"SetReviewLensOffset Send CMD Error(Act_Home)");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"SetReviewLensOffset Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_SetScopeLensOffset(bool *pBreakProc, VS24MotData*pProcData)
{
	bool MotRunRet = false;
	//Offset 적용 체크용
	long OffsetSettingPosShift = 0;
	long OffsetSettingPosDrive = 0;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
		}break;
	case VS24MotData::Act_Move:
		{

			G_WriteInfo(Log_Check,"SetScopeLensOffset Old:%d, %d ==>New:%d, %d ",
				G_MotCtrlObj.m_ScopeLensOffsetShift, G_MotCtrlObj.m_ScopeLensOffsetDrive,
				pProcData->newPosition1, pProcData->newPosition2);
			//이전 Offset을 빼서 기준 위치로 가고
			//3, 4는 현재 위치에서 적용 위치가 들어온다.
			G_MotCtrlObj.m_ScopeLensOffsetShiftApply_Value = G_MotCtrlObj.m_ScopeLensOffsetShift;
			G_MotCtrlObj.m_ScopeLensOffsetDriveApply_Value = G_MotCtrlObj.m_ScopeLensOffsetDrive;
			//1, 2는 Offset값이 들어오고 
			G_MotCtrlObj.m_ScopeLensOffsetShift = pProcData->newPosition1;
			G_MotCtrlObj.m_ScopeLensOffsetDrive = pProcData->newPosition2;
			G_MotCtrlObj.m_bScopeLensOffsetApply = true;//Thread에서 이것을 읽고 모션 중지 상태에서 Apply Value를 적용 한다.
			MotRunRet = true;
		}break;
	case VS24MotData::Act_Stop:
		{
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		if(pProcData->nAction == VS24MotData::Act_Move)
		{
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"SetScopeLensOffset Act_Move Ok");
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"SetScopeLensOffset Send CMD Error(Act_Home)");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"SetScopeLensOffset Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_ScanScopeSyncMove(bool *pBreakProc, VS24MotData*pProcData)
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
		}break;
	case VS24MotData::Act_Move:
		{
			int LensChtimer = 500;
			while(G_MotCtrlObj.m_bReviewLensOffsetApply == true ||G_MotCtrlObj.m_bScopeLensOffsetApply == true)
			{
				LensChtimer--;
				if(LensChtimer <=0)
					break;
				Sleep(10);
			}

			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScopeShift);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScopeDrive);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScanShift);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_ScanDrive);
			MotRunRet = G_MotCtrlObj.m_fnAM01SyncMoveXY((long)pProcData->newPosition1, (long)pProcData->newPosition2,(long)pProcData->newPosition3, (long)pProcData->newPosition4);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01SyncStopXY();
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"fn_Seq_AM01_ScanScopeSyncMove Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue1 = AXIS_INPOS_CTS;
			int InpositionValue2 = AXIS_INPOS_CTS;
			int InpositionValue3 = AXIS_INPOS_CTS;
			int InpositionValue4 = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue1 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScanShift] - pProcData->newPosition1);
				InpositionValue2 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScanDrive] - pProcData->newPosition2);
				InpositionValue3 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScopeShift] - pProcData->newPosition3);
				InpositionValue4 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScopeDrive] - pProcData->newPosition4);
				if(InpositionValue1<=AXIS_INPOS_CTS && InpositionValue2<=AXIS_INPOS_CTS &&
					InpositionValue3<=AXIS_INPOS_CTS && InpositionValue4<=AXIS_INPOS_CTS &&
					G_MotCtrlObj.m_LocalAxisST[2].m_AxisStB.St00_InPosition == 1 &&//Scope Shift;
					G_MotCtrlObj.m_LocalAxisST[3].m_AxisStB.St00_InPosition == 1 &&//Scope Shift;

					G_MotCtrlObj.m_LocalAxisST[4].m_AxisStB.St00_InPosition == 1 &&//Scope Drive;

					G_MotCtrlObj.m_LocalAxisST[1].m_AxisStB.St00_InPosition == 1 &&//Scan Shift;
					G_MotCtrlObj.m_LocalAxisST[5].m_AxisStB.St00_InPosition == 1)//Scan Drive;
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"fn_Seq_AM01_ScanScopeSyncMove Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"fn_Seq_AM01_ScanScopeSyncMove Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"fn_Seq_AM01_ScanScopeSyncMove Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"fn_Seq_AM01_ScanScopeSyncMove Send CMD Error(Act_Home)");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"fn_Seq_AM01_ScanScopeSyncMove Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_ScopeSpaceZSyncMove(bool *pBreakProc, VS24MotData*pProcData)
{
	bool MotRunRet = false;
	CString UmacCMD;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
		}break;
	case VS24MotData::Act_Move:
		{
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_Z_NewPOS, pProcData->newPosition1);//AXIS_AM01_ScopeUpDn://AxisLink_D Z
			G_MotCtrlObj.m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_Y_NewPOS, pProcData->newPosition2);//AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
			G_MotCtrlObj.m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x02|0x4);
			G_MotCtrlObj.m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("ena plc 18");
			G_MotCtrlObj.m_fnUmacOnlineCommand(UmacCMD);
			MotRunRet = true;
		}break;
	case VS24MotData::Act_Stop:
		{
			
			UmacCMD.Format("%s=%d", AL_MOT_BLOCK_D_SelectAxis, 0x02|0x4);
			G_MotCtrlObj.m_fnUmacOnlineCommand(UmacCMD);
			UmacCMD.Format("%s=1", AL_MOT_BLOCK_D_SSTOP);
			G_MotCtrlObj.m_fnUmacOnlineCommand(UmacCMD);
			MotRunRet = true;
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"fn_Seq_AM01_ScopeSpaceZSyncMove Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue1 = AXIS_INPOS_CTS;
			int InpositionValue2 = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue1 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_ScopeUpDn] - pProcData->newPosition1);
				InpositionValue2 = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_SpaceSnUpDn] - pProcData->newPosition2);
				if(InpositionValue1<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[14].St06_SINP == 1 &&
				   InpositionValue2<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[13].St06_SINP == 1 )
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"fn_Seq_AM01_ScopeSpaceZSyncMove Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"fn_Seq_AM01_ScopeSpaceZSyncMove Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"fn_Seq_AM01_ScopeSpaceZSyncMove Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"fn_Seq_AM01_ScopeSpaceZSyncMove Send CMD Error(Act_Home)");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"fn_Seq_AM01_ScopeSpaceZSyncMove Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_TensionUpDn(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_A Z
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_TensionLeftUpDn);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_TensionLeftUpDn);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_TensionRightUpDn);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_TensionLeftUpDn,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_TensionLeftUpDn);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_TensionLeftUpDn Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_TensionLeftUpDn] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[2].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_TensionLeftUpDn Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_TensionLeftUpDn Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_TensionLeftUpDn Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_TensionLeftUpDn Act_Home Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_TensionLeftUpDn Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_TensionLeftUpDn Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_TensionLeftUpDn Send CMD Error");
	}
	return 1;
}
//int CMDFunList::fn_Seq_AM01_TensionRightUpDn(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_A U
//{
//	bool MotRunRet = false;
//	switch(pProcData->nAction)
//	{
//	case VS24MotData::Act_None:
//		break;
//	case VS24MotData::Act_Home:
//		{
//			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_TensionRightUpDn);
//		}break;
//	case VS24MotData::Act_Move:
//		{
//			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_TensionRightUpDn);
//			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_TensionRightUpDn,(long)pProcData->newPosition1);
//		}break;
//	case VS24MotData::Act_Stop:
//		{
//			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_TensionRightUpDn);
//		}break;
//	default:
//		break;
//	}
//	if(MotRunRet == true)
//	{
//		int TimeOutValue = 0;
//		//////////////////////////////////////////////////////////////////////////
//		//동작 완료 체크.
//		if(pProcData->nAction == VS24MotData::Act_Stop)
//		{//체크 항목이 있나?
//			pProcData->nResult = VS24MotData::Ret_Ok;
//			G_WriteInfo(Log_Info,"AM01_TensionRightUpDn Act_Stop Ok");
//		}
//		else if(pProcData->nAction == VS24MotData::Act_Move)
//		{
//			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
//			//TimeOutValue = 300*10;//30Sec
//			int InpositionValue = AXIS_INPOS_CTS;
//			Sleep(100);//기본동식 시간을 준다.
//			while(*pBreakProc == true)
//			{
//				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_TensionRightUpDn] - pProcData->newPosition1);
//				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[3].St06_SINP == 1)
//				{//Inposition 안에 들어옴.(3um)
//
//					pProcData->nResult = VS24MotData::Ret_Ok;
//					G_WriteInfo(Log_Info,"AM01_TensionRightUpDn Act_Move Ok");
//					break;
//				}
//				TimeOutValue--;
//				if(TimeOutValue<0)
//				{//1분 Time Out.
//					pProcData->nResult = VS24MotData::Ret_TimeOut;
//					G_WriteInfo(Log_Worrying,"AM01_TensionRightUpDn Act_Move Time Out");
//					break;
//				}
//				Sleep(10);
//			}
//			if(*pBreakProc == false)
//			{
//				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
//				G_WriteInfo(Log_Worrying,"AM01_TensionRightUpDn Act_Move Run Stop");
//			}
//		}
//		else if(pProcData->nAction == VS24MotData::Act_Home)
//		{
//			TimeOutValue = 300*10;//30Sec
//			CString strPLCRun;
//			Sleep(100);//기본PLC 동식 시간을 준다.
//			while(*pBreakProc == true)
//			{
//				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
//				if(strPLCRun == '4')
//				{//정상 완료.
//					pProcData->nResult = VS24MotData::Ret_Ok;
//					G_WriteInfo(Log_Info,"AM01_TensionRightUpDn Act_Home Ok");
//					break;
//				}
//				TimeOutValue--;
//				if(TimeOutValue<0)
//				{//1분 Time Out.
//					pProcData->nResult = VS24MotData::Ret_TimeOut;
//					G_WriteInfo(Log_Worrying,"AM01_TensionRightUpDn Act_Move Time Out");
//					break;
//				}
//				Sleep(100);
//			}
//			if(*pBreakProc == false)
//			{
//				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
//				G_WriteInfo(Log_Worrying,"AM01_TensionRightUpDn Act_Move Run Stop");
//			}
//		}
//	}
//	else
//	{
//		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
//		G_WriteInfo(Log_Error,"AM01_TensionRightUpDn Send CMD Error");
//	}
//	return 1;
//}


int CMDFunList::fn_Seq_AM01_TensionStart(bool *pBreakProc, VS24MotData*pProcData)
{
	bool MotRunRet = false;
	float fTargetValue = 0.0f;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		{
		}break;
	case VS24MotData::Act_Home:
		{
		}break;
	case VS24MotData::Act_Move:
		{
			fTargetValue = ((float)pProcData->newPosition1/1000.0f);//정수 임으로 1000단위 실수로
			MotRunRet = G_MotCtrlObj.m_fnAM01TensionStart(pProcData->SelectPoint, fTargetValue);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01TensionStop();
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_TensionStart Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_04_STICK_TENSION));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_TensionStart Tension End");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_TensionStart Tension Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_MotCtrlObj.m_fnAM01TensionStop();
				G_WriteInfo(Log_Worrying,"AM01_TensionStart Act_Move Run Stop");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			G_MotCtrlObj.m_fnAM01TensionStop();
			G_WriteInfo(Log_Worrying,"AM01_TensionStart Act_Move None Proc");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_MotCtrlObj.m_fnAM01TensionStop();
		G_WriteInfo(Log_Error,"AM01_TensionStart Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_SpaceMeasureStart(bool *pBreakProc, VS24MotData*pProcData)
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		{
		}break;
	case VS24MotData::Act_Home:
		{
		}break;
	case VS24MotData::Act_Move:
		{
			float fSpaceValue = ((float)pProcData->newPosition1/1000.0f);
			MotRunRet = G_MotCtrlObj.m_fnAM01SpaceMeasureStart(fSpaceValue);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01SpaceMeasureStop();
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_SpaceMeasureStart Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_06_SPACE_MEASURE));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_SpaceMeasureStart Measur End");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_SpaceMeasureStart Measur Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_MotCtrlObj.m_fnAM01TensionStop();
				G_WriteInfo(Log_Worrying,"AM01_SpaceMeasureStart Act_Move Run Stop");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			G_MotCtrlObj.m_fnAM01TensionStop();
			G_WriteInfo(Log_Worrying,"AM01_SpaceMeasureStart Act_Move None Proc");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_MotCtrlObj.m_fnAM01TensionStop();
		G_WriteInfo(Log_Error,"AM01_SpaceMeasureStart Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_LGripper1(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_B X
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_LTension1);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_LTension1);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_LTension1,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_LTension1);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_LGripper1 Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_LTension1] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[4].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_LGripper1 Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_LGripper1 Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_LGripper1 Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_LGripper1 Act_Home Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_LGripper1 Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_LGripper1 Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_LGripper1 Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_LGripper2(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_B Y
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			//MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_LTension2);
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"AM01_LGripper2 Home CMD Only AM01_LGripper1");
			MotRunRet = false;
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_LTension2);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_LTension2,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_LTension2);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_LGripper2 Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_LTension2] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[5].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_LGripper2 Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_LGripper2 Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_LGripper2 Act_Move Run Stop");
			}
		}
		//else if(pProcData->nAction == VS24MotData::Act_Home)
		//{
		//	TimeOutValue = 300*10;//30Sec
		//	CString strPLCRun;
		//	Sleep(100);//기본PLC 동식 시간을 준다.
		//	while(*pBreakProc == true)
		//	{
		//		strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
		//		if(strPLCRun == '4')
		//		{//정상 완료.
		//			pProcData->nResult = VS24MotData::Ret_Ok;
		//			G_WriteInfo(Log_Info,"AM01_LGripper2 Act_Home Ok");
		//			break;
		//		}
		//		TimeOutValue--;
		//		if(TimeOutValue<0)
		//		{//1분 Time Out.
		//			pProcData->nResult = VS24MotData::Ret_TimeOut;
		//			G_WriteInfo(Log_Worrying,"AM01_LGripper2 Act_Move Time Out");
		//			break;
		//		}
		//		Sleep(100);
		//	}
		//	if(*pBreakProc == false)
		//	{
		//		pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
		//		G_WriteInfo(Log_Worrying,"AM01_LGripper2 Act_Move Run Stop");
		//	}
		//}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_LGripper2 Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_LGripper3(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_B Z
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			//MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_LTension3);
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"AM01_LGripper3 Home CMD Only AM01_LGripper1");
			MotRunRet = false;
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_LTension3);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_LTension3,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_LTension3);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_LGripper3 Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_LTension3] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[6].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_LGripper3 Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_LGripper3 Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_LGripper3 Act_Move Run Stop");
			}
		}
		//else if(pProcData->nAction == VS24MotData::Act_Home)
		//{
		//	TimeOutValue = 300*10;//30Sec
		//	CString strPLCRun;
		//	Sleep(100);//기본PLC 동식 시간을 준다.
		//	while(*pBreakProc == true)
		//	{
		//		strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
		//		if(strPLCRun == '4')
		//		{//정상 완료.
		//			pProcData->nResult = VS24MotData::Ret_Ok;
		//			G_WriteInfo(Log_Info,"AM01_LGripper3 Act_Home Ok");
		//			break;
		//		}
		//		TimeOutValue--;
		//		if(TimeOutValue<0)
		//		{//1분 Time Out.
		//			pProcData->nResult = VS24MotData::Ret_TimeOut;
		//			G_WriteInfo(Log_Worrying,"AM01_LGripper3 Act_Move Time Out");
		//			break;
		//		}
		//		Sleep(100);
		//	}
		//	if(*pBreakProc == false)
		//	{
		//		pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
		//		G_WriteInfo(Log_Worrying,"AM01_LGripper3 Act_Move Run Stop");
		//	}
		//}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_LGripper3 Send CMD Error");
	}
	return 1;
}	
int CMDFunList::fn_Seq_AM01_LGripper4(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_B U
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			//MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_LTension4);
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"AM01_LGripper4 Home CMD Only AM01_LGripper1");
			MotRunRet = false;
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_LTension4);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_LTension4,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_LTension4);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_LGripper4 Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_LTension4] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[7].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_LGripper4 Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_LGripper4 Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_LGripper4 Act_Move Run Stop");
			}
		}
		//else if(pProcData->nAction == VS24MotData::Act_Home)
		//{
		//	TimeOutValue = 300*10;//30Sec
		//	CString strPLCRun;
		//	Sleep(100);//기본PLC 동식 시간을 준다.
		//	while(*pBreakProc == true)
		//	{
		//		strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
		//		if(strPLCRun == '4')
		//		{//정상 완료.
		//			pProcData->nResult = VS24MotData::Ret_Ok;
		//			G_WriteInfo(Log_Info,"AM01_LGripper4 Act_Home Ok");
		//			break;
		//		}
		//		TimeOutValue--;
		//		if(TimeOutValue<0)
		//		{//1분 Time Out.
		//			pProcData->nResult = VS24MotData::Ret_TimeOut;
		//			G_WriteInfo(Log_Worrying,"AM01_LGripper4 Act_Move Time Out");
		//			break;
		//		}
		//		Sleep(100);
		//	}
		//	if(*pBreakProc == false)
		//	{
		//		pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
		//		G_WriteInfo(Log_Worrying,"AM01_LGripper4 Act_Move Run Stop");
		//	}
		//}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_LGripper4 Send CMD Error");
	}
	return 1;
}	
int CMDFunList::fn_Seq_AM01_RGripper1(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_C X
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_RTension1);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_RTension1);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_RTension1,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_RTension1);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_RGripper1 Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_RTension1] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[8].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_RGripper1 Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_RGripper1 Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_RGripper1 Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				if(strPLCRun == '4')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_RGripper1 Act_Home Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_RGripper1 Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_RGripper1 Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_RGripper1 Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_RGripper2(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_C Y
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			//MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_RTension2);
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"AM01_RGripper2 Home CMD Only AM01_RGripper1");
			MotRunRet = false;
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_RTension2);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_RTension2,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_RTension2);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_RGripper2 Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_RTension2] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[9].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_RGripper2 Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_RGripper2 Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_RGripper2 Act_Move Run Stop");
			}
		}
		//else if(pProcData->nAction == VS24MotData::Act_Home)
		//{
		//	TimeOutValue = 300*10;//30Sec
		//	CString strPLCRun;
		//	Sleep(100);//기본PLC 동식 시간을 준다.
		//	while(*pBreakProc == true)
		//	{
		//		strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
		//		if(strPLCRun == '4')
		//		{//정상 완료.
		//			pProcData->nResult = VS24MotData::Ret_Ok;
		//			G_WriteInfo(Log_Info,"AM01_RGripper2 Act_Home Ok");
		//			break;
		//		}
		//		TimeOutValue--;
		//		if(TimeOutValue<0)
		//		{//1분 Time Out.
		//			pProcData->nResult = VS24MotData::Ret_TimeOut;
		//			G_WriteInfo(Log_Worrying,"AM01_RGripper2 Act_Move Time Out");
		//			break;
		//		}
		//		Sleep(100);
		//	}
		//	if(*pBreakProc == false)
		//	{
		//		pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
		//		G_WriteInfo(Log_Worrying,"AM01_RGripper2 Act_Move Run Stop");
		//	}
		//}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_RGripper2 Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_RGripper3(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_C Z
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			//MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_RTension3);
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"AM01_RGripper3 Home CMD Only AM01_LGripper1");
			MotRunRet = false;
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_RTension3);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_RTension3,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_RTension3);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_RGrippe3 Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_RTension3] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[10].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_RGrippe3 Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_RGrippe3 Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_RGrippe3 Act_Move Run Stop");
			}
		}
		//else if(pProcData->nAction == VS24MotData::Act_Home)
		//{
		//	TimeOutValue = 300*10;//30Sec
		//	CString strPLCRun;
		//	Sleep(100);//기본PLC 동식 시간을 준다.
		//	while(*pBreakProc == true)
		//	{
		//		strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
		//		if(strPLCRun == '4')
		//		{//정상 완료.
		//			pProcData->nResult = VS24MotData::Ret_Ok;
		//			G_WriteInfo(Log_Info,"AM01_RGrippe3 Act_Home Ok");
		//			break;
		//		}
		//		TimeOutValue--;
		//		if(TimeOutValue<0)
		//		{//1분 Time Out.
		//			pProcData->nResult = VS24MotData::Ret_TimeOut;
		//			G_WriteInfo(Log_Worrying,"AM01_RGrippe3 Act_Move Time Out");
		//			break;
		//		}
		//		Sleep(100);
		//	}
		//	if(*pBreakProc == false)
		//	{
		//		pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
		//		G_WriteInfo(Log_Worrying,"AM01_RGrippe3 Act_Move Run Stop");
		//	}
		//}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_RGrippe3 Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_RGripper4(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_C U
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			//MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_RTension4);
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"AM01_RGripper4 Home CMD Only AM01_LGripper1");
			MotRunRet = false;
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_RTension4);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_RTension4,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_RTension4);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_RGrippe4 Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_RTension4] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[11].St06_SINP == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_RGrippe4 Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_RGrippe4 Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_RGrippe4 Act_Move Run Stop");
			}
		}
		//else if(pProcData->nAction == VS24MotData::Act_Home)
		//{
		//	TimeOutValue = 300*10;//30Sec
		//	CString strPLCRun;
		//	Sleep(100);//기본PLC 동식 시간을 준다.
		//	while(*pBreakProc == true)
		//	{
		//		strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
		//		if(strPLCRun == '4')
		//		{//정상 완료.
		//			pProcData->nResult = VS24MotData::Ret_Ok;
		//			G_WriteInfo(Log_Info,"AM01_RGrippe4 Act_Home Ok");
		//			break;
		//		}
		//		TimeOutValue--;
		//		if(TimeOutValue<0)
		//		{//1분 Time Out.
		//			pProcData->nResult = VS24MotData::Ret_TimeOut;
		//			G_WriteInfo(Log_Worrying,"AM01_RGrippe4 Act_Move Time Out");
		//			break;
		//		}
		//		Sleep(100);
		//	}
		//	if(*pBreakProc == false)
		//	{
		//		pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
		//		G_WriteInfo(Log_Worrying,"AM01_RGrippe4 Act_Move Run Stop");
		//	}
		//}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_RGrippe4 Send CMD Error");
	}
	return 1;
}
//AM01
int CMDFunList::fn_Seq_AM01_JigInOut(bool *pBreakProc, VS24MotData*pProcData)//ML3 #23
{
#ifndef MOT_USE_UMAC_JIG_23AXIS
	pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
	G_WriteInfo(Log_Error,"AM01_JigInOut Not Use");
	return 1;
#endif
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_JigInOut);
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_JigInOut);
			MotRunRet = G_MotCtrlObj.m_fnAM01AbsMove(AXIS_AM01_JigInOut,(long)pProcData->newPosition1);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01Stop(AXIS_AM01_JigInOut);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue = 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_JigInOut Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:5)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			int InpositionValue = AXIS_INPOS_CTS;
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				InpositionValue = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_JigInOut] - pProcData->newPosition1);
				if(InpositionValue<=AXIS_INPOS_CTS && G_MotCtrlObj.m_ML3AxisST[13].m_AxisStB.St00_InPosition == 1)
				{//Inposition 안에 들어옴.(3um)

					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_JigInOut Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_JigInOut Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_JigInOut Act_Move Run Stop");
			}
		}
		else if(pProcData->nAction == VS24MotData::Act_Home)
		{
			TimeOutValue = 300*10;//30Sec
			CString strPLCRun, strMotComplet;
			Sleep(100);//기본PLC 동식 시간을 준다.
			while(*pBreakProc == true)
			{
				strPLCRun.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(PLC_26_AM01_Home));
				strMotComplet.Format("%s", G_MotCtrlObj.m_fnUmacOnlineCommand(HOME_COMPLET_AM01_JigInOut));
				if(strPLCRun == '4' && strMotComplet == '1')
				{//정상 완료.
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_JigInOut Act_Home Ok");
					break;
				}

				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_JigInOut Act_Move Time Out");
					break;
				}
				Sleep(100);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_JigInOut Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_JigInOut Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_LTensionMulti(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_B block
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			//MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_RTension4);
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"AM01_LTensionMulti Home CMD Only AM01_LGripper1");
			MotRunRet = false;
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_LTension1);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_LTension2);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_LTension3);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_LTension4);
			MotRunRet = G_MotCtrlObj.m_fnAM01TenstionLMultiAbsMove(pProcData->SelectPoint, 
																							pProcData->newPosition1,
																							pProcData->newPosition2,
																							pProcData->newPosition3,
																							pProcData->newPosition4);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01TenstionLMultiAbsStop(false, pProcData->SelectPoint);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue= 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_LTensionMulti Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			bool MultiActionEnd = true;
			int InpositionValue[4] = {AXIS_INPOS_CTS, AXIS_INPOS_CTS, AXIS_INPOS_CTS, AXIS_INPOS_CTS};
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				MultiActionEnd = true;
				if((pProcData->SelectPoint)&0x01)
					InpositionValue[0] = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_LTension1] - pProcData->newPosition1);
				if((pProcData->SelectPoint)&0x02)
					InpositionValue[1] = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_LTension2] - pProcData->newPosition2);
				if((pProcData->SelectPoint)&0x04)
					InpositionValue[2] = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_LTension3] - pProcData->newPosition3);
				if((pProcData->SelectPoint)&0x08)
					InpositionValue[3] = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_LTension4] - pProcData->newPosition4);
				//모두가 완료 되었는지를 감시 한다.
				for(int CStep =0; CStep<4; CStep++)
				{
					if(InpositionValue[CStep] > AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[4+CStep].St06_SINP == 1)
						MultiActionEnd = false;
				}
				if(MultiActionEnd == true)
				{//Inposition 안에 들어옴.(3um)
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_LTensionMulti Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_LTensionMulti Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_LTensionMulti Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_LTensionMulti Send CMD Error");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_RTensionMulti(bool *pBreakProc, VS24MotData*pProcData)//AxisLink_C block
{
	bool MotRunRet = false;
	switch(pProcData->nAction)
	{
	case VS24MotData::Act_None:
		break;
	case VS24MotData::Act_Home:
		{
			//MotRunRet = G_MotCtrlObj.m_fnAM01Homing(AXIS_AM01_RTension4);
			pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
			G_WriteInfo(Log_Error,"AM01_RTensionMulti Home CMD Only AM01_RGripper1");
			MotRunRet = false;
		}break;
	case VS24MotData::Act_Move:
		{
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_RTension1);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_RTension2);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_RTension3);
			G_MotCtrlObj.m_fnSetMotSpeed(pProcData->SpeedMode, AXIS_AM01_RTension4);
			MotRunRet = G_MotCtrlObj.m_fnAM01TenstionRMultiAbsMove(pProcData->SelectPoint, 
				pProcData->newPosition1,
				pProcData->newPosition2,
				pProcData->newPosition3,
				pProcData->newPosition4);
		}break;
	case VS24MotData::Act_Stop:
		{
			MotRunRet = G_MotCtrlObj.m_fnAM01TenstionRMultiAbsStop(false, pProcData->SelectPoint);
		}break;
	default:
		break;
	}
	if(MotRunRet == true)
	{
		int TimeOutValue= 0;
		//////////////////////////////////////////////////////////////////////////
		//동작 완료 체크.
		if(pProcData->nAction == VS24MotData::Act_Stop)
		{//체크 항목이 있나?
			pProcData->nResult = VS24MotData::Ret_Ok;
			G_WriteInfo(Log_Info,"AM01_RTensionMulti Act_Stop Ok");
		}
		else if(pProcData->nAction == VS24MotData::Act_Move)
		{
			TimeOutValue = (pProcData->TimeOut>0?pProcData->TimeOut:30)*100;//기본 5초 Time Out을 사용 한다.
			//TimeOutValue = 300*10;//30Sec
			bool MultiActionEnd = true;
			int InpositionValue[4] = {AXIS_INPOS_CTS, AXIS_INPOS_CTS, AXIS_INPOS_CTS, AXIS_INPOS_CTS};
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				MultiActionEnd = true;
				if((pProcData->SelectPoint)&0x01)
					InpositionValue[0] = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_RTension1] - pProcData->newPosition1);
				if((pProcData->SelectPoint)&0x02)
					InpositionValue[1] = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_RTension2] - pProcData->newPosition2);
				if((pProcData->SelectPoint)&0x04)
					InpositionValue[2] = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_RTension3] - pProcData->newPosition3);
				if((pProcData->SelectPoint)&0x08)
					InpositionValue[3] = abs(G_MotCtrlObj.m_AxisNowPos[AXIS_AM01_RTension4] - pProcData->newPosition4);
				//모두가 완료 되었는지를 감시 한다.
				for(int CStep =0; CStep<4; CStep++)
				{
					if(InpositionValue[CStep] > AXIS_INPOS_CTS && G_MotCtrlObj.m_ALinkAxisST[11+CStep].St06_SINP == 1)
						MultiActionEnd = false;
				}
				if(MultiActionEnd == true)
				{//Inposition 안에 들어옴.(3um)
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"AM01_RTensionMulti Act_Move Ok");
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"AM01_RTensionMulti Act_Move Time Out");
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"AM01_RTensionMulti Act_Move Run Stop");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//구동 명령 전달 오류.
		G_WriteInfo(Log_Error,"AM01_RTensionMulti Send CMD Error");
	}
	return 1;
}




//////////////////////////////////////////////////////////////////////////
//Cylinder Sol Action
//////////////////////////////////////////////////////////////////////////
int CMDFunList::fn_Seq_TM01_FORK_UP	(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetTM01State_CySol_FORK();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"TM01_FORK_UP Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if(G_MotCtrlObj.m_fnTM01SolInterLockCheck_FORK_UP()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutC.Out18_ForkUpSol = 1;
					G_MotCtrlObj.m_IO_OutC.Out19_ForkDownSol = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetTM01State_CySol_FORK();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"TM01_FORK_UP Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"TM01_FORK_UP Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutC.Out18_ForkUpSol = 0;
					G_MotCtrlObj.m_IO_OutC.Out19_ForkDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"TM01_FORK_UP Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"TM01_FORK_UP Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"TM01_FORK_UP Sensor State Error");
		break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM01_FORK_UP Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM01_FORK_DN(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetTM01State_CySol_FORK();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnTM01SolInterLockCheck_FORK_DN()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutC.Out18_ForkUpSol = 0;
					G_MotCtrlObj.m_IO_OutC.Out19_ForkDownSol = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetTM01State_CySol_FORK();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"TM01_FORK_DN Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"TM01_FORK_DN Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutC.Out18_ForkUpSol = 0;
					G_MotCtrlObj.m_IO_OutC.Out19_ForkDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"TM01_FORK_DN Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"TM01_FORK_DN Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"TM01_FORK_DN Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"TM01_FORK_DN Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM01_FORK_DN Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_LT01_GUIDE_FWD(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetLT01State_CySol_GUIDE();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"LT01_GUIDE_FWD Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnLT01SolInterLockCheck_GUIDE_FWD()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutC.Out10_LoadBoxAlignCylinderForward = 1;
					G_MotCtrlObj.m_IO_OutC.Out11_LoadBoxAlignCylinderBackward = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetLT01State_CySol_GUIDE();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"LT01_GUIDE_FWD Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"LT01_GUIDE_FWD Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutC.Out10_LoadBoxAlignCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out11_LoadBoxAlignCylinderBackward = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"LT01_GUIDE_FWD Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"LT01_GUIDE_FWD Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"LT01_GUIDE_FWD Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"LT01_GUIDE_FWD Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_LT01_GUIDE_BWD(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetLT01State_CySol_GUIDE();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnLT01SolInterLockCheck_GUIDE_BWD()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutC.Out10_LoadBoxAlignCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out11_LoadBoxAlignCylinderBackward = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetLT01State_CySol_GUIDE();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"LT01_GUIDE_BWD Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"LT01_GUIDE_BWD Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutC.Out10_LoadBoxAlignCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out11_LoadBoxAlignCylinderBackward = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"LT01_GUIDE_BWD Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"LT01_GUIDE_BWD Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"LT01_GUIDE_BWD Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"LT01_GUIDE_BWD Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"LT01_GUIDE_BWD Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_UT01_GUIDE_FWD(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetUT01State_CySol_GUIDE();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"UT01_GUIDE_FWD Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnUT01SolInterLockCheck_GUIDE_FWD()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutC.Out14_UnloadBoxAlignCylinderForward = 1;
					G_MotCtrlObj.m_IO_OutC.Out15_UnloadBoxAlignCylinderBackward = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetUT01State_CySol_GUIDE();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"UT01_GUIDE_FWD Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"UT01_GUIDE_FWD Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutC.Out14_UnloadBoxAlignCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out15_UnloadBoxAlignCylinderBackward = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"UT01_GUIDE_FWD Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"UT01_GUIDE_FWD Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"UT01_GUIDE_FWD Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"UT01_GUIDE_FWD Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_UT01_GUIDE_BWD(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetUT01State_CySol_GUIDE();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnUT01SolInterLockCheck_GUIDE_BWD()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutC.Out14_UnloadBoxAlignCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out15_UnloadBoxAlignCylinderBackward = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetUT01State_CySol_GUIDE();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"UT01_GUIDE_BWD Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"UT01_GUIDE_BWD Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutC.Out14_UnloadBoxAlignCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out15_UnloadBoxAlignCylinderBackward = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"UT01_GUIDE_BWD Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"UT01_GUIDE_BWD Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"UT01_GUIDE_BWD Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"UT01_GUIDE_BWD Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"UT01_GUIDE_BWD Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_BO01_PICKER_FWD(bool *pBreakProc, VS24MotData*pProcData)
{
	//Left Right 동시동작
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetBO01State_CySol_PICKER();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"BO01_PICKER_FWD Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnBO01SolInterLockCheck_PICKER_FWD()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutC.Out00_BoxLeftCylinderForward = 1;
					G_MotCtrlObj.m_IO_OutC.Out04_BoxRightCylinderForward = 1;
					G_MotCtrlObj.m_IO_OutC.Out01_BoxLeftCylinderBackward = 0;
					G_MotCtrlObj.m_IO_OutC.Out05_BoxRightCylinderBackward = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetBO01State_CySol_PICKER();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"BO01_PICKER_FWD Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"BO01_PICKER_FWD Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutC.Out00_BoxLeftCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out04_BoxRightCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out01_BoxLeftCylinderBackward = 0;
					G_MotCtrlObj.m_IO_OutC.Out05_BoxRightCylinderBackward = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"BO01_PICKER_FWD Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"BO01_PICKER_FWD Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"BO01_PICKER_FWD Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"BO01_PICKER_FWD Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_BO01_PICKER_BWD(bool *pBreakProc, VS24MotData*pProcData)
{//Left Right 동시동작
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetBO01State_CySol_PICKER();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnBO01SolInterLockCheck_PICKER_BWD()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutC.Out00_BoxLeftCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out04_BoxRightCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out01_BoxLeftCylinderBackward = 1;
					G_MotCtrlObj.m_IO_OutC.Out05_BoxRightCylinderBackward = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetBO01State_CySol_PICKER();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"BO01_PICKER_BWD Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"BO01_PICKER_BWD Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutC.Out00_BoxLeftCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out04_BoxRightCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutC.Out01_BoxLeftCylinderBackward = 0;
					G_MotCtrlObj.m_IO_OutC.Out05_BoxRightCylinderBackward = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"BO01_PICKER_BWD Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"BO01_PICKER_BWD Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"BO01_PICKER_BWD Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"BO01_PICKER_BWD Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"BO01_PICKER_BWD Unknown Action");
	}
	return 1;
}
//int fn_Seq_BO01_RIGHT_PICKER_FWD(bool *pBreakProc, VS24MotData*pProcData)
//{
//
//	return 1;
//}
//int fn_Seq_BO01_RIGHT_PICKER_BWD(bool *pBreakProc, VS24MotData*pProcData)
//{
//
//	return 1;
//}

int CMDFunList::fn_Seq_TM02_LR_PAPER_PICKER_UP(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetTM02State_CySol_LR_PAPER_PICKER();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"TM02_LR_PAPER_PICKER_UP Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnTM02SolInterLockCheck_LR_PAPER_PICKER_UP()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutF.Out20_PickerPaperLeftRightUpSol = 1;
					G_MotCtrlObj.m_IO_OutF.Out21_PickerPaperLeftRightDownSol = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetTM02State_CySol_LR_PAPER_PICKER();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"TM02_LR_PAPER_PICKER_UP Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"TM02_LR_PAPER_PICKER_UP Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutF.Out20_PickerPaperLeftRightUpSol = 0;
					G_MotCtrlObj.m_IO_OutF.Out21_PickerPaperLeftRightDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"TM02_LR_PAPER_PICKER_UP Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"TM02_LR_PAPER_PICKER_UP Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"TM02_LR_PAPER_PICKER_UP Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM02_LR_PAPER_PICKER_UP Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM02_LR_PAPER_PICKER_DN(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetTM02State_CySol_LR_PAPER_PICKER();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnTM02SolInterLockCheck_LR_PAPER_PICKER_DN()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutF.Out20_PickerPaperLeftRightUpSol = 0;
					G_MotCtrlObj.m_IO_OutF.Out21_PickerPaperLeftRightDownSol = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetTM02State_CySol_LR_PAPER_PICKER();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"TM02_LR_PAPER_PICKER_DN Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"TM02_LR_PAPER_PICKER_DN Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutF.Out20_PickerPaperLeftRightUpSol = 0;
					G_MotCtrlObj.m_IO_OutF.Out21_PickerPaperLeftRightDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"TM02_LR_PAPER_PICKER_DN Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"TM02_LR_PAPER_PICKER_DN Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"TM02_LR_PAPER_PICKER_DN Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"TM02_LR_PAPER_PICKER_DN Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM02_LR_PAPER_PICKER_DN Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM02_CNT_PAPER_PICKER_UP(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetTM02State_CySol_CNT_PAPER_PICKER();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"TM02_CNT_PAPER_PICKER_UP Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnTM02SolInterLockCheck_CNT_PAPER_PICKER_UP()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutF.Out22_PickerPaperCenterUpSol = 1;
					G_MotCtrlObj.m_IO_OutF.Out23_PickerPaperCenterDownSol = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetTM02State_CySol_CNT_PAPER_PICKER();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"TM02_CNT_PAPER_PICKER_UP Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_UP Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutF.Out22_PickerPaperCenterUpSol = 0;
					G_MotCtrlObj.m_IO_OutF.Out23_PickerPaperCenterDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_UP Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_UP Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"TM02_CNT_PAPER_PICKER_UP Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_UP Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM02_CNT_PAPER_PICKER_DN(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetTM02State_CySol_CNT_PAPER_PICKER();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnTM02SolInterLockCheck_CNT_PAPER_PICKER_DN()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutF.Out22_PickerPaperCenterUpSol = 0;
					G_MotCtrlObj.m_IO_OutF.Out23_PickerPaperCenterDownSol = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetTM02State_CySol_CNT_PAPER_PICKER();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"TM02_CNT_PAPER_PICKER_DN Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_DN Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutF.Out22_PickerPaperCenterUpSol = 0;
					G_MotCtrlObj.m_IO_OutF.Out23_PickerPaperCenterDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_DN Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_DN Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"TM02_CNT_PAPER_PICKER_DN Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"TM02_CNT_PAPER_PICKER_DN Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_DN Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_AM01_OPEN_TEN_GRIPPER(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->SelectPoint == 0)
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_OPEN_TEN_GRIPPER No Select Gripper");
	}
	else if(pProcData->nAction == VS24MotData::Act_None)
	{
		CString SelecGripper;
		//pProcData->SelectPoint==>에 Open할 Gripper가 들어 있다. 위치를 나타내기때문에 Position에 넣는다.
		int CylinenderNowState[4]={SOL_NONE, SOL_NONE, SOL_NONE, SOL_NONE};
		if((pProcData->SelectPoint&0x01) == 0x01)
		{
			CylinenderNowState[0] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER1();
			SelecGripper.AppendFormat("<1");
		}
		if((pProcData->SelectPoint&0x02) == 0x02)
		{
			CylinenderNowState[1] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER2();
			SelecGripper.AppendFormat("<2");
		}
		if((pProcData->SelectPoint&0x04) == 0x04)
		{
			CylinenderNowState[2] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER3();
			SelecGripper.AppendFormat("<3");
		}
		if((pProcData->SelectPoint&0x08) == 0x08)
		{
			CylinenderNowState[3] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER4();
			SelecGripper.AppendFormat("<4");
		}
		G_WriteInfo(Log_Info,"AM01_OPEN_TEN_GRIPPER (%s)", SelecGripper.GetBuffer(SelecGripper.GetLength()));
			//체크 할것										//체크 안하는것은 NONE으로 Pass시킨다.
		if( (CylinenderNowState[0] == SOL_UP || CylinenderNowState[0] == SOL_NONE) &&
			(CylinenderNowState[1] == SOL_UP || CylinenderNowState[1] == SOL_NONE) &&
			(CylinenderNowState[2] == SOL_UP || CylinenderNowState[2] == SOL_NONE) &&
			(CylinenderNowState[3] == SOL_UP || CylinenderNowState[3] == SOL_NONE) )
		{//해당 Sol이 모두 Up 상태라면
			pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
			G_WriteInfo(Log_Info,"AM01_OPEN_TEN_GRIPPER Ret_Ok");
		}
		else
		{
			//Open동작에관한 통칭이다.
			if( G_MotCtrlObj.m_fnAM01SolInterLockCheck_OPEN_TEN_GRIPPER()== false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
			{

				if(CylinenderNowState[0] != SOL_DOWN && CylinenderNowState[0] != SOL_NONE && CylinenderNowState[0] != SOL_UP)
				{//선택 되거나 UP상태가 아니면 Error상태 이다.
					G_WriteInfo(Log_Error,"AM01_OPEN_TEN_GRIPPER GRIPPER1 State Error(%d)", CylinenderNowState[0]);
					pProcData->nResult = VS24MotData::Ret_Error;
				}
				if(CylinenderNowState[1] != SOL_DOWN && CylinenderNowState[1] != SOL_NONE && CylinenderNowState[1] != SOL_UP)
				{//선택 되거나 UP상태가 아니면 Error상태 이다.
					G_WriteInfo(Log_Error,"AM01_OPEN_TEN_GRIPPER GRIPPER2 State Error(%d)", CylinenderNowState[1]);
					pProcData->nResult = VS24MotData::Ret_Error;
				}
				if(CylinenderNowState[2] != SOL_DOWN && CylinenderNowState[2] != SOL_NONE && CylinenderNowState[2] != SOL_UP)
				{//선택 되거나 UP상태가 아니면 Error상태 이다.
					G_WriteInfo(Log_Error,"AM01_OPEN_TEN_GRIPPER GRIPPER3 State Error(%d)", CylinenderNowState[2]);
					pProcData->nResult = VS24MotData::Ret_Error;
				}
				if(CylinenderNowState[3] != SOL_DOWN && CylinenderNowState[3] != SOL_NONE && CylinenderNowState[3] != SOL_UP)
				{//선택 되거나 UP상태가 아니면 Error상태 이다.
					G_WriteInfo(Log_Error,"AM01_OPEN_TEN_GRIPPER GRIPPER4 State Error(%d)", CylinenderNowState[3]);
					pProcData->nResult = VS24MotData::Ret_Error;
				}

				if(pProcData->nResult == VS24MotData::Ret_None)//상태 체크 결과가 none이 아니면 동작 하지 않는다.
				{
					
					//출력을 준다. ==>Error나 Down상태 가 정상이 아닌 Sol은 동작 하지 않는다.(좌우 한쌍을 체크 하는것이다.)
					if(CylinenderNowState[0] == SOL_DOWN)//현상태가 Down이면 Up동자(Open)을 시행 한다.(이외의 상태이면 동작 X)
					{
						G_MotCtrlObj.m_IO_OutD.Out00_LeftOpenGripper1 = 1;
						G_MotCtrlObj.m_IO_OutD.Out10_RightOpenGripper1 = 1;
						G_MotCtrlObj.m_IO_OutD.Out01_LeftCloseGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out11_RightCloseGripper1 = 0;
					}

					if(CylinenderNowState[1] == SOL_DOWN)//현상태가 Down이면 Up동자(Open)을 시행 한다.(이외의 상태이면 동작 X)
					{
						G_MotCtrlObj.m_IO_OutD.Out02_LeftOpenGripper2 = 1;
						G_MotCtrlObj.m_IO_OutD.Out12_RightOpenGripper2 = 1;
						G_MotCtrlObj.m_IO_OutD.Out03_LeftCloseGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out13_RightCloseGripper2 = 0;
					}

					if(CylinenderNowState[2] == SOL_DOWN)//현상태가 Down이면 Up동자(Open)을 시행 한다.(이외의 상태이면 동작 X)
					{
						G_MotCtrlObj.m_IO_OutD.Out04_LeftOpenGripper3 = 1;
						G_MotCtrlObj.m_IO_OutD.Out14_RightOpenGripper3 = 1;
						G_MotCtrlObj.m_IO_OutD.Out05_LeftCloseGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out15_RightCloseGripper3 = 0;
					}

					if(CylinenderNowState[3] == SOL_DOWN)//현상태가 Down이면 Up동자(Open)을 시행 한다.(이외의 상태이면 동작 X)
					{
						G_MotCtrlObj.m_IO_OutD.Out06_LeftOpenGripper4 = 1;
						G_MotCtrlObj.m_IO_OutD.Out16_RightOpenGripper4 = 1;
						G_MotCtrlObj.m_IO_OutD.Out07_LeftCloseGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out17_RightCloseGripper4 = 0;
					}

					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						if((pProcData->SelectPoint&0x01) == 0x01)
							CylinenderNowState[0] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER1();
						if((pProcData->SelectPoint&0x02) == 0x02)
							CylinenderNowState[1] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER2();
						if((pProcData->SelectPoint&0x04) == 0x04)
							CylinenderNowState[2] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER3();
						if((pProcData->SelectPoint&0x08) == 0x08)
							CylinenderNowState[3] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER4();


						//대상이 모두 Up되었는지 체크
						if( (CylinenderNowState[0] == SOL_UP || CylinenderNowState[0] == SOL_NONE) &&
							(CylinenderNowState[1] == SOL_UP || CylinenderNowState[1] == SOL_NONE) &&
							(CylinenderNowState[2] == SOL_UP || CylinenderNowState[2] == SOL_NONE) &&
							(CylinenderNowState[3] == SOL_UP || CylinenderNowState[3] == SOL_NONE) )
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"AM01_OPEN_TEN_GRIPPER Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"AM01_OPEN_TEN_GRIPPER Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다. 해당 Sol의 출력만
					if((pProcData->SelectPoint&0x01) == 0x01)
					{
						G_MotCtrlObj.m_IO_OutD.Out00_LeftOpenGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out10_RightOpenGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out01_LeftCloseGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out11_RightCloseGripper1 = 0;
					}
					if((pProcData->SelectPoint&0x02) == 0x02)
					{
						G_MotCtrlObj.m_IO_OutD.Out02_LeftOpenGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out12_RightOpenGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out03_LeftCloseGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out13_RightCloseGripper2 = 0;
					}

					if((pProcData->SelectPoint&0x04) == 0x04)
					{
						G_MotCtrlObj.m_IO_OutD.Out04_LeftOpenGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out14_RightOpenGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out05_LeftCloseGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out15_RightCloseGripper3 = 0;
					}

					if((pProcData->SelectPoint&0x08) == 0x08)
					{
						G_MotCtrlObj.m_IO_OutD.Out06_LeftOpenGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out16_RightOpenGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out07_LeftCloseGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out17_RightCloseGripper4 = 0;
					}
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"AM01_OPEN_TEN_GRIPPER Run Stop");
					}
				}
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
				G_WriteInfo(Log_Worrying,"AM01_OPEN_TEN_GRIPPER Send CMD Worrying");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_OPEN_TEN_GRIPPER Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_CLOSE_TEN_GRIPPER(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->SelectPoint == 0)
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_CLOSE_TEN_GRIPPER No Select Gripper");
	}
	else if(pProcData->nAction == VS24MotData::Act_None)
	{
		CString SelecGripper;
		//pProcData->SelectPoint==>에 Open할 Gripper가 들어 있다. 위치를 나타내기때문에 Position에 넣는다.
		int CylinenderNowState[4]={SOL_NONE, SOL_NONE, SOL_NONE, SOL_NONE};
		if((pProcData->SelectPoint&0x01) == 0x01)
		{
			CylinenderNowState[0] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER1();
			SelecGripper.AppendFormat("<1");
		}
		if((pProcData->SelectPoint&0x02) == 0x02)
		{
			CylinenderNowState[1] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER2();
			SelecGripper.AppendFormat("<2");
		}
		if((pProcData->SelectPoint&0x04) == 0x04)
		{
			CylinenderNowState[2] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER3();
			SelecGripper.AppendFormat("<3");
		}
		if((pProcData->SelectPoint&0x08) == 0x08)
		{
			CylinenderNowState[3] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER4();
			SelecGripper.AppendFormat("<4");
		}
		G_WriteInfo(Log_Info,"AM01_CLOSE_TEN_GRIPPER (%s)", SelecGripper.GetBuffer(SelecGripper.GetLength()));

		//체크 할것										//체크 안하는것은 NONE으로 Pass시킨다.
		if( (CylinenderNowState[0] == SOL_DOWN || CylinenderNowState[0] == SOL_NONE) &&
			(CylinenderNowState[1] == SOL_DOWN || CylinenderNowState[1] == SOL_NONE) &&
			(CylinenderNowState[2] == SOL_DOWN || CylinenderNowState[2] == SOL_NONE) &&
			(CylinenderNowState[3] == SOL_DOWN || CylinenderNowState[3] == SOL_NONE) )
		{//해당 Sol이 모두 Down 상태라면
			pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
			G_WriteInfo(Log_Info,"AM01_CLOSE_TEN_GRIPPER Ret_Ok");
		}
		else
		{
			//Open동작에관한 통칭이다.
			if( G_MotCtrlObj.m_fnAM01SolInterLockCheck_CLOSE_TEN_GRIPPER()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
			{

				if(CylinenderNowState[0] != SOL_DOWN && CylinenderNowState[0] != SOL_NONE && CylinenderNowState[0] != SOL_UP)
				{//선택 되거나 UP상태가 아니면 Error상태 이다.
					G_WriteInfo(Log_Error,"AM01_CLOSE_TEN_GRIPPER GRIPPER1 State Error(%d)", CylinenderNowState[0]);
					pProcData->nResult = VS24MotData::Ret_Error;
				}
				if(CylinenderNowState[1] != SOL_DOWN && CylinenderNowState[1] != SOL_NONE && CylinenderNowState[1] != SOL_UP)
				{//선택 되거나 UP상태가 아니면 Error상태 이다.
					G_WriteInfo(Log_Error,"AM01_CLOSE_TEN_GRIPPER GRIPPER2 State Error(%d)", CylinenderNowState[1]);
					pProcData->nResult = VS24MotData::Ret_Error;
				}
				if(CylinenderNowState[2] != SOL_DOWN && CylinenderNowState[2] != SOL_NONE && CylinenderNowState[2] != SOL_UP)
				{//선택 되거나 UP상태가 아니면 Error상태 이다.
					G_WriteInfo(Log_Error,"AM01_CLOSE_TEN_GRIPPER GRIPPER3 State Error(%d)", CylinenderNowState[2]);
					pProcData->nResult = VS24MotData::Ret_Error;
				}
				if(CylinenderNowState[3] != SOL_DOWN && CylinenderNowState[3] != SOL_NONE && CylinenderNowState[3] != SOL_UP)
				{//선택 되거나 UP상태가 아니면 Error상태 이다.
					G_WriteInfo(Log_Error,"AM01_CLOSE_TEN_GRIPPER GRIPPER4 State Error(%d)", CylinenderNowState[3]);
					pProcData->nResult = VS24MotData::Ret_Error;
				}

				if(pProcData->nResult == VS24MotData::Ret_None)//상태 체크 결과가 none이 아니면 동작 하지 않는다.
				{
					//출력을 준다. ==>Error나 Down상태 가 정상이 아닌 Sol은 동작 하지 않는다.(좌우 한쌍을 체크 하는것이다.)
					if(CylinenderNowState[0] == SOL_UP)//현상태가 Down이면 Up동자(Open)을 시행 한다.(이외의 상태이면 동작 X)
					{
						G_MotCtrlObj.m_IO_OutD.Out00_LeftOpenGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out10_RightOpenGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out01_LeftCloseGripper1 = 1;
						G_MotCtrlObj.m_IO_OutD.Out11_RightCloseGripper1 = 1;
					}

					if(CylinenderNowState[1] == SOL_UP)//현상태가 Down이면 Up동자(Open)을 시행 한다.(이외의 상태이면 동작 X)
					{
						G_MotCtrlObj.m_IO_OutD.Out02_LeftOpenGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out12_RightOpenGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out03_LeftCloseGripper2 = 1;
						G_MotCtrlObj.m_IO_OutD.Out13_RightCloseGripper2 = 1;
					}

					if(CylinenderNowState[2] == SOL_UP)//현상태가 Down이면 Up동자(Open)을 시행 한다.(이외의 상태이면 동작 X)
					{
						G_MotCtrlObj.m_IO_OutD.Out04_LeftOpenGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out14_RightOpenGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out05_LeftCloseGripper3 = 1;
						G_MotCtrlObj.m_IO_OutD.Out15_RightCloseGripper3 = 1;
					}

					if(CylinenderNowState[3] == SOL_UP)//현상태가 Down이면 Up동자(Open)을 시행 한다.(이외의 상태이면 동작 X)
					{
						G_MotCtrlObj.m_IO_OutD.Out06_LeftOpenGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out16_RightOpenGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out07_LeftCloseGripper4 = 1;
						G_MotCtrlObj.m_IO_OutD.Out17_RightCloseGripper4 = 1;
					}

					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						if((pProcData->SelectPoint&0x01) == 0x01)
							CylinenderNowState[0] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER1();
						if((pProcData->SelectPoint&0x02) == 0x02)
							CylinenderNowState[1] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER2();
						if((pProcData->SelectPoint&0x04) == 0x04)
							CylinenderNowState[2] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER3();
						if((pProcData->SelectPoint&0x08) == 0x08)
							CylinenderNowState[3] = G_MotCtrlObj.m_fnGetAM01State_CySol_TEN_GRIPPER4();


						//대상이 모두 Up되었는지 체크
						if( (CylinenderNowState[0] == SOL_DOWN || CylinenderNowState[0] == SOL_NONE) &&
							(CylinenderNowState[1] == SOL_DOWN || CylinenderNowState[1] == SOL_NONE) &&
							(CylinenderNowState[2] == SOL_DOWN || CylinenderNowState[2] == SOL_NONE) &&
							(CylinenderNowState[3] == SOL_DOWN || CylinenderNowState[3] == SOL_NONE) )
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"AM01_CLOSE_TEN_GRIPPER Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"AM01_CLOSE_TEN_GRIPPER Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다. 해당 Sol의 출력만
					if((pProcData->SelectPoint&0x01) == 0x01)
					{
						G_MotCtrlObj.m_IO_OutD.Out00_LeftOpenGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out10_RightOpenGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out01_LeftCloseGripper1 = 0;
						G_MotCtrlObj.m_IO_OutD.Out11_RightCloseGripper1 = 0;
					}
					if((pProcData->SelectPoint&0x02) == 0x02)
					{
						G_MotCtrlObj.m_IO_OutD.Out02_LeftOpenGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out12_RightOpenGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out03_LeftCloseGripper2 = 0;
						G_MotCtrlObj.m_IO_OutD.Out13_RightCloseGripper2 = 0;
					}

					if((pProcData->SelectPoint&0x04) == 0x04)
					{
						G_MotCtrlObj.m_IO_OutD.Out04_LeftOpenGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out14_RightOpenGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out05_LeftCloseGripper3 = 0;
						G_MotCtrlObj.m_IO_OutD.Out15_RightCloseGripper3 = 0;
					}

					if((pProcData->SelectPoint&0x08) == 0x08)
					{
						G_MotCtrlObj.m_IO_OutD.Out06_LeftOpenGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out16_RightOpenGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out07_LeftCloseGripper4 = 0;
						G_MotCtrlObj.m_IO_OutD.Out17_RightCloseGripper4 = 0;
					}
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"AM01_CLOSE_TEN_GRIPPER Run Stop");
					}
					//Cylinder 안정화 시간.
					Sleep(100);
				}
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
				G_WriteInfo(Log_Worrying,"AM01_CLOSE_TEN_GRIPPER Send CMD Worrying");
			}
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_CLOSE_TEN_GRIPPER Unknown Action");
	}
	return 1;
}


int CMDFunList::fn_Seq_AM01_TENTION_FWD(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_TENTION();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"AM01_TENTION_FWD Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnAM01SolInterLockCheck_TENTION_FWD()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutD.Out08_LeftTensionCylinderForward = 1;
					G_MotCtrlObj.m_IO_OutD.Out18_RightTensionCylinderForward = 1;
					G_MotCtrlObj.m_IO_OutD.Out09_LeftTensionCylinderBackward = 0;
					G_MotCtrlObj.m_IO_OutD.Out19_RightTensionCylinderBackward = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_TENTION();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"AM01_TENTION_FWD Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"AM01_TENTION_FWD Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutD.Out08_LeftTensionCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutD.Out18_RightTensionCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutD.Out09_LeftTensionCylinderBackward = 0;
					G_MotCtrlObj.m_IO_OutD.Out19_RightTensionCylinderBackward = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"AM01_TENTION_FWD Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"AM01_TENTION_FWD Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"AM01_TENTION_FWD Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM02_CNT_PAPER_PICKER_UP Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_TENTION_BWD(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_TENTION();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnAM01SolInterLockCheck_TENTION_BWD()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutD.Out08_LeftTensionCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutD.Out18_RightTensionCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutD.Out09_LeftTensionCylinderBackward = 1;
					G_MotCtrlObj.m_IO_OutD.Out19_RightTensionCylinderBackward = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_TENTION();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"AM01_TENTION_BWD Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"AM01_TENTION_BWD Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutD.Out08_LeftTensionCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutD.Out18_RightTensionCylinderForward = 0;
					G_MotCtrlObj.m_IO_OutD.Out09_LeftTensionCylinderBackward = 0;
					G_MotCtrlObj.m_IO_OutD.Out19_RightTensionCylinderBackward = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"AM01_TENTION_BWD Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"AM01_TENTION_BWD Send CMD Worrying(Gripper Open)");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"AM01_TENTION_BWD Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"AM01_TENTION_BWD Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_TENTION_BWD Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_AM01_DoonTuk_Laser_On(bool *pBreakProc, VS24MotData*pProcData)  //정의천 추가 
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
//		int LaserNowState = G_MotCtrlObj.m_fnGetAM01State_DoonTuk_Laser_OnOff();
//		switch(LaserNowState)
//		{
//		case 0:  //출력이 나가지 않아야 Laser On
//			{
		//		pProcData->nResult = VS24MotData::Ret_Ok;
		//		G_WriteInfo(Log_Info,"AM01_LASER_ON Ret_Ok");
		//	}break;
		//case 1:
		//	{
				G_MotCtrlObj.m_IO_OutA.Out31_Space_Sensor_LaserOnOff=0;
				Sleep(100);
				pProcData->nResult = VS24MotData::Ret_Ok;
				G_WriteInfo(Log_Info,"AM01_LASER_ON Ret_Ok");
			//}break;

		//default:
		//	pProcData->nResult = VS24MotData::Ret_Ok;
		//	G_WriteInfo(Log_Info,"AM01_LASER_ON Ret_Ok");
		//	break;
//		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_LASER_ON Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_DoonTuk_Laser_Off(bool *pBreakProc, VS24MotData*pProcData) ////정의천 추가 
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		//		int LaserNowState = G_MotCtrlObj.m_fnGetAM01State_DoonTuk_Laser_OnOff();
		//		switch(LaserNowState)
		//		{
		//		case 0:  //출력이 나가지 않아야 Laser On
		//			{
		G_MotCtrlObj.m_IO_OutA.Out31_Space_Sensor_LaserOnOff=1;
		Sleep(100);
		pProcData->nResult = VS24MotData::Ret_Ok;
		G_WriteInfo(Log_Info,"AM01_LASER_OFF Ret_Ok");
		//	}break;
		//case 1:
		//	{
		//	pProcData->nResult = VS24MotData::Ret_Ok;
		//	G_WriteInfo(Log_Info,"AM01_LASER_OFF Ret_Ok");
		//}break;

		//default:
		//	pProcData->nResult = VS24MotData::Ret_Ok;
		//	G_WriteInfo(Log_Info,"AM01_LASER_ON Ret_Ok");
		//	break;
		//		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_LASER_OFF Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_AM01_RingLight_UP(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_RingLight();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"AM01_ScanLight_UP Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnAM01SolInterLockCheck_RingLight_UP()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutI.Out24_OutputRingLightCylinderUpSol = 1;
					G_MotCtrlObj.m_IO_OutI.Out25_OutputRingLightCylinderDownSol = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_RingLight();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"AM01_ScanLight_UP Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"AM01_ScanLight_UP Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutI.Out24_OutputRingLightCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out25_OutputRingLightCylinderDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"AM01_ScanLight_UP Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"AM01_ScanLight_UP Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"AM01_ScanLight_UP Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_ScanLight_UP Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_RingLight_DN(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_RingLight();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnAM01SolInterLockCheck_RingLight_DN()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutI.Out24_OutputRingLightCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out25_OutputRingLightCylinderDownSol = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_RingLight();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"AM01_RingLight_DN Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"AM01_RingLight_DN Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutI.Out24_OutputRingLightCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out25_OutputRingLightCylinderDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"AM01_RingLight_DN Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"AM01_RingLight_DN Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"AM01_RingLight_DN Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"AM01_RingLight_DN Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_RingLight_DN Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_ReviewLens_UP(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_Review();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"ReviewLens_UP Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnAM01SolInterLockCheck_Review_UP()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutI.Out26_OutputReviewCylinderUpSol = 1;
					G_MotCtrlObj.m_IO_OutI.Out27_OutputReviewCylinderDownSol = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_Review();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"ReviewLens_UP Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"ReviewLens_UP Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutI.Out26_OutputReviewCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out27_OutputReviewCylinderDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"ReviewLens_UP Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"ReviewLens_UP Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"ReviewLens_UP Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"ReviewLens_UP Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_AM01_ReviewLens_DN(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_Review();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnAM01SolInterLockCheck_Review_DN()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutI.Out26_OutputReviewCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out27_OutputReviewCylinderDownSol = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetAM01State_CySol_Review();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"AM01_ReviewLens_DN Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"AM01_ReviewLens_DN Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutI.Out26_OutputReviewCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out27_OutputReviewCylinderDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"AM01_ReviewLens_DN Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"AM01_ReviewLens_DN Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"AM01_ReviewLens_DN Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"AM01_ReviewLens_DN Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"AM01_ReviewLens_DN Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_UT02_TABLE_UP(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetUT02State_CySol_TABLE();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"UT02_TABLE_UP Ret_Ok");
			}break;
		case SOL_DOWN:
			{
				if( G_MotCtrlObj.m_fnUT02SolInterLockCheck_TABLE_UP()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutI.Out20_OutputTableCylinderUpSol = 1;
					G_MotCtrlObj.m_IO_OutI.Out21_OutputTableCylinderDownSol = 0;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetUT02State_CySol_TABLE();
						if(CylinenderNowState ==  SOL_UP)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"UT02_TABLE_UP Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"UT02_TABLE_UP Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutI.Out20_OutputTableCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out21_OutputTableCylinderDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"UT02_TABLE_UP Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"UT02_TABLE_UP Send CMD Worrying");
				}
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"UT02_TABLE_UP Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"UT02_TABLE_UP Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_UT02_TABLE_DN(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int CylinenderNowState = G_MotCtrlObj.m_fnGetUT02State_CySol_TABLE();
		switch(CylinenderNowState)
		{
		case SOL_UP:
			{
				if(G_MotCtrlObj.m_fnUT02SolInterLockCheck_TABLE_DN()==false)//false: inter Lock이 걸리지 않았다.Cylinder를 움직여되 될지를 체크 한다.
				{
					int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
					TimeOutValue *=100;//Sec->10msec
					//출력을 준다.
					G_MotCtrlObj.m_IO_OutI.Out20_OutputTableCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out21_OutputTableCylinderDownSol = 1;
					Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
					while(*pBreakProc == true)
					{
						CylinenderNowState = G_MotCtrlObj.m_fnGetUT02State_CySol_TABLE();
						if(CylinenderNowState ==  SOL_DOWN)
						{//SOL_UP
							pProcData->nResult = VS24MotData::Ret_Ok;
							G_WriteInfo(Log_Info,"UT02_TABLE_DN Ret_Ok");
							break;
						}
						TimeOutValue--;
						if(TimeOutValue<0)
						{//1분 Time Out.
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"UT02_TABLE_DN Time Out");
							break;
						}
						Sleep(10);
					}
					//출력을 꺼준다.
					G_MotCtrlObj.m_IO_OutI.Out20_OutputTableCylinderUpSol = 0;
					G_MotCtrlObj.m_IO_OutI.Out21_OutputTableCylinderDownSol = 0;
					if(*pBreakProc == false)
					{
						pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
						G_WriteInfo(Log_Worrying,"UT02_TABLE_DN Run Stop");
					}
				}
				else
				{
					pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
					G_WriteInfo(Log_Worrying,"UT02_TABLE_DN Send CMD Worrying");
				}
			}break;
		case SOL_DOWN:
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Interlock에 걸린다.
				G_WriteInfo(Log_Info,"UT02_TABLE_DN Ret_Ok");
			}break;
		case SOL_ERROR_BOTH_OFF:
		case SOL_ERROR_BOTH_ON:
		case SOL_ERROR_TIMEOUT:
		case SOL_ERROR_SENSOR:
		case SOL_NONE://상태 정의 혹은 감지 없음(사용 X) 의미적 정의.
		default:
			pProcData->nResult = VS24MotData::Ret_Error;//Interlock에 걸린다.
			G_WriteInfo(Log_Error,"UT02_TABLE_DN Sensor State Error");
			break;
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"UT02_TABLE_DN Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_Door_Release_Front(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
		{
			//if(G_MotCtrlObj.m_fnGetDoorReleaseStateFront() == true)
			//{
			//	pProcData->nResult = VS24MotData::Ret_Ok;
			//	G_WriteInfo(Log_Info,"Door_Release_Front Ret_Ok");
			//}
			//else
			{
				G_MotCtrlObj.m_fnSetDoorReleaseFront(true);
				//Sleep(1500);
				//if(G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock == 1)
				//{
				//	pProcData->nResult = VS24MotData::Ret_Ok;
				//	G_WriteInfo(Log_Info,"Door_Release_Front Ret_Ok");
				//}
				//else
				//{
				//	pProcData->nResult = VS24MotData::Ret_TimeOut;
				//	G_WriteInfo(Log_Worrying,"Door_Release_Front Time Out");
				//}
				int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:2;//기본 5초 Time Out을 사용 한다.
				TimeOutValue *=20;//Sec->2msec

				Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
				while(*pBreakProc == true)
				{
					if(G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock == 1)
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						G_WriteInfo(Log_Info,"Door_Release_Front Ret_Ok");
						break;
					}
					TimeOutValue--;
					if(TimeOutValue<0)
					{//1분 Time Out.
						pProcData->nResult = VS24MotData::Ret_TimeOut;
						G_WriteInfo(Log_Worrying,"Door_Release_Front Time Out");
						break;
					}
					Sleep(10);
				}
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;
			G_WriteInfo(Log_Worrying,"Door_Release_Front Auto Teach Mode Now");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Door_Release_Front Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_Door_Release_Side(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
		{
			//if(G_MotCtrlObj.m_fnGetDoorReleaseStateSide() == true)
			//{
			//	pProcData->nResult = VS24MotData::Ret_Ok;
			//	G_WriteInfo(Log_Info,"Door_Release_Side Ret_Ok");
			//}
			//else
			{
				G_MotCtrlObj.m_fnSetDoorReleaseSide(true);
				//Sleep(1500);
				//if(	G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 1&&	
				//	G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock == 1&&
				//	G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock == 1&&
				//	G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock == 1)	
				//{
				//	pProcData->nResult = VS24MotData::Ret_Ok;
				//	G_WriteInfo(Log_Info,"Door_Release_Side Ret_Ok");
				//}
				//else
				//{
				//	pProcData->nResult = VS24MotData::Ret_TimeOut;
				//	G_WriteInfo(Log_Worrying,"Door_Release_Side Time Out");
				//}

				int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:2;//기본 5초 Time Out을 사용 한다.
				TimeOutValue *=20;//Sec->2msec

				Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
				while(*pBreakProc == true)
				{
					if(	G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 1&&	
						G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock == 1&&
						G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock == 1&&
						G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock == 1)	
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						G_WriteInfo(Log_Info,"Door_Release_Side Ret_Ok");
						break;
					}
					TimeOutValue--;
					if(TimeOutValue<0)
					{//1분 Time Out.
						pProcData->nResult = VS24MotData::Ret_TimeOut;
						G_WriteInfo(Log_Worrying,"Door_Release_Side Time Out");
						break;
					}
					Sleep(10);
				}
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;
			G_WriteInfo(Log_Worrying,"Door_Release_Side Auto Teach Mode Now");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Door_Release_Side Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_Door_Release_Back(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
		{
			//if(G_MotCtrlObj.m_fnGetDoorReleaseStateBack() == true)
			//{
			//	pProcData->nResult = VS24MotData::Ret_Ok;
			//	G_WriteInfo(Log_Info,"Door_Release_Back Ret_Ok");
			//}
			//else
			{
				G_MotCtrlObj.m_fnSetDoorReleaseBack(true);
				//Sleep(1500);
				//if(	G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock == 1&&	
				//	G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock == 1)	
				//{
				//	pProcData->nResult = VS24MotData::Ret_Ok;
				//	G_WriteInfo(Log_Info,"Door_Release_Back Ret_Ok");
				//}
				//else
				//{
				//	pProcData->nResult = VS24MotData::Ret_TimeOut;
				//	G_WriteInfo(Log_Worrying,"Door_Release_Back Time Out");
				//}
				int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:2;//기본 5초 Time Out을 사용 한다.
				TimeOutValue *=20;//Sec->2msec

				Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
				while(*pBreakProc == true)
				{
					if(	G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock == 1&&	
						G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock == 1)	
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						G_WriteInfo(Log_Info,"Door_Release_Back Ret_Ok");
						break;
					}
					TimeOutValue--;
					if(TimeOutValue<0)
					{//1분 Time Out.
						pProcData->nResult = VS24MotData::Ret_TimeOut;
						G_WriteInfo(Log_Worrying,"Door_Release_Back Time Out");
						break;
					}
					Sleep(10);
				}
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;
			G_WriteInfo(Log_Worrying,"Door_Release_Back Auto Teach Mode Now");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Door_Release_Back Unknown Action");
	}
	return 1;
}


int CMDFunList::fn_Seq_Door_Lock_Front(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
		{
			//if(G_MotCtrlObj.m_fnGetDoorReleaseStateFront() == false)
			//{
			//	pProcData->nResult = VS24MotData::Ret_Ok;
			//	G_WriteInfo(Log_Info,"Door_Lock_Front Ret_Ok");
			//}
			//else
			{
				G_MotCtrlObj.m_fnSetDoorReleaseFront(false);
				//Sleep(1500);
				//if(	G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock == 0)	
				//{
				//	pProcData->nResult = VS24MotData::Ret_Ok;
				//	G_WriteInfo(Log_Info,"Door_Lock_Front Ret_Ok");
				//}
				//else
				//{
				//	pProcData->nResult = VS24MotData::Ret_TimeOut;
				//	G_WriteInfo(Log_Worrying,"Door_Lock_Front Time Out");
				//}
				int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:2;//기본 5초 Time Out을 사용 한다.
				TimeOutValue *=20;//Sec->2msec

				Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
				while(*pBreakProc == true)
				{
					if(G_MotCtrlObj.m_IO_InA.In04_Sn_IDXFrontDoorLock == 0)
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						G_WriteInfo(Log_Info,"Door_Lock_Front Ret_Ok");
						break;
					}
					TimeOutValue--;
					if(TimeOutValue<0)
					{//1분 Time Out.
						pProcData->nResult = VS24MotData::Ret_TimeOut;
						G_WriteInfo(Log_Worrying,"Door_Lock_Front Time Out");
						break;
					}
					Sleep(10);
				}
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;
			G_WriteInfo(Log_Worrying,"Door_Lock_Front Auto Teach Mode Now");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Door_Lock_Front Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_Door_Lock_Side(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
		{
			//if(G_MotCtrlObj.m_fnGetDoorReleaseStateSide() == false)
			//{
			//	pProcData->nResult = VS24MotData::Ret_Ok;
			//	G_WriteInfo(Log_Info,"Door_Lock_Side Ret_Ok");
			//}
			//else
			{
				G_MotCtrlObj.m_fnSetDoorReleaseSide(false);
				//Sleep(1500);
				//if(	G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 0&&	
				//	G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock == 0&&
				//	G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock == 0&&
				//	G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock == 0)	
				//{
				//	pProcData->nResult = VS24MotData::Ret_Ok;
				//	G_WriteInfo(Log_Info,"Door_Lock_Side Ret_Ok");
				//}
				//else
				//{
				//	pProcData->nResult = VS24MotData::Ret_TimeOut;
				//	G_WriteInfo(Log_Worrying,"Door_Lock_Side Time Out");
				//}
				int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:2;//기본 5초 Time Out을 사용 한다.
				TimeOutValue *=20;//Sec->2msec

				Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
				while(*pBreakProc == true)
				{
					if(	G_MotCtrlObj.m_IO_InA.In08_Sn_IDXSideDoorLeftLock == 0&&	
						G_MotCtrlObj.m_IO_InA.In10_Sn_IDXSideDoorRightLock == 0&&
						G_MotCtrlObj.m_IO_InA.In12_Sn_InspSideDoorLeftLock == 0&&
						G_MotCtrlObj.m_IO_InA.In14_Sn_InspSideDoorRightLock == 0)	
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						G_WriteInfo(Log_Info,"Door_Lock_Side Ret_Ok");
						break;
					}
					TimeOutValue--;
					if(TimeOutValue<0)
					{//1분 Time Out.
						pProcData->nResult = VS24MotData::Ret_TimeOut;
						G_WriteInfo(Log_Worrying,"Door_Lock_Side Time Out");
						break;
					}
					Sleep(10);
				}
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;
			G_WriteInfo(Log_Worrying,"Door_Lock_Side Auto Teach Mode Now");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Door_Lock_Side Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_Door_Lock_Back(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(G_MotCtrlObj.m_IO_InA.In19_AutoTeachMode == 0)
		{
			//if(G_MotCtrlObj.m_fnGetDoorReleaseStateBack() == false)
			//{
			//	pProcData->nResult = VS24MotData::Ret_Ok;
			//	G_WriteInfo(Log_Info,"Door_Lock_Back Ret_Ok");
			//}
			//else
			{
				G_MotCtrlObj.m_fnSetDoorReleaseBack(false);
				//Sleep(1500);
				//if(	G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock == 0&&	
				//	G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock == 0)	
				//{
				//	pProcData->nResult = VS24MotData::Ret_Ok;
				//	G_WriteInfo(Log_Info,"Door_Lock_Back Ret_Ok");
				//}
				//else
				//{
				//	pProcData->nResult = VS24MotData::Ret_TimeOut;
				//	G_WriteInfo(Log_Worrying,"Door_Lock_Back Time Out");
				//}

				int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:2;//기본 5초 Time Out을 사용 한다.
				TimeOutValue *=20;//Sec->2msec

				Sleep(100);//기본동식 시간을 준다.(I/O 출력시간.
				while(*pBreakProc == true)
				{
					if(	G_MotCtrlObj.m_IO_InA.In16_Sn_InspBackDoorLeftLock == 0&&	
						G_MotCtrlObj.m_IO_InA.In18_Sn_InspBackDoorRightLock == 0)	
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						G_WriteInfo(Log_Info,"Door_Lock_Back Ret_Ok");
						break;
					}
					TimeOutValue--;
					if(TimeOutValue<0)
					{//1분 Time Out.
						pProcData->nResult = VS24MotData::Ret_TimeOut;
						G_WriteInfo(Log_Worrying,"Door_Lock_Back Time Out");
						break;
					}
					Sleep(10);
				}
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;
			G_WriteInfo(Log_Worrying,"Door_Lock_Back Auto Teach Mode Now");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Door_Lock_Back Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_Set_SignalTower(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		pProcData->nResult = VS24MotData::Ret_Ok;//
		G_MotCtrlObj.m_fnSignalTModeReset((STWOER_MODE)pProcData->newPosition1);
		//G_MotCtrlObj.m_fnSignalTowerSet((STWOER_MODE)pProcData->newPosition1);
	}
	else if(pProcData->nAction == VS24MotData::Act_Home)
	{
		pProcData->nResult = VS24MotData::Ret_Ok;//
		G_MotCtrlObj.m_fnSignalTowerBuzzerOn();
	}
	else if(pProcData->nAction == VS24MotData::Act_Stop)
	{
		pProcData->nResult = VS24MotData::Ret_Ok;//
		G_MotCtrlObj.m_fnSignalTowerBuzzerOff();
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Door_Lock_Back Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff == true)
		{
			G_MotCtrlObj.m_IO_OutC.Out16_ForkVacuumSol = 1;
			//G_MotCtrlObj.m_IO_OutC.Out17_ForkVacuumSol2 = 1;
		}
		else
		{
			G_MotCtrlObj.m_IO_OutC.Out16_ForkVacuumSol = 0;
			//G_MotCtrlObj.m_IO_OutC.Out17_ForkVacuumSol2 = 0;
		}
		
		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				if(G_MotCtrlObj.m_IO_InC.In22_Sn_ForkBoxVacuum == 1)
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM01_Vac_ON Ret_Ok");
					break;
				}
			}
			else
			{
				if(G_MotCtrlObj.m_IO_InC.In22_Sn_ForkBoxVacuum == 0)
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM01_Vac_OFF Ret_Ok");
					break;
				}
			}
			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"TM01_Vac_ON Time Out");
				else
					G_WriteInfo(Log_Worrying,"TM01_Vac_OFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"TM01_Vac_ON Run Stop");
			else
				G_WriteInfo(Log_Worrying,"TM01_Vac_OFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"TM01_Vac_ON Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"TM01_Vac_OFF Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_TM01_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		G_MotCtrlObj.m_IO_OutC.Out17_ForkPurge = 1;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TM01_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		G_MotCtrlObj.m_IO_OutC.Out17_ForkPurge = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TM01_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM01_Purge Unknown Action");
	}
	return 1;
}

//LT01
int CMDFunList::fn_Seq_LT01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff ==  true)
			G_MotCtrlObj.m_IO_OutC.Out08_LoadBoxVacuum = 1;
		else
			G_MotCtrlObj.m_IO_OutC.Out08_LoadBoxVacuum = 0;

		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				if(G_MotCtrlObj.m_IO_InC.In10_Sn_LoadBoxVacuum == 1)
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"LT01_Vac_ON Ret_Ok");
					break;
				}
			}
			else
			{
				if(G_MotCtrlObj.m_IO_InC.In10_Sn_LoadBoxVacuum == 0)
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"LT01_Vac_OFF Ret_Ok");
					break;
				}
			}
			
			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"LT01_Vac_ON Time Out");
				else
					G_WriteInfo(Log_Worrying,"LT01_Vac_OFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"LT01_Vac_ON Run Stop");
			else
				G_WriteInfo(Log_Worrying,"LT01_Vac_OFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.

		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"LT01_Vac_ON Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"LT01_Vac_OFF Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_LT01_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		G_MotCtrlObj.m_IO_OutC.Out09_LoadBoxPurge = 1;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"LT01_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		G_MotCtrlObj.m_IO_OutC.Out09_LoadBoxPurge = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"LT01_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"LT01_Purge Unknown Action");
	}
	return 1;
}
//BO01
int CMDFunList::fn_Seq_BO01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff ==  true)
		{
			G_MotCtrlObj.m_IO_OutC.Out02_BoxLeftVacuum = 1;
			G_MotCtrlObj.m_IO_OutC.Out06_BoxRightVacuum = 1;
		}
		else
		{
			G_MotCtrlObj.m_IO_OutC.Out02_BoxLeftVacuum = 0;
			G_MotCtrlObj.m_IO_OutC.Out06_BoxRightVacuum = 0;
		}

		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				if( G_MotCtrlObj.m_IO_InC.In03_Sn_BoxLeftVacuum == 1 &&
					G_MotCtrlObj.m_IO_InC.In06_Sn_BoxRightVacuum == 1 )
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BO01_Vac_ON Ret_Ok");
					break;
				}
			}
			else
			{
				if( G_MotCtrlObj.m_IO_InC.In03_Sn_BoxLeftVacuum == 0 &&
					G_MotCtrlObj.m_IO_InC.In06_Sn_BoxRightVacuum == 0 )
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"BO01_Vac_OFF Ret_Ok");
					break;
				}
			}

			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"BO01_Vac_ON Time Out");
				else
					G_WriteInfo(Log_Worrying,"BO01_Vac_OFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"BO01_Vac_ON Run Stop");
			else
				G_WriteInfo(Log_Worrying,"BO01_Vac_OFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.

		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"BO01_Vac_ON Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"BO01_Vac_OFF Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_BO01_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		G_MotCtrlObj.m_IO_OutC.Out03_BoxLeftPurge = 1;
		G_MotCtrlObj.m_IO_OutC.Out07_BoxRightPurge = 1;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"BO01_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		G_MotCtrlObj.m_IO_OutC.Out03_BoxLeftPurge = 0;
		G_MotCtrlObj.m_IO_OutC.Out07_BoxRightPurge = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"BO01_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"BO01_Purge Unknown Action");
	}
	return 1;
}
//UT01
int CMDFunList::fn_Seq_UT01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff ==  true)
			G_MotCtrlObj.m_IO_OutC.Out12_UnloadBoxVacuum = 1;
		else
			G_MotCtrlObj.m_IO_OutC.Out12_UnloadBoxVacuum = 0;

		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				if(G_MotCtrlObj.m_IO_InC.In16_Sn_UnloadBoxVacuum == 1)
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"UT01_Vac_ON Ret_Ok");
					break;
				}
			}
			else
			{
				if(G_MotCtrlObj.m_IO_InC.In16_Sn_UnloadBoxVacuum == 0)
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"UT01_Vac_OFF Ret_Ok");
					break;
				}
			}

			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"UT01_Vac_ON Time Out");
				else
					G_WriteInfo(Log_Worrying,"UT01_Vac_OFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"UT01_Vac_ON Run Stop");
			else
				G_WriteInfo(Log_Worrying,"UT01_Vac_OFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.

		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"UT01_Vac_ON Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"UT01_Vac_OFF Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_UT01_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		G_MotCtrlObj.m_IO_OutC.Out13_UnloadBoxPurge = 1;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"UT01_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		G_MotCtrlObj.m_IO_OutC.Out13_UnloadBoxPurge = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"UT01_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"UT01_Purge Unknown Action");
	}
	return 1;
}
//TM02
int CMDFunList::fn_Seq_TM02_ST_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff ==  true)
		{
			if((pProcData->SelectPoint &0x0001)>0) G_MotCtrlObj.m_IO_OutE.Out00_PickerStickAbsorb01_100mm = 1; else G_MotCtrlObj.m_IO_OutE.Out00_PickerStickAbsorb01_100mm = 0;
			if((pProcData->SelectPoint &0x0002)>0) G_MotCtrlObj.m_IO_OutE.Out01_PickerStickAbsorb02_100mm = 1; else G_MotCtrlObj.m_IO_OutE.Out01_PickerStickAbsorb02_100mm = 0;
			if((pProcData->SelectPoint &0x0004)>0) G_MotCtrlObj.m_IO_OutE.Out02_PickerStickAbsorb03_100mm = 1; else G_MotCtrlObj.m_IO_OutE.Out02_PickerStickAbsorb03_100mm = 0;
			if((pProcData->SelectPoint &0x0008)>0) G_MotCtrlObj.m_IO_OutE.Out03_PickerStickAbsorb04_200mm = 1; else G_MotCtrlObj.m_IO_OutE.Out03_PickerStickAbsorb04_200mm = 0;
			if((pProcData->SelectPoint &0x0010)>0) G_MotCtrlObj.m_IO_OutE.Out04_PickerStickAbsorb05_200mm = 1; else G_MotCtrlObj.m_IO_OutE.Out04_PickerStickAbsorb05_200mm = 0;
			if((pProcData->SelectPoint &0x0020)>0) G_MotCtrlObj.m_IO_OutE.Out05_PickerStickAbsorb06_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out05_PickerStickAbsorb06_440mm = 0;
			if((pProcData->SelectPoint &0x0040)>0) G_MotCtrlObj.m_IO_OutE.Out06_PickerStickAbsorb07_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out06_PickerStickAbsorb07_440mm = 0;
			if((pProcData->SelectPoint &0x0080)>0) G_MotCtrlObj.m_IO_OutE.Out07_PickerStickAbsorb08_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out07_PickerStickAbsorb08_440mm = 0;
			if((pProcData->SelectPoint &0x0100)>0) G_MotCtrlObj.m_IO_OutE.Out08_PickerStickAbsorb09_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out08_PickerStickAbsorb09_440mm = 0;
			if((pProcData->SelectPoint &0x0200)>0) G_MotCtrlObj.m_IO_OutE.Out09_PickerStickAbsorb10_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out09_PickerStickAbsorb10_440mm = 0;

			//G_WriteInfo(Log_Normal,"TM02_ST_Vac_ON 0x%h", pProcData->SelectPoint);

		}
		else
		{
			/*if((pProcData->SelectPoint &0x0001)>0)*/ G_MotCtrlObj.m_IO_OutE.Out00_PickerStickAbsorb01_100mm = 0;
			/*if((pProcData->SelectPoint &0x0002)>0)*/ G_MotCtrlObj.m_IO_OutE.Out01_PickerStickAbsorb02_100mm = 0;
			/*if((pProcData->SelectPoint &0x0004)>0)*/ G_MotCtrlObj.m_IO_OutE.Out02_PickerStickAbsorb03_100mm = 0;
			/*if((pProcData->SelectPoint &0x0008)>0)*/ G_MotCtrlObj.m_IO_OutE.Out03_PickerStickAbsorb04_200mm = 0;
			/*if((pProcData->SelectPoint &0x0010)>0)*/ G_MotCtrlObj.m_IO_OutE.Out04_PickerStickAbsorb05_200mm = 0;
			/*if((pProcData->SelectPoint &0x0020)>0)*/ G_MotCtrlObj.m_IO_OutE.Out05_PickerStickAbsorb06_440mm = 0;
			/*if((pProcData->SelectPoint &0x0040)>0)*/ G_MotCtrlObj.m_IO_OutE.Out06_PickerStickAbsorb07_440mm = 0;
			/*if((pProcData->SelectPoint &0x0080)>0)*/ G_MotCtrlObj.m_IO_OutE.Out07_PickerStickAbsorb08_440mm = 0;
			/*if((pProcData->SelectPoint &0x0100)>0)*/ G_MotCtrlObj.m_IO_OutE.Out08_PickerStickAbsorb09_440mm = 0;
			/*if((pProcData->SelectPoint &0x0200)>0)*/ G_MotCtrlObj.m_IO_OutE.Out09_PickerStickAbsorb10_440mm = 0;

			//G_WriteInfo(Log_Normal,"TM02_ST_Vac_OFF 0x%h", pProcData->SelectPoint);

		}

		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				//if(G_MotCtrlObj.m_IO_InE.In00_Sn_PickerVacuum == 1)
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_ST_Vac_ON Ret_Ok");
					break;
				}
			}
			else
			{
				//if(G_MotCtrlObj.m_IO_InE.In00_Sn_PickerVacuum == 0)
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_ST_Vac_OFF Ret_Ok");
					break;
				}
			}

			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"TM02_ST_Vac_ON Time Out");
				else
					G_WriteInfo(Log_Worrying,"TM02_ST_Vac_OFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"TM02_ST_Vac_ON Run Stop");
			else
				G_WriteInfo(Log_Worrying,"TM02_ST_Vac_OFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.

		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"TM02_ST_Vac_ON Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"TM02_ST_Vac_OFF Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM02_ST_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutE.Out10_PickerStickPurge01_100mm = 1; else G_MotCtrlObj.m_IO_OutE.Out10_PickerStickPurge01_100mm = 0;
		if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutE.Out11_PickerStickPurge02_100mm = 1; else G_MotCtrlObj.m_IO_OutE.Out11_PickerStickPurge02_100mm = 0;
		if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutE.Out12_PickerStickPurge03_100mm = 1; else G_MotCtrlObj.m_IO_OutE.Out12_PickerStickPurge03_100mm = 0;
		if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutE.Out13_PickerStickPurge04_200mm = 1; else G_MotCtrlObj.m_IO_OutE.Out13_PickerStickPurge04_200mm = 0;
		if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutE.Out14_PickerStickPurge05_200mm = 1; else G_MotCtrlObj.m_IO_OutE.Out14_PickerStickPurge05_200mm = 0;
		if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutE.Out15_PickerStickPurge06_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out15_PickerStickPurge06_440mm = 0;
		if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutE.Out16_PickerStickPurge07_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out16_PickerStickPurge07_440mm = 0;
		if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutE.Out17_PickerStickPurge08_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out17_PickerStickPurge08_440mm = 0;
		if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutE.Out18_PickerStickPurge09_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out18_PickerStickPurge09_440mm = 0;
		if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutE.Out19_PickerStickPurge10_440mm = 1; else G_MotCtrlObj.m_IO_OutE.Out19_PickerStickPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TM02_ST_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutE.Out10_PickerStickPurge01_100mm = 0;
		/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutE.Out11_PickerStickPurge02_100mm = 0;
		/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutE.Out12_PickerStickPurge03_100mm = 0;
		/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutE.Out13_PickerStickPurge04_200mm = 0;
		/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutE.Out14_PickerStickPurge05_200mm = 0;
		/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutE.Out15_PickerStickPurge06_440mm = 0;
		/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutE.Out16_PickerStickPurge07_440mm = 0;
		/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutE.Out17_PickerStickPurge08_440mm = 0;
		/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutE.Out18_PickerStickPurge09_440mm = 0;
		/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutE.Out19_PickerStickPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TM02_ST_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM02_ST_Purge Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM02_PP_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff ==  true)
		{
			if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutF.Out00_PickerPaperAbsorb01_100mm = 1; else G_MotCtrlObj.m_IO_OutF.Out00_PickerPaperAbsorb01_100mm = 0;
			if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutF.Out01_PickerPaperAbsorb02_100mm = 1; else G_MotCtrlObj.m_IO_OutF.Out01_PickerPaperAbsorb02_100mm = 0;
			if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutF.Out02_PickerPaperAbsorb03_100mm = 1; else G_MotCtrlObj.m_IO_OutF.Out02_PickerPaperAbsorb03_100mm = 0;
			if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutF.Out03_PickerPaperAbsorb04_200mm = 1; else G_MotCtrlObj.m_IO_OutF.Out03_PickerPaperAbsorb04_200mm = 0;
			if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutF.Out04_PickerPaperAbsorb05_200mm = 1; else G_MotCtrlObj.m_IO_OutF.Out04_PickerPaperAbsorb05_200mm = 0;
			if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutF.Out05_PickerPaperAbsorb06_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out05_PickerPaperAbsorb06_440mm = 0;
			if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutF.Out06_PickerPaperAbsorb07_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out06_PickerPaperAbsorb07_440mm = 0;
			if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutF.Out07_PickerPaperAbsorb08_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out07_PickerPaperAbsorb08_440mm = 0;
			if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutF.Out08_PickerPaperAbsorb09_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out08_PickerPaperAbsorb09_440mm = 0;
			if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutF.Out09_PickerPaperAbsorb10_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out09_PickerPaperAbsorb10_440mm = 0;
			//G_WriteInfo(Log_Normal,"TM02_PP_Vac_ON 0x%h", pProcData->SelectPoint);
		}
		else
		{
			/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutF.Out00_PickerPaperAbsorb01_100mm = 0;
			/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutF.Out01_PickerPaperAbsorb02_100mm = 0;
			/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutF.Out02_PickerPaperAbsorb03_100mm = 0;
			/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutF.Out03_PickerPaperAbsorb04_200mm = 0;
			/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutF.Out04_PickerPaperAbsorb05_200mm = 0;
			/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutF.Out05_PickerPaperAbsorb06_440mm = 0;
			/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutF.Out06_PickerPaperAbsorb07_440mm = 0;
			/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutF.Out07_PickerPaperAbsorb08_440mm = 0;
			/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutF.Out08_PickerPaperAbsorb09_440mm = 0;
			/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutF.Out09_PickerPaperAbsorb10_440mm = 0;
			//G_WriteInfo(Log_Normal,"TM02_PP_Vac_OFF 0x%h", pProcData->SelectPoint);
		}

		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				//if(G_MotCtrlObj.m_IO_InE.In00_Sn_PickerVacuum == 1)//Stick과 간지 진공센서는 하나로 관리
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PP_Vac_ON Ret_Ok");
					break;
				}
			}
			else
			{
				//if(G_MotCtrlObj.m_IO_InE.In00_Sn_PickerVacuum == 0)//Stick과 간지 진공센서는 하나로 관리
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TM02_PP_Vac_OFF Ret_Ok");
					break;
				}
			}

			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"TM02_PP_Vac_ON Time Out");
				else
					G_WriteInfo(Log_Worrying,"TM02_PP_Vac_OFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"TM02_PP_Vac_ON Run Stop");
			else
				G_WriteInfo(Log_Worrying,"TM02_PP_Vac_OFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.

		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"TM02_PP_Vac_ON Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"TM02_PP_Vac_OFF Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TM02_PP_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutF.Out10_PickerPaperPurge01_100mm = 1; else G_MotCtrlObj.m_IO_OutF.Out10_PickerPaperPurge01_100mm = 0;
		if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutF.Out11_PickerPaperPurge02_100mm = 1; else G_MotCtrlObj.m_IO_OutF.Out11_PickerPaperPurge02_100mm = 0;
		if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutF.Out12_PickerPaperPurge03_100mm = 1; else G_MotCtrlObj.m_IO_OutF.Out12_PickerPaperPurge03_100mm = 0;
		if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutF.Out13_PickerPaperPurge04_200mm = 1; else G_MotCtrlObj.m_IO_OutF.Out13_PickerPaperPurge04_200mm = 0;
		if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutF.Out14_PickerPaperPurge05_200mm = 1; else G_MotCtrlObj.m_IO_OutF.Out14_PickerPaperPurge05_200mm = 0;
		if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutF.Out15_PickerPaperPurge06_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out15_PickerPaperPurge06_440mm = 0;
		if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutF.Out16_PickerPaperPurge07_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out16_PickerPaperPurge07_440mm = 0;
		if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutF.Out17_PickerPaperPurge08_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out17_PickerPaperPurge08_440mm = 0;
		if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutF.Out18_PickerPaperPurge09_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out18_PickerPaperPurge09_440mm = 0;
		if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutF.Out19_PickerPaperPurge10_440mm = 1; else G_MotCtrlObj.m_IO_OutF.Out19_PickerPaperPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TM02_PP_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutF.Out10_PickerPaperPurge01_100mm = 0;
		/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutF.Out11_PickerPaperPurge02_100mm = 0;
		/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutF.Out12_PickerPaperPurge03_100mm = 0;
		/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutF.Out13_PickerPaperPurge04_200mm = 0;
		/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutF.Out14_PickerPaperPurge05_200mm = 0;
		/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutF.Out15_PickerPaperPurge06_440mm = 0;
		/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutF.Out16_PickerPaperPurge07_440mm = 0;
		/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutF.Out17_PickerPaperPurge08_440mm = 0;
		/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutF.Out18_PickerPaperPurge09_440mm = 0;
		/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutF.Out19_PickerPaperPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TM02_PP_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TM02_PP_Purge Unknown Action");
	}
	return 1;
}
//TT01
int CMDFunList::fn_Seq_TT01_UP_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff ==  true)
		{
			if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutG.Out00_TurnTableUpperStickAbsorb01_100mm = 1; else G_MotCtrlObj.m_IO_OutG.Out00_TurnTableUpperStickAbsorb01_100mm = 0;
			if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutG.Out01_TurnTableUpperStickAbsorb02_100mm = 1; else G_MotCtrlObj.m_IO_OutG.Out01_TurnTableUpperStickAbsorb02_100mm = 0;
			if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutG.Out02_TurnTableUpperStickAbsorb03_100mm = 1; else G_MotCtrlObj.m_IO_OutG.Out02_TurnTableUpperStickAbsorb03_100mm = 0;
			if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutG.Out03_TurnTableUpperStickAbsorb04_200mm = 1; else G_MotCtrlObj.m_IO_OutG.Out03_TurnTableUpperStickAbsorb04_200mm = 0;
			if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutG.Out04_TurnTableUpperStickAbsorb05_200mm = 1; else G_MotCtrlObj.m_IO_OutG.Out04_TurnTableUpperStickAbsorb05_200mm = 0;
			if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutG.Out05_TurnTableUpperStickAbsorb06_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out05_TurnTableUpperStickAbsorb06_440mm = 0;
			if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutG.Out06_TurnTableUpperStickAbsorb07_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out06_TurnTableUpperStickAbsorb07_440mm = 0;
			if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutG.Out07_TurnTableUpperStickAbsorb08_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out07_TurnTableUpperStickAbsorb08_440mm = 0;
			if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutG.Out08_TurnTableUpperStickAbsorb09_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out08_TurnTableUpperStickAbsorb09_440mm = 0;
			if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutG.Out09_TurnTableUpperStickAbsorb10_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out09_TurnTableUpperStickAbsorb10_440mm = 0;
			//G_WriteInfo(Log_Normal,"TT01_UP_Vac_ON 0x%h", pProcData->SelectPoint);
		}
		else
		{
			/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutG.Out00_TurnTableUpperStickAbsorb01_100mm = 0;
			/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutG.Out01_TurnTableUpperStickAbsorb02_100mm = 0;
			/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutG.Out02_TurnTableUpperStickAbsorb03_100mm = 0;
			/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutG.Out03_TurnTableUpperStickAbsorb04_200mm = 0;
			/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutG.Out04_TurnTableUpperStickAbsorb05_200mm = 0;
			/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutG.Out05_TurnTableUpperStickAbsorb06_440mm = 0;
			/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutG.Out06_TurnTableUpperStickAbsorb07_440mm = 0;
			/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutG.Out07_TurnTableUpperStickAbsorb08_440mm = 0;
			/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutG.Out08_TurnTableUpperStickAbsorb09_440mm = 0;
			/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutG.Out09_TurnTableUpperStickAbsorb10_440mm = 0;
			//G_WriteInfo(Log_Normal,"TT01_UP_Vac_OFF 0x%h", pProcData->SelectPoint);
		}

		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				if(G_MotCtrlObj.m_IO_InG.In00_Sn_TurnTableUpperVacuum == 1)//Stick과 간지 진공센서는 하나로 관리
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_UP_Vac_ONOFF Ret_Ok");
					break;
				}
			}
			else
			{
				if(G_MotCtrlObj.m_IO_InG.In00_Sn_TurnTableUpperVacuum == 0)//Stick과 간지 진공센서는 하나로 관리
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_UP_Vac_ONOFF Ret_Ok");
					break;
				}
			}

			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"TT01_UP_Vac_ONOFF Time Out");
				else
					G_WriteInfo(Log_Worrying,"TT01_UP_Vac_ONOFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"TT01_UP_Vac_ONOFF Run Stop");
			else
				G_WriteInfo(Log_Worrying,"TT01_UP_Vac_ONOFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.

		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"TT01_UP_Vac_ONOFF Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"TT01_UP_Vac_ONOFF Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TT01_UP_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutG.Out10_TurnTableUpperStickPurge01_100mm = 1; else G_MotCtrlObj.m_IO_OutG.Out10_TurnTableUpperStickPurge01_100mm = 0;
		if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutG.Out11_TurnTableUpperStickPurge02_100mm = 1; else G_MotCtrlObj.m_IO_OutG.Out11_TurnTableUpperStickPurge02_100mm = 0;
		if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutG.Out12_TurnTableUpperStickPurge03_100mm = 1; else G_MotCtrlObj.m_IO_OutG.Out12_TurnTableUpperStickPurge03_100mm = 0;
		if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutG.Out13_TurnTableUpperStickPurge04_200mm = 1; else G_MotCtrlObj.m_IO_OutG.Out13_TurnTableUpperStickPurge04_200mm = 0;
		if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutG.Out14_TurnTableUpperStickPurge05_200mm = 1; else G_MotCtrlObj.m_IO_OutG.Out14_TurnTableUpperStickPurge05_200mm = 0;
		if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutG.Out15_TurnTableUpperStickPurge06_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out15_TurnTableUpperStickPurge06_440mm = 0;
		if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutG.Out16_TurnTableUpperStickPurge07_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out16_TurnTableUpperStickPurge07_440mm = 0;
		if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutG.Out17_TurnTableUpperStickPurge08_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out17_TurnTableUpperStickPurge08_440mm = 0;
		if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutG.Out18_TurnTableUpperStickPurge09_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out18_TurnTableUpperStickPurge09_440mm = 0;
		if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutG.Out19_TurnTableUpperStickPurge10_440mm = 1; else G_MotCtrlObj.m_IO_OutG.Out19_TurnTableUpperStickPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TT01_UP_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutG.Out10_TurnTableUpperStickPurge01_100mm = 0;
		/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutG.Out11_TurnTableUpperStickPurge02_100mm = 0;
		/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutG.Out12_TurnTableUpperStickPurge03_100mm = 0;
		/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutG.Out13_TurnTableUpperStickPurge04_200mm = 0;
		/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutG.Out14_TurnTableUpperStickPurge05_200mm = 0;
		/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutG.Out15_TurnTableUpperStickPurge06_440mm = 0;
		/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutG.Out16_TurnTableUpperStickPurge07_440mm = 0;
		/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutG.Out17_TurnTableUpperStickPurge08_440mm = 0;
		/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutG.Out18_TurnTableUpperStickPurge09_440mm = 0;
		/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutG.Out19_TurnTableUpperStickPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TT01_UP_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TT01_UP_Purge Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TT01_DN_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff ==  true)
		{
			if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutH.Out00_TurnTableLowerStickAbsorb01_100mm = 1; else G_MotCtrlObj.m_IO_OutH.Out00_TurnTableLowerStickAbsorb01_100mm = 0;
			if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutH.Out01_TurnTableLowerStickAbsorb02_100mm = 1; else G_MotCtrlObj.m_IO_OutH.Out01_TurnTableLowerStickAbsorb02_100mm = 0;
			if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutH.Out02_TurnTableLowerStickAbsorb03_100mm = 1; else G_MotCtrlObj.m_IO_OutH.Out02_TurnTableLowerStickAbsorb03_100mm = 0;
			if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutH.Out03_TurnTableLowerStickAbsorb04_200mm = 1; else G_MotCtrlObj.m_IO_OutH.Out03_TurnTableLowerStickAbsorb04_200mm = 0;
			if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutH.Out04_TurnTableLowerStickAbsorb05_200mm = 1; else G_MotCtrlObj.m_IO_OutH.Out04_TurnTableLowerStickAbsorb05_200mm = 0;
			if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutH.Out05_TurnTableLowerStickAbsorb06_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out05_TurnTableLowerStickAbsorb06_440mm = 0;
			if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutH.Out06_TurnTableLowerStickAbsorb07_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out06_TurnTableLowerStickAbsorb07_440mm = 0;
			if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutH.Out07_TurnTableLowerStickAbsorb08_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out07_TurnTableLowerStickAbsorb08_440mm = 0;
			if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutH.Out08_TurnTableLowerStickAbsorb09_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out08_TurnTableLowerStickAbsorb09_440mm = 0;
			if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutH.Out09_TurnTableLowerStickAbsorb10_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out09_TurnTableLowerStickAbsorb10_440mm = 0;
			//G_WriteInfo(Log_Normal,"TT01_DN_Vac_ON 0x%h", pProcData->SelectPoint);
		}
		else
		{
			/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutH.Out00_TurnTableLowerStickAbsorb01_100mm = 0;
			/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutH.Out01_TurnTableLowerStickAbsorb02_100mm = 0;
			/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutH.Out02_TurnTableLowerStickAbsorb03_100mm = 0;
			/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutH.Out03_TurnTableLowerStickAbsorb04_200mm = 0;
			/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutH.Out04_TurnTableLowerStickAbsorb05_200mm = 0;
			/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutH.Out05_TurnTableLowerStickAbsorb06_440mm = 0;
			/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutH.Out06_TurnTableLowerStickAbsorb07_440mm = 0;
			/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutH.Out07_TurnTableLowerStickAbsorb08_440mm = 0;
			/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutH.Out08_TurnTableLowerStickAbsorb09_440mm = 0;
			/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutH.Out09_TurnTableLowerStickAbsorb10_440mm = 0;
			//G_WriteInfo(Log_Normal,"TT01_DN_Vac_OFF 0x%h", pProcData->SelectPoint);
		}

		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				if(G_MotCtrlObj.m_IO_InG.In07_Sn_TurnTableDownVacuum == 1)//Stick과 간지 진공센서는 하나로 관리
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_DN_Vac_ONOFF Ret_Ok");
					break;
				}
			}
			else
			{
				if(G_MotCtrlObj.m_IO_InG.In07_Sn_TurnTableDownVacuum == 0)//Stick과 간지 진공센서는 하나로 관리
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"TT01_DN_Vac_ONOFF Ret_Ok");
					break;
				}
			}

			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"TT01_DN_Vac_ONOFF Time Out");
				else
					G_WriteInfo(Log_Worrying,"TT01_DN_Vac_ONOFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"TT01_DN_Vac_ONOFF Run Stop");
			else
				G_WriteInfo(Log_Worrying,"TT01_DN_Vac_ONOFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.

		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"TT01_DN_Vac_ONOFF Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"TT01_DN_Vac_ONOFF Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_TT01_DN_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutH.Out10_TurnTableLowerStickPurge01_100mm = 1; else G_MotCtrlObj.m_IO_OutH.Out10_TurnTableLowerStickPurge01_100mm = 0;
		if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutH.Out11_TurnTableLowerStickPurge02_100mm = 1; else G_MotCtrlObj.m_IO_OutH.Out11_TurnTableLowerStickPurge02_100mm = 0;
		if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutH.Out12_TurnTableLowerStickPurge03_100mm = 1; else G_MotCtrlObj.m_IO_OutH.Out12_TurnTableLowerStickPurge03_100mm = 0;
		if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutH.Out13_TurnTableLowerStickPurge04_200mm = 1; else G_MotCtrlObj.m_IO_OutH.Out13_TurnTableLowerStickPurge04_200mm = 0;
		if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutH.Out14_TurnTableLowerStickPurge05_200mm = 1; else G_MotCtrlObj.m_IO_OutH.Out14_TurnTableLowerStickPurge05_200mm = 0;
		if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutH.Out15_TurnTableLowerStickPurge06_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out15_TurnTableLowerStickPurge06_440mm = 0;
		if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutH.Out16_TurnTableLowerStickPurge07_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out16_TurnTableLowerStickPurge07_440mm = 0;
		if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutH.Out17_TurnTableLowerStickPurge08_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out17_TurnTableLowerStickPurge08_440mm = 0;
		if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutH.Out18_TurnTableLowerStickPurge09_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out18_TurnTableLowerStickPurge09_440mm = 0;
		if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutH.Out19_TurnTableLowerStickPurge10_440mm = 1; else G_MotCtrlObj.m_IO_OutH.Out19_TurnTableLowerStickPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TT01_DN_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutH.Out10_TurnTableLowerStickPurge01_100mm = 0;
		/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutH.Out11_TurnTableLowerStickPurge02_100mm = 0;
		/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutH.Out12_TurnTableLowerStickPurge03_100mm = 0;
		/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutH.Out13_TurnTableLowerStickPurge04_200mm = 0;
		/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutH.Out14_TurnTableLowerStickPurge05_200mm = 0;
		/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutH.Out15_TurnTableLowerStickPurge06_440mm = 0;
		/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutH.Out16_TurnTableLowerStickPurge07_440mm = 0;
		/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutH.Out17_TurnTableLowerStickPurge08_440mm = 0;
		/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutH.Out18_TurnTableLowerStickPurge09_440mm = 0;
		/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutH.Out19_TurnTableLowerStickPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"TT01_DN_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"TT01_DN_Purge Unknown Action");
	}
	return 1;
}
//UT02
int CMDFunList::fn_Seq_UT02_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(OnOff ==  true)
		{
			if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutI.Out00_OutPutStickAbsorb01_100mm = 1; else G_MotCtrlObj.m_IO_OutI.Out00_OutPutStickAbsorb01_100mm = 0;
			if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutI.Out01_OutPutStickAbsorb02_100mm = 1; else G_MotCtrlObj.m_IO_OutI.Out01_OutPutStickAbsorb02_100mm = 0;
			if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutI.Out02_OutPutStickAbsorb03_100mm = 1; else G_MotCtrlObj.m_IO_OutI.Out02_OutPutStickAbsorb03_100mm = 0;
			if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutI.Out03_OutPutStickAbsorb04_200mm = 1; else G_MotCtrlObj.m_IO_OutI.Out03_OutPutStickAbsorb04_200mm = 0;
			if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutI.Out04_OutPutStickAbsorb05_200mm = 1; else G_MotCtrlObj.m_IO_OutI.Out04_OutPutStickAbsorb05_200mm = 0;
			if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutI.Out05_OutPutStickAbsorb06_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out05_OutPutStickAbsorb06_440mm = 0;
			if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutI.Out06_OutPutStickAbsorb07_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out06_OutPutStickAbsorb07_440mm = 0;
			if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutI.Out07_OutPutStickAbsorb08_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out07_OutPutStickAbsorb08_440mm = 0;
			if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutI.Out08_OutPutStickAbsorb09_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out08_OutPutStickAbsorb09_440mm = 0;
			if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutI.Out09_OutPutStickAbsorb10_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out09_OutPutStickAbsorb10_440mm = 0;
			//G_WriteInfo(Log_Normal,"UT02_Vac_ON 0x%h", pProcData->SelectPoint);
		}
		else
		{
			/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutI.Out00_OutPutStickAbsorb01_100mm = 0;
			/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutI.Out01_OutPutStickAbsorb02_100mm = 0;
			/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutI.Out02_OutPutStickAbsorb03_100mm = 0;
			/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutI.Out03_OutPutStickAbsorb04_200mm = 0;
			/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutI.Out04_OutPutStickAbsorb05_200mm = 0;
			/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutI.Out05_OutPutStickAbsorb06_440mm = 0;
			/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutI.Out06_OutPutStickAbsorb07_440mm = 0;
			/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutI.Out07_OutPutStickAbsorb08_440mm = 0;
			/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutI.Out08_OutPutStickAbsorb09_440mm = 0;
			/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutI.Out09_OutPutStickAbsorb10_440mm = 0;
			//G_WriteInfo(Log_Normal,"UT02_Vac_OFF 0x%h", pProcData->SelectPoint);
		}

		Sleep(100);//기본동식 시간을 준다.
		while(*pBreakProc == true)
		{
			if(OnOff == true)
			{
				if(G_MotCtrlObj.m_IO_InI.In04_Sn_OutputTableVacuum == 1)//Stick과 간지 진공센서는 하나로 관리
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"UT02_Vac_ON Ret_Ok");
					break;
				}
			}
			else
			{
				if(G_MotCtrlObj.m_IO_InI.In04_Sn_OutputTableVacuum == 0)//Stick과 간지 진공센서는 하나로 관리
				{//SOL_UP
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"UT02_Vac_OFF Ret_Ok");
					break;
				}
			}

			TimeOutValue--;
			if(TimeOutValue<0)
			{//1분 Time Out.
				pProcData->nResult = VS24MotData::Ret_TimeOut;
				if(OnOff == true)
					G_WriteInfo(Log_Worrying,"UT02_Vac_ONOFF Time Out");
				else
					G_WriteInfo(Log_Worrying,"UT02_Vac_ONOFF Time Out");
				break;
			}
			Sleep(10);
		}
		if(*pBreakProc == false)
		{
			pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
			if(OnOff == true)
				G_WriteInfo(Log_Worrying,"UT02_Vac_ONOFF Run Stop");
			else
				G_WriteInfo(Log_Worrying,"UT02_Vac_ONOFF Run Stop");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.

		if(OnOff == true)
			G_WriteInfo(Log_Worrying,"UT02_Vac_ONOFF Unknown Action");
		else
			G_WriteInfo(Log_Worrying,"UT02_Vac_ONOFF Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_UT02_Purge(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int PurgeTime = pProcData->ActionTime>0?pProcData->ActionTime:2000;//기본 2초 Purge을 사용 한다.
		//출력을 준다.
		if((pProcData->SelectPoint &0x0001)>0)  G_MotCtrlObj.m_IO_OutI.Out10_OutPutStickPurge01_100mm = 1; else G_MotCtrlObj.m_IO_OutI.Out10_OutPutStickPurge01_100mm = 0 ;
		if((pProcData->SelectPoint &0x0002)>0)  G_MotCtrlObj.m_IO_OutI.Out11_OutPutStickPurge02_100mm = 1; else G_MotCtrlObj.m_IO_OutI.Out11_OutPutStickPurge02_100mm = 0 ;
		if((pProcData->SelectPoint &0x0004)>0)  G_MotCtrlObj.m_IO_OutI.Out12_OutPutStickPurge03_100mm = 1; else G_MotCtrlObj.m_IO_OutI.Out12_OutPutStickPurge03_100mm = 0 ;
		if((pProcData->SelectPoint &0x0008)>0)  G_MotCtrlObj.m_IO_OutI.Out13_OutPutStickPurge04_200mm = 1; else G_MotCtrlObj.m_IO_OutI.Out13_OutPutStickPurge04_200mm = 0 ;
		if((pProcData->SelectPoint &0x0010)>0)  G_MotCtrlObj.m_IO_OutI.Out14_OutPutStickPurge05_200mm = 1; else G_MotCtrlObj.m_IO_OutI.Out14_OutPutStickPurge05_200mm = 0 ;
		if((pProcData->SelectPoint &0x0020)>0)  G_MotCtrlObj.m_IO_OutI.Out15_OutPutStickPurge06_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out15_OutPutStickPurge06_440mm = 0 ;
		if((pProcData->SelectPoint &0x0040)>0)  G_MotCtrlObj.m_IO_OutI.Out16_OutPutStickPurge07_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out16_OutPutStickPurge07_440mm = 0 ;
		if((pProcData->SelectPoint &0x0080)>0)  G_MotCtrlObj.m_IO_OutI.Out17_OutPutStickPurge08_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out17_OutPutStickPurge08_440mm = 0 ;
		if((pProcData->SelectPoint &0x0100)>0)  G_MotCtrlObj.m_IO_OutI.Out18_OutPutStickPurge09_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out18_OutPutStickPurge09_440mm = 0 ;
		if((pProcData->SelectPoint &0x0200)>0)  G_MotCtrlObj.m_IO_OutI.Out19_OutPutStickPurge10_440mm = 1; else G_MotCtrlObj.m_IO_OutI.Out19_OutPutStickPurge10_440mm = 0 ;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"UT02_Purge ON");
		Sleep(PurgeTime);
		//출력을 해지한다.
		/*if((pProcData->SelectPoint &0x0001)>0)*/  G_MotCtrlObj.m_IO_OutI.Out10_OutPutStickPurge01_100mm = 0;
		/*if((pProcData->SelectPoint &0x0002)>0)*/  G_MotCtrlObj.m_IO_OutI.Out11_OutPutStickPurge02_100mm = 0;
		/*if((pProcData->SelectPoint &0x0004)>0)*/  G_MotCtrlObj.m_IO_OutI.Out12_OutPutStickPurge03_100mm = 0;
		/*if((pProcData->SelectPoint &0x0008)>0)*/  G_MotCtrlObj.m_IO_OutI.Out13_OutPutStickPurge04_200mm = 0;
		/*if((pProcData->SelectPoint &0x0010)>0)*/  G_MotCtrlObj.m_IO_OutI.Out14_OutPutStickPurge05_200mm = 0;
		/*if((pProcData->SelectPoint &0x0020)>0)*/  G_MotCtrlObj.m_IO_OutI.Out15_OutPutStickPurge06_440mm = 0;
		/*if((pProcData->SelectPoint &0x0040)>0)*/  G_MotCtrlObj.m_IO_OutI.Out16_OutPutStickPurge07_440mm = 0;
		/*if((pProcData->SelectPoint &0x0080)>0)*/  G_MotCtrlObj.m_IO_OutI.Out17_OutPutStickPurge08_440mm = 0;
		/*if((pProcData->SelectPoint &0x0100)>0)*/  G_MotCtrlObj.m_IO_OutI.Out18_OutPutStickPurge09_440mm = 0;
		/*if((pProcData->SelectPoint &0x0200)>0)*/  G_MotCtrlObj.m_IO_OutI.Out19_OutPutStickPurge10_440mm = 0;
		Sleep(30);//최소 출력 나가는 시간.
		G_WriteInfo(Log_Normal,"UT02_Purge OFF");
		pProcData->nResult = VS24MotData::Ret_Ok;
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"UT02_Purge Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_Light_Scan(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(G_LightCtlrObj.LinkOpen() == true)
		{

			CString strStartObj;
			if((pProcData->SelectPoint&0x01)>0)
			{
				G_LightCtlrObj.SetLightValue(LIGHT_CH_1, (pProcData->newPosition1&0x00FF));
				strStartObj.AppendFormat(", Ring(%d)", pProcData->newPosition1);
				Sleep(100);//기본동식 시간을 준다.
			}
			if((pProcData->SelectPoint&0x02)>0)
			{
				G_LightCtlrObj.SetLightValue(LIGHT_CH_2, (pProcData->newPosition2&0x00FF));
				strStartObj.AppendFormat(", Back(%d)", pProcData->newPosition2);
				Sleep(100);//기본동식 시간을 준다.
			}
			if((pProcData->SelectPoint&0x04)>0)
			{
				G_LightCtlrObj.SetLightValue(LIGHT_CH_3, (pProcData->newPosition3&0x00FF));
				strStartObj.AppendFormat(", Reflect(%d)", pProcData->newPosition3);
				Sleep(100);//기본동식 시간을 준다.
			}
			//if((pProcData->SelectPoint&0x08)>0)
			//{
			//	G_LightCtlrObj.SetLightValue(LIGHT_CH_4, (pProcData->newPosition4&0x00FF));
			//	strStartObj.AppendFormat(", Align");
			//}
			
			while(*pBreakProc == true)
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Default Set
				if((pProcData->SelectPoint&0x01)>0)
				{
					if(G_LightCtlrObj.m_LightValue[LIGHT_CH_1-1] != (pProcData->newPosition1&0x00FF))
						pProcData->nResult = VS24MotData::Ret_Error;
				}
				if((pProcData->SelectPoint&0x02)>0)
				{
					if(G_LightCtlrObj.m_LightValue[LIGHT_CH_2-1] != (pProcData->newPosition2&0x00FF))
						pProcData->nResult = VS24MotData::Ret_Error;
				}
				if((pProcData->SelectPoint&0x04)>0)
				{
					if(G_LightCtlrObj.m_LightValue[LIGHT_CH_3-1] != (pProcData->newPosition3&0x00FF))
						pProcData->nResult = VS24MotData::Ret_Error;
				}
				//if((pProcData->SelectPoint&0x08)>0)
				//{
				//	if(G_LightCtlrObj.m_LightValue[LIGHT_CH_4-1] != (pProcData->newPosition4&0x00FF))
				//		pProcData->nResult = VS24MotData::Ret_Error;
				//}

				if(pProcData->nResult == VS24MotData::Ret_Ok)
				{
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"Light_Scan Ret_Ok(%s)", strStartObj.GetBuffer( strStartObj.GetLength()));
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"Light_Scan Time Out(%s)", strStartObj.GetBuffer( strStartObj.GetLength()));
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"Light_Scan Run Stop");
			}
			
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"Light_Scan_Ring Link Error");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Light_Scan Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_Light_Align(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec
		//출력을 준다.
		if(G_LightCtlrObj.LinkOpen() == true)
		{

			CString strStartObj;
			if((pProcData->SelectPoint&0x01)>0)
			{
				G_LightCtlrObj.SetLightValue(LIGHT_CH_4, (pProcData->newPosition1&0x00FF));
				strStartObj.AppendFormat(", Ring");
			}
			//if((pProcData->SelectPoint&0x02)>0)
			//{
			//	G_LightCtlrObj[1].SetLightValue(LIGHT_CH_2, (pProcData->newPosition2&0x00FF));
			//	strStartObj.AppendFormat(", Back");
			//}
			//if((pProcData->SelectPoint&0x04)>0)
			//{
			//	G_LightCtlrObj[1].SetLightValue(LIGHT_CH_3, (pProcData->newPosition3&0x00FF));
			//	strStartObj.AppendFormat(", Reflect");
			//}
			//if((pProcData->SelectPoint&0x08)>0)
			//{
			//	G_LightCtlrObj[1].SetLightValue(LIGHT_CH_4, (pProcData->newPosition4&0x00FF));
			//	strStartObj.AppendFormat(", None");
			//}
			Sleep(100);//기본동식 시간을 준다.
			while(*pBreakProc == true)
			{
				pProcData->nResult = VS24MotData::Ret_Ok;//Default Set
				if((pProcData->SelectPoint&0x01)>0)
				{
					if(G_LightCtlrObj.m_LightValue[LIGHT_CH_4-1] != (pProcData->newPosition1&0x00FF))
						pProcData->nResult = VS24MotData::Ret_Error;
				}
				//if((pProcData->SelectPoint&0x02)>0)
				//{
				//	if(G_LightCtlrObj[1].m_LightValue[LIGHT_CH_2-1] != (pProcData->newPosition2&0x00FF))
				//		pProcData->nResult = VS24MotData::Ret_Error;
				//}
				//if((pProcData->SelectPoint&0x04)>0)
				//{
				//	if(G_LightCtlrObj[1].m_LightValue[LIGHT_CH_3-1] != (pProcData->newPosition3&0x00FF))
				//		pProcData->nResult = VS24MotData::Ret_Error;
				//}
				//if((pProcData->SelectPoint&0x08)>0)
				//{
				//	if(G_LightCtlrObj[1].m_LightValue[LIGHT_CH_4-1] != (pProcData->newPosition4&0x00FF))
				//		pProcData->nResult = VS24MotData::Ret_Error;
				//}

				if(pProcData->nResult == VS24MotData::Ret_Ok)
				{
					pProcData->nResult = VS24MotData::Ret_Ok;
					G_WriteInfo(Log_Info,"LoadCell_L_Start Ret_Ok(%s)", strStartObj.GetBuffer( strStartObj.GetLength()));
					break;
				}
				TimeOutValue--;
				if(TimeOutValue<0)
				{//1분 Time Out.
					pProcData->nResult = VS24MotData::Ret_TimeOut;
					G_WriteInfo(Log_Worrying,"Light_Scan Time Out(%s)", strStartObj.GetBuffer( strStartObj.GetLength()));
					break;
				}
				Sleep(10);
			}
			if(*pBreakProc == false)
			{
				pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
				G_WriteInfo(Log_Worrying,"Light_Scan Run Stop");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"Light_Scan_Ring Link Error");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Light_Scan Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_LoadCell_L_SetZero(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(pProcData->SelectPoint>=0)
		{
			int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
			TimeOutValue *=100;//Sec->10msec
			//출력을 준다.
			pProcData->nResult = VS24MotData::Ret_Ok;//Default Set
			if((pProcData->SelectPoint&0x01)>0)
			{
				if(G_TesionCtlrObj[0].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x02)>0)
			{
				if(G_TesionCtlrObj[1].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x04)>0)
			{
				if(G_TesionCtlrObj[2].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x08)>0)
			{
				if(G_TesionCtlrObj[3].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}

			if(pProcData->nResult == VS24MotData::Ret_Ok)
			{
				CString strStartObj;
				if((pProcData->SelectPoint&0x01)>0)
				{
					G_TesionCtlrObj[0].SetZeroPos();
					strStartObj.AppendFormat(", 1");
				}
				if((pProcData->SelectPoint&0x02)>0)
				{
					G_TesionCtlrObj[1].SetZeroPos();
					strStartObj.AppendFormat(", 2");
				}
				if((pProcData->SelectPoint&0x04)>0)
				{
					G_TesionCtlrObj[2].SetZeroPos();
					strStartObj.AppendFormat(", 3");
				}
				if((pProcData->SelectPoint&0x08)>0)
				{
					G_TesionCtlrObj[3].SetZeroPos();
					strStartObj.AppendFormat(", 4");
				}
				pProcData->nResult = VS24MotData::Ret_Ok;
				G_WriteInfo(Log_Info,"fn_Seq_LoadCell_L_SetZero Ret_Ok(%s)",
					strStartObj.GetBuffer( strStartObj.GetLength()));
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
				G_WriteInfo(Log_Worrying,"fn_Seq_LoadCell_L_SetZero Link Error");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"fn_Seq_LoadCell_L_SetZero None Select Target");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"fn_Seq_LoadCell_L_SetZero Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_LoadCell_R_SetZero(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(pProcData->SelectPoint>=0)
		{
			int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
			TimeOutValue *=100;//Sec->10msec
			//출력을 준다.
			pProcData->nResult = VS24MotData::Ret_Ok;//Default Set
			if((pProcData->SelectPoint&0x01)>0)
			{
				if(G_TesionCtlrObj[4].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x02)>0)
			{
				if(G_TesionCtlrObj[5].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x04)>0)
			{
				if(G_TesionCtlrObj[6].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x08)>0)
			{
				if(G_TesionCtlrObj[7].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}

			if(pProcData->nResult == VS24MotData::Ret_Ok)
			{
				CString strStartObj;
				if((pProcData->SelectPoint&0x01)>0)
				{
					G_TesionCtlrObj[4].SetZeroPos();
					strStartObj.AppendFormat(", 1");
				}
				if((pProcData->SelectPoint&0x02)>0)
				{
					G_TesionCtlrObj[5].SetZeroPos();
					strStartObj.AppendFormat(", 2");
				}
				if((pProcData->SelectPoint&0x04)>0)
				{
					G_TesionCtlrObj[6].SetZeroPos();
					strStartObj.AppendFormat(", 3");
				}
				if((pProcData->SelectPoint&0x08)>0)
				{
					G_TesionCtlrObj[7].SetZeroPos();
					strStartObj.AppendFormat(", 4");
				}
				pProcData->nResult = VS24MotData::Ret_Ok;
				G_WriteInfo(Log_Info,"fn_Seq_LoadCell_R_SetZero Ret_Ok(%s)",
					strStartObj.GetBuffer( strStartObj.GetLength()));
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
				G_WriteInfo(Log_Worrying,"fn_Seq_LoadCell_R_SetZero Link Error");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"fn_Seq_LoadCell_R_SetZero None Select Target");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"fn_Seq_LoadCell_R_SetZero Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_LoadCell_L_Start(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(pProcData->SelectPoint>=0)
		{
			int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
			TimeOutValue *=100;//Sec->10msec
			//출력을 준다.
			pProcData->nResult = VS24MotData::Ret_Ok;//Default Set
			if((pProcData->SelectPoint&0x01)>0)
			{
				if(G_TesionCtlrObj[0].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x02)>0)
			{
				if(G_TesionCtlrObj[1].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x04)>0)
			{
				if(G_TesionCtlrObj[2].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x08)>0)
			{
				if(G_TesionCtlrObj[3].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}

			if(pProcData->nResult == VS24MotData::Ret_Ok)
			{
				CString strStartObj;
				if((pProcData->SelectPoint&0x01)>0)
				{
					G_TesionCtlrObj[0].SetStreamMod(OnOff);//Stream Mode는 자동)
					strStartObj.AppendFormat(", 1");
				}
				if((pProcData->SelectPoint&0x02)>0)
				{
					G_TesionCtlrObj[1].SetStreamMod(OnOff);//Stream Mode는 자동)
					strStartObj.AppendFormat(", 2");
				}
				if((pProcData->SelectPoint&0x04)>0)
				{
					G_TesionCtlrObj[2].SetStreamMod(OnOff);//Stream Mode는 자동)
					strStartObj.AppendFormat(", 3");
				}
				if((pProcData->SelectPoint&0x08)>0)
				{
					G_TesionCtlrObj[3].SetStreamMod(OnOff);//Stream Mode는 자동)
					strStartObj.AppendFormat(", 4");
				}
				pProcData->nResult = VS24MotData::Ret_Ok;
				G_WriteInfo(Log_Info,"LoadCell_L_Start Ret_Ok(%s : %s)",(OnOff==true?"ON":"OFF"), 
					strStartObj.GetBuffer( strStartObj.GetLength()));
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
				G_WriteInfo(Log_Worrying,"LoadCell_L_Start Link Error");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"LoadCell_L_Start None Select Target");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"LoadCell_L_Start Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_LoadCell_R_Start(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(pProcData->SelectPoint>=0)
		{
			int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
			TimeOutValue *=100;//Sec->10msec
			//출력을 준다.
			pProcData->nResult = VS24MotData::Ret_Ok;//Default Set
			if((pProcData->SelectPoint&0x01)>0)
			{
				if(G_TesionCtlrObj[4].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x02)>0)
			{
				if(G_TesionCtlrObj[5].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x04)>0)
			{
				if(G_TesionCtlrObj[6].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}
			if((pProcData->SelectPoint&0x08)>0)
			{
				if(G_TesionCtlrObj[7].LinkOpen() == false)
					pProcData->nResult = VS24MotData::Ret_Error;
			}

			if(pProcData->nResult == VS24MotData::Ret_Ok)
			{
				CString strStartObj;
				if((pProcData->SelectPoint&0x01)>0)
				{
					G_TesionCtlrObj[4].SetStreamMod(OnOff);//Stream Mode는 자동)
					strStartObj.AppendFormat(", 1");
				}
				if((pProcData->SelectPoint&0x02)>0)
				{
					G_TesionCtlrObj[5].SetStreamMod(OnOff);//Stream Mode는 자동)
					strStartObj.AppendFormat(", 2");
				}
				if((pProcData->SelectPoint&0x04)>0)
				{
					G_TesionCtlrObj[6].SetStreamMod(OnOff);//Stream Mode는 자동)
					strStartObj.AppendFormat(", 3");
				}
				if((pProcData->SelectPoint&0x08)>0)
				{
					G_TesionCtlrObj[7].SetStreamMod(OnOff);//Stream Mode는 자동)
					strStartObj.AppendFormat(", 4");
				}
				pProcData->nResult = VS24MotData::Ret_Ok;
				G_WriteInfo(Log_Info,"LoadCell_R_Start Ret_Ok(%s : %s)",(OnOff==true?"ON":"OFF"), 
					strStartObj.GetBuffer( strStartObj.GetLength()));
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
				G_WriteInfo(Log_Worrying,"LoadCell_R_Start Link Error");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"LoadCell_R_Start None Select Target");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"LoadCell_R_Start Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_Read_QR(bool *pBreakProc, VS24MotData*pProcData, CString *pReturnValue)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(pProcData->SelectPoint>=0)
		{
			int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
			TimeOutValue *=100;//Sec->10msec
			//출력을 준다.
			pProcData->nResult = VS24MotData::Ret_Ok;//Default Set

			if(G_QRCtlrObj.LinkOpen()== true)
			{
				G_QRCtlrObj.m_LastReadCode.Format("");//이전것을 지우고
				G_QRCtlrObj.SetTriggerOnOff(true);
			
				Sleep(100);//기본동식 시간을 준다.
				while(*pBreakProc == true)
				{
					if(G_QRCtlrObj.m_LastReadCode.GetLength()>3)//신규입력 갱신을 보자.
					{
						if(G_QRCtlrObj.m_LastReadCode.CompareNoCase("READ_TIME_OUT") == 0)
						{
							pProcData->nResult = VS24MotData::Ret_TimeOut;
							G_WriteInfo(Log_Worrying,"Read_QR Time Out");
						}
						else
						{
							pProcData->nResult = VS24MotData::Ret_Ok;
							pReturnValue->Format("%s", G_QRCtlrObj.m_LastReadCode.GetBuffer( G_QRCtlrObj.m_LastReadCode.GetLength()));
							G_WriteInfo(Log_Info,"Read_QR Ret_Ok(%s)", G_QRCtlrObj.m_LastReadCode.GetBuffer( G_QRCtlrObj.m_LastReadCode.GetLength()));
						}
						break;
					}
					TimeOutValue--;
					if(TimeOutValue<0 || G_QRCtlrObj.m_LastReadCode.CompareNoCase("READ_TIME_OUT") == 0)
					{//1분 Time Out.
						pProcData->nResult = VS24MotData::Ret_TimeOut;
						G_WriteInfo(Log_Worrying,"Read_QR Time Out");
						break;
					}
					Sleep(10);
				}
				if(*pBreakProc == false)
				{
					pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
					G_WriteInfo(Log_Worrying,"Read_QR Run Stop");
				}
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
				G_WriteInfo(Log_Worrying,"Read_QR Link Error");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"Read_QR None Select Target");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Read_QR Unknown Action");
	}
	return 1;
}

int CMDFunList::fn_Seq_SPaceSensor(bool *pBreakProc, VS24MotData*pProcData, bool OnOff)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		if(pProcData->SelectPoint>=0)
		{
			int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
			TimeOutValue *=100;//Sec->10msec
			//출력을 준다.
			if(G_SpaceCtrObj.LinkOpen()== true)
			{
				CString strStartObj;
				G_SpaceCtrObj.SetStreamMod(OnOff);//Stream Mode는 자동)
				pProcData->nResult = VS24MotData::Ret_Ok;
				G_WriteInfo(Log_Info,"SPaceSensor Ret_Ok(%s : %s)",(OnOff==true?"Start":"Stop"), 
					strStartObj.GetBuffer( strStartObj.GetLength()));
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
				G_WriteInfo(Log_Worrying,"SPaceSensor Link Error");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"SPaceSensor None Select Target");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"SPaceSensor Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_Read_AirN2(bool *pBreakProc, VS24MotData*pProcData)
{
	if(pProcData->nAction == VS24MotData::Act_None)
	{
		int TimeOutValue = pProcData->TimeOut>0?pProcData->TimeOut:5;//기본 5초 Time Out을 사용 한다.
		TimeOutValue *=100;//Sec->10msec

		pProcData->newPosition1 = -1;
		pProcData->newPosition2 = -1;
		//출력을 준다.
		if(G_AirCtlrObj.LinkOpen() == true)
		{
			if(G_AirCtlrObj.ReadOutAll() == true)
			{
				Sleep(100);//기본동식 시간을 준다.
				while(*pBreakProc == true)
				{
					if(G_AirCtlrObj.m_NowValue[0] > 0 && G_AirCtlrObj.m_NowValue[1]  > 0 )
					{
						pProcData->nResult = VS24MotData::Ret_Ok;
						pProcData->newPosition1 = G_AirCtlrObj.m_NowValue[0];
						pProcData->newPosition2 = G_AirCtlrObj.m_NowValue[1];
						G_WriteInfo(Log_Info,"Read_AirN2 Ret_Ok(Air:%d, N2:%d)", G_AirCtlrObj.m_NowValue[0], G_AirCtlrObj.m_NowValue[1]);
						break;
					}
					TimeOutValue--;
					if(TimeOutValue<0)
					{//1분 Time Out.
						pProcData->nResult = VS24MotData::Ret_TimeOut;
						G_WriteInfo(Log_Worrying,"Read_AirN2 Time Out");
						break;
					}
					Sleep(10);
				}
				if(*pBreakProc == false)
				{
					pProcData->nResult = VS24MotData::Ret_Error;//동작 체크 구동 정지에 의한 오류.
					G_WriteInfo(Log_Worrying,"Read_AirN2 Run Stop");
				}
			}
			else
			{
				pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
				G_WriteInfo(Log_Worrying,"Read_AirN2 CMD Send Error");
			}
		}
		else
		{
			pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
			G_WriteInfo(Log_Worrying,"Read_AirN2 Link Error");
		}
	}
	else
	{
		pProcData->nResult = VS24MotData::Ret_Error;//처리 할수 없는 명령.
		G_WriteInfo(Log_Worrying,"Read_AirN2 Unknown Action");
	}
	return 1;
}
int CMDFunList::fn_Seq_SetTrigger_Inspection(bool *pBreakProc, VS24MotData*pProcData)
{
	G_MotCtrlObj.fn_SetTrigger_Inspection((float)(pProcData->newPosition1/10.0f));
	return 1;
}
int CMDFunList::fn_Seq_SetTrigger_AreaScan(bool *pBreakProc, VS24MotData*pProcData)
{
	G_MotCtrlObj.fn_SetTrigger_AreaScan((float)(pProcData->newPosition1/10.0f));
	return 1;
}

CMDFunList G_CMDFnObj;


int G_CmtThProc= 0;
UINT Thread_VS24CMDProc(LPVOID pParam)
{
	G_WriteInfo(Log_Info, "★ = MOT Thread Start = ♠");
	CString QRCodeResult;
	QRCodeResult.Format("");
	FnProcThData *pData = (FnProcThData *)pParam;
	if(pData->m_RunThread == true)
	{
		switch(pData->uFunID_Dest)
		{
		case nBiz_Func_MotionAct:
			{
				switch(pData->uSeqID_Dest)
				{
						//TT01
				case nBiz_Seq_TT01_TableDrive:
					{ 
						G_CMDFnObj.fn_Seq_TT01_TableDrive(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_TT01_ThetaAlign:
					{
						G_CMDFnObj.fn_Seq_TT01_ThetaAlign(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_TT01_TableTurn:
					{
						G_CMDFnObj.fn_Seq_TT01_TableTurn(&pData->m_RunThread, &pData->nMotionData);
					}break;
					//BT01
				case nBiz_Seq_BT01_CassetteUpDown:
					{
						G_CMDFnObj.fn_Seq_BT01_CassetteUpDown(&pData->m_RunThread, &pData->nMotionData);
					}break;
					//LT01
				case nBiz_Seq_LT01_BoxLoadPush:
					{
						G_CMDFnObj.fn_Seq_LT01_BoxLoadPush(&pData->m_RunThread, &pData->nMotionData);
					}break;
					//BO01
				case nBiz_Seq_BO01_BoxRotate:
					{
						G_CMDFnObj.fn_Seq_BO01_BoxRotate(&pData->m_RunThread, &pData->nMotionData);
					}break;				
				case nBiz_Seq_BO01_BoxUpDn:
					{
						G_CMDFnObj.fn_Seq_BO01_BoxUpDn(&pData->m_RunThread, &pData->nMotionData);
					}break;				
				case nBiz_Seq_BO01_BoxDrive	:
					{
						G_CMDFnObj.fn_Seq_BO01_BoxDrive(&pData->m_RunThread, &pData->nMotionData);
					}break;			
					//UT01
				case nBiz_Seq_UT01_BoxUnLoadPush:
					{
						G_CMDFnObj.fn_Seq_UT01_BoxUnLoadPush(&pData->m_RunThread, &pData->nMotionData);
					}break;
					//TM01
				case nBiz_Seq_TM01_ForwardBackward	:
					{
						G_CMDFnObj.fn_Seq_TM01_ForwardBackward(&pData->m_RunThread, &pData->nMotionData);
					}break;

					//TM02
				case nBiz_Seq_TM02_PickerDrive	:
					{
						G_CMDFnObj.fn_Seq_TM02_PickerDrive(&pData->m_RunThread, &pData->nMotionData);
					}break;	
				case nBiz_Seq_TM02_PickerUpDown	:
					{
						G_CMDFnObj.fn_Seq_TM02_PickerUpDown(&pData->m_RunThread, &pData->nMotionData);
					}break;	
				case nBiz_Seq_TM02_PickerShift	:
					{
						G_CMDFnObj.fn_Seq_TM02_PickerShift(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_TM02_PickerRotate:
					{
						G_CMDFnObj.fn_Seq_TM02_PickerRotate(&pData->m_RunThread, &pData->nMotionData);
					}break;

					//AM01
				case nBiz_Seq_AM01_ScanDrive:
					{
						G_CMDFnObj.fn_Seq_AM01_ScanDrive(&pData->m_RunThread, &pData->nMotionData);
					}break;	
				case nBiz_Seq_AM01_ScanShift:
					{
						G_CMDFnObj.fn_Seq_AM01_ScanShift(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_ScanUpDn	:
					{
						G_CMDFnObj.fn_Seq_AM01_ScanUpDn(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_AM01_SpaceSnUpDn	:
					{
						G_CMDFnObj.fn_Seq_AM01_SpaceSnUpDn(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_ScopeDrive:
					{
						G_CMDFnObj.fn_Seq_AM01_ScopeDrive(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_AM01_ScopeShift	:
					{
						G_CMDFnObj.fn_Seq_AM01_ScopeShift(&pData->m_RunThread, &pData->nMotionData);
					}break;	
				case nBiz_Seq_AM01_ScopeUpDn:
					{
						G_CMDFnObj.fn_Seq_AM01_ScopeUpDn(&pData->m_RunThread, &pData->nMotionData);
					}break;			
				case nBiz_Seq_AM01_AirBlowLeftRight:
					{
						G_CMDFnObj.fn_Seq_AM01_AirBlowLeftRight(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_MultiMoveScan:
					{
						G_CMDFnObj.fn_Seq_AM01_ScanMultiMove(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_MultiMoveScofe:
					{
						G_CMDFnObj.fn_Seq_AM01_ScopeMultiMove(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_SyncMoveScanScofe:
					{
						G_CMDFnObj.fn_Seq_AM01_ScanScopeSyncMove(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_SyncMoveScopeSpaceZ:
					{
						G_CMDFnObj.fn_Seq_AM01_ScopeSpaceZSyncMove(&pData->m_RunThread, &pData->nMotionData);
					}break;
				//case nBiz_Seq_AM01_TensionLeftUpDn:
				case nBiz_Seq_AM01_TensionUpDn:
					{
						G_CMDFnObj.fn_Seq_AM01_TensionUpDn(&pData->m_RunThread, &pData->nMotionData);
					}break;
				//case nBiz_Seq_AM01_TensionRightUpDn:
				//	{
				//		G_CMDFnObj.fn_Seq_AM01_TensionRightUpDn(&pData->m_RunThread, &pData->nMotionData);
				//	}break;
				case nBiz_Seq_AM01_TensionStart:
					{
						G_CMDFnObj.fn_Seq_AM01_TensionStart(&pData->m_RunThread, &pData->nMotionData);
					}break;	
				case nBiz_Seq_AM01_SpaceMeasurStart:
					{
						G_CMDFnObj.fn_Seq_AM01_SpaceMeasureStart(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_LTension1	:
					{
						G_CMDFnObj.fn_Seq_AM01_LGripper1(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_AM01_LTension2:
					{
						G_CMDFnObj.fn_Seq_AM01_LGripper2(&pData->m_RunThread, &pData->nMotionData);
					}break;	
				case nBiz_Seq_AM01_LTension3:
					{
						G_CMDFnObj.fn_Seq_AM01_LGripper3(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_AM01_LTension4:
					{
						G_CMDFnObj.fn_Seq_AM01_LGripper4(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_AM01_RTension1:
					{
						G_CMDFnObj.fn_Seq_AM01_RGripper1(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_AM01_RTension2:
					{
						G_CMDFnObj.fn_Seq_AM01_RGripper2(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_AM01_RTension3:
					{
						G_CMDFnObj.fn_Seq_AM01_RGripper3(&pData->m_RunThread, &pData->nMotionData);
					}break;		
				case nBiz_Seq_AM01_RTension4:
					{
						G_CMDFnObj.fn_Seq_AM01_RGripper4(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_JigInout:
					{
						G_CMDFnObj.fn_Seq_AM01_JigInOut(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_MultiLTension:
					{
						G_CMDFnObj.fn_Seq_AM01_LTensionMulti(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_MultiRTension:
					{
						G_CMDFnObj.fn_Seq_AM01_RTensionMulti(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_ReviewLensOffset:
					{
						G_CMDFnObj.fn_Seq_AM01_SetReviewLensOffset(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_ScopeLensOffset:
					{
						G_CMDFnObj.fn_Seq_AM01_SetScopeLensOffset(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_SetTrigger_Inspect:
					{
						G_CMDFnObj.fn_Seq_SetTrigger_Inspection(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_SetTrigger_AreaScan:
					{
						G_CMDFnObj.fn_Seq_SetTrigger_AreaScan(&pData->m_RunThread, &pData->nMotionData);
					}break;
				default:
					break;
				}
			}break;
		case nBiz_Func_CylinderAct:
			{
				switch(pData->uSeqID_Dest)
				{
				case nBiz_Seq_TM01_FORK_UP:
					{
						G_CMDFnObj.fn_Seq_TM01_FORK_UP(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_TM01_FORK_DN:
					{
						G_CMDFnObj.fn_Seq_TM01_FORK_DN(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_LT01_GUIDE_FWD:
					{
						G_CMDFnObj.fn_Seq_LT01_GUIDE_FWD(&pData->m_RunThread, &pData->nMotionData);
					}break;	
				case nBiz_Seq_LT01_GUIDE_BWD:
					{
						G_CMDFnObj.fn_Seq_LT01_GUIDE_BWD(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_UT01_GUIDE_FWD:
					{
						G_CMDFnObj.fn_Seq_UT01_GUIDE_FWD(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_UT01_GUIDE_BWD:
					{
						G_CMDFnObj.fn_Seq_UT01_GUIDE_BWD(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_BO01_PICKER_FWD:
					{
						G_CMDFnObj.fn_Seq_BO01_PICKER_FWD(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_BO01_PICKER_BWD:
					{
						G_CMDFnObj.fn_Seq_BO01_PICKER_BWD(&pData->m_RunThread, &pData->nMotionData);
					}break;
				//case nBiz_Seq_BO01_RIGHT_PICKER_FWD:
				//	{
				//		G_CMDFnObj.fn_Seq_BO01_RIGHT_PICKER_FWD(&pData->m_RunThread, &pData->nMotionData);
				//	}break;
				//case nBiz_Seq_BO01_RIGHT_PICKER_BWD:
				//	{
				//		G_CMDFnObj.fn_Seq_BO01_RIGHT_PICKER_BWD(&pData->m_RunThread, &pData->nMotionData);
				//	}break;
				case nBiz_Seq_TM02_LR_PAPER_PICKER_UP:
					{
						G_CMDFnObj.fn_Seq_TM02_LR_PAPER_PICKER_UP(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_TM02_LR_PAPER_PICKER_DN:
					{
						G_CMDFnObj.fn_Seq_TM02_LR_PAPER_PICKER_DN(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_TM02_CNT_PAPER_PICKER_UP:
					{
						G_CMDFnObj.fn_Seq_TM02_CNT_PAPER_PICKER_UP(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_TM02_CNT_PAPER_PICKER_DN:
					{
						G_CMDFnObj.fn_Seq_TM02_CNT_PAPER_PICKER_DN(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_OPEN_TEN_GRIPPER	:
					{
						G_CMDFnObj.fn_Seq_AM01_OPEN_TEN_GRIPPER(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_CLOSE_TEN_GRIPPER:
					{
						G_CMDFnObj.fn_Seq_AM01_CLOSE_TEN_GRIPPER(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_TENTION_FWD:
					{
						G_CMDFnObj.fn_Seq_AM01_TENTION_FWD(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_TENTION_BWD:
					{
						G_CMDFnObj.fn_Seq_AM01_TENTION_BWD(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_Doontuk_ON:
					{
						G_CMDFnObj.fn_Seq_AM01_DoonTuk_Laser_On(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_Doontuk_OFF:
					{
						G_CMDFnObj.fn_Seq_AM01_DoonTuk_Laser_Off(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_RingLight_UP:
					{
						G_CMDFnObj.fn_Seq_AM01_RingLight_UP(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_RingLight_DN:
					{
						G_CMDFnObj.fn_Seq_AM01_RingLight_DN(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_AM01_Review_UP:
					{
						G_CMDFnObj.fn_Seq_AM01_ReviewLens_UP(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_AM01_Review_DN:
					{
						G_CMDFnObj.fn_Seq_AM01_ReviewLens_DN(&pData->m_RunThread, &pData->nMotionData);
					}break;


				case nBiz_Seq_UT02_TABLE_UP:
					{
						G_CMDFnObj.fn_Seq_UT02_TABLE_UP(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_UT02_TABLE_DN:
					{
						G_CMDFnObj.fn_Seq_UT02_TABLE_DN(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_Door_Release_Front:
					{
						G_CMDFnObj.fn_Seq_Door_Release_Front(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_Door_Release_Side:
					{
						G_CMDFnObj.fn_Seq_Door_Release_Side(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_Door_Release_Back:
					{
						G_CMDFnObj.fn_Seq_Door_Release_Back(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_Door_Lock_Front:
					{
						G_CMDFnObj.fn_Seq_Door_Lock_Front(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_Door_Lock_Side:
					{
						G_CMDFnObj.fn_Seq_Door_Lock_Side(&pData->m_RunThread, &pData->nMotionData);
					}break;

				case nBiz_Seq_Door_Lock_Back:
					{
						G_CMDFnObj.fn_Seq_Door_Lock_Back(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_SetSignalTower:
					{
						G_CMDFnObj.fn_Seq_Set_SignalTower(&pData->m_RunThread, &pData->nMotionData);
					}break;
				default:
					break;
				}
			}break;
			

		case nBiz_Func_ACC:
			{
				switch(pData->uSeqID_Dest)
				{
			case nBiz_Seq_Light_Scan:
				{
					G_CMDFnObj.fn_Seq_Light_Scan(&pData->m_RunThread, &pData->nMotionData);
				}break;
			case nBiz_Seq_Light_Align:
				{
					G_CMDFnObj.fn_Seq_Light_Align(&pData->m_RunThread, &pData->nMotionData);
				}break;
			case nBiz_Seq_LoadCell_L_ZeroSet:
				{
					G_CMDFnObj.fn_Seq_LoadCell_L_SetZero(&pData->m_RunThread, &pData->nMotionData);
				}break;
			case nBiz_Seq_LoadCell_R_ZeroSet:
				{
					G_CMDFnObj.fn_Seq_LoadCell_R_SetZero(&pData->m_RunThread, &pData->nMotionData);
				}break;
			case nBiz_Seq_LoadCell_L_Start	:
			case nBiz_Seq_LoadCell_L_Stop :
				{
					if(pData->uSeqID_Dest == nBiz_Seq_LoadCell_L_Start)
						G_CMDFnObj.fn_Seq_LoadCell_L_Start(&pData->m_RunThread, &pData->nMotionData, true);
					else
						G_CMDFnObj.fn_Seq_LoadCell_L_Start(&pData->m_RunThread, &pData->nMotionData, false);
				}break;
			case nBiz_Seq_LoadCell_R_Start	:
			case nBiz_Seq_LoadCell_R_Stop :
				{
					pData->nMotionData.nResult = VS24MotData::Ret_Error;
					G_WriteInfo(Log_Error, "LoadCell_Right Not USE");
					//if(pData->uSeqID_Dest == nBiz_Seq_LoadCell_R_Start)
					//	G_CMDFnObj.fn_Seq_LoadCell_R_Start(&pData->m_RunThread, &pData->nMotionData, true);
					//else
					//	G_CMDFnObj.fn_Seq_LoadCell_R_Start(&pData->m_RunThread, &pData->nMotionData, false);
				}break;
			case nBiz_Seq_Read_QR:
				{
					G_CMDFnObj.fn_Seq_Read_QR(&pData->m_RunThread, &pData->nMotionData, &QRCodeResult);
				}break;
			case nBiz_Seq_SpaceSensorStart:
				{
					G_CMDFnObj.fn_Seq_SPaceSensor(&pData->m_RunThread, &pData->nMotionData,true);
				}break;
			case nBiz_Seq_SpaceSensorStop:
				{
					G_CMDFnObj.fn_Seq_SPaceSensor(&pData->m_RunThread, &pData->nMotionData,true);
				}break;
			case nBiz_Seq_Read_AirN2:
				{
					G_CMDFnObj.fn_Seq_Read_AirN2(&pData->m_RunThread, &pData->nMotionData);
				}break;
			default:
				break;
				}
			}break;
		case nBiz_Func_Vac:
			{
				//m_stLT01_Mess.nAction = m_stLT01_Mess.Act_None;
				//m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;	
				//m_stLT01_Mess.ActionTime = nPurgeTime;  //500ms
				//m_stLT01_Mess.TimeOut_IO = NORMAL_TIMEOUT;
				switch(pData->uSeqID_Dest)
				{
					//TM01
				case nBiz_Seq_TM01_Vac_ON	:
					{
						G_CMDFnObj.fn_Seq_TM01_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_TM01_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_TM01_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_TM01_Purge	:
					{
						G_CMDFnObj.fn_Seq_TM01_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;
					//LT01
				case nBiz_Seq_LT01_Vac_ON	:
					{
						G_CMDFnObj.fn_Seq_LT01_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_LT01_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_LT01_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_LT01_Purge	:
					{
						G_CMDFnObj.fn_Seq_LT01_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;
					//BO01
				case nBiz_Seq_BO01_Vac_ON		:
					{
						G_CMDFnObj.fn_Seq_BO01_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_BO01_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_BO01_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_BO01_Purge	:
					{
						G_CMDFnObj.fn_Seq_BO01_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;
					//UT01
				case nBiz_Seq_UT01_Vac_ON		:
					{
						G_CMDFnObj.fn_Seq_UT01_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_UT01_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_UT01_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_UT01_Purge	:
					{
						G_CMDFnObj.fn_Seq_UT01_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;

					//TM02
				case nBiz_Seq_TM02_ST_Vac_ON	:
					{
						G_CMDFnObj.fn_Seq_TM02_ST_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_TM02_ST_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_TM02_ST_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_TM02_ST_Purge	:
					{
						G_CMDFnObj.fn_Seq_TM02_ST_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_TM02_PP_Vac_ON	:
					{
						G_CMDFnObj.fn_Seq_TM02_PP_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_TM02_PP_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_TM02_PP_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_TM02_PP_Purge	:
					{
						G_CMDFnObj.fn_Seq_TM02_PP_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;
					//TT01
				case nBiz_Seq_TT01_UP_Vac_ON	:
					{
						G_CMDFnObj.fn_Seq_TT01_UP_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_TT01_UP_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_TT01_UP_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_TT01_UP_Purge	:
					{
						G_CMDFnObj.fn_Seq_TT01_UP_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;
				case nBiz_Seq_TT01_DN_Vac_ON	:
					{
						G_CMDFnObj.fn_Seq_TT01_DN_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_TT01_DN_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_TT01_DN_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_TT01_DN_Purge	:
					{
						G_CMDFnObj.fn_Seq_TT01_DN_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;

					//UT02
				case nBiz_Seq_UT02_Vac_ON		:
					{
						G_CMDFnObj.fn_Seq_UT02_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, true);
					}break;
				case nBiz_Seq_UT02_Vac_OFF	:
					{
						G_CMDFnObj.fn_Seq_UT02_Vac_ONOFF(&pData->m_RunThread, &pData->nMotionData, false);
					}break;
				case nBiz_Seq_UT02_Purge	:
					{
						G_CMDFnObj.fn_Seq_UT02_Purge(&pData->m_RunThread, &pData->nMotionData);
					}break;
				default:
					break;
				}
			}break;

		case nBiz_Func_Status:
			{
				switch(pData->uSeqID_Dest)
				{
				case nBiz_Seq_BT01:
				case nBiz_Seq_TM01:
				case nBiz_Seq_LT01:
				case nBiz_Seq_BO01:
				case nBiz_Seq_UT01	:
				case nBiz_Seq_TM02:
				case nBiz_Seq_TT01:
				case nBiz_Seq_UT02:
				case nBiz_Seq_AM01	:
					{
						G_WriteInfo(Log_Check, "nBiz_Func_Status Not Use CMD");
					}break;
				case nBiz_Seq_ALL:
					{

					}break;
				default:
					break;
				}

			}break;
		default:
			break;
		}
		//
		Sleep(30);//꼭해야하나>????? ㅡ,.ㅡ;;;;;;

		int nMessSize = 0;
		//////////////////////////////////////////////////////////////////////////
		if(pData->m_pCMDSender != nullptr)
		{//정상 처리 된것에 대한 응답을 준다.(커멘드의 정상 처리 이다.)
			if(QRCodeResult.GetLength()>0)
			{
				nMessSize = sizeof(VS24MotData)+QRCodeResult.GetLength();
				UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
				memset((void*)chMsgBuf, 0x00, nMessSize+1);	
				memcpy((char*)chMsgBuf,(char*)&pData->nMotionData, sizeof(VS24MotData) );
				if(QRCodeResult.GetLength()>0)
				{
					memcpy((chMsgBuf+sizeof(VS24MotData)),QRCodeResult.GetBuffer(QRCodeResult.GetLength()), QRCodeResult.GetLength());
				}
				pData->m_pCMDSender->m_fnSendCommand_Nrs(
					pData->uTask_Src, pData->uFunID_Dest, pData->uSeqID_Dest, pData->uUnitID_Dest,
					nMessSize,	chMsgBuf);

				delete []chMsgBuf;	
			}
			////Send IO 요청이면. 따로 처리.
			//else if(pData->uFunID_Dest == nBiz_Func_Status && pData->uSeqID_Dest == nBiz_Seq_ALL)
			//{
			//	G_WriteInfo(Log_Info, "nBiz_Func_Status Recv");
			//	DWORD SendIOBuf[9];
			//	int SendMotPos[AXIS_Cnt];
			//	G_MotCtrlObj.GetInputData(&SendIOBuf[0]);
			//	memcpy(&SendMotPos[0], &G_MotCtrlObj.m_AxisNowPos[0], sizeof(int)*AXIS_Cnt);

			//	nMessSize = sizeof(VS24MotData)+(sizeof(DWORD)*9)+ (sizeof(int)*AXIS_Cnt);
			//	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
			//	memset((void*)chMsgBuf, 0x00, nMessSize+1);	
			//	pData->nMotionData.nResult = VS24MotData::Ret_Ok;
			//	memcpy((char*)chMsgBuf,(char*)&pData->nMotionData, sizeof(VS24MotData) );
			//	memcpy((chMsgBuf+sizeof(VS24MotData)),&SendIOBuf[0], (sizeof(DWORD)*9));
			//	memcpy((chMsgBuf+sizeof(VS24MotData)+(sizeof(DWORD)*9)),&SendMotPos[0], (sizeof(int)*AXIS_Cnt));

			//	pData->m_pCMDSender->m_fnSendCommand_Nrs(
			//		pData->uTask_Src, pData->uFunID_Dest, pData->uSeqID_Dest, pData->uUnitID_Dest,
			//		nMessSize,	chMsgBuf);

			//	delete []chMsgBuf;	

			//	G_WriteInfo(Log_Info, "nBiz_Func_Status Send");
			//}
			else
			{
				nMessSize = sizeof(VS24MotData);
				UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
				memset((void*)chMsgBuf, 0x00, nMessSize+1);	
				memcpy((char*)chMsgBuf,(char*)&pData->nMotionData, nMessSize );

				pData->m_pCMDSender->m_fnSendCommand_Nrs(
					pData->uTask_Src, pData->uFunID_Dest, pData->uSeqID_Dest, pData->uUnitID_Dest,
					nMessSize,	chMsgBuf);

				delete []chMsgBuf;	
			}
			
			if(pData->uFunID_Dest != 10 && pData->uSeqID_Dest != 10)
			{
				G_WriteInfo(Log_Info, "To(%d): F%d-S%d-U%d-D%d => Result Send",
				pData->uTask_Src, pData->uFunID_Dest, 
				pData->uSeqID_Dest, pData->uUnitID_Dest,
				nMessSize);
			}
			
		}
	}
	pData->m_RunThread = false;
	pData->m_pHandleThread = nullptr;

	//절대 다른곳에서 호출 하면 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX!!!!!!!!!!
	ProcMsgQ*	pCMDQ = (ProcMsgQ*)pData->m_pCMDQ;
	DataNodeObj*	MyQPointer = (DataNodeObj*)pData->m_MyQPointer;
	pCMDQ->QSelfDelete(MyQPointer);

	pCMDQ = nullptr;
	MyQPointer = nullptr;
	G_WriteInfo(Log_Info, "★ = MOT Thread End(%d) = ♠", G_CmtThProc++);
	return 0;
}










//////////////////////////////////////////////////////////////////////
// Defect Write 시간Format Create를 줄이기 위해서.(Overflow 일수도....)
//////////////////////////////////////////////////////////////////////

/*
*	Module Name		:	QCMDCtrlList
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

ProcMsgQ::ProcMsgQ()
{
	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_CntNode					= 0;

	InitializeCriticalSection(&m_CrtSection);
}
ProcMsgQ::~ProcMsgQ()
{
	EnterCriticalSection(&m_CrtSection);
	//QClean();
	{
		DataNodeObj * pDeleteNode = NULL;

		if(m_HeadNode.m_pNextNode != &m_TailNode)
		{
			pDeleteNode =  m_HeadNode.m_pNextNode;

			DataNodeObj * pNextNodeBuf = NULL;
			while (pDeleteNode != &m_TailNode)
			{
				pNextNodeBuf = pDeleteNode->m_pNextNode;
				delete pDeleteNode;
				pDeleteNode = pNextNodeBuf;
				pNextNodeBuf = NULL;
			}
			m_HeadNode.m_pFrontNode		= NULL;
			m_HeadNode.m_pNextNode		= &m_TailNode;

			m_TailNode.m_pFrontNode		= &m_HeadNode;
			m_TailNode.m_pNextNode		= NULL;
		}
	}
	LeaveCriticalSection(&m_CrtSection);

	DeleteCriticalSection(&m_CrtSection);
}
//현재 같은 명령이 진행중이면 추가 하지 않는다.(실행중이면 true를 반환)
bool	ProcMsgQ::QNowProcCheck(FnProcThData	*pNewData)
{

	DataNodeObj * pCheckNode = NULL;

	if(m_HeadNode.m_pNextNode != &m_TailNode)
	{
		pCheckNode =  m_HeadNode.m_pNextNode;
		DataNodeObj * pNextNodeBuf = NULL;
		while (pCheckNode != &m_TailNode)
		{
			pNextNodeBuf = pCheckNode->m_pNextNode;
			if(	pNewData->uFunID_Dest == pCheckNode->thData.uFunID_Dest &&
				pNewData->uSeqID_Dest == pCheckNode->thData.uSeqID_Dest &&
				pNewData->uUnitID_Dest == pCheckNode->thData.uUnitID_Dest)
			{
				//신규 명령이 Motion의 Stop이면 전달 한다.(언제나 실행 될 수 있어야 한다.)
				if(pNewData->nMotionData.nAction == VS24MotData::Act_Stop)
				{
					//Move나 Home 명령 수행중이 었다면 중지 시킨다.
					if(pCheckNode->thData.nMotionData.nAction == VS24MotData::Act_Move || 
						pCheckNode->thData.nMotionData.nAction == VS24MotData::Act_Home)
					{
						pCheckNode->thData.m_RunThread = false;
						//VS24MotData::Ret_Error;를 리턴 할것이다(완료 전이라면.)//Thread에서.
					}
					return false;
				}

				return true;//같은 명령을 진행중인 것이 있으면 True 를 반환
			}
			pCheckNode = pNextNodeBuf;
			pNextNodeBuf = NULL;
		}
	}
	return false;//해당명령 등록이 없다.
}
bool ProcMsgQ::QPutNode(FnProcThData	*pNewData)
{
	EnterCriticalSection(&m_CrtSection);

	if(QNowProcCheck(pNewData) == true)
	{
		LeaveCriticalSection(&m_CrtSection);
		return false;
	}

	DataNodeObj *pNewNode = new DataNodeObj;
	memcpy(&pNewNode->thData, pNewData, sizeof(FnProcThData));//Data저장.

	pNewNode->thData.m_pCMDQ = (void*)this;
	pNewNode->thData.m_MyQPointer = (void*)pNewNode;

	pNewNode->thData.m_RunThread = true;
	pNewNode->thData.m_pHandleThread = ::AfxBeginThread(Thread_VS24CMDProc, &pNewNode->thData, THREAD_PRIORITY_NORMAL);

	pNewNode->m_pFrontNode						= &m_HeadNode;
	pNewNode->m_pNextNode							= m_HeadNode.m_pNextNode;
	m_HeadNode.m_pNextNode						= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
	return true;
}

FnProcThData  ProcMsgQ::QGetNode()
{
	EnterCriticalSection(&m_CrtSection);
	FnProcThData	retData;
	
	DataNodeObj *pReturnNode;
	pReturnNode = m_TailNode.m_pFrontNode;

	if(pReturnNode ==  &m_HeadNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return retData;
	}

	m_TailNode.m_pFrontNode = pReturnNode->m_pFrontNode;
	(pReturnNode->m_pFrontNode)->m_pNextNode = &m_TailNode;

	retData = pReturnNode->thData;

	delete pReturnNode;
	m_CntNode --;

	LeaveCriticalSection(&m_CrtSection);
	return retData;
}
bool ProcMsgQ::QSelfDelete(DataNodeObj *pSelfDel)
{
	EnterCriticalSection(&m_CrtSection);
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return false;//여기 들어오면 심각한 오류 일것이다.(Q에 등록 되지 않은 Node가 지우기 위해 호출 된것 임으로.
	}
	if(pSelfDel != nullptr)//null이면 ㅡ.,ㅡ 멀까 .......
	{
		pSelfDel->m_pFrontNode->m_pNextNode = pSelfDel->m_pNextNode;
		pSelfDel->m_pNextNode->m_pFrontNode = pSelfDel->m_pFrontNode;
		delete pSelfDel;
		pSelfDel = nullptr;

		m_CntNode --;
	}
	else 
	{
		LeaveCriticalSection(&m_CrtSection);
		return false;//여기 들어오면 심각한 오류 일것이다.(Q에 등록 되지 않은 Node가 지우기 위해 호출 된것 임으로.
	}
	LeaveCriticalSection(&m_CrtSection);
	return true;
}

void ProcMsgQ::QClean(void)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pDeleteNode = nullptr;

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pDeleteNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = nullptr;
	while (pDeleteNode != &m_TailNode)
	{
		pNextNodeBuf = pDeleteNode->m_pNextNode;
		delete pDeleteNode;

		pDeleteNode = pNextNodeBuf;
		pNextNodeBuf = nullptr;
	}

	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_CntNode = 0;
	LeaveCriticalSection(&m_CrtSection);
}

