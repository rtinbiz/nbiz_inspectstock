
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원


#include <afxsock.h>            // MFC 소켓 확장

//Load Cell 5, 6, 7, 8번을 사용 할경우 
//#define TENTION_CELL_USE_5_8
//BCR를 Net Work으로 연결 할경우. QR Code Event와 같이 사용 한다.
//#define  BCR_USE_LINK

#define TIMER_MOT_POS_UPDATE				10101
#define TIMER_TOWER_SIGNAL					10102
#define SERVER_CHECK_TIMER					10103
#define TIMER_MOT_STATE_UPDATE			10104
#define TIMER_PASSWORD_INPUT				10105
#define TIMER_ACC_UPDATE						10106
#define TIMER_SYS_INIT_CHECK					10107
#define TIMER_CYLINDER_STATE_UPDATE	10108

#define TIMER_MOT_INIT_STATE				10109
//UI Hide Time Value
#define TIMER_UI_HIDE_CHECK				10110
extern int G_UI_HideCounter;
#define CNT_UI_HIDE_TIME_SEC	15
//#define  SIMUL_UI_TEST	
#define  SIMUL_CMD_TEST	 //초기화 상관없이 구동시킨다.


#define  WM_USER_ACC_LIGHT_UPDATE			 (WM_USER + 1036)   
#define  WM_USER_ACC_LOADCELL_UPDATE		 (WM_USER + 1037)  
#define  WM_USER_ACC_SpaceSensor_UPDATE	 (WM_USER + 1038)
#define  WM_USER_ACC_AIR_UPDATE				 (WM_USER + 1039)
#define  WM_USER_ACC_CODE_UPDATE			 (WM_USER + 1040)
#define  WM_USER_CYLINDER_INTERNAL_CMD	 (WM_USER + 1041)


#include "SystemMotionParam.h"


//Image Process Lib Loading
#include "..\..\CommonHeader\ImgProcLink.h"
//VS64 Interface Lib Loading
#include "..\..\CommonHeader\VS_ServerLink.h"
//UI용 Header File Link
#include "..\..\CommonHeader\UI_LinkHeader.h"

extern CInterServerInterface  *G_pServerInterface;
void G_WriteInfo(ListColor nColorType, const char* pWritLog, ...);

extern COLORREF G_Color_WinBack;
extern COLORREF G_Color_On;
extern COLORREF G_Color_Off;
extern COLORREF G_Color_Lock;
extern COLORREF G_Color_Unlock;
extern COLORREF G_Color_Open;
extern COLORREF G_Color_Close;

extern COLORREF G_Color_LightOn;
extern CFont		G_PLCStateFont;
extern CFont		G_PosEditFont;
extern CFont		G_InfoFont;

extern bool		G_SystemInitOK;

#include "MotionProcMsgQ.h"
#include "..\..\CommonHeader\Motion\MotionAxisDef.h"
#include "..\..\CommonHeader\Motion\MotionIO.h"
#include "..\..\CommonHeader\Motion\MotStatickValueDefine.h"

#include "..\..\..\CommonHeader\MCC_Command.h"

#include "..\..\CommonHeader\Sequence\SeqBase.h"
#include "MotCtrl.h"
extern CMotCtrl G_MotCtrlObj;

#include "./ACC_Class/ACCDataType.h"
#include "./ACC_Class/LightCtrl.h"
#include "./ACC_Class/AirPressCtrl.h"
#include "./ACC_Class/QRCodeCtrl.h"
#include "./ACC_Class/TensionCtrl.h"
#include "./ACC_Class/SpaceSensor.h"
#ifdef BCR_USE_LINK
	#include "BCRCtrl.h"
#endif

extern CMCC_Command G_MccCommand;

//ACC Object
extern CLightCtrl			G_LightCtlrObj;//[2];
extern CAirPressCtrl		G_AirCtlrObj;
//extern CLens3DRevCtrl	m_LensCtlrObj;
extern CQRCodeCtrl		G_QRCtlrObj;
extern CTensionCtrl		G_TesionCtlrObj[8];
extern CSpaceSensor	G_SpaceCtrObj;
#ifdef BCR_USE_LINK
	extern CBCRCtrl		G_BCRCtrlObj;
#endif

#define  LOG_TEXT_MAX_SIZE 1024
#define BT01_Coupling_OverCnt_Limit				1000

#define  MOT_MEM_SHARE   "MOT_MEMORY"
typedef struct tag_Machine_Status
{
	ALIO_A_In stNormalStatus;  //Door, Temperature, 장비 일반 사항.
	ALIO_B_In stBoxTableSensor; //Box Table, Box Detect Sensor
	ALIO_C_In stModuleStatus; //TM01, LT01, BO01, UT01
	ALIO_D_In stGripperStatus; //Gripper
	ALIO_E_In stPickerSensor;  //TM02
	ALIO_F_In stPickerCylinder;  //TM02 Cylinder
	ALIO_G_In stTurnTableSensor; //TT01 
	ALIO_H_In stReserve;
	ALIO_I_In stEmissionTable; //UT02 Status

	ALIO_A_Out		m_IO_OutA;
	ALIO_B_Out		m_IO_OutB;
	ALIO_C_Out		m_IO_OutC;
	ALIO_D_Out		m_IO_OutD;
	ALIO_E_Out		m_IO_OutE;
	ALIO_F_Out		m_IO_OutF;
	ALIO_G_Out		m_IO_OutG;
	ALIO_H_Out		m_IO_OutH;
	ALIO_I_Out		m_IO_OutI;

	int				nMotionPos[AXIS_Cnt];

	bool			m_bScopeSafetyPos;
	tag_Machine_Status()
	{
		memset(this, 0x00, sizeof(tag_Machine_Status));
	}
}STMACHINE_STATUS;
typedef struct SMem_Handle_Tag
{	
	HANDLE hFile;
	HANDLE hFMap;
	int nTSize;
	STMACHINE_STATUS *m_pMotState;
	SMem_Handle_Tag()
	{
		//memset(this, 0x00, sizeof(SFile_Handle));
		hFile = INVALID_HANDLE_VALUE;
		hFMap = INVALID_HANDLE_VALUE;

		nTSize = sizeof(STMACHINE_STATUS);
		m_pMotState = nullptr;
	}
}SMem_Handle;
extern SMem_Handle G_MotInfo;
bool OpenMotionMem(SMem_Handle *pTargetMem);
bool CloseMotionMem(SMem_Handle *pTargetMem);








#define  MOT_USE_UMAC_JIG_23AXIS 
//////////////////////////////////////////////////////////////////////////
//Motion Default IP
//PC IP Setting : 192.6.94.2
//
//////////////////////////////////////////////////////////////////////////
#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


