#pragma once
#include "afxwin.h"


UINT Thread_IPPORTCheck(LPVOID pParam);
typedef struct ListCallBackData_TAG
{
	LPVOID pCallObj;
	HTREEITEM Root1Item;
	ListCallBackData_TAG()
	{
		pCallObj = nullptr;
		Root1Item = NULL;
	}
}ListCBData;

// CAccCtrlDlg 대화 상자입니다.

class CAccCtrlDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CAccCtrlDlg)

public:
	CAccCtrlDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAccCtrlDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_ACC_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CFont m_EditFont;

	COLORREF m_btBackColor;
	//////////////////////////////////////////////////////////////////////////
	CImageList m_ImageList;
	CTreeCtrl m_SPRServerTree;
	DWORD m_ServerUpdateTime;
	void LoadSPRServerList();
	void SPR_ServerLinkCheck();
	//void SPR_ServerConnectTry();

	void static PortListCallBack(LPVOID CtrlObj, LPVOID pBuf, DWORD recvLen);
	//Server Check Thread
	bool		m_RunThread;
	CWinThread	*m_pHandleThread;


//public://ACC Ctrl Obj
//	CLightCtrl			G_LightCtlrObj[2];
//	CAirPressCtrl		G_AirCtlrObj;
//	//CLens3DRevCtrl	m_LensCtlrObj;
//	CQRCodeCtrl		G_QRCtlrObj;
//	CTensionCtrl		G_TesionCtlrObj[8];
//	CSpaceSensor		G_SpaceCtrObj;
//
//#ifdef BCR_USE_LINK
//	CBCRCtrl				G_BCRCtrlObj;
//#endif
	
public:
	void InitCtrl_UI();

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	virtual void PostNcDestroy();

	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btLightOnOff[8];
	CRoundButton2 m_btSetLightValue[2][4];
	CEdit m_edLightValue[2][4];
	CRoundButton2 m_btStartTention[8];
	//CRoundButton2 m_bt3DScofLens[5];
	CRoundButton2 m_btRefleshAir;
	CRoundButton2 m_btChangeSOL;
	CRoundButton2 m_btQRCodeRead;
	CRoundButton2 m_btSpeceSnRead;
	CRoundButton2 m_btOpenSetFile;
	afx_msg void OnBnClickedBtOpenSettingFile();

	//afx_msg void OnBnClickedBt3dLens();
	afx_msg void OnBnClickedBtOnoff();
	afx_msg void OnBnClickedBtSet();
	afx_msg void OnBnClickedBtStartLoadcell();
	afx_msg void OnBnClickedBtPRefreshAir();
	afx_msg void OnBnClickedBtChangeSolView();
	afx_msg void OnBnClickedBtReadQrcode();
	afx_msg void OnBnClickedBtSpaceSenserRead();

	LRESULT OnUserUpdateLight(WPARAM wParam, LPARAM lParam);
	LRESULT OnUserUpdateLoadCell(WPARAM wParam, LPARAM lParam);
	LRESULT OnUserUpdateSpaceSensor(WPARAM wParam, LPARAM lParam);
	LRESULT OnUserUpdateAir(WPARAM wParam, LPARAM lParam);
	LRESULT OnUserUpdateCode(WPARAM wParam, LPARAM lParam);

	
	
	//afx_msg void OnEnChangeEdValue();
	
};
