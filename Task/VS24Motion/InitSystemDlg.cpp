// InitSystem.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS24Motion.h"
#include "InitSystemDlg.h"
#include "afxdialogex.h"


// CInitSyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CInitSyDlg, CDialogEx)

CInitSyDlg::CInitSyDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInitSyDlg::IDD, pParent)
{
	m_NowStep = CMotInitSeq::SEQ_STEP_IDLE;
	m_bInitEnd = false;
}

CInitSyDlg::~CInitSyDlg()
{
}

void CInitSyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PG_INIT_STEP, m_pgInistStep);
	DDX_Control(pDX, IDC_BT_START_INIT, m_btSysInit);
	DDX_Control(pDX, IDC_BT_STOP_INIT, m_btStopInit);
}


BEGIN_MESSAGE_MAP(CInitSyDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BT_START_INIT, &CInitSyDlg::OnBnClickedBtStartInit)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_STOP_INIT, &CInitSyDlg::OnBnClickedBtStopInit)
END_MESSAGE_MAP()


// CInitSyDlg 메시지 처리기입니다.


BOOL CInitSyDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();

	m_SeqSysInitObj.fnSeqInit((void*)this, SeqFlowStep);


	SetTimer(TIMER_SYS_INIT_CHECK, 100, NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CInitSyDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CInitSyDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
}


HBRUSH CInitSyDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	//if(DLG_ID_Number == IDC_LIST_INIT_INFO)
	//	return hbr;

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CInitSyDlg::InitCtrl_UI()
{
	//////////////////////////////////////////////////////////////////////////
	m_btBackColor = G_Color_WinBack;

	m_pgInistStep.SetBarColor(RGB(255,0,0));
	m_pgInistStep.SetBkColor(RGB(150,150,150));

	m_pgInistStep.SetRange(CMotInitSeq::SEQ_STEP_IDLE, CMotInitSeq::SEQ_STEP_CNT);
	m_pgInistStep.SetPos(CMotInitSeq::SEQ_STEP_IDLE);


	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btSysInit.GetTextColor(&tColor);
		m_btSysInit.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btSysInit.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSysInit.SetTextColor(&tColor);
		m_btSysInit.SetCheckButton(true, true);
		m_btSysInit.SetFont(&tFont);

		m_btStopInit.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStopInit.SetTextColor(&tColor);
		m_btStopInit.SetCheckButton(true, true);
		m_btStopInit.SetFont(&tFont);
		
	}
}
int CInitSyDlg::SeqFlowStep(void *pObj, int NowFlowStep)
{
	CInitSyDlg*pCtrOBJ = (CInitSyDlg*)pObj;

	pCtrOBJ->m_pgInistStep.SetPos(NowFlowStep);

	switch(NowFlowStep)
	{
	case CMotInitSeq::SEQ_STEP_ERROR:
		{
		}break;
	case CMotInitSeq::SEQ_STEP_IDLE:
		{
		}break;
	case CMotInitSeq::SEQ_STEP_RUN:
		{
		}break;
		//////////////////////////////////////////////////////////////////////////



		//////////////////////////////////////////////////////////////////////////
	case CMotInitSeq::SEQ_STEP_CNT:
		{
			pCtrOBJ->m_bInitEnd = true;
		}break;
	}
	pCtrOBJ->m_NowStep = NowFlowStep;
	return 0;
}

void CInitSyDlg::OnBnClickedBtStartInit()
{
	if(m_SeqSysInitObj.fnSeqGetNowStep() == CMotInitSeq::SEQ_STEP_IDLE)
	{
		m_btSysInit.EnableWindow(FALSE);
		m_SeqSysInitObj.fnSeqFlowStart();
	}


	m_btStopInit.SetCheck(true);
	m_btStopInit.SetWindowText("Initialize Stop");
}


void CInitSyDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == TIMER_SYS_INIT_CHECK)
	{
		if(m_bInitEnd == true)
		{
			m_SeqSysInitObj.fnSeqExit();
			KillTimer(TIMER_SYS_INIT_CHECK);
			OnOK();//최종 단계에 도달 하였음으로 종료 한다.
		}
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CInitSyDlg::OnBnClickedBtStopInit()
{
	if(m_btStopInit.GetCheck() ==true)
	{//Initialize Stop
		m_SeqSysInitObj.fnSeqSetStop();

		m_btStopInit.SetCheck(false);
		m_btStopInit.SetWindowText("Exit Initialize");

		m_btSysInit.EnableWindow(TRUE);
	}
	else
	{//exit Initialize

		OnCancel();
	}
}
