// PassWord.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS24Motion.h"
#include "PassWord.h"
#include "afxdialogex.h"


// CPassWord 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPassWord, CDialogEx)

CPassWord::CPassWord(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPassWord::IDD, pParent)
{
	m_PasswordInputTimeOut = PASSWORD_INPUT_TIMEOUT_Sec;
}

CPassWord::~CPassWord()
{
}

void CPassWord::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_btOK);
	DDX_Control(pDX, IDCANCEL, m_btNG);
}


BEGIN_MESSAGE_MAP(CPassWord, CDialogEx)
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDOK, &CPassWord::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPassWord::OnBnClickedCancel)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CPassWord 메시지 처리기입니다.
BOOL CPassWord::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	m_btBackColor = G_Color_WinBack;
	CenterWindow();
	CString strCnt;
	strCnt.Format("%d", m_PasswordInputTimeOut);
	SetDlgItemText(IDC_ST_TIME_CNT, strCnt);

	m_ViewFont.CreateFont(30, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");


	GetDlgItem(IDC_ST_TIME_CNT)->SetFont(&m_ViewFont);
	GetDlgItem(IDC_ED_PASSWORD)->SetFont(&m_ViewFont);
	SetTimer(TIMER_PASSWORD_INPUT, 1000, 0);

	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btOK.GetTextColor(&tColor);
		m_btOK.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btOK.SetRoundButtonStyle(&m_tButtonStyle);
		m_btOK.SetTextColor(&tColor);
		m_btOK.SetCheckButton(false);
		m_btOK.SetFont(&tFont);

		m_btNG.SetRoundButtonStyle(&m_tButtonStyle);
		m_btNG.SetTextColor(&tColor);
		m_btNG.SetCheckButton(false);
		m_btNG.SetFont(&tFont);
	}



	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPassWord::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == TIMER_PASSWORD_INPUT && IsWindowVisible() == TRUE)
	{
		m_PasswordInputTimeOut--;
		if(m_PasswordInputTimeOut < 0)
		{
			m_PasswordInputTimeOut = PASSWORD_INPUT_TIMEOUT_Sec;
			KillTimer(TIMER_PASSWORD_INPUT);
			OnBnClickedCancel();
		}
		CString strCnt;
		strCnt.Format("%d", m_PasswordInputTimeOut);
		SetDlgItemText(IDC_ST_TIME_CNT, strCnt);

		
	}
	CDialogEx::OnTimer(nIDEvent);
}
HBRUSH CPassWord::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();


	if(DLG_ID_Number == IDC_ST_TIME_CNT)
	{
		pDC->SetTextColor(RGB(255, 255, 0)); 
		pDC->SetBkColor(RGB(0, 0, 0));
		return (HBRUSH)GetStockObject(BLACK_BRUSH);
	}

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	return hbr;
}
BOOL CPassWord::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{

		if(this->GetFocus() == GetDlgItem(IDC_ED_PASSWORD))
			OnBnClickedOk();

		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CPassWord::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strPasswordCheck;
	GetDlgItemText(IDC_ED_PASSWORD, strPasswordCheck);

	if(strPasswordCheck.GetLength()>0)
	{
		if(strPasswordCheck.CompareNoCase(PASSWORD_INPUT1)==0 ||
			strPasswordCheck.CompareNoCase(PASSWORD_INPUT2)==0 )
		{
			CDialogEx::OnOK();
		}
		else
		{
			m_PasswordInputTimeOut = PASSWORD_INPUT_TIMEOUT_Sec;
			SetDlgItemText(IDC_ED_PASSWORD, "");
		}
	}
	else
	{
		OnBnClickedCancel();
	}
}


void CPassWord::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}


void CPassWord::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow)
	{
		CEdit*pEdit = (CEdit*)GetDlgItem(IDC_ED_PASSWORD);
		pEdit->SetFocus();
	}
}
