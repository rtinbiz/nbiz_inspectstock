
// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// VS24Motion.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

CMCC_Command G_MccCommand;
//////////////////////////////////////////////////////////////////////////
COLORREF G_Color_WinBack = RGB(64, 64, 64);//= RGB(192, 192, 192);

COLORREF	G_Color_On = RGB(128,240,128);
COLORREF	G_Color_Off = RGB(128,128,128);
COLORREF	G_Color_LightOn = RGB(250,128,128);

COLORREF G_Color_Lock = RGB(240,128,128);
COLORREF G_Color_Unlock = RGB(128,128,128);

COLORREF G_Color_Open = RGB(128,240,128);
COLORREF G_Color_Close= RGB(128,128,240);


CFont			G_PLCStateFont;
CFont			G_PosEditFont;
CFont			G_InfoFont;

CMotCtrl G_MotCtrlObj;

//ACC Object
CLightCtrl			G_LightCtlrObj;//[2];
CAirPressCtrl		G_AirCtlrObj;
//CLens3DRevCtrl	m_LensCtlrObj;
CQRCodeCtrl		G_QRCtlrObj;
CTensionCtrl		G_TesionCtlrObj[8];
CSpaceSensor		G_SpaceCtrObj;
#ifdef BCR_USE_LINK
CBCRCtrl				G_BCRCtrlObj;
#endif

//Motion, ACC, IO등의 Task 구동 초기화가 수행 되었다.
bool		G_SystemInitOK = false;

CXListBox *G_pLogList = nullptr;

//UI Hide Time Value
int G_UI_HideCounter = -1;

CInterServerInterface  *G_pServerInterface = nullptr;
void  G_WriteInfo(ListColor nColorType, const char* pWritLog, ...)
{

	char bufLog[LOG_TEXT_MAX_SIZE];
	memset(bufLog, 0x00, LOG_TEXT_MAX_SIZE);
	//////////////////////////////////////////////////////////////////////////
	va_list vargs;
	va_start(vargs, pWritLog);
	int LenStr = vsprintf_s((char*)bufLog, LOG_TEXT_MAX_SIZE-1,pWritLog, (va_list)vargs);
	va_end(vargs);

	if(G_pServerInterface != nullptr)
	{
		int uLogLevel = 0;
		switch(nColorType)
		{
		case Log_Normal:		uLogLevel = LOG_LEVEL_4; 	break;
		case Log_Check:		uLogLevel = LOG_LEVEL_2; 	break;
		case Log_Worrying:	uLogLevel = LOG_LEVEL_3; 	break;
		case Log_Error:		uLogLevel = LOG_LEVEL_1; 	break;
		case Log_Info:			uLogLevel = LOG_LEVEL_2; 	break;
		case Log_Exception:	uLogLevel = LOG_LEVEL_3; 	break;
		}
		G_pServerInterface->m_fnPrintLog(TASK_24_Motion, uLogLevel, bufLog);
	}
	
	//Ns_WriteLogProc(bufLog);
	
	if(G_pLogList != nullptr)
	{
		if(G_pLogList->GetCount() > LIST_ALARM_COUNT)
			G_pLogList->DeleteString(0);
		CXListBoxAddString(G_pLogList, nColorType, bufLog);
		G_pLogList->SetScrollPos(SB_VERT , G_pLogList->GetCount(), TRUE);
		G_pLogList->SetTopIndex(G_pLogList->GetCount() - 1);	
	}
}

//Master Share Mem;
SMem_Handle					G_MotInfo;
bool OpenMotionMem(SMem_Handle *pTargetMem)
{
	if(pTargetMem == nullptr)
		return false;
	pTargetMem->hFMap = ::OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, MOT_MEM_SHARE);
	if(!pTargetMem->hFMap) {

		pTargetMem->hFMap = CreateFileMapping(pTargetMem->hFile, NULL, PAGE_READWRITE,	0, pTargetMem->nTSize, MOT_MEM_SHARE);
		if(!pTargetMem->hFMap) 
		{
			G_WriteInfo(Log_Error, "1) Mot_SMP를 생성할 수 없습니다.");
			return false;
		}
		else
			G_WriteInfo(Log_Check, "1) Mot_SMP를 신규 생성 하였습니다.");
	}
	else if(GetLastError() == ERROR_ALREADY_EXISTS)
	{
		return true;
	}
	pTargetMem->m_pMotState = (STMACHINE_STATUS *)::MapViewOfFile(pTargetMem->hFMap,FILE_MAP_ALL_ACCESS,0,0, pTargetMem->nTSize);
	if(!pTargetMem->m_pMotState)
	{
		CloseHandle(pTargetMem->hFMap);
		G_WriteInfo(Log_Error, "2) Mot_SMP를 생성할 수 없습니다.");
		return false;
	}
	return true;
}
bool CloseMotionMem(SMem_Handle *pTargetMem)
{
	if(pTargetMem == nullptr)
		return false;

	if(pTargetMem->m_pMotState)
		UnmapViewOfFile(pTargetMem->m_pMotState);
	if(pTargetMem->hFMap)
		CloseHandle(pTargetMem->hFMap);
	pTargetMem->hFile = INVALID_HANDLE_VALUE;
	pTargetMem->hFMap = INVALID_HANDLE_VALUE;
	pTargetMem->m_pMotState = nullptr;
	return true;
}
