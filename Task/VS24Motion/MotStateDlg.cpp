// MotStateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS24Motion.h"
#include "MotStateDlg.h"
#include "afxdialogex.h"


// CMotStateDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMotStateDlg, CDialogEx)

CMotStateDlg::CMotStateDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMotStateDlg::IDD, pParent)
	, m_rdSpeedMode(0)
{

	m_pCuData_StateCu = nullptr;
	m_pCuData_StateCuL = nullptr;
	m_pCuData_StateCuR = nullptr;

	m_pCuData_StateML3 = nullptr;
	m_pCuData_StateML3L = nullptr;
	m_pCuData_StateML3R = nullptr;

	m_pALink_State = nullptr;

	m_pUIUpdate = nullptr;

	InitializeCriticalSection(&m_CrtSection);
}

CMotStateDlg::~CMotStateDlg()
{
	DeleteCriticalSection(&m_CrtSection);
}

void CMotStateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_MOT_STATE1, m_GridMotState1);
	DDX_Control(pDX, IDC_GRID_MOT_STATE2, m_GridMotState2);
	DDX_Control(pDX, IDC_BT_SAVE_SPEED_PARAM, m_btSaveSpeedParam);
	DDX_Radio(pDX, IDC_RD_SELECT_1, m_rdSpeedMode);
}


BEGIN_MESSAGE_MAP(CMotStateDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BT_SAVE_SPEED_PARAM, &CMotStateDlg::OnBnClickedBtSaveSpeedParam)
	ON_EN_CHANGE(IDC_ED_NOR_MOVE_ACC_G, &CMotStateDlg::OnEnChangeEdNorMoveAccG)
END_MESSAGE_MAP()


// CMotStateDlg 메시지 처리기입니다.
void CMotStateDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;

	m_BitOnColor = RGB(128, 255, 128);
	m_BitOffColor = RGB(128, 128, 128);
	m_BitNoneColor = RGB(0, 0, 0);

	m_GridFont.CreateFont(12, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");
	m_SpeedFont.CreateFont(15, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btSaveSpeedParam.GetTextColor(&tColor);
		m_btSaveSpeedParam.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}
		m_btSaveSpeedParam.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSaveSpeedParam.SetTextColor(&tColor);
		m_btSaveSpeedParam.SetCheckButton(false);
		m_btSaveSpeedParam.SetFont(&tFont);
	}
	m_GridMotState1.SetDefCellWidth(20);
	m_GridMotState1.SetDefCellHeight(21);
	m_GridMotState1.SetEditable(FALSE);
	m_GridMotState1.SetSingleColSelection(TRUE);
	m_GridMotState1.SetRowCount(64+1);
	m_GridMotState1.SetColumnCount(4);
	m_GridMotState1.SetFixedRowCount(1);
	//m_GridMotState1.SetListMode(TRUE);
	m_GridMotState1.SetFont(&m_GridFont);
	m_GridMotState1.SetItemText(0, 0, "M");			m_GridMotState1.SetColumnWidth(0, 23);
	m_GridMotState1.SetItemText(0, 1, "L");			m_GridMotState1.SetColumnWidth(1, 20);
	m_GridMotState1.SetItemText(0, 2, "R");			m_GridMotState1.SetColumnWidth(2, 20);
	m_GridMotState1.SetItemText(0, 3, "Category");		m_GridMotState1.SetColumnWidth(3, 170);

	m_GridMotState2.SetDefCellWidth(20);
	m_GridMotState2.SetDefCellHeight(21);
	m_GridMotState2.SetEditable(FALSE);
	m_GridMotState2.SetSingleColSelection(TRUE);
	m_GridMotState2.SetRowCount(26+1);
	m_GridMotState2.SetColumnCount(2);
	m_GridMotState2.SetFixedRowCount(1);
	//m_GridMotState2.SetListMode(TRUE);
	m_GridMotState2.SetFont(&m_GridFont);
	m_GridMotState2.SetItemText(0, 0, "M");			m_GridMotState2.SetColumnWidth(0, 25);
	m_GridMotState2.SetItemText(0, 1, "Category");		m_GridMotState2.SetColumnWidth(1, 233-25);
	
	GetDlgItem(IDC_ED_NOR_MOVE_ACC_G)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_NOR_MOVE_ACC)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_NOR_MOVE_DEC)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_NOR_MOVE_SPEED)->SetFont(&m_SpeedFont);

	GetDlgItem(IDC_ED_PICKUP_MOVE_ACC_G)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_PICKUP_MOVE_ACC)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_PICKUP_MOVE_DEC)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_PICKUP_MOVE_SPEED)->SetFont(&m_SpeedFont);

	GetDlgItem(IDC_ED_TEACH_MOVE_ACC_G)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_TEACH_MOVE_ACC)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_TEACH_MOVE_DEC)->SetFont(&m_SpeedFont);
	GetDlgItem(IDC_ED_TEACH_MOVE_SPEED)->SetFont(&m_SpeedFont);

	int AddIndex = 0;
	CString strAdd;
	for(int AddStep = 1; AddStep<=64; AddStep++)
	{
		switch(AddIndex)
		{
		case   0: strAdd.Format("Reserve");break; 
		case   1: strAdd.Format("Reserve");break;
		case   2: strAdd.Format("Reserve");break;
		case   3: strAdd.Format("Reserve");break;
		case   4: strAdd.Format("Reserve");break;
		case   5: strAdd.Format("Reserve");break;
		case   6: strAdd.Format("Reserve");break;
		case   7: strAdd.Format("Reserve");break;
		case   8: strAdd.Format("Reserve");break;
		case   9: strAdd.Format("Reserve");break;
		case 10: strAdd.Format("Home Search Active");break;
		case 11: strAdd.Format("Block Request");break;
		case 12: strAdd.Format("Abort Deceleration");break;
		case 13: strAdd.Format("Desired Velocity Zero");break;
		case 14: strAdd.Format("Data Block Error");break;
		case 15: strAdd.Format("Dwell In Progress");break;
		case 16: strAdd.Format("Intergration Mode");break;
		case 17: strAdd.Format("Running Program");break;
		case 18: strAdd.Format("Open Loop Mode	");break;
		case 19: strAdd.Format("Phased Motor");break;
		case 20: strAdd.Format("Hand wheel Enabled");break;
		case 21: strAdd.Format("Positive End Limit Set");break;
		case 22: strAdd.Format("Negative End Limit Set");break;
		case 23: strAdd.Format("Motor Activated");break;
		case 24: strAdd.Format("Reserve");break;
		case 25: strAdd.Format("Reserve");break;
		case 26: strAdd.Format("Reserve");break;
		case 27: strAdd.Format("Reserve");break;
		case 28: strAdd.Format("Reserve");break;
		case 29: strAdd.Format("Reserve");break;
		case 30: strAdd.Format("Reserve");break;
		case 31: strAdd.Format("Reserve");break;

		case 32+  0: strAdd.Format("InPosition");break;
		case 32+  1: strAdd.Format("Warning Following Error");break;
		case 32+  2: strAdd.Format("Fatal Following Error");break;
		case 32+  3: strAdd.Format("Amp Fault Error");break;
		case 32+  4: strAdd.Format("Backlash Direction Flag");break;
		case 32+  5: strAdd.Format("I2 TAmp Fault Error");break;
		case 32+  6: strAdd.Format("Integrated Fatal Follow Error");break;
		case 32+  7: strAdd.Format("Trigger Move");break;
		case 32+  8: strAdd.Format("Phasing Search Error");break;
		case 32+  9: strAdd.Format("Reserve");break;
		case 32+10: strAdd.Format("Home Complete");break;
		case 32+11: strAdd.Format("Stopped Position Limit");break;
		case 32+12: strAdd.Format("Reserve");break;
		case 32+13: strAdd.Format("Reserve");break;
		case 32+14: strAdd.Format("Amplifier Enabled");break;
		case 32+15: strAdd.Format("Reserve");break;
		case 32+16: strAdd.Format("Reserve");break;
		case 32+17: strAdd.Format("Reserve");break;
		case 32+18: strAdd.Format("Reserve");break;
		case 32+19: strAdd.Format("Reserve");break;
		case 32+20: strAdd.Format("Reserve");break;
		case 32+21: strAdd.Format("Reserve");break;
		case 32+22: strAdd.Format("Reserve");break;
		case 32+23: strAdd.Format("Reserve");break;
		case 32+24: strAdd.Format("Reserve");break;
		case 32+25: strAdd.Format("Reserve");break;
		case 32+26: strAdd.Format("Reserve");break;
		case 32+27: strAdd.Format("Reserve");break;
		case 32+28: strAdd.Format("Reserve");break;
		case 32+29: strAdd.Format("Reserve");break;
		case 32+30: strAdd.Format("Reserve");break;
		case 32+31: strAdd.Format("Reserve");break;
		
		}
		if(strAdd.CompareNoCase("Reserve")==0)
			m_GridMotState1.SetRowHeight(AddStep, 0);

		m_GridMotState1.SetItemText(AddStep , 3, strAdd.GetBuffer(strAdd.GetLength()));
		AddIndex++;
	}
	m_GridMotState1.Refresh();
	AddIndex = 0;
	for(int AddStep = 1; AddStep<=16; AddStep++)
	{
		switch(AddIndex)
		{
		case   0: strAdd.Format("SRUN-펄스 출력중");break;
		case   1: strAdd.Format("SEND-펄스 출력종료");break;
		case   2: strAdd.Format("SERR-에러 발생시 1 (상태 읽어야 해지)");break;
		case   3: strAdd.Format("SALM-알람 입력시");break;
		case   4: strAdd.Format("SRDY-Servo Drive Ready 신호 입력상태");break;
		case   5: strAdd.Format("SVON-SERVO ON 출력 신호 상태");break;
		case   6: strAdd.Format("SINP-INP 입력상태");break;
		case   7: strAdd.Format("SPEL-양방향 리미트");break;
		case   8: strAdd.Format("SMEL-음방향 리미트");break;
		case   9: strAdd.Format("SORG-원점 입력");break;
		case 10: strAdd.Format("UI0-범용입력");break;
		case 11: strAdd.Format("UI1-범용입력");break;
		case 12: strAdd.Format("UI2-범용입력");break;
		case 13: strAdd.Format("UO0-범용출력");break;
		case 14: strAdd.Format("UO1-범용출력");break;
		case 15: strAdd.Format("UO2-범용출력");break;

		}
		m_GridMotState2.SetItemText(AddStep , 1, strAdd.GetBuffer(strAdd.GetLength()));
		AddIndex++;
	}
	m_GridMotState2.Refresh();
	m_GridMotState1.ShowWindow(SW_SHOW);
	m_GridMotState2.ShowWindow(SW_HIDE);


	SetTimer(TIMER_MOT_STATE_UPDATE, 100, 0);
}
void CMotStateDlg::ViewMotState_CruiserML3(int AxisNum)
{
	m_GridMotState1.ShowWindow(SW_SHOW);
	m_GridMotState2.ShowWindow(SW_HIDE);
	m_GridMotState1.Refresh();

	EnterCriticalSection(&m_CrtSection);
	m_NowViewStateAxis = AxisNum;
	m_fnSpeedTableRead(m_NowViewStateAxis);

	m_pCuData_StateCu = nullptr;
	m_pCuData_StateCuL = nullptr;
	m_pCuData_StateCuR = nullptr;

	m_pCuData_StateML3 = nullptr;
	m_pCuData_StateML3L = nullptr;
	m_pCuData_StateML3R = nullptr;

	m_pALink_State = nullptr;

	
	CString strMotName;
	switch(m_NowViewStateAxis)
	{
	case AXIS_TT01_TableDrive://Cruiser Axis #1
		{
			m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[0];
			strMotName.Format("%s", "TableDrive(Axis #1)");
		}break;
	case AXIS_AM01_ScanShift://Cruiser Axis #2
		{
			m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[1];
			strMotName.Format("%s", "ScanShift(Axis #2)");
		}break;

	case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
		{
			m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[8];//#30
			m_pCuData_StateCuL = &G_MotCtrlObj.m_LocalAxisST[2];// #3
			m_pCuData_StateCuR = &G_MotCtrlObj.m_LocalAxisST[3];// #4
			strMotName.Format("%s", "ScopeShift(Axis #3, #4, #30)");
		}break;
	case AXIS_AM01_ScopeDrive://Cruiser Axis #5
		{
			m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[4];
			strMotName.Format("%s", "ScopeDrive (Axis #5)");	
		}break;
	case AXIS_AM01_ScanDrive://Cruiser Axis #6
		{
			m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[5];
			strMotName.Format("%s", "ScanDrive (Axis #6)");
		}break;
	case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
		{
			m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[9];//#31
			m_pCuData_StateCuL = &G_MotCtrlObj.m_LocalAxisST[6];//#7
			m_pCuData_StateCuR = &G_MotCtrlObj.m_LocalAxisST[7];//#8
			m_GridMotState1.SetItemText(0, 3, "PickerDrive(Axis #7, #8, #31)");	

		}break;	
		//LT01
	case AXIS_LT01_BoxLoadPush://ML3 #14
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[4];
			strMotName.Format("%s", "BoxLoadPush(ML3 #14)");
		}break;
		//////////////////////////////////////////////////////////////////////////
	case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
		{
			m_pCuData_StateML3L = &G_MotCtrlObj.m_ML3AxisST[0];
			m_pCuData_StateML3R = &G_MotCtrlObj.m_ML3AxisST[1];
			strMotName.Format("%s", "BoxRotate(ML3 #10, #11)");

		}break;
	case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
		{
			m_pCuData_StateML3L = &G_MotCtrlObj.m_ML3AxisST[2];
			m_pCuData_StateML3R = &G_MotCtrlObj.m_ML3AxisST[3];
			strMotName.Format("%s", "BoxUpDn(ML3 #12, #13)");
		}break;
	case AXIS_BO01_BoxDrive://ML3 #16
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[6];
			strMotName.Format("%s", "Box Drive(ML3 #16)");
		}break;	
		//UT01
	case AXIS_UT01_BoxUnLoadPush://ML3 #15
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[5];
			strMotName.Format("%s", "BoxUnLoadPush(ML3 #15)");
		}break;
		//TM01
	case AXIS_TM01_ForwardBackward://ML3 #17
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[7];
			strMotName.Format("%s", "ForwardBackward(ML3 #17)");	
		}break;
	case AXIS_TT01_ThetaAlign://ML3 #18
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[8];
			strMotName.Format("%s", "ThetaAlign(ML3 #18)");	
		}break;
	case AXIS_TT01_TableTurn://ML3 #19
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[9];
			strMotName.Format("%s", "TableTurn(ML3 #19)");
		}break;
	case AXIS_TM02_PickerUpDown://ML3 #20
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[10];
			strMotName.Format("%s", "PickerUpDown(ML3 #20)");
		}break;
	case AXIS_TM02_PickerShift://ML3 #21
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[11];
			strMotName.Format("%s", "PickerShift(ML3 #21)");	
		}break;
	case AXIS_BT01_CassetteUpDown://ML3 #22
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[12];
			strMotName.Format("%s", "CassetteUpDown(ML3 #22)");
		}break;

	case AXIS_AM01_JigInOut://ML3 #23
		{
			m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[13];
			strMotName.Format("%s", "JigInOut(ML3 #23)");
		}break;
	default:
		strMotName.AppendFormat("%s", "??????");
		break;
	}
	LeaveCriticalSection(&m_CrtSection);
	m_GridMotState1.SetItemText(0, 3, strMotName.GetBuffer(strMotName.GetLength()));

}
void CMotStateDlg::ViewMotState_Axislink(int AxisNum)
{

	m_GridMotState1.ShowWindow(SW_HIDE);
	m_GridMotState2.ShowWindow(SW_SHOW);
	m_GridMotState2.Refresh();

	EnterCriticalSection(&m_CrtSection);
	m_NowViewStateAxis = AxisNum;
	m_fnSpeedTableRead(m_NowViewStateAxis);

	m_pCuData_StateCu = nullptr;
	m_pCuData_StateCuL = nullptr;
	m_pCuData_StateCuR = nullptr;

	m_pCuData_StateML3 = nullptr;
	m_pCuData_StateML3L = nullptr;
	m_pCuData_StateML3R = nullptr;

	m_pALink_State = nullptr;

	CString strMotName;
	switch(m_NowViewStateAxis)
	{
	//case AXIS_TM02_PickerRotate://AxisLink_A X
	//	{
	//		m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[0];
	//		strMotName.Format("%s", "PickerRotate (AxisLink_A X)");
	//	}break;
	case AXIS_TM02_PickerRotate://AxisLink_A Y
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[1];
		   strMotName.Format("%s", "PickerRotate (AxisLink_A Y)");
		}break;
	case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[2];
			strMotName.Format("%s", "Tension Left UpDn(AxisLink_A Z)");
		}break;
	case AXIS_AM01_TensionRightUpDn://AxisLink_A U
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[3];
			strMotName.Format("%s", "Tension Right UpDn(AxisLink_A U)");
		}break;
	case AXIS_AM01_LTension1://AxisLink_B X
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[4];
			strMotName.Format("%s", "LTension1(AxisLink_B X)");
		}break;
	case AXIS_AM01_LTension2://AxisLink_B Y
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[5];
			strMotName.Format("%s", "LTension2(AxisLink_B Y)");
		}break;
	case AXIS_AM01_LTension3://AxisLink_B Z
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[6];
			strMotName.Format("%s", "LTension3(AxisLink_B Z)");
		}break;
	case AXIS_AM01_LTension4://AxisLink_B U
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[7];
			strMotName.Format("%s", "LTension4(AxisLink_B U)");
		}break;
	case AXIS_AM01_RTension1://AxisLink_C X
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[8];
			strMotName.Format("%s", "RTension1(AxisLink_C X)");
		}break;
	case AXIS_AM01_RTension2://AxisLink_C Y
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[9];
			strMotName.Format("%s", "RTension2(AxisLink_C Y)");
		}break;
	case AXIS_AM01_RTension3://AxisLink_C Z
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[10];
			strMotName.Format("%s", "RTension3(AxisLink_C Z)");
		}break;
	case AXIS_AM01_RTension4://AxisLink_C U
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[11];
			strMotName.Format("%s", "RTension4(AxisLink_C U)");
		}break;
	case AXIS_AM01_ScanUpDn://AxisLink_D X
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[12];
			strMotName.Format("%s", "Scan Up Dn(AxisLink_D X)");
		}break;
	case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[13];
			strMotName.Format("%s", "Space Sn Up Dn(AxisLink_D Y)");
		}break;
	case AXIS_AM01_ScopeUpDn://AxisLink_D Z
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[14];
			strMotName.AppendFormat("%s", "Scope Up Dn(AxisLink_D Z)");
		}break;
	case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
		{
			m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[15];
			strMotName.Format("%s", "Air Blow(AxisLink_D U)");
		}break;
	default:
		strMotName.AppendFormat("%s", "??????");
		break;
	}
	LeaveCriticalSection(&m_CrtSection);
	m_GridMotState2.SetItemText(0, 1, strMotName.GetBuffer(strMotName.GetLength()));
}

BOOL CMotStateDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
BOOL CMotStateDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
HBRUSH CMotStateDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(DLG_ID_Number == IDC_TREE_PORT_LIST)
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CMotStateDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CDialogEx::PostNcDestroy();
	delete this;
}
void CMotStateDlg::m_fnSpeedTableRead(int SelectAxis)
{
	CString SpeedSection;
	CString SpeedAxisKey;
	char strReadData[256];
	switch(SelectAxis)
	{
	case AXIS_TM02_PickerRotate://AxisLink_A Y
	case AXIS_AM01_ScanUpDn://AxisLink_D X
	case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
	case AXIS_AM01_ScopeUpDn://AxisLink_D Z
	case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
	case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
	case AXIS_AM01_TensionRightUpDn://AxisLink_A U
	case AXIS_AM01_LTension1://AxisLink_B X
	case AXIS_AM01_LTension2://AxisLink_B Y
	case AXIS_AM01_LTension3://AxisLink_B Z
	case AXIS_AM01_LTension4://AxisLink_B U
	case AXIS_AM01_RTension1://AxisLink_C X
	case AXIS_AM01_RTension2://AxisLink_C Y
	case AXIS_AM01_RTension3://AxisLink_C Z
	case AXIS_AM01_RTension4://AxisLink_C U
		GetDlgItem(IDC_ED_NOR_MOVE_ACC_G)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_NOR_MOVE_ACC)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_NOR_MOVE_DEC)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_NOR_MOVE_SPEED)->SetWindowText("NONE");

		GetDlgItem(IDC_ED_PICKUP_MOVE_ACC_G)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_PICKUP_MOVE_ACC)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_PICKUP_MOVE_DEC)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_PICKUP_MOVE_SPEED)->SetWindowText("NONE");

		GetDlgItem(IDC_ED_TEACH_MOVE_ACC_G)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_TEACH_MOVE_ACC)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_TEACH_MOVE_DEC)->SetWindowText("NONE");
		GetDlgItem(IDC_ED_TEACH_MOVE_SPEED)->SetWindowText("NONE");

		GetDlgItem(IDC_ED_NOR_MOVE_ACC_G)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_NOR_MOVE_ACC)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_NOR_MOVE_DEC)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_NOR_MOVE_SPEED)->EnableWindow(FALSE);

		GetDlgItem(IDC_ED_PICKUP_MOVE_ACC_G)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_PICKUP_MOVE_ACC)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_PICKUP_MOVE_DEC)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_PICKUP_MOVE_SPEED)->EnableWindow(FALSE);

		GetDlgItem(IDC_ED_TEACH_MOVE_ACC_G)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_TEACH_MOVE_ACC)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_TEACH_MOVE_DEC)->EnableWindow(FALSE);
		GetDlgItem(IDC_ED_TEACH_MOVE_SPEED)->EnableWindow(FALSE);

		return ;//Axis Link쪽은 표시 하지 않는다.
		break;
	default:
		GetDlgItem(IDC_ED_NOR_MOVE_ACC_G)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_NOR_MOVE_ACC)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_NOR_MOVE_DEC)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_NOR_MOVE_SPEED)->EnableWindow(TRUE);

		GetDlgItem(IDC_ED_PICKUP_MOVE_ACC_G)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_PICKUP_MOVE_ACC)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_PICKUP_MOVE_DEC)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_PICKUP_MOVE_SPEED)->EnableWindow(TRUE);

		GetDlgItem(IDC_ED_TEACH_MOVE_ACC_G)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_TEACH_MOVE_ACC)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_TEACH_MOVE_DEC)->EnableWindow(TRUE);
		GetDlgItem(IDC_ED_TEACH_MOVE_SPEED)->EnableWindow(TRUE);
		break;
	}
	//////////////////////////////////////////////////////////////////////////
	SpeedSection.Format("%s", Section_SPEED_NORMAL);
	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_MaxAccValue);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "0.1", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_NOR_MOVE_ACC_G)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_AccTime);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection, SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_NOR_MOVE_ACC)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_DecTime);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_NOR_MOVE_DEC)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_MoveSpeed);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "50", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_NOR_MOVE_SPEED)->SetWindowText(strReadData);

	//////////////////////////////////////////////////////////////////////////
	SpeedSection.Format("%s", Section_SPEED_TEACH);
	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_MaxAccValue);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "0.1", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_TEACH_MOVE_ACC_G)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_AccTime);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection, SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_TEACH_MOVE_ACC)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_DecTime);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_TEACH_MOVE_DEC)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_MoveSpeed);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "50", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_TEACH_MOVE_SPEED)->SetWindowText(strReadData);

	//////////////////////////////////////////////////////////////////////////
	SpeedSection.Format("%s", Section_SPEED_PICKUP);
	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_MaxAccValue);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "0.1", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_PICKUP_MOVE_ACC_G)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_AccTime);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection, SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_PICKUP_MOVE_ACC)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_DecTime);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_PICKUP_MOVE_DEC)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	SpeedAxisKey.Format("%s%s", G_strAxisName[SelectAxis], Key_MoveSpeed);
	GetPrivateProfileString(SpeedSection, SpeedAxisKey, "50", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	//WritePrivateProfileString(SpeedSection,SpeedAxisKey,strReadData, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_PICKUP_MOVE_SPEED)->SetWindowText(strReadData);
	
}

void CMotStateDlg::UpdateMotStateCu()
{
	int MainRowSizeW = 170;
	if(m_pCuData_StateCu != nullptr || m_pCuData_StateCuL != nullptr || m_pCuData_StateCuR != nullptr)
	{
		int AddIndex = 0;
		if(m_pCuData_StateCu != nullptr)
		{
			m_GridMotState1.SetColumnWidth(0, 23);
			AddIndex = 0;
			for(int AddStep = 1; AddStep<=32; AddStep++)
			{
				if( ((m_pCuData_StateCu->m_AxisStA.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 0, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 0, m_BitOffColor);

				AddIndex++;
			}
			AddIndex = 0;
			for(int AddStep = 33; AddStep<=63; AddStep++)
			{
				if( ((m_pCuData_StateCu->m_AxisStB.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 0, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 0, m_BitOffColor);

				AddIndex++;
			}
		}
		else
		{
			//m_GridMotState1.SetColumnWidth(0, 23);
			//m_GridMotState1.SetColumnWidth(1, 20);
			//m_GridMotState1.SetColumnWidth(2, 20);
			m_GridMotState1.SetColumnWidth(0, 0);
			MainRowSizeW+=23;
			
		}
		//////////////////////////////////////////////////////////////////////////
		if(m_pCuData_StateCuL != nullptr)
		{
			m_GridMotState1.SetColumnWidth(1, 20);
			AddIndex = 0;
			for(int AddStep = 1; AddStep<=32; AddStep++)
			{
				if( ((m_pCuData_StateCuL->m_AxisStA.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 1, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 1, m_BitOffColor);

				AddIndex++;
			}
			AddIndex = 0;
			for(int AddStep = 33; AddStep<=63; AddStep++)
			{
				if( ((m_pCuData_StateCuL->m_AxisStB.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 1, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 1, m_BitOffColor);

				AddIndex++;
			}
		}
		else
		{
			//m_GridMotState1.SetColumnWidth(0, 23);
			//m_GridMotState1.SetColumnWidth(1, 20);
			//m_GridMotState1.SetColumnWidth(2, 20);
			m_GridMotState1.SetColumnWidth(1, 0);
			MainRowSizeW+=20;
		}
		//////////////////////////////////////////////////////////////////////////
		if(m_pCuData_StateCuR != nullptr)
		{
			m_GridMotState1.SetColumnWidth(2, 20);
			AddIndex = 0;
			for(int AddStep = 1; AddStep<=32; AddStep++)
			{
				if( ((m_pCuData_StateCuR->m_AxisStA.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 2, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 2, m_BitOffColor);

				AddIndex++;
			}
			AddIndex = 0;
			for(int AddStep = 33; AddStep<=63; AddStep++)
			{
				if( ((m_pCuData_StateCuR->m_AxisStB.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 2, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 2, m_BitOffColor);

				AddIndex++;
			}
		}
		else
		{
			//m_GridMotState1.SetColumnWidth(0, 23);
			//m_GridMotState1.SetColumnWidth(1, 20);
			//m_GridMotState1.SetColumnWidth(2, 20);
			m_GridMotState1.SetColumnWidth(2, 0);
			MainRowSizeW+=20;
		}
	}
	m_GridMotState1.SetColumnWidth(3, MainRowSizeW);
	m_GridMotState1.Refresh();
}

void CMotStateDlg::UpdateMotStateML3()
{
	int MainRowSizeW = 170;
	if(m_pCuData_StateML3 != nullptr || m_pCuData_StateML3L != nullptr || m_pCuData_StateML3R != nullptr)
	{
		int AddIndex = 0;
		if(m_pCuData_StateML3 != nullptr)
		{
			m_GridMotState1.SetColumnWidth(0, 23);
			AddIndex = 0;
			for(int AddStep = 1; AddStep<=32; AddStep++)
			{
				if( ((m_pCuData_StateML3->m_AxisStA.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 0, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 0, m_BitOffColor);

				AddIndex++;
			}
			AddIndex = 0;
			for(int AddStep = 33; AddStep<=63; AddStep++)
			{
				if( ((m_pCuData_StateML3->m_AxisStB.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 0, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 0, m_BitOffColor);

				AddIndex++;
			}
		}
		else
		{
			//m_GridMotState1.SetColumnWidth(0, 23);
			//m_GridMotState1.SetColumnWidth(1, 20);
			//m_GridMotState1.SetColumnWidth(2, 20);
			m_GridMotState1.SetColumnWidth(0, 0);
			MainRowSizeW+=23;
		}
		//////////////////////////////////////////////////////////////////////////
		if(m_pCuData_StateML3L != nullptr)
		{
			m_GridMotState1.SetColumnWidth(1, 20);
			AddIndex = 0;
			for(int AddStep = 1; AddStep<=32; AddStep++)
			{
				if( ((m_pCuData_StateML3L->m_AxisStA.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 1, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 1, m_BitOffColor);

				AddIndex++;
			}
			AddIndex = 0;
			for(int AddStep = 33; AddStep<=63; AddStep++)
			{
				if( ((m_pCuData_StateML3L->m_AxisStB.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 1, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 1, m_BitOffColor);

				AddIndex++;
			}
		}
		else
		{
			//m_GridMotState1.SetColumnWidth(0, 23);
			//m_GridMotState1.SetColumnWidth(1, 20);
			//m_GridMotState1.SetColumnWidth(2, 20);
			m_GridMotState1.SetColumnWidth(1, 0);
			MainRowSizeW+=20;
		}
		//////////////////////////////////////////////////////////////////////////
		if(m_pCuData_StateML3R != nullptr)
		{
			m_GridMotState1.SetColumnWidth(2, 20);
			AddIndex = 0;
			for(int AddStep = 1; AddStep<=32; AddStep++)
			{
				if( ((m_pCuData_StateML3R->m_AxisStA.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 2, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 2, m_BitOffColor);

				AddIndex++;
			}
			AddIndex = 0;
			for(int AddStep = 33; AddStep<=63; AddStep++)
			{
				if( ((m_pCuData_StateML3R->m_AxisStB.MotStatus>>AddIndex)  &0x01) ==1)
					m_GridMotState1.SetItemBkColour(AddStep, 2, m_BitOnColor);
				else
					m_GridMotState1.SetItemBkColour(AddStep, 2, m_BitOffColor);

				AddIndex++;
			}
		}
		else
		{
			//m_GridMotState1.SetColumnWidth(0, 23);
			//m_GridMotState1.SetColumnWidth(1, 20);
			//m_GridMotState1.SetColumnWidth(2, 20);
			m_GridMotState1.SetColumnWidth(2, 0);
			MainRowSizeW+=20;
		}
	}
	m_GridMotState1.SetColumnWidth(3, MainRowSizeW);
	m_GridMotState1.Refresh();
}
void CMotStateDlg::UpdateMotStateAxisLink()
{
	if(m_pALink_State != nullptr)
	{
		int AddIndex = 0;
		for(int AddStep = 1; AddStep<=16; AddStep++)
		{
			if( ((m_pALink_State->MotStatus>>AddIndex)  &0x01) ==1)
				m_GridMotState2.SetItemBkColour(AddStep, 0, m_BitOnColor);
			else
				m_GridMotState2.SetItemBkColour(AddStep, 0, m_BitOffColor);

			AddIndex++;
		}
	}
	m_GridMotState2.Refresh();
	//////////////////////////////////////////////////////////////////////////
}
DWORD UpdateTime = 0;
void CMotStateDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(TIMER_MOT_STATE_UPDATE == nIDEvent && this->IsWindowVisible() == TRUE)
	{
		if(m_pUIUpdate != nullptr)
		{
			if(*m_pUIUpdate == true)
			{
				EnterCriticalSection(&m_CrtSection);
				if(m_pCuData_StateCu != nullptr ||
					m_pCuData_StateCuL != nullptr ||
					m_pCuData_StateCuR != nullptr)
					UpdateMotStateCu();
				else if(m_pCuData_StateML3 != nullptr ||
					m_pCuData_StateML3L != nullptr ||
					m_pCuData_StateML3R != nullptr)
					UpdateMotStateML3();
				else if(m_pALink_State != nullptr )
					UpdateMotStateAxisLink();
				LeaveCriticalSection(&m_CrtSection);
			}
		}
		CString strUpdateTime;
		strUpdateTime.Format("UI:%d, Pos:%d",::GetTickCount() -UpdateTime, G_MotCtrlObj.m_UpdateTime_AxisPos);
		SetDlgItemText(IDC_ST_UPDATE_TIME, strUpdateTime);

		strUpdateTime.Format("IO:%d, L:%d, M:%d, A:%d", 
			G_MotCtrlObj.m_UpdateTime_IO, 
			G_MotCtrlObj.m_UpdateTime_MotState, 
			G_MotCtrlObj.m_UpdateTime_ML3State, 
			G_MotCtrlObj.m_UpdateTime_ALinkState);
		SetDlgItemText(IDC_ST_UPDATE_TIME2, strUpdateTime);

		UpdateTime = ::GetTickCount();
	}
	CDialogEx::OnTimer(nIDEvent);
}
void CMotStateDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

}


void CMotStateDlg::OnBnClickedBtSaveSpeedParam()
{
	CString nowValue;
	CString SpeedSection;
	CString SpeedAxisKey;
	//switch(m_NowViewStateAxis)
	//{
	//case AXIS_TM02_PickerRotate://AxisLink_A Y
	//case AXIS_AM01_ScanUpDn://AxisLink_D X
	//case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
	//case AXIS_AM01_ScopeUpDn://AxisLink_D Z
	//case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
	//case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
	//case AXIS_AM01_TensionRightUpDn://AxisLink_A U
	//case AXIS_AM01_LTension1://AxisLink_B X
	//case AXIS_AM01_LTension2://AxisLink_B Y
	//case AXIS_AM01_LTension3://AxisLink_B Z
	//case AXIS_AM01_LTension4://AxisLink_B U
	//case AXIS_AM01_RTension1://AxisLink_C X
	//case AXIS_AM01_RTension2://AxisLink_C Y
	//case AXIS_AM01_RTension3://AxisLink_C Z
	//case AXIS_AM01_RTension4://AxisLink_C U
	//	return ;//Axis Link쪽은 표시 하지 않는다.
	//	break;
	//default:
	//	break;
	//}

	//////////////////////////////////////////////////////////////////////////
	SpeedSection.Format("%s", Section_SPEED_NORMAL);
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_MaxAccValue);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "0.1", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_NOR_MOVE_ACC_G)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);
	
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_AccTime);
	///GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_NOR_MOVE_ACC)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection, SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);

	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_DecTime);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_NOR_MOVE_DEC)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);
	
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_MoveSpeed);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "50", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_NOR_MOVE_SPEED)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);
	
	//////////////////////////////////////////////////////////////////////////
	SpeedSection.Format("%s", Section_SPEED_TEACH);
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_MaxAccValue);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "0.1", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_TEACH_MOVE_ACC_G)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);
	
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_AccTime);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_TEACH_MOVE_ACC)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection, SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);
	
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_DecTime);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_TEACH_MOVE_DEC)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);
	
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_MoveSpeed);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "50", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_TEACH_MOVE_SPEED)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);
	
	//////////////////////////////////////////////////////////////////////////
	SpeedSection.Format("%s", Section_SPEED_PICKUP);
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_MaxAccValue);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "0.1", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_PICKUP_MOVE_ACC_G)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);
	
	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_AccTime);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_PICKUP_MOVE_ACC)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection, SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);

	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_DecTime);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "5", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_PICKUP_MOVE_DEC)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);

	SpeedAxisKey.Format("%s%s", G_strAxisName[m_NowViewStateAxis], Key_MoveSpeed);
	//GetPrivateProfileString(SpeedSection, SpeedAxisKey, "50", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	GetDlgItem(IDC_ED_PICKUP_MOVE_SPEED)->GetWindowText(nowValue);
	WritePrivateProfileString(SpeedSection,SpeedAxisKey,nowValue, VS24Motion_PARAM_INI_PATH);

	switch(m_rdSpeedMode)
	{
	case 0:
		G_MotCtrlObj.m_fnSetMotSpeed(Speed_Normal, m_NowViewStateAxis);
		break;
	case 1:
		G_MotCtrlObj.m_fnSetMotSpeed(Speed_Pickup, m_NowViewStateAxis);
		break;
	case 2:
		G_MotCtrlObj.m_fnSetMotSpeed(Speed_Teach, m_NowViewStateAxis);
		break;
	}	
}


void CMotStateDlg::OnEnChangeEdNorMoveAccG()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
