#pragma once
#include "afxwin.h"


// CCtrlSolPanel 대화 상자입니다.

class CCtrlSolPanel : public CDialogEx
{
	DECLARE_DYNAMIC(CCtrlSolPanel)

public:
	CCtrlSolPanel(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCtrlSolPanel();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SOL_ACTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtChangeAccView();




	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btChangeACC;
	CRoundButton2 m_btCylinderAction[26+4];
	CRoundButton2 m_btUnlockFrontDoor;
	CRoundButton2 m_btUnlockSideDoor;
	CRoundButton2 m_btUnlockBackDoor;
	afx_msg void OnBnClickedBtDoorFront();
	afx_msg void OnBnClickedBtDoorEqSide();
	afx_msg void OnBnClickedBtDoorBack();

	afx_msg void OnBnClickedBtCylinenderAction();
	afx_msg void OnTimer(UINT_PTR nIDEvent);


	CColorStaticST m_stOpenCloseDoor[8];
	CColorStaticST m_stLockCheckDoor[8];
	
	afx_msg void OnBnClickedBtAm01RinglightUp();
	afx_msg void OnBnClickedBtAm01RinglightDn();
	afx_msg void OnBnClickedBtAm01ReviewUp();
	afx_msg void OnBnClickedBtAm01ReviewDn();
};
