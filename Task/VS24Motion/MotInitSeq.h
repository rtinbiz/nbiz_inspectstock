#pragma once
class CMotInitSeq: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		//Sys Pram Load
		SEQ_LOAD_PARM,
		SEQ_ACC_LINK_OPNE,
		//////////////////////////////////////////////////////////////////////////
		//초기화 진행전에 상태 Check.
		SEQ_CHECK_IO_START,
		SEQ_CHECK_IO_SYS,
		SEQ_CHECK_IO_BOX,
		SEQ_CHECK_IO_STICK,
		SEQ_CHECK_IO_TT01,	
		SEQ_CHECK_IO_BT01,	
		SEQ_CHECK_IO_LT01,	
		SEQ_CHECK_IO_BO01,	
		SEQ_CHECK_IO_UT01,	
		SEQ_CHECK_IO_TM01,	
		SEQ_CHECK_IO_TM02,	
		SEQ_CHECK_IO_AM01,	
		SEQ_CHECK_IO_END,
		//////////////////////////////////////////////////////////////////////////
		//초기화 시작.
		SEQ_INIT_CYL_START,
		SEQ_INIT_CYL_TT01,	
		SEQ_INIT_CYL_BT01,	
		SEQ_INIT_CYL_LT01,	
		SEQ_INIT_CYL_BO01,	
		SEQ_INIT_CYL_UT01,	
		SEQ_INIT_CYL_TM01,	
		SEQ_INIT_CYL_TM02,	
		SEQ_INIT_CYL_AM01,	
		SEQ_INIT_CYL_END,
		//////////////////////////////////////////////////////////////////////////
		//Motion
		SEQ_INIT_MOT_START,
		SEQ_INIT_MOT_TT01,	
		SEQ_INIT_MOT_BT01,	
		SEQ_INIT_MOT_LT01,	
		SEQ_INIT_MOT_BO01,	
		SEQ_INIT_MOT_UT01,	
		SEQ_INIT_MOT_TM01,	
		SEQ_INIT_MOT_TM02,	
		SEQ_INIT_MOT_AM01,	
		SEQ_INIT_MOT_END,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT,
	};

public:
	CMotInitSeq(void);
	~CMotInitSeq(void);

	//CXListBox *m_plistSeqInfo;
	//void G_WriteInfo(ListColor nColorType, const char* pWritLog, ...);
	//////////////////////////////////////////////////////////////////////////
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();

	//////////////////////////////////////////////////////////////////////////
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();


};

