
// VS24MotionDlg.h : 헤더 파일
//

#pragma once
#include "IOMapDlg.h"
#include "AccCtrlDlg.h"
#include "MotStateDlg.h"
#include "CtrlSolPanel.h"
#include "PassWord.h"

#include ".\MotionLink\Runtime.h"
#include "afxwin.h"



// CVS24MotionDlg 대화 상자
class CVS24MotionDlg : public CDialogEx, public CProcInitial//VS64 Interface
{
// 생성입니다.
public:
	CVS24MotionDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~CVS24MotionDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VS24MOTION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	COLORREF m_btBackColor;
	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	void InitCtrl_UI();
public:
	IplImage *m_pSampleView;
	HDC			m_HDCView;
	CRect			m_RectiewArea;
	void DrawImageView();

	
public:
	CInterServerInterface  m_ServerInterface;
	void m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...);
	bool Sys_fnInitVS64Interface();
	int				Sys_fnAnalyzeMsg(CMDMSG* cmdMsg);//Message Process Function.
	LRESULT		Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam);

	//Action Process
public:
	

	ProcMsgQ m_MsgProcThreadQ;
//View Dlg
public:
	CRect				m_AccDlgArea;
	CIOMapDlg		*m_pViewIOMap;
	CRect				m_MotStDlgArea;
	CMotStateDlg  *m_pViewMotState;
	CAccCtrlDlg		*m_pViewACCDlg;
	CCtrlSolPanel	*m_pViewSolCtrlDlg;
	void ChangeView_ACC_SOL(int ID_Viewer);

	afx_msg void OnBnClickedBtHwIoView();
	afx_msg void OnBnClickedBtMotTaskExit();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	bool m_ExpendUI;
	afx_msg void OnBnClickedBtExpend();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	//Task 상태 전환.
	TASK_State m_VS24TaskState;
	CColorStaticST m_stTaskState;
	void m_fnChangeTaskState_ReleasePM();
	TASK_State m_fnChangeTaskState(TASK_State NewState);
	CEdit m_editMotPostion[AXIS_Cnt];
	CColorStaticST m_stPLCState[32];

	CRoundButtonStyle m_tBTStyle_MotNotInit;
	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btAxisSelect[AXIS_Cnt];

	CRoundButton2 m_btAxisHoming;
	CRoundButton2 m_btAxisLeft;
	CRoundButton2 m_btAxisRight;
	CRoundButton2 m_btAxisABSMove[5];

	CRoundButton2 m_btIOView;
	CRoundButton2 m_btExpendUI;
	CRoundButton2 m_btExitSW;
	CRoundButton2 m_btMotSysInit;
	CRoundButton2 m_btAxisStop;
	CRoundButton2 m_btViewChange;

	CRoundButton2 m_btSetPMMode;
	afx_msg void OnBnClickedBtMotSystemPm();

	CXListBox m_listLogView;

	int m_AxisSelectedIndex;

	//Motion Internal Cmd Send
	void SendMotInternalCMDQ(int AxisIndex,  VS24MotData::MotAct nAction, int Position);
	//Cylinder Internal Cmd Send
	LRESULT OnUserSendCylInternalCMDQ(WPARAM wParam, LPARAM lParam);

	void ReadTestPositionData(int SelectAxis);
	void WrriteTestPositionData(int SelectAxis);

	afx_msg void OnBnClickedBtAxisBtEvent();
	afx_msg void OnBnClickedBtMotHome();
	afx_msg void OnBnClickedBtMotLeft();
	afx_msg void OnBnClickedBtMotRight();
	afx_msg void OnBnClickedBtMotMovePos();
	
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedBtMotSystemInit();
	afx_msg void OnBnClickedBtMotStop();
	
	afx_msg void OnBnClickedBtChangeView();

	//Motion Button에 초기화 상태를 표시 한다.
	void CheckMotState_All_LinkButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
