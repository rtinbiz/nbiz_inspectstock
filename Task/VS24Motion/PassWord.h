#pragma once
#include "afxwin.h"


// CPassWord 대화 상자입니다.
#define  PASSWORD_INPUT_TIMEOUT_Sec 8

#define  PASSWORD_INPUT1 "nbiz"	
#define  PASSWORD_INPUT2 "mot24"
class CPassWord : public CDialogEx
{
	DECLARE_DYNAMIC(CPassWord)

public:
	CPassWord(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPassWord();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_PASSWORD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
private: 
	CFont m_ViewFont;
	int m_PasswordInputTimeOut;
public:
	COLORREF m_btBackColor;
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btOK;
	CRoundButton2 m_btNG;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
