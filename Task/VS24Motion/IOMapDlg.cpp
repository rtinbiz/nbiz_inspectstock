// IOMapDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS24Motion.h"
#include "IOMapDlg.h"
#include "afxdialogex.h"


// CIOMapDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CIOMapDlg, CDialogEx)

CIOMapDlg::CIOMapDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CIOMapDlg::IDD, pParent)
{

}

CIOMapDlg::~CIOMapDlg()
{
}

void CIOMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_IO_IN_EXIT, m_btnExit);
	DDX_Control(pDX, IDC_GRID_IO_VIEW2, m_GridInputList);
	DDX_Control(pDX, IDC_GRID_IO_VIEW3, m_GridOutputList);
	DDX_Control(pDX, IDC_BTN_IO_IN_SELECT, m_btInputSelect);
	DDX_Control(pDX, IDC_BTN_IO_OUT_SELECT, m_btOutputSelect);
	DDX_Control(pDX, IDC_STATIC_MAIN_TITLE, m_stSelectIO);

}


BEGIN_MESSAGE_MAP(CIOMapDlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_IO_IN_EXIT, &CIOMapDlg::OnBnClickedBtnIoInExit)
	ON_NOTIFY(GVN_SELCHANGED, IDC_GRID_IO_VIEW3, &CIOMapDlg::OnTagGridClickOuput)   //-> 이 줄을 추가....
	ON_BN_CLICKED(IDC_BTN_IO_IN_SELECT, &CIOMapDlg::OnBnClickedBtnIoInSelect)
	ON_BN_CLICKED(IDC_BTN_IO_OUT_SELECT, &CIOMapDlg::OnBnClickedBtnIoOutSelect)
END_MESSAGE_MAP()


// CIOMapDlg 메시지 처리기입니다.

void CIOMapDlg::m_fnInit(/*CDioReadThread *pDioReadThread*/)
{
	//m_pDioReadThread = pDioReadThread;

	//CDialogEx::Create(IDD, AfxGetApp()->m_pMainWnd);
	//this->MoveWindow(0,0,1024,768, TRUE);
}
void CIOMapDlg::m_fnDeInit()
{

}
BOOL CIOMapDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	InitCtrl_UI();

	SetTimer(IO_READ_TIME, 100, NULL);

	//this->MoveWindow(0,0,1024,768, TRUE);
	CenterWindow();

	OnBnClickedBtnIoInSelect();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CIOMapDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;

	m_BitOnColor = RGB(128, 255, 128);
	m_BitOffColor = RGB(128, 128, 128);

	m_BitBlockColor1	= RGB(230, 200, 200);
	m_BitBlockColor2	= RGB(200, 200, 230);
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btnExit.GetTextColor(&tColor);
		m_btnExit.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,22,"굴림");
			tFont.lfHeight =18;
		}

		// Get default Style
		m_tButtonStyle.GetButtonStyle(&tStyle);
		
		m_btnExit.SetRoundButtonStyle(&m_tButtonStyle);
		m_btnExit.SetTextColor(&tColor);
		m_btnExit.SetCheckButton(false);
		m_btnExit.SetFont(&tFont);

		m_btInputSelect.SetRoundButtonStyle(&m_tButtonStyle);
		m_btInputSelect.SetTextColor(&tColor);
		m_btInputSelect.SetCheckButton(false);
		m_btInputSelect.SetFont(&tFont);

		m_btOutputSelect.SetRoundButtonStyle(&m_tButtonStyle);
		m_btOutputSelect.SetTextColor(&tColor);
		m_btOutputSelect.SetCheckButton(false);
		m_btOutputSelect.SetFont(&tFont);
	}
	m_GridFont.CreateFont(11, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	m_Font.CreateFont(35, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	m_stSelectIO.SetFont(&m_Font);
	m_stSelectIO.SetTextColor(RGB(0,0,0));
	m_stSelectIO.SetBkColor(G_Color_On);

	m_GridInputList.SetDefCellWidth(20);
	m_GridInputList.SetDefCellHeight(25);
	m_GridInputList.SetEditable(FALSE);
	m_GridInputList.SetSingleColSelection(TRUE);

	m_GridInputList.SetRowCount(16+1);
	m_GridInputList.SetColumnCount(18*4);
	m_GridInputList.SetFixedRowCount(1);
	//m_GridInputList.SetListMode(TRUE);
	m_GridInputList.SetFont(&m_GridFont);
	//////////////////////////////////////////////////////////////////////////
	m_GridOutputList.SetDefCellWidth(20);
	m_GridOutputList.SetDefCellHeight(25);
	m_GridOutputList.SetEditable(FALSE);
	m_GridOutputList.SetSingleColSelection(TRUE);

	m_GridOutputList.SetRowCount(16+1);
	m_GridOutputList.SetColumnCount(18*4);
	m_GridOutputList.SetFixedRowCount(1);
	//m_GridOutputList.SetListMode(TRUE);
	m_GridOutputList.SetFont(&m_GridFont);
	//////////////////////////////////////////////////////////////////////////
	CString BlockName;
	for(int pStep = 0, CIndex = 0; pStep<18; pStep++, CIndex+=4)
	{
		m_GridInputList.SetItemText(0, CIndex+0, "V");			m_GridInputList.SetColumnWidth(CIndex+0, 30);
		m_GridInputList.SetItemText(0, CIndex+1, "NO.");		m_GridInputList.SetColumnWidth(CIndex+1, 50);

		BlockName.Format("Axis Link %c", 'A'+(pStep/2));
		m_GridInputList.SetItemText(0, CIndex+2, BlockName);	m_GridInputList.SetColumnWidth(CIndex+2, 150);

		m_GridInputList.SetItemText(0, CIndex+3, "");			m_GridInputList.SetColumnWidth(CIndex+3, 10);
	}
	for(int pStep = 0, CIndex = 0; pStep<18; pStep++, CIndex+=4)
	{
		m_GridOutputList.SetItemText(0, CIndex+0, "V");			m_GridOutputList.SetColumnWidth(CIndex+0, 30);
		m_GridOutputList.SetItemText(0, CIndex+1, "NO.");		m_GridOutputList.SetColumnWidth(CIndex+1, 50);

		BlockName.Format("Axis Link %c", 'A'+(pStep/2));
		m_GridOutputList.SetItemText(0, CIndex+2, BlockName/*"SIGNAL"*/);	m_GridOutputList.SetColumnWidth(CIndex+2, 150);

		m_GridOutputList.SetItemText(0, CIndex+3, "");				m_GridOutputList.SetColumnWidth(CIndex+3, 10);
	}
	

	//////////////////////////////////////////////////////////////////////////

	//m_GridInputList. SetBkColor(RGB(200, 203, 85));	


	m_GridOutputList.SetGridLineColor(RGB(255, 0, 0));
	m_GridInputList.SetGridLineColor(RGB(0, 255, 0));
	//////////////////////////////////////////////////////////////////////////
	CString strAdd;
	int AddIndex = 0;

	for(int LineStep = 0; LineStep<(18*4); LineStep+=4)
	{
		for(int AddStep = 1; AddStep<=16; AddStep++)
		{
			//m_GridInputList.SetItemImage(AddStep , LineStep, 1);
			m_GridInputList.SetItemBkColour(AddStep, LineStep, m_BitOffColor);
		}
	}
	//////////////////////////////////////////////////////////////////////////
	AddIndex = 0;

	for(int LineStep = 0; LineStep<(18*4); LineStep+=4)
	{
		for(int AddStep = 1; AddStep<=16; AddStep++)
		{
			//m_GridOutputList.SetItemImage(AddStep , LineStep, 1);
			m_GridOutputList.SetItemBkColour(AddStep, LineStep, m_BitOffColor);
		}
	}
	////////////////////////////////////////////////////////////////////////////
	AddIndex = 0;


	COLORREF *pBitBlockColor= &m_BitBlockColor1;
	for(int LineStep = 1; LineStep<(18*4); LineStep+=4)
	{
		if(AddIndex == 32)
		{
			AddIndex =0;
			if(pBitBlockColor == &m_BitBlockColor1)
				pBitBlockColor = &m_BitBlockColor2;
			else
				pBitBlockColor = &m_BitBlockColor1;
		}
		for(int AddStep = 1; AddStep<=16; AddStep++)
		{
			strAdd.Format("DI%02d", AddIndex);
			AddIndex++;
			m_GridInputList.SetItemText(AddStep , LineStep, strAdd.GetBuffer(strAdd.GetLength()));

			m_GridInputList.SetItemBkColour(AddStep, LineStep, *pBitBlockColor);
			m_GridInputList.SetItemBkColour(AddStep, LineStep+1, *pBitBlockColor);
		}

	}
	//////////////////////////////////////////////////////////////////////////
	AddIndex = 0;
	for(int LineStep = 1; LineStep<(18*4); LineStep+=4)
	{
		if(AddIndex == 32)
		{
			AddIndex =0;
			if(pBitBlockColor == &m_BitBlockColor1)
				pBitBlockColor = &m_BitBlockColor2;
			else
				pBitBlockColor = &m_BitBlockColor1;
		}
		for(int AddStep = 1; AddStep<=16; AddStep++)
		{
			strAdd.Format("DO%02d", AddIndex);
			AddIndex++;
			m_GridOutputList.SetItemText(AddStep , LineStep, strAdd.GetBuffer(strAdd.GetLength()));

			m_GridOutputList.SetItemBkColour(AddStep, LineStep, *pBitBlockColor);
			m_GridOutputList.SetItemBkColour(AddStep, LineStep+1, *pBitBlockColor);
		}

	}
	//////////////////////////////////////////////////////////////////////////
	AddIndex = 0;
	for(int LineStep = 2; LineStep<(18*4); LineStep+=4) 
	{
		for(int AddStep = 1; AddStep<=16; AddStep++)
		{
			switch(AddIndex)
			{
			//Block A Input
			case   0: strAdd.Format("EMO1");break;
			case   1: strAdd.Format("EMO2");break;
			case   2: strAdd.Format("EMO3");break;
			case   3: strAdd.Format("Indexer Front Door Left Open Close");break;
			case   4: strAdd.Format("Indexer Front Door Left Lock Unlock");break;
			case   5: strAdd.Format("BT01 Coupling Left");break;
			case   6: strAdd.Format("BT01 Coupling Right");break;
			case   7: strAdd.Format("Indexer Side Door Left Open Close");break;
			case   8: strAdd.Format("Indexer Side Door Left Lock Unlock	");break;
			case   9: strAdd.Format("Indexer Side Door Right Open Close");break;
			case 10: strAdd.Format("Indexer Side Door Right Lock Unlock");break;
			case 11: strAdd.Format("Inspecter Side Door Left Open Close");break;
			case 12: strAdd.Format("Inspecter Side Door Left Lock Unlock");break;
			case 13: strAdd.Format("Inspecter Side Door Right Openv");break;
			case 14: strAdd.Format("Inspecter Side Door Right Lock Unlock");break;
			case 15: strAdd.Format("Inspecter Back Door Left Open Close");break;
			case 16: strAdd.Format("Inspecter Back Door Left Lock Unlock");break;
			case 17: strAdd.Format("Inspecter Back Door Right Open Close");break;
			case 18: strAdd.Format("Inspecter Back Door Right Lock Unlock");break;
			case 19: strAdd.Format("Auto Teach Mode");break;
			case 20: strAdd.Format("Electronic Box Temp Alarm 1");break;
			case 21: strAdd.Format("Electronic Box Temp Alarm 2");break;
			case 22: strAdd.Format("PC Rack Temp Alarm 1");break;
			case 23: strAdd.Format("PC Rack Temp Alarm 2");break;
			case 24: strAdd.Format("System On");break;
			case 25: strAdd.Format("Buzzer Reset");break;
			case 26: strAdd.Format("Cassette Stick Output Pos(Error Stick)");break;
			case 27: strAdd.Format("EQ Lamp");break;
			case 28: strAdd.Format("EMO4");break;
			case 29: strAdd.Format("Safety Enable");break;
			case 30: strAdd.Format("Two Paper Absorb Interlock1");break;
			case 31: strAdd.Format("Two Paper Absorb Interlock2");break;
			//Block B Input
			case 32+  0: strAdd.Format("Box Table 1 100mm");break;
			case 32+  1: strAdd.Format("Box Table 1 200mm");break;
			case 32+  2: strAdd.Format("Box Table 1 440mm");break;
			case 32+  3: strAdd.Format("Box Table 2 100mm");break;
			case 32+  4: strAdd.Format("Box Table 2 200mm");break;
			case 32+  5: strAdd.Format("Box Table 2 440mm");break;
			case 32+  6: strAdd.Format("Box Table 3 100mm");break;
			case 32+  7: strAdd.Format("Box Table 3 200mm");break;
			case 32+  8: strAdd.Format("Box Table 3 440mm");break;
			case 32+  9: strAdd.Format("Box Table 4 100mm");break;
			case 32+10: strAdd.Format("Box Table 4 200mm");break;
			case 32+11: strAdd.Format("Box Table 4 440mm");break;
			case 32+12: strAdd.Format("Box Table 5 100mm");break;
			case 32+13: strAdd.Format("Box Table 5 200mm");break;
			case 32+14: strAdd.Format("Box Table 5 440mm");break;
			case 32+15: strAdd.Format("Box Table 6 100mm");break;
			case 32+16: strAdd.Format("Box Table 6 200mm");break;
			case 32+17: strAdd.Format("Box Table 6 440mm");break;
			case 32+18: strAdd.Format("Box Table 7 100mm");break;
			case 32+19: strAdd.Format("Box Table 7 200mm");break;
			case 32+20: strAdd.Format("Box Table 7 440mm");break;
			case 32+21: strAdd.Format("Box Table F 100mm");break;
			case 32+22: strAdd.Format("Box Table F 200mm");break;
			case 32+23: strAdd.Format("Box Table F 440mm");break;
			case 32+24: strAdd.Format("Reserve");break;
			case 32+25: strAdd.Format("Reserve");break;
			case 32+26: strAdd.Format("Reserve");break;
			case 32+27: strAdd.Format("Reserve");break;
			case 32+28: strAdd.Format("Reserve");break;
			case 32+29: strAdd.Format("Reserve");break;
			case 32+30: strAdd.Format("Reserve");break;
			case 32+31: strAdd.Format("Reserve");break;

				//Block C Input
			case 64+  0: strAdd.Format("Box Left Cylinder Forward");break;
			case 64+  1: strAdd.Format("Box Left Cylinder Backward");break;
			case 64+  2: strAdd.Format("Box Detection");break;
			case 64+  3: strAdd.Format("Box Left Vacuum");break;
			case 64+  4: strAdd.Format("Box Right Cylinder Forward");break;
			case 64+  5: strAdd.Format("Box Right Cylinder Backward");break;
			case 64+  6: strAdd.Format("Box Right Vacuum");break;
			case 64+  7: strAdd.Format("Load Box Small Detection");break;
			case 64+  8: strAdd.Format("Load Box Medium Detection");break;
			case 64+  9: strAdd.Format("Load Box Large Detection");break;
			case 64+10: strAdd.Format("Load Box Vacuum");break;
			case 64+11: strAdd.Format("Load Left Align Forward");break;
			case 64+12: strAdd.Format("Load Left Align Backward");break;
			case 64+13: strAdd.Format("Load Right Align Forward");break;
			case 64+14: strAdd.Format("Load Right Align Backward");break;
			case 64+15: strAdd.Format("Unload Box Detection");break;
			case 64+16: strAdd.Format("Unload Box Vacuum");break;
			case 64+17: strAdd.Format("Unload Box Left Align Forward");break;
			case 64+18: strAdd.Format("Unload Box Left Align Backward");break;
			case 64+19: strAdd.Format("Unload Box Right Align Forward");break;
			case 64+20: strAdd.Format("Unload Box Right Align Backward");break;
			case 64+21: strAdd.Format("Fork Box Detection");break;
			case 64+22: strAdd.Format("Fork Box Vacuum");break;
			case 64+23: strAdd.Format("Fork Front Up");break;
			case 64+24: strAdd.Format("Fork Front Down");break;
			case 64+25: strAdd.Format("Fork Rear Up");break;
			case 64+26: strAdd.Format("Fork Rear Down");break;
			case 64+27: strAdd.Format("Indexer Main CDA");break;
			case 64+28: strAdd.Format("Inspect Main CDA");break;
			case 64+29: strAdd.Format("Main N2");break;
			case 64+30: strAdd.Format("Indexer Main Vacuum");break;
			case 64+31: strAdd.Format("Reserve");break;


			//Block D Input
			case 96+  0: strAdd.Format("Left Open Tension 1");break;
			case 96+  1: strAdd.Format("Left Close Tension 1");break;
			case 96+  2: strAdd.Format("Left Open Tension 2");break;
			case 96+  3: strAdd.Format("Left Close Tension 2");break;
			case 96+  4: strAdd.Format("Left Open Tension 3");break;
			case 96+  5: strAdd.Format("Left Close Tension 3");break;
			case 96+  6: strAdd.Format("Left Open Tension 4");break;
			case 96+  7: strAdd.Format("Left Close Tension 4");break;
			case 96+  8: strAdd.Format("Left Tension Cylinder Forward");break;
			case 96+  9: strAdd.Format("Left Tension Cylinder Backward");break;
			case 96+10: strAdd.Format("Left Tension Stick Detection");break;
			case 96+11: strAdd.Format("Left Tension Pressure");break;
			case 96+12: strAdd.Format("Right Open Tension 1	");break;
			case 96+13: strAdd.Format("Right Close Tension 1");break;
			case 96+14: strAdd.Format("Right Open Tension 2	");break;
			case 96+15: strAdd.Format("Right Close Tension 2");break;
			case 96+16: strAdd.Format("Right Open Tension 3	");break;
			case 96+17: strAdd.Format("Right Close Tension 3");break;
			case 96+18: strAdd.Format("Right Open Tension 4	");break;
			case 96+19: strAdd.Format("Right Close Tension 4");break;
			case 96+20: strAdd.Format("Right Tension Cylinder Forward");break;
			case 96+21: strAdd.Format("Right Tension Cylinder Backward");break;
			case 96+22: strAdd.Format("Right Tension Stick Detection");break;
			case 96+23: strAdd.Format("Right Tension Pressure");break;
			case 96+24: strAdd.Format("Reserve");break;
			case 96+25: strAdd.Format("Reserve");break;
			case 96+26: strAdd.Format("Reserve");break;
			case 96+27: strAdd.Format("Reserve");break;
			case 96+28: strAdd.Format("Reserve");break;
			case 96+29: strAdd.Format("Reserve");break;
			case 96+30: strAdd.Format("Reserve");break;
			case 96+31: strAdd.Format("Reserve");break;

				//Block E Input
			case 128+  0: strAdd.Format("Picker Front Stick Vacuum");break;
			case 128+  1: strAdd.Format("Picker Left Input Stick Detect");break;
			case 128+  2: strAdd.Format("Picker Left Input Paper Detect");break;
			case 128+  3: strAdd.Format("Picker Left Output Stick Detect");break;
			case 128+  4: strAdd.Format("Picker Left Output Paper Detect");break;
			case 128+  5: strAdd.Format("Picker Right Input Stick Detect");break;
			case 128+  6: strAdd.Format("Picker Right Input Paper Detect");break;
			case 128+  7: strAdd.Format("Picker Right Output Stick Detect	");break;
			case 128+  8: strAdd.Format("Picker Right Output Paper Detect");break;
			case 128+  9: strAdd.Format("Picker Front Paper Vacuum");break;
			case 128+10: strAdd.Format("Picker Back Stick Vacuum");break;
			case 128+11: strAdd.Format("Picker Back Paper Vacuum");break;
			case 128+12: strAdd.Format("Picker Z Axis Clash Prevent");break;
			case 128+13: strAdd.Format("Reserve");break;
			case 128+14: strAdd.Format("Reserve");break;
			case 128+15: strAdd.Format("Reserve");break;

			case 128+16: strAdd.Format("Reserve");break;
			case 128+17: strAdd.Format("Reserve");break;
			case 128+18: strAdd.Format("Reserve");break;
			case 128+19: strAdd.Format("Reserve");break;
			case 128+20: strAdd.Format("Reserve");break;
			case 128+21: strAdd.Format("Reserve");break;
			case 128+22: strAdd.Format("Reserve");break;
			case 128+23: strAdd.Format("Reserve");break;
			case 128+24: strAdd.Format("Reserve");break;
			case 128+25: strAdd.Format("Reserve");break;
			case 128+26: strAdd.Format("Reserve");break;
			case 128+27: strAdd.Format("Reserve");break;
			case 128+28: strAdd.Format("Reserve");break;
			case 128+29: strAdd.Format("Reserve");break;
			case 128+30: strAdd.Format("Reserve");break;
			case 128+31: strAdd.Format("Reserve");break;

				//Block F Input
			case 160+  0: strAdd.Format("Picker Paper Left Up");break;
			case 160+  1: strAdd.Format("Picker Paper Left Down");break;
			case 160+  2: strAdd.Format("Picker Paper Right Up");break;
			case 160+  3: strAdd.Format("Picker Paper Right Down");break;
			case 160+  4: strAdd.Format("Picker Paper Center Up");break;
			case 160+  5: strAdd.Format("Picker Paper Center Down");break;
			case 160+  6: strAdd.Format("Reserve");break;
			case 160+  7: strAdd.Format("Reserve");break;
			case 160+  8: strAdd.Format("Reserve");break;
			case 160+  9: strAdd.Format("Reserve");break;
			case 160+10: strAdd.Format("Reserve");break;
			case 160+11: strAdd.Format("Reserve");break;
			case 160+12: strAdd.Format("Reserve");break;
			case 160+13: strAdd.Format("Reserve");break;
			case 160+14: strAdd.Format("Reserve");break;
			case 160+15: strAdd.Format("Reserve");break;

			case 160+16: strAdd.Format("Reserve");break;
			case 160+17: strAdd.Format("Reserve");break;
			case 160+18: strAdd.Format("Reserve");break;
			case 160+19: strAdd.Format("Reserve");break;
			case 160+20: strAdd.Format("Reserve");break;
			case 160+21: strAdd.Format("Reserve");break;
			case 160+22: strAdd.Format("Reserve");break;
			case 160+23: strAdd.Format("Reserve");break;
			case 160+24: strAdd.Format("Reserve");break;
			case 160+25: strAdd.Format("Reserve");break;
			case 160+26: strAdd.Format("Reserve");break;
			case 160+27: strAdd.Format("Reserve");break;
			case 160+28: strAdd.Format("Reserve");break;
			case 160+29: strAdd.Format("Reserve");break;
			case 160+30: strAdd.Format("Reserve");break;
			case 160+31: strAdd.Format("Reserve");break;

				//Block G Input
			case 192+  0: strAdd.Format("Turn Table Vacuum");break;
			case 192+  1: strAdd.Format("Turn Table Upper Left StickDetect");break;
			case 192+  2: strAdd.Format("Turn Table Upper Right StickDetect");break;
			case 192+  3: strAdd.Format("Turn Table Lower Left StickDetect");break;
			case 192+  4: strAdd.Format("Turn Table Lower Right StickDetect");break;
			case 192+  5: strAdd.Format("Turn Table Upper Position");break;
			case 192+  6: strAdd.Format("Turn Table Lower Position");break;
			case 192+  7: strAdd.Format("Turn Table Down Vacuum	");break;
			case 192+  8: strAdd.Format("Reserve");break;
			case 192+  9: strAdd.Format("Reserve");break;
			case 192+10: strAdd.Format("Clash Prevent Turn Table And CCD");break;
			case 192+11: strAdd.Format("Reserve");break;
			case 192+12: strAdd.Format("Reserve");break;
			case 192+13: strAdd.Format("Reserve");break;
			case 192+14: strAdd.Format("Reserve");break;
			case 192+15: strAdd.Format("Reserve");break;

			case 192+16: strAdd.Format("Reserve");break;
			case 192+17: strAdd.Format("Reserve");break;
			case 192+18: strAdd.Format("Reserve");break;
			case 192+19: strAdd.Format("Reserve");break;
			case 192+20: strAdd.Format("Reserve");break;
			case 192+21: strAdd.Format("Reserve");break;
			case 192+22: strAdd.Format("Reserve");break;
			case 192+23: strAdd.Format("Reserve");break;
			case 192+24: strAdd.Format("Reserve");break;
			case 192+25: strAdd.Format("Reserve");break;
			case 192+26: strAdd.Format("Reserve");break;
			case 192+27: strAdd.Format("Reserve");break;
			case 192+28: strAdd.Format("Reserve");break;
			case 192+29: strAdd.Format("Reserve");break;
			case 192+30: strAdd.Format("Reserve");break;
			case 192+31: strAdd.Format("Reserve");break;

				//Block H Input
			case 224+  0: strAdd.Format("Reserve");break;
			case 224+  1: strAdd.Format("Reserve");break;
			case 224+  2: strAdd.Format("Reserve");break;
			case 224+  3: strAdd.Format("Reserve");break;
			case 224+  4: strAdd.Format("Reserve");break;
			case 224+  5: strAdd.Format("Reserve");break;
			case 224+  6: strAdd.Format("Reserve");break;
			case 224+  7: strAdd.Format("Reserve");break;
			case 224+  8: strAdd.Format("Reserve");break;
			case 224+  9: strAdd.Format("Reserve");break;
			case 224+10: strAdd.Format("Reserve");break;
			case 224+11: strAdd.Format("Reserve");break;
			case 224+12: strAdd.Format("Reserve");break;
			case 224+13: strAdd.Format("Reserve");break;
			case 224+14: strAdd.Format("Reserve");break;
			case 224+15: strAdd.Format("Reserve");break;

			case 224+16: strAdd.Format("Reserve");break;
			case 224+17: strAdd.Format("Reserve");break;
			case 224+18: strAdd.Format("Reserve");break;
			case 224+19: strAdd.Format("Reserve");break;
			case 224+20: strAdd.Format("Reserve");break;
			case 224+21: strAdd.Format("Reserve");break;
			case 224+22: strAdd.Format("Reserve");break;
			case 224+23: strAdd.Format("Reserve");break;
			case 224+24: strAdd.Format("Reserve");break;
			case 224+25: strAdd.Format("Reserve");break;
			case 224+26: strAdd.Format("Reserve");break;
			case 224+27: strAdd.Format("Reserve");break;
			case 224+28: strAdd.Format("Reserve");break;
			case 224+29: strAdd.Format("Reserve");break;
			case 224+30: strAdd.Format("Reserve");break;
			case 224+31: strAdd.Format("Reserve");break;

				//Block I Input
			case 256+  0: strAdd.Format("Output Table Left Stick Detect");break;
			case 256+  1: strAdd.Format("Output Table Right Stick Detect");break;
			case 256+  2: strAdd.Format("Output Table Cylinder Up");break;
			case 256+  3: strAdd.Format("Output Table Cylinder Down");break;
			case 256+  4: strAdd.Format("Output Table Vacuum");break;
			case 256+  5: strAdd.Format("Output R Table Cylinder Up");break;
			case 256+  6: strAdd.Format("Output R Table Cylinder Down");break;
			case 256+  7: strAdd.Format("Reserve");break;
			case 256+  8: strAdd.Format("MC On(0)/Off(1)==>Bit On Alarm(MC Off임)");break;
			case 256+  9: strAdd.Format("Fork Standby Position Sensor");break;
			case 256+10: strAdd.Format("Turn Table Rotate Enable Position Sensor");break;
			case 256+11: strAdd.Format("Scan Light Cylinder Up");break;
			case 256+12: strAdd.Format("Scan Light Cylinder Down");break;
			case 256+13: strAdd.Format("Review Cylinder Up");break;
			case 256+14: strAdd.Format("Review Cylinder Down");break;
			case 256+15: strAdd.Format("Mask Jig Backword");break;

			case 256+16: strAdd.Format("Reserve");break;
			case 256+17: strAdd.Format("Reserve");break;
			case 256+18: strAdd.Format("Reserve");break;
			case 256+19: strAdd.Format("Reserve");break;
			case 256+20: strAdd.Format("Reserve");break;
			case 256+21: strAdd.Format("Reserve");break;
			case 256+22: strAdd.Format("Reserve");break;
			case 256+23: strAdd.Format("Reserve");break;
			case 256+24: strAdd.Format("Reserve");break;
			case 256+25: strAdd.Format("Reserve");break;
			case 256+26: strAdd.Format("Reserve");break;
			case 256+27: strAdd.Format("Reserve");break;
			case 256+28: strAdd.Format("Reserve");break;
			case 256+29: strAdd.Format("Reserve");break;
			case 256+30: strAdd.Format("Reserve");break;
			case 256+31: strAdd.Format("Reserve");break;
			}
			if(strAdd.CompareNoCase("Reserve")==0)
				m_GridInputList.SetItemBkColour(AddStep, LineStep, m_BitOffColor);

			//if( (AddIndex >=128+16 && AddIndex <=128+31) ||
			//	(AddIndex >=160+16 && AddIndex <=160+31) ||
			//	(AddIndex >=192+16 && AddIndex <=192+31) ||
			//	(AddIndex >=224+0 && AddIndex <=224+16) ||
			//	(AddIndex >=256+16 && AddIndex <=256+31) )
			//{
			//		m_GridInputList.SetColumnWidth(LineStep-2,0);
			//		m_GridInputList.SetColumnWidth(LineStep-1,0);
			//		m_GridInputList.SetColumnWidth(LineStep,0);
			//		m_GridInputList.SetColumnWidth(LineStep+1,0);
			//}

			m_GridInputList.SetItemText(AddStep , LineStep, strAdd.GetBuffer(strAdd.GetLength()));
			AddIndex++;
		}
	}
	////////////////////////////////////////////////////////////////////////////
	AddIndex = 0;
	for(int LineStep = 2; LineStep<(18*4); LineStep+=4) 
	{
		for(int AddStep = 1; AddStep<=16; AddStep++)
		{
			switch(AddIndex)
			{
				//Block A Output
			case   0: strAdd.Format("Indexer Front Door Release");break;
			case   1: strAdd.Format("Side Door All Release");break;
			case   2: strAdd.Format("Inspector Back Door Release");break;
			case   3: strAdd.Format("Teach Key Unlock");break;
			case   4: strAdd.Format("Reserve");break;
			case   5: strAdd.Format("Reserve");break;
			case   6: strAdd.Format("Reserve");break;
			case   7: strAdd.Format("Reserve");break;
			case   8: strAdd.Format("Signal Tower Red");break;
			case   9: strAdd.Format("Signal Tower Yellow");break;
			case 10: strAdd.Format("Signal Tower Green");break;
			case 11: strAdd.Format("Buzzer 1");break;
			case 12: strAdd.Format("Buzzer 2");break;
			case 13: strAdd.Format("Reserve");break;
			case 14: strAdd.Format("Reserve");break;
			case 15: strAdd.Format("Reserve");break;
			case 16: strAdd.Format("Reserve");break;
			case 17: strAdd.Format("Reserve");break;
			case 18: strAdd.Format("Reserve");break;
			case 19: strAdd.Format("Reserve");break;
			case 20: strAdd.Format("Reserve");break;
			case 21: strAdd.Format("Reserve");break;
			case 22: strAdd.Format("Reserve");break;
			case 23: strAdd.Format("Reserve");break;
			case 24: strAdd.Format("Reserve");break;
			case 25: strAdd.Format("Reserve");break;
			case 26: strAdd.Format("Reserve");break;
			case 27: strAdd.Format("Reserve");break;
			case 28: strAdd.Format("Reserve");break;
			case 29: strAdd.Format("Reserve");break;
			case 30: strAdd.Format("Reserve");break;
			case 31: strAdd.Format("Space Sensor Laser On/Off");break;

				//Block B Output
			case 32+  0: strAdd.Format("Reserve");break;
			case 32+  1: strAdd.Format("Reserve");break;
			case 32+  2: strAdd.Format("Reserve");break;
			case 32+  3: strAdd.Format("Reserve");break;
			case 32+  4: strAdd.Format("Reserve");break;
			case 32+  5: strAdd.Format("Reserve");break;
			case 32+  6: strAdd.Format("Reserve");break;
			case 32+  7: strAdd.Format("Reserve");break;
			case 32+  8: strAdd.Format("Reserve");break;
			case 32+  9: strAdd.Format("Reserve");break;
			case 32+10: strAdd.Format("Reserve");break;
			case 32+11: strAdd.Format("Reserve");break;
			case 32+12: strAdd.Format("Reserve");break;
			case 32+13: strAdd.Format("Reserve");break;
			case 32+14: strAdd.Format("Reserve");break;
			case 32+15: strAdd.Format("Reserve");break;
			case 32+16: strAdd.Format("Reserve");break;
			case 32+17: strAdd.Format("Reserve");break;
			case 32+18: strAdd.Format("Reserve");break;
			case 32+19: strAdd.Format("Reserve");break;
			case 32+20: strAdd.Format("Reserve");break;
			case 32+21: strAdd.Format("Reserve");break;
			case 32+22: strAdd.Format("Reserve");break;
			case 32+23: strAdd.Format("Reserve");break;
			case 32+24: strAdd.Format("Reserve");break;
			case 32+25: strAdd.Format("Reserve");break;
			case 32+26: strAdd.Format("Reserve");break;
			case 32+27: strAdd.Format("Reserve");break;
			case 32+28: strAdd.Format("Reserve");break;
			case 32+29: strAdd.Format("Reserve");break;
			case 32+30: strAdd.Format("Reserve");break;
			case 32+31: strAdd.Format("Reserve");break;

				//Block C Output
			case 64+  0: strAdd.Format("Box Left Cylinder Forward");break;
			case 64+  1: strAdd.Format("Box Left Cylinder Backward");break;
			case 64+  2: strAdd.Format("Box Left Vacuum");break;
			case 64+  3: strAdd.Format("Box Left Purge");break;
			case 64+  4: strAdd.Format("Box Right Cylinder Forward");break;
			case 64+  5: strAdd.Format("Box Right Cylinder Backward");break;
			case 64+  6: strAdd.Format("Box Right Vacuum");break;
			case 64+  7: strAdd.Format("Box Right Purge");break;
			case 64+  8: strAdd.Format("Load Box Vacuum");break;
			case 64+  9: strAdd.Format("Load Box Purge");break;
			case 64+10: strAdd.Format("Load Box Align Cylinder Forward");break;
			case 64+11: strAdd.Format("Load Box Align Cylinder Backward");break;
			case 64+12: strAdd.Format("Unload Box Vacuum");break;
			case 64+13: strAdd.Format("Unload Box Purge");break;
			case 64+14: strAdd.Format("Unload Box Align Cylinder Forward");break;
			case 64+15: strAdd.Format("Unload Box Align Cylinder Backward");break;
			case 64+16: strAdd.Format("Fork Vacuum Sol");break;
			case 64+17: strAdd.Format("Fork Purge");break;
			case 64+18: strAdd.Format("Fork Up Sol	");break;
			case 64+19: strAdd.Format("Fork Down Sol	");break;
			case 64+20: strAdd.Format("N2 Blowing");break;
			case 64+21: strAdd.Format("Reserve");break;
			case 64+22: strAdd.Format("Reserve");break;
			case 64+23: strAdd.Format("Reserve");break;
			case 64+24: strAdd.Format("Reserve");break;
			case 64+25: strAdd.Format("Reserve");break;
			case 64+26: strAdd.Format("Reserve");break;
			case 64+27: strAdd.Format("Reserve");break;
			case 64+28: strAdd.Format("Reserve");break;
			case 64+29: strAdd.Format("Reserve");break;
			case 64+30: strAdd.Format("Reserve");break;
			case 64+31: strAdd.Format("Reserve");break;


				//Block D Output
			case 96+  0: strAdd.Format("Left Open Tension 1");break;
			case 96+  1: strAdd.Format("Left Close Tension 1");break;
			case 96+  2: strAdd.Format("Left Open Tension 2");break;
			case 96+  3: strAdd.Format("Left Close Tension 2");break;
			case 96+  4: strAdd.Format("Left Open Tension 3");break;
			case 96+  5: strAdd.Format("Left Close Tension 3");break;
			case 96+  6: strAdd.Format("Left Open Tension 4");break;
			case 96+  7: strAdd.Format("Left Close Tension 4");break;
			case 96+  8: strAdd.Format("Left Tension Cylinder Forward");break;
			case 96+  9: strAdd.Format("Left Tension Cylinder Backward");break;
			case 96+10: strAdd.Format("Right Open Tension 1	");break;
			case 96+11: strAdd.Format("Right Close Tension 1");break;
			case 96+12: strAdd.Format("Right Open Tension 2	");break;
			case 96+13: strAdd.Format("Right Close Tension 2");break;
			case 96+14: strAdd.Format("Right Open Tension 3	");break;
			case 96+15: strAdd.Format("Right Close Tension 3");break;
			case 96+16: strAdd.Format("Right Open Tension 4	");break;
			case 96+17: strAdd.Format("Right Close Tension 4");break;
			case 96+18: strAdd.Format("Right Tension Cylinder Forward");break;
			case 96+19: strAdd.Format("Right Tension Cylinder Backward");break;
			case 96+20: strAdd.Format("Reserve");break;
			case 96+21: strAdd.Format("Reserve");break;
			case 96+22: strAdd.Format("Reserve");break;
			case 96+23: strAdd.Format("Reserve");break;
			case 96+24: strAdd.Format("Reserve");break;
			case 96+25: strAdd.Format("Reserve");break;
			case 96+26: strAdd.Format("Reserve");break;
			case 96+27: strAdd.Format("Reserve");break;
			case 96+28: strAdd.Format("Reserve");break;
			case 96+29: strAdd.Format("Reserve");break;
			case 96+30: strAdd.Format("Reserve");break;
			case 96+31: strAdd.Format("Reserve");break;

				//Block E Output
			case 128+  0: strAdd.Format("Picker Stick Absorb 01_100mm");break;
			case 128+  1: strAdd.Format("Picker Stick Absorb 02_100mm");break;
			case 128+  2: strAdd.Format("Picker Stick Absorb 03_100mm");break;
			case 128+  3: strAdd.Format("Picker Stick Absorb 04_200mm");break;
			case 128+  4: strAdd.Format("Picker Stick Absorb 05_200mm");break;
			case 128+  5: strAdd.Format("Picker Stick Absorb 06_440mm");break;
			case 128+  6: strAdd.Format("Picker Stick Absorb 07_440mm");break;
			case 128+  7: strAdd.Format("Picker Stick Absorb 08_440mm");break;
			case 128+  8: strAdd.Format("Picker Stick Absorb 09_440mm");break;
			case 128+  9: strAdd.Format("Picker Stick Absorb 10_440mm");break;
			case 128+10: strAdd.Format("Picker Stick Purge 01_100mm");break;
			case 128+11: strAdd.Format("Picker Stick Purge 02_100mm");break;
			case 128+12: strAdd.Format("Picker Stick Purge 03_100mm");break;
			case 128+13: strAdd.Format("Picker Stick Purge 04_200mm");break;
			case 128+14: strAdd.Format("Picker Stick Purge 05_200mm");break;
			case 128+15: strAdd.Format("Picker Stick Purge 06_440mm");break;
			case 128+16: strAdd.Format("Picker Stick Purge 07_440mm");break;
			case 128+17: strAdd.Format("Picker Stick Purge 08_440mm");break;
			case 128+18: strAdd.Format("Picker Stick Purge 09_440mm");break;
			case 128+19: strAdd.Format("Picker Stick Purge 10_440mm");break;
			case 128+20: strAdd.Format("Reserve");break;
			case 128+21: strAdd.Format("Reserve");break;
			case 128+22: strAdd.Format("Reserve");break;
			case 128+23: strAdd.Format("Reserve");break;
			case 128+24: strAdd.Format("Reserve");break;
			case 128+25: strAdd.Format("Reserve");break;
			case 128+26: strAdd.Format("Reserve");break;
			case 128+27: strAdd.Format("Reserve");break;
			case 128+28: strAdd.Format("Reserve");break;
			case 128+29: strAdd.Format("Reserve");break;
			case 128+30: strAdd.Format("Reserve");break;
			case 128+31: strAdd.Format("Reserve");break;

				//Block F Output
			case 160+  0: strAdd.Format("Picker Paper Absorb 01_100mm");break;
			case 160+  1: strAdd.Format("Picker Paper Absorb 02_100mm");break;
			case 160+  2: strAdd.Format("Picker Paper Absorb 03_100mm");break;
			case 160+  3: strAdd.Format("Picker Paper Absorb 04_200mm");break;
			case 160+  4: strAdd.Format("Picker Paper Absorb 05_200mm");break;
			case 160+  5: strAdd.Format("Picker Paper Absorb 06_440mm");break;
			case 160+  6: strAdd.Format("Picker Paper Absorb 07_440mm");break;
			case 160+  7: strAdd.Format("Picker Paper Absorb 08_440mm");break;
			case 160+  8: strAdd.Format("Picker Paper Absorb 09_440mm");break;
			case 160+  9: strAdd.Format("Picker Paper Absorb 10_440mm");break;
			case 160+10: strAdd.Format("Picker Paper Purge 01_100mm");break;
			case 160+11: strAdd.Format("Picker Paper Purge 02_100mm");break;
			case 160+12: strAdd.Format("Picker Paper Purge 03_100mm");break;
			case 160+13: strAdd.Format("Picker Paper Purge 04_200mm");break;
			case 160+14: strAdd.Format("Picker Paper Purge 05_200mm");break;
			case 160+15: strAdd.Format("Picker Paper Purge 06_440mm");break;
			case 160+16: strAdd.Format("Picker Paper Purge 07_440mm");break;
			case 160+17: strAdd.Format("Picker Paper Purge 08_440mm");break;
			case 160+18: strAdd.Format("Picker Paper Purge 09_440mm");break;
			case 160+19: strAdd.Format("Picker Paper Purge 10_440mm");break;
			case 160+20: strAdd.Format("Picker Paper Left Right Up Sol");break;
			case 160+21: strAdd.Format("Picker Paper Left Right Down Sol");break;
			case 160+22: strAdd.Format("Picker Paper Center Up Sol	");break;
			case 160+23: strAdd.Format("Picker Paper Center Down Sol	");break;
			case 160+24: strAdd.Format("Reserve");break;
			case 160+25: strAdd.Format("Reserve");break;
			case 160+26: strAdd.Format("Reserve");break;
			case 160+27: strAdd.Format("Reserve");break;
			case 160+28: strAdd.Format("Reserve");break;
			case 160+29: strAdd.Format("Reserve");break;
			case 160+30: strAdd.Format("Reserve");break;
			case 160+31: strAdd.Format("Reserve");break;

				//Block G Output
			case 192+  0: strAdd.Format("Turn Table Upper Stick Absorb 01_100mm");break;
			case 192+  1: strAdd.Format("Turn Table Upper Stick Absorb 02_100mm");break;
			case 192+  2: strAdd.Format("Turn Table Upper Stick Absorb 03_100mm");break;
			case 192+  3: strAdd.Format("Turn Table Upper Stick Absorb 04_200mm");break;
			case 192+  4: strAdd.Format("Turn Table Upper Stick Absorb 05_200mm");break;
			case 192+  5: strAdd.Format("Turn Table Upper Stick Absorb 06_440mm");break;
			case 192+  6: strAdd.Format("Turn Table Upper Stick Absorb 07_440mm");break;
			case 192+  7: strAdd.Format("Turn Table Upper Stick Absorb 08_440mm");break;
			case 192+  8: strAdd.Format("Turn Table Upper Stick Absorb 09_440mm");break;
			case 192+  9: strAdd.Format("Turn Table Upper Stick Absorb 10_440mm");break;
			case 192+10: strAdd.Format("Turn Table Upper Stick Purge 01_100mm");break;
			case 192+11: strAdd.Format("Turn Table Upper Stick Purge 02_100mm");break;
			case 192+12: strAdd.Format("Turn Table Upper Stick Purge 03_100mm");break;
			case 192+13: strAdd.Format("Turn Table Upper Stick Purge 04_200mm");break;
			case 192+14: strAdd.Format("Turn Table Upper Stick Purge 05_200mm");break;
			case 192+15: strAdd.Format("Turn Table Upper Stick Purge 06_440mm");break;
			case 192+16: strAdd.Format("Turn Table Upper Stick Purge 07_440mm");break;
			case 192+17: strAdd.Format("Turn Table Upper Stick Purge 08_440mm");break;
			case 192+18: strAdd.Format("Turn Table Upper Stick Purge 09_440mm");break;
			case 192+19: strAdd.Format("Turn Table Upper Stick Purge 10_440mm");break;
			case 192+20: strAdd.Format("Reserve");break;
			case 192+21: strAdd.Format("Reserve");break;
			case 192+22: strAdd.Format("Reserve");break;
			case 192+23: strAdd.Format("Reserve");break;
			case 192+24: strAdd.Format("Reserve");break;
			case 192+25: strAdd.Format("Reserve");break;
			case 192+26: strAdd.Format("Reserve");break;
			case 192+27: strAdd.Format("Reserve");break;
			case 192+28: strAdd.Format("Reserve");break;
			case 192+29: strAdd.Format("Reserve");break;
			case 192+30: strAdd.Format("Reserve");break;
			case 192+31: strAdd.Format("Reserve");break;

				//Block H Output
			case 224+  0: strAdd.Format("Turn Table Lower Stick Absorb 01_100mm");break;
			case 224+  1: strAdd.Format("Turn Table Lower Stick Absorb 02_100mm");break;
			case 224+  2: strAdd.Format("Turn Table Lower Stick Absorb 03_100mm");break;
			case 224+  3: strAdd.Format("Turn Table Lower Stick Absorb 04_200mm");break;
			case 224+  4: strAdd.Format("Turn Table Lower Stick Absorb 05_200mm");break;
			case 224+  5: strAdd.Format("Turn Table Lower Stick Absorb 06_440mm");break;
			case 224+  6: strAdd.Format("Turn Table Lower Stick Absorb 07_440mm");break;
			case 224+  7: strAdd.Format("Turn Table Lower Stick Absorb 08_440mm");break;
			case 224+  8: strAdd.Format("Turn Table Lower Stick Absorb 09_440mm");break;
			case 224+  9: strAdd.Format("Turn Table Lower Stick Absorb 10_440mm");break;
			case 224+10: strAdd.Format("Turn Table Lower Stick Purge 01_100mm");break;
			case 224+11: strAdd.Format("Turn Table Lower Stick Purge 02_100mm");break;
			case 224+12: strAdd.Format("Turn Table Lower Stick Purge 03_100mm");break;
			case 224+13: strAdd.Format("Turn Table Lower Stick Purge 04_200mm");break;
			case 224+14: strAdd.Format("Turn Table Lower Stick Purge 05_200mm");break;
			case 224+15: strAdd.Format("Turn Table Lower Stick Purge 06_440mm");break;
			case 224+16: strAdd.Format("Turn Table Lower Stick Purge 07_440mm");break;
			case 224+17: strAdd.Format("Turn Table Lower Stick Purge 08_440mm");break;
			case 224+18: strAdd.Format("Turn Table Lower Stick Purge 09_440mm");break;
			case 224+19: strAdd.Format("Turn Table Lower Stick Purge 10_440mm");break;
			case 224+20: strAdd.Format("Reserve");break;
			case 224+21: strAdd.Format("Reserve");break;
			case 224+22: strAdd.Format("Reserve");break;
			case 224+23: strAdd.Format("Reserve");break;
			case 224+24: strAdd.Format("Reserve");break;
			case 224+25: strAdd.Format("Reserve");break;
			case 224+26: strAdd.Format("Reserve");break;
			case 224+27: strAdd.Format("Reserve");break;
			case 224+28: strAdd.Format("Reserve");break;
			case 224+29: strAdd.Format("Reserve");break;
			case 224+30: strAdd.Format("Reserve");break;
			case 224+31: strAdd.Format("Reserve");break;

				//Block I Output
			case 256+  0: strAdd.Format("Out Put Stick Absorb 01_100mm");break;
			case 256+  1: strAdd.Format("Out Put Stick Absorb 02_100mm");break;
			case 256+  2: strAdd.Format("Out Put Stick Absorb 03_100mm");break;
			case 256+  3: strAdd.Format("Out Put Stick Absorb 04_200mm");break;
			case 256+  4: strAdd.Format("Out Put Stick Absorb 05_200mm");break;
			case 256+  5: strAdd.Format("Out Put Stick Absorb 06_440mm");break;
			case 256+  6: strAdd.Format("Out Put Stick Absorb 07_440mm");break;
			case 256+  7: strAdd.Format("Out Put Stick Absorb 08_440mm");break;
			case 256+  8: strAdd.Format("Out Put Stick Absorb 09_440mm");break;
			case 256+  9: strAdd.Format("Out Put Stick Absorb 10_440mm");break;
			case 256+10: strAdd.Format("Out Put Stick Purge 01_100mm");break;
			case 256+11: strAdd.Format("Out Put Stick Purge 02_100mm");break;
			case 256+12: strAdd.Format("Out Put Stick Purge 03_100mm");break;
			case 256+13: strAdd.Format("Out Put Stick Purge 04_200mm");break;
			case 256+14: strAdd.Format("Out Put Stick Purge 05_200mm");break;
			case 256+15: strAdd.Format("Out Put Stick Purge 06_440mm");break;
			case 256+16: strAdd.Format("Out Put Stick Purge 07_440mm");break;
			case 256+17: strAdd.Format("Out Put Stick Purge 08_440mm");break;
			case 256+18: strAdd.Format("Out Put Stick Purge 09_440mm");break;
			case 256+19: strAdd.Format("Out Put Stick Purge 10_440mm");break;
			case 256+20: strAdd.Format("Output Table Cylinder Up Sol");break;
			case 256+21: strAdd.Format("Output Table Cylinder Down Sol");break;
			case 256+22: strAdd.Format("Reserve");break;
			case 256+23: strAdd.Format("Reserve");break;
			case 256+24: strAdd.Format("Output Scan Light Cylinder Up Sol");break;
			case 256+25: strAdd.Format("Output Scan Light Cylinder Down Sol");break;
			case 256+26: strAdd.Format("Output Review Cylinder Up Sol");break;
			case 256+27: strAdd.Format("Output Review Cylinder Down Sol");break;
			case 256+28: strAdd.Format("Output 3D Electromagnet On");break;
			case 256+29: strAdd.Format("Reserve");break;
			case 256+30: strAdd.Format("Reserve");break;
			case 256+31: strAdd.Format("Reserve");break;
			}
			if(strAdd.CompareNoCase("Reserve")==0)
				m_GridOutputList.SetItemBkColour(AddStep, LineStep, m_BitOffColor);

			m_GridOutputList.SetItemText(AddStep , LineStep, strAdd.GetBuffer(strAdd.GetLength()));
			AddIndex++;
		}
	}
	m_GridInputList.Refresh();
	m_GridOutputList.Refresh();
}
DWORD InputBlock1[9];
DWORD OutputBlock1[9];
void CIOMapDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if (nIDEvent == IO_READ_TIME && IsWindowVisible() == TRUE)
	{
#ifndef SIMUL_UI_TEST
		G_MotCtrlObj.GetInputData(&InputBlock1[0]);
		G_MotCtrlObj.GetOutputData(&OutputBlock1[0]);
#else
		InputBlock1[0]++;
		InputBlock1[1]++;
		InputBlock1[2]++;
		InputBlock1[3]++;
		InputBlock1[4]++;
		InputBlock1[5]++;
		InputBlock1[6]++;
		InputBlock1[7]++;
		InputBlock1[8]++;

		OutputBlock1[0]--;
		OutputBlock1[1]--;
		OutputBlock1[2]--;
		OutputBlock1[3]--;
		OutputBlock1[4]--;
		OutputBlock1[5]--;
		OutputBlock1[6]--;
		OutputBlock1[7]--;
		OutputBlock1[8]--;
#endif

		int BlockStep = 0;
		int AddIndex = 0;
		for(int LineStep = 0; LineStep<(18*4); LineStep+=4) 
		{
			if(AddIndex == 32)
			{
				BlockStep++;
				AddIndex =0;
			}

			for(int AddStep = 1; AddStep<=16; AddStep++)
			{
				if( ((InputBlock1[BlockStep]>>AddIndex)  &0x01) ==1)
					m_GridInputList.SetItemBkColour(AddStep, LineStep, m_BitOnColor);
				else
					m_GridInputList.SetItemBkColour(AddStep, LineStep, m_BitOffColor);

				AddIndex++;
			}
		}
		m_GridInputList.Refresh();

		BlockStep = 0;
		AddIndex = 0;
		for(int LineStep = 0; LineStep<(18*4); LineStep+=4) 
		{
			if(AddIndex == 32)
			{
				BlockStep++;
				AddIndex =0;
			}

			for(int AddStep = 1; AddStep<=16; AddStep++)
			{
				if( ((OutputBlock1[BlockStep]>>AddIndex)  &0x01) ==1)
					m_GridOutputList.SetItemBkColour(AddStep, LineStep, m_BitOnColor);
				else
					m_GridOutputList.SetItemBkColour(AddStep, LineStep, m_BitOffColor);

				AddIndex++;
			}
		}
		m_GridOutputList.Refresh();
	}

	CDialogEx::OnTimer(nIDEvent);
}

HBRUSH CIOMapDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	return hbr;
}

void CIOMapDlg::OnBnClickedBtnIoInExit()
{
	ShowWindow(SW_HIDE);
}

BOOL CIOMapDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}
void CIOMapDlg::OnTagGridClickOuput(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
	int nRow = pItem->iRow;//   <<- 선택된 ROW
	int nCol = pItem->iColumn;//  <<- 선택된 COLUMN

	if(nCol%4==1)//4로나누어 1이남는 위치
	{
		int MemBlock = nCol/8;
		int BitBlock16 = (nCol/4)%2;
		DWORD NowOutPut[9];
		G_MotCtrlObj.GetOutputData(&NowOutPut[0]);
		DWORD TargetBlock = NowOutPut[MemBlock];

		int BitPos = 0;
		if(BitBlock16 == 0)//16bit이하.
		{
			BitPos = (nRow-1);
		}
		else//32bit 이하.
		{
			BitPos = 16+(nRow-1);
		}
		TargetBlock ^= 0x01<<BitPos;
	
		//MemBlock
		//if(SelectIndex<32)
		//{
		//	if(SelectIndex>7 && SelectIndex <= 15)//사용하지 않는 Bit
		//		return;

		//	if( ((OutputBlock1buf>>SelectIndex)  &0x01) ==1)//켜져 있으면.
		//	{
		//		OutputBlock1buf ^= 0x01<<SelectIndex;
		//	}
		//	else
		//	{
		//		OutputBlock1buf ^= 0x01<<SelectIndex;
		//	}
		//}
		//else
		//{
		//	SelectIndex-=32;

		//	if( ((OutputBlock2buf>>SelectIndex)  &0x01) ==1)//켜져 있으면.
		//	{
		//		OutputBlock2buf ^= 0x01<<SelectIndex;
		//	}
		//	else
		//	{
		//		OutputBlock2buf ^= 0x01<<SelectIndex;
		//	}
		//}


		NowOutPut[MemBlock] = TargetBlock;
		G_MotCtrlObj.SetOutputData(&NowOutPut[0]);
	}
}
void CIOMapDlg::OnBnClickedBtnIoInSelect()
{
	//m_stSelectIO.SetWindowText("INPUT I/O");
	//m_GridInputList.ShowWindow(SW_SHOW);
	//m_GridOutputList.ShowWindow(SW_HIDE);

	//m_btInputSelect.EnableWindow(FALSE);
	//m_btOutputSelect.EnableWindow(TRUE);

}

void CIOMapDlg::OnBnClickedBtnIoOutSelect()
{
	//m_stSelectIO.SetWindowText("OUTPUT I/O");
	//m_GridInputList.ShowWindow(SW_HIDE);
	//m_GridOutputList.ShowWindow(SW_SHOW);

	//m_btInputSelect.EnableWindow(TRUE);
	//m_btOutputSelect.EnableWindow(FALSE);
}


void CIOMapDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CDialogEx::PostNcDestroy();
	delete this;
}
