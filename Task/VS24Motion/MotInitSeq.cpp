#include "StdAfx.h"
#include "MotInitSeq.h"


CMotInitSeq::CMotInitSeq(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
}


CMotInitSeq::~CMotInitSeq(void)
{
}
bool CMotInitSeq::fnSeqFlowStart()
{
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}

void CMotInitSeq::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
//void  CMotInitSeq::G_WriteInfo(ListColor nColorType, const char* pWritLog, ...)
//{
//	if(m_plistSeqInfo == nullptr)
//		return;
//	char bufLog[LOG_TEXT_MAX_SIZE];
//	memset(bufLog, 0x00, LOG_TEXT_MAX_SIZE);
//	//////////////////////////////////////////////////////////////////////////
//	va_list vargs;
//	va_start(vargs, pWritLog);
//	int LenStr = vsprintf_s((char*)bufLog, LOG_TEXT_MAX_SIZE-1,pWritLog, (va_list)vargs);
//	va_end(vargs);
//
//
//	if(m_plistSeqInfo->GetCount() > LIST_ALARM_COUNT)
//		m_plistSeqInfo->DeleteString(0);
//	CXListBoxAddString(m_plistSeqInfo, nColorType, bufLog);
//	m_plistSeqInfo->SetScrollPos(SB_VERT , m_plistSeqInfo->GetCount(), TRUE);
//	m_plistSeqInfo->SetTopIndex(m_plistSeqInfo->GetCount() - 1);	
//
//	//Ns_WriteLogProc(bufLog);
//}
int CMotInitSeq::fnSeqErrorStepProc()
{

	return 0;
}
int CMotInitSeq::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> Sequence Start", ProcStep);
			G_SystemInitOK = false;
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
		//Sys Pram Load
	case SEQ_LOAD_PARM:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_LOAD_PARM", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_ACC_LINK_OPNE:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_ACC_LINK_OPNE", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
		//초기화 진행전에 상태 Check.
	case SEQ_CHECK_IO_START:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_START", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_CHECK_IO_SYS:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_SYS", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_CHECK_IO_BOX:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_BOX", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_CHECK_IO_STICK:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_STICK", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_CHECK_IO_TT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_TT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_CHECK_IO_BT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_BT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_CHECK_IO_LT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_LT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_CHECK_IO_BO01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_BO01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_CHECK_IO_UT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_UT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_CHECK_IO_TM01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_TM01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_CHECK_IO_TM02:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_TM02", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_CHECK_IO_AM01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_AM01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_CHECK_IO_END:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_CHECK_IO_END", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
		//초기화 시작.
	case SEQ_INIT_CYL_START:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_START", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_CYL_TT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_TT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_CYL_BT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_BT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_CYL_LT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_LT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_CYL_BO01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_BO01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_CYL_UT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_UT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_CYL_TM01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_TM01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_INIT_CYL_TM02:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_TM02", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_INIT_CYL_AM01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_AM01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_INIT_CYL_END:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_CYL_END", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
		//Motion
		case SEQ_INIT_MOT_START:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_START", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_MOT_TT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_TT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_INIT_MOT_BT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_BT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;	
	case SEQ_INIT_MOT_LT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_LT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_MOT_BO01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_BO01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_MOT_UT01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_UT01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_MOT_TM01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_TM01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_MOT_TM02:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_TM02", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_INIT_MOT_AM01:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_AM01", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
			//////////////////////////////////////////////////////////////////////////
	case SEQ_INIT_MOT_END:
		{
			G_WriteInfo(Log_Normal, "<Sys Init_%03d> SEQ_INIT_MOT_END", ProcStep);
			Sleep(200);
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			G_WriteInfo(Log_Normal, "<S1Seq_%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<Step1Seq> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}