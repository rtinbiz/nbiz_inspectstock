#pragma once

class CMDFunList
{
public:
	//TT01
	int fn_Seq_TT01_TableDrive(bool *pBreakProc, VS24MotData*pProcData);//Cruiser Axis #1
	int fn_Seq_TT01_ThetaAlign(bool *pBreakProc, VS24MotData*pProcData);//ML3 #18
	int fn_Seq_TT01_TableTurn(bool *pBreakProc, VS24MotData*pProcData);//ML3 #19
	//BT01
	int fn_Seq_BT01_CassetteUpDown(bool *pBreakProc, VS24MotData*pProcData);//ML3 #22
	//LT01
	int fn_Seq_LT01_BoxLoadPush(bool *pBreakProc, VS24MotData*pProcData);//ML3 #14
	//BO01
	int fn_Seq_BO01_BoxRotate(bool *pBreakProc, VS24MotData*pProcData);//ML3 #10, #11 //좌우 동기축.
	int fn_Seq_BO01_BoxUpDn(bool *pBreakProc, VS24MotData*pProcData);//ML3 #12, #13 //좌우 동기축.
	int fn_Seq_BO01_BoxDrive(bool *pBreakProc, VS24MotData*pProcData);//ML3 #16
	//UT01
	int fn_Seq_UT01_BoxUnLoadPush(bool *pBreakProc, VS24MotData*pProcData);//ML3 #15
	//TM01
	int fn_Seq_TM01_ForwardBackward(bool *pBreakProc, VS24MotData*pProcData);//ML3 #17
	//TM02
	int fn_Seq_TM02_PickerDrive(bool *pBreakProc, VS24MotData*pProcData);//Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
	int fn_Seq_TM02_PickerUpDown(bool *pBreakProc, VS24MotData*pProcData);//ML3 #20
	int fn_Seq_TM02_PickerShift(bool *pBreakProc, VS24MotData*pProcData);//ML3 #21
	int fn_Seq_TM02_PickerRotate(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_A Y
	//AM01
	int fn_Seq_AM01_ScanDrive(bool *pBreakProc, VS24MotData*pProcData);//Cruiser Axis #6
	int fn_Seq_AM01_ScanShift(bool *pBreakProc, VS24MotData*pProcData);//Cruiser Axis #2
	int fn_Seq_AM01_ScanUpDn(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_D X
	int fn_Seq_AM01_SpaceSnUpDn(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_D Y
	int fn_Seq_AM01_ScopeDrive(bool *pBreakProc, VS24MotData*pProcData);//Cruiser Axis #5
	int fn_Seq_AM01_ScopeShift(bool *pBreakProc, VS24MotData*pProcData);//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축
	int fn_Seq_AM01_ScopeUpDn(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_D Z
	int fn_Seq_AM01_AirBlowLeftRight(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_D U

	int fn_Seq_AM01_ScanMultiMove(bool *pBreakProc, VS24MotData*pProcData);//Cruiser Axis #6//Cruiser Axis #2
	int fn_Seq_AM01_ScopeMultiMove(bool *pBreakProc, VS24MotData*pProcData);//Cruiser Axis #5//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축
	
	int fn_Seq_AM01_SetReviewLensOffset(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_SetScopeLensOffset(bool *pBreakProc, VS24MotData*pProcData);


	int fn_Seq_AM01_ScanScopeSyncMove(bool *pBreakProc, VS24MotData*pProcData);

	int fn_Seq_AM01_ScopeSpaceZSyncMove(bool *pBreakProc, VS24MotData*pProcData);

	int fn_Seq_AM01_TensionUpDn(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_A Z //좌우 동기화축.
	//int fn_Seq_AM01_TensionRightUpDn(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_A U
	int fn_Seq_AM01_TensionStart(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_SpaceMeasureStart(bool *pBreakProc, VS24MotData*pProcData);
	
	int fn_Seq_AM01_LGripper1(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_B X//Home 명령을 받을수있다.
	int fn_Seq_AM01_LGripper2(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_B Y
	int fn_Seq_AM01_LGripper3(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_B Z
	int fn_Seq_AM01_LGripper4(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_B U
	int fn_Seq_AM01_RGripper1(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_C X//Home 명령을 받을수있다.
	int fn_Seq_AM01_RGripper2(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_C Y
	int fn_Seq_AM01_RGripper3(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_C Z
	int fn_Seq_AM01_RGripper4(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_C U

	int fn_Seq_AM01_JigInOut(bool *pBreakProc, VS24MotData*pProcData);//ML3 #23

	int fn_Seq_AM01_LTensionMulti(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_B block
	int fn_Seq_AM01_RTensionMulti(bool *pBreakProc, VS24MotData*pProcData);//AxisLink_C block

	int fn_Seq_SetTrigger_Inspection(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_SetTrigger_AreaScan(bool *pBreakProc, VS24MotData*pProcData);

	//nBiz_Func_CylinderAct:
	int fn_Seq_TM01_FORK_UP(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_TM01_FORK_DN(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_LT01_GUIDE_FWD(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_LT01_GUIDE_BWD(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_UT01_GUIDE_FWD(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_UT01_GUIDE_BWD(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_BO01_PICKER_FWD(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_BO01_PICKER_BWD(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_TM02_LR_PAPER_PICKER_UP(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_TM02_LR_PAPER_PICKER_DN(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_TM02_CNT_PAPER_PICKER_UP(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_TM02_CNT_PAPER_PICKER_DN(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_OPEN_TEN_GRIPPER(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_CLOSE_TEN_GRIPPER(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_TENTION_FWD(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_TENTION_BWD(bool *pBreakProc, VS24MotData*pProcData);

	int fn_Seq_AM01_DoonTuk_Laser_On(bool *pBreakProc, VS24MotData*pProcData);  //정의천 추가 
	int fn_Seq_AM01_DoonTuk_Laser_Off(bool *pBreakProc, VS24MotData*pProcData); ////정의천 추가 

	int fn_Seq_AM01_RingLight_UP(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_RingLight_DN(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_ReviewLens_UP(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_AM01_ReviewLens_DN(bool *pBreakProc, VS24MotData*pProcData);

	int fn_Seq_UT02_TABLE_UP(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_UT02_TABLE_DN(bool *pBreakProc, VS24MotData*pProcData);

	int fn_Seq_Door_Release_Front(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Door_Release_Side(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Door_Release_Back(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Door_Lock_Front(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Door_Lock_Side(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Door_Lock_Back(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Set_SignalTower(bool *pBreakProc, VS24MotData*pProcData);

	//nBiz_Func_Vac:
	//int fn_Seq_TM01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_TM01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_TM01_Purge(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_LT01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_LT01_Purge(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_BO01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_BO01_Purge(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_UT01_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_UT01_Purge(bool *pBreakProc, VS24MotData*pProcData);
	//TM02
	int fn_Seq_TM02_ST_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_TM02_ST_Purge(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_TM02_PP_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_TM02_PP_Purge(bool *pBreakProc, VS24MotData*pProcData);
	//TT01
	int fn_Seq_TT01_UP_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_TT01_UP_Purge(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_TT01_DN_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_TT01_DN_Purge(bool *pBreakProc, VS24MotData*pProcData);
	//UT02
	int fn_Seq_UT02_Vac_ONOFF(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_UT02_Purge(bool *pBreakProc, VS24MotData*pProcData);

	int fn_Seq_Light_Scan(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Light_Scan_Back(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Light_Scan_Reflect(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Light_Align(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Light_Align1_2(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Light_Align2_1(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_Light_Align2_2(bool *pBreakProc, VS24MotData*pProcData);

	int fn_Seq_LoadCell_L_SetZero(bool *pBreakProc, VS24MotData*pProcData);
	int fn_Seq_LoadCell_R_SetZero(bool *pBreakProc, VS24MotData*pProcData);

	int fn_Seq_LoadCell_L_Start(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_LoadCell_R_Start(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_Read_QR(bool *pBreakProc, VS24MotData*pProcData, CString *pReturnValue);
	int fn_Seq_SPaceSensor(bool *pBreakProc, VS24MotData*pProcData, bool OnOff);
	int fn_Seq_Read_AirN2(bool *pBreakProc, VS24MotData*pProcData);




};

//thread Main function
UINT Thread_VS24CMDProc(LPVOID pParam);

typedef struct FuncProcThData_TAG
{
	
	//CMD Process(Q에서 투입전에 thread생성)
	bool					m_RunThread;
	CWinThread	*		m_pHandleThread;

	// Q에 등록된 자신의 Pointer이다. 종료시 빠른 등록 해지를 위해서
	void*					m_pCMDQ;//(ProcMsgQ)
	void*					m_MyQPointer;//(DataNodeObj)
	//CVS24MotionDlg
	LPVOID pCallObj;

	//CMD Sender Link
	CInterServerInterface * m_pCMDSender;//내부 명령시에는 Return을 하지않는다. 그래서 null을 넣는다.

	//수행 명령후 Return할 명령
	USHORT	uTask_Src;				///< 근원지 Task
	//USHORT	uFunID_Src;			///< 근원지 Function No
	//USHORT	uSeqID_Src;			///< 근원지 Sequence No
	//USHORT	uUnitID_Src;			///< 근원지 Unit No

	//수행할 명령 수신.
	USHORT	uTask_Dest;			///< 목적지 Task
	USHORT	uFunID_Dest;			///< 목적지 Function No
	USHORT	uSeqID_Dest;			///< 목적지 Sequence No
	USHORT	uUnitID_Dest;			///< 목적지 Unit No


	VS24MotData nMotionData;
	FuncProcThData_TAG()
	{
		m_RunThread		= false;
		m_pHandleThread = nullptr;
		m_pCMDQ			= nullptr;
		m_MyQPointer		= nullptr;

		m_pCMDSender	= nullptr;
		pCallObj				= nullptr;


		uTask_Src			= 0;
		//uFunID_Src			= 0;
		//uSeqID_Src			= 0;	
		//uUnitID_Src			= 0;

		uTask_Dest			= 0;	
		uFunID_Dest		= 0;	
		uSeqID_Dest		= 0;
		uUnitID_Dest		= 0;	

	}
	~FuncProcThData_TAG()
	{
		m_RunThread		= false;
		int KillThreadTimeOut = 500;
		while(m_pHandleThread != nullptr)
		{
			KillThreadTimeOut--;
			if(KillThreadTimeOut<=0)
			{
				TerminateThread(m_pHandleThread->m_hThread, 0);
				break;
			}
			Sleep(10);
		}
		m_pCMDSender	= nullptr;
		pCallObj				= nullptr;

		m_pCMDQ			= nullptr;
		m_MyQPointer		= nullptr;

		uTask_Src			= 0;
		//uFunID_Src			= 0;
		//uSeqID_Src			= 0;	
		//uUnitID_Src			= 0;

		uTask_Dest			= 0;	
		uFunID_Dest		= 0;	
		uSeqID_Dest		= 0;
		uUnitID_Dest		= 0;	

	}
}FnProcThData;


typedef struct DataNodeObj_AGT
{
	DataNodeObj_AGT *	m_pFrontNode;
	DataNodeObj_AGT *	m_pNextNode;

	FnProcThData thData;

	DataNodeObj_AGT()
	{
		m_pFrontNode	= NULL;
		m_pNextNode		= NULL;
	}

}DataNodeObj;
//////////////////////////////////////////////////////////////////////////
//Sen Command Queue./ Receive Data Queue.
class ProcMsgQ  
{
public:
	ProcMsgQ();
	~ProcMsgQ();
private:
	DataNodeObj m_HeadNode;
	DataNodeObj m_TailNode; 
	CRITICAL_SECTION m_CrtSection;
	int		m_CntNode;

	//현재 같은 명령이 진행중이면 추가 하지 않는다.(실행중이면 true를 반환)
	bool	QNowProcCheck(FnProcThData	*pNewData);
public:
	
	int		QGetCnt(){return m_CntNode;};
	bool	QPutNode(FnProcThData	*pNewData);
	

	FnProcThData	QGetNode();
	void	QClean(void);

	bool	QSelfDelete(DataNodeObj *pSelfDel);
};
//////////////////////////////////////////////////////////////////////////
