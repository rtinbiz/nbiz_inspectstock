#include "StdAfx.h"
#include "BCRCtrl.h"
UINT Thread_BCRCMDProc(LPVOID pParam)
{
	CBCRCtrl *pPortObj = (CBCRCtrl*)pParam;

	const int buflen = 2048*5;
	char buf[buflen] = {0};
	int nReturn = 0;

	int TokenPacket = 0;
	char *pBufPos = nullptr;

	while(pPortObj->m_RunThread == true && pPortObj->m_BCRSockfd != INVALID_SOCKET)
	{
		memset(buf, 0x00, buflen);
		nReturn = recv(pPortObj->m_BCRSockfd, buf, buflen-1, 0);
		if (nReturn == 0 || nReturn == SOCKET_ERROR)
		{
			pPortObj->m_RunThread  = false;
			break;
		}
		DWORD CntMsg = 0;
		char *pStartPos = &buf[0];
		char *pEndPos = &buf[0];
		for(int RStep= 0; RStep<nReturn; RStep++)
		{
			if(*pEndPos == 0 || *pEndPos =='\n')
			{
				pPortObj->RecvCallBack((LPVOID)pStartPos, CntMsg);

				CntMsg = 0;
				pEndPos = pEndPos+1;
				pStartPos = pEndPos;
			}
			else
			{
				CntMsg++;
				pEndPos = pEndPos+1;
			}
		}
		
		Sleep(10);
	}
	pPortObj->m_pHandleThread = nullptr;
	return 0;
}

VOID CALLBACK BCRTimerRoutine(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
	if (lpParam == NULL)
	{
		//printf("TimerRoutine lpParam is NULL\n");
	}
	else
	{
		CBCRCtrl *pCallObj =(CBCRCtrl *) lpParam;
		pCallObj->SetTriggerOnOff(false);
		if(pCallObj->m_ParentUpdate != 0)
			::SendMessage(pCallObj->m_ParentUpdate, WM_USER_ACC_CODE_UPDATE, 2, NULL);//LParam이 NULL이면 Time Out

		if(TimerOrWaitFired)
		{
			//printf("The wait timed out.\n");
		}
		else
		{
			//printf("The wait event was signaled.\n");
		}
	}
}

CBCRCtrl::CBCRCtrl(void)
{
	 m_BCRSockfd= INVALID_SOCKET; 
	 m_ParentUpdate = 0;

	 bTimerRun = false;
	 m_pHandleThread = nullptr;
	 hTimer = NULL;
	 hTimerQueue = NULL;
	 // Create the timer queue.
	 hTimerQueue = CreateTimerQueue();
	 if (NULL == hTimerQueue)
	 {
		 //printf("CreateTimerQueue failed (%d)\n", GetLastError());
	 }
}


CBCRCtrl::~CBCRCtrl(void)
{
	if (!DeleteTimerQueue(hTimerQueue))
	{
		//printf("DeleteTimerQueue failed (%d)\n", GetLastError());
	}
	hTimerQueue = NULL;
}

void CBCRCtrl::SetConnectInfo(CString IPAddress, CString PortName)
{
	m_IPAddress.Format("%s", IPAddress);
	m_PortName.Format("%s", PortName);
}
bool CBCRCtrl::BCRConnect(DWORD nIPADD, int PortNum)
{
	WSADATA SendData; 
	struct sockaddr_in addrTClient; 

	memset((void *)&addrTClient, 0x00, sizeof(addrTClient)); 
	addrTClient.sin_family = AF_INET; 
	addrTClient.sin_addr.s_addr = nIPADD; 
	addrTClient.sin_port = htons(PortNum);

	//Socke Time Out Setting
	//struct timeval tv_timeo = { 2, 500000 };  /* 3.5 second */
	int Optval = 3000;//Time Out
	int TestInc = 0;

	int SendLen =0;
	int RecvLen =0;

	if(m_BCRSockfd != INVALID_SOCKET)
		closesocket(m_BCRSockfd);
	m_BCRSockfd = INVALID_SOCKET;

	//통신 Clear 
	if(WSAStartup(MAKEWORD(2,2), &SendData) != NO_ERROR) 
	{ 
		return 0;
	} 
	//Winsocket Create(Client Sock)
	if((m_BCRSockfd = socket(AF_INET,SOCK_STREAM, IPPROTO_HOPOPTS)) == INVALID_SOCKET)  
	{ 
		return false;
	} 

	//접속 시도는 Blocking Mode로 하고 접속 후에는 None Blocking으로 전환 하자.
	ULONG nonBlk = TRUE;
	struct  timeval     timevalue;

	ioctlsocket( m_BCRSockfd, FIONBIO, &nonBlk );
	connect ( m_BCRSockfd,  (struct sockaddr *)&addrTClient, sizeof(addrTClient));
	fd_set fdset;
	FD_ZERO( &fdset );
	FD_SET( m_BCRSockfd, &fdset );
	timevalue.tv_sec = 4;
	timevalue.tv_usec = 0;

	::select( m_BCRSockfd+1, NULL, &fdset, NULL, &timevalue );
	if ( !FD_ISSET( m_BCRSockfd, &fdset ) )
	{
		closesocket( m_BCRSockfd );
		WSACleanup();
		m_BCRSockfd = INVALID_SOCKET;
		return false;
	}
	nonBlk = FALSE;
	ioctlsocket( m_BCRSockfd, FIONBIO, &nonBlk );//None Blocking 에서 Blocking으로 변환 한다.
	if(m_pHandleThread == nullptr)
	{
		m_RunThread =  true;
		m_pHandleThread = ::AfxBeginThread(Thread_BCRCMDProc, this,	 THREAD_PRIORITY_NORMAL);
	}
	return true;
}
bool CBCRCtrl::BCRDisconnect()
{
	if(m_BCRSockfd != INVALID_SOCKET)
		closesocket(m_BCRSockfd);
	m_BCRSockfd = INVALID_SOCKET;
	m_RunThread =  false;
	while(m_pHandleThread != nullptr)
		Sleep(10);
	return true;
}

void CBCRCtrl::RecvCallBack(LPVOID pBuf, DWORD recvLen)
{
	TimerStop();//Data가 들어왔음으로 Time Out을 종료 한다.

	char RecvMSG[128];
	memset(RecvMSG, 0x00, 128);
	recvLen = (recvLen>127?127:recvLen);
	memcpy(RecvMSG, (char*)pBuf, recvLen);

	if(m_ParentUpdate != 0)
		::SendMessage(m_ParentUpdate, WM_USER_ACC_CODE_UPDATE, 2, (LPARAM)RecvMSG);

	SetLightAimerOnOff(false);
}
int CBCRCtrl::TimerStop()
{
	if(bTimerRun == true)
		CancelTimerQueueTimer(hTimerQueue, hTimer);

	bTimerRun = false;
	return 0;
}
int CBCRCtrl::TimerRun()
{
	TimerStop();//기존에 돌고 있던것이 있으면 취소.
	// Set a timer to call the timer routine in 10 seconds.
	if (!CreateTimerQueueTimer( &hTimer, hTimerQueue, 
		(WAITORTIMERCALLBACK)BCRTimerRoutine, (LPVOID)this  , 3000, 0, 0))
	{
		printf("CreateTimerQueueTimer failed (%d)\n", GetLastError());
		return 3;
	}
	bTimerRun = true;
	return 0;
}
//////////////////////////////////////////////////////////////////////////

bool CBCRCtrl::LinkOpen()
{
	if(m_pHandleThread == nullptr)//null이 아니명 연결 상태.
	{
		if(BCRConnect(inet_addr(BCR_NET_IP), BCR_NET_PORT) != true)
		{
			return false;
		}
	}
	
	return true;
}
bool CBCRCtrl::SetTriggerOnOff(bool OnOff)//Data Read를 할때 On을 한다.
{

	if(OnOff==true)
	{
		SetLightAimerOnOff(true);
		TimerRun();//Data Read Time Out을 시작 한다.
	}
	else
	{
		SetLightAimerOnOff(false);
	}

	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r\n", (OnOff?BCR_TRIGGER_ON:BCR_TRIGGER_OFF));

	if(m_BCRSockfd != INVALID_SOCKET)
	{
		if(SOCKET_ERROR != send(m_BCRSockfd, SendMSG, strlen(SendMSG), 0))
			return true;
	}
	return false;
}
bool CBCRCtrl::SetLightAimerOnOff(bool OnOff)//Code위치를 지정 할때 켠다.
{
	char SendMSG[128];
	memset(SendMSG, 0x00, 128);
	sprintf_s(SendMSG, "%s\r\n", (OnOff?BCR_LIGHT_AIMER_ON:BCR_LIGHT_AIMER_OFF));
	if(m_BCRSockfd != INVALID_SOCKET)
	{
		if(SOCKET_ERROR != send(m_BCRSockfd, SendMSG, strlen(SendMSG), 0))
			return true;
	}
	return false;
}