#pragma once

typedef struct MotThreadData_Tag
{
	bool					m_RunIOThread;
	CWinThread *		m_pThreadObj;

	void*					m_pCallObj;

	DWORD				m_ThCyclesTime;
	MotThreadData_Tag()
	{
		m_RunIOThread = false;
		m_pThreadObj = nullptr;
		m_pCallObj = nullptr;

		m_ThCyclesTime = 0;
	}

}MotThreadData;

#define  Cruiser_Encoder_DivValue1 6.41
#define  Cruiser_Encoder_DivValue2 20

UINT DataUpdateThread(LPVOID pParam);
UINT IOScenarioProcThread(LPVOID pParam);

class CMotCtrl
{
public:
	CMotCtrl(void);
	~CMotCtrl(void);
public:
	HINSTANCE m_hPmacLib;	// Pmac Runtime Library를 사용하기 위한 Handle을 정의
	BOOL m_bDriverOpen;		// Driver Open 되었는지 확인하기 위한 BOOL 변수
	DWORD m_dwDevice;		// 사용할 Device 번호 정의

public:
	bool InitMotDevice();


public:
	inline CString m_fnUmacOnlineCommand(CString strCommand);

	// IO Read/Write
public:
	
	//AL_INPUT_CHECK	m_InputCheck;
	ALIO_A_In		m_IO_InA;
	ALIO_B_In		m_IO_InB;
	ALIO_C_In		m_IO_InC;
	ALIO_D_In		m_IO_InD;
	ALIO_E_In		m_IO_InE;
	ALIO_F_In		m_IO_InF;
	ALIO_G_In		m_IO_InG;
	ALIO_H_In		m_IO_InH;
	ALIO_I_In		m_IO_InI;

	ALIO_A_Out		m_IO_OutA;
	ALIO_B_Out		m_IO_OutB;
	ALIO_C_Out		m_IO_OutC;
	ALIO_D_Out		m_IO_OutD;
	ALIO_E_Out		m_IO_OutE;
	ALIO_F_Out		m_IO_OutF;
	ALIO_G_Out		m_IO_OutG;
	ALIO_H_Out		m_IO_OutH;
	ALIO_I_Out		m_IO_OutI;

	bool				m_bScopeSafetyPos;
private:
	ALIO_A_In			m_OldIO_InA;
	ALIO_B_In			m_OldIO_InB;
	ALIO_C_In			m_OldIO_InC;
	ALIO_D_In			m_OldIO_InD;
	ALIO_E_In			m_OldIO_InE;
	ALIO_F_In			m_OldIO_InF;
	ALIO_G_In			m_OldIO_InG;
	ALIO_H_In			m_OldIO_InH;
	ALIO_I_In			m_OldIO_InI;


	ALIO_A_Out		m_OldIO_OutA;
	ALIO_B_Out			m_OldIO_OutB;
	ALIO_C_Out		m_OldIO_OutC;
	ALIO_D_Out		m_OldIO_OutD;
	ALIO_E_Out			m_OldIO_OutE;
	ALIO_F_Out			m_OldIO_OutF;
	ALIO_G_Out		m_OldIO_OutG;
	ALIO_H_Out		m_OldIO_OutH;
	ALIO_I_Out			m_OldIO_OutI;
public:
	void GetInputData(DWORD *pBlock1);
	void GetOutputData(DWORD *pBlock1);
	void SetOutputData(DWORD *pBlock1);
	void m_fnMotionPerInit();


	int m_fnSpaceSensorLaserOnOff(bool OnOff);
public:
	//Motion 상태 IO Read Write를 수행한다 Block 단위다.
	MotThreadData m_MotStateThData;
	void m_fnRunStateReadThread();
	//읽어 들인 I/O 처리를 담담한다.
	MotThreadData m_IOProcessThData;
	void m_fnIOProcessThread();

	void m_fnStopProcThreadAll();

	//////////////////////////////////////////////////////////////////////////
	DWORD m_UpdateTime_IO;
	bool m_fnUpdateIO();
	DWORD m_UpdateTime_MotState;
	bool m_fnUpdateLocalMotState();
	DWORD m_UpdateTime_ML3State;
	bool m_fnUpdateML3MotState();
	DWORD m_UpdateTime_ALinkState;
	bool m_fnUpdateAxisLinkMotState();

	DWORD m_UpdateTime_AxisPos;
	bool m_fnUpdateAxisPosition();
	bool m_fnIOTeachKeyUnlockEnableCheck();//Teach => Auto Mode 전환시 체크.항목.
	bool m_fnIOMCCUpdateProc();
	bool m_fnIOScenarioProc();

	void m_fnLensOffsetApplyCheck();
	
public:
	//Signal Tower
	bool m_bBuzzerOFF;
	STWOER_MODE m_SignalTowerMode;
	int m_fnSignalTowerSet(STWOER_MODE STMode);
	void m_fnSignalTModeCheck();
	void m_fnSignalTModeReset(STWOER_MODE STMode);//모드 우선순위와 상관없이 변경.
	void m_fnSignalTowerBuzzerOn();
	void m_fnSignalTowerBuzzerOff();
public:
	bool m_bReviewLensOffsetApply;
	int m_ReviewLensOffsetShiftApply_Value;
	int m_ReviewLensOffsetDriveApply_Value;

	int m_ReviewLensOffsetShift;
	int m_ReviewLensOffsetDrive;

	bool m_bScopeLensOffsetApply;
	int m_ScopeLensOffsetShiftApply_Value;
	int m_ScopeLensOffsetDriveApply_Value;

	int m_ScopeLensOffsetShift;
	int m_ScopeLensOffsetDrive;

	int m_AxisNowPos[AXIS_Cnt];//동기 or Gantry는 기준축 위치만 표시

	AxisStateCruiser m_LocalAxisST[8+2];//가상축 #30=>8 #31=>9 Index를 갖는다.
	//AxisStateCruiser m_LocalAxisST_Old[8+2];

	AxisStateML3 m_ML3AxisST[13+1];
	//AxisStateML3 m_ML3AxisST_Old[13];

	AxisStateALink m_ALinkAxisST[16];
	//AxisStateALink m_ALinkAxisST_Old[16];

	bool				m_PLCProActive[32];
	int					m_BT01CouplingCnt[2];
public:
	//동기화 축에 좌우의 편차를 보정 하기 위한 값.(초기화에서 ??)
	long m_BO01_BoxRotateOffset;
	long m_BO01_BoxUpDnOffset;
	long m_AM01_TensionUpDnOffset;
public:
	//////////////////////////////////////////////////////////////////////////
	//Motion Ctrl
	bool m_fnMotHomeInterlockCheck(int AxisNum);
	bool m_fnMotHoming(int AxisNum);

	bool m_fnMotInterlockCheck(int AxisNum, long newPosition);
	bool m_fnMotAbsMove(int AxisNum, long newPosition);
	bool m_fnMotStop(int AxisNum);

	bool m_fnMotAllStop();

	bool m_fnMotStandby(int AxisNum);
	bool m_fnMotIdling(int AxisNum);

	int m_NowAllSpeedMode;
	bool m_fnSetMotSpeed(int SpeedMode, int SelectAxis =-1);//Select Axis 가 0이면 전축
	//////////////////////////////////////////////////////////////////////////
	//TT01
	bool m_fnTT01HomeInterlockCheck(int AxisNum);
	bool m_fnTT01Homing(int AxisNum);

	bool m_fnTT01InterlockCheck(int AxisNum, long newPosition);
	bool m_fnTT01AbsMove(int AxisNum, long newPosition);
	bool m_fnTT01Stop(int AxisNum);
	bool m_fnTT01Standby(int AxisNum);
	bool m_fnTT01Idling(int AxisNum);
	//////////////////////////////////////////////////////////////////////////
	//LT01
	bool m_fnLT01HomeInterlockCheck(int AxisNum);
	bool m_fnLT01Homing(int AxisNum);

	bool m_fnLT01InterlockCheck(int AxisNum, long newPosition);
	bool m_fnLT01AbsMove(int AxisNum, long newPosition);
	bool m_fnLT01Stop(int AxisNum);

	bool m_fnLT01Standby(int AxisNum);
	bool m_fnLT01Idling(int AxisNum);
	//////////////////////////////////////////////////////////////////////////
	//BT01
	bool m_fnBT01HomeInterlockCheck(int AxisNum);
	bool m_fnBT01Homing(int AxisNum);

	bool m_fnBT01InterlockCheck(int AxisNum, long newPosition);
	bool m_fnBT01AbsMove(int AxisNum, long newPosition);
	bool m_fnBT01Stop(int AxisNum);

	bool m_fnBT01Standby(int AxisNum);
	bool m_fnBT01Idling(int AxisNum);
	//////////////////////////////////////////////////////////////////////////
	//BO01
	bool m_fnBO01HomeInterlockCheck(int AxisNum);
	bool m_fnBO01Homing(int AxisNum);

	bool m_fnBO01InterlockCheck(int AxisNum, long newPosition);
	bool m_fnBO01AbsMove(int AxisNum, long newPosition);
	bool m_fnBO01Stop(int AxisNum);

	bool m_fnBO01Standby(int AxisNum);
	bool m_fnBO01Idling(int AxisNum);
	//////////////////////////////////////////////////////////////////////////
	//UT01
	bool m_fnUT01HomeInterlockCheck(int AxisNum);
	bool m_fnUT01Homing(int AxisNum);

	bool m_fnUT01InterlockCheck(int AxisNum, long newPosition);
	bool m_fnUT01AbsMove(int AxisNum, long newPosition);
	bool m_fnUT01Stop(int AxisNum);

	bool m_fnUT01Standby(int AxisNum);
	bool m_fnUT01Idling(int AxisNum);
	//////////////////////////////////////////////////////////////////////////
	//TM01
	bool m_fnTM01HomeInterlockCheck(int AxisNum);
	bool m_fnTM01Homing(int AxisNum);

	bool m_fnTM01InterlockCheck(int AxisNum, long newPosition);
	bool m_fnTM01AbsMove(int AxisNum, long newPosition);
	bool m_fnTM01Stop(int AxisNum);

	bool m_fnTM01Standby(int AxisNum);
	bool m_fnTM01Idling(int AxisNum);
	//////////////////////////////////////////////////////////////////////////
	//TM02
	bool m_fnTM02HomeInterlockCheck(int AxisNum);
	bool m_fnTM02Homing(int AxisNum);

	bool m_fnTM02InterlockCheck(int AxisNum, long newPosition);
	bool m_fnTM02AbsMove(int AxisNum, long newPosition);
	bool m_fnTM02Stop(int AxisNum);

	bool m_fnTM02Standby(int AxisNum);
	bool m_fnTM02Idling(int AxisNum);

	//////////////////////////////////////////////////////////////////////////
	//AM01
	bool m_fnAM01HomeInterlockCheck(int AxisNum);
	bool m_fnAM01Homing(int AxisNum);
	//bool m_fnAM01ScanMultiHomingXY();
	//bool m_fnAM01ScopeMultiHomingXY();

	bool m_fnAM01InterlockCheck(int AxisNum, long newPosition);
	bool m_fnAM01AbsMove(int AxisNum, long newPosition);
	bool m_fnAM01Stop(int AxisNum);

	bool m_fnAM01Standby(int AxisNum);
	bool m_fnAM01Idling(int AxisNum);

	bool m_fnAM01ScanMultiMoveXY(long newPosShift, long newPosDirve);
	bool m_fnAM01ScanMultiStopXY();

	bool m_fnAM01ScopeMultiMoveXY(long newPosShift, long newPosDirve);
	bool m_fnAM01ScopeMultiStopXY();

	bool m_fnAM01SyncMoveXY(long newScanPosShift, long newScanPosDirve, long newScopePosShift, long newScopePosDirve);
	bool m_fnAM01SyncStopXY();

	bool m_fnAM01TensionUpDn(long newPosition);
	bool m_fnAM01TensionStart(int SelectAxis, float fTargetValue);
	bool m_fnAM01TensionStop();

	bool m_fnAM01SpaceMeasureStart(float fSpaceValue);
	bool m_fnAM01SpaceMeasureStop();
	//////////////////////////////////////////////////////////////////////////
	//AxisLink_B XGripper Left Home은 축 X를 지정 하고 전체 가 Home을 수행한다.
	bool m_fnAM01TenstionLMultiAbsMoveInterlockCheck();
	bool m_fnAM01TenstionLMultiAbsMove(int SelectAxis, int AxisXPos, int AxisYPos, int AxisZPos, int AxisUPos );
	bool m_fnAM01TenstionLMultiAbsStop(bool AllEStop, int SelectAxis);
	//AxisLink_C XGripper Left Home은 축 X를 지정 하고 전체 가 Home을 수행한다.
	bool m_fnAM01TenstionRMultiAbsMoveInterlockCheck();
	bool m_fnAM01TenstionRMultiAbsMove(int SelectAxis, int AxisXPos, int AxisYPos, int AxisZPos, int AxisUPos );
	bool m_fnAM01TenstionRMultiAbsStop(bool AllEStop, int SelectAxis);

	bool fn_SetTrigger_Inspection(float TriggerSize_um);
	bool fn_SetTrigger_AreaScan(float TriggerSize_um);

	//////////////////////////////////////////////////////////////////////////
	//Cylinder Action
	bool m_fnTM01SolInterLockCheck_FORK_UP();
	bool m_fnTM01SolInterLockCheck_FORK_DN();
	bool m_fnLT01SolInterLockCheck_GUIDE_FWD();
	bool m_fnLT01SolInterLockCheck_GUIDE_BWD();
	bool m_fnUT01SolInterLockCheck_GUIDE_FWD();
	bool m_fnUT01SolInterLockCheck_GUIDE_BWD();
	bool m_fnBO01SolInterLockCheck_PICKER_FWD();
	bool m_fnBO01SolInterLockCheck_PICKER_BWD();
	/*	bool m_fnBO01SolInterLockCheck_RIGHT_PICKER_FWD();
	bool m_fnBO01SolInterLockCheck_RIGHT_PICKER_BWD()*/;

	bool m_fnTM02SolInterLockCheck_LR_PAPER_PICKER_UP();
	bool m_fnTM02SolInterLockCheck_LR_PAPER_PICKER_DN();
	bool m_fnTM02SolInterLockCheck_CNT_PAPER_PICKER_UP();
	bool m_fnTM02SolInterLockCheck_CNT_PAPER_PICKER_DN();

	bool m_fnAM01SolInterLockCheck_OPEN_TEN_GRIPPER();
	bool m_fnAM01SolInterLockCheck_CLOSE_TEN_GRIPPER();

	bool m_fnAM01SolInterLockCheck_TENTION_FWD();
	bool m_fnAM01SolInterLockCheck_TENTION_BWD();

	bool m_fnAM01SolInterLockCheck_RingLight_UP();
	bool m_fnAM01SolInterLockCheck_RingLight_DN();

	bool m_fnAM01SolInterLockCheck_Review_UP();
	bool m_fnAM01SolInterLockCheck_Review_DN();

	bool m_fnUT02SolInterLockCheck_TABLE_UP();
	bool m_fnUT02SolInterLockCheck_TABLE_DN();

	//Cylinder Sol 상태
	int m_fnGetTM01State_CySol_FORK();
	int m_fnGetLT01State_CySol_GUIDE();
	int m_fnGetUT01State_CySol_GUIDE();
	int m_fnGetBO01State_CySol_PICKER();

	int m_fnGetTM02State_CySol_LR_PAPER_PICKER();
	int m_fnGetTM02State_CySol_CNT_PAPER_PICKER();

	int m_fnGetAM01State_CySol_TEN_GRIPPER1();
	int m_fnGetAM01State_CySol_TEN_GRIPPER2();
	int m_fnGetAM01State_CySol_TEN_GRIPPER3();
	int m_fnGetAM01State_CySol_TEN_GRIPPER4();

	int m_fnGetAM01State_CySol_TENTION();

	int m_fnGetAM01State_DoonTuk_Laser_OnOff();

	int m_fnGetAM01State_CySol_RingLight();
	int m_fnGetAM01State_CySol_Review();

	int m_fnGetUT02State_CySol_TABLE();

	bool m_fnGetDoorReleaseStateFront();
	bool m_fnGetDoorReleaseStateSide();
	bool m_fnGetDoorReleaseStateBack();

	void m_fnSetDoorReleaseFront(bool OnOff);
	void m_fnSetDoorReleaseSide(bool OnOff);
	void m_fnSetDoorReleaseBack(bool OnOff);


};

