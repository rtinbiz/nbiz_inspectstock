#pragma once
#include "afxwin.h"


// CMotStateDlg 대화 상자입니다.

class CMotStateDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMotStateDlg)

public:
	CMotStateDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMotStateDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MOT_STATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()


public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	virtual void PostNcDestroy();
private:
	CRITICAL_SECTION m_CrtSection;//Update Pointer 전환시 


	//Cruiser/ML3 A, B State
	CFont m_GridFont;
	CFont m_SpeedFont;
	CGridCtrl m_GridMotState1;
	CGridCtrl m_GridMotState2;
	int m_NowViewStateAxis;
	void m_fnSpeedTableRead(int SelectAxis);

	COLORREF m_BitOnColor;
	COLORREF m_BitOffColor;
	COLORREF m_BitNoneColor;

	AxisStateCruiser *m_pCuData_StateCu;
	AxisStateCruiser *m_pCuData_StateCuL;
	AxisStateCruiser *m_pCuData_StateCuR;

	AxisStateML3 *m_pCuData_StateML3;
	AxisStateML3 *m_pCuData_StateML3L;
	AxisStateML3 *m_pCuData_StateML3R;

	AxisStateALink *m_pALink_State;
	
	void UpdateMotStateCu();
	void UpdateMotStateML3();
	void UpdateMotStateAxisLink();
public:
	bool *m_pUIUpdate;
	void ViewMotState_CruiserML3(int AxisNum);
	void ViewMotState_Axislink(int AxisNum);

	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btSaveSpeedParam;
	afx_msg void OnBnClickedBtSaveSpeedParam();
	int m_rdSpeedMode;
	afx_msg void OnEnChangeEdNorMoveAccG();
};
