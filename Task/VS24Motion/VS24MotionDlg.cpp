
// VS90SampleDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VS24Motion.h"
#include "VS24MotionDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CVS24MotionDlg 대화 상자




CVS24MotionDlg::CVS24MotionDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVS24MotionDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_pSampleView			= nullptr;
	m_pViewIOMap			= nullptr;
	m_pViewACCDlg			= nullptr;
	m_pViewMotState		= nullptr;
	m_pViewSolCtrlDlg		= nullptr;


	m_ExpendUI				= false;
	m_AxisSelectedIndex		= -1;

	m_VS24TaskState		= TASK_STATE_NONE;

}
CVS24MotionDlg::~CVS24MotionDlg()
{
	m_AxisSelectedIndex		= -1;

	if(m_pSampleView != nullptr)
		cvReleaseImage(&m_pSampleView);
	m_pSampleView = nullptr;

	CloseMotionMem(&G_MotInfo);
}
void CVS24MotionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);


	DDX_Control(pDX, IDC_ST_PLC_01, m_stPLCState[0]);
	DDX_Control(pDX, IDC_ST_PLC_2,	m_stPLCState[1]);
	DDX_Control(pDX, IDC_ST_PLC_3,	m_stPLCState[2]);
	DDX_Control(pDX, IDC_ST_PLC_4,	m_stPLCState[3]);
	DDX_Control(pDX, IDC_ST_PLC_5,	m_stPLCState[4]);
	DDX_Control(pDX, IDC_ST_PLC_6,	m_stPLCState[5]);
	DDX_Control(pDX, IDC_ST_PLC_7,	m_stPLCState[6]);
	DDX_Control(pDX, IDC_ST_PLC_8,	m_stPLCState[7]);
	DDX_Control(pDX, IDC_ST_PLC_9,	m_stPLCState[8]);
	DDX_Control(pDX, IDC_ST_PLC_10, m_stPLCState[9]);
	DDX_Control(pDX, IDC_ST_PLC_11, m_stPLCState[10]);
	DDX_Control(pDX, IDC_ST_PLC_12, m_stPLCState[11]);
	DDX_Control(pDX, IDC_ST_PLC_13, m_stPLCState[12]);
	DDX_Control(pDX, IDC_ST_PLC_14, m_stPLCState[13]);
	DDX_Control(pDX, IDC_ST_PLC_15, m_stPLCState[14]);
	DDX_Control(pDX, IDC_ST_PLC_16, m_stPLCState[15]);
	DDX_Control(pDX, IDC_ST_PLC_17, m_stPLCState[16]);
	DDX_Control(pDX, IDC_ST_PLC_18, m_stPLCState[17]);
	DDX_Control(pDX, IDC_ST_PLC_19, m_stPLCState[18]);
	DDX_Control(pDX, IDC_ST_PLC_20, m_stPLCState[19]);
	DDX_Control(pDX, IDC_ST_PLC_21, m_stPLCState[20]);
	DDX_Control(pDX, IDC_ST_PLC_22, m_stPLCState[21]);
	DDX_Control(pDX, IDC_ST_PLC_23, m_stPLCState[22]);
	DDX_Control(pDX, IDC_ST_PLC_24, m_stPLCState[23]);
	DDX_Control(pDX, IDC_ST_PLC_25, m_stPLCState[24]);
	DDX_Control(pDX, IDC_ST_PLC_26, m_stPLCState[25]);
	DDX_Control(pDX, IDC_ST_PLC_27, m_stPLCState[26]);
	DDX_Control(pDX, IDC_ST_PLC_28, m_stPLCState[27]);
	DDX_Control(pDX, IDC_ST_PLC_29, m_stPLCState[28]);
	DDX_Control(pDX, IDC_ST_PLC_30, m_stPLCState[29]);
	DDX_Control(pDX, IDC_ST_PLC_31, m_stPLCState[30]);
	DDX_Control(pDX, IDC_ST_PLC_32, m_stPLCState[31]);

	DDX_Control(pDX, IDC_BT_TT01_TableDrive,				m_btAxisSelect[0]);
	DDX_Control(pDX,IDC_BT_TT01_ThetaAlign,				m_btAxisSelect[1]);
	DDX_Control(pDX,IDC_BT_TT01_TableTurn,				m_btAxisSelect[2]);
	DDX_Control(pDX,IDC_BT_BT01_CassetteUpDown,		m_btAxisSelect[3]);
	DDX_Control(pDX,IDC_BT_LT01_BoxLoadPush,			m_btAxisSelect[4]);
	DDX_Control(pDX,IDC_BT_BO01_BoxRotate,				m_btAxisSelect[5]);
	DDX_Control(pDX,IDC_BT_BO01_BoxUpDn,					m_btAxisSelect[6]);
	DDX_Control(pDX,IDC_BT_BO01_BoxDrive,					m_btAxisSelect[7]);
	DDX_Control(pDX,IDC_BT_UT01_BoxUnLoadPush,		m_btAxisSelect[8]);
	DDX_Control(pDX,IDC_BT_TM01_ForwardBackward,	m_btAxisSelect[9]);
	DDX_Control(pDX,IDC_BT_TM02_PickerDrive,				m_btAxisSelect[10]);
	DDX_Control(pDX,IDC_BT_TM02_PickerUpDown,			m_btAxisSelect[11]);
	DDX_Control(pDX,IDC_BT_TM02_PickerShift,				m_btAxisSelect[12]);
	DDX_Control(pDX,IDC_BT_TM02_PickerRotate,			m_btAxisSelect[13]);
	DDX_Control(pDX,IDC_BT_AM01_ScanDrive,				m_btAxisSelect[14]);
	DDX_Control(pDX,IDC_BT_AM01_ScanShift,				m_btAxisSelect[15]);
	DDX_Control(pDX,IDC_BT_AM01_ScanUpDn,				m_btAxisSelect[16]);
	DDX_Control(pDX,IDC_BT_AM01_SpaceSnUpDn,			m_btAxisSelect[17]);
	DDX_Control(pDX,IDC_BT_AM01_ScopeDrive,				m_btAxisSelect[18]);
	DDX_Control(pDX,IDC_BT_AM01_ScopeShift,				m_btAxisSelect[19]);
	DDX_Control(pDX,IDC_BT_AXIS_AM01_ScopeUpDn,		m_btAxisSelect[20]);
	DDX_Control(pDX,IDC_BT_AM01_AirBlowLeftRight,		m_btAxisSelect[21]);
	DDX_Control(pDX,IDC_BT_AM01_TensionLeftUpDn,		m_btAxisSelect[22]);
	DDX_Control(pDX,IDC_BT_AM01_TensionRightUpDn	,	m_btAxisSelect[23]);
	DDX_Control(pDX,IDC_BT_AM01_LTension1,				m_btAxisSelect[24]);
	DDX_Control(pDX,IDC_BT_AM01_LTension2,				m_btAxisSelect[25]);
	DDX_Control(pDX,IDC_BT_AM01_LTension3,				m_btAxisSelect[26]);
	DDX_Control(pDX,IDC_BT_AM01_LTension4,				m_btAxisSelect[27]);
	DDX_Control(pDX,IDC_BT_AM01_RTension1,				m_btAxisSelect[28]);
	DDX_Control(pDX,IDC_BT_AM01_RTension2,				m_btAxisSelect[29]);
	DDX_Control(pDX,IDC_BT_AM01_RTension3,				m_btAxisSelect[30]);
	DDX_Control(pDX,IDC_BT_AM01_RTension4,				m_btAxisSelect[31]);
	DDX_Control(pDX,IDC_BT_AM01_JigInOut,					m_btAxisSelect[32]);


	DDX_Control(pDX, IDC_BT_MOT_HOME, m_btAxisHoming);
	DDX_Control(pDX, IDC_BT_MOT_LEFT, m_btAxisLeft);
	DDX_Control(pDX, IDC_BT_MOT_RIGHT, m_btAxisRight);
	DDX_Control(pDX, IDC_BT_MOT_MOVE_POS1, m_btAxisABSMove[0]);
	DDX_Control(pDX, IDC_BT_MOT_MOVE_POS2, m_btAxisABSMove[1]);
	DDX_Control(pDX, IDC_BT_MOT_MOVE_POS3, m_btAxisABSMove[2]);
	DDX_Control(pDX, IDC_BT_MOT_MOVE_POS4, m_btAxisABSMove[3]);
	DDX_Control(pDX, IDC_BT_MOT_MOVE_POS5, m_btAxisABSMove[4]);
	DDX_Control(pDX, IDC_BT_HW_IO_VIEW, m_btIOView);
	DDX_Control(pDX, IDC_BT_EXPEND, m_btExpendUI);
	DDX_Control(pDX, IDC_BT_MOT_TASK_EXIT, m_btExitSW);
	DDX_Control(pDX, IDC_BT_MOT_SYSTEM_INIT, m_btMotSysInit);
	DDX_Control(pDX, IDC_BT_MOT_STOP, m_btAxisStop);
	DDX_Control(pDX, IDC_BT_CHANGE_VIEW, m_btViewChange);
	DDX_Control(pDX, IDC_LIST_MOT_LOG, m_listLogView);
	DDX_Control(pDX, IDC_ST_TASK_STATE, m_stTaskState);
	DDX_Control(pDX, IDC_BT_MOT_SYSTEM_PM, m_btSetPMMode);





	//TT01
	DDX_Control(pDX, IDC_ED_TT01_TableDrive, m_editMotPostion[0]);//Cruiser Axis #1
	DDX_Control(pDX, IDC_ED_TT01_ThetaAlign, m_editMotPostion[1]);//ML3 #18
	DDX_Control(pDX, IDC_ED_TT01_TableTurn, m_editMotPostion[2]);				//ML3 #19

		//BT01
	DDX_Control(pDX, IDC_ED_BT01_CassetteUpDown, m_editMotPostion[3]);		//ML3 #22

		//LT01
	DDX_Control(pDX, IDC_ED_LT01_BoxLoadPush, m_editMotPostion[4]);			//ML3 #14

		//BO01
	DDX_Control(pDX, IDC_ED_BO01_BoxRotate, m_editMotPostion[5]);					//ML3 #10, #11 //좌우 동기축.
	DDX_Control(pDX, IDC_ED_BO01_BoxUpDn, m_editMotPostion[6]);					//ML3 #12, #13 //좌우 동기축.
	DDX_Control(pDX, IDC_ED_BO01_BoxDrive, m_editMotPostion[7]);					//ML3 #16

		//UT01
	DDX_Control(pDX, IDC_ED_UT01_BoxUnLoadPush, m_editMotPostion[8]);			//ML3 #15

		//TM01
	DDX_Control(pDX, IDC_ED_TM01_ForwardBackward, m_editMotPostion[9]);		//ML3 #17

		//TM02
	DDX_Control(pDX, IDC_ED_TM02_PickerDrive, m_editMotPostion[10]);			//Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
	DDX_Control(pDX, IDC_ED_TM02_PickerUpDown, m_editMotPostion[11]);			//ML3 #20
	DDX_Control(pDX, IDC_ED_TM02_PickerShift, m_editMotPostion[12]);			//ML3 #21
	DDX_Control(pDX, IDC_ED_TM02_PickerRotate, m_editMotPostion[13]);			//AxisLink_A Y

		//AM01
	DDX_Control(pDX, IDC_ED_AM01_ScanDrive, m_editMotPostion[14]);				//Cruiser Axis #6
	DDX_Control(pDX, IDC_ED_AM01_ScanShift, m_editMotPostion[15]);				//Cruiser Axis #2
	DDX_Control(pDX, IDC_ED_AM01_ScanUpDn, m_editMotPostion[16]);				//AxisLink_D X
	DDX_Control(pDX, IDC_ED_AM01_SpaceSnUpDn, m_editMotPostion[17]);			//AxisLink_D Y

	DDX_Control(pDX, IDC_ED_AM01_ScopeDrive, m_editMotPostion[18]);				//Cruiser Axis #5
	DDX_Control(pDX, IDC_ED_AM01_ScopeShift, m_editMotPostion[19]);				//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
	DDX_Control(pDX, IDC_ED_AM01_ScopeUpDn, m_editMotPostion[20]);				//AxisLink_D Z
	DDX_Control(pDX, IDC_ED_AM01_AirBlowLeftRight, m_editMotPostion[21]);		//AxisLink_D U

	DDX_Control(pDX, IDC_ED_AM01_TensionLeftUpDn, m_editMotPostion[22]);		//AxisLink_A Z
	DDX_Control(pDX, IDC_ED_AM01_TensionRightUpDn, m_editMotPostion[23]);		//AxisLink_A U
	DDX_Control(pDX, IDC_ED_AM01_LTension1, m_editMotPostion[24]);				//AxisLink_B X
	DDX_Control(pDX, IDC_ED_AM01_LTension2, m_editMotPostion[25]);				//AxisLink_B Y
	DDX_Control(pDX, IDC_ED_AM01_LTension3, m_editMotPostion[26]);				//AxisLink_B Z
	DDX_Control(pDX, IDC_ED_AM01_LTension4, m_editMotPostion[27]);				//AxisLink_B U
	DDX_Control(pDX, IDC_ED_AM01_RTension1, m_editMotPostion[28]);				//AxisLink_C X
	DDX_Control(pDX, IDC_ED_AM01_RTension2, m_editMotPostion[29]);				//AxisLink_C Y
	DDX_Control(pDX, IDC_ED_AM01_RTension3, m_editMotPostion[30]);				//AxisLink_C Z
	DDX_Control(pDX, IDC_ED_AM01_RTension4, m_editMotPostion[31]);				//AxisLink_C U
	DDX_Control(pDX, IDC_ED_AM01_JigInOut, m_editMotPostion[32]);				//ML3 #23
}

BEGIN_MESSAGE_MAP(CVS24MotionDlg, CDialogEx)
	ON_MESSAGE(WM_VS64USER_CALLBACK, &CVS24MotionDlg::Sys_fnMessageCallback)//VS64 Interface
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BT_HW_IO_VIEW, &CVS24MotionDlg::OnBnClickedBtHwIoView)
	ON_BN_CLICKED(IDC_BT_MOT_TASK_EXIT, &CVS24MotionDlg::OnBnClickedBtMotTaskExit)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_EXPEND, &CVS24MotionDlg::OnBnClickedBtExpend)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BT_TT01_TableDrive, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_TT01_ThetaAlign, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_TT01_TableTurn, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_BT01_CassetteUpDown, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_BO01_BoxRotate, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_BO01_BoxUpDn, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_LT01_BoxLoadPush, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_UT01_BoxUnLoadPush,	 &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_BO01_BoxDrive, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_TM01_ForwardBackward, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_TM02_PickerDrive, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_TM02_PickerUpDown, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_TM02_PickerShift, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_TM02_PickerRotate, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_ScanDrive, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_ScanShift, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_ScanUpDn, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_SpaceSnUpDn, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_ScopeDrive, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_ScopeShift, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AXIS_AM01_ScopeUpDn, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_AirBlowLeftRight, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_TensionLeftUpDn, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_TensionRightUpDn, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_LTension1, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_LTension2, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_LTension3, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_LTension4, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_RTension1, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_RTension2, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_RTension3, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_RTension4, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_AM01_JigInOut, &CVS24MotionDlg::OnBnClickedBtAxisBtEvent)
	ON_BN_CLICKED(IDC_BT_MOT_HOME, &CVS24MotionDlg::OnBnClickedBtMotHome)
	ON_BN_CLICKED(IDC_BT_MOT_LEFT, &CVS24MotionDlg::OnBnClickedBtMotLeft)
	ON_BN_CLICKED(IDC_BT_MOT_RIGHT, &CVS24MotionDlg::OnBnClickedBtMotRight)
	ON_BN_CLICKED(IDC_BT_MOT_MOVE_POS1, &CVS24MotionDlg::OnBnClickedBtMotMovePos)
	ON_BN_CLICKED(IDC_BT_MOT_MOVE_POS2, &CVS24MotionDlg::OnBnClickedBtMotMovePos)
	ON_BN_CLICKED(IDC_BT_MOT_MOVE_POS3, &CVS24MotionDlg::OnBnClickedBtMotMovePos)
	ON_BN_CLICKED(IDC_BT_MOT_MOVE_POS4, &CVS24MotionDlg::OnBnClickedBtMotMovePos)
	ON_BN_CLICKED(IDC_BT_MOT_MOVE_POS5, &CVS24MotionDlg::OnBnClickedBtMotMovePos)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BT_MOT_SYSTEM_INIT, &CVS24MotionDlg::OnBnClickedBtMotSystemInit)
	ON_BN_CLICKED(IDC_BT_MOT_STOP, &CVS24MotionDlg::OnBnClickedBtMotStop)
	ON_BN_CLICKED(IDC_BT_CHANGE_VIEW, &CVS24MotionDlg::OnBnClickedBtChangeView)
	ON_MESSAGE(WM_USER_CYLINDER_INTERNAL_CMD, &CVS24MotionDlg::OnUserSendCylInternalCMDQ)
	ON_BN_CLICKED(IDC_BT_MOT_SYSTEM_PM, &CVS24MotionDlg::OnBnClickedBtMotSystemPm)
	
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CVS24MotionDlg 메시지 처리기

BOOL CVS24MotionDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	
	//////////////////////////////////////////////////////////////////////////
	//1) Server Interface Connection.
	bool bLinkVS64Server = Sys_fnInitVS64Interface();
	
	m_VS24TaskState = TASK_STATE_IDLE;//Test용
	G_TaskStateChange(this, &m_stTaskState, m_VS24TaskState);//Test용
	
	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	if(bLinkVS64Server == true)
	{
	
		G_pServerInterface = &m_ServerInterface;

		G_WriteInfo(Log_Normal, "Server Link OK");
		extern CXListBox *G_pLogList;
		G_pLogList = &m_listLogView;
		OpenMotionMem(&G_MotInfo);

		CString NewWindowsName;
		NewWindowsName.Format("VS64 - Motion Task(No.%02d)", m_ProcInitVal.m_nTaskNum);
		SetWindowText(NewWindowsName);

		//////////////////////////////////////////////////////////////////////////
		GetDlgItem(IDC_ST_IMG_VIEW)->GetClientRect(&m_RectiewArea);
		m_HDCView	=  GetDlgItem(IDC_ST_IMG_VIEW)->GetDC()->m_hAttribDC;
		m_pSampleView = cvLoadImage("./DSC_0078.jpg");
		DrawImageView();
		GetDlgItem(IDC_ST_ACC_AREA)->GetWindowRect(&m_AccDlgArea);
		//ClientToScreen(&m_AccDlgArea);
		ScreenToClient(&m_AccDlgArea);

		GetDlgItem(IDC_ST_MOT_STATE)->GetWindowRect(&m_MotStDlgArea);
		//ClientToScreen(&m_AccDlgArea);
		ScreenToClient(&m_MotStDlgArea);
	
		//////////////////////////////////////////////////////////////////////////
		//1) UI Init
		InitCtrl_UI();
		G_WriteInfo(Log_Normal, "================= Motion Task Start =================");
		//2) Mot Obj Init
		if(G_MotCtrlObj.InitMotDevice() == true)
			G_WriteInfo(Log_Normal, "Motion Device Open Ok(Device 0)");
		else
			G_WriteInfo(Log_Error, "Motion Device Open Fail");

		//////////////////////////////////////////////////////////////////////////
		SetTimer(TIMER_MOT_POS_UPDATE, 200, 0);
		SetTimer(TIMER_TOWER_SIGNAL, 1000, 0);
		SetTimer(TIMER_MOT_INIT_STATE, 500, 0);

		SetTimer(TIMER_UI_HIDE_CHECK, 1000, 0);
		
	}


	
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CVS24MotionDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVS24MotionDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		DrawImageView();
		CDialogEx::OnPaint();
	}
}
void CVS24MotionDlg::ChangeView_ACC_SOL(int ID_Viewer)
{
	switch(ID_Viewer)
	{
	case 1:
		{
			m_pViewACCDlg->ShowWindow(SW_SHOW);
			m_pViewSolCtrlDlg->ShowWindow(SW_HIDE);
		}break;
	case 2:
		{
			m_pViewACCDlg->ShowWindow(SW_HIDE);
			m_pViewSolCtrlDlg->ShowWindow(SW_SHOW);
		}break;
	case 3:
		{

		}break;
	default:
		break;
		
	}
}
void CVS24MotionDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;

	G_PLCStateFont.CreateFont(10, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");
	G_PosEditFont.CreateFont(25, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");
	G_InfoFont.CreateFont(15, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	GetDlgItem(IDC_ED_MOT_MOVE_STEP)->SetFont(&G_PosEditFont);
	GetDlgItem(IDC_ED_MOT_MOVE_POS1)->SetFont(&G_PosEditFont);
	GetDlgItem(IDC_ED_MOT_MOVE_POS2)->SetFont(&G_PosEditFont);
	GetDlgItem(IDC_ED_MOT_MOVE_POS3)->SetFont(&G_PosEditFont);
	GetDlgItem(IDC_ED_MOT_MOVE_POS4)->SetFont(&G_PosEditFont);
	GetDlgItem(IDC_ED_MOT_MOVE_POS5)->SetFont(&G_PosEditFont);
	

	//Sub Ctrl UI
	m_pViewIOMap	=  new CIOMapDlg(this);//1
	m_pViewIOMap->Create(IDD_DLG_IO_VIEW);
	m_pViewIOMap->ShowWindow(SW_HIDE);


	m_pViewACCDlg	=  new CAccCtrlDlg(this);//1
	m_pViewACCDlg->Create(IDD_DLG_ACC_INFO);
	m_pViewACCDlg->SetWindowPos(nullptr,  m_AccDlgArea.left, m_AccDlgArea.top, m_AccDlgArea.Width(), m_AccDlgArea.Height(), SWP_NOSIZE);
	m_pViewACCDlg->ShowWindow(SW_SHOW);
	m_pViewSolCtrlDlg	=  new CCtrlSolPanel(this);//1
	m_pViewSolCtrlDlg->Create(IDD_DLG_SOL_ACTION);
	m_pViewSolCtrlDlg->SetWindowPos(nullptr,  m_AccDlgArea.left, m_AccDlgArea.top, m_AccDlgArea.Width(), m_AccDlgArea.Height(), SWP_NOSIZE);
	m_pViewSolCtrlDlg->ShowWindow(SW_HIDE);

	m_pViewMotState	=  new CMotStateDlg(this);//1
	m_pViewMotState->Create(IDD_DLG_MOT_STATE);
	m_pViewMotState->SetWindowPos(nullptr,  m_MotStDlgArea.left, m_MotStDlgArea.top, m_MotStDlgArea.Width(), m_MotStDlgArea.Height(), SWP_NOSIZE);
	m_pViewMotState->ShowWindow(SW_SHOW);
	m_pViewMotState->m_pUIUpdate = &m_ExpendUI;// UI가 Display시에만 Update를 하기 위해서.

	for(int StStep =0; StStep<32; StStep++)
	{
		m_stPLCState[StStep].SetFont(&G_PLCStateFont);
		m_stPLCState[StStep].SetTextColor(RGB(0,0,0));
		m_stPLCState[StStep].SetBkColor(G_Color_Off);
	}
	m_stTaskState.SetFont(&G_PosEditFont);
	m_stTaskState.SetTextColor(RGB(0,0,0));
	m_stTaskState.SetBkColor(G_Color_Off);
	
	//////////////////////////////////////////////////////////////////////////
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);


	
	// Get default Style Dark Gray
	m_tBTStyle_MotNotInit.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 12.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x20, 0x20, 0x20);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tBTStyle_MotNotInit.SetButtonStyle(&tStyle);




	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btAxisSelect[0].GetTextColor(&tColor);
		m_btAxisSelect[0].GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		int BTIndex = 0;
		for(int BTStep = 0; BTStep<AXIS_Cnt; BTStep++)
		{
			m_btAxisSelect[BTStep].SetRoundButtonStyle(&m_tBTStyle_MotNotInit);
			m_btAxisSelect[BTStep].SetTextColor(&tColor);
			m_btAxisSelect[BTStep].SetCheckButton(true, true);
			m_btAxisSelect[BTStep].SetFont(&tFont);
			m_btAxisSelect[BTStep].m_SettingFlag = -1;
		}

		m_btAxisHoming.SetRoundButtonStyle(&m_tButtonStyle);
		m_btAxisHoming.SetTextColor(&tColor);
		m_btAxisHoming.SetCheckButton(false);
		m_btAxisHoming.SetFont(&tFont);

		m_btAxisLeft.SetRoundButtonStyle(&m_tButtonStyle);
		m_btAxisLeft.SetTextColor(&tColor);
		m_btAxisLeft.SetCheckButton(false);
		m_btAxisLeft.SetFont(&tFont);

		m_btAxisRight.SetRoundButtonStyle(&m_tButtonStyle);
		m_btAxisRight.SetTextColor(&tColor);
		m_btAxisRight.SetCheckButton(false);
		m_btAxisRight.SetFont(&tFont);

		m_btAxisStop.SetRoundButtonStyle(&m_tButtonStyle);
		m_btAxisStop.SetTextColor(&tColor);
		m_btAxisStop.SetCheckButton(false);
		m_btAxisStop.SetFont(&tFont);

		m_btViewChange.SetRoundButtonStyle(&m_tButtonStyle);
		m_btViewChange.SetTextColor(&tColor);
		m_btViewChange.SetCheckButton(false);
		m_btViewChange.SetFont(&tFont);
		
		m_btSetPMMode.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSetPMMode.SetTextColor(&tColor);
		m_btSetPMMode.SetCheckButton(true, true);
		m_btSetPMMode.SetFont(&tFont);

		
		for(int BTStep = 0; BTStep<5; BTStep++)
		{
			m_btAxisABSMove[BTStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btAxisABSMove[BTStep].SetTextColor(&tColor);
			m_btAxisABSMove[BTStep].SetCheckButton(false);
			m_btAxisABSMove[BTStep].SetFont(&tFont);
		}

		m_btIOView.SetRoundButtonStyle(&m_tButtonStyle);
		m_btIOView.SetTextColor(&tColor);
		m_btIOView.SetCheckButton(false);
		m_btIOView.SetFont(&tFont);

		m_btExpendUI.SetRoundButtonStyle(&m_tButtonStyle);
		m_btExpendUI.SetTextColor(&tColor);
		m_btExpendUI.SetCheckButton(false);
		m_btExpendUI.SetFont(&tFont);

		m_btExitSW.SetRoundButtonStyle(&m_tButtonStyle);
		m_btExitSW.SetTextColor(&tColor);
		m_btExitSW.SetCheckButton(false);
		m_btExitSW.SetFont(&tFont);

		m_btMotSysInit.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMotSysInit.SetTextColor(&tColor);
		m_btMotSysInit.SetCheckButton(false);
		m_btMotSysInit.SetFont(&tFont);
		
	}	
	//m_fnChangeTaskState(TASK_STATE_NONE);
	
	for(int UpStep = AXIS_TT01_TableDrive; UpStep<AXIS_Cnt; UpStep++)
	{
		SetDlgItemInt(m_editMotPostion[UpStep].GetDlgCtrlID(), 0);
	}

	SIZE Screen;
	memset(&Screen, 0x00, sizeof(SIZE));
	Screen.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN);
	Screen.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN);
	this->SetWindowPos(nullptr, Screen.cx-200, 0, 200, 1080, SWP_SHOWWINDOW);
}

void CVS24MotionDlg::DrawImageView()
{
	if(m_pSampleView != nullptr)
	{
		CvvImage	ViewImage;
		ViewImage.Create(m_pSampleView->width, m_pSampleView->height, ((IPL_DEPTH_8U & 255)*3) );
		ViewImage.CopyOf(m_pSampleView);
		ViewImage.DrawToHDC(m_HDCView, m_RectiewArea);
		ViewImage.Destroy();
	}
}
// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVS24MotionDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CVS24MotionDlg::m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...)
{
	char	cLogBuffer[MAX_LOG_BUFF];
	va_list vargs;

	va_start(vargs, pErrLog);

	vsprintf_s(cLogBuffer, pErrLog, (va_list)vargs);

	va_end(vargs);

	m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nTaskNum ,uLogLevel, cLogBuffer);

	//strcpy_s(&cLogBuffer[strlen(cLogBuffer)], sizeof("\r\n"),"\r\n");

	//printf(cLogBuffer);
}


bool CVS24MotionDlg::Sys_fnInitVS64Interface()
{
	if(1 < __argc)
	{
		int nInputTaskNum = atoi(__argv[1]);
		char *pLinkPath = NULL;

		char path_buffer[_MAX_PATH];
		char chFilePath[_MAX_PATH] = {0,};
		char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		//실행 파일 이름을 포함한 Full path 가 얻어진다.
		::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
		//디렉토리만 구해낸다.
		_splitpath_s(path_buffer, drive, dir, fname, ext);
		strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
		strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

		switch(nInputTaskNum)
		{
		case TASK_10_Master		: pLinkPath = MSG_TASK_INI_FullPath_TN10; break;
		case TASK_11_Indexer		: pLinkPath = MSG_TASK_INI_FullPath_TN11; break;
		case TASK_21_Inspector	: pLinkPath = MSG_TASK_INI_FullPath_TN21; break;
		case TASK_24_Motion		: pLinkPath = MSG_TASK_INI_FullPath_TN24; break;
		case TASK_31_Mesure3D	: pLinkPath = MSG_TASK_INI_FullPath_TN31; break;
		case TASK_32_Mesure2D	: pLinkPath = MSG_TASK_INI_FullPath_TN32; break;
		case TASK_33_SerialPort	: pLinkPath = MSG_TASK_INI_FullPath_TN33; break;
		case TASK_60_Log			: pLinkPath = MSG_TASK_INI_FullPath_TN60; break;
		case TASK_90_TaskLoader: pLinkPath = MSG_TASK_INI_FullPath_TN90; break;

		case TASK_99_Sample: pLinkPath = MSG_TASK_INI_FullPath_TN99; break;
		default:
			{
				CString ErrorMessage;
				ErrorMessage.Format("Unkown TaskNum(%d)", nInputTaskNum);
				AfxMessageBox(ErrorMessage);
				SendMessage(WM_CLOSE, 0,  0);	
			}return false;
		}

		strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

		m_fnReadDatFile(chFilePath);
		m_ProcInitVal.m_nTaskNum = nInputTaskNum;
		//m_ProcInitVal.m_nLogFileID = nInputTaskNum;

	}
	else
	{
		AfxMessageBox("Input Run Task Number");
		SendMessage(WM_CLOSE, 0,  0);
		return false;
	}
	m_ServerInterface.m_fnSetLogTaskInfo(m_ProcInitVal.m_nLogTaskNo, m_ProcInitVal.m_nLogBlockLovel);	
	if(m_ServerInterface.m_fnRegisterTask(m_ProcInitVal.m_nTaskNum, m_ProcInitVal.m_sServerIP, m_ProcInitVal.m_nPortNo) != 0)
	{
		AfxMessageBox("Client Regist Fail");
		SendMessage(WM_CLOSE, 0,  0);
		return false;
	}
	else
	{
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, "Client Regist OK : %d", m_ProcInitVal.m_nTaskNum);
	}

	m_ServerInterface.m_fnSetAppWindowHandle(m_hWnd);
	m_ServerInterface.m_fnSetAppUserMessage(WM_VS64USER_CALLBACK);

	return true;
}

/*
*	Module Name		:	AOI_fnAnalyzeMsg
*	Parameter		:	Massage Parameter Pointer
*	Return			:	없음
*	Function		:	Sever로 부터 저달 되는 Command를 내부 Function과 Link 한다.(처리)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
int CVS24MotionDlg::Sys_fnAnalyzeMsg(CMDMSG* RcvCmdMsg)
{
	try
	{
		//AOI_AppendList("RcvComMsg:T(%d), F(%d), S(%d)",RcvCmdMsg->uTask_Dest, RcvCmdMsg->uFunID_Dest, RcvCmdMsg->uSeqID_Dest) ;
		if(RcvCmdMsg->uTask_Dest == m_ProcInitVal.m_nTaskNum) 
		{	
#ifndef SIMUL_CMD_TEST
			//Task 초기화 전에는 명령어중 System명령만 처리 한다.
			if(G_SystemInitOK == false )//&& RcvCmdMsg->uFunID_Dest != nBiz_Func_TLSystem)
			{
				int nMessSize = sizeof(VS24MotData);
				UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
				memset((void*)chMsgBuf, 0x00, nMessSize+1);	
				memcpy((char*)chMsgBuf,(char*)&RcvCmdMsg->cMsgBuf, nMessSize );
				m_ServerInterface.m_fnSendCommand_Nrs(
					RcvCmdMsg->uTask_Src, RcvCmdMsg->uFunID_Dest, RcvCmdMsg->uSeqID_Dest, RcvCmdMsg->uUnitID_Dest,
					nMessSize,	chMsgBuf);
				delete []chMsgBuf;	

				return OKAY;
			}
#endif
			if(nBiz_Func_Status != RcvCmdMsg->uFunID_Dest && nBiz_Func_System != RcvCmdMsg->uFunID_Dest)
			{
				G_WriteInfo(Log_Info, "From(%d): F(%d), S(%d), U(%d)-Data(%d)",
					RcvCmdMsg->uTask_Src, RcvCmdMsg->uFunID_Dest, RcvCmdMsg->uSeqID_Dest, RcvCmdMsg->uUnitID_Dest,RcvCmdMsg->uMsgSize);
			}
			switch(RcvCmdMsg->uFunID_Dest)
			{
				//////////////////////////////////////////////////////////////////////////
				//Project Default Command.
			case nBiz_Func_System:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Seq_Alive_Question:
						{
							m_ServerInterface.m_fnSendCommand_Nrs(RcvCmdMsg->uTask_Src, nBiz_Func_System, nBiz_Seq_Alive_Response, m_VS24TaskState);
						}break;
					case nBiz_Seq_Alive_Response:
						{
							switch (RcvCmdMsg->uTask_Src)
							{
							case TASK_10_Master:break;
							case TASK_11_Indexer:break;
							//case TASK_12_Indexer:break;
							case TASK_21_Inspector:break;
							//case TASK_24_Motion:break;
							case TASK_31_Mesure3D:break;
							case TASK_32_Mesure2D:break;
							case TASK_33_SerialPort:break;
							default:					
								throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
								break;					
							}//End Switch	
							break;
						}
					case nBiz_Seq_Mot_All_Stop:
						{
							G_MotCtrlObj.m_fnMotAllStop();
							break;
						}
					default:
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
				//////////////////////////////////////////////////////////////////////////.
				//
			case nBiz_Func_MotionAct:
			case nBiz_Func_CylinderAct:
			case nBiz_Func_ACC:
			case nBiz_Func_Vac:
			case nBiz_Func_Status:
				{
					FnProcThData newData;
					newData.m_RunThread		= false;//Q에서 생성
					newData.m_pHandleThread = nullptr;//Q에서 생성

					newData.m_pCMDSender	= &m_ServerInterface;//Internall 명령이면 null이 들어 간다.
					newData.pCallObj				= (void*)this;

					newData.uTask_Src			= RcvCmdMsg->uTask_Src;	
					//newData.uFunID_Src		= RcvCmdMsg->uFunID_Src;	
					//newData.uSeqID_Src		= RcvCmdMsg->uSeqID_Src;
					//newData.uUnitID_Src		= RcvCmdMsg->uUnitID_Src;

					newData.uTask_Dest		= RcvCmdMsg->uTask_Dest;	
					newData.uFunID_Dest		= RcvCmdMsg->uFunID_Dest;	
					newData.uSeqID_Dest		= RcvCmdMsg->uSeqID_Dest;
					newData.uUnitID_Dest		= RcvCmdMsg->uUnitID_Dest;

					if(RcvCmdMsg->uMsgSize == sizeof(VS24MotData))
					{
						memcpy(&newData.nMotionData, RcvCmdMsg->cMsgBuf, sizeof(VS24MotData));
					}
					//Send IO 요청이면. 따로 처리.(Thread 구동은 막자.)
					if(RcvCmdMsg->uFunID_Dest == nBiz_Func_Status && RcvCmdMsg->uSeqID_Dest == nBiz_Seq_ALL)
					{
						//G_WriteInfo(Log_Info, "nBiz_Func_Status Recv");
						DWORD SendIOBuf[9];
						int SendMotPos[AXIS_Cnt];
						G_MotCtrlObj.GetInputData(&SendIOBuf[0]);
						memcpy(&SendMotPos[0], &G_MotCtrlObj.m_AxisNowPos[0], sizeof(int)*AXIS_Cnt);

						int nMessSize = sizeof(VS24MotData)+(sizeof(DWORD)*9)+ (sizeof(int)*AXIS_Cnt);
						UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
						memset((void*)chMsgBuf, 0x00, nMessSize+1);	
						newData.nMotionData.nResult = VS24MotData::Ret_Ok;
						memcpy((char*)chMsgBuf,(char*)&newData.nMotionData, sizeof(VS24MotData) );
						memcpy((chMsgBuf+sizeof(VS24MotData)),&SendIOBuf[0], (sizeof(DWORD)*9));
						memcpy((chMsgBuf+sizeof(VS24MotData)+(sizeof(DWORD)*9)),&SendMotPos[0], (sizeof(int)*AXIS_Cnt));

						m_ServerInterface.m_fnSendCommand_Nrs(
							newData.uTask_Src, newData.uFunID_Dest, newData.uSeqID_Dest, newData.uUnitID_Dest,
							nMessSize,	chMsgBuf);

						delete []chMsgBuf;	

						//G_WriteInfo(Log_Info, "nBiz_Func_Status Send");
					}
					else if(m_MsgProcThreadQ.QPutNode(&newData ) == false)
					{//해당 명령 수행중이다.(Home이나 Move에서 Stop이 들어 오면 명령을 처리 한다.)
						int nMessSize = sizeof(VS24MotData);
						UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
						memset((void*)chMsgBuf, 0x00, nMessSize+1);	
						memcpy((char*)chMsgBuf,(char*)&RcvCmdMsg->cMsgBuf, nMessSize );

						VS24MotData*pSend = (VS24MotData*)chMsgBuf;
						pSend->nResult = VS24MotData::Ret_NowRun;
						m_ServerInterface.m_fnSendCommand_Nrs(
							RcvCmdMsg->uTask_Src, RcvCmdMsg->uFunID_Dest, RcvCmdMsg->uSeqID_Dest, RcvCmdMsg->uUnitID_Dest,
							nMessSize,	chMsgBuf);
						delete []chMsgBuf;	

						//G_WriteInfo(Log_Check, "To(%d): F%d-S%d-U%d-D%d => Already Run CMD",
						//	RcvCmdMsg->uTask_Src, RcvCmdMsg->uFunID_Dest, 
						//	RcvCmdMsg->uSeqID_Dest, RcvCmdMsg->uUnitID_Dest,
						//	RcvCmdMsg->uMsgSize);

					}
				}break;
			default:
				{
					throw CVs64Exception(_T("CVS24MotionDlg::AOI_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
				}break;
			}
			
		}
		else
		{
			throw CVs64Exception(_T("CVS24MotionDlg::AOI_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
		}
	}
	catch (...)
	{
		throw; 
	}
	return OKAY;
}

/*
*	Module Name		:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function		:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS24MotionDlg::Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	CMDMSG* CmdMsg = (CMDMSG*)lParam;		//SmartPointer 버전으로 개조가 필요하다... Work Item..
	try
	{
		if(CmdMsg != NULL)
		{
			nRet = Sys_fnAnalyzeMsg(CmdMsg);

			if(OKAY != nRet)
			{
				//Client 메시지 처리에대한 에러처리...
			}

			m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
			m_ServerInterface.m_fnFreeMemory(CmdMsg);
			return OKAY;
		}
		else
		{
			throw CVs64Exception(_T("CVS19MWDlg::m_fnVS64MsgProc, Message did not become well."));
		}
	}
	catch (CVs64Exception& e)
	{
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS,  e.m_fnGetErrorMsg());

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, exception -> %s"), e.what());
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, CException -> %s"), wszErr);
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	return OKAY;
}


void CVS24MotionDlg::OnBnClickedBtHwIoView()
{
	if(m_pViewIOMap != nullptr)
	{
		m_pViewIOMap->CenterWindow(this);
		m_pViewIOMap->ShowWindow(SW_SHOW);
	}
}

void CVS24MotionDlg::OnBnClickedBtMotTaskExit()
{
	CPassWord InputPassWord;
	if(InputPassWord.DoModal() == IDOK)
	{
		if(AfxMessageBox("System Exit ?",MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDYES)
		{
			CDialogEx::OnOK();
		}
	}
}
//int CntChange = 0;
void CVS24MotionDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(TIMER_TOWER_SIGNAL == nIDEvent)
	{
		G_MotCtrlObj.m_fnSignalTModeCheck();
		
		CString strReviewShift, strReviewDrive;
		strReviewShift = G_MotCtrlObj.m_fnUmacOnlineCommand("m268");
		strReviewDrive = G_MotCtrlObj.m_fnUmacOnlineCommand("m568");
		strReviewShift.Format("%0.1f", abs((float)atof(strReviewShift)*0.009));
		strReviewDrive.Format("%0.1f", abs((float)atof(strReviewDrive)*0.009));
		WritePrivateProfileString(Section_SVID, Key_MOT_AXIS_INSPECT_X_OVERLOAD, strReviewShift, SVID_LIST_PATH);
		WritePrivateProfileString(Section_SVID, Key_MOT_AXIS_INSPECT_Y_OVERLOAD, strReviewDrive, SVID_LIST_PATH);

		CString strScopeShift1,strScopeShift2, strScopeDrive;
		strScopeShift1 = G_MotCtrlObj.m_fnUmacOnlineCommand("m368");
		strScopeShift2 = G_MotCtrlObj.m_fnUmacOnlineCommand("m468");
		strScopeDrive = G_MotCtrlObj.m_fnUmacOnlineCommand("m568");
		strScopeShift1.Format("%0.1f", abs((float)atof(strScopeShift1)*0.009));
		strScopeShift2.Format("%0.1f", abs((float)atof(strScopeShift2)*0.009));
		strScopeDrive.Format("%0.1f", abs((float)atof(strScopeDrive)*0.009));
		WritePrivateProfileString(Section_SVID, Key_MOT_AXIS_MEASURE_X1_OVERLOAD, strScopeShift1, SVID_LIST_PATH);
		WritePrivateProfileString(Section_SVID, Key_MOT_AXIS_MEASURE_X2_OVERLOAD, strScopeShift2, SVID_LIST_PATH);
		WritePrivateProfileString(Section_SVID, Key_MOT_AXIS_MEASURE_Y_OVERLOAD, strReviewDrive, SVID_LIST_PATH);

		if (G_MotInfo.m_pMotState->stNormalStatus.In22_PCRackTempAlarm1 == 1)
			WritePrivateProfileString(Section_SVID, Key_TEMPERATURE_STATE_PC_BOX, "1", SVID_LIST_PATH);//온도 상태 (0:정상, 1:Warning, 2:Alarm)
		else if (G_MotInfo.m_pMotState->stNormalStatus.In23_PCRackTempAlarm2 == 1)
			WritePrivateProfileString(Section_SVID, Key_TEMPERATURE_STATE_PC_BOX, "2", SVID_LIST_PATH);//온도 상태 (0:정상, 1:Warning, 2:Alarm)
		else
			WritePrivateProfileString(Section_SVID, Key_TEMPERATURE_STATE_PC_BOX, "0", SVID_LIST_PATH);//온도 상태 (0:정상, 1:Warning, 2:Alarm)

		if (G_MotInfo.m_pMotState->stNormalStatus.In20_ElectronicBoxTempAlarm1 == 1)
			WritePrivateProfileString(Section_SVID, Key_TEMPERATURE_STATE_ELECTRICAL_BOX, "1", SVID_LIST_PATH);//온도 상태 (0:정상, 1:Warning, 2:Alarm)
		else if (G_MotInfo.m_pMotState->stNormalStatus.In20_ElectronicBoxTempAlarm1 == 1)
			WritePrivateProfileString(Section_SVID, Key_TEMPERATURE_STATE_ELECTRICAL_BOX, "2", SVID_LIST_PATH);//온도 상태 (0:정상, 1:Warning, 2:Alarm)
		else
			WritePrivateProfileString(Section_SVID, Key_TEMPERATURE_STATE_ELECTRICAL_BOX, "0", SVID_LIST_PATH);//온도 상태 (0:정상, 1:Warning, 2:Alarm)


		/*if(CntChange>3)
		{

		CntChange = 0;
		switch(m_VS24TaskState)
		{
		case TASK_STATE_NONE:
		m_VS24TaskState = TASK_STATE_INIT;
		break;
		case TASK_STATE_INIT:
		m_VS24TaskState = TASK_STATE_IDLE;
		break;
		case TASK_STATE_IDLE:
		m_VS24TaskState = TASK_STATE_RUN;
		break;
		case TASK_STATE_RUN:
		m_VS24TaskState = TASK_STATE_ERROR;
		break;
		case TASK_STATE_ERROR:
		m_VS24TaskState = TASK_STATE_PM;
		break;
		case TASK_STATE_PM:
		m_VS24TaskState = TASK_STATE_NONE;
		break;
		}
		G_TaskStateChange(this, &m_stTaskState, m_VS24TaskState);
		}
		CntChange++;*/
	}
	if(TIMER_MOT_POS_UPDATE == nIDEvent)// && IsWindowVisible() == TRUE)
	{	
		CString strUpdateData;
		CString strNowData;
		for(int UpStep = AXIS_TT01_TableDrive; UpStep<AXIS_Cnt; UpStep++)
		{

			if(G_MotCtrlObj.m_AxisNowPos[UpStep] != GetDlgItemInt(m_editMotPostion[UpStep].GetDlgCtrlID()))
			{
				strUpdateData.Format("%d", G_MotCtrlObj.m_AxisNowPos[UpStep]);
				m_editMotPostion[UpStep].SetWindowText(strUpdateData);
			}
		}

		//Action Process Cnt Display
		strUpdateData.Format("%d/%d", m_MsgProcThreadQ.QGetCnt(), G_UI_HideCounter);
		SetDlgItemText( IDC_ST_TIME_CNT, strUpdateData);
		
		//Master 공유 메모리에 Update하자.
		if(G_MotInfo.m_pMotState != nullptr && G_MotCtrlObj.m_bDriverOpen == FALSE)//Device Open이 안된경우.. 의미 없음.
		{
			DWORD SendIOBuf[9];
			G_MotCtrlObj.GetInputData(&SendIOBuf[0]);
			G_MotInfo.m_pMotState->stNormalStatus.shAL_IN			= SendIOBuf[0]; //Door, Temperature, 장비 일반 사항.
			G_MotInfo.m_pMotState->stBoxTableSensor.shAL_IN			= SendIOBuf[1]; //Box Table, Box Detect Sensor
			G_MotInfo.m_pMotState->stModuleStatus.shAL_IN			= SendIOBuf[2]; //TM01, LT01, BO01, UT01
			G_MotInfo.m_pMotState->stGripperStatus.shAL_IN			= SendIOBuf[3]; //Gripper
			G_MotInfo.m_pMotState->stPickerSensor.shAL_IN			= SendIOBuf[4]; //TM02
			G_MotInfo.m_pMotState->stPickerCylinder.shAL_IN			= SendIOBuf[5]; //TM02 Cylinder
			G_MotInfo.m_pMotState->stTurnTableSensor.shAL_IN			= SendIOBuf[6]; //TT01 
			G_MotInfo.m_pMotState->stReserve.shAL_IN					= SendIOBuf[7];
			G_MotInfo.m_pMotState->stEmissionTable.shAL_IN			= SendIOBuf[8] ; //UT02 Status
			
			//G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock = rand()%2>0?1:0;

			//G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose = rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In24_SystemOn =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode =rand()%2>0?1:0;


			//G_MotInfo.m_pMotState->stNormalStatus.In00_EMO1 =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In01_EMO2 =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In02_EMO3 =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stNormalStatus.In28_EMO4 =rand()%2>0?1:0;

			//G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection  =rand()%2>0?1:0;
			//G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection  =rand()%2>0?1:0;

			G_MotCtrlObj.GetOutputData(&SendIOBuf[0]);
			G_MotInfo.m_pMotState->m_IO_OutA.shAL_Out	= SendIOBuf[0]; //Door, Temperature, 장비 일반 사항.
			G_MotInfo.m_pMotState->m_IO_OutB.shAL_Out	= SendIOBuf[1]; //Box Table, Box Detect Sensor
			G_MotInfo.m_pMotState->m_IO_OutC.shAL_Out	= SendIOBuf[2]; //TM01, LT01, BO01, UT01
			G_MotInfo.m_pMotState->m_IO_OutD.shAL_Out	= SendIOBuf[3]; //Gripper
			G_MotInfo.m_pMotState->m_IO_OutE.shAL_Out	= SendIOBuf[4]; //TM02
			G_MotInfo.m_pMotState->m_IO_OutF.shAL_Out	= SendIOBuf[5]; //TM02 Cylinder
			G_MotInfo.m_pMotState->m_IO_OutG.shAL_Out	= SendIOBuf[6]; //TT01 
			G_MotInfo.m_pMotState->m_IO_OutH.shAL_Out	= SendIOBuf[7];
			G_MotInfo.m_pMotState->m_IO_OutI.shAL_Out		= SendIOBuf[8] ; //UT02 Status

			/*G_MotInfo.m_pMotState->m_IO_OutA.Out03_TeachKeyUnlock =rand()%2>0?1:0;*/
			memcpy(&G_MotInfo.m_pMotState->nMotionPos[0], &G_MotCtrlObj.m_AxisNowPos[0], sizeof(int)*AXIS_Cnt);
		}

		

#ifdef SIMUL_UI_TEST
		for(int StStep =0; StStep<32; StStep++)
			G_MotCtrlObj.m_PLCProActive[StStep] = (rand()%2==0?true:false);
#endif
		if(m_ExpendUI == true)
		{
			for(int StStep =0; StStep<32; StStep++)
			{
				if(G_MotCtrlObj.m_PLCProActive[StStep]==true)
					m_stPLCState[StStep].SetBkColor(G_Color_On);
				else
					m_stPLCState[StStep].SetBkColor(G_Color_Off);
			}
		}
	}
	if(TIMER_MOT_INIT_STATE == nIDEvent)// && m_ExpendUI == true)// && IsWindowVisible() == TRUE)
	{	
		//G_MotCtrlObj.m_LocalAxisST[0].m_AxisStA.St18_OpenLoopMode = 0;
		//G_MotCtrlObj.m_LocalAxisST[0].m_AxisStB.St10_HomeComplete =1;

		//G_MotCtrlObj.m_ALinkAxisST[0].St05_SVON = 1;
		//G_MotCtrlObj.m_ALinkAxisST[0].St02_SERR =0;
		//G_MotCtrlObj.m_ALinkAxisST[0].St03_SALM =0;
		CheckMotState_All_LinkButton();

	}
	if(TIMER_UI_HIDE_CHECK == nIDEvent)
	{//일정시간동안 확장되어 있다면 축소 시킨다.
		if(G_UI_HideCounter>0)
			G_UI_HideCounter--;

		if(G_UI_HideCounter == 0)
		{
			if(m_ExpendUI == true)//확장 상태이면 줄인다.
				OnBnClickedBtExpend();

			G_UI_HideCounter =-1;
		}
	}
	
	__super::OnTimer(nIDEvent);
}

void CVS24MotionDlg::CheckMotState_All_LinkButton()
{

	for(int MStep = AXIS_TT01_TableDrive; MStep< AXIS_Cnt; MStep++)
	{
		switch(MStep)
		{
		case AXIS_TT01_TableDrive://Cruiser Axis #1
			{
				if(G_MotCtrlObj.m_LocalAxisST[0].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_LocalAxisST[0].m_AxisStB.St10_HomeComplete ==1 )
					m_btAxisSelect[AXIS_TT01_TableDrive].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_TT01_TableDrive].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_ScanShift://Cruiser Axis #2
			{
				if(G_MotCtrlObj.m_LocalAxisST[1].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_LocalAxisST[1].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_AM01_ScanShift].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_ScanShift].m_SettingFlag =  -1;
			}break;

		case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
			{
				//m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[8];//#30
				//m_pCuData_StateCuL = &G_MotCtrlObj.m_LocalAxisST[2];// #3
				//m_pCuData_StateCuR = &G_MotCtrlObj.m_LocalAxisST[3];// #4

				if(G_MotCtrlObj.m_LocalAxisST[8].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_LocalAxisST[8].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_AM01_ScopeShift].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_ScopeShift].m_SettingFlag =  -1;
			}break;
		case AXIS_AM01_ScopeDrive://Cruiser Axis #5
			{
				//m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[4];
				if(G_MotCtrlObj.m_LocalAxisST[4].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_LocalAxisST[4].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_AM01_ScopeDrive].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_ScopeDrive].m_SettingFlag =  -1;
			}break;
		case AXIS_AM01_ScanDrive://Cruiser Axis #6
			{
				//m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[5];
				if(G_MotCtrlObj.m_LocalAxisST[5].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_LocalAxisST[5].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_AM01_ScanDrive].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_ScanDrive].m_SettingFlag =  -1;
			}break;
		case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
			{
				//m_pCuData_StateCu = &G_MotCtrlObj.m_LocalAxisST[9];//#31
				//m_pCuData_StateCuL = &G_MotCtrlObj.m_LocalAxisST[6];//#7
				//m_pCuData_StateCuR = &G_MotCtrlObj.m_LocalAxisST[7];//#8

				if((G_MotCtrlObj.m_LocalAxisST[9].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_LocalAxisST[9].m_AxisStB.St10_HomeComplete ==1) &&
					(G_MotCtrlObj.m_LocalAxisST[6].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_LocalAxisST[6].m_AxisStB.St10_HomeComplete ==1) &&
					(G_MotCtrlObj.m_LocalAxisST[7].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_LocalAxisST[7].m_AxisStB.St10_HomeComplete ==1) )
					m_btAxisSelect[AXIS_TM02_PickerDrive].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_TM02_PickerDrive].m_SettingFlag =  -1;
			}break;	
			//LT01
		case AXIS_LT01_BoxLoadPush://ML3 #14
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[4];

				if(G_MotCtrlObj.m_ML3AxisST[4].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[4].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_LT01_BoxLoadPush].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_LT01_BoxLoadPush].m_SettingFlag =  -1;
			}break;
			//////////////////////////////////////////////////////////////////////////
		case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
			{
				//m_pCuData_StateML3L = &G_MotCtrlObj.m_ML3AxisST[0];
				//m_pCuData_StateML3R = &G_MotCtrlObj.m_ML3AxisST[1];
				if((G_MotCtrlObj.m_ML3AxisST[0].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[0].m_AxisStB.St10_HomeComplete ==1) ||
					(G_MotCtrlObj.m_ML3AxisST[1].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[1].m_AxisStB.St10_HomeComplete ==1))
					m_btAxisSelect[AXIS_BO01_BoxRotate].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_BO01_BoxRotate].m_SettingFlag =  -1;
			}break;
		case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
			{
				//m_pCuData_StateML3L = &G_MotCtrlObj.m_ML3AxisST[2];
				//m_pCuData_StateML3R = &G_MotCtrlObj.m_ML3AxisST[3];
				if((G_MotCtrlObj.m_ML3AxisST[2].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[2].m_AxisStB.St10_HomeComplete ==1) ||
					(G_MotCtrlObj.m_ML3AxisST[3].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[3].m_AxisStB.St10_HomeComplete ==1))
					m_btAxisSelect[AXIS_BO01_BoxUpDn].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_BO01_BoxUpDn].m_SettingFlag =  -1;
			}break;
		case AXIS_BO01_BoxDrive://ML3 #16
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[6];
				if(G_MotCtrlObj.m_ML3AxisST[6].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[6].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_BO01_BoxDrive].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_BO01_BoxDrive].m_SettingFlag =  -1;
			}break;	
			//UT01
		case AXIS_UT01_BoxUnLoadPush://ML3 #15
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[5];
				if(G_MotCtrlObj.m_ML3AxisST[5].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[5].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_UT01_BoxUnLoadPush].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_UT01_BoxUnLoadPush].m_SettingFlag =  -1;
			}break;
			//TM01
		case AXIS_TM01_ForwardBackward://ML3 #17
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[7];
				if(G_MotCtrlObj.m_ML3AxisST[7].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[7].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_TM01_ForwardBackward].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_TM01_ForwardBackward].m_SettingFlag =  -1;
			}break;
		case AXIS_TT01_ThetaAlign://ML3 #18
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[8];
				if(G_MotCtrlObj.m_ML3AxisST[8].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[8].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_TT01_ThetaAlign].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_TT01_ThetaAlign].m_SettingFlag =  -1;
			}break;
		case AXIS_TT01_TableTurn://ML3 #19
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[9];
				if(G_MotCtrlObj.m_ML3AxisST[9].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[9].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_TT01_TableTurn].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_TT01_TableTurn].m_SettingFlag =  -1;
			}break;
		case AXIS_TM02_PickerUpDown://ML3 #20
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[10];
				if(G_MotCtrlObj.m_ML3AxisST[10].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[10].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_TM02_PickerUpDown].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_TM02_PickerUpDown].m_SettingFlag =  -1;
			}break;
		case AXIS_TM02_PickerShift://ML3 #21
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[11];
				if(G_MotCtrlObj.m_ML3AxisST[11].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[11].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_TM02_PickerShift].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_TM02_PickerShift].m_SettingFlag =  -1;
			}break;
		case AXIS_BT01_CassetteUpDown://ML3 #22
			{
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[12];
				if(G_MotCtrlObj.m_ML3AxisST[12].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[12].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_BT01_CassetteUpDown].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_BT01_CassetteUpDown].m_SettingFlag =  -1;
			}break;
		case AXIS_AM01_JigInOut://ML3 #23
			{
#ifdef MOT_USE_UMAC_JIG_23AXIS
				//m_pCuData_StateML3 = &G_MotCtrlObj.m_ML3AxisST[13];
				if(G_MotCtrlObj.m_ML3AxisST[13].m_AxisStA.St18_OpenLoopMode == 0 && 
					G_MotCtrlObj.m_ML3AxisST[13].m_AxisStB.St10_HomeComplete ==1)
					m_btAxisSelect[AXIS_AM01_JigInOut].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_JigInOut].m_SettingFlag =  -1;
#endif
			}break;
			//////////////////////////////////////////////////////////////////////////
			//case XXXXXXXXX://AxisLink_A X
			//	{
			//		m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[0]
			//      strMotName.AppendFormat("%s", "");
			//	}break;
		case AXIS_TM02_PickerRotate://AxisLink_A Y
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[0];
				if(G_MotCtrlObj.m_ALinkAxisST[1].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[1].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[1].St03_SALM ==0 )
					m_btAxisSelect[AXIS_TM02_PickerRotate].m_SettingFlag =  1;
				else
					m_btAxisSelect[AXIS_TM02_PickerRotate].m_SettingFlag =  -1;
			}break;
			
		case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[2];
				if(G_MotCtrlObj.m_ALinkAxisST[2].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[2].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[2].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_TensionLeftUpDn].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_TensionLeftUpDn].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_TensionRightUpDn://AxisLink_A U
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[3];
				if(G_MotCtrlObj.m_ALinkAxisST[3].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[3].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[3].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_TensionRightUpDn].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_TensionRightUpDn].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_LTension1://AxisLink_B X
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[4];
				if(G_MotCtrlObj.m_ALinkAxisST[4].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[4].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[4].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_LTension1].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_LTension1].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_LTension2://AxisLink_B Y
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[5];
				if(G_MotCtrlObj.m_ALinkAxisST[5].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[5].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[5].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_LTension2].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_LTension2].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_LTension3://AxisLink_B Z
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[6];
				if(G_MotCtrlObj.m_ALinkAxisST[6].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[6].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[6].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_LTension3].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_LTension3].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_LTension4://AxisLink_B U
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[7];
				if(G_MotCtrlObj.m_ALinkAxisST[7].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[7].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[7].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_LTension4].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_LTension4].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_RTension1://AxisLink_C X
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[8];
				if(G_MotCtrlObj.m_ALinkAxisST[8].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[8].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[8].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_RTension1].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_RTension1].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_RTension2://AxisLink_C Y
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[9];
				if(G_MotCtrlObj.m_ALinkAxisST[9].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[9].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[9].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_RTension2].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_RTension2].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_RTension3://AxisLink_C Z
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[10];
				if(G_MotCtrlObj.m_ALinkAxisST[10].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[10].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[10].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_RTension3].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_RTension3].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_RTension4://AxisLink_C U
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[11];
				if(G_MotCtrlObj.m_ALinkAxisST[11].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[11].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[11].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_RTension4].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_RTension4].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_ScanUpDn://AxisLink_D X
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[12];
				if(G_MotCtrlObj.m_ALinkAxisST[12].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[12].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[12].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_ScanUpDn].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_ScanUpDn].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[13];
				if(G_MotCtrlObj.m_ALinkAxisST[13].St05_SVON == 1 && 
					//G_MotCtrlObj.m_ALinkAxisST[13].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[13].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_SpaceSnUpDn].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_SpaceSnUpDn].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_ScopeUpDn://AxisLink_D Z
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[14];
				if(G_MotCtrlObj.m_ALinkAxisST[14].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[14].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[14].St03_SALM ==0 )
					m_btAxisSelect[AXIS_AM01_ScopeUpDn].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_ScopeUpDn].m_SettingFlag = -1;
			}break;
		case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
			{
				//m_pALink_State = &G_MotCtrlObj.m_ALinkAxisST[15];
				if(G_MotCtrlObj.m_ALinkAxisST[15].St05_SVON == 1 && 
					G_MotCtrlObj.m_ALinkAxisST[15].St02_SERR ==0  && 
					G_MotCtrlObj.m_ALinkAxisST[15].St03_SALM ==0 )

					m_btAxisSelect[AXIS_AM01_AirBlowLeftRight].m_SettingFlag = 1;
				else
					m_btAxisSelect[AXIS_AM01_AirBlowLeftRight].m_SettingFlag = -1;
			}break;
		default:
			break;
		}
	}
	for(int MStep = AXIS_TT01_TableDrive; MStep< AXIS_Cnt; MStep++)
	{
		if(m_btAxisSelect[MStep].m_SettingFlag_Old != m_btAxisSelect[MStep].m_SettingFlag )
		{
			if(m_btAxisSelect[MStep].m_SettingFlag == -1)
			{
				m_btAxisSelect[MStep].SetRoundButtonStyle(&m_tBTStyle_MotNotInit);
				m_btAxisSelect[MStep].Invalidate();

				m_editMotPostion[MStep].EnableWindow(FALSE);
			}
			if(m_btAxisSelect[MStep].m_SettingFlag == 1)
			{
				m_btAxisSelect[MStep].SetRoundButtonStyle(&m_tButtonStyle);
				m_btAxisSelect[MStep].Invalidate();
				m_editMotPostion[MStep].EnableWindow(TRUE);
			}
			m_btAxisSelect[MStep].m_SettingFlag_Old = m_btAxisSelect[MStep].m_SettingFlag;
		}
	}
}


void CVS24MotionDlg::OnBnClickedBtExpend()
{

	SIZE Screen;
	memset(&Screen, 0x00, sizeof(SIZE));
	Screen.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN);
	Screen.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN);
	if(m_ExpendUI == false)
	{
		//CPassWord InputPassWord;
		//if(InputPassWord.DoModal() == IDOK)
		{
			SetDlgItemText(IDC_BT_EXPEND, ">>>>>>>");
			m_ExpendUI = true;
			G_UI_HideCounter = CNT_UI_HIDE_TIME_SEC;

			for(int FStep = 1; FStep<46; FStep++)
			{
				this->SetWindowPos(nullptr, Screen.cx-(200+(FStep*20)), 0, 200+(FStep*20), 1080, SWP_SHOWWINDOW);
				Sleep(10);
			}
		}
	}
	else
	{
		SetDlgItemText(IDC_BT_EXPEND, "<<<<<<<");
		m_ExpendUI = false;
		m_AxisSelectedIndex = -1;//선택 축을 리셋하고 다시 선택 하게 한다.
		this->SetWindowPos(nullptr, Screen.cx-200, 0, 200, 1080, SWP_SHOWWINDOW);
	}
}


HBRUSH CVS24MotionDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	//PLC State ID Skip
	if((DLG_ID_Number>=IDC_ST_PLC_01 && DLG_ID_Number<= IDC_ST_PLC_32)||
		DLG_ID_Number==IDC_ST_TASK_STATE )
		return hbr;



	//if(DLG_ID_Number== IDC_ED_TT01_TableDrive          ||
	//	DLG_ID_Number== IDC_ED_TT01_ThetaAlign          ||
	//	DLG_ID_Number== IDC_ED_TT01_TableTurn           ||
	//	DLG_ID_Number== IDC_ED_BT01_CassetteUpDown  ||
	//	DLG_ID_Number== IDC_ED_BO01_BoxRotate          ||
	//	DLG_ID_Number== IDC_ED_BO01_BoxUpDn            ||
	//	DLG_ID_Number== IDC_ED_LT01_BoxLoadPush       ||
	//	DLG_ID_Number== IDC_ED_UT01_BoxUnLoadPush   ||
	//	DLG_ID_Number== IDC_ED_BO01_BoxDrive             ||
	//	DLG_ID_Number== IDC_ED_TM01_ForwardBackward||
	//	DLG_ID_Number== IDC_ED_TM02_PickerDrive         ||
	//	DLG_ID_Number== IDC_ED_TM02_PickerUpDown     ||
	//	DLG_ID_Number== IDC_ED_TM02_PickerShift         ||
	//	DLG_ID_Number== IDC_ED_TM02_PickerRotate       ||
	//	DLG_ID_Number== IDC_ED_AM01_ScanDrive          ||
	//	DLG_ID_Number== IDC_ED_AM01_ScanShift           ||
	//	DLG_ID_Number== IDC_ED_AM01_ScanUpDn          ||
	//	DLG_ID_Number== IDC_ED_AM01_SpaceSnUpDn||
	//	DLG_ID_Number== IDC_ED_AM01_ScopeDrive ||
	//	DLG_ID_Number== IDC_ED_AM01_ScopeShift||
	//	DLG_ID_Number== IDC_ED_AM01_ScopeUpDn||
	//	DLG_ID_Number== IDC_ED_AM01_AirBlowLeftRight ||
	//	DLG_ID_Number== IDC_ED_AM01_TensionLeftUpDn||
	//	DLG_ID_Number== IDC_ED_AM01_TensionRightUpDn||
	//	DLG_ID_Number== IDC_ED_AM01_LTension1||
	//	DLG_ID_Number== IDC_ED_AM01_LTension2||
	//	DLG_ID_Number== IDC_ED_AM01_LTension3||
	//	DLG_ID_Number== IDC_ED_AM01_LTension4||
	//	DLG_ID_Number== IDC_ED_AM01_RTension1||
	//	DLG_ID_Number== IDC_ED_AM01_RTension2||
	//	DLG_ID_Number== IDC_ED_AM01_RTension3||
	//	DLG_ID_Number== IDC_ED_AM01_RTension4)
	//{
	//	return hbr;
	//}

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CVS24MotionDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if((pMsg->message==WM_KEYDOWN || pMsg->message==WM_LBUTTONDOWN) && m_ExpendUI == true)
	{
		G_UI_HideCounter = CNT_UI_HIDE_TIME_SEC;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return __super::PreTranslateMessage(pMsg);

	//BOOL retvalue = __super::PreTranslateMessage(pMsg);
	//CRect ViewMapSize;
	//this->GetWindowRect(&ViewMapSize);
	//if(ViewMapSize.PtInRect(pMsg->pt) == TRUE && retvalue== TRUE )
	//{
	//	this->SetForegroundWindow();
	//}
	//return retvalue;
}
void CVS24MotionDlg::m_fnChangeTaskState_ReleasePM()
{
	m_VS24TaskState = TASK_STATE_NONE;
	G_WriteInfo(Log_Check, "PM Mode Release PM=>NONE(Init System.)");
	G_SystemInitOK = false;
}
TASK_State CVS24MotionDlg::m_fnChangeTaskState(TASK_State NewState)
{

	if(m_VS24TaskState == TASK_STATE_PM)
	{
		return TASK_STATE_PM;
	}

	TASK_State LocalState = m_VS24TaskState;
	switch(NewState)
	{
	case TASK_STATE_NONE:
		{
			switch(LocalState)
			{
			case TASK_STATE_NONE:
			//case TASK_STATE_INIT:
			case TASK_STATE_IDLE:
			//case TASK_STATE_RUN:
			case TASK_STATE_ERROR:
			case TASK_STATE_PM:
				{
					LocalState = NewState;
				}break;
			}
		}break;
	//////////////////////////////////////////////////////////////////////////
	case TASK_STATE_INIT:
		{
			switch(LocalState)
			{
			case TASK_STATE_NONE:
			case TASK_STATE_INIT:
			case TASK_STATE_IDLE:
			//case TASK_STATE_RUN:
			case TASK_STATE_ERROR:
				{
					LocalState = NewState;
				}break;
			case TASK_STATE_PM:
				{
					m_VS24TaskState = TASK_STATE_PM;
				}break;
			}
		}break;
	case TASK_STATE_IDLE:
		{
			switch(LocalState)
			{
			//case TASK_STATE_NONE:
			case TASK_STATE_INIT:
			case TASK_STATE_IDLE:
			case TASK_STATE_RUN:
			//case TASK_STATE_ERROR:
			//case TASK_STATE_PM:
				{
					LocalState = NewState;
				}break;
			}
		}break;
	case TASK_STATE_RUN:
		{
			switch(m_VS24TaskState)
			{
			//case TASK_STATE_NONE:
			//case TASK_STATE_INIT:
			case TASK_STATE_IDLE:
			case TASK_STATE_RUN:
			//case TASK_STATE_ERROR:
			//case TASK_STATE_PM:
				{
					LocalState = NewState;
				}break;
			}
		}break;
	case TASK_STATE_ERROR:
		{
			switch(LocalState)
			{
			case TASK_STATE_NONE:
			case TASK_STATE_INIT:
			case TASK_STATE_IDLE:
			case TASK_STATE_RUN:
			case TASK_STATE_ERROR:
			case TASK_STATE_PM:
				{
					LocalState = NewState;
				}break;
			}
		}break;
	case TASK_STATE_PM:
		{
			switch(LocalState)
			{
			case TASK_STATE_NONE:
			case TASK_STATE_INIT:
			case TASK_STATE_IDLE:
			case TASK_STATE_RUN:
			case TASK_STATE_ERROR:
			case TASK_STATE_PM:
				{
					LocalState = NewState;
				}break;
			}
		}break;
	}
	if(LocalState != m_VS24TaskState)
	{
		G_WriteInfo(Log_Normal,"Change Task State %s=>%s", G_strTASK_State[m_VS24TaskState], G_strTASK_State[NewState]);
		m_VS24TaskState = LocalState;
	}
	else
	{
		G_WriteInfo(Log_Normal,"Not Change Task State %s=>%s", G_strTASK_State[m_VS24TaskState], G_strTASK_State[NewState]);
	}

	G_TaskStateChange(this, &m_stTaskState, m_VS24TaskState);
	return m_VS24TaskState;
}

void CVS24MotionDlg::SendMotInternalCMDQ(int AxisIndex,  VS24MotData::MotAct nAction, int Position)
{
	FnProcThData newData;
	newData.m_RunThread		= false;//Q에서 생성
	newData.m_pHandleThread = nullptr;//Q에서 생성

	newData.m_pCMDSender	= nullptr;//Internall 명령이면 null이 들어 간다.
	newData.pCallObj				= (void*)this;


	USHORT AxisCMDLink = -1;
	switch(AxisIndex)
	{
		//TT01
	case AXIS_TT01_TableDrive:
		AxisCMDLink = nBiz_Seq_TT01_TableDrive;
		break;
	case AXIS_TT01_ThetaAlign:
		AxisCMDLink = nBiz_Seq_TT01_ThetaAlign;
		break;
	case AXIS_TT01_TableTurn:
		AxisCMDLink = nBiz_Seq_TT01_TableTurn;
		break;
		//LT01
	case AXIS_LT01_BoxLoadPush:
		AxisCMDLink = nBiz_Seq_LT01_BoxLoadPush;
		break;
		//BT01
	case AXIS_BT01_CassetteUpDown:
		AxisCMDLink = nBiz_Seq_BT01_CassetteUpDown;
		break;
		//BO01
	case AXIS_BO01_BoxRotate://좌우 동기축.
		AxisCMDLink = nBiz_Seq_BO01_BoxRotate;
		break;
	case AXIS_BO01_BoxUpDn://좌우 동기축.
		AxisCMDLink = nBiz_Seq_BO01_BoxUpDn;
		break;
	case AXIS_BO01_BoxDrive:
		AxisCMDLink = nBiz_Seq_BO01_BoxDrive;
		break;
		//UT01
	case AXIS_UT01_BoxUnLoadPush:
		AxisCMDLink = nBiz_Seq_UT01_BoxUnLoadPush;
		break;
		//TM01
	case AXIS_TM01_ForwardBackward:
		AxisCMDLink = nBiz_Seq_TM01_ForwardBackward;
		break;
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
		AxisCMDLink = nBiz_Seq_TM02_PickerDrive;
		break;
	case AXIS_TM02_PickerUpDown:
		AxisCMDLink = nBiz_Seq_TM02_PickerUpDown;
		break;
	case AXIS_TM02_PickerShift:
		AxisCMDLink = nBiz_Seq_TM02_PickerShift;
		break;
	case AXIS_TM02_PickerRotate:
		AxisCMDLink = nBiz_Seq_TM02_PickerRotate;
		break;
		//AM01
	case AXIS_AM01_ScanDrive:
		AxisCMDLink = nBiz_Seq_AM01_ScanDrive;
		break;
	case AXIS_AM01_ScanShift:
		AxisCMDLink = nBiz_Seq_AM01_ScanShift;
		break;
	case AXIS_AM01_ScanUpDn:
		AxisCMDLink = nBiz_Seq_AM01_ScanUpDn;
		break;
	case AXIS_AM01_SpaceSnUpDn:
		AxisCMDLink = nBiz_Seq_AM01_SpaceSnUpDn;
		break;
	case AXIS_AM01_ScopeDrive:
		AxisCMDLink = nBiz_Seq_AM01_ScopeDrive;
		break;
	case AXIS_AM01_ScopeShift://좌우 Gantry구동
		AxisCMDLink = nBiz_Seq_AM01_ScopeShift;
		break;
	case AXIS_AM01_ScopeUpDn:
		AxisCMDLink = nBiz_Seq_AM01_ScopeUpDn;
		break;
	case AXIS_AM01_AirBlowLeftRight:
		AxisCMDLink = nBiz_Seq_AM01_AirBlowLeftRight;
		break;

	case AXIS_AM01_TensionLeftUpDn:
	case AXIS_AM01_TensionRightUpDn:
		AxisCMDLink = nBiz_Seq_AM01_TensionUpDn;
		break;
	//case AXIS_AM01_TensionLeftUpDn:
	//	AxisCMDLink = nBiz_Seq_AM01_TensionLeftUpDn;
	//	break;
	//case AXIS_AM01_TensionRightUpDn:
	//	AxisCMDLink = nBiz_Seq_AM01_TensionRightUpDn;
	//	break;
	case AXIS_AM01_LTension1:
		AxisCMDLink = nBiz_Seq_AM01_LTension1;
		break;
	case AXIS_AM01_LTension2:
		AxisCMDLink = nBiz_Seq_AM01_LTension2;
		break;
	case AXIS_AM01_LTension3:
		AxisCMDLink = nBiz_Seq_AM01_LTension3;
		break;
	case AXIS_AM01_LTension4:
		AxisCMDLink = nBiz_Seq_AM01_LTension4;
		break;
	case AXIS_AM01_RTension1:
		AxisCMDLink = nBiz_Seq_AM01_RTension1;
		break;
	case AXIS_AM01_RTension2:
		AxisCMDLink = nBiz_Seq_AM01_RTension2;
		break;
	case AXIS_AM01_RTension3:
		AxisCMDLink = nBiz_Seq_AM01_RTension3;
		break;
	case AXIS_AM01_RTension4:
		AxisCMDLink = nBiz_Seq_AM01_RTension4;
		break;
	case AXIS_AM01_JigInOut:
		AxisCMDLink = nBiz_Seq_AM01_JigInout;
		break;
	default: 
		return;
	}

	newData.uFunID_Dest		= nBiz_Func_MotionAct;	
	newData.uSeqID_Dest		= AxisCMDLink;
	newData.uUnitID_Dest		= nBiz_Unit_Zero;

	newData.nMotionData.Reset();
	newData.nMotionData.nAction = nAction;
	newData.nMotionData.newPosition1 = Position;
	newData.nMotionData.ActionTime = 30;//30sec
	if(m_MsgProcThreadQ.QPutNode(&newData ) == false)
	{
		G_WriteInfo(Log_Check, "해당 Motion 동작 수행 중 입니다.");
	}
}

LRESULT CVS24MotionDlg::OnUserSendCylInternalCMDQ(WPARAM wParam, LPARAM lParam)
{
	FnProcThData newData;
	newData.m_RunThread		= false;//Q에서 생성
	newData.m_pHandleThread = nullptr;//Q에서 생성

	newData.m_pCMDSender	= nullptr;//Internall 명령이면 null이 들어 간다.
	newData.pCallObj				= (void*)this;

	newData.uFunID_Dest		= nBiz_Func_CylinderAct;	
	newData.uSeqID_Dest		= (USHORT)wParam;
	newData.uUnitID_Dest		= nBiz_Unit_Zero;

	newData.nMotionData.Reset();
	newData.nMotionData.nAction = VS24MotData::Act_None;
	newData.nMotionData.SelectPoint =(int)lParam;

	if(m_MsgProcThreadQ.QPutNode(&newData ) == false)
	{
		G_WriteInfo(Log_Check, "해당 Cylinder 동작 수행 중 입니다.");
	}
	return 0;
}

void CVS24MotionDlg::WrriteTestPositionData(int SelectAxis)
{
	if(SelectAxis<AXIS_TT01_TableDrive)
		return;

	CString strSection;
	strSection.Format("%s%02d", Section_AXIS_, SelectAxis);

	CString strReadData;
	GetDlgItem(IDC_ED_MOT_MOVE_STEP)->GetWindowText(strReadData);
	WritePrivateProfileString(strSection, Key_MOVE_STEP, strReadData.GetBuffer(strReadData.GetLength()), VS24Motion_PARAM_INI_PATH);

	GetDlgItem(IDC_ED_MOT_MOVE_POS1)->GetWindowText(strReadData);
	WritePrivateProfileString(strSection, Key_MOVE_POS1, strReadData.GetBuffer(strReadData.GetLength()), VS24Motion_PARAM_INI_PATH);

	GetDlgItem(IDC_ED_MOT_MOVE_POS2)->GetWindowText(strReadData);
	WritePrivateProfileString(strSection, Key_MOVE_POS2, strReadData.GetBuffer(strReadData.GetLength()), VS24Motion_PARAM_INI_PATH);

	GetDlgItem(IDC_ED_MOT_MOVE_POS3)->GetWindowText(strReadData);
	WritePrivateProfileString(strSection, Key_MOVE_POS3, strReadData.GetBuffer(strReadData.GetLength()), VS24Motion_PARAM_INI_PATH);

	GetDlgItem(IDC_ED_MOT_MOVE_POS4)->GetWindowText(strReadData);
	WritePrivateProfileString(strSection, Key_MOVE_POS4, strReadData.GetBuffer(strReadData.GetLength()), VS24Motion_PARAM_INI_PATH);

	GetDlgItem(IDC_ED_MOT_MOVE_POS5)->GetWindowText(strReadData);
	WritePrivateProfileString(strSection, Key_MOVE_POS5, strReadData.GetBuffer(strReadData.GetLength()), VS24Motion_PARAM_INI_PATH);
}
void CVS24MotionDlg::ReadTestPositionData(int SelectAxis)
{
	CString strSection;
	strSection.Format("%s%02d", Section_AXIS_, SelectAxis);

	char strReadData[256];

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(strSection, Key_MOVE_STEP, "", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	if(strlen(strReadData)==0)
		GetDlgItem(IDC_ED_MOT_MOVE_STEP)->SetWindowText("");
	else
		GetDlgItem(IDC_ED_MOT_MOVE_STEP)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(strSection, Key_MOVE_POS1, "", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	if(strlen(strReadData)==0)
		GetDlgItem(IDC_ED_MOT_MOVE_POS1)->SetWindowText("");
	else
		GetDlgItem(IDC_ED_MOT_MOVE_POS1)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(strSection, Key_MOVE_POS2, "", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	if(strlen(strReadData)==0)
		GetDlgItem(IDC_ED_MOT_MOVE_POS2)->SetWindowText("");
	else
		GetDlgItem(IDC_ED_MOT_MOVE_POS2)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(strSection, Key_MOVE_POS3, "", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	if(strlen(strReadData)==0)
		GetDlgItem(IDC_ED_MOT_MOVE_POS3)->SetWindowText("");
	else
		GetDlgItem(IDC_ED_MOT_MOVE_POS3)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(strSection, Key_MOVE_POS4, "", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	if(strlen(strReadData)==0)
		GetDlgItem(IDC_ED_MOT_MOVE_POS4)->SetWindowText("");
	else
		GetDlgItem(IDC_ED_MOT_MOVE_POS4)->SetWindowText(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(strSection, Key_MOVE_POS5, "", &strReadData[0], 256, VS24Motion_PARAM_INI_PATH);
	if(strlen(strReadData)==0)
		GetDlgItem(IDC_ED_MOT_MOVE_POS5)->SetWindowText("");
	else
		GetDlgItem(IDC_ED_MOT_MOVE_POS5)->SetWindowText(strReadData);
}
void CVS24MotionDlg::OnBnClickedBtAxisBtEvent()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();
	int SelectAxis = -1;
	for(int nIndex = 0; nIndex < AXIS_Cnt; nIndex++)
	{
		if(m_btAxisSelect[nIndex].GetDlgCtrlID() == wID)
		{
			m_btAxisSelect[nIndex].SetCheck(true);
			SelectAxis = nIndex;
		}
		else
		{
			m_btAxisSelect[nIndex].SetCheck(false);
		}
	}
	WrriteTestPositionData(m_AxisSelectedIndex);
	m_AxisSelectedIndex = SelectAxis;
	ReadTestPositionData(m_AxisSelectedIndex);
	


	switch(m_AxisSelectedIndex)
	{
		//TT01
	case AXIS_TT01_TableDrive://Cruiser Axis #1
	case AXIS_TT01_ThetaAlign://ML3 #18
	case AXIS_TT01_TableTurn://ML3 #19
		//BT01
	case AXIS_BT01_CassetteUpDown://ML3 #22
		//LT01
	case AXIS_LT01_BoxLoadPush://ML3 #14
		//BO01
	case AXIS_BO01_BoxRotate://ML3 #10, #11 //좌우 동기축.
	case AXIS_BO01_BoxUpDn://ML3 #12, #13 //좌우 동기축.
	case AXIS_BO01_BoxDrive://ML3 #16
		//UT01
	case AXIS_UT01_BoxUnLoadPush://ML3 #15
		//TM01
	case AXIS_TM01_ForwardBackward://ML3 #17
		//TM02
	case AXIS_TM02_PickerDrive://Cruiser Axis #7, #8 //좌우 Gantry구동 #31가상축
	case AXIS_TM02_PickerUpDown://ML3 #20
	case AXIS_TM02_PickerShift://ML3 #21
		//AM01
	case AXIS_AM01_ScanDrive://Cruiser Axis #6
	case AXIS_AM01_ScanShift://Cruiser Axis #2
	case AXIS_AM01_ScopeDrive://Cruiser Axis #5
	case AXIS_AM01_ScopeShift://Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
	case AXIS_AM01_JigInOut://ML3 #23
		{
			if( m_AxisSelectedIndex == AXIS_TT01_TableDrive ||
				m_AxisSelectedIndex == AXIS_TM02_PickerDrive ||
				m_AxisSelectedIndex == AXIS_AM01_ScanDrive ||
				m_AxisSelectedIndex == AXIS_AM01_ScanShift ||
				m_AxisSelectedIndex == AXIS_AM01_ScopeDrive ||
				m_AxisSelectedIndex == AXIS_AM01_ScopeShift )
				GetDlgItem(IDC_ST_MOT_STATE)->SetWindowText("Motion State(Cruiser)");
			else
				GetDlgItem(IDC_ST_MOT_STATE)->SetWindowText("Motion State(ML3)");
			m_pViewMotState->ViewMotState_CruiserML3(m_AxisSelectedIndex);
		}break;

		//TM02
	case AXIS_TM02_PickerRotate://AxisLink_A Y
		//AM01
	case AXIS_AM01_ScanUpDn://AxisLink_D X
	case AXIS_AM01_SpaceSnUpDn://AxisLink_D Y
	case AXIS_AM01_ScopeUpDn://AxisLink_D Z
	case AXIS_AM01_AirBlowLeftRight://AxisLink_D U
	case AXIS_AM01_TensionLeftUpDn://AxisLink_A Z
	case AXIS_AM01_TensionRightUpDn://AxisLink_A U
	case AXIS_AM01_LTension1://AxisLink_B X
	case AXIS_AM01_LTension2://AxisLink_B Y
	case AXIS_AM01_LTension3://AxisLink_B Z
	case AXIS_AM01_LTension4://AxisLink_B U
	case AXIS_AM01_RTension1://AxisLink_C X
	case AXIS_AM01_RTension2://AxisLink_C Y
	case AXIS_AM01_RTension3://AxisLink_C Z
	case AXIS_AM01_RTension4://AxisLink_C U

		{
			GetDlgItem(IDC_ST_MOT_STATE)->SetWindowText("Motion State(Axis Link)");
			m_pViewMotState->ViewMotState_Axislink(m_AxisSelectedIndex);
		}break;
	default:
		break;
	}
}

void CVS24MotionDlg::OnBnClickedBtMotHome()
{
	if(m_AxisSelectedIndex>=0)
	{
		SendMotInternalCMDQ(m_AxisSelectedIndex, VS24MotData::Act_Home, 0);
	}
	else
		G_WriteInfo(Log_Worrying, "Select Target Axis");
}
void CVS24MotionDlg::OnBnClickedBtMotLeft()
{
	CString strNewPos;
	if(m_AxisSelectedIndex>=0)
	{
		GetDlgItemText(IDC_ED_MOT_MOVE_STEP, strNewPos);
		if(strNewPos.GetLength()>0)
		{
			int NewPos = (int)(G_MotCtrlObj.m_AxisNowPos[m_AxisSelectedIndex]-(atoi(strNewPos)));
			SendMotInternalCMDQ(m_AxisSelectedIndex, VS24MotData::Act_Move, NewPos);
		}
	}
	else
		G_WriteInfo(Log_Worrying, "Select Target Axis");
}
void CVS24MotionDlg::OnBnClickedBtMotRight()
{
	CString strNewPos;
	if(m_AxisSelectedIndex>=0)
	{
		GetDlgItemText(IDC_ED_MOT_MOVE_STEP, strNewPos);
		if(strNewPos.GetLength()>0)
		{
			int NewPos = (int)(G_MotCtrlObj.m_AxisNowPos[m_AxisSelectedIndex]+(atoi(strNewPos)));
			SendMotInternalCMDQ(m_AxisSelectedIndex, VS24MotData::Act_Move, NewPos);
		}
	}
	else
		G_WriteInfo(Log_Worrying, "Select Target Axis");
}
void CVS24MotionDlg::OnBnClickedBtMotMovePos()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();
	CString strNewPos;

	if(m_AxisSelectedIndex>=0)
	{
		switch(wID)
		{
		case IDC_BT_MOT_MOVE_POS1:
			{
				GetDlgItemText(IDC_ED_MOT_MOVE_POS1, strNewPos);
			}break;
		case IDC_BT_MOT_MOVE_POS2:
			{
				GetDlgItemText(IDC_ED_MOT_MOVE_POS2, strNewPos);
			}break;
		case IDC_BT_MOT_MOVE_POS3:
			{
				GetDlgItemText(IDC_ED_MOT_MOVE_POS3, strNewPos);
			}break;
		case IDC_BT_MOT_MOVE_POS4:
			{
				GetDlgItemText(IDC_ED_MOT_MOVE_POS4, strNewPos);
			}break;
		case IDC_BT_MOT_MOVE_POS5:
			{
				GetDlgItemText(IDC_ED_MOT_MOVE_POS5, strNewPos);
			}break;
		default:
			return;
		}
		if(strNewPos.GetLength()>0)
			SendMotInternalCMDQ(m_AxisSelectedIndex, VS24MotData::Act_Move, atoi(strNewPos));
		else
			G_WriteInfo(Log_Worrying, "Input New Position");
	}
	else
		G_WriteInfo(Log_Worrying, "Select Target Axis");
	
}


void CVS24MotionDlg::OnBnClickedBtMotStop()
{
	if(m_AxisSelectedIndex>=0)
	{
		SendMotInternalCMDQ(m_AxisSelectedIndex, VS24MotData::Act_Stop, 0);
	}
	else
		G_WriteInfo(Log_Worrying, "Select Target Axis");
}


#include "InitSystemDlg.h"
void CVS24MotionDlg::OnBnClickedBtMotSystemInit()
{
	//CPassWord InputPassWord;
	//if(InputPassWord.DoModal() == IDOK)
	{
		CInitSyDlg SysInitDlg;
		SysInitDlg.DoModal();
	}
}


void CVS24MotionDlg::OnDestroy()
{
	__super::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

}

void CVS24MotionDlg::OnBnClickedBtChangeView()
{
	if(m_pViewACCDlg->IsWindowVisible() == TRUE)
	{
		m_btViewChange.SetWindowText("View ACC");
		ChangeView_ACC_SOL(2);
	}
	else
	{
		m_btViewChange.SetWindowText("View Cylinder");
		ChangeView_ACC_SOL(1);
	}
	
}


void CVS24MotionDlg::OnBnClickedBtMotSystemPm()
{
	if(m_btSetPMMode.GetCheck() == true)
	{
		m_btSetPMMode.SetCheck(false);
		m_fnChangeTaskState_ReleasePM();
		m_btSetPMMode.SetWindowText("Set PM");
	}
	else
	{
		m_fnChangeTaskState(TASK_STATE_PM);
		m_btSetPMMode.SetCheck(true);
		m_btSetPMMode.SetWindowText("Release PM");
	}
	
}


void CVS24MotionDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//CRect ViewMapSize;
	//this->GetWindowRect(&ViewMapSize);
	//if(ViewMapSize.PtInRect(point) == TRUE )
	//{
	//	this->SetForegroundWindow();
	//}
	__super::OnMouseMove(nFlags, point);
}
