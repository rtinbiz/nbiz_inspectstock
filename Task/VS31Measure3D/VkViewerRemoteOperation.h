#pragma once

#ifndef VKAPI
#define VKAPI	__stdcall
#endif

/**
	@brief 汎用リ??ンコ?ド
*/
enum VKResult
{
	VKResult_OK = 0,								/**< 正常終了 */
	VKResult_UnknownError = -1,						/**< 不明なエラ? */
	VKResult_AlreadyInitialized = -2,				/**< 既に初期済み */
	VKResult_NotInitialized = -3,					/**< 初期化が行われていない */
	VKResult_NotAccepted = -5,						/**< 観?アプリとの通信に失敗 */
	VKResult_InvalidArgument = -6,					/**< 引数が不正 */
	VKResult_ReachedLimit = -7,						/**< リ?ットに接触 */
	VKResult_ShortageDistance = -9,					/**< ディス?ンスが短すぎる */
	VKResult_ExcessDistance = -10,					/**< ディス?ンスが長すぎる */
	VKResult_NotSupportFaceFilmThickness = -11,		/**< 面膜厚に対応していない */
	VKResult_ExcessPitch = -13,						/**< ピッ?が大きすぎる */
	VKResult_OutOfMemory = -14,						/**< メモリ不足 */
	VKResult_AutoProcessCancel = -15,				/**< オ?ト処理がキャンセルされた */
	VKResult_AutoProcessChangedRevolver = -16,		/**< オ?ト処理中にレ?ルバが変更された */
	VKResult_NotSupportThicknessParam = -17,		/**< (コントロ?ラ?が)膜厚に対応していない */
	VKResult_NotSupportTopLayer = -20,				/**< 膜光学調整を行っていない状態で最?面測定を行おうとした*/
	VKResult_PmtProtectionDetected = -22,			/**< レ?ザ?受光素子保護??が働いたため、処理が中?された */
	VKResult_ConnectionLost = -23,					/**< 処理中にコントロ?ラ?から切断された */
	VKResult_LaserFailureAbort = -24,				/**< レ?ザ?出力異常を検知したため、処理が中?された */
	VKResult_ComputerSuspended = -25,				/**< コンピュ???がスリ?プ状態(休?状態)に移行したため、処理が中?された */
	VKResult_AutoFocusFail = -26,					/**< オ?トフォ?カスでピントが合わなかった */
	VKResult_StillZMoving = -27,					/**< Z軸移動中のため、移動できなかった */
	VKResult_ControllerIncompatible = -28,			/**< コントロ?ラ?のバ?ジョンが一致しない（コントロ?ラ?アップデ?トが必要） */
	VKResult_NotSupportedZoomAndSize = -29,			/**< 測定できないズ??・測定サイズの組み合わせ */
	VKResult_InvalidMeasureRange = -30,				/**< 測定できない測定範囲設定 */
};

/**
	@brief エラ?コ?ド
*/
enum VKError
{
	VKError_NoError = 0,				/**< 正常終了 */
	VKError_Unknown = -1,				/**< 不明なエラ? */
	VKError_Cancel = -2,				/**< 測定がキャンセルされた */
	VKError_ChangedRevolver = -4,		/**< レ?ルバが変更された */
	VKError_ConnectionLost = -6,		/**< コントロ?ラ?から切断された */
	VKError_PmtProtection = -7,			/**< レ?ザ?受光素子保護??が働いたため、処理が中?された */
	VKError_LaserFailure = -8,			/**< レ?ザ?出力異常を検知したため、処理が中?された */
	VKError_ComputerSuspended = -9,		/**< コンピュ???がスリ?プ状態(休?状態)に移行したため、処理が中?された */
	VKError_ReachedZAxisLimit = -10,	/**< Z軸のリ?ットスイッ?に接触したため、処理が中?された */
};

/**
	@brief 初期化処理
	@retval VKResult_OK 正常終了
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
	@note 通信DLLを使用する場合この関数を最初に呼び出して下さい。
*/
long VKAPI VK_Initialize();

/**
	@brief 終了処理
	@retval VKResult_OK 成功
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_UnknownError 不明なエラ?
	@note 通信DLLを使用した後この関数を呼び出して下さい。
*/
long VKAPI VK_Uninitialize();

/**
	@brief 測定モ?ド設定・取得
	@param mode, *pMode FaceMeasurement：面測定
						LineMeasurement：ライン測定
						FaceFilmThicknessMeasurement：面膜厚測定
						LineFilmThicknessMeasurement：ライン膜厚測定
						FaceTopLayerMeasurement : 面最?面測定
						LineTopLayerMeasurement : ライン最?面測定
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_NotSupportThicknessParam 最?面は未対応
	@retval VKResult_UnknownError 不明なエラ?
*/
typedef enum {
	Face,
	Line,
	FaceFilmThickness,
	LineFilmThickness,
	FaceTopLayer,
	LineTopLayer
} MeasurementMode;
long VKAPI VK_SetMeasurementMode(MeasurementMode mode);
long VKAPI VK_GetMeasurementMode(MeasurementMode* pMode);

/**
	@brief 測定品質設定・取得
	@param quality, *pQuality HighSpeed：標?サイズ＆超高速
							  HighResolution：標?サイズ＆高精度
							  HighDensity：高精細サイズ＆高精度
							  Part1of12Accuracy：パ?ト1/12サイズ＆高精度
							  Part1of12SuperHighSpeed：パ?ト1/12サイズ＆超高速
							  StandardHighSpeed：標?サイズ＆高速
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
typedef enum {
	HighSpeed,
	HighResolution,
	HighDensity,
	Part1Of12Accuracy,
	Part1Of12SuperHighSpeed,
	StandardHighSpeed,
} MeasurementQuality;
long VKAPI VK_SetMeasurementQuality(MeasurementQuality quality);
long VKAPI VK_GetMeasurementQuality(MeasurementQuality* pQuality);

/**
	@brief ?示?イプ設定・取得
	@param type, *pType	Camera：カメラ
						Laser：レ?ザ?
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
typedef enum {
	Camera,
	Laser,
} ViewType;
long VKAPI VK_SetViewType(ViewType type);
long VKAPI VK_GetViewType(ViewType* pType);

/**
	@brief レンズ移動
	@param lMove レンズ移動量(nm) [正方向の値は下方向への移動、負方向の値は上方向への移動]
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ReachedLimit リ?ットに接触したため、移動できない
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_MoveLens(long lMove);

/**
	@brief レンズ原?移動
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
	@note 衝突の危険性がありますので安全なステ?ジ位置の状態で実行してください
*/
long VKAPI VK_MoveLensToOrigin();

/**
	@brief レンズ位置取得
	@param *plPosition レンズ位置(nm)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_GetLensPosition(long* plPosition);

/**
	@brief 測定レンズ上限設定・取得
	@param lPosition, *plPosition レンズ位置(nm)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
	@note 引数無しでVK_SetUpperPositionを呼び出すと現在のレンズ位置で測定上限設定を行う
*/
long VKAPI VK_SetUpperPosition(long lPosition = -1);
long VKAPI VK_GetUpperPosition(long* plPosition);

/**
	@brief 測定レンズ下限設定・取得
	@param lPosition, *plPosition レンズ位置(nm)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
	@note 引数無しでVK_SetLowerPositionを呼び出すと現在のレンズ位置で測定下限設定を行う
*/
long VKAPI VK_SetLowerPosition(long lPosition = -1);
long VKAPI VK_GetLowerPosition(long* plPosition);

/**
	@brief ピッ?設定
	@param pitch Pitch_001um：0.01μm
				 Pitch_002um：0.02μm
				 Pitch_005um：0.05μm
				 Pitch_01um：0.1μm
				 Pitch_02um：0.2μm
				 Pitch_05um：0.5μm
				 Pitch_1um：1μm
				 Pitch_2um：2μm
				 Pitch_5um：5μm
				 Pitch_10um：10μm
				 Pitch_20um：20μm
				 Pitch_50um：50μm
				 Pitch_100um：50μm
				 Pitch_200um：50μm
				 Pitch_500um：50μm
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
typedef enum {
	Pitch_001um,
	Pitch_002um,
	Pitch_005um,
	Pitch_01um,
	Pitch_02um,
	Pitch_05um,
	Pitch_1um,
	Pitch_2um,
	Pitch_5um,
	Pitch_10um,
	Pitch_20um,
	Pitch_50um,
	Pitch_100um,
	Pitch_200um,
	Pitch_500um,
} PitchIndex;
long VKAPI VK_SetPitch(PitchIndex pitch);

/**
	@brief ピッ?取得
	@param *plPitch ピッ?(nm)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_GetPitch(long* plPitch);

/**
	@brief ND設定・取得
	@param lNd, *plNd ND(0-4)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_SetNd(long lNd);
long VKAPI VK_GetNd(long* plNd);

/**
	@brief ゲイン設定・取得
	@param lGain, *plGain ゲイン(底値-16000)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_SetLaserGain1(long lGain);
long VKAPI VK_GetLaserGain1(long* plGain);

/**
	@brief ライン位置設定・取得
	@param lLinePosition, *plLinePosition ライン位置(0-767)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_SetMeasureLinePosition(long lLinePosition);
long VKAPI VK_GetMeasureLinePosition(long* plLinePosition);

/**
	@brief RPD設定・取得
	@param bIsRpd, *pbIsRpd RPDがONかどうか
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_SetRpd(BOOL bIsRpd);
long VKAPI VK_IsRpd(BOOL* pbIsRpd);

/**
	@brief RPDのモ?ド設定・取得
	@param rpdPreference, *pRpdPreference
			Quality		精度優先
			Speed		速度優先
			Automatic	自動(レンズ?率により精度優先・速度優先のいずれかになる)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
typedef enum {
	Quality,
	Speed,
	Automatic,
} RpdPreference;
long VKAPI VK_SetRpdPreference(RpdPreference rpdPreference);
long VKAPI VK_GetRpdPreference(RpdPreference* pRpdPreference);

/**
	@brief ズ??設定・取得
	@param zoom, pZoom Zoom_10x 1.0?
					? Zoom_15x 1.5?
					   Zoom_20x 2.0?
					   Zoom_30x 3.0?
					   Zoom_50x 5.0?
					   Zoom_80x 8.0?
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
typedef enum {
	Zoom_10x,
	Zoom_15x,
	Zoom_20x,
	Zoom_30x,
	Obsoleted_Zoom_40x,
	Obsoleted_Zoom_60x,
	Zoom_50x,
	Zoom_80x,
} ZoomIndex;
long VKAPI VK_SetZoom(ZoomIndex zoom);
long VKAPI VK_GetZoom(ZoomIndex* pZoom);

/**
	@brief 白黒モ?ド設定・取得
	@param bEnableColorAcquisition, *pbEnableColorAcquisition カラ?を取得するかどうか
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_SetEnableColorAcquisition(BOOL bEnableColorAcquisition);
long VKAPI VK_GetEnableColorAcquisition(BOOL* pbEnableColorAcquisition);

typedef enum {
	None,
    Weak,
    Middle,
    Strong,
} LayerAdjustFilter;

typedef enum {
    PeakDetect_Standard,
    PeakDetect_Strong,
    PeakDetect_SpecialStrong,
} PeakDetectParam;

/**
	@brief オ?トフォ?カス
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_DoAutofocus();
/**
	@brief 測定開始
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 測定中、観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_ShortageDistance ディス?ンスが短い
	@retval VKResult_ExcessDistance ディス?ンスが長すぎる
	@retval VKResult_NotSupportFaceFilmThickness 面膜厚に対応していないレンズ
	@retval VKResult_NotSupportTopLayer 最?面測定に対応していないレンズ
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_StartMeasurement();

/**
	@brief 測定中?
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 測定中でない、観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_StopMeasurement();

/**
	@brief 測定中かどうか
	@param pbIsMeasuring 測定中かどうか
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_IsMeasuring(BOOL* pbIsMeasuring);

/**
	@brief 測定結果
	@param plLastError VKErrorを参照
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_GetLastError(long* plLastError);

/**
	@brief レ?ザ?+カラ?画像の取得
	@param *pbyColorData カラ?デ??
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_OutOfMemory メモリ不足
	@retval VKResult_UnknownError 不明なエラ?
	@note 幅*高さ*3(RGB)分のBYTE?デ??領域を確保しておく
	@note 測定サイズにより確保が必要なサイズが異なるので注意
*/
long VKAPI VK_GetColorData(BYTE* pbyColorData);

/**
	@brief 光量デ??取得 (VK-8700/9700互換)
	@param *psLightData 光量デ??(14bit)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_OutOfMemory メモリ不足
	@retval VKResult_UnknownError 不明なエラ?
	@note 幅*高さ分のshort?デ??領域を確保しておく
	@note 測定サイズにより確保が必要なサイズが異なるので注意
*/
long VKAPI VK_GetLightData(short* psLightData);

/**
	@brief 高さデ??取得
	@param *lHeightData 高さデ??
	@param dataType    HeightDataType_Unit1Nanometer 1nm単位(VK-8700/9700互換)
					? HeightDataType_Unit100Picometer 0.1nm(=100pm)単位
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_OutOfMemory メモリ不足
	@retval VKResult_UnknownError 不明なエラ?
	@note 幅*高さ分のlong?デ??領域を確保しておく
	@note 測定サイズにより確保が必要なサイズが異なるので注意
*/
typedef enum {
    HeightDataType_Unit1Nanometer,
    HeightDataType_Unit100Picometer,
} HeightDataType;
long VKAPI VK_GetHeightData2(long* plHeightData, HeightDataType dataType);

/**
	@brief ライン光量デ??取得
	@param lLineNumber ライン番号(1のみ)
	@param *psLightData 光量デ??(14bit)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_OutOfMemory メモリ不足
	@retval VKResult_UnknownError 不明なエラ?
	@note 1024(幅)*1(高さ)分のshort?デ??領域を確保しておく
*/
long VKAPI VK_GetLineLightData(long lLineNumber, short* psLightData);

/**
	@brief ライン高さデ??取得
	@param *plHeightData 高さデ??
	@param dataType    HeightDataType_Unit1Nanometer 1nm単位(VK-8700/9700互換)
					? HeightDataType_Unit100Picometer 0.1nm(=100pm)単位
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_OutOfMemory メモリ不足
	@retval VKResult_UnknownError 不明なエラ?
	@note 1024(幅)*1(高さ)分のlong?デ??領域を確保しておく
*/
long VKAPI VK_GetLineHeightData2(long* plHeightData, HeightDataType dataType);

/**
	@brief 測定条件取得
	@param *pParameter 測定条件?造体デ??
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
	@note 必ずnSizeに?造体のサイズ(sizeof(MeasurementParameter2))を代入しておくこと。
*/
typedef struct
{
	size_t nSize;						// ?造体のサイズ
	MeasurementMode mode;				// 測定モ?ド
	MeasurementQuality quality;			// 測定品質
	long lUpperPosition;				// レンズ上限(nm)
	long lLowerPosition;				// レンズ下限(nm)
	long lPitch;						// ピッ?(nm)
	long lDistance;						// ディス?ンス(nm)
	long lNd;							// レ?ザ?照明フィル??1
	long lGain;							// レ?ザ?明るさ1 (VK-8700/VK-9700互換 底値-4095)
	long lShutterSpeed;					// シャッ??スピ?ド (VK-8700/VK-9700互換 0-255)
	BOOL bIsShutterSpeedAuto;			// シャッ??スピ?ド自動
	long lLinePosition[3];				// ライン位置(pixel)
	long lLineNumber;					// ライン数(?)
	BOOL bIsRpd;						// RPD
	RpdPreference rpdPreference;		// Z軸モ?ド
	ZoomIndex zoom;						// ズ??
	double dXYCalibration;				// XYキャリブレ?ション(μm/pixel)
	double dZCalibrationFactor;			// Zキャリブレ?ション(比)
	BOOL bEnableColorAcquisition;		// 白黒モ?ド
	BOOL bThicknessParameterAuto;		// 膜厚パラメ??の自動設定
	LayerAdjustFilter filter;			// 層間調整フィル?
	PeakDetectParam peakDetectParam;	// 不要ピ?ク除去
	long lThickness;					// 膜厚(nm)
	long lMeasureLinePosition;			// ライン測定位置
	long lLaserGain1;					// レ?ザ?明るさ1 (底値-16000)
	long lCameraShutterSpeed;			// シャッ??スピ?ド (0-789)
} MeasurementParameter2;
long VKAPI VK_GetMeasurementParameter2(MeasurementParameter2* pParameter);

/**
	@brief 測定結果保存
	@param *pcFileName 保存フ?イル名
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_SaveMeasurementResult(const char* szFileName);

/**
	@brief スナップショット保存
	@param *pcFileName 保存フ?イル名
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_SaveSnapshot(const char* szFileName);

/**
	@brief RPDピッ?の取得･�??
	@param dataType    RpdPitch_Normal RPD標?ピッ?
					? RpdPitch_HighSpeed RPD高速ピッ?
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
	@note 透明体（膜厚）・透明体（最?面）では高速ピッ?が使用できないため、設定しようとした場合は引数が不正とみなされ、エラ?となる
	@note RPD OFFの場合は必ず標?ピッ?として取得される。RPD OFF時は設定不可。
*/
typedef enum
{
	RpdPitch_Normal = 1,		/**< 標?ピッ? */
	RpdPitch_HighSpeed = 3,		/**< 高速ピッ? */
} RpdPitch;
long VKAPI VK_GetRpdPitch(RpdPitch* value);
long VKAPI VK_SetRpdPitch(RpdPitch value);

/**
	@brief コントロ?ラ?起因のイベントID
*/
typedef enum 
{
	PmtProtectionDetected = 1,	/**< レ?ザ?受光素子保護検知 */
} VKHardwareEventId;
/**
	@brief コントロ?ラ起因のイベント通知コ?ルバック関数
	@param eventId イベントID
	@param pvUserParam ユ?ザ?任意のパラメ??
*/
typedef void (__stdcall *VKHardwareEventCallback)(VKHardwareEventId eventId, void* pvUserParam);

/**
	@brief ハ?ドウェア起因のイベントが発生した場合に呼び出されるコ?ルバック関数を設定する
	@param pCallback コ?ルバック関数の?イン? NULL指定時は設定解除
	@param pvUserParam ユ?ザ?任意のパラメ??
	@retval VKResult_NotInitialized 未初期化
	@note 複数の関数を同時に登?できない。既に登?されている状態で登?した場合は直前の登?が失われる。
*/
long VKAPI VK_SetHardwareEventCallback(VKHardwareEventCallback pCallback, void* pvUserParam);