#pragma once
#include "resource.h"
#include "MeasureProc.h"
//#include "Viewer3D.h"
typedef struct RUNNER_PROCESS_TRG
{
	BOOL						m_bProcessRun;
	ObsAppState				*	m_pObservationAppState;
	ObsActionState			*	m_pObsActionState;
	CWinThread				*	m_pRunnerThread;
	BOOL					*	m_pObserAPPStart;
	HWND						m_MainWinHWnd;

	CInterServerInterface	*	m_pServerObj;
	//////////////////////////////////////////////////////////////////////////
	//Up/Down Position 계산용
	int						*	m_pDetectThreshold;
	int						*	m_pDetectCountResult;
	//////////////////////////////////////////////////////////////////////////
	//Live Display용
	CRect					*	m_pViewRect;
	HDC						*	m_pHDcView;
	RUNNER_PROCESS_TRG()
	{
		m_bProcessRun			= FALSE;
		m_pObservationAppState  = NULL;
		m_pObsActionState		= NULL;

		m_pRunnerThread			= NULL;
		m_pObserAPPStart		= NULL;
		m_MainWinHWnd			= NULL;

		m_pServerObj			= NULL;

		m_pDetectThreshold		= NULL;

		m_pDetectCountResult	= 0;

		m_pViewRect				= NULL;
		m_pHDcView				= NULL;
	}

} RUNNER_PROCESS, *LPRUNNER_PROCESS;
UINT fn_OBS_RunnerProcess(LPVOID pParam);
void CenterWindowTopMost(HWND TargetWind, BOOL MoveCenter= TRUE);
UINT fn_OBS_LiveProcess(LPVOID pParam);





//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
typedef struct ACTION_PROC_TRG
{
	BOOL						m_bProcessRun;
	ObsActionState			*	m_pObsActionState;
	CWinThread				*	m_pRunnerThread;
	HWND						m_MainWinHWnd;
	CInterServerInterface	*	m_pServerObj;

	int						*	m_pDetectThreshold;
	//////////////////////////////////////////////////////////////////////////
	HWND						m_3DViewerHandle;
	DATA_3D_RAW				*	m_pNowMeasurData;
	//////////////////////////////////////////////////////////////////////////
	//Auto Measuer용
	Act_Tyep					m_StepList[10];
	long						m_TargetThick;
	char						m_WriteDataFilePath[MAX_PATH];

	int						*	m_pDetectCountResult;

	int						*	m_pMaskType;

	CMeasureProc				*m_pMeasurWin3D;

	RESULT_DATA				*m_pMeasureResultData;

	ACTION_PROC_TRG()
	{
		m_bProcessRun			= FALSE;
		m_pObsActionState		= NULL;
		m_pRunnerThread			= NULL;
		m_MainWinHWnd			= NULL;

		m_pServerObj			= NULL;

		m_pDetectThreshold		= NULL;

		m_3DViewerHandle				= NULL;
		m_pNowMeasurData		= NULL;

		m_TargetThick			=45;  //60->25 정의천

		memset(&m_StepList, 0x00, sizeof(Act_Tyep)*10);
		memset(m_WriteDataFilePath, 0x00, MAX_PATH);

		m_pDetectCountResult	= NULL;

		m_pMaskType				= NULL;

		m_pMeasurWin3D			= NULL;
		m_pMeasureResultData	= NULL;
	}

} ACTION_PROC, *LPACTION_PROC;


typedef struct ACTION_FN_TRG
{
	BOOL					*	m_bpMainProcRun;
	BOOL						m_bProcessRun;
	ObsActionState			*	m_pObsActionState;
	HWND						m_MainWinHWnd;
	CInterServerInterface	*	m_pServerObj;
	
	int						*	m_pDetectThreshold;
	int						*	m_pDetectCountResult;

	ACTION_FN_TRG()
	{
		m_bpMainProcRun			= NULL;
		m_bProcessRun			= FALSE;
		m_pObsActionState		= NULL;
		m_MainWinHWnd			= NULL;

		m_pServerObj			= NULL;

		m_pDetectThreshold		= NULL;
		m_pDetectCountResult	= NULL;
	}

} ACTION_FN, *LPACTION_FN;
//////////////////////////////////////////////////////////////////////////

void PutImage_Text(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor=CV_RGB(0,0,0), CvScalar BackColor=CV_RGB(255,255,255), CvScalar RectColor=CV_RGB(255,0,0));
void PutImage_Text_Small(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor=CV_RGB(0,0,0), CvScalar BackColor=CV_RGB(255,255,255), CvScalar RectColor=CV_RGB(255,0,0));
void PutImage_Text_VSmall(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor=CV_RGB(0,0,0));//, CvScalar BackColor=CV_RGB(255,255,255), CvScalar RectColor=CV_RGB(255,0,0));
void PutImage_TextBig(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor=CV_RGB(0,0,0), CvScalar BackColor=CV_RGB(255,255,255), CvScalar RectColor=CV_RGB(255,0,0));
void PutImage_TextData(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor=CV_RGB(0,0,0), CvScalar BackColor=CV_RGB(255,255,255), CvScalar RectColor=CV_RGB(255,0,0));

//단동 Command 조합.
//UINT fn_OBS_AutoGainActionProc(LPVOID pParam);
UINT fn_OBS_AutoFocusActionProc(LPVOID pParam);
//UINT fn_OBS_AutoUpDnPosActionProc(LPVOID pParam);
UINT fn_OBS_StartMeasureActionProc(LPVOID pParam);
//Auto Command 조합.
UINT fn_OBS_AutoMeasureActionProc(LPVOID pParam);

//ImageSave Command 조합.
UINT fn_OBS_ImageSaveActionProc(LPVOID pParam);

//////////////////////////////////////////////////////////////////////////
//void Action_AutoGain(ACTION_FN *pParam);
void Action_AutoFocus(ACTION_FN *pParam);
//void Action_AutoUpDnPos(ACTION_FN *pParam);
void Action_AutoUpDnPosByImage(ACTION_FN *pParam, int *pDetectValue, long TargetThick);
		int FindDetectedValue(HDC h_memory_dc, HDC h_screen_dc, BYTE *p_image_data,IplImage *pViewPort, IplImage *pHistViewPort, int DetectThreshold = 30);
void Action_StartMeasure(ACTION_FN *pParam, long TargetThick = 0);
void Action_AutoSaveData(ACTION_FN *pParam, char *pWriteFileFullPath);///, int *pMaskType = NULL ,	int Thickness=0, HWND D3DViewerHandle = NULL, DATA_3D_RAW *pNowMeasurData = NULL );