// VS31Measure3DDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"

#include "MeasureProc.h"
#include "MeasureAlg.h"

struct BATCH_DATA {
	char m_szDir[128];
	char m_szActiveType[16];
	char m_szDataFileName[MAX_PATH];
	char m_szDataPath[MAX_PATH];
	MEASURE_DIR m_nAngle;
	int m_nIndex;

	BATCH_DATA() {
		m_szDir[0] = 0x00;
		m_szActiveType[0] = 0x00;
		m_szDataFileName[0] = 0x00;
		m_szDataPath[0] = 0x00;
		m_nAngle = MEASURE_ANGLE_0;
		m_nIndex = 0;
	}
};

struct BATCH_RESULT {
	char m_szDataFileName[MAX_PATH];
	char m_szActiveType[16];

	RESULT_DATA m_dHoriz;
	RESULT_DATA m_dVert;
	RESULT_DATA m_dDiag;

	BATCH_RESULT() {
		m_szDataFileName[0] = 0x00;
		m_szActiveType[0] = 0x00;
		memset(&m_dHoriz, 0x00, sizeof(RESULT_DATA));
		memset(&m_dVert, 0x00, sizeof(RESULT_DATA));
		memset(&m_dDiag, 0x00, sizeof(RESULT_DATA));
	}
};

struct SEND_STARTMEASURE_DATA {
	int nMaskType;
	int nMeasureAngle;
	float nTargetThick;
	float nSpaceGap;
	BOOL bMeasureAll;
};

// CVS31Measure3DDlg 대화 상자
class CVS31Measure3DDlg : public CDialog, public CProcInitial
{
// 생성입니다.
public:
	CVS31Measure3DDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VS31MEASURE3D_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

public://Log Print
	CInterServerInterface m_ServerInterface;
	void			AOI_AppendList(char *pMsg,...);
	void			m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...);
private://AOI Server Interface.
	void			AOI_fnInitVS64Interface(void);
	int				AOI_fnAnalyzeMsg(CMDMSG* cmdMsg);//Message Process Function.
	LRESULT			AOI_fnMessageCallback(WPARAM wParam, LPARAM lParam);
// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public://System Parameter
	int			m_DetectThreshold;
	int			m_DetectCountResult;
public://Observation Application을 찾기 위한
	
	BOOL m_ObserAPPStart;
	ObsAppState fnObservationAPPCtrlState(BOOL ManualModeTogle =FALSE);

	ObsAppState					m_ObservationAPPState;
	RUNNER_PROCESS				m_RunnerProcData;

	ObsActionState				m_ObsActionState;
	ACTION_PROC					m_ActionProcData;

	ObsActionState				m_ObsActOLDState;
public:
	CMeasureProc		*m_pMeasurWin3D;
	long			 m_TargetThick;
	int				 m_MaskType;
	/// [2014-11-1] Charles, Added.
	MEASURE_DIR	m_nMeasureAngle;
	float m_nSpaceGap;
	BOOL m_bMeasureAll;
	int m_nMeasureIndex;
	int m_nMeasureOffsetX;
	int m_nMeasureOffsetY;


	RESULT_DATA		m_MeasureResultData;

	DATA_3D_RAW		 m_NowMeasurData;

public:
	CRect			m_ViewRect;
	HDC *			m_pHDcView;
	
	IplImage *m_pResultColorViewData;
public:
	CPictureEx m_stMeasurRun;
	BOOL m_ConnectOK;
	BOOL fnConnectHW();
	BOOL fnObsAPPActionCheck();
	BOOL  fnActionObsProcess(int RunTarget, int Index = 0, char *pMeaserDataSavePath = NULL);

	MeasurementParameter2 m_Parameter2;
	//////////////////////////////////////////////////////////////////////////
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	LRESULT OnProcFileData(WPARAM wParam, LPARAM lParam);
protected:
	virtual void PostNcDestroy();
public:
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedBtLoadParamList();
	afx_msg void OnBnClickedButtonStartmeasurement();
	afx_msg void OnBnClickedButtonStopmeasurement();
	afx_msg void OnBnClickedButtonDoautogain();
	afx_msg void OnBnClickedButtonDoautoUpDn();
	afx_msg void OnBnClickedButtonDoautofocus();
	
	afx_msg void OnBnClickedButtonToggleManual();
	afx_msg void OnBnClickedButtonMeaserSequnece();
	afx_msg void OnBnClickedButtonResetActionState();

	void m_fnLoadFile(int MaskType);
	afx_msg void OnBnClickedButtonLoadRaw3dData();
	afx_msg void OnBnClickedButtonLoadRaw3dData2();

	CListBox m_listLogView;
	void InitCtrlButtons();
	CFont m_GridFont;
	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btChangeMode;
	CRoundButton2 m_btRunAF;
	CRoundButton2 m_btStartMeaser;
	CRoundButton2 m_btStopMeasure;
	CRoundButton2 m_btAlarmReset;
	CRoundButton2 m_btViewParam;
	CRoundButton2 m_btLoadRawDataFile;
	CRoundButton2 m_btLoadRawDataFile2;
	CRoundButton2 m_btMeasure3DDataFile;
	CRoundButton2 m_btMeasureBatchData;
	CRoundButton2 m_btLensSelect[4];
	afx_msg void OnBnClickedBtTest3dImg();
	afx_msg void OnBnClickedShowResult();
	afx_msg void OnBnClickedChkEditResult();
	
	afx_msg void OnBnClickedMeasureAngle0();
	afx_msg void OnBnClickedMeasureAngle45();
	afx_msg void OnBnClickedChkStrip();
	afx_msg void OnBnClickedChkDot();
	CComboBox m_cbFilterLevelList;

	float m_BaseDepth;
	afx_msg void OnEnChangeEdBaseDepth();
	void LensChange(int LensIndex);
	afx_msg void OnBnClickedBtLens();
	afx_msg void OnBnClickedButtonMeasure3dDataFile();

public :
	/// [2014-11-05] Charles, Added.
	BOOL SelectTestDataFile();
	BOOL SelectBatchDataFile();
	BOOL MakeMeasureDataList();
	void SetTestParameter();
	void UpdateUIParameter();

	BOOL ProcMeasureAlg();
	BOOL SaveMeasureResult();
	BOOL ProcMeasureBatch();
	BOOL SaveMeasureBatchResult();

	void LoadMeasureAlgParameter();
	void SaveMeasureAlgParameter();

#ifdef _SIMULATE_MEASURE
	void SimulateMeasure();
#endif

private :
	CMeasureAlg *m_pMeasureAlg;
	char m_szDataFile[MAX_PATH];
	char m_szBatchDataFile[MAX_PATH];
	std::vector<BATCH_DATA> m_listBatchData;
	std::map<string, BATCH_RESULT> m_mapBatchResult;

	int m_nTargetThickEdit;
	float m_nSpaceGapEdit;
	int m_nMeasureIndexEdit;
	char m_szResultSavePath[MAX_PATH];

	RESULT_DATA *m_plistResultData;
	int m_nCountResultData;
public:
	afx_msg void OnBnClickedSaveAnaysisData();
	afx_msg void OnBnClickedButtonMeasureBatchData();
	int m_nMeasureOffsetXEdit;
	int m_nMeasureOffsetYEdit;
	float m_nCornerDistance;
};
