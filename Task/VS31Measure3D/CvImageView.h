#pragma once


// CvImageView

class CvImageView : public CWnd
{
	DECLARE_DYNAMIC(CvImageView)

public:
	CvImageView();
	virtual ~CvImageView();
	void Clear();

	void Show(IplImage *pImage);
	void Close();
	void DrawImage();
	void ResetScrollSize(BOOL bResetPosition = FALSE);

	static CvImageView *_Instance;
	static CvImageView *GetInstance();

protected:
	DECLARE_MESSAGE_MAP()

	virtual void PostNcDestroy();
public:
	afx_msg void OnClose();
	afx_msg void OnPaint();

private :
	IplImage *m_pOrigImage;
	CvvImage m_dViewImage;
	int m_nScrollSizeX; 
	int m_nScrollPageSizeX; 
	int m_nScrollMaxPosX; 
	int m_nScrollSizeY; 
	int m_nScrollPageSizeY; 
	int m_nScrollMaxPosY; 

public:
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg LRESULT OnNcHitTest(CPoint point);
};


