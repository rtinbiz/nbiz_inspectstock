// CvImageView.cpp : implementation file
//

#include "stdafx.h"
#include "VS31Measure3D.h"
#include "CvImageView.h"

#define DEFAULT_VIEW_WIDTH 1600
#define DEFAULT_VIEW_HEIGHT 800

// CvImageView

CvImageView *CvImageView::_Instance = 0x00;
CvImageView *CvImageView::GetInstance() 
{
	if(_Instance == 0x00) {
		_Instance = new CvImageView();
		if(_Instance == 0x00) {
			return 0x00;
		}

		_Instance->Create(NULL, "IMAGE VIEW", WS_OVERLAPPEDWINDOW | WS_VSCROLL | WS_HSCROLL, CRect(0,0,DEFAULT_VIEW_WIDTH,DEFAULT_VIEW_HEIGHT), GetDesktopWindow(), 1210);
	}

	return _Instance;
}

IMPLEMENT_DYNAMIC(CvImageView, CWnd)

CvImageView::CvImageView()
{
	m_pOrigImage = 0x00;
	m_nScrollSizeX = 0;
	m_nScrollPageSizeX = 0;
	m_nScrollMaxPosX = 0;
	m_nScrollSizeY = 0;
	m_nScrollPageSizeY = 0;
	m_nScrollMaxPosY = 0;
}

CvImageView::~CvImageView()
{
}

BEGIN_MESSAGE_MAP(CvImageView, CWnd)
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_SHOWWINDOW()
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

// CvImageView message handlers
void CvImageView::PostNcDestroy()
{
	CWnd::PostNcDestroy();

	Clear();
	delete this;
}

void CvImageView::Clear()
{
	ResetScrollSize(TRUE);

	if(m_pOrigImage) {
		cvReleaseImage(&m_pOrigImage);
		m_pOrigImage = 0x00;
	}

	m_dViewImage.Destroy();
}

void CvImageView::Show(IplImage *pImage)
{
	if(pImage == 0x00) {
		return;
	}
	
	Clear();
	m_pOrigImage = cvCloneImage(pImage);
	if(m_pOrigImage == 0x00) {
		return;
	}

	int nSizeX = pImage->width;
	int nSizeY = pImage->height;

	ResetScrollSize(TRUE);

	CRect rcClient(0,0,DEFAULT_VIEW_WIDTH,DEFAULT_VIEW_HEIGHT);
	SetWindowPos(&wndTop,rcClient.left, rcClient.top, rcClient.Width(), rcClient.Height(), SW_SHOW);
	ShowWindow(SW_RESTORE);
	CenterWindow();
	BringWindowToTop();
}

void CvImageView::Close()
{
	Clear();
	ShowWindow(SW_HIDE);
}

void CvImageView::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	ShowWindow(SW_HIDE);
}

void CvImageView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CWnd::OnPaint() for painting messages

	DrawImage();
}

void CvImageView::DrawImage()
{
	if(m_pOrigImage == 0x00) {
		return;
	}

	CRect rcClient;
	GetClientRect(&rcClient);

	CvRect rcROI;
	rcROI.x = GetScrollPos(SB_HORZ);
	rcROI.y = GetScrollPos(SB_VERT);
	rcROI.width = rcClient.Width();
	rcROI.height = rcClient.Height();

	if(rcROI.width == 0 || rcROI.height == 0 || m_pOrigImage->width ==0 || m_pOrigImage->height == 0) {
		return;
	}

	if(rcROI.width != m_dViewImage.Width() || rcROI.height!= m_dViewImage.Height()) {
		m_dViewImage.Destroy();
		m_dViewImage.Create(rcROI.width, rcROI.height, ((IPL_DEPTH_8U & 255)*3));
	}

	if(m_dViewImage.GetImage()) {
		cvSetImageROI(m_pOrigImage, rcROI);
		cvResize(m_pOrigImage, m_dViewImage.GetImage());

		m_dViewImage.DrawToHDC(GetDC()->m_hAttribDC, rcClient);
	}
}

void CvImageView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	int nCurPos = GetScrollPos(SB_HORZ);
	switch(nSBCode) {
	case SB_THUMBPOSITION :
	case SB_THUMBTRACK :
		break;
	case SB_PAGELEFT :
		nPos = max(0, nCurPos - m_nScrollPageSizeX);
		break;
	case SB_PAGERIGHT :
		nPos = min(m_nScrollMaxPosX, nCurPos + m_nScrollPageSizeX);
		break;
	case SB_LINELEFT :
		nPos = max(0, nCurPos - m_nScrollSizeX);
		break;
	case SB_LINERIGHT :
		nPos = min(m_nScrollMaxPosX, nCurPos + m_nScrollSizeX);
		break;

	default : 
		return;
	}

	SetScrollPos(SB_HORZ, nPos);
	DrawImage();

	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CvImageView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	int nCurPos = GetScrollPos(SB_VERT);
	switch(nSBCode) {
	case SB_THUMBPOSITION :
	case SB_THUMBTRACK :
		if(nPos & 0xFFFF0000) nPos &= 0x0000FFFF;
		break;
	case SB_PAGEUP :
		nPos = max(0, nCurPos - m_nScrollPageSizeY);
		break;
	case SB_PAGEDOWN :
		nPos = min(m_nScrollMaxPosY, nCurPos + m_nScrollPageSizeY);
		break;
	case SB_LINEUP :
		nPos = max(0, nCurPos - m_nScrollSizeY);
		break;
	case SB_LINEDOWN :
		nPos = min(m_nScrollMaxPosY, nCurPos + m_nScrollSizeY);
		break;

	default : 
		return;
	}

	SetScrollPos(SB_VERT, nPos);
	DrawImage();

	CWnd::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CvImageView::OnSize(UINT nType, int cx, int cy)
{
	ResetScrollSize();

	CWnd::OnSize(nType, cx, cy);
}

void CvImageView::ResetScrollSize(BOOL bResetPosition)
{
	if(m_pOrigImage == 0x00) {
		return;
	}

	if(bResetPosition) {
		SetScrollPos(SB_HORZ, 0);
		SetScrollPos(SB_VERT, 0);
	}


	int nSizeX = m_pOrigImage->width;
	int nSizeY = m_pOrigImage->height;

	CRect rcClient;
	GetClientRect(&rcClient);
	int nViewSizeX = rcClient.Width();
	int nViewSizeY = rcClient.Height();

	m_nScrollSizeX = max(1, nSizeX / 20);
	m_nScrollPageSizeX = max(1, nSizeX / 10);
	m_nScrollMaxPosX = nSizeX- nViewSizeX;
	SetScrollRange(SB_HORZ, 0, m_nScrollMaxPosX);

	m_nScrollSizeY = max(1, nSizeY / 20);
	m_nScrollPageSizeY = max(1, nSizeY / 10);
	m_nScrollMaxPosY = nSizeY - nViewSizeY;
	SetScrollRange(SB_VERT, 0, m_nScrollMaxPosY);

	if(nSizeX < nViewSizeX) ShowScrollBar(SB_HORZ, FALSE);
	else ShowScrollBar(SB_HORZ, TRUE);

	if(nSizeY < nViewSizeY) ShowScrollBar(SB_VERT, FALSE);
	else ShowScrollBar(SB_VERT, TRUE);

	DrawImage();
}

void CvImageView::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: Add your message handler code here and/or call default
	lpMMI->ptMinTrackSize.x = DEFAULT_VIEW_WIDTH / 4;
	lpMMI->ptMinTrackSize.y = DEFAULT_VIEW_HEIGHT / 4;

	CWnd::OnGetMinMaxInfo(lpMMI);
}

void CvImageView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	// TODO: Add your message handler code here
	ResetScrollSize();
}

LRESULT CvImageView::OnNcHitTest(CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CRect rc;
	GetClientRect(&rc);
	ClientToScreen(&rc);

	if(rc.PtInRect(point)) {       
		return HTCAPTION;
	}

	return CWnd::OnNcHitTest(point);
}
