#pragma once

#include "vk/COMM_Define.h"
#include "vk/FAS_EziMOTIONPlusR.h"
#include "vk/MOTION_DEFINE.h"
#include "vk/ReturnCodes_Define.h"
enum
{
	LENS_CMD_10X = 0,
	LENS_CMD_20X,
	LENS_CMD_50X,
	LENS_CMD_150X,

};

class CVKRevolver
{
public:
	CVKRevolver(void);
	~CVKRevolver(void);

	BYTE m_nVKPortNum;
	BYTE m_nSlaveNo;

	int m_nPosTablePos[10];
	int m_nNowLensIndex;

	BOOL m_fnVKConnect(BYTE nPortNo, DWORD dwBaud);
	void m_fnVKClose(DWORD dwBaud);
	void m_fnVKServoEnable(BOOL bOnOff);
	void m_fnVKServoAlarmReset();
	void m_fnGetActualPos(long* lActPos);
	void m_fnVKMoveOriginSingleAxis();
	void m_fnVKMoveStop();	
	void m_fnVKPosTableRunItem(WORD wItemNo);
	void m_fnVReadKPosTable();
	int	 m_fnIsLensIndex();

	DWORD m_fnReadState(BYTE nPortNo, BYTE iSlaveNo);
};
