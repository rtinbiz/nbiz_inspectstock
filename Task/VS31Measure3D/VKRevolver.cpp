#include "StdAfx.h"
#include "VKRevolver.h"

CVKRevolver::CVKRevolver(void)
{
	m_nVKPortNum = 0;
	m_nSlaveNo = 5;

	memset(m_nPosTablePos, 0x00, sizeof(int)*10);
	m_nNowLensIndex = LENS_INDEX_ING;
}

CVKRevolver::~CVKRevolver(void)
{

}

BOOL CVKRevolver::m_fnVKConnect(BYTE nPortNo, DWORD dwBaud)
{
	m_nVKPortNum = nPortNo;
	m_nSlaveNo = 5;
	return FAS_Connect(nPortNo, dwBaud);
}

void CVKRevolver::m_fnVKClose(DWORD dwBaud)
{	
	FAS_Close(m_nVKPortNum);
}

void CVKRevolver::m_fnVKServoEnable(BOOL bOnOff)
{
	FAS_ServoEnable(m_nVKPortNum, m_nSlaveNo, bOnOff);
}

void CVKRevolver::m_fnVKServoAlarmReset()
{
	FAS_ServoAlarmReset(m_nVKPortNum, m_nSlaveNo);
}

void CVKRevolver::m_fnGetActualPos(long* lActPos)
{
	//00, 
	FAS_GetActualPos(m_nVKPortNum, m_nSlaveNo, lActPos);
}

void CVKRevolver::m_fnVKMoveOriginSingleAxis()
{
	FAS_MoveOriginSingleAxis(m_nVKPortNum, m_nSlaveNo);
}

void CVKRevolver::m_fnVKMoveStop()
{
	FAS_MoveStop(m_nVKPortNum, m_nSlaveNo);
}

void CVKRevolver::m_fnVKPosTableRunItem(WORD wItemNo)
{
	FAS_PosTableRunItem(m_nVKPortNum, m_nSlaveNo,  wItemNo);
}

void CVKRevolver::m_fnVReadKPosTable()
{
	ITEM_NODE pointNode;

	for(int i=0; i<10; i++)
	{
		FAS_PosTableReadItem(m_nVKPortNum, m_nSlaveNo, i,  &pointNode);
		m_nPosTablePos[i] = pointNode.lPosition; 
	}	
};
DWORD  CVKRevolver::m_fnReadState(BYTE nPortNo, BYTE iSlaveNo)
{
	DWORD dwAxisStatus;
	FAS_GetAxisStatus( nPortNo,  iSlaveNo, &dwAxisStatus);
	return dwAxisStatus;
};
int CVKRevolver::m_fnIsLensIndex()
{
	long nAcPos = 0;
//////////////////////////////////////////
/*	DWORD dwInStatus, dwOutStatus, dwAxisStatus;
	long lCmdPos, lActPos, lPosErr, lActVel;
	WORD wPosItemNo;
	FAS_GetAllStatus(m_nVKPortNum, m_nSlaveNo, &dwInStatus, &dwOutStatus, &dwAxisStatus, &lCmdPos, &lActPos, &lPosErr, &lActVel, &wPosItemNo);
*/
	////////////////////////////////////////////
	m_fnGetActualPos(&nAcPos);

	if(abs(m_nPosTablePos[0] - nAcPos) < 4)
	{
		m_nNowLensIndex = LENS_INDEX_10X;
	}
	/*
	else if(abs(m_nPosTablePos[1] - nAcPos) < 4)
	{
		m_nNowLensIndex = LENS_INDEX_20X;
	}
	else if(abs(m_nPosTablePos[2] - nAcPos) < 4)
	{
		m_nNowLensIndex = LENS_INDEX_50X;
	}
	else if(abs(m_nPosTablePos[3] - nAcPos) < 4)
	{
		m_nNowLensIndex = LENS_INDEX_150X;
	}
	*/
	else if(abs(m_nPosTablePos[4] - nAcPos) < 4)
	{
		m_nNowLensIndex = LENS_INDEX_20X;
	}
	else if(abs(m_nPosTablePos[5] - nAcPos) < 4)
	{
		m_nNowLensIndex = LENS_INDEX_50X;
	}
	else if(abs(m_nPosTablePos[6] - nAcPos) < 4)
	{
		m_nNowLensIndex = LENS_INDEX_150X;
	}
	else
	{
		m_nNowLensIndex = LENS_INDEX_ING;
	}

	return m_nNowLensIndex;
}