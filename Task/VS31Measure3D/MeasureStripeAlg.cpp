/// [2014-11-05] Charles, Added.

#include "StdAfx.h"
#include "MeasureStripeAlg.h"

CMeasureStripeAlg g_iMeasureStripeAlg;

// Stripe 형 측정 알고리즘 Instance 를 반환하는 함수
CMeasureAlg *CMeasureStripeAlg::GetInstance()
{
	return &g_iMeasureStripeAlg;
}

CMeasureStripeAlg::CMeasureStripeAlg()
{
	m_nMaskType = MASK_TYPE_STRIPE;
	m_dParam.SetStripeParam();
}

CMeasureStripeAlg::~CMeasureStripeAlg()
{
}

// Stripe 형 측정 알고리즘을 수행하는 함수.
BOOL CMeasureStripeAlg::Measure()
{
	if(CMeasureAlg::Measure() == FALSE) {
		_PrintDebugString("==== End Measure : FAIL ====");
		return FALSE;
	}

	DWORD nTotalStartTime = _SetStartTime();
	// 참조 데이터를 생성한다.
	BOOL bResult = _GenerateRefData();
	m_dTime.m_msGenerateRefData = _GetElapsedTime();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Generate Ref Data");
		return FALSE;
	}

	// 측정 데이터(MEASURE_DATA) 를 생성하여 추가한다.
	bResult =_AddMeasureData();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Create MeasureData");
		return FALSE;
	}

	_SetStartTime();
	// MinCenterPoints 를 찾는다.
	bResult = _GetMinCenterPoints();
	m_dTime.m_msGetMinCenterPoints = _GetElapsedTime();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Get Min Center Points");
		return FALSE;
	}

	_SetStartTime();
	// 측정 위치의 HeightProfile 을 취득한다.
	bResult = _GetHeightProfile();
	m_dTime.m_msGetMinCenterPoints = _GetElapsedTime();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Get Height Profile");
		return FALSE;
	}

	_SetStartTime();
	// HeightProfile 을 통해 데이터를 측정한다.
	bResult = _MeasureHeightProfile();
	m_dTime.m_msMeasureHeightProfile = _GetElapsedTime();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Measure Height Profile");
		return FALSE;
	}

	_SetStartTime(nTotalStartTime);
	m_dTime.m_msTotalMeasure = _GetElapsedTime();

	_PrintDebugString("> Time to Generate Ref Data : %d ms", m_dTime.m_msGenerateRefData);
	_PrintDebugString("> Time to Find Min Center Points : %d ms", m_dTime.m_msGetMinCenterPoints);
	_PrintDebugString("> Time to Get Height Profile : %d ms", m_dTime.m_msGetMinCenterPoints);
	_PrintDebugString("> Time to Measure Height Profile : %d ms", m_dTime.m_msMeasureHeightProfile);
	_PrintDebugString("> Totlal Time to Measure : %d ms", m_dTime.m_msTotalMeasure);
	_PrintDebugString("==== End Measure : SUCCESS ====");

	return TRUE;
}

// 참조 데이터를 구한다.
// 참조 데이터는 이진화 이미지로써 검사 영역일 경우 1의 값을, 아닐 경우 0 의 값을 가진다.
BOOL CMeasureStripeAlg::_GenerateRefData()
{
	if(CMeasureAlg::_GenerateRefData() == FALSE) {
		return FALSE;
	}

	// 참조 데이터를 생성한다. Stripe 형은 하나의 라인만 참조한다.
	m_dProc.m_pRefImage = cvCreateImage(cvSize(m_dProc.m_nWidth, 1), IPL_DEPTH_8U, 1);
	if(m_dProc.m_pRefImage == 0x00) {
		return FALSE;
	}

	// 분산 Profile 을 위한 버퍼를 생성한다.
	IplImage *pSTDProfile = cvCreateImage(cvSize(m_dProc.m_nWidth, 1), IPL_DEPTH_32F, 1);
	if(pSTDProfile == 0x00) {
		return FALSE;
	}

	// 현재 샘플 영상에 바닥면에 심한 노이즈가 있어서 이를 이용하여 검사 대상 여부를 판단한다.
	// 추후 영상의 상태에 따라 본 알고리즘은 바뀔 가능성이 높음.
	int nSizeX = m_dProc.m_nWidth;
	int nSizeY = m_dProc.m_nHeight;
	UINT32 *pHeightData = (UINT32 *)m_dProc.m_pHeightImage->imageData;
	FLOAT *pSTDData = (FLOAT *)pSTDProfile->imageData;
	// 분산 Profile 을 구하고, Smoothing 을 한다.
	for(int x=0; x<nSizeX; x++) {
		pSTDData[x] = _CalcSTDColumnData(pHeightData, x, nSizeX, nSizeY);
	}
	cvSmooth(pSTDProfile, pSTDProfile, CV_BLUR, m_dParam.m_nGenerateRefDataSmoothFilerSize);

//	_PrintDebugVector(pSTDProfile, 0, nSizeX, "Smooth STDProfile");

	// 분산을 0~1 사이의 값으로 정규화한다.
	FLOAT nMax = _GetMaxData(pSTDData, 0, nSizeX-1);
	for(int x=0; x<nSizeX; x++) {
		pSTDData[x] /= nMax;
	}

//	_PrintDebugVector(pSTDProfile, 0, nSizeX, "Normalized Smooth STDProfile");

	UINT8 *pData = (UINT8 *)m_dProc.m_pRefImage->imageData;
	FLOAT nThreshold = m_dParam.m_nGenerateRefDataThreshold;
	for(int x=0; x<nSizeX; x++) {
		// 분산이 임계치 이하인 경우에만 검사 대상 영역으로 판단하여 1로 설정한다.
		if(pSTDData[x] < nThreshold) {
			pData[x] = 1;
		}
		else {
			pData[x] = 0;
		}
	}
	cvReleaseImage(&pSTDProfile);

//	_PrintDebugVector(m_dProc.m_pRefImage, 0, nSizeX, "REF_IMAGE");

	return TRUE;
}

// MinCenterPoints 생성 함수.
// MinCenterPoints 는 참조 이미지의 최소값을 가진 위치를 의미하며,
// 데이터 측정에 사용되는 HeightProfile 의 시작점과 끝점을 찾는데 활용된다.
BOOL CMeasureStripeAlg::_GetMinCenterPoints()
{
	if(CMeasureAlg::_GetMinCenterPoints() == FALSE) {
		return FALSE;
	}

	// 검사 시작점을 찾는다.
	if(_FindStartCenterPoint() == FALSE) {
		_PrintDebugString("Fail to Find Start Center Point");
		return FALSE;
	}
	_PrintDebugString("Start Center Point : (%d, %d)", _GetCurMeasureData()->m_ptStart.x,  _GetCurMeasureData()->m_ptStart.y);

	// 검사 끝점을 찾는다.
	if(_SelectEndMinCenterPoint() == FALSE) {
		_PrintDebugString("Fail to Find End Center Point");
		return FALSE;
	}
	_PrintDebugString("End Center Point : (%d, %d)", _GetCurMeasureData()->m_ptEnd.x,  _GetCurMeasureData()->m_ptEnd.y);

	return TRUE;
}

// 실제 검사가 가능한 위치를 찾아내는 함수
// Stripe 형은 단일 검사만 지원하므로 본 함수의 내용은 없음.
BOOL CMeasureStripeAlg::_GetMeasurePoints()
{
	// Implement later when stripe type should do multiple measure in a image.
	return TRUE;
}

// 검사 대상 시작 포인트를 찾는다.
BOOL CMeasureStripeAlg::_FindStartCenterPoint()
{
	// 가로(X)축 Line 들의 평균값을 나타내는 HorizMeanLine 생성
	m_dProc.m_pHorizMeanLine = cvCreateImage(cvSize(m_dProc.m_nWidth, 1), IPL_DEPTH_32F, 1);
	if(m_dProc.m_pHorizMeanLine == 0x00) {
		return FALSE;
	}

	// 참조 데이터를 복사하여 Smoothing 을 수행한다.
	int nSizeY = m_dProc.m_nHeight;
	int nSizeX = m_dProc.m_nWidth;
	UINT8 *pSrcData = (UINT8 *)m_dProc.m_pRefImage->imageData;
	FLOAT *pDstData = (FLOAT *)m_dProc.m_pHorizMeanLine->imageData;
	for(int i=0; i<nSizeX; i++) pDstData[i] = (FLOAT)(pSrcData[i]);
	cvSmooth(m_dProc.m_pHorizMeanLine, m_dProc.m_pHorizMeanLine, CV_BLUR, m_dParam.m_nMeanLineSmoothFilerSize);

//	_PrintDebugImage(m_dProc.m_pHorizMeanLine, 0, 0, 10, 1, "HorizMeanLine");

	// HorizMeanLine 에서 최소값을 찾아냄. 이 최소값들이 X 좌표상의 최소값을 가지는 위치를 의미함.
	if(_FindMinCenterPoints(TRUE) == FALSE) {
		_PrintDebugString("Fail to Get Min Center Points");
		return FALSE;
	}

	if(m_dProc.m_dHorizMinCenterPoints.size() < 2) {
		_PrintDebugString("Fail to Find Center Points on Horiz Mean Line. Point Count : %d", m_dProc.m_dHorizMinCenterPoints.size());
		return FALSE;
	}

//	_PrintDebugPoints(m_dProc.m_pHorizMinCenterPoints, m_dProc.m_nCountHorizMinCenterPoints, "HorizMinCenterPoints");

	// 시작 점 X 좌표 확인
	int nStartPosX = 0;
	int nStartPosXIndex = 0;
	if(_SelectStartMinCenterPoint(TRUE, nStartPosX, nStartPosXIndex) == FALSE) {
		return FALSE;
	}

	// 시작 점 Y 좌표 확인. 화면 중앙 위치로 설정함.
	int nStartPosY = cvFloor(nSizeY/2)-1;
	if(nStartPosY < 0) nStartPosY = 0;

	_GetCurMeasureData()->m_ptStart = cvPoint(nStartPosX, nStartPosY);

	return TRUE;
}

// 검사 대상 끝 포인트를 찾는다.
// Stripe 형은 가로 수평 방향만 확인한다.
BOOL CMeasureStripeAlg::_SelectEndMinCenterPoint()
{
	int nStartPosX = _GetCurMeasureData()->m_ptStart.x;
	int nStartPosY = _GetCurMeasureData()->m_ptStart.y;
	int idxStartX = _GetMinCenterPointIndex(nStartPosX, TRUE);

	UINT8 *pRefData = (UINT8 *)m_dProc.m_pRefImage->imageData;
	int nSizeX = m_dProc.m_nWidth;

	int &nPosX = _GetCurMeasureData()->m_ptEnd.x;
	int &nPosY = _GetCurMeasureData()->m_ptEnd.y;
	Points &dHorizMinCenterPoints = m_dProc.m_dHorizMinCenterPoints;
	Points &dVertMinCenterPoints = m_dProc.m_dVertMinCenterPoints;
	int nLengthHorizCenterPoints = dHorizMinCenterPoints.size();
	int nLengthVertCenterPoints = dVertMinCenterPoints.size();

	if(	m_nMeasureAngle == MEASURE_ANGLE_0) {
		nPosY = nStartPosY;

		// right position
		for(int i = idxStartX+1; i< nLengthHorizCenterPoints; i++) {
			nPosX = dHorizMinCenterPoints[i];
			if(pRefData[nPosX] < 1) {
				return TRUE;
			}
		}

		// left position
		for(int i = idxStartX-1; i>=0; i--) {
			nPosX = dHorizMinCenterPoints[i];
			if(pRefData[nPosX] < 1) {
				return TRUE;
			}
		}
	}

	return FALSE;
}

// HeightProfile 을 반환한다.
// 검사 시작점과 끝점을 연결한 라인의 높이 Profile 을 반환한다.
BOOL CMeasureStripeAlg::_GetHeightProfile()
{
	if(CMeasureAlg::_GetHeightProfile() == FALSE) {
		return FALSE;
	}

	MEASURE_DATA *pMeasureData = _GetCurMeasureData();
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	int nStartPosX = pMeasureData->m_ptStart.x;
	int nEndPosX = pMeasureData->m_ptEnd.x;
	int nStartPosY = pMeasureData->m_ptStart.y;
	int nLengthProfile = abs(nEndPosX - nStartPosX)+1;

	// 노이즈 검사를 위해 분산 Profile 을 생성한다.
	IplImage *pSTDProfile = cvCreateImage(cvSize(nLengthProfile, 1), IPL_DEPTH_32F, 1);
	if(pSTDProfile == 0x00) {
		return FALSE;
	}

	int nSizeX = m_dProc.m_nWidth;
	int nSizeY = m_dProc.m_nHeight;
	FLOAT *pSTDData = (FLOAT *)pSTDProfile->imageData;
	UINT32 *pHeightData = (UINT32 *)m_dProc.m_pHeightImage->imageData;
	for(int x=0; x<nLengthProfile; x++) {
		pSTDData[x] = _CalcSTDColumnData(pHeightData, nStartPosX+x, nSizeX, nSizeY);
	}

	// 분산을 0~1 사이의 값으로 정규화한다.
	FLOAT nMax = _GetMaxData(pSTDData, 0, nLengthProfile-1);
	for(int x=0; x<nLengthProfile; x++) {
		pSTDData[x] /= nMax;
	}

	// 양 끝쪽에서 분산이 심한 노이즈 영역을 검사 대상에서 제외시킨다.
	int idxStart = 0;
	for(int i=0; i<nLengthProfile; i++) {
		if(pSTDData[i] < m_dParam.m_nHeightSTDProfileThreshold) {
			break;
		}

		idxStart = i;
	}

	int idxEnd = nLengthProfile - 1;
	for(int i=nLengthProfile-1; i>=0; i--) {
		if(pSTDData[i] < m_dParam.m_nHeightSTDProfileThreshold) {
			break;
		}

		idxEnd = i;
	}

	nLengthProfile = abs(idxEnd - idxStart) + 1;
	// HeightProfile 를 생성한다.
	pMeasureData->m_pHeightProfile = cvCreateImage(cvSize(nLengthProfile, 1), IPL_DEPTH_32F, 1);
	if(pMeasureData->m_pHeightProfile == 0x00) {
		return FALSE;
	}
	FLOAT *pProfileData = (FLOAT *)pMeasureData->m_pHeightProfile->imageData;
	for(int x=0; x<nLengthProfile; x++) {
		pProfileData[x] = (FLOAT)pHeightData[nStartPosY*nSizeX+nStartPosX+idxStart+x];
	}

//	_PrintDebugVector(pMeasureData->m_pHeightProfile, 0, 10, "pProfileData");

	return TRUE;
}

// 결과 데이터 측정 함수. Base 함수의 내용이 그대로 Stripe 형에 적용된다.
BOOL CMeasureStripeAlg::_MeasureHeightProfile()
{
	if(CMeasureAlg::_MeasureHeightProfile() == FALSE) {
		return FALSE;
	}

	return TRUE;
}

// 측정 결과 데이터(MEASURE_RESULT_DATA)를 생성한다.
// Stripe 형의 경우, TargetThick 파라미터값을 이용하여 전체 높이를 구한다.
BOOL CMeasureStripeAlg::_MakeResultData()
{
	if(CMeasureAlg::_MakeResultData() == FALSE) {
		return FALSE;
	}

	MEASURE_RESULT_DATA *pMeasureResultData = _GetCurMeasureResultData();
	if(pMeasureResultData == 0x00) {
		return FALSE;
	}

	pMeasureResultData->RIB_HEIGHT = (int)m_dInput.m_nTargetThick;
	pMeasureResultData->RIB_HEIGHT_TOTAL_L = pMeasureResultData->RIB_HEIGHT;
	pMeasureResultData->RIB_HEIGHT_TOTAL_R = pMeasureResultData->RIB_HEIGHT;
	pMeasureResultData->RIB_HEIGHT_E_L = pMeasureResultData->RIB_HEIGHT - pMeasureResultData->RIB_HEIGHT_L;
	pMeasureResultData->RIB_HEIGHT_E_R = pMeasureResultData->RIB_HEIGHT - pMeasureResultData->RIB_HEIGHT_R;

	pMeasureResultData->Validate();

	return TRUE;
}

// 특정 Column 에 대한 분산을 구한다. (분산은 표준편차값으로 대체하여 사용한다.)
FLOAT CMeasureStripeAlg::_CalcSTDColumnData(UINT32 *pData, int idxColumn, int nSizeX, int nSizeY)
{
	if(pData == 0x00) {
		return 0;
	}

	if(nSizeX <=0 || nSizeY <=0) {
		return 0;
	}

	FLOAT nMean=0;
	for(int i=0; i<nSizeY; i++) {
		nMean += pData[i*nSizeX+idxColumn];
	}
	nMean /= nSizeY;

	FLOAT nVar=0;
	for(int i=0; i<nSizeY; i++) {
		nVar += powf(pData[i*nSizeX+idxColumn] - nMean, 2);
	}
	if(nSizeY>1) {
		nVar /= (nSizeY - 1);
	}
	else {
		nVar /= nSizeY;
	}

	return sqrtf(nVar);
}

// 결과 이미지 생성 함수. Base 함수의 내용이 그대로 Stripe 형에 적용된다.
BOOL CMeasureStripeAlg::MakeResultImage()
{
	if(CMeasureAlg::MakeResultImage() == FALSE) {
		return FALSE;
	}

	return TRUE;
}

// 결과 전체 이미지 생성 함수.
BOOL CMeasureStripeAlg::_MakeResultFullImage()
{
	if(CMeasureAlg::_MakeResultFullImage() == FALSE) {
		return FALSE;
	}

	int nCount = m_dProc.m_dMeasure.size();
	if(nCount == 0) {
		return FALSE;
	}

	if(m_bSaveAnalysisData) {
		// 분석용 결과 이미지일 경우 별도의 배경 이미지를 로드.
		m_dOutput.m_pDataImage = cvLoadImage(RESULT_VIEW_STICK_IMG_FOR_ANALYSIS);
	}
	else {
		m_dOutput.m_pDataImage = cvLoadImage(RESULT_VIEW_STICK_IMG);
	}
	if(m_dOutput.m_pDataImage == 0x00) {
		return FALSE;
	}

	int nSizeX = m_dOutput.m_pDataImage->width * 2;
	int nSizeY = m_dOutput.m_pDataImage->height * nCount;
	m_dOutput.m_pFullImage = cvCreateImage(cvSize(nSizeX, nSizeY), IPL_DEPTH_8U, 3);
	memset(m_dOutput.m_pFullImage->imageData, 0xFF, m_dOutput.m_pFullImage->imageSize);

	return TRUE;
}

// 높이 이미지를 생성하여 전체 이미지에 통합한다.
BOOL CMeasureStripeAlg::_MakeResultHeightImage()
{
	if(CMeasureAlg::_MakeResultHeightImage() == FALSE) {
		return FALSE;
	}

	int nSizeX = m_dOutput.m_pDataImage->width;
	int nSizeY = m_dOutput.m_pDataImage->height;
	CvRect rcROI;
	rcROI.x = 0; 
	rcROI.y = 0; 
	rcROI.width = nSizeX; 
	rcROI.height = nSizeY; 

	CvPoint ptStart;
	CvPoint ptEnd;
	float  nRatioX = (float)nSizeX / m_dOutput.m_pHeightImage->width;
	float  nRatioY = (float)nSizeY / m_dOutput.m_pHeightImage->height;

	UINT nCount = m_dProc.m_dMeasure.size();
	MEASURE_DATA *pMeasureData = 0x00;
	for(UINT i=0; i<nCount; i++) {
		pMeasureData = _GetMeasureData(i);
		if(pMeasureData == 0x00) {
			continue;
		}

		ptStart.x = (int)(pMeasureData->m_ptStart.x *nRatioX);
		ptStart.y = (int)(pMeasureData->m_ptStart.y * nRatioY);
		ptEnd.x = (int)(pMeasureData->m_ptEnd.x * nRatioX);
		ptEnd.y = (int)(pMeasureData->m_ptEnd.y * nRatioY);

		rcROI.y = i*nSizeY; 
		cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
		cvResize(m_dOutput.m_pHeightImage, m_dOutput.m_pFullImage);

		cvLine(m_dOutput.m_pFullImage, ptStart, ptEnd, HEIGHT_LINE_HORIZ_COLOR, 2);

		cvDrawRect(m_dOutput.m_pFullImage, cvPoint(0, 0),cvPoint(rcROI.width,rcROI.height), RESULT_DATA_BOX_LINE_COLOR, 2);

		cvResetImageROI(m_dOutput.m_pFullImage);
	}

	return TRUE;
}

// 결과 데이터 이미지 부분을 생성하여 전체 이미지에 통합한다.
BOOL CMeasureStripeAlg::_MakeResultDataImage()
{
	if(CMeasureAlg::_MakeResultDataImage() == FALSE) {
		return FALSE;
	}

	int nCount = m_dProc.m_dMeasure.size();
	if(nCount == 0) {
		return FALSE;
	}

	if(m_dOutput.m_pDataImage == 0x00) {
		return FALSE;
	}

	char szPrintMessage[256];
	double arrDisplayData[11] = {0.0,};

	CvPoint ptDrawDataPos[11];
	ptDrawDataPos[0] = cvPoint(318,83);//1)
	ptDrawDataPos[1] = cvPoint(325,500);//2)
	ptDrawDataPos[2] = cvPoint(208,468);//3)
	ptDrawDataPos[3] = cvPoint(440,468);//4)
	ptDrawDataPos[4] = cvPoint(88,302);//5)
	ptDrawDataPos[5] = cvPoint(98,136);//6)
	ptDrawDataPos[6] = cvPoint(597,302);//7)
	ptDrawDataPos[7] = cvPoint(606,136);//8)
	ptDrawDataPos[8] = cvPoint(190,180);//9)
	ptDrawDataPos[9] = cvPoint(450,180);//10)
	ptDrawDataPos[10] = cvPoint(37,263);//11)

	int nSizeX = m_dOutput.m_pDataImage->width;
	int nSizeY = m_dOutput.m_pDataImage->height;
	CvRect rcROI;
	rcROI.x = nSizeX; 
	rcROI.y = 0; 
	rcROI.width = nSizeX; 
	rcROI.height = nSizeY; 

	MEASURE_RESULT_DATA *pMeasureResultData = 0x00;
	for(int i=0; i<nCount; i++) {
		pMeasureResultData = _GetMeasureResultData(i);
		if(pMeasureResultData == 0x00) {
			continue;
		}

		int nDataIndex  =0;
		//1번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH/1000.0;
		//2번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_C/1000.0;
		//3번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_L/1000.0;
		//4번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_R/1000.0;
		//5번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_L/1000.0;
		//6번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_E_L/1000.0;
		//7번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_R/1000.0;
		//8번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_E_R/1000.0;
		//9번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_ANGLE_L;
		//10번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_ANGLE_R;
		//11번 거리
		arrDisplayData[nDataIndex++] = (pMeasureResultData->RIB_HEIGHT/1000.0);

		rcROI.y = i*nSizeY; 
		cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
		cvCopy(m_dOutput.m_pDataImage, m_dOutput.m_pFullImage);

		for(int nDrawStep = 0; nDrawStep<11; nDrawStep++) {
			if(nDrawStep == 8 || nDrawStep ==9) {
				sprintf_s(szPrintMessage, "(%d) %0.2f' ",(nDrawStep+1), arrDisplayData[nDrawStep]);
			} else {
				sprintf_s(szPrintMessage, "(%d) %0.2fum",(nDrawStep+1), arrDisplayData[nDrawStep]);
			}

			PutImage_Text(m_dOutput.m_pFullImage, ptDrawDataPos[nDrawStep], szPrintMessage);
		}

		cvDrawRect(m_dOutput.m_pFullImage, cvPoint(0, 0),cvPoint(rcROI.width,rcROI.height), RESULT_DATA_BOX_LINE_COLOR, 2);

		cvResetImageROI(m_dOutput.m_pFullImage);
	}

	return TRUE;
}

// 분석용 결과 데이터 이미지 부분을 생성하여 전체 이미지에 통합한다.
BOOL CMeasureStripeAlg::_MakeAnalysisResultDataImage()
{
	if(CMeasureAlg::_MakeAnalysisResultDataImage() == FALSE) {
		return FALSE;
	}

	int nCount = m_dProc.m_dMeasure.size();
	if(nCount == 0) {
		return FALSE;
	}

	if(m_dOutput.m_pDataImage == 0x00) {
		return FALSE;
	}

	char szPrintMessage[256];
	double arrDisplayData[11] = {0.0,};

	CvPoint ptDrawDataPos[11];
	ptDrawDataPos[0] = cvPoint(318,83);//1)
	ptDrawDataPos[1] = cvPoint(325,500);//2)
	ptDrawDataPos[2] = cvPoint(208,468);//3)
	ptDrawDataPos[3] = cvPoint(440,468);//4)
	ptDrawDataPos[4] = cvPoint(88,302);//5)
	ptDrawDataPos[5] = cvPoint(98,136);//6)
	ptDrawDataPos[6] = cvPoint(597,302);//7)
	ptDrawDataPos[7] = cvPoint(606,136);//8)
	ptDrawDataPos[8] = cvPoint(140,180);//9)
	ptDrawDataPos[9] = cvPoint(550,180);//10)
	ptDrawDataPos[10] = cvPoint(37,263);//11)

	int nSizeX = m_dOutput.m_pDataImage->width;
	int nSizeY = m_dOutput.m_pDataImage->height;
	CvRect rcROI;
	CvRect rcView;
	rcROI.x = nSizeX; 
	rcROI.y = 0; 
	rcROI.width = nSizeX; 
	rcROI.height = nSizeY; 

	MEASURE_DATA *pMeasureData = 0x00;
	MEASURE_RESULT_DATA *pMeasureResultData = 0x00;
	for(int i=0; i<nCount; i++) {
		pMeasureData = _GetMeasureData(i);
		pMeasureResultData = _GetMeasureResultData(i);
		if(pMeasureData == 0x00 || pMeasureResultData == 0x00) {
			continue;
		}

		int nDataIndex  =0;
		//1번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH/1000.0;
		//2번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_C/1000.0;
		//3번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_L/1000.0;
		//4번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_R/1000.0;
		//5번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_L/1000.0;
		//6번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_E_L/1000.0;
		//7번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_R/1000.0;
		//8번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_E_R/1000.0;
		//9번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_ANGLE_L;
		//10번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_ANGLE_R;
		//11번 거리
		arrDisplayData[nDataIndex++] = (pMeasureResultData->RIB_HEIGHT/1000.0);

		rcROI.y = i*nSizeY; 
		cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
		cvCopy(m_dOutput.m_pDataImage, m_dOutput.m_pFullImage);

		rcView = rcROI;
		int nReduceWidth = (int)(rcView.width * 0.2f);
		int nReduceHeight = (int)(rcView.height * 0.2f);
		rcView.x += nReduceWidth;
		rcView.y += nReduceHeight;
		rcView.width -= (2*nReduceWidth);
		rcView.height -= (2*nReduceHeight);
		// HeightProfile 을 그린다.
		_DrawHeightProfile(m_dOutput.m_pFullImage, rcView, pMeasureData);

		for(int nDrawStep = 0; nDrawStep<11; nDrawStep++) {
			if(nDrawStep == 8 || nDrawStep ==9) {
				sprintf_s(szPrintMessage, "(%d) %0.2f' ",(nDrawStep+1), arrDisplayData[nDrawStep]);
			} else {
				sprintf_s(szPrintMessage, "(%d) %0.2fum",(nDrawStep+1), arrDisplayData[nDrawStep]);
			}

			PutImage_Text(m_dOutput.m_pFullImage, ptDrawDataPos[nDrawStep], szPrintMessage);
		}

		cvDrawRect(m_dOutput.m_pFullImage, cvPoint(0, 0),cvPoint(rcROI.width,rcROI.height), RESULT_DATA_BOX_LINE_COLOR, 2);
		cvResetImageROI(m_dOutput.m_pFullImage);
	}

	return TRUE;
}
