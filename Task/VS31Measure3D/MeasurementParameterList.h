#pragma once


#pragma once
#include "afxwin.h"
#include "VkViewerRemoteOperation.h"
#include "VkViewerRemoteOperationCompatible.h"



class CMeasurementParameterList : public CDialog
{
	DECLARE_DYNAMIC(CMeasurementParameterList)

public:
	CMeasurementParameterList(MeasurementParameter* parameter, CWnd* pParent = NULL);
	CMeasurementParameterList(MeasurementParameter2* parameter, CWnd* pParent = NULL);
	virtual ~CMeasurementParameterList();


	enum { IDD = IDD_MEASUREMENTPARMETERLIST_DLG};

private:
	void SetMeasurementParameter(MeasurementParameter* pParameter);	
	void SetMeasurementParameter2(MeasurementParameter2* pParameter);
	void SetMeasurementMode(MeasurementMode mode);
	void SetMeasurementQuality(MeasurementQuality quality);
	void SetNDFIlter(long lFilterIndex);
	void SetShutterSpeedMode(BOOL bMode);
	void SetRPD(BOOL bRPD);
	void SetRpdMode(RpdPitch rpdPitch);
	void SetRpdPreference(RpdPreference rpdPreference);
	void SetOpticalZoom(ZoomIndex zoom);
	void SetCalibration(CStatic *label, double dbCalibration);
	void SetParameter(CStatic *label, long lParameter);
	void SetUmParameter(CStatic *label, long lParameter);
	void SetEnableColorAcquisition(BOOL bEnableColorAcquisition);
	void SetThicknessParameter(BOOL bParameter);
	void SetLayerAdjustFilter(LayerAdjustFilter filter);
	void SetPeakDetectParam(PeakDetectParam param);	
public:
	//static void strSetMeasurementParameter(char *pStrData, MeasurementParameter* pParameter);	
	//static void strSetMeasurementParameter2(char *pStrData, MeasurementParameter2* pParameter);
	static void strSetMeasurementMode(char *pStrData, MeasurementMode mode);
	static void strSetMeasurementQuality(char *pStrData, MeasurementQuality quality);
	static void strSetNDFIlter(char *pStrData, long lFilterIndex);
	static void strSetShutterSpeedMode(char *pStrData, BOOL bMode);
	static void strSetRPD(char *pStrData, BOOL bRPD);
	static void strSetRpdMode(char *pStrData, RpdPitch rpdPitch);
	static void strSetRpdPreference(char *pStrData, RpdPreference rpdPreference);
	static void strSetOpticalZoom(char *pStrData, ZoomIndex zoom);
	static void strSetCalibration(char *pStrData, double dbCalibration);
	static void strSetParameter(char *pStrData, long lParameter);
	static void strSetUmParameter(char *pStrData,  long lParameter);
	static void strSetEnableColorAcquisition(char *pStrData, BOOL bEnableColorAcquisition);
	static void strSetThicknessParameter(char *pStrData, BOOL bParameter);
	static void strSetLayerAdjustFilter(char *pStrData, LayerAdjustFilter filter);
	static void strSetPeakDetectParam(char *pStrData, PeakDetectParam param);	
protected:
	virtual void DoDataExchange(CDataExchange* pDX); 

	DECLARE_MESSAGE_MAP()
private:
	CStatic m_labelMeasurementMode;
	CStatic m_labelMeasurementQuality;
	CStatic m_labelUpperPosition;
	CStatic m_labelLowerPosition;
	CStatic m_labelPitch;
	CStatic m_labelDistance;
	CStatic m_labelNDFilter;
	CStatic m_labelGain;
	CStatic m_labelRPD;
	CStatic m_labelShutterSpeed;
	CStatic m_labelShutterSpeedMode;
	CStatic m_labelLine1Position;
	CStatic m_labelLine2Position;
	CStatic m_labelLine3Position;
	CStatic m_labelLineCount;
	CStatic m_labelOpticalZoom;
	CStatic m_labelXYCalibration;
	CStatic m_labelZCalibration;
	CStatic m_labelRpdPreference;
	CStatic m_labelEnableColorAcquisition;
	CStatic m_labelThicknessParam;
	CStatic m_labelLayerAdjustFilter;
	CStatic m_labelThickness;
	CStatic m_labelPeakDetectParam;
	CStatic m_labelShutterSpeed2;
	MeasurementParameter* m_pParameter;
	MeasurementParameter2* m_pParameter2;
public:
	CStatic m_labelGain2;
	virtual BOOL OnInitDialog();
};
