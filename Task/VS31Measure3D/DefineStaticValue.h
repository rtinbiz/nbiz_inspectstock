#pragma once

#include "VkViewerRemoteOperationCompatible.h"
//입력되는 Task Number에 따라 Server Interface File이 바뀐다.
//#define  MSG_WIN_TASK_INI_FILE_FULL_NAME_TN27 "\\Config\\3DM_APP_T27.DAT"
//Client DLL 에서 Callback 되어질 때 사용되는 이벤트
#define  WM_USER_CALLBACK				 (WM_USER + 1000)





#pragma comment (lib, "D:\\nBiz_InspectStock\\CommonDLL32\\nBizMathLib.lib")

#pragma pack(1)

typedef struct
{
	struct
	{
		int YY;
		int MM;
		int DD;
		int HH;
		int Min;
		int Sec;
	}MeasurTime;

	int ZMeasurDis_um;
	int Pitch_um;
	int OpticalZoom;
	
	int ObjectiveLens;
	int LaserGain1;
	
	int WhiteBalaceR;
	int WhiteBalaceB;

	int XCalibration_nmPerPixel;
	int YCalibration_nmPerPixel;
	int ZCalibration_nmPerDigit;

	int NA;

	int UpperPosition;
	int LowerPosition;

	struct
	{
		int ImgWidth;
		int ImgHeight;
		int ImgBitDept;
		int ImgSize;
	}IMG_Info[4];

}VK4_FILEINFO;
#pragma pack()

extern "C" __declspec(dllexport) VK4_FILEINFO  _stdcall  nBizM_Vk4FileLoad(BYTE * pFileData, int nReadSize);
extern "C" __declspec(dllexport) BOOL  _stdcall  nBizM_Vk4GetImgData(BYTE*pCImg, BYTE*pCLImg, BYTE*pLImg, BYTE*pDImg );
typedef struct RGB_V_TAG
{
	BYTE B;
	BYTE G;
	BYTE R;
}RGB_V;

typedef struct FORMAT_MEASER_DATA_TAG
{
	
	MeasurementParameter2 m_MeasureParam;

	int		m_Height;				//Pixel 단위
	int		m_Width;				//Pixel 단위.
	int		m_TargetThicknessNano;	//Nano 단위.

	int		m_ZDepth;				//3D 용.

	BYTE	*m_pColorData;

	long	*m_pDepthData;

	
	FORMAT_MEASER_DATA_TAG()
	{

		memset(&m_MeasureParam, 0x00, sizeof(MeasurementParameter2));
		m_Height	= 0;
		m_Width		= 0;
		m_TargetThicknessNano = 0;

		m_ZDepth	= 0;

		m_pColorData	= NULL;
		m_pDepthData	= NULL;
	}
	FORMAT_MEASER_DATA_TAG(	MeasurementParameter2 *pMeasureParam, int nWidth, int nHeight, int TargetThickness)
	{
		if(pMeasureParam != NULL)
			memcpy(&m_MeasureParam, pMeasureParam, sizeof(MeasurementParameter2));
		m_Height	= nHeight;
		m_Width		= nWidth;
		
		m_TargetThicknessNano = TargetThickness;

		m_pColorData	= new BYTE[nHeight*nWidth*3];
		m_pDepthData	= new long[nHeight*nWidth];
	}
	~FORMAT_MEASER_DATA_TAG()
	{
		if(m_pColorData != NULL)
			delete [] m_pColorData;
		m_pColorData = NULL;

		if(m_pDepthData != NULL)
			delete [] m_pDepthData;
		m_pDepthData = NULL;
	}
	void CreateStorage(MeasurementParameter2 *pMeasureParam, int nWidth, int nHeight, int TargetThickness)
	{
		if(pMeasureParam != NULL)
			memcpy(&m_MeasureParam, pMeasureParam, sizeof(MeasurementParameter2));

		m_Height	= nHeight;
		m_Width		= nWidth;

		m_TargetThicknessNano = TargetThickness;

		if(m_pColorData != NULL)
			delete [] m_pColorData;
		m_pColorData	= new BYTE[nHeight*nWidth*3];

		if(m_pDepthData != NULL)
			delete [] m_pDepthData;
		m_pDepthData	= new long[nHeight*nWidth];
	}

	void CreateStorageByFileData(BYTE *pFileData, int SizeFile)
	{
//#ifdef FILE_FORMAT_VK4
		memset(&m_MeasureParam, 0x00, sizeof(MeasurementParameter2));

		VK4_FILEINFO ReadVK4FileFormat;
		ReadVK4FileFormat = nBizM_Vk4FileLoad(pFileData, SizeFile);
		m_Height	= ReadVK4FileFormat.IMG_Info[0].ImgHeight;
		m_Width		= ReadVK4FileFormat.IMG_Info[0].ImgWidth;

		if(m_pColorData != NULL)
			delete [] m_pColorData;
		m_pColorData	= new BYTE[m_Height*m_Width*3];

		if(m_pDepthData != NULL)
			delete [] m_pDepthData;
		m_pDepthData	= new long[m_Height*m_Width];

		nBizM_Vk4GetImgData(NULL, (BYTE*)m_pColorData, NULL, (BYTE*)m_pDepthData);

		m_MeasureParam.lUpperPosition	= ReadVK4FileFormat.UpperPosition;
		m_MeasureParam.lLowerPosition	= ReadVK4FileFormat.LowerPosition;
		m_MeasureParam.lNd					= ReadVK4FileFormat.NA;
		m_MeasureParam.lPitch				= ReadVK4FileFormat.Pitch_um;
		m_MeasureParam.dXYCalibration	= (ReadVK4FileFormat.XCalibration_nmPerPixel/1000000.0);
		m_MeasureParam.lLaserGain1		= ReadVK4FileFormat.LaserGain1;
		m_TargetThicknessNano			= ReadVK4FileFormat.ZMeasurDis_um;


		m_MeasureParam.dZCalibrationFactor = HeightDataType_Unit100Picometer;

//#else
//		memcpy(&m_MeasureParam, pFileData, sizeof(MeasurementParameter2));
//
//		m_Height				= *(int*)(pFileData + sizeof(MeasurementParameter2));
//		m_Width					= *(int*)(pFileData + sizeof(MeasurementParameter2) + sizeof(int));
//		m_TargetThicknessNano	= *(int*)(pFileData + sizeof(MeasurementParameter2) + (sizeof(int)*2));
//
//		BYTE *pImagePos = pFileData+(sizeof(int)*3 + sizeof(MeasurementParameter2));
//		BYTE *pDepthPos = pFileData+(sizeof(int)*3 + (m_Height*m_Width*3) + sizeof(MeasurementParameter2));
//		
//		if(m_pColorData != NULL)
//			delete [] m_pColorData;
//		m_pColorData	= new BYTE[m_Height*m_Width*3];
//		memcpy(m_pColorData, pImagePos, m_Height*m_Width*3*sizeof(BYTE));
//
//		if(m_pDepthData != NULL)
//			delete [] m_pDepthData;
//		m_pDepthData	= new long[m_Height*m_Width];
//		memcpy(m_pDepthData, pDepthPos, m_Height*m_Width*sizeof(long));
//#endif
//
		//변환(100pico -> Nano)
		if(m_MeasureParam.dZCalibrationFactor == HeightDataType_Unit100Picometer)//100Pico단위 Data
		{//Pico단위이면 nano 단위로 변환 하자.
			for(int i=0; i<(m_Width*m_Height); i++)
			{
				*(m_pDepthData+i) = (UINT)(*(m_pDepthData+i)*0.1);
			}
		}


		m_MeasureParam.dZCalibrationFactor = HeightDataType_Unit1Nanometer;
		//Z축 높이 Display를 위해 영역 검사. (Find Max Depth)
		int NowValue_Z = 0;
		m_ZDepth = 0;
		for(int i=0; i<(m_Width*m_Height); i++)
		{
			NowValue_Z = *(m_pDepthData+i);

			if(m_ZDepth<NowValue_Z)
				m_ZDepth = NowValue_Z;
		}
	}

	void SetData(BYTE *pColorData, long *pDepthData)
	{
		if(pColorData == NULL || pDepthData== NULL)
			return;

		if(m_pColorData == NULL || m_pDepthData== NULL)
			return;

		memcpy(m_pColorData, pColorData, m_Height*m_Width*3*sizeof(BYTE));
		memcpy(m_pDepthData, pDepthData, m_Height*m_Width*sizeof(long));


		//변환(100pico -> Nano)
		if(m_MeasureParam.dZCalibrationFactor == HeightDataType_Unit100Picometer)//100Pico단위 Data
		{//Pico단위이면 nano 단위로 변환 하자.
			for(int i=0; i<(m_Width*m_Height); i++)
			{
				*(m_pDepthData+i) = (long)(*(m_pDepthData+i)*0.1);
			}
		}

		m_MeasureParam.dZCalibrationFactor = HeightDataType_Unit1Nanometer;

		//Z축 높이 Display를 위해 영역 검사. (Find Max Depth)
		int NowValue_Z = 0;
		for(int i=0; i<(m_Width*m_Height); i++)
		{
			NowValue_Z = *(m_pDepthData+i);

			if(m_ZDepth<NowValue_Z)
				m_ZDepth = NowValue_Z;
		}
	}

	BOOL WriteFileData(char *pSavePath)
	{
		if(!(m_Width>0 && m_Width<=2048  && 
			m_Height>0 && m_Height<=1536 &&
			m_pColorData != NULL && m_pDepthData != NULL))
			return FALSE;

		CFile WriteFileObj;

		if(WriteFileObj.m_hFile != INVALID_HANDLE_VALUE)
			WriteFileObj.Close();

		WriteFileObj.m_hFile = INVALID_HANDLE_VALUE;

		if( !WriteFileObj.Open( pSavePath, CFile::modeCreate| CFile::typeBinary/*| CFile::modeNoTruncate */
			| CFile::shareDenyWrite | CFile::modeWrite))
		{
			WriteFileObj.m_hFile = INVALID_HANDLE_VALUE;
			return FALSE;
		}
		WriteFileObj.SeekToBegin();

		CString WritFileBuffer;
		try
		{
			CArchive ArchProcObj(&WriteFileObj, CArchive::store);

			ArchProcObj.Write((void*)&m_MeasureParam, sizeof(MeasurementParameter2));

			ArchProcObj.Write((void*)&m_Height, sizeof(int));
			ArchProcObj.Write((void*)&m_Width, sizeof(int));
			ArchProcObj.Write((void*)&m_TargetThicknessNano, sizeof(int));

			ArchProcObj.Write((void*)m_pColorData, m_Height*m_Width*3);
			ArchProcObj.Write((void*)m_pDepthData, m_Height*m_Width*sizeof(long));
			ArchProcObj.Close();
		}
		catch (CException* pe)
		{
			pe = pe;
			WriteFileObj.Close();
			WriteFileObj.m_hFile = INVALID_HANDLE_VALUE;
			return FALSE;
		}
		if(WriteFileObj.m_hFile != INVALID_HANDLE_VALUE)
			WriteFileObj.Close();
		return TRUE;
	}


}FORMAT_MEASER_DATA, DATA_3D_RAW;


typedef struct MEASER_2D_DATA_TRG
{
	long LineIndex;

	long MaxValueX;
	long MaxValueY;

	//급증부분.
	long LeftMinValueX;
	long LeftMinValueY;

	long RightMinValueX;
	long RightMinValueY;


	//급감부분
	long LeftDnValueX;
	long LeftDnValueY;

	long RightDnValueX;
	long RightDnValueY;

	//Dot Type Data
	long LeftAngleValueX;
	long LeftAngleValueY;

	long RightAngleValueX;
	long RightAngleValueY;

	MEASER_2D_DATA_TRG()
	{
		LineIndex = (-1);
		MaxValueX		= (-1);
		MaxValueY		= (-1);

		LeftMinValueX	= (-1);
		LeftMinValueY	= (-1);

		RightMinValueX	= (-1);
		RightMinValueY	= (-1);

		LeftDnValueX	= (-1);
		LeftDnValueY	= (-1);

		RightDnValueX	= (-1);
		RightDnValueY	= (-1);


		LeftAngleValueX	= (-1);
		LeftAngleValueY	= (-1);

		RightAngleValueX	= (-1);
		RightAngleValueY	= (-1);
	}

} MEASER_2D, *LPMEASER_2D;

