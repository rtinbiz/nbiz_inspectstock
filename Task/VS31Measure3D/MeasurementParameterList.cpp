// MeasurementParameterList.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS31Measure3D.h"
#include "MeasurementParameterList.h"


// MeasurementParameterList.cpp
//

IMPLEMENT_DYNAMIC(CMeasurementParameterList, CDialog)
CMeasurementParameterList::CMeasurementParameterList(MeasurementParameter* parameter, CWnd* pParent /*=NULL*/)
: CDialog(CMeasurementParameterList::IDD, pParent)
{
	m_pParameter2 = NULL;
	m_pParameter = parameter;
}

CMeasurementParameterList::CMeasurementParameterList(MeasurementParameter2* parameter, CWnd* pParent /*=NULL*/)
: CDialog(CMeasurementParameterList::IDD, pParent)
{
	m_pParameter = NULL;
	m_pParameter2 = parameter;
}

CMeasurementParameterList::~CMeasurementParameterList()
{
}

void CMeasurementParameterList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_MEASUREMENTMODE, m_labelMeasurementMode);
	DDX_Control(pDX, IDC_STATIC_MEASUREMENTQUALITY, m_labelMeasurementQuality);
	DDX_Control(pDX, IDC_STATIC_UPPERPOSITION, m_labelUpperPosition);
	DDX_Control(pDX, IDC_STATIC_LOWERPOSITION, m_labelLowerPosition);
	DDX_Control(pDX, IDC_STATIC_PITCH, m_labelPitch);
	DDX_Control(pDX, IDC_STATIC_DISTANCE, m_labelDistance);
	DDX_Control(pDX, IDC_STATIC_NDFILTER, m_labelNDFilter);
	DDX_Control(pDX, IDC_STATIC_GAIN2, m_labelGain);
	DDX_Control(pDX, IDC_STATIC_RPD, m_labelRPD);
	DDX_Control(pDX, IDC_STATIC_SHUTTERSPEED2, m_labelShutterSpeed);
	DDX_Control(pDX, IDC_STATIC_SHUTTERSPEEDMODE2, m_labelShutterSpeedMode);
	DDX_Control(pDX, IDC_STATIC_LINE1POS2, m_labelLine1Position);
	DDX_Control(pDX, IDC_STATIC_LINE2POS2, m_labelLine2Position);
	DDX_Control(pDX, IDC_STATIC_LINE3POS2, m_labelLine3Position);
	DDX_Control(pDX, IDC_STATIC_LINECOUNT2, m_labelLineCount);
	DDX_Control(pDX, IDC_STATIC_OPTICALZOOM2, m_labelOpticalZoom);
	DDX_Control(pDX, IDC_STATIC_XYCALIBRATION, m_labelXYCalibration);
	DDX_Control(pDX, IDC_STATIC_ZCALIBRATION, m_labelZCalibration);
	DDX_Control(pDX, IDC_STATIC_RPDPREFERENCE2, m_labelRpdPreference);
	DDX_Control(pDX, IDC_STATIC_ENABLECOLORACQUISITION, m_labelEnableColorAcquisition);
	DDX_Control(pDX, IDC_STATIC_THICKNESSPARAMETER, m_labelThicknessParam);
	DDX_Control(pDX, IDC_STATIC_LAYERADJUSTFILTER, m_labelLayerAdjustFilter);
	DDX_Control(pDX, IDC_STATIC_THICKNESS, m_labelThickness);
	DDX_Control(pDX, IDC_STATIC_PEAKDETECTPARAM, m_labelPeakDetectParam);
	DDX_Control(pDX, IDC_STATIC_SHUTTERSPEED3, m_labelShutterSpeed2);
	DDX_Control(pDX, IDC_STATIC_GAIN3, m_labelGain2);
}

BOOL CMeasurementParameterList::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetMeasurementParameter(m_pParameter);
	SetMeasurementParameter2(m_pParameter2);

	return TRUE;
}

void CMeasurementParameterList::SetMeasurementParameter(MeasurementParameter* pParameter)
{
	if(pParameter == NULL) return;

	SetMeasurementMode(pParameter->mode);
	SetMeasurementQuality(pParameter->quality);
	SetNDFIlter(pParameter->lNd);
	SetShutterSpeedMode(pParameter->bIsShutterSpeedAuto);
	SetRPD(pParameter->bIsRpd);
	SetRpdPreference(pParameter->rpdPreference);
	SetOpticalZoom(pParameter->zoom);
	SetEnableColorAcquisition(pParameter->bEnableColorAcquisition);
	SetThicknessParameter(pParameter->bThicknessParameterAuto);
	SetLayerAdjustFilter(pParameter->filter);
	SetPeakDetectParam(pParameter->peakDetectParam);


	SetCalibration(&m_labelXYCalibration, pParameter->dXYCalibration);
	SetCalibration(&m_labelZCalibration, pParameter->dZCalibration);
	SetUmParameter(&m_labelUpperPosition, pParameter->lUpperPosition);
	SetUmParameter(&m_labelLowerPosition, pParameter->lLowerPosition);
	SetUmParameter(&m_labelDistance, pParameter->lDistance);
	SetUmParameter(&m_labelPitch, pParameter->lPitch);
	SetUmParameter(&m_labelThickness, pParameter->lThickness);

	SetParameter(&m_labelGain, pParameter->lGain);
	SetParameter(&m_labelShutterSpeed, pParameter->lShutterSpeed);
	SetParameter(&m_labelLineCount, pParameter->lLineNumber);
	SetParameter(&m_labelLine1Position, pParameter->lLinePosition[0]);
	SetParameter(&m_labelLine2Position, pParameter->lLinePosition[1]);
	SetParameter(&m_labelLine3Position, pParameter->lLinePosition[2]);
}

void CMeasurementParameterList::SetMeasurementParameter2(MeasurementParameter2* pParameter)
{
	if(pParameter == NULL) return;

	SetMeasurementMode(pParameter->mode);
	SetMeasurementQuality(pParameter->quality);
	SetNDFIlter(pParameter->lNd);
	SetShutterSpeedMode(pParameter->bIsShutterSpeedAuto);
	SetRPD(pParameter->bIsRpd);
	SetRpdPreference(pParameter->rpdPreference);
	SetOpticalZoom(pParameter->zoom);
	SetEnableColorAcquisition(pParameter->bEnableColorAcquisition);
	SetThicknessParameter(pParameter->bThicknessParameterAuto);
	SetLayerAdjustFilter(pParameter->filter);
	SetPeakDetectParam(pParameter->peakDetectParam);
	SetCalibration(&m_labelXYCalibration, pParameter->dXYCalibration);
	SetCalibration(&m_labelZCalibration, pParameter->dZCalibrationFactor);
	SetUmParameter(&m_labelUpperPosition, pParameter->lUpperPosition);
	SetUmParameter(&m_labelLowerPosition, pParameter->lLowerPosition);
	SetUmParameter(&m_labelDistance, pParameter->lDistance);
	SetUmParameter(&m_labelPitch, pParameter->lPitch);
	SetUmParameter(&m_labelThickness, pParameter->lThickness);
	SetParameter(&m_labelGain, pParameter->lGain);
	SetParameter(&m_labelGain2, pParameter->lLaserGain1);
	SetParameter(&m_labelShutterSpeed, pParameter->lShutterSpeed);
	SetParameter(&m_labelShutterSpeed2, pParameter->lCameraShutterSpeed);
	SetParameter(&m_labelLineCount, pParameter->lLineNumber);
	SetParameter(&m_labelLine1Position, pParameter->lMeasureLinePosition);
	SetParameter(&m_labelLine2Position, pParameter->lLinePosition[1]);
	SetParameter(&m_labelLine3Position, pParameter->lLinePosition[2]);
}

void CMeasurementParameterList::SetMeasurementMode(MeasurementMode mode)
{
	switch(mode)
	{
	case Face:
		m_labelMeasurementMode.SetWindowText(_T("Plane"));
		break;
	case Line:
		m_labelMeasurementMode.SetWindowText(_T("Line"));
		break;
	case FaceFilmThickness:
		m_labelMeasurementMode.SetWindowText(_T("Film"));
		break;
	case LineFilmThickness:
		m_labelMeasurementMode.SetWindowText(_T("Line Film"));
		break;
	case FaceTopLayer:
		m_labelMeasurementMode.SetWindowText(_T("Plane top"));
		break;
	case LineTopLayer:
		m_labelMeasurementMode.SetWindowText(_T("Line top"));
		break;
	}
}

void CMeasurementParameterList::SetMeasurementQuality(MeasurementQuality quality)
{
	switch(quality)
	{
	case HighSpeed:
		m_labelMeasurementQuality.SetWindowText(_T("Standard size & high-speed"));
		break;
	case HighResolution:
		m_labelMeasurementQuality.SetWindowText(_T("Standard size & high-accuracy"));
		break;
	case HighDensity:
		m_labelMeasurementQuality.SetWindowText(_T("Super-fine size & high-accuracy"));
		break;
	case Part1Of12Accuracy:
		m_labelMeasurementQuality.SetWindowText(_T("Part 1/12 & high-accuracy"));
		break;
	case Part1Of12SuperHighSpeed:
		m_labelMeasurementQuality.SetWindowText(_T("Part 1/12 & high-speed"));
		break;
	case StandardHighSpeed:
		m_labelMeasurementQuality.SetWindowText(_T("Standard size & high-speed"));
		break;
	}
}

void CMeasurementParameterList::SetNDFIlter(long lFilterIndex)
{
	switch(lFilterIndex)
	{
	case 0:
		m_labelNDFilter.SetWindowText(_T("100%"));
		break;
	case 1:
		m_labelNDFilter.SetWindowText(_T("30%"));
		break;
	case 2:
		m_labelNDFilter.SetWindowText(_T("10%"));
		break;
	case 3:
		m_labelNDFilter.SetWindowText(_T("3%"));
		break;
	case 4:
		m_labelNDFilter.SetWindowText(_T("1%"));
		break;
	}
}

void CMeasurementParameterList::SetShutterSpeedMode(BOOL bMode)
{
	if(bMode)
	{
		m_labelShutterSpeedMode.SetWindowText(_T("Auto"));
	}
	else
	{
		m_labelShutterSpeedMode.SetWindowText(_T("Manual"));
	}
}

void CMeasurementParameterList::SetRPD(BOOL bRPD)
{
	if(bRPD)
	{
		m_labelRPD.SetWindowText(_T("ON"));
	}
	else
	{
		m_labelRPD.SetWindowText(_T("OFF"));
	}
}

void CMeasurementParameterList::SetEnableColorAcquisition(BOOL bEnableColorAcquisition)
{
	if(bEnableColorAcquisition)
	{
		m_labelEnableColorAcquisition.SetWindowText(_T("ON"));
	}
	else
	{
		m_labelEnableColorAcquisition.SetWindowText(_T("OFF"));
	}
}

void CMeasurementParameterList::SetThicknessParameter(BOOL bParameter)
{
	if(bParameter)
	{
		m_labelThicknessParam.SetWindowText(_T("Auto"));
	}
	else
	{
		m_labelThicknessParam.SetWindowText(_T("Manual"));
	}
}

void CMeasurementParameterList::SetLayerAdjustFilter(LayerAdjustFilter filter)
{
	switch(filter)
	{
	case None:
		m_labelLayerAdjustFilter.SetWindowText(_T("None"));
		break;
	case Weak:
		m_labelLayerAdjustFilter.SetWindowText(_T("Week"));
		break;
	case Middle:
		m_labelLayerAdjustFilter.SetWindowText(_T("Normal"));
		break;
	case  Strong:
		m_labelLayerAdjustFilter.SetWindowText(_T("Strong"));
		break;
	}
}

void CMeasurementParameterList::SetPeakDetectParam(PeakDetectParam param)
{
	switch(param)
	{
	case PeakDetect_Standard:
		m_labelPeakDetectParam.SetWindowText(_T("Normal"));
		break;
	case PeakDetect_Strong:
		m_labelPeakDetectParam.SetWindowText(_T("Strong"));
		break;
	case  PeakDetect_SpecialStrong:
		m_labelPeakDetectParam.SetWindowText(_T("Extra strong"));
		break;
	}
}

void CMeasurementParameterList::SetRpdMode(RpdPitch rpdPitch)
{
	switch(rpdPitch)
	{
	case RpdPitch_Normal:
		m_labelRpdPreference.SetWindowText(_T("Normal"));
		break;
	case RpdPitch_HighSpeed:
		m_labelRpdPreference.SetWindowText(_T("High-speed"));
		break;
	}
}

void CMeasurementParameterList::SetRpdPreference(RpdPreference rpdPreference)
{
	switch(rpdPreference)
	{
	case Quality:
		m_labelRpdPreference.SetWindowText(_T("Accuracy priority"));
		break;
	case Speed:
		m_labelRpdPreference.SetWindowText(_T("Speed priority"));
		break;
	case Automatic:
		m_labelRpdPreference.SetWindowText(_T("Auto"));
		break;
	}
}

void CMeasurementParameterList::SetOpticalZoom(ZoomIndex zoom)
{
	switch(zoom)
	{
	case Zoom_10x:
		m_labelOpticalZoom.SetWindowText(_T("1.0x"));
		break;
	case Zoom_15x:
		m_labelOpticalZoom.SetWindowText(_T("1.5x"));
		break;
	case Zoom_20x:
		m_labelOpticalZoom.SetWindowText(_T("2.0x"));
		break;
	case Zoom_30x:
		m_labelOpticalZoom.SetWindowText(_T("3.0x"));
		break;
	case Zoom_50x:
		m_labelOpticalZoom.SetWindowText(_T("5.0x"));
		break;
	case Zoom_80x:
		m_labelOpticalZoom.SetWindowText(_T("8.0x"));
		break;
	}
}

void CMeasurementParameterList::SetCalibration(CStatic *label, double dbCalibration)
{
	CString strCalibration;
	strCalibration.Format(_T("%g"), dbCalibration);
	label->SetWindowText(strCalibration);
}

void CMeasurementParameterList::SetParameter(CStatic *label, long lParameter)
{
	CString strParameter;
	strParameter.Format(_T("%d"), lParameter);
	label->SetWindowText(strParameter);
}

void CMeasurementParameterList::SetUmParameter(CStatic *label, long lParameter)
{
	CString strParameter;
	strParameter.Format(_T("%ld"), lParameter);
	label->SetWindowText(strParameter);
}


//void CMeasurementParameterList::strSetMeasurementParameter(char *pStrData, MeasurementParameter* pParameter)
//{
//	if(pParameter == NULL) return;
//
//	//strSetMeasurementMode(pStrData, pParameter->mode);
//	//strSetMeasurementQuality(pStrData, pParameter->quality);
//	//strSetNDFIlter(pStrData, pParameter->lNd);
//	//strSetShutterSpeedMode(pStrData, pParameter->bIsShutterSpeedAuto);
//	//strSetRPD(pStrData, pParameter->bIsRpd);
//	//strSetRpdPreference(pStrData, pParameter->rpdPreference);
//	//strSetOpticalZoom(pStrData, pParameter->zoom);
//	//strSetEnableColorAcquisition(pStrData, pParameter->bEnableColorAcquisition);
//	//strSetThicknessParameter(pStrData, pParameter->bThicknessParameterAuto);
//	//strSetLayerAdjustFilter(pStrData, pParameter->filter);
//	//strSetPeakDetectParam(pStrData, pParameter->peakDetectParam);
//
//
//	//strSetCalibration(pStrData, &m_labelXYCalibration, pParameter->dXYCalibration);
//	//strSetCalibration(pStrData, &m_labelZCalibration, pParameter->dZCalibration);
//	//strSetUmParameter(pStrData, &m_labelUpperPosition, pParameter->lUpperPosition);
//	//strSetUmParameter(pStrData, &m_labelLowerPosition, pParameter->lLowerPosition);
//	//strSetUmParameter(pStrData, &m_labelDistance, pParameter->lDistance);
//	//strSetUmParameter(pStrData, &m_labelPitch, pParameter->lPitch);
//	//strSetUmParameter(pStrData, &m_labelThickness, pParameter->lThickness);
//
//	//strSetParameter(pStrData, &m_labelGain, pParameter->lGain);
//	//strSetParameter(pStrData, &m_labelShutterSpeed, pParameter->lShutterSpeed);
//	//strSetParameter(pStrData, &m_labelLineCount, pParameter->lLineNumber);
//	//strSetParameter(pStrData, &m_labelLine1Position, pParameter->lLinePosition[0]);
//	//strSetParameter(pStrData, &m_labelLine2Position, pParameter->lLinePosition[1]);
//	//strSetParameter(pStrData, &m_labelLine3Position, pParameter->lLinePosition[2]);
//
//	return;
//}

//void CMeasurementParameterList::strSetMeasurementParameter2(char *pStrData, MeasurementParameter2* pParameter)
//{
//	if(pParameter == NULL) return;
//
//	strSetMeasurementMode(pStrData, pParameter->mode);
//	strSetMeasurementQuality(pStrData, pParameter->quality);
//	strSetNDFIlter(pStrData, pParameter->lNd);
//	strSetShutterSpeedMode(pStrData, pParameter->bIsShutterSpeedAuto);
//	strSetRPD(pStrData, pParameter->bIsRpd);
//	strSetRpdPreference(pStrData, pParameter->rpdPreference);
//	strSetOpticalZoom(pStrData, pParameter->zoom);
//	strSetEnableColorAcquisition(pStrData, pParameter->bEnableColorAcquisition);
//	strSetThicknessParameter(pStrData, pParameter->bThicknessParameterAuto);
//	strSetLayerAdjustFilter(pStrData, pParameter->filter);
//	strSetPeakDetectParam(pStrData, pParameter->peakDetectParam);
//	strSetCalibration(pStrData, &m_labelXYCalibration, pParameter->dXYCalibration);
//	strSetCalibration(pStrData, &m_labelZCalibration, pParameter->dZCalibrationFactor);
//	strSetUmParameter(pStrData, &m_labelUpperPosition, pParameter->lUpperPosition);
//	strSetUmParameter(pStrData, &m_labelLowerPosition, pParameter->lLowerPosition);
//	strSetUmParameter(pStrData, &m_labelDistance, pParameter->lDistance);
//	strSetUmParameter(pStrData, &m_labelPitch, pParameter->lPitch);
//	strSetUmParameter(pStrData, &m_labelThickness, pParameter->lThickness);
//	strSetParameter(pStrData, &m_labelGain, pParameter->lGain);
//	strSetParameter(pStrData, &m_labelGain2, pParameter->lLaserGain1);
//	strSetParameter(pStrData, &m_labelShutterSpeed, pParameter->lShutterSpeed);
//	strSetParameter(pStrData, &m_labelShutterSpeed2, pParameter->lCameraShutterSpeed);
//	strSetParameter(pStrData, &m_labelLineCount, pParameter->lLineNumber);
//	strSetParameter(pStrData, &m_labelLine1Position, pParameter->lMeasureLinePosition);
//	strSetParameter(pStrData, &m_labelLine2Position, pParameter->lLinePosition[1]);
//	strSetParameter(pStrData, &m_labelLine3Position, pParameter->lLinePosition[2]);
//
//	return;
//}

void CMeasurementParameterList::strSetMeasurementMode(char *pStrData, MeasurementMode mode)
{
	switch(mode)
	{
	case Face:
		sprintf(pStrData,"Plane");
		break;
	case Line:
		sprintf(pStrData,"Line");
		break;
	case FaceFilmThickness:
		sprintf(pStrData,"Film");
		break;
	case LineFilmThickness:
		sprintf(pStrData,"Line Film");
		break;
	case FaceTopLayer:
		sprintf(pStrData,"Plane top");
		break;
	case LineTopLayer:
		sprintf(pStrData,"Line top");
		break;
	}
}

void CMeasurementParameterList::strSetMeasurementQuality(char *pStrData, MeasurementQuality quality)
{
	switch(quality)
	{
	case HighSpeed:
		sprintf(pStrData,"Standard size & high-speed");
		break;
	case HighResolution:
		sprintf(pStrData,"Standard size & high-accuracy");
		break;
	case HighDensity:
		sprintf(pStrData,"Super-fine size & high-accuracy");
		break;
	case Part1Of12Accuracy:
		sprintf(pStrData,"Part 1/12 & high-accuracy");
		break;
	case Part1Of12SuperHighSpeed:
		sprintf(pStrData,"Part 1/12 & high-speed");
		break;
	case StandardHighSpeed:
		sprintf(pStrData,"Standard size & high-speed");
		break;
	}
}

void CMeasurementParameterList::strSetNDFIlter(char *pStrData, long lFilterIndex)
{
	switch(lFilterIndex)
	{
	case 0:
		sprintf(pStrData,"100%");
		break;
	case 1:
		sprintf(pStrData,"30%");
		break;
	case 2:
		sprintf(pStrData,"10%");
		break;
	case 3:
		sprintf(pStrData,"3%");
		break;
	case 4:
		sprintf(pStrData,"1%");
		break;
	}
}

void CMeasurementParameterList::strSetShutterSpeedMode(char *pStrData, BOOL bMode)
{
	if(bMode)
	{
		sprintf(pStrData,"Auto");
	}
	else
	{
		sprintf(pStrData,"Manual");
	}
}

void CMeasurementParameterList::strSetRPD(char *pStrData, BOOL bRPD)
{
	if(bRPD)
	{
		sprintf(pStrData,"ON");
	}
	else
	{
		sprintf(pStrData,"OFF");
	}
}

void CMeasurementParameterList::strSetEnableColorAcquisition(char *pStrData, BOOL bEnableColorAcquisition)
{
	if(bEnableColorAcquisition)
	{
		sprintf(pStrData,"ON");
	}
	else
	{
		sprintf(pStrData,"OFF");
	}
}

void CMeasurementParameterList::strSetThicknessParameter(char *pStrData, BOOL bParameter)
{
	if(bParameter)
	{
		sprintf(pStrData,"Auto");
	}
	else
	{
		sprintf(pStrData,"Manual");
	}
}

void CMeasurementParameterList::strSetLayerAdjustFilter(char *pStrData, LayerAdjustFilter filter)
{
	switch(filter)
	{
	case None:
		sprintf(pStrData,"None");
		break;
	case Weak:
		sprintf(pStrData,"Week");
		break;
	case Middle:
		sprintf(pStrData,"Normal");
		break;
	case  Strong:
		sprintf(pStrData,"Strong");
		break;
	}
}

void CMeasurementParameterList::strSetPeakDetectParam(char *pStrData, PeakDetectParam param)
{
	switch(param)
	{
	case PeakDetect_Standard:
		sprintf(pStrData,"Normal");
		break;
	case PeakDetect_Strong:
		sprintf(pStrData,"Strong");
		break;
	case  PeakDetect_SpecialStrong:
		sprintf(pStrData,"Extra strong");
		break;
	}
}

void CMeasurementParameterList::strSetRpdMode(char *pStrData, RpdPitch rpdPitch)
{
	switch(rpdPitch)
	{
	case RpdPitch_Normal:
		sprintf(pStrData,"Normal");
		break;
	case RpdPitch_HighSpeed:
		sprintf(pStrData,"High-speed");
		break;
	}
}

void CMeasurementParameterList::strSetRpdPreference(char *pStrData, RpdPreference rpdPreference)
{
	switch(rpdPreference)
	{
	case Quality:
		sprintf(pStrData,"Accuracy priority");
		break;
	case Speed:
		sprintf(pStrData,"Speed priority");
		break;
	case Automatic:
		sprintf(pStrData,"Auto");
		break;
	}
}

void CMeasurementParameterList::strSetOpticalZoom(char *pStrData, ZoomIndex zoom)
{
	switch(zoom)
	{
	case Zoom_10x:
		sprintf(pStrData,"1.0x");
		break;
	case Zoom_15x:
		sprintf(pStrData,"1.5x");
		break;
	case Zoom_20x:
		sprintf(pStrData,"2.0x");
		break;
	case Zoom_30x:
		sprintf(pStrData,"3.0x");
		break;
	case Zoom_50x:
		sprintf(pStrData,"5.0x");
		break;
	case Zoom_80x:
		sprintf(pStrData,"8.0x");
		break;
	}
	
}

void CMeasurementParameterList::strSetCalibration(char *pStrData, double dbCalibration)
{
	sprintf(pStrData,"%g", dbCalibration );
}

void CMeasurementParameterList::strSetParameter(char *pStrData, long lParameter)
{
	sprintf(pStrData,"%d", lParameter );
}

void CMeasurementParameterList::strSetUmParameter(char *pStrData, long lParameter)
{
	sprintf(pStrData,"%ld", lParameter );
}

BEGIN_MESSAGE_MAP(CMeasurementParameterList, CDialog)
	ON_WM_CREATE()
END_MESSAGE_MAP()

