/// [2014-11-05] Charles, Added.

#pragma once

#include "MeasureAlg.h"

//
// STRIPE 형 측정 알고리즘 객체
//
class CMeasureStripeAlg : public CMeasureAlg
{
public:
	CMeasureStripeAlg(void);
	virtual ~CMeasureStripeAlg(void);

	static CMeasureAlg *GetInstance();

	virtual BOOL Measure();
	virtual BOOL MakeResultImage();

protected :
	virtual BOOL _GenerateRefData();
	virtual BOOL _GetMinCenterPoints();
	virtual BOOL _GetMeasurePoints();
	virtual BOOL _GetHeightProfile();
	virtual BOOL _MeasureHeightProfile();

	virtual BOOL _MakeResultData();

	virtual BOOL _MakeResultFullImage();
	virtual BOOL _MakeResultHeightImage();
	virtual BOOL _MakeResultDataImage();
	virtual BOOL _MakeAnalysisResultDataImage();

	BOOL _FindStartCenterPoint();
	BOOL _SelectEndMinCenterPoint();

	FLOAT _CalcSTDColumnData(UINT32 *pData, int idxColumn, int nSizeX, int nSizeY);
};
