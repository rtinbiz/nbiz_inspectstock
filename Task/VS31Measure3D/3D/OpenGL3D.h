#pragma once

#include "..\3D\gl\gl.h"
#include "..\3D\gl\glu.h"
#include "..\DataFormat.h"

class COpenGL3D
{
public:
	COpenGL3D(void);
	~COpenGL3D(void);
public:
	BOOL InitOpenGL(HWND hWnd, HWND hWnd2DView);
	BOOL DataLoading_Dn(RENDER_DATA *pNewData);

	void RenderScreen();
	void ChangeViewSize(int newWindowWidth, int newWindowHeight);
	
	IplImage * m_pReanderImage;


	void MoveObject(float xRate, float yRate);
	void RotatObject(float xRate, float yRate);
	void ZoomObject(float UpDownValue);

	void XMoveObject(int MoveValue);
	void YMoveObject(int MoveValue);
	void Z2OMoveObject(int MoveValue);
	void Z1OMoveObject(int MoveValue);

	//void MeaserInfoSet(UINT Axis, int Position, BOOL bAlign=FALSE);

	void SetDefValue(){m_ActionData.setDefValue();m_ActionData.SetScale(abs(m_3DViewerInfo.DnZMax)+abs(m_3DViewerInfo.DnZMin));RenderScreen();};
private:

	void Get3DImage();
	void DrawBox();
	void DrawBase();
	void DrawAxis();
    void DrawMeaserBox(UINT Axis, int PositionY, int PositionX, float fAngle = 0.0f);


private:
	void DestroyOpenGL();
	void DeleteData_UpDn(RENDER_DATA *pDeleteData);
	BOOL DataLoading_UpDn(RENDER_DATA *pNewData, RENDER_DATA **ppLocalData);
	// raster font support
	void RasterFont();
	void PrintString(const char *str);
	BOOL SetWindowPixelFormat();
	BOOL CreateViewGLContext();

private:
	//대상 Window Drawing을 위한.
	HWND	m_hWnd;

	HDC		m_hDC;
	HGLRC	m_hGLContext;
	GLuint  m_nFontOffset;

private:
	RENDER_VIEWER m_3DViewerInfo;
	RENDER_ACTION m_ActionData;
	//RENDER_DATA *m_pUpData;
	RENDER_DATA *m_pDnData;

	//////////////////////////////////////////////////////////////////////////
	DATA_ALIGN	m_3DDataAlignInfo;

	//////////////////////////////////////////////////////////////////////////
public:
	MEASER_POS	m_MeaserInfo;
	//MEASER_DVIEW m_MeaserViewInfo;
	MEASER_2D *m_pStripeLastResult;
};
