#include "StdAfx.h"
#include "OpenGL3D.h"

COpenGL3D::COpenGL3D(void)
{
	m_hWnd		= NULL;
	m_pDnData	= NULL;
	m_pReanderImage = NULL;

}

COpenGL3D::~COpenGL3D(void)
{
	if(m_pReanderImage != NULL)
		cvReleaseImage(&m_pReanderImage);
	m_pReanderImage = NULL;
	DestroyOpenGL();
}
BOOL COpenGL3D::InitOpenGL(HWND hWnd, HWND hWnd2DView)
{
	if (!::IsWindow(hWnd))
		return FALSE;

	m_hWnd = hWnd;
	m_hDC = ::GetDC(m_hWnd);
   if(!SetWindowPixelFormat())
			return FALSE;
	if(!CreateViewGLContext())
		return FALSE;
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	RasterFont();

	m_pStripeLastResult = NULL;
	return TRUE;
}

void COpenGL3D::DestroyOpenGL()
{
	if(wglGetCurrentContext())
		wglMakeCurrent(NULL, NULL);

	if(m_hGLContext)
	{
		wglDeleteContext(m_hGLContext);
		m_hGLContext = NULL;
	}
	::ReleaseDC(m_hWnd, m_hDC);
}
void COpenGL3D::RasterFont()
{
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	m_nFontOffset = glGenLists(128);
	for (GLuint i = 32; i < 127; i++)
	{
		glNewList(i + m_nFontOffset, GL_COMPILE);
		glBitmap(8, 13, 0.0f, 2.0f, 10.0f, 0.0f, G_RasterFont[i-32]);
		glEndList();
	}
}

void COpenGL3D::PrintString(const char* str)
{
	glPushAttrib(GL_LIST_BIT);
	glListBase(m_nFontOffset);
	glCallLists((int)strlen(str), GL_UNSIGNED_BYTE, (GLubyte*)str);
	glPopAttrib();
}
BOOL COpenGL3D::SetWindowPixelFormat()
{
	PIXELFORMATDESCRIPTOR pfd;

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);	// Size of this pfd
	pfd.nVersion = 1;							// Version number : must be 1
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |			// Support window
		PFD_SUPPORT_OPENGL |			// Support OpenGL
		PFD_DOUBLEBUFFER |			// Double buffered
		PFD_STEREO_DONTCARE;			// Support either monoscopic or stereoscopic
	pfd.iPixelType = PFD_TYPE_RGBA;				// RGBA type
	pfd.cColorBits = 32;						// Specifies the number of color bitplanes in each color buffer
	pfd.cRedBits = 8;							// Specifies the number of red bitplanes in each RGBA color buffer
	pfd.cRedShift = 16;							// Specifies the shift count for red bitplanes in each RGBA color buffer
	pfd.cGreenBits = 8;							// Specifies the number of green bitplanes in each RGBA color buffer
	pfd.cGreenShift = 8;						// Specifies the shift count for green bitplanes in each RGBA color buffer
	pfd.cBlueBits = 8;							// Specifies the number of blue bitplanes in each RGBA color buffer
	pfd.cBlueShift = 0;							// Specifies the shift count for blue bitplanes in each RGBA color buffer
	pfd.cAlphaBits = 0;							// Specifies the number of alpha bitplanes in each RGBA color buffer. Alpha bitplanes are not supported
	pfd.cAlphaShift = 0;						// Specifies the shift count for alpha bitplanes in each RGBA color buffer. Alpha bitplanes are not supported
	pfd.cAccumBits = 64;						// Specifies the total number of bitplanes in the accumulation buffer
	pfd.cAccumRedBits = 16;						// Specifies the number of red bitplanes in the accumulation buffer
	pfd.cAccumGreenBits = 16;					// Specifies the number of green bitplanes in the accumulation buffer
	pfd.cAccumBlueBits = 16;					// Specifies the number of blue bitplanes in the accumulation buffer
	pfd.cAccumAlphaBits = 0;					// Specifies the number of alpha bitplanes in the accumulation buffer
	pfd.cDepthBits = 32;						// Specifies the depth of the depth (z-axis) buffer
	pfd.cStencilBits = 8;						// Specifies the depth of the stencil buffer
	pfd.cAuxBuffers = 0;						// Specifies the number of auxiliary buffers. Auxiliary buffers are not supported
	pfd.iLayerType = PFD_MAIN_PLANE;			// Ignored. Earlier implementations of OpenGL used this member, but it is no longer used
	pfd.bReserved = 0;							// Specifies the number of overlay and underlay planes
	pfd.dwLayerMask = 0;						// Ignored. Earlier implementations of OpenGL used this member, but it is no longer used
	pfd.dwVisibleMask = 0;						// Specifies the transparent color or index of an underlay plane
	pfd.dwDamageMask = 0;						// Ignored. Earlier implementations of OpenGL used this member, but it is no longer used

	int m_GLPixelIndex = ChoosePixelFormat(m_hDC, &pfd);// Attempts to match an appropriate pixel format supported by a device context to a given pixel format specification
	if(m_GLPixelIndex == 0)								// Choose default
	{
		m_GLPixelIndex = 1;
		if(DescribePixelFormat(m_hDC, m_GLPixelIndex,	// Obtains information about the pixel format identified by iPixelFormat of the device associated with hdc
			sizeof(PIXELFORMATDESCRIPTOR), &pfd)==0)
			return FALSE;
	}
	if(!SetPixelFormat(m_hDC, m_GLPixelIndex, &pfd))	//Sets the pixel format of the specified device context to the format specified by the iPixelFormat index
		return FALSE;

	return TRUE;
}
BOOL COpenGL3D::CreateViewGLContext()
{
	m_hGLContext = wglCreateContext(m_hDC); // Create an OpenGL rendering context
	if(!m_hGLContext)
		return FALSE;
	if(!wglMakeCurrent(m_hDC, m_hGLContext)) // Set the current rendering context
		return FALSE;

	return TRUE;
}





//////////////////////////////////////////////////////////////////////////
void COpenGL3D::DeleteData_UpDn(RENDER_DATA *pDeleteData)
{
	if(pDeleteData != NULL)
	{
		if(pDeleteData->pDepthData != NULL)
			delete [] pDeleteData->pDepthData;
		if(pDeleteData->pImgData != NULL)
			delete [] pDeleteData->pImgData;
		if(pDeleteData->pElementIndex != NULL)
			delete [] pDeleteData->pElementIndex;
		if(pDeleteData->pVectorData != NULL)
			delete[] pDeleteData->pVectorData;
		if(pDeleteData->pColorData != NULL)
			delete[] pDeleteData->pColorData;

		
		delete pDeleteData;
		pDeleteData = NULL;
	}
}

BOOL COpenGL3D::DataLoading_UpDn(RENDER_DATA *pNewData, RENDER_DATA **ppLocalData)
{
	if(pNewData == NULL)
		return FALSE;
	DeleteData_UpDn((*ppLocalData));

	(*ppLocalData) =  new RENDER_DATA;
	RENDER_DATA *pLocalData = *ppLocalData;
	memset(pLocalData, 0x00, sizeof(RENDER_DATA));
	
	pLocalData->DnX = pNewData->DnX;				//Max Width Size.
	pLocalData->DnY = pNewData->DnY;				//Max Height Size.
	pLocalData->DnZ = pNewData->DnZ;				//Max Depth Size.
	
	pLocalData->PixelPerNano = pNewData->PixelPerNano;
	pLocalData->pImgData		= new IMG_PIXEL[pLocalData->DnX * pLocalData->DnY];	//Dnx *DnY Size RGB Image.
	memcpy(pLocalData->pImgData, pNewData->pImgData, sizeof(IMG_PIXEL)*pLocalData->DnX * pLocalData->DnY);

	pLocalData->pDepthData		= new long[pLocalData->DnX * pLocalData->DnY];		//Dnx *DnY Size Depth Data.
	memcpy(pLocalData->pDepthData, pNewData->pDepthData, sizeof(long)*pLocalData->DnX * pLocalData->DnY);
	//////////////////////////////////////////////////////////////////////////
	//
	pLocalData->pVectorData		= new GLfloat[pLocalData->DnX * pLocalData->DnY*3];//3Point XYZ Position Data(Rendering);
	pLocalData->pColorData		= new GLubyte[pLocalData->DnX * pLocalData->DnY*3];//3Point XYZ Position Data(Rendering);
	
	GLfloat *pVPoint = NULL;
	GLubyte *pCPoint = NULL;

	IMG_PIXEL *pMapIMG2 = NULL; //;
	IMG_PIXEL *pMapIMG_Up = NULL; //;
	for(UINT VecStepR = 0; VecStepR<pLocalData->DnY; VecStepR++)//768
	{
		for(UINT VecStepC = 0; VecStepC<pLocalData->DnX; VecStepC++)//1024
		{
			//////////////////////////////////////////////////////////////////////////
			pVPoint = (GLfloat *)&pLocalData->pVectorData[(VecStepR*pLocalData->DnX*3) + (VecStepC*3)];

			*(pVPoint+0) = (GLfloat) VecStepC -(pLocalData->DnX/2);//VecStepC;
			*(pVPoint+1) = (GLfloat) VecStepR -(pLocalData->DnY/2);//VecStepR;
			*(pVPoint+2) =/* (-1.0f)**/(GLfloat)(pLocalData->pDepthData[(VecStepR*pLocalData->DnX)+VecStepC]/(GLfloat)pLocalData->PixelPerNano);//m_ImageDepth[(VecStepR*col)+VecStepC]/1000;
			//////////////////////////////////////////////////////////////////////////
			pMapIMG2 = (IMG_PIXEL *)(pNewData->pImgData+(VecStepR*pLocalData->DnX)+VecStepC);
			pCPoint = (GLubyte*)&pLocalData->pColorData[(VecStepR*pLocalData->DnX*3) + (VecStepC*3)];
			

			*(pCPoint+0) = (GLubyte)pMapIMG2->R;
			*(pCPoint+1) = (GLubyte)pMapIMG2->G;
			*(pCPoint+2) = (GLubyte)pMapIMG2->B;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	pLocalData->pElementIndex	= new UINT[pLocalData->DnX* pLocalData->DnY*4];//4Point Elements;
	memset(pLocalData->pElementIndex, 0x00, (sizeof(UINT) * pLocalData->DnX* pLocalData->DnY * 4));
	UINT *pElement4 = NULL;
	
	pLocalData->ElementCnt =0;// (pLocalData->DnY-1)*(pLocalData->DnX-1)*4;

	for(UINT HStep = 0; HStep<pLocalData->DnY-1; HStep++)
	{
		for(UINT WStep = 0; WStep<pLocalData->DnX-1; WStep++)
		{
			pElement4 = &pLocalData->pElementIndex[(HStep*4*pLocalData->DnX)+WStep*4];

			*(pElement4+0) = ((HStep)*	pLocalData->DnX)	+ WStep; 
			*(pElement4+1) = ((HStep)*	pLocalData->DnX)	+ (WStep + 1);
			*(pElement4+2) = ((HStep+1)*	pLocalData->DnX)	+ (WStep + 1);
			*(pElement4+3) = ((HStep+1)*	pLocalData->DnX)	+ WStep;

			pLocalData->ElementCnt += 4;
		}
	}
	//RenderScreen();
	return TRUE;
}

BOOL COpenGL3D::DataLoading_Dn(RENDER_DATA *pNewData)
{
	m_3DViewerInfo.DnXMin = (-1.0f)*((float)(pNewData->DnX/2));
	m_3DViewerInfo.DnXMax = (float)(pNewData->DnX/2);

	m_3DViewerInfo.DnYMin = (-1.0f)*(float)(pNewData->DnY/2);
	m_3DViewerInfo.DnYMax = ((float)(pNewData->DnY/2));

	//처음 Loding 시에는 합산이 아닌 Data를 뿌린다.
	//m_3DViewerInfo.DnZMin = (-1.0f)*(float)(pNewData->DnZ);
	//m_3DViewerInfo.DnZMax = 10.0f;
	m_3DViewerInfo.DnZMin = -10.0f;
	m_3DViewerInfo.DnZMax = (float)(pNewData->DnZ);
	m_ActionData.SetScale(abs(m_3DViewerInfo.DnZMax)+abs(m_3DViewerInfo.DnZMin));

	return DataLoading_UpDn(pNewData, &m_pDnData);
}



void COpenGL3D::ChangeViewSize(int newWindowWidth, int newWindowHeight)
{
	glViewport(0, 0, newWindowWidth, newWindowHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0,   (double)newWindowWidth /(double)newWindowHeight , 1.0f, 400.0f);
	//glShadeModel(GL_SMOOTH);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
	RenderScreen();
}

void COpenGL3D::DrawBox()
{

	//glBegin(GL_QUADS);
	//glColor3f(0.8f, 0.8f, 0.0f);
	//glVertex3f(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax, m_3DViewerInfo.DnZMin);
	//glVertex3f(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin, m_3DViewerInfo.DnZMin);
	//glVertex3f(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin, m_3DViewerInfo.DnZMin);
	//glVertex3f(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax, m_3DViewerInfo.DnZMin);
	//glEnd();
}

void COpenGL3D::DrawBase()
{


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glBegin(GL_QUADS);
	glColor4f(0.8f, 0.0f, 0.0f,0.2f);
	glVertex3f(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax, 0.0f);
	glVertex3f(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin, 0.0f);
	glVertex3f(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin, 0.0f);
	glVertex3f(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax, 0.0f);
	glEnd();


	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}
void COpenGL3D::DrawMeaserBox(UINT Axis, int PositionY, int PositionX, float fAngle)
{
	if(Axis == 0)
		return;



	//double radian_data = (fAngle) * 3.141592 / 180.0f;//회전 거꾸로 돌려라. ㅡ.ㅡ;;;;

	//float sourcex = 0.0f;
	//float sourcey =  0.0f;
	//float sourcexs = 0.0f;
	//float sourceys = 0.0f;


	//for(int DrawStep = 0; DrawStep<pNewElement->nCntPoint; DrawStep++)
	//{
	//	sourcex = (float)(pNewElement->pPoints[DrawStep].x/5000.0f);
	//	sourcey = (float)(pNewElement->pPoints[DrawStep].y/5000.0f);

	//	//sourcexs = (float)(sourcex*cos(radian_data) + (sourcey)*sin(radian_data)); 
	//	//sourceys = (float)(sourcey*cos(radian_data) - (sourcex)*sin(radian_data)); 
	//	sourcexs = (float)(sourcex)*cos(radian_data) - (sourcey)*sin(radian_data); 
	//	sourceys = (float)(sourcex)*sin(radian_data) + (sourcey)*cos(radian_data); 

	//}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glBegin(GL_QUADS);
	
	{
		if(Axis == 1 )//X축 기준.
		{
			glColor4f(0.0f, 0.0f, 0.8f,0.5f);
			glVertex3f((float)PositionX, m_3DViewerInfo.DnYMax+100, m_3DViewerInfo.DnZMax+10);
			glVertex3f((float)PositionX, m_3DViewerInfo.DnYMax+100, m_3DViewerInfo.DnZMin-10);
			glVertex3f((float)PositionX, m_3DViewerInfo.DnYMin-100, m_3DViewerInfo.DnZMin-10);
			glVertex3f((float)PositionX, m_3DViewerInfo.DnYMin-100, m_3DViewerInfo.DnZMax+10);
		}
		else if(Axis == 2 )//Y축 기준.
		{
			glColor4f(0.0f, 0.8f, 0.0f,0.5f);
			glVertex3f(m_3DViewerInfo.DnXMax+100, (float)PositionY, m_3DViewerInfo.DnZMax+10);
			glVertex3f(m_3DViewerInfo.DnXMax+100, (float)PositionY, m_3DViewerInfo.DnZMin-10);
			glVertex3f(m_3DViewerInfo.DnXMin-100, (float)PositionY, m_3DViewerInfo.DnZMin-10);
			glVertex3f(m_3DViewerInfo.DnXMin-100, (float)PositionY, m_3DViewerInfo.DnZMax+10);
		}
		else if(Axis == 3 )//X, Y둘다
		{
			if(fAngle == 0.0)
			{
				glColor4f(0.0f, 0.8f, 0.0f,0.5f);
				//X 축
				glVertex3f((float)PositionX, m_3DViewerInfo.DnYMax+100, m_3DViewerInfo.DnZMax+10);
				glVertex3f((float)PositionX, m_3DViewerInfo.DnYMax+100, m_3DViewerInfo.DnZMin-10);
				glVertex3f((float)PositionX, m_3DViewerInfo.DnYMin-100, m_3DViewerInfo.DnZMin-10);
				glVertex3f((float)PositionX, m_3DViewerInfo.DnYMin-100, m_3DViewerInfo.DnZMax+10);

				glColor4f(0.0f, 0.0f, 0.8f,0.5f);
				//Y축 기준
				glVertex3f(m_3DViewerInfo.DnXMax+100, (float)PositionY, m_3DViewerInfo.DnZMax+10);
				glVertex3f(m_3DViewerInfo.DnXMax+100, (float)PositionY, m_3DViewerInfo.DnZMin-10);
				glVertex3f(m_3DViewerInfo.DnXMin-100, (float)PositionY, m_3DViewerInfo.DnZMin-10);
				glVertex3f(m_3DViewerInfo.DnXMin-100, (float)PositionY, m_3DViewerInfo.DnZMax+10);
			}
			else
			{
				//glColor4f(0.0f, 0.8f, 0.0f,0.5f);
				////X 축
				//glVertex3f((float)PositionX, m_3DViewerInfo.DnYMax+100, m_3DViewerInfo.DnZMax+10);
				//glVertex3f((float)PositionX, m_3DViewerInfo.DnYMax+100, m_3DViewerInfo.DnZMin-10);
				//glVertex3f((float)PositionX, m_3DViewerInfo.DnYMin-100, m_3DViewerInfo.DnZMin-10);
				//glVertex3f((float)PositionX, m_3DViewerInfo.DnYMin-100, m_3DViewerInfo.DnZMax+10);

				//glColor4f(0.0f, 0.0f, 0.8f,0.5f);
				////Y축 기준
				//glVertex3f(m_3DViewerInfo.DnXMax+100, (float)PositionY, m_3DViewerInfo.DnZMax+10);
				//glVertex3f(m_3DViewerInfo.DnXMax+100, (float)PositionY, m_3DViewerInfo.DnZMin-10);
				//glVertex3f(m_3DViewerInfo.DnXMin-100, (float)PositionY, m_3DViewerInfo.DnZMin-10);
				//glVertex3f(m_3DViewerInfo.DnXMin-100, (float)PositionY, m_3DViewerInfo.DnZMax+10);

				float OrgPosX = (float)PositionX;
				float OrgPosY = (float)PositionY;

				float sourcex = 0.0f;
				float sourcey = 0.0f;
				float sourcexP = 0.0f;
				float sourceyP = 0.0f;
				double radian_data = (fAngle) * 3.141592 / 180.0f;

				//x' = (x-a) * cosR - (y-b)sinR
				//y' = (x-a) * sinR + (y-b)cosR

				glColor4f(0.0f, 0.0f, 0.8f,0.5f);
				sourcex = OrgPosX+(1024.0f/2.0f);
				sourcey =  (float)PositionY;
				sourcexP = (float)((sourcex-OrgPosX)*cos(radian_data) - (sourcey-OrgPosY)*sin(radian_data)) + OrgPosX; 
				sourceyP = (float)((sourcex-OrgPosX)*sin(radian_data) + (sourcey-OrgPosY)*cos(radian_data)) + OrgPosY; 

				glVertex3f(sourcexP, sourceyP, m_3DViewerInfo.DnZMax);

				sourcex = OrgPosX+(1024.0f/2.0f);
				sourcey = (float)PositionY;
				sourcexP = (float)((sourcex-OrgPosX)*cos(radian_data) - (sourcey-OrgPosY)*sin(radian_data)) + OrgPosX; 
				sourceyP = (float)((sourcex-OrgPosX)*sin(radian_data) + (sourcey-OrgPosY)*cos(radian_data)) + OrgPosY; 
				glVertex3f(sourcexP, sourceyP, m_3DViewerInfo.DnZMin);

				sourcex = OrgPosX-(1024.0f/2.0f);
				sourcey = (float)PositionY;
				sourcexP = (float)((sourcex-OrgPosX)*cos(radian_data) - (sourcey-OrgPosY)*sin(radian_data)) + OrgPosX; 
				sourceyP = (float)((sourcex-OrgPosX)*sin(radian_data) + (sourcey-OrgPosY)*cos(radian_data)) + OrgPosY; 
				glVertex3f(sourcexP, sourceyP, m_3DViewerInfo.DnZMin);

				sourcex =  OrgPosX-(1024.0f/2.0f);
				sourcey = (float)PositionY;
				sourcexP = (float)((sourcex-OrgPosX)*cos(radian_data) - (sourcey-OrgPosY)*sin(radian_data)) + OrgPosX; 
				sourceyP = (float)((sourcex-OrgPosX)*sin(radian_data) + (sourcey-OrgPosY)*cos(radian_data)) + OrgPosY; 
				glVertex3f(sourcexP, sourceyP, m_3DViewerInfo.DnZMax);

				glColor4f(0.0f, 0.8f, 0.0f,0.5f);
				sourcex = (float)PositionX;
				sourcey =  OrgPosY+(1024.0f/2.0f);
				sourcexP = (float)((sourcex-OrgPosX)*cos(radian_data) - (sourcey-OrgPosY)*sin(radian_data)) + OrgPosX; 
				sourceyP = (float)((sourcex-OrgPosX)*sin(radian_data) + (sourcey-OrgPosY)*cos(radian_data)) + OrgPosY; 

				glVertex3f(sourcexP, sourceyP, m_3DViewerInfo.DnZMax);

				//sourcex = (float)PositionX;
				sourcey =  OrgPosY+(1024.0f/2.0f);
				sourcexP = (float)((sourcex-OrgPosX)*cos(radian_data) - (sourcey-OrgPosY)*sin(radian_data)) + OrgPosX; 
				sourceyP = (float)((sourcex-OrgPosX)*sin(radian_data) + (sourcey-OrgPosY)*cos(radian_data)) + OrgPosY; 
				glVertex3f(sourcexP, sourceyP, m_3DViewerInfo.DnZMin);

				//sourcex = (float)PositionX;
				sourcey =  OrgPosY-(1024.0f/2.0f);
				sourcexP = (float)((sourcex-OrgPosX)*cos(radian_data) - (sourcey-OrgPosY)*sin(radian_data)) + OrgPosX; 
				sourceyP = (float)((sourcex-OrgPosX)*sin(radian_data) + (sourcey-OrgPosY)*cos(radian_data)) + OrgPosY; 
				glVertex3f(sourcexP, sourceyP, m_3DViewerInfo.DnZMin);

				//sourcex = (float)PositionX;
				sourcey =  OrgPosY-(1024.0f/2.0f);
				sourcexP = (float)((sourcex-OrgPosX)*cos(radian_data) - (sourcey-OrgPosY)*sin(radian_data)) + OrgPosX; 
				sourceyP = (float)((sourcex-OrgPosX)*sin(radian_data) + (sourcey-OrgPosY)*cos(radian_data)) + OrgPosY; 
				glVertex3f(sourcexP, sourceyP, m_3DViewerInfo.DnZMax);
			}



			//Z축 기준
			//glVertex3f(m_3DViewerInfo.DnXMax+100, m_3DViewerInfo.DnYMax+100, (float)Position);
			//glVertex3f(m_3DViewerInfo.DnXMax+100, m_3DViewerInfo.DnYMin-100, (float)Position);
			//glVertex3f(m_3DViewerInfo.DnXMin-100, m_3DViewerInfo.DnYMin-100, (float)Position);
			//glVertex3f(m_3DViewerInfo.DnXMin-100, m_3DViewerInfo.DnYMax+100, (float)Position);
		}
	}
	glEnd();
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}
void COpenGL3D::DrawAxis()
{
	glLineWidth(60.0f);

	glColor3f(1.0f, 0, 0);//X
	glBegin(GL_LINES);
	glVertex3f(m_3DViewerInfo.DnXMin, 0.0f, 0.0f);
	glVertex3f(m_3DViewerInfo.DnXMax, 0.0f, 0.0f);
	glEnd();

	glColor3f(0,1.0f, 0);//Y
	glBegin(GL_LINES);
	glVertex3f(0.0f, m_3DViewerInfo.DnYMin, 0.0f);
	glVertex3f(0.0f, m_3DViewerInfo.DnYMax, 0.0f);
	glEnd();

	glColor3f(0, 0, 1.0f);//Z
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, m_3DViewerInfo.DnZMin);
	glVertex3f(0.0f, 0.0f, m_3DViewerInfo.DnZMax);

	glEnd();
}
void COpenGL3D::RenderScreen()
{
	// begin OpenGL call
	wglMakeCurrent(m_hDC, m_hGLContext);

	//Background Color Setting
	COLORREF BackColor = ::GetSysColor(COLOR_3DFACE);
	glClearColor(	(float)GetRValue(BackColor)/255.0f,
					(float)GetGValue(BackColor)/255.0f,
					(float)GetBValue(BackColor)/255.0f,1.0f);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



	glPushMatrix();
	//////////////////////////////////////////////////////////////////////////
	//
	{//형상 변환.
		glTranslated(m_ActionData.m_xTrans, m_ActionData.m_yTrans, m_ActionData.m_zTrans);

		glRotatef(m_ActionData.m_xRotat,1.0, 0.0, 0.0);
		glRotatef(m_ActionData.m_yRotat, 0.0, 1.0, 0.0);
		glRotatef(m_ActionData.m_zRotat, 0.0, 0.0, 1.0);

		if(m_ActionData.m_xRotat > 360)		m_ActionData.m_xRotat -= 360;
		if(m_ActionData.m_xRotat < -360)	m_ActionData.m_xRotat += 360;
		if(m_ActionData.m_yRotat > 360)		m_ActionData.m_yRotat -= 360;
		if(m_ActionData.m_yRotat < -360)	m_ActionData.m_yRotat += 360;
		if(m_ActionData.m_zRotat > 360)		m_ActionData.m_zRotat -= 360;
		if(m_ActionData.m_zRotat < -360)	m_ActionData.m_zRotat += 360;

		glScalef(m_ActionData.m_xScale, m_ActionData.m_yScale, m_ActionData.m_zScale);
	}
	{//View 영역 설정.
		glOrtho (	m_3DViewerInfo.DnXMin + m_ActionData.m_xMove,	
					m_3DViewerInfo.DnXMax + m_ActionData.m_xMove, 
					m_3DViewerInfo.DnYMin + m_ActionData.m_yMove,
					m_3DViewerInfo.DnYMax + m_ActionData.m_yMove,
					m_3DViewerInfo.DnZMin,
					m_3DViewerInfo.DnZMax);
					
	}
	{//Object Drawing.

		{
			//전체 View Position 제어.
			float ViewCenter  = m_3DViewerInfo.DnZMin-30;//abs(m_3DViewerInfo.DnZMin + m_3DViewerInfo.DnZMax);
			glTranslatef(0,0,ViewCenter);//전체
			
			DrawBox();
			//DrawAxis();
		}
		if(m_pDnData != NULL)
		{
			glTranslated(m_3DDataAlignInfo.m_xMove,m_3DDataAlignInfo.m_yMove,m_3DDataAlignInfo.m_z1OMove);
			glVertexPointer(3, GL_FLOAT,		  0, m_pDnData->pVectorData);
			glColorPointer( 3, GL_UNSIGNED_BYTE,  0, m_pDnData->pColorData);
			glDrawElements(GL_QUADS, m_pDnData->ElementCnt, GL_UNSIGNED_INT, (GLvoid *)m_pDnData->pElementIndex);
		}
		
		//좌표계 정보 출력.
		{
			//View 좌표계 복귀
			glTranslatef(0,0,(m_3DDataAlignInfo.m_z2OMove));
			glTranslated((-1.0f)*m_3DDataAlignInfo.m_xMove,(-1.0f)*m_3DDataAlignInfo.m_yMove,(-1.0f)*m_3DDataAlignInfo.m_z1OMove);

			DrawMeaserBox(m_MeaserInfo.m_SelectAxis, m_MeaserInfo.MPositionY, m_MeaserInfo.MPositionX, m_MeaserInfo.MAngle);
			DrawBase();
		}
	}
	//////////////////////////////////////////////////////////////////////////
	glPopMatrix();
	
	glFinish();
	Get3DImage();
	SwapBuffers(wglGetCurrentDC());
	glFlush();
	
}
void COpenGL3D::Get3DImage()
{
	GLubyte * bits; //RGB bits   
	GLint viewport[4]; //current viewport   

	//get current viewport   
	glGetIntegerv(GL_VIEWPORT, viewport);   

	int w = viewport[2];   
	int h = viewport[3];   
	if(w<100 || w>1024)
		return;
	if(h<100 || h>1024)
		return;

	//////////////////////////////////////////////////////////////////////////
	bits = new GLubyte[w*3*h];   

	//read pixel from frame buffer   
	//glFinish(); //finish all commands of OpenGL   
	glPixelStorei(GL_PACK_ALIGNMENT,1); //or glPixelStorei(GL_PACK_ALIGNMENT,4);   
	glPixelStorei(GL_PACK_ROW_LENGTH, 0);   
	glPixelStorei(GL_PACK_SKIP_ROWS, 0);   
	glPixelStorei(GL_PACK_SKIP_PIXELS, 0);   
	glReadPixels(0, 0, w, h, GL_BGR_EXT, GL_UNSIGNED_BYTE, bits);   


	if(m_pReanderImage != NULL)
		cvReleaseImage(&m_pReanderImage);
	m_pReanderImage = cvCreateImage( cvSize(w,h), IPL_DEPTH_8U, 3);   
	for(int i=0; i < h; ++i)   
	{   
		for(int j=0; j < w; ++j)   
		{   
			m_pReanderImage->imageData[i*m_pReanderImage->widthStep + j*3+0] = (unsigned char)(bits[(h-i-1)*3*w + j*3+0]);   
			m_pReanderImage->imageData[i*m_pReanderImage->widthStep + j*3+1] = (unsigned char)(bits[(h-i-1)*3*w + j*3+1]);   
			m_pReanderImage->imageData[i*m_pReanderImage->widthStep + j*3+2] = (unsigned char)(bits[(h-i-1)*3*w + j*3+2]);   
		}   
	}     
	delete[] bits;    
}
void COpenGL3D::MoveObject(float xRate, float yRate)
{
	//m_ActionData.m_xMove -= 20.0f * (float)(xRate);
	m_ActionData.m_zRotat -= 20.0f *(float)(xRate);

	m_ActionData.m_yMove += 20.0f * (float)(yRate);
	RenderScreen();

}
void COpenGL3D::RotatObject(float xRate, float yRate)
{
	m_ActionData.m_xRotat -= (float)(xRate);
	m_ActionData.m_yRotat += (float)(yRate);
	RenderScreen();
	
}
void COpenGL3D::ZoomObject(float UpDownValue)
{
	float ScaleValue =  UpDownValue/10;

	m_ActionData.m_xScale += ScaleValue *m_ActionData.m_xScale;
	m_ActionData.m_yScale += ScaleValue *m_ActionData.m_yScale;
	m_ActionData.m_zScale += ScaleValue *m_ActionData.m_zScale;

	RenderScreen();
}

void COpenGL3D::XMoveObject(int MoveValue)
{
	m_3DDataAlignInfo.m_xMove = MoveValue;
	RenderScreen();
}
void COpenGL3D::YMoveObject(int MoveValue)
{
	m_3DDataAlignInfo.m_yMove = MoveValue;
	RenderScreen();
}
void COpenGL3D::Z2OMoveObject(int MoveValue)
{
	m_3DDataAlignInfo.m_z2OMove = (float)MoveValue;
	RenderScreen();
}
void COpenGL3D::Z1OMoveObject(int MoveValue)
{
	m_3DDataAlignInfo.m_z1OMove = (float)MoveValue;
	RenderScreen();
}
