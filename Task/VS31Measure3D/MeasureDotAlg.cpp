/// [2014-11-05] Charles, Added.

#include "StdAfx.h"
#include "MeasureDotAlg.h"
#include <math.h>

extern CvFont G_ImageInfoSFont;

CMeasureDotAlg g_iMeasureDotAlg;

// Dot 형 측정 알고리즘 Instance 를 반환하는 함수
CMeasureAlg *CMeasureDotAlg::GetInstance()
{
	return &g_iMeasureDotAlg;
}

CMeasureDotAlg::CMeasureDotAlg()
{
	m_nMaskType = MASK_TYPE_DOT;
	m_dParam.SetDotParam();
}

CMeasureDotAlg::~CMeasureDotAlg()
{
}

// Dot 형 측정 알고리즘을 수행하는 함수.
BOOL CMeasureDotAlg::Measure()
{
	if(CMeasureAlg::Measure() == FALSE) {
		_PrintDebugString("==== End Measure : FAIL ====");
		return FALSE;
	}

	DWORD nTotalStartTime = _SetStartTime();

	// 데이터 전처리를 수행한다.
	BOOL bResult = _PreprocessData();
	m_dTime.m_msPreprocessData = _GetElapsedTime();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Preprocess Data");
		return FALSE;
	}

	// 참조 데이터를 생성한다.
	bResult = _GenerateRefData();
	m_dTime.m_msGenerateRefData = _GetElapsedTime();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Generate Ref Data");
		return FALSE;
	}

	_SetStartTime();
	// MinCenterPoints 를 찾는다.
	bResult = _GetMinCenterPoints();
	m_dTime.m_msGetMinCenterPoints = _GetElapsedTime();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Get Min Center Points");
		return FALSE;
	}

	if(m_bCheckCornerDistance) {
		_SetStartTime();
		// Corner 이미지 일 경우, Corner hold 과 이미지 중심의 거리를 계산한다.
		bResult = _CheckCornerDistance();
		m_dTime.m_msCheckCornerDistance = _GetElapsedTime();
		if(bResult == FALSE) {
			_PrintDebugString("Fail to Check Corner Distance");
		}
	}

	_SetStartTime();
	// 측정이 가능한 Points 들을 선별하여 측정 데이터(MEASURE_DATA) 리스트를 생성한다.
	bResult = _GetMeasurePoints();
	m_dTime.m_msGetMeasurePoints = _GetElapsedTime();
	if(bResult == FALSE) {
		_PrintDebugString("Fail to Get Measure Points");
		return FALSE;
	}

	if(m_nMeasureIndex > 0) {
		// Measure Index 가 지정되어 있을 경우, 해당 데이터만 남긴다.
		MEASURE_DATA dMeasureData;
		BOOL bFound = FALSE;
		for(UINT i=0; i<m_dProc.m_dMeasure.size(); i++) {
			if(m_nMeasureIndex == (i+1)) {
				bFound = TRUE;
				memcpy(&dMeasureData, m_dProc.m_dMeasure[i], sizeof(MEASURE_DATA));
			}
			m_dProc.m_dMeasure[i]->Clear();
		}
		m_dProc.m_dMeasure.clear();

		if(bFound == FALSE) {
			return FALSE;
		}

		_AddMeasureData(dMeasureData.m_ptStart, dMeasureData.m_ptEnd);
	}

	//  각 위치별로 측정을 진행한다.
	for(UINT i=0; i<m_dProc.m_dMeasure.size(); i++) {
		_PrintDebugString("[MEASURE] : %d/%d", (i+1), m_dProc.m_dMeasure.size());

		if(_SetCurMeasureData(i) == FALSE) {
			_PrintDebugString("Fail to Set Current Measure Data (%d)", i+1);
			continue;
		}

		_SetStartTime();
		// 측정 위치의 HeightProfile 을 취득한다.
		bResult = _GetHeightProfile();
		m_dTime.m_msGetHeightProfile += _GetElapsedTime();
		if(bResult == FALSE) {
			_PrintDebugString("Fail to Get Height Profile (%d)", i+1);
			continue;
		}

		_SetStartTime();
		// HeightProfile 을 통해 데이터를 측정한다.
		bResult = _MeasureHeightProfile();
		m_dTime.m_msMeasureHeightProfile += _GetElapsedTime();
		if(bResult == FALSE) {
			_PrintDebugString("Fail to Measure Height Profile (%d)", i+1);
			continue;
		}

		_PrintDebugResultData(_GetCurMeasureResultData(), "[RESULT] : %d", (i+1));
	}

	_SetStartTime(nTotalStartTime);
	m_dTime.m_msTotalMeasure = _GetElapsedTime();

	_PrintDebugString("> Time to Preprocess Data : %d ms", m_dTime.m_msPreprocessData);
	_PrintDebugString("> Time to Generate Ref Data : %d ms", m_dTime.m_msGenerateRefData);
	_PrintDebugString("> Time to Find Min Center Points : %d ms", m_dTime.m_msGetMinCenterPoints);
	_PrintDebugString("> Time to Check Corner Distance : %d ms", m_dTime.m_msCheckCornerDistance);
	_PrintDebugString("> Time to Find Measure Points : %d ms", m_dTime.m_msGetMeasurePoints);
	_PrintDebugString("> Time to Get Height Profile : %d ms", m_dTime.m_msGetHeightProfile);
	_PrintDebugString("> Time to Measure Height Profile : %d ms", m_dTime.m_msMeasureHeightProfile);
	_PrintDebugString("> Totlal Time to Measure : %d ms", m_dTime.m_msTotalMeasure);
	_PrintDebugString("==== End Measure : SUCCESS ====");

	return TRUE;
}

// 데이터 전처리 함수.
// Height 데이터에 2D Smoothing 을 수행한다.
BOOL CMeasureDotAlg::_PreprocessData()
{
	if(CMeasureAlg::_PreprocessData() == FALSE) {
		return FALSE;
	}

	if(m_b2DDataSmoothing) {
		cvSmooth(m_dProc.m_pHeightImage, m_dProc.m_pHeightImage, CV_BLUR, m_dParam.m_nPreprocessSmoothFilterSize, m_dParam.m_nPreprocessSmoothFilterSize);
	}

	return TRUE;
}


// 참조 데이터를 구한다.
// 참조 데이터는 이진화 이미지로써 검사 영역일 경우 1의 값을, 아닐 경우 0 의 값을 가진다.
BOOL CMeasureDotAlg::_GenerateRefData()
{
	if(CMeasureAlg::_GenerateRefData() == FALSE) {
		return FALSE;
	}

	// 참조 이미지를 생성한다.
	m_dProc.m_pRefImage = cvCreateImage(cvSize(m_dProc.m_nWidth, m_dProc.m_nHeight), IPL_DEPTH_8U, 1);
	if(m_dProc.m_pRefImage == 0x00) {
		return FALSE;
	}

	CvScalar dValue = cvAvg(m_dProc.m_pHeightImage);
	double nMean = dValue.val[0];

	UINT32 nThreshold = (UINT32)cvFloor(nMean * m_dParam.m_nGenerateRefDataThreshold);

	int nSizeY = m_dProc.m_nHeight;
	int nSizeX = m_dProc.m_nWidth;
	UINT32 nValue;
	UINT32 *pSrcData = (UINT32 *)m_dProc.m_pHeightImage->imageData;
	UINT8 *pDstData = (UINT8 *)m_dProc.m_pRefImage->imageData;
	memset(pDstData, 0x00, sizeof(UINT8) * nSizeX * nSizeY);
	for(int y=0; y<nSizeY; y++) {
		for(int x=0; x<nSizeX; x++) {
			nValue = pSrcData[y*nSizeX+x];
			if(nValue > nThreshold) {
				pDstData[y*nSizeX+x] = 1;
			}
		}
	}

	// MeanLine 생성용 RefData 를 만든다.
	m_dProc.m_pRefMeanLineImage = cvCloneImage(m_dProc.m_pRefImage);
	UINT8 *pData = (UINT8 *)m_dProc.m_pRefMeanLineImage->imageData;
	for(int i=0; i<nSizeX*nSizeY; i++) {
		pData[i] = !pData[i];
	}

	CvMemStorage *dStorage = cvCreateMemStorage(0); 
	CvSeq *pContours = 0x00; 
	int nCount = cvFindContours(m_dProc.m_pRefMeanLineImage, dStorage, &pContours);
	if(nCount > 1) {
		int nAreaSize;
		std::vector<int> dArea;
		while(pContours) {
			nAreaSize = (int)cvContourArea(pContours);
			if(nAreaSize > 100) {		// Noise 성은 포함시키지 않는다.
				dArea.push_back(nAreaSize);
			}
			pContours = pContours->h_next;
		}
		std::sort(dArea.begin(), dArea.end());
		nCount = dArea.size();

		int nMidAreaSize = dArea[cvFloor(2*nCount/3)];		// 2/3 지점의 영역을 중간 크기의 대표로 삼는다.
		double nRadius = sqrt(nMidAreaSize/CV_PI);
		int nFilterSize = cvFloor(nRadius/5) * 2 - 1;
		if(nFilterSize >= 3) {
			IplConvKernel *pElement = cvCreateStructuringElementEx(nFilterSize, nFilterSize, cvFloor(nFilterSize/2.f), cvFloor(nFilterSize/2.f), CV_SHAPE_RECT, NULL);
			IplImage *pTemp = cvCloneImage(m_dProc.m_pRefMeanLineImage);
			cvMorphologyEx(m_dProc.m_pRefMeanLineImage, m_dProc.m_pRefMeanLineImage, pTemp, pElement, CV_MOP_ERODE, 1); 
			cvReleaseImage(&pTemp);
			cvReleaseStructuringElement(&pElement);
		}
	}
	cvReleaseMemStorage(&dStorage);
	for(int i=0; i<nSizeX*nSizeY; i++) {
		pData[i] = !pData[i];
	}

//	_ShowDebugBinaryImage(m_dProc.m_pRefImage, "REF_IMAGE");
//	_ShowDebugBinaryImage(m_dProc.m_pRefMeanLineImage, "REF_MEAN_LINE_)IMAGE");
	
	return TRUE;
}

// MinCenterPoints 생성 함수.
// MinCenterPoints 는 참조 이미지의 최소값을 가진 위치를 의미하며,
// 데이터 측정에 사용되는 HeightProfile 의 시작점과 끝점을 찾는데 활용된다.
BOOL CMeasureDotAlg::_GetMinCenterPoints()
{
	if(CMeasureAlg::_GetMinCenterPoints() == FALSE) {
		return FALSE;
	}

	// 가로(X)축 Line 들의 평균값을 나타내는 HorizMeanLine 생성
	m_dProc.m_pHorizMeanLine = cvCreateImage(cvSize(m_dProc.m_nWidth, 1), IPL_DEPTH_32F, 1);
	if(m_dProc.m_pHorizMeanLine == 0x00) {
		return FALSE;
	}

	// m_nMinCenterPointsThreshold 를 자동으로 계산함.
	int nNonCenterArea = cvCountNonZero(m_dProc.m_pRefMeanLineImage);
	m_dParam.m_nMinCenterPointsThreshold = (float)nNonCenterArea / (m_dProc.m_nWidth * m_dProc.m_nHeight);

	// HorizMeanLine 을 구하고, Smoothing 수행
	cvReduce(m_dProc.m_pRefMeanLineImage, m_dProc.m_pHorizMeanLine, 0, CV_REDUCE_AVG);
	cvSmooth(m_dProc.m_pHorizMeanLine, m_dProc.m_pHorizMeanLine, CV_BLUR, m_dParam.m_nMeanLineSmoothFilerSize);

	// HorizMeanLine 에서 최소값을 찾아냄. 이 최소값들이 X 좌표상의 최소값을 가지는 위치를 의미함.
	if(_FindMinCenterPoints(TRUE) == FALSE) {
		return FALSE;
	}

	if(m_dProc.m_dHorizMinCenterPoints.size() < 1) {
		_PrintDebugString("Fail to Find Center Points on Horiz Mean Line. Point Count : %d", m_dProc.m_dHorizMinCenterPoints.size());
		return FALSE;
	}

	// 양쪽 끝점의 위치 보정
	_AdjustMinCenterEndPoints(TRUE);

	// 측정 Offset 적용
	_ApplyMinCenterPointsOffset(TRUE);

//	_PrintDebugPoints(m_dProc.m_dHorizMinCenterPoints, "HorizMinCenterPoints");

	// Y 좌표상의 최소값을 가지는 위치를 찾아냄
	if(_GetVertMinCenterPoints(m_dProc.m_dHorizMinCenterPoints[0]) == FALSE) {
		return FALSE;
	}

	if(m_dProc.m_dHorizMinCenterPoints.size() >= 2) {
		// 대각 격자 패턴일 경우를 대비하여 X 축의 다음 최소값 위치를 통해 한번 더 Y 좌표 위치 검사.
		_GetVertMinCenterPoints(m_dProc.m_dHorizMinCenterPoints[1], TRUE);
	}
	
	// 양쪽 끝점의 위치 보정
	_AdjustMinCenterEndPoints(FALSE);

	// 측정 Offset 적용
	_ApplyMinCenterPointsOffset(FALSE);

//	_PrintDebugPoints(m_dProc.m_dVertMinCenterPoints, "VertMinCenterPoints");

	return TRUE;
}

// 실제 검사가 가능한 위치를 찾아내는 함수
// MinCenterPoint 들로 시작점과 끝점을 조합하여 실제 검사가 가능한 위치만 
// 측정 데이터(MEASURE_DATA)를 생성함.
BOOL CMeasureDotAlg::_GetMeasurePoints()
{
	if(m_bMeasureAll) {
		// 모든 검사일 경우 전체의 측정 위치를 찾아냄.
		UINT8 *pRefData = (UINT8 *)m_dProc.m_pRefImage->imageData;
		int nSizeX = m_dProc.m_nWidth;
		int nSizeY = m_dProc.m_nHeight;

		Points &dHorizMinCenterPoints = m_dProc.m_dHorizMinCenterPoints;
		Points &dVertMinCenterPoints = m_dProc.m_dVertMinCenterPoints;
		int nLengthHorizCenterPoints = dHorizMinCenterPoints.size();
		int nLengthVertCenterPoints = dVertMinCenterPoints.size();

		int nStartPosX = 0;
		int nStartPosY = 0;
		int nEndPosX = 0;
		int nEndPosY = 0;
		BOOL bSecondPhase = FALSE;
		BOOL bSecondPhaseOnly = FALSE;

		// X축과 Y축의 MinCenterPoint 를 모두 검사하여 검사 가능한 위치일 경우 
		// 측정 데이터(MEASURE_DATA)를 생성
		for(int j=0; j < nLengthVertCenterPoints; j++) {
			for(int i=0; i<nLengthHorizCenterPoints; i++) {
				nStartPosY = dVertMinCenterPoints[j];
				nStartPosX = dHorizMinCenterPoints[i];

				// 1 일 경우는 양 끝점이 아닌 중간 지점이므로 Pass 함.
				if(pRefData[nStartPosY*nSizeX+nStartPosX] == 1) {
					continue;
				}

				bSecondPhase = FALSE;
				bSecondPhaseOnly = TRUE;
				// 가로 라인 끝점 선택
				if(_SelectEndMinCenterPoint(nStartPosX, nStartPosY, nEndPosX, nEndPosY, bSecondPhase )) { 
					if(_AddMeasureData(cvPoint(nStartPosX, nStartPosY), cvPoint(nEndPosX, nEndPosY))) {
						bSecondPhaseOnly = FALSE;
					}
				}

				nStartPosY = dVertMinCenterPoints[j];
				nStartPosX = dHorizMinCenterPoints[i];
				bSecondPhase = TRUE;
				// 가로 라인 끝점 선택
				if(_SelectEndMinCenterPoint(nStartPosX, nStartPosY, nEndPosX, nEndPosY, bSecondPhase)) {
					_AddMeasureData(cvPoint(nStartPosX, nStartPosY), cvPoint(nEndPosX, nEndPosY), bSecondPhase, bSecondPhaseOnly);
				}
			}
		}
	}
	else {
		// 단일 검사일 경우, 이미지 중앙에 가까운 한 위치만 찾아냄.
		int nStartPosX = 0;
		int nStartPosXIndex = 0;
		// 시작 점 X 좌표 확인
		if(_SelectStartMinCenterPoint(TRUE, nStartPosX, nStartPosXIndex) == FALSE) {
			return FALSE;
		}

		int nStartPosY = 0;
		int nStartPosYIndex = 0;
		// 시작 점 Y 좌표 확인
		if(_SelectStartMinCenterPoint(FALSE, nStartPosY, nStartPosYIndex) == FALSE) {
			return FALSE;
		}

		UINT8 *pRefData = (UINT8 *)m_dProc.m_pRefImage->imageData;
		int nSizeX = m_dProc.m_nWidth;
		int nSizeY = m_dProc.m_nHeight;

		// 시작점 위치가 적합한 곳인지 판단한다.
		if(pRefData[nStartPosY*nSizeX+nStartPosX] == 1) {
			// 적합하지 않을 경우, 오른쪽/왼쪽 위치를 검사하여 적합한 곳을 찾는다.
			BOOL bFound = FALSE;
			if(nStartPosXIndex < (int)m_dProc.m_dHorizMinCenterPoints.size() - 1) {		// 오른쪽
				nStartPosX = m_dProc.m_dHorizMinCenterPoints[nStartPosXIndex+1];
				if(pRefData[nStartPosY*nSizeX+nStartPosX] < 1) {
					bFound = TRUE;
				}
			}

			if(bFound == FALSE && nStartPosXIndex > 0) {												// 왼쪽
				nStartPosX = m_dProc.m_dHorizMinCenterPoints[nStartPosXIndex-1];
				if(pRefData[nStartPosY*nSizeX+nStartPosX] < 1) {
					bFound = TRUE;
				}
			}

			if(bFound == FALSE) {
				return FALSE;
			}
		}

		int nStartPosOrigX = nStartPosX;
		int nStartPosOrigY = nStartPosY;
		int nEndPosX = 0;
		int nEndPosY = 0;

		BOOL bSecondPhase = FALSE;
		BOOL bSecondPhaseOnly = TRUE;
		// 가로 라인 끝점 선택
		if(_SelectEndMinCenterPoint(nStartPosX, nStartPosY, nEndPosX, nEndPosY, bSecondPhase)) {
			if(_AddMeasureData(cvPoint(nStartPosX, nStartPosY), cvPoint(nEndPosX, nEndPosY))) {
				bSecondPhaseOnly = FALSE;
			}
		}

		nStartPosX = nStartPosOrigX;
		nStartPosY = nStartPosOrigY;
		bSecondPhase = TRUE;
		// 세로 라인 끝점 선택
		if(_SelectEndMinCenterPoint(nStartPosX, nStartPosY, nEndPosX, nEndPosY, bSecondPhase)) {
			_AddMeasureData(cvPoint(nStartPosX, nStartPosY), cvPoint(nEndPosX, nEndPosY), bSecondPhase, bSecondPhaseOnly);
		}
	}

	if(m_dProc.m_dMeasure.size()==0) {
		return FALSE;
	}

	return TRUE;
}

// Y 축상의 최소값을 갖는 포인트(VertMinCenterPoints)를 반환한다.
// 이 함수는 두 번 호출되는 구조로, 두번째인 경우 bAddtional 이 TRUE 로 설정되어
// 이전에 찾아낸 포인트들과 합쳐 최종 결과물을 만들어 낸다.
BOOL CMeasureDotAlg::_GetVertMinCenterPoints(int nPos, BOOL bAdditional)
{
	IplImage *pTempLine = cvCreateImage(cvSize(1, m_dProc.m_nHeight), IPL_DEPTH_32F, 1);
	if(pTempLine == 0x00) {
		return FALSE;
	}

	IplImage *pPrevVertMeanLine = 0x00;
	Points dPrevVertMinCenterPoints;
	if(bAdditional) {
		pPrevVertMeanLine = cvCloneImage(m_dProc.m_pVertMeanLine);
		if(pPrevVertMeanLine == 0x00) {
			cvReleaseImage(&pTempLine);
			return FALSE;
		}

		dPrevVertMinCenterPoints = m_dProc.m_dVertMinCenterPoints;
		m_dProc.m_dVertMinCenterPoints.clear();
	}
	else {
		// 세로(Y)축 Line 들의 평균값을 나타내는 VertMeanLine 생성
		m_dProc.m_pVertMeanLine = cvCreateImage(cvSize(m_dProc.m_nHeight, 1), IPL_DEPTH_32F, 1);
		if(m_dProc.m_pVertMeanLine == 0x00) {
			cvReleaseImage(&pTempLine);
			return FALSE;
		}
	}

	// VertMinCenterPoint 는 이미지 전체를 활용하지 않고,
	// 특정 X 좌표 위치 근방의 이미지만을 대상으로 평균을 구한다.
	// (이미지 전체로 할 경우 대각 패턴에서 최소값을 찾아내지 못하는 문제가 있음)
	FLOAT *pLineData = (FLOAT *)m_dProc.m_pHorizMeanLine->imageData;
	int nLengthLine = m_dProc.m_nWidth;
	int nMinPos = nPos;
	int nMaxPos = nPos;
	float nThreshold = m_dParam.m_nMinCenterPointsThreshold;
	// 주어진 X 좌표 좌/우측으로 특정 임계치 보다 작은 영역을 찾아냄.
	while(pLineData[nMinPos] < nThreshold && nMinPos > 0) nMinPos--;
	while(pLineData[nMaxPos] < nThreshold && nMaxPos < nLengthLine-1) nMaxPos++;

	int nWidthSubImage = nMaxPos - nMinPos + 1;
	// 해당 부분의 평균값을 구하고, Smoothing 을 한다.
	cvSetImageROI(m_dProc.m_pRefMeanLineImage, cvRect(nMinPos, 0, nWidthSubImage, m_dProc.m_nHeight));
	cvReduce(m_dProc.m_pRefMeanLineImage, pTempLine, 1, CV_REDUCE_AVG);
	cvTranspose(pTempLine, m_dProc.m_pVertMeanLine);
	cvReleaseImage(&pTempLine);
	cvResetImageROI(m_dProc.m_pRefMeanLineImage);
	cvSmooth(m_dProc.m_pVertMeanLine, m_dProc.m_pVertMeanLine, CV_BLUR, m_dParam.m_nMeanLineSmoothFilerSize);

	//	_PrintDebugImage(m_dProc.m_pVertMeanLine, 50, 0, 10, 1, "VertMeanLine");

	// VertMeanLine 에서 최소값을 찾아냄. 이 최소값들이 Y 좌표상의 최소값을 가지는 위치를 의미함.
	BOOL bResult = _FindMinCenterPoints(FALSE);

	// 두번째 호출일 경우, 이전 포인트들과 결과를 합친다.
	if(bAdditional) {
		Points &dPoints1 = m_dProc.m_dVertMinCenterPoints;
		Points &dPoints2 = dPrevVertMinCenterPoints;
		int nCountPoint1 = dPoints1.size();
		int nCountPoint2 = dPoints2.size();
		// 이전 포인트들과의 거리상 오차가 작을 경우 동일한 위치로 간주한다.
		int nMinDist = m_dProc.m_nHeight/4;
		if(dPoints1.size()>=2) {
			nMinDist = (dPoints1[1] - dPoints1[0])/4;
		}
		BOOL bAddPoint = FALSE;
		for(int i=0; i<nCountPoint2; i++) {
			bAddPoint = TRUE;
			for(int j=0; j<nCountPoint1; j++) {
				if(abs(dPoints2[i] - dPoints1[j]) < nMinDist) {
					bAddPoint = FALSE;
					break;
				}
			}

			if(bAddPoint) {
				dPoints1.push_back(dPoints2[i]);
			}
		}
		std::sort(dPoints1.begin(), dPoints1.end());

		// 두개의 평균값을 넣는다.
		cvAdd(m_dProc.m_pVertMeanLine, pPrevVertMeanLine, m_dProc.m_pVertMeanLine);
		cvSet(pPrevVertMeanLine, cvScalar(2));
		cvDiv(m_dProc.m_pVertMeanLine, pPrevVertMeanLine, m_dProc.m_pVertMeanLine);

		cvReleaseImage(&pPrevVertMeanLine);
	}

	return bResult;
}


// 검사 대상 끝 포인트를 찾는다.
// 검사 시작점과 방향(가로/세로)가 주어지면 검사가 가능한 끝점이 있는지 확인한다.
BOOL CMeasureDotAlg::_SelectEndMinCenterPoint(int &nStartPosX, int &nStartPosY, int &nEndPosX, int &nEndPosY, BOOL bSecondPhase)
{
	MEASURE_DATA *pMeasureData = _GetCurMeasureData();

	int idxStartX = _GetMinCenterPointIndex(nStartPosX, TRUE);
	int idxStartY = _GetMinCenterPointIndex(nStartPosY, FALSE);

	UINT8 *pRefData = (UINT8 *)m_dProc.m_pRefImage->imageData;
	int nSizeX = m_dProc.m_nWidth;
	int nSizeY = m_dProc.m_nHeight;

	Points &dHorizMinCenterPoints = m_dProc.m_dHorizMinCenterPoints;
	Points &dVertMinCenterPoints = m_dProc.m_dVertMinCenterPoints;
	int nLengthHorizCenterPoints = dHorizMinCenterPoints.size();
	int nLengthVertCenterPoints = dVertMinCenterPoints.size();

	int nIndexX = 0;
	int nIndexY = 0;
	BOOL bFound = FALSE;
	if(m_nMeasureAngle == MEASURE_ANGLE_0) {
		if(bSecondPhase == FALSE) {
			// right position
			nEndPosY = nStartPosY;
			for(int i = idxStartX+1; i< nLengthHorizCenterPoints; i++) {
				nEndPosX = dHorizMinCenterPoints[i];
				if(pRefData[nEndPosY*nSizeX+nEndPosX] < 1) {
					return TRUE;
				}
			}

			if(m_bMeasureAll) {
				return FALSE;
			}

			// left position
			nEndPosY = nStartPosY;
			for(int i = idxStartX-1; i>=0; i--) {
				nEndPosX = dHorizMinCenterPoints[i];
				if(pRefData[nEndPosY*nSizeX+nEndPosX] < 1) {
					return TRUE;
				}
			}
		}
		else {
			// down position
			nEndPosX = nStartPosX;
			for (int i = idxStartY+1; i < nLengthVertCenterPoints; i++) {
				nEndPosY = dVertMinCenterPoints[i];
				if(pRefData[nEndPosY*nSizeX+nEndPosX] < 1) {
					return TRUE;
				}
			}

			if(m_bMeasureAll) {
				return FALSE;
			}

			// up position
			nEndPosX = nStartPosX;
			for(int 	i = idxStartY-1; i >= 0; i--) {
				nEndPosY = dVertMinCenterPoints[i];
				if(pRefData[nEndPosY*nSizeX+nEndPosX] < 1) {
					return TRUE;
				}
			}
		}
	}
	
	if(m_nMeasureAngle == MEASURE_ANGLE_45) {
		int nStartPosOrigX = nStartPosX;
		int nStartPosOrigY = nStartPosY;
		int nLengthX = 0;
		int nLengthY = 0;
		int nLength = 0;
		if(bSecondPhase == FALSE) {
			// right-down position
			nIndexX = idxStartX + 1;
			nIndexY = idxStartY + 1;
			while(nIndexX < nLengthHorizCenterPoints && nIndexY < nLengthVertCenterPoints) {
				nStartPosX = nStartPosOrigX;
				nStartPosY = nStartPosOrigY;
				nEndPosX = dHorizMinCenterPoints[nIndexX];
				nEndPosY = dVertMinCenterPoints[nIndexY];

				// 대각 방향은 X 축과 Y 축의 이동 길이를 동일하게 맞춰준다.
				nLengthX = abs(nEndPosX - nStartPosX);
				nLengthY = abs(nEndPosY - nStartPosY);
				nLength = max(nLengthX, nLengthY);
				if(nStartPosX + nLength < nSizeX) nEndPosX = nStartPosX + nLength;
				else nStartPosX = nEndPosX - nLength;
				if(nEndPosY + nLength < nSizeY) nEndPosY = nStartPosY + nLength;
				else 	nStartPosY = nEndPosY - nLength;

				if(pRefData[nStartPosY*nSizeX+nStartPosX] < 1 && pRefData[nEndPosY*nSizeX+nEndPosX] < 1) {
					return TRUE;
				}

				nIndexX++;
				nIndexY++;
			}

			if(m_bMeasureAll) {
				return FALSE;
			}

			// left-up position
			nIndexX = idxStartX - 1;
			nIndexY = idxStartY - 1;
			while(nIndexX >= 0 && nIndexY >=0 ) {
				nStartPosX = nStartPosOrigX;
				nStartPosY = nStartPosOrigY;
				nEndPosX = dHorizMinCenterPoints[nIndexX];
				nEndPosY = dVertMinCenterPoints[nIndexY];

				// 대각 방향은 X 축과 Y 축의 이동 길이를 동일하게 맞춰준다.
				nLengthX = abs(nEndPosX - nStartPosX);
				nLengthY = abs(nEndPosY - nStartPosY);
				nLength = max(nLengthX, nLengthY);
				if(nStartPosX - nLength >= 0) nEndPosX = nStartPosX - nLength;
				else nStartPosX = nEndPosX + nLength;
				if(nStartPosY - nLength >= 0) nEndPosY = nStartPosY - nLength;
				else nStartPosY = nEndPosY + nLength;

				if(pRefData[nStartPosY*nSizeX+nStartPosX] < 1 && pRefData[nEndPosY*nSizeX+nEndPosX] < 1) {
					return TRUE;
				}

				nIndexX--;
				nIndexY--;
			}
		}
		else {
			// left-down position
			nIndexX = idxStartX - 1;
			nIndexY = idxStartY + 1;
			while(nIndexX >= 0 && nIndexY < nLengthVertCenterPoints) {
				nStartPosX = nStartPosOrigX;
				nStartPosY = nStartPosOrigY;
				nEndPosX = dHorizMinCenterPoints[nIndexX];
				nEndPosY = dVertMinCenterPoints[nIndexY];

				// 대각 방향은 X 축과 Y 축의 이동 길이를 동일하게 맞춰준다.
				nLengthX = abs(nEndPosX - nStartPosX);
				nLengthY = abs(nEndPosY - nStartPosY);
				nLength = max(nLengthX, nLengthY);
				if(nStartPosX - nLength >= 0) nEndPosX = nStartPosX - nLength;
				else nStartPosX = nEndPosX + nLength;
				if(nStartPosY + nLength < nSizeY) nEndPosY = nStartPosY + nLength;
				else nStartPosY = nEndPosY - nLength;

				if(pRefData[nStartPosY*nSizeX+nStartPosX] < 1 && pRefData[nEndPosY*nSizeX+nEndPosX] < 1) {
					return TRUE;
				}

				nIndexX--;
				nIndexY++;
			}

			if(m_bMeasureAll) {
				return FALSE;
			}

			// right-up position
			nIndexX = idxStartX + 1;
			nIndexY = idxStartY - 1;
			while(nIndexX < nLengthHorizCenterPoints && nIndexY >=0 ) {
				nStartPosX = nStartPosOrigX;
				nStartPosY = nStartPosOrigY;
				nEndPosX = dHorizMinCenterPoints[nIndexX];
				nEndPosY = dVertMinCenterPoints[nIndexY];

				// 대각 방향은 X 축과 Y 축의 이동 길이를 동일하게 맞춰준다.
				nLengthX = abs(nEndPosX - nStartPosX);
				nLengthY = abs(nEndPosY - nStartPosY);
				nLength = max(nLengthX, nLengthY);
				if(nStartPosX + nLength < nSizeX) nEndPosX = nStartPosX + nLength;
				else nStartPosX = nEndPosX - nLength;
				if(nStartPosY - nLength >= 0) nEndPosY = nStartPosY - nLength;
				else nStartPosY = nEndPosY + nLength;

				if(pRefData[nStartPosY*nSizeX+nStartPosX] < 1 && pRefData[nEndPosY*nSizeX+nEndPosX] < 1) {
					return TRUE;
				}

				nIndexX++;
				nIndexY--;
			}
		}
	}

	return FALSE;
}

// HeightProfile 을 반환한다.
// 검사 시작점과 끝점을 연결한 라인의 높이 Profile 을 반환한다.
BOOL CMeasureDotAlg::_GetHeightProfile()
{
	if(CMeasureAlg::_GetHeightProfile() == FALSE) {
		return FALSE;
	}

	MEASURE_DATA *pMeasureData = _GetCurMeasureData();
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	int nStartPosX = pMeasureData->m_ptStart.x;
	int nStartPosY = pMeasureData->m_ptStart.y;
	int nEndPosX = pMeasureData->m_ptEnd.x;
	int nEndPosY = pMeasureData->m_ptEnd.y;

	int nLengthProfile = 0;
	int nLengthProfileX = abs(nEndPosX - nStartPosX) + 1;
	int nLengthProfileY = abs(nEndPosY - nStartPosY) + 1;
	if(nLengthProfileX == 1) {
		nLengthProfile = nLengthProfileY;
	}
	else if(nLengthProfileY == 1) {
		nLengthProfile = nLengthProfileX;
	}
	else {
		nLengthProfile = min(nLengthProfileX, nLengthProfileY);
	}

	// HeightProfile 를 생성한다.
	IplImage *pHeightProfile = cvCreateImage(cvSize(nLengthProfile, 1), IPL_DEPTH_32F, 1);
	if(pHeightProfile == 0x00) {
		return FALSE;
	}
	pMeasureData->m_pHeightProfile = pHeightProfile;
	FLOAT *pProfileData = (FLOAT *)pHeightProfile->imageData;
	INT32 *pHeightData = (INT32 *)m_dProc.m_pHeightImage->imageData;
	int nSizeX = m_dProc.m_nWidth;

	int nPosX = 0;
	int nPosY = 0;
	int nDeltaX = 0;
	int nDeltaY = 0;
	int nSignX = (nEndPosX - nStartPosX) == 0? 0:((nEndPosX - nStartPosX)>0? 1:-1);
	int nSignY = (nEndPosY - nStartPosY) == 0? 0:((nEndPosY - nStartPosY)>0? 1:-1);
	int nMaxValue = 0;
	for(int i = 0; i < nLengthProfile; i++) {
		nDeltaX = nSignX * i;
		nDeltaY = nSignY * i;
		nPosX = nStartPosX + nDeltaX;
		nPosY = nStartPosY + nDeltaY;
		pProfileData[i]=(FLOAT)pHeightData[nPosY*nSizeX+nPosX];
	}

//	_PrintDebugVector(pHeightProfile, 0, 10, "pProfileData");

	return TRUE;
}

// Corner Hole 과 Image 중심의 거리를 구한다.
BOOL CMeasureDotAlg::_CheckCornerDistance()
{
	Points &dHorizMinCenterPoints = m_dProc.m_dHorizMinCenterPoints;
	Points &dVertMinCenterPoints = m_dProc.m_dVertMinCenterPoints;
	int nCountHorizMinCenterPoints = dHorizMinCenterPoints.size();
	int nCountVertMinCenterPoints = dVertMinCenterPoints.size();

	if(nCountHorizMinCenterPoints < 2 || nCountVertMinCenterPoints < 2) {
		return FALSE;
	}

	int nLengthX = m_dProc.m_nWidth;
	int nLengthY = m_dProc.m_nHeight;
	float nCenterX = nLengthX/2.f;
	float nCenterY = nLengthY/2.f;

	int nIndexLeft = _CheckMeanLineMargin(m_dProc.m_pHorizMeanLine, FALSE);
	int nIndexRight = _CheckMeanLineMargin(m_dProc.m_pHorizMeanLine, TRUE);
	int nIndexTop = _CheckMeanLineMargin(m_dProc.m_pVertMeanLine, FALSE);
	int nIndexBottom = _CheckMeanLineMargin(m_dProc.m_pVertMeanLine, TRUE);

	BOOL bCornerLeft = (nIndexLeft >= 0) ? TRUE : FALSE;
	BOOL bCornerRight = (nIndexRight >= 0) ? TRUE : FALSE;
	BOOL bCornerTop = (nIndexTop >= 0) ? TRUE : FALSE;
	BOOL bCornerBottom = (nIndexBottom >= 0) ? TRUE : FALSE;

	int nCornerCount = 0;
	if(bCornerLeft) nCornerCount++;
	if(bCornerRight) nCornerCount++;
	if(bCornerTop) nCornerCount++;
	if(bCornerBottom) nCornerCount++;

	if(nCornerCount != 2) {
		return FALSE;
	}

	// 전체 이미지가 회전되었을 경우,  Corner Hole 의 시작 위치를 MeanLine 으로 구하면 오차가 발생하므로
	// Corner Hole 부근 이미지를 이용하여 시작 위치를 정확히 구한다.
	int nStartX = 0;
	int nEndX = 0;
	int nStartY = 0;
	int nEndY = 0;
	int nGapX = (dHorizMinCenterPoints[1] - dHorizMinCenterPoints[0]) / 4;
	int nGapY = (dVertMinCenterPoints[1] - dVertMinCenterPoints[0]) / 4;
	if(bCornerLeft) {
		nStartX = 0;
		nEndX = dHorizMinCenterPoints[0];
		if(bCornerTop) nStartY = dVertMinCenterPoints[0] - nGapY;
		else nStartY = dVertMinCenterPoints[nCountVertMinCenterPoints-1] - nGapY;
		nEndY = nStartY + 2*nGapY;

		IplImage *pMeanLine = _GetSubImageMeanLine(m_dProc.m_pHeightImage, nStartX, nEndX, nStartY, nEndY, TRUE); 
		if(pMeanLine == 0x00) {
			return FALSE;
		}

		nIndexLeft = _GetCornerPos(pMeanLine, FALSE);
		m_nCornerPosX = nIndexLeft;
		cvReleaseImage(&pMeanLine);
	}
	else if(bCornerRight) {
		nStartX = dHorizMinCenterPoints[nCountHorizMinCenterPoints-1];
		nEndX = nLengthX - 1;
		if(bCornerTop) nStartY = dVertMinCenterPoints[0] - nGapY;
		else nStartY = dVertMinCenterPoints[nCountVertMinCenterPoints-1] - nGapY;
		nEndY = nStartY + 2*nGapY;

		IplImage *pMeanLine = _GetSubImageMeanLine(m_dProc.m_pHeightImage, nStartX, nEndX, nStartY, nEndY, TRUE); 
		if(pMeanLine == 0x00) {
			return FALSE;
		}

		nIndexRight = _GetCornerPos(pMeanLine, TRUE);
		m_nCornerPosX = nStartX + nIndexRight;
		cvReleaseImage(&pMeanLine);
	}
	if(bCornerTop) {
		nStartY = 0;
		nEndY = dVertMinCenterPoints[0];
		if(bCornerLeft) nStartX = dHorizMinCenterPoints[0] - nGapX;
		else nStartX = dHorizMinCenterPoints[nCountHorizMinCenterPoints-1] - nGapX;
		nEndX = nStartX + 2*nGapX;

		IplImage *pMeanLine = _GetSubImageMeanLine(m_dProc.m_pHeightImage, nStartX, nEndX, nStartY, nEndY, FALSE); 
		if(pMeanLine == 0x00) {
			return FALSE;
		}

		nIndexTop = _GetCornerPos(pMeanLine, FALSE);
		m_nCornerPosY = nIndexTop;
		cvReleaseImage(&pMeanLine);
	}
	else if(bCornerBottom) {
		nStartY = dVertMinCenterPoints[nCountVertMinCenterPoints-1];
		nEndY = nLengthY - 1;
		if(bCornerLeft) nStartX = dHorizMinCenterPoints[0] - nGapX;
		else nStartX = dHorizMinCenterPoints[nCountHorizMinCenterPoints-1] - nGapX;
		nEndX = nStartX + 2*nGapX;

		IplImage *pMeanLine = _GetSubImageMeanLine(m_dProc.m_pHeightImage, nStartX, nEndX, nStartY, nEndY, FALSE); 
		if(pMeanLine == 0x00) {
			return FALSE;
		}

		nIndexBottom = _GetCornerPos(pMeanLine, TRUE);
		m_nCornerPosY = nStartY + nIndexBottom;
		cvReleaseImage(&pMeanLine);
	}

	m_nCornerDistance = sqrtf(powf(m_nCornerPosX - nCenterX, 2) + powf(m_nCornerPosY - nCenterY, 2));

	return TRUE;
}

// 결과 데이터 측정 함수. Base 함수의 내용이 그대로 DOT 형에 적용된다.
BOOL CMeasureDotAlg::_MeasureHeightProfile()
{
	if(CMeasureAlg::_MeasureHeightProfile() == FALSE) {
		return FALSE;
	}

	return TRUE;
}

// 측정 결과 데이터(MEASURE_RESULT_DATA)를 생성한다.
// TargetThick 파라미터값이 있을 경우(0보다 클 경우), 전체 높이를 파라미터 값으로 처리한다.
BOOL CMeasureDotAlg::_MakeResultData()
{
	if(CMeasureAlg::_MakeResultData() == FALSE) {
		return FALSE;
	}

	if(m_dInput.m_nTargetThick > 0.f) {
		MEASURE_RESULT_DATA *pMeasureResultData = _GetCurMeasureResultData();
		if(pMeasureResultData == 0x00) {
			return FALSE;
		}

//		pMeasureResultData->RIB_HEIGHT = (int)m_dInput.m_nTargetThick;   // 정의천//2015.04.07 꼭 확인하시오. 코멘트를 풀지 말지??
//		pMeasureResultData->RIB_HEIGHT_TOTAL_L = pMeasureResultData->RIB_HEIGHT;
//		pMeasureResultData->RIB_HEIGHT_TOTAL_R = pMeasureResultData->RIB_HEIGHT;
//		pMeasureResultData->RIB_HEIGHT_E_L = pMeasureResultData->RIB_HEIGHT - pMeasureResultData->RIB_HEIGHT_L;
//		pMeasureResultData->RIB_HEIGHT_E_R = pMeasureResultData->RIB_HEIGHT - pMeasureResultData->RIB_HEIGHT_R;

		pMeasureResultData->Validate();
	}

	return TRUE;
}

// 결과 이미지 생성 함수. Base 함수의 내용이 그대로 DOT 형에 적용된다.
BOOL CMeasureDotAlg::MakeResultImage()
{
	if(CMeasureAlg::MakeResultImage() == FALSE) {
		return FALSE;
	}

	return TRUE;
}

// 결과 전체 이미지 생성 함수.
BOOL CMeasureDotAlg::_MakeResultFullImage()
{
	if(CMeasureAlg::_MakeResultFullImage() == FALSE) {
		return FALSE;
	}

	int nCount = _GetRealMeasureResultDataCount();
	if(nCount == 0) {
		return FALSE;
	}

	if(m_bSaveAnalysisData) {
		// 분석용 결과 이미지일 경우 별도의 배경 이미지를 로드.
		m_dOutput.m_pDataImage = cvLoadImage(RESULT_VIEW_DOT_IMG_FOR_ANALYSIS);
	}
	else {
		m_dOutput.m_pDataImage = cvLoadImage(RESULT_VIEW_DOT_IMG);
	}
	if(m_dOutput.m_pDataImage == 0x00) {
		return FALSE;
	}

	// 전체 결과 이미지 생성 및 초기화
	int nSizeX = m_dOutput.m_pDataImage->width * 2;
	int nSizeY = m_dOutput.m_pDataImage->height * nCount;
	m_dOutput.m_pFullImage = cvCreateImage(cvSize(nSizeX, nSizeY), IPL_DEPTH_8U, 3);
	memset(m_dOutput.m_pFullImage->imageData, 0xFF, m_dOutput.m_pFullImage->imageSize);

	return TRUE;
}

// 높이 이미지를 생성하여 전체 이미지에 통합한다.
BOOL CMeasureDotAlg::_MakeResultHeightImage()
{
	if(CMeasureAlg::_MakeResultHeightImage() == FALSE) {
		return FALSE;
	}

	int nSizeX = m_dOutput.m_pDataImage->width;
	int nSizeY = m_dOutput.m_pDataImage->height;
	CvRect rcROI;
	rcROI.x = 0; 
	rcROI.y = 0; 
	rcROI.width = nSizeX; 
	rcROI.height = nSizeY; 

	cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
	cvResize(m_dOutput.m_pHeightImage, m_dOutput.m_pFullImage);
	cvDrawRect(m_dOutput.m_pFullImage, cvPoint(0, 0),cvPoint(rcROI.width,rcROI.height), RESULT_DATA_BOX_LINE_COLOR, 2);

	CvPoint ptStart;
	CvPoint ptEnd;
	CvPoint ptCenter;
	float  nRatioX = (float)nSizeX / m_dOutput.m_pHeightImage->width;
	float  nRatioY = (float)nSizeY / m_dOutput.m_pHeightImage->height;

	BOOL bFirstData = true;
	int nIndexY = 0;
	char szPrintMessage[256];
	int nCount = m_dProc.m_dMeasure.size();
	MEASURE_DATA *pMeasureData = 0x00;
	for(int i=0; i<nCount; i++) {
		pMeasureData = _GetMeasureData(i);
		if(pMeasureData == 0x00) {
			continue;
		}

		ptStart.x = (int)(pMeasureData->m_ptStart.x * nRatioX);
		ptStart.y = (int)(pMeasureData->m_ptStart.y * nRatioY);
		ptEnd.x = (int)(pMeasureData->m_ptEnd.x * nRatioX);
		ptEnd.y = (int)(pMeasureData->m_ptEnd.y * nRatioY);
		if(m_nMeasureIndex > 0) {
			sprintf_s(szPrintMessage, "(%d)",m_nMeasureIndex);
		}
		else {
			sprintf_s(szPrintMessage, "(%d)",i+1);
		}

		if(_IsNewRealMeasureResultData(pMeasureData)) {
			// 새로운 측정 결과일 경우, 밑으로 한 칸 더 내려서 출력할 수 있도록 한다.
			rcROI.y = nIndexY*nSizeY; 
			cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
			cvResize(m_dOutput.m_pHeightImage, m_dOutput.m_pFullImage);
			cvDrawRect(m_dOutput.m_pFullImage, cvPoint(0, 0),cvPoint(rcROI.width,rcROI.height), RESULT_DATA_BOX_LINE_COLOR, 2);
			nIndexY++;
		}
		else {
			cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
		}

		if(pMeasureData->m_bSecondPhase == FALSE) {
			// 첫번째 Phase 측정 결과일 경우 이미지 상단에 출력
			cvLine(m_dOutput.m_pFullImage, ptStart, ptEnd, HEIGHT_LINE_HORIZ_COLOR, 2);
			ptCenter.x = (ptStart.x + ptEnd.x)/2;
			ptCenter.y = (ptStart.y + ptEnd.y)/2 - 5;
			if(ptCenter.y < 0) 	ptCenter.y = (ptStart.y + ptEnd.y)/2 + 15;

			cvPutText(m_dOutput.m_pFullImage, szPrintMessage, ptCenter, &G_ImageInfoSFont, HEIGHT_LINE_HORIZ_TEXT_COLOR);
		}
		else {
			// 두번째 Phase 측정 결과일 경우 이미지 하단에 출력
			cvLine(m_dOutput.m_pFullImage, ptStart, ptEnd, HEIGHT_LINE_VERT_COLOR, 2);
			ptCenter.x = (ptStart.x + ptEnd.x)/2 + 5;
			if(ptCenter.x > nSizeX) ptCenter.x = (ptStart.x + ptEnd.x)/2 - 25;
			ptCenter.y = (ptStart.y + ptEnd.y)/2 + 5;
			cvPutText(m_dOutput.m_pFullImage, szPrintMessage, ptCenter, &G_ImageInfoSFont, HEIGHT_LINE_VERT_TEXT_COLOR);
		}

		if(m_bCheckCornerDistance && bFirstData) {
			bFirstData = FALSE;

			if(m_nCornerDistance > 0.0f) {
				ptStart.x = (int)(m_nCornerPosX * nRatioX);
				ptStart.y = (int)(m_nCornerPosY * nRatioY);
				ptEnd.x = (int)(m_dProc.m_nWidth/2.f * nRatioX);
				ptEnd.y = (int)(m_dProc.m_nHeight/2.f * nRatioY);

				// Corner Distance 대각선 
				cvLine(m_dOutput.m_pFullImage, ptStart, ptEnd, HEIGHT_LINE_CORNDER_DISTANCE_COLOR, 1);

				ptCenter.x = (ptStart.x + ptEnd.x)/2;
				ptCenter.y = (ptStart.y + ptEnd.y)/2 + 10;

				sprintf_s(szPrintMessage, "%.2f", m_nCornerDistance);
				cvPutText(m_dOutput.m_pFullImage, szPrintMessage, ptCenter, &G_ImageInfoSFont, HEIGHT_LINE_CORNDER_DISTANCE_TEXT_COLOR);

				BOOL bLeft = FALSE;
				BOOL bTop = FALSE;
				if(ptStart.x < ptEnd.x) bLeft = TRUE;
				if(ptStart.y < ptEnd.y) bTop = TRUE;

				// Cross 가로선
				if(bLeft) {
					ptStart.x = 5;
					ptEnd.x = (int)(m_dProc.m_nWidth/4.f * nRatioX);
				}
				else {
					ptStart.x = (int)(3.f*m_dProc.m_nWidth/4.f * nRatioX);
					ptEnd.x = (int)((m_dProc.m_nWidth - 5) * nRatioX);
				}
				ptStart.y = (int)(m_nCornerPosY * nRatioY);
				ptEnd.y = (int)(m_nCornerPosY * nRatioY);
				cvLine(m_dOutput.m_pFullImage, ptStart, ptEnd, HEIGHT_LINE_CORNDER_DISTANCE_CROSS_COLOR, 1);

				// Cross 세로선
				if(bTop) {
					ptStart.y = 5;
					ptEnd.y = (int)(m_dProc.m_nHeight/4.f * nRatioY);
				}
				else {
					ptStart.y = (int)(3.f*m_dProc.m_nHeight/4.f * nRatioY);
					ptEnd.y = (int)((m_dProc.m_nHeight - 5) * nRatioY);
				}
				ptStart.x = (int)(m_nCornerPosX * nRatioX);
				ptEnd.x = (int)(m_nCornerPosX * nRatioX);
				cvLine(m_dOutput.m_pFullImage, ptStart, ptEnd, HEIGHT_LINE_CORNDER_DISTANCE_CROSS_COLOR, 1);
			}
		}
	}

	cvResetImageROI(m_dOutput.m_pFullImage);

	return TRUE;
}

// 결과 데이터 이미지 부분을 생성하여 전체 이미지에 통합한다.
BOOL CMeasureDotAlg::_MakeResultDataImage()
{
	if(CMeasureAlg::_MakeResultDataImage() == FALSE) {
		return FALSE;
	}

	int nCount = m_dProc.m_dMeasure.size();
	if(nCount == 0) {
		return FALSE;
	}

	if(m_dOutput.m_pDataImage == 0x00) {
		return FALSE;
	}

	char szPrintMessage[256];
	double arrDisplayData[12] = {0.0,};

	CvPoint ptDrawDataPos[12];
	ptDrawDataPos[0] = cvPoint(318+7,83/2);//1)
	ptDrawDataPos[1] = cvPoint(325+3,500/2);//2)
	ptDrawDataPos[2] = cvPoint(208-30,468/2);//3)
	ptDrawDataPos[3] = cvPoint(440+50,468/2);//4)
	ptDrawDataPos[4] = cvPoint(88-13,263/2+40);//5)
	ptDrawDataPos[5] = cvPoint(88+5,136/2);//6)
	ptDrawDataPos[6] = cvPoint(597+25,263/2+40);//7)
	ptDrawDataPos[7] = cvPoint(606-20,136/2);//8)
	ptDrawDataPos[8] = cvPoint(190-35,180/2+12);//9)
	ptDrawDataPos[9] = cvPoint(450+50,180/2+12);//10)
	ptDrawDataPos[10] = cvPoint(34-9,290/2 );//11)
	ptDrawDataPos[11] = cvPoint(34+610,290/2 );//11)

	int ImageH = m_dOutput.m_pDataImage->height/2;
//	double arrDisplayData2[12] = {0.0,};
	CvPoint ptDrawDataPos2[12];
	ptDrawDataPos2[0]  = cvPoint(ptDrawDataPos[0].x,ptDrawDataPos[0].y+ImageH);//1)
	ptDrawDataPos2[1]  = cvPoint(ptDrawDataPos[1].x,ptDrawDataPos[1].y+ImageH);//2)
	ptDrawDataPos2[2]  = cvPoint(ptDrawDataPos[2].x,ptDrawDataPos[2].y+ImageH);//3)
	ptDrawDataPos2[3]  = cvPoint(ptDrawDataPos[3].x,ptDrawDataPos[3].y+ImageH);//4)
	ptDrawDataPos2[4]  = cvPoint(ptDrawDataPos[4].x,ptDrawDataPos[4].y+ImageH);//5)
	ptDrawDataPos2[5]  = cvPoint(ptDrawDataPos[5].x,ptDrawDataPos[5].y+ImageH);//6)
	ptDrawDataPos2[6]  = cvPoint(ptDrawDataPos[6].x,ptDrawDataPos[6].y+ImageH);//7)
	ptDrawDataPos2[7]  = cvPoint(ptDrawDataPos[7].x,ptDrawDataPos[7].y+ImageH);//8)
	ptDrawDataPos2[8]  = cvPoint(ptDrawDataPos[8].x,ptDrawDataPos[8].y+ImageH);//9)
	ptDrawDataPos2[9]  = cvPoint(ptDrawDataPos[9].x,ptDrawDataPos[9].y+ImageH);//10)
	ptDrawDataPos2[10] = cvPoint(ptDrawDataPos[10].x,ptDrawDataPos[10].y+ImageH);//11)
	ptDrawDataPos2[11] = cvPoint(ptDrawDataPos[11].x,ptDrawDataPos[11].y+ImageH);//11)

	int nSizeX = m_dOutput.m_pDataImage->width;
	int nSizeY = m_dOutput.m_pDataImage->height;
	CvRect rcROI;
	rcROI.x = nSizeX; 
	rcROI.y = 0; 
	rcROI.width = nSizeX; 
	rcROI.height = nSizeY; 

	CvPoint ptIndex;
	int nIndexY = 0;
	MEASURE_DATA *pMeasureData = 0x00;
	MEASURE_RESULT_DATA *pMeasureResultData = 0x00;
	for(int i=0; i<nCount; i++) {
		pMeasureData = _GetMeasureData(i);
		pMeasureResultData = _GetMeasureResultData(i);
		if(pMeasureData == 0x00 || pMeasureResultData == 0x00) {
			continue;
		}

		int nDataIndex=0;
		//1번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH/1000.0;
		//2번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_C/1000.0;
		//3번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_L/1000.0;
		//4번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_R/1000.0;
		//5번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_L/1000.0;
		//6번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_E_L/1000.0;
		//7번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_R/1000.0;
		//8번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_E_R/1000.0;
		//9번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_ANGLE_L;
		//10번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_ANGLE_R;
		//11번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_TOTAL_L/1000.0;
		//12번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_TOTAL_R/1000.0;

		if(_IsNewRealMeasureResultData(pMeasureData)) {
			// 새로운 측정 결과일 경우, 밑으로 한 칸 더 내려서 출력할 수 있도록 한다.
			rcROI.y = nIndexY*nSizeY; 
			cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
			cvCopy(m_dOutput.m_pDataImage, m_dOutput.m_pFullImage);
			cvDrawRect(m_dOutput.m_pFullImage, cvPoint(0, 0),cvPoint(rcROI.width,rcROI.height), RESULT_DATA_BOX_LINE_COLOR, 2);
			nIndexY++;
		}
		else {
			cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
		}

		//////////////////////////////////////////////////////////////////////////
		for(int nDrawStep = 0; nDrawStep<12; nDrawStep++)
		{
			if(nDrawStep == 8 || nDrawStep ==9) {
				sprintf_s(szPrintMessage, "(%d) %0.2f' ",(nDrawStep+1), arrDisplayData[nDrawStep]);
			} else {
				sprintf_s(szPrintMessage, "(%d) %0.2fum",(nDrawStep+1), arrDisplayData[nDrawStep]);
			}

			if(pMeasureData->m_bSecondPhase == FALSE) {
				// 첫번째 Phase 측정 결과일 경우 이미지 상단에 출력
				PutImage_Text_Small(m_dOutput.m_pFullImage, ptDrawDataPos[nDrawStep], szPrintMessage);
			}
			else {
				// 두번째 Phase 측정 결과일 경우 이미지 하단에 출력
				PutImage_Text_Small(m_dOutput.m_pFullImage, ptDrawDataPos2[nDrawStep], szPrintMessage);
			}
		}

		if(pMeasureData->m_bSecondPhase == FALSE) ptIndex = cvPoint(70, 23);
		else ptIndex = cvPoint(70, 293);

		if(m_nMeasureIndex > 0) {
			sprintf_s(szPrintMessage, "(%d)", m_nMeasureIndex);
		}
		else {
			sprintf_s(szPrintMessage, "(%d/%d)",i+1,nCount);
		}
		PutImage_Text(m_dOutput.m_pFullImage, ptIndex, szPrintMessage, CV_RGB(0,0,0), CV_RGB(255,255,255), CV_RGB(255,255,255));

		cvResetImageROI(m_dOutput.m_pFullImage);
	}

	return TRUE;
}

// 분석용 결과 데이터 이미지 부분을 생성하여 전체 이미지에 통합한다.
BOOL CMeasureDotAlg::_MakeAnalysisResultDataImage()
{
	if(CMeasureAlg::_MakeAnalysisResultDataImage() == FALSE) {
		return FALSE;
	}

	int nCount = m_dProc.m_dMeasure.size();
	if(nCount == 0) {
		return FALSE;
	}

	if(m_dOutput.m_pDataImage == 0x00) {
		return FALSE;
	}

	char szPrintMessage[256];
	double arrDisplayData[12] = {0.0,};

	CvPoint ptDrawDataPos[12];
	ptDrawDataPos[0] = cvPoint(318+7,83/2);//1)
	ptDrawDataPos[1] = cvPoint(325+3,500/2);//2)
	ptDrawDataPos[2] = cvPoint(208-30,468/2);//3)
	ptDrawDataPos[3] = cvPoint(440+50,468/2);//4)
	ptDrawDataPos[4] = cvPoint(88-13,263/2+40);//5)
	ptDrawDataPos[5] = cvPoint(88+5,136/2);//6)
	ptDrawDataPos[6] = cvPoint(597+25,263/2+40);//7)
	ptDrawDataPos[7] = cvPoint(606-20,136/2);//8)
	ptDrawDataPos[8] = cvPoint(140-35,180/2+12);//9)
	ptDrawDataPos[9] = cvPoint(550+50,180/2+12);//10)
	ptDrawDataPos[10] = cvPoint(34-9,290/2 );//11)
	ptDrawDataPos[11] = cvPoint(34+610,290/2 );//11)

	int ImageH = m_dOutput.m_pDataImage->height/2;
	//	double arrDisplayData2[12] = {0.0,};
	CvPoint ptDrawDataPos2[12];
	ptDrawDataPos2[0]  = cvPoint(ptDrawDataPos[0].x,ptDrawDataPos[0].y+ImageH);//1)
	ptDrawDataPos2[1]  = cvPoint(ptDrawDataPos[1].x,ptDrawDataPos[1].y+ImageH);//2)
	ptDrawDataPos2[2]  = cvPoint(ptDrawDataPos[2].x,ptDrawDataPos[2].y+ImageH);//3)
	ptDrawDataPos2[3]  = cvPoint(ptDrawDataPos[3].x,ptDrawDataPos[3].y+ImageH);//4)
	ptDrawDataPos2[4]  = cvPoint(ptDrawDataPos[4].x,ptDrawDataPos[4].y+ImageH);//5)
	ptDrawDataPos2[5]  = cvPoint(ptDrawDataPos[5].x,ptDrawDataPos[5].y+ImageH);//6)
	ptDrawDataPos2[6]  = cvPoint(ptDrawDataPos[6].x,ptDrawDataPos[6].y+ImageH);//7)
	ptDrawDataPos2[7]  = cvPoint(ptDrawDataPos[7].x,ptDrawDataPos[7].y+ImageH);//8)
	ptDrawDataPos2[8]  = cvPoint(ptDrawDataPos[8].x,ptDrawDataPos[8].y+ImageH);//9)
	ptDrawDataPos2[9]  = cvPoint(ptDrawDataPos[9].x,ptDrawDataPos[9].y+ImageH);//10)
	ptDrawDataPos2[10] = cvPoint(ptDrawDataPos[10].x,ptDrawDataPos[10].y+ImageH);//11)
	ptDrawDataPos2[11] = cvPoint(ptDrawDataPos[11].x,ptDrawDataPos[11].y+ImageH);//11)

	int nSizeX = m_dOutput.m_pDataImage->width;
	int nSizeY = m_dOutput.m_pDataImage->height;
	CvRect rcROI;
	rcROI.x = nSizeX; 
	rcROI.y = 0; 
	rcROI.width = nSizeX; 
	rcROI.height = nSizeY; 

	CvRect rcView;
	CvPoint ptIndex;
	int nIndexY = 0;
	MEASURE_DATA *pMeasureData = 0x00;
	MEASURE_RESULT_DATA *pMeasureResultData = 0x00;
	for(int i=0; i<nCount; i++) {
		pMeasureData = _GetMeasureData(i);
		pMeasureResultData = _GetMeasureResultData(i);
		if(pMeasureData == 0x00 || pMeasureResultData == 0x00) {
			continue;
		}

		int nDataIndex=0;
		//1번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH/1000.0;
		//2번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_C/1000.0;
		//3번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_L/1000.0;
		//4번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_WIDTH_R/1000.0;
		//5번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_L/1000.0;
		//6번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_E_L/1000.0;
		//7번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_R/1000.0;
		//8번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_E_R/1000.0;
		//9번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_ANGLE_L;
		//10번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_ANGLE_R;
		//11번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_TOTAL_L/1000.0;
		//12번 거리
		arrDisplayData[nDataIndex++] = pMeasureResultData->RIB_HEIGHT_TOTAL_R/1000.0;

		if(_IsNewRealMeasureResultData(pMeasureData)) {
			// 새로운 측정 결과일 경우, 밑으로 한 칸 더 내려서 출력할 수 있도록 한다.
			rcROI.y = nIndexY*nSizeY; 

			cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
			cvCopy(m_dOutput.m_pDataImage, m_dOutput.m_pFullImage);
			cvDrawRect(m_dOutput.m_pFullImage, cvPoint(0, 0),cvPoint(rcROI.width,rcROI.height), RESULT_DATA_BOX_LINE_COLOR, 2);
			nIndexY++;
		}
		else {
			cvSetImageROI(m_dOutput.m_pFullImage, rcROI);
		}

		rcView = rcROI;
		if(pMeasureData->m_bSecondPhase) 	rcView.y += rcROI.height/2;

		rcView.height /= 2;
		int nReduceWidth = (int)(rcView.width * 0.2f);
		int nReduceHeight = (int)(rcView.height * 0.2f);
		rcView.x += nReduceWidth;
		rcView.y += nReduceHeight;
		rcView.width -= (2*nReduceWidth);
		rcView.height -= (2*nReduceHeight);
		// HeightProfile 을 그린다.
		_DrawHeightProfile(m_dOutput.m_pFullImage, rcView, pMeasureData);

		//////////////////////////////////////////////////////////////////////////
		for(int nDrawStep = 0; nDrawStep<12; nDrawStep++)
		{
			if(nDrawStep == 8 || nDrawStep ==9) {
				sprintf_s(szPrintMessage, "(%d) %0.2f' ",(nDrawStep+1), arrDisplayData[nDrawStep]);
			} else {
				sprintf_s(szPrintMessage, "(%d) %0.2fum",(nDrawStep+1), arrDisplayData[nDrawStep]);
			}

			if(pMeasureData->m_bSecondPhase == FALSE) {
				// 첫번째 Phase 측정 결과일 경우 이미지 상단에 출력
				PutImage_Text_Small(m_dOutput.m_pFullImage, ptDrawDataPos[nDrawStep], szPrintMessage);
			}
			else {
				// 두번째 Phase 측정 결과일 경우 이미지 하단에 출력
				PutImage_Text_Small(m_dOutput.m_pFullImage, ptDrawDataPos2[nDrawStep], szPrintMessage);
			}
		}

		if(pMeasureData->m_bSecondPhase == FALSE) ptIndex = cvPoint(70, 23);
		else ptIndex = cvPoint(70, 293);

		if(m_nMeasureIndex > 0) {
			sprintf_s(szPrintMessage, "(%d)", m_nMeasureIndex);
		}
		else {
			sprintf_s(szPrintMessage, "(%d/%d)",i+1,nCount);
		}
		PutImage_Text(m_dOutput.m_pFullImage, ptIndex, szPrintMessage, CV_RGB(0,0,0), CV_RGB(255,255,255), CV_RGB(255,255,255));

		cvResetImageROI(m_dOutput.m_pFullImage);
	}

	return TRUE;
}
