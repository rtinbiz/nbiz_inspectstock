#pragma once

static int		G_MasterTaskIndex = 10;
//Observation Application Link State.
typedef enum
{
	ObserveSW_Init =-1,				//<<< 초기 상태
	ObserveSW_None =0,				//<<< Observation APP이 구동되지 않았다.
	ObserveSW_OK ,					//<<< Observation APP이 정상 구동중이다.
	ObserveSW_CtrlPowerOff,			//<<< Observation APP는 구동중이나 Controller가 Off 상태 이다.
	ObserveSW_NoneRemote,			//<<< Observation APP와 Controller는 구동중 이지만 Remote Mode가 아니다.
	////////
	ObserveSW_ManualMode			//<<< Main UI에서 Manual Mode로 변환 한것이다.
}ObsAppState;

//Observation App 동작상태 (ObserveSW_OK APP상태에서만 Check.)
typedef enum
{
	ActionObs_Init =-2,						//<<< 초기 상태
	ActionObs_Manual =-1,					//<<< ObserveSW_ManualMode일때는 상태 Check를 하지 않는다.
	ActionObs_None  =0,						//<<< 동작 명령 수행중이 아닐때.

	//ActionObs_AutoGain,						//<<< Auto Gain 동작 수행시.
	ActionObs_AutoFocus,					//<<< Auto Focus 동작 수행시.
	//ActionObs_AutoUpDownPosSetting,			//<<< 측정 Upper/Lower 위치 선정 수행시.
	ActionObs_AutoUpDownPosSetByImg,		//<<< 측정 Upper/Lower 위치 선정 수행시.
	ActionObs_StartMeasurement,				//<<< 3D Scan 수행시.
	ActionObs_AutoSaveData,					//<<< 측정 Data 저장 수행시.

	//ActionObs_AutoGain_Error,				//<<< Auto Gain 동작 수행시 Error.
	ActionObs_AutoFocus_Error,				//<<< Auto Focus 동작 수행시 Error.
	//ActionObs_AutoUpDownPosSet_Error,		//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
	ActionObs_AutoUpDownPosSetByImg_Error,	//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
	ActionObs_StartMeasurement_Error,		//<<< 3D Scan 수행시 Error.
	ActionObs_AutoSaveData_Error,			//<<< 측정 Data 저장 수행시 Error.

	ActionObs_RemoteMode_Error//<<< 측정 가능 Remote Mode가 아니다.
}ObsActionState;


typedef enum
{ 
	Run_None = 0,				//동작진행 없음(진행중이라면 Step이 종료됨)
	//단동작.
	//Run_AutoGain,			//Auto Gain 동작 수행.
	Run_AutoFocus,				//Auto Focus 동작 수행.
	//Run_AutoUpDownPosSetting,	//Auto Upper/Lower Position Setting
	Run_AutoUpDownPosSetByImg,	//Auto Upper/Lower Position Setting By Image Process.
	Run_StartMeasurement,		//3d 측정을 시작.
	Run_AutoSaveData,			//완료된 측정 Data를 자동 저장.(내부 포멧을 따로 갖는다)
	Run_ImageGrab,
	//연속 동작
	Run_StartMeaserSequence =100 //동작 정의 Sequence를 실행 한다.
}Act_Tyep;


typedef struct ResultDataFormat_ATG
{//기본 거리 단위nm Nano
	int P_Z_UPPER_L;
	int P_Z_LOWER_L;
	int P_PITCH;
	int P_Z_MEA_DIS;

	int P_ND_FILTER;
	int P_LASER_BR;

	int RIB_WIDTH;
	int RIB_WIDTH_C;
	int RIB_WIDTH_L;
	int RIB_WIDTH_R;
	int RIB_HEIGHT_L;
	int RIB_HEIGHT_E_L;
	int RIB_HEIGHT_R;
	int RIB_HEIGHT_E_R;
	float RIB_ANGLE_L;
	float RIB_ANGLE_R;
	int RIB_HEIGHT;

	int RIB_WIDTH2;
	int RIB_WIDTH_C2;
	int RIB_WIDTH_L2;
	int RIB_WIDTH_R2;
	int RIB_HEIGHT_L2;
	int RIB_HEIGHT_E_L2;
	int RIB_HEIGHT_R2;
	int RIB_HEIGHT_E_R2;
	float RIB_ANGLE_L2;
	float RIB_ANGLE_R2;
	int RIB_HEIGHT2;

}RESULT_DATA;


