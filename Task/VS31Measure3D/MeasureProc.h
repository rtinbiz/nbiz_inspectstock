#pragma once
#include "afxwin.h"
#include "afxcmn.h"


#include "MacSliderCtrl.h"
typedef struct DLineData_TAG
{
	CvPoint StartPoint;
	CvPoint EndPoint;


	DLineData_TAG()
	{
		memset(this, 0x00, sizeof(CvPoint)*2);
	}

}DLineData;


typedef struct DataNodeObj_AGT
{
	DataNodeObj_AGT *	m_pFrontNode;
	DataNodeObj_AGT *	m_pNextNode;

	MEASER_2D			InpectObj;


	DataNodeObj_AGT()
	{
		m_pFrontNode	= NULL;
		m_pNextNode		= NULL;
	}

}DataNodeObj;
//////////////////////////////////////////////////////////////////////////
//Sen Command Queue./ Receive Data Queue.
class ProcMsgQ  
{
public:
	ProcMsgQ();
	~ProcMsgQ();
private:
	DataNodeObj m_HeadNode;
	DataNodeObj m_TailNode; 
	CRITICAL_SECTION m_CrtSection;
	int		m_CntNode;
public:
	int		QGetCnt(){return m_CntNode;};
	void	QPutNode(MEASER_2D	*pNewObj);
	MEASER_2D QGetNode();
	void	QClean(void);

	double	QGetAvrHight();
	double	QGetMinHight();
	double	QGetAvrWidth(double PixelPerNano);
	int			QGetAvrWidth_WidthPixel();
	int		QSortByCenterX(double PixelPerNano);//거리 속성이 몇개 인지를 반환.


	void QRemoveAvrBigW(double PixelPerNano);
};

void MEASER_2D_BubbleSort(MEASER_2D *pTargetList, int ListCount);


#define  MAX_TOP_FIND_COUNT		10

// CMeasureProc 대화 상자입니다.

class CMeasureProc : public CDialog
{
	DECLARE_DYNAMIC(CMeasureProc)

public:
	CMeasureProc(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMeasureProc();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_IMG_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
protected:
	virtual void PostNcDestroy();
public:
	CRect			m_RectViewStick;
	HDC  			m_HDcViewStick;

	CRect			m_RectViewDot;
	HDC  			m_HDcViewDot;

	HWND			m_3DViewHandl;

	BOOL			m_showResult;
public:
	int		 m_MaskType;

	float *m_pBaseDepth;
	double		m_PixelPerNano;
	IplImage *m_pAxis_V_CalcImg;
	IplImage *m_pAxis_H_CalcImg;
	DATA_3D_RAW *m_pNewData_3D;
	//LRESULT OnLoad2DData(WPARAM wParam, LPARAM lParam);
	DWORD  m_ScanTime;
	int			m_MasureIndex;
	float		m_MeasurDotAngle;
	char		m_strSavePath[MAX_PATH];

	RESULT_DATA *m_pResultBuf;
	BOOL		m_bMeasureEditResult;
	int fn_SetProcData(int MaskType, DATA_3D_RAW *pNewData_3D, RESULT_DATA *pResultBuf, int FilterLevel);
	int fn_ProcDataMeasure(CvPoint CheckCenter, BOOL NoDrawRet = FALSE);
		//Stripe Type
		BOOL m_StripMagnet;
		int fn_ProcStrip(DATA_3D_RAW *pNewData_3D, CvPoint MPoint);
			int fn_SetMeasureValue_Strip( MEASER_2D *pFindMeasurObj);
			int fn_SetMeasureValue_MagnetStrip( MEASER_2D *pFindMeasurObj);
			void MeaserAreaStickType(int PositionY, MEASER_2D *pResultBuf);
			void MeaserAreaStickType_Magnet(int PositionY, MEASER_2D *pResultBuf, int *pDepthDataList);
			void DrawProcLineStrip(IplImage *pDrawTarget, MEASER_2D *pResultBuf, BOOL bMeaserEdit = FALSE);
			void DrawProcLineStrip_Magnet(IplImage *pDrawTarget, MEASER_2D *pResultBuf, int *pDepthDataList, BOOL bMeaserEdit = FALSE);

		//Dot Type(함수 구조 변경 2013.01.18)
		int fn_ProcDot(DATA_3D_RAW *pNewData_3D, CvPoint MPoint);
			int fn_SetMeasureValue_Dot( MEASER_2D *pFindMeasurObjX, MEASER_2D *pFindMeasurObjY);
			CvPoint FindHoleAreaDotType(DATA_3D_RAW *pNewData_3D, long HoleDetectDepth, BYTE GrayHoleThreshold);
			void MeaserAreaDotType(int *pCntMeasure, MEASER_2D *pResultBuf, int *pDepthDataList, IplImage *pTragetImg); 
			void DrawProcLineDot(IplImage *pDrawTarget, MEASER_2D *pResultBuf, int *pDepthDataList, BOOL bMeaserEdit = FALSE);

	int fn_ResultImgCreate( IplImage *pSumImg, IplImage *p3DImage, IplImage *pResutImage, IplImage *pCheckPointImg, IplImage *pCheckPointImg2);

	int	fn_ProcDataResultDraw();

	void DrawMeaserData();



	

	int m_DepthData_H[1024];
	int m_DepthData_V[1024];

	IplImage	*	m_pSResultViewImg;
	CRect			m_Rect3DViewArea;
	HDC  			m_HDC3DView;
	void DrawResultStripe(  IplImage *pNewImage = NULL);
	void DrawResultStripe_Magnet(  IplImage *pNewImage = NULL);

	IplImage	*	m_pDResultViewImg;
	CRect			m_RectResultViewArea;
	HDC  			m_HDCResultView;
	void DrawResultDot(IplImage *pNewImage = NULL);
	
	
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	////////////////////////////////////////////////////////////////////////////
	//IplImage	*	m_pMEditHImg;
	//CRect		m_MeaserEditH;
	//CRect		m_MCheck_HP[6];
	//HDC  		m_HDCMEdit_HView;

	//IplImage	*	m_pMEditVImg;
	//CRect		m_MeaserEditV;
	//CRect		m_MCheck_VP[6];
	//HDC  		m_HDCMEdit_VView;

	//MEASER_2D m_ORGMeasurePosX;
	//MEASER_2D m_ORGMeasurePosY;

	//MEASER_2D m_EditMeasurePosX;
	//MEASER_2D m_EditMeasurePosY;
	//void DrawMeasureBar(IplImage *pTarget, MEASER_2D *pMeasurePos, int BOLDIndex);
	//void DrawTraceMEdit_H(int BOLDIndex = -1);
	//void DrawTraceMEdit_V(int BOLDIndex = -1);


	//////////////////////////////////////////////////////////////////////////
public:
	COpenGL3D m_3DViewObj;

	BOOL m_LButtonDown;
	BOOL m_RButtonDown;
	CPoint OldMovePoint;
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//////////////////////////////////////////////////////////////////////////
	void m_fnFFTFilter(IplImage*pDepthImg, int DO, int N);
	int fn_Load3DData(int MaskType, DATA_3D_RAW *pNewData_3D, RESULT_DATA *pResultBuf, int FilterLevel = 17);
	LRESULT OnMaserPostion(WPARAM wParam, LPARAM lParam);

	
	//////////////////////////////////////////////////////////////////////////
	CScrollBar m_SBAxisX;
	CScrollBar m_SBAxisY;
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	CvPoint	m_MaserDefaultPos;
	afx_msg void OnBnClickedBtDefaultPos();
	afx_msg void OnBnClickedBtComplete();

	MEASER_2D m_MeaserH_Edit;
	MEASER_2D m_MeaserV_Edit;

	CMacSliderCtrl m_slider_H1;
	CMacSliderCtrl m_slider_H2;
	CMacSliderCtrl m_slider_H3;
	CMacSliderCtrl m_slider_H4;
	CMacSliderCtrl m_slider_H5;
	CMacSliderCtrl m_slider_H6;

	CMacSliderCtrl m_slider_V1;
	CMacSliderCtrl m_slider_V2;
	CMacSliderCtrl m_slider_V3;
	CMacSliderCtrl m_slider_V4;
	CMacSliderCtrl m_slider_V5;
	CMacSliderCtrl m_slider_V6;
	afx_msg void OnBnClickedBtCancel();
	afx_msg void OnBnClickedBtResult();


	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btResultView;
	CRoundButton2 m_btCancel;
	CRoundButton2 m_btComplete;
};
