// VS31Measure3DDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VS31Measure3D.h"
#include "VS31Measure3DDlg.h"
#include "MeasurementParameterList.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
BOOL OnlyOneUpdatePos = FALSE;
/*
*	Module Name		:	G_fnCheckDirAndCreate
*	Parameter		:	Dir Check Path.
*	Return			:	TRUE/FALSE
*	Function		:	Drive상에 경로 존재 여부와 경로생성을 하게 된다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
/*BOOL G_fnCheckDirAndCreate(char * pCheckPath)
{
	char CreateDir[512] = {0,};
	memcpy(CreateDir, pCheckPath, strlen(pCheckPath));
	//뒤에서부터 '\'를 체크한다.(대표이름 자르기)
	for(size_t DirPos= strlen(pCheckPath);  DirPos > 0 && CreateDir[DirPos] != '\\';DirPos--)
	{
		CreateDir[(int)DirPos] = 0;
	}

	DWORD Error = 0;
	::SetLastError(0);

	//앞에서 부터 '\'를체크하여 Directiory를 만든다.
	for(size_t i = 0; CreateDir[(int)i] != 0 ; i++)
	{
		if(CreateDir[(int)i] == '\\')
		{
			char CreateDirBuff[512] = {0,};
			memcpy(CreateDirBuff, CreateDir, i);
			::CreateDirectory(CreateDirBuff, NULL);
			Error = ::GetLastError();
			if(ERROR_INVALID_NAME == Error)//Name error
				return FALSE;
			//ERROR_ALREADY_EXISTS no error
		}
	}
	return TRUE;
}*/
//Sleep -> WaitTime
void WaitTime(DWORD dwMillisecond)
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();
	while(GetTickCount() - dwStart < dwMillisecond)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}



CInterServerInterface* G_pServerInterface = NULL;
PER_PROCESS_INIT	* G_pProcInitVal	 = NULL;
void __stdcall OnHardwareEventCallback(VKHardwareEventId eventId, void* pvUserParam)
{
	VKHardwareEventId vkHardwareEventId = static_cast<VKHardwareEventId>(eventId);
	switch(vkHardwareEventId)
	{
	case PmtProtectionDetected:
		if(G_pServerInterface != NULL)
		{
			G_pServerInterface->m_fnPrintLog(G_pProcInitVal->m_nTaskNum ,LOG_LEVEL_1, "Failed to register.");
		}
		break;
	}
}
//inline void SetGridCellBackColor(CGridCtrl *pTargetGrid, int Row, int Col, COLORREF clr)
//{
//	GV_ITEM Item;
//
//	//Item.mask = GVIF_TEXT;
//	Item.row = Row;
//	Item.col = Col;
//	Item.crBkClr = clr;             // or - m_Grid.SetItemBkColour(row, col, clr);
//	Item.crFgClr = RGB(255,0,0);    // or - m_Grid.SetItemFgColour(row, col, RGB(255,0,0));				    
//	Item.mask    = (GVIF_BKCLR);
//
//	pTargetGrid->SetItem(&Item);
//}

RpdPitch ConvertIndexToRpdPitch(int index)
{
	switch(index)
	{
	case 0:
		return RpdPitch_Normal;
	case 1:
		return RpdPitch_HighSpeed;
	default:
		return RpdPitch_Normal;
	}
}

int ConvertRpdPitchToIndex(RpdPitch rpdPitch)
{
	switch(rpdPitch)
	{
	case RpdPitch_Normal:
		return 0;
	case RpdPitch_HighSpeed:
		return 1;
	default:
		return 0;
	}
}

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
//	ON_WM_VSCROLL()
//	ON_WM_GETMINMAXINFO()
//	ON_WM_NCLBUTTONDOWN()
END_MESSAGE_MAP()


// CVS31Measure3DDlg 대화 상자




CVS31Measure3DDlg::CVS31Measure3DDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVS31Measure3DDlg::IDD, pParent)
	, m_nTargetThickEdit(0)
	, m_nSpaceGapEdit(0.f)
	, m_nMeasureIndex(0)
	, m_nMeasureOffsetXEdit(0)
	, m_nMeasureOffsetYEdit(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pResultColorViewData	= NULL;
	m_pHDcView				= NULL;

	m_ObserAPPStart			= FALSE;
	m_ObservationAPPState	= ObserveSW_Init;
	// for test
	//	m_ObservationAPPState	= ObserveSW_ManualMode;
	m_ObsActionState		= ActionObs_None;
	m_ObsActOLDState		= ActionObs_Init;//최초 무조건 갱신을 위해


	m_DetectThreshold		= 40;
	m_DetectCountResult		= 0;
	//////////////////////////////////////////////////////////////////////////
	//	m_pView3D		= NULL;

	m_MaskType		= MASK_TYPE_STRIPE;

	m_BaseDepth = 0.0f;

	/// [2014-11-05] Charles, Added.
	m_pMeasureAlg = 0x00;
	m_szDataFile[0] = 0x00;
	m_szBatchDataFile[0] = 0x00;
	m_nMeasureAngle = MEASURE_ANGLE_0;
	m_nSpaceGap = 0.f;
	m_nCountResultData = 0;
	m_plistResultData = 0x00;
	m_bMeasureAll = FALSE;
	m_nMeasureIndex = 0;
	m_nMeasureOffsetX = 0;
	m_nMeasureOffsetY = 0;
	m_nCornerDistance = 0.0f;
}

void CVS31Measure3DDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ST_PIC_RUN, m_stMeasurRun);
	DDX_Control(pDX, IDC_LOG_LIST, m_listLogView);
	DDX_Control(pDX, IDC_BUTTON_TOGGLE_MANUAL, m_btChangeMode);
	DDX_Control(pDX, IDC_BUTTON_DOAUTOFOCUS, m_btRunAF);
	DDX_Control(pDX, IDC_BUTTON_MEASER_SEQUNECE, m_btStartMeaser);
	DDX_Control(pDX, IDC_BUTTON_STOPMEASUREMENT, m_btStopMeasure);
	DDX_Control(pDX, IDC_BUTTON_RESET_ACTION_STATE, m_btAlarmReset);
	DDX_Control(pDX, IDC_BT_LOAD_PARAM_LIST, m_btViewParam);
	DDX_Control(pDX, IDC_BUTTON_LOAD_RAW3D_DATA, m_btLoadRawDataFile);
	DDX_Control(pDX, IDC_BUTTON_LOAD_RAW3D_DATA2, m_btLoadRawDataFile2);
	DDX_Control(pDX, IDC_BUTTON_MEASURE_3D_DATA_FILE, m_btMeasure3DDataFile);
	DDX_Control(pDX, IDC_CB_FILTER_LEVEL, m_cbFilterLevelList);
	DDX_Control(pDX, IDC_BT_LENS_1, m_btLensSelect[0]);
	DDX_Control(pDX, IDC_BT_LENS_2, m_btLensSelect[1]);
	DDX_Control(pDX, IDC_BT_LENS_3, m_btLensSelect[2]);
	DDX_Control(pDX, IDC_BT_LENS_4, m_btLensSelect[3]);

	DDX_Text(pDX, IDC_ED_TARGET_THICK, m_nTargetThickEdit);
	DDX_Text(pDX, IDC_ED_SPACE_GAP, m_nSpaceGapEdit);
	DDX_Control(pDX, IDC_BUTTON_MEASURE_BATCH_DATA, m_btMeasureBatchData);
	DDX_Text(pDX, IDC_ED_MEASURE_INDEX, m_nMeasureIndexEdit);
	DDX_Text(pDX, IDC_ED_MEASURE_OFFSET_X, m_nMeasureOffsetXEdit);
	DDX_Text(pDX, IDC_ED_MEASURE_OFFSET_Y, m_nMeasureOffsetYEdit);
	DDX_Text(pDX, IDC_CORNER_DISTANCE, m_nCornerDistance);
}

BEGIN_MESSAGE_MAP(CVS31Measure3DDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

	ON_MESSAGE(WM_USER_CALLBACK, &CVS31Measure3DDlg::AOI_fnMessageCallback)
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()

	ON_BN_CLICKED(IDC_BT_LOAD_PARAM_LIST, &CVS31Measure3DDlg::OnBnClickedBtLoadParamList)
	ON_BN_CLICKED(IDC_BUTTON_STARTMEASUREMENT, &CVS31Measure3DDlg::OnBnClickedButtonStartmeasurement)
	ON_BN_CLICKED(IDC_BUTTON_STOPMEASUREMENT, &CVS31Measure3DDlg::OnBnClickedButtonStopmeasurement)
	ON_BN_CLICKED(IDC_BUTTON_DOAUTOGAIN, &CVS31Measure3DDlg::OnBnClickedButtonDoautogain)
	ON_BN_CLICKED(IDC_BUTTON_DOAUTO_UP_DN, &CVS31Measure3DDlg::OnBnClickedButtonDoautoUpDn)
	ON_BN_CLICKED(IDC_BUTTON_DOAUTOFOCUS, &CVS31Measure3DDlg::OnBnClickedButtonDoautofocus)
	ON_BN_CLICKED(IDC_BUTTON_TOGGLE_MANUAL, &CVS31Measure3DDlg::OnBnClickedButtonToggleManual)
	ON_BN_CLICKED(IDC_BUTTON_MEASER_SEQUNECE, &CVS31Measure3DDlg::OnBnClickedButtonMeaserSequnece)
	ON_BN_CLICKED(IDC_BUTTON_RESET_ACTION_STATE, &CVS31Measure3DDlg::OnBnClickedButtonResetActionState)
	ON_BN_CLICKED(IDC_BUTTON_LOAD_RAW3D_DATA, &CVS31Measure3DDlg::OnBnClickedButtonLoadRaw3dData)
	ON_BN_CLICKED(IDC_BUTTON_LOAD_RAW3D_DATA2, &CVS31Measure3DDlg::OnBnClickedButtonLoadRaw3dData2)
	ON_BN_CLICKED(IDC_BT_TEST_3D_IMG, &CVS31Measure3DDlg::OnBnClickedBtTest3dImg)
	ON_BN_CLICKED(IDC_SHOW_RESULT, &CVS31Measure3DDlg::OnBnClickedShowResult)


	ON_MESSAGE(WM_PROC_FILE_DATA, &CVS31Measure3DDlg::OnProcFileData)
	ON_BN_CLICKED(IDC_CHK_EDIT_RESULT, &CVS31Measure3DDlg::OnBnClickedChkEditResult)
	
	ON_BN_CLICKED(IDC_MEASURE_ANGLE_0, &CVS31Measure3DDlg::OnBnClickedMeasureAngle0)
	ON_BN_CLICKED(IDC_MEASURE_ANGLE_45, &CVS31Measure3DDlg::OnBnClickedMeasureAngle45)
	ON_BN_CLICKED(IDC_CHK_STRIP, &CVS31Measure3DDlg::OnBnClickedChkStrip)
	ON_BN_CLICKED(IDC_CHK_DOT, &CVS31Measure3DDlg::OnBnClickedChkDot)

	ON_EN_CHANGE(IDC_ED_BASE_DEPTH, &CVS31Measure3DDlg::OnEnChangeEdBaseDepth)
	ON_BN_CLICKED(IDC_BT_LENS_1, &CVS31Measure3DDlg::OnBnClickedBtLens)
	ON_BN_CLICKED(IDC_BT_LENS_2, &CVS31Measure3DDlg::OnBnClickedBtLens)
	ON_BN_CLICKED(IDC_BT_LENS_3, &CVS31Measure3DDlg::OnBnClickedBtLens)
	ON_BN_CLICKED(IDC_BT_LENS_4, &CVS31Measure3DDlg::OnBnClickedBtLens)

	ON_BN_CLICKED(IDC_BUTTON_MEASURE_3D_DATA_FILE, &CVS31Measure3DDlg::OnBnClickedButtonMeasure3dDataFile)
	ON_BN_CLICKED(IDC_SAVE_ANAYSIS_DATA, &CVS31Measure3DDlg::OnBnClickedSaveAnaysisData)
	ON_BN_CLICKED(IDC_BUTTON_MEASURE_BATCH_DATA, &CVS31Measure3DDlg::OnBnClickedButtonMeasureBatchData)
END_MESSAGE_MAP()


// CVS31Measure3DDlg 메시지 처리기

//void CVS31Measure3DDlg::SetMeasurementResultDataSize(MeasurementQuality measurementQuality)
//{
//	long m_nLastMeasurementResultDataWidth = 1024;
//	long m_nLastMeasurementResultDataHeight = 768;
//	switch (measurementQuality)
//	{
//	case HighSpeed:
//	case HighResolution:
//	case StandardHighSpeed:
//		break;
//	case HighDensity:
//		m_nLastMeasurementResultDataWidth *= 2;
//		m_nLastMeasurementResultDataHeight *= 2;
//		break;
//	case Part1Of12Accuracy:
//	case Part1Of12SuperHighSpeed:
//		m_nLastMeasurementResultDataHeight = 64;
//		break;
//	default:
//		break;
//	}
//
//	if(m_pResultColorViewData != NULL)
//		cvReleaseImage(&m_pResultColorViewData);
//
//	m_pResultColorViewData = cvCreateImage(cvSize(m_nLastMeasurementResultDataWidth,m_nLastMeasurementResultDataHeight),IPL_DEPTH_8U, 3);
//	
//
//}

BOOL CVS31Measure3DDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	m_ConnectOK = FALSE;
	//////////////////////////////////////////////////////////////////////////
	//1) Server Interface Connection.
	AOI_fnInitVS64Interface();
	CString NewWindowsName;
	NewWindowsName.Format("Visual Station 64 - Inspection Task(No.%02d)", m_ProcInitVal.m_nTaskNum);
	SetWindowText(NewWindowsName);

	G_pServerInterface = &m_ServerInterface;
	G_pProcInitVal	 = &m_ProcInitVal;
	//2) UI Init
	GetDlgItem(IDC_ST_VIEW_LIVE)->GetClientRect(&m_ViewRect);
	m_pHDcView =  &GetDlgItem(IDC_ST_VIEW_LIVE)->GetDC()->m_hAttribDC;


	m_pMeasurWin3D = new CMeasureProc(this);
	m_pMeasurWin3D->Create(IDD_DLG_IMG_VIEW);
	m_pMeasurWin3D->m_pBaseDepth = &m_BaseDepth;
	//m_pMeasurWin3D->ShowWindow(SW_SHOW);
	m_pMeasurWin3D->ShowWindow(SW_HIDE);
	m_pMeasurWin3D->SetWindowText("Measure Window");
	//////////////////////////////////////////////////////////////////////////

	CRect View3DRect;
	//m_pView3D->GetWindowRect(&View3DRect);
	//m_pView3D->SetWindowText("3D View");
	//m_pMeasurWin3D->SetWindowText("2D View");
	//m_pMeasurWin3D->MoveWindow(0, 0, View3DRect.Width(), View3DRect.Height());
	//////////////////////////////////////////////////////////////////////////
	m_RunnerProcData.m_bProcessRun			= TRUE;
	m_RunnerProcData.m_pObservationAppState = &m_ObservationAPPState;
	m_RunnerProcData.m_pObsActionState		= &m_ObsActionState;
	m_RunnerProcData.m_pObserAPPStart		= &m_ObserAPPStart;
	m_RunnerProcData.m_MainWinHWnd			= this->m_hWnd;
	m_RunnerProcData.m_pServerObj			= &m_ServerInterface;

	//////////////////////////////////////////////////////////////////////////
	m_RunnerProcData.m_pDetectThreshold		= &m_DetectThreshold;
	m_RunnerProcData.m_pDetectCountResult	= &m_DetectCountResult;

	//////////////////////////////////////////////////////////////////////////
	m_RunnerProcData.m_pViewRect			= &m_ViewRect;
	m_RunnerProcData.m_pHDcView				= m_pHDcView;

	m_RunnerProcData.m_pRunnerThread		= ::AfxBeginThread(fn_OBS_RunnerProcess, &m_RunnerProcData, THREAD_PRIORITY_NORMAL);
	

	if(m_stMeasurRun.Load(MAKEINTRESOURCE(IDR_MGIF1), _T("MGif")))
	//if(m_stMeasurRun.Load("D:\\NBIZ_3DInspect\\Task\\VS31Measure3D\\res\\Mesure.gif"))
	{
		m_stMeasurRun.Draw();
	}

	SetWindowPos(NULL,0, 0, 190*2,150, SWP_SHOWWINDOW);

	CheckDlgButton(IDC_SHOW_RESULT, FALSE);

	InitCtrlButtons();

	//측정 Type
	CheckDlgButton(IDC_RD1, BST_CHECKED);
	CheckDlgButton(IDC_RD2, BST_UNCHECKED);
	
	((CButton*)GetDlgItem(IDC_CHK_DOT))->SetCheck(BST_CHECKED);
	((CButton*)GetDlgItem(IDC_CHK_STRIP))->SetCheck(BST_UNCHECKED);

	CString strLevelValue;
	for(int LevelStep = 0; LevelStep<23;LevelStep++)
	{
		if(LevelStep%2== 1)
		{
			strLevelValue.Format("%d", LevelStep);
			m_cbFilterLevelList.AddString(strLevelValue);
		}
	}
	m_cbFilterLevelList.SetCurSel(8);

	m_BaseDepth = 0.0f;
	GetDlgItem(IDC_ED_BASE_DEPTH)->SetWindowText("0.0");

	LoadMeasureAlgParameter();

	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Revolver, Key_PortIndex, "1", &strReadData[0], 256, VS31Motion_PARAM_INI_PATH);

	//WritePrivateProfileString(Section_Revolver, Key_PortIndex, &strReadData[0], VS31Motion_PARAM_INI_PATH);
	//if(G_VKRevolver.m_fnVKConnect(atoi(strReadData), 115200) == FALSE)
	if(G_VKRevolver.m_fnVKConnect(atoi(strReadData), 115200) == FALSE)
	{
		m_vtAddLog(LOG_LEVEL_1, "포트연결 실패 %d번", strReadData);
	}		
	else
	{
		//		G_VKRevolver.m_fnVKMoveOriginSingleAxis();
		G_VKRevolver.m_fnVReadKPosTable();
		//Revolver Home.
		G_VKRevolver.m_fnVKServoEnable(TRUE);
		G_VKRevolver.m_fnVKMoveOriginSingleAxis();
	}
	//3) HW를 연결을 시도하고 Init를 진행한다.
	{//연결이 되어 있지 않다면 Timer를 동작 시켜 지속적으로 접근을 시도하자.
		//UI Update Timer
		::SetTimer(this->m_hWnd, HW_OBS_APP_CHECK_TIMER, HW_OBS_APP_CHECK_Interval, 0);
		m_vtAddLog(LOG_LEVEL_1, "Observe Application Check Start Timer");
	}

	// temporary
	//ShowWindow(SW_MAXIMIZE);
	G_VS31TASK_STATE=TASK_STATE_IDLE;

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

LRESULT CVS31Measure3DDlg::OnProcFileData(WPARAM wParam, LPARAM lParam)
{
	ACTION_PROC *pProcParam = (ACTION_PROC*)lParam;
	ACTION_FN *pParam = (ACTION_FN*)wParam;

	/// [2014-11-17] Charles, Modified. 
	strcpy(m_szDataFile, pProcParam->m_WriteDataFilePath);
	BOOL bResult = ProcMeasureAlg();

	int nSendData = m_nCountResultData;		// 전체 Result Data 개수
	int nSizeSendData =sizeof(int) + sizeof(RESULT_DATA)*m_nCountResultData;
	UCHAR *pSendData = new UCHAR[nSizeSendData];
	if(pSendData == 0x00) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Send Result Data");
		return 1;
	}
	memcpy(pSendData, &nSendData, sizeof(int));
	if(m_nCountResultData>0) {
		memcpy(pSendData+sizeof(int), m_plistResultData, sizeof(RESULT_DATA) * m_nCountResultData);
	}
	pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_Recv_StartMeasureEnd, bResult, nSizeSendData, pSendData);
	delete [] pSendData;
/*

	//File 선택 부터 Scan Time으로 하자.
	//m_pMeasurWin3D->m_ScanTime	= ::GetTickCount();
	////////////////////////////////////////////////////////////////////////////////
	CString RowData;
	CFile loadFile;
	try
	{
		if( !loadFile.Open( pProcParam->m_WriteDataFilePath, CFile::shareExclusive | CFile::shareDenyRead | CFile::modeReadWrite))
		{
			return 0;
		}
		loadFile.Seek(0,CFile::begin);
		UINT FileSize = (UINT)loadFile.GetLength();
		BYTE *pbuf =  new BYTE[FileSize];
		UINT nBytesRead = loadFile.Read( pbuf,  FileSize );
		m_NowMeasurData.CreateStorageByFileData(pbuf, FileSize);
		delete [] pbuf;
		loadFile.Close();

		cvDestroyAllWindows();
		int ProcRet = 1;
		if(m_pMeasurWin3D != NULL)
		{
			CString strFilterValue;

			m_cbFilterLevelList.GetLBText(m_cbFilterLevelList.GetCurSel(), strFilterValue);
			int nFilterLevel = atoi(strFilterValue);
			ProcRet = m_pMeasurWin3D->fn_SetProcData(*pProcParam->m_pMaskType, &m_NowMeasurData, &m_MeasureResultData, nFilterLevel);
		}
		else
			ProcRet = 0;


		pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_Recv_StartMeasureEnd, ProcRet, sizeof(RESULT_DATA), (UCHAR*)&m_MeasureResultData);
		
	}
	catch (CException* pe)
	{
		pe = pe;
	}
*/
	return 0;
}

void CVS31Measure3DDlg::InitCtrlButtons()
{
	m_GridFont.CreateFont(18, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Times New Roman");


	//////////////////////////////////////////////////////////////////////////
	tButtonStyle tStyle;
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_tColorFace.m_tClicked		= RGB(0x50, 0x50, 0x50);
	//tStyle.m_tColorBorder.m_tClicked	= RGB(0xFF, 0xFF, 0xFF);
	m_tButtonStyle.SetButtonStyle(&tStyle);




	tColorScheme tColor;
	tColorScheme tColor2;
	LOGFONT tFont;
	LOGFONT tFont2;
	{
		m_btChangeMode.GetTextColor(&tColor);
		m_btChangeMode.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,31,"Times New Roman");
			tFont.lfHeight =18;
		}

		m_btChangeMode.GetFont(&tFont2);
		{
			tColor2.m_tEnabled	= RGB(0xAF, 0xAF, 0x00);
			tColor2.m_tClicked	= RGB(0x00, 0xAF, 0x00);
			tColor2.m_tPressed	= RGB(0x00, 0x00, 0xAF);
			strcpy_s(tFont2.lfFaceName,31,"Times New Roman");
			tFont2.lfHeight =25;
		}


		m_btChangeMode.SetRoundButtonStyle(&m_tButtonStyle);
		m_btChangeMode.SetTextColor(&tColor);
		m_btChangeMode.SetCheckButton(false);
		m_btChangeMode.SetFont(&tFont);

		m_btRunAF.SetRoundButtonStyle(&m_tButtonStyle);
		m_btRunAF.SetTextColor(&tColor);
		m_btRunAF.SetCheckButton(false);
		m_btRunAF.SetFont(&tFont);

		m_btStartMeaser.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStartMeaser.SetTextColor(&tColor);
		m_btStartMeaser.SetCheckButton(false);
		m_btStartMeaser.SetFont(&tFont);

		m_btStopMeasure.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStopMeasure.SetTextColor(&tColor2);
		m_btStopMeasure.SetCheckButton(false);
		m_btStopMeasure.SetFont(&tFont2);

		m_btAlarmReset.SetRoundButtonStyle(&m_tButtonStyle);
		m_btAlarmReset.SetTextColor(&tColor);
		m_btAlarmReset.SetCheckButton(false);
		m_btAlarmReset.SetFont(&tFont);

		m_btViewParam.SetRoundButtonStyle(&m_tButtonStyle);
		m_btViewParam.SetTextColor(&tColor);
		m_btViewParam.SetCheckButton(false);
		m_btViewParam.SetFont(&tFont);

		m_btLoadRawDataFile.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLoadRawDataFile.SetTextColor(&tColor);
		m_btLoadRawDataFile.SetCheckButton(false);
		m_btLoadRawDataFile.SetFont(&tFont);


		m_btLoadRawDataFile2.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLoadRawDataFile2.SetTextColor(&tColor);
		m_btLoadRawDataFile2.SetCheckButton(false);
		m_btLoadRawDataFile2.SetFont(&tFont);

		m_btMeasure3DDataFile.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMeasure3DDataFile.SetTextColor(&tColor);
		m_btMeasure3DDataFile.SetCheckButton(false);
		m_btMeasure3DDataFile.SetFont(&tFont);

		m_btMeasureBatchData.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMeasureBatchData.SetTextColor(&tColor);
		m_btMeasureBatchData.SetCheckButton(false);
		m_btMeasureBatchData.SetFont(&tFont);

		for(int LStep =0;LStep<4; LStep++)
		{
			m_btLensSelect[LStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btLensSelect[LStep].SetTextColor(&tColor);
			m_btLensSelect[LStep].SetCheckButton(true,true);
			m_btLensSelect[LStep].SetFont(&tFont);
		}
	}
}

void CVS31Measure3DDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVS31Measure3DDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVS31Measure3DDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CVS31Measure3DDlg::m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...)
{
	char	cLogBuffer[MAX_LOG_BUFF];
	va_list vargs;

	va_start(vargs, pErrLog);

	vsprintf_s(cLogBuffer, pErrLog, (va_list)vargs);

	va_end(vargs);

	m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nTaskNum ,uLogLevel, cLogBuffer);


	char LogTime[30/*STRING_LENGTHE_LOG_TIME*/+1] = {0,};
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime); 
	//Format ex)"2006/10/20-16/48/52-102"
	sprintf_s(LogTime, "%02d:%02d:%02d:%03d", 
		SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, SystemTime.wMilliseconds);

	if(m_listLogView.GetCount()>200)
	{
		m_listLogView.DeleteString(m_listLogView.GetCount()-200);
	}
	CString WriteData;
	WriteData.Format("[%s] %s", LogTime, cLogBuffer);
	m_listLogView.AddString(WriteData);
	m_listLogView.SetScrollPos(SB_VERT , m_listLogView.GetCount(), TRUE);
	m_listLogView.SetTopIndex(m_listLogView.GetCount()-1);

}
/*
*	Module Name		:	AOI_AppendList
*	Parameter		:	Message String
*	Return			:	없음
*	Function		:	정보 List 및 Log file저장.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
void CVS31Measure3DDlg::AOI_AppendList(char *pMsg,...)
{
	//m_ViewStateList.
	char	outstr[302];
	char	buf[260];
	va_list argptr;

	va_start(argptr,pMsg);
	vsprintf(buf,pMsg,argptr);
	memset(&outstr[0],0,302);
	sprintf(&outstr[0], "%s",buf);
	va_end(argptr);

	char LogTime[30/*STRING_LENGTHE_LOG_TIME*/+1] = {0,};
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime); 
	//Format ex)"2006/10/20-16/48/52-102"
	sprintf_s(LogTime, "%02d:%02d:%02d:%03d", 
		SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, SystemTime.wMilliseconds);

	//if(m_ViewStateList.GetCount()>LISTDATACOUNT)
	//{
	//	m_ViewStateList.DeleteString(m_ViewStateList.GetCount()-LISTDATACOUNT);
	//}
	//CString WriteData;
	//WriteData.Format("[%s] %s", LogTime, outstr);
	//m_ViewStateList.AddString(WriteData);
	//m_ViewStateList.SetScrollPos(SB_VERT , m_ViewStateList.GetCount(), TRUE);
	//m_ViewStateList.SetTopIndex(m_ViewStateList.GetCount()-1);

	m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS,  outstr);
}
//////////////////////////////////////////////////////////////////////////
/*
*	Module Name		:	AOI_InitVS64Interface
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	VS64 Connection Function.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/


void CVS31Measure3DDlg::AOI_fnInitVS64Interface()
{
	if(1 < __argc)
	{
		int nInputTaskNum = atoi(__argv[1]);
		char *pLinkPath = NULL;

		char path_buffer[_MAX_PATH];
		char chFilePath[_MAX_PATH] = {0,};
		char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		//실행 파일 이름을 포함한 Full path 가 얻어진다.
		::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
		//디렉토리만 구해낸다.
		_splitpath_s(path_buffer, drive, dir, fname, ext);
		strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
		strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

		switch(nInputTaskNum)
		{
		case TASK_10_Master		: pLinkPath = MSG_TASK_INI_FullPath_TN10; break;
		case TASK_11_Indexer		: pLinkPath = MSG_TASK_INI_FullPath_TN11; break;
		case TASK_21_Inspector	: pLinkPath = MSG_TASK_INI_FullPath_TN21; break;
		case TASK_24_Motion		: pLinkPath = MSG_TASK_INI_FullPath_TN24; break;
		case TASK_31_Mesure3D	: pLinkPath = MSG_TASK_INI_FullPath_TN31; break;
		case TASK_32_Mesure2D	: pLinkPath = MSG_TASK_INI_FullPath_TN32; break;
		case TASK_33_SerialPort	: pLinkPath = MSG_TASK_INI_FullPath_TN33; break;
		case TASK_60_Log			: pLinkPath = MSG_TASK_INI_FullPath_TN60; break;
		case TASK_90_TaskLoader: pLinkPath = MSG_TASK_INI_FullPath_TN90; break;

		case TASK_99_Sample: pLinkPath = MSG_TASK_INI_FullPath_TN99; break;
		default:
			{
				CString ErrorMessage;
				ErrorMessage.Format("Unknown TaskNum(%d)", nInputTaskNum);
				AfxMessageBox(ErrorMessage);
				SendMessage(WM_CLOSE, 0,  0);	
				m_vtAddLog(LOG_LEVEL_1, "Unknown TaskNum(%d)", nInputTaskNum);
			}return;
		}

		strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

		m_fnReadDatFile(chFilePath);
	}
	else
	{
		AfxMessageBox("Input Run Task Number");
		m_vtAddLog(LOG_LEVEL_1, "Input Run Task Number");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	m_ServerInterface.m_fnSetLogTaskInfo(m_ProcInitVal.m_nLogTaskNo, m_ProcInitVal.m_nLogBlockLovel);	
	if(m_ServerInterface.m_fnRegisterTask(m_ProcInitVal.m_nTaskNum, m_ProcInitVal.m_sServerIP, m_ProcInitVal.m_nPortNo) != 0)
	{
		AfxMessageBox("Client Register Fail");
		m_vtAddLog(LOG_LEVEL_1,"Client Register Fail");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	else
	{
		//m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, "Client Register OK : %d", m_ProcInitVal.m_nTaskNum);
		m_vtAddLog(LOG_LEVEL_1,"Client Register OK : %d", m_ProcInitVal.m_nTaskNum);
	}

	m_ServerInterface.m_fnSetAppWindowHandle(m_hWnd);
	m_ServerInterface.m_fnSetAppUserMessage(WM_USER_CALLBACK);
}

/*
*	Module Name		:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function		:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS31Measure3DDlg::AOI_fnMessageCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	CMDMSG* CmdMsg = (CMDMSG*)lParam;		//SmartPointer 버전으로 개조가 필요하다... Work Item..

	try
	{
		if(CmdMsg != NULL)
		{
			nRet = AOI_fnAnalyzeMsg(CmdMsg);

			if(OKAY != nRet)
			{
				//Client 메시지 처리에대한 에러처리...
			}

			m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
			m_ServerInterface.m_fnFreeMemory(CmdMsg);
			return OKAY;
		}
		else
		{
			throw CVs64Exception(_T("CVS31Measure3DDlg::m_fnVS64MsgProc, Message did not become well."));
		}

	}
	catch (CVs64Exception& e)
	{
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS,  e.m_fnGetErrorMsg());

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS31Measure3DDlg::m_fnVS64MsgProc, exception -> %s"), e.what());
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS31Measure3DDlg::m_fnVS64MsgProc, CException -> %s"), wszErr);
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS31Measure3DDlg::m_fnVS64MsgProc, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	return OKAY;
}
int CVS31Measure3DDlg::AOI_fnAnalyzeMsg(CMDMSG* RcvCmdMsg)
{
	try
	{
		//AOI_AppendList("RcvComMsg:T(%d), F(%d), S(%d)",RcvCmdMsg->uTask_Dest, RcvCmdMsg->uFunID_Dest, RcvCmdMsg->uSeqID_Dest) ;
		if(RcvCmdMsg->uTask_Dest == m_ProcInitVal.m_nTaskNum) 
		{	
			G_MasterTaskIndex = RcvCmdMsg->uTask_Src;
			switch(RcvCmdMsg->uFunID_Dest)
			{
				//////////////////////////////////////////////////////////////////////////
				//Project Default Command.
			case nBiz_Func_System:
				{
					switch (RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Seq_Alive_Question:
						{
							m_ServerInterface.m_fnSendCommand_Nrs(RcvCmdMsg->uTask_Src, nBiz_Func_System, nBiz_Seq_Alive_Response, G_VS31TASK_STATE);
						}break;
					default:
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
					break;
				}
			case nBiz_3DMeasure_System://nBiz_3DMeasure_System:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_3DSend_UnitInitialize:
						{
							m_vtAddLog(LOG_LEVEL_1, "Input Unit Initialize");
							
							HWND ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
							DWORD dwProcessId = NULL;
							GetWindowThreadProcessId( ObserHWND,    &dwProcessId);
							HANDLE  process;
							process = OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId );
							TerminateProcess(process, (UINT)-1);							

						}break;
					case nBiz_3DSend_AlarmReset:
						{
							m_vtAddLog(LOG_LEVEL_1, "Input Alarm Reset");
							if(m_ObsActionState != ActionObs_None)
								m_ObsActionState =	ActionObs_None;
						}break;
					case nBiz_3DSend_AutoFocus:
						{	
							m_vtAddLog(LOG_LEVEL_1, "Input Auto Focus");
							fnActionObsProcess(Run_AutoFocus);
						}break;
					case nBiz_3DSend_StartMeasure:
						{
							m_vtAddLog(LOG_LEVEL_1, "Input Start Measure");
							if(RcvCmdMsg->uMsgSize >= sizeof(int)*2)
							{
								cvDestroyAllWindows();

								SEND_STARTMEASURE_DATA DataBuffer;
								memcpy(&DataBuffer, RcvCmdMsg->cMsgBuf, sizeof(SEND_STARTMEASURE_DATA));
								m_MaskType = DataBuffer.nMaskType;			//1:MASK_TYPE_STRIPE, 2:MASK_TYPE_DOT
								int MeaserAngle = DataBuffer.nMeasureAngle;		//0: 0.0Degree, 1:45.0Degree
								m_TargetThick = (long)DataBuffer.nTargetThick;		// um 단위. 
								if(m_TargetThick>200) m_TargetThick=200;
								m_nSpaceGap = DataBuffer.nSpaceGap;		// um 단위
								m_bMeasureAll = DataBuffer.bMeasureAll;			// 전체 검사 여부

								/// [2014-11-18] Charles, Added.
								m_nMeasureAngle = (MeaserAngle == 0) ? MEASURE_ANGLE_0 : MEASURE_ANGLE_45;

								memset(m_szResultSavePath,0x00,MAX_PATH);
								memcpy(m_szResultSavePath, (char*)(RcvCmdMsg->cMsgBuf+sizeof(SEND_STARTMEASURE_DATA)), (RcvCmdMsg->uMsgSize-sizeof(SEND_STARTMEASURE_DATA)));

								int ScanIndex =  (int)RcvCmdMsg->uUnitID_Dest;

								m_pMeasurWin3D->m_ScanTime	= ::GetTickCount();
								m_pMeasurWin3D->m_MasureIndex =  ScanIndex;
								m_pMeasurWin3D->m_MeasurDotAngle = (MeaserAngle ==1) ? 45.0f:0.0f;
								memcpy(m_pMeasurWin3D->m_strSavePath, (char*)(RcvCmdMsg->cMsgBuf+sizeof(SEND_STARTMEASURE_DATA)), (RcvCmdMsg->uMsgSize-sizeof(SEND_STARTMEASURE_DATA)));

								m_vtAddLog(LOG_LEVEL_1, "===> Index %02d, TYPE : %-7s, Angle : %dum", ScanIndex, (m_MaskType==MASK_TYPE_STRIPE?"STRIPE":" DOT "),MeaserAngle);
								m_vtAddLog(LOG_LEVEL_1, "===> Save Path : %s", m_szResultSavePath);
								
								UpdateUIParameter();

								if(fnActionObsProcess(Run_StartMeaserSequence, ScanIndex, m_szResultSavePath) == FALSE)
								{
									m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_Recv_StartMeasureEnd, 0);
								}
							}
							else
								m_vtAddLog(LOG_LEVEL_1, "===> Data Error");
						}break;

					case nBiz_3DSend_StopMeasure:
						{
							m_vtAddLog(LOG_LEVEL_1, "Input Stop Measure");
							OnBnClickedButtonStopmeasurement();
						}break;
					case nBiz_3DSend_SelectLens:
						{
							switch(RcvCmdMsg->uUnitID_Dest)
							{
							case 1:
								LensChange(LENS_CMD_10X);//G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_10X);
								m_vtAddLog(LOG_LEVEL_1, "Change Lens 10x" );
								break;
							case 2:
								LensChange(LENS_CMD_20X);//G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_20X);
								m_vtAddLog(LOG_LEVEL_1, "Change Lens 20x");
								break;
							case 3:
								LensChange(LENS_CMD_50X);//G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_50X);
								m_vtAddLog(LOG_LEVEL_1, "Change Lens 50x");
								break;
							case 4:
								LensChange(LENS_CMD_150X);//G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_150X);
								m_vtAddLog(LOG_LEVEL_1, "Change Lens 150x");
								break;
							default:
								break;
							}
						}break;
					case nBiz_3DSend_AlignGrabImg:
						{
							char ResultSavePath[MAX_PATH];
							memset(ResultSavePath, 0x00, MAX_PATH);
							memcpy(ResultSavePath, (char*)RcvCmdMsg->cMsgBuf, RcvCmdMsg->uMsgSize);
							int GrabIndex =  (int)RcvCmdMsg->uUnitID_Dest;
							m_vtAddLog(LOG_LEVEL_1, "Align Grab Img");
							fnActionObsProcess(Run_ImageGrab, GrabIndex, ResultSavePath);
						}break;
					case nBiz_3DSend_ManualModeON:
						{
							m_vtAddLog(LOG_LEVEL_1, "Input Manual Mode ON");

							if(m_ObservationAPPState == ObserveSW_ManualMode)
							{
								m_vtAddLog(LOG_LEVEL_1, "Set Auto Mode");
								fnObservationAPPCtrlState(TRUE);
							}

						}break;
					case nBiz_3DSend_ManualModeOFF:
						{
							m_vtAddLog(LOG_LEVEL_1, "Input Manual Mode OFF");
							if(m_ObservationAPPState != ObserveSW_ManualMode)
							{
								m_vtAddLog(LOG_LEVEL_1, "Set Manual Mode");
								fnObservationAPPCtrlState(TRUE);
							}
						}break;
					default:
						{
							throw CVs64Exception(_T("VS31Measure3D::AOI_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
							break;
						}
					}
				}break;
			default:
				{
					throw CVs64Exception(_T("VS31Measure3D::AOI_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
					break;
				}
			}
		}
		else
		{
			throw CVs64Exception(_T("VS31Measure3D::AOI_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
		}
	}
	catch (...)
	{
		throw; 
	}


	return OKAY;
}

//////////////////////////////////////////////////////////////////////////


ObsAppState CVS31Measure3DDlg::fnObservationAPPCtrlState(BOOL ManualModeTogle)
{
	HWND ObserHWND = 0;
	//Menual Mode 동작시에는 일반상태 Check를 전혀 하지 않느다.(제어권이 전혀 없다.)
	if(m_ObservationAPPState == ObserveSW_ManualMode && ManualModeTogle == FALSE)
		return m_ObservationAPPState;


	ObsAppState ReturnStateValue = ObserveSW_None;
	int OldState = m_ObservationAPPState;

	if(ManualModeTogle)
	{
		if(m_ObservationAPPState != ObserveSW_ManualMode)
		{
			//ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
			//::SetWindowPos(ObserHWND, HWND_TOP,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);

			m_ObservationAPPState = ObserveSW_ManualMode;//App Manual Mode로 진입.
			HWND RemoteModeH = ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Under remote control");
			if( RemoteModeH != NULL)
			{
				::PostMessage(RemoteModeH,WM_CLOSE,0,0);
			}
		}
		else
			m_ObservationAPPState = ObserveSW_Init;//Manual에서 Auto Mode로 재진입.
	}
	else
	{
		ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
		if(ObserHWND != NULL && ::IsWindowVisible(ObserHWND) == TRUE)
		{
			::ShowWindow(ObserHWND, SW_RESTORE);
			//::SetWindowPos(ObserHWND, HWND_TOP,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);

			if(m_ObserAPPStart == TRUE)
			{
				HWND Depth1				= ::FindWindowEx(ObserHWND, 0, "WindowsForms10.Window.8.app.0.378734a","");
				Depth1						= ::FindWindowEx(ObserHWND, Depth1, "WindowsForms10.Window.8.app.0.378734a","");
				HWND Depth2				= ::FindWindowEx(Depth1, 0, "WindowsForms10.Window.8.app.0.378734a","");
				Depth2						= ::FindWindowEx(Depth1, Depth2, "WindowsForms10.Window.8.app.0.378734a","");
				HWND PHandle				= ::FindWindowEx(Depth2, 0, 0,"Pause");
				//////////////////////////////////////////////////////////////////////////
				Depth1						= ::FindWindowEx(ObserHWND, 0, "WindowsForms10.Window.8.app.0.378734a","");
				Depth1						= ::FindWindowEx(ObserHWND, Depth1, "WindowsForms10.Window.8.app.0.378734a","");

				Depth2						= ::FindWindowEx(Depth1, 0, "WindowsForms10.Window.8.app.0.378734a","");
				HWND Depth3				= ::FindWindowEx(Depth2, 0, "WindowsForms10.Window.8.app.0.378734a","");

				HWND Depth4				= ::FindWindowEx(Depth3, 0, "WindowsForms10.Window.8.app.0.378734a","");
				Depth4						= ::FindWindowEx(Depth4, 0, "WindowsForms10.Window.8.app.0.378734a","");

				HWND Depth5				= ::FindWindowEx(Depth4, 0, "WindowsForms10.Window.8.app.0.378734a","");
				HWND AutoUpDnH		    = ::FindWindowEx(Depth5, 0, "WindowsForms10.BUTTON.app.0.378734a","Set auto up/low pos.");
				
				if(::IsWindowEnabled(PHandle) == FALSE && 
					::IsWindowEnabled(AutoUpDnH) == FALSE && 
					m_ObsActionState == ActionObs_None &&
					m_ObservationAPPState != ObserveSW_OK)
					ReturnStateValue = ObserveSW_CtrlPowerOff;
				else
				{
					HWND RemoteModeH = ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Under remote control");
					if(RemoteModeH != NULL)
						ReturnStateValue = ObserveSW_OK;
					else
						ReturnStateValue = ObserveSW_NoneRemote;
				}
			}
		}
		else
			ReturnStateValue = ObserveSW_None;

		m_ObservationAPPState = ReturnStateValue;

		if(OldState != m_ObservationAPPState)
		{
			OnlyOneUpdatePos = FALSE;//App위치를 다시 잡기 위해서 ^^;
		}
	}

	CString NowStateObs;
	if(OldState != m_ObservationAPPState)
	{
		switch(m_ObservationAPPState)
		{
		case ObserveSW_Init://<<< 초기 상태
			{	
				NowStateObs.Format("Init State");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ObserveSW_None:				//<<< Observation APP이 구동되지 않았다.
			{
				NowStateObs.Format("None State");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ObserveSW_OK:					//<<< Observation APP이 정상 구동중이다.
			{
				NowStateObs.Format("OK State");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ObserveSW_CtrlPowerOff:			//<<< Observation APP는 구동중이나 Controller가 Off 상태 이다.
			{
				NowStateObs.Format("Ctrl Power Off State");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ObserveSW_NoneRemote:			//<<< Observation APP와 Controller는 구동중 이지만 Remote Mode가 아니다.
			{
				NowStateObs.Format("None Remote State");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
				////////
		case ObserveSW_ManualMode:			//<<< Main UI에서 Manual Mode로 변환 한것이다.
			{
				NowStateObs.Format("Manual Mode State");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		}
		GetDlgItem(IDC_ST_HW_CONNECTION)->SetWindowText(NowStateObs);

		m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_Status, (USHORT)m_ObservationAPPState);
	}
	
	return ReturnStateValue;

}


BOOL CVS31Measure3DDlg::fnConnectHW()
{

	VKResult result = (VKResult)VK_Initialize();
	switch(result)
	{
	case VKResult_OK:
		{
			result = (VKResult)VK_SetHardwareEventCallback(OnHardwareEventCallback, this);
			if(result != VKResult_OK)
			{
				m_vtAddLog(LOG_LEVEL_1, "Connect HW Failed to register.");
				m_ConnectOK = FALSE;
			}
			else
			{
				m_ConnectOK = TRUE;
			}
		}
		break;
	case VKResult_AlreadyInitialized:
		m_vtAddLog(LOG_LEVEL_1, "Connect HW Already initialized.");
		m_ConnectOK = TRUE;
		break;
	case VKResult_NotAccepted:
		m_vtAddLog(LOG_LEVEL_1, "Connect HW Invalid status.");
		m_ConnectOK = FALSE;
		break;
	default:
		m_vtAddLog(LOG_LEVEL_1, "Connect HW Failed to connect.");
		m_ConnectOK = FALSE;
		break;
	}
	return m_ConnectOK;
}
BOOL CVS31Measure3DDlg::fnObsAPPActionCheck()
{
	//랜즈의 위치를 주기적으로 검사하여 Connection을 확인 한다.
	BOOL CheckConnection = TRUE;
	CString NowStateObs;
	if(m_ObsActOLDState != m_ObsActionState)
	{
		switch(m_ObsActionState)
		{
		case ActionObs_Manual:						//<<< ObserveSW_ManualMode일때는 상태 Check를 하지 않는다.
			{	
				NowStateObs.Format("Manual");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ActionObs_None:						//<<< 동작 명령 수행중이 아닐때.
			{	
				NowStateObs.Format("None(IDLE)");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;

		//case ActionObs_AutoGain:					//<<< Auto Gain 동작 수행시.
		//	{	
		//		NowStateObs.Format("Auto Gain");
		//		m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
		//	}break;
		//case ActionObs_AutoGain_Error:				//<<< Auto Gain 동작 수행시 Error.
		//	{	
		//		NowStateObs.Format("Auto Gain Error");
		//		m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
		//	}break;

		case ActionObs_AutoFocus:					//<<< Auto Focus 동작 수행시.
			{	
				NowStateObs.Format("Auto Focus");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ActionObs_AutoFocus_Error:				//<<< Auto Focus 동작 수행시 Error.
			{	
				NowStateObs.Format("Auto Focus Error");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;

		//case ActionObs_AutoUpDownPosSetting:		//<<< 측정 Upper/Lower 위치 선정 수행시.
		//	{	
		//		NowStateObs.Format("Up/Down Pos Setting");
		//		m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
		//	}break;
		//case ActionObs_AutoUpDownPosSet_Error:		//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
		//	{	
		//		NowStateObs.Format("Up/Down Pos Set Error");
		//		m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
		//	}break;

		case ActionObs_AutoUpDownPosSetByImg:		//<<< 측정 Upper/Lower 위치 선정 수행시.
			{	
				NowStateObs.Format("Up/Down Pos Set By Img");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ActionObs_AutoUpDownPosSetByImg_Error:		//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
			{	
				NowStateObs.Format("Up/Down Pos Set By Img Error");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;

		case ActionObs_StartMeasurement:			//<<< 3D Scan 수행시.
			{	
				NowStateObs.Format("Start Measurement");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ActionObs_StartMeasurement_Error:		//<<< 3D Scan 수행시 Error.
			{	
				NowStateObs.Format("Start Measurement Error");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;

		case ActionObs_AutoSaveData:				//<<< 측정 Data 저장 수행시.
			{	
				NowStateObs.Format("Auto Save Data");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		case ActionObs_AutoSaveData_Error:			//<<< 측정 Data 저장 수행시 Error.
			{	
				NowStateObs.Format("Auto Save Data Error");
				m_vtAddLog(LOG_LEVEL_1, "%s", NowStateObs);
			}break;
		}
		GetDlgItem(IDC_ST_ACTION_STATE)->SetWindowText(NowStateObs);

		if(ActionObs_None < m_ObsActionState && m_ObsActionState <= ActionObs_AutoSaveData)
		{
			if(GetDlgItem(IDC_ST_PIC_RUN)->IsWindowVisible() == FALSE)
				GetDlgItem(IDC_ST_PIC_RUN)->ShowWindow(SW_SHOW);
		}
		else
		{
			if(GetDlgItem(IDC_ST_PIC_RUN)->IsWindowVisible() == TRUE)
				GetDlgItem(IDC_ST_PIC_RUN)->ShowWindow(SW_HIDE);
		}
		m_ObsActOLDState = m_ObsActionState;
	}

	return TRUE;
}
BOOL CVS31Measure3DDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		::TranslateMessage(pMsg);
		::DispatchMessage(pMsg);
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		::TranslateMessage(pMsg);
		::DispatchMessage(pMsg);
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return __super::PreTranslateMessage(pMsg);
}

void CVS31Measure3DDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(HW_OBS_APP_CHECK_TIMER == nIDEvent)
	{

		//Menual Mode일때는 아무동작도 하지 않는다.
		fnObservationAPPCtrlState();
		fnObsAPPActionCheck();

		 if(m_ObservationAPPState == ObserveSW_OK )
		 {
			HWND ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
			if(OnlyOneUpdatePos == FALSE)
			{
				CRect WinRect;
				::GetWindowRect(ObserHWND, &WinRect);
				this->SetWindowPos(NULL,WinRect.left+WinRect.Width(), WinRect.top, 190*2,WinRect.Height(), SWP_SHOWWINDOW);
				OnlyOneUpdatePos = TRUE;
			}

			if(m_ConnectOK == FALSE)
			{
				m_vtAddLog(LOG_LEVEL_1, "Try Connection!");
				if(fnConnectHW() == TRUE)//HW에 접근 한다.
				{//연결이 되어 있지 않다면 Timer를 동작 시켜 지속적으로 접근을 시도하자.
					m_vtAddLog(LOG_LEVEL_1, "Remote Control Link OK!");
				}
			}
		}

		if(m_ObservationAPPState == ObserveSW_ManualMode)
		{
			GetDlgItem(IDC_BUTTON_TOGGLE_MANUAL)->SetWindowText("Change Auto Mode");
			if(GetDlgItem(IDC_BUTTON_DOAUTOFOCUS)->IsWindowEnabled() == TRUE)
			{
				GetDlgItem(IDC_BUTTON_DOAUTOFOCUS)->EnableWindow(FALSE);
				GetDlgItem(IDC_BUTTON_MEASER_SEQUNECE)->EnableWindow(FALSE);
				GetDlgItem(IDC_BUTTON_RESET_ACTION_STATE)->EnableWindow(FALSE);
				GetDlgItem(IDC_BT_LOAD_PARAM_LIST)->EnableWindow(FALSE);
			}
		}
		else
		{
			GetDlgItem(IDC_BUTTON_TOGGLE_MANUAL)->SetWindowText("Change Manual Mode");
			if(GetDlgItem(IDC_BUTTON_DOAUTOFOCUS)->IsWindowEnabled() == FALSE)
			{
				GetDlgItem(IDC_BUTTON_DOAUTOFOCUS)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_MEASER_SEQUNECE)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_RESET_ACTION_STATE)->EnableWindow(TRUE);
				GetDlgItem(IDC_BT_LOAD_PARAM_LIST)->EnableWindow(TRUE);
			}
		}

		m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_Status, (USHORT)m_ObservationAPPState);
		m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)m_ObsActionState);


		//for(int CStep =0;CStep<4; CStep++)
		//	m_btLensSelect[CStep].SetCheck(false);

		//switch(G_VKRevolver.m_fnIsLensIndex())
		//{
		//case LENS_INDEX_10X:
		//	TimeOut =0;
		//	NowLensIndex=1;
		//	m_btLensSelect[0].SetCheck(true);
		//	break;
		//case LENS_INDEX_20X:
		//	TimeOut =0;
		//	NowLensIndex=2;
		//	m_btLensSelect[1].SetCheck(true);
		//	break;
		//case LENS_INDEX_50X:
		//	TimeOut =0;
		//	NowLensIndex=3;
		//	m_btLensSelect[2].SetCheck(true);
		//	break;
		//case LENS_INDEX_150X:
		//	TimeOut =0;
		//	NowLensIndex=4;
		//	m_btLensSelect[3].SetCheck(true);
		//	break;
		//default:
		//	m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, 0);
		//	break;
		//}
	}
	__super::OnTimer(nIDEvent);
}

HBRUSH CVS31Measure3DDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		//투명하게 만들기.
		//pDC->SetBkMode(TRANSPARENT);
		//return(HBRUSH)GetStockObject(NULL_BRUSH);;

	case CTLCOLOR_STATIC:
		{


		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_LISTBOX://List Color
		{
		}break;
	}

	pDC->SetTextColor(RGB(0, 255, 0)); 
	pDC->SetBkColor(RGB(0, 0, 0));

	
	return (HBRUSH)GetStockObject(BLACK_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CVS31Measure3DDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	__super::PostNcDestroy();
}

void CVS31Measure3DDlg::OnDestroy()
{
	__super::OnDestroy();

	m_RunnerProcData.m_bProcessRun = FALSE; // Observation App Runner Thread End

	KillTimer(HW_OBS_APP_CHECK_TIMER);	//Observation APP Check Timer Off


	HWND RemoteModeH = ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Under remote control");
	if( RemoteModeH != NULL)
	{
		::PostMessage(RemoteModeH,WM_CLOSE,0,0);
	}

	if(m_ConnectOK == TRUE)
	{
		//Scan 중이면 Scan을 중지 한다.
		BOOL bMeasuring;
		VKResult result = (VKResult)VK_IsMeasuring(&bMeasuring);
		switch(result)
		{
		case VKResult_OK:

			if(bMeasuring)
			{
				//측정중 Check Timer를 종료 한다.
				//KillTimer(HW_MEASUREMENT_CHECK_TIMER);

				result = (VKResult)VK_StopMeasurement();
				switch(result)
				{
				case VKResult_OK:
					m_vtAddLog(LOG_LEVEL_1, "Stop Mesurement status.");
					break;
				case VKResult_NotAccepted:
					m_vtAddLog(LOG_LEVEL_1, "Invalid status.");
					break;
				default:
					m_vtAddLog(LOG_LEVEL_1, "Cancel failed.");
					break;
				}
			}
			break;
		case VKResult_NotAccepted:
			m_vtAddLog(LOG_LEVEL_1, "VK_IsMeasuring Invalid status.");
			break;
		case VKResult_InvalidArgument:
			m_vtAddLog(LOG_LEVEL_1, "VK_IsMeasuring Invalid parameter.");
			break;
		default:
			m_vtAddLog(LOG_LEVEL_1, "VK_IsMeasuring Acquisition failed.");
			break;
		}

		//////////////////////////////////////////////////////////////////////////
		//
		result = (VKResult)VK_Uninitialize();
		switch(result)
		{
		case VKResult_OK:
			m_vtAddLog(LOG_LEVEL_1, "VK_Uninitialize OK");
			break;
		case VKResult_NotInitialized:
			m_vtAddLog(LOG_LEVEL_1, "VKResult_NotInitialized");
			break;
		case VKResult_NotAccepted:
			m_vtAddLog(LOG_LEVEL_1, "Invalid status.");//AfxMessageBox(_T("Invalid status."));
			break;
		default:
			m_vtAddLog(LOG_LEVEL_1, "Failed to end process");// AfxMessageBox(_T("Failed to end process"));
			break;
		}
	}
	if(m_pResultColorViewData != NULL)
		cvReleaseImage(&m_pResultColorViewData);
}


void CVS31Measure3DDlg::OnBnClickedBtLoadParamList()
{
	GetDlgItem(IDC_ST_PIC_RUN)->ShowWindow(SW_SHOW);

	MeasurementParameter2 parameter;
	ZeroMemory(&parameter, sizeof(parameter));
	parameter.nSize = sizeof(parameter);
	VKResult result = (VKResult)VK_GetMeasurementParameter2(&parameter);
	switch(result)
	{
	case VKResult_OK:
		break;
	case VKResult_NotAccepted:
		m_vtAddLog(LOG_LEVEL_1, "VK_GetMeasurementParameter2 Invalid status.");
		AfxMessageBox(_T("VK_GetMeasurementParameter2 Invalid status."));
		return;
	case VKResult_InvalidArgument:
		m_vtAddLog(LOG_LEVEL_1, "Invalid parameter.");
		AfxMessageBox(_T("VK_GetMeasurementParameter2 Invalid parameter."));
		return;
	default:
		m_vtAddLog(LOG_LEVEL_1, "Failed to get.");
		AfxMessageBox(_T("VK_GetMeasurementParameter2 Failed to get."));
		return;
	}

	CMeasurementParameterList dlgParameterList(&parameter);
	dlgParameterList.DoModal();

	
}

void CVS31Measure3DDlg::OnBnClickedButtonStartmeasurement()
{
	//fnActionObsProcess(Run_StartMeasurement);
}

void CVS31Measure3DDlg::OnBnClickedButtonStopmeasurement()
{
	//Action중이면 Action 중지.
	if(m_ActionProcData.m_bProcessRun ==  TRUE)
		m_ActionProcData.m_bProcessRun = FALSE;

	VKResult result = (VKResult)VK_StopMeasurement();
	switch(result)
	{
	case VKResult_OK:
		m_vtAddLog(LOG_LEVEL_1, "Stop Mesurement status.");
		break;
	case VKResult_NotAccepted:
		m_vtAddLog(LOG_LEVEL_1, "Invalid status.");
		break;
	default:
		m_vtAddLog(LOG_LEVEL_1, "Cancel failed.");
		break;
	}
	
}

BOOL CVS31Measure3DDlg::fnActionObsProcess(int RunTarget, int Index, char *pMeaserDataSavePath)
{
	if(m_ActionProcData.m_bProcessRun == TRUE)//이미 실행중이면.
	{
		OnBnClickedButtonStopmeasurement();
		Sleep( 1000);
		if(m_ActionProcData.m_pRunnerThread!= NULL)
			TerminateThread(m_ActionProcData.m_pRunnerThread, 0);
		m_ActionProcData.m_pRunnerThread = NULL;
	}
	m_ActionProcData.m_bProcessRun			= TRUE;
	m_ActionProcData.m_pObsActionState		= &m_ObsActionState;
	m_ActionProcData.m_MainWinHWnd			= this->m_hWnd;
	m_ActionProcData.m_pServerObj			= &m_ServerInterface;

#ifndef _SIMULATE_MEASURE
	if(m_ObservationAPPState != ObserveSW_OK)
	{
		m_vtAddLog(LOG_LEVEL_1, "Obs APP State is not ObserveSW_OK");
		return FALSE;
	}
#endif
	if(m_ActionProcData.m_pRunnerThread == NULL)
	{
		switch(RunTarget)
		{
		//case Run_AutoGain:
			//m_ActionProcData.m_pRunnerThread		= ::AfxBeginThread(fn_OBS_AutoGainActionProc, &m_ActionProcData, THREAD_PRIORITY_NORMAL);
			break;
		case Run_AutoFocus:
			m_ActionProcData.m_pRunnerThread		= ::AfxBeginThread(fn_OBS_AutoFocusActionProc, &m_ActionProcData, THREAD_PRIORITY_NORMAL);
			break;
		//case Run_AutoUpDownPosSetting:
			//m_ActionProcData.m_pRunnerThread		= ::AfxBeginThread(fn_OBS_AutoUpDnPosActionProc, &m_ActionProcData, THREAD_PRIORITY_NORMAL);
			break;
		case Run_StartMeasurement:
			{
				MeasurementQuality quality;
				VK_GetMeasurementQuality(&quality);
				//SetMeasurementResultDataSize(quality);
				m_ActionProcData.m_pRunnerThread		= ::AfxBeginThread(fn_OBS_StartMeasureActionProc, &m_ActionProcData, THREAD_PRIORITY_NORMAL);
			}break;
		case Run_StartMeaserSequence:
			{
				//1)
				//m_ActionProcData.m_StepList[0] = Run_AutoFocus;
				//m_ActionProcData.m_StepList[1] = Run_AutoGain;
				//m_ActionProcData.m_StepList[2] = Run_AutoUpDownPosSetting;
				//m_ActionProcData.m_StepList[3] = Run_StartMeasurement;
				//m_ActionProcData.m_StepList[4] = Run_AutoSaveData;
				//m_ActionProcData.m_StepList[5] = Run_None;

				//2)
				//m_ActionProcData.m_StepList[0] = Run_AutoFocus;
				//m_ActionProcData.m_StepList[1] = Run_AutoUpDownPosSetting;
				//m_ActionProcData.m_StepList[2] = Run_StartMeasurement;
				//m_ActionProcData.m_StepList[3] = Run_AutoSaveData;
				//m_ActionProcData.m_StepList[4] = Run_None;

				//3)
				//m_ActionProcData.m_StepList[0] = Run_AutoFocus;
				//m_ActionProcData.m_StepList[1] = Run_StartMeasurement;
				//m_ActionProcData.m_StepList[2] = Run_AutoSaveData;
				//m_ActionProcData.m_StepList[3] = Run_None;

				////4)
				//m_ActionProcData.m_StepList[0] = Run_AutoFocus;
				//m_ActionProcData.m_StepList[1] = Run_AutoUpDownPosSetByImg;
				//m_ActionProcData.m_StepList[2] = Run_StartMeasurement;
				//m_ActionProcData.m_StepList[3] = Run_AutoSaveData;
				//m_ActionProcData.m_StepList[4] = Run_None;

				//4)
				m_ActionProcData.m_StepList[0] = Run_AutoUpDownPosSetByImg;
				m_ActionProcData.m_StepList[1] = Run_StartMeasurement;
				m_ActionProcData.m_StepList[2] = Run_AutoSaveData;
				m_ActionProcData.m_StepList[3] = Run_None;


				//이하 Param은 결과 혹은 측정을 위한 기본 Data가 된다. 필수 입력.
				m_ActionProcData.m_TargetThick			= m_TargetThick;//(Nano단위) Recipe별 
				m_ActionProcData.m_pDetectThreshold		= &m_DetectThreshold;
				m_ActionProcData.m_pDetectCountResult	= &m_DetectCountResult;//고정.
			
				m_ActionProcData.m_3DViewerHandle		= m_pMeasurWin3D->m_hWnd;
				m_ActionProcData.m_pNowMeasurData		= &m_NowMeasurData;
			
				m_ActionProcData.m_pMaskType			= &m_MaskType;

				m_ActionProcData.m_pMeasurWin3D				= m_pMeasurWin3D;

				m_ActionProcData.m_pMeasureResultData	= &m_MeasureResultData;

				if(pMeaserDataSavePath != NULL && Index>0)
				{
					if(G_fnCheckDirAndCreate(pMeaserDataSavePath) == TRUE)
						sprintf_s(	m_ActionProcData.m_WriteDataFilePath, "%s%s%02d.vk4", pMeaserDataSavePath, (*m_ActionProcData.m_pMaskType ==MASK_TYPE_STRIPE?"STRIPE_":"DOT_"), Index);
					else
						return FALSE;
				}
				else
				{
					//측정 위치별
					SYSTEMTIME NowTime;
					::GetLocalTime(&NowTime );
					sprintf_s(m_ActionProcData.m_WriteDataFilePath, "D:\\NewData_%04d_%02d_%02d_%02d_%02d_%02d.vk4",NowTime.wYear,NowTime.wMonth, NowTime.wDay, NowTime.wHour, NowTime.wMinute, NowTime.wSecond);

				}

				m_ActionProcData.m_pRunnerThread		= ::AfxBeginThread(fn_OBS_AutoMeasureActionProc, &m_ActionProcData, THREAD_PRIORITY_NORMAL);
			}
		}		
	}
	return TRUE;
}


void CVS31Measure3DDlg::OnBnClickedButtonDoautogain()
{
	//fnActionObsProcess(Run_AutoGain);
	HWND ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
	DWORD dwProcessId = NULL;
	GetWindowThreadProcessId( ObserHWND,    &dwProcessId);
	HANDLE  process;
	process = OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId );
	TerminateProcess(process, (UINT)-1);
}

void CVS31Measure3DDlg::OnBnClickedButtonDoautoUpDn()
{
	//fnActionObsProcess(Run_AutoUpDownPosSetting);
	
}

void CVS31Measure3DDlg::OnBnClickedButtonDoautofocus()
{
	fnActionObsProcess(Run_AutoFocus);
}

void CVS31Measure3DDlg::OnBnClickedButtonToggleManual()
{
	fnObservationAPPCtrlState(TRUE);
	
}

void CVS31Measure3DDlg::OnBnClickedButtonMeaserSequnece()
{

	if(IsDlgButtonChecked(IDC_CHK_STRIP) == TRUE)
		m_MaskType = MASK_TYPE_STRIPE;
	else 	if(IsDlgButtonChecked(IDC_CHK_DOT) == TRUE)
		m_MaskType = MASK_TYPE_DOT;
	else
	{
		AfxMessageBox("Select Measure Mask Type!");
		return;
	}

	if(IsDlgButtonChecked(IDC_CHK_EDIT_RESULT)==TRUE)
		((CButton*)GetDlgItem(IDC_CHK_EDIT_RESULT))->SetCheck(BST_UNCHECKED);

	int MeaserAngle = 0;
	if(IsDlgButtonChecked(IDC_RD1) == TRUE)
		MeaserAngle = 0;
	else
	{
		if(m_MaskType == MASK_TYPE_STRIPE)
		{
			MeaserAngle = 0;
		}
		else//45.0'는 Dot Type만 갖는다.
			MeaserAngle = 1;
	}
	cvDestroyAllWindows();
	 
	m_nTargetThickEdit=GetDlgItemInt(IDC_ED_TARGET_THICK);

	if (m_nTargetThickEdit > 10  && m_nTargetThickEdit<200)
	 {
		 m_TargetThick     = m_nTargetThickEdit;//45;//*1000;	//60->25  : 정의천, Nano단위로 사용 함으로 um->Nano로 변경.
	}
	else{
		m_TargetThick   =45;
	}
	char ResultSavePath[MAX_PATH];
	memset(ResultSavePath, 0x00, MAX_PATH);
		

	int ScanIndex =  1;

	m_pMeasurWin3D->m_ScanTime	= ::GetTickCount();
	m_pMeasurWin3D->m_MasureIndex =  ScanIndex;
	m_pMeasurWin3D->m_MeasurDotAngle =  (MeaserAngle ==1 ? 45.0f:0.0f);

	char DemoSaveFolderName[MAX_PATH];
	SYSTEMTIME NowTime;
	::GetLocalTime(&NowTime );
	sprintf_s(DemoSaveFolderName, "d:\\Demo3DScan\\M_%04d%02d%02d%02d%02d%02d",NowTime.wYear,NowTime.wMonth, NowTime.wDay, NowTime.wHour, NowTime.wMinute, NowTime.wSecond);

	G_fnCheckDirAndCreate(DemoSaveFolderName);
	sprintf_s(m_pMeasurWin3D->m_strSavePath, DemoSaveFolderName);
	sprintf_s(ResultSavePath, DemoSaveFolderName);


	m_vtAddLog(LOG_LEVEL_1, "===> Index %02d, TYPE : %-7s, Thick : %dum", ScanIndex, (m_MaskType==MASK_TYPE_STRIPE?"STRIPE":" DOT "),m_TargetThick);
	m_vtAddLog(LOG_LEVEL_1, "===> Save Path : %s", ResultSavePath);

	fnActionObsProcess(Run_StartMeaserSequence, ScanIndex, ResultSavePath);
}

void CVS31Measure3DDlg::OnBnClickedButtonResetActionState()
{
	if(m_ObsActionState != ActionObs_None)
		m_ObsActionState =	ActionObs_None;
}
void CVS31Measure3DDlg::m_fnLoadFile(int MaskType)
{
	m_MaskType = MaskType;
	
	int nCount = 1000;
	char *szFile = new char[nCount];
	ZeroMemory(szFile, nCount);

	//File 선택 부터 Scan Time으로 하자.
	m_pMeasurWin3D->m_ScanTime	= ::GetTickCount();

	CFileDialog dlg(TRUE);
	dlg.m_ofn.Flags |= OFN_ALLOWMULTISELECT;
//#ifdef FILE_FORMAT_VK4
	dlg.m_ofn.lpstrFilter = "Measuer 3D File (vk4)\0*.vk4\0";
//#else
	//dlg.m_ofn.lpstrFilter = "Measuer 3D File (m3d)\0*.m3d\0";
//#endif
	dlg.m_ofn.lpstrFile = szFile;
	dlg.m_ofn.nMaxFile = nCount;

	if ( dlg.DoModal() == IDCANCEL )
	{
		delete [] szFile;
		return;
	}
	memcpy(szFile, dlg.m_ofn.lpstrFile, strlen(dlg.m_ofn.lpstrFile));
	////////////////////////////////////////////////////////////////////////////////
	{
		CString RowData;
		CFile loadFile;
		try
		{
			if( !loadFile.Open( szFile, CFile::shareExclusive | CFile::shareDenyRead | CFile::modeReadWrite))
			{
				return;
			}
			//TRACE("OpenFile-%s\r\n", pFilePath);
			loadFile.Seek(0,CFile::begin);
			UINT FileSize = (UINT)loadFile.GetLength();
			BYTE *pbuf =  new BYTE[FileSize];
			UINT nBytesRead = loadFile.Read( pbuf,  FileSize );
			m_NowMeasurData.CreateStorageByFileData(pbuf, FileSize);
			delete [] pbuf;


			if(m_pMeasurWin3D != NULL)
			{
				CString strFilterValue;

				m_cbFilterLevelList.GetLBText(m_cbFilterLevelList.GetCurSel(), strFilterValue);
				int nFilterLevel = atoi(strFilterValue);

				cvDestroyAllWindows();
				m_TargetThick =45;  // 60->25 정의천
				m_NowMeasurData.m_TargetThicknessNano = m_TargetThick*1000;
				m_pMeasurWin3D->fn_SetProcData(m_MaskType, &m_NowMeasurData, &m_MeasureResultData, nFilterLevel );
				::SetWindowText(m_pMeasurWin3D->m_hWnd, szFile);
			}
			loadFile.Close();
		}
		catch (CException* pe)
		{

			pe = pe;
		}
	}
	delete [] szFile;
}
void CVS31Measure3DDlg::OnBnClickedButtonLoadRaw3dData()
{
	m_pMeasurWin3D->m_StripMagnet = TRUE;
	//Stripe는 직각측정이다.
	CheckDlgButton(IDC_RD1, BST_CHECKED);
	CheckDlgButton(IDC_RD2, BST_UNCHECKED);
	m_pMeasurWin3D->m_MeasurDotAngle = 0.0f;

	m_fnLoadFile(MASK_TYPE_STRIPE);
}


void CVS31Measure3DDlg::OnBnClickedButtonLoadRaw3dData2()
{
	m_fnLoadFile(MASK_TYPE_DOT);
}


void CVS31Measure3DDlg::OnBnClickedBtTest3dImg()
{

}


void CVS31Measure3DDlg::OnBnClickedShowResult()
{
	if(IsDlgButtonChecked(IDC_SHOW_RESULT) == TRUE)
		m_pMeasurWin3D->m_showResult = TRUE;
	else
		m_pMeasurWin3D->m_showResult = FALSE;
}


void CVS31Measure3DDlg::OnBnClickedChkEditResult()
{
	if(IsDlgButtonChecked(IDC_CHK_EDIT_RESULT) == TRUE)
		m_pMeasurWin3D->m_bMeasureEditResult  = TRUE;
	else
		m_pMeasurWin3D->m_bMeasureEditResult  = FALSE;
}


void CVS31Measure3DDlg::OnBnClickedMeasureAngle0()
{
	//	if(IsDlgButtonChecked(IDC_MEASURE_ANGLE_0) == TRUE)
	//	{
	CheckDlgButton(IDC_MEASURE_ANGLE_0, BST_CHECKED);
	CheckDlgButton(IDC_MEASURE_ANGLE_45, BST_UNCHECKED);
	m_pMeasurWin3D->m_MeasurDotAngle = 0.0f;
	//	}
}


void CVS31Measure3DDlg::OnBnClickedMeasureAngle45()
{
	//	if(IsDlgButtonChecked(IDC_MEASURE_ANGLE_45) == TRUE)
	//	{
	CheckDlgButton(IDC_MEASURE_ANGLE_45, BST_CHECKED);
	CheckDlgButton(IDC_MEASURE_ANGLE_0, BST_UNCHECKED);
	m_pMeasurWin3D->m_MeasurDotAngle = 45.0f;
	//	}
}


void CVS31Measure3DDlg::OnBnClickedChkStrip()
{
	//	if(IsDlgButtonChecked(IDC_CHK_STRIP) == TRUE)
	//	{
	CheckDlgButton(IDC_CHK_STRIP, BST_CHECKED);
	CheckDlgButton(IDC_CHK_DOT, BST_UNCHECKED);
	CheckDlgButton(IDC_MEASURE_ALL, BST_UNCHECKED);

	OnBnClickedMeasureAngle0();
	GetDlgItem(IDC_MEASURE_ANGLE_0)->EnableWindow(FALSE);
	GetDlgItem(IDC_MEASURE_ANGLE_45)->EnableWindow(FALSE);
	GetDlgItem(IDC_MEASURE_ALL)->EnableWindow(FALSE);

	//	}
}


void CVS31Measure3DDlg::OnBnClickedChkDot()
{
	//	if(IsDlgButtonChecked(IDC_CHK_DOT) == TRUE)
	//	{
	CheckDlgButton(IDC_CHK_STRIP, BST_UNCHECKED);
	CheckDlgButton(IDC_CHK_DOT, BST_CHECKED);
	GetDlgItem(IDC_MEASURE_ANGLE_0)->EnableWindow(TRUE);
	GetDlgItem(IDC_MEASURE_ANGLE_45)->EnableWindow(TRUE);
	GetDlgItem(IDC_MEASURE_ALL)->EnableWindow(TRUE);
	//	}
}

void CVS31Measure3DDlg::OnEnChangeEdBaseDepth()
{
	CString strNewValue;
	GetDlgItem(IDC_ED_BASE_DEPTH)->GetWindowText(strNewValue);
	m_BaseDepth = (float)atof(strNewValue);
}
void CVS31Measure3DDlg::LensChange(int LensIndex)
{
	DWORD NowState = 0;
	NowState = 	G_VKRevolver.m_fnReadState(1, 5);
	
	if(G_VKRevolver.m_fnIsLensIndex() == LensIndex)
	{
		for(int CStep =0;CStep<4; CStep++)
			m_btLensSelect[CStep].SetCheck(false);

		switch(LensIndex)
		{
		case LENS_INDEX_10X:
			m_btLensSelect[0].SetCheck(true);
			break;
		case LENS_INDEX_20X:
			m_btLensSelect[1].SetCheck(true);
			break;
		case LENS_INDEX_50X:
			m_btLensSelect[2].SetCheck(true);
			break;
		case LENS_INDEX_150X:
			m_btLensSelect[3].SetCheck(true);
			break;
		default:
			break;
		}

		m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, LensIndex+1);
		return;
	}
	switch(LensIndex)
	{
	case LENS_CMD_10X:
		G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_10X);
		break;
	case LENS_CMD_20X:
		G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_20X);
		break;
	case LENS_CMD_50X:
		G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_50X);
		break;
	case LENS_CMD_150X:
		G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_150X);
		break;
	}	
	
	for(int CStep =0;CStep<4; CStep++)
		m_btLensSelect[CStep].SetCheck(false);

	//Sleep(10000);

	int NowLensIndex = G_VKRevolver.m_fnIsLensIndex();
	int TimeOut = 100;
	while (NowLensIndex != LensIndex)
	{
			
		NowLensIndex = G_VKRevolver.m_fnIsLensIndex() ; 
		Sleep(100);
		TimeOut--;
		if(TimeOut<0)
		{
			NowLensIndex = LENS_INDEX_ING;
			break;
		}
	}
	switch(NowLensIndex)
	{
	case LENS_INDEX_10X:
		m_btLensSelect[0].SetCheck(true);
		break;
	case LENS_INDEX_20X:
		m_btLensSelect[1].SetCheck(true);
		break;
	case LENS_INDEX_50X:
		m_btLensSelect[2].SetCheck(true);
		break;
	case LENS_INDEX_150X:
		m_btLensSelect[3].SetCheck(true);
		break;
	default:
		break;
	}
	m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, NowLensIndex+1);



	//int TimeOut =100; //20150319: 50->100
	//while(TimeOut>0)
	//{
	//	int NowLensIndex = G_VKRevolver.m_fnIsLensIndex();
	//	if( NowLensIndex == LensIndex  )
	//	{
	//		m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, NowLensIndex+1);
	//		switch(NowLensIndex)
	//		{
	//		case LENS_INDEX_10X:
	//			TimeOut=0;
	//			m_btLensSelect[0].SetCheck(true);
	//			break;
	//		case LENS_INDEX_20X:
	//			TimeOut=0;
	//			m_btLensSelect[1].SetCheck(true);
	//			break;
	//		case LENS_INDEX_50X:
	//			TimeOut=0;
	//			m_btLensSelect[2].SetCheck(true);
	//			break;
	//		case LENS_INDEX_150X:
	//			TimeOut=0;
	//			m_btLensSelect[3].SetCheck(true);
	//			break;
	//		default:
	//			break;
	//		}
	//		break;
	//	}
	//	TimeOut--;
	//	Sleep(100);
	//	if(TimeOut == 0)
	//		break;
	//	if(TimeOut<0)
	//	{
	//		m_ServerInterface.m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, 0);
	//		break;
	//	}
	//}

	//NowState = 	G_VKRevolver.m_fnReadState(1, 5);


}

void CVS31Measure3DDlg::OnBnClickedBtLens()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();

	switch(wID)
	{
	case IDC_BT_LENS_1:
		LensChange(LENS_CMD_10X);//G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_10X);
		break;
	case IDC_BT_LENS_2:
		LensChange(LENS_CMD_20X);//G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_20X);
		break;
	case IDC_BT_LENS_3:
		LensChange(LENS_CMD_50X);//G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_50X);
		break;
	case IDC_BT_LENS_4:
		LensChange(LENS_CMD_150X);//G_VKRevolver.m_fnVKPosTableRunItem(LENS_CMD_150X);
		break;
	}
}

/// [2014-11-05] Charles, Added.
void CVS31Measure3DDlg::OnBnClickedButtonMeasure3dDataFile()
{
	// TODO: Add your control notification handler code here
#ifdef _SIMULATE_MEASURE
	SimulateMeasure();
	return;
#endif

	SetTestParameter();

	BOOL bResult = SelectTestDataFile();
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Open Data File");
		return;
	}

	bResult = ProcMeasureAlg();
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Perform Measure Process.");
		return;
	}

	m_vtAddLog(LOG_LEVEL_1, "Success to Measure Data");
}

/// [2014-11-05] Charles, Added.
BOOL CVS31Measure3DDlg::SelectTestDataFile()
{
	CFileDialog dlg(TRUE);
	dlg.m_ofn.lpstrFilter = "Measure 3D File (vk4)\0*.vk4\0";
	dlg.m_ofn.lpstrFile = m_szDataFile;
	dlg.m_ofn.nMaxFile = MAX_PATH;

	if (dlg.DoModal() == IDCANCEL )
	{
		return FALSE;
	}

	char *pFound = strrchr(m_szDataFile, '\\');
	if(pFound != 0x00) {
		m_vtAddLog(LOG_LEVEL_1, "DataFile : %s", (pFound+1));
	}

	return TRUE;
}

/// [2014-11-18] Charles, Added.
void CVS31Measure3DDlg::SetTestParameter()
{
	UpdateData(TRUE);

	m_MaskType = MASK_TYPE_DOT;
	if(IsDlgButtonChecked(IDC_CHK_DOT) == TRUE) {
		m_MaskType = MASK_TYPE_DOT;
	}
	else if(IsDlgButtonChecked(IDC_CHK_STRIP) == TRUE) {
		m_MaskType = MASK_TYPE_STRIPE;
	}

	m_nMeasureAngle = MEASURE_ANGLE_0;
	if(IsDlgButtonChecked(IDC_MEASURE_ANGLE_0) == TRUE) {
		m_nMeasureAngle = MEASURE_ANGLE_0;
	}
	else if(IsDlgButtonChecked(IDC_MEASURE_ANGLE_45) == TRUE) {
		m_nMeasureAngle = MEASURE_ANGLE_45;
	}

	m_bMeasureAll = IsDlgButtonChecked(IDC_MEASURE_ALL);

	m_TargetThick = m_nTargetThickEdit;
	m_nSpaceGap = m_nSpaceGapEdit;

	m_nMeasureIndex = m_nMeasureIndexEdit;
	m_nMeasureOffsetX= m_nMeasureOffsetXEdit;
	m_nMeasureOffsetY= m_nMeasureOffsetYEdit;

	strcpy(m_szResultSavePath, "D:\\");
}

void CVS31Measure3DDlg::UpdateUIParameter()
{
	switch(m_MaskType) {
	case MASK_TYPE_DOT : OnBnClickedChkDot(); break;
	case MASK_TYPE_STRIPE : OnBnClickedChkStrip(); break;
	}

	switch(m_nMeasureAngle) {
	case MEASURE_ANGLE_0 : OnBnClickedMeasureAngle0(); break;
	case MEASURE_ANGLE_45 : OnBnClickedMeasureAngle45(); break;
	}

	if(m_bMeasureAll) CheckDlgButton(IDC_MEASURE_ALL, BST_CHECKED);
	else CheckDlgButton(IDC_MEASURE_ALL, BST_UNCHECKED);

	OnBnClickedSaveAnaysisData();

	m_nMeasureIndexEdit = m_nMeasureIndex;
	m_nTargetThickEdit = m_TargetThick;//1000;  //정의천
	m_nSpaceGapEdit = m_nSpaceGap;
	UpdateData(FALSE);
}

/// [2014-11-05] Charles, Added.
BOOL CVS31Measure3DDlg::ProcMeasureAlg()
{
	// Mask Type 별로 측정 알고리즘 Instance 를 가져옴.
	m_pMeasureAlg = CMeasureAlg::GetInstance(m_MaskType);
	if(m_pMeasureAlg == 0x00) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Get Measure Algorithm Instance");
		return FALSE;
	}

	if(m_pMeasureAlg == 0x00) {
		return FALSE;
	}

	m_vtAddLog(LOG_LEVEL_1, "Start Measure Process");

	m_pMeasureAlg->Clear();

	// 기본 파라미터 설정
	m_pMeasureAlg->SetMeasureAngle(m_nMeasureAngle);
	m_pMeasureAlg->SetMeasureAll(m_bMeasureAll);
	m_pMeasureAlg->SetTargetThick((FLOAT)m_TargetThick*1000.f);		// um 단위를 nm 단위로 변경
	m_pMeasureAlg->SetGlassSpaceGap(m_nSpaceGap * 1000.f);			// um 단위를 nm 단위로 변경
	m_pMeasureAlg->SetMeasureIndex(m_nMeasureIndex);
	m_pMeasureAlg->SetMeasureOffset(m_nMeasureOffsetX, m_nMeasureOffsetY);

	// 이미지 처리 옵션 설정
	BOOL b2DDataSmoothing = IsDlgButtonChecked(IDC_2D_DATA_SMOOTHING);
	m_pMeasureAlg->Set2DDataSmoothing(b2DDataSmoothing);
	BOOL bEstimateFeaturePoint = IsDlgButtonChecked(IDC_ESTIMATE_FEATURE_POINT);
	m_pMeasureAlg->SetEstimateFeaturePoint(bEstimateFeaturePoint);
	// 분석용(디버깅용) 데이터를 출력/저장하기위한 플래그 설정
	BOOL bSaveHeightData = IsDlgButtonChecked(IDC_SAVE_HEIGHT_DATA);
	m_pMeasureAlg->SetSaveHeightData(bSaveHeightData);
	BOOL bSaveAnalysisData = IsDlgButtonChecked(IDC_SAVE_ANAYSIS_DATA);
	m_pMeasureAlg->SetSaveAnalysisData(bSaveAnalysisData);
	BOOL bCheckCornerDistance = IsDlgButtonChecked(IDC_CHECK_CORNER_DISTANCE);
	m_pMeasureAlg->SetCheckCornerDistance(bCheckCornerDistance);

	// 검사를 할 데이터 파일(.vk4)을 로드함.
	BOOL bResult = m_pMeasureAlg->LoadDataFile(m_szDataFile);
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to load DataFile");
		return FALSE;
	}

	// 측정 알고리즘 수행.
	bResult = m_pMeasureAlg->Measure();

	// 분석용 데이터 저장. bSaveAnalysisData 값이 TRUE 일 경우에만 유효함.
	m_pMeasureAlg->SaveAnalysisData();	

	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Measure Data");
		return FALSE;
	} 

	// Master 에 결과값을 전달하기위한 변수값 설정.
	m_nCountResultData = m_pMeasureAlg->GetResultDataCount();
	m_plistResultData = m_pMeasureAlg->GetResultData();

	m_nCornerDistance = 0;
	if(bCheckCornerDistance) {
		m_nCornerDistance = m_pMeasureAlg->GetCornerDistance();
	}
	char szValue[256];
	sprintf_s(szValue,"%.2f", m_nCornerDistance);
	SetDlgItemTextA(IDC_CORNER_DISTANCE, szValue);

	// 측정 결과 이미지를 저장함.
	bResult = SaveMeasureResult();
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Save Measure Data");
		return FALSE;
	}

	// 알고리즘 옵션 저장
	SaveMeasureAlgParameter();

	m_vtAddLog(LOG_LEVEL_1, "Success Measure Process");
	return TRUE;
}

/// [2014-11-11] Charles, Added.
BOOL CVS31Measure3DDlg::SaveMeasureResult()
{
	if(m_pMeasureAlg == 0x00) {
		return FALSE;
	}

	// 결과 이미지 생성.
	BOOL bResult = m_pMeasureAlg->MakeResultImage();
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Make Result Image");
		return FALSE;
	}

	// 주어진 경로에 저장함.
	m_pMeasureAlg->SetSaveResultPath(m_szResultSavePath);
	m_pMeasureAlg->SaveResultImage();

	// '결과 보기' 플래그가 ON 일 경우, 화면에 출력.
	if(IsDlgButtonChecked(IDC_SHOW_RESULT) == TRUE) {
		m_pMeasureAlg->ShowResultImage();
	}

	m_vtAddLog(LOG_LEVEL_1, "Success to Save Result");
	return TRUE;
}

void CVS31Measure3DDlg::LoadMeasureAlgParameter()
{
	int nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_MaskType, 0, VS31Motion_PARAM_INI_PATH);
	m_MaskType = nValue;
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_MeasureAngle, 0, VS31Motion_PARAM_INI_PATH);
	m_nMeasureAngle = (MEASURE_DIR)nValue;
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_TargetThick, 0, VS31Motion_PARAM_INI_PATH);
	m_TargetThick = nValue;
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_MeasureAll, 0, VS31Motion_PARAM_INI_PATH);
	m_bMeasureAll = nValue;
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_SpaceGap, 0, VS31Motion_PARAM_INI_PATH);
	m_nSpaceGap = (float)nValue / 1000.f;
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_ShowResult, 0, VS31Motion_PARAM_INI_PATH);
	CheckDlgButton(IDC_SHOW_RESULT, nValue);
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_SaveAnalysisData, 0, VS31Motion_PARAM_INI_PATH);
	CheckDlgButton(IDC_SAVE_ANAYSIS_DATA, nValue);
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_2DDataSmoothing, 0, VS31Motion_PARAM_INI_PATH);
	CheckDlgButton(IDC_2D_DATA_SMOOTHING, nValue);
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_EstimageFeaturePoint, 0, VS31Motion_PARAM_INI_PATH);
	CheckDlgButton(IDC_ESTIMATE_FEATURE_POINT, nValue);
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_SaveHeightData, 0, VS31Motion_PARAM_INI_PATH);
	CheckDlgButton(IDC_SAVE_HEIGHT_DATA, nValue);
	nValue = GetPrivateProfileInt(Section_MeasureAlg, Key_CheckCornerDistance, 0, VS31Motion_PARAM_INI_PATH);
	CheckDlgButton(IDC_CHECK_CORNER_DISTANCE, nValue);

	UpdateUIParameter();
}

void CVS31Measure3DDlg::SaveMeasureAlgParameter()
{
	char szValue[256];
	sprintf(szValue, "%d", m_MaskType); 
	WritePrivateProfileString(Section_MeasureAlg, Key_MaskType, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", m_nMeasureAngle); 
	WritePrivateProfileString(Section_MeasureAlg, Key_MeasureAngle, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", IsDlgButtonChecked(IDC_MEASURE_ALL)); 
	WritePrivateProfileString(Section_MeasureAlg, Key_MeasureAll, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", m_TargetThick); 
	WritePrivateProfileString(Section_MeasureAlg, Key_TargetThick, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", (int)(m_nSpaceGap * 1000)); 
	WritePrivateProfileString(Section_MeasureAlg, Key_SpaceGap, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", IsDlgButtonChecked(IDC_SHOW_RESULT)); 
	WritePrivateProfileString(Section_MeasureAlg, Key_ShowResult, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", IsDlgButtonChecked(IDC_SAVE_ANAYSIS_DATA)); 
	WritePrivateProfileString(Section_MeasureAlg, Key_SaveAnalysisData, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", IsDlgButtonChecked(IDC_2D_DATA_SMOOTHING)); 
	WritePrivateProfileString(Section_MeasureAlg, Key_2DDataSmoothing, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", IsDlgButtonChecked(IDC_ESTIMATE_FEATURE_POINT)); 
	WritePrivateProfileString(Section_MeasureAlg, Key_EstimageFeaturePoint, szValue, VS31Motion_PARAM_INI_PATH);	
	sprintf(szValue, "%d", IsDlgButtonChecked(IDC_SAVE_HEIGHT_DATA)); 
	WritePrivateProfileString(Section_MeasureAlg, Key_SaveHeightData, szValue, VS31Motion_PARAM_INI_PATH);
	sprintf(szValue, "%d", IsDlgButtonChecked(IDC_CHECK_CORNER_DISTANCE)); 
	WritePrivateProfileString(Section_MeasureAlg, Key_CheckCornerDistance, szValue, VS31Motion_PARAM_INI_PATH);
}

/// [2014-11-17] Charles, Added.
#ifdef _SIMULATE_MEASURE
// Network 및 측정기 연결없이 Local 에서 테스트가 가능하도록 Simulation 함.
// 'Debug_Simulate' 모드로 빌드를 하면 활성화됨.
void CVS31Measure3DDlg::SimulateMeasure()
{
	SetTestParameter();

	SEND_STARTMEASURE_DATA SendData;
	SendData.nMaskType = m_MaskType;		//1:MASK_TYPE_STRIPE, 2:MASK_TYPE_DOT
	SendData.nMeasureAngle = (m_nMeasureAngle==MEASURE_ANGLE_0)? 0 : 1;		//0: 0.0Degree, 1:45.0Degree
	SendData.nTargetThick = (int)m_TargetThick;		// um 단위.
	SendData.nSpaceGap = (float)m_nSpaceGap;	// um 단위. Space Gap
	SendData.bMeasureAll = m_bMeasureAll;	// 0 : 가운데 하나 검사, 1 : 전체 검사

	CString strSavePath;
	strSavePath.Format("Z:\\Measer3D\\");
	char *pSendData = new char[sizeof(SEND_STARTMEASURE_DATA) + strSavePath.GetLength()+1];
	memcpy(pSendData, &SendData, sizeof(SEND_STARTMEASURE_DATA));
	memcpy(pSendData+sizeof(SEND_STARTMEASURE_DATA), strSavePath.GetBuffer(strSavePath.GetLength()), strSavePath.GetLength());

	CMDMSG dCmdMsg;
	dCmdMsg.uTask_Dest = TASK_31_Mesure3D;
	dCmdMsg.uFunID_Dest = nBiz_3DMeasure_System;
	dCmdMsg.uSeqID_Dest = nBiz_3DSend_StartMeasure;
	dCmdMsg.uUnitID_Dest = 31;
	dCmdMsg.cMsgBuf = (UCHAR *)pSendData;
	dCmdMsg.uMsgSize = (USHORT)(sizeof(SEND_STARTMEASURE_DATA) + strSavePath.GetLength());

	AOI_fnAnalyzeMsg(&dCmdMsg);

	delete [] pSendData;
}
#endif


void CVS31Measure3DDlg::OnBnClickedSaveAnaysisData()
{
	// TODO: Add your control notification handler code here
	if(IsDlgButtonChecked(IDC_SAVE_ANAYSIS_DATA) == FALSE) {
		GetDlgItem(IDC_SAVE_HEIGHT_DATA)->EnableWindow(FALSE);
		CheckDlgButton(IDC_SAVE_HEIGHT_DATA, FALSE);
	}
	else {
		GetDlgItem(IDC_SAVE_HEIGHT_DATA)->EnableWindow(TRUE);
	} 
}


void CVS31Measure3DDlg::OnBnClickedButtonMeasureBatchData()
{
	// TODO: Add your control notification handler code here
	SetTestParameter();

	BOOL bResult = SelectBatchDataFile();
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Open Batch Data");
		return;
	}

	bResult = MakeMeasureDataList();
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Read Batch Data");
		return;
	}

	bResult = ProcMeasureBatch();
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Measure Batch Data");
		return;
	}

	bResult = SaveMeasureBatchResult();
	if(bResult == FALSE) {
		m_vtAddLog(LOG_LEVEL_1, "Fail to Save Measure Batch Data");
		return;
	}

	m_vtAddLog(LOG_LEVEL_1, "Success to Measure Batch Data");

}

/// [2014-11-05] Charles, Added.
BOOL CVS31Measure3DDlg::SelectBatchDataFile()
{
	CFileDialog dlg(TRUE);

	dlg.m_ofn.lpstrFilter = "Measure Batch Data File (vkb)\0*.vkb\0";
	dlg.m_ofn.lpstrFile = m_szBatchDataFile;
	dlg.m_ofn.nMaxFile = MAX_PATH;

	if (dlg.DoModal() == IDCANCEL )
	{
		return FALSE;
	}

	char *pFound = strrchr(m_szBatchDataFile, '\\');
	if(pFound != 0x00) {
		m_vtAddLog(LOG_LEVEL_1, "Batch Data : %s", (pFound+1));
	}

	return TRUE;
}

BOOL CVS31Measure3DDlg::MakeMeasureDataList()
{
	m_listBatchData.clear();

	char szPath[MAX_PATH];
	char *pFound = strrchr(m_szBatchDataFile, '\\');
	if(pFound != 0x00) {
		*pFound = 0x00;
		strcpy(szPath, m_szBatchDataFile);
		*pFound = '\\';
	}

	CStdioFile iFile;
	if(iFile.Open(m_szBatchDataFile, CFile::modeRead) == FALSE) {
		return FALSE;
	}

	CString strLine;
	while(iFile.ReadString(strLine)) {
		BATCH_DATA dData;
		sscanf(LPCTSTR(strLine), "%s %s %s %d %d",
			dData.m_szDir, dData.m_szActiveType, dData.m_szDataFileName, &dData.m_nAngle, &dData.m_nIndex);

		if(strlen(dData.m_szDir) == 0) {
			continue;
		}

		sprintf_s(dData.m_szDataPath, "%s\\%s", szPath, dData.m_szDataFileName);
		m_listBatchData.push_back(dData);
	}

	iFile.Close();

	return TRUE;
}

/// [2014-11-05] Charles, Added.
BOOL CVS31Measure3DDlg::ProcMeasureBatch()
{
	m_mapBatchResult.clear();
	for(int i = 0; i<(int)m_listBatchData.size(); i++) {
		BATCH_DATA dData = m_listBatchData[i];

		strcpy(m_szDataFile, dData.m_szDataPath);
		m_nMeasureAngle = dData.m_nAngle;
		m_nMeasureIndex = dData.m_nIndex;
		m_bMeasureAll = TRUE;

		if(ProcMeasureAlg()) {
			map<string, BATCH_RESULT>::const_iterator it = m_mapBatchResult.find(dData.m_szDataFileName);
			if(it == m_mapBatchResult.end()) {
				BATCH_RESULT dBatchResult;
				m_mapBatchResult[dData.m_szDataFileName] = dBatchResult;
			}

			BATCH_RESULT &dBatchResult = m_mapBatchResult[dData.m_szDataFileName];

			strcpy(dBatchResult.m_szDataFileName, dData.m_szDataFileName);
			strcpy(dBatchResult.m_szActiveType, dData.m_szActiveType);

			RESULT_DATA *pResultData = 0x00;
			if(stricmp(dData.m_szDir, "HORIZ") == 0) pResultData = &dBatchResult.m_dHoriz;
			else if(stricmp(dData.m_szDir, "VERT") == 0) pResultData = &dBatchResult.m_dVert;
			else if(stricmp(dData.m_szDir, "DIAG") == 0) pResultData = &dBatchResult.m_dDiag;

			if(pResultData) {
				memcpy(pResultData, m_plistResultData, sizeof(RESULT_DATA));
			}
		}
	}

	return TRUE;
}

BOOL CVS31Measure3DDlg::SaveMeasureBatchResult()
{
	char szResultFileName[MAX_PATH];
	sprintf_s(szResultFileName, "%s.result.csv", m_szBatchDataFile);

	CStdioFile iFile;
	if(iFile.Open(szResultFileName, CFile::modeCreate | CFile::modeWrite) == FALSE) {
		return FALSE;
	}

	char szBuffer[256];
	map<string, BATCH_RESULT>::const_iterator it = m_mapBatchResult.begin();
	while(it != m_mapBatchResult.end()) {
		BATCH_RESULT dResult = it->second;

		sprintf_s(szBuffer, "%s,%s,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f\n", 
			dResult.m_szDataFileName, dResult.m_szActiveType,
			dResult.m_dHoriz.RIB_HEIGHT_E_L/1000.f,dResult.m_dHoriz.RIB_HEIGHT_E_R/1000.f,dResult.m_dHoriz.RIB_WIDTH/1000.f,dResult.m_dHoriz.RIB_HEIGHT/1000.f,
			dResult.m_dVert.RIB_HEIGHT_E_L/1000.f,dResult.m_dVert.RIB_HEIGHT_E_R/1000.f,dResult.m_dVert.RIB_WIDTH/1000.f,dResult.m_dVert.RIB_HEIGHT/1000.f,
			dResult.m_dDiag.RIB_HEIGHT_E_L/1000.f,dResult.m_dDiag.RIB_HEIGHT_E_R/1000.f,dResult.m_dDiag.RIB_WIDTH/1000.f,dResult.m_dDiag.RIB_HEIGHT/1000.f);

		iFile.WriteString(szBuffer);

		it++;
	}

	iFile.Close();

	ShellExecute(NULL, "open", szResultFileName, NULL, NULL, SW_SHOW);

	return TRUE;
}

