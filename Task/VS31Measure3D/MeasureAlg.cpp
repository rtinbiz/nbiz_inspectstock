/// [2014-11-05] Charles, Added.

#include "StdAfx.h"
#include "MeasureAlg.h"
#include "MeasureDotAlg.h"
#include "MeasureStripeAlg.h"

//
// Macro 정의
//
#define MAX_VALUE 99999999.f
#define MIN_VALUE -99999999.f

#define REVERSE_DATA(_Data, _Length) \
{ \
	for(int i=0; i<_Length/2; i++) { \
		swap(_Data[i], _Data[_Length-1-i]); \
	} \
}

#define REVERSE_POINT_POS(_Pos, nProfileLength) \
{ \
	_Pos = nProfileLength-1-_Pos; \
}

#define REVERSE_POINTS(_Points, nProfileLength) \
{ \
	int nCount = _Points.size(); \
	REVERSE_DATA(_Points, nCount); \
	for(int i=0; i<nCount; i++) { \
		REVERSE_POINT_POS(_Points[i], nProfileLength); \
	} \
}

#define SAVE_ANALYSIS_PROC_VALUE(_Id, _Index, _Value) \
{ \
	nCount = 1; \
	sprintf(szBuffer, "%s_%d,%d,%d\n", _Id, _Index, nCount, _Value); \
	iFile.Write(szBuffer,strlen(szBuffer)); \
}

#define SAVE_ANALYSIS_PROC_VALUE_FLOAT(_Id, _Index, _Value) \
{ \
	nCount = 1; \
	sprintf(szBuffer, "%s_%d,%d,%.3f\n", _Id, _Index, nCount, _Value); \
	iFile.Write(szBuffer,strlen(szBuffer)); \
}

#define SAVE_ANALYSIS_VALUE(_Id, _Value) \
{ \
	nCount = 1; \
	sprintf(szBuffer, "%s,%d,%d\n", _Id, nCount, _Value); \
	iFile.Write(szBuffer,strlen(szBuffer)); \
}

#define SAVE_ANALYSIS_VALUE_FLOAT(_Id, _Value) \
{ \
	nCount = 1; \
	sprintf(szBuffer, "%s,%d,%.3f\n", _Id, nCount, _Value); \
	iFile.Write(szBuffer,strlen(szBuffer)); \
}

#define SAVE_ANALYSIS_PROC_POS_DATA(_Id, _Index, _PosX, _PosY) \
{ \
	nCount = 2; \
	sprintf(szBuffer, "%s_%d,%d,%d,%d\n", _Id, _Index, nCount,_PosX, _PosY); \
	iFile.Write(szBuffer,strlen(szBuffer)); \
}

#define SAVE_ANALYSIS_PROC_FEATUREPOINT_DATA(_Id, _Index, _FeaturePoint) \
{ \
	nCount = 3; \
	sprintf(szBuffer, "%s_%d,%d,%d,%d,%d,%.3f,%.3f,%.3f\n", _Id, _Index, nCount, _FeaturePoint.m_nHighPos, _FeaturePoint.m_nMidPos,_FeaturePoint.m_nLowPos, \
		_FeaturePoint.m_nHighPosHeight, _FeaturePoint.m_nMidPosHeight,_FeaturePoint.m_nLowPosHeight); \
	iFile.Write(szBuffer,strlen(szBuffer)); \
}

#define SAVE_ANALYSIS_DATA(_Id, _Type, _Data, _Count) \
{ \
	if(_Data != 0x00) { \
		nCount = _Count; \
		sprintf(szBuffer, "%s,%d", _Id, nCount); \
		_Type *pData = (_Type *)_Data; \
		for(int i=0; i<nCount; i++) { \
			sprintf(szValue,",%.3f", (FLOAT)pData[i]); \
			strcat(szBuffer, szValue); \
			if(strlen(szBuffer) >= 10000) { \
			iFile.Write(szBuffer,strlen(szBuffer)); \
				szBuffer[0]=0x00; \
			} \
		} \
		strcat(szBuffer,"\n"); \
		iFile.Write(szBuffer,strlen(szBuffer)); \
	} \
}

#define SAVE_ANALYSIS_POINTS_DATA(_Id, _Points) \
{ \
	nCount = _Points.size(); \
	if(nCount > 0) { \
		sprintf(szBuffer, "%s,%d", _Id, nCount); \
		for(int i=0; i<nCount; i++) { \
			sprintf(szValue,",%d", _Points[i]); \
			strcat(szBuffer, szValue); \
			if(strlen(szBuffer) >= 10000) { \
				iFile.Write(szBuffer,strlen(szBuffer)); \
				szBuffer[0]=0x00; \
			} \
		} \
		strcat(szBuffer,"\n"); \
		iFile.Write(szBuffer,strlen(szBuffer)); \
	} \
}

#define SAVE_ANALYSIS_IMAGE_DATA(_Id, _Image) \
{ \
	if(_Image != 0x00) { \
		switch(_Image->depth) { \
		case IPL_DEPTH_8U : SAVE_ANALYSIS_DATA(_Id, UINT8, _Image->imageData, _Image->width); break; \
		case IPL_DEPTH_16U : SAVE_ANALYSIS_DATA(_Id, UINT16, _Image->imageData, _Image->width); break; \
		case IPL_DEPTH_32F : SAVE_ANALYSIS_DATA(_Id, FLOAT, _Image->imageData, _Image->width); break; \
		case IPL_DEPTH_8S : SAVE_ANALYSIS_DATA(_Id, INT8, _Image->imageData, _Image->width); break; \
		case IPL_DEPTH_16S : SAVE_ANALYSIS_DATA(_Id, INT16, _Image->imageData, _Image->width); break; \
		case IPL_DEPTH_32S : SAVE_ANALYSIS_DATA(_Id, INT32, _Image->imageData, _Image->width); break; \
		} \
	} \
}

#define SAVE_ANALYSIS_PROC_IMAGE_DATA(_Id, _Index, _Image) \
{ \
	sprintf(szId, "%s_%d", _Id, _Index); \
	SAVE_ANALYSIS_IMAGE_DATA(szId, _Image); \
}

#define SAVE_ANALYSIS_PROC_POINTS_DATA(_Id, _Index, _Points) \
{ \
	sprintf(szId, "%s_%d", _Id, _Index); \
	SAVE_ANALYSIS_POINTS_DATA(szId, _Points); \
}

#define ADD_ANALYSIS_RESULT_DATA_COLUMN(_Id) \
{ \
	sprintf(szValue, ",%s", _Id); \
	strcat(szBuffer, szValue); \
}

#define ADD_ANALYSIS_RESULT_DATA_VALUE(_Value) \
{ \
	ADD_ANALYSIS_RESULT_DATA_VALUE_FLOAT(_Value/1000.f); \
}

#define ADD_ANALYSIS_RESULT_DATA_VALUE_FLOAT(_Value) \
{ \
	sprintf(szValue, ",%.3f", _Value); \
	strcat(szBuffer, szValue); \
}

#define SAVE_ANALYSIS_RESULT_DATA() \
{ \
	strcat(szBuffer, "\n"); \
	iFile.Write(szBuffer,strlen(szBuffer)); \
}


// 알고리즘 Instance 반환 함수
// MaskType 에 맞는 측정 알고리즘 Instance 를 반환한다.
CMeasureAlg *CMeasureAlg::GetInstance(MASK_TYPE nMaskType)
{
	CMeasureAlg *pInstance = 0x00;

	switch(nMaskType) {
	case MASK_TYPE_DOT : 
		pInstance = CMeasureDotAlg::GetInstance();
		break;
	case MASK_TYPE_STRIPE: 
		pInstance = CMeasureStripeAlg::GetInstance();
		break;
	}

	return pInstance;
}

CMeasureAlg::CMeasureAlg()
{
}

CMeasureAlg::~CMeasureAlg()
{
}

void CMeasureAlg::SetTargetThick(FLOAT nTargetThick)
{
	m_dInput.m_nTargetThick = nTargetThick;
}

void CMeasureAlg::SetGlassSpaceGap(FLOAT nGap)
{
	m_dInput.m_nSpaceGap= nGap;
}

void CMeasureAlg::SetMeasureAngle(MEASURE_DIR nMeasureAngle)
{
	m_nMeasureAngle = nMeasureAngle;
}

void CMeasureAlg::SetMeasureIndex(int nMeasureIndex)
{
	m_nMeasureIndex = nMeasureIndex;
}

void CMeasureAlg::SetMeasureOffset(int nMeasureOffsetX, int nMeasureOffsetY)
{
	m_nMeasureOffsetX = nMeasureOffsetX;
	m_nMeasureOffsetY = nMeasureOffsetY;
}

void CMeasureAlg::SetMeasureAll(BOOL bSet)
{
	m_bMeasureAll = bSet;
}

void CMeasureAlg::Set2DDataSmoothing(BOOL bSet)
{
	m_b2DDataSmoothing= bSet;
}

void CMeasureAlg::SetEstimateFeaturePoint(BOOL bSet)
{
	m_bEstimateFeaturePoint= bSet;
}

void CMeasureAlg::SetSaveHeightData(BOOL bSet)
{
	m_bSaveHeightData= bSet;
}

void CMeasureAlg::SetSaveAnalysisData(BOOL bSet)
{
	m_bSaveAnalysisData = bSet;
}

void CMeasureAlg::SetCheckCornerDistance(BOOL bSet)
{
	m_bCheckCornerDistance= bSet;
}

// 데이터 정리 함수
// 측정시 사용된 모든 데이터를 해제 및 초기화 한다.
void CMeasureAlg::Clear()
{
	m_dInput.Clear();
	m_dProc.Clear();
	m_dOutput.Clear();
	m_dTime.Clear();

	m_nMeasureAngle = MEASURE_ANGLE_0;
	m_bMeasureAll = FALSE;
	m_nMeasureIndex = 0;
	m_nMeasureOffsetX = 0;
	m_nMeasureOffsetY = 0;

	m_nCornerPosX = 0;
	m_nCornerPosY = 0;
	m_nCornerDistance = 0.f;

	m_b2DDataSmoothing = FALSE;
	m_bEstimateFeaturePoint = FALSE;

	m_bSaveHeightData = FALSE;
	m_bSaveAnalysisData = FALSE;

	m_pImageView = CvImageView::GetInstance();
	if(m_pImageView) {
		m_pImageView->Close();
	}
}

BOOL CMeasureAlg::LoadDataFile(char *szDataPath)
{
	_SetStartTime();

	if(szDataPath == 0x00) {
		return FALSE;
	}
	strcpy(m_dInput.m_szDataPath, szDataPath);
	_MakeResultDataFileName();

	CFile iFile;
	try
	{
		if(!iFile.Open(szDataPath, CFile::shareExclusive | CFile::shareDenyRead | CFile::modeReadWrite)) {
			return FALSE;
		}

		iFile.Seek(0, CFile::begin);
		UINT nFileSize = (UINT)iFile.GetLength();
		BYTE *pBuf =  new BYTE[nFileSize];
		UINT nBytesRead = iFile.Read( pBuf,  nFileSize );
		m_d3DRaw.CreateStorageByFileData(pBuf, nFileSize);
		delete [] pBuf;
		iFile.Close();

		// 검사 대상 이미지의 높이와 넓이를 저장한다.
		m_dProc.m_nHeight = m_d3DRaw.m_Height;
		m_dProc.m_nWidth = m_d3DRaw.m_Width;

		// Pixel 당 길이(nano-meter) 를 저장한다.
		m_dProc.m_nmPerPixel = (float)m_d3DRaw.m_MeasureParam.dXYCalibration * 1000;
		m_dProc.m_nmPerHeight = 1;		// always 1 nano meter
	}
	catch (CException* pe) {
		pe;	// not to show warning message.
		return FALSE;
	}
	m_dTime.m_msLoadDataFile = _GetElapsedTime();

	_PrintDebugString("# Load Data File : %s", szDataPath);
	_PrintDebugString("> Time to Load Data File : %d ms", m_dTime.m_msLoadDataFile);

	// 높이 데이터에 대한 이미지를 복사한다.
	m_dProc.m_pHeightImage = cvCreateImage(cvSize(m_dProc.m_nWidth, m_dProc.m_nHeight), IPL_DEPTH_32S, 1);
	if(m_dProc.m_pHeightImage == 0x00) {
		return FALSE;
	}
	memcpy(m_dProc.m_pHeightImage->imageData, m_d3DRaw.m_pDepthData, sizeof(UINT)*m_dProc.m_nWidth*m_dProc.m_nHeight);

	return TRUE;
}

// 결과 데이터 파일의 이름을 생성한다.
void CMeasureAlg::_MakeResultDataFileName()
{
	char szPath[MAX_PATH] = {0,};
	if(m_dInput.m_szDataPath[0] == 0x00) {
		::GetModuleFileNameA(NULL, szPath, MAX_PATH);
	}
	else {
		strcpy(szPath, m_dInput.m_szDataPath);
	}

	char *pCut = strrchr(szPath,'\\');
	if(pCut == 0x00) {
		return;
	}

	char szFileName[MAX_PATH] = {0,};
	strcpy(szFileName, pCut+1);
	*(pCut+1) = 0x00;

	pCut = strrchr(szFileName,'.');
	if(pCut) {
		*pCut = 0x00;
	}

	char szProperty[128] = {0,};
	sprintf_s(szProperty, "%d-%d", m_nMeasureAngle, m_nMeasureIndex);

	char szTime[128] = {0,};
	SYSTEMTIME dTime;
	::GetLocalTime(&dTime);
	sprintf_s(szTime, "%04d-%02d-%02d-%02d-%02d-%02d-%02d", 
		dTime.wYear,dTime.wMonth,dTime.wDay,dTime.wHour,dTime.wMinute,dTime.wSecond, dTime.wMilliseconds);

	sprintf_s(m_dOutput.m_szDataFileName, "%s_%s_%s", szFileName, szProperty, szTime);
}

// 분석용 데이터 파일이 저장된 경로를 생성한다.
void CMeasureAlg::_MakeAnalysisDataFilePath()
{
	char szPath[MAX_PATH] = {0,};
	if(m_dInput.m_szDataPath[0] == 0x00) {
		::GetModuleFileNameA(NULL, szPath, MAX_PATH);
	}
	else {
		strcpy(szPath, m_dInput.m_szDataPath);
	}

	char *pCut = strrchr(szPath,'\\');
	if(pCut == 0x00) {
		return;
	}

	*(pCut+1) = 0x00;
	strcat(szPath, "AnalysisData");
	::CreateDirectoryA(szPath, NULL);

	strcpy(m_szAnalysisDataPath, szPath);
}

// 높이 데이터를 csv 파일로 저장한다. (분석용 데이터 저장 Flag 가 ON 일 경우에만 저장됨.)
void CMeasureAlg::_SaveHeightDataFile()
{
	IplImage *pImage = m_dProc.m_pHeightImage;
	if(pImage == 0x00) {
		return;
	}
	
	char szValue[128];
	char szBuffer[10240];
	CFile iFile;
	try
	{
		char szHeightDataFilePath[MAX_PATH];
		sprintf(szHeightDataFilePath,"%s\\%s_H.csv", m_szAnalysisDataPath, m_dOutput.m_szDataFileName);
		if(!iFile.Open(szHeightDataFilePath, CFile::modeCreate | CFile::modeWrite)) {
			return;
		}

		int nSizeY = m_dProc.m_nHeight;
		int nSizeX = m_dProc.m_nWidth;
		INT32 *pData = (INT32 *)pImage->imageData;
		INT32 nValue;
		for(int y=0; y<nSizeY; y++) {
			szBuffer[0]=0x00;
			for(int x=0; x<nSizeX; x++) {
				nValue = pData[y*nSizeX+x];
				sprintf(szValue,"%ld",nValue);

				strcat(szBuffer,szValue);
				if(x != nSizeX-1) {
					strcat(szBuffer,",");
				}

				if(strlen(szBuffer) >= 10000) {
					iFile.Write(szBuffer,strlen(szBuffer));
					szBuffer[0]=0x00;
				}
			}

			if(y != nSizeY-1) {
				strcat(szBuffer,"\n");
			}
			iFile.Write(szBuffer,strlen(szBuffer));
		}
		iFile.Close();
		_PrintDebugString("Save Height Data File : %s", szHeightDataFilePath);
	}
	catch (CException* pe) {
		pe;	// not to show warning message.
		return;
	}
}

// 분석용 데이터를 저장한다.
void CMeasureAlg::SaveAnalysisData()
{
	if(m_bSaveAnalysisData) {
		_SetStartTime();
		_MakeAnalysisDataFilePath();
		if(m_bSaveHeightData) {
			_SaveHeightDataFile();
		}
		_SaveAnalysisDataFile();

		m_dTime.m_msSaveAnalysisDataFile = _GetElapsedTime();
		_PrintDebugString("> Time to Save Analysis Data File : %d ms", m_dTime.m_msSaveAnalysisDataFile);
	}
}

// 분석용 데이터를 파일로 저장한다.
void CMeasureAlg::_SaveAnalysisDataFile()
{
//	char szId[16];
	char szValue[128];
	char szBuffer[10240];
	int nCount;
	MEASURE_DATA *pMeasureData = 0x00;
	MEASURE_RESULT_DATA *pMeasureResultData = 0x00;

	CFile iFile;
	try
	{
		char szResultDataFilePath[MAX_PATH];
		sprintf(szResultDataFilePath,"%s\\%s_A.csv", m_szAnalysisDataPath, m_dOutput.m_szDataFileName);
		if(!iFile.Open(szResultDataFilePath, CFile::modeCreate | CFile::modeWrite)) {
			return;
		}

		// 파라미터 저장
		SAVE_ANALYSIS_VALUE_FLOAT("NPP", m_dProc.m_nmPerPixel);
		SAVE_ANALYSIS_VALUE_FLOAT("NPH", m_dProc.m_nmPerHeight);
		SAVE_ANALYSIS_VALUE_FLOAT("THK", m_dInput.m_nTargetThick);
		SAVE_ANALYSIS_VALUE_FLOAT("NSG", m_dInput.m_nSpaceGap);
		SAVE_ANALYSIS_VALUE("MST", m_nMaskType);
		SAVE_ANALYSIS_VALUE("AGL", m_nMeasureAngle);
		SAVE_ANALYSIS_VALUE("ALL", m_bMeasureAll);

		// 알고리즘 공통 데이터 저장
/*
		SAVE_ANALYSIS_IMAGE_DATA("REF", m_dProc.m_pRefImage);
		SAVE_ANALYSIS_IMAGE_DATA("RML", m_dProc.m_pRefMeanLineImage);
		SAVE_ANALYSIS_IMAGE_DATA("HML", m_dProc.m_pHorizMeanLine);
		SAVE_ANALYSIS_POINTS_DATA("HMP", m_dProc.m_dHorizMinCenterPoints);
		SAVE_ANALYSIS_IMAGE_DATA("VML", m_dProc.m_pVertMeanLine);
		SAVE_ANALYSIS_POINTS_DATA("VMP", m_dProc.m_dVertMinCenterPoints);
*/

		// Colum Title 저장
		strcpy(szBuffer, "INDEX");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MW");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MWC");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MWL");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MWR");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MHT");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MHTL");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MHTR");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MHL");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MHEL");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MHR");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MHER");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MAL");
		ADD_ANALYSIS_RESULT_DATA_COLUMN("MAR");
		SAVE_ANALYSIS_RESULT_DATA();

		// 각 위치별 측정 데이터 저장
		for(UINT i=0; i<m_dProc.m_dMeasure.size(); i++) {
			pMeasureData = _GetMeasureData(i);
			if(pMeasureData==0x00) {
				continue;
			}

/*
			SAVE_ANALYSIS_PROC_IMAGE_DATA("HF",(i+1), pMeasureData->m_pHeightProfile);
			SAVE_ANALYSIS_PROC_IMAGE_DATA("IHF",(i+1), pMeasureData->m_pInverseHeightProfile);
			SAVE_ANALYSIS_PROC_IMAGE_DATA("AF",(i+1), pMeasureData->m_pAngleProfile);
			SAVE_ANALYSIS_PROC_POINTS_DATA("AP",(i+1), pMeasureData->m_dAngleMinPoints);

			SAVE_ANALYSIS_PROC_POS_DATA("SP", (i+1), pMeasureData->m_ptStart.x, pMeasureData->m_ptStart.y);
			SAVE_ANALYSIS_PROC_POS_DATA("EP", (i+1), pMeasureData->m_ptEnd.x, pMeasureData->m_ptEnd.y);

			SAVE_ANALYSIS_PROC_FEATUREPOINT_DATA("FPL", (i+1), pMeasureData->m_dFreaturePointLeft);
			SAVE_ANALYSIS_PROC_FEATUREPOINT_DATA("FPR", (i+1), pMeasureData->m_dFreaturePointRight);
*/
			// 측정 결과 저장
			pMeasureResultData = _GetMeasureResultData(i);
			sprintf(szBuffer, "%d", i+1);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_WIDTH);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_WIDTH_C);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_WIDTH_L);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_WIDTH_R);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_HEIGHT);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_HEIGHT_TOTAL_L);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_HEIGHT_TOTAL_R);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_HEIGHT_L);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_HEIGHT_E_L);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_HEIGHT_R);
			ADD_ANALYSIS_RESULT_DATA_VALUE(pMeasureResultData->RIB_HEIGHT_E_R);
			ADD_ANALYSIS_RESULT_DATA_VALUE_FLOAT(pMeasureResultData->RIB_ANGLE_L);
			ADD_ANALYSIS_RESULT_DATA_VALUE_FLOAT(pMeasureResultData->RIB_ANGLE_R);
			SAVE_ANALYSIS_RESULT_DATA();
		}

		iFile.Close();
		_PrintDebugString("# Save Result Data File : %s", szResultDataFilePath);
	}
	catch (CException* pe) {
		pe;	// not to show warning message.
		return;
	}
}

// 측정 알고리즘 수행 함수.
// Base 객체의 함수이며, Mask Type 별 공통 처리 사항이 구현되어 있음.
// 각 Mask Type 별 세부 측정 알고리즘은 해당 클래스에 구현되어 있음.
BOOL CMeasureAlg::Measure()
{
	_PrintDebugString("==== Start Measure ====");
	_PrintDebugString("> nmPerPixel : %.3f", m_dProc.m_nmPerPixel);
	_PrintDebugString("> nmPerHeight : %.3f", m_dProc.m_nmPerHeight);
	_PrintDebugString("> Target Thick : %.3f", m_dInput.m_nTargetThick);
	_PrintDebugString("> Sapce Gap : %.3f", m_dInput.m_nSpaceGap);

	switch(m_nMaskType) {
	case MASK_TYPE_DOT : _PrintDebugString("> Mask Type : DOT"); break;
	case MASK_TYPE_STRIPE : _PrintDebugString("> Mask Type : STRIPE"); break;
	default : 
		_PrintDebugString("Fail to Measure : Unknown Mask Type");
		return FALSE;
	}

	switch(m_nMeasureAngle) {
	case MEASURE_ANGLE_0 : _PrintDebugString("> Measure Dir : 0"); break;
	case MEASURE_ANGLE_45 : _PrintDebugString("> Measure Dir : 45"); break;
	default : 
		_PrintDebugString("Fail to Measure : Unknown Measure Dir");
		return FALSE;
	}

	if(m_bMeasureAll) _PrintDebugString("> Measure All : TRUE");
	else _PrintDebugString("> Measure All : FALSE");

	return TRUE;
}

// 결과 데이터(RESULT_DATA) 의 개수. Master 에 전달될 실제 결과 데이터 개수.
int CMeasureAlg::GetResultDataCount()
{
	return (int)_GetRealMeasureResultDataCount();
}

// 결과 데이터(RESULT_DATA) 배열을 반환하는 함수.
// 이 배열에 저장된 데이터가 Network 을 통해 Master 에 전달됨.
RESULT_DATA *CMeasureAlg::GetResultData()
{
	int nCount = GetResultDataCount();
	if(nCount == 0) {
		return 0x00;
	}

	m_dOutput.m_pResultDataBuffer = new RESULT_DATA[nCount];
	if(m_dOutput.m_pResultDataBuffer == 0x00) {
		return 0x00;
	}
	memset(m_dOutput.m_pResultDataBuffer, 0x00, sizeof(RESULT_DATA)*nCount);

	int nIndex = -1;
	BOOL bPrevSecondPhase = FALSE;
	MEASURE_DATA *pMeasureData = 0x00;
	MEASURE_RESULT_DATA *pMeasureResultData = 0x00;
	for(UINT i=0; i<m_dProc.m_dMeasure.size(); i++) {
		pMeasureData = _GetMeasureData(i);
		pMeasureResultData = _GetMeasureResultData(i);
		if(pMeasureData == 0x00 || pMeasureResultData == 0x00) {
			continue;
		}

		if(_IsNewRealMeasureResultData(pMeasureData)) {
			nIndex++;
		}

		if(pMeasureData->m_bSecondPhase == FALSE) {
			memcpy(&(m_dOutput.m_pResultDataBuffer[nIndex]), pMeasureResultData, sizeof(RESULT_DATA));
		}
		else {
			pMeasureResultData->CopyToSecondPhase(&(m_dOutput.m_pResultDataBuffer[nIndex]));
		}

		bPrevSecondPhase = pMeasureData->m_bSecondPhase;
	}

	return m_dOutput.m_pResultDataBuffer;
}

float CMeasureAlg::GetCornerDistance()
{
	return m_nCornerDistance;
}

// 데이터 전처리 함수. Base 함수로 실제 내용 구현부는 각 Mask Type 별 클래스에 있음.
BOOL CMeasureAlg::_PreprocessData()
{
	// Do Noting here. Logic should be implemented in a derived class.
	return TRUE;
}


// 참조 데이터 생성 함수. Base 함수로 실제 내용 구현부는 각 Mask Type 별 클래스에 있음.
BOOL CMeasureAlg::_GenerateRefData()
{
	// Do Noting here. Logic should be implemented in a derived class.
	return TRUE;
}

// MinCenterPoints 생성 함수. Base 함수로 실제 내용 구현부는 각 Mask Type 별 클래스에 있음.
BOOL CMeasureAlg::_GetMinCenterPoints()
{
	// Do Noting here. Logic should be implemented in a derived class.
	return TRUE;
}

// HeightProfile 생성 함수. Base 함수로 실제 내용 구현부는 각 Mask Type 별 클래스에 있음.
BOOL CMeasureAlg::_GetHeightProfile()
{
	// Do Noting here. Logic should be implemented in a derived class.
	return TRUE;
}

// 결과 데이터 측정 함수. Base 함수에는 공통 부분이 구현되어 있으며, 
// Mask Type 별 세부 내용은 개별 클래스에 구현되어 있음.
BOOL CMeasureAlg::_MeasureHeightProfile()
{
	MEASURE_DATA *pMeasureData = _GetCurMeasureData();
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	// 상하반전 높이 (Inversed Height) Profile 저장용 이미지 생성.
	IplImage *pInverseHeightProfile = cvCloneImage(pMeasureData->m_pHeightProfile);
	if(pInverseHeightProfile == 0x00) {
		return FALSE;
	}
	pMeasureData->m_pInverseHeightProfile = pInverseHeightProfile;

	FLOAT *pProfileData = (FLOAT *)pInverseHeightProfile->imageData;
	int nLengthProfile = pInverseHeightProfile->width;

	// Inversed Height Profile 데이터 생성.
	FLOAT nMaxHeight= _GetMaxData(pProfileData, 0, nLengthProfile-1);
	for(int i = 0; i < nLengthProfile; i++) pProfileData[i] = nMaxHeight - pProfileData[i];

	// 2D Data Smoothing 방식이 아닐 경우에 수행.
	if(m_b2DDataSmoothing == FALSE) {
		// 노이즈 제거를 위한 Smoothing 및 Closing 수행.
		// Smoothing. 전반적인 밝기 평준화(완화) 목적.
		cvSmooth(pInverseHeightProfile, pInverseHeightProfile, CV_BLUR, m_dParam.m_nHeightProfileSmoothFilerSize);
		// Closing. MidPoint 근처의 심하게 튀는 노이즈의 완화 목적.
		IplImage *pTemp = cvCloneImage(pInverseHeightProfile);
		IplConvKernel *pElement = cvCreateStructuringElementEx(m_dParam.m_nHeightProfileSmoothFilerSize, 1, cvFloor(m_dParam.m_nHeightProfileSmoothFilerSize/2.f), 0, CV_SHAPE_RECT, NULL);
		cvMorphologyEx(pInverseHeightProfile, pInverseHeightProfile, pTemp, pElement, CV_MOP_CLOSE, 1); 
		cvReleaseImage(&pTemp);
		cvReleaseStructuringElement(&pElement);
	}

	 // Angle Profile (포인트별 각도를 저장한 Profile) 를 구함.
	 if(_GetAngleProfile() == FALSE) {
		 return FALSE;
	 }

	 // Angle Profile 에서 MinPoints(지역적으로 Angle값이 최소값인 포인트)를 찾는다.
	if(_FindAngleMinPoints() == FALSE) {
		return FALSE;
	}

//	_PrintDebugPoints(pMeasureData->m_dAngleMinPoints, "AngleMinPoints");

	// 좌측 부분의 FeaturePoint (데이터 측정/계산에 사용되는 주요 3개의 특징점)를 선택한다.
	if(_SelectFeaturePoints(FALSE) == FALSE) {
		return FALSE;
	}
	// 우측 부분의 FeaturePoint 를 선택한다.
	if(_SelectFeaturePoints(TRUE) == FALSE) {
		return FALSE;
	}

	_PrintDebugString("  Left Feature Point : High(%d, %.3f), Mid(%d, %.3f), Low(%d, %.3f)", 
		pMeasureData->m_dFreaturePointLeft.m_nHighPos, pMeasureData->m_dFreaturePointLeft.m_nHighPosHeight,
		pMeasureData->m_dFreaturePointLeft.m_nMidPos, pMeasureData->m_dFreaturePointLeft.m_nMidPosHeight,
		pMeasureData->m_dFreaturePointLeft.m_nLowPos, pMeasureData->m_dFreaturePointLeft.m_nLowPosHeight);
	_PrintDebugString("  Right Feature Point : High(%d, %.3f), Mid(%d, %.3f), Low(%d, %.3f)", 
		pMeasureData->m_dFreaturePointRight.m_nHighPos, pMeasureData->m_dFreaturePointRight.m_nHighPosHeight,
		pMeasureData->m_dFreaturePointRight.m_nMidPos, pMeasureData->m_dFreaturePointRight.m_nMidPosHeight,
		pMeasureData->m_dFreaturePointRight.m_nLowPos,  pMeasureData->m_dFreaturePointRight.m_nLowPosHeight);

	// 결과 데이터(MEASURE_RESULT_DATA)를 생성한다.
	if(_MakeResultData() == FALSE) {
		return FALSE;
	}

	return TRUE;
}

DWORD CMeasureAlg::_SetStartTime(DWORD nStartTime)
{
	if(nStartTime != 0) {
		m_nStartTime = nStartTime;
	}
	else {
		m_nStartTime = ::GetTickCount();
	}

	return m_nStartTime;
}

DWORD CMeasureAlg::_GetElapsedTime()
{
	return ::GetTickCount() - m_nStartTime;
}

// MinCenterPoints 를 찾는 함수.
BOOL CMeasureAlg::_FindMinCenterPoints(BOOL bHoriz)
{
	FLOAT *pLineData = 0x00;
	int nLengthLine = 0;
	Points *pCenterPoints = 0x00;
	if(bHoriz) {
		pLineData = (FLOAT *)m_dProc.m_pHorizMeanLine->imageData;
		nLengthLine = m_dProc.m_nWidth;
		pCenterPoints = &m_dProc.m_dHorizMinCenterPoints;
	}
	else {
		pLineData = (FLOAT *)m_dProc.m_pVertMeanLine->imageData;
		nLengthLine = m_dProc.m_nHeight;
		pCenterPoints = &m_dProc.m_dVertMinCenterPoints;
	}

	int nBarSize = m_dParam.m_nMinCenterPointsBarSize;
	FLOAT nThreshold = m_dParam.m_nMinCenterPointsThreshold;

	float nMeanLeftBar = 0;
	float nMeanRightBar = 0;
	float nDiffCur = 0;
	float nDiffNext = 0;
	float nValuePoint = 0;

	Points dPoints;
	BOOL bAddPoint = FALSE;

	// Find Candidate Min Points
	BOOL bDirDownCur = TRUE;
	BOOL bDirUpNext = FALSE;
	for(int i=nBarSize;i<nLengthLine-nBarSize;i++) {
		// 현재점을 기준으로 좌측 및 우측 부분의 평균값을 구한다.
		nMeanLeftBar = _CalcAvgData(pLineData, i-nBarSize, i-1);
		nMeanRightBar = _CalcAvgData(pLineData, i+1, i+nBarSize);

		// 좌측 및 우측 부분의 기울기를 구한다.
		nValuePoint = pLineData[i];
		nDiffCur = pLineData[i] - nMeanLeftBar;
		nDiffNext = nMeanRightBar - pLineData[i];

		// 현재 위치 '하강' 여부 확인
		if(nDiffCur < 0) bDirDownCur = TRUE;
		else if (nDiffCur > 0) bDirDownCur = FALSE;
		// if nDiffCur == 0, then don't change bDirDownCur.

		// 현재 위치 '상승' 여부 확인
		if(nDiffNext > 0) bDirUpNext = TRUE;
		else bDirUpNext = FALSE;

		// 다음의 경우, MinPoint(최소값을 가지는 포인트)로 간주한다.
		// 참조 데이터가 임계치가 보다 낮고,
		if(nValuePoint<nThreshold) {
			bAddPoint = FALSE;
			if(i == nBarSize && bDirUpNext) bAddPoint = TRUE;				// 첫번째 포인트이면서 상승하고 있는 경우
			else if(i == (nLengthLine-nBarSize-1) && bDirUpNext == FALSE) bAddPoint = TRUE;		// 하강을 했다가 상승을 하는 경우
			else if(bDirDownCur && bDirUpNext) bAddPoint = TRUE;		// 마지막 포인트이면서 하강을 하고 있은 경우

			if(bAddPoint) {
				dPoints.push_back(i);
			}
		}
	}

	// 최소값이 포인트가 아닌 영역을 형성하고 있는 경우를 대비하여,
	// 그 영역의 가운데 점만을 MinPoint 로 간주한다.
	int nPos = 0;
	int nMinPos = -1;
	int nMaxPos = -1;
	for(UINT i=0;i<dPoints.size();i++) {
		nPos = dPoints[i];
		if(nPos <= nMaxPos) {		// 이미 최소 영역내에 포함된 경우는 Pass.
			continue;
		}

		nMinPos = nPos;
		nMaxPos = nPos;

		// 현재점을 기준으로 좌측으로 임계치보다 낮은 위치를 찾아냄.
		while(pLineData[nMinPos] < nThreshold) {
			nMinPos--;
			if(nMinPos < 0) {
				break;
			}
		}
		// 현재점을 기준으로 우측으로 임계치보다 낮은 위치를 찾아냄.
		while(pLineData[nMaxPos] < nThreshold) {
			nMaxPos++;
			if(nMaxPos > nLengthLine-1) {
				break;
			}
		}

		if(nMinPos < 0) nPos = 0;		// 좌측 위치가 벗어날 경우, 첫번째 포인트 위치로 설정. 추후 위치 보정됨.
		else if(nMaxPos > nLengthLine-1) nPos = nLengthLine-1;		// 좌측 위치가 벗어날 경우, 마지막 포인트 위치로 설정. 추후 위치 보정됨.
		else nPos = cvFloor((nMinPos + nMaxPos)/2.f);		// 양측 위치의 중간값으로 설정.

		pCenterPoints->push_back(nPos);
	}

	return TRUE;
}

// 첫번째 및 마지막 MinCenterPoint 의 위치를 보정함.
void CMeasureAlg::_AdjustMinCenterEndPoints(BOOL bHoriz)
{
	Points *pCenterPoints = 0x00;
	if(bHoriz) {
		pCenterPoints = &m_dProc.m_dHorizMinCenterPoints;
	}
	else {
		pCenterPoints = &m_dProc.m_dVertMinCenterPoints;
	}

	int nLength = pCenterPoints->size();
	if(nLength<4) {		// 알고리즘 수행을 위해서는 기본 4개의 포인트가 필요함.
		return;
	}

	// 첫번째와 마지막 포인트를 제외한 중간 포인트들 만으로
	// 각 포인트들 사이의 평균 거리를 구함.
	float nAvgDiff = 0;
	for(int i=2; i<=nLength-2; i++) {
		nAvgDiff += ((*pCenterPoints)[i] - (*pCenterPoints)[i-1]);
	}
	nAvgDiff = nAvgDiff/(nLength-3);

	int nDist = cvFloor(nAvgDiff) + 1;
	// 첫번째 포인트가 평균 거리보다 멀리 떨어져 있으며 평균 거리로 보정
	if(((*pCenterPoints)[1] - (*pCenterPoints)[0]) > nDist) {
		(*pCenterPoints)[0] = (*pCenterPoints)[1] - nDist;
	}
	// 마지막 포인트가 평균 거리보다 멀리 떨어져 있으며 평균 거리로 보정
	if(((*pCenterPoints)[nLength-1] - (*pCenterPoints)[nLength-2]) > nDist) {
		(*pCenterPoints)[nLength-1] = (*pCenterPoints)[nLength-2] + nDist;
	}
}

// 측정 위치 보정을 위한 Offset 을 적용한다.
void CMeasureAlg::_ApplyMinCenterPointsOffset(BOOL bHoriz)
{
	Points *pCenterPoints = 0x00;
	int nMaxSize = 0;
	int nOffset = 0;
	if(bHoriz) {
		pCenterPoints = &m_dProc.m_dHorizMinCenterPoints;
		nMaxSize = m_dProc.m_nWidth;
		nOffset = m_nMeasureOffsetX;
	}
	else {
		pCenterPoints = &m_dProc.m_dVertMinCenterPoints;
		nMaxSize = m_dProc.m_nHeight;
		nOffset = m_nMeasureOffsetY;
	}

	int nNewPos = 0;
	int nLength = pCenterPoints->size();
	for(int i = 0; i < nLength; i++) {
		nNewPos = (*pCenterPoints)[i] + nOffset;
		if(nNewPos < 0) nNewPos = 0;
		else if(nNewPos > nMaxSize - 1) nNewPos = nMaxSize - 1;
		(*pCenterPoints)[i] = nNewPos;
	}
}

// Corner 의 여백 부분의 길이를 계산한다.
int CMeasureAlg::_CheckMeanLineMargin(IplImage *pMeanLine, BOOL bReverse)
{
	if(pMeanLine == 0x00) {
		return -1;
	}

	FLOAT *pLineData = (FLOAT *)pMeanLine->imageData;
	int nLength = pMeanLine->width;

	if(bReverse) {	// swap both sides
		REVERSE_DATA(pLineData, nLength);
	}

	int nMarginSize = m_dParam.m_nCornerDistanceMarginSize;
	int nIndex = -1;
	for(int i = 0; i < nLength; i++) {
		if(pLineData[i] < 1) {
			if(i >= nMarginSize) {
				if(bReverse) {
					nIndex = nLength - i;
				}
				else {
					nIndex = i - 1;
				}
			}

			break;
		}
	}

	if(bReverse) {	// swap both sides
		REVERSE_DATA(pLineData, nLength);
	}

	return nIndex;
}

// Corner Hole 의 경계 시작 위치를 찾아낸다.
int CMeasureAlg::_GetCornerPos(IplImage *pMeanLine, BOOL bReverse)
{
	if(pMeanLine == 0x00) {
		return -1;
	}

	FLOAT *pLineData = (FLOAT *)pMeanLine->imageData;
	int nLength = pMeanLine->width;

	if(bReverse) {	// swap both sides
		REVERSE_DATA(pLineData, nLength);
	}

	// 각도는 Pixel 단위가 아니라 실제 거리로 변환하여 구한다.
	FLOAT nmPerPixel = m_dProc.m_nmPerPixel;
	FLOAT nmPerHeight = m_dProc.m_nmPerHeight;

	int nLeftPosStart = 0;
	int nLeftPosEnd = 0;
	int nRightPosStart = 0;
	int nRightPosEnd = 0;
	int nBarSize = m_dParam.m_nCornerDiatanceAngleBarSize;
	CvPoint2D32f ptCenter;
	CvPoint2D32f ptLeft;
	CvPoint2D32f ptRight;
	FLOAT nAngle = 0;
	FLOAT nMinAngle = 180;
	int nIndex = -1;

	double nMaxHeight = 0;
	double nMinHeight = 0;
	cvMinMaxLoc(pMeanLine, &nMinHeight, &nMaxHeight);
	FLOAT nThreshold = (FLOAT)(nMaxHeight * m_dParam.m_nCornerDistanceThreshold);

	for (int i=nBarSize; i<nLength-nBarSize; i++) {
		if(pLineData[i] < nThreshold) {
			continue;
		}

		// 각도를 구하기 위해서는 현재 포인트를 Center 로 하고, 좌측과 우측의 (가상의) 점을 만들어
		// 세 개의 점을 이용한 내적을 통해 구한다.
		nLeftPosStart = i-nBarSize;
		nLeftPosEnd = i-1;
		if(nLeftPosStart < 0) nLeftPosStart = 0;
		if(nLeftPosEnd < 0) nLeftPosEnd = 0;

		nRightPosStart = i+1;
		nRightPosEnd = i+nBarSize;
		if(nRightPosStart > nLength-1) nRightPosStart = nLength-1;
		if(nRightPosEnd > nLength-1) nRightPosEnd = nLength-1;

		ptCenter.x = i * nmPerPixel;
		ptCenter.y = pLineData[i] * nmPerHeight;
		ptLeft.x = _CalcAvgData(nLeftPosStart,nLeftPosEnd) * nmPerPixel;
		ptLeft.y = _CalcAvgData(pLineData,nLeftPosStart,nLeftPosEnd) * nmPerHeight;
		ptRight.x = _CalcAvgData(nRightPosStart,nRightPosEnd) * nmPerPixel;
		ptRight.y = _CalcAvgData(pLineData,nRightPosStart,nRightPosEnd) * nmPerHeight;

		nAngle = _CalcAngle(ptLeft, ptCenter, ptRight);
		if(nAngle < nMinAngle) {
			nMinAngle = nAngle;
			nIndex = i;
		}
	}

	if(bReverse) {	// swap both sides
		REVERSE_DATA(pLineData, nLength);

		// 좌우 변경에따라 인덱스를 조정해 준다.
		nIndex = nLength - 1 - nIndex;
	}

	return nIndex;
}

// Image 의 SubImage 를 반환한다.
IplImage *CMeasureAlg::_GetSubImage(IplImage *pImage, int nStartX, int nEndX, int nStartY, int nEndY)
{
	if(pImage == 0x00) {
		return 0x00;
	}

	int nSizeX = nEndX - nStartX + 1;
	int nSizeY = nEndY - nStartY + 1;

	CvRect rcROI;
	rcROI.x = nStartX;
	rcROI.y = nStartY;
	rcROI.width = nSizeX;
	rcROI.height = nSizeY;

	cvSetImageROI(pImage, rcROI);
	IplImage *pSubImage = cvCreateImage(cvSize(nSizeX, nSizeY), pImage->depth, pImage->nChannels);
	if(pSubImage == 0x00) {
		cvResetImageROI(pImage);
		return 0x00;
	}
	cvCopy(pImage, pSubImage);
	cvResetImageROI(pImage);

	return pSubImage;
}

// Image 의 가로/세로의 Mean Line 을 반환한다.
IplImage *CMeasureAlg::_GetMeanLine(IplImage *pImage, BOOL bHoriz)
{
	if(pImage == 0x00) {
		return 0x00;
	}

	int nSizeX = pImage->width;
	int nSizeY = pImage->height;

	IplImage *pFloatImage = _GetFloatImage(pImage);
	if(pFloatImage == 0x00) {
		return 0x00;
	}

	IplImage *pMeanLine = 0x00;
	if(bHoriz) {
		pMeanLine = cvCreateImage(cvSize(nSizeX, 1), IPL_DEPTH_32F, 1);
		if(pMeanLine == 0x00) {
			return 0x00;
		}

		cvReduce(pFloatImage, pMeanLine, 0, CV_REDUCE_AVG);
	}
	else {
		pMeanLine = cvCreateImage(cvSize(nSizeY, 1), IPL_DEPTH_32F, 1);
		if(pMeanLine == 0x00) {
			return 0x00;
		}
		IplImage *pTempLine = cvCreateImage(cvSize(1, nSizeY), IPL_DEPTH_32F, 1);
		if(pTempLine == 0x00) {
			cvReleaseImage(&pMeanLine);
			return 0x00;
		}
		cvReduce(pFloatImage, pTempLine, 1, CV_REDUCE_AVG);
		cvTranspose(pTempLine, pMeanLine);

		cvReleaseImage(&pTempLine);
	}

	cvReleaseImage(&pFloatImage);
	
	return pMeanLine;
}

IplImage *CMeasureAlg::_GetFloatImage(IplImage *pImage)
{
	if(pImage == 0x00) {
		return 0x00;
	}

	int nSizeX = pImage->width;
	int nSizeY = pImage->height;

	IplImage *pConvertedImage = cvCreateImage(cvSize(nSizeX, nSizeY), IPL_DEPTH_32F, 1);
	if(pConvertedImage == 0x00) {
		return 0x00;
	}

	UINT32 nValue;
	for(int y=0; y<nSizeY; y++) {
		for(int x=0; x<nSizeX; x++) {
			nValue = CV_IMAGE_ELEM(pImage, UINT32, y, x);
			CV_IMAGE_ELEM(pConvertedImage, FLOAT, y, x) = (FLOAT)nValue;
		}
	}

	return pConvertedImage;
}

// Image 의 가로/세로의 Mean Line 을 반환한다.
IplImage *CMeasureAlg::_GetSubImageMeanLine(IplImage *pImage, int nStartX, int nEndX, int nStartY, int nEndY, BOOL bHoriz)
{
	IplImage *pSubImage = _GetSubImage(pImage, nStartX, nEndX, nStartY, nEndY); 
	if(pSubImage == 0x00) {
		return 0x00;
	}

	// 노이즈 완화를 위해 Smoothing 을 한다.
	cvSmooth(pSubImage, pSubImage, CV_BLUR, m_dParam.m_nCornerDiatanceSmoothFilerSize);

	IplImage *pMeanLine = _GetMeanLine(pSubImage, bHoriz);
	if(pMeanLine == 0x00) {
		cvReleaseImage(&pSubImage);
		return 0x00;
	}

	cvReleaseImage(&pSubImage);
	return pMeanLine;
}

// 전체 검사(MeasureAll) 모드가 아닐 경우, 이미지 중앙에 있는 포인트를 시작점으로 간주하고 찾는다.
BOOL CMeasureAlg::_SelectStartMinCenterPoint(BOOL bHoriz, int &nStartPos, int &nStartPosIndex)
{
	int nLengthLine = 0;
	Points *pCenterPoints = 0x00;
	if(bHoriz) {
		nLengthLine = m_dProc.m_nWidth;
		pCenterPoints = &m_dProc.m_dHorizMinCenterPoints;
	}
	else {
		nLengthLine = m_dProc.m_nHeight;
		pCenterPoints = &m_dProc.m_dVertMinCenterPoints;
	}
	int nCountCenterPoints = pCenterPoints->size();

	FLOAT nThreshold = m_dParam.m_nMinCenterPointsThreshold;

	// Find a position most closed to the image center
	int nPos = 0;
	int nDist = 0;
	int nMinDist = nLengthLine;
	int nMinDistPos = -1;
	int nMinDistIndex = -1;
	int nCenterPos = cvFloor(nLengthLine/2.f);

	// 각 포인트별로 Center 위치로 부터의 거리를 구하여
	// 최소값을 가지는 포인트를 선택한다.
	for(int i=0;i<nCountCenterPoints;i++) {
		nPos = (*pCenterPoints)[i];
		nDist = abs(nPos - nCenterPos);

		if(nDist < nMinDist) {
			nMinDist = nDist;
			nMinDistPos = nPos;
			nMinDistIndex = i;
		}
	}

	if(nMinDistPos == -1) {
		return FALSE;
	}

	nStartPos = nMinDistPos;
	nStartPosIndex = nMinDistIndex;

	return TRUE;
}

int CMeasureAlg::_GetMinCenterPointIndex(int nPos, BOOL bHoriz)
{
	Points *pPoints = 0x00;
	if(bHoriz) pPoints = &m_dProc.m_dHorizMinCenterPoints;
	else pPoints = &m_dProc.m_dVertMinCenterPoints;

	for(UINT i=0; i<pPoints->size(); i++) {
		if((*pPoints)[i] == nPos) {
			return i;
		}
	}

	return -1;
}

// AngleProfile 를 반환한다. AngleProfile 은 각 포인트에서의 각도를 저장하고 있음.
BOOL CMeasureAlg::_GetAngleProfile()
{
	MEASURE_DATA *pMeasureData = _GetCurMeasureData();
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	// Inversed Height Profile 의 Angle 을 구함.
	FLOAT *pProfileData = (FLOAT *)pMeasureData->m_pInverseHeightProfile->imageData;
	int nLengthProfile = pMeasureData->m_pInverseHeightProfile->width;

	IplImage *pAngleProfile = cvCreateImage(cvSize(nLengthProfile, 1), IPL_DEPTH_32F, 1);
	if(pAngleProfile == 0x00) {
		return FALSE;
	}
	pMeasureData->m_pAngleProfile = pAngleProfile;

	FLOAT *pAngleData = (FLOAT *)pAngleProfile->imageData;

	// 각도는 Pixel 단위가 아니라 실제 거리로 변환하여 구한다.
	FLOAT nmPerPixel = m_dProc.m_nmPerPixel;
	FLOAT nmPerHeight = m_dProc.m_nmPerHeight;
	if(m_nMeasureAngle == MEASURE_ANGLE_45) nmPerPixel *= (FLOAT)sqrt(2.0);

	int nLeftPosStart = 0;
	int nLeftPosEnd = 0;
	int nRightPosStart = 0;
	int nRightPosEnd = 0;
	int nBarSize = m_dParam.m_nAngleProfileBarSize;
	CvPoint2D32f ptCenter;
	CvPoint2D32f ptLeft;
	CvPoint2D32f ptRight;
	FLOAT nAngle = 0;

	for (int i=0; i<nLengthProfile; i++) {
		if(i==0 || i==nLengthProfile-1) {			// 첫번째 및 마지막 포인트는 180 도로 간주
			nAngle = 180.0;
		}
		else {
			// 각도를 구하기 위해서는 현재 포인트를 Center 로 하고, 좌측과 우측의 (가상의) 점을 만들어
			// 세 개의 점을 이용한 내적을 통해 구한다.
			nLeftPosStart = i-nBarSize;
			nLeftPosEnd = i-1;
			if(nLeftPosStart < 0) nLeftPosStart = 0;
			if(nLeftPosEnd < 0) nLeftPosEnd = 0;

			nRightPosStart = i+1;
			nRightPosEnd = i+nBarSize;
			if(nRightPosStart > nLengthProfile-1) nRightPosStart = nLengthProfile-1;
			if(nRightPosEnd > nLengthProfile-1) nRightPosEnd = nLengthProfile-1;

			ptCenter.x = i * nmPerPixel;
			ptCenter.y = pProfileData[i] * nmPerHeight;
			ptLeft.x = _CalcAvgData(nLeftPosStart,nLeftPosEnd) * nmPerPixel;
			ptLeft.y = _CalcAvgData(pProfileData,nLeftPosStart,nLeftPosEnd) * nmPerHeight;
			ptRight.x = _CalcAvgData(nRightPosStart,nRightPosEnd) * nmPerPixel;
			ptRight.y = _CalcAvgData(pProfileData,nRightPosStart,nRightPosEnd) * nmPerHeight;

			nAngle = _CalcAngle(ptLeft, ptCenter, ptRight);
		}

		if(nAngle > m_dParam.m_nAngleProfileThreshold) {
			nAngle = 180;
		}

		pAngleData[i] = nAngle;
	}

//	_PrintDebugVector(pAngleProfile, 0, nLengthProfile, "AngleProfile");

	return TRUE;
}

// AngleMinPoints 를 구한다. 
// Angle Min Points 는 AngleProfile 내의 지역적으로 최소값을 갖는 포인트를 의미한다.
BOOL CMeasureAlg::_FindAngleMinPoints()
{
	MEASURE_DATA *pMeasureData = _GetCurMeasureData();
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	IplImage *pAngleProfile = pMeasureData->m_pAngleProfile;
	FLOAT *pAngleData = (FLOAT *)pAngleProfile->imageData;
	int nLengthProfile = pAngleProfile->width;

	int nBarSize = m_dParam.m_nAngleMinPointsBarSize;
	FLOAT nThreshold = m_dParam.m_nAngleProfileThreshold;

	float nMeanLeftBar = 0;
	float nMeanRightBar = 0;
	float nDiffCur = 0;
	float nDiffNext = 0;
	float nValuePoint = 0;

	Points &dPoints = pMeasureData->m_dAngleMinPoints;

	// Find Candidate Min Points
	BOOL bDirDownCur = TRUE;
	BOOL bDirUpNext = FALSE;
	for(int i=nBarSize;i<nLengthProfile-nBarSize;i++) {
		// 현재 포인트를 기준으로 좌측과 우측의 기울기를 구한다.
		nMeanLeftBar = _CalcAvgData(pAngleData, i-nBarSize, i-1);
		nMeanRightBar = _CalcAvgData(pAngleData, i+1, i+nBarSize);

		nValuePoint = pAngleData[i];
		nDiffCur = pAngleData[i] - nMeanLeftBar;
		nDiffNext = nMeanRightBar - pAngleData[i];

		// 현재 위치에서의 '하강' 여부
		if(nDiffCur < 0) bDirDownCur = TRUE;
		else if (nDiffCur > 0) bDirDownCur = FALSE;
		// if nDiffCur == 0, then don't change bDirDownCur.

		// 다음 위치에서의 '상승' 여부
		if(nDiffNext > 0) bDirUpNext = TRUE;
		else bDirUpNext = FALSE;

		// Angle 값이 임계치보다 낮으며
		if(nValuePoint<nThreshold) {
			// 현재 하강하여 다음에 상승하는 포인트를 찾음.
			if(bDirDownCur && bDirUpNext) {
				dPoints.push_back(i);
			}
		}
	}

	return TRUE;
}

// FeaturePoints 를 선택한다.
// Feature Points 는 데이터 측정시 사용되는 3개의 주요 특징점으로 HighPos, MidPos, LowPos 로 구성되어 있다.
BOOL CMeasureAlg::_SelectFeaturePoints(BOOL bRightPart)
{
	MEASURE_DATA *pMeasureData = _GetCurMeasureData();
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	int *pHighPos = 0x00;
	int *pMidPos = 0x00;
	int *pLowPos = 0x00;
	float *pHighPosHeight = 0x00;
	float *pMidPosHeight = 0x00;
	float *pLowPosHeight = 0x00;
	if(bRightPart) {
		pHighPos = &pMeasureData->m_dFreaturePointRight.m_nHighPos;
		pMidPos = &pMeasureData->m_dFreaturePointRight.m_nMidPos;
		pLowPos = &pMeasureData->m_dFreaturePointRight.m_nLowPos;
		pHighPosHeight = &pMeasureData->m_dFreaturePointRight.m_nHighPosHeight;
		pMidPosHeight = &pMeasureData->m_dFreaturePointRight.m_nMidPosHeight;
		pLowPosHeight = &pMeasureData->m_dFreaturePointRight.m_nLowPosHeight;
	}
	else {
		pHighPos = &pMeasureData->m_dFreaturePointLeft.m_nHighPos;
		pMidPos = &pMeasureData->m_dFreaturePointLeft.m_nMidPos;
		pLowPos = &pMeasureData->m_dFreaturePointLeft.m_nLowPos;
		pHighPosHeight = &pMeasureData->m_dFreaturePointLeft.m_nHighPosHeight;
		pMidPosHeight = &pMeasureData->m_dFreaturePointLeft.m_nMidPosHeight;
		pLowPosHeight = &pMeasureData->m_dFreaturePointLeft.m_nLowPosHeight;
	}

	Points &dAngleMinPoints = pMeasureData->m_dAngleMinPoints;
	int nLengthPoints = dAngleMinPoints.size();
	if(nLengthPoints<3) {		// AnglePoint 의 수가 적으면 그냥 빠져나간다.
		return FALSE;
	}

	// InverseHeightProfile 을 이용한다.
	IplImage *pHeightProfile = pMeasureData->m_pInverseHeightProfile;
	FLOAT *pHeightData = (FLOAT *)pHeightProfile->imageData;
	int nLengthProfile = pHeightProfile->width;

	// AngleProfile 을 이용한다.
	IplImage *pAngleProfile = pMeasureData->m_pAngleProfile;
	FLOAT *pAngleData = (FLOAT *)pAngleProfile->imageData;

	if(bRightPart) {	// swap both sides
		REVERSE_DATA(pHeightData, nLengthProfile);
		REVERSE_DATA(pAngleData, nLengthProfile);
		REVERSE_POINTS(dAngleMinPoints, nLengthProfile);
	}

	int nHighPos = 0;
	int nMidPos = 0;
	int nLowPos = 0;

	//
	// High Pos 찾기. 첫번째 Angle 위치부터 임계치 이상인 포인트들 중 각도가 가장 작은 것을 찾음.
	//
	FLOAT nHighThreshold = pHeightData[dAngleMinPoints[0]] * (1.f - m_dParam.m_nFeaturePointHighPosThreshold);
	FLOAT nHighAngleThreshold = m_dParam.m_nFeaturePointHighPosAngleThreshold;

	int nPos = 0;
	float nAngle = 0.f;
	float nMinAngle = 180.f;
	for(int i = 0; i<=nLengthPoints-1; i++) {
		nPos = dAngleMinPoints[i];
		if(pHeightData[nPos] < nHighThreshold) {
			break;
		}

		nAngle = pAngleData[nPos];
		if(nAngle < nHighAngleThreshold && nAngle < nMinAngle)  {
			nHighPos = nPos;
			nMinAngle = nAngle;
		}
	}

	int nHighPosOppsit = dAngleMinPoints[nLengthPoints-1];
	nAngle = 0.f;
	nMinAngle = 180.f;
	for(int i = nLengthPoints-1; i>=0; i--) {
		nPos = dAngleMinPoints[i];
		if(pHeightData[nPos] < nHighThreshold) {
			break;
		}
		nAngle = pAngleData[nPos];
		if(nAngle < nHighAngleThreshold && nAngle < nMinAngle)  {
			nHighPosOppsit = nPos;
			nMinAngle = nAngle;
		}
	}

	//
	// Mid Pos 후보 찾기. 첫번째 Angle 위치부터 임계치 이하인 첫번째 포인트를 찾음.
	//
	nHighThreshold = pHeightData[dAngleMinPoints[0]] * (1.f - m_dParam.m_nFeaturePointMidPosThreshold);
	nPos = 0;
	nAngle = 0.f;
	nMinAngle = 180.f;
	for(int i=0; i<nLengthPoints; i++) {
		nPos = dAngleMinPoints[i];
		if(nPos <= nHighPos) {
			continue;
		}

		nAngle = pAngleData[nPos];
		if(nAngle < nMinAngle) {
			nMidPos = nPos;
			nMinAngle = nAngle;
		}

		if(pHeightData[nPos] < nHighThreshold) {
			break;
		}
	}

	//
	// Low Pos 후보 찾기. 가운데를 중심으로 좌우로 최소값을 가지는 포인트를 찾는다.
	// 최소값이 두개가 있을 수도 있고 하나만 있을 수도 있기 때문에, 기울기 검사를 통해
	// Real 최소값을 가지는 포인트를 찾아 Low Pos 후보로 한다.
	// 만약, 양쪽 모두 Real 최소값일 경우 왼쪽편의 것을 선택한다.
	//

	int nHalfPos = cvFloor((nHighPos + nHighPosOppsit)/2.f);
	int nCheckSize = cvFloor((nHalfPos- nHighPos)/2.f);
	int nHalfIndex = 0;
	int nMinPosLeftPart;
	int nMinPosRightPart;

	// Check Left Part
	// Add min height point of left part into dAngleMinPoints 
	int nMinPos = _GetMinDataIndex(pHeightData, nHalfPos-nCheckSize, nHalfPos);
	Points::iterator it = std::find(dAngleMinPoints.begin(), dAngleMinPoints.end(), nMinPos);
	if(it == dAngleMinPoints.end()) {
		dAngleMinPoints.push_back(nMinPos);
		std::sort(dAngleMinPoints.begin(), dAngleMinPoints.end());
	}
	nMinPosLeftPart = nMinPos;

	// Check Right Part
	// Add min height point of right part into dAnglePoint 
	nMinPos = _GetMinDataIndex(pHeightData, nHalfPos, nHalfPos+nCheckSize);
	it = std::find(dAngleMinPoints.begin(), dAngleMinPoints.end(), nMinPos);
	if(it == dAngleMinPoints.end()) {
		dAngleMinPoints.push_back(nMinPos);
		std::sort(dAngleMinPoints.begin(), dAngleMinPoints.end());
	}
	nMinPosRightPart = nMinPos;

	int nBarSize = (int)(nLengthProfile * m_dParam.m_nFeaturePointLowPosBarSizeRatio);
	FLOAT nHeightLeftBar = _CalcAvgData(pHeightData, nMinPosLeftPart-nBarSize, nMinPosLeftPart-1);
	FLOAT nHeightRightBar = _CalcAvgData(pHeightData, nMinPosLeftPart+1, nMinPosLeftPart+nBarSize);
	FLOAT nHeight = pHeightData[nMinPosLeftPart];
	
	if(nHeightLeftBar >= nHeight && nHeightRightBar >= nHeight) {
		nLowPos = nMinPosLeftPart;
	}
	else {
		nLowPos = nMinPosRightPart;
	}

	//
	// Low Pos 위치 조정. 중앙 가운데가 평평한 경우 최소값을 최대한 왼쪽편으로 이동시킨다.
	// Mid 와 Low 위치사이 포인트중에서 바닥면에 위치하면서 각도가 작은 것을 찾는다.
	//
	int nStartPos = nMidPos + 1;
	FLOAT nMax = _GetMaxData(pHeightData, nStartPos, nLowPos);
	FLOAT nMin = _GetMinData(pHeightData, nStartPos, nLowPos);
	FLOAT nLowThreshold = nMin + (nMax - nMin) * m_dParam.m_nFeaturePointLowPosThreshold;
	FLOAT nAngleThreshold = m_dParam.m_nFeaturePointLowPosAngleThreshold;

	for(int i=nLengthPoints-1; i>=0; i--) {
		nPos = dAngleMinPoints[i];
		if(nPos>=nLowPos || nPos <= nStartPos) {
			continue;
		}

		if(pHeightData[nPos] > nLowThreshold) {
			break;
		}

		if(pAngleData[nPos] < nAngleThreshold) {
			nLowPos = nPos;
		} 
	}

	//
	// Mid Pos 위치 조정. Mid 위치 근처에 노이즈가 많은 경우가 있어서 이에 대한 보정을 한다.
	//
	float nEstMidPosHeight = 0.f;
	if(m_bEstimateFeaturePoint) {
		// Mid Point 의 위치를 추정하는 방식.
		// Mid 와 Low 위치사이 포인트들로 근사 함수(Gamma/Log/Line) 을 만들어 Mid Point 의 Height 값을 유추한다.
		Point2D dPoint;
		Point2Ds dPoints;
		for(int i=0; i<nLengthPoints; i++) {
			nPos = dAngleMinPoints[i];
			if(nPos > nMidPos && nPos < nLowPos) {
				dPoint.x = (FLOAT)nPos;
				dPoint.y = pHeightData[nPos];
				dPoints.push_back(dPoint);
			}
		}

		nEstMidPosHeight = _EstimateMidPosHeight(dPoints, nLowPos, pHeightData[nLowPos], nMidPos, pHeightData[nMidPos]);
	}
	else {
		// Mid 와 Low 위치사이 포인트들로 근사 Line 을 만들어 각 포인트들과 근사 Line 의 평균 오차를 구한다.
		// Mid 포인트의 오차도 계산하여 만약 평균 오차보다 크게 차이가 날 경우 
		// 노이즈로 인한 포인트로 간주하고 오른쪽 포인트를 새로운 Mid 포인트로 설정한 오차 검증을 반복한다.
		CvPoint2D32f *pFittingPoints = new CvPoint2D32f[nLengthPoints];
		if(pFittingPoints != 0x00) {
			int nLengthFittingPoints= 0;
			for(int i=0; i<nLengthPoints; i++) {
				nPos = dAngleMinPoints[i];
				if(nPos >= nMidPos && nPos < nLowPos) {
					pFittingPoints[nLengthFittingPoints].x = (FLOAT)nPos;
					pFittingPoints[nLengthFittingPoints].y = pHeightData[nPos];
					nLengthFittingPoints++;
				}
			}

			int nFittingPointCount = m_dParam.m_nFeaturePointMidPosFittingPointCount;
			FLOAT nFittingLineError = m_dParam.m_nFeaturePointMidPosFittingLineError;
			FLOAT nA, nB;
			FLOAT nAvgErr, nCurErr;
			int nStartIndex, nEndIndex;
			for(int i=0; i<nLengthFittingPoints-nFittingPointCount; i++) {
				nStartIndex = i+1;
				nEndIndex = i + nFittingPointCount;

				if(_GetFittingLine(&(pFittingPoints[nStartIndex]), nFittingPointCount, nA, nB)) {
					nAvgErr = 0;
					for(int j=nStartIndex; j<=nEndIndex; j++) {
						nAvgErr += abs(pFittingPoints[j].y - (nA*pFittingPoints[j].x+nB));
					}
					nAvgErr /= nFittingPointCount;
					nCurErr = abs(pFittingPoints[i].y - (nA*pFittingPoints[i].x+nB));

					if(nCurErr < nFittingLineError * nAvgErr) {
						nMidPos = (int)pFittingPoints[i].x;
						break;
					}
				}
			}

			delete [] pFittingPoints;
		}
	}

	if(bRightPart) {	// swap both sides
		REVERSE_POINT_POS(nHighPos, nLengthProfile);
		REVERSE_POINT_POS(nMidPos, nLengthProfile);
		REVERSE_POINT_POS(nLowPos, nLengthProfile);

		REVERSE_DATA(pHeightData, nLengthProfile);
		REVERSE_DATA(pAngleData, nLengthProfile);
		REVERSE_POINTS(dAngleMinPoints, nLengthProfile);
	}

	*pHighPos = nHighPos;
	*pMidPos = nMidPos;
	*pLowPos = nLowPos;
	*pHighPosHeight = pHeightData[nHighPos];
	if(m_bEstimateFeaturePoint) *pMidPosHeight = nEstMidPosHeight;
	else *pMidPosHeight = pHeightData[nMidPos];
	*pLowPosHeight = pHeightData[nLowPos];;

	return TRUE;
}

// MidPos 추정값을 구한다.
float CMeasureAlg:: _EstimateMidPosHeight(Point2Ds &dPoints, int nLowPos, float nLowPosHeight, int nMidPos, float nMidPosHeight)
{
	int nLength = dPoints.size();
	for(int i =0; i<nLength; i++) {
		Point2D &dPoint = dPoints[i];
		dPoint.x = nLowPos - dPoint.x;
		dPoint.y = dPoint.y - nLowPosHeight;
	}
	REVERSE_DATA(dPoints, nLength);

	nMidPos = nLowPos - nMidPos;
	nMidPosHeight = nMidPosHeight - nLowPosHeight;

	// first eliminate outlier data
	if(nLength >= 5) {
		int nIndexHalf = cvCeil(nLength/2.f)-1;
		Points dIndexValid;
		dIndexValid.push_back(nIndexHalf-1);
		dIndexValid.push_back(nIndexHalf);
		dIndexValid.push_back(nIndexHalf+1);
		Points dIndexCheckSequence;

		for(int i=1; i<=nIndexHalf; i++) {
			int nIndexRight = nIndexHalf + 1 + i;
			int nIndexLeft = nIndexHalf - 1 - i;
			if(nIndexRight <= nLength-1) {
				dIndexCheckSequence.push_back(nIndexRight);
			}
			if(nIndexLeft >= 0) {
				dIndexCheckSequence.push_back(nIndexLeft);
			}
		}

		// normalize
		float nMaxHeight = MIN_VALUE;
		float nMinHeight = MAX_VALUE;
		for(int i=0; i<nLength; i++) {
			Point2D &dPoint = dPoints[i];
			if(dPoint.y > nMaxHeight) nMaxHeight = dPoint.y;
			if(dPoint.y < nMinHeight) nMinHeight = dPoint.y;
		}

		Point2Ds dNormPoints;
		for(int i=0; i<nLength; i++) {
			Point2D dPoint;
			dPoint.x = dPoints[i].x;
			dPoint.y = (dPoints[i].y - nMinHeight) / (nMaxHeight - nMinHeight);

			dNormPoints.push_back(dPoint);
		}

		float nErrThreshold = m_dParam.m_nFeaturePointMidPosEstimateOutlierError;
		float 	nSigmaErrThreshold = m_dParam.m_nFeaturePointMidPosEstimateOutlierSigmaError;
		for(int i=0; i<(int)dIndexCheckSequence.size(); i++) {
			int nIndexCheck = dIndexCheckSequence[i];
			Point2Ds dFittingPoints;
			for(int j=0; j<(int)dIndexValid.size(); j++) {
				int nIndex = dIndexValid[j];
				dFittingPoints.push_back(dNormPoints[nIndex]);
			}

			float nA, nB;
			Point2D dPointCheck = dNormPoints[nIndexCheck];
			_LinearFit(dFittingPoints, nA, nB);
			float nEstHeight = _GetLinearFitValue(dPointCheck.x, nA, nB);
			float nErr = abs(nEstHeight - dPointCheck.y);

			if(nErr < nErrThreshold) {
				dIndexValid.push_back(nIndexCheck);
			}
		}

		std::sort(dIndexValid.begin(), dIndexValid.end());
		Point2Ds dValidPoints;
		for(int i=0; i<(int)dIndexValid.size(); i++) {
			int nIndex = dIndexValid[i];
			dValidPoints.push_back(dPoints[nIndex]);
		}

		dPoints = dValidPoints;
		nLength = dPoints.size();
	}
	else {
		// add mid pos to the fitting list
		Point2D dPoint;
		dPoint.x = (float)nMidPos;
		dPoint.y = nMidPosHeight;
		dPoints.push_back(dPoint);
	}

	float nEstMidPosHeight = nMidPosHeight;
	if(nLength > 3) {
		float nAGammaFit, nBGammaFit;
		float nMeanErrorGammaFit = _GammaFit(dPoints, nAGammaFit, nBGammaFit);
		float nALogFit, nBLogFit;
		float nMeanErrorLogFit = _LogFit(dPoints, nALogFit, nBLogFit);
		float nALinearFit, nBLinearFit;
		float nMeanErrorLinearFit = _LinearFit(dPoints, nALinearFit, nBLinearFit);

		if(nBGammaFit > 1) nMeanErrorGammaFit = MAX_VALUE;

		int nIndexMinMeanError = 1;
		if(nMeanErrorGammaFit <= nMeanErrorLogFit) {
			if(nMeanErrorGammaFit <= nMeanErrorLinearFit) nIndexMinMeanError = 1;
			else nIndexMinMeanError = 3;
		}
		else {
			if(nMeanErrorLogFit <= nMeanErrorLinearFit) nIndexMinMeanError = 2;
			else nIndexMinMeanError = 3;
		}

		switch(nIndexMinMeanError) {
		case 1 :
			nEstMidPosHeight = _GetGammaFitValue((float)nMidPos, nAGammaFit, nBGammaFit);
			break;
		case 2 :       
			nEstMidPosHeight = _GetLogFitValue((float)nMidPos, nALogFit, nBLogFit);
			break;
		case 3 :
			nEstMidPosHeight = _GetLinearFitValue((float)nMidPos, nALinearFit, nBLinearFit);
		}
	}

	nEstMidPosHeight += nLowPosHeight;

	return nEstMidPosHeight;
}

BOOL CMeasureAlg::_GetFittingLine(CvPoint2D32f *pPoints, int nLength, float &nA, float &nB)
{
	if(pPoints==0x00) {
		return FALSE;
	}

	CvMemStorage* pStorage = cvCreateMemStorage(0); 
	if(pStorage == 0x00) {
		return FALSE;
	}
	CvSeq* pSeq = cvCreateSeq(CV_32FC2, sizeof(CvSeq), sizeof(CvPoint2D32f), pStorage); 
	if(pSeq == 0x00) {
		cvReleaseMemStorage(&pStorage);
		return FALSE;
	}

	for(int i=0; i<nLength; i++) {
		cvSeqPush(pSeq, &(pPoints[i]));
	}

	FLOAT dLine[4];
	cvFitLine(pSeq, CV_DIST_L2, 0, 0.01, 0.01, dLine);

	nA = dLine[1] / dLine[0];
	nB = dLine[3] - nA * dLine[2];

	cvClearSeq(pSeq);
	cvReleaseMemStorage(&pStorage);

	return TRUE;
}

float CMeasureAlg::_LinearFit(const Point2Ds &dPoints, float &nA, float &nB)
{
	cv::Mat dMatX= cv::Mat::ones(2, dPoints.size(), CV_32FC1);
	cv::Mat dMatY(1, dPoints.size(), CV_32FC1);
	float *pData = 0x00;
	for(int i=0; i<(int)dPoints.size(); i++) {
		*((float *)dMatX.ptr(0, i)) = dPoints[i].x; 
		*((float *)dMatY.ptr(0, i)) = dPoints[i].y; 
	}

	cv::Mat dMatA = dMatY * dMatX.inv(cv::DECOMP_SVD);

	nA = *((float *)dMatA.ptr(0,0));
	nB = *((float *)dMatA.ptr(0,1));

	float nEstY =0.f;
	float nErrorSum = 0.;
	for(int i=0; i<(int)dPoints.size(); i++) {
		nEstY = _GetLinearFitValue(dPoints[i].x, nA, nB);
		nErrorSum += abs(dPoints[i].y - nEstY);
	}

	return nErrorSum/dPoints.size();
}

float CMeasureAlg::_GetLinearFitValue(float nX, float nA, float nB)
{
	return (nA * nX + nB);
}

float CMeasureAlg::_GammaFit(const Point2Ds &dPoints, float &nA, float &nB)
{
	Point2Ds dLogPoints; 
	for(int i=0; i<(int)dPoints.size(); i++) {
		Point2D dPoint;
		dPoint.x = logf(dPoints[i].x);
		dPoint.y = logf(dPoints[i].y);
		dLogPoints.push_back(dPoint);
	}

	float nALinearFit, nBLinearFit;
	_LinearFit(dLogPoints, nALinearFit, nBLinearFit);
	nA = expf(nBLinearFit);
	nB = nALinearFit;

	float nEstY =0.f;
	float nErrorSum = 0.;
	for(int i=0; i<(int)dPoints.size(); i++) {
		nEstY = _GetGammaFitValue(dPoints[i].x, nA, nB);
		nErrorSum += abs(dPoints[i].y - nEstY);
	}

	return nErrorSum/dPoints.size();
}

float CMeasureAlg::_GetGammaFitValue(float nX, float nA, float nB)
{
	return nA * powf(nX, nB);
}

float CMeasureAlg::_LogFit(const Point2Ds &dPoints, float &nA, float &nB)
{
	Point2Ds dLogPoints; 
	for(int i=0; i<(int)dPoints.size(); i++) {
		Point2D dPoint;
		dPoint.x = logf(dPoints[i].x);
		dPoint.y = dPoints[i].y;
		dLogPoints.push_back(dPoint);
	}

	_LinearFit(dLogPoints, nA, nB);

	float nEstY =0.f;
	float nErrorSum = 0.;
	for(int i=0; i<(int)dPoints.size(); i++) {
		nEstY = _GetLogFitValue(dPoints[i].x, nA, nB);
		nErrorSum += abs(dPoints[i].y - nEstY);
	}

	return nErrorSum/dPoints.size();
}

float CMeasureAlg::_GetLogFitValue(float nX, float nA, float nB)
{
	return nA * logf(nX) + nB;
}


// 측정 결과 데이터(MEASURE_RESULT_DATA)를 생성한다.
// Feature Points 를 이용하여 넓이, 높이, 각도를 구한다.
BOOL CMeasureAlg::_MakeResultData()
{
	MEASURE_RESULT_DATA *pMeasureResultData = _GetCurMeasureResultData();
	if(pMeasureResultData == 0x00) {
		_PrintDebugString("Fail to Get CurMeasureResultData");
		return FALSE;
	}

	MEASURE_DATA *pMeasureData = _GetCurMeasureData();
	if(pMeasureData == 0x00) {
		_PrintDebugString("Fail to Get CurMeasureData");
		return FALSE;
	}

	IplImage *pHeightProfile = pMeasureData->m_pInverseHeightProfile;
	FLOAT *pProfileData = (FLOAT *)pHeightProfile->imageData;
	int nPos_Left_High = pMeasureData->m_dFreaturePointLeft.m_nHighPos;
	int nPos_Left_Mid = pMeasureData->m_dFreaturePointLeft.m_nMidPos;
	int nPos_Left_Low = pMeasureData->m_dFreaturePointLeft.m_nLowPos;
	float nPos_Left_High_Height = pMeasureData->m_dFreaturePointLeft.m_nHighPosHeight;
	float nPos_Left_Mid_Height = pMeasureData->m_dFreaturePointLeft.m_nMidPosHeight;
	float nPos_Left_Low_Height = pMeasureData->m_dFreaturePointLeft.m_nLowPosHeight;
	int nPos_Right_High = pMeasureData->m_dFreaturePointRight.m_nHighPos;
	int nPos_Right_Mid = pMeasureData->m_dFreaturePointRight.m_nMidPos;
	int nPos_Right_Low = pMeasureData->m_dFreaturePointRight.m_nLowPos;
	float nPos_Right_High_Height = pMeasureData->m_dFreaturePointRight.m_nHighPosHeight;
	float nPos_Right_Mid_Height = pMeasureData->m_dFreaturePointRight.m_nMidPosHeight;
	float nPos_Right_Low_Height = pMeasureData->m_dFreaturePointRight.m_nLowPosHeight;

	FLOAT nmPerPixel = m_dProc.m_nmPerPixel;
	FLOAT nmPerHeight = m_dProc.m_nmPerHeight;
	if(m_nMeasureAngle == MEASURE_ANGLE_45) nmPerPixel *= (FLOAT)sqrt(2.0);
	FLOAT nmSpaceGap = m_dInput.m_nSpaceGap;

	// Pixel 당 크기 및 높이를 적용하여 실제 거리를 계산함.
	FLOAT nResultData_WIDTH = (abs(nPos_Right_Mid - nPos_Left_Mid) + 1) * nmPerPixel;
	FLOAT nResultData_WIDTH_C = (abs(nPos_Right_Low - nPos_Left_Low) + 1) * nmPerPixel;
	FLOAT nResultData_WIDTH_L = (abs(nPos_Left_Mid - nPos_Left_Low) + 1) * nmPerPixel;
	FLOAT nResultData_WIDTH_R = (abs(nPos_Right_Mid - nPos_Right_Low) +1) * nmPerPixel;
	FLOAT nResultData_HEIGHT_TOTAL_L = (abs(nPos_Left_High_Height - nPos_Left_Low_Height) + 1) * nmPerHeight - nmSpaceGap;
	FLOAT nResultData_HEIGHT_TOTAL_R = (abs(nPos_Right_High_Height - nPos_Right_Low_Height) + 1) * nmPerHeight - nmSpaceGap;
	FLOAT nResultData_HEIGHT_L = (abs(nPos_Left_Mid_Height - nPos_Left_Low_Height) + 1) * nmPerHeight;
	FLOAT nResultData_HEIGHT_R = (abs(nPos_Right_Mid_Height - nPos_Right_Low_Height) + 1) * nmPerHeight;
	FLOAT nResultData_HEIGHT_E_L = nResultData_HEIGHT_TOTAL_L - nResultData_HEIGHT_L;
	FLOAT nResultData_HEIGHT_E_R = nResultData_HEIGHT_TOTAL_R - nResultData_HEIGHT_R;

	CvPoint2D32f ptCenter;
	CvPoint2D32f ptLow;
	CvPoint2D32f ptMid;
	ptCenter.x = nPos_Left_Mid * nmPerPixel;
	ptCenter.y = nPos_Left_Mid_Height * nmPerHeight;
	ptLow.x = nPos_Left_Low * nmPerPixel;
	ptLow.y = nPos_Left_Low_Height * nmPerHeight;
	ptMid.x = ptLow.x;
	ptMid.y = ptCenter.y;
	FLOAT nResultData_ANGLE_L = _CalcAngle(ptMid, ptCenter, ptLow);

	ptCenter.x = nPos_Right_Mid * nmPerPixel;
	ptCenter.y = nPos_Right_Mid_Height * nmPerHeight;
	ptLow.x = nPos_Right_Low * nmPerPixel;
	ptLow.y = nPos_Right_Low_Height * nmPerHeight;
	ptMid.x = ptLow.x;
	ptMid.y = ptCenter.y;
	FLOAT nResultData_ANGLE_R = _CalcAngle(ptMid, ptCenter, ptLow);

	// MEASURE_DATA_RESULT 구조체에 저장.
	pMeasureResultData->RIB_WIDTH = (int)nResultData_WIDTH;
	pMeasureResultData->RIB_WIDTH_C = (int)nResultData_WIDTH_C;
	pMeasureResultData->RIB_WIDTH_L = (int)nResultData_WIDTH_L;
	pMeasureResultData->RIB_WIDTH_R = (int)nResultData_WIDTH_R;
	pMeasureResultData->RIB_HEIGHT_TOTAL_L = (int)nResultData_HEIGHT_TOTAL_L;
	pMeasureResultData->RIB_HEIGHT_TOTAL_R = (int)nResultData_HEIGHT_TOTAL_R;
	pMeasureResultData->RIB_HEIGHT_L = (int)nResultData_HEIGHT_L;
	pMeasureResultData->RIB_HEIGHT_E_L =(int)nResultData_HEIGHT_E_L ;
	pMeasureResultData->RIB_HEIGHT_R = (int)nResultData_HEIGHT_R;
	pMeasureResultData->RIB_HEIGHT_E_R = (int)nResultData_HEIGHT_E_R;
	pMeasureResultData->RIB_ANGLE_L = nResultData_ANGLE_L;
	pMeasureResultData->RIB_ANGLE_R = nResultData_ANGLE_R;
	pMeasureResultData->RIB_HEIGHT = (int)((nResultData_HEIGHT_TOTAL_L>nResultData_HEIGHT_TOTAL_R)?nResultData_HEIGHT_TOTAL_L:nResultData_HEIGHT_TOTAL_R);

	pMeasureResultData->Validate();

	return TRUE;
}

FLOAT CMeasureAlg::_CalcAvgData(int nStartPos, int nEndPos)
{
	FLOAT nDenom = (FLOAT)(nEndPos-nStartPos+1);
	if(nDenom == 0.f) {
		return 0.f;
	}

	FLOAT nSum=0;
	for(int i=nStartPos; i<=nEndPos; i++) nSum += i;
	return nSum / nDenom;
}

FLOAT CMeasureAlg::_CalcAvgData(FLOAT *pData, int idxStart, int idxEnd, int nSizeX)
{
	if(pData == 0x00) { 
		return 0;
	}

	FLOAT nDenom = (FLOAT)(idxEnd-idxStart+1);
	if(nDenom == 0.f) {
		return 0.f;
	}

	FLOAT nSum=0;
	for(int i=idxStart; i<=idxEnd; i++) nSum += pData[i*nSizeX];
	return nSum / nDenom;
}

FLOAT CMeasureAlg::_CalcAngle(CvPoint2D32f ptLeft, CvPoint2D32f ptCenter, CvPoint2D32f ptRight)
{
	CvPoint2D32f pt1 = cvPoint2D32f(ptLeft.x - ptCenter.x, ptLeft.y - ptCenter.y);
	CvPoint2D32f pt2 = cvPoint2D32f(ptRight.x - ptCenter.x, ptRight.y - ptCenter.y);;

	FLOAT nDenom = (sqrtf(powf(pt1.x,2) + powf(pt1.y,2))*sqrtf(powf(pt2.x,2) + powf(pt2.y,2)));
	if(nDenom == 0.f) {
		return 0.f;
	}

	FLOAT nCos= (pt1.x*pt2.x + pt1.y*pt2.y) / nDenom;
	if(nCos < -1.f) nCos = -1.f;
	else if(nCos > 1.f) nCos = 1.f;
	FLOAT nAngle= acosf(nCos) * (180.f / (FLOAT)CV_PI);

	return nAngle;
}

FLOAT CMeasureAlg::_GetMaxData(FLOAT *pData, int idxStart, int idxEnd)
{
	if(pData == 0x00) {
		return 0;
	}

	FLOAT nMax=MIN_VALUE;
	for(int i=idxStart; i<=idxEnd; i++) {
		if(	pData[i]>nMax) {
			nMax = pData[i];
		}
	}

	return nMax;
}

FLOAT CMeasureAlg::_GetMinData(FLOAT *pData, int idxStart, int idxEnd)
{
	if(pData == 0x00) {
		return 0;
	}

	FLOAT nMin=MAX_VALUE;
	for(int i=idxStart; i<=idxEnd; i++) {
		if(pData[i]<nMin) {
			nMin = pData[i];
		}
	}

	return nMin;
}

int CMeasureAlg::_GetMinDataIndex(FLOAT *pData, int idxStart, int idxEnd)
{
	if(pData == 0x00) {
		return -1;
	}

	FLOAT nMin=MAX_VALUE;
	int idxMin = 0;
	for(int i=idxStart; i<=idxEnd; i++) {
		if(pData[i]<nMin) {
			nMin = pData[i];
			idxMin = i;
		}
	}

	return idxMin;
}

int CMeasureAlg::_GetPointsIndex(const Points &dPoints, int nValue)
{
	for(int i=0; i<=(int)dPoints.size(); i++) {
		if(dPoints[i] == nValue) {
			return i;
		}
	}

	return -1;
}

// 새로운 측정 데이터(MEASURE_DATA) 객체를 생성한다.
BOOL CMeasureAlg::_AddMeasureData(BOOL bSecondPhase)
{
	MEASURE_DATA *pMeasureData = new MEASURE_DATA();
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	if(bSecondPhase) {
		pMeasureData->CopyStartPoint(m_dProc.m_pCurMeasureData);
	}

	m_dProc.m_dMeasure.push_back(pMeasureData);

	m_dProc.m_pCurMeasureData = pMeasureData;
	m_dProc.m_pCurMeasureData->m_bSecondPhase = bSecondPhase;

	return TRUE;
}

// 새로운 측정 데이터(MEASURE_DATA) 객체를 생성한다.
BOOL CMeasureAlg::_AddMeasureData(CvPoint ptStart, CvPoint ptEnd, BOOL bSecondPhase, BOOL bSecondPhaseOnly)
{
	MEASURE_DATA *pMeasureData = new MEASURE_DATA();
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	pMeasureData->m_ptStart = ptStart;
	pMeasureData->m_ptEnd= ptEnd;
	pMeasureData->m_bSecondPhase = bSecondPhase;
	pMeasureData->m_bSecondPhaseOnly = bSecondPhaseOnly;

	m_dProc.m_dMeasure.push_back(pMeasureData);

	m_dProc.m_pCurMeasureData = pMeasureData;

	return TRUE;
}

// 특정 index 의 측정 데이터(MEASURE_DATA)를 현재 검사 대상으로 설정한다.
BOOL CMeasureAlg::_SetCurMeasureData(UINT nIndex)
{
	if(nIndex >= m_dProc.m_dMeasure.size()) {
		return FALSE;
	}

	m_dProc.m_pCurMeasureData = m_dProc.m_dMeasure[nIndex];
	return TRUE;
}

// 현재 다루고 있는 측정 데이터(MEASURE_DATA)를 반환한다.
MEASURE_DATA *CMeasureAlg::_GetCurMeasureData()
{
	return m_dProc.m_pCurMeasureData;
}

// 특정 index 의 측정 데이터(MEASURE_DATA)를 반환한다.
MEASURE_DATA *CMeasureAlg::_GetMeasureData(UINT nIndex)
{
	if(nIndex >= m_dProc.m_dMeasure.size()) {
		return 0x00;
	}

	return m_dProc.m_dMeasure[nIndex];
}

// 현재 다루고 있은 결과 데이터(MEASURE_RESULT_DATA)를 반환한다.
MEASURE_RESULT_DATA *CMeasureAlg::_GetCurMeasureResultData()
{
	MEASURE_DATA *pMeasureData = _GetCurMeasureData();
	if(pMeasureData == 0x00) {
		return 0x00;
	}

	return &(pMeasureData->m_dResult);
}

// 특정 index 의 측정 결과 데이터(MEASURE_RESULT_DATA)를 반환한다.
MEASURE_RESULT_DATA *CMeasureAlg::_GetMeasureResultData(UINT nIndex)
{
	MEASURE_DATA *pMeasureData = _GetMeasureData(nIndex);
	if(pMeasureData == 0x00) {
		return 0x00;
	}

	return &(pMeasureData->m_dResult);
}

// 결과 이미지를 저장할 경로를 설정한다.
void CMeasureAlg::SetSaveResultPath(char *szPath)
{
	if(szPath == 0x00) {
		return;
	}

	strcpy(m_dOutput.m_szDataPath, szPath);
	int nLen = strlen(m_dOutput.m_szDataPath);
	if(m_dOutput.m_szDataPath[nLen-1] == '\\') {		// remove '\\' at end of path
		m_dOutput.m_szDataPath[nLen-1] = 0x00;
	}
}

// 결과 이미지를 생성한다.
BOOL CMeasureAlg::MakeResultImage()
{
	// 전체 이미지 버퍼를 생성.
	if(_MakeResultFullImage()==FALSE) {
		return FALSE;
	}

	// 높이 이미지 부분을 생성하여 전체 이미지에 통합
	if(_MakeResultHeightImage() == FALSE) {
		return FALSE;
	}

	// 결과 데이터 이미지 부분을 생성하여 전체 이미지에 통합
	if(m_bSaveAnalysisData) {
		if(_MakeAnalysisResultDataImage() == FALSE) {
			return FALSE;
		}
	}
	else if(_MakeResultDataImage() == FALSE) {
		return FALSE;
	}

	return TRUE;
}

// 높이 이미지를 생성하여 전체 이미지에 통합한다.
// Base 함수이며, 공통 로직 부분이 구현되어 있음. Mask Type 별 상세 로직은 개별 클래스에 구현되어 있음.
BOOL CMeasureAlg::_MakeResultHeightImage()
{
	m_dOutput.m_pHeightImage = cvCreateImage(cvGetSize(m_dProc.m_pHeightImage), IPL_DEPTH_8U, 3);

//	_PrintDebugImage(m_dProc.m_pHeightImage,0,0,100,1,"HEIGHT_IMAGE");

	double nMaxHeight = 0;
	double nMinHeight = 0;
	cvMinMaxLoc(m_dProc.m_pHeightImage, &nMinHeight, &nMaxHeight);

	int nSizeX = m_dOutput.m_pHeightImage->width;
	int nSizeY = m_dOutput.m_pHeightImage->height;
	INT32 *pSrcData = (INT32 *)m_dProc.m_pHeightImage->imageData;
	UINT8 *pDstData = (UINT8 *)m_dOutput.m_pHeightImage->imageData;
	int nOffset = 0;
	UINT32 nSrcValue;
	double nDstValue;
	CvScalar dColor;
	for(int y=0; y<nSizeY; y++) {
		for(int x=0; x<nSizeX; x++) {
			nSrcValue = pSrcData[y*nSizeX+x];
			nDstValue = (((double)nSrcValue - nMinHeight)/(nMaxHeight-nMinHeight));
			dColor = _GetColormapValue(nDstValue);

			nOffset = (y*nSizeX+x)*3;
			pDstData[nOffset]=(UINT8)(dColor.val[0]*255);
			pDstData[nOffset+1]=(UINT8)(dColor.val[1]*255);
			pDstData[nOffset+2]=(UINT8)(dColor.val[2]*255);
		}
	}

	return TRUE;
}

// Intensity 를 Jet Colormap 기반의 RGB color 로 반환한다.
CvScalar CMeasureAlg::_GetColormapValue(double nValue)
{
	// nValue should be between 0 and 1
	if(nValue <0) nValue = 0;
	else if(nValue >1) nValue = 1;

	double nX = nValue * 2.0f - 1.0f;
	double nY_R = 0;
	double nY_G = 0;
	double nY_B = 0;

	if(nX <= -0.75) {
		nY_R = 0;
		nY_G = 0;
		nY_B = 2*nX + 2.5;
	}
	else if(nX <= -0.25) {
		nY_R = 0;
		nY_G = 2*nX + 1.5;
		nY_B = 1;
	}
	else if(nX <= 0.25) {
		nY_R = 2*nX + 0.5;
		nY_G = 1;
		nY_B = -2*nX + 0.5;
	}
	else if(nX <= 0.75f) {
		nY_R = 1;
		nY_G = -2*nX + 1.5;
		nY_B = 0;
	}
	else if(nX <= 1) {
		nY_R = -2*nX + 2.5;
		nY_G = 0;
		nY_B = 0;
	}

	// return R,G,B values between 0 and 1
	return CV_RGB(nY_R, nY_G, nY_B);
}

// 결과 데이터 이미지 부분을 생성하여 전체 이미지에 통합
// Base 함수이며, 상세 내용은 Mask Type 별 클래스에 구현되어 있음.
BOOL CMeasureAlg::_MakeResultDataImage()
{
	// Do Noting here. Logic should be implemented in a derived class.
	return TRUE;
}

// 분석용 결과 데이터 이미지 부분을 생성하여 전체 이미지에 통합
// Base 함수이며, 상세 내용은 Mask Type 별 클래스에 구현되어 있음.
BOOL CMeasureAlg::_MakeAnalysisResultDataImage()
{
	// Do Noting here. Logic should be implemented in a derived class.
	return TRUE;
}

// 전체 결과 이미지 생성. Base 함수이며, 상세 내용은 Mask Type 별 클래스에 구현되어 있음.
BOOL CMeasureAlg::_MakeResultFullImage()
{
	// Do Noting here. Logic should be implemented in a derived class.
	return TRUE;
}

// 결과 이미지를 지정된 경로에 저장한다.
void CMeasureAlg::SaveResultImage()
{
	if(m_dOutput.m_pFullImage == 0x00) {
		return;
	}

	char szFilePath[MAX_PATH*2]={0};
	if(m_bSaveAnalysisData) {
		   sprintf_s(szFilePath, "%s\\%s.jpg", m_szAnalysisDataPath, m_dOutput.m_szDataFileName);
	}
	else {
		  sprintf_s(szFilePath, "%s\\%s.jpg", m_dOutput.m_szDataPath, m_dOutput.m_szDataFileName);
	}

	_SetStartTime();
	cvSaveImage(szFilePath, m_dOutput.m_pFullImage);
	m_dTime.m_msSaveResultImageFile = _GetElapsedTime();

	_PrintDebugString("# Save Result Image File : %s", szFilePath);
	_PrintDebugString("> Time to Save Result Image File : %d ms", m_dTime.m_msSaveResultImageFile);
}

// 결과 이미지를 화면에 출력한다.
void CMeasureAlg::ShowResultImage()
{
	m_pImageView = CvImageView::GetInstance();
	if(m_pImageView) {
		m_pImageView->Show(m_dOutput.m_pFullImage);
	}
//	cvNamedWindow("MEASURE_RESULT", CV_WINDOW_AUTOSIZE);
//	cvShowImage("MEASURE_RESULT", m_dOutput.m_pFullImage);
}

// 측정 결과 데이터(RESULT_DATA)의 개수를 반환한다.
// RESULT_DATA 는 가로(Horiz)/세로(Vert) 두개의 결과값을 저장할 수 있도록 되어 있어,
// 개별 측정 결과값을 저장하는 MEASURE_DATA 의 개수와는 다를 수 있다.
// Master 에 전달하는 데이터는 RESULT_DATA 의 개수이며, 이에 대한 값을 계산하여 반환한다. 
int CMeasureAlg::_GetRealMeasureResultDataCount()
{
	if(m_nMeasureIndex > 0) {
		// Measure Index 가 지정되어 있는 경우에는 해당 1개만 처리한다.
		return 1;
	}

	int nCount = 0;
	MEASURE_DATA *pMeasureData;
	for(UINT i=0; i<m_dProc.m_dMeasure.size(); i++) {
		pMeasureData = m_dProc.m_dMeasure[i];
		if(pMeasureData) {
			// 새로운 결과 데이터일 경우에만 Count 를 증가
			if(_IsNewRealMeasureResultData(pMeasureData)) {
				nCount++;
			}
		}
	}

	return nCount;
}

BOOL CMeasureAlg::_IsNewRealMeasureResultData(MEASURE_DATA *pMeasureData)
{
	if(pMeasureData == 0x00) {
		return FALSE;
	}

	if(m_dProc.m_dMeasure.size() == 0) {
		return FALSE;
	}

	// 새로운 결과 데이터를 판명하는 두가지 조건.
	// 1) 첫번째 Phase 일 경우
	// 2) 첫번째 Phase 측정 결과가 없이 두번째 측정 결과만 있을 경우
	if(pMeasureData->m_bSecondPhase == FALSE || pMeasureData->m_bSecondPhaseOnly) {
		return TRUE;
	}

	return FALSE;
}

// 분석용 결과 이미지에 InverseHeightProfile 와 FeaturePoints 를 그린다.
void CMeasureAlg::_DrawHeightProfile(IplImage *pView, CvRect rcView, MEASURE_DATA *pMeasureData)
{
	if(pView == 0x00) {
		return;
	}
	if(pMeasureData == 0x00) {
		return;
	}
	if(pMeasureData->m_pInverseHeightProfile == 0x00) {
		return;
	}

	IplImage *pProfile = pMeasureData->m_pInverseHeightProfile;
	CvRect rcROIBefore = cvGetImageROI(pView);
	cvSetImageROI(pView, rcView);

	int nLengthProfile = pProfile->width;
	FLOAT *pProfileData = (FLOAT *)pProfile->imageData;
	FLOAT nMaxHeight = _GetMaxData(pProfileData, 0, nLengthProfile-1);
	FLOAT nMinHeight = _GetMinData(pProfileData, 0, nLengthProfile-1);
	FLOAT nRatio = 0.8f;
	FLOAT nRatioY = (((FLOAT)rcView.height / (nMaxHeight - nMinHeight))) * nRatio;
	FLOAT nRatioX = (FLOAT)rcView.width / nLengthProfile;
	int nBaseY = (int)(nMinHeight * nRatioY - (rcView.height * ((1.f - nRatio) / 2.f)));

	CvPoint ptStart = cvPoint(0, rcView.height - (int)(pProfileData[0]*nRatioY) + nBaseY);
	CvPoint ptEnd;
	for(int i=1; i<nLengthProfile; i++) {
		ptEnd = cvPoint((int)(i*nRatioX), rcView.height - (int)(pProfileData[i]*nRatioY) + nBaseY);
		cvDrawLine(pView, ptStart, ptEnd, CV_RGB(0, 0, 255), 2);

		ptStart = ptEnd;
	}

	CvPoint ptFeaturePoints[6];
	int nPos = pMeasureData->m_dFreaturePointLeft.m_nHighPos;
	float nPosHeight = pMeasureData->m_dFreaturePointLeft.m_nHighPosHeight;
	int nIndex = 0;
	ptFeaturePoints[nIndex++] = cvPoint((int)(nPos*nRatioX), rcView.height - (int)(nPosHeight*nRatioY) + nBaseY);
	nPos = pMeasureData->m_dFreaturePointLeft.m_nLowPos;
	nPosHeight = pMeasureData->m_dFreaturePointLeft.m_nLowPosHeight;
	ptFeaturePoints[nIndex++] = cvPoint((int)(nPos*nRatioX), rcView.height - (int)(nPosHeight*nRatioY) + nBaseY);
	nPos = pMeasureData->m_dFreaturePointRight.m_nHighPos;
	nPosHeight = pMeasureData->m_dFreaturePointRight.m_nHighPosHeight;
	ptFeaturePoints[nIndex++] = cvPoint((int)(nPos*nRatioX), rcView.height - (int)(nPosHeight*nRatioY) + nBaseY);
	nPos = pMeasureData->m_dFreaturePointRight.m_nLowPos;
	nPosHeight = pMeasureData->m_dFreaturePointRight.m_nLowPosHeight;
	ptFeaturePoints[nIndex++] = cvPoint((int)(nPos*nRatioX), rcView.height - (int)(nPosHeight*nRatioY) + nBaseY);
	nPos = pMeasureData->m_dFreaturePointLeft.m_nMidPos;
	nPosHeight = pMeasureData->m_dFreaturePointLeft.m_nMidPosHeight;
	ptFeaturePoints[nIndex++] = cvPoint((int)(nPos*nRatioX), rcView.height - (int)(nPosHeight*nRatioY) + nBaseY);
	nPos = pMeasureData->m_dFreaturePointRight.m_nMidPos;
	nPosHeight = pMeasureData->m_dFreaturePointRight.m_nMidPosHeight;
	ptFeaturePoints[nIndex++] = cvPoint((int)(nPos*nRatioX), rcView.height - (int)(nPosHeight*nRatioY) + nBaseY);

	for(int i=0; i<6; i++) {
		if(i<4) {
			cvDrawCircle(pView, ptFeaturePoints[i], 2, CV_RGB(255,0,0), 8);
		}
		else {
			cvDrawCircle(pView, ptFeaturePoints[i], 2, CV_RGB(192,0,192), 6);
		}
	}

	cvSetImageROI(pView, rcROIBefore);
}

// 이하 디버깅용 함수들
// 문자열, 이미지, Points, Result Data 등 디버깅용 데이터를 출력하는 함수임.
void CMeasureAlg::_PrintDebugImage(IplImage *pImage, int nStartX, int nStartY, int nSizeX, int nSizeY, const char* szDebugString, ...)
{
	if(m_bSaveAnalysisData == FALSE) {
		return;
	}

	if(pImage == 0x00) {
		return;
	}

	switch(pImage->depth) {
	case IPL_DEPTH_8U : 
	case IPL_DEPTH_16U : 
	case IPL_DEPTH_32F : 
	case IPL_DEPTH_8S : 
	case IPL_DEPTH_16S :
	case IPL_DEPTH_32S :
		break;
	default :
		return;
	}

	char	szBufferString[2048] = {0, };
	va_list vargs;

	va_start(vargs, szDebugString);
	vsprintf_s(szBufferString, szDebugString, (va_list)vargs);
	va_end(vargs);

	char szBuffer[2048] = "[M3D] Print Image Data : ";
	strcat(szBuffer, szBufferString);
	OutputDebugStringA(szBuffer);

	sprintf(szBuffer,"[M3D] nStartX : %d, nStart Y : %d, nSizeX : %d, nSizeY : %d", nStartX, nStartY, nSizeX, nSizeY);
	OutputDebugStringA(szBuffer);

	INT8 *pSrcData = (INT8 *)pImage->imageData;
	INT8 *pValue;
	char szValue[128] = {0, };

	int nUnitSize = ((pImage->depth)&0x00FFFFFF) / 8;
	int nWidthImage = pImage->width;

	for(int y=nStartY; y<nStartY+nSizeY; y++) {
		sprintf(szBuffer,"[M3D] (%d, %d) : ", nStartX, y);

		for(int x=nStartX; x<nStartX+nSizeX; x++) {
			pValue = pSrcData+(y*nWidthImage+x)*nUnitSize;
			switch(pImage->depth) {
			case IPL_DEPTH_8U : sprintf(szValue,"%u",(UINT8)*pValue); break;
			case IPL_DEPTH_16U : sprintf(szValue,"%u",(UINT16)*((UINT16 *)pValue)); break;
			case IPL_DEPTH_32F : sprintf(szValue,"%.3f",(FLOAT)*((FLOAT *)pValue)); break;
			case IPL_DEPTH_8S : sprintf(szValue,"%d",(INT8)*pValue); break;
			case IPL_DEPTH_16S : sprintf(szValue,"%d",(INT16)*((INT16 *)pValue)); break;
			case IPL_DEPTH_32S : sprintf(szValue,"%d",(INT32)*((INT32 *)pValue)); break;
			}

			if(strlen(szBuffer) + strlen(szValue) >= 2047) {
				break;
			}

			strcat(szBuffer,szValue);
			if(x != nStartX+nSizeX-1) {
				strcat(szBuffer,",");
			}
		}
		OutputDebugStringA(szBuffer);
	}

	OutputDebugStringA("[M3D] Print Image Data : END");
}

void CMeasureAlg::_PrintDebugVector(IplImage *pImage, int nStartX, int nSizeX, const char* szDebugString, ...)
{
	_PrintDebugImage(pImage, nStartX, 0, nSizeX, 1, szDebugString);
}

void CMeasureAlg::_PrintDebugPoints(int *pPoints, int nCount, const char* szDebugString, ...)
{
	if(m_bSaveAnalysisData == FALSE) {
		return;
	}

	if(pPoints == 0x00) {
		return;
	}

	char	szBufferString[2048] = {0, };
	va_list vargs;

	va_start(vargs, szDebugString);
	vsprintf_s(szBufferString, szDebugString, (va_list)vargs);
	va_end(vargs);

	char szBuffer[2048] = "[M3D] Print Points : ";
	strcat(szBuffer, szBufferString);
	OutputDebugStringA(szBuffer);

	sprintf(szBuffer,"[M3D] (%d) ", nCount);

	char szValue[128] = {0, };
	for(int i=0;i<nCount;i++) {
		sprintf(szValue,"%d",pPoints[i]);

		if(strlen(szBuffer) + strlen(szValue) >= 2047) {
			break;
		}

		strcat(szBuffer,szValue);
		if(i != nCount-1) {
			strcat(szBuffer,",");
		}
	}
	OutputDebugStringA(szBuffer);
}

void CMeasureAlg::_PrintDebugPoints(Points dPoints, const char* szDebugString, ...)
{
	if(m_bSaveAnalysisData == FALSE) {
		return;
	}

	char	szBufferString[2048] = {0, };
	va_list vargs;

	va_start(vargs, szDebugString);
	vsprintf_s(szBufferString, szDebugString, (va_list)vargs);
	va_end(vargs);

	char szBuffer[2048] = "[M3D] Print Points : ";
	strcat(szBuffer, szBufferString);
	OutputDebugStringA(szBuffer);

	int nCount = dPoints.size();

	sprintf(szBuffer,"[M3D] (%d) ", nCount);

	char szValue[128] = {0, };
	for(int i=0;i<nCount;i++) {
		sprintf(szValue,"%d",dPoints[i]);

		if(strlen(szBuffer) + strlen(szValue) >= 2047) {
			break;
		}

		strcat(szBuffer,szValue);
		if(i != nCount-1) {
			strcat(szBuffer,",");
		}
	}
	OutputDebugStringA(szBuffer);
	//	OutputDebugStringA("[M3D] Print Points : END");
}

void CMeasureAlg::_PrintDebugResultData(MEASURE_RESULT_DATA *pResultData, const char* szDebugString, ...)
{
	if(m_bSaveAnalysisData == FALSE) {
		return;
	}

	char	szBufferString[2048] = {0, };
	va_list vargs;

	va_start(vargs, szDebugString);
	vsprintf_s(szBufferString, szDebugString, (va_list)vargs);
	va_end(vargs);

	char szBuffer[2048] = "[M3D] ";
	strcat(szBuffer, szBufferString);
	OutputDebugStringA(szBuffer);

	_PrintDebugString("  WIDTH : %d, WIDTH_C : %d, WIDTH_L : %d, WIDTH_R : %d", 
		pResultData->RIB_WIDTH, pResultData->RIB_WIDTH_C, pResultData->RIB_WIDTH_L, pResultData->RIB_WIDTH_R);
	_PrintDebugString("  HEIGHT_TOTAL_L : %d, HEIGHT_L : %d, HEIGHT_E_L : %d", 
		pResultData->RIB_HEIGHT_TOTAL_L, pResultData->RIB_HEIGHT_L,pResultData->RIB_HEIGHT_E_L);
	_PrintDebugString("  HEIGHT_TOTAL_R : %d, HEIGHT_R : %d, HEIGHT_E_R : %d", 
		pResultData->RIB_HEIGHT_TOTAL_R, pResultData->RIB_HEIGHT_R,pResultData->RIB_HEIGHT_E_R);
	_PrintDebugString("  ANGLE_L : %.3f, ANGLE_R: %.3f", pResultData->RIB_ANGLE_L, pResultData->RIB_ANGLE_R);
}

void CMeasureAlg::_PrintDebugString(const char* szDebugString, ...)
{
	if(m_bSaveAnalysisData == FALSE) {
		return;
	}

	char	szBufferString[2048] = {0, };
	va_list vargs;

	va_start(vargs, szDebugString);
	vsprintf_s(szBufferString, szDebugString, (va_list)vargs);
	va_end(vargs);

	char	szBuffer[2048] = "[M3D] ";
	strcat(szBuffer, szBufferString);
	strcat(szBuffer, "\n");

	OutputDebugStringA(szBuffer);
}

void CMeasureAlg::_ShowDebugImage(IplImage *pImage, char *szWindowName)
{
	cvNamedWindow(szWindowName);
	cvShowImage(szWindowName, pImage);	
}

void CMeasureAlg::_ShowDebugLine(IplImage *pImage, char *szWindowName)
{
	int nSizeX = pImage->width;
	int nSizeY = 200;
	IplImage *pView = cvCreateImage(cvSize(nSizeX, nSizeY), IPL_DEPTH_8U, 3);
	if(pView == 0x00) {
		return;
	}
	memset(pView->imageData,0x00,sizeof(UINT8)*nSizeX*nSizeY*3);

	double nMaxHeight = 0;
	double nMinHeight = 0;
	cvMinMaxLoc(pImage, &nMinHeight, &nMaxHeight);

	FLOAT nRatio = nSizeY / (FLOAT)(nMaxHeight - nMinHeight);

	CvPoint ptStart = cvPoint(0, (int)((CV_IMAGE_ELEM(pImage,FLOAT,0,0) - nMinHeight) * nRatio));
	CvPoint ptEnd;
	for(int i=1; i<nSizeX; i++) {
		ptEnd = cvPoint(i, (int)((CV_IMAGE_ELEM(pImage,FLOAT,0,i) - nMinHeight) * nRatio));
		cvDrawLine(pView, ptStart, ptEnd, CV_RGB(0, 0, 255), 2);

		ptStart = ptEnd;
	}

	cvNamedWindow(szWindowName);
	cvShowImage(szWindowName, pView);

	cvReleaseImage(&pView);
}

void CMeasureAlg::_ShowDebugBinaryImage(IplImage *pImage, char *szWindowName)
{
	IplImage *pBinaryImage = cvCloneImage(pImage);
	if(pBinaryImage == 0x00) {
		return;
	}

	cvThreshold(pImage, pBinaryImage, 0.5, 255, CV_THRESH_BINARY);

	cvNamedWindow(szWindowName);
	cvShowImage(szWindowName, pBinaryImage);	
	cvReleaseImage(&pBinaryImage);
}
