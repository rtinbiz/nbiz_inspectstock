#include "StdAfx.h"
#include "ObservationAPPRunner.h"

//#include "winable.h"
#include "Winuser.h"
CvFont		G_ImageInfoFont;
void PutImage_Text(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor, CvScalar BackColor, CvScalar RectColor)
{
	extern CvFont		G_ImageInfoFont;
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_ImageInfoFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= NamePos.x-5;
	StartTxt.y	= NamePos.y+baseline;
	EndTxt.x	= NamePos.x+text_size.width+5;
	EndTxt.y	= NamePos.y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, BackColor);

	cvPutText(pMainMap, text, NamePos, &G_ImageInfoFont, FontColor);

	cvRectangle(pMainMap, StartTxt, EndTxt, RectColor);
}
CvFont		G_DataInfoFont;
void PutImage_TextData(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor, CvScalar BackColor, CvScalar RectColor)
{
	//extern CvFont		G_ImageInfoFont;
	//CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	//int    baseline;
	//cvGetTextSize( text, &G_ImageInfoFont, &text_size, &baseline ); 
	//CvPoint StartTxt, EndTxt;
	//StartTxt.x	= NamePos.x-5;
	//StartTxt.y	= NamePos.y+baseline;
	//EndTxt.x	= NamePos.x+text_size.width+5;
	//EndTxt.y	= NamePos.y-(text_size.height+baseline);
	//CvRect TxtRect;
	//TxtRect.x = StartTxt.x;
	//TxtRect.y = StartTxt.y;
	//TxtRect.width = EndTxt.x - StartTxt.x;
	//TxtRect.height = EndTxt.y - StartTxt.y;

	//CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
	//	cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
	//	cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
	//	cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	//CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	//int PolyVertexNumber[2] = { 4 , 0 } ;
	//int PolyNumber = 1 ;
	//cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, BackColor);

	cvPutText(pMainMap, text, NamePos, &G_DataInfoFont, FontColor);

	//cvRectangle(pMainMap, StartTxt, EndTxt, RectColor);
}

CvFont		G_ImageInfoSFont;
void PutImage_Text_Small(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor, CvScalar BackColor, CvScalar RectColor)
{
	extern CvFont		G_ImageInfoSFont;
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_ImageInfoSFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= NamePos.x-5;
	StartTxt.y	= NamePos.y+baseline;
	EndTxt.x	= NamePos.x+text_size.width+5;
	EndTxt.y	= NamePos.y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, BackColor);

	cvPutText(pMainMap, text, NamePos, &G_ImageInfoSFont, FontColor);

	cvRectangle(pMainMap, StartTxt, EndTxt, RectColor);
}
CvFont		G_ImageInfoVSFont;
void PutImage_Text_VSmall(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor)//, CvScalar BackColor, CvScalar RectColor)
{
	//extern CvFont		G_ImageInfoSFont;
	//CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	//int    baseline;
	//cvGetTextSize( text, &G_ImageInfoVSFont, &text_size, &baseline ); 
	//CvPoint StartTxt, EndTxt;
	//StartTxt.x	= NamePos.x-5;
	//StartTxt.y	= NamePos.y+baseline;
	//EndTxt.x	= NamePos.x+text_size.width+5;
	//EndTxt.y	= NamePos.y-(text_size.height+baseline);
	//CvRect TxtRect;
	//TxtRect.x = StartTxt.x;
	//TxtRect.y = StartTxt.y;
	//TxtRect.width = EndTxt.x - StartTxt.x;
	//TxtRect.height = EndTxt.y - StartTxt.y;

	//CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
	//	cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
	//	cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
	//	cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	//CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	//int PolyVertexNumber[2] = { 4 , 0 } ;
	//int PolyNumber = 1 ;
	//cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, BackColor);

	cvPutText(pMainMap, text, NamePos, &G_ImageInfoVSFont, FontColor);

	//cvRectangle(pMainMap, StartTxt, EndTxt, RectColor);
}
CvFont		G_ImageInfoBFont;
void PutImage_TextBig(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor, CvScalar BackColor, CvScalar RectColor)
{
//	extern CvFont		G_ImageInfoFont;
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_ImageInfoBFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= NamePos.x-5;
	StartTxt.y	= NamePos.y+baseline;
	EndTxt.x	= NamePos.x+text_size.width+5;
	EndTxt.y	= NamePos.y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, BackColor);

	cvPutText(pMainMap, text, NamePos, &G_ImageInfoBFont, FontColor);

	cvRectangle(pMainMap, StartTxt, EndTxt, RectColor);
}
void GetImgHistogram(IplImage* simg, IplImage* hist_img)
{
	cvZero(hist_img);
	if(simg == NULL)
	{
		return;
	}
	CvHistogram* hist;
	//float max_value = 0;
	float hist_val = 0;
	int intensity = 0;

	int axis_point = 150;
	int hist_size[] = {255};

	float ranges[] = { 0, 255 }; 
	float *hist_range[] = {ranges};
	hist =cvCreateHist( 1, hist_size, CV_HIST_UNIFORM, 0, 1);
	//hist = cvCreateHist(1, hist_size, CV_HIST_ARRAY, hist_range, 1); 

	cvCalcHist( &simg, hist, 0, 0);

	float max_bin_value = 0;
	float min_bin_value = 0;
	int max_level = 0;
	int min_level = 0;
	cvGetMinMaxHistValue( hist, &min_bin_value, &max_bin_value, &min_level, &max_level);

	int MaxIntensity = -1;
	int MinValue = -1;
	int MaxValue = -1;

	for ( int i = 0; i < 255; i++)
	{
		hist_val  = cvQueryHistValue_1D(hist,i);
		intensity = cvRound( hist_val*255/max_bin_value);
		cvLine(hist_img, cvPoint(i+20, axis_point), cvPoint(i+20, abs(axis_point - intensity) ), CV_RGB(255,255,0), 1);
		if(intensity>0 && 3.0f < (hist_val/max_bin_value)*100)
		{
			MaxValue = i;
			MaxIntensity = intensity;
		}
		if(intensity>0 && MinValue == -1)
			MinValue = i;
	}
	//cvLine(hist_img, cvPoint(20, (axis_point - MaxIntensity)), cvPoint(275, (axis_point - MaxIntensity)), CV_RGB(255,0,255), 1);
	//cvLine(hist_img, cvPoint(20+MaxValue, 30), cvPoint(20+MaxValue, axis_point), CV_RGB(255,0,255), 1);

	cvLine(hist_img, cvPoint(20, axis_point+3), cvPoint(275, axis_point+3), CV_RGB(0,255,0), 2);

	cvLine(hist_img, cvPoint(20, 0), cvPoint(20, axis_point), CV_RGB(255,0,0), 1);
	cvLine(hist_img, cvPoint(275, 0), cvPoint(275, axis_point), CV_RGB(255,0,0), 1);

	cvReleaseHist(&hist);
}

void G_vtAddLog(CInterServerInterface *pServerObj,const char* pErrLog, ...)
{
	char	cLogBuffer[MAX_LOG_BUFF];
	va_list vargs;

	va_start(vargs, pErrLog);

	vsprintf_s(cLogBuffer, pErrLog, (va_list)vargs);

	va_end(vargs);
	extern PER_PROCESS_INIT	* G_pProcInitVal;
	if(G_pProcInitVal != NULL && pServerObj != NULL)
		pServerObj->m_fnPrintLog(G_pProcInitVal->m_nTaskNum ,LOG_LEVEL_1, cLogBuffer);

}

void CenterWindowTopMost(HWND TargetWind, BOOL MoveCenter)
{
	CRect WinRect;
	if(MoveCenter)
	{
		::GetWindowRect(TargetWind, &WinRect);
		::MoveWindow(TargetWind, GetSystemMetrics(SM_CXSCREEN)/2-WinRect.Width()/2, GetSystemMetrics(SM_CYSCREEN)/2-WinRect.Height()/2 , 0, 0, TRUE);
	}
	
	::ShowWindow(TargetWind, SW_SHOWNORMAL); 
}
void ObservationAppChangeRemote()
{
	HWND RemoteModeH = ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Under remote control");
	if( RemoteModeH == NULL)
	{
		HWND ObserHWND = ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
		HWND MenuHHWnd = ::FindWindowEx(ObserHWND, 0, 0,"VK-X Image Pane");
		//CenterWindowTopMost(ObserHWND);
		ShowWindow(ObserHWND, SW_RESTORE);
		BlockInput(TRUE);
		::SetWindowPos(ObserHWND, HWND_TOPMOST,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);
		::SetFocus(ObserHWND);
		::SetActiveWindow(ObserHWND);
		SetForegroundWindow(ObserHWND);

		if(IsWindowEnabled(ObserHWND) &&  ObserHWND == GetForegroundWindow())
		{
			keybd_event(VK_ESCAPE,0xb8,0 , 0); //Right Press
			keybd_event(VK_ESCAPE,0xb8,KEYEVENTF_KEYUP,0); // Right Release
			Sleep(200);
			keybd_event(VK_MENU,0xb8,0 , 0); //Alt Press
			keybd_event(VK_MENU,0xb8,KEYEVENTF_KEYUP,0); // Alt Release
			Sleep(200);
			keybd_event(VK_ESCAPE,0xb8,0 , 0); //Right Press
			keybd_event(VK_ESCAPE,0xb8,KEYEVENTF_KEYUP,0); // Right Release

			Sleep(50);
			//Menu Open : "File"
			keybd_event(VK_MENU,0xb8,0 , 0); //Alt Press
			keybd_event(VK_MENU,0xb8,KEYEVENTF_KEYUP,0); // Alt Release

			Sleep(50);
			//Menu Select :"View"
			keybd_event(VK_RIGHT,0xb8,0 , 0); //Right Press
			keybd_event(VK_RIGHT,0xb8,KEYEVENTF_KEYUP,0); // Right Release

			Sleep(50);
			//Menu Select :"Tool"
			keybd_event(VK_RIGHT,0xb8,0 , 0); //Right Press
			keybd_event(VK_RIGHT,0xb8,KEYEVENTF_KEYUP,0); // Right Release

			//Sleep(100);
			//keybd_event(VK_UP,0xb8,0 , 0); //Right Press
			//keybd_event(VK_UP,0xb8,KEYEVENTF_KEYUP,0); // Right Release
			Sleep(50);
			for(int MoveItem = 0; MoveItem<18; MoveItem++)
			{
				//Menu Select :"Tool"
				keybd_event(VK_DOWN,0xb8,0 , 0); //Right Press
				keybd_event(VK_DOWN,0xb8,KEYEVENTF_KEYUP,0); // Right Release
				Sleep(50);
			}
			Sleep(50);
			keybd_event(VK_RETURN,0xb8,0, 0); //Right Press
			keybd_event(VK_RETURN,0xb8,KEYEVENTF_KEYUP,0); // Right Release
		}
		
		::SetWindowPos(ObserHWND, HWND_NOTOPMOST,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);

		{
			VK_SetViewType(Camera);
			Sleep(300);
			//저장용 UI Dlg가 떠있으면 "아니오"로 종료 시키자.
			HWND DlgModeH = ::FindWindowEx(NULL, 0,"#32770","Observation application");
			if(DlgModeH != NULL)
			{
				HWND ExtHHWnd = ::FindWindowEx(DlgModeH, 0,"Button","아니요(&N)");

				::SendMessage(ExtHHWnd, BM_CLICK, 0, 0); 
			}
		}
		BlockInput(FALSE);

	}
}

UINT fn_OBS_LiveProcess(LPVOID pParam)
{
	RUNNER_PROCESS	*		pProcParam			= (RUNNER_PROCESS *) pParam;
	CInterServerInterface *pServerObj = pProcParam->m_pServerObj;

	

	IplImage	* pHistogramImg = cvCreateImage( cvSize(300, 170), IPL_DEPTH_8U, 3);
	IplImage *pViewPort = cvCreateImage(cvSize(1024,768),IPL_DEPTH_8U, 3 );
	IplImage *pHistViewPort = cvCreateImage(cvSize(1024,768),IPL_DEPTH_8U, 1 );


	CString strDisplayText;
	CvRect HistPos;
	HistPos.x = pViewPort->width - pHistogramImg->width -3;
	HistPos.y = pViewPort->height - pHistogramImg->height -3;
	HistPos.width = pHistogramImg->width;
	HistPos.height = pHistogramImg->height;

	CvvImage ViewImage;
	ViewImage.Create(pViewPort->width, pViewPort->height, ((IPL_DEPTH_8U & 255)*3) );

	HWND ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
	ShowWindow(ObserHWND, SW_RESTORE);
	HWND LiveHWnd = ::FindWindowEx(ObserHWND, 0, 0,"VK-X Image Pane");
	while (pProcParam->m_bProcessRun == TRUE)
	{
		ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
		LiveHWnd	= ::FindWindowEx(ObserHWND, 0, 0,"VK-X Image Pane");
		if(ObserHWND != NULL && LiveHWnd != NULL)
		{
			HDC h_screen_dc = ::GetDC(LiveHWnd);

			if(h_screen_dc != NULL)
			{
				// 현재 스크린의 해상도를 얻는다.
				int width = 1024;//::GetDeviceCaps(h_screen_dc, HORZRES);
				int height = 768;// ::GetDeviceCaps(h_screen_dc, VERTRES);

				// DIB의 형식을 정의한다.
				BITMAPINFO dib_define;
				dib_define.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
				dib_define.bmiHeader.biWidth = width;
				dib_define.bmiHeader.biHeight = height;
				dib_define.bmiHeader.biPlanes = 1;
				dib_define.bmiHeader.biBitCount = 24;
				dib_define.bmiHeader.biCompression = BI_RGB;
				dib_define.bmiHeader.biSizeImage = (((width * 24 + 31) & ~31) >> 3) * height;
				dib_define.bmiHeader.biXPelsPerMeter = 0;
				dib_define.bmiHeader.biYPelsPerMeter = 0;
				dib_define.bmiHeader.biClrImportant = 0;
				dib_define.bmiHeader.biClrUsed = 0;

				// DIB의 내부 이미지 비트 패턴을 참조할 포인터 변수
				BYTE *p_image_data = NULL;

				// dib_define에 정의된 내용으로 DIB를 생성한다.
				HBITMAP h_bitmap = ::CreateDIBSection(h_screen_dc, &dib_define, DIB_RGB_COLORS, (void **)&p_image_data, 0, 0);

				// 이미지를 추출하기 위해서 가상 DC를 생성한다. 메인 DC에서는 직접적으로 비트맵에 접근하여
				// 이미지 패턴을 얻을 수 없기 때문이다.
				HDC h_memory_dc = ::CreateCompatibleDC(h_screen_dc);

				// 가상 DC에 이미지를 추출할 비트맵을 연결한다.
				HBITMAP h_old_bitmap = (HBITMAP)::SelectObject(h_memory_dc, h_bitmap);

				while (pProcParam->m_bProcessRun == TRUE && LiveHWnd != NULL)
				{
					LiveHWnd	= ::FindWindowEx(ObserHWND, 0, 0,"VK-X Image Pane");
					if(LiveHWnd!= NULL  && *pProcParam->m_pObsActionState != ActionObs_AutoUpDownPosSetByImg)
					{
						// 현재 스크린 화면을 캡쳐한다.
						::BitBlt(h_memory_dc, 0, 0, width, height, h_screen_dc, 0, 0, SRCCOPY);

						if(p_image_data != NULL)
						{
							memcpy(pViewPort->imageData, p_image_data, pViewPort->imageSize);
							cvFlip(pViewPort, pViewPort, 1);
							cvCvtColor(pViewPort,pHistViewPort,CV_BGR2GRAY); // cimg -> gimg
							
							GetImgHistogram(pHistViewPort, pHistogramImg);
							cvThreshold(pHistViewPort, pHistViewPort, *pProcParam->m_pDetectThreshold, 255, CV_THRESH_BINARY); 
							*pProcParam->m_pDetectCountResult = cvCountNonZero(pHistViewPort);

							strDisplayText.Format("Detect Cnt : %d",*pProcParam->m_pDetectCountResult);
							PutImage_Text( pViewPort, cvPoint(10,25), strDisplayText.GetBuffer(strDisplayText.GetLength()));
							cvSetImageROI(pViewPort, HistPos);
							cvCopyImage( pHistogramImg, pViewPort);
							cvResetImageROI(pViewPort);
							
							ViewImage.CopyOf(pViewPort);
							ViewImage.DrawToHDC(*pProcParam->m_pHDcView, pProcParam->m_pViewRect);
							
							cvWaitKey(10);
						}
						
					}
					else
						 break;
				}
				// 본래의 비트맵으로 복구한다.
				::SelectObject(h_memory_dc, h_old_bitmap); 

				// 가상 DC를 제거한다.
				DeleteDC(h_memory_dc);
			}
			if(h_screen_dc != NULL)
				::ReleaseDC(LiveHWnd, h_screen_dc);
		}
		Sleep(100);
	}
	cvReleaseImage(&pViewPort);
	cvReleaseImage(&pHistViewPort);
	cvReleaseImage(&pHistogramImg);
	ViewImage.Destroy();

	pProcParam->m_bProcessRun = FALSE;
	pProcParam->m_pRunnerThread = NULL;
	return 0;
}
UINT fn_OBS_RunnerProcess(LPVOID pParam)
{
	RUNNER_PROCESS	*		pProcParam			= (RUNNER_PROCESS *) pParam;
	CInterServerInterface *pServerObj = pProcParam->m_pServerObj;

	cvInitFont (&G_DataInfoFont, CV_FONT_HERSHEY_SIMPLEX , 0.3f, 0.4f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	
	cvInitFont (&G_ImageInfoFont, CV_FONT_HERSHEY_TRIPLEX , 0.5f, 0.6f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	cvInitFont (&G_ImageInfoSFont, CV_FONT_HERSHEY_TRIPLEX , 0.3f, 0.4f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	cvInitFont (&G_ImageInfoVSFont, CV_FONT_HERSHEY_SIMPLEX , 0.3f, 0.4f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX

	cvInitFont (&G_ImageInfoBFont, CV_FONT_HERSHEY_SIMPLEX , 1.0f, 1.2f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX

	
	//RUNNER_PROCESS		stLiveProc;

	//stLiveProc = *pProcParam;
	//stLiveProc.m_pRunnerThread		= ::AfxBeginThread(fn_OBS_LiveProcess, &stLiveProc, THREAD_PRIORITY_NORMAL);

	HWND ObserHWND = NULL;
	while (pProcParam->m_bProcessRun == TRUE)
	{
		if(*pProcParam->m_pObservationAppState == ObserveSW_None)
		{//Program이 구동중이 아님으로 구동 Start를 시킨다.
			ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
			ShowWindow(ObserHWND, SW_RESTORE);
			if(ObserHWND == NULL)
			{
				::WinExec("VKViewerApp.exe", 1);
				G_vtAddLog(pServerObj, "Start VKViewerApp.exe");
			}

			while (pProcParam->m_bProcessRun == TRUE)
			{
				ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
				if(ObserHWND != NULL)
				{
					if(::IsWindowVisible(ObserHWND) == TRUE)
					{
						Sleep(2000);
						*pProcParam->m_pObserAPPStart = TRUE;

						CRect WinRect;
						::GetWindowRect(ObserHWND, &WinRect);
						::SetWindowPos(pProcParam->m_MainWinHWnd,NULL,WinRect.left+WinRect.Width(), WinRect.top, 190*2,WinRect.Height(), SWP_SHOWWINDOW);
						break;
					}
				}
				Sleep(50);
			}
		}
		if(*pProcParam->m_pObservationAppState == ObserveSW_NoneRemote)
		{
			ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
			HWND RemoteModeH = ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Under remote control");
			ShowWindow(ObserHWND, SW_RESTORE);

			if(ObserHWND != NULL)
			{
				HWND Depth1				= ::FindWindowEx(ObserHWND, 0, "WindowsForms10.Window.8.app.0.378734a","");
				Depth1					= ::FindWindowEx(ObserHWND, Depth1, "WindowsForms10.Window.8.app.0.378734a","");
				HWND Depth2				= ::FindWindowEx(Depth1, 0, "WindowsForms10.Window.8.app.0.378734a","");
				HWND Depth3				= ::FindWindowEx(Depth2, 0, "WindowsForms10.Window.8.app.0.378734a","");
				Depth3					= ::FindWindowEx(Depth2, Depth3, "WindowsForms10.Window.8.app.0.378734a","");
				Depth3					= ::FindWindowEx(Depth2, Depth3, "WindowsForms10.Window.8.app.0.378734a","");
				HWND FinishH		= ::FindWindowEx(Depth3, 0, "WindowsForms10.BUTTON.app.0.378734a","Finish");
				//if(::IsWindowEnabled(FinishH) == TRUE)
				::SendMessage(FinishH, BM_CLICK, 0, 0);
				Sleep(200);
			}

			if(ObserHWND != NULL && RemoteModeH == NULL)
			{// 자동실행일경우 처음 Remote로 전환 하여 제어권을 가져온다.
				G_vtAddLog(pServerObj, "Remote Mode Select");
				ObservationAppChangeRemote();
			}
		}

		Sleep(500);
	}
	Sleep(50);
	pProcParam->m_bProcessRun = FALSE;
	pProcParam->m_pRunnerThread = NULL;
	return 0;
}

void Action_AutoFocus(ACTION_FN *pParam)
{
	G_vtAddLog(pParam->m_pServerObj, "Start Action_AutoFocus");
	*pParam->m_pObsActionState = ActionObs_AutoFocus;
	pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*pParam->m_pObsActionState);

	//정의천 추가
	if(G_VKRevolver.m_fnIsLensIndex() == LENS_INDEX_10X)
	{	
		// for test
		OutputDebugStringA("[M3D] Start  VK_MoveLensToOrigin");

		VKResult result =(VKResult)VK_MoveLensToOrigin();
		OutputDebugStringA("[M3D] End VK_MoveLensToOrigin");

		Sleep(300);
		// 10x에서 AF 할때는 항상 기준 위치(org)에서 시작 하도록 추가함
		// Scope Main Z-Axis 운영은 Scope의 org에서 10X AF가 맞도록 설정 해야한다. 
		// 결과 처리 무시....
/*		
		switch(result)
		{
		case VKResult_OK:
			*pParam->m_pObsActionState = ActionObs_None;
			break;
		default:
			*pParam->m_pObsActionState = ActionObs_AutoFocus_Error;
			break ;
		}
*/
	}
	//////////////////////////////////
	OutputDebugStringA("[M3D] Start VK_DoAutofocus");
	VKResult result = (VKResult)VK_DoAutofocus();
	switch(result)
	{
	case VKResult_OK:
		*pParam->m_pObsActionState = ActionObs_None;
		break;
	default:
		*pParam->m_pObsActionState = ActionObs_AutoFocus_Error;
		break ;
	}
	pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*pParam->m_pObsActionState);
	G_vtAddLog(pParam->m_pServerObj, "End Action_AutoFocus");
	pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System, nBiz_Recv_AutoFocusEnd, (USHORT)*pParam->m_pObsActionState);

}


int FindDetectedValue(HDC h_memory_dc, HDC h_screen_dc, BYTE *p_image_data,IplImage *pViewPort, IplImage *pHistViewPort, int DetectThreshold)
{
	int ReturnValue =0;
	::BitBlt(h_memory_dc, 0, 0, 1024, 768, h_screen_dc, 0, 0, SRCCOPY);

	if(p_image_data != NULL)
	{
		memcpy(pViewPort->imageData, p_image_data, pViewPort->imageSize);
		cvFlip(pViewPort, pViewPort, 1);
		cvCvtColor(pViewPort,pHistViewPort,CV_BGR2GRAY); // cimg -> gimg
		cvThreshold(pHistViewPort, pHistViewPort, DetectThreshold, 255, CV_THRESH_BINARY); 
		ReturnValue = cvCountNonZero(pHistViewPort);

	}
	return ReturnValue;
}
void Action_AutoUpDnPosByImage(ACTION_FN *pParam, int *pDetectValue, long TargetThick)
{
	long lStartLensPos= 0;
	long lNowLensPos= 0;
	long FindUpperPos = 0;
	long FindLowerPos = 0;

	int MoveDelay = 200;
	int UpDnOffse = 3000;
	int JupNanoMeter =1000;
	int DetectValue = 0;
	G_vtAddLog(pParam->m_pServerObj, "Start Action_AutoUpDnPos");
	HWND ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
	if(ObserHWND != NULL && pDetectValue != NULL)
	{
		HWND LiveHWnd = ::FindWindowEx(ObserHWND, 0, 0,"VK-X Image Pane");

		IplImage *pViewPort = cvCreateImage(cvSize(1024,768),IPL_DEPTH_8U, 3 );
		IplImage *pHistViewPort = cvCreateImage(cvSize(1024,768),IPL_DEPTH_8U, 1 );
		//////////////////////////////////////////////////////////////////////////
		HDC h_screen_dc = ::GetDC(LiveHWnd);

		if(h_screen_dc != NULL)
		{
			// 현재 스크린의 해상도를 얻는다.
			int width = 1024;//::GetDeviceCaps(h_screen_dc, HORZRES);
			int height = 768;// ::GetDeviceCaps(h_screen_dc, VERTRES);

			// DIB의 형식을 정의한다.
			BITMAPINFO dib_define;
			dib_define.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
			dib_define.bmiHeader.biWidth = width;
			dib_define.bmiHeader.biHeight = height;
			dib_define.bmiHeader.biPlanes = 1;
			dib_define.bmiHeader.biBitCount = 24;
			dib_define.bmiHeader.biCompression = BI_RGB;
			dib_define.bmiHeader.biSizeImage = (((width * 24 + 31) & ~31) >> 3) * height;
			dib_define.bmiHeader.biXPelsPerMeter = 0;
			dib_define.bmiHeader.biYPelsPerMeter = 0;
			dib_define.bmiHeader.biClrImportant = 0;
			dib_define.bmiHeader.biClrUsed = 0;

			// DIB의 내부 이미지 비트 패턴을 참조할 포인터 변수
			BYTE *p_image_data = NULL;

			// dib_define에 정의된 내용으로 DIB를 생성한다.
			HBITMAP h_bitmap = ::CreateDIBSection(h_screen_dc, &dib_define, DIB_RGB_COLORS, (void **)&p_image_data, 0, 0);

			// 이미지를 추출하기 위해서 가상 DC를 생성한다. 메인 DC에서는 직접적으로 비트맵에 접근하여
			// 이미지 패턴을 얻을 수 없기 때문이다.
			HDC h_memory_dc = ::CreateCompatibleDC(h_screen_dc);

			// 가상 DC에 이미지를 추출할 비트맵을 연결한다.
			HBITMAP h_old_bitmap = (HBITMAP)::SelectObject(h_memory_dc, h_bitmap);
		
			//////////////////////////////////////////////////////////////////////////
			*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg;
			if((VKResult)VK_SetViewType(Laser) == VKResult_OK)
			{
				//Camera ->Laser 변환후 View 변환 속도가 늦다.
				Sleep(2000);

				//////////////////////////////////////////////////////////////////////////
				//1)현재 위치 가져 온기.
				//long nanoThick = TargetThick;//um to nm
				
				//이위치는 10x의 Focus위치이다. 
				if((VKResult)VK_GetLensPosition(&lStartLensPos) == VKResult_OK)
				{
					//2) 지정 두깨만큼 위로 올리기.(+알파)
					long StepHeightValue = TargetThick;//+(TargetThick/2);//50% ++
			//		if((VKResult)VK_MoveLens((-1)*(StepHeightValue*2) ) == VKResult_OK) // 정의천 수정
					if((VKResult)VK_MoveLens((-1)*(StepHeightValue) ) == VKResult_OK)
					{
						Sleep(((TargetThick/1000)*2)*15);//지정위치로 가는 거리와 시간이 크다.

						//2배이상 위로 올리고. 내리면서 0이 아닌 위치를 찾는다.
						DetectValue = FindDetectedValue(h_memory_dc, h_screen_dc, p_image_data,pViewPort, pHistViewPort, *pParam->m_pDetectThreshold);
						while(*pParam->m_bpMainProcRun == TRUE && DetectValue < 800)
						{
							if((VKResult)VK_MoveLens(JupNanoMeter*2) != VKResult_OK)
							{
								*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;
								break;
							}
							// 정의천 추가 (For safety )
							if((VKResult)VK_GetLensPosition(&lNowLensPos) == VKResult_OK)
							{
								if((lNowLensPos-lStartLensPos) >TargetThick*3)
								{
									*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;
									break;
								}							
							}
							////////////////////////////////////////////////추가 end
							Sleep(MoveDelay*2);
							DetectValue = FindDetectedValue(h_memory_dc, h_screen_dc, p_image_data,pViewPort, pHistViewPort, *pParam->m_pDetectThreshold);	
						}
						//상단부 찾기 완료.

						//지정수치 입력(하위 탐색없이 바로 입력.)
						{
							if((VKResult)VK_GetLensPosition(&FindUpperPos) == VKResult_OK)
							{ 
								G_vtAddLog(pParam->m_pServerObj, "Find Upper Pos(%d)", FindUpperPos);
								if((VKResult)VK_SetUpperPosition(FindUpperPos-(UpDnOffse*2)) == VKResult_OK)
								{
									//if((VKResult)VK_SetLowerPosition(FindUpperPos+TargetThick+(UpDnOffse*3)) != VKResult_OK)  //정의천 수정
									if((VKResult)VK_SetLowerPosition(FindUpperPos+TargetThick+(UpDnOffse*2)) != VKResult_OK)
									{
										*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;
									}
									else
									{
										G_vtAddLog(pParam->m_pServerObj, "Scan Height Set(Up:%d, lower:%d)", FindUpperPos-UpDnOffse, FindUpperPos+TargetThick+(UpDnOffse*3));
									}
								}
								else
									*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;
							}
							else
								*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;
						}
					}
					else
						*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;
				}
				else
					*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;
			}
			else
				*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;

			// 본래의 비트맵으로 복구한다.
			::SelectObject(h_memory_dc, h_old_bitmap); 
			// 가상 DC를 제거한다.
			DeleteDC(h_memory_dc);
			if(h_screen_dc != NULL)
				::ReleaseDC(LiveHWnd, h_screen_dc);
		}

		if(*pParam->m_pObsActionState == ActionObs_AutoUpDownPosSetByImg)
			*pParam->m_pObsActionState = ActionObs_None;
		else
		{
			G_vtAddLog(pParam->m_pServerObj, "AutoUpDownPosSetByImg Error");
		}
		VK_SetViewType(Camera);
		
	}
	else
		*pParam->m_pObsActionState = ActionObs_AutoUpDownPosSetByImg_Error;

	pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*pParam->m_pObsActionState);

	G_vtAddLog(pParam->m_pServerObj, "End Action_AutoUpDnPos");
}
void Action_StartMeasure(ACTION_FN *pParam, long TargetThick)
{
	G_vtAddLog(pParam->m_pServerObj, "Start Action_StartMeasure");
	HWND ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
	if(ObserHWND != NULL)
	{
		::SetWindowPos(ObserHWND, HWND_TOPMOST,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);

		::SetFocus(ObserHWND);
		::SetActiveWindow(ObserHWND);
		SetForegroundWindow(ObserHWND);

		VKResult result =VKResult_OK;

		*pParam->m_pObsActionState = ActionObs_StartMeasurement;
		pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*pParam->m_pObsActionState);
		result = (VKResult)VK_StartMeasurement();
		switch(result)
		{
		case VKResult_OK:
			{
				//::SetTimer(pProcParam->m_MainWinHWnd, HW_MEASUREMENT_CHECK_TIMER, HW_MEASUREMENT_CHECK_Interval, 0);
				BOOL bMeasuring;
				VKResult result = (VKResult)VK_IsMeasuring(&bMeasuring);
				while (pParam->m_bProcessRun == TRUE && bMeasuring == TRUE)
				{
					result = (VKResult)VK_IsMeasuring(&bMeasuring);
					switch(result)
					{
					case VKResult_OK:
						break;
					case VKResult_NotAccepted:
						break;
					case VKResult_InvalidArgument:
						break;
					default:
						break;
					}
				}

				*pParam->m_pObsActionState = ActionObs_None;
			}break;
		default:
			*pParam->m_pObsActionState = ActionObs_StartMeasurement_Error;
			break;
		}
	}
	else
		*pParam->m_pObsActionState = ActionObs_StartMeasurement_Error;

	pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*pParam->m_pObsActionState);
	::SetWindowPos(ObserHWND, HWND_NOTOPMOST,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);
	//::SetWindowPos(pParam->m_MainWinHWnd, HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	G_vtAddLog(pParam->m_pServerObj, "End Action_StartMeasure");
}

void Action_AutoSaveData(ACTION_FN *pParam, char *pWriteFileFullPath)//, int *pMaskType, int Thickness, HWND D3DViewerHandle, DATA_3D_RAW * pNowMeasurData)
{
	G_vtAddLog(pParam->m_pServerObj, "Start Action_AutoSaveData");
	*pParam->m_pObsActionState = ActionObs_AutoSaveData;
	pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*pParam->m_pObsActionState);

	VKResult result = VKResult_UnknownError;
	if(/*result == VKResult_OK && */pWriteFileFullPath != NULL)
	{
		CString Vk4SavePath;
		Vk4SavePath.Format("%s", pWriteFileFullPath);
		CT2A cstr(Vk4SavePath);
		if(VKResult_OK != VK_SaveMeasurementResult(cstr))
			*pParam->m_pObsActionState = ActionObs_AutoSaveData_Error;
	}
	else
	{
		*pParam->m_pObsActionState = ActionObs_AutoSaveData_Error;
		pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*pParam->m_pObsActionState);
		return;
	}

	result = (VKResult)VK_SetViewType(Camera);
	pParam->m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status,  nBiz_3DRecv_ActionStatus, (USHORT)*pParam->m_pObsActionState);
	G_vtAddLog(pParam->m_pServerObj, "End Action_AutoSaveData");

}

UINT fn_OBS_AutoFocusActionProc(LPVOID pParam)
{
	ACTION_PROC *pProcParam = (ACTION_PROC*)pParam;
	CInterServerInterface *pServerObj = pProcParam->m_pServerObj;
	//이전 Data가 중단되면서 남아있다면 제거 하는 Step과 같다.
	VK_SetViewType(Camera);
	//1) Command 보내기.
	{
		ACTION_FN Param;
		Param.m_bpMainProcRun		= &pProcParam->m_bProcessRun;
		Param.m_bProcessRun			= TRUE;
		Param.m_pObsActionState		= pProcParam->m_pObsActionState;
		Param.m_MainWinHWnd			= pProcParam->m_MainWinHWnd;
		Param.m_pServerObj				= pProcParam->m_pServerObj;
		Param.m_pDetectThreshold	= pProcParam->m_pDetectThreshold;

		Action_AutoFocus(&Param);
	}
	pProcParam->m_bProcessRun = FALSE;
	pProcParam->m_pRunnerThread = NULL;
	return 0;
}

UINT fn_OBS_StartMeasureActionProc(LPVOID pParam)
{
	ACTION_PROC *pProcParam = (ACTION_PROC*)pParam;
	CInterServerInterface *pServerObj = pProcParam->m_pServerObj;
	//1) Command 보내기.
	//이전 Data가 중단되면서 남아있다면 제거 하는 Step과 같다.
	VK_SetViewType(Camera);
	{
		ACTION_FN Param;
		Param.m_bpMainProcRun		= &pProcParam->m_bProcessRun;
		Param.m_bProcessRun			= TRUE;
		Param.m_pObsActionState		= pProcParam->m_pObsActionState;
		Param.m_MainWinHWnd			= pProcParam->m_MainWinHWnd;
		Param.m_pServerObj				= pProcParam->m_pServerObj;
		Param.m_pDetectThreshold	= pProcParam->m_pDetectThreshold;

		Action_StartMeasure(&Param);
	}
	pProcParam->m_bProcessRun = FALSE;
	pProcParam->m_pRunnerThread = NULL;
	return 0;
}

UINT fn_OBS_AutoMeasureActionProc(LPVOID pParam)
{
	ACTION_PROC *pProcParam = (ACTION_PROC*)pParam;
	CInterServerInterface *pServerObj = pProcParam->m_pServerObj;

	ACTION_FN Param;
	Param.m_bpMainProcRun		= &pProcParam->m_bProcessRun;
	Param.m_bProcessRun			= TRUE;
	Param.m_pObsActionState		= pProcParam->m_pObsActionState;
	Param.m_MainWinHWnd			= pProcParam->m_MainWinHWnd;
	Param.m_pServerObj				= pProcParam->m_pServerObj;
	Param.m_pDetectThreshold	= pProcParam->m_pDetectThreshold;


	HWND ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
	HWND RemoteModeH = ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Under remote control");
#ifndef _SIMULATE_MEASURE
	if(ObserHWND == NULL)
	{
	}
	if(RemoteModeH == NULL)
	{
		*Param.m_pObsActionState	 = ActionObs_RemoteMode_Error;
		Param.m_pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*Param.m_pObsActionState);
		G_vtAddLog(pServerObj, "fn_OBS_AutoMeasureActionProc Remote Mode Error");
	}
	else
#endif
	{
		::SetWindowPos(ObserHWND, HWND_TOPMOST,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);
		::SetFocus(ObserHWND);
		::SetActiveWindow(ObserHWND);
		SetForegroundWindow(ObserHWND);
		//이전 Data가 중단되면서 남아있다면 제거 하는 Step과 같다.
		VK_SetViewType(Camera);
		G_vtAddLog(pServerObj, "Start fn_OBS_AutoMeasureActionProc");
		for(int RunStep =0; pProcParam->m_StepList[RunStep] != Run_None && pProcParam->m_bProcessRun == TRUE; RunStep++)
		{
#ifndef _SIMULATE_MEASURE
			if(*Param.m_pObsActionState != ActionObs_None)
			{//동작중 오류가 있었다.
				break;
			}
#endif
			Act_Tyep ActionIndex = pProcParam->m_StepList[RunStep];

			G_vtAddLog(pServerObj, "%d) Start Step(%d)", RunStep, ActionIndex);
			switch(ActionIndex)
			{
			case Run_AutoFocus:
				Action_AutoFocus(&Param);
				break;
			case Run_AutoUpDownPosSetByImg:
				::SetWindowPos(ObserHWND, HWND_TOPMOST,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);
				::SetFocus(ObserHWND);
				::SetActiveWindow(ObserHWND);
				SetForegroundWindow(ObserHWND);
				Sleep(1000);
				if(pProcParam->m_TargetThick >200)  pProcParam->m_TargetThick=200;
//				Action_AutoUpDnPosByImage(&Param, pProcParam->m_pDetectCountResult, pProcParam->m_TargetThick);
				Action_AutoUpDnPosByImage(&Param, pProcParam->m_pDetectCountResult, pProcParam->m_TargetThick*1000);
				break;
			case Run_StartMeasurement:
				//Action_StartMeasure(&Param, pProcParam->m_TargetThick);
				Action_StartMeasure(&Param, pProcParam->m_TargetThick*1000);
				break;
			case Run_AutoSaveData:
#ifdef _SIMULATE_MEASURE
				if(*pProcParam->m_pMaskType == MASK_TYPE_DOT) {
						::CopyFile("Z:\\sample_dot.vk4", pProcParam->m_WriteDataFilePath, TRUE);
				}
				else if(*pProcParam->m_pMaskType == MASK_TYPE_STRIPE) {
					::CopyFile("Z:\\sample_stripe.vk4", pProcParam->m_WriteDataFilePath, TRUE);
				}
#else
				Action_AutoSaveData(&Param, &pProcParam->m_WriteDataFilePath[0]);
#endif
				break;
			}
			Sleep(500);
		}
		::SetWindowPos(ObserHWND, HWND_NOTOPMOST,0,0,0,0,/*SWP_NOMOVE|*/SWP_NOSIZE);
		//////////////////////////////////////////////////////////////////////////
		::SendMessage(pProcParam->m_MainWinHWnd,WM_PROC_FILE_DATA, (WPARAM)&Param, (LPARAM)pProcParam );

		pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_Status, nBiz_3DRecv_ActionStatus, (USHORT)*pProcParam->m_pObsActionState);
		G_vtAddLog(pServerObj, "End fn_OBS_AutoMeasureActionProc");
	}
	pProcParam->m_bProcessRun = FALSE;
	pProcParam->m_pRunnerThread = NULL;
	return 0;
}


//ImageSave Command 조합.
UINT fn_OBS_ImageSaveActionProc(LPVOID pParam)
{
	ACTION_PROC *pProcParam = (ACTION_PROC*)pParam;
	CInterServerInterface *pServerObj = pProcParam->m_pServerObj;
	//Review Camera Mode로 바꾼다.
	VK_SetViewType(Camera);
	HWND ObserHWND	= ::FindWindow("WindowsForms10.Window.8.app.0.378734a", "Observation application");
	if(ObserHWND != NULL)
	{
		HWND LiveHWnd = ::FindWindowEx(ObserHWND, 0, 0,"VK-X Image Pane");

		IplImage *pViewPort = cvCreateImage(cvSize(1024,768),IPL_DEPTH_8U, 3 );
		//////////////////////////////////////////////////////////////////////////
		HDC h_screen_dc = ::GetDC(LiveHWnd);

		// 현재 스크린의 해상도를 얻는다.
		int width = 1024;//::GetDeviceCaps(h_screen_dc, HORZRES);
		int height = 768;// ::GetDeviceCaps(h_screen_dc, VERTRES);

		// DIB의 형식을 정의한다.
		BITMAPINFO dib_define;
		dib_define.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		dib_define.bmiHeader.biWidth = width;
		dib_define.bmiHeader.biHeight = height;
		dib_define.bmiHeader.biPlanes = 1;
		dib_define.bmiHeader.biBitCount = 24;
		dib_define.bmiHeader.biCompression = BI_RGB;
		dib_define.bmiHeader.biSizeImage = (((width * 24 + 31) & ~31) >> 3) * height;
		dib_define.bmiHeader.biXPelsPerMeter = 0;
		dib_define.bmiHeader.biYPelsPerMeter = 0;
		dib_define.bmiHeader.biClrImportant = 0;
		dib_define.bmiHeader.biClrUsed = 0;

		// DIB의 내부 이미지 비트 패턴을 참조할 포인터 변수
		BYTE *p_image_data = NULL;
		// dib_define에 정의된 내용으로 DIB를 생성한다.
		HBITMAP h_bitmap = ::CreateDIBSection(h_screen_dc, &dib_define, DIB_RGB_COLORS, (void **)&p_image_data, 0, 0);
		// 이미지를 추출하기 위해서 가상 DC를 생성한다. 메인 DC에서는 직접적으로 비트맵에 접근하여
		// 이미지 패턴을 얻을 수 없기 때문이다.
		HDC h_memory_dc = ::CreateCompatibleDC(h_screen_dc);

		// 가상 DC에 이미지를 추출할 비트맵을 연결한다.
		HBITMAP h_old_bitmap = (HBITMAP)::SelectObject(h_memory_dc, h_bitmap);
		::BitBlt(h_memory_dc, 0, 0, 1024, 768, h_screen_dc, 0, 0, SRCCOPY);
		memcpy(pViewPort->imageData, p_image_data, pViewPort->imageSize);
		cvFlip(pViewPort, pViewPort, 1);

		cvSaveImage(pProcParam->m_WriteDataFilePath, pViewPort);
		G_vtAddLog(pServerObj, "Save Image[%s]", pProcParam->m_WriteDataFilePath);
		// 본래의 비트맵으로 복구한다.
		::SelectObject(h_memory_dc, h_old_bitmap); 
		// 가상 DC를 제거한다.
		DeleteDC(h_memory_dc);
		if(h_screen_dc != NULL)
			::ReleaseDC(LiveHWnd, h_screen_dc);
		cvReleaseImage(&pViewPort);

		//Unit을 1로 전달 하여 정상 저장을 알리자.
		pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System,  nBiz_Recv_AlignGrabEnd, 1);
	}
	else
	{
		//Error nBiz_Unit_Zero로 Return 하자.
		pServerObj->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_3DMeasure_System,  nBiz_Recv_AlignGrabEnd, nBiz_Unit_Zero);
	}
	pProcParam->m_bProcessRun = FALSE;
	pProcParam->m_pRunnerThread = NULL;
	return 0;
}