/// [2014-11-05] Charles, Added.

#pragma once

#include "MeasureAlg.h"

//
// DOT 형 측정 알고리즘 객체
//
class CMeasureDotAlg : public CMeasureAlg
{
public:
	CMeasureDotAlg(void);
	virtual ~CMeasureDotAlg(void);

	static CMeasureAlg *GetInstance();

	virtual BOOL Measure();
	virtual BOOL MakeResultImage();

protected:
	virtual BOOL _PreprocessData();
	virtual BOOL _GenerateRefData();
	virtual BOOL _GetMinCenterPoints();
	virtual BOOL _GetMeasurePoints();
	virtual BOOL _GetHeightProfile();
	virtual BOOL _CheckCornerDistance();
	virtual BOOL _MeasureHeightProfile();

	virtual BOOL _MakeResultData();

	virtual BOOL _MakeResultFullImage();
	virtual BOOL _MakeResultHeightImage();
	virtual BOOL _MakeResultDataImage();
	virtual BOOL _MakeAnalysisResultDataImage();

	BOOL _GetVertMinCenterPoints(int nPosX, BOOL bAdditional = FALSE);
	BOOL _SelectEndMinCenterPoint(int &nStartPosX, int &nStartPosY, int &nEndPosX, int &nEndPosY, BOOL bSecondPhase);
};
