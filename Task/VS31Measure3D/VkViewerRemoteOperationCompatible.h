#pragma once
#include "VkViewerRemoteOperation.h"

/**
	@brief 高さデ??取得 (VK-8700/9700互換)
	@param *lHeightData 高さデ??
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_OutOfMemory メモリ不足
	@retval VKResult_UnknownError 不明なエラ?
	@note 幅*高さ分のlong?デ??領域を確保しておく
	@note 測定サイズにより確保が必要なサイズが異なるので注意
*/
long VKAPI VK_GetHeightData(long* plHeightData);

/**
	@brief ライン高さデ??取得 (VK-8700/9700互換)
	@param lLineNumber ライン番号(1のみ)
	@param *plHeightData 高さデ??
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_OutOfMemory メモリ不足
	@retval VKResult_UnknownError 不明なエラ?
	@note 1024(幅)*1(高さ)分のlong?デ??領域を確保しておく
*/
long VKAPI VK_GetLineHeightData(long lLineNumber, long* plHeightData);

/**
	@brief ゲイン設定・取得(VK-8700/9700互換用)
	@param lGain, *plGain ゲイン(底値-4095)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
	@note VK_GetLaserGain1/VK_SetLaserGain1で取得・設定できる値をスケ?リングして取り扱う
*/
long VKAPI VK_SetGain(long lGain);
long VKAPI VK_GetGain(long* plGain);

/**
	@brief ライン位置設定・取得(VK-8700/9700互換用)
	@param lLineNumber ライン番号(1-3)
	@param lLinePosition, *plLinePosition ライン位置(0-767)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
*/
long VKAPI VK_SetLinePosition(long lLineNumber, long lLinePosition);
long VKAPI VK_GetLinePosition(long lLineNumber, long* plLinePosition);

/**
	@brief ライン数設定・取得(VK-8700/9700互換用)
	@param lNumberOfLine, *plNumberOfLine ライン数(1のみ)
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_UnknownError 不明なエラ?
	@note この関数は使用する必要がありません (VK-8700/VK-9700との互換目的で用意されています)
*/
long VKAPI VK_SetNumberOfLine(long lNumberOfLine);
long VKAPI VK_GetNumberOfLine(long* plNumberOfLine);

/**
	@brief 測定条件取得 (VK-8700/9700互換)
	@param *pParameter 測定条件?造体デ??
	@retval VKResult_OK 成功
	@retval VKResult_NotInitialized 未初期化
	@retval VKResult_NotAccepted 観?アプリがリモ?トモ?ドになっていない、状態が不正
	@retval VKResult_InvalidArgument 引数が不正
	@retval VKResult_ConnectionLost コントロ?ラ?と接続されていない/切断された
	@retval VKResult_UnknownError 不明なエラ?
*/
typedef struct
{
	MeasurementMode mode;				// 測定モ?ド
	MeasurementQuality quality;			// 測定品質
	long lUpperPosition;				// レンズ上限(nm)
	long lLowerPosition;				// レンズ下限(nm)
	long lPitch;						// ピッ?(nm)
	long lDistance;						// ディス?ンス(nm)
	long lNd;							// レ?ザ?照明フィル??1
	long lGain;							// レ?ザ?明るさ1 (VK-8700/VK-9700互換 底値-4095)
	long lShutterSpeed;					// シャッ??スピ?ド (VK-8700/VK-9700互換 0-255)
	BOOL bIsShutterSpeedAuto;			// シャッ??スピ?ド自動
	long lLinePosition[3];				// ライン位置(pixel)
	long lLineNumber;					// ライン数(?)
	BOOL bIsRpd;						// RPD
	RpdPreference rpdPreference;		// Z軸モ?ド
	ZoomIndex zoom;						// ズ??
	double dXYCalibration;				// XYキャリブレ?ション(μm/pixel)
	double dZCalibration;				// Zキャリブレ?ション(μm, VK-8700/VK-9700互換, GetHeightData/GetLineHeightData向け)
	BOOL bEnableColorAcquisition;		// 白黒モ?ド
	BOOL bThicknessParameterAuto;		// 膜厚パラメ??の自動設定
	LayerAdjustFilter filter;			// 層間調整フィル?
	PeakDetectParam peakDetectParam;	// 不要ピ?ク除去
	long lThickness;					// 膜厚(nm)
} MeasurementParameter;
long VKAPI VK_GetMeasurementParameter(MeasurementParameter* pParameter);
