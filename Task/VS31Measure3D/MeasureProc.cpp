// Viewer2D.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS31Measure3D.h"
#include "MeasureProc.h"
#include "VS31Measure3DDlg.h"
#include "BlobLabeling.h"

#include "MeasurementParameterList.h"
typedef struct DEFECT_3DDATA_TAG
{
	int		DefectType;
	int		StartX;
	int		StartY;
	int		EndX;
	int		EndY;

	int		nDefectCount;
	DEFECT_3DDATA_TAG()
	{
		memset(this,0x00, sizeof(DEFECT_3DDATA_TAG));
	}

}DEFECT_3DDATA;

struct SRGB_VALUE
{
	BYTE	nRValue;	///해당 픽셀의 Red Value
	BYTE	nGValue;	///해당 픽셀의 Green Value
	BYTE	nBValue;	///해당 픽셀의 Blue Value
	SRGB_VALUE()
	{
		memset(this, 0x00, sizeof(SRGB_VALUE));
	}
};

static const unsigned char		DEAD_PIXEL		= 0xFF ;
static const unsigned char		LIVE_PIXEL			= 0x00 ;

DEFECT_3DDATA m_stPreDefectData[100];
void m_fnResultDefectPos(unsigned char* m_chImageData )
{
	SRGB_VALUE	s_TargetRGBValue;
	bool		bSearchResult	= false;	
	int			nTargetStep	= 0;
	int			nTempBack		= 0;		
	
	int			nDefectDistance = 20;
	ZeroMemory(m_stPreDefectData,sizeof(DEFECT_3DDATA) * 100);


	
	for(int nXLineCnt = 1; nXLineCnt < 1024- 1; nXLineCnt++)
	{
		//<해당 열에 결함이 검출되지 않았다면 Skip
		
		for(int nYLineCnt = 0; nYLineCnt < 768- 1; nYLineCnt++)
		{			
			s_TargetRGBValue.nBValue =*((BYTE*)(m_chImageData+nXLineCnt +(nYLineCnt*768)));
			//m_fnReadPosition(nXLineCnt,nYLineCnt,&s_TargetRGBValue);		

			if(	s_TargetRGBValue.nBValue != LIVE_PIXEL) //검출된 에러인가?
			{
				bSearchResult	= TRUE;							//검출 플래그 셑.

				for(int nIndex = 0; nIndex < 100; nIndex++)
				{
					if(m_stPreDefectData[nIndex].nDefectCount == 0)			//현재 Index 의 시작 좌표가 0 인가?
					{
						m_stPreDefectData[nIndex].nDefectCount++;
						m_stPreDefectData[nIndex].StartX = nXLineCnt;	//입력된 좌표를 셑 한다.
						m_stPreDefectData[nIndex].StartY = nYLineCnt;
						m_stPreDefectData[nIndex].EndX = nXLineCnt;
						m_stPreDefectData[nIndex].EndY = nYLineCnt;

						break;
					}
					else										//현재 Index 의 시작 좌표가 0 가 아닌가?
					{
						//if((abs(nYLineCnt - m_nDefectPos[nIndex][1]) > m_stCommonPara.nDefectDistance) &&	//입력된 Y 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
						//	(abs(nYLineCnt - m_nDefectPos[nIndex][3]) > m_stCommonPara.nDefectDistance))
						//	continue;

						if((abs(nXLineCnt - m_stPreDefectData[nIndex].StartX) > nDefectDistance) &&	//입력된 Y 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
							(abs(nXLineCnt -m_stPreDefectData[nIndex].EndX) > nDefectDistance))
							continue;


						if(nYLineCnt > m_stPreDefectData[nIndex].StartY && nYLineCnt < m_stPreDefectData[nIndex].EndY )
						{	
							m_stPreDefectData[nIndex].nDefectCount++;

							if(nYLineCnt < m_stPreDefectData[nIndex].StartY)		//입력된 X 좌표가 현재 Index 의 시작 좌표보다 작은가?
							{								
								m_stPreDefectData[nIndex].StartY = nYLineCnt;	//시작 좌표를 입력된 좌표로 설정한다.
							}
							else if(nYLineCnt > m_stPreDefectData[nIndex].EndY)//입력된 X 좌표가 현재 Index 의 끝 좌표보다 큰가?
							{
								m_stPreDefectData[nIndex].EndY = nYLineCnt;	//끝 좌표를 입력된 좌표로 설정한다.
							}
							m_stPreDefectData[nIndex].EndX = nXLineCnt;
							break;
						}
						
						else if((abs(nYLineCnt - m_stPreDefectData[nIndex].StartY) > nDefectDistance) &&    //입력된 X 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
						   (abs(nYLineCnt - m_stPreDefectData[nIndex].EndY) > nDefectDistance))	//입력된 X 좌표의 거리가 끝 좌표와 설정된 오차 범위 밖 인가?
						  				//입력된 Y 좌표의 거리가 끝 좌표와 설정된 오차 범위 밖 인가?
						{
							continue;	//다음 포인트를 읽는다.
						}

						else			//오차 범위 내 이라면?
						{	
							m_stPreDefectData[nIndex].nDefectCount++;

							if(nYLineCnt < m_stPreDefectData[nIndex].StartY)		//입력된 X 좌표가 현재 Index 의 시작 좌표보다 작은가?
							{								
								m_stPreDefectData[nIndex].StartY = nYLineCnt;	//시작 좌표를 입력된 좌표로 설정한다.
							}
							else if(nYLineCnt > m_stPreDefectData[nIndex].EndY)//입력된 X 좌표가 현재 Index 의 끝 좌표보다 큰가?
							{
								m_stPreDefectData[nIndex].EndY = nYLineCnt;	//끝 좌표를 입력된 좌표로 설정한다.
							}
							m_stPreDefectData[nIndex].EndX = nXLineCnt;
							break;
						}
					}
				}
			}
			else//결함이 아닌 픽셀인가?
			{

			}
		}
	}	
}
void MEASER_2D_BubbleSort(MEASER_2D *pTargetList, int ListCount)
{
	int i,j;
	MEASER_2D SwapBuffer;
	for (i=0;i<ListCount-1;i++) 
	{
		for (j=1;j<ListCount-i;j++) 
		{
			long Dis1 = abs(pTargetList[j-1].RightMinValueY - pTargetList[j-1].LeftMinValueY);
			long Dis2 = abs(pTargetList[j].RightMinValueY - pTargetList[j].LeftMinValueY);
			if (Dis1 > Dis2) 
			{
				SwapBuffer = pTargetList[j-1];
				pTargetList[j-1] = pTargetList[j];
				pTargetList[j] = SwapBuffer;
			}
		}
	}
}
double dist_2d(double x1,  double y1, double x2, double y2)
{
	return sqrt((double)((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2)));
}
void dist_2PointCenter(int x1, int y1, int x2, int y2, int *cx, int *cy)
{
	int CBx = (abs(x1-x2)/2);
	int CBy = (abs(y1-y2)/2);
	if(x1>x2)
		*cx = x1-CBx;
	else
		*cx = x2-CBx;

	if(y1>y2)
		*cy = y1-CBy;
	else
		*cy = y2-CBy;

}
BOOL GetIntersectPoint(const CvPoint& AP1, const CvPoint& AP2, const CvPoint& BP1, const CvPoint& BP2, CvPoint* IP) 
{ 
	double t; double s; 
	double under = (BP2.y-BP1.y)*(AP2.x-AP1.x)-(BP2.x-BP1.x)*(AP2.y-AP1.y);
	if(under==0) 
		return false; 
	double _t = (BP2.x-BP1.x)*(AP1.y-BP1.y) - (BP2.y-BP1.y)*(AP1.x-BP1.x); 
	double _s = (AP2.x-AP1.x)*(AP1.y-BP1.y) - (AP2.y-AP1.y)*(AP1.x-BP1.x); 
	t = _t/under; 
	s = _s/under; 
	if(t<0.0 || t>1.0 || s<0.0 || s>1.0) 
		return false; 
	if(_t==0 && _s==0) return false; 
	IP->x = (LONG)(AP1.x + t * (double)(AP2.x-AP1.x));
	IP->y = (LONG)(AP1.y + t * (double)(AP2.y-AP1.y)); 
	return true; 
}
double Area3Point(CvPoint A, CvPoint B, CvPoint C)
{
	return abs(((B.x - A.x)*(C.y-A.y))-((B.y-A.y)*(C.x-A.x)))/2.0;
}
//////////////////////////////////////////////////////////////////////////
#define PI 3.14159265
#define SQR(x) ((x) * (x))
double dist_2dPoint(CvPoint a, CvPoint b)
{
	return sqrt((double)((a.x-b.x)*(a.x-b.x))+((a.y-b.y)*(a.y-b.y)));
}
double get_inner_angle(CvPoint o, CvPoint a, CvPoint b)
{
	double radian = acos(((double)(SQR(dist_2dPoint(o, a)) + SQR(dist_2dPoint(o, b)) - SQR(dist_2dPoint(a,b))))/(2 * dist_2dPoint(o, a) * (dist_2dPoint(o, b))));  

	return (radian*180)/PI;
} 



/*
*	Module Name		:	QCMDCtrlList
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

ProcMsgQ::ProcMsgQ()
{
	m_HeadNode.m_pFrontNode		= NULL;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= NULL;

	m_CntNode					= 0;
	InitializeCriticalSection(&m_CrtSection);
}
ProcMsgQ::~ProcMsgQ()
{
	EnterCriticalSection(&m_CrtSection);
	//QClean();
	{
		DataNodeObj * pDeleteNode = NULL;

		if(m_HeadNode.m_pNextNode != &m_TailNode)
		{
			pDeleteNode =  m_HeadNode.m_pNextNode;

			DataNodeObj * pNextNodeBuf = NULL;
			while (pDeleteNode != &m_TailNode)
			{

				pNextNodeBuf = pDeleteNode->m_pNextNode;

				delete pDeleteNode;

				pDeleteNode = pNextNodeBuf;
				pNextNodeBuf = NULL;
			}
			m_HeadNode.m_pFrontNode		= NULL;
			m_HeadNode.m_pNextNode		= &m_TailNode;

			m_TailNode.m_pFrontNode		= &m_HeadNode;
			m_TailNode.m_pNextNode		= NULL;
		}
	}
	LeaveCriticalSection(&m_CrtSection);

	DeleteCriticalSection(&m_CrtSection);
}
double	ProcMsgQ::QGetAvrHight()
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pTargetNode = NULL;
	double RetValue =0.0;
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return -1.0;
	}
	pTargetNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = NULL;
	while (pTargetNode != &m_TailNode)
	{
		pNextNodeBuf = pTargetNode->m_pNextNode;


		RetValue+=pTargetNode->InpectObj.MaxValueY;

		pTargetNode = pNextNodeBuf;
		pNextNodeBuf = NULL;
	}
	RetValue=RetValue/m_CntNode;

	LeaveCriticalSection(&m_CrtSection);
	return RetValue;
}
double	ProcMsgQ::QGetMinHight()
{
	double RetValue =QGetAvrHight();
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pTargetNode = NULL;
	
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return -1.0;
	}
	pTargetNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = NULL;
	while (pTargetNode != &m_TailNode)
	{
		pNextNodeBuf = pTargetNode->m_pNextNode;

		if(RetValue > pTargetNode->InpectObj.MaxValueY)
			RetValue= pTargetNode->InpectObj.MaxValueY;

		pTargetNode = pNextNodeBuf;
		pNextNodeBuf = NULL;
	}
	LeaveCriticalSection(&m_CrtSection);
	return RetValue;
}

int ProcMsgQ::QSortByCenterX(double PixelPerNano)
{
	EnterCriticalSection(&m_CrtSection);
	MEASER_2D SwapBuffer;
	DataNodeObj * pTargetNode		= NULL;
	DataNodeObj * pFindNode			= NULL;
	DataNodeObj * pFindSwapNode		= NULL;
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return 0;
	}
	pTargetNode = m_HeadNode.m_pNextNode;

	int NowCenterX = 0;
	int NowCenterY = 0;

	int FindCenterX = 0;
	int FindCenterY = 0;

	//Sort By Center Pos X
	while(pTargetNode != &m_TailNode)
	{
		pFindNode= pTargetNode->m_pNextNode;
		dist_2PointCenter(	(int)pTargetNode->InpectObj.LeftMinValueX,	(int)(pTargetNode->InpectObj.LeftMinValueY/PixelPerNano),
							(int)pTargetNode->InpectObj.RightMinValueX,	(int)(pTargetNode->InpectObj.RightMinValueY/PixelPerNano),
							&NowCenterX, &NowCenterY);
		//pTargetNode->InpectObj.MaxValueX = NowCenterX;
		//pTargetNode->InpectObj.MaxValueY = NowCenterY;

		pFindSwapNode = NULL;
		while(pFindNode != &m_TailNode)
		{
			dist_2PointCenter(	(int)pFindNode->InpectObj.LeftMinValueX,	(int)(pFindNode->InpectObj.LeftMinValueY/PixelPerNano),
								(int)pFindNode->InpectObj.RightMinValueX,	(int)(pFindNode->InpectObj.RightMinValueY/PixelPerNano),
								&FindCenterX, &FindCenterY);
			if(NowCenterX<FindCenterX)
			{
				NowCenterX=FindCenterX;
				pFindSwapNode = pFindNode;
			}
			pFindNode= pFindNode->m_pNextNode;
		}

		if(pFindSwapNode != NULL)
		{
			SwapBuffer = pFindSwapNode->InpectObj;
			pFindSwapNode->InpectObj = pTargetNode->InpectObj;
			pTargetNode->InpectObj = SwapBuffer;
		}
		pTargetNode = pTargetNode->m_pNextNode;
	}
	//Div Center By Distance

	pTargetNode = m_HeadNode.m_pNextNode;
	int IdexNumber = 1;

	int IndexCtX=0;
	int IndexCtY=0;
	int IndexCtX2=0;
	int IndexCtY2=0;
	double Dis2Point = 0;
	double Dis2Point2= 0;
	while(pTargetNode != &m_TailNode)
	{
		pFindNode= pTargetNode->m_pNextNode;

		pTargetNode->InpectObj.LeftDnValueX = IdexNumber;//임시로 이필드를 Index버퍼로 쓰자.
		if(pFindNode != &m_TailNode)
		{
			Dis2Point = dist_2d((int)pTargetNode->InpectObj.LeftMinValueX,	(int)(pTargetNode->InpectObj.LeftMinValueY/PixelPerNano),
								(int)pTargetNode->InpectObj.RightMinValueX,	(int)(pTargetNode->InpectObj.RightMinValueY/PixelPerNano))/2;

			dist_2PointCenter(	(int)pTargetNode->InpectObj.LeftMinValueX,	(int)(pTargetNode->InpectObj.LeftMinValueY/PixelPerNano),
								(int)pTargetNode->InpectObj.RightMinValueX,	(int)(pTargetNode->InpectObj.RightMinValueY/PixelPerNano),
								&IndexCtX, &IndexCtY);


			dist_2PointCenter(	(int)pFindNode->InpectObj.LeftMinValueX,	(int)(pFindNode->InpectObj.LeftMinValueY/PixelPerNano),
								(int)pFindNode->InpectObj.RightMinValueX,	(int)(pFindNode->InpectObj.RightMinValueY/PixelPerNano),
								&IndexCtX2, &IndexCtY2);

			Dis2Point2 = dist_2d(IndexCtX,	IndexCtY, IndexCtX2,	IndexCtY2);


			//Dis2Point2 = dist_2d(pTargetNode->InpectObj.MaxValueX,	pTargetNode->InpectObj.MaxValueY, pFindNode->InpectObj.MaxValueX,	pFindNode->InpectObj.MaxValueY);

			if(Dis2Point<Dis2Point2)//다른 속성의 선분 경계 변경.
				IdexNumber++;

		}
		pTargetNode = pTargetNode->m_pNextNode;
	}
	LeaveCriticalSection(&m_CrtSection);
	return IdexNumber;
}
double ProcMsgQ::QGetAvrWidth(double PixelPerNano)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pTargetNode = NULL;
	double RetValue =0.0;
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return -1.0;
	}
	pTargetNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = NULL;
	while (pTargetNode != &m_TailNode)
	{
		pNextNodeBuf = pTargetNode->m_pNextNode;

		RetValue+=dist_2d(	pTargetNode->InpectObj.LeftMinValueX*PixelPerNano, (pTargetNode->InpectObj.LeftMinValueY), 
									pTargetNode->InpectObj.RightMinValueX*PixelPerNano, (pTargetNode->InpectObj.RightMinValueY))/1000.0;

		pTargetNode = pNextNodeBuf;
		pNextNodeBuf = NULL;
	}
	RetValue=RetValue/m_CntNode;
	RetValue*=1000.0;

	LeaveCriticalSection(&m_CrtSection);
	return RetValue;
}
int ProcMsgQ::QGetAvrWidth_WidthPixel()
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pTargetNode = NULL;
	int RetValue = 0;
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return -1;
	}
	pTargetNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = NULL;
	while (pTargetNode != &m_TailNode)
	{
		pNextNodeBuf = pTargetNode->m_pNextNode;

		RetValue+=abs(pTargetNode->InpectObj.LeftMinValueX - pTargetNode->InpectObj.RightMinValueX);

		pTargetNode = pNextNodeBuf;
		pNextNodeBuf = NULL;
	}
	RetValue=RetValue/m_CntNode;

	LeaveCriticalSection(&m_CrtSection);
	return RetValue;
}
void ProcMsgQ::QRemoveAvrBigW(double PixelPerNano)
{
	double AVR_W = QGetAvrWidth(PixelPerNano);


	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pTargetNode = NULL;
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pTargetNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = NULL;
	while (pTargetNode != &m_TailNode)
	{
		pNextNodeBuf = pTargetNode->m_pNextNode;
		double DetectDis = dist_2d(	pTargetNode->InpectObj.LeftMinValueX*PixelPerNano, (pTargetNode->InpectObj.LeftMinValueY), 
			pTargetNode->InpectObj.RightMinValueX*PixelPerNano, (pTargetNode->InpectObj.RightMinValueY));
		if(AVR_W*1.2 < DetectDis)
		{

			pTargetNode->m_pNextNode->m_pFrontNode =  pTargetNode->m_pFrontNode;
			pTargetNode->m_pFrontNode->m_pNextNode = pTargetNode->m_pNextNode;

			delete pTargetNode;
			pTargetNode =  NULL;
			m_CntNode--;
		}

		pTargetNode = pNextNodeBuf;
		pNextNodeBuf = NULL;
	}
	LeaveCriticalSection(&m_CrtSection);
}
void ProcMsgQ::QPutNode(MEASER_2D	*pNewObj)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj *pNewNode = new DataNodeObj;

	pNewNode->InpectObj = *pNewObj;//Data저장.

	pNewNode->m_pFrontNode					= &m_HeadNode;
	pNewNode->m_pNextNode					= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode					= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
}

MEASER_2D	 ProcMsgQ::QGetNode()
{
	EnterCriticalSection(&m_CrtSection);
	MEASER_2D	retData;

	DataNodeObj *pReturnNode;
	pReturnNode = m_TailNode.m_pFrontNode;

	if(pReturnNode ==  &m_HeadNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return retData;
	}

	m_TailNode.m_pFrontNode = pReturnNode->m_pFrontNode;
	(pReturnNode->m_pFrontNode)->m_pNextNode = &m_TailNode;

	retData = pReturnNode->InpectObj;

	delete pReturnNode;
	m_CntNode --;

	LeaveCriticalSection(&m_CrtSection);
	return retData;
}

void ProcMsgQ::QClean(void)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pDeleteNode = NULL;

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pDeleteNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = NULL;
	while (pDeleteNode != &m_TailNode)
	{
		pNextNodeBuf = pDeleteNode->m_pNextNode;
		delete pDeleteNode;

		pDeleteNode = pNextNodeBuf;
		pNextNodeBuf = NULL;
	}

	m_HeadNode.m_pFrontNode		= NULL;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= NULL;

	m_CntNode = 0;
	LeaveCriticalSection(&m_CrtSection);
}

// CMeasureProc 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMeasureProc, CDialog)

CMeasureProc::CMeasureProc(CWnd* pParent /*=NULL*/)
	: CDialog(CMeasureProc::IDD, pParent)
{
	m_pNewData_3D	= NULL;
	m_pAxis_V_CalcImg = NULL;
	m_pAxis_H_CalcImg = NULL;

	m_PixelPerNano =0.0;

	m_pSResultViewImg = NULL;
	m_pDResultViewImg = NULL;

	m_MasureIndex	  = 0;
	m_MeasurDotAngle = 0.0f;

	m_showResult = FALSE;

	m_ScanTime = 0;

	m_bMeasureEditResult = FALSE;
	m_pResultBuf	= NULL;

	m_StripMagnet = FALSE;

	m_pBaseDepth = NULL;
}

CMeasureProc::~CMeasureProc()
{
	if(m_pAxis_V_CalcImg != NULL)
		cvReleaseImage(&m_pAxis_V_CalcImg);
	m_pAxis_V_CalcImg = NULL;

	if(m_pAxis_H_CalcImg != NULL)
		cvReleaseImage(&m_pAxis_H_CalcImg);
	m_pAxis_H_CalcImg = NULL;


	if(m_pSResultViewImg != NULL)
		cvReleaseImage(&m_pSResultViewImg);
	m_pSResultViewImg = NULL;

	if(m_pDResultViewImg != NULL)
		cvReleaseImage(&m_pDResultViewImg);
	m_pDResultViewImg = NULL;

	m_pBaseDepth = NULL;
}

void CMeasureProc::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SBAR_AXIS_X, m_SBAxisX);
	DDX_Control(pDX, IDC_SBAR_AXIS_Y, m_SBAxisY);
	DDX_Control(pDX, IDC_SL_H_1, m_slider_H1);
	DDX_Control(pDX, IDC_SL_H_2, m_slider_H2);
	DDX_Control(pDX, IDC_SL_H_3, m_slider_H3);
	DDX_Control(pDX, IDC_SL_H_4, m_slider_H4);
	DDX_Control(pDX, IDC_SL_H_5, m_slider_H5);
	DDX_Control(pDX, IDC_SL_H_6, m_slider_H6);

	DDX_Control(pDX, IDC_SL_V_1, m_slider_V1);
	DDX_Control(pDX, IDC_SL_V_2, m_slider_V2);
	DDX_Control(pDX, IDC_SL_V_3, m_slider_V3);
	DDX_Control(pDX, IDC_SL_V_4, m_slider_V4);
	DDX_Control(pDX, IDC_SL_V_5, m_slider_V5);
	DDX_Control(pDX, IDC_SL_V_6, m_slider_V6);
	DDX_Control(pDX, IDC_BT_RESULT, m_btResultView);
	DDX_Control(pDX, IDC_BT_CANCEL, m_btCancel);
	DDX_Control(pDX, IDC_BT_COMPLETE, m_btComplete);
}


BEGIN_MESSAGE_MAP(CMeasureProc, CDialog)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_PAINT()

	ON_WM_SIZE()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_BT_DEFAULT_POS, &CMeasureProc::OnBnClickedBtDefaultPos)
	ON_BN_CLICKED(IDC_BT_COMPLETE, &CMeasureProc::OnBnClickedBtComplete)
	ON_BN_CLICKED(IDC_BT_CANCEL, &CMeasureProc::OnBnClickedBtCancel)
	ON_BN_CLICKED(IDC_BT_RESULT, &CMeasureProc::OnBnClickedBtResult)
END_MESSAGE_MAP()


// CMeasureProc 메시지 처리기입니다.
BOOL CMeasureProc::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_MaskType	= MASK_TYPE_STRIPE;

	GetDlgItem(IDC_ST_DETECT_VIEW_STICK)->GetClientRect(&m_RectViewStick);
	m_HDcViewStick	=  GetDlgItem(IDC_ST_DETECT_VIEW_STICK)->GetDC()->m_hAttribDC;
	//GetDlgItem(IDC_ST_DETECT_VIEW_STICK)->SetWindowPos(NULL, 0, 0, 1024,m_RectViewStick.Height(),SWP_NOMOVE );
	GetDlgItem(IDC_ST_DETECT_VIEW_STICK)->GetClientRect(&m_RectViewStick);

	GetDlgItem(IDC_ST_DETECT_VIEW_DOT)->GetClientRect(&m_RectViewDot);
	m_HDcViewDot	=  GetDlgItem(IDC_ST_DETECT_VIEW_DOT)->GetDC()->m_hAttribDC;
	//GetDlgItem(IDC_ST_DETECT_VIEW_DOT)->SetWindowPos(NULL, 0, 0, 1024,m_RectViewDot.Height(),SWP_NOMOVE );
	GetDlgItem(IDC_ST_DETECT_VIEW_DOT)->GetClientRect(&m_RectViewDot);

	GetDlgItem(IDC_ST_3DVIEW)->GetClientRect(&m_Rect3DViewArea);
	m_HDC3DView	=  GetDlgItem(IDC_ST_3DVIEW)->GetDC()->m_hAttribDC;
	m_HDCResultView = m_HDC3DView;
	m_RectResultViewArea = m_Rect3DViewArea;

	//GetDlgItem(IDC_ST_RESULT_VIEW)->GetClientRect(&m_RectResultViewArea);
	//m_HDCResultView	=  GetDlgItem(IDC_ST_RESULT_VIEW)->GetDC()->m_hAttribDC;

	m_pSResultViewImg = cvLoadImage(RESULT_VIEW_STICK_IMG);
	m_pDResultViewImg = cvLoadImage(RESULT_VIEW_DOT_IMG);

	m_MasureIndex = 0;
	memset(m_strSavePath, 0x00, MAX_PATH);

	//////////////////////////////////////////////////////////////////////////
	m_3DViewObj.InitOpenGL(GetDlgItem(IDC_ST_3DVIEW)->m_hWnd, NULL);//OpenGL Init.
	m_LButtonDown = FALSE;
	m_RButtonDown = FALSE;
	m_MaserDefaultPos.x = 0;
	m_MaserDefaultPos.y = 0;
	CRect WinInitRect;
	GetDlgItem(IDC_ST_3DVIEW)->GetClientRect(&WinInitRect);
	m_3DViewObj.ChangeViewSize(WinInitRect.Width(), WinInitRect.Height());
	//////////////////////////////////////////////////////////////////////////
	//m_slider_H1.SetSelectionColor(RGB(0,255,255));
	//m_slider_H1.SetChannelColor(RGB(0,255,0));
	m_slider_H1.SetThumbColor(RGB(255,0,0));
	m_slider_H2.SetThumbColor(RGB(255,0,0));
	m_slider_H3.SetThumbColor(RGB(0,255,0));
	m_slider_H4.SetThumbColor(RGB(0,255,0));
	m_slider_H5.SetThumbColor(RGB(0,0,255));
	m_slider_H6.SetThumbColor(RGB(0,0,255));

	m_slider_V1.SetThumbColor(RGB(255,0,0));
	m_slider_V2.SetThumbColor(RGB(255,0,0));
	m_slider_V3.SetThumbColor(RGB(0,255,0));
	m_slider_V4.SetThumbColor(RGB(0,255,0));
	m_slider_V5.SetThumbColor(RGB(0,0,255));
	m_slider_V6.SetThumbColor(RGB(0,0,255));


	m_slider_H1.SetRange(0, 1023);
	m_slider_H2.SetRange(0, 1023);
	m_slider_H3.SetRange(0, 1023);
	m_slider_H4.SetRange(0, 1023);
	m_slider_H5.SetRange(0, 1023);
	m_slider_H6.SetRange(0, 1023);

	m_slider_V1.SetRange(0, 1023);
	m_slider_V2.SetRange(0, 1023);
	m_slider_V3.SetRange(0, 1023);
	m_slider_V4.SetRange(0, 1023);
	m_slider_V5.SetRange(0, 1023);
	m_slider_V6.SetRange(0, 1023);

	m_slider_H1.SetTicFreq(1);
	m_slider_H2.SetTicFreq(1);
	m_slider_H3.SetTicFreq(1);
	m_slider_H4.SetTicFreq(1);
	m_slider_H5.SetTicFreq(1);
	m_slider_H6.SetTicFreq(1);

	m_slider_V1.SetTicFreq(1);
	m_slider_V2.SetTicFreq(1);
	m_slider_V3.SetTicFreq(1);
	m_slider_V4.SetTicFreq(1);
	m_slider_V5.SetTicFreq(1);
	m_slider_V6.SetTicFreq(1);

	m_slider_H1.SetPageSize(1);
	m_slider_H2.SetPageSize(1);
	m_slider_H3.SetPageSize(1);
	m_slider_H4.SetPageSize(1);
	m_slider_H5.SetPageSize(1);
	m_slider_H6.SetPageSize(1);

	m_slider_V1.SetPageSize(1);
	m_slider_V2.SetPageSize(1);
	m_slider_V3.SetPageSize(1);
	m_slider_V4.SetPageSize(1);
	m_slider_V5.SetPageSize(1);
	m_slider_V6.SetPageSize(1);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	tButtonStyle tStyle;
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_tColorFace.m_tClicked		= RGB(0x50, 0x50, 0x50);
	//tStyle.m_tColorBorder.m_tClicked	= RGB(0xFF, 0xFF, 0xFF);
	m_tButtonStyle.SetButtonStyle(&tStyle);




	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btResultView.GetTextColor(&tColor);
		m_btResultView.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,31,"Times New Roman");
			tFont.lfHeight =23;
		}

		m_btResultView.SetRoundButtonStyle(&m_tButtonStyle);
		m_btResultView.SetTextColor(&tColor);
		m_btResultView.SetCheckButton(false);
		m_btResultView.SetFont(&tFont);

		m_btCancel.SetRoundButtonStyle(&m_tButtonStyle);
		m_btCancel.SetTextColor(&tColor);
		m_btCancel.SetCheckButton(false);
		m_btCancel.SetFont(&tFont);

		m_btComplete.SetRoundButtonStyle(&m_tButtonStyle);
		m_btComplete.SetTextColor(&tColor);
		m_btComplete.SetCheckButton(false);
		m_btComplete.SetFont(&tFont);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CMeasureProc::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}


void CMeasureProc::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(m_pAxis_V_CalcImg != NULL)
		cvReleaseImage(&m_pAxis_V_CalcImg);

	if(m_pAxis_H_CalcImg != NULL)
		cvReleaseImage(&m_pAxis_H_CalcImg);

	if(m_pSResultViewImg != NULL)
		cvReleaseImage(&m_pSResultViewImg);

	if(m_pDResultViewImg != NULL)
		cvReleaseImage(&m_pDResultViewImg);

	CDialog::PostNcDestroy();
	delete this;
}

HBRUSH CMeasureProc::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	//switch(DLG_ID_Number)
	//{
	//case IDC_SL_H_1:
	//	{
	//		pDC->SetBkColor(RGB(255, 255, 255));
	//		return (HBRUSH)GetStockObject(BLACK_BRUSH);
	//	}break;

	//}
	return (HBRUSH)GetStockObject(BLACK_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

int CMeasureProc::fn_SetMeasureValue_Strip( MEASER_2D *pFindMeasurObj)
{
	if(m_StripMagnet == TRUE)
	{
		fn_SetMeasureValue_MagnetStrip(pFindMeasurObj);
		return 0;

	}
	//1번 거리
	m_pResultBuf->RIB_WIDTH = (int)dist_2d(	pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftMinValueY), 
		pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightMinValueY));

	if(m_pResultBuf->RIB_WIDTH<0)
		m_pResultBuf->RIB_WIDTH =0;
	//2번 거리
	m_pResultBuf->RIB_WIDTH_C = (int)dist_2d(	pFindMeasurObj->LeftDnValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY), 
		pFindMeasurObj->RightDnValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY));
	if(m_pResultBuf->RIB_WIDTH_C<0)
		m_pResultBuf->RIB_WIDTH_C =0;
	//3번 거리
	m_pResultBuf->RIB_WIDTH_L = (int)dist_2d(	pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY), 
		pFindMeasurObj->LeftDnValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY));

	//4번 거리
	m_pResultBuf->RIB_WIDTH_R = (int)dist_2d(	pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY), 
		pFindMeasurObj->RightDnValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY));
	if(m_pResultBuf->RIB_WIDTH_R<0)
		m_pResultBuf->RIB_WIDTH_R =0;
	//5번 거리
	m_pResultBuf->RIB_HEIGHT_L = (int)dist_2d(	pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftMinValueY), 
		pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY));
	if(m_pResultBuf->RIB_HEIGHT_L<0)
		m_pResultBuf->RIB_HEIGHT_L =0;
	//6번 거리
	m_pResultBuf->RIB_HEIGHT_E_L = (m_pNewData_3D->m_TargetThicknessNano) - m_pResultBuf->RIB_HEIGHT_L;
	if(m_pResultBuf->RIB_HEIGHT_E_L<0)
		m_pResultBuf->RIB_HEIGHT_E_L =0;
	//7번 거리
	m_pResultBuf->RIB_HEIGHT_R = (int)dist_2d(	pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightMinValueY), 
		pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY));
	if(m_pResultBuf->RIB_HEIGHT_R<0)
		m_pResultBuf->RIB_HEIGHT_R =0;
	//8번 거리
	m_pResultBuf->RIB_HEIGHT_E_R = (m_pNewData_3D->m_TargetThicknessNano)- m_pResultBuf->RIB_HEIGHT_R;
	if(m_pResultBuf->RIB_HEIGHT_E_R<0)
		m_pResultBuf->RIB_HEIGHT_E_R =0;
	//9번 거리
	CvPoint  StartPt, CenterPt, EndPt;
	StartPt.x =(long) (pFindMeasurObj->LeftMinValueX*m_PixelPerNano)+ 1000;//+1um
	StartPt.y = pFindMeasurObj->LeftMinValueY;

	CenterPt.x =(long) (pFindMeasurObj->LeftMinValueX*m_PixelPerNano);
	CenterPt.y = pFindMeasurObj->LeftMinValueY;

	EndPt.x = (long) (pFindMeasurObj->LeftDnValueX*m_PixelPerNano);
	EndPt.y = (long) (pFindMeasurObj->LeftDnValueY);

	StartPt.x  /=100;// =(long) (pFindMeasurObj->LeftMinValueX*m_PixelPerNano)+ 1000;//+1um
	StartPt.y /=100;// pFindMeasurObj->LeftMinValueY;

	CenterPt.x /=100;//(long) (pFindMeasurObj->LeftMinValueX*m_PixelPerNano);
	CenterPt.y /=100;// pFindMeasurObj->LeftMinValueY;

	EndPt.x /=100;// (long) (pFindMeasurObj->LeftDnValueX*m_PixelPerNano);
	EndPt.y /=100;// (long) (pFindMeasurObj->LeftDnValueY);

	m_pResultBuf->RIB_ANGLE_L = (float)get_inner_angle(CenterPt, StartPt, EndPt);
	if(m_pResultBuf->RIB_ANGLE_L<0.0f)
		m_pResultBuf->RIB_ANGLE_L =0.0f;
	StartPt.x =(long) (pFindMeasurObj->RightMinValueX*m_PixelPerNano)- 1000;//-1um
	StartPt.y = pFindMeasurObj->RightMinValueY;

	CenterPt.x =(long) (pFindMeasurObj->RightMinValueX*m_PixelPerNano);
	CenterPt.y = pFindMeasurObj->RightMinValueY;

	EndPt.x = (long) (pFindMeasurObj->RightDnValueX*m_PixelPerNano);
	EndPt.y = (pFindMeasurObj->RightDnValueY);


	StartPt.x /=100;//(long) (pFindMeasurObj->RightMinValueX*m_PixelPerNano)- 1000;//-1um
	StartPt.y /=100;// pFindMeasurObj->RightMinValueY;

	CenterPt.x /=100;//(long) (pFindMeasurObj->RightMinValueX*m_PixelPerNano);
	CenterPt.y /=100;// pFindMeasurObj->RightMinValueY;

	EndPt.x /=100;// (long) (pFindMeasurObj->RightDnValueX*m_PixelPerNano);
	EndPt.y /=100;// (pFindMeasurObj->RightDnValueY);
	//10번 거리
	m_pResultBuf->RIB_ANGLE_R = (float)get_inner_angle(CenterPt, StartPt, EndPt);
	if(m_pResultBuf->RIB_ANGLE_R<0.0f)
		m_pResultBuf->RIB_ANGLE_R =0.0f;
	//11번 거리
	m_pResultBuf->RIB_HEIGHT = (m_pNewData_3D->m_TargetThicknessNano);
	if(m_pResultBuf->RIB_HEIGHT<0)
		m_pResultBuf->RIB_HEIGHT =0;

	return 0;
}

int CMeasureProc::fn_SetMeasureValue_MagnetStrip( MEASER_2D *pFindMeasurObj)
{
	//1번 거리
	m_pResultBuf->RIB_WIDTH = (int)(dist_2d(	pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftMinValueY), 
		pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightMinValueY))*(m_MeasurDotAngle == 0?1:1.4142));
	if(m_pResultBuf->RIB_WIDTH<0)
		m_pResultBuf->RIB_WIDTH =0;
	//2번 거리
	m_pResultBuf->RIB_WIDTH_C = (int)(dist_2d(	pFindMeasurObj->LeftDnValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY), 
		pFindMeasurObj->RightDnValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
	if(m_pResultBuf->RIB_WIDTH_C<0)
		m_pResultBuf->RIB_WIDTH_C =0;
	//3번 거리
	m_pResultBuf->RIB_WIDTH_L = (int)(dist_2d(	pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY), 
		pFindMeasurObj->LeftDnValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
	if(m_pResultBuf->RIB_WIDTH_L<0)
		m_pResultBuf->RIB_WIDTH_L =0;
	//4번 거리
	m_pResultBuf->RIB_WIDTH_R = (int)(dist_2d(	pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY), 
		pFindMeasurObj->RightDnValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
	if(m_pResultBuf->RIB_WIDTH_R<0)
		m_pResultBuf->RIB_WIDTH_R =0;
	//5번 거리
	//m_pResultBuf->RIB_HEIGHT_L = (int)dist_2d(	pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftMinValueY), 
	//	pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY));
	m_pResultBuf->RIB_HEIGHT_L = (int)dist_2d(	pFindMeasurObj->LeftAngleValueX*m_PixelPerNano, (pFindMeasurObj->LeftAngleValueY), 
		pFindMeasurObj->LeftAngleValueX*m_PixelPerNano, (pFindMeasurObj->LeftDnValueY));
	if(m_pResultBuf->RIB_HEIGHT_L<0)
		m_pResultBuf->RIB_HEIGHT_L =0;
	//6번 거리
	//m_pResultBuf->RIB_HEIGHT_E_L = (pNewData_3D->m_TargetThicknessNano)-DisplayData[4];
	m_pResultBuf->RIB_HEIGHT_E_L = (int)(dist_2d(	pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftMinValueY), 
		pFindMeasurObj->LeftMinValueX*m_PixelPerNano, (pFindMeasurObj->LeftAngleValueY)));
	if(m_pResultBuf->RIB_HEIGHT_E_L<0)
		m_pResultBuf->RIB_HEIGHT_E_L =0;
	//7번 거리
	//m_pResultBuf->RIB_HEIGHT_R = (int)dist_2d(	pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightMinValueY), 
	//	pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY));
	m_pResultBuf->RIB_HEIGHT_R  = (int)dist_2d(	pFindMeasurObj->RightAngleValueX*m_PixelPerNano, (pFindMeasurObj->RightAngleValueY), 
		pFindMeasurObj->RightAngleValueX*m_PixelPerNano, (pFindMeasurObj->RightDnValueY));
	if(m_pResultBuf->RIB_HEIGHT_R<0)
		m_pResultBuf->RIB_HEIGHT_R =0;
	//8번 거리
	//m_pResultBuf->RIB_HEIGHT_E_R = (pNewData_3D->m_TargetThicknessNano)-DisplayData[6];
	m_pResultBuf->RIB_HEIGHT_E_R  = (int)(dist_2d(	pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightMinValueY), 
		pFindMeasurObj->RightMinValueX*m_PixelPerNano, (pFindMeasurObj->RightAngleValueY)));
	if(m_pResultBuf->RIB_HEIGHT_E_R<0)
		m_pResultBuf->RIB_HEIGHT_E_R =0;
	//9번 거리
	CvPoint  StartPt, CenterPt, EndPt;
	//StartPt.x =(long) (pFindMeasurObj->LeftMinValueX*m_PixelPerNano)+ 1000;//+1um
	//StartPt.y = pFindMeasurObj->LeftMinValueY;

	//CenterPt.x =(long) (pFindMeasurObj->LeftMinValueX*m_PixelPerNano);
	//CenterPt.y = pFindMeasurObj->LeftMinValueY;
	StartPt.x =(long) (pFindMeasurObj->LeftAngleValueX*m_PixelPerNano)+ 1000;//+1um
	StartPt.y = pFindMeasurObj->LeftAngleValueY;

	CenterPt.x =(long) (pFindMeasurObj->LeftAngleValueX*m_PixelPerNano);
	CenterPt.y = pFindMeasurObj->LeftAngleValueY;

	EndPt.x = (long) (pFindMeasurObj->LeftDnValueX*m_PixelPerNano);
	EndPt.y = (long) (pFindMeasurObj->LeftDnValueY);

	StartPt.x /=100;//(long) (pFindMeasurObj->LeftAngleValueX*m_PixelPerNano)+ 1000;//+1um
	StartPt.y /=100;// pFindMeasurObj->LeftAngleValueY;

	CenterPt.x /=100;//(long) (pFindMeasurObj->LeftAngleValueX*m_PixelPerNano);
	CenterPt.y /=100;// pFindMeasurObj->LeftAngleValueY;

	EndPt.x /=100;// (long) (pFindMeasurObj->LeftDnValueX*m_PixelPerNano);
	EndPt.y /=100;// (long) (pFindMeasurObj->LeftDnValueY);

	m_pResultBuf->RIB_ANGLE_L = (float)get_inner_angle(CenterPt, StartPt, EndPt);
	if(m_pResultBuf->RIB_ANGLE_L<0.0f)
		m_pResultBuf->RIB_ANGLE_L =0.0f;
	//StartPt.x =(long) (pFindMeasurObj->RightMinValueX*m_PixelPerNano)- 1000;//-1um
	//StartPt.y = pFindMeasurObj->RightMinValueY;

	//CenterPt.x =(long) (pFindMeasurObj->RightMinValueX*m_PixelPerNano);
	//CenterPt.y = pFindMeasurObj->RightMinValueY;
	StartPt.x =(long) (pFindMeasurObj->RightAngleValueX*m_PixelPerNano)- 1000;//-1um
	StartPt.y = pFindMeasurObj->RightAngleValueY;

	CenterPt.x =(long) (pFindMeasurObj->RightAngleValueX*m_PixelPerNano);
	CenterPt.y = pFindMeasurObj->RightAngleValueY;

	EndPt.x = (long) (pFindMeasurObj->RightDnValueX*m_PixelPerNano);
	EndPt.y = (pFindMeasurObj->RightDnValueY);

	StartPt.x /=100;//(long) (pFindMeasurObj->RightAngleValueX*m_PixelPerNano)- 1000;//-1um
	StartPt.y /=100;// pFindMeasurObj->RightAngleValueY;

	CenterPt.x /=100;//(long) (pFindMeasurObj->RightAngleValueX*m_PixelPerNano);
	CenterPt.y /=100;// pFindMeasurObj->RightAngleValueY;

	EndPt.x /=100;// (long) (pFindMeasurObj->RightDnValueX*m_PixelPerNano);
	EndPt.y /=100;// (pFindMeasurObj->RightDnValueY);
	//10번 거리
	m_pResultBuf->RIB_ANGLE_R = (float)get_inner_angle(CenterPt, StartPt, EndPt);
	if(m_pResultBuf->RIB_ANGLE_R<0.0f)
		m_pResultBuf->RIB_ANGLE_R =0.0f;
	//11번 거리
	//m_pResultBuf->RIB_HEIGHT = (pNewData_3D->m_TargetThicknessNano);
	int HBuf1= m_pResultBuf->RIB_HEIGHT_L + m_pResultBuf->RIB_HEIGHT_E_L;
	int HBuf2= m_pResultBuf->RIB_HEIGHT_R + m_pResultBuf->RIB_HEIGHT_E_R;
	m_pResultBuf->RIB_HEIGHT =HBuf1>HBuf2? HBuf1: HBuf2;
	if(m_pResultBuf->RIB_HEIGHT<0)
		m_pResultBuf->RIB_HEIGHT =0;

	return 0;
}
int CMeasureProc::fn_ProcStrip(DATA_3D_RAW *pNewData_3D, CvPoint MPoint)
{

	memset(m_DepthData_H, 0x00, 1024);
	memset(m_DepthData_V, 0x00, 1024);

	MEASER_2D CheckListAxisX;
	MEASER_2D CheckStrip;
	BOOL bDetectCenter = FALSE;

	bDetectCenter = TRUE;

	if(MPoint.x == -1 && MPoint.y == -1)
	{
		MPoint.x  = (1024/2);
		MPoint.y = (768/2);
		m_MaserDefaultPos = MPoint;
	}

	for(int YDataStep = 0; YDataStep<1024; YDataStep++)
	{
		if(YDataStep>m_pNewData_3D->m_Width)
			break;
		m_DepthData_H[YDataStep] = pNewData_3D->m_pDepthData[ YDataStep + (MPoint.y*m_pNewData_3D->m_Width)];
	}

	MeaserAreaStickType(MPoint.y, &CheckStrip);
	DrawProcLineStrip(m_pAxis_H_CalcImg, &CheckStrip);
	fn_SetMeasureValue_Strip( &CheckStrip);

	m_slider_H1.SetPos(CheckStrip.LeftMinValueX);
	m_slider_H2.SetPos(CheckStrip.RightMinValueX);
	m_slider_H3.SetPos(CheckStrip.LeftAngleValueX);
	m_slider_H4.SetPos(CheckStrip.RightAngleValueX);
	m_slider_H5.SetPos(CheckStrip.LeftDnValueX);
	m_slider_H6.SetPos(CheckStrip.RightDnValueX);

	m_MeaserH_Edit = CheckStrip;


	m_3DViewObj.m_MeaserInfo.MPositionX = MPoint.x - (1024/2);
	m_3DViewObj.m_MeaserInfo.MPositionY = MPoint.y - (768/2);
	m_3DViewObj.RenderScreen();

	
	//m_SBAxisX.SetScrollPos(MPoint.x);
	m_SBAxisY.SetScrollPos(MPoint.y);

	return bDetectCenter?1:0;
}

int CMeasureProc::fn_SetMeasureValue_Dot( MEASER_2D *pFindMeasurObjX, MEASER_2D *pFindMeasurObjY)
{
	int BaseDepth = 0;
	if(m_pBaseDepth != NULL)
	{
		BaseDepth = (int)(((*m_pBaseDepth)*1000.0f) *(-1.0f));
	}
	//1번 거리
	m_pResultBuf->RIB_WIDTH = (int)(dist_2d(	pFindMeasurObjX->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjX->LeftMinValueY), 
		pFindMeasurObjX->RightMinValueX*m_PixelPerNano, (pFindMeasurObjX->RightMinValueY))*(m_MeasurDotAngle == 0?1:1.4142));
	if(m_pResultBuf->RIB_WIDTH<0)
		m_pResultBuf->RIB_WIDTH =0;
	//2번 거리
	m_pResultBuf->RIB_WIDTH_C = (int)(dist_2d(	pFindMeasurObjX->LeftDnValueX*m_PixelPerNano, (pFindMeasurObjX->LeftDnValueY), 
		pFindMeasurObjX->RightDnValueX*m_PixelPerNano, (pFindMeasurObjX->RightDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
	if(m_pResultBuf->RIB_WIDTH_C<0)
		m_pResultBuf->RIB_WIDTH_C =0;
	//3번 거리
	m_pResultBuf->RIB_WIDTH_L = (int)(dist_2d(	pFindMeasurObjX->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjX->LeftDnValueY), 
		pFindMeasurObjX->LeftDnValueX*m_PixelPerNano, (pFindMeasurObjX->LeftDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
	if(m_pResultBuf->RIB_WIDTH_L<0)
		m_pResultBuf->RIB_WIDTH_L =0;
	//4번 거리
	m_pResultBuf->RIB_WIDTH_R = (int)(dist_2d(	pFindMeasurObjX->RightMinValueX*m_PixelPerNano, (pFindMeasurObjX->RightDnValueY), 
		pFindMeasurObjX->RightDnValueX*m_PixelPerNano, (pFindMeasurObjX->RightDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
	if(m_pResultBuf->RIB_WIDTH_R<0)
		m_pResultBuf->RIB_WIDTH_R =0;
	//5번 거리
	//m_pResultBuf->RIB_HEIGHT_L = (int)dist_2d(	pFindMeasurObjX->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjX->LeftMinValueY), 
	//	pFindMeasurObjX->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjX->LeftDnValueY));
	m_pResultBuf->RIB_HEIGHT_L = (int)dist_2d(	pFindMeasurObjX->LeftAngleValueX*m_PixelPerNano, (pFindMeasurObjX->LeftAngleValueY), 
		pFindMeasurObjX->LeftAngleValueX*m_PixelPerNano, (pFindMeasurObjX->LeftDnValueY));
	if(m_pResultBuf->RIB_HEIGHT_L<0)
		m_pResultBuf->RIB_HEIGHT_L =0;
	//6번 거리
	//m_pResultBuf->RIB_HEIGHT_E_L = (pNewData_3D->m_TargetThicknessNano)-DisplayData[4];
	m_pResultBuf->RIB_HEIGHT_E_L = (int)(dist_2d(	pFindMeasurObjX->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjX->LeftMinValueY), 
		pFindMeasurObjX->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjX->LeftAngleValueY)));
	m_pResultBuf->RIB_HEIGHT_E_L  += BaseDepth;
	if(m_pResultBuf->RIB_HEIGHT_E_L<0)
		m_pResultBuf->RIB_HEIGHT_E_L =0;
	//7번 거리
	//m_pResultBuf->RIB_HEIGHT_R = (int)dist_2d(	pFindMeasurObjX->RightMinValueX*m_PixelPerNano, (pFindMeasurObjX->RightMinValueY), 
	//	pFindMeasurObjX->RightMinValueX*m_PixelPerNano, (pFindMeasurObjX->RightDnValueY));
	m_pResultBuf->RIB_HEIGHT_R  = (int)dist_2d(	pFindMeasurObjX->RightAngleValueX*m_PixelPerNano, (pFindMeasurObjX->RightAngleValueY), 
		pFindMeasurObjX->RightAngleValueX*m_PixelPerNano, (pFindMeasurObjX->RightDnValueY));
	if(m_pResultBuf->RIB_HEIGHT_R<0)
		m_pResultBuf->RIB_HEIGHT_R =0;
	//8번 거리
	//m_pResultBuf->RIB_HEIGHT_E_R = (pNewData_3D->m_TargetThicknessNano)-DisplayData[6];
	m_pResultBuf->RIB_HEIGHT_E_R  = (int)(dist_2d(	pFindMeasurObjX->RightMinValueX*m_PixelPerNano, (pFindMeasurObjX->RightMinValueY), 
		pFindMeasurObjX->RightMinValueX*m_PixelPerNano, (pFindMeasurObjX->RightAngleValueY)));

	m_pResultBuf->RIB_HEIGHT_E_R  += BaseDepth;
	if(m_pResultBuf->RIB_HEIGHT_E_R<0)
		m_pResultBuf->RIB_HEIGHT_E_R =0;
	//9번 거리
	CvPoint  StartPt, CenterPt, EndPt;
	//StartPt.x =(long) (pFindMeasurObjX->LeftMinValueX*m_PixelPerNano)+ 1000;//+1um
	//StartPt.y = pFindMeasurObjX->LeftMinValueY;

	//CenterPt.x =(long) (pFindMeasurObjX->LeftMinValueX*m_PixelPerNano);
	//CenterPt.y = pFindMeasurObjX->LeftMinValueY;
	StartPt.x =(long) (pFindMeasurObjX->LeftAngleValueX*m_PixelPerNano)+ 1000;//+1um
	StartPt.y = pFindMeasurObjX->LeftAngleValueY;

	CenterPt.x =(long) (pFindMeasurObjX->LeftAngleValueX*m_PixelPerNano);
	CenterPt.y = pFindMeasurObjX->LeftAngleValueY;

	EndPt.x = (long) (pFindMeasurObjX->LeftDnValueX*m_PixelPerNano);
	EndPt.y = (long) (pFindMeasurObjX->LeftDnValueY);

	StartPt.x /=100;//(long) (pFindMeasurObjX->LeftAngleValueX*m_PixelPerNano)+ 1000;//+1um
	StartPt.y /=100;// pFindMeasurObjX->LeftAngleValueY;

	CenterPt.x /=100;//(long) (pFindMeasurObjX->LeftAngleValueX*m_PixelPerNano);
	CenterPt.y /=100;// pFindMeasurObjX->LeftAngleValueY;

	EndPt.x /=100;// (long) (pFindMeasurObjX->LeftDnValueX*m_PixelPerNano);
	EndPt.y /=100;// (long) (pFindMeasurObjX->LeftDnValueY);

	m_pResultBuf->RIB_ANGLE_L = (float)get_inner_angle(CenterPt, StartPt, EndPt);
	if(m_pResultBuf->RIB_ANGLE_L<0.0f)
		m_pResultBuf->RIB_ANGLE_L =0.0f;
	//StartPt.x =(long) (pFindMeasurObjX->RightMinValueX*m_PixelPerNano)- 1000;//-1um
	//StartPt.y = pFindMeasurObjX->RightMinValueY;

	//CenterPt.x =(long) (pFindMeasurObjX->RightMinValueX*m_PixelPerNano);
	//CenterPt.y = pFindMeasurObjX->RightMinValueY;
	StartPt.x =(long) (pFindMeasurObjX->RightAngleValueX*m_PixelPerNano)- 1000;//-1um
	StartPt.y = pFindMeasurObjX->RightAngleValueY;

	CenterPt.x =(long) (pFindMeasurObjX->RightAngleValueX*m_PixelPerNano);
	CenterPt.y = pFindMeasurObjX->RightAngleValueY;

	EndPt.x = (long) (pFindMeasurObjX->RightDnValueX*m_PixelPerNano);
	EndPt.y = (pFindMeasurObjX->RightDnValueY);

	StartPt.x /=100;//(long) (pFindMeasurObjX->RightAngleValueX*m_PixelPerNano)- 1000;//-1um
	StartPt.y /=100;// pFindMeasurObjX->RightAngleValueY;

	CenterPt.x /=100;//(long) (pFindMeasurObjX->RightAngleValueX*m_PixelPerNano);
	CenterPt.y /=100;// pFindMeasurObjX->RightAngleValueY;

	EndPt.x /=100;// (long) (pFindMeasurObjX->RightDnValueX*m_PixelPerNano);
	EndPt.y /=100;// (pFindMeasurObjX->RightDnValueY);
	//10번 거리
	m_pResultBuf->RIB_ANGLE_R = (float)get_inner_angle(CenterPt, StartPt, EndPt);
	if(m_pResultBuf->RIB_ANGLE_R<0.0f)
		m_pResultBuf->RIB_ANGLE_R =0.0f;
	//11번 거리
	//m_pResultBuf->RIB_HEIGHT = (pNewData_3D->m_TargetThicknessNano);
	int HBuf1= m_pResultBuf->RIB_HEIGHT_L + m_pResultBuf->RIB_HEIGHT_E_L;
	int HBuf2= m_pResultBuf->RIB_HEIGHT_R + m_pResultBuf->RIB_HEIGHT_E_R;
	m_pResultBuf->RIB_HEIGHT =HBuf1>HBuf2? HBuf1: HBuf2;

	m_pResultBuf->RIB_HEIGHT  += BaseDepth;
	if(m_pResultBuf->RIB_HEIGHT<0)
		m_pResultBuf->RIB_HEIGHT =0;

	//Dot Type
	// if(m_MaskType == MASK_TYPE_DOT)
	{
		//1번 거리
		m_pResultBuf->RIB_WIDTH2 = (int)(dist_2d(	pFindMeasurObjY->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjY->LeftMinValueY), 
			pFindMeasurObjY->RightMinValueX*m_PixelPerNano, (pFindMeasurObjY->RightMinValueY))*(m_MeasurDotAngle == 0?1:1.4142));
		if(m_pResultBuf->RIB_WIDTH2<0)
			m_pResultBuf->RIB_WIDTH2 =0;
		//2번 거리
		m_pResultBuf->RIB_WIDTH_C2 = (int)(dist_2d(	pFindMeasurObjY->LeftDnValueX*m_PixelPerNano, (pFindMeasurObjY->LeftDnValueY), 
			pFindMeasurObjY->RightDnValueX*m_PixelPerNano, (pFindMeasurObjY->RightDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
		if(m_pResultBuf->RIB_WIDTH_C2<0)
			m_pResultBuf->RIB_WIDTH_C2 =0;
		//3번 거리
		m_pResultBuf->RIB_WIDTH_L2 = (int)(dist_2d(	pFindMeasurObjY->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjY->LeftDnValueY), 
			pFindMeasurObjY->LeftDnValueX*m_PixelPerNano, (pFindMeasurObjY->LeftDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
		if(m_pResultBuf->RIB_WIDTH_L2<0)
			m_pResultBuf->RIB_WIDTH_L2 =0;
		//4번 거리
		m_pResultBuf->RIB_WIDTH_R2 = (int)(dist_2d(	pFindMeasurObjY->RightMinValueX*m_PixelPerNano, (pFindMeasurObjY->RightDnValueY), 
			pFindMeasurObjY->RightDnValueX*m_PixelPerNano, (pFindMeasurObjY->RightDnValueY))*(m_MeasurDotAngle == 0?1:1.4142));
		if(m_pResultBuf->RIB_WIDTH_R2<0)
			m_pResultBuf->RIB_WIDTH_R2 =0;
		//5번 거리
		//m_pResultBuf->RIB_HEIGHT_L = (int)dist_2d(	pFindMeasurObjY->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjY->LeftMinValueY), 
		//	pFindMeasurObjY->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjY->LeftDnValueY));
		m_pResultBuf->RIB_HEIGHT_L2 = (int)dist_2d(	pFindMeasurObjY->LeftAngleValueX*m_PixelPerNano, (pFindMeasurObjY->LeftAngleValueY), 
			pFindMeasurObjY->LeftAngleValueX*m_PixelPerNano, (pFindMeasurObjY->LeftDnValueY));
		if(m_pResultBuf->RIB_HEIGHT_L2<0)
			m_pResultBuf->RIB_HEIGHT_L2 =0;
		//6번 거리
		//m_pResultBuf->RIB_HEIGHT_E_L = (pNewData_3D->m_TargetThicknessNano)-DisplayData[4];
		m_pResultBuf->RIB_HEIGHT_E_L2 = (int)dist_2d(	pFindMeasurObjY->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjY->LeftMinValueY), 
			pFindMeasurObjY->LeftMinValueX*m_PixelPerNano, (pFindMeasurObjY->LeftAngleValueY));

		m_pResultBuf->RIB_HEIGHT_E_L2  += BaseDepth;
		if(m_pResultBuf->RIB_HEIGHT_E_L2<0)
			m_pResultBuf->RIB_HEIGHT_E_L2 =0;
		//7번 거리
		//m_pResultBuf->RIB_HEIGHT_R = (int)dist_2d(	pFindMeasurObjY->RightMinValueX*m_PixelPerNano, (pFindMeasurObjY->RightMinValueY), 
		//	pFindMeasurObjY->RightMinValueX*m_PixelPerNano, (pFindMeasurObjY->RightDnValueY));
		m_pResultBuf->RIB_HEIGHT_R2  = (int)dist_2d(	pFindMeasurObjY->RightAngleValueX*m_PixelPerNano, (pFindMeasurObjY->RightAngleValueY), 
			pFindMeasurObjY->RightAngleValueX*m_PixelPerNano, (pFindMeasurObjY->RightDnValueY));
		if(m_pResultBuf->RIB_HEIGHT_R2<0)
			m_pResultBuf->RIB_HEIGHT_R2 =0;
		//8번 거리
		//m_pResultBuf->RIB_HEIGHT_E_R = (pNewData_3D->m_TargetThicknessNano)-DisplayData[6];
		m_pResultBuf->RIB_HEIGHT_E_R2  = (int)dist_2d(	pFindMeasurObjY->RightMinValueX*m_PixelPerNano, (pFindMeasurObjY->RightMinValueY), 
			pFindMeasurObjY->RightMinValueX*m_PixelPerNano, (pFindMeasurObjY->RightAngleValueY));
		m_pResultBuf->RIB_HEIGHT_E_R2  += BaseDepth;
		if(m_pResultBuf->RIB_HEIGHT_E_R2<0)
			m_pResultBuf->RIB_HEIGHT_E_R2 =0;
		//9번 거리
		//CvPoint  StartPt, CenterPt, EndPt;
		//StartPt.x =(long) (pFindMeasurObjY->LeftMinValueX*m_PixelPerNano)+ 1000;//+1um
		//StartPt.y = pFindMeasurObjY->LeftMinValueY;

		//CenterPt.x =(long) (pFindMeasurObjY->LeftMinValueX*m_PixelPerNano);
		//CenterPt.y = pFindMeasurObjY->LeftMinValueY;
		StartPt.x =(long) (pFindMeasurObjY->LeftAngleValueX*m_PixelPerNano)+ 1000;//+1um
		StartPt.y = pFindMeasurObjY->LeftAngleValueY;

		CenterPt.x =(long) (pFindMeasurObjY->LeftAngleValueX*m_PixelPerNano);
		CenterPt.y = pFindMeasurObjY->LeftAngleValueY;

		EndPt.x = (long) (pFindMeasurObjY->LeftDnValueX*m_PixelPerNano);
		EndPt.y = (long) (pFindMeasurObjY->LeftDnValueY);

		StartPt.x /=100;//(long) (pFindMeasurObjY->LeftAngleValueX*m_PixelPerNano)+ 1000;//+1um
		StartPt.y /=100;//pFindMeasurObjY->LeftAngleValueY;

		CenterPt.x /=100;//(long) (pFindMeasurObjY->LeftAngleValueX*m_PixelPerNano);
		CenterPt.y /=100;// pFindMeasurObjY->LeftAngleValueY;

		EndPt.x /=100;// (long) (pFindMeasurObjY->LeftDnValueX*m_PixelPerNano);
		EndPt.y /=100;// (long) (pFindMeasurObjY->LeftDnValueY);


		m_pResultBuf->RIB_ANGLE_L2 = (float)get_inner_angle(CenterPt, StartPt, EndPt);
		if(m_pResultBuf->RIB_ANGLE_L2<0.0f)
			m_pResultBuf->RIB_ANGLE_L2 =0.0f;
		//StartPt.x =(long) (pFindMeasurObjY->RightMinValueX*m_PixelPerNano)- 1000;//-1um
		//StartPt.y = pFindMeasurObjY->RightMinValueY;

		//CenterPt.x =(long) (pFindMeasurObjY->RightMinValueX*m_PixelPerNano);
		//CenterPt.y = pFindMeasurObjY->RightMinValueY;
		StartPt.x =(long) (pFindMeasurObjY->RightAngleValueX*m_PixelPerNano)- 1000;//-1um
		StartPt.y = pFindMeasurObjY->RightAngleValueY;

		CenterPt.x =(long) (pFindMeasurObjY->RightAngleValueX*m_PixelPerNano);
		CenterPt.y = pFindMeasurObjY->RightAngleValueY;

		EndPt.x = (long) (pFindMeasurObjY->RightDnValueX*m_PixelPerNano);
		EndPt.y = (pFindMeasurObjY->RightDnValueY);

		StartPt.x /=100;//(long) (pFindMeasurObjY->RightAngleValueX*m_PixelPerNano)- 1000;//-1um
		StartPt.y /=100;// pFindMeasurObjY->RightAngleValueY;

		CenterPt.x /=100;//(long) (pFindMeasurObjY->RightAngleValueX*m_PixelPerNano);
		CenterPt.y /=100;// pFindMeasurObjY->RightAngleValueY;

		EndPt.x /=100;//(long) (pFindMeasurObjY->RightDnValueX*m_PixelPerNano);
		EndPt.y /=100;// (pFindMeasurObjY->RightDnValueY);
		//10번 거리
		m_pResultBuf->RIB_ANGLE_R2 = (float)get_inner_angle(CenterPt, StartPt, EndPt);
		if(m_pResultBuf->RIB_ANGLE_R2<0.0f)
			m_pResultBuf->RIB_ANGLE_R2 =0.0f;
		//11번 거리
		//m_pResultBuf->RIB_HEIGHT = (pNewData_3D->m_TargetThicknessNano);
		HBuf1= m_pResultBuf->RIB_HEIGHT_L2 + m_pResultBuf->RIB_HEIGHT_E_L2;
		HBuf2= m_pResultBuf->RIB_HEIGHT_R2 + m_pResultBuf->RIB_HEIGHT_E_R2;
		m_pResultBuf->RIB_HEIGHT2 =HBuf1>HBuf2? HBuf1: HBuf2;
		m_pResultBuf->RIB_HEIGHT2  += BaseDepth;
		if(m_pResultBuf->RIB_HEIGHT2<0)
			m_pResultBuf->RIB_HEIGHT2 =0;
	}
	return 0;
}


int CMeasureProc::fn_ProcDot(DATA_3D_RAW *pNewData_3D, CvPoint MPoint)
{
	memset(m_DepthData_H, 0x00, 1024);
	memset(m_DepthData_V, 0x00, 1024);

	BOOL bDetectCenter = FALSE;
	CvPoint CenterPos;
	MEASER_2D CheckListAxisY;
	MEASER_2D CheckListAxisX;
	//MEASER_2D CheckStrip;



	if(MPoint.x == -1 && MPoint.y ==-1)
	{
		CenterPos.x = -1;
		CenterPos.y = -1;
		//Cross Check.
		CenterPos = FindHoleAreaDotType(pNewData_3D, 3000, 20);

		m_MaserDefaultPos = CenterPos;
	}
	else
	{
		CenterPos = MPoint;
	}
	if(CenterPos.x != -1 && CenterPos.y != -1)
		bDetectCenter = TRUE;

//////////////////////////////////////////////////////////////////////////

	m_3DViewObj.m_MeaserInfo.MPositionX = CenterPos.x - (1024/2);
	m_3DViewObj.m_MeaserInfo.MPositionY = CenterPos.y - (768/2);
	m_3DViewObj.RenderScreen();
	
	m_SBAxisX.SetScrollPos(CenterPos.x);
	m_SBAxisY.SetScrollPos(CenterPos.y);
	
	m_3DViewObj.m_MeaserInfo.MAngle	= m_MeasurDotAngle;
#ifdef PROC_DOT_FIND_IMG
	cvNamedWindow("Label");
	cvShowImage("Label", pLabeled);
	cvReleaseImage(&pLabeled);
#endif
	//////////////////////////////////////////////////////////////////////////
	//측정 영역 Data 취득.(최대 Size는 Image 대각 Size이다)
	if(CenterPos.x<0 || CenterPos.x>1024)
	{
		return 0;
	}
	if(CenterPos.y<0 || CenterPos.y>768)
	{
		return 0;
	}
	//X축 Data
	int ShortDis = CenterPos.x>CenterPos.y?CenterPos.y:CenterPos.x;
	CvPoint XDepthStart, XDepthEnd;
	CvPoint YDepthStart, YDepthEnd;
	XDepthStart.x = CenterPos.x  - ShortDis;
	XDepthStart.y = CenterPos.y - ShortDis;

	ShortDis = (m_pNewData_3D->m_Width-CenterPos.x)> (m_pNewData_3D->m_Height-CenterPos.y)? (m_pNewData_3D->m_Height-CenterPos.y):(m_pNewData_3D->m_Width-CenterPos.x);
	XDepthEnd.x = CenterPos.x + ShortDis;
	XDepthEnd.y = CenterPos.y + ShortDis;
	double Dis45UpX = dist_2d((double)CenterPos.x, (double)CenterPos.y, (double)XDepthStart.x, (double)XDepthStart.y);
	double Dis45UpX2= dist_2d((double)CenterPos.x, (double)CenterPos.y, (double)XDepthEnd.x, (double)XDepthEnd.y);
	double DisLongX = dist_2d((double)XDepthStart.x, (double)XDepthStart.y, (double)XDepthEnd.x, (double)XDepthEnd.y);
	//////////////////////////////////////////////////////////////////////////
	ShortDis = (m_pNewData_3D->m_Width-CenterPos.x)>CenterPos.y ? CenterPos.y: (m_pNewData_3D->m_Width-CenterPos.x);
	YDepthEnd.x = CenterPos.x  + ShortDis;
	YDepthEnd.y = CenterPos.y - ShortDis;

	ShortDis = (CenterPos.x)<(m_pNewData_3D->m_Height-CenterPos.y)? (CenterPos.x): (m_pNewData_3D->m_Height-CenterPos.y);
	YDepthStart.x = CenterPos.x - ShortDis;
	YDepthStart.y = CenterPos.y + ShortDis;
	double Dis45UpY = dist_2d((double)CenterPos.x, (double)CenterPos.y, (double)YDepthEnd.x, (double)YDepthEnd.y);
	double Dis45UpY2 = dist_2d((double)CenterPos.x, (double)CenterPos.y, (double)YDepthStart.x, (double)YDepthStart.y);
	double DisLongY = dist_2d((double)YDepthStart.x, (double)YDepthStart.y, (double)YDepthEnd.x, (double)YDepthEnd.y);

	int Cnt45XStep = 0;
	int Cnt45YStep = 0;

	
	memset(m_DepthData_H, 0x00, sizeof(int)*1024);
	memset(m_DepthData_V, 0x00, sizeof(int)*1024);
	if(m_MeasurDotAngle == 45.0f)
	{
		for(int YDataStep = 0; YDataStep<1024; YDataStep++)
		{
			if((YDepthStart.x+YDataStep)>m_pNewData_3D->m_Width || (YDepthStart.y-YDataStep)<0)
				break;
			if(CenterPos.y == (YDepthStart.y-YDataStep))
				Cnt45YStep = YDataStep;
			m_DepthData_V[YDataStep] = pNewData_3D->m_pDepthData[(YDepthStart.x+YDataStep)+ ((YDepthStart.y-YDataStep)*m_pNewData_3D->m_Width)];
		}


		for(int XDataStep = 0; XDataStep<1024; XDataStep++)
		{
			if((XDepthStart.x+XDataStep)>m_pNewData_3D->m_Width || (XDepthStart.y+XDataStep)>=m_pNewData_3D->m_Height)
				break;

			if(CenterPos.y ==  (XDepthStart.y+XDataStep))
				Cnt45XStep = XDataStep;
			m_DepthData_H[XDataStep] = pNewData_3D->m_pDepthData[(XDepthStart.x+XDataStep)+ ((XDepthStart.y+XDataStep)*m_pNewData_3D->m_Width)];
		}
	}
	else
	{
		for(int YDataStep = 0; YDataStep<1024; YDataStep++)
		{
			if(YDataStep>m_pNewData_3D->m_Width)
				break;
			m_DepthData_H[YDataStep] = pNewData_3D->m_pDepthData[ YDataStep + (CenterPos.y*m_pNewData_3D->m_Width)];
		}

		for(int XDataStep = 0; XDataStep<1024; XDataStep++)
		{
			if(XDataStep>m_pNewData_3D->m_Height)
				break;
			m_DepthData_V[XDataStep] = pNewData_3D->m_pDepthData[ CenterPos.x + (XDataStep*m_pNewData_3D->m_Width)];
		}
	}


	int MeasureXCnt = 0;
	MEASER_2D MeasureListX[100];
	int MeasureYCnt = 0;
	MEASER_2D MeasureListY[100];

	memset(MeasureListX, 0x00, sizeof(MEASER_2D)*100);
	memset(MeasureListY, 0x00, sizeof(MEASER_2D)*100);

	MeaserAreaDotType(&MeasureXCnt, &MeasureListX[0], m_DepthData_H, m_pAxis_H_CalcImg);
	MeaserAreaDotType(&MeasureYCnt, &MeasureListY[0], m_DepthData_V, m_pAxis_V_CalcImg);
	

	int SelectedXIndex = 0;
	int SelectedYIndex = 0;
	//기본 입력.
	CheckListAxisX = MeasureListX[0];
	CheckListAxisY = MeasureListY[0];
	DrawProcLineDot(m_pAxis_H_CalcImg, &CheckListAxisX, m_DepthData_H);
	DrawProcLineDot(m_pAxis_V_CalcImg, &CheckListAxisY, m_DepthData_V);
	
	m_MeaserH_Edit = CheckListAxisX;
	m_MeaserV_Edit = CheckListAxisY;

	m_slider_H1.SetPos(CheckListAxisX.LeftMinValueX);
	m_slider_H2.SetPos(CheckListAxisX.RightMinValueX);
	m_slider_H3.SetPos(CheckListAxisX.LeftAngleValueX);
	m_slider_H4.SetPos(CheckListAxisX.RightAngleValueX);
	m_slider_H5.SetPos(CheckListAxisX.LeftDnValueX);
	m_slider_H6.SetPos(CheckListAxisX.RightDnValueX);

	m_slider_V1.SetPos(CheckListAxisY.LeftMinValueX);
	m_slider_V2.SetPos(CheckListAxisY.RightMinValueX);
	m_slider_V3.SetPos(CheckListAxisY.LeftAngleValueX);
	m_slider_V4.SetPos(CheckListAxisY.RightAngleValueX);
	m_slider_V5.SetPos(CheckListAxisY.LeftDnValueX);
	m_slider_V6.SetPos(CheckListAxisY.RightDnValueX);

	//if(m_bMeasurPosSelect == TRUE)
	{
		//0.일때는 좌표가 0쪽에 있는것을 선정한다.
		if(m_MeasurDotAngle == 0.0)
		{
			for(int MsStep= 0; MsStep<MeasureXCnt; MsStep++)
			{
				int MCenterX = MeasureListX[MsStep].LeftMinValueX+(abs(MeasureListX[MsStep].LeftMinValueX-MeasureListX[MsStep].RightMinValueX)/2);

				if(CenterPos.x>MCenterX)// ||(MeasureListX[MsStep].LeftMinValueX<CenterPos.x && CenterPos.x<-MeasureListX[MsStep].RightMinValueX) )
				{
					SelectedXIndex = MsStep;
					CheckListAxisX = MeasureListX[MsStep];
				}
			}
			for(int MsStep= 0; MsStep<MeasureYCnt; MsStep++)
			{
				int MCenterX = MeasureListY[MsStep].LeftMinValueX+(abs(MeasureListY[MsStep].LeftMinValueX-MeasureListY[MsStep].RightMinValueX)/2);
				if(CenterPos.y>MCenterX)//||(MeasureListY[MsStep].LeftMinValueX<CenterPos.x && CenterPos.x<-MeasureListY[MsStep].RightMinValueX) )
				{
					SelectedYIndex = MsStep;
					CheckListAxisY = MeasureListY[MsStep];
				}
			}
		}
		else
		{
			//45.일때는 둘다 Center 높이에 위쪽에서 가까운것.
			for(int MsStep= 0; MsStep<MeasureXCnt; MsStep++)
			{
				int MCenterX = MeasureListX[MsStep].LeftMinValueX+(abs(MeasureListX[MsStep].LeftMinValueX-MeasureListX[MsStep].RightMinValueX)/2);

				if(Cnt45XStep>MCenterX)//||(MeasureListX[MsStep].LeftMinValueX<CenterPos.y && CenterPos.y<-MeasureListX[MsStep].RightMinValueX) )
				{
					SelectedXIndex = MsStep;
					CheckListAxisX = MeasureListX[MsStep];
				}
			}
			for(int MsStep= 0; MsStep<MeasureYCnt; MsStep++)
			{
				int MCenterX = MeasureListY[MsStep].LeftMinValueX+(abs(MeasureListY[MsStep].LeftMinValueX-MeasureListY[MsStep].RightMinValueX)/2);
				if(Cnt45YStep>MCenterX)//||(MeasureListY[MsStep].LeftMinValueX<CenterPos.y && CenterPos.y<-MeasureListY[MsStep].RightMinValueX) )
				{
					SelectedYIndex = MsStep;
					CheckListAxisY = MeasureListY[MsStep];
				}
			}
		}
	}

	CvPoint printPos;
	printPos.x = CheckListAxisX.LeftMinValueX+abs(CheckListAxisX.LeftMinValueX-CheckListAxisX.RightMinValueX)/2-25;
	printPos.y = (int)((CheckListAxisX.LeftMinValueY+abs(CheckListAxisX.LeftMinValueY-CheckListAxisX.LeftDnValueY)/2)/m_PixelPerNano);
	PutImage_Text(m_pAxis_H_CalcImg, printPos, "<*>", CV_RGB(0,0,0), CV_RGB(121, 120, 222), CV_RGB(255,0,0));

	printPos.x = CheckListAxisY.LeftMinValueX+abs(CheckListAxisY.LeftMinValueX-CheckListAxisY.RightMinValueX)/2-25;
	printPos.y = (int)((CheckListAxisY.LeftMinValueY+abs(CheckListAxisY.LeftMinValueY-CheckListAxisY.LeftDnValueY)/2)/m_PixelPerNano);
	PutImage_Text(m_pAxis_V_CalcImg, printPos, "<*>", CV_RGB(0,0,0), CV_RGB(125, 221, 121), CV_RGB(255,0,0));

	if(m_pResultBuf != NULL)
	{
		fn_SetMeasureValue_Dot( &CheckListAxisX, &CheckListAxisY);
	}
	return bDetectCenter?1:0;
}
int CMeasureProc::fn_ResultImgCreate( IplImage *pSumImg, IplImage *p3DImage, IplImage *pResutImage, IplImage *pCheckPointImg, IplImage *pCheckPointImg2)
{
	memset(pSumImg->imageData, 0xFF, pSumImg->imageSize);

	CvRect SaveRect;
	SaveRect.x =0; 
	SaveRect.y =0; 
	SaveRect.width  = pResutImage->width; 
	SaveRect.height = pResutImage->height; 
	cvSetImageROI(pSumImg, SaveRect);
	cvResize(p3DImage, pSumImg);
	cvDrawRect(pSumImg, cvPoint(3,3),cvPoint(SaveRect.width-3,SaveRect.height-3),CV_RGB( 0, 0, 0), 3 );
	cvResetImageROI(pSumImg);

	SaveRect.y = SaveRect.y + SaveRect.height;
	SaveRect.height = pCheckPointImg->height;
	cvSetImageROI(pSumImg, SaveRect);
	cvResize(pCheckPointImg, pSumImg);
	cvDrawRect(pSumImg, cvPoint(3,3),cvPoint(SaveRect.width-3,SaveRect.height-3),CV_RGB( 0, 0, 0), 3 );
	cvResetImageROI(pSumImg);
	if(pCheckPointImg2 != NULL)
	{
		SaveRect.y = SaveRect.y + SaveRect.height;
		SaveRect.height = pCheckPointImg->height;
		cvSetImageROI(pSumImg, SaveRect);
		cvResize(pCheckPointImg2, pSumImg);
		cvDrawRect(pSumImg, cvPoint(3,3),cvPoint(SaveRect.width-3,SaveRect.height-3),CV_RGB( 0, 0, 0), 3 );
		cvResetImageROI(pSumImg);
	}
	SaveRect.x = pResutImage->width;
	SaveRect.width = pResutImage->width;
	SaveRect.y = 0;
	SaveRect.height = pResutImage->height;
	cvSetImageROI(pSumImg, SaveRect);
	cvResize(pResutImage, pSumImg);
	cvDrawRect(pSumImg, cvPoint(3,3), cvPoint(SaveRect.width-3,SaveRect.height-3),CV_RGB( 0, 0, 0), 3 );
	cvResetImageROI(pSumImg);
	{
		//tickProcRun = ::GetTickCount() - tickProcRun;
		//측정 위치별
		SYSTEMTIME NowTime;
		::GetLocalTime(&NowTime );
		char savePath[MAX_PATH];
		if(strlen(m_strSavePath)>0 && m_MasureIndex>0)
		{
			sprintf_s(	savePath, "%s%s%02d.jpg", m_strSavePath,(m_MaskType == MASK_TYPE_STRIPE?"STRIPE_":"DOT_"), m_MasureIndex);
		}
		else
		{				
			sprintf_s(savePath, "D:\\%s%d_%04d_%02d_%02d_%02d_%02d_%02d.jpg",
				(m_MaskType == MASK_TYPE_STRIPE?"STRIPE_":"DOT_"),
				m_MasureIndex, 
				NowTime.wYear,
				NowTime.wMonth, 
				NowTime.wDay, 
				NowTime.wHour, 
				NowTime.wMinute, 
				NowTime.wSecond);

		}
		char PrintMessage[512];
		sprintf_s(PrintMessage, "Measuring Date : %d/%02d/%02d - %02dH %02dM %02dS",
			NowTime.wYear,
			NowTime.wMonth, 
			NowTime.wDay, 
			NowTime.wHour, 
			NowTime.wMinute, 
			NowTime.wSecond);
		PutImage_Text(pSumImg, cvPoint(SaveRect.x+20, SaveRect.height+20), PrintMessage,CV_RGB(0, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));

		sprintf_s(PrintMessage, "Save Path : %s(*.vk4)",savePath);
		PutImage_Text(pSumImg, cvPoint(SaveRect.x+20, SaveRect.height+20+23+23 /*SaveRect.height+20+23+23+23*/), PrintMessage,CV_RGB(0, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));

		sprintf_s(PrintMessage, "nBiz Co., Ltd.(http://nbiz.biz/)",savePath);
		PutImage_Text_Small(pSumImg, cvPoint(pSumImg->width-200, pSumImg->height-10), PrintMessage,CV_RGB(0, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		//////////////////////////////////////////////////////////////////////////


		int InfoTextStartX = SaveRect.x+20 ;
		int InfoTextStartY = SaveRect.height+25+23+23+23+20;

		int XStepIndex = 0;
		int YStepIndex = 0;

		int InfoYStep = 14;
		int infoXStep = 200;
		int ValueSpace = 75;
		sprintf_s(PrintMessage, "Z_UPPER_L");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->P_Z_UPPER_L);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;


		sprintf_s(PrintMessage, "Z_LOWER_L");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->P_Z_LOWER_L);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;


		sprintf_s(PrintMessage, "PITCH");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d ", m_pResultBuf->P_PITCH);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "Z_MEA_DIS");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d ", m_pResultBuf->P_Z_MEA_DIS);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "ND_FILTER");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d", m_pResultBuf->P_ND_FILTER);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "LASER_BR");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d", m_pResultBuf->P_LASER_BR);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		XStepIndex = 1;
		YStepIndex = 0;
		sprintf_s(PrintMessage,"WIDTH");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_WIDTH);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage,"WIDTH_C");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_WIDTH_C);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "WIDTH_L");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_WIDTH_L);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "WIDTH_R");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_WIDTH_R);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "HEIGHT_L");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm",m_pResultBuf->RIB_HEIGHT_L);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "HEIGHT_E_L");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_HEIGHT_E_L);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "HEIGHT_R");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_HEIGHT_R);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "HEIGHT_E_R");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_HEIGHT_E_R);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;


		sprintf_s(PrintMessage, "ANGLE_L");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %0.3f '", m_pResultBuf->RIB_ANGLE_L);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "ANGLE_R");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %0.3f '", m_pResultBuf->RIB_ANGLE_R);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

		sprintf_s(PrintMessage, "HEIGHT");
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
		sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_HEIGHT);
		PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;
		if( m_MaskType == MASK_TYPE_DOT)
		{
			XStepIndex = 2;
			YStepIndex = 0;
			sprintf_s(PrintMessage,"WIDTH2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_WIDTH2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage,"WIDTH_C2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_WIDTH_C2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage, "WIDTH_L2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_WIDTH_L2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage, "WIDTH_R2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_WIDTH_R2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage, "HEIGHT_L2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm",m_pResultBuf->RIB_HEIGHT_L2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage, "HEIGHT_E_L2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_HEIGHT_E_L2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage, "HEIGHT_R2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_HEIGHT_R2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage, "HEIGHT_E_R2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_HEIGHT_E_R2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;


			sprintf_s(PrintMessage, "ANGLE_L2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %0.3f '", m_pResultBuf->RIB_ANGLE_L2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage, "ANGLE_R2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %0.3f '", m_pResultBuf->RIB_ANGLE_R2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;

			sprintf_s(PrintMessage, "HEIGHT2");
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep), InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(100, 0, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));
			sprintf_s(PrintMessage, ": %d nm", m_pResultBuf->RIB_HEIGHT2);
			PutImage_TextData(pSumImg, cvPoint(InfoTextStartX+(XStepIndex*infoXStep)+ValueSpace, InfoTextStartY+(YStepIndex*InfoYStep)), PrintMessage,CV_RGB(0, 100, 0), CV_RGB(255, 255, 255), CV_RGB(255, 255, 255));YStepIndex++;
		}


		cvSaveImage(savePath, pSumImg);
		memset(m_strSavePath, 0x00, MAX_PATH);
	}
	if(m_showResult == TRUE)
	{
		cvNamedWindow("RetView");
		cvShowImage("RetView", pSumImg);
	}		

	return 0;
}

int CMeasureProc::fn_SetProcData(int MaskType, DATA_3D_RAW *pNewData_3D, RESULT_DATA *pResultBuf, int FilterLevel)
{
	m_MaskType = MaskType;
	m_pNewData_3D = pNewData_3D;

	m_pResultBuf = pResultBuf;
	//3D Data Loading.
	fn_Load3DData(m_MaskType, m_pNewData_3D, m_pResultBuf, FilterLevel );
	if(m_pResultBuf != NULL)
		memset(m_pResultBuf, 0x00, sizeof(RESULT_DATA));

	//Render Data Setting
	m_PixelPerNano = m_pNewData_3D->m_MeasureParam.dXYCalibration*1000.0;//um->Nano

	int DepthOverView = 0;//(int)(m_pNewData_3D->m_ZDepth/m_PixelPerNano);
	//DepthOverView = (int)(DepthOverView*0.3);

	if(m_pAxis_H_CalcImg != NULL)
		cvReleaseImage(&m_pAxis_H_CalcImg);
	m_pAxis_H_CalcImg = cvCreateImage(cvSize(m_pNewData_3D->m_Width,(int)(m_pNewData_3D->m_ZDepth/m_PixelPerNano)+DepthOverView),IPL_DEPTH_8U, 3 );


	if(m_pAxis_V_CalcImg != NULL)
		cvReleaseImage(&m_pAxis_V_CalcImg);
	m_pAxis_V_CalcImg = cvCreateImage(cvSize(m_pNewData_3D->m_Width,(int)(m_pNewData_3D->m_ZDepth/m_PixelPerNano)+DepthOverView),IPL_DEPTH_8U, 3 );

	memset(m_pAxis_H_CalcImg->imageData, RET_BACK_COLOR, m_pAxis_H_CalcImg->imageSize);
	memset(m_pAxis_V_CalcImg->imageData, RET_BACK_COLOR, m_pAxis_V_CalcImg->imageSize);
	//cvZero(m_pAxis_H_CalcImg);
	//cvZero(m_pAxis_V_CalcImg);
	{
		//기본 정보 기록
		if(m_pResultBuf != NULL)
		{
			m_pResultBuf->P_Z_UPPER_L	= (int) pNewData_3D->m_MeasureParam.lUpperPosition;
			m_pResultBuf->P_Z_LOWER_L	= (int) pNewData_3D->m_MeasureParam.lLowerPosition;
			m_pResultBuf->P_PITCH		= pNewData_3D->m_MeasureParam.lPitch;
			m_pResultBuf->P_Z_MEA_DIS = abs(m_pResultBuf->P_Z_UPPER_L-m_pResultBuf->P_Z_LOWER_L);
			switch(pNewData_3D->m_MeasureParam.lNd)
			{
			case 0:
				m_pResultBuf->P_ND_FILTER = 100;
				break;
			case 1:
				m_pResultBuf->P_ND_FILTER = 30;
				break;
			case 2:
				m_pResultBuf->P_ND_FILTER = 10;
				break;
			case 3:
				m_pResultBuf->P_ND_FILTER = 3;
				break;
			case 4:
				m_pResultBuf->P_ND_FILTER = 1;
				break;
			}
			m_pResultBuf->P_LASER_BR = pNewData_3D->m_MeasureParam.lLaserGain1;
		}
	}
	if(m_bMeasureEditResult == TRUE)
		::SetWindowPos(this->m_hWnd,NULL, 10, 10, 0, 0, SWP_SHOWWINDOW|SWP_NOSIZE);
	else
		this->ShowWindow(SW_HIDE);

	m_3DViewObj.SetDefValue();
	return fn_ProcDataMeasure(cvPoint(-1,-1), TRUE);
}
int CMeasureProc::fn_ProcDataMeasure(CvPoint CheckCenter, BOOL NoDrawRet)
{
	DWORD tickProcRun = ::GetTickCount();
	m_ScanTime	= tickProcRun - m_ScanTime;


//#ifdef  nBiz_SIMUL_RUN
//	if(MASK_TYPE_DOT == m_MaskType)
//		m_MeasurDotAngle = 45.0f;
//		//m_MeasurDotAngle = 0.0f;
//	else
//		m_MeasurDotAngle= 0.0f;
//#endif

	IplImage *pResutImage  = NULL;
	if(m_MaskType == MASK_TYPE_STRIPE)
	{
		fn_ProcStrip(m_pNewData_3D, CheckCenter);

		pResutImage  = cvCloneImage(m_pSResultViewImg);

		if(NoDrawRet != TRUE)
			DrawResultStripe(pResutImage);
	}
	else if(m_MaskType == MASK_TYPE_DOT)
	{
		fn_ProcDot(m_pNewData_3D, CheckCenter);

		pResutImage  = cvCloneImage(m_pDResultViewImg);

		if(NoDrawRet != TRUE)
			DrawResultDot(pResutImage);
	}

	cvReleaseImage(&pResutImage);

	//m_3DViewObj.SetDefValue();

	if(m_bMeasureEditResult == FALSE)
		return fn_ProcDataResultDraw();
	else
		//Dsiplay용이다.
		DrawMeaserData();
	//
	m_3DViewObj.RenderScreen();
	return 0;
}
int CMeasureProc::fn_ProcDataResultDraw()
{
	BOOL bDetectCenter = FALSE;

	//////////////////////////////////////////////////////////////////////////
	IplImage *pSumImg = NULL;
	IplImage *pResutImage  = NULL;
	IplImage *pCheckPointImg = cvCreateImage(cvSize(m_pAxis_H_CalcImg->width,(int)150),IPL_DEPTH_8U, 3 );
	IplImage *pCheckPointImg2 = NULL;
	IplImage *p3DImage = NULL;

	m_3DViewObj.SetDefValue();
	p3DImage = m_3DViewObj.m_pReanderImage;
	m_3DViewObj.RenderScreen();
	if(m_MaskType == MASK_TYPE_STRIPE)
	{
		//////////////////////////////////////////////////////////////////////////
		cvResize(m_pAxis_H_CalcImg, pCheckPointImg);
		pResutImage  = cvCloneImage(m_pSResultViewImg);
		DrawResultStripe(pResutImage);
		//////////////////////////////////////////////////////////////////////////
		pSumImg = cvCreateImage(cvSize(pResutImage->width + pResutImage->width, pResutImage->height+pCheckPointImg->height*2), IPL_DEPTH_8U, 3);

		fn_ResultImgCreate(pSumImg, p3DImage, pResutImage, pCheckPointImg, NULL);

	}
	else if(m_MaskType == MASK_TYPE_DOT)
	{
		//////////////////////////////////////////////////////////////////////////
		pCheckPointImg2 = cvCreateImage(cvSize(m_pAxis_V_CalcImg->width,(int)150),IPL_DEPTH_8U, 3 );
		//////////////////////////////////////////////////////////////////////////
		cvFlip(m_pAxis_H_CalcImg, m_pAxis_H_CalcImg, 0);
		cvFlip(m_pAxis_V_CalcImg, m_pAxis_V_CalcImg, 0);
		cvResize(m_pAxis_H_CalcImg, pCheckPointImg);
		cvResize(m_pAxis_V_CalcImg, pCheckPointImg2);

		pResutImage  = cvCloneImage(m_pDResultViewImg);

		DrawResultDot(pResutImage);
		//////////////////////////////////////////////////////////////////////////
		pSumImg = cvCreateImage(cvSize(pResutImage->width + pResutImage->width, pResutImage->height+pCheckPointImg->height+ pCheckPointImg2->height ), IPL_DEPTH_8U, 3);

		fn_ResultImgCreate(pSumImg, p3DImage, pResutImage, pCheckPointImg, pCheckPointImg2);
	}
	

	cvReleaseImage(&pSumImg);
	cvReleaseImage(&pCheckPointImg);
	if(pCheckPointImg2!= NULL)
		cvReleaseImage(&pCheckPointImg2);
	cvReleaseImage(&pResutImage);

	return bDetectCenter== TRUE ? 1 : 0;
}

void CMeasureProc::DrawResultStripe(IplImage *pNewImage)
{
	if(m_StripMagnet == TRUE)
	{
		DrawResultStripe_Magnet(pNewImage);
		return;
	}
	char PrintMessage[256];
	double DisplayData[11] = {0.0,};

	CvPoint DrawDataPos[11];
	DrawDataPos[0] = cvPoint(318,83);//1)
	DrawDataPos[1] = cvPoint(325,500);//2)
	DrawDataPos[2] = cvPoint(208,468);//3)
	DrawDataPos[3] = cvPoint(440,468);//4)
	DrawDataPos[4] = cvPoint(88,302);//5)
	DrawDataPos[5] = cvPoint(98,136);//6)
	DrawDataPos[6] = cvPoint(597,302);//7)
	DrawDataPos[7] = cvPoint(606,136);//8)
	DrawDataPos[8] = cvPoint(190,180);//9)
	DrawDataPos[9] = cvPoint(450,180);//10)
	DrawDataPos[10] = cvPoint(37,263);//11)

	int DataIndex  =0;
	//1번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_WIDTH/1000.0;
	//2번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_WIDTH_C/1000.0;

	//3번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_WIDTH_L/1000.0;

	//4번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_WIDTH_R/1000.0;

	//5번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_HEIGHT_L/1000.0;

	//6번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_HEIGHT_E_L/1000.0;

	//7번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_HEIGHT_R/1000.0;

	//8번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_HEIGHT_E_R/1000.0;

	//9번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_ANGLE_L;

	//10번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_ANGLE_R;

	//11번 거리
	DisplayData[DataIndex++] = (m_pResultBuf->RIB_HEIGHT/1000.0);

	for(int ChStep = 0; ChStep<11; ChStep++)
	{
		if(DisplayData[ChStep] <0.0f)
		{
			DisplayData[ChStep] = 0.0f;
		}
	}
	//////////////////////////////////////////////////////////////////////////

	if(pNewImage != NULL)
	{
		for(int DrawStep = 0; DrawStep<11; DrawStep++)
		{
			if(DrawStep == 8 || DrawStep ==9)
				sprintf_s(PrintMessage, "(%d) %0.2f' ",(DrawStep+1), DisplayData[DrawStep]);
			else
				sprintf_s(PrintMessage, "(%d) %0.2fum",(DrawStep+1), DisplayData[DrawStep]);
			PutImage_Text(pNewImage, DrawDataPos[DrawStep], PrintMessage);
		}
	}
	//else
	{
		CvvImage ViewImage;
		ViewImage.Create(m_pSResultViewImg->width, m_pSResultViewImg->height, ((IPL_DEPTH_8U & 255)*3) );
		ViewImage.CopyOf(m_pSResultViewImg);
		IplImage *pDrawBuf = ViewImage.GetImage();

		{
			for(int DrawStep = 0; DrawStep<11; DrawStep++)
			{
				if(DrawStep == 8 || DrawStep ==9)
					sprintf_s(PrintMessage, "(%d) %0.2f' ",(DrawStep+1), DisplayData[DrawStep]);
				else
					sprintf_s(PrintMessage, "(%d) %0.2fum",(DrawStep+1), DisplayData[DrawStep]);
				PutImage_Text(pDrawBuf, DrawDataPos[DrawStep], PrintMessage);
			}
		}

		ViewImage.DrawToHDC(m_HDCResultView, m_RectResultViewArea);
		ViewImage.Destroy();
	}

}
void CMeasureProc::DrawResultStripe_Magnet( IplImage *pNewImage )
{
	char PrintMessage[256];
	double DisplayData[11] = {0.0,};

	CvPoint DrawDataPos[11];
	DrawDataPos[0] = cvPoint(318,83);//1)
	DrawDataPos[1] = cvPoint(325,500);//2)
	DrawDataPos[2] = cvPoint(208,468);//3)
	DrawDataPos[3] = cvPoint(440,468);//4)
	DrawDataPos[4] = cvPoint(88,302);//5)
	DrawDataPos[5] = cvPoint(98,136);//6)
	DrawDataPos[6] = cvPoint(597,302);//7)
	DrawDataPos[7] = cvPoint(606,136);//8)
	DrawDataPos[8] = cvPoint(190,180);//9)
	DrawDataPos[9] = cvPoint(450,180);//10)
	DrawDataPos[10] = cvPoint(37,263);//11)

	int DataIndex  =0;
	//1번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_WIDTH/1000.0;
	//2번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_WIDTH_C/1000.0;

	//3번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_WIDTH_L/1000.0;

	//4번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_WIDTH_R/1000.0;

	//5번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_HEIGHT_L/1000.0;

	//6번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_HEIGHT_E_L/1000.0;

	//7번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_HEIGHT_R/1000.0;

	//8번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_HEIGHT_E_R/1000.0;

	//9번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_ANGLE_L;

	//10번 거리
	DisplayData[DataIndex++] = m_pResultBuf->RIB_ANGLE_R;

	//11번 거리
	DisplayData[DataIndex++] = (m_pResultBuf->RIB_HEIGHT/1000.0);

	for(int ChStep = 0; ChStep<11; ChStep++)
	{
		if(DisplayData[ChStep] <0.0f)
		{
			DisplayData[ChStep] = 0.0f;
		}
	}
	//////////////////////////////////////////////////////////////////////////

	if(pNewImage != NULL)
	{
		for(int DrawStep = 0; DrawStep<11; DrawStep++)
		{
			if(DrawStep == 8 || DrawStep ==9)
				sprintf_s(PrintMessage, "(%d) %0.2f' ",(DrawStep+1), DisplayData[DrawStep]);
			else
				sprintf_s(PrintMessage, "(%d) %0.2fum",(DrawStep+1), DisplayData[DrawStep]);
			PutImage_Text(pNewImage, DrawDataPos[DrawStep], PrintMessage);
		}
	}
	//else
	{
		CvvImage ViewImage;
		ViewImage.Create(m_pSResultViewImg->width, m_pSResultViewImg->height, ((IPL_DEPTH_8U & 255)*3) );
		ViewImage.CopyOf(m_pSResultViewImg);
		IplImage *pDrawBuf = ViewImage.GetImage();

		{
			for(int DrawStep = 0; DrawStep<11; DrawStep++)
			{
				if(DrawStep == 8 || DrawStep ==9)
					sprintf_s(PrintMessage, "(%d) %0.2f' ",(DrawStep+1), DisplayData[DrawStep]);
				else
					sprintf_s(PrintMessage, "(%d) %0.2fum",(DrawStep+1), DisplayData[DrawStep]);
				PutImage_Text(pDrawBuf, DrawDataPos[DrawStep], PrintMessage);
			}
		}

		ViewImage.DrawToHDC(m_HDCResultView, m_RectResultViewArea);
		ViewImage.Destroy();
	}

}
void CMeasureProc::DrawResultDot( IplImage *pNewImage )
{
	char PrintMessage[256];
	double DisplayDataX[12] = {0.0,};

	CvPoint DrawDataPosX[12];
	DrawDataPosX[0] = cvPoint(318+7,83/2);//1)
	DrawDataPosX[1] = cvPoint(325+3,500/2);//2)
	DrawDataPosX[2] = cvPoint(208-30,468/2);//3)
	DrawDataPosX[3] = cvPoint(440+50,468/2);//4)
	DrawDataPosX[4] = cvPoint(88-13,263/2+40);//5)
	DrawDataPosX[5] = cvPoint(88+5,136/2);//6)
	DrawDataPosX[6] = cvPoint(597+25,263/2+40);//7)
	DrawDataPosX[7] = cvPoint(606-20,136/2);//8)
	DrawDataPosX[8] = cvPoint(190-35,180/2+12);//9)
	DrawDataPosX[9] = cvPoint(450+50,180/2+12);//10)
	DrawDataPosX[10] = cvPoint(34-9,290/2 );//11)
	DrawDataPosX[11] = cvPoint(34+610,290/2 );//11)

	int ImageH = m_pDResultViewImg->height/2;
	double DisplayDataY[12] = {0.0,};
	CvPoint DrawDataPosY[12];
	DrawDataPosY[0]  = cvPoint(DrawDataPosX[0].x,DrawDataPosX[0].y+ImageH);//1)
	DrawDataPosY[1]  = cvPoint(DrawDataPosX[1].x,DrawDataPosX[1].y+ImageH);//2)
	DrawDataPosY[2]  = cvPoint(DrawDataPosX[2].x,DrawDataPosX[2].y+ImageH);//3)
	DrawDataPosY[3]  = cvPoint(DrawDataPosX[3].x,DrawDataPosX[3].y+ImageH);//4)
	DrawDataPosY[4]  = cvPoint(DrawDataPosX[4].x,DrawDataPosX[4].y+ImageH);//5)
	DrawDataPosY[5]  = cvPoint(DrawDataPosX[5].x,DrawDataPosX[5].y+ImageH);//6)
	DrawDataPosY[6]  = cvPoint(DrawDataPosX[6].x,DrawDataPosX[6].y+ImageH);//7)
	DrawDataPosY[7]  = cvPoint(DrawDataPosX[7].x,DrawDataPosX[7].y+ImageH);//8)
	DrawDataPosY[8]  = cvPoint(DrawDataPosX[8].x,DrawDataPosX[8].y+ImageH);//9)
	DrawDataPosY[9]  = cvPoint(DrawDataPosX[9].x,DrawDataPosX[9].y+ImageH);//10)
	DrawDataPosY[10] = cvPoint(DrawDataPosX[10].x,DrawDataPosX[10].y+ImageH);//11)
	DrawDataPosY[11] = cvPoint(DrawDataPosX[11].x,DrawDataPosX[11].y+ImageH);//11)

	int DataIndex  =0;
	//1번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_WIDTH/1000.0;
	//2번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_WIDTH_C/1000.0;
	//3번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_WIDTH_L/1000.0;
	//4번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_WIDTH_R/1000.0;
	//5번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_HEIGHT_L/1000.0;
	//6번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_HEIGHT_E_L/1000.0;
	//7번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_HEIGHT_R/1000.0;
	//8번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_HEIGHT_E_R/1000.0;
	//9번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_ANGLE_L;
	//10번 거리
	DisplayDataX[DataIndex++] = m_pResultBuf->RIB_ANGLE_R;
	//11번 거리
	DisplayDataX[DataIndex++] = DisplayDataX[4] +DisplayDataX[5];
	//12번 거리
	DisplayDataX[DataIndex++] = DisplayDataX[6] +DisplayDataX[7];



	DataIndex = 0;
	//1번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_WIDTH2/1000.0;
	//2번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_WIDTH_C2/1000.0;
	//3번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_WIDTH_L2/1000.0;
	//4번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_WIDTH_R2/1000.0;
	//5번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_HEIGHT_L2/1000.0;
	//6번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_HEIGHT_E_L2/1000.0;
	//7번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_HEIGHT_R2/1000.0;
	//8번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_HEIGHT_E_R2/1000.0;
	//9번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_ANGLE_L2;
	//10번 거리
	DisplayDataY[DataIndex++] = m_pResultBuf->RIB_ANGLE_R2;
	//11번 거리
	DisplayDataY[DataIndex++] = DisplayDataY[4] +DisplayDataY[5];
	//12번 거리
	DisplayDataY[DataIndex++] = DisplayDataY[6] +DisplayDataY[7];

	for(int ChStep = 0; ChStep<12; ChStep++)
	{
		if(DisplayDataY[ChStep] <0.0f)
		{
			DisplayDataY[ChStep] = 0.0f;
		}
	}
	//////////////////////////////////////////////////////////////////////////
	if(pNewImage != NULL)
	{
		for(int DrawStep = 0; DrawStep<12; DrawStep++)
		{
			if(DrawStep == 8 || DrawStep ==9)
				sprintf_s(PrintMessage, "(%d) %0.2f' ",(DrawStep+1), DisplayDataX[DrawStep]);
			else
				sprintf_s(PrintMessage, "(%d) %0.2fum",(DrawStep+1), DisplayDataX[DrawStep]);
			PutImage_Text_Small(pNewImage, DrawDataPosX[DrawStep], PrintMessage);
		}
		for(int DrawStep = 0; DrawStep<12; DrawStep++)
		{
			if(DrawStep == 8 || DrawStep ==9)
				sprintf_s(PrintMessage, "(%d) %0.2f' ",(DrawStep+1), DisplayDataY[DrawStep]);
			else
				sprintf_s(PrintMessage, "(%d) %0.2fum",(DrawStep+1), DisplayDataY[DrawStep]);
			PutImage_Text_Small(pNewImage, DrawDataPosY[DrawStep], PrintMessage);
		}
	}

	CvvImage ViewImage;
	ViewImage.Create(m_RectResultViewArea.Width(), m_RectResultViewArea.Height(), ((IPL_DEPTH_8U & 255)*3) );
	ViewImage.CopyOf(m_pDResultViewImg);
	IplImage *pDrawBuf = ViewImage.GetImage();
	{
		for(int DrawStep = 0; DrawStep<12; DrawStep++)
		{
			if(DrawStep == 8 || DrawStep ==9)
				sprintf_s(PrintMessage, "(%d) %0.2f' ",(DrawStep+1), DisplayDataX[DrawStep]);
			else
				sprintf_s(PrintMessage, "(%d) %0.2fum",(DrawStep+1), DisplayDataX[DrawStep]);
			PutImage_Text_Small(pDrawBuf, DrawDataPosX[DrawStep], PrintMessage);
		}

		for(int DrawStep = 0; DrawStep<12; DrawStep++)
		{
			if(DrawStep == 8 || DrawStep ==9)
				sprintf_s(PrintMessage, "(%d) %0.2f' ",(DrawStep+1), DisplayDataY[DrawStep]);
			else
				sprintf_s(PrintMessage, "(%d) %0.2fum",(DrawStep+1), DisplayDataY[DrawStep]);
			PutImage_Text_Small(pDrawBuf, DrawDataPosY[DrawStep], PrintMessage);
		}
	}
	ViewImage.DrawToHDC(m_HDCResultView, m_RectResultViewArea);
	ViewImage.Destroy();
}
void CMeasureProc::DrawProcLineStrip(IplImage *pDrawTarget, MEASER_2D *pResultBuf, BOOL bMeaserEdit)
{
	memset(pDrawTarget->imageData, RET_BACK_COLOR, pDrawTarget->imageSize);


	if(m_StripMagnet == TRUE)
	{
		DrawProcLineStrip_Magnet(pDrawTarget, pResultBuf, m_DepthData_H, bMeaserEdit);
		return;
	}

	//위치 Drawing.
	int DrawZPos = (int)(m_DepthData_H[0]/m_PixelPerNano);
	CvPoint StartPos, EndPos;
	StartPos.x = 0; 
	StartPos.y = DrawZPos;

	for(int WStep = 1; WStep<m_pNewData_3D->m_Width; WStep++)
	{
		DrawZPos = (int)(m_DepthData_H[WStep]/m_PixelPerNano);

		EndPos = cvPoint(WStep,DrawZPos);
		cvDrawLine(pDrawTarget, StartPos, EndPos, CV_RGB(255,0,0));

		StartPos = EndPos;

	}
	//char PrintMessage[256];
	//CvPoint StartPos, EndPos;
	CvPoint CenterPos, CenterPos2;
	//Depth Data를 표현 하기 위해서는 m_PixelPerNano로 나누어줘야 한다.(Nano 단위 임으로)
	StartPos	= cvPoint((int)pResultBuf->LeftMinValueX,			(int)(pResultBuf->LeftMinValueY/m_PixelPerNano));
	EndPos		= cvPoint((int)pResultBuf->RightMinValueX,			(int)(pResultBuf->RightMinValueY/m_PixelPerNano));
	cvDrawLine(pDrawTarget, StartPos, EndPos, CV_RGB(0,255,0), 1);
	dist_2PointCenter(	(int)pResultBuf->LeftMinValueX,	(int)(pResultBuf->LeftMinValueY/m_PixelPerNano),
		(int)pResultBuf->RightMinValueX,	(int)(pResultBuf->RightMinValueY/m_PixelPerNano),
		&CenterPos.x, &CenterPos.y);
	dist_2PointCenter(	(int)pResultBuf->LeftDnValueX,	(int)(pResultBuf->LeftDnValueY/m_PixelPerNano),
		(int)pResultBuf->RightDnValueX,(int)(pResultBuf->RightDnValueY/m_PixelPerNano),										
		&CenterPos2.x, &CenterPos2.y);
	dist_2PointCenter(CenterPos.x, CenterPos.y, CenterPos2.x, CenterPos2.y, &StartPos.x, &StartPos.y);

	StartPos	= cvPoint((int)pResultBuf->LeftDnValueX,			(int)(pResultBuf->LeftDnValueY/m_PixelPerNano));
	EndPos		= cvPoint((int)pResultBuf->LeftMinValueX,			(int)(pResultBuf->LeftMinValueY/m_PixelPerNano));
	cvDrawLine(pDrawTarget, StartPos, EndPos, CV_RGB(0,255,0), 1);

	StartPos	= cvPoint((int)pResultBuf->RightDnValueX,			(int)(pResultBuf->RightDnValueY/m_PixelPerNano));
	EndPos		= cvPoint((int)pResultBuf->RightMinValueX,			(int)(pResultBuf->RightMinValueY/m_PixelPerNano));
	cvDrawLine(pDrawTarget, StartPos, EndPos, CV_RGB(0,255,0), 1);

	StartPos	= cvPoint((int)pResultBuf->LeftDnValueX,			(int)(pResultBuf->LeftDnValueY/m_PixelPerNano));
	EndPos		= cvPoint((int)pResultBuf->RightDnValueX,			(int)(pResultBuf->RightDnValueY/m_PixelPerNano));
	cvDrawLine(pDrawTarget, StartPos, EndPos, CV_RGB(0,255,0), 1);


	//StartPos	= cvPoint((int)pResultBuf->MaxValueX,			(int)(pResultBuf->MaxValueY/m_PixelPerNano));
	//cvCircle(pDrawTarget,StartPos,5, CV_RGB(0,0,255),2);

	if(bMeaserEdit == FALSE)
	{
		StartPos	= cvPoint((int)pResultBuf->LeftMinValueX,			(int)(pResultBuf->LeftMinValueY/m_PixelPerNano));
		cvCircle(pDrawTarget,StartPos,2, CV_RGB(255,0,255),2);

		StartPos	= cvPoint((int)pResultBuf->RightMinValueX,			(int)(pResultBuf->RightMinValueY/m_PixelPerNano));
		cvCircle(pDrawTarget,StartPos,2, CV_RGB(255,0,255),2);

		StartPos	= cvPoint((int)pResultBuf->LeftDnValueX,			(int)(pResultBuf->LeftDnValueY/m_PixelPerNano));
		cvCircle(pDrawTarget,StartPos,2, CV_RGB(255,0,255),2);

		StartPos	= cvPoint((int)pResultBuf->RightDnValueX,			(int)(pResultBuf->RightDnValueY/m_PixelPerNano));
		cvCircle(pDrawTarget,StartPos,2, CV_RGB(255,0,255),2);
	}
	else
	{
		StartPos	= cvPoint((int)pResultBuf->LeftMinValueX,			(int)(pResultBuf->LeftMinValueY/m_PixelPerNano));
		cvDrawLine(pDrawTarget, StartPos, cvPoint(StartPos.x, pDrawTarget->height), CV_RGB(255,0,0), 1);

		StartPos	= cvPoint((int)pResultBuf->RightMinValueX,			(int)(pResultBuf->RightMinValueY/m_PixelPerNano));
		cvDrawLine(pDrawTarget, StartPos, cvPoint(StartPos.x, pDrawTarget->height), CV_RGB(255,0,0), 1);

		StartPos	= cvPoint((int)pResultBuf->LeftDnValueX,			(int)(pResultBuf->LeftDnValueY/m_PixelPerNano));
		cvDrawLine(pDrawTarget, StartPos, cvPoint(StartPos.x, pDrawTarget->height), CV_RGB(0,0,255), 1);

		StartPos	= cvPoint((int)pResultBuf->RightDnValueX,			(int)(pResultBuf->RightDnValueY/m_PixelPerNano));
		cvDrawLine(pDrawTarget, StartPos, cvPoint(StartPos.x, pDrawTarget->height), CV_RGB(0,0,255), 1);
	}

}
void CMeasureProc::DrawProcLineStrip_Magnet(IplImage *pDrawTarget, MEASER_2D *pResultBuf, int *pDepthDataList, BOOL bMeaserEdit)
{

	memset(pDrawTarget->imageData, RET_BACK_COLOR, pDrawTarget->imageSize);

	CvPoint StartPos, EndPos;
	StartPos.x = 0; 
	StartPos.y = (int)(((double)pDepthDataList[0])/m_PixelPerNano);
	for(int WStep = 1; WStep<1024; WStep++)
	{
		EndPos = cvPoint(WStep,(int)(((double)pDepthDataList[WStep]/m_PixelPerNano)));
		cvDrawLine(pDrawTarget, StartPos, EndPos, CV_RGB(255,0,0));
		StartPos = EndPos;
	}

	CvPoint PolyData[6];
	PolyData[0] = cvPoint(pResultBuf->LeftMinValueX, (int)(pResultBuf->LeftMinValueY/m_PixelPerNano));
	PolyData[1] = cvPoint(pResultBuf->RightMinValueX, (int)(pResultBuf->RightMinValueY/m_PixelPerNano));
	PolyData[2] = cvPoint(pResultBuf->RightAngleValueX,(int)(pResultBuf->RightAngleValueY/m_PixelPerNano));
	PolyData[3] = cvPoint(pResultBuf->RightDnValueX, (int)(pResultBuf->RightDnValueY/m_PixelPerNano));
	PolyData[4] = cvPoint(pResultBuf->LeftDnValueX, (int)(pResultBuf->LeftDnValueY/m_PixelPerNano));
	PolyData[5] = cvPoint(pResultBuf->LeftAngleValueX,(int)(pResultBuf->LeftAngleValueY/m_PixelPerNano));

	for(int DrawStep = 0; DrawStep<6; DrawStep++)
	{
		cvDrawLine(pDrawTarget,	PolyData[DrawStep],PolyData[DrawStep+1==6?0:DrawStep+1] , CV_RGB(0,255,0), 1);
		if(bMeaserEdit == FALSE)
			cvDrawCircle(pDrawTarget,PolyData[DrawStep], 2, CV_RGB(255,0,255),2);
	}

	if(bMeaserEdit == TRUE)
	{
		for(int DrawStep = 0; DrawStep<6; DrawStep++)
		{
			if(DrawStep == 0 || DrawStep == 1)
				cvDrawLine(pDrawTarget,	PolyData[DrawStep],cvPoint(PolyData[DrawStep].x, pDrawTarget->height) , CV_RGB(255,0,0), 1);

			if(DrawStep ==2 || DrawStep == 5)
				cvDrawLine(pDrawTarget,	PolyData[DrawStep],cvPoint(PolyData[DrawStep].x, pDrawTarget->height) , CV_RGB(0,255,0), 1);

			if(DrawStep == 3 || DrawStep == 4)
				cvDrawLine(pDrawTarget,	PolyData[DrawStep],cvPoint(PolyData[DrawStep].x, pDrawTarget->height) , CV_RGB(0,0,255), 1);
		}
	}


}
void CMeasureProc::DrawProcLineDot(IplImage *pDrawTarget, MEASER_2D *pResultBuf, int *pDepthDataList, BOOL bMeaserEdit)
{

	memset(pDrawTarget->imageData, RET_BACK_COLOR, pDrawTarget->imageSize);

	CvPoint StartPos, EndPos;
	StartPos.x = 0; 
	StartPos.y = (int)(((double)pDepthDataList[0])/m_PixelPerNano);
	for(int WStep = 1; WStep<1024; WStep++)
	{
		EndPos = cvPoint(WStep,(int)(((double)pDepthDataList[WStep]/m_PixelPerNano)));
		cvDrawLine(pDrawTarget, StartPos, EndPos, CV_RGB(255,0,0));
		StartPos = EndPos;
	}
	
	CvPoint PolyData[6];
	PolyData[0] = cvPoint(pResultBuf->LeftMinValueX, (int)(pResultBuf->LeftMinValueY/m_PixelPerNano));
	PolyData[1] = cvPoint(pResultBuf->RightMinValueX, (int)(pResultBuf->RightMinValueY/m_PixelPerNano));
	PolyData[2] = cvPoint(pResultBuf->RightAngleValueX,(int)(pResultBuf->RightAngleValueY/m_PixelPerNano));
	PolyData[3] = cvPoint(pResultBuf->RightDnValueX, (int)(pResultBuf->RightDnValueY/m_PixelPerNano));
	PolyData[4] = cvPoint(pResultBuf->LeftDnValueX, (int)(pResultBuf->LeftDnValueY/m_PixelPerNano));
	PolyData[5] = cvPoint(pResultBuf->LeftAngleValueX,(int)(pResultBuf->LeftAngleValueY/m_PixelPerNano));

	for(int DrawStep = 0; DrawStep<6; DrawStep++)
	{
		cvDrawLine(pDrawTarget,	PolyData[DrawStep],PolyData[DrawStep+1==6?0:DrawStep+1] , CV_RGB(0,255,0), 1);
		if(bMeaserEdit == FALSE)
			cvDrawCircle(pDrawTarget,PolyData[DrawStep], 2, CV_RGB(255,0,255),2);
	}

	if(bMeaserEdit == TRUE)
	{
		for(int DrawStep = 0; DrawStep<6; DrawStep++)
		{
			if(DrawStep == 0 || DrawStep == 1)
				cvDrawLine(pDrawTarget,	PolyData[DrawStep],cvPoint(PolyData[DrawStep].x, pDrawTarget->height) , CV_RGB(255,0,0), 1);

			if(DrawStep ==2 || DrawStep == 5)
				cvDrawLine(pDrawTarget,	PolyData[DrawStep],cvPoint(PolyData[DrawStep].x, pDrawTarget->height) , CV_RGB(0,255,0), 1);

			if(DrawStep == 3 || DrawStep == 4)
				cvDrawLine(pDrawTarget,	PolyData[DrawStep],cvPoint(PolyData[DrawStep].x, pDrawTarget->height) , CV_RGB(0,0,255), 1);
		}
	}


}
void CMeasureProc::MeaserAreaStickType(int PositionY, MEASER_2D *pResultBuf)
{

	memset(m_pAxis_H_CalcImg->imageData, RET_BACK_COLOR, m_pAxis_H_CalcImg->imageSize);

	if(m_StripMagnet == TRUE)
	{
		MeaserAreaStickType_Magnet(PositionY, pResultBuf, m_DepthData_H);
		return;
	}
	//cvSetZero(m_pAxis_H_CalcImg);
	//PositionY = 177;
	if(m_pNewData_3D != NULL)
	{


		//////////////////////////////////////////////////////////////////////////
		//Max 위치는 총 5Point를 찾고.Max Index, Left Index, Right Index를 기록 한다.
		MEASER_2D aryMaxPoint[MAX_TOP_FIND_COUNT];

		long OldValue =0;
		long NowValue =0;

		//////////////////////////////////////////////////////////////////////////
		for(int MaxFindStep = 0; MaxFindStep<MAX_TOP_FIND_COUNT; MaxFindStep++)
		{
			//////////////////////////////////////////////////////////////////////////
			//Max위치 찾기.
			BOOL AlreadyFindArea = FALSE;
			OldValue =0;
			for(int WStep = 0; WStep<m_pNewData_3D->m_Width; WStep++)
			{
				AlreadyFindArea = FALSE;
				for(int CheckStep = 0; CheckStep<MAX_TOP_FIND_COUNT; CheckStep++)
				{
					if(aryMaxPoint[CheckStep].MaxValueX >= 0)//처음것부터 없으면 걍 나가자.
					{
						if(	(aryMaxPoint[CheckStep].LeftMinValueX != (-1) && aryMaxPoint[CheckStep].RightMinValueX  != (-1)) &&//초기화 값이 아니어야 하고.
							(aryMaxPoint[CheckStep].LeftMinValueX<=WStep && WStep <= aryMaxPoint[CheckStep].RightMinValueX ))//Left/Right안에 들어오고.
						{
							AlreadyFindArea = TRUE;
							break;
						}
					}
					else
						break;
				}

				//이전 탐색 위치 인지 확인.
				if(AlreadyFindArea == FALSE)
				{
					NowValue = *(m_pNewData_3D->m_pDepthData+WStep+(m_pNewData_3D->m_Width*PositionY));

					if(OldValue < NowValue)
					{
						OldValue = NowValue;

						aryMaxPoint[MaxFindStep].MaxValueX = WStep;
						aryMaxPoint[MaxFindStep].MaxValueY = OldValue;
					}
				}
			}
			//////////////////////////////////////////////////////////////////////////
			//해당위치에서 좌우 Limit 찾기 (점점 작아지는 시점.-Data변화가 급격히 나타나면 Limit점으로 판단 Index를 기록.'
			//Left위치 찾기.


			int leftFindLimit=0;
			OldValue =aryMaxPoint[MaxFindStep].MaxValueY;
			for(int WStep = aryMaxPoint[MaxFindStep].MaxValueX; WStep>=leftFindLimit; WStep--)
			{
				NowValue =	*(m_pNewData_3D->m_pDepthData+WStep+(m_pNewData_3D->m_Width*PositionY));

				if(OldValue>=NowValue)
				{
					OldValue = NowValue;
					aryMaxPoint[MaxFindStep].LeftMinValueX = WStep;
					aryMaxPoint[MaxFindStep].LeftMinValueY = OldValue;
				}

				//최 하위값 Limit결정.
				if(abs(OldValue-NowValue)>800)//800 Nano이상 다시 상승하면.
					break;
			}

			int RightFindLimit=m_pNewData_3D->m_Width;
			OldValue =aryMaxPoint[MaxFindStep].MaxValueY;
			for(int WStep = aryMaxPoint[MaxFindStep].MaxValueX; WStep<RightFindLimit; WStep++)
			{
				NowValue =	*(m_pNewData_3D->m_pDepthData+WStep+(m_pNewData_3D->m_Width*PositionY));

				if(OldValue>=NowValue)
				{
					OldValue = NowValue;
					aryMaxPoint[MaxFindStep].RightMinValueX= WStep;
					aryMaxPoint[MaxFindStep].RightMinValueY= OldValue;
				}
				//최 하위값 Limit결정.
				if(abs(OldValue-NowValue)>800)//800 Nano이상 다시 상승하면.
					break;
			}

			////이런현상은 좌측 끝, 우측 끝이됨으로(끝을 Limit으로 잡는다.
			////Max와 Left Min 값의 높이 차가 얼마 안나면 거리 오류 이다.
			long DisLeftDepth = abs(aryMaxPoint[MaxFindStep].MaxValueY-aryMaxPoint[MaxFindStep].LeftMinValueY);
			long DisRightDepth = abs(aryMaxPoint[MaxFindStep].MaxValueY-aryMaxPoint[MaxFindStep].RightMinValueY);

			long DisDivValue = 0;
			//큰쪽의 반보다 작으면 오류.
			if(DisLeftDepth>DisRightDepth)
				DisDivValue = DisLeftDepth/2;
			else
				DisDivValue = DisRightDepth/2;

			if(DisLeftDepth<DisDivValue)//왼쪽오류.
				aryMaxPoint[MaxFindStep].LeftMinValueX = 0;

			if(DisRightDepth<DisDivValue)//오른쪽 오류.
				aryMaxPoint[MaxFindStep].RightMinValueX = m_pNewData_3D->m_Width;
		}
		//////////////////////////////////////////////////////////////////////////
		//Draw Result(Max Index)
		int DetectBufCnt = 0;
		MEASER_2D DetectBuf[MAX_TOP_FIND_COUNT];
		//////////////////////////////////////////////////////////////////////////
		//Result Filtering.
		int AVR_TopDistance	= 0;
		for(int CheckStep = 0; CheckStep<MAX_TOP_FIND_COUNT; CheckStep++)
		{
			if(aryMaxPoint[CheckStep].MaxValueX >= 0)//처음것부터 없으면 걍 나가자.
			{
				if(	(aryMaxPoint[CheckStep].LeftMinValueX != (-1) && aryMaxPoint[CheckStep].RightMinValueX != (-1) ) &&//초기화 상태인넘 제거.
					(aryMaxPoint[CheckStep].LeftMinValueX != 0 && aryMaxPoint[CheckStep].RightMinValueX<m_pNewData_3D->m_Width-1))//양 끝에 붙은넘 제거.
				{
					DetectBuf[DetectBufCnt++] = aryMaxPoint[CheckStep];

					AVR_TopDistance +=abs(aryMaxPoint[CheckStep].MaxValueY-aryMaxPoint[CheckStep].LeftMinValueY);
				}
			}
			else 
				break;
		}
		if(DetectBufCnt>0)
		{
			AVR_TopDistance		=(int) ((AVR_TopDistance/DetectBufCnt)*0.8);
		}

		double AVR_AreaSize	= 0.0;
		double AreaSizeList[MAX_TOP_FIND_COUNT];
		int DetectCnt = 0;
		for(int CheckStep = 0; CheckStep<DetectBufCnt; CheckStep++)
		{
			if(DetectBuf[CheckStep].MaxValueX >= 0)//처음것부터 없으면 걍 나가자.
			{
				int NowDisLeft = abs(DetectBuf[CheckStep].MaxValueY-DetectBuf[CheckStep].LeftMinValueY);
				int NowDisRight = abs(DetectBuf[CheckStep].MaxValueY-DetectBuf[CheckStep].RightMinValueY);

				if(	(AVR_TopDistance < NowDisLeft) &&	//평균 높이 미만은 제거 한다.(좌측이 짧을때.)
					(AVR_TopDistance < NowDisRight))	//평균 높이 미만은 제거 한다.(우측이 짧을때)
				{

					AreaSizeList[CheckStep] = Area3Point(	cvPoint(aryMaxPoint[CheckStep].MaxValueX, aryMaxPoint[CheckStep].MaxValueY), 
															cvPoint(aryMaxPoint[CheckStep].LeftMinValueX, aryMaxPoint[CheckStep].LeftMinValueY), 
															cvPoint(aryMaxPoint[CheckStep].RightMinValueX, aryMaxPoint[CheckStep].RightMinValueY));

					AVR_AreaSize += AreaSizeList[CheckStep];
					aryMaxPoint[DetectCnt++] = DetectBuf[CheckStep];
				}

			}
			else 
				break;
		}

		BOOL CheckAreaSize = FALSE;
		if(DetectCnt>0)
		{
			AVR_AreaSize		= (AVR_AreaSize/DetectCnt);//*0.95;

			double MinArea = AVR_AreaSize;
			double MaxArea = AVR_AreaSize;
			for(int CheckStep = 0; CheckStep<DetectCnt; CheckStep++)
			{
				if(MaxArea <= AreaSizeList[CheckStep])
					MaxArea = AreaSizeList[CheckStep];

				if(MinArea >= AreaSizeList[CheckStep])
					MinArea = AreaSizeList[CheckStep];
			}
			if(abs(MaxArea-MinArea)>(AVR_AreaSize/2))
			{
				CheckAreaSize = TRUE;
			}
		}
		int ResultCnt =0;
		for(int CheckStep = 0; CheckStep<DetectCnt; CheckStep++)
		{
			if(DetectBuf[CheckStep].MaxValueX >= 0)//처음것부터 없으면 걍 나가자.
			{
				double NowAreaSize = Area3Point(	cvPoint(aryMaxPoint[CheckStep].MaxValueX, aryMaxPoint[CheckStep].MaxValueY), 
													cvPoint(aryMaxPoint[CheckStep].LeftMinValueX, aryMaxPoint[CheckStep].LeftMinValueY), 
													cvPoint(aryMaxPoint[CheckStep].RightMinValueX, aryMaxPoint[CheckStep].RightMinValueY));

				if(CheckAreaSize == TRUE)
				{
					if(	(AVR_AreaSize<=NowAreaSize) )////평균 면적이 작은것 제거.(80%미만)
					{
						DetectBuf[ResultCnt++] = aryMaxPoint[CheckStep];
					}
				}
				else
				{
					DetectBuf[ResultCnt++] = aryMaxPoint[CheckStep];
				}
			}
			else
			{//제거 항목 보기.
				break;
			}
		}
		//높이가 가장 높은것 부터 정렬(Detect가 가장 잘된것이라고 볼수 있다)-Left/Right의 단차가 적은것.
		MEASER_2D_BubbleSort(DetectBuf, ResultCnt);

		//찾은 Obj중에서 To에서 좌우로 급감부분을 찾는다.
		//필터링은 필요 없다.!!(위에서 절대 Data만을 찾았음으로)
		for(int CheckStep = 0; CheckStep<ResultCnt; CheckStep++)
		{

			//좌측
			OldValue =0;
			for(int WStep = DetectBuf[CheckStep].MaxValueX; WStep>=DetectBuf[CheckStep].LeftMinValueX; WStep--)
			{
				NowValue =	*(m_pNewData_3D->m_pDepthData+WStep+(m_pNewData_3D->m_Width*PositionY));

				if(OldValue<NowValue)
					OldValue = NowValue;

				//최 하위값 Limit결정.
				if(abs(OldValue-NowValue)>1000)//1000 Nano이상 급감부분찾기.
				{
					DetectBuf[CheckStep].LeftDnValueX = WStep;
					DetectBuf[CheckStep].LeftDnValueY = OldValue;
					break;
				}
			}

			OldValue = 0;
			for(int WStep = DetectBuf[CheckStep].MaxValueX; WStep<DetectBuf[CheckStep].RightMinValueX; WStep++)
			{
				NowValue =	*(m_pNewData_3D->m_pDepthData+WStep+(m_pNewData_3D->m_Width*PositionY));

				if(OldValue<NowValue)
					OldValue = NowValue;
					
				//최 하위값 Limit결정.
				if(abs(OldValue-NowValue)>1000)//1000 Nano이상 급감부분찾기.
				{
					DetectBuf[CheckStep].RightDnValueX= WStep;
					DetectBuf[CheckStep].RightDnValueY= OldValue;
					break;
				}
			}

		}

		if(pResultBuf != NULL)
			*pResultBuf = DetectBuf[0];
	}
}

void CMeasureProc::MeaserAreaStickType_Magnet(int PositionY, MEASER_2D *pResultBuf, int *pDepthDataList)
{

	MEASER_2D aryMaxPoint[MAX_TOP_FIND_COUNT*2];
	long OldValue =0;
	long NowValue =0;
	//////////////////////////////////////////////////////////////////////////
	//Max위치 찾기.
	BOOL AlreadyFindArea = FALSE;
	OldValue =0;
	long MaxHeightValue = 0;
	long MinHeightValue = 0;
	for(int FindStep= 0; FindStep<1024; FindStep++)//가장 높은값을 찾는다.
	{
		if(MaxHeightValue <pDepthDataList[FindStep])
			MaxHeightValue = pDepthDataList[FindStep];
	}
	MinHeightValue = MaxHeightValue;

	for(int FindStep= 0; FindStep<1024; FindStep++)//가장 높은값을 찾는다.
	{
		if(MinHeightValue >pDepthDataList[FindStep] && pDepthDataList[FindStep]>0)
			MinHeightValue = pDepthDataList[FindStep];
	}
	MEASER_2D bufUpEdge;
	MEASER_2D bufDnEdge;
	int PutIndex =0;
	//for(int MaxFindStep = 0; MaxFindStep<MAX_TOP_FIND_COUNT; MaxFindStep++)
	{
		OldValue = pDepthDataList[0];
		//단차가 나는 부위 검색.
		for(int FindStep= 5; FindStep<1024; FindStep++)
		{
			if(MAX_TOP_FIND_COUNT*2<(PutIndex+2))//너무 많으면 중간에 끊자.
				break;

			if(pDepthDataList[FindStep] ==0)//높이가 0이면 끝난것이다.
			{
				aryMaxPoint[PutIndex++] = bufUpEdge;

				bufUpEdge.LeftMinValueX = -1;
				bufUpEdge.LeftMinValueY = -1;

				aryMaxPoint[PutIndex++] = bufDnEdge;

				bufDnEdge.RightMinValueX = -1;
				bufDnEdge.RightMinValueY = -1;

				break;
			}

			if(abs(pDepthDataList[FindStep-2]-pDepthDataList[FindStep])>300)
			{
				if(pDepthDataList[FindStep-2]>pDepthDataList[FindStep])//하강.
				{
					if(bufUpEdge.LeftMinValueX>0)//상승에서 하강으로 변환 된것
					{
						aryMaxPoint[PutIndex++] = bufUpEdge;
						bufUpEdge.LeftMinValueX = -1;
						bufUpEdge.LeftMinValueY = -1;

						OldValue = pDepthDataList[FindStep];
						//OldValue = 0;
					}

					if(OldValue>pDepthDataList[FindStep] )
					{
						bufDnEdge.RightMinValueX = FindStep;
						OldValue = bufDnEdge.RightMinValueY = pDepthDataList[FindStep];
						//cvDrawCircle(pTragetImg, cvPoint(FindStep, (int)(pDepthDataList[FindStep]/m_PixelPerNano)), 5, CV_RGB(0,0,255), 1);
					}
				}
				else//상승.
				{
					if(bufDnEdge.RightMinValueX>0)//하강에서 상승으로 변환된것
					{
						aryMaxPoint[PutIndex++] = bufDnEdge;

						bufDnEdge.RightMinValueX = -1;
						bufDnEdge.RightMinValueY = -1;

						//OldValue = pDepthDataList[FindStep];
						OldValue = 0;
						//OldValue = MaxHeightValue;
					}

					if(OldValue<pDepthDataList[FindStep] && bufUpEdge.LeftMinValueX == (-1))
					{
						bufUpEdge.LeftMinValueX = FindStep-2;
						OldValue = bufUpEdge.LeftMinValueY = pDepthDataList[FindStep-2];
					}
				}			
			}
		}
	}

	if(bufUpEdge.LeftMinValueX !=(-1))
	{
		aryMaxPoint[PutIndex++] = bufUpEdge;

		bufUpEdge.LeftMinValueX = -1;
		bufUpEdge.LeftMinValueY = -1;
	}
	if(bufDnEdge.RightMinValueX !=(-1))
	{
		aryMaxPoint[PutIndex++] = bufDnEdge;

		bufDnEdge.RightMinValueX = -1;
		bufDnEdge.RightMinValueY = -1;
	}

	int ObjIndex = 0;
	MEASER_2D aryObjPoint[MAX_TOP_FIND_COUNT];
	BOOL bStartPoint = FALSE;
	for(int MaxFindStep = 0; MaxFindStep<MAX_TOP_FIND_COUNT*2; MaxFindStep++)
	{
		if(aryMaxPoint[MaxFindStep].LeftMinValueX != (-1))//Start Point
		{
			if(bStartPoint == FALSE)
			{
				aryObjPoint[ObjIndex].LeftMinValueY = aryMaxPoint[MaxFindStep].LeftMinValueY;
				aryObjPoint[ObjIndex].LeftMinValueX = aryMaxPoint[MaxFindStep].LeftMinValueX;
				bStartPoint =TRUE;
			}			
		}
		if(aryMaxPoint[MaxFindStep].RightMinValueX != (-1))//End Point 
		{
			if(bStartPoint == TRUE)
			{
				aryObjPoint[ObjIndex].RightMinValueY = aryMaxPoint[MaxFindStep].RightMinValueY;
				aryObjPoint[ObjIndex].RightMinValueX = aryMaxPoint[MaxFindStep].RightMinValueX;
				bStartPoint =FALSE;

				ObjIndex++;
			}
		}
	}

	//구분된 것의 Max위치를 찾는다.
	for(int MaxFindStep = 0; MaxFindStep<ObjIndex; MaxFindStep++)
	{
		OldValue = aryObjPoint[MaxFindStep].LeftMinValueY;
		if(abs(aryObjPoint[MaxFindStep].LeftMinValueX - aryObjPoint[MaxFindStep].RightMinValueX)>10)
		{
			for(int MaxStep =  aryObjPoint[MaxFindStep].LeftMinValueX+2; MaxStep<aryObjPoint[MaxFindStep].RightMinValueX; MaxStep++)//왼쪽에서 Max치를 찾는다.
			{
				if(abs(pDepthDataList[MaxStep-2]-pDepthDataList[MaxStep])>=0 &&
					pDepthDataList[MaxStep-2]>pDepthDataList[MaxStep])//상승 Line임으로 감소 최대치를 찾는다.
				{
					aryObjPoint[MaxFindStep].LeftDnValueX = MaxStep-2;
					aryObjPoint[MaxFindStep].LeftDnValueY = pDepthDataList[MaxStep-2];

					aryObjPoint[MaxFindStep].RightDnValueX = aryObjPoint[MaxFindStep].LeftDnValueX;
					aryObjPoint[MaxFindStep].RightDnValueY = aryObjPoint[MaxFindStep].LeftDnValueY;


					aryObjPoint[MaxFindStep].MaxValueX = aryObjPoint[MaxFindStep].LeftDnValueX;
					aryObjPoint[MaxFindStep].MaxValueY = aryObjPoint[MaxFindStep].LeftDnValueY;
					break;
				}
			}
			for(int MaxStep =  aryObjPoint[MaxFindStep].RightMinValueX; MaxStep>=aryObjPoint[MaxFindStep].LeftDnValueX; MaxStep--)//오른쪽에서 왼쪽에 찾은것까지 또 Max치를 찾는다.
			{

				if(abs(pDepthDataList[MaxStep-2] - pDepthDataList[MaxStep])>=0 &&
					pDepthDataList[MaxStep-2]<=pDepthDataList[MaxStep])//상승 Line임으로 감소 최대치를 찾는다.
				{
					aryObjPoint[MaxFindStep].RightDnValueX = MaxStep;
					aryObjPoint[MaxFindStep].RightDnValueY = pDepthDataList[MaxStep];

					if(aryObjPoint[MaxFindStep].MaxValueY<aryObjPoint[MaxFindStep].RightDnValueY)
					{
						aryObjPoint[MaxFindStep].MaxValueX = aryObjPoint[MaxFindStep].RightDnValueX;
						aryObjPoint[MaxFindStep].MaxValueY = aryObjPoint[MaxFindStep].RightDnValueY;
					}

					break;
				}
			}
		}
	}

	ProcMsgQ DataList;
	ProcMsgQ DataListBuf;
	DataList.QClean();
	DataListBuf.QClean();

	MEASER_2D checkNode ;
	double MaxDis		= 0.0f;
	double checkDis	= 0.0f;
	double checkDnDis	= 0.0f;
	int checkHDis	= 0;
	int checkHDis2	= 0;

	for(int MaxFindStep = 0; MaxFindStep<ObjIndex; MaxFindStep++)
	{
		BOOL IsSkip = FALSE;
		if(aryObjPoint[MaxFindStep].LeftMinValueX <0 ||
			aryObjPoint[MaxFindStep].LeftMinValueY <0 ||
			aryObjPoint[MaxFindStep].RightMinValueX <0 ||
			aryObjPoint[MaxFindStep].RightMinValueY <0 )
			IsSkip = TRUE;
		checkDis = dist_2d(aryObjPoint[MaxFindStep].LeftMinValueX*m_PixelPerNano, aryObjPoint[MaxFindStep].LeftMinValueY, aryObjPoint[MaxFindStep].RightMinValueX*m_PixelPerNano, aryObjPoint[MaxFindStep].RightMinValueY);

		if(MaxDis<checkDis  && IsSkip == FALSE)
		{
			MaxDis = checkDis;
		}

		int MinDis = abs(aryObjPoint[MaxFindStep].LeftMinValueX - aryObjPoint[MaxFindStep].RightDnValueX);
		int DnDis = abs(aryObjPoint[MaxFindStep].LeftDnValueX - aryObjPoint[MaxFindStep].RightDnValueX);
		//Double Line중에 단차가 많이 나는건 오류로 제거 한다.
		if(abs(aryObjPoint[MaxFindStep].LeftDnValueY - aryObjPoint[MaxFindStep].RightDnValueY)<9000 && IsSkip == FALSE)
			DataList.QPutNode(&aryObjPoint[MaxFindStep]);
	}

	//일정 높이가 안되면 제거 한다.
	//long AvrHight = (long)DataList.QGetAvrHight();
	//long AvrWidth = (long)DataList.QGetAvrWidth(m_PixelPerNano);
	int InputCCnt = 0;
	MEASER_2D CBufNode[100];

	MEASER_2D CenterNode ;
	int ListCnt = DataList.QGetCnt();
	int AVRWidthHalfPixel= DataList.QGetAvrWidth_WidthPixel()/2;
	int HalfWidth = 0;
	for(int CheckStep = 0; CheckStep<ListCnt; CheckStep++)
	{
		checkNode = DataList.QGetNode();

		checkDis = dist_2d(checkNode.LeftMinValueX*m_PixelPerNano, checkNode.LeftMinValueY, checkNode.RightMinValueX*m_PixelPerNano, checkNode.RightMinValueY);
		HalfWidth = abs(checkNode.LeftMinValueX - checkNode.RightMinValueX);



		checkDnDis = dist_2d(checkNode.LeftDnValueX*m_PixelPerNano, checkNode.LeftDnValueY, checkNode.RightDnValueX*m_PixelPerNano, checkNode.RightDnValueY);
		checkHDis = abs(checkNode.LeftMinValueY - checkNode.RightMinValueY);
		checkHDis2= abs(checkNode.LeftDnValueY - checkNode.RightDnValueY);

		if(AVRWidthHalfPixel< HalfWidth && 
			(checkNode.LeftMinValueX > 4 && checkNode.RightMinValueX < 1020) &&
			//checkDis/2>checkDnDis &&
			checkHDis<5000&&
			checkHDis2<2000)
		{

			//if( abs(abs(CenterNode.LeftMinValueX -CenterNode.RightMinValueX)- 1024/2) > abs(abs(checkNode.LeftMinValueX -checkNode.RightMinValueX)- 1024/2))
			{


				//각도 계산용 좌측 계산.
				//Angle Position 찾기.
				//CenterNode.MaxValueX
				CvPoint StartPoint, EndPoint, BufPoint;
				//StartPoint.x = CenterNode.LeftMinValueX;
				//StartPoint.y = (int)(CenterNode.LeftMinValueY/m_PixelPerNano);
				//BufPoint.x = CenterNode.LeftDnValueX;
				//BufPoint.y = (int)(CenterNode.LeftDnValueY/m_PixelPerNano);
				//dist_2PointCenter(StartPoint.x, StartPoint.y, BufPoint.x, BufPoint.y,  &EndPoint.x, &EndPoint.y );

				//직선 방정식~
				//float AvxVal = (float)(StartPoint.y - EndPoint.y)/(float)(StartPoint.x - EndPoint.x);//기울기.
				//float nValue =  (AvxVal*StartPoint.x) - StartPoint.y;//절편
				StartPoint.x = checkNode.LeftMinValueX;
				StartPoint.y = checkNode.LeftMinValueY;
				BufPoint.x = checkNode.LeftDnValueX;
				BufPoint.y = checkNode.LeftDnValueY;
				dist_2PointCenter(StartPoint.x, StartPoint.y, BufPoint.x, BufPoint.y,  &EndPoint.x, &EndPoint.y );
				float AvxVal = (float)(StartPoint.y - EndPoint.y)/(float)(StartPoint.x - EndPoint.x);//기울기.
				float nValue =  (AvxVal*StartPoint.x) - StartPoint.y;//절편

				float MaxDisAngle = 0.0f;
				float OldDisAngle = 0.0f;
				for(int axisXStep=StartPoint.x;axisXStep<EndPoint.x; axisXStep++)
				{
					float NewY= AvxVal*axisXStep -(nValue);
					//cvDrawLine(pTragetImg,cvPoint(StartPoint.x,(int)(StartPoint.y/m_PixelPerNano)), cvPoint(axisXStep,(int)(NewY/m_PixelPerNano)), CV_RGB(0,255,255), 2);
					MaxDisAngle = (float)dist_2d(axisXStep, (int)NewY, axisXStep, pDepthDataList[axisXStep]);
					if(MaxDisAngle>OldDisAngle)
					{
						OldDisAngle= MaxDisAngle;
						checkNode.LeftAngleValueX = axisXStep;
						checkNode.LeftAngleValueY = pDepthDataList[axisXStep];
					}
				}
				//cvDrawCircle(pTragetImg,cvPoint(CenterNode.LeftAngleValueX,(int)(CenterNode.LeftAngleValueY/m_PixelPerNano)), 2, CV_RGB(255,0,255),2);
				//각도 계산용 우측 계산
				StartPoint.x = checkNode.RightMinValueX;
				StartPoint.y = checkNode.RightMinValueY;
				BufPoint.x = checkNode.RightDnValueX;
				BufPoint.y = checkNode.RightDnValueY;
				dist_2PointCenter(StartPoint.x, StartPoint.y, BufPoint.x, BufPoint.y,  &EndPoint.x, &EndPoint.y );

				BufPoint = EndPoint;
				EndPoint = StartPoint;
				StartPoint = BufPoint;

				AvxVal = (float)(StartPoint.y - EndPoint.y)/(float)(StartPoint.x - EndPoint.x);//기울기.
				nValue =  (AvxVal*StartPoint.x) - StartPoint.y;//절편

				MaxDisAngle = 0.0f;
				OldDisAngle = 0.0f;
				for(int axisXStep=StartPoint.x;axisXStep<EndPoint.x; axisXStep++)
				{
					float NewY= AvxVal*axisXStep -(nValue);
					//cvDrawLine(pTragetImg,cvPoint(StartPoint.x,(int)(StartPoint.y/m_PixelPerNano)), cvPoint(axisXStep,(int)(NewY/m_PixelPerNano)), CV_RGB(0,255,255), 2);
					MaxDisAngle = (float)dist_2d(axisXStep, (int)NewY, axisXStep, pDepthDataList[axisXStep]);
					if(MaxDisAngle>OldDisAngle)
					{
						OldDisAngle= MaxDisAngle;
						checkNode.RightAngleValueX = axisXStep;
						checkNode.RightAngleValueY = pDepthDataList[axisXStep];
					}
				}
				CBufNode[InputCCnt++] = checkNode;
			}
		}
	}

	*pResultBuf = CBufNode[0];
	//for(int CpStep = 0; CpStep<InputCCnt && CpStep<100; CpStep++)
	//{
	//	pResultBuf[CpStep] = CBufNode[CpStep];
	//}

}
CvPoint CMeasureProc::FindHoleAreaDotType(DATA_3D_RAW *pNewData_3D, long HoleDetectDepth, BYTE GrayHoleThreshold)
{
	//검사 위치 선정
	ProcMsgQ AxisYDataList;
	MEASER_2D TestNode;
	IplImage *pLabeled  = cvCreateImage(cvSize(1024,768), IPL_DEPTH_8U, 1);   
	cvZero(pLabeled);

	BYTE * pGrayData = NULL;
	long * pDepthData = NULL;
	//long * pDepthBuf = NULL;
	//Max Min Value 찾기.
	long MaxValue = 0;
	for(int ImgStepH =  0; ImgStepH< pNewData_3D->m_Height; ImgStepH++)
	{
		for(int ImgStepW =  0; ImgStepW< pNewData_3D->m_Width; ImgStepW++)
		{
			pDepthData = (pNewData_3D->m_pDepthData+(ImgStepH*pNewData_3D->m_Width)+ImgStepW);
			if(MaxValue < *pDepthData)
			{
				MaxValue = *pDepthData;
			}

		}
	}

	long MinValue = MaxValue;
	for(int ImgStepH =  0; ImgStepH< pNewData_3D->m_Height; ImgStepH++)
	{
		for(int ImgStepW =  0; ImgStepW<pNewData_3D->m_Width; ImgStepW++)
		{
			pDepthData = (pNewData_3D->m_pDepthData+(ImgStepH*pNewData_3D->m_Width)+ImgStepW);
			if(MinValue > *pDepthData)
			{
				MinValue = *pDepthData;
			}
		}
	}
	//Base Image생성.Depth Image 생성(Gray Level과 Depth로 Hole감지.)
	for(int ImgStepH =  0; ImgStepH< pNewData_3D->m_Height; ImgStepH++)
	{
		for(int ImgStepW =  0; ImgStepW<pNewData_3D->m_Width; ImgStepW++)
		{
			pGrayData = (BYTE *)(pLabeled->imageData+(ImgStepH*pNewData_3D->m_Width)+ImgStepW);
			pDepthData = (pNewData_3D->m_pDepthData+(ImgStepH*pNewData_3D->m_Width)+ImgStepW);
			if( *pDepthData < MinValue+HoleDetectDepth && *pDepthData > MinValue-HoleDetectDepth)
			{
				*pGrayData = 255;
			}
		}
	}


	CvRect SelectedNode;
	memset(&SelectedNode, 0x00, sizeof(CvRect));
	CvPoint CenterPos;
	CBlobLabeling blob;

	//영상 노이즈 제거.
	cvErode(pLabeled, pLabeled, NULL, 5);

	blob.SetParam(pLabeled, 1);
	blob.DoLabeling();

	int				detLineCnt = 0;
	DLineData detectLine[10000];
	int DetectCnt =0;
	int MaxAreaSize = 0;
	for(int i = 0; i<blob.m_nBlobs; i++)
	{
		int SizeBuffer = (blob.m_recBlobs[i].width * blob.m_recBlobs[i].height);
		if(MaxAreaSize < SizeBuffer)
		{
			MaxAreaSize = SizeBuffer;
		}
	}

	for(int i = 0; i<blob.m_nBlobs; i++)
	{
		if((blob.m_recBlobs[i].width*blob.m_recBlobs[i].height)>(SelectedNode.width*SelectedNode.height))
		{
			SelectedNode.width = blob.m_recBlobs[i].width;
			SelectedNode.height = blob.m_recBlobs[i].height;

			CenterPos.x = blob.m_recBlobs[i].x +(blob.m_recBlobs[i].width/2);
			CenterPos.y = blob.m_recBlobs[i].y +(blob.m_recBlobs[i].height/2);

		}
#ifdef PROC_DOT_FIND_IMG
		cvDrawRect(pLabeled, cvPoint(blob.m_recBlobs[i].x , blob.m_recBlobs[i].y), cvPoint(blob.m_recBlobs[i].x+blob.m_recBlobs[i].width , blob.m_recBlobs[i].y+blob.m_recBlobs[i].height), CV_RGB(255,255,255), 2);
#endif
		if((MaxAreaSize/3)<(blob.m_recBlobs[i].width * blob.m_recBlobs[i].height))
		{

			detectLine[detLineCnt].StartPoint.x = blob.m_recBlobs[i].x+blob.m_recBlobs[i].width/2;
			detectLine[detLineCnt].StartPoint.y = 0;

			detectLine[detLineCnt].EndPoint.x = blob.m_recBlobs[i].x+blob.m_recBlobs[i].width/2;
			detectLine[detLineCnt].EndPoint.y = 767;
			detLineCnt++;

			detectLine[detLineCnt].StartPoint.x = 0;
			detectLine[detLineCnt].StartPoint.y = blob.m_recBlobs[i].y+blob.m_recBlobs[i].height/2;

			detectLine[detLineCnt].EndPoint.x = 1023;
			detectLine[detLineCnt].EndPoint.y = blob.m_recBlobs[i].y+blob.m_recBlobs[i].height/2;

			detLineCnt++;
#ifdef PROC_DOT_FIND_IMG
			cvLine(pLabeled,	cvPoint(blob.m_recBlobs[i].x+blob.m_recBlobs[i].width/2 , 0),
				cvPoint(blob.m_recBlobs[i].x+blob.m_recBlobs[i].width/2 , 768),
				CV_RGB(255,255,255), 1);

			cvLine(pLabeled,	cvPoint(0 , blob.m_recBlobs[i].y+blob.m_recBlobs[i].height/2),
				cvPoint(1024 , blob.m_recBlobs[i].y+blob.m_recBlobs[i].height/2),
				CV_RGB(255,255,255), 1);
#endif
		}
	}

	if(m_MeasurDotAngle == 0.0)
	{
		//선들의 교점을 구한다.
		int			CrossCnt = 0;
		CvPoint CrossPoints[10000];

		int InputCnt = 0;
		for(int LFindStep = 0; LFindStep<detLineCnt; LFindStep++)
		{
			for(int L2FindStep = 0; L2FindStep<detLineCnt; L2FindStep++)
			{
				if(GetIntersectPoint(detectLine[LFindStep].StartPoint, detectLine[LFindStep].EndPoint, detectLine[L2FindStep].StartPoint, detectLine[L2FindStep].EndPoint, &CrossPoints[CrossCnt]) == TRUE)
				{
					POINT CheckP;

					CheckP.x = CrossPoints[CrossCnt].x;
					CheckP.y = CrossPoints[CrossCnt].y;
					CRect RetBuf;
					BOOL InputPT = FALSE;
					//홀 안에꺼는 뺀다.
					for(int i = 0; i<blob.m_nBlobs; i++)
					{
						RetBuf.left = blob.m_recBlobs[i].x;
						RetBuf.top = blob.m_recBlobs[i].y;
						RetBuf.right = blob.m_recBlobs[i].x+blob.m_recBlobs[i].width;
						RetBuf.bottom = blob.m_recBlobs[i].y+blob.m_recBlobs[i].height;
						if(RetBuf.PtInRect( CheckP) == TRUE)
						{
							InputPT = TRUE;
							InputCnt++;
						}
					}
					if(InputPT != TRUE)
						CrossCnt++;

					if(CrossCnt>10000)
					{
						break;
					}
				}
			}
			if(CrossCnt>10000)
			{
				CrossCnt = 10000;
				break;
			}
		}

		BOOL DetecWX = FALSE;
		BOOL DetecWY = FALSE;
		BOOL DetecWX2 = FALSE;
		BOOL DetecWY2 = FALSE;
		int	       DetectPCnt = 0;
		CvPoint DetectPoints[10000];

		if(CrossCnt>10000)
		{
			CrossCnt = 10000;
		}

		for(int LFindStep = 0; LFindStep<CrossCnt; LFindStep++)
		{
			DetecWX = FALSE;
			DetecWY = FALSE;
			DetecWX2 = FALSE;
			DetecWY2 = FALSE;
			BYTE * pDData = NULL;
			//X축에대해 측정 가능 한가.
			for(int WStep = CrossPoints[LFindStep].x; WStep<1024; WStep++ )//오른쪽 화이트가 있는가.
			{
				pDData = (BYTE*)(pLabeled->imageData+WStep+(CrossPoints[LFindStep].y*1024));
				if(*pDData > 200)
				{
					DetecWX = TRUE;
					break;
				}
			}
			for(int WStep = CrossPoints[LFindStep].x; WStep>=0 && DetecWX == TRUE; WStep-- )//오른쪽 화이트가 있는가.
			{
				pDData = (BYTE*)(pLabeled->imageData+WStep+(CrossPoints[LFindStep].y*1024));
				if(*pDData > 200)
				{
					DetecWX2 = TRUE;
					break;
				}
			}

			//Y축에대해 측정 가능 한가.
			for(int HStep = CrossPoints[LFindStep].y; HStep<768; HStep++ )//오른쪽 화이트가 있는가.
			{
				pDData = (BYTE*)(pLabeled->imageData+CrossPoints[LFindStep].x+(HStep*1024));
				if(*pDData > 200)
				{
					DetecWY = TRUE;
					break;
				}
			}
			for(int HStep = CrossPoints[LFindStep].y; HStep>=0 && DetecWY == TRUE; HStep-- )//오른쪽 화이트가 있는가.
			{
				pDData = (BYTE*)(pLabeled->imageData+CrossPoints[LFindStep].x+(HStep*1024));
				if(*pDData > 200)
				{
					DetecWY2 = TRUE;
					break;
				}
			}

			if(DetecWX && DetecWY && DetecWX2 && DetecWY2)
			{
				DetectPoints[DetectPCnt] = CrossPoints[LFindStep];
				DetectPCnt++;
			}

		}
		for(int LFindStep = 0; LFindStep<DetectPCnt; LFindStep++)
		{
			cvDrawCircle(pLabeled, DetectPoints[LFindStep], 5, CV_RGB(128,128,128), 2);
		}



		if(DetectPCnt==0)
		{
			CenterPos.x = -1;
			CenterPos.y = -1;
		}
		else
		{
			//중앙에서 가까운거리 찾기
			double MinDisCenter = 1000000.0;
			double MinDisCBuf = MinDisCenter;
			for(int CStep = 0; CStep<DetectPCnt; CStep++ )
			{
				MinDisCBuf = dist_2d(DetectPoints[CStep].x, DetectPoints[CStep].y, 1024/2, 768/2);
				if(MinDisCBuf<MinDisCenter)
				{
					CenterPos.x = DetectPoints[CStep].x;
					CenterPos.y = DetectPoints[CStep].y;
					MinDisCenter = MinDisCBuf;
				}
			}
		}
	}
	cvReleaseImage(&pLabeled);
	return CenterPos;
}

void CMeasureProc::MeaserAreaDotType(int *pMeaserCnt, MEASER_2D *pResultBuf, int *pDepthDataList, IplImage *pTragetImg)
{




	MEASER_2D aryMaxPoint[MAX_TOP_FIND_COUNT*2];
	long OldValue =0;
	long NowValue =0;
	//////////////////////////////////////////////////////////////////////////
	//Max위치 찾기.
	BOOL AlreadyFindArea = FALSE;
	OldValue =0;
	long MaxHeightValue = 0;
	long MinHeightValue = 0;
	for(int FindStep= 0; FindStep<1024; FindStep++)//가장 높은값을 찾는다.
	{
		if(MaxHeightValue <pDepthDataList[FindStep])
			MaxHeightValue = pDepthDataList[FindStep];
	}
	MinHeightValue = MaxHeightValue;

	for(int FindStep= 0; FindStep<1024; FindStep++)//가장 높은값을 찾는다.
	{
		if(MinHeightValue >pDepthDataList[FindStep] && pDepthDataList[FindStep]>0)
			MinHeightValue = pDepthDataList[FindStep];
	}
	MEASER_2D bufUpEdge;
	MEASER_2D bufDnEdge;
	int PutIndex =0;
	//for(int MaxFindStep = 0; MaxFindStep<MAX_TOP_FIND_COUNT; MaxFindStep++)
	{
		OldValue = pDepthDataList[0];
		//단차가 나는 부위 검색.
		for(int FindStep= 5; FindStep<1024; FindStep++)
		{
			if(MAX_TOP_FIND_COUNT*2<(PutIndex+2))//너무 많으면 중간에 끊자.
				break;

			if(pDepthDataList[FindStep] ==0)//높이가 0이면 끝난것이다.
			{
				aryMaxPoint[PutIndex++] = bufUpEdge;

				bufUpEdge.LeftMinValueX = -1;
				bufUpEdge.LeftMinValueY = -1;

				aryMaxPoint[PutIndex++] = bufDnEdge;

				bufDnEdge.RightMinValueX = -1;
				bufDnEdge.RightMinValueY = -1;

				break;
			}

			if(abs(pDepthDataList[FindStep-2]-pDepthDataList[FindStep])>300)
			{
				if(pDepthDataList[FindStep-2]>pDepthDataList[FindStep])//하강.
				{
					if(bufUpEdge.LeftMinValueX>0)//상승에서 하강으로 변환 된것
					{
						aryMaxPoint[PutIndex++] = bufUpEdge;
						bufUpEdge.LeftMinValueX = -1;
						bufUpEdge.LeftMinValueY = -1;

						OldValue = pDepthDataList[FindStep];
						//OldValue = 0;
					}

					if(OldValue>pDepthDataList[FindStep] )
					{
						bufDnEdge.RightMinValueX = FindStep;
						OldValue = bufDnEdge.RightMinValueY = pDepthDataList[FindStep];
						//cvDrawCircle(pTragetImg, cvPoint(FindStep, (int)(pDepthDataList[FindStep]/m_PixelPerNano)), 5, CV_RGB(0,0,255), 1);
					}
				}
				else//상승.
				{
					if(bufDnEdge.RightMinValueX>0)//하강에서 상승으로 변환된것
					{
						aryMaxPoint[PutIndex++] = bufDnEdge;

						bufDnEdge.RightMinValueX = -1;
						bufDnEdge.RightMinValueY = -1;

						//OldValue = pDepthDataList[FindStep];
						OldValue = 0;
						//OldValue = MaxHeightValue;
					}

					if(OldValue<pDepthDataList[FindStep] && bufUpEdge.LeftMinValueX == (-1))
					{
						bufUpEdge.LeftMinValueX = FindStep-2;
						OldValue = bufUpEdge.LeftMinValueY = pDepthDataList[FindStep-2];
					}
				}			
			}
		}
	}

	if(bufUpEdge.LeftMinValueX !=(-1))
	{
		aryMaxPoint[PutIndex++] = bufUpEdge;

		bufUpEdge.LeftMinValueX = -1;
		bufUpEdge.LeftMinValueY = -1;
	}
	if(bufDnEdge.RightMinValueX !=(-1))
	{
		aryMaxPoint[PutIndex++] = bufDnEdge;

		bufDnEdge.RightMinValueX = -1;
		bufDnEdge.RightMinValueY = -1;
	}

	int ObjIndex = 0;
	MEASER_2D aryObjPoint[MAX_TOP_FIND_COUNT];
	BOOL bStartPoint = FALSE;
	for(int MaxFindStep = 0; MaxFindStep<MAX_TOP_FIND_COUNT*2; MaxFindStep++)
	{
		if(aryMaxPoint[MaxFindStep].LeftMinValueX != (-1))//Start Point
		{
			if(bStartPoint == FALSE)
			{
				aryObjPoint[ObjIndex].LeftMinValueY = aryMaxPoint[MaxFindStep].LeftMinValueY;
				aryObjPoint[ObjIndex].LeftMinValueX = aryMaxPoint[MaxFindStep].LeftMinValueX;
				bStartPoint =TRUE;
			}			
		}
		if(aryMaxPoint[MaxFindStep].RightMinValueX != (-1))//End Point 
		{
			if(bStartPoint == TRUE)
			{
				aryObjPoint[ObjIndex].RightMinValueY = aryMaxPoint[MaxFindStep].RightMinValueY;
				aryObjPoint[ObjIndex].RightMinValueX = aryMaxPoint[MaxFindStep].RightMinValueX;
				bStartPoint =FALSE;

				ObjIndex++;
			}
		}
	}

	//구분된 것의 Max위치를 찾는다.
	for(int MaxFindStep = 0; MaxFindStep<ObjIndex; MaxFindStep++)
	{
		OldValue = aryObjPoint[MaxFindStep].LeftMinValueY;
		if(abs(aryObjPoint[MaxFindStep].LeftMinValueX - aryObjPoint[MaxFindStep].RightMinValueX)>10)
		{
			for(int MaxStep =  aryObjPoint[MaxFindStep].LeftMinValueX+2; MaxStep<aryObjPoint[MaxFindStep].RightMinValueX; MaxStep++)//왼쪽에서 Max치를 찾는다.
			{
				if(abs(pDepthDataList[MaxStep-2]-pDepthDataList[MaxStep])>=0 &&
					pDepthDataList[MaxStep-2]>pDepthDataList[MaxStep])//상승 Line임으로 감소 최대치를 찾는다.
				{
					aryObjPoint[MaxFindStep].LeftDnValueX = MaxStep-2;
					aryObjPoint[MaxFindStep].LeftDnValueY = pDepthDataList[MaxStep-2];

					aryObjPoint[MaxFindStep].RightDnValueX = aryObjPoint[MaxFindStep].LeftDnValueX;
					aryObjPoint[MaxFindStep].RightDnValueY = aryObjPoint[MaxFindStep].LeftDnValueY;


					aryObjPoint[MaxFindStep].MaxValueX = aryObjPoint[MaxFindStep].LeftDnValueX;
					aryObjPoint[MaxFindStep].MaxValueY = aryObjPoint[MaxFindStep].LeftDnValueY;
					break;
				}
			}
			for(int MaxStep =  aryObjPoint[MaxFindStep].RightMinValueX; MaxStep>=aryObjPoint[MaxFindStep].LeftDnValueX; MaxStep--)//오른쪽에서 왼쪽에 찾은것까지 또 Max치를 찾는다.
			{

				if(abs(pDepthDataList[MaxStep-2] - pDepthDataList[MaxStep])>=0 &&
					pDepthDataList[MaxStep-2]<=pDepthDataList[MaxStep])//상승 Line임으로 감소 최대치를 찾는다.
				{
					aryObjPoint[MaxFindStep].RightDnValueX = MaxStep;
					aryObjPoint[MaxFindStep].RightDnValueY = pDepthDataList[MaxStep];

					if(aryObjPoint[MaxFindStep].MaxValueY<aryObjPoint[MaxFindStep].RightDnValueY)
					{
						aryObjPoint[MaxFindStep].MaxValueX = aryObjPoint[MaxFindStep].RightDnValueX;
						aryObjPoint[MaxFindStep].MaxValueY = aryObjPoint[MaxFindStep].RightDnValueY;
					}
					
					break;
				}
			}
		}
	}

	ProcMsgQ DataList;
	ProcMsgQ DataListBuf;
	DataList.QClean();
	DataListBuf.QClean();

	MEASER_2D checkNode ;
	double MaxDis		= 0.0f;
	double checkDis	= 0.0f;
	double checkDnDis	= 0.0f;
	int checkHDis	= 0;
	int checkHDis2	= 0;

	for(int MaxFindStep = 0; MaxFindStep<ObjIndex; MaxFindStep++)
	{
		BOOL IsSkip = FALSE;
		if(aryObjPoint[MaxFindStep].LeftMinValueX <0 ||
			aryObjPoint[MaxFindStep].LeftMinValueY <0 ||
			aryObjPoint[MaxFindStep].RightMinValueX <0 ||
			aryObjPoint[MaxFindStep].RightMinValueY <0 )
			IsSkip = TRUE;
		checkDis = dist_2d(aryObjPoint[MaxFindStep].LeftMinValueX*m_PixelPerNano, aryObjPoint[MaxFindStep].LeftMinValueY, aryObjPoint[MaxFindStep].RightMinValueX*m_PixelPerNano, aryObjPoint[MaxFindStep].RightMinValueY);

		if(MaxDis<checkDis  && IsSkip == FALSE)
		{
			MaxDis = checkDis;
		}

		int MinDis = abs(aryObjPoint[MaxFindStep].LeftMinValueX - aryObjPoint[MaxFindStep].RightDnValueX);
		int DnDis = abs(aryObjPoint[MaxFindStep].LeftDnValueX - aryObjPoint[MaxFindStep].RightDnValueX);
		//Double Line중에 단차가 많이 나는건 오류로 제거 한다.
		if(abs(aryObjPoint[MaxFindStep].LeftDnValueY - aryObjPoint[MaxFindStep].RightDnValueY)<9000 && IsSkip == FALSE)
			DataList.QPutNode(&aryObjPoint[MaxFindStep]);
	}
	
	//일정 높이가 안되면 제거 한다.
	//long AvrHight = (long)DataList.QGetAvrHight();
	//long AvrWidth = (long)DataList.QGetAvrWidth(m_PixelPerNano);
	int InputCCnt = 0;
	MEASER_2D CBufNode[100];

	MEASER_2D CenterNode ;
	int ListCnt = DataList.QGetCnt();
	int AVRWidthHalfPixel= DataList.QGetAvrWidth_WidthPixel()/2;
	int HalfWidth = 0;
	for(int CheckStep = 0; CheckStep<ListCnt; CheckStep++)
	{
		checkNode = DataList.QGetNode();

		checkDis = dist_2d(checkNode.LeftMinValueX*m_PixelPerNano, checkNode.LeftMinValueY, checkNode.RightMinValueX*m_PixelPerNano, checkNode.RightMinValueY);
		HalfWidth = abs(checkNode.LeftMinValueX - checkNode.RightMinValueX);



		checkDnDis = dist_2d(checkNode.LeftDnValueX*m_PixelPerNano, checkNode.LeftDnValueY, checkNode.RightDnValueX*m_PixelPerNano, checkNode.RightDnValueY);
		checkHDis = abs(checkNode.LeftMinValueY - checkNode.RightMinValueY);
		checkHDis2= abs(checkNode.LeftDnValueY - checkNode.RightDnValueY);

		if(AVRWidthHalfPixel< HalfWidth && 
			(checkNode.LeftMinValueX > 4 && checkNode.RightMinValueX < 1020) &&
			//checkDis/2>checkDnDis &&
			checkHDis<5000&&
			checkHDis2<2000)
		{

			//if( abs(abs(CenterNode.LeftMinValueX -CenterNode.RightMinValueX)- 1024/2) > abs(abs(checkNode.LeftMinValueX -checkNode.RightMinValueX)- 1024/2))
			{
				

				//각도 계산용 좌측 계산.
				//Angle Position 찾기.
				//CenterNode.MaxValueX
				CvPoint StartPoint, EndPoint, BufPoint;
				//StartPoint.x = CenterNode.LeftMinValueX;
				//StartPoint.y = (int)(CenterNode.LeftMinValueY/m_PixelPerNano);
				//BufPoint.x = CenterNode.LeftDnValueX;
				//BufPoint.y = (int)(CenterNode.LeftDnValueY/m_PixelPerNano);
				//dist_2PointCenter(StartPoint.x, StartPoint.y, BufPoint.x, BufPoint.y,  &EndPoint.x, &EndPoint.y );

				//직선 방정식~
				//float AvxVal = (float)(StartPoint.y - EndPoint.y)/(float)(StartPoint.x - EndPoint.x);//기울기.
				//float nValue =  (AvxVal*StartPoint.x) - StartPoint.y;//절편
				StartPoint.x = checkNode.LeftMinValueX;
				StartPoint.y = checkNode.LeftMinValueY;
				BufPoint.x = checkNode.LeftDnValueX;
				BufPoint.y = checkNode.LeftDnValueY;
				dist_2PointCenter(StartPoint.x, StartPoint.y, BufPoint.x, BufPoint.y,  &EndPoint.x, &EndPoint.y );
				float AvxVal = (float)(StartPoint.y - EndPoint.y)/(float)(StartPoint.x - EndPoint.x);//기울기.
				float nValue =  (AvxVal*StartPoint.x) - StartPoint.y;//절편

				float MaxDisAngle = 0.0f;
				float OldDisAngle = 0.0f;
				for(int axisXStep=StartPoint.x;axisXStep<EndPoint.x; axisXStep++)
				{
					float NewY= AvxVal*axisXStep -(nValue);
					//cvDrawLine(pTragetImg,cvPoint(StartPoint.x,(int)(StartPoint.y/m_PixelPerNano)), cvPoint(axisXStep,(int)(NewY/m_PixelPerNano)), CV_RGB(0,255,255), 2);
					MaxDisAngle = (float)dist_2d(axisXStep, (int)NewY, axisXStep, pDepthDataList[axisXStep]);
					if(MaxDisAngle>OldDisAngle)
					{
						OldDisAngle= MaxDisAngle;
						checkNode.LeftAngleValueX = axisXStep;
						checkNode.LeftAngleValueY = pDepthDataList[axisXStep];
					}
				}
				//cvDrawCircle(pTragetImg,cvPoint(CenterNode.LeftAngleValueX,(int)(CenterNode.LeftAngleValueY/m_PixelPerNano)), 2, CV_RGB(255,0,255),2);
				//각도 계산용 우측 계산
				StartPoint.x = checkNode.RightMinValueX;
				StartPoint.y = checkNode.RightMinValueY;
				BufPoint.x = checkNode.RightDnValueX;
				BufPoint.y = checkNode.RightDnValueY;
				dist_2PointCenter(StartPoint.x, StartPoint.y, BufPoint.x, BufPoint.y,  &EndPoint.x, &EndPoint.y );

				BufPoint = EndPoint;
				EndPoint = StartPoint;
				StartPoint = BufPoint;

				AvxVal = (float)(StartPoint.y - EndPoint.y)/(float)(StartPoint.x - EndPoint.x);//기울기.
				nValue =  (AvxVal*StartPoint.x) - StartPoint.y;//절편

				MaxDisAngle = 0.0f;
				OldDisAngle = 0.0f;
				for(int axisXStep=StartPoint.x;axisXStep<EndPoint.x; axisXStep++)
				{
					float NewY= AvxVal*axisXStep -(nValue);
					//cvDrawLine(pTragetImg,cvPoint(StartPoint.x,(int)(StartPoint.y/m_PixelPerNano)), cvPoint(axisXStep,(int)(NewY/m_PixelPerNano)), CV_RGB(0,255,255), 2);
					MaxDisAngle = (float)dist_2d(axisXStep, (int)NewY, axisXStep, pDepthDataList[axisXStep]);
					if(MaxDisAngle>OldDisAngle)
					{
						OldDisAngle= MaxDisAngle;
						checkNode.RightAngleValueX = axisXStep;
						checkNode.RightAngleValueY = pDepthDataList[axisXStep];
					}
				}
				CBufNode[InputCCnt++] = checkNode;
			}
		}
	}

	*pMeaserCnt = InputCCnt;

	for(int CpStep = 0; CpStep<InputCCnt && CpStep<100; CpStep++)
	{
		pResultBuf[CpStep] = CBufNode[CpStep];
	}
}
void CMeasureProc::DrawMeaserData()
{
	if(m_pAxis_H_CalcImg != NULL)
	{
		CvvImage ViewImage;
		ViewImage.Create(m_RectViewStick.Width(), m_RectViewStick.Height(), ((IPL_DEPTH_8U & 255)*3) );
		cvResize(m_pAxis_H_CalcImg, ViewImage.GetImage());
		//cvFlip(ViewImage.GetImage(),ViewImage.GetImage(),0);

		//ViewImage.Create(m_pAxis_H_CalcImg->width, m_RectViewStick.Height(), ((IPL_DEPTH_8U & 255)*3) );
		//ViewImage.CopyOf(m_pAxis_H_CalcImg);

		ViewImage.DrawToHDC(m_HDcViewStick, m_RectViewStick);
		//ViewImage.Show(m_HDcViewStick, 0, 0,m_pAxis_H_CalcImg->width, m_RectViewStick.Height());
		ViewImage.Destroy();
	}
	if(m_pAxis_V_CalcImg != NULL)// && m_MaskType == MASK_TYPE_DOT)
	{
		CvvImage ViewImage;
		ViewImage.Create(m_RectViewDot.Width(), m_RectViewDot.Height(), ((IPL_DEPTH_8U & 255)*3) );
		cvResize(m_pAxis_V_CalcImg, ViewImage.GetImage());
		////cvFlip(ViewImage.GetImage(),ViewImage.GetImage(),0);
		//ViewImage.Create(m_pAxis_V_CalcImg->width, m_RectViewDot.Height(), ((IPL_DEPTH_8U & 255)*3) );
		//ViewImage.CopyOf(m_pAxis_V_CalcImg);

		ViewImage.DrawToHDC(m_HDcViewDot, m_RectViewDot);
		//ViewImage.Show(m_HDcViewDot, 0, 0,m_pAxis_V_CalcImg->width, m_RectViewDot.Height());
		ViewImage.Destroy();
	}
}
int YViewValue = 1;
int XViewValue = 1;
void CMeasureProc::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(nIDEvent == 1010)
	{

	}
	CDialog::OnTimer(nIDEvent);
}

void CMeasureProc::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	m_3DViewObj.RenderScreen();
	DrawMeaserData();

	//if(m_MaskType == MASK_TYPE_STRIPE)
	//{


	//	IplImage *pResutImage  = cvCloneImage(m_pSResultViewImg);
	//	DrawResultStripe(pResutImage);
	//	cvReleaseImage(&pResutImage);
	//}
	//else
	//{
	//	IplImage *pResutImage  = cvCloneImage(m_pDResultViewImg);
	//	DrawResultDot(pResutImage);
	//	cvReleaseImage(&pResutImage);
	//}

}



void CMeasureProc::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CMeasureProc::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CPoint MPos = point;
	ScreenToClient(&MPos);
	if(m_Rect3DViewArea.PtInRect(point) == TRUE)
		GetDlgItem(IDC_BT_DEFAULT_POS)->SetFocus();


	//CRect CheckPos;
	//GetDlgItem(IDC_ST_RESULT_VIEW)->GetWindowRect(&CheckPos);
	//ScreenToClient(&CheckPos);
	//if(CheckPos.PtInRect(point) == TRUE)
	//{
	//	IplImage *pViewImg = NULL;
	//	if(m_MaskType == MASK_TYPE_STRIPE)
	//	{
	//		pViewImg = cvCloneImage(m_pSResultViewImg);
	//		DrawResultStripe(pViewImg);
	//	}
	//	else
	//	{
	//		pViewImg = cvCloneImage(m_pDResultViewImg);
	//		DrawResultDot(pViewImg);
	//	}
	//	

	//	cvNamedWindow("Result_View");
	//	cvShowImage("Result_View", pViewImg);
	//	cvWaitKey(10);
	//	cvReleaseImage(&pViewImg);
	//}


	m_LButtonDown = TRUE;
	OldMovePoint = point;
	CDialog::OnLButtonDown(nFlags, point);
}


void CMeasureProc::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CPoint MPos = point;
	if(m_Rect3DViewArea.PtInRect(MPos) == FALSE)
	{
		CDialog::OnLButtonUp(nFlags, point);
		return ;
	}
	m_LButtonDown = FALSE;
	OldMovePoint = point;
	CDialog::OnLButtonUp(nFlags, point);
}


void CMeasureProc::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(m_Rect3DViewArea.PtInRect(point) == FALSE)
	{
		CDialog::OnRButtonDown(nFlags, point);
		return ;
	}
	m_RButtonDown = TRUE;
	CDialog::OnRButtonDown(nFlags, point);
}

void CMeasureProc::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(m_Rect3DViewArea.PtInRect(point) == FALSE)
	{
		CDialog::OnRButtonUp(nFlags, point);
		return ;
	}

	m_RButtonDown = FALSE;
	CDialog::OnRButtonUp(nFlags, point);
}
BOOL CMeasureProc::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CPoint MPos = pt;
	ScreenToClient(&MPos);
	if(m_Rect3DViewArea.PtInRect(MPos) == FALSE)
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);

	if(zDelta>0)
		m_3DViewObj.ZoomObject(0.5f);
	else
		m_3DViewObj.ZoomObject(-0.5f);
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}


void CMeasureProc::OnMouseMove(UINT nFlags, CPoint point)
{
	//////////////////////////////////////////////////////////////////////////

	//CString Viewstr;
	//Viewstr.Format("%d, %d==>(%d, %d)", point.x, point.y,MPos.x, MPos.y);
	//this->SetWindowText(Viewstr);
	if(m_Rect3DViewArea.PtInRect(point) == FALSE)
	{
		if(m_RButtonDown == TRUE)
			m_RButtonDown = FALSE;
		if(m_LButtonDown == TRUE)
			m_LButtonDown = FALSE;
		CDialog::OnMouseMove(nFlags, point);
		return;
	}

	if(m_RButtonDown || m_LButtonDown)
	{
		float UpDown =2.0f ;
		float RightLeft = 2.0f;
		if(OldMovePoint.x < point.x)//오른쪽.
		{
			if (abs(OldMovePoint.x - point.x)>3)
				RightLeft = (-1)*RightLeft;
			else
				RightLeft = 0;

			if(abs(OldMovePoint.y-point.y)>3)
			{
				if(OldMovePoint.y < point.y)//아래쪽.
					UpDown = (-1)* UpDown;
				else//위쪽
					UpDown = UpDown;
			}
			else
				UpDown = 0;
		}
		else//왼쪽
		{
			if (abs(OldMovePoint.x - point.x)>3)
				RightLeft = RightLeft;
			else
				RightLeft = 0;

			if(abs(OldMovePoint.y-point.y)>3)
			{
				if(OldMovePoint.y < point.y)//아래쪽.
					UpDown = (-1)* UpDown;
				else//위쪽
					UpDown = UpDown;
			}
			else
				UpDown = 0;
		}
		if(m_LButtonDown)
			m_3DViewObj.MoveObject((float)(RightLeft*(-0.5))/10, (float)(UpDown*(0.5))/10);
		else
			m_3DViewObj.RotatObject((float)(UpDown*(2.0)), (float)(RightLeft*(2.0)));

		OldMovePoint = point;
	}
	CDialog::OnMouseMove(nFlags, point);
}

int CMeasureProc::fn_Load3DData(int MaskType, DATA_3D_RAW *pNewData_3D, RESULT_DATA *pResultBuf, int FilterLevel)
{

	m_pNewData_3D = pNewData_3D;
	SCROLLINFO  scrinfo;
	scrinfo.cbSize = sizeof(scrinfo);
	scrinfo.fMask = SIF_ALL;
	scrinfo.nMin = 0;          // 최소값
	scrinfo.nMax = m_pNewData_3D->m_Width-1;      // 최대값
	scrinfo.nPage = 10;      // 페이지단위 증가값
	scrinfo.nTrackPos = 0;  // 트랙바가 움직일때의 위치값
	scrinfo.nPos = 50;        // 위치
	m_SBAxisX.SetScrollInfo(&scrinfo);

	scrinfo.cbSize = sizeof(scrinfo);
	scrinfo.fMask = SIF_ALL;
	scrinfo.nMin = 0;          // 최소값
	scrinfo.nMax =  m_pNewData_3D->m_Height-1;      // 최대값
	scrinfo.nPage = 10;      // 페이지단위 증가값
	scrinfo.nTrackPos = 0;  // 트랙바가 움직일때의 위치값
	scrinfo.nPos = 50;        // 위치
	m_SBAxisY.SetScrollInfo(&scrinfo);


	if(MaskType==MASK_TYPE_DOT)
	{
		m_3DViewObj.m_MeaserInfo.m_SelectAxis = 3;//Both Measer
		GetDlgItem(IDC_SBAR_AXIS_X)->EnableWindow(TRUE);
		GetDlgItem(IDC_SBAR_AXIS_Y)->EnableWindow(TRUE);
	}
	else
	{
		m_3DViewObj.m_MeaserInfo.m_SelectAxis = 2;//Y Axis
		GetDlgItem(IDC_SBAR_AXIS_X)->EnableWindow(FALSE);
		GetDlgItem(IDC_SBAR_AXIS_Y)->EnableWindow(TRUE);
	}
	

	//Render Data Setting
	RENDER_DATA new3DUpData;
	new3DUpData.PixelPerNano	= m_pNewData_3D->m_MeasureParam.dXYCalibration*1000.0;//um->Nano
	new3DUpData.DnX				= m_pNewData_3D->m_Width;
	new3DUpData.DnY				= m_pNewData_3D->m_Height;
	new3DUpData.pImgData		= (IMG_PIXEL *)m_pNewData_3D->m_pColorData;
	new3DUpData.pDepthData		= m_pNewData_3D->m_pDepthData;

	//Data Depth는 Nano단위로 들어온다.
	new3DUpData.DnZ				= (UINT)m_pNewData_3D->m_ZDepth/(UINT)new3DUpData.PixelPerNano;//수치 1당 1nano Data로 Raw Data가 만들어진다.

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//Depth Data Noise 제거.(필수)
	IplImage *pDepthImage = cvCreateImage(cvSize(m_pNewData_3D->m_Width, m_pNewData_3D->m_Height), IPL_DEPTH_32F, 1);
	memcpy(pDepthImage->imageData, new3DUpData.pDepthData, sizeof(UINT)*m_pNewData_3D->m_Width*m_pNewData_3D->m_Height);

	
	cvSmooth(pDepthImage, pDepthImage, CV_GAUSSIAN, FilterLevel, 0);
	//if(MaskType==MASK_TYPE_STRIPE)
	//	cvSmooth(pDepthImage, pDepthImage, CV_GAUSSIAN, 17, 0);//27, 0);
	//else
	//	cvSmooth(pDepthImage, pDepthImage, CV_GAUSSIAN, 17, 0);//27, 0);

	memcpy(new3DUpData.pDepthData, pDepthImage->imageData, sizeof(UINT)*m_pNewData_3D->m_Width*m_pNewData_3D->m_Height);
	cvReleaseImage(&pDepthImage);
	//////////////////////////////////////////////////////////////////////////
	m_3DViewObj.DataLoading_Dn(&new3DUpData);
	m_3DViewObj.RenderScreen();

	return	0;
}

void CMeasureProc::OnBnClickedBtDefaultPos()
{

	m_3DViewObj.m_MeaserInfo.MPositionX = m_MaserDefaultPos.x - (1024/2);
	m_3DViewObj.m_MeaserInfo.MPositionY = m_MaserDefaultPos.y - (768/2);
	m_3DViewObj.SetDefValue();
	//m_3DViewObj.RenderScreen();
	if(m_MaskType==MASK_TYPE_DOT)
	{
		m_SBAxisX.SetScrollPos(m_MaserDefaultPos.x);
		m_SBAxisY.SetScrollPos(m_MaserDefaultPos.y);
	}
	else
	{
		m_SBAxisY.SetScrollPos(m_MaserDefaultPos.y);
	}

	fn_ProcDataMeasure (m_MaserDefaultPos);
}

void CMeasureProc::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(pScrollBar)
	{
		if(pScrollBar == (CScrollBar*)&m_SBAxisY)
		{
			SCROLLINFO  scrinfo;
			// 스크롤바 정보를 가져온다.
			if(pScrollBar->GetScrollInfo(&scrinfo))
			{
				switch(nSBCode)
				{
				case SB_PAGEUP:   // 스크롤 바의 위쪽 바를 클릭
					scrinfo.nPos -= scrinfo.nPage;
					break;
				case SB_PAGEDOWN:  // 스크롤 바의 아래쪽 바를 클릭
					scrinfo.nPos += scrinfo.nPage;
					break;
				case SB_LINEUP:   // 스크롤 바의 위쪽 화살표를 클릭
					scrinfo.nPos -= scrinfo.nPage/10;
					break;
				case SB_LINEDOWN:  // 스크롤 바의 아래쪽 화살표를 클릭
					scrinfo.nPos += scrinfo.nPage/10;
					break;
				case SB_THUMBPOSITION: // 스크롤바의 트랙이 움직이고 나서
				case SB_THUMBTRACK:  // 스크롤바의 트랙이 움직이는 동안
					scrinfo.nPos = scrinfo.nTrackPos;   // 16bit값 이상을 사용

					break;
				}
				// 스크롤바의 위치를 변경한다.
				pScrollBar->SetScrollPos(scrinfo.nPos);
				//m_3DViewObj.m_MeaserInfo.MPositionY = scrinfo.nPos - (m_pNewData_3D->m_Height/2);
				//m_3DViewObj.m_MeaserInfo.MPositionY = m_SBAxisY.GetScrollPos() - (768/2);
				//m_3DViewObj.RenderScreen();
				fn_ProcDataMeasure (cvPoint(m_SBAxisX.GetScrollPos(),m_SBAxisY.GetScrollPos()), TRUE);

			}
			return;
		}
	}
	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CMeasureProc::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(pScrollBar == (CScrollBar*)&m_SBAxisX)
	{
		SCROLLINFO  scrinfo;
		// 스크롤바 정보를 가져온다.
		if(pScrollBar->GetScrollInfo(&scrinfo))
		{
			switch(nSBCode)
			{
			case SB_PAGEUP:   // 스크롤 바의 위쪽 바를 클릭
				scrinfo.nPos -= scrinfo.nPage;
				break;
			case SB_PAGEDOWN:  // 스크롤 바의 아래쪽 바를 클릭
				scrinfo.nPos += scrinfo.nPage;
				break;
			case SB_LINEUP:   // 스크롤 바의 위쪽 화살표를 클릭
				scrinfo.nPos -= scrinfo.nPage/10;
				break;
			case SB_LINEDOWN:  // 스크롤 바의 아래쪽 화살표를 클릭
				scrinfo.nPos += scrinfo.nPage/10;
				break;
			case SB_THUMBPOSITION: // 스크롤바의 트랙이 움직이고 나서
			case SB_THUMBTRACK:  // 스크롤바의 트랙이 움직이는 동안
				scrinfo.nPos = scrinfo.nTrackPos;   // 16bit값 이상을 사용

				break;
			}
			// 스크롤바의 위치를 변경한다.
			pScrollBar->SetScrollPos(scrinfo.nPos);
			//m_3DViewObj.m_MeaserInfo.MPositionX = scrinfo.nPos - (m_pNewData_3D->m_Width/2);
			//m_3DViewObj.RenderScreen();
			fn_ProcDataMeasure (cvPoint(m_SBAxisX.GetScrollPos(),m_SBAxisY.GetScrollPos()), TRUE);
		}
		return;
	}

	if(pScrollBar == (CScrollBar*)&m_slider_H1)
	{
		m_MeaserH_Edit.LeftMinValueX = m_slider_H1.GetPos();
		m_MeaserH_Edit.LeftMinValueY = m_DepthData_H[m_MeaserH_Edit.LeftMinValueX];

		//m_slider_H1.ClearSel();
		//m_slider_H1.SetSelection(m_MeaserH_Edit.LeftMinValueX, m_MeaserH_Edit.RightMinValueX);
		
	}
	if(pScrollBar == (CScrollBar*)&m_slider_H2)
	{
		m_MeaserH_Edit.RightMinValueX = m_slider_H2.GetPos();
		m_MeaserH_Edit.RightMinValueY = m_DepthData_H[m_MeaserH_Edit.RightMinValueX];

		//m_slider_H2.ClearSel();
		//m_slider_H2.SetSelection(m_MeaserH_Edit.LeftMinValueX, m_MeaserH_Edit.RightMinValueX);
	}
	if(pScrollBar == (CScrollBar*)&m_slider_H3)
	{
		m_MeaserH_Edit.LeftAngleValueX = m_slider_H3.GetPos();
		m_MeaserH_Edit.LeftAngleValueY = m_DepthData_H[m_MeaserH_Edit.LeftAngleValueX];

		//m_slider_H3.ClearSel();
		//m_slider_H3.SetSelection(m_MeaserH_Edit.LeftAngleValueX, m_MeaserH_Edit.RightAngleValueX);
	}
	if(pScrollBar == (CScrollBar*)&m_slider_H4)
	{
		m_MeaserH_Edit.RightAngleValueX = m_slider_H4.GetPos();
		m_MeaserH_Edit.RightAngleValueY = m_DepthData_H[m_MeaserH_Edit.RightAngleValueX];

		//m_slider_H4.ClearSel();
		//m_slider_H4.SetSelection(m_MeaserH_Edit.LeftAngleValueX, m_MeaserH_Edit.RightAngleValueX);
	}
	if(pScrollBar == (CScrollBar*)&m_slider_H5)
	{
		m_MeaserH_Edit.LeftDnValueX =m_slider_H5.GetPos();
		m_MeaserH_Edit.LeftDnValueY = m_DepthData_H[m_MeaserH_Edit.LeftDnValueX];

		//m_slider_H5.ClearSel();
		//m_slider_H5.SetSelection(m_MeaserH_Edit.LeftDnValueX, m_MeaserH_Edit.RightDnValueX);
	}
	if(pScrollBar == (CScrollBar*)&m_slider_H6)
	{
		m_MeaserH_Edit.RightDnValueX = m_slider_H6.GetPos();
		m_MeaserH_Edit.RightDnValueY = m_DepthData_H[m_MeaserH_Edit.RightDnValueX];

		//m_slider_H6.ClearSel();
		//m_slider_H6.SetSelection(m_MeaserH_Edit.LeftDnValueX, m_MeaserH_Edit.RightDnValueX);
	}
	if(m_MaskType == MASK_TYPE_STRIPE)
	{
		DrawProcLineStrip(m_pAxis_H_CalcImg, &m_MeaserH_Edit, TRUE);
		fn_SetMeasureValue_Strip(&m_MeaserH_Edit);
		DrawMeaserData();

		IplImage *pResutImage  = cvCloneImage(m_pSResultViewImg);
		DrawResultStripe(pResutImage);
		cvReleaseImage(&pResutImage);

		//Invalidate();
		return;
	}
	else
		DrawProcLineDot(m_pAxis_H_CalcImg, &m_MeaserH_Edit, m_DepthData_H, TRUE);

	//////////////////////////////////////////////////////////////////////////
	if(pScrollBar == (CScrollBar*)&m_slider_V1)
	{
		m_MeaserV_Edit.LeftMinValueX = m_slider_V1.GetPos();
		m_MeaserV_Edit.LeftMinValueY = m_DepthData_V[m_MeaserV_Edit.LeftMinValueX];


		
	}
	if(pScrollBar == (CScrollBar*)&m_slider_V2)
	{
		m_MeaserV_Edit.RightMinValueX = m_slider_V2.GetPos();
		m_MeaserV_Edit.RightMinValueY = m_DepthData_V[m_MeaserV_Edit.RightMinValueX];
	}
	if(pScrollBar == (CScrollBar*)&m_slider_V3)
	{
		m_MeaserV_Edit.LeftAngleValueX = m_slider_V3.GetPos();
		m_MeaserV_Edit.LeftAngleValueY = m_DepthData_V[m_MeaserV_Edit.LeftAngleValueX];
	}
	if(pScrollBar == (CScrollBar*)&m_slider_V4)
	{
		m_MeaserV_Edit.RightAngleValueX = m_slider_V4.GetPos();
		m_MeaserV_Edit.RightAngleValueY = m_DepthData_V[m_MeaserV_Edit.RightAngleValueX];
	}
	if(pScrollBar == (CScrollBar*)&m_slider_V5)
	{
		m_MeaserV_Edit.LeftDnValueX = m_slider_V5.GetPos();
		m_MeaserV_Edit.LeftDnValueY = m_DepthData_V[m_MeaserV_Edit.LeftDnValueX];
	}
	if(pScrollBar == (CScrollBar*)&m_slider_V6)
	{
		m_MeaserV_Edit.RightDnValueX = m_slider_V6.GetPos();
		m_MeaserV_Edit.RightDnValueY = m_DepthData_V[m_MeaserV_Edit.RightDnValueX];
	}
	DrawProcLineDot(m_pAxis_V_CalcImg, &m_MeaserV_Edit, m_DepthData_V, TRUE);
	DrawMeaserData();
	fn_SetMeasureValue_Dot( &m_MeaserH_Edit, &m_MeaserV_Edit);
	IplImage*pResutImage  = cvCloneImage(m_pDResultViewImg);
	DrawResultDot(pResutImage);
	cvReleaseImage(&pResutImage);
	//Invalidate();
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CMeasureProc::OnBnClickedBtComplete()
{
	if(m_MaskType == MASK_TYPE_STRIPE)
	{
		DrawProcLineStrip(m_pAxis_H_CalcImg, &m_MeaserH_Edit, FALSE);
	}
	else
	{
		DrawProcLineDot(m_pAxis_H_CalcImg, &m_MeaserH_Edit, m_DepthData_H, FALSE);
		DrawProcLineDot(m_pAxis_V_CalcImg, &m_MeaserV_Edit, m_DepthData_V, FALSE);
	}


	m_3DViewObj.SetDefValue();
	ShowWindow(SW_HIDE);

	fn_ProcDataResultDraw();
}
void CMeasureProc::OnBnClickedBtCancel()
{
	ShowWindow(SW_HIDE);
}
void CMeasureProc::OnBnClickedBtResult()
{
	IplImage *pViewImg = NULL;
	if(m_MaskType == MASK_TYPE_STRIPE)
	{
		pViewImg = cvCloneImage(m_pSResultViewImg);
		DrawResultStripe(pViewImg);
	}
	else
	{
		pViewImg = cvCloneImage(m_pDResultViewImg);
		DrawResultDot(pViewImg);
	}
	cvReleaseImage(&pViewImg);
}


//
//IplImage *Filter::ideal_high_filter( IplImage*src_image, int D0 )
//{
//	int i, j;
//	int temp_h, temp_w;
//	double sqrt_value;
//	double var, real_var, imag_var;
//	// FFT 변환
//	IplImage *freq= FFT2D( src_image );
//	if( !freq ) return NULL;
//	// 변환주파수데이터초기화
//	IplImage *i_real= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 1 );
//	IplImage *i_imag= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 1 );
//
//	// 주파수데이터의실수허수부분을생성
//	IplImage *freq_real= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 1 );
//	IplImage *freq_imag= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 1 );
//	// freq의실수부를freq_real로복사및허수부를freq_imag로복사
//	cvCvtPixToPlane( freq, freq_real, freq_imag, NULL, NULL );
//	int height = freq->height;
//	int width = freq->width;
//	int half_height = height/2;
//	int half_width = width/2;
//	for(i=0; i<height; i++)
//	{
//		if(i >= half_height) 
//			temp_h= i - half_height- half_height;
//		else 
//			temp_h = i;
//
//		for(j=0; j<width; j++)
//		{
//			if(j >= half_width) 
//				temp_w= j - half_width- half_width;
//			else 
//				temp_w = j ;
//			//D(U,V) = SQRT(U^2 + V^2);
//			sqrt_value = sqrt((double)(temp_h * temp_h) + (temp_w* temp_w ));
//			// HIGH PASS
//			// IF D(U,V) >= D0 : 1 THEN 0
//			var = (sqrt_value > = (double)D0) ? 1.0 : 0.0;
//			// 원본데이터로부터주파수영역실수및허수영역의값을생성
//			real_var = cvGetReal2D( freq_real, i, j ) * var;
//			imag_var = cvGetReal2D( freq_imag, i, j ) * var;
//			// 결과주파수데이터에 실수및허수 영역의값을저장
//			cvSetReal2D( i_real, i, j, real_var );
//			cvSetReal2D( i_imag, i, j, imag_var );
//		}
//	}
//
//
//	// 필터링거친결과인실수부와허수부를조합
//	IplImage *i_freq= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 2 );
//	cvCvtPlaneToPix( i_real, i_imag, NULL, NULL, i_freq );
//	// Inverse FFT 수행
//	IplImage *dst_image= inverse_FFT2D( i_freq);
//	// 할당한메모리해제
//	cvReleaseImage( &freq_real );
//	cvReleaseImage( &freq_imag );
//	cvReleaseImage( &freq );
//	cvReleaseImage( &i_real );
//	cvReleaseImage( &i_imag );
//	cvReleaseImage( &i_freq );
//	return dst_image;
//}
//IplImage *Filter::ideal_low_filter(IplImage *src_image,int D0 )
//{
//	int i, j;
//	int temp_h, temp_w;
//	double sqrt_value;
//	double var, real_var, imag_var;
//
//	// FFT 변환
//	IplImage *freq= FFT2D( src_image );
//	if( !freq ) return NULL;
//	// 변환주파수데이터초기화
//	IplImage *i_real= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 1 );
//	IplImage *i_imag= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 1 );
//
//	// 주파수데이터의실수허수부분을생성
//	IplImage *freq_real= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 1 );
//	IplImage *freq_imag= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 1 );
//
//	// freq의실수부를freq_real로복사및허수부를freq_imag로복사
//	cvCvtPixToPlane( freq,freq_real, freq_imag,NULL, NULL );
//
//	int height = freq->height;
//	int width = freq->width;
//	int half_height = height/2; 
//	int half_width = width/2;
//
//	// HEIGHT 반복
//	for(i=0; i<height; i++)
//	{
//		if(i >= half_height)
//			temp_h= i - half_height- half_height;
//		else 
//			temp_h = i;
//
//		for(j=0; j<width; j++)
//		{
//			if(j >= half_width) 
//				temp_w= j - half_width  - half_width;
//			else 
//				temp_w = j;
//
//			//D(U,V) = SQRT(U^2 + V^2);
//			sqrt_value = sqrt((double)(temp_h * temp_h) + (temp_w* temp_w ));
//
//			// LOW PASS
//			// IF D(U,V) <= D0 : 1 THEN 0
//			var = (sqrt_value< = (double)D0) ? 1.0 : 0.0;
//			// 원본데이터로부터주파수영역실수및허수영역의값을생성
//			real_var = cvGetReal2D(freq_real, i,j ) * var;
//			imag_var = cvGetReal2D(freq_imag, i,j ) * var;
//			// 결과주파수데이터에 실수및허수 영역의값을저장
//			cvSetReal2D( i_real,i, j, real_var );
//			cvSetReal2D( i_imag,i, j, imag_var );
//		}
//	}
//	// 필터링거친결과인실수부와허수부를조합
//	IplImage *i_freq= cvCreateImage( cvGetSize(freq), IPL_DEPTH_32F, 2 );
//	cvCvtPlaneToPix( i_real,i_imag, NULL,NULL, i_freq );
//
//	// Inverse FFT 수행하여결과이미지저장
//	IplImage *dst_image= inverse_FFT2D( i_freq);
//
//	// 할당한메모리해제
//	cvReleaseImage( &freq_real );
//	cvReleaseImage( &freq_imag );
//	cvReleaseImage( &freq );
//	cvReleaseImage( &i_real );
//	cvReleaseImage( &i_imag );
//	cvReleaseImage( &i_freq );
//	return dst_image;
//}



