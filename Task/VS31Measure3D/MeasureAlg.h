/// [2014-11-05] Charles, Added.

#pragma once

#include "stdafx.h"
#include "CvImageView.h"

typedef int MASK_TYPE;

// 출력용 Color 정의
#define HEIGHT_LINE_HORIZ_COLOR CV_RGB(121,120,222)
#define HEIGHT_LINE_VERT_COLOR CV_RGB(125,221,121)
#define HEIGHT_LINE_HORIZ_TEXT_COLOR CV_RGB(81,80,182)
#define HEIGHT_LINE_VERT_TEXT_COLOR CV_RGB(85,181,81)
#define HEIGHT_LINE_CORNDER_DISTANCE_COLOR CV_RGB(255,120,120)
#define HEIGHT_LINE_CORNDER_DISTANCE_CROSS_COLOR CV_RGB(120,120,255)
#define HEIGHT_LINE_CORNDER_DISTANCE_TEXT_COLOR CV_RGB(10,10,10)
#define RESULT_DATA_BOX_LINE_COLOR CV_RGB(64, 64, 64)

// 측정 방향 정의
// 0 : 가로/세로 방향으로 측정
// 1 : 대각 방향으로 측정
enum MEASURE_DIR {
	MEASURE_ANGLE_0 = 0,
	MEASURE_ANGLE_45 = 1
};

//
// 측정 알고리즘의 입력 데이터에 대한 구조체
//
struct INPUT_DATA {
	char m_szDataPath[MAX_PATH];
	FLOAT m_nTargetThick;			// nm 단위
	FLOAT m_nSpaceGap;			// nm 단위

public :
	INPUT_DATA() {
		Init();
	}

	void Init() {
		m_szDataPath[0] = 0x00;
		m_nTargetThick = 0.f;
		m_nSpaceGap = 0.f;
	}

	void Clear() {
		Init();
	}
};

//
// 데이터 측정시 사용되는 '특징점' (High, Mid, Low) 에 대한 구조체
//
struct FEATURE_POINT_DATA {
	int m_nHighPos;
	int m_nMidPos;
	int m_nLowPos;
	float m_nHighPosHeight;
	float m_nMidPosHeight;
	float m_nLowPosHeight;

	FEATURE_POINT_DATA() {
		Init();
	}

	void Init() {
		m_nHighPos = 0;
		m_nMidPos = 0;
		m_nLowPos = 0;
		m_nHighPosHeight = 0;
		m_nMidPosHeight = 0;
		m_nLowPosHeight = 0;
	}

	void Clear() {
		Init();
	}
};

//
// 데이터 측정 결과 구조체
// 기존 RESULT_DATA 에 전체 높이에 대한 4개의 변수를 추가하여 확장.
//
struct MEASURE_RESULT_DATA : public RESULT_DATA {
	// values for display
	int RIB_HEIGHT_TOTAL_L;		
	int RIB_HEIGHT_TOTAL_R;
	int RIB_HEIGHT_TOTAL_L2;
	int RIB_HEIGHT_TOTAL_R2;

	MEASURE_RESULT_DATA() {
		Init();
	}

	~MEASURE_RESULT_DATA() {
		Clear();
	}

	void Init() {
		memset(this, 0x00, sizeof(RESULT_DATA));

		RIB_HEIGHT_TOTAL_L = 0;
		RIB_HEIGHT_TOTAL_R = 0;
		RIB_HEIGHT_TOTAL_L2 = 0;
		RIB_HEIGHT_TOTAL_R2 = 0;
	}

	void Clear() {
		Init();
	}

	void CopyToSecondPhase(RESULT_DATA *pMeasureData) {
		if(pMeasureData == 0x00) {
			return;
		}

		pMeasureData->RIB_WIDTH2 = RIB_WIDTH;
		pMeasureData->RIB_WIDTH_C2 = RIB_WIDTH_C;
		pMeasureData->RIB_WIDTH_L2 = RIB_WIDTH_L;
		pMeasureData->RIB_WIDTH_R2 = RIB_WIDTH_R;
		pMeasureData->RIB_HEIGHT_L2 = RIB_HEIGHT_L;
		pMeasureData->RIB_HEIGHT_E_L2 = RIB_HEIGHT_E_L;
		pMeasureData->RIB_HEIGHT_R2 = RIB_HEIGHT_R;
		pMeasureData->RIB_HEIGHT_E_R2 = RIB_HEIGHT_E_R;
		pMeasureData->RIB_ANGLE_L2 = RIB_ANGLE_L;
		pMeasureData->RIB_ANGLE_R2 = RIB_ANGLE_R;
		pMeasureData->RIB_HEIGHT2 = RIB_HEIGHT;
	}

#define VALIDATE_RESULT_DATA(_Value) if(_Value < 0) _Value = 0;
	void Validate() {
		VALIDATE_RESULT_DATA(RIB_WIDTH);
		VALIDATE_RESULT_DATA(RIB_WIDTH_C);
		VALIDATE_RESULT_DATA(RIB_WIDTH_L);
		VALIDATE_RESULT_DATA(RIB_WIDTH_R);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_L);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_E_L);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_R);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_E_R);
		VALIDATE_RESULT_DATA(RIB_ANGLE_L);
		VALIDATE_RESULT_DATA(RIB_ANGLE_R);
		VALIDATE_RESULT_DATA(RIB_HEIGHT);

		VALIDATE_RESULT_DATA(RIB_WIDTH2);
		VALIDATE_RESULT_DATA(RIB_WIDTH_C2);
		VALIDATE_RESULT_DATA(RIB_WIDTH_L2);
		VALIDATE_RESULT_DATA(RIB_WIDTH_R2);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_L2);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_E_L2);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_R2);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_E_R2);
		VALIDATE_RESULT_DATA(RIB_ANGLE_L2);
		VALIDATE_RESULT_DATA(RIB_ANGLE_R2);
		VALIDATE_RESULT_DATA(RIB_HEIGHT2);

		VALIDATE_RESULT_DATA(RIB_HEIGHT_TOTAL_L);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_TOTAL_R);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_TOTAL_L2);
		VALIDATE_RESULT_DATA(RIB_HEIGHT_TOTAL_R2);
	}
};

// 실수형 Point 타입 및 리스트 정의.
typedef CvPoint2D32f Point2D;
typedef std::vector<Point2D> Point2Ds;

// Points 타입 정의. Min Points, Angle Points 등 비연속적 Point 들의 집합에 사용됨.
typedef std::vector<int> Points;

//
// 측정 데이터 구조체
// 데이터 측정시 사용되는 중간 및 결과 데이터들의 집합
//
struct MEASURE_DATA {
	IplImage *m_pHeightProfile;
	IplImage *m_pInverseHeightProfile;
	IplImage *m_pAngleProfile;
	Points m_dAngleMinPoints;

	FEATURE_POINT_DATA m_dFreaturePointLeft;
	FEATURE_POINT_DATA m_dFreaturePointRight;

	CvPoint m_ptStart;
	CvPoint m_ptEnd;

	MEASURE_RESULT_DATA m_dResult;
	// SecondPhase 는 DOT 형 타입과 같이 한번에 두가지 방향을 검사해야 할 경우, 두번째 방향의 검사를 의미함.
	BOOL m_bSecondPhase;				// 두번째 방향의 검사인지 여부
	BOOL m_bSecondPhaseOnly;		// 첫번째 방향의 검사는 수행되지 않고, 두번째 방향의 검사만 수행된 경우 TRUE. 

public :
	MEASURE_DATA() {
		Init();
	}

	~MEASURE_DATA() {
		Clear();
	}

	void Init() {
		m_pHeightProfile = 0x00;
		m_pInverseHeightProfile = 0x00;
		m_pAngleProfile = 0x00;

		m_dAngleMinPoints.clear();

		m_ptStart = cvPoint(0, 0);
		m_ptEnd = cvPoint(0, 0);

		m_dFreaturePointLeft.Clear();
		m_dFreaturePointRight.Clear();

		m_dResult.Clear();
		m_bSecondPhase = FALSE;
		m_bSecondPhaseOnly = FALSE;
	}

	void Clear() {
		if(m_pHeightProfile != 0x00) {
			cvReleaseImage(&m_pHeightProfile);
		}

		if(m_pInverseHeightProfile != 0x00) {
			cvReleaseImage(&m_pInverseHeightProfile);
		}

		if(m_pAngleProfile != 0x00) {
			cvReleaseImage(&m_pAngleProfile);
		}

		Init();
	}

	void CopyStartPoint(MEASURE_DATA *pMeasureData) {
		m_ptStart = pMeasureData->m_ptStart;
	}
};

// MeasureData 타입 정의. 다수의 MEASURE_DATA 를 다룰 목적으로 사용.
typedef std::vector<MEASURE_DATA *> MeasureData;

//
// 처리 데이터 구조체
// 알고리즘 수행/처리 에 관련된 데이터의 집합.
// 모든 위치의 측정 검사에 공통의 사용되는 데이터와
// 각 위치별 측정 결과를 포함하고 있음.
//
struct PROC_DATA {
	int m_nHeight;
	int m_nWidth;

	float m_nmPerPixel;
	float m_nmPerHeight;

	// Shared images
	IplImage *m_pHeightImage;
	IplImage *m_pRefImage;
	IplImage *m_pRefMeanLineImage;
	IplImage *m_pHorizMeanLine;
	IplImage *m_pVertMeanLine;
	Points m_dHorizMinCenterPoints;
	Points m_dVertMinCenterPoints;

	MeasureData m_dMeasure; 
	MEASURE_DATA *m_pCurMeasureData;

public :
	PROC_DATA() {
		Init();
	}

	~PROC_DATA() {
		Clear();
	}

	void Init() {
		m_pHeightImage = 0x00;
		m_pRefImage = 0x00;
		m_pRefMeanLineImage = 0x00;
		m_pHorizMeanLine = 0x00;
		m_pVertMeanLine = 0x00;
	
		m_dHorizMinCenterPoints.clear();
		m_dVertMinCenterPoints.clear();

		m_dMeasure.clear();
		m_pCurMeasureData = 0x00;
	}

	void Clear() {
		if(m_pHeightImage != 0x00) {
			cvReleaseImage(&m_pHeightImage);
		}

		if(m_pRefImage != 0x00) {
			cvReleaseImage(&m_pRefImage);
		}

		if(m_pRefMeanLineImage != 0x00) {
			cvReleaseImage(&m_pRefMeanLineImage);
		}

		if(m_pHorizMeanLine != 0x00) {
			cvReleaseImage(&m_pHorizMeanLine);
		}

		if(m_pVertMeanLine != 0x00) {
			cvReleaseImage(&m_pVertMeanLine);
		}

		MEASURE_DATA *pMeasureData;
		for(UINT i=0; i<m_dMeasure.size(); i++) {
			pMeasureData = (MEASURE_DATA *)m_dMeasure[i];
			if(pMeasureData) {
				pMeasureData->Clear();
				delete pMeasureData;
			}
		}

		Init();
	}
};

//
// 출력 데이터 구조체
// 알고리즘 수행 이후 출력 이미지 및 바이너리(RESULT_DATA) 데이터의 집합.
// 이미지는 지정된 특정 경로에 저장이 되면, 바이너리 데이터는 Network 을 통해
// Master 로 전달될 일렬의 RESULT_DATA 데이터 배열임.
//
struct OUTPUT_DATA {
	IplImage *m_pFullImage;
	IplImage	*m_pHeightImage;
	IplImage	*m_pDataImage;

	char m_szDataPath[MAX_PATH];
	char m_szDataFileName[MAX_PATH];

	RESULT_DATA *m_pResultDataBuffer;

	OUTPUT_DATA() {
		Init();
	}

	~OUTPUT_DATA() {
		Clear();
	}

	void Init() {
		m_pFullImage = 0x00;
		m_pHeightImage = 0x00;
		m_pDataImage = 0x00;

		m_szDataPath[0] = 0x00;
		m_szDataFileName[0] = 0x00;

		m_pResultDataBuffer = 0x00;
	}

	void Clear() {
		if(m_pFullImage != 0x00) {
			cvReleaseImage(&m_pFullImage);
		}

		if(m_pHeightImage != 0x00) {
			cvReleaseImage(&m_pHeightImage);
		}

		if(m_pDataImage != 0x00) {
			cvReleaseImage(&m_pDataImage);
		}

		if(m_pResultDataBuffer != 0x00) {
			delete [] m_pResultDataBuffer;
		}

		Init();
	}
};

//
// 시간 데이터 구조체
// 알고리즘 수행 및 데이터 측정에 소요된 시간을 저장.
//
struct TIME_DATA {
	DWORD m_msLoadDataFile;						// 데이터 파일 로드 시간
	DWORD m_msSaveResultImageFile;			// 결과 이미지 파일 저장 시간
	DWORD m_msSaveAnalysisDataFile;			// 분석용 데이터 파일 저장 시간

	DWORD m_msTotalMeasure;						// 전체 측정 알고리즘 소요 시간
	DWORD m_msPreprocessData;					// 데이터 전처리 수행 시간
	DWORD m_msGenerateRefData;					// 참조 데이터 생성 시간
	DWORD m_msGetMinCenterPoints;				// MinCenterPoints 생성 시간
	DWORD m_msCheckCornerDistance;			// Corner Hole 과 Image 중심의 거리를 계산하는 시간
	DWORD m_msGetMeasurePoints;				// MeasurePoints 생성 시간
	DWORD m_msGetHeightProfile;					// HeightProfile 생성 시간
	DWORD m_msMeasureHeightProfile;			// 데이터 측정 시간 

	TIME_DATA() {
		Init();
	}

	~TIME_DATA() {
		Clear();
	}

	void Init() {
		m_msLoadDataFile = 0;
		m_msSaveAnalysisDataFile = 0;
		m_msSaveResultImageFile = 0;

		m_msTotalMeasure = 0;
		m_msGenerateRefData= 0;
		m_msGetMinCenterPoints = 0;
		m_msCheckCornerDistance = 0;
		m_msGetMeasurePoints = 0;
		m_msGetHeightProfile = 0;
		m_msMeasureHeightProfile = 0;
	}

	void Clear() {
		Init();
	}
};

//
// 측정 알고리즘 파라미터 구조체
//
struct ALG_PARAM {
	// 데이터 전처리 관련 파라미터
	int m_nPreprocessSmoothFilterSize;

	// 참조 데이터 생성 관련 파라미터
	float m_nGenerateRefDataThreshold;
	int m_nGenerateRefDataSmoothFilerSize;

	// MeanLine 생성 관련 파라미터
	int m_nMeanLineSmoothFilerSize;

	// MinCenterPoints 생성 관련 파라미터
	float m_nMinCenterPointsThreshold;
	int m_nMinCenterPointsBarSize;

	// CornerDistance 계산 관련 파라미터
	int m_nCornerDistanceMarginSize;
	int m_nCornerDiatanceSmoothFilerSize;
	int m_nCornerDiatanceAngleBarSize;
	float m_nCornerDistanceThreshold;

	// HeightProfile 생성 관련 파라미터
	int m_nHeightProfileSmoothFilerSize;
	int m_nHeightProfileClosingFilterSize;
	float m_nHeightSTDProfileThreshold;

	// AngleProfile 생성 관련 파라미터
	int m_nAngleProfileBarSize;
	float m_nAngleProfileThreshold;
	int m_nAngleMinPointsBarSize;
	int m_nAngleMinPointsBarSize2;

	// FeaturePoint 생성 관련 파라미터
	float m_nFeaturePointHighPosThreshold;
	float m_nFeaturePointHighPosAngleThreshold;
	float m_nFeaturePointLowPosBarSizeRatio;
	float m_nFeaturePointLowPosThreshold;
	float m_nFeaturePointLowPosAngleThreshold;
	float m_nFeaturePointMidPosThreshold;
	int m_nFeaturePointMidPosFittingPointCount;
	float m_nFeaturePointMidPosFittingLineError;
	float m_nFeaturePointMidPosEstimateOutlierError;
	float m_nFeaturePointMidPosEstimateOutlierSigmaError;

	ALG_PARAM() {
		Init();
	}

	void Init() {		// Set Dot Param as Default
		m_nPreprocessSmoothFilterSize = 5;
		m_nGenerateRefDataThreshold = 0.8f;
		m_nMeanLineSmoothFilerSize = 25;
		m_nMinCenterPointsThreshold = 0.7f;		// 알고리즘 내부에서 자동으로 계산되도록 변경
		m_nMinCenterPointsBarSize = 1;
		m_nCornerDistanceMarginSize = 10;
		m_nCornerDistanceThreshold = 0.75f;
		m_nCornerDiatanceSmoothFilerSize = 5;
		m_nCornerDiatanceAngleBarSize = 1;
		m_nHeightProfileSmoothFilerSize = 3;
		m_nHeightProfileClosingFilterSize = 3;
		m_nAngleProfileBarSize = 1;
		m_nAngleProfileThreshold = 175.f;
		m_nAngleMinPointsBarSize = 1;
		m_nAngleMinPointsBarSize2 = 5;
		m_nFeaturePointHighPosThreshold = 0.07f;
		m_nFeaturePointHighPosAngleThreshold = 170; 
		m_nFeaturePointLowPosBarSizeRatio = 0.05f;
		m_nFeaturePointLowPosThreshold = 0.1f;
		m_nFeaturePointLowPosAngleThreshold = 165.f;
		m_nFeaturePointMidPosThreshold = 0.2f;
		m_nFeaturePointMidPosFittingPointCount = 5;
		m_nFeaturePointMidPosFittingLineError = 11.f;
		m_nFeaturePointMidPosEstimateOutlierError = 0.5f;
		m_nFeaturePointMidPosEstimateOutlierSigmaError = 1.4f;
	}

	void SetDotParam() {
		Init();
	}

	void SetStripeParam() {
		Init();

		m_nGenerateRefDataThreshold = 0.5f;
		m_nGenerateRefDataSmoothFilerSize = 25;
		m_nMeanLineSmoothFilerSize = 25;
		m_nHeightSTDProfileThreshold = 0.2f;
	}
};

//
// 측정 알고리즘 객체
// 타입별(DOT 형, STRIPE 형) 측정 알고리즘 객체의 Base 객체
//
class CMeasureAlg
{
protected :
	INPUT_DATA m_dInput;
	PROC_DATA m_dProc;
	OUTPUT_DATA m_dOutput;
	TIME_DATA m_dTime;

	ALG_PARAM m_dParam;

	DATA_3D_RAW m_d3DRaw;

	MASK_TYPE m_nMaskType;
	MEASURE_DIR m_nMeasureAngle;
	BOOL m_bMeasureAll;
	int m_nMeasureIndex;
	int m_nMeasureOffsetX;
	int m_nMeasureOffsetY;

	BOOL m_b2DDataSmoothing;
	BOOL m_bEstimateFeaturePoint;

	BOOL m_bCheckCornerDistance;
	BOOL m_bSaveHeightData;
	BOOL m_bSaveAnalysisData;
	char m_szAnalysisDataPath[MAX_PATH];

	DWORD m_nStartTime;

	int m_nCornerPosX;
	int m_nCornerPosY;
	float m_nCornerDistance;

	CvImageView *m_pImageView;
public:
	CMeasureAlg(void);
	virtual ~CMeasureAlg(void);

	static CMeasureAlg *GetInstance(MASK_TYPE nMaskType);

public :
	void Clear();

	// 파라미터 설정 관련 함수
	void SetTargetThick(FLOAT nTargetThick);
	void SetGlassSpaceGap(FLOAT nGap);
	void SetMeasureAngle(MEASURE_DIR nMeasureAngle);
	void SetMeasureAll(BOOL bSet);
	void SetMeasureIndex(int nMeasureIndex);
	void SetMeasureOffset(int nMeasureOffsetX, int nMeasureOffsetY);
	void Set2DDataSmoothing(BOOL bSet);
	void SetEstimateFeaturePoint(BOOL bSet);
	void SetSaveHeightData(BOOL bSet);
	void SetSaveAnalysisData(BOOL bSet);
	void SetCheckCornerDistance(BOOL bSet);

	// 데이터 로드 함수
	BOOL LoadDataFile(char *szDataFile);

	// 측정 수행 함수.
	virtual BOOL Measure();

	// 측정 결과(RESULT_DATA 배열) 반환 함수
	int GetResultDataCount();
	RESULT_DATA *GetResultData();
	float GetCornerDistance();
	
	// 결과 이미지 저장/출력 관련 함수들
	virtual BOOL MakeResultImage();
	void SetSaveResultPath(char *szPath);
	void SaveResultImage();
	void ShowResultImage();
	void SaveAnalysisData();

protected :
	virtual BOOL _PreprocessData();
	virtual BOOL _GenerateRefData();
	virtual BOOL _GetMinCenterPoints();
	virtual BOOL _GetMeasurePoints() = 0;
	virtual BOOL _GetHeightProfile();
	virtual BOOL _MeasureHeightProfile();

	virtual BOOL _MakeResultFullImage();
	virtual BOOL _MakeResultHeightImage();
	virtual BOOL _MakeResultDataImage();
	virtual BOOL _MakeAnalysisResultDataImage();

	BOOL _SelectStartMinCenterPoint(BOOL bHoriz, int &nStartPos, int &nStartPosIndex);
	int _GetMinCenterPointIndex(int nPos, BOOL bHoriz);
	int _GetRealMeasureResultDataCount();
	BOOL _IsNewRealMeasureResultData(MEASURE_DATA *pMeasureData);

	void _PrintDebugString(const char* szDebugString, ...);
	void _PrintDebugImage(IplImage *pImage, int nStartX, int nStartY, int nSizeY, int nSizeX, const char* szDebugString, ...);
	void _PrintDebugVector(IplImage *pImage, int nStartX, int nSizeX, const char* szDebugString, ...);
	void _PrintDebugPoints(int *pPoints, int nCount, const char* szDebugString, ...);
	void _PrintDebugPoints(Points dPoints, const char* szDebugString, ...);
	void _PrintDebugResultData(MEASURE_RESULT_DATA *pResultData, const char* szDebugString, ...);

	void _ShowDebugImage(IplImage *pImage, char *szWindowName = "IMAGE");
	void _ShowDebugBinaryImage(IplImage *pImage, char *szWindowName = "BINARY IMAGE");
	void _ShowDebugLine(IplImage *pImage, char *szWindowName = "Line");

	DWORD _SetStartTime(DWORD nStartTime = 0);
	DWORD _GetElapsedTime();

	// COMMON Alg functions
	BOOL _FindMinCenterPoints(BOOL bHoriz);
	void _AdjustMinCenterEndPoints(BOOL bHoriz);
	void _ApplyMinCenterPointsOffset(BOOL bHoriz);
	int _CheckMeanLineMargin(IplImage *pMeanLine, BOOL bReverse);
	int _GetCornerPos(IplImage *pMeanLine, BOOL bReverse);
	IplImage *_GetFloatImage(IplImage *pImage);
	IplImage *_GetSubImage(IplImage *pImage, int nStartX, int nEndX, int nStartY, int nEndY);
	IplImage *_GetMeanLine(IplImage *pImage, BOOL bHoriz);
	IplImage *_GetSubImageMeanLine(IplImage *pImage, int nStartX, int nEndX, int nStartY, int nEndY, BOOL bHoriz);

	BOOL _GetAngleProfile();
	BOOL _FindAngleMinPoints();
	BOOL _SelectFeaturePoints(BOOL bLeftPart);
	BOOL _GetFittingLine(CvPoint2D32f *pPoints, int nLength, FLOAT &nA, FLOAT &nB);
	float _EstimateMidPosHeight(Point2Ds &dPoints, int nLowPos, float nLowPosHeight, int nMidPos, float nMidPosHeight);
	float _LinearFit(const Point2Ds &dPoints, float &nA, float &nB);
	float _GetLinearFitValue(float nX, float nA, float nB);
	float _GammaFit(const Point2Ds &dPoints, float &nA, float &nB);
	float _GetGammaFitValue(float nX, float nA, float nB);
	float _LogFit(const Point2Ds &dPoints, float &nA, float &nB);
	float _GetLogFitValue(float nX, float nA, float nB);

	virtual BOOL _MakeResultData();

	FLOAT _CalcAvgData(int nStartPos, int nEndPos);
	FLOAT _CalcAvgData(FLOAT *pData, int idxStart, int idxEnd, int nSizeX=1);
	FLOAT _CalcAngle(CvPoint2D32f ptLeft, CvPoint2D32f ptCenter, CvPoint2D32f ptRight);
	FLOAT _GetMaxData(FLOAT *pData, int idxStart, int idxEnd);
	FLOAT _GetMinData(FLOAT *pData, int idxStart, int idxEnd);
	int _GetMinDataIndex(FLOAT *pData, int idxStart, int idxEnd);
	int _GetPointsIndex(const Points &dPoints, int nValue);

	BOOL _AddMeasureData(BOOL bSecondPhase = FALSE);
	BOOL _AddMeasureData(CvPoint ptStart, CvPoint ptEnd, BOOL bSecondPhase = FALSE, BOOL bSecondPhaseOnly = FALSE);
	BOOL _SetCurMeasureData(UINT nIndex);
	MEASURE_DATA *_GetCurMeasureData();
	MEASURE_DATA *_GetMeasureData(UINT nIndex);
	MEASURE_RESULT_DATA *_GetCurMeasureResultData();
	MEASURE_RESULT_DATA *_GetMeasureResultData(UINT nIndex);

	void _MakeResultDataFileName();
	void _MakeAnalysisDataFilePath();
	void _SaveHeightDataFile();
	void _SaveAnalysisDataFile();

	void _DrawHeightProfile(IplImage *pView, CvRect rcView, MEASURE_DATA *pMeasureData);

private :
	void _IntResultDataImage();
	void _ClearResultDataImage();
	CvScalar _GetColormapValue(double nValue);
};
