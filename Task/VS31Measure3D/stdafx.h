// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.

#define VS31Motion_PARAM_INI_PATH "D:\\nBiz_InspectStock\\Data\\Task_INI\\VS31Measure3D_Sys_Param.ini"
#define  Section_Revolver		"Revolver"
#define  Key_PortIndex		"PortIndex"
#define  Section_MeasureAlg "MeasureAlg"
#define  Key_MaskType "MaskType"
#define  Key_MeasureAngle "MeasureAngle"
#define  Key_MeasureAll "MeasureAll"
#define  Key_TargetThick "TargetThick"
#define  Key_SpaceGap "SpaceGap"
#define  Key_ShowResult "ShowResult"
#define  Key_SaveAnalysisData "SaveAnalysisData"
#define  Key_CheckCornerDistance "CheckCornerDistance"
#define  Key_2DDataSmoothing "2DDataSmoothing"
#define  Key_EstimageFeaturePoint "EstimageFeaturePoint"
#define  Key_SaveHeightData "SaveHeightData"

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

//Image Process Lib Loading
#include "..\..\CommonHeader\ImgProcLink.h"
//VS64 Interface Lib Loading
#include "..\..\CommonHeader\VS_ServerLink.h"
//UI용 Header File Link
#include "..\..\CommonHeader\UI_LinkHeader.h"

//#include "..\\CommonHeader\\DefineStaticValue.h"
#include "DefineStaticValue.h"
#include ".\\3D\\OpenGL3D.h"

#include "PictureEx.h"

//////////////////////////////////////////////////////////////////////////
#include "VS31Measure3D_Link.h"
#include "ObservationAPPRunner.h"


#define  LENS_INDEX_10X    0
#define  LENS_INDEX_20X    1
#define  LENS_INDEX_50X     2
#define  LENS_INDEX_150X    3
#define  LENS_INDEX_ING     4
#include "VKRevolver.h"

#include "VkViewerRemoteOperation.h"
#include "VkViewerRemoteOperationCompatible.h"


extern CVKRevolver G_VKRevolver;
extern TASK_State G_VS31TASK_STATE;
//초기 접속 Timer
#define  HW_OBS_APP_CHECK_TIMER		10101
#define  HW_OBS_APP_CHECK_Interval	10000


#define RESULT_VIEW_STICK_IMG	"D:\\nBiz_InspectStock\\Data\\V31Measer3D_Data\\StickDataView.bmp"
#define RESULT_VIEW_DOT_IMG	"D:\\nBiz_InspectStock\\Data\\V31Measer3D_Data\\DotDataView.bmp"
#define RESULT_VIEW_STICK_IMG_FOR_ANALYSIS	"D:\\nBiz_InspectStock\\Data\\V31Measer3D_Data\\StickDataViewForAnalysis.bmp"
#define RESULT_VIEW_DOT_IMG_FOR_ANALYSIS "D:\\nBiz_InspectStock\\Data\\V31Measer3D_Data\\DotDataViewForAnalysis.bmp"


#define RET_BACK_COLOR		0xEF

//검사 진행 Check Timer
//#define  HW_MEASUREMENT_CHECK_TIMER		10103
//#define  HW_MEASUREMENT_CHECK_Interval	100
//////////////////////////////////////////////////////////////////////////

//#define WM_LOAD_DATA				(WM_USER + 2000)

//#define WM_3DVIEWER_UPDATE				(WM_USER + 2001)
#define WM_PROC_FILE_DATA				(WM_USER + 2002)

#define MASK_TYPE_STRIPE			1
#define MASK_TYPE_DOT				2

#define WM_DEF_VIEW			2
#define WM_FILTERONOFF		3


//#define FILE_FORMAT_VK4 1



#define  nBiz_SIMUL_RUN  1
#define  PROC_SAVE_FILE 1
//#define  PROC_DOT_FIND_IMG 1
#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


