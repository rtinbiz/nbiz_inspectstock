#pragma once

#include <fstream>
//#include "MsgWindowTaskConstant.h"
#include "..\..\CommonHeader\Class\VS64Exceptions.h"

using namespace std;

// 개발환경 VS6.0 일경우 당 Header File 첨부시 이하 Defien을 적용 한다. _VS6_DEV_

/*
*	Module name		:	PER_PROCESS_INIT
*	Function		:	환경 파일의 구조체
*	Create			:	2006.11.02
*	Author			:	김용태
*	Version			:	1.0
*	
*/

struct	PER_PROCESS_INIT
{
	char	m_sServerIP[MAX_IP_LENGTH];
	int		m_nPortNo;
	int		m_nStationNum;	
	int		m_nTaskNum;
	int		m_nMode;				//Mode 1 = Runtime, 2 = Demo
	int		m_nLogTaskNo;
	int		m_nLogBlockLovel;
	int		m_nLogFileID;

	PER_PROCESS_INIT()
	{
		memset(this, 0x00, sizeof(PER_PROCESS_INIT));
	}
};

class CProcInitial
{
public:
	PER_PROCESS_INIT				m_ProcInitVal;

public:
	/*
	*	Module name	:	m_fnReadDatFile
	*	Parameter	:	FileName
	*	Return		:	정상 종료 : 환경 파일에 기록된 Task Number 	
	*					이상 종료 : 50000 이상의 에러 코드
	*	Function	:	환경 파일을 읽고 환경 설정.
	*	Create		:	2006.03.01
	*	Author		:	고정진
	*	Version		:	1.1
	*	- Version 1.1 (김용태)
	*		- Process initial 을 위한 Class 에서 구조체로 변경하면서 함수 내부를 수정함
	*/
	USHORT m_fnReadDatFile(const char* FileName)
	{
		char			chBuff[READ_BUFFER];					//파일을 읽을때 사용하는 임시 버퍼
		char			chSeps[]   = "=,:\n";					//코큰 구분자
		char			*chToken;								//토큰
		int				nFileNumber=0;							//파일 넘버 인덱스	
		char			*next_token;

		INT				*nValuePointer;

		try
		{

			nValuePointer = &(m_ProcInitVal.m_nPortNo);

			ifstream infile (FileName);								//파일을 Open 한다.

			if(infile==NULL)										//파일이 없을때에 대한 처리 - 보류.
			{
				throw CVs64Exception(_T("Dat 파일이 존재하지 않습니다."));
			}

			//PER_PROCESS_INIT 구조체 내에 값을 차례로 넣는다.

			while (infile.getline(chBuff,READ_BUFFER))				//한 라인을 읽는다.
			{
				if(chBuff[0]!=';'&& strlen(chBuff)>0)				//버퍼의 첫 문자가 ';' 가 아니면, 읽은 문장을 토큰 구분해서 저장한다.
				{
					//chToken = strtok_s( chBuff, chSeps, &next_token );
					chToken = strtok( chBuff, chSeps );
					if( chToken != NULL )
					{
						/* Get next token: */
						next_token = strtok( NULL, chSeps );
					}

					if(strncmp(m_fnStrngToUpper(chToken), "SERVER IP", strlen("SERVER IP")) == 0)
					{
						strcpy(m_ProcInitVal.m_sServerIP, m_fnTrim(next_token));
					}
					else
					{
						*nValuePointer=atoi(next_token);
						nValuePointer++;
					}
				}
				else												//버퍼의 첫 문자가 ';' 일 경우 처리 없음.
				{
					continue;										
				}
			}

			infile.close();

		}
		catch (exception& e)
		{
			wchar_t wszErr[255]; 

			size_t nTemp;

			nTemp = mbstowcs( wszErr, e.what(), strlen(e.what()));

			throw CVs64Exception((char*)wszErr);
		}
		catch (CVs64Exception& e)
		{
			throw e;
		}
		catch(...)
		{
			throw CVs64Exception(_T("예기치 않은 Error 가 발생 되었습니다."));
		}

		return OKAY;
	}

private:
	/*
	*	Module Name			:	m_fnStrngToUpper
	*	Parameter			:	Identify
	*	Return				:	NONE
	*	Function			:	전달받은 문자열을 해당 대분자로 바꿈
	*	Create				:	2006.09.18
	*	Author				:	고정진
	*	Version				:	1.0
	*/
	char* m_fnStrngToUpper(char* chRcvID)       
	{
		char* RcvParam = chRcvID;

		while(*chRcvID != '\0')
		{
			if (*chRcvID >= 'a' && *chRcvID <= 'z')
				*chRcvID-= 32;
			chRcvID++;
		}
		return RcvParam;
	}

	char* m_fnTrim(char* s1)
	{
		int i = 0;
		char *p = s1;
		char s2[_MAX_PATH];

		strcpy(s2, p);
		while(s2[i] != 0){
			while(isspace(s2[i])) // skip while space
				++i;
			if(s2[i] == 0) break;
			*p++ = s2[i++];
		}
		*p = '\0';
		return s1;
	}


};