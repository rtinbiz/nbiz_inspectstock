#pragma once

//#include "VS61LOGMCCaskDefine.h"
#include "../../../CommonHeader/MCCDataStruct.h"

typedef struct WritersInfomationTag
{
	UINT		WriterFileID;
	char		FilePath[512];
	char		FolderName[512];
	UINT		CustodyDate;
	UINT		RecordTime;
	UINT		BlockLogLevel;
	USHORT		Approval[10];
	USHORT		Disapproval[10];
	WritersInfomationTag()						// Default Init Data
	{
		memset(this, 0x00, sizeof(WritersInfomationTag));
		BlockLogLevel =  LOG_LEVEL_4;
	}
}WritersInfo, * LPWritersInfo;

class CLogWriter
{
public:
	CLogWriter(void);
	~CLogWriter(void);
private:
	BOOL		m_bWriterReady;
	CFile		m_FileObj;
	
	WritersInfo	m_WriterInfo;

	// change next Log File
	BOOL m_fnChangeLogFile(int WriteFileID);
	// Create Log File Name
	void m_fnCreateLogFileName(int nFileID, UINT * pRecodTime, char* pFileName, char* pFolderName, int FileNameMaxLength);
public:
	// Initialize Log
	BOOL m_fnisReady(void);
	UINT m_fnGetLogWaiterFileID(void);
	BOOL m_fnInitWriter(LPWritersInfo pInitData);
	
	// Write Log
	BOOL m_fnPrintLogData(WRITE_LOG_MSG* pLogData, USHORT TaskNumber);

	// MCC Log Add
	STRU_MCC_LOG		m_MCCWriteData;
	BOOL				m_fnReadMCC_CommonData();

	CString m_fnChangeMccLog(WRITE_LOG_MSG* pLogData);
	CString m_fnInfoLog_MakeHeader();
	CString m_fnGetTimeMccInfoLog();
	

private:
	//오래된 파일을 제거 한다. 
	BOOL m_fnRemoveOldLogFile(UINT CustodyDate);
	SYSTEMTIME m_StartLogFileTime;
	BOOL m_fnCheckChangeFileTime(void);

};
