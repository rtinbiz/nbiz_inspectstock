
#pragma once
//#include "stdafx.h"

//User Windows Message
#define		WM_USER_CALLBACK					WM_USER + 1000

#define     VS10_TASK_MASTER					10

//VS61LOGMCC Task Number
#define		VS61LOGMCC_TASK_NUMBER				61
#define		VS61LOGMCC_SERVER_IP				"127.0.0.1"

//Log Data Root Dir
//#define	VS61LOGMCC_ROOT_DIRECTORY		"D:\\TEST_LOG\\VS64LOG\\"
#define		VS61LOGMCC_INIT_FILE_PATH			"D:\\nBiz_InspectStock\\EXE\\Config\\APP_T61_Log.DAT"

//default LogCount/File
#define		MAX_FILE_LOG_COUNT					1000

//Log command
#define		FUN_ID_LOG_61						61
#define		SEQ_ID_MCC_WRITE_END				1
#define		SEQ_ID_MCC_LOG_WRITE				2
#define		SEQ_ID_MCC_INFO_VALUE				3

#define		SEQ_ID_MCC_COMMAND					4
	#define     MCC_Unit_Param					1  // Recv Master
	#define     MCC_Unit_File_Write_End			2  // To Master


#define STRING_LENGTHE_LOG_DATA_BUF				150

#define STRING_LENGTHE_LOG_TIME					30
#define STRING_LENGTHE_PATH 					50
#define STRING_LENGTHE_LOG						100

#define T_STR_EQP								"T-A3MSI01N-"
#define I_STR_EQP								"I-A3MSI01N-"
