// VS61LOGMCCDlg.cpp : 구현 파일
//

#include "stdafx.h"
//#include "VS60LogCtr.h"
//#include "VS61LOGMCCaskDefine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
// CVS61LOGMCCDlg 대화 상자
CVS60LogCtr::CVS60LogCtr(void)
{
}

CVS60LogCtr::~CVS60LogCtr(void)
{
	m_fnDeleteWriterList();
}
/*
*	Module name	:	CVS60LogCtr
*	Function	:	Log Writer 목록을 Clear한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
void CVS60LogCtr::m_fnDeleteWriterList(void)
{
	list<CLogWriter*>::iterator	IT_DefectPointList;
	IT_DefectPointList = this->m_WriterLogTasks.begin();
	CLogWriter* pDataBuff = NULL;
	while( IT_DefectPointList != this->m_WriterLogTasks.end())
	{
		pDataBuff = (CLogWriter*)(*IT_DefectPointList);
		delete pDataBuff;
		pDataBuff = NULL;
		(*IT_DefectPointList) =  NULL;

		IT_DefectPointList++;
	}
	m_WriterLogTasks.remove(NULL);
}
/*
*	Module name	:	CVS60LogCtr
*	Function	:	Log Writer를 관리 하기 위해 Writer를 list에 추가 한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL CVS60LogCtr::m_fnAppendWriter(LPWritersInfo pWriterData)
{
	/************************************************************************/
	/*이하는 Log Writer의 추가가 필요한것을 말한다.                         */
	/************************************************************************/
	if(!m_fnCheckDirAndCreate(pWriterData->FilePath))
		return FALSE;

	CLogWriter* pNewWriter = new CLogWriter();
	if(pNewWriter->m_fnInitWriter(pWriterData))
	{//Add Writer
		m_WriterLogTasks.push_back(pNewWriter);
	}
	else
	{//Init Error
		delete pNewWriter;
		pNewWriter = NULL;
		return FALSE;
	}
	return TRUE;
}
/*
*	Module name	:	CVS60LogCtr
*	Function	:	Log 기록을 위한 File위치의 directory를 검사하고 생성 한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL CVS60LogCtr::m_fnCheckDirAndCreate(char * pCheckPath)
{
	char CreateDir[512] = {0,};
	memcpy(CreateDir, pCheckPath, strlen(pCheckPath));
	//뒤에서부터 '\'를 체크한다.(대표이름 자르기)
	for(size_t DirPos= strlen(pCheckPath);  DirPos > 0 && CreateDir[DirPos] != '\\';DirPos--)
	{
		CreateDir[(int)DirPos] = 0;
	}

	DWORD Error = 0;
	::SetLastError(0);

	//앞에서 부터 '\'를체크하여 Directiory를 만든다.
	for(size_t i = 0; CreateDir[(int)i] != 0 ; i++)
	{
		 if(CreateDir[(int)i] == '\\')
		 {
			char CreateDirBuff[512] = {0,};
			memcpy(CreateDirBuff, CreateDir, i);
			::CreateDirectory(CreateDirBuff, NULL);
			Error = ::GetLastError();
			if(ERROR_INVALID_NAME == Error)//Name error
				return FALSE;
			//ERROR_ALREADY_EXISTS no error
		 }
	}
	return TRUE;
}
/*
*	Module name	:	CVS60LogCtr
*	Function	:	요청 Log Data를 Writer를 찾아 기록 한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL CVS60LogCtr::m_fnWriteLogeData(WRITE_LOG_MSG* pLogData, UINT WriterFileID, USHORT TaskNumber)
{
	list<CLogWriter*>::iterator IT_DefectPointList;
	IT_DefectPointList = this->m_WriterLogTasks.begin();
	CLogWriter* pDataBuff = NULL;
	while( IT_DefectPointList != this->m_WriterLogTasks.end())
	{
		pDataBuff = (CLogWriter*)(*IT_DefectPointList);
		if(pDataBuff->m_fnGetLogWaiterFileID() == WriterFileID)
		{//등록되어 Log를 기록 중이던 것은 여기서 기록.
			if(!pDataBuff->m_fnPrintLogData(pLogData, WriterFileID))
				m_fnVS60WriteLogData(0, 0, TaskNumber,"VS60 Log Task Error (Connect the Server)");
			return TRUE;
		}
		pDataBuff = NULL;
		IT_DefectPointList++;
	}
	return FALSE;
}
/*
*	Module name	:	CVS60LogCtr
*	Function	:	Log Task자체 Log를 기록하기위한 Function.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL CVS60LogCtr::m_fnVS60WriteLogData(UINT LogFileID, USHORT LogLevel, USHORT thisTaskNum, char* LogData)
{
	WRITE_LOG_MSG SendLogData;
	ZeroMemory((void*)&SendLogData,sizeof(WRITE_LOG_MSG));
	SendLogData.LogLevel		= LogLevel;

	/////////////////////////////////////////////////////////////////////////////////////////////
	//사용 모듈에 따른 변경요망.(2005 .Net 기준.)
	char LogTime[STRING_LENGTHE_LOG_TIME+1] = {0,};
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime); 
	//Format ex)"2006/10/20-16/48/52-102"
	sprintf_s(LogTime, "%02d:%02d:%02d:%03d", 
		SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, SystemTime.wMilliseconds);

	if(strlen(LogTime) <= STRING_LENGTHE_LOG_TIME)
		StrCpy((char*)SendLogData.LogTime, LogTime);
	if(strlen(LogData) > 0)
		StrCpy((char*)SendLogData.LogData, (char*)LogData);

	m_fnWriteLogeData(&SendLogData, 0, thisTaskNum);
	return TRUE;
}


BOOL CVS60LogCtr::m_fnReadMCC_CommData(UINT WriterFileID, USHORT TaskNumber)
{
	list<CLogWriter*>::iterator IT_DefectPointList;
	IT_DefectPointList = this->m_WriterLogTasks.begin();
	CLogWriter* pDataBuff = NULL;
	while( IT_DefectPointList != this->m_WriterLogTasks.end())
	{
		pDataBuff = (CLogWriter*)(*IT_DefectPointList);
		if(pDataBuff->m_fnGetLogWaiterFileID() == WriterFileID)
		{
			pDataBuff->m_fnReadMCC_CommonData();
			return TRUE;
		}
		pDataBuff = NULL;
		IT_DefectPointList++;
	}
	return FALSE;
}