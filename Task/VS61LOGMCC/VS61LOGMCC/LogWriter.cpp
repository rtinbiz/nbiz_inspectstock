#pragma once
#include "StdAfx.h"
//#include "LogWriter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "..\..\..\CommonHeader\MCCDataStruct.h"

/*
*	Structure name	:	_printfLog
*	Function		:	Client 에서 VS61LOGMCC로 LogData를 보내는 funcion 
*	Create			:	2006.10.23
*	Author			:	최지형.
*	Version			:	1.0
*/

BOOL m_fnCheckDirAndCreate(char * pCheckPath)
{
	char CreateDir[512] = {0,};
	memcpy(CreateDir, pCheckPath, strlen(pCheckPath));
	//뒤에서부터 '\'를 체크한다.(대표이름 자르기)
	for(size_t DirPos= strlen(pCheckPath);  DirPos > 0 && CreateDir[DirPos] != '\\';DirPos--)
	{
		CreateDir[(int)DirPos] = 0;
	}

	DWORD Error = 0;
	::SetLastError(0);

	//앞에서 부터 '\'를체크하여 Directiory를 만든다.
	for(size_t i = 0; CreateDir[(int)i] != 0 ; i++)
	{
		if(CreateDir[(int)i] == '\\')
		{
			char CreateDirBuff[512] = {0,};
			memcpy(CreateDirBuff, CreateDir, i);
			::CreateDirectory(CreateDirBuff, NULL);
			Error = ::GetLastError();
			if(ERROR_INVALID_NAME == Error)//Name error
				return FALSE;
			//ERROR_ALREADY_EXISTS no error
		}
	}
	return TRUE;
}

CLogWriter::CLogWriter(void)	
{
	m_bWriterReady		= FALSE;
}

CLogWriter::~CLogWriter(void)
{
	if(m_bWriterReady)
		m_FileObj.Close();
}
/*
*	Module name	:	CLogWriter
*	Function	:	Log Writer의 준비 상황을 알려준다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL CLogWriter::m_fnisReady(void)
{
	return this->m_bWriterReady;
}
/*
*	Module name	:	CLogWriter
*	Function	:	Log Writer가 기록중인 Log File ID를 알려준다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
UINT CLogWriter::m_fnGetLogWaiterFileID(void)
{
	return this->m_WriterInfo.WriterFileID;
}
/*
*	Module name	:	CLogWriter
*	Function	:	등록 Writer 정보를 이용 Log 기록을 위한 초기화 작업을 수행한다. (File Open.)
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL CLogWriter::m_fnInitWriter(LPWritersInfo pInitData)
{
	memcpy(&this->m_WriterInfo, pInitData, sizeof(WritersInfo));
	//////////////////////////////////////////////////////////////////////////
	//초기화시 삭제 되어야 할파일들 제거.
	m_fnRemoveOldLogFile(m_WriterInfo.CustodyDate);
	//////////////////////////////////////////////////////////////////////////
	//새로운혹은 이전파일 오픈
	//char PathNameFile[512] ={0,};
	//char FolderName[512] ={0,};	

	//StrCpy(PathNameFile, m_WriterInfo.FilePath);
	
	// PathNameFile 검토
	m_fnCreateLogFileName(m_WriterInfo.WriterFileID, &m_WriterInfo.RecordTime, m_WriterInfo.FilePath, m_WriterInfo.FolderName, 512);

	try
	{
		if( !m_FileObj.Open( m_WriterInfo.FilePath, CFile::modeCreate | CFile::modeNoTruncate 
										 | CFile::shareDenyWrite | CFile::modeWrite))
		{
			m_bWriterReady = FALSE;
			return FALSE;
		}

		if(m_WriterInfo.WriterFileID == 2)
		{
			CString strHeaderInfo;
			strHeaderInfo = m_fnInfoLog_MakeHeader();
			m_FileObj.Write(strHeaderInfo.GetBuffer(), strHeaderInfo.GetLength());
		}

		m_FileObj.Seek(0,CFile::end);


	}
	catch (CException* pe)
	{
		m_bWriterReady		= FALSE;

		pe = pe;
		return FALSE;
	}
	m_bWriterReady = TRUE;
	return TRUE;
}
/*
*	Module name	:	CLogWriter
*	Function	:	보관 기간이 지난 Log File을 검색하여 제거 한다.( Init시에 실행한다.)
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL CLogWriter::m_fnRemoveOldLogFile(UINT CustodyDate)
{
	CFileFind finder;
	CString strWildcard="";
	strWildcard.AppendFormat("%s*.csv", this->m_WriterInfo.FilePath);
	/////////
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime); 
	FILETIME CreateFileTime;

	BOOL bWorking = finder.FindFile(strWildcard);
	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (!finder.IsDirectory())
		{
			if(!finder.GetCreationTime(&CreateFileTime))
				return FALSE;
			CTimeSpan ts = CTime(SystemTime)-CTime(CreateFileTime);             // Subtract 2 CTimes
			if(ts.GetTotalSeconds() >= (CustodyDate*24*60*60))
			{//Delete File.
				CFile::Remove(finder.GetFilePath());
			}
		}
	}
	finder.Close();
	return TRUE;
}
/*
*	Module name	:	CLogWriter
*	Function	:	기본 Log File Name에 년 월 일을 추가하여 Log 기록 File Name을 만든다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
//Create Log File name
void CLogWriter::m_fnCreateLogFileName(int nFileID, UINT * pRecodTime, char *pFileName, char* pFolderName, int FileNameMaxLength)
{
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime);
	UINT NameTime = 0;
// 	if((*pRecodTime) == 0 || (*pRecodTime) == 24)
// 	{
// 		NameTime = 0;
// 		sprintf_s(pFileName, FileNameMaxLength, "%s%04d%02d%02d.txt", pFileName,
// 			SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
// 	}
// 	if((*pRecodTime) == 0 || (*pRecodTime) == 24)
// 	{
// 		NameTime = 0;
// 		sprintf_s(pFileName, FileNameMaxLength, "%s%04d%02d%02d.txt", pFileName,
// 			SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
// 	}
// 	else
	{
// 		if((24%(*pRecodTime))!=0)
// 		{//24시간으로 나누어 떨어지지 않으면 작은수를 찾는다.
// 			for(int i = (*pRecodTime); i > 1 ; i--)
// 			{
// 				if((24%(*pRecodTime))!=0)
// 				{
// 					(*pRecodTime) = i;
// 					break;
// 				}
// 			}
// 		}
		CString strEqpName;
		if(nFileID == 1) // T Log
		{
			strEqpName.Format("%s", T_STR_EQP);
		}
		else if(nFileID == 2)
		{
			strEqpName.Format("%s", I_STR_EQP);
		}

		CString strMakeFolder;		

		sprintf_s(pFolderName, FileNameMaxLength, "%04d%02d%02d%02d%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay, SystemTime.wHour, SystemTime.wMinute);
		strMakeFolder.Format("%s%s\\", pFileName, pFolderName);
		
		m_fnCheckDirAndCreate(strMakeFolder.GetBuffer());

		NameTime = (SystemTime.wMinute/(*pRecodTime))*(*pRecodTime);
		sprintf_s(pFileName, FileNameMaxLength, "%s\\%s\\%s%04d%02d%02d%02d%02d00.csv", pFileName, pFolderName, strEqpName, 
		SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay, SystemTime.wHour, NameTime);
		
	}

	//Init Start Time;
	GetLocalTime(&m_StartLogFileTime);
	//m_StartLogFileTime.wHour			= NameTime;
	//m_StartLogFileTime.wHour			= 0;
	//m_StartLogFileTime.wMinute			= NameTime;
	//m_StartLogFileTime.wSecond			= 0;
	//m_StartLogFileTime.wMilliseconds	= 0;
}

int isASCII(int c)
{
	//ASCII 문자인지 판별..
	if(c>=0x00 && c <= 0x7f)
		return 1;
	else 
		return 0;
}

/*
*	Module name	:	CLogWriter
*	Function	:	입력 Log Data를 Log File에 기록한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
// Write Log
BOOL CLogWriter::m_fnPrintLogData(WRITE_LOG_MSG* pLogData, USHORT TaskNumber)
{
	if(!m_bWriterReady)
		return FALSE;
	if(pLogData->LogLevel > this->m_WriterInfo.BlockLogLevel)
		return FALSE;
	//change File
	if(m_fnCheckChangeFileTime())
		if(!m_fnChangeLogFile(this->m_WriterInfo.WriterFileID))
			return FALSE;//File Create Error
	try
	{
		CArchive ar(&this->m_FileObj, CArchive::store);

		char str[MAX_LOG_BUFF] = {0,};

		// MCC Log Data를 변환한다.
		CString strWrite;
		if(TaskNumber == 1)       
			strWrite = m_fnChangeMccLog(pLogData);		
		else
		{
			strWrite = pLogData->LogData;
			strWrite = m_fnGetTimeMccInfoLog() + strWrite;
			//sprintf_s(strWrite.GetBuffer(MAX_LOG_BUFF), MAX_LOG_BUFF, "%s", pLogData->LogData);
		}

		ar.WriteString((LPCTSTR)strWrite);
		ar.WriteString("\r\n");
		ar.Close();

		strWrite.ReleaseBuffer();
	}
	catch (CException* pe)
	{
		pe = pe;
		return FALSE;
	}
	return TRUE;
}


BOOL CLogWriter::m_fnReadMCC_CommonData()
{
	char chReturnStr[MAX_MCC_LOG_BUFF];
	memset(chReturnStr, 0x00, MAX_MCC_LOG_BUFF);

	BOOL bRes = FALSE;
	STRU_MCC_COMM_READ struCommData;
	for(int i=0; i < sizeof(STR_MCC_COMMON_TABLE) / sizeof(CString); i++)
	{
		bRes = GetPrivateProfileString(MCC_SECTION, STR_MCC_COMMON_TABLE[i],"",chReturnStr, MAX_MCC_LOG_BUFF, MCC_COMMON_INI_SAVE_PATH);
		if(bRes != FALSE)	
		{
			memcpy((char*)&m_MCCWriteData  + /*sizeof(SYSTEMTIME) +*/ ( i * MAX_MCC_LOG_BUFF ),  chReturnStr, MAX_MCC_LOG_BUFF);
		}
	}

	return TRUE;
}

CString CLogWriter::m_fnChangeMccLog(WRITE_LOG_MSG* pLogData)
{
	CString strLogData;
	CString strTempData;

	// 처음 4Byte는 Index
	int nLogType = 0;
	memcpy(&nLogType, pLogData->LogData, sizeof(int));

	m_MCCWriteData.ResetEvent();

	SYSTEMTIME sysTime;
	GetLocalTime(&sysTime);

	// 그다음은 System Time
	//memcpy(&m_MCCWriteData.tEventTime, pLogData->LogData+sizeof(int), sizeof(SYSTEMTIME));

	// Module ID
	memcpy(&m_MCCWriteData.chModuleID, pLogData->LogData+sizeof(int) /*+sizeof(SYSTEMTIME)*/, MAX_MCC_LOG_BUFF);
	
	// Stick ID
	memcpy(&m_MCCWriteData.chStickID, pLogData->LogData+sizeof(int) /*+sizeof(SYSTEMTIME)*/ + MAX_MCC_LOG_BUFF*1 , MAX_MCC_LOG_BUFF);
	
	// Write Info
	memcpy(m_MCCWriteData.chEvent_1, pLogData->LogData+sizeof(int)+/*sizeof(SYSTEMTIME) +*/ MAX_MCC_LOG_BUFF*2, 
		sizeof(STRU_MCC_EVENT_INPUT) - sizeof(int) - /*sizeof(SYSTEMTIME)*/ MAX_MCC_LOG_BUFF*2);	
 	
	sprintf_s(m_MCCWriteData.chLogType, "%s", STR_MCC_LOGTYPE_TABLE[nLogType]);
 
 	strTempData.Format("%02d%02d_%02d%02d_%02d.%03d", 
		sysTime.wMonth,
		sysTime.wDay,
		sysTime.wHour,
		sysTime.wMinute,
		sysTime.wSecond,
		sysTime.wMilliseconds); 

	strLogData = strTempData + ","; 
 
 	strTempData = m_MCCWriteData.chModuleID;
 	strLogData+= strTempData + ",";
 
 	strTempData = m_MCCWriteData.chLogType;
 	strLogData+= strTempData + ",";
 
 	strTempData = m_MCCWriteData.chStepID;
 	strLogData+= strTempData + ",";
 
 	strTempData = m_MCCWriteData.chStickID;
 	strLogData+= strTempData + ",";
 
 	strTempData = m_MCCWriteData.chBoxID;
 	strLogData+= strTempData + ",";
 
 	strTempData = m_MCCWriteData.chHostPPID;
 	strLogData+= strTempData + "=";

 	strTempData = m_MCCWriteData.chEqpPPID;
 	strLogData+= strTempData + ","; 
	
	if(nLogType == MCC_ACCTION_LOG || nLogType == MCC_SIGNAL_LOG || nLogType == MCC_EVENT_LOG)
	{
 		strTempData = m_MCCWriteData.chEvent_1;	
 		strLogData+= strTempData + "=";
 
 		strTempData = m_MCCWriteData.chEvent_2;		
 		strLogData+= strTempData + "=";
 
 		strTempData = m_MCCWriteData.chEvent_3;
		if(!strcmp(m_MCCWriteData.chEvent_4, ""))
		{
			strLogData += strTempData;
			return strLogData;
		}
		else
			strLogData+= strTempData + "=";
 
 		strTempData = m_MCCWriteData.chEvent_4;
		if(!strcmp(m_MCCWriteData.chEvent_5, ""))
		{
			strLogData += strTempData;
			return strLogData;
		}
		else
			strLogData+= strTempData + "=";

		strTempData = m_MCCWriteData.chEvent_5;
		strLogData+= strTempData;
	}	

	return strLogData;
}

CString CLogWriter::m_fnGetTimeMccInfoLog()
{
	SYSTEMTIME sysTime;
	GetLocalTime(&sysTime);

	CString strData;

	CString strTime;

	strTime.Format("%02d%02d_%02d%02d_%02d.%03d", 
		sysTime.wMonth,
		sysTime.wDay,
		sysTime.wHour,
		sysTime.wMinute,
		sysTime.wSecond,
		sysTime.wMilliseconds); 

	strData.Format("%s,I,", strTime);

	return strData;

}
CString CLogWriter::m_fnInfoLog_MakeHeader()
{
	BOOL nResultCode = FALSE;

	int nLogCount = sizeof(G_InformationLog_Format) / sizeof(STRU_INFORMATION_LOG);

	CString strHeader;

	strHeader = "EventTime,LogType,";

	CString strParamName, strModuleID, strUnit;

	for(int i=0; i<nLogCount; i++)
	{
		strParamName = G_InformationLog_Format[i].pChParameter;
		strModuleID  = G_InformationLog_Format[i].pChModuleID;
		strUnit		 = G_InformationLog_Format[i].pChUnit;

		strHeader += strModuleID + "=" + strParamName + "(" + strUnit + ")";
		if(i != nLogCount-1)
			strHeader += ",";	
		else 
			strHeader += "\r\n";
	}

	int nSize = strHeader.GetLength();

	return strHeader;//OK;
}

/*
*	Module name	:	CLogWriter
*	Function	:	설정 입력 시간이 지나면 Log File을 변경 한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
// change next Log File
BOOL CLogWriter::m_fnChangeLogFile(int WriteFileID)
{
	// Clear File
	m_FileObj.Close();

	
	// File Write 보고
	G_pServerInteface->m_fnSendCommand_Nrs(VS10_TASK_MASTER, FUN_ID_LOG_61, MCC_Unit_Param, MCC_Unit_File_Write_End, 512, (UCHAR*)m_WriterInfo.FolderName);

	//char PathNameFile[512] ={0,};
	//char FolderName[512] ={0,};

	// New file
	//StrCpy(PathNameFile, m_WriterInfo.FilePath);
	m_fnCreateLogFileName(WriteFileID, &m_WriterInfo.RecordTime, m_WriterInfo.FilePath, m_WriterInfo.FolderName, 512);	

	//

	try
	{
		if( !m_FileObj.Open( m_WriterInfo.FilePath, CFile::modeCreate | CFile::modeNoTruncate 
										 | CFile::shareDenyWrite | CFile::modeWrite))
		{
			m_bWriterReady = FALSE;
			return FALSE;
		}

		if(WriteFileID == 2)
		{
			CString strHeaderInfo;
			strHeaderInfo = m_fnInfoLog_MakeHeader();
			m_FileObj.Write(strHeaderInfo.GetBuffer(), strHeaderInfo.GetLength());

		}

		m_FileObj.Seek(0,CFile::end);
	}
	catch (CException* pe)
	{
		m_bWriterReady		= FALSE;

		pe = pe;
		return FALSE;
	}
	m_bWriterReady = TRUE;
	return TRUE;
}
/*
*	Module name	:	CLogWriter
*	Function	:	Log기록 입력시 Log 기록 File갱신이  필요 한지를 검사 한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL CLogWriter::m_fnCheckChangeFileTime(void)
{
	BOOL resultValue = FALSE;

	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime);

	CTimeSpan ts = CTime(SystemTime) - CTime(m_StartLogFileTime);             // Subtract 2 CTimes
	//if(ts.GetTotalMinutes() >= (this->m_WriterInfo.RecordTime))
	if(ts.GetTotalMinutes() >= (G_MCCFileMakeRefresh_Min))
	{//Delete File.
		resultValue = TRUE;
	}
	else
	{
		resultValue = FALSE;
	}
	return resultValue;
}