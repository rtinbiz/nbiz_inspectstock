// VS61LOGMCCDlg.h : 헤더 파일
//
#pragma once

#include "LogWriter.h"


// CVS61LOGMCCDlg 대화 상자
class CVS60LogCtr
{
// 생성입니다.
public:
	CVS60LogCtr(void);	// 표준 생성자입니다.
	~CVS60LogCtr(void);

private://Log data Control.
	list<CLogWriter*> m_WriterLogTasks;
	void m_fnDeleteWriterList(void);
	BOOL m_fnCheckDirAndCreate(char * pCheckPath);
public:
	//add Log Writer
	BOOL m_fnAppendWriter(LPWritersInfo pWriterData);
	//Target Tasks log Write
	BOOL m_fnWriteLogeData(WRITE_LOG_MSG* pLogData, UINT WriterFileID, USHORT TaskNumber);
	//내부 Log 찍기
	BOOL m_fnVS60WriteLogData(UINT LogFileID, USHORT LogLevel, USHORT thisTaskNum, char* LogData);
	
	//MCC
	BOOL m_fnReadMCC_CommData(UINT WriterFileID, USHORT TaskNumber);
};

