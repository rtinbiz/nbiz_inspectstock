
#pragma once
#include "stdafx.h"
#include "LogWriter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define AFXBEGINTHREAD(pfnThreadProc,pParam) AfxBeginThread(pfnThreadProc, pParam, THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED,NULL)

/*
*	Module name	:	ApplicationInfoTag
*	Struct		:	Log Task의 기본 구동 정보를 저장한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
//Task System Information.
typedef struct ApplicationInfoTag 
{
	char	ServerIP[20];
	USHORT	ServerPortNo;
	USHORT	StationNo;
	USHORT  TaskNo;
	USHORT  Mode;
	USHORT  I_File_RefreshTimeSec;	
	USHORT  File_Write_OnOff;

	ApplicationInfoTag()
	{
		memset(this, 0x00, sizeof(ApplicationInfoTag));
		I_File_RefreshTimeSec = 1000;
		StrCpy(ServerIP, VS61LOGMCC_SERVER_IP);
	}
}AppInfo, *LPAppInfo;

CInterServerInterface ServerInteface;				//Command Interface
CVS60LogCtr G_MainLogControl;						//Log file Control Interface
AppInfo		G_LogTaskInfor;							//기본 Task Information를 나타 낸다.(실행 Task.)
/*
*	Module name	:	Main Function.
*	Function	:	Log Task의 초기화 작업을 수행 한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
BOOL InitLogTask(LPCTSTR pFileName); // Log Task Initialize form file.
/*
*	Module name	:	Main Function.
*	Function	:	Log Task로 들어오는 Command를 처리 한다.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
int m_fnAnalyzeMsg(CMDMSG* cmdMsg);  //Command 처리 Sequence

// MCC  Info Thread
CWinThread  *G_MCCInfoThread;
CString G_strMccInfoValue;
#define MCC_INFO_MAXDATA  1000
float G_InfoDataSumValue[MCC_INFO_MAXDATA];

DWORD dwStart;
volatile bool m_bFlagThread;  


UINT WriteMCC_InfoThread(LPVOID lpThread)
{
	USHORT nSleepTime = (USHORT)lpThread;
	if(G_LogTaskInfor.I_File_RefreshTimeSec <= 0)
	{
		AfxMessageBox("Time 설정 Error");
		return 0;
	}

	while(1)
	{				
		Sleep(G_LogTaskInfor.I_File_RefreshTimeSec * 1000);
		if(m_bFlagThread != TRUE)	break;

		CString strTemp, strDataValue;

		for(int i=0; i<G_InfoSize; i++)
		{
			strTemp.Format("%.3f", G_InfoDataSumValue[i]);
			strDataValue += strTemp;
			if(i != G_InfoSize-1)
			{
				strDataValue += ",";
			}
		}		

		WRITE_LOG_MSG InMessage;
		memcpy(InMessage.LogData, strDataValue, strDataValue.GetLength());

		BOOL nRet = FALSE;
		nRet = G_MainLogControl.m_fnWriteLogeData(&InMessage, 2, 999);		
	}

	return 0;
}
void m_fnReadMCC_Param(int *pRefreshTime, int* pFileMakeTime, int* pSave)
{
	CStdioFile cstFile;

	if (!cstFile.Open(VS61LOGMCC_INIT_FILE_PATH, CFile::modeRead|CFile::typeText, NULL))
	{
		//G_AddLog(3, "Data File Open Error %s", GLASS_ALARM_DAT_PATH);
		return;
	}

	CStringArray strArrayRead;
	CString strRead;
	CString strWriteData;
	CString strValue;

	while(cstFile.ReadString(strRead))
	{
		if(strRead.Find(";") >= 0)
		{
			continue;
		}

		if(strRead.Find("RefreshTime") >= 0)
		{
			strValue = strRead.Mid(strRead.Find("=")+1);
			strValue = strValue.Trim();	
			*pRefreshTime = atoi(strValue);			
		}
		else if(strRead.Find("Record Time") >= 0)
		{
			strValue = strRead.Mid(strRead.Find(":")+1);
			strValue = strValue.Trim();			
			*pFileMakeTime = atoi(strValue);
		}
		else if(strRead.Find("Write OnOff") >= 0)
		{
			strValue = strRead.Mid(strRead.Find("=")+1);
			strValue = strValue.Trim();		
			*pSave = atoi(strValue);
		}
	}

	cstFile.Close();
}

void m_fnSaveMCC_Param()
{
	CStdioFile cstFile;

	if (!cstFile.Open(VS61LOGMCC_INIT_FILE_PATH, CFile::modeRead|CFile::typeText, NULL))
	{
		//G_AddLog(3, "Data File Open Error %s", GLASS_ALARM_DAT_PATH);
		return;
	}	

	CStringArray strArrayRead;
	CString strRead;
	CString strWriteData;
	CString strValue;	

	while(cstFile.ReadString(strRead))
	{
		if(strRead.Find(";") >= 0)
		{
			strArrayRead.Add(strRead);	
		}
		else if(strRead.Find("RefreshTime") >= 0)
		{
			strValue.Format("%d", G_LogTaskInfor.I_File_RefreshTimeSec);
			strWriteData = "RefreshTime     = " + strValue;
			strArrayRead.Add(strWriteData);
		}
		else if(strRead.Find("Record Time") >= 0)
		{
			strValue.Format("%d", G_MCCFileMakeRefresh_Min);
			strWriteData = "Record Time             : " + strValue;
			strArrayRead.Add(strWriteData);
		}
		else if(strRead.Find("Write OnOff") >= 0)
		{
			strValue.Format("%d", G_LogTaskInfor.File_Write_OnOff);
			strWriteData = "Write OnOff     = " + strValue;
			strArrayRead.Add(strWriteData);
		}
		else
			strArrayRead.Add(strRead);	

		strArrayRead.Add("\n");
	}

	cstFile.Close();

	if (!cstFile.Open(VS61LOGMCC_INIT_FILE_PATH, CFile::modeWrite|CFile::typeText, NULL))
	{
		//G_AddLog(3, "Data File Open Error %s", GLASS_ALARM_DAT_PATH);
		return;
	}	

	for(int i=0; i<strArrayRead.GetSize(); i++)
	{
		cstFile.WriteString(strArrayRead.GetAt(i));
	}

	cstFile.Close();

}


/*
*	Module name	:	Main Function.
*	Function	:	Main Function.
*	Create		:	2006.11.08
*	Author		:	최지형.
*	Version		:	1.0
*/
int _tmain(int argc, _TCHAR* argv[])
{
	char*	pMsg;
	char	PathOfInformationFile[512] = {VS61LOGMCC_INIT_FILE_PATH};

//#ifndef VS61LOGMCC_DEBUG
//	FreeConsole();
//#endif
	G_pServerInteface = &ServerInteface;

	if( argv[1] != NULL )//인자 값이 Null이 아니면 기본 Init File이 아닌 주어진 경로의 파일을 사용한다.
	{
		memcpy(PathOfInformationFile, argv[1], strlen(argv[1]));
	}
	if(!InitLogTask(PathOfInformationFile))
	{
		return 0;
	}

	if(PROC_EXE_MODE_RUNTIME == G_LogTaskInfor.Mode)
	{
		FreeConsole();
	}	

	m_fnSaveMCC_Param();	

	int nMmSec = 1000;

	m_bFlagThread = TRUE;
	
	G_MCCInfoThread = AFXBEGINTHREAD(WriteMCC_InfoThread, (LPVOID)G_LogTaskInfor.I_File_RefreshTimeSec);
	G_MCCInfoThread->m_bAutoDelete = FALSE;	
	G_MCCInfoThread->ResumeThread(); 	

	while(true)
	{
		pMsg = ServerInteface.m_fnPeekMessage();

		if(NULL == pMsg)
		{
			G_MainLogControl.m_fnVS60WriteLogData( 0, 0, G_LogTaskInfor.TaskNo, "VS61 MccLog Task Error (Peek Message Fail)");
			return 0;
		}

		//메시지처리...
		if(m_fnAnalyzeMsg((CMDMSG*)pMsg)==0)
		{
			CMDMSG* pMsgBuff =  (CMDMSG*)pMsg;
			char UnknownMessage[100] = {0,};
			sprintf_s(UnknownMessage, "VS61 MccLog Task Warning (UnKnown Message): From T:%d, F:%d, S:%d, U:%d", 
				pMsgBuff->uTask_Src, pMsgBuff->uFunID_Dest, pMsgBuff->uSeqID_Dest, pMsgBuff->uUnitID_Dest);

			G_MainLogControl.m_fnVS60WriteLogData( 0, 0, G_LogTaskInfor.TaskNo, UnknownMessage);
		}

		ServerInteface.m_fnFreeMemory(((CMDMSG*)pMsg)->cMsgBuf);
		ServerInteface.m_fnFreeMemory(pMsg);
	}

// 	m_bFlagThread = FALSE;		
// 
// 	if(NULL != G_MCCInfoThread)
// 	{
// 		::WaitForSingleObject(G_MCCInfoThread->m_hThread,INFINITE);
// 		delete G_MCCInfoThread;
// 		G_MCCInfoThread = NULL;
// 	}

	return 0;
}


//Task Initialize.
BOOL InitLogTask(LPCTSTR pFileName)
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	CFile  InitFileData;
	CFileException e;

	CString strFileData;
	
	if( !InitFileData.Open( pFileName, CFile::shareDenyRead | CFile::modeRead, &e ) )
	{
		AfxMessageBox("VS61 Log Task Can't open Data File");
		return FALSE;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	try
	{
		ULONGLONG dlLength = InitFileData.GetLength();
		dlLength = InitFileData.Read(strFileData.GetBufferSetLength((UINT)dlLength), (UINT)dlLength);
		strFileData.GetBufferSetLength((UINT)dlLength);
		InitFileData.Close();

		CString RealWriterInfo;
		//////////////////////////////////////////////////////////////////////////
		//주석 제거.
		int nToken = 0; 
		CString strToken = strFileData.Tokenize("\r\n", nToken);
		int midFirst = 0;
		while(strToken.GetLength() > 0)
		{
			midFirst = strToken.Find(';');
			if( midFirst != -1)
			{//주석 이후 File 제거. 
				strToken =  strToken.Mid(0,midFirst);
			}
			if(strToken.GetLength()>0)
			{
				strToken.Remove(' ');
				strToken.Remove('\t');
				RealWriterInfo.AppendFormat("%s\r\n", strToken.GetBuffer(strToken.GetLength()));
			}
			strToken.ReleaseBuffer();
			/////////////////
			//다음 Line Read
			strToken = strFileData.Tokenize("\r\n", nToken);
		}
		strFileData.ReleaseBuffer();
		//////////////////////////////////////////////////////////////////////////
		//Data 검출.
		nToken = 0; 
		strToken = RealWriterInfo.Tokenize("\r\n", nToken);
		//APPData
		/*
		Server IP 	= 127.0.0.1
		Station No 	= 0
		Task No 	= 60
		Mode 		= 1
		*/
		strToken = strToken.Mid(strToken.Find("=")+1);
		StrCpy(G_LogTaskInfor.ServerIP, strToken.GetBuffer(strToken.GetLength()));
		strToken = RealWriterInfo.Tokenize("\r\n", nToken);
		G_LogTaskInfor.ServerPortNo	= (USHORT) atoi(strToken.Mid(strToken.Find("=")+1));
		strToken = RealWriterInfo.Tokenize("\r\n", nToken);
		G_LogTaskInfor.StationNo	= (USHORT) atoi(strToken.Mid(strToken.Find("=")+1));
		strToken = RealWriterInfo.Tokenize("\r\n", nToken);
		G_LogTaskInfor.TaskNo		= (USHORT) atoi(strToken.Mid(strToken.Find("=")+1));
		strToken = RealWriterInfo.Tokenize("\r\n", nToken);
		G_LogTaskInfor.Mode			= (USHORT) atoi(strToken.Mid(strToken.Find("=")+1));
		strToken = RealWriterInfo.Tokenize("\r\n", nToken);
		G_LogTaskInfor.I_File_RefreshTimeSec	= (USHORT) atoi(strToken.Mid(strToken.Find("=")+1));
		strToken = RealWriterInfo.Tokenize("\r\n", nToken);
		G_LogTaskInfor.File_Write_OnOff			= (USHORT) atoi(strToken.Mid(strToken.Find("=")+1));
		strToken = RealWriterInfo.Tokenize("\r\n", nToken);
		////////////////////////////////////////////////////////////////////////////
		////Connect Server.
		int ErrorCode = ServerInteface.m_fnRegisterTask(G_LogTaskInfor.TaskNo, G_LogTaskInfor.ServerIP, G_LogTaskInfor.ServerPortNo);
		if(ErrorCode != 0)
		{
			//AfxMessageBox("VS60 Log Task Error (Connect the Server)");
			//G_MainLogControl.m_fnVS60WriteLogData( 0, 0, G_LogTaskInfor.TaskNo, "VS60 Log Task Error (Connect the Server)");
			//return FALSE;

			throw "VS60 Log Task Error (Connect the Server)";
		}


		//Log File 
		/*
		File ID				: 0
		File Name			: D:\VS64Log\Log12\abcd_0
		Custody Date		: 12 
		Record Time			: 4
		Approval Tasks		: 16, 17, 18
		Disapproval Tasks	: 20, 21, 30
		*/
		while(strToken.GetLength() > 0)
		{
			WritersInfo newWriterInfo;
			newWriterInfo.WriterFileID	= (UINT) atoi(strToken.Mid(strToken.Find(":")+1));
			strToken = RealWriterInfo.Tokenize("\r\n", nToken);
			strToken = strToken.Mid(strToken.Find(":")+1);
			StrCpy(newWriterInfo.FilePath, strToken.GetBuffer(strToken.GetLength()));
			strToken = RealWriterInfo.Tokenize("\r\n", nToken);
			newWriterInfo.CustodyDate	= (UINT) atoi(strToken.Mid(strToken.Find(":")+1));
			strToken = RealWriterInfo.Tokenize("\r\n", nToken);
			newWriterInfo.RecordTime	= (UINT) atoi(strToken.Mid(strToken.Find(":")+1));
			strToken = RealWriterInfo.Tokenize("\r\n", nToken);
			newWriterInfo.BlockLogLevel	= (UINT) atoi(strToken.Mid(strToken.Find(":")+1));
			strToken = RealWriterInfo.Tokenize("\r\n", nToken);
			//newWriterInfo.Approval		= (UINT) atoi(strToken.Mid(strToken.Find(":")+1));
			strToken = RealWriterInfo.Tokenize("\r\n", nToken);
			//newWriterInfo.Disapproval	= (UINT) atoi(strToken.Mid(strToken.Find(":")+1));
			strToken = RealWriterInfo.Tokenize("\r\n", nToken);

			G_MCCFileMakeRefresh_Min = newWriterInfo.RecordTime;

			if(!G_MainLogControl.m_fnAppendWriter(&newWriterInfo))
			{
				AfxMessageBox("Error : Log Information File Error");
				return FALSE;
			}
		}
		//////////////////////////////////////////////////////////////////////////
		RealWriterInfo.ReleaseBuffer();

	}
	catch (CException* pe)
	{
		AfxMessageBox("Can't open Data File(Exception)");
		pe = NULL;
		return FALSE;
	}
	catch (char* szErrMsg)
	{
		AfxMessageBox(szErrMsg);
		return FALSE;
	}

	printf("Client Regist OK : %d \n", G_LogTaskInfor.TaskNo);

	G_MainLogControl.m_fnVS60WriteLogData( 0, 0, G_LogTaskInfor.TaskNo, "--> VS61 MccLog Task Start");

	return TRUE;
}
int m_fnAnalyzeMsg(CMDMSG* cmdMsg)
{
	int nRet = OKAY;

	switch(cmdMsg->uFunID_Dest)
	{
	case FUN_ID_LOG_61:
		{
			switch(cmdMsg->uSeqID_Dest)
			{
			case SEQ_ID_MCC_WRITE_END:
				{
					WRITE_LOG_MSG InMessage;
					memcpy(&InMessage, cmdMsg->cMsgBuf, sizeof(WRITE_LOG_MSG));
					nRet = G_MainLogControl.m_fnReadMCC_CommData(cmdMsg->uUnitID_Dest, cmdMsg->uTask_Src);

					if(PROC_EXE_MODE_DEMO == G_LogTaskInfor.Mode)
					{
						char strText[MAX_LOG_BUFF] ={0,};
						sprintf_s(strText, MAX_LOG_BUFF, "From(%3d-%d):[%s] %s\r\n",
							cmdMsg->uTask_Src, InMessage.LogLevel, InMessage.LogTime, InMessage.LogData);
						printf("%s", strText);
					}
					break;
				}
			case SEQ_ID_MCC_LOG_WRITE:
				{
					if(G_LogTaskInfor.File_Write_OnOff == 0)    // False 일때는 Log를 적지 않는다.
						return FALSE;

					WRITE_LOG_MSG InMessage;
					memcpy(&InMessage, cmdMsg->cMsgBuf, sizeof(WRITE_LOG_MSG));

					nRet = G_MainLogControl.m_fnWriteLogeData(&InMessage, cmdMsg->uUnitID_Dest, cmdMsg->uTask_Src);
					if(PROC_EXE_MODE_DEMO == G_LogTaskInfor.Mode)
					{
						//G_MainLogControl.m_fnVS60WriteLogData(0, 4, G_LogTaskInfor.TaskNo, "WRITE_LOG_MSG : VS60 Log Write.....");

						char strText[MAX_LOG_BUFF] ={0,};
						sprintf_s(strText, MAX_LOG_BUFF, "From(%3d-%d):[%s] %s\r\n",
							cmdMsg->uTask_Src, InMessage.LogLevel, InMessage.LogTime, InMessage.LogData);
						printf("%s", strText);
					}
					break;
				}
			case SEQ_ID_MCC_INFO_VALUE:  
				{
					WRITE_LOG_MSG InMessage;
					memcpy(&InMessage, cmdMsg->cMsgBuf, sizeof(WRITE_LOG_MSG));									
					
					memcpy(&G_InfoValue, InMessage.LogData, sizeof(G_InfoValue));

					if(G_InfoValue.nIndex >= 0 && G_InfoValue.nIndex < MCC_INFO_MAXDATA)
						G_InfoDataSumValue[G_InfoValue.nIndex] = G_InfoValue.nfValue;					

					break;
				}
			case SEQ_ID_MCC_COMMAND:  
				{
					// MCC 조성호 씨와 통화 결과 2015/02/12 Host에서 Time 및 Onoff 
					// 기능설정은 쓰지 않는다. Master에서 설정할 수 있게끔 변경				

					// MCC 조성호 씨와 통화 결과 2015/02/12 Host에서 Time 및 Onoff 
					// 기능설정은 쓰지 않는다. Master에서 설정할 수 있게끔 변경				
					char chMsg[100] = {0};
					memcpy(chMsg, cmdMsg->cMsgBuf, cmdMsg->uMsgSize);
					CString subString;

					if(cmdMsg->uUnitID_Dest == MCC_Unit_Param)
					{				
						CString strTime, strOnOff;
						int i = 0;						
						while(AfxExtractSubString(subString, chMsg, i++,_T(',')))
						{
							if(i == 1) // Make Time (min)
							{
								G_MCCFileMakeRefresh_Min = atoi(subString);
							}
							else if(i == 2)  // I file Time Sec
							{
								G_LogTaskInfor.I_File_RefreshTimeSec = atoi(subString);
							}
							else if(i == 3)  // On Off
							{
								G_LogTaskInfor.File_Write_OnOff = atoi(subString);
							}
						}	
						m_fnSaveMCC_Param();
					}				
					break;
				}
			default:
				nRet = 0;
				break;
			}
			break;
		}
	default:
		nRet = 0;
		break;
	}

	return nRet;
}




