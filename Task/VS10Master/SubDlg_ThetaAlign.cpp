// SubDlg_ThetaAlign.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SubDlg_ThetaAlign.h"
#include "afxdialogex.h"


// CSubDlg_ThetaAlign 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSubDlg_ThetaAlign, CDialogEx)

CSubDlg_ThetaAlign::CSubDlg_ThetaAlign(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSubDlg_ThetaAlign::IDD, pParent)
{
	m_pAlignImage1 = nullptr;
	m_pAlignImage2 = nullptr;

	m_pScopeLensOffset = nullptr;
	m_pScopePos = nullptr;
	m_pServerInterface = nullptr;

	m_GrabIndex = 0;
	m_Align1ImgPos = 0;
	m_Align2ImgPos = 0;



	//m_pThetaAlignLiveObj = nullptr;
	m_ThetaMot_Pulse = 0;
}

CSubDlg_ThetaAlign::~CSubDlg_ThetaAlign()
{
	if(m_pAlignImage1 != nullptr)
		cvReleaseImage(&m_pAlignImage1);
	m_pAlignImage1 = nullptr;

	if(m_pAlignImage2 != nullptr)
		cvReleaseImage(&m_pAlignImage2);
	m_pAlignImage2 = nullptr;
}

void CSubDlg_ThetaAlign::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BT_MOVE_ALIGN_1POS, m_btMoveAlignPos1);
	DDX_Control(pDX, IDC_BT_MOVE_ALIGN_2POS, m_btMoveAlignPos2);
	DDX_Control(pDX, IDC_BT_GRAB_ALIGN_IMG, m_btGrabThetaAlignImg);
	DDX_Control(pDX, IDC_ST_THETA_ALIGN_CORRECT_VALUE, m_stThetaAlignCorrectValue);
	DDX_Control(pDX, IDC_BT_THETA_CORRECT, m_btStickThetaCorrect);
	DDX_Control(pDX, IDC_BT_MOVE_AF_1POS, m_btMoveAFPos1);
	DDX_Control(pDX, IDC_BT_MOVE_AF_2POS, m_btMoveAFPos2);
	DDX_Control(pDX, IDC_BT_AUTO_FOCUS, m_btScopeAutofocus);
}


BEGIN_MESSAGE_MAP(CSubDlg_ThetaAlign, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_MOVE_ALIGN_1POS, &CSubDlg_ThetaAlign::OnBnClickedBtMoveAlign1pos)
	ON_BN_CLICKED(IDC_BT_MOVE_ALIGN_2POS, &CSubDlg_ThetaAlign::OnBnClickedBtMoveAlign2pos)
	ON_BN_CLICKED(IDC_BT_GRAB_ALIGN_IMG, &CSubDlg_ThetaAlign::OnBnClickedBtGrabAlignImg)
	ON_BN_CLICKED(IDC_BT_THETA_CORRECT, &CSubDlg_ThetaAlign::OnBnClickedBtThetaCorrect)
	ON_BN_CLICKED(IDC_BT_MOVE_AF_1POS, &CSubDlg_ThetaAlign::OnBnClickedBtMoveAf1pos)
	ON_BN_CLICKED(IDC_BT_MOVE_AF_2POS, &CSubDlg_ThetaAlign::OnBnClickedBtMoveAf2pos)
	ON_BN_CLICKED(IDC_BT_AUTO_FOCUS, &CSubDlg_ThetaAlign::OnBnClickedBtAutoFocus)
	ON_WM_PAINT()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CSubDlg_CDTP 메시지 처리기입니다.


BOOL CSubDlg_ThetaAlign::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	GetDlgItem(IDC_ST_ALIGN_IMG1)->GetClientRect(&m_RectiewArea[0]);
	m_HDCView[0]	=  GetDlgItem(IDC_ST_ALIGN_IMG1)->GetDC()->m_hAttribDC;
	GetDlgItem(IDC_ST_ALIGN_IMG2)->GetClientRect(&m_RectiewArea[1]);
	m_HDCView[1]	=  GetDlgItem(IDC_ST_ALIGN_IMG2)->GetDC()->m_hAttribDC;

	m_pAlignImage1 = cvCreateImage(cvSize(1024,768), IPL_DEPTH_8U, 3);
	m_pAlignImage2 = cvCreateImage(cvSize(1024,768), IPL_DEPTH_8U, 3);

	cvZero(m_pAlignImage1);
	cvZero(m_pAlignImage2);


	//if(m_pThetaAlignLiveObj != nullptr)
	//{
	//	m_pThetaAlignLiveObj->SetLiveDisplayObj(m_HDCView[0], &m_RectiewArea[0]);
	//}
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CSubDlg_ThetaAlign::DrawImageView()
{
	if(m_pAlignImage1 != nullptr)
	{
		CvvImage	ViewImage;
		ViewImage.Create(m_pAlignImage1->width, m_pAlignImage1->height, ((IPL_DEPTH_8U & 255)*3) );
		ViewImage.CopyOf(m_pAlignImage1);
		ViewImage.DrawToHDC(m_HDCView[0], m_RectiewArea[0]);
		ViewImage.Destroy();
	}

	if(m_pAlignImage2 != nullptr)
	{
		CvvImage	ViewImage;
		ViewImage.Create(m_pAlignImage2->width, m_pAlignImage2->height, ((IPL_DEPTH_8U & 255)*3) );
		ViewImage.CopyOf(m_pAlignImage2);
		ViewImage.DrawToHDC(m_HDCView[1], m_RectiewArea[1]);
		ViewImage.Destroy();
	}
}
void CSubDlg_ThetaAlign::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btMoveAlignPos1.GetTextColor(&tColor);
		m_btMoveAlignPos1.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}
		m_btMoveAlignPos1.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMoveAlignPos1.SetTextColor(&tColor);
		m_btMoveAlignPos1.SetCheckButton(true, true);
		m_btMoveAlignPos1.SetFont(&tFont);

		m_btMoveAlignPos2.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMoveAlignPos2.SetTextColor(&tColor);
		m_btMoveAlignPos2.SetCheckButton(true, true);
		m_btMoveAlignPos2.SetFont(&tFont);

		m_btGrabThetaAlignImg.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGrabThetaAlignImg.SetTextColor(&tColor);
		m_btGrabThetaAlignImg.SetCheckButton(true, true);
		m_btGrabThetaAlignImg.SetFont(&tFont);

		m_btStickThetaCorrect.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStickThetaCorrect.SetTextColor(&tColor);
		m_btStickThetaCorrect.SetCheckButton(true, true);
		m_btStickThetaCorrect.SetFont(&tFont);

		m_btMoveAFPos1.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMoveAFPos1.SetTextColor(&tColor);
		m_btMoveAFPos1.SetCheckButton(true, true);
		m_btMoveAFPos1.SetFont(&tFont);

		m_btMoveAFPos2.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMoveAFPos2.SetTextColor(&tColor);
		m_btMoveAFPos2.SetCheckButton(true, true);
		m_btMoveAFPos2.SetFont(&tFont);

		m_btScopeAutofocus.SetRoundButtonStyle(&m_tButtonStyle);
		m_btScopeAutofocus.SetTextColor(&tColor);
		m_btScopeAutofocus.SetCheckButton(true, true);
		m_btScopeAutofocus.SetFont(&tFont);
	}

	m_stThetaAlignCorrectValue.SetFont(&G_TaskStateSmallFont);
	m_stThetaAlignCorrectValue.SetTextColor(RGB(255, 255, 0));
	m_stThetaAlignCorrectValue.SetBkColor(RGB(0, 0, 0));
}
void CSubDlg_ThetaAlign::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


HBRUSH CSubDlg_ThetaAlign::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(IDC_ST_THETA_ALIGN_CORRECT_VALUE == DLG_ID_Number)
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CSubDlg_ThetaAlign::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSubDlg_ThetaAlign::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}







void CSubDlg_ThetaAlign::OnBnClickedBtGrabAlignImg()
{
	//if(m_pServerInterface != nullptr)
	//{
	//	G_WriteInfo(Log_Normal,"Measure Task Grab Align Image");
	//	
	//	if(m_btScopeAutofocus.GetCheck() == false && m_btGrabThetaAlignImg.GetCheck() == false)
	//	{
	//			CString strSavePath;
	//			if(m_GrabIndex ==1)
	//			{
	//				strSavePath.Format("%s", THEATA_ALIGN_GRAB1_IMG_PATH);
	//				m_Align1ImgPos = 0;
	//			}
	//			else if(m_GrabIndex==2)
	//			{
	//				strSavePath.Format("%s", THEATA_ALIGN_GRAB2_IMG_PATH);
	//				m_Align2ImgPos = 0;
	//			}
	//			else
	//			{
	//				strSavePath.Format("%s", THEATA_ALIGN_GRABT_IMG_PATH);
	//				m_Align1ImgPos = 0;
	//			}

	//			m_pServerInterface->m_fnSendCommand_Nrs(
	//				TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_AlignGrabImg, nBiz_Unit_Zero, strSavePath.GetLength() , (UCHAR*)strSavePath.GetBuffer(strSavePath.GetLength()));
	//			m_btGrabThetaAlignImg.SetCheck(true);
	//	}
	//	else
	//		G_WriteInfo(Log_Worrying, "Now AF Running Or Grab Running");
	//	
	//
	//}
	//else
	//	G_WriteInfo(Log_Error,"Server Obj Link Error!!!!");
}


void CSubDlg_ThetaAlign::OnBnClickedBtThetaCorrect()
{
	G_WriteInfo(Log_Normal,"Theta Align Theta Correct");
	if(m_ThetaMot_Pulse>0)
	{
		m_btStickThetaCorrect.SetCheck(true);
		int TT01_AlignPos = G_MotInfo.m_pMotState->nMotionPos[AXIS_TT01_ThetaAlign];
		TT01_AlignPos = TT01_AlignPos+m_ThetaMot_Pulse;
		G_MotObj.m_fnTT01AlignMove(TT01_AlignPos);
		m_ThetaMot_Pulse = 0;
	}
}

void CSubDlg_ThetaAlign::OnBnClickedBtMoveAf1pos()
{
	//if(m_btMoveAlignPos1.GetCheck() == true && m_btMoveAlignPos2.GetCheck() == true  &&
	//	m_btMoveAFPos1.GetCheck() == true && m_btMoveAFPos2.GetCheck() == true )
	//{
	//	G_WriteInfo(Log_Normal,"3D Scope Move Auto Focus Pos 1");
	//	G_MotObj.m_fnScopeMultiMove(m_pScopePos->nThetaAlignPos1_Shift+m_pScopePos->nThetaAlignPosAFDis_Shift, m_pScopePos->nThetaAlignPos1_Drive);
	//	m_btMoveAFPos1.SetCheck(true);
	//}
	//else
	//	G_WriteInfo(Log_Worrying, "Now Measure Motion Run");
}


void CSubDlg_ThetaAlign::OnBnClickedBtMoveAf2pos()
{
	//if(m_btMoveAlignPos1.GetCheck() == true && m_btMoveAlignPos2.GetCheck() == true  &&
	//	m_btMoveAFPos1.GetCheck() == true && m_btMoveAFPos2.GetCheck() == true )
	//{
	//	G_WriteInfo(Log_Normal,"3D Scope Move Auto Focus Pos 2");
	//	G_MotObj.m_fnScopeMultiMove(m_pScopePos->nThetaAlignPos2_Shift+m_pScopePos->nThetaAlignPosAFDis_Shift, m_pScopePos->nThetaAlignPos2_Drive);
	//	m_btMoveAFPos2.SetCheck(true);
	//	
	//}
	//else
	//	G_WriteInfo(Log_Worrying, "Now Measure Motion Run");
}
void CSubDlg_ThetaAlign::OnBnClickedBtMoveAlign1pos()
{
	if(m_btMoveAlignPos1.GetCheck() == true && m_btMoveAlignPos2.GetCheck() == true  &&
		m_btMoveAFPos1.GetCheck() == true && m_btMoveAFPos2.GetCheck() == true )
	{
		G_WriteInfo(Log_Normal,"3D Scope Move Align Pos 1");
		G_MotObj.m_fnScopeMultiMove(m_pScopePos->nThetaAlignPos1_Shift, m_pScopePos->nThetaAlignPos1_Drive);
		m_btMoveAlignPos1.SetCheck(true);

		m_GrabIndex = 1;
	}
	else
		G_WriteInfo(Log_Worrying, "Now Measure Motion Run");
}
void CSubDlg_ThetaAlign::OnBnClickedBtMoveAlign2pos()
{
	if(m_btMoveAlignPos1.GetCheck() == true && m_btMoveAlignPos2.GetCheck() == true  &&
		m_btMoveAFPos1.GetCheck() == true && m_btMoveAFPos2.GetCheck() == true )
	{
		G_WriteInfo(Log_Normal,"3D Scope Move Align Pos 2");
		G_MotObj.m_fnScopeMultiMove(m_pScopePos->nThetaAlignPos2_Shift, m_pScopePos->nThetaAlignPos2_Drive);
		m_btMoveAlignPos2.SetCheck(true);

		m_GrabIndex = 2;
	}
	else
		G_WriteInfo(Log_Worrying, "Now Measure Motion Run");
}

void CSubDlg_ThetaAlign::OnBnClickedBtAutoFocus()
{
	if(m_pServerInterface != nullptr)
	{
		if(m_btGrabThetaAlignImg.GetCheck() == false && m_btScopeAutofocus.GetCheck() == false)
		{
			if(m_btMoveAlignPos1.IsWindowEnabled() == TRUE && m_btMoveAlignPos2.IsWindowEnabled() == TRUE  &&
				m_btMoveAFPos1.IsWindowEnabled() == TRUE && m_btMoveAFPos2.IsWindowEnabled() == TRUE  )
			{
				G_WriteInfo(Log_Info,"Measure Task Auto Focus");
				m_pServerInterface->m_fnSendCommand_Nrs(
					TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_AutoFocus, nBiz_Unit_Zero);
				m_btScopeAutofocus.SetCheck(true);

			}
			else
				G_WriteInfo(Log_Worrying, "Now Measure Motion Run AutoFocus");
		}
		else
			G_WriteInfo(Log_Worrying, "Now AF Running Or Grab Running");
	}
	else
		G_WriteInfo(Log_Error,"Server Obj Link Error!!!!");
}


void CSubDlg_ThetaAlign::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
	DrawImageView();
}


void CSubDlg_ThetaAlign::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	//if(bShow == TRUE)
	//{
	//	m_pThetaAlignLiveObj->SetCamLiveMode(LIVE_MODE_VIEW);
	//}
	//else
	//{
	//	m_pThetaAlignLiveObj->SetCamLiveMode(LIVE_MODE_GRAB);
	//}
}
