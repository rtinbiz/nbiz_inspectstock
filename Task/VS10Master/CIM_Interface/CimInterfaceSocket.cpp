#include "StdAfx.h"
#include "CimInterfaceSocket.h"

//#include "CimInterfaceTestDlg.h"

IMPLEMENT_DYNAMIC(CCimInterfaceSocket, CWnd)

CCimInterfaceSocket::CCimInterfaceSocket(void)
{
	// 클래스 생성 **
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));

	wndclass.style			= CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= AfxWndProc;
	wndclass.hInstance		= AfxGetInstanceHandle();
	wndclass.hIcon			= NULL;
	wndclass.hCursor		= AfxGetApp()->LoadStandardCursor(IDC_ARROW);
	wndclass.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wndclass.lpszMenuName	= NULL;
	wndclass.lpszClassName	= "CimSocket";

	AfxRegisterClass(&wndclass);

	CreateEx(NULL, "CimSocket", NULL, NULL, CRect(0,0,0,0), NULL, NULL, NULL);
	// 클래스 생성 ##	

	m_ClientSocket.InitSocket(this);
	memset(m_chServerIP, 0x00, 50);
	memset(m_chServerPortNum, 0x00, 50);

//	m_cimInfodlg.m_fnInit(this);
	m_CIMInfodlg = nullptr;

	for(int InStep = 0; InStep<= OperatorID; InStep++)
	{
		m_strArrayMaterialInfo.Add("");
	}
	
	m_bRecvMaterialData = false;
	m_bRecvJobStartCMD = false;
	InitializeCriticalSection(&m_CriCimInter);
}

CCimInterfaceSocket::~CCimInterfaceSocket(void)
{
	DeleteCriticalSection(&m_CriCimInter);
	//KillTimer(TIMER_CIM_CHECK);
	m_ClientSocket.Close();
	KillTimer(TIMER_CIM_RECONNECT);
}


BEGIN_MESSAGE_MAP(CCimInterfaceSocket, CWnd)
	ON_WM_TIMER()
	ON_MESSAGE(UM_CIM_RECEIVE, OnClientReceive)	// Client로 부터 수신되는 데이터
	ON_MESSAGE(UM_CIM_CLOSE, OnClientReConnect)	// Client로 부터 수신되는 데이터
	ON_MESSAGE(UM_CIM_CONNECT_OK, OnConnectOK)	// Client로 부터 수신되는 데이터

END_MESSAGE_MAP()

void CCimInterfaceSocket::m_fnInitSocket(CInterServerInterface *pServerInterface)
{
	m_pServerInterface = pServerInterface;

	iniSocket.SetFileName(CIM_INFO_PATH);

	iniSocket.GetProfileString("CIM_NETWORK", "IP", "127.0.0.1", m_chServerIP, 50);
	iniSocket.GetProfileString("CIM_NETWORK", "PORT", "5000", m_chServerPortNum, 50);	

	BOOL  bRes = FALSE;
	bRes = m_ClientSocket.Create();

	if(bRes)
	{
		SendMsgCIMView(Log_Check, "Cim Clinet 생성");
	}
	else
	{
		SendMsgCIMView(Log_Worrying, "Cim Clinet 생성 실패");
		return;
	}

	SetTimer(TIMER_CIM_RECONNECT, 1000, NULL);
}

void CCimInterfaceSocket::m_fnSendToCim(CString strMessageDefine, CString strData1, CString strData2)
{
	EnterCriticalSection(&m_CriCimInter);
	int nCnt = atoi(m_CimDataFormat.chNumber);
	CString strSendData, strCRLF;
	strCRLF.Format("%c%c",13, 10);
	
	if(strData2 != "")
		strSendData.Format("%d|%s|%s|%s|%s", nCnt, strMessageDefine, strData1, strData2, strCRLF);	
	else
		strSendData.Format("%d|%s|%s|%s", nCnt, strMessageDefine, strData1, strCRLF);	

 	m_ClientSocket.Send(strSendData.GetBuffer(), strSendData.GetLength());
 	strSendData.ReleaseBuffer();
	LeaveCriticalSection(&m_CriCimInter);
}

void CCimInterfaceSocket::OnTimer(UINT_PTR nIDEvent)
{
// 	if(nIDEvent == TIMER_CIM_CHECK)
// 	{
// 		//m_fnSendToCim(SEND_LOAD_READY_ACK, "OK");
// 	}
	if(nIDEvent == TIMER_CIM_RECONNECT)
	{
		m_ClientSocket.Connect(m_chServerIP, atoi(m_chServerPortNum));

		if(m_bConnect == TRUE)
		{

			SendMsgCIMView(Log_Normal, "Cim 접속 성공");
			KillTimer(TIMER_CIM_RECONNECT);
		}
	}

	CWnd::OnTimer(nIDEvent);
}

LRESULT CCimInterfaceSocket::OnClientReceive(WPARAM wParam, LPARAM lParam)
{
// 	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
// 
// 	if(G_SystemModeData.unSystemMode == INSPECT_EXIT || pMainWnd == NULL)
// 		return 0;

	int nReceiveSize = (int)wParam;	

	if(nReceiveSize <= 0 || nReceiveSize >= 4096)
	{
		SendMsgCIMView(Log_Normal, "Return Receive Cim %d", nReceiveSize);
		return 0;
	}

	char* buf = new char [nReceiveSize+1];
	memset(buf, 0x00, nReceiveSize+1);
	memcpy(buf, (void*)lParam, nReceiveSize);

	CString strReadData;
	CString strCRLF;
	strCRLF.Format("%c%c",13, 10);	

	for(int i  = 0 ; i < nReceiveSize; i++)
	{	
		char chRead = 0;
		chRead = buf[i];
		strReadData.Format("%c", chRead);

		m_strReciveString += strReadData;

		if(m_strReciveString.GetLength() > 2)
		{
			if(m_strReciveString.Right(2) == strCRLF)
			{
				m_strReciveString = m_strReciveString.TrimRight();
				CString subString;
				CString subString1;
				BOOL bLogExcept = FALSE;

				int i = 0;				
				while(AfxExtractSubString(subString, (LPCTSTR)m_strReciveString, i++,_T('|')))
				{
					if(m_strReciveString.Find(RECV_CIM_ALIVE) > 0)
						bLogExcept = TRUE;
					if(i == 1)     // Msg Number
					{
						if(subString.GetLength() > sizeof(m_CimDataFormat.chNumber))
						{
							continue;
						}
						memcpy(m_CimDataFormat.chNumber, subString, subString.GetLength());
						if(bLogExcept != TRUE)
							SendMsgCIMView(Log_Normal,"From Cim CNT : %s", m_CimDataFormat.chNumber);
					}
					else if(i == 2) // Msg Name
					{
						if(subString.GetLength() > sizeof(m_CimDataFormat.chMessageName))
						{
							continue;
						}						
						memcpy(m_CimDataFormat.chMessageName, subString, subString.GetLength());
						if(bLogExcept != TRUE)
						{
							SendMsgCIMView(Log_Normal, "From Cim Num Define : %s", m_CimDataFormat.chMessageName);
						}
					}
					else if(i == 3)   // Data 1
					{
						if(subString.GetLength() > sizeof(m_CimDataFormat.chData1))
						{

							continue;
						}
						memcpy(m_CimDataFormat.chData1, subString, subString.GetLength());
						if(bLogExcept != TRUE)
							SendMsgCIMView(Log_Normal, "From Cim Data1 : %s", m_CimDataFormat.chData1);
					}
					else if(i == 4)    // Data 2
					{						
						if(subString.GetLength() > sizeof(m_CimDataFormat.chData2) || subString.GetLength() == 0)
						{							
							continue;
						}
						memcpy(m_CimDataFormat.chData2, subString, subString.GetLength());
						if(bLogExcept != TRUE)
						{
							if(strlen(m_CimDataFormat.chData2)<30)
								SendMsgCIMView(Log_Normal,"From Cim Data2 : %s", m_CimDataFormat.chData2);
							else
								SendMsgCIMView(Log_Normal,"From Cim Data2 : Long Size");
						}
					}
					else if(i > 5)
					{
						if(bLogExcept != TRUE)
							SendMsgCIMView(Log_Worrying, "Error From Cim : %s", subString);
					}					
				} 

				if(!strcmp(m_CimDataFormat.chMessageName, RECV_CIM_ALIVE))
				{
					m_fnSendToCim(SEND_CIM_ALIVE_ACK, DATA1_OK);
				}		
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_SVID_REQUEST))        
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_SVID_REQUEST);
					//sendCim SVID List를 보내준다.
					CString strSVIDValue;
					if(m_CIMInfodlg != nullptr)
					{
						strSVIDValue.Format("");

						char strReadData[33];
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_TENSION_GRIP, "0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_TENSION_FINE, "0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_INSPEC_CAM_GRAY_MAX, "0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_INSPEC_CAM_GRAY_MIN, "0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_INSPEC_CAM_VOLTAGE, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_INSPEC_CAM_TEMPERATURE, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_INSPEC_DARKFIELD_VALUE, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_INSPEC_BACKLIGHT_VALUE, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_INSPEC_MACRO_LIGHT_VALUE, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_REVIEW_IMG_CONTRAST, "0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_STICK_REVIEWAF_TIME, "0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_MOT_AXIS_MEASURE_X1_OVERLOAD, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_MOT_AXIS_MEASURE_X2_OVERLOAD, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_MOT_AXIS_MEASURE_Y_OVERLOAD, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_MOT_AXIS_INSPECT_X_OVERLOAD, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_MOT_AXIS_INSPECT_Y_OVERLOAD, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_TEMPERATURE_STATE_PC_BOX, "0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_TEMPERATURE_STATE_ELECTRICAL_BOX, "0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_PNEUMATIC_AIR, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s,", strReadData);
						memset(strReadData, 0x00, 33);
						GetPrivateProfileString(Section_SVID, Key_PNEUMATIC_N2, "0.0", &strReadData[0], 32, SVID_LIST_PATH);
						strSVIDValue.AppendFormat("%s", strReadData);
					}
					m_fnSendToCim(SEND_SVID_REQUEST_ACK, DATA1_OK, strSVIDValue);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_LOAD_COMPLETE_ACK))
 				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_LOAD_COMPLETE_ACK);
					//sendCim LoadComp 를 보내준다.
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))       
					{					

					}
					else // Error
					{

					}				
				}	
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_MATERIAL_INFO))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_MATERIAL_INFO);
					int i = 0;			

					m_strArrayMaterialInfo.RemoveAll();
					while(AfxExtractSubString(subString, m_CimDataFormat.chData2, i++,_T(',')))
					{
						m_strArrayMaterialInfo.Add(subString);
					}
					CString strPPID=m_strArrayMaterialInfo.GetAt(PPID);
					
					if(m_fnRecipeIDCheck(strPPID) == FALSE)
					{
						// Alarm 코드 작업
						SendMsgCIMView(Log_Error, "PPID Error : %s", strPPID);
						G_AlarmAdd(AL_2133);//Recipe : Recipe Not Found
					}

					if(m_CIMInfodlg != nullptr)
					{
						WritePrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, strPPID.GetBuffer(strPPID.GetLength()), VS10Master_PARAM_INI_PATH);
						::SendMessage(m_CIMInfodlg->GetParent()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 0);
					}

					if(m_CIMInfodlg != nullptr)
					{
						m_CIMInfodlg->GetDlgItem(IDC_ST_MATERIAL_INFO)->SetWindowText("MATERIAL_INFO : OK");
						m_bRecvMaterialData = true;
					}

					m_fnSendToCim(SEND_MATERIAL_INFO_ACK, DATA1_OK);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_JOBSTART_CMD))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_JOBSTART_CMD);

					int nError = m_fnGetSystemErrorCheck(TRUE);   // True 이면 Error 종류별 Type을 Return 한다.
					if(nError != ERROR_NORMAL)                    // Error Type에 따른 Ack
					{
						CString strSend;
						strSend.Format("%0d", nError);
						m_fnSendToCim(SEND_JOBSTART_CMD_ACK, strSend);

						if(m_CIMInfodlg != nullptr)
						{
							m_CIMInfodlg->GetDlgItem(IDC_ST_JOBSTART_CMD_ACK)->SetWindowText("JOBSTART_CMD_ACK : OK");
							m_bRecvJobStartCMD = true;
						}
						
					}					
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_INSPECTION_START_ACK))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_INSPECTION_START_ACK);
					//sendCim Inspection Start 를 보내준다.
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_INSPECTION_COMPLETE_ACK))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_INSPECTION_COMPLETE_ACK);
					//sendCim Inspection Comp 를 보내준다.
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_UNLOAD_COMPLETE_ACK))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_UNLOAD_COMPLETE_ACK);
					//sendCim Unload Comp 를 보내준다.
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_EQST_REQUEST))  
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_EQST_REQUEST);
					// Eqp Status Return 1. NORMAL, 2. FAULT 3. PM
					// Alarm 이 있으면 Alarm ID List를 보고
					CString strSend;
					int nEqpStatus = EQP_NORMAL;

					if(m_CIMInfodlg != nullptr)
					{
					//if(m_CIMInfodlg->m_pEQST_Mode != nullptr)
						nEqpStatus = m_CIMInfodlg->m_EQST_Mode;
						strSend.Format("%d,%s,", nEqpStatus, m_CIMInfodlg->SendAlarmList());
						m_fnSendToCim(SEND_EQST_REQUEST_ACK, DATA1_OK, strSend);
					}
					else
						m_fnSendToCim(SEND_EQST_REQUEST_ACK, DATA1_ERR, "");
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_EQST_CHANGE_REPORT_ACK))  
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_EQST_CHANGE_REPORT_ACK);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_EQST_CHANGE_CMD))   // 설비상태 변경 지시
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_EQST_CHANGE_CMD);
					// Data2 : CMD(Normal=1, PM=3),ByWho(ByHost=1, ByOperator=2, ByEquipment itself=3)
					int i = 0;				
					while(AfxExtractSubString(subString, m_CimDataFormat.chData2, i++,_T(',')))
					{
						if(i == 1) // CMD Normal or PM 이 온다...
						{
							if( atoi(subString) == EQP_NORMAL ) // EQP Normal 로 변경 지시
							{
								if(m_CIMInfodlg != nullptr)
								{
									//if(m_CIMInfodlg->m_pEQST_Mode != nullptr)
										m_CIMInfodlg->m_EQST_Mode = EQP_NORMAL;
								}
								SendMsgCIMView(Log_Info, "==> EQP_NORMAL");
							}
							else if(atoi(subString) == EQP_PM ) // Eqp Pm으로 변경 지시 
							{
								//if(m_CIMInfodlg->m_pEQST_Mode != nullptr)
									m_CIMInfodlg->m_EQST_Mode = EQP_PM;
									m_CIMInfodlg->m_PRST_Process = PRST_SETUP;
									G_VS10TASK_STATE = TASK_STATE_PM;

								SendMsgCIMView(Log_Info, "==> EQP_PM");
							}
						}
						else if(i == 2) // ByWho 
						{
							if( atoi(subString) == BY_HOST ) 
							{
								SendMsgCIMView(Log_Info, "==> BY_HOST");
							}
							else if( atoi(subString) == BY_OP) 
							{
								SendMsgCIMView(Log_Info, "==> BY_OP");
							}
							else if( atoi(subString) == BY_EQUIP) 
							{
								SendMsgCIMView(Log_Info, "==> BY_EQUIP");
							}
						}
					}

					m_fnSendToCim(SEND_EQST_CAHNGE_CMD_ACK, DATA1_OK);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_PRST_REQUEST))  // 설비 Process 상태 조회
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_PRST_REQUEST);
					//"PRST(Process State : IDLE=1, SETUP=2, EXECUTE=3, PAUSE=4, DISABLE=5),
					//ByWho(ByHost=1, ByOperator=2, ByEquipment itself=3), ALID(alarm ID)"

					CString strSend;
					int nEqpProcess = EQP_NORMAL;

					if(m_CIMInfodlg != nullptr)
					{
						//if(m_CIMInfodlg->m_pPRST_Process != nullptr)
							nEqpProcess = m_CIMInfodlg->m_PRST_Process;
					}
					else
					{
						m_fnSendToCim(SEND_PRST_REQUEST_ACK, DATA1_ERR, "");
					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_PRST_CHANGE_REPORT_ACK))
				{		
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_PRST_CHANGE_REPORT_ACK);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_PRST_CHANGE_CMD))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_PRST_CHANGE_CMD);
					// host로 부터 Pause
					int i = 0;				
					while(AfxExtractSubString(subString, m_CimDataFormat.chData2, i++,_T(',')))
					{
						if(i == 1) // CMD(PAUSE=4, RESUME=8),ByWho(ByHost=1, ByOperator=2, ByEquipment itself=3)
						{
							if( atoi(subString) ==  PRST_PAUSE) 
							{
								if(m_CIMInfodlg != nullptr)
								{
									//if(m_CIMInfodlg->m_pPRST_Process != nullptr)
									{
										
										m_CIMInfodlg->m_PRST_Process_Resume = m_CIMInfodlg->m_PRST_Process;
										m_CIMInfodlg->m_PRST_Process = PRST_PAUSE;
									}

									SendMsgCIMView(Log_Info, "==> PRST_PAUSE");
								}
							}
							else if(atoi(subString) == PRST_RESUME) // Eqp Pm으로 변경 지시 
							{
								if(m_CIMInfodlg != nullptr)
								{
									//if(m_CIMInfodlg->m_pPRST_Process != nullptr)
									{
										m_CIMInfodlg->m_PRST_Process = m_CIMInfodlg->m_PRST_Process_Resume;
										m_CIMInfodlg->m_PRST_Process_Resume = PRST_RESUME;
									}

									SendMsgCIMView(Log_Info, "==> PRST_RESUME");
								}
							}
						}
						else if(i == 2)
						{
							if( atoi(subString) == BY_HOST ) 
							{
								SendMsgCIMView(Log_Info, "==> BY_HOST");
							}
							else if( atoi(subString) == BY_OP) 
							{
								SendMsgCIMView(Log_Info, "==> BY_OP");
							}
							else if( atoi(subString) == BY_EQUIP) 
							{
								SendMsgCIMView(Log_Info, "==> BY_EQUIP");
							}
						}
					}

					m_fnSendToCim(SEND_PRST_CHANGE_CMD_ACK, DATA1_OK);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_JOB_PROCESS_CMD))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_JOB_PROCESS_CMD);
					int i = 0;				
					while(AfxExtractSubString(subString, m_CimDataFormat.chData2, i++,_T(',')))
					{
						if(i == 1) // Process Abort or Cancel 이 온다. 6 혹은 7
						{
							if( atoi(subString) == PROCESS_ABORT )
							{//Start후 들어오고 현재 것을 NG로 리턴한다.JUDGEMENT = ‘NG’
								
							}
							else if(atoi(subString) == PROCESS_CANCEL)
							{//Start전에 들어온다. 검사 진행 없이 뺀다.(Normal OK로 보고, 협의 하자)
							
							}
							else
							{

							}
						}
						else if(i == 2)
						{
							if( atoi(subString) == BY_HOST ) 
							{

							}
							else if( atoi(subString) == BY_OP) 
							{

							}
							else if( atoi(subString) == BY_EQUIP) 
							{

							}
						}
					}	
					m_fnSendToCim(SEND_JOBSTART_CMD_ACK, DATA1_OK);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_TIME_SET)) // 시간 동기화 
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_TIME_SET);
					// 어느 특정한 시점에만 Time 동기화를 진행한다. IDLE이나 Normal 상태에서만 받게끔.
					if(m_fnTimeSyncSet(m_CimDataFormat.chData2))
					{
						m_fnSendToCim(SEND_TIME_SET_ACK, DATA1_OK);
					}
					else
					{
						m_fnSendToCim(SEND_TIME_SET_ACK, DATA1_ERR);
					}					
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_BUZZER_CMD)) 
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_BUZZER_CMD);
					// Buzzer On Off
					if(!strcmp(m_CimDataFormat.chData1, DATA1_ON))
					{
						// Buzzer On
						G_MotObj.m_fnSetSignalTower_BuzOn();
					}
					else if(!strcmp(m_CimDataFormat.chData1, DATA1_OFF))
					{
						// Buzzer Off
						G_MotObj.m_fnSetSignalTower_StopBuz();
					}

					m_fnSendToCim(SEND_BUZZER_CMD_ACK, DATA1_OK);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_MCMD_REQUEST_ACK)) 
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_MCMD_REQUEST_ACK);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{
						if( atoi(m_CimDataFormat.chData2) == OFFLINE)
						{
							if(m_CIMInfodlg != nullptr)
							{
								m_CIMInfodlg->GetDlgItem(IDC_ST_MCMD_STATE)->SetWindowText("OFFLINE");
							}
							SendMsgCIMView(Log_Info, "==> OFFLINE");
						}
						else if(atoi(m_CimDataFormat.chData2) == ONLINE_LOCAL)
						{
							if(m_CIMInfodlg != nullptr)
							{
								m_CIMInfodlg->GetDlgItem(IDC_ST_MCMD_STATE)->SetWindowText("ONLINE_LOCAL");
							}
							SendMsgCIMView(Log_Info, "==> ONLINE_LOCAL");
						}
						else if(atoi(m_CimDataFormat.chData2) == ONLINE_REMOTE)
						{
							if(m_CIMInfodlg != nullptr)
							{
								m_CIMInfodlg->GetDlgItem(IDC_ST_MCMD_STATE)->SetWindowText("ONLINE_REMOTE");
							}
							SendMsgCIMView(Log_Info, "==> ONLINE_REMOTE");
						}
					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_MCMD_CHANGE_REPORT))
				{		
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_MCMD_CHANGE_REPORT);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_REPORT))
					{
						if( atoi(m_CimDataFormat.chData2) == OFFLINE)
						{
							if(m_CIMInfodlg != nullptr)
							{
								m_CIMInfodlg->GetDlgItem(IDC_ST_MCMD_STATE)->SetWindowText("OFFLINE");
							}
							SendMsgCIMView(Log_Info, "==> OFFLINE");
						}
						else if(atoi(m_CimDataFormat.chData2) == ONLINE_LOCAL)
						{
							if(m_CIMInfodlg != nullptr)
							{
								m_CIMInfodlg->GetDlgItem(IDC_ST_MCMD_STATE)->SetWindowText("ONLINE_LOCAL");
							}
							SendMsgCIMView(Log_Info, "==> ONLINE_LOCAL");
						}
						else if(atoi(m_CimDataFormat.chData2) == ONLINE_REMOTE)
						{
							if(m_CIMInfodlg != nullptr)
							{
								m_CIMInfodlg->GetDlgItem(IDC_ST_MCMD_STATE)->SetWindowText("ONLINE_REMOTE");
							}
							SendMsgCIMView(Log_Info, "==> ONLINE_REMOTE");
						}
					}
					else  // From Cim Error
					{

					}

					m_fnSendToCim(SEND_MCMD_CHANGE_REPORT_ACK, DATA1_OK);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_STICK_EXIST_REPORT_ACK))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_STICK_EXIST_REPORT_ACK);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{
						
					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_ALARM_REPORT_ACK))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_ALARM_REPORT_ACK);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_ALARM_WAITLIST_REQUEST))  // Alarm List Request
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_ALARM_WAITLIST_REQUEST);
					// Alarm List Data 를 보내준다.  ALID1,ALID2,.....ALIDN 이 데이터 2로 들어간다.
					CString strAlarmIDList;
					if(m_CIMInfodlg != nullptr)
					{
						m_fnSendToCim(SEND_ALARM_WAITLIST_REQUEST_ACK, DATA1_OK,  m_CIMInfodlg->SendAlarmList());
					}
					else
						m_fnSendToCim(SEND_ALARM_WAITLIST_REQUEST_ACK, DATA1_ERR,  "");

				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_ECID_LIST_REQUEST))    // ECID List를 보내준다.
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_ECID_LIST_REQUEST);
					//ECID=ECSLL=ECWLL=ECDEF=ECWUL=ECSUL, … (n)
					m_fnSendECID_ListReqAck();
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_ECID_REQUEST))        // 요청받은 ECID Value만 보내준다.
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_ECID_REQUEST);					
					m_fnSendECID_ReqAck(m_CimDataFormat.chData2);
					//ECID=ECSLL=ECWLL=ECDEF=ECWUL=ECSUL
					//m_fnSendToCim(SEND_ECID_REQUEST_ACK, DATA1_OK,  );
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_ECID_CHANGE_REPORT_ACK))        // ECID 변경 보고
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_ECID_CHANGE_REPORT_ACK);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_ECID_CHANGE_CMD))        // ECID 변경 지시 From Cim
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_ECID_CHANGE_CMD);
					// 해당 ECID 변경
					int i = 0;									
					//CStringArray strECIDChList;
					bool retOkECID = true;
					while(AfxExtractSubString(subString, m_CimDataFormat.chData2, i++,_T(',')))
					{
						//strECIDChList.Add(subString);
						if(m_fnSendECID_ChangeCmdAck(subString) == false)
						{
							m_fnSendToCim(SEND_ECID_CHANGE_CMD_ACK, DATA1_ERR);
							retOkECID = false;
							break;
						}
						subString.Format("");
					}
					if(retOkECID == true)
					{
						//ECIDList_ValueUpDate();
						m_fnSendToCim(SEND_ECID_CHANGE_CMD_ACK, DATA1_OK);
						::SendMessageA(G_CIM_I_value, WM_USER_ECID_CHANGE,0,0);
					}
					//m_fnSendToCim(SEND_ECID_CHANGE_CMD_ACK, DATA1_ERR);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_PPID_LIST_REQUEST))      
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_PPID_LIST_REQUEST);
					//"PPIDNAME1, PPIDNAME2, .... "  PPID Body ID가 , 로 분리되어서 들어감.					
					m_fnSendToCim(SEND_PPID_LIST_REQUEST_ACK, DATA1_OK, m_fnGetPPIDLIst());
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_PPID_REQUEST))      
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_PPID_REQUEST);
					//PPID_NAME,Param_Name=ParamValue1, …….,Param_Name=ParamValue(n)
					CString strID = m_CimDataFormat.chData2; // 요청 Recipe ID

					// Log strID 추가
					m_fnSendToCim(SEND_PPID_REQUEST_ACK, DATA1_OK, m_fnGetPPIDValue(strID));
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_PPID_CURRENT_REQUEST))      
				{
					
					//PPID_NAME					
					char strReadData[256];
					memset(strReadData, 0x00, 256);
					GetPrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
					
					SendMsgCIMView(Log_Check, "Recv : %s(%s)", RECV_PPID_CURRENT_REQUEST,strReadData);

					//m_fnSendToCim(SEND_PPID_CURRENT_REQUEST_ACK, DATA1_OK, CString(strReadData));

					CString strSendCim;
					strSendCim.Format("%s,%d",strReadData, BY_EQUIP);
					m_fnSendToCim(SEND_PPID_CURRENT_REQUEST_ACK, DATA1_OK, strSendCim);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_PPID_REPORT_ACK))  
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_PPID_REPORT_ACK);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_PPID_CMD))      
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_PPID_CMD);

					int i = 0;									
					int nMode = 0;
					CString strRecipeName;
					CStringArray strArrayRecipe;
					while(AfxExtractSubString(subString, m_CimDataFormat.chData2, i++,_T(',')))
					{
						if(i == 1) // PPID Name
						{
							strRecipeName = subString;
						}
						else if(i == 2) // Mode 1 생성, 2 수정, 3 삭제
						{
							nMode = atoi(subString);
							if(nMode == DELETE_PPID)
								break;
						}
						else // Item Value
						{
							strArrayRecipe.Add(subString);
						}
					}

					BOOL bRes = m_fnCreateRecipeFile(strRecipeName, nMode, &strArrayRecipe);

					if(bRes)
					{
						if(nMode == DELETE_PPID)
						{
							//m_fnSendToCim(SEND_PPID_CMD_ACK, DATA1_OK);
							//m_fnSendPPIDCmd(strRecipeName, DELETE_PPID);
						}
						else
						{
							m_fnSendToCim(SEND_PPID_CMD_ACK, DATA1_OK);
							m_fnSendPPIDCmd(strRecipeName, NEW_PPID);
						}
						
					}
					else
						m_fnSendToCim(SEND_PPID_CMD_ACK, DATA1_ERR);

				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_EOID_REQUEST))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_EOID_REQUEST);
					//EOID,…(n) (BCR READING MODE=7, VCR READING MODE=8, VCR READING COUNT=16)					
					//m_fnSendToCim(SEND_ECID_REQUEST_ACK, DATA1_OK, );
					m_fnSendToCim(SEND_ECID_REQUEST_ACK, DATA1_ERR);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_ECID_CHANGE_REPORT_ACK))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_ECID_CHANGE_REPORT_ACK);
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{

					}
					else  // From Cim Error
					{

					}
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_EOID_CMD))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_EOID_CMD);
					//EOID=EOV,…(n)
					m_fnSendToCim(SEND_EOID_CMD_ACK, DATA1_OK);
					//m_fnSendToCim(SEND_EOID_CMD_ACK, DATA1_ERR);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_FILESERVER_UPLOAD_REPORT))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_FILESERVER_UPLOAD_REPORT);
					// File Upload가 완료되면 Report 가 온다. OK 또는 ERR
					if(!strcmp(m_CimDataFormat.chData1, DATA1_OK))
					{
						SendMsgCIMView(Log_Info, "Upload File OK");
						m_CIMInfodlg->GetDlgItem(IDC_ST_FILESERVER_UPLOAD_REPORT_ACK)->SetWindowText("UPLOAD_REPORT_ACK : OK");
					}
					else
					{
						SendMsgCIMView(Log_Error, "Upload File NG");
						m_CIMInfodlg->GetDlgItem(IDC_ST_FILESERVER_UPLOAD_REPORT_ACK)->SetWindowText("UPLOAD_REPORT_ACK : NG");
					}

					
					m_fnSendToCim(SEND_FILESERVER_UPLOAD_REPORT_ACK, DATA1_OK);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_FILESERVER_DISCONNET_REPORT))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_FILESERVER_DISCONNET_REPORT);
					m_fnSendToCim(SEND_FILESERVER_DISCONNECT_REPORT_ACK, DATA1_OK);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_OP_CALL))
				{
					G_MotObj.m_fnSetSignalTower(STWOER_MODE8_OPCall);

					SendMsgCIMView(Log_Check, "Recv : %s", RECV_OP_CALL);
					//SendMsgCIMView(Log_Info, "==> %s", m_CimDataFormat.chData2);

					m_fnSendToCim(SEND_OP_CALL_ACK, DATA1_OK);


					//Message는 100단위 10개가 들어온다.
					char strMsg[100+1];
					for(int MStep=0; MStep<10; MStep++)
					{
						memset(strMsg, 0x00, 101);
						memcpy(strMsg, &m_CimDataFormat.chData2[MStep*100], 100);
						SendMsgCIMView(Log_Info, "%d) : %s", MStep+1, strMsg);
					}

					::SendMessage(m_CIMInfodlg->GetParent()->m_hWnd, WM_USER_RECV_HOST_MSG, 1,(LPARAM)m_CimDataFormat.chData2);
					//CCIMMsgViewDlg MsgDlg;
					//MsgDlg.DoModal();
					//CString strMsg;
					//strMsg.Format("OPCALL : %s",m_CimDataFormat.chData2 );
					//AfxMessageBox(strMsg);
					
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_TERMINAL_MESSAGE))
				{
					SendMsgCIMView(Log_Check, "Recv : %s", RECV_TERMINAL_MESSAGE);
					//SendMsgCIMView(Log_Info, "==> %s", m_CimDataFormat.chData2);
					m_fnSendToCim(SEND_TERMINAL_MESSAGE_ACK, DATA1_OK);
					//Message는 100단위 10개가 들어온다.
					char strMsg[100+1];
					for(int MStep=0; MStep<10; MStep++)
					{
						memset(strMsg, 0x00, 101);
						memcpy(strMsg, &m_CimDataFormat.chData2[MStep*100], 100);
						SendMsgCIMView(Log_Info, "%d) : %s", MStep+1, strMsg);
					}
					::SendMessage(m_CIMInfodlg->GetParent()->m_hWnd, WM_USER_RECV_HOST_MSG, 2,(LPARAM)m_CimDataFormat.chData2);
					//CString strMsg;
					//strMsg.Format("OPCALL : %s",m_CimDataFormat.chData2 );
					//AfxMessageBox(strMsg);
				}
				else if(!strcmp(m_CimDataFormat.chMessageName, RECV_MCC_CMD)) 
				{
					if(!strcmp(m_CimDataFormat.chData1, DATA1_REQ))
					{
						SendMsgCIMView(Log_Check, "Recv : %s ====>> Check....CMD", RECV_MCC_CMD);	
						//Data 2 SAMPING_TIME(TIME),ON/OFF
						CString strTime, strOnOff;
						int i = 0;						
						while(AfxExtractSubString(subString, m_CimDataFormat.chData2, i++,_T(',')))
						{
							if(i == 1) // Time (min)
							{
								strTime = subString;
							}
							else if(i == 2)
							{
								strOnOff = subString;
							}
						}

						if(strTime.GetLength() !=0 && strOnOff.GetLength() !=0)
						{
// 							m_pServerInterface->m_fnSendCommand_Nrs(TASK_61_MCC_Log, nBiz_MCC_Func, nBiz_MCC_SeqSendCommand,  nBiz_MCC_Unit_Time, strTime.GetLength(), (UCHAR*)strTime.GetBuffer());
// 							m_pServerInterface->m_fnSendCommand_Nrs(TASK_61_MCC_Log, nBiz_MCC_Func, nBiz_MCC_SeqSendCommand,  nBiz_MCC_Unit_OnOff, strOnOff.GetLength(), (UCHAR*)strOnOff.GetBuffer());
							m_fnSendToCim(SEND_MCC_CMD_ACK, DATA1_OK);							
						}
						else
						{
							SendMsgCIMView(Log_Check, "Recv : %s", "Error Time, OnOff Msg");							
						}
					}
				}

				m_strReciveString = "";
				m_CimDataFormat.CimDataFormatInit();
			}
 		}
 	}
	delete [] buf;

	return 0;
}

BOOL CCimInterfaceSocket::m_fnTimeSyncSet(CString strTime)
{

	SendMsgCIMView(Log_Info, "m_fnTimeSyncSet : %s", strTime);
	//year, month, day, hour, min, sec
	int nParsingArray[] = {4, 2, 2, 2, 2};	
	CString strData;

	if(strTime.GetLength() != 14) // 정해진 규약 yyyyMMddHHmmss
	{
		// Log strTime
		return FALSE;
	}

	SYSTEMTIME st;
	GetLocalTime(&st);

	for(int i=0; i<5; i++)
	{
		strData = strTime.Left(nParsingArray[i]);
		strTime = strTime.Mid(nParsingArray[i]);

		if(i == 0)
		{
			st.wYear = atoi(strData);
		}
		else if(i == 1)
		{
			st.wMonth = atoi(strData);
		}
		else if(i == 2)
		{
			st.wDay = atoi(strData);
		}
		else if(i == 3)
		{
			st.wHour = atoi(strData);
		}
		else if(i == 4)
		{
			st.wMinute = atoi(strData);
			st.wSecond = atoi(strTime);
		}
	}
	return SetLocalTime(&st);	
}

CString CCimInterfaceSocket::m_fnGetMaterialInfo(int nIndex)
{
	return m_strArrayMaterialInfo.GetAt(nIndex);		
}

CString CCimInterfaceSocket::m_fnGetPPIDLIst()
{

	SendMsgCIMView(Log_Info, "m_fnGetPPIDLIst");
	CFileFind finder;
	CString strFind;

	CString strFindPath;

	strFindPath.Format("%s*.*", RECIPE_FOLDER_PATH);

	BOOL bWorking = finder.FindFile(strFindPath);
	CString strSendPPIDList, strSendData;
	int nCount = 0;

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		strFind = (LPCTSTR)finder.GetFilePath();
		if(strFind.Find(".ini") > 0)
		{
			strFind = finder.GetFileTitle();
			strSendPPIDList += strFind + ",";
			nCount += 1;

			SendMsgCIMView(Log_Info, "PPIDLIst :%d) %s", nCount, strFind);
		}
	}	

	if(strSendPPIDList.GetLength() > 0)
	{
		strSendPPIDList = strSendPPIDList.Left(strSendPPIDList.GetLength()-1);   // 끝에 쉼표 자름
	}
	
	//strSendData.Format("%d, %s", nCount, strSendPPIDList);
	strSendData.Format("%s", strSendPPIDList);	

	//SendMsgCIMView(Log_Normal, strSendData.GetBuffer());	
	strSendData.ReleaseBuffer();

	//return strSendData;
	return strSendPPIDList;
}

CString CCimInterfaceSocket::m_fnGetPPIDValue(CString strPPID, int nMode)
{
	SendMsgCIMView(Log_Info, "m_fnGetPPIDValue");
	CStdioFile cstFileDst;
	CString strRead;

	CStringArray arrayDst, arraryCim;
	CString strRecipeID;
	CString strRevNo;
	CString strValueData, strSend;

	CBIniFile iniTemp;

	CString strRecipeFullPath;
	strRecipeFullPath.Format("%s%s.ini", RECIPE_FOLDER_PATH, strPPID);

	if (!cstFileDst.Open(strRecipeFullPath, CFile::modeRead|CFile::typeText, NULL))
	{
		SendMsgCIMView(Log_Worrying, "Recipe Dst File Open Error %s", strRecipeFullPath);
		return "";
	}

	while(cstFileDst.ReadString(strRead))
	{
		if(strRead.Find('=')>0)//공백 Line을 줄수 있게 하려고
			arrayDst.Add(strRead);
	}

	int nDstLength = 0;	
	nDstLength = (int)arrayDst.GetCount();

	for(int i =0; i< nDstLength; i++)
	{
		CString strReadDst;		
		CString strParmName, strValue;

		strReadDst = arrayDst.GetAt(i);
		if(strReadDst.Find("[") >= 0)
			continue;
		else if(strReadDst.Find("*") >= 0)
			continue;			

		arraryCim.Add(strReadDst);
	}

	CString strSendCimMeesage;
	//strSendCimMeesage.Format("%d,%s,%s,", arraryCim.GetCount()+2,"PPID", strRecipeID);
	if(nMode !=0) // 수정 생성 삭제 보고를 한다.
	{
		strSendCimMeesage.Format("%s,%d,", strPPID, nMode);
	}
	else
	{
		strSendCimMeesage.Format("%s,", strPPID);
	}	

	for(int i=0; i<arraryCim.GetCount(); i++)
	//for(int i=0; i<arraryCim.GetCount()-4; i++)//PPID 갯수가 변경 모두 수정해야 한다.
	{
		strSendCimMeesage += arraryCim.GetAt(i);
		strSendCimMeesage += ",";
		SendMsgCIMView(Log_Info, "PPIDBody :%d) %s", i+1,  arraryCim.GetAt(i));
	}

	strSendCimMeesage = strSendCimMeesage.Left(strSendCimMeesage.GetLength()-1); // 끝에 쉼표 제거

	//SendMsgCIMView(Log_Normal, "%s", strSendCimMeesage);

	arrayDst.RemoveAll();
	cstFileDst.Close();	

	return strSendCimMeesage;
}

int CCimInterfaceSocket::m_fnGetSystemErrorCheck(BOOL bReturnTypeCheck)
{	

	SendMsgCIMView(Log_Info, "m_fnGetSystemErrorCheck");
	if(bReturnTypeCheck == FALSE) 			// 일반적인 System Error
	{
		return ERROR_NORMAL;
	}
	else		// Error 원인 Check
	{
		if(1) 
			return ERROR_NOSTICK;
		else if(1)
			return ERROR_STAGE_ERROR;
		else if(1)
			return ERROR_MATERIAL_ID_MISMATCH;			
	}

	return ERROR_NORMAL;	
}

void CCimInterfaceSocket::m_fnGetSystemErrorStatus(int *pIndex)
{
	SendMsgCIMView(Log_Info, "m_fnGetSystemErrorStatus");
//	*pIndex = G_SystemModeData.unSystemErrorNum;

}

void CCimInterfaceSocket::m_fnDataReset()
{
	SendMsgCIMView(Log_Info, "m_fnDataReset");
	m_strReciveString = "";
	m_CimDataFormat.CimDataFormatInit();
}

LRESULT CCimInterfaceSocket::OnClientReConnect(WPARAM wParam, LPARAM lParam)
{
// 	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
// 	pMainWnd->m_ServerDlg.m_staConnectCim.SetBackTextColor(UI_OFF_BACK, UI_OFF_FONT);
// 	G_AddLog(3, "Cim Server 접속 종료");
 	m_bConnect = FALSE;

	BOOL  bRes = FALSE;
	
	m_ClientSocket.Close();
	bRes = m_ClientSocket.Create();

	if(bRes)
	{
		SendMsgCIMView(Log_Normal, "Cim Clinet 생성");
	}
	else
	{
		SendMsgCIMView(Log_Worrying, "Cim Clinet 생성 실패");
		return 0;
	}

	KillTimer(TIMER_CIM_RECONNECT);	
	SetTimer(TIMER_CIM_RECONNECT, 1000, NULL);

	return 0;
}

LRESULT CCimInterfaceSocket::OnConnectOK(WPARAM wParam, LPARAM lParam)
{
// 	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
// 	if(G_SystemModeData.unSystemMode == INSPECT_EXIT)
// 		return 0;

	int nErrorCode = (int)wParam;
	
	if(nErrorCode == 0)	
	{
		SendMsgCIMView(Log_Normal, "CIM Connect Ok : %d", nErrorCode);
		m_bConnect = TRUE;
	}
	else
	{
		SendMsgCIMView(Log_Worrying, "CIM Connect NG : Error %d", nErrorCode);
		m_bConnect = FALSE;
	}
	return 0;
}

void CCimInterfaceSocket::m_fnSendAlarmReport(BOOL bSet, int nAlarmType, int nAlarmID, CString strAlarmText, CTime tOccurTime)
{	

	SendMsgCIMView(Log_Info, "m_fnSendAlarmReport");
	CString strTime;
	strTime = tOccurTime.Format("%Y%m%d%H%M%S");

	CString strMsg;
	strMsg.Format("%d,%d,%s,%s", nAlarmType, nAlarmID, strAlarmText, strTime);
	
	if(bSet == TRUE)
	{
		m_fnSendToCim(SEND_ALARM_REPORT, DATA1_SET, strMsg);
		SendMsgCIMView(Log_Info, "==>Set : %s ", strMsg);
	}
	else 
	{
		m_fnSendToCim(SEND_ALARM_REPORT, DATA1_RESET, strMsg);
		SendMsgCIMView(Log_Info, "==>Reset : %s ", strMsg);
	}
}

void CCimInterfaceSocket::m_fnSendAlarmWaitListAck(CString strAlarmIDList /*,로구분 */)
{	
	SendMsgCIMView(Log_Info, "m_fnSendAlarmWaitListAck : %s", strAlarmIDList);

	m_fnSendToCim(SEND_ALARM_WAITLIST_REQUEST_ACK, DATA1_OK, strAlarmIDList);
}

void CCimInterfaceSocket::m_fnSendLoadComplete(CString strStickID)
{	
	SendMsgCIMView(Log_Info, "m_fnSendLoadComplete : %s", strStickID);
	m_fnSendToCim(SEND_LOAD_COMPLETE, DATA1_REPORT, strStickID);

	//해당 정보 받을 준비.
	m_bRecvMaterialData = false;
	m_bRecvJobStartCMD = false;
}

void CCimInterfaceSocket::m_fnSendInspectionStart(CString strStickID)
{	
	SendMsgCIMView(Log_Info, "m_fnSendInspectionStart : %s", strStickID);
	m_fnSendToCim(SEND_INSPECTION_START, DATA1_REPORT, strStickID);
}

void CCimInterfaceSocket::m_fnSendInspectionComp(CString strStickID, CString strJudge)
{	
	SendMsgCIMView(Log_Info, "m_fnSendInspectionComp : %s", strStickID);
	SendMsgCIMView(Log_Info, "strJudge : %s", strJudge);

	CString strSend;
	strSend.Format("%s,%s", strStickID, strJudge);

	m_fnSendToCim(SEND_INSPECTION_COMPLETE, DATA1_REPORT, strSend);
}

void CCimInterfaceSocket::m_fnSendUnloadComp(CString strStickID)
{	
	SendMsgCIMView(Log_Info, "m_fnSendUnloadComp : %s", strStickID);
	m_fnSendToCim(SEND_UNLOAD_COMPLETE, DATA1_REPORT, strStickID);
}

void CCimInterfaceSocket::m_fnSendEqstChangeReport(int nEqpStatus, int nByWho, int nAlarmID)
{	

	SendMsgCIMView(Log_Info, "m_fnSendEqstChangeReport : %d, %d, %d", nEqpStatus, nByWho, nAlarmID);
	CString strSend;
	strSend.Format("%d,%d", nEqpStatus, nByWho);

	m_fnSendToCim(SEND_EQST_CHANGE_REPORT, DATA1_REPORT, strSend);
}

void CCimInterfaceSocket::m_fnSendPrstChangeReport(int nPrstStatus, int nByWho)
{	
	SendMsgCIMView(Log_Info, "m_fnSendPrstChangeReport : %d, %d", nPrstStatus, nByWho);

	CString strSend;
	strSend.Format("%d,%d", nPrstStatus, nByWho);

	m_fnSendToCim(SEND_PRST_CHANGE_REPORT, DATA1_REPORT, strSend);
}

void CCimInterfaceSocket::m_fnSendMCMDRequest()
{
	SendMsgCIMView(Log_Info, "m_fnSendMCMDRequest ");
	m_fnSendToCim(SEND_MCMD_REQUEST, DATA1_REQ);
}

void CCimInterfaceSocket::m_fnSendStickExistReport()
{
	SendMsgCIMView(Log_Info, "m_fnSendStickExistReport ");
	// EXIST/EMPTY ??? 물어봐야함.
	//m_fnSendToCim(SEND_STICK_EXIST_REPORT, DATA1_REPORT, );
}


void CCimInterfaceSocket::m_fnSendECID_ListReqAck()
{
	SendMsgCIMView(Log_Info, "m_fnSendECID_ListReqAck ");

	CString strSend;

	strSend.Format("");
	for(int AStep= 0; AStep<ECID_List_Cnt-1; AStep++)
	{
		strSend.AppendFormat("%d=%d=%d=%d=%d=%d,", 
			G_strECIDList[AStep].nECID,
			G_strECIDList[AStep].nECSLL,
			G_strECIDList[AStep].nECWLL,
			G_strECIDList[AStep].nECDEF,
			G_strECIDList[AStep].nECWUL,
			G_strECIDList[AStep].nECSUL);
	}
	strSend.AppendFormat("%d=%d=%d=%d=%d=%d", 
		G_strECIDList[ECID_List_Cnt-1].nECID,
		G_strECIDList[ECID_List_Cnt-1].nECSLL,
		G_strECIDList[ECID_List_Cnt-1].nECWLL,
		G_strECIDList[ECID_List_Cnt-1].nECDEF,
		G_strECIDList[ECID_List_Cnt-1].nECWUL,
		G_strECIDList[ECID_List_Cnt-1].nECSUL);

	//strSend.
	// List 전송
	m_fnSendToCim(SEND_ECID_LIST_REQUEST_ACK, DATA1_OK, strSend);
}

void CCimInterfaceSocket::m_fnSendECID_ReqAck(CString strECID)
{
	SendMsgCIMView(Log_Info, "m_fnSendECID_ReqAck :%s ",strECID);

	int nReqECID =  atoi(strECID);
	int nSelectIndex = -1;
	for(int AStep= 0; AStep<ECID_List_Cnt-1; AStep++)
	{
		if(G_strECIDList[AStep].nECID == nReqECID)
		{
			nSelectIndex = AStep;
			break;
		}
	}

	CString strSend;
	// 해당 ECID 보고 ECID=ECSLL=ECWLL=ECDEF=ECWUL=ECSUL
	if(nSelectIndex>0)
	{
		strSend.Format("");
		strSend.AppendFormat("%d=%d=%d=%d=%d=%d", 
			G_strECIDList[nSelectIndex].nECID,
			G_strECIDList[nSelectIndex].nECSLL,
			G_strECIDList[nSelectIndex].nECWLL,
			G_strECIDList[nSelectIndex].nECDEF,
			G_strECIDList[nSelectIndex].nECWUL,
			G_strECIDList[nSelectIndex].nECSUL);
		m_fnSendToCim(SEND_ECID_REQUEST_ACK, DATA1_OK, strSend);
	}
	else
		m_fnSendToCim(SEND_ECID_REQUEST_ACK, DATA1_ERR, "");
}

void CCimInterfaceSocket::m_fnSendECID_ChangeReport()
{
	SendMsgCIMView(Log_Info, "m_fnSendECID_ChangeReport");
	// ECID전체를 보고

	CString strSend;

	strSend.Format("");
	for(int AStep= 0; AStep<ECID_List_Cnt-1; AStep++)
	{
		strSend.AppendFormat("%d=%d=%d=%d=%d=%d,", 
			G_strECIDList[AStep].nECID,
			G_strECIDList[AStep].nECSLL,
			G_strECIDList[AStep].nECWLL,
			G_strECIDList[AStep].nECDEF,
			G_strECIDList[AStep].nECWUL,
			G_strECIDList[AStep].nECSUL);
	}
	strSend.AppendFormat("%d=%d=%d=%d=%d=%d", 
		G_strECIDList[ECID_List_Cnt-1].nECID,
		G_strECIDList[ECID_List_Cnt-1].nECSLL,
		G_strECIDList[ECID_List_Cnt-1].nECWLL,
		G_strECIDList[ECID_List_Cnt-1].nECDEF,
		G_strECIDList[ECID_List_Cnt-1].nECWUL,
		G_strECIDList[ECID_List_Cnt-1].nECSUL);

	m_fnSendToCim(SEND_ECID_CHANGE_REPORT, DATA1_REPORT, strSend);
}

bool CCimInterfaceSocket::m_fnSendECID_ChangeCmdAck(CString strNewvalue)
{
	SendMsgCIMView(Log_Info, "m_fnSendECID_ChangeCmdAck(%s)", strNewvalue);
	// 해당 ECID를 바꾼뒤 보고한다.
	// 1=50=60=70=80=200,4=50=100=150=200=400
	strNewvalue.Remove(' ');
	ECIDInfo nEcid;
	CStringArray strECID;
	int i = 0;
	CString subString;
	while(AfxExtractSubString(subString, strNewvalue.GetBuffer(strNewvalue.GetLength()), i++,_T('=')))
	{
		strECID.Add(subString);
	}
	int SelectedECID = -1;
	nEcid.nECID = atoi(strECID.GetAt(0));
	nEcid.nECSLL = atoi(strECID.GetAt(1));
	nEcid.nECWLL = atoi(strECID.GetAt(2));
	nEcid.nECDEF = atoi(strECID.GetAt(3));
	nEcid.nECWUL = atoi(strECID.GetAt(4));
	nEcid.nECSUL = atoi(strECID.GetAt(5));

	for(int fStep = 0; fStep< ECID_List_Cnt;fStep++)
	{
		if(G_strECIDList[fStep].nECID == nEcid.nECID)
		{
			SelectedECID = fStep;
			break;
		}
	}

	if(SelectedECID>=0)
	{
		G_strECIDList[SelectedECID].nECID = nEcid.nECID ;
		G_strECIDList[SelectedECID].nECSLL = nEcid.nECSLL;
		G_strECIDList[SelectedECID].nECWLL = nEcid.nECWLL;
		G_strECIDList[SelectedECID].nECDEF = nEcid.nECDEF;
		G_strECIDList[SelectedECID].nECWUL = nEcid.nECWUL;
		G_strECIDList[SelectedECID].nECSUL = nEcid.nECSUL;

		return true;
	}
	return false;
	//m_fnSendToCim(SEND_ECID_CHANGE_CMD_ACK, DATA1_OK);
	//m_fnSendToCim(SEND_ECID_CHANGE_CMD_ACK, DATA1_ERR);
}

//void CCimInterfaceSocket::m_fnSendSVID_ReqAck(CString strValue)
//{
//	SendMsgCIMView(Log_Info, "m_fnSendSVID_ReqAck");
//
//	CString strSVID;
//	strSVID = "3, 223, 444, B";
//	m_fnSendToCim(SEND_SVID_REQUEST_ACK, DATA1_OK, strSVID);
//}

void CCimInterfaceSocket::m_fnSendEOID_ReqAck()
{
	SendMsgCIMView(Log_Info, "m_fnSendEOID_ReqAck");
	//m_fnSendToCim(SEND_EOID_REQUEST_ACK, DATA1_OK, )
}

void CCimInterfaceSocket::m_fnSendEOID_ChangeReport(CString strValue)
{
	SendMsgCIMView(Log_Info, "m_fnSendEOID_ChangeReport : %d", strValue);
	// EOID=EOV
	m_fnSendToCim(SEND_EOID_REQUEST_ACK, DATA1_OK, strValue);
}

void CCimInterfaceSocket::m_fnSendEOID_ChangeCmdAck()
{
	SendMsgCIMView(Log_Info, "m_fnSendEOID_ChangeCmdAck");
	m_fnSendToCim(SEND_EOID_CMD_ACK, DATA1_OK);
	m_fnSendToCim(SEND_EOID_CMD_ACK, DATA1_ERR);
}

void CCimInterfaceSocket::m_fnSendPPIDCmd(CString strID, int nMode)
{
	m_fnSendToCim(SEND_PPID_REPORT, DATA1_REPORT, m_fnGetPPIDValue(strID, nMode));
}

void CCimInterfaceSocket::SendMsgCIMView(ListColor nType, const char* pWritLog, ...)
{
	if(m_CIMInfodlg != nullptr)
	{
		
		char bufLog[LOG_TEXT_MAX_SIZE];
		memset(bufLog, 0x00, LOG_TEXT_MAX_SIZE);
		//////////////////////////////////////////////////////////////////////////
		va_list vargs;
		va_start(vargs, pWritLog);
		int LenStr = vsprintf_s((char*)bufLog, LOG_TEXT_MAX_SIZE-1,pWritLog, (va_list)vargs);
		va_end(vargs);
		m_CIMInfodlg->WriteCIMMsg(nType, bufLog);
	}
}

void CCimInterfaceSocket::m_fnSendStickExistReport(BOOL bONOFF)
{
	CString strSEnd;

	if(bONOFF == TRUE)
	{
		strSEnd = "EXIST";
	}
	else
	{
		strSEnd = "EMPTY";
	}
	m_fnSendToCim(SEND_STICK_EXIST_REPORT, DATA1_REPORT, strSEnd);
}

BOOL CCimInterfaceSocket::m_fnRecipeIDCheck(CString strRecipeID)
{
	CString strIniFile;
	BOOL bInIni = FALSE;

	CFileFind finder; 
	CString tpath;
	tpath.Format("%s\\*.ini", VS10Master_RECIPE_INI_PATH);

	BOOL bWorking = finder.FindFile(tpath);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		strIniFile = (LPCTSTR) finder.GetFileTitle();

		if(strIniFile ==  strRecipeID) // Rcp 가 존재하면
		{
			bInIni = TRUE;
			break;
		}
		bInIni = FALSE;
	}

	if(bInIni == FALSE) // 마스크 레시피가 존재 하지 않을 때!!
	{
		//		CInformDlg dlg;
		//		dlg.m_fnSetInform(_SW_YES, " Recipe 가 존재하지 않습니다.\n 확인 해 주십시오");
		//		dlg.DoModal();
		return FALSE;
	}
	return TRUE;
}

BOOL CCimInterfaceSocket::m_fnCreateRecipeFile(CString strRecipeName, int nMode, CStringArray *pArray)
{
	CBIniFile iniFile;

	CString tpath;
	tpath.Format("%s\\%s.ini", VS10Master_RECIPE_INI_PATH, strRecipeName);	
	

	if(nMode == DELETE_PPID)
	{
		m_fnSendToCim(SEND_PPID_CMD_ACK, DATA1_OK);
		m_fnSendPPIDCmd(strRecipeName, DELETE_PPID);
		return DeleteFile(tpath);
		//if(m_fnRecipeIDCheck(tpath) == TRUE) // Recipe File 있으면?
		//{
		//	return DeleteFile(tpath);
		//}
		//else 
		//{
		//	return FALSE;
		//}
	}
	else // New PPID 혹은 MODIFY_PPID
	{
		iniFile.SetFileName(tpath);

		for(INT_PTR i=0; i<pArray->GetSize(); i++)
		{
			CString strLine = pArray->GetAt(i);

			CString strItem;
			CString strValue;

			strItem = strLine.Left(strLine.Find("="));
			strValue = strLine.Mid(strLine.Find("=")+1);

			iniFile.WriteProfileString(Section_CELL_INFO, strItem, strValue);
		}
	}

	return TRUE;
}

void CCimInterfaceSocket::m_fnSendMCCFileUploadReport(char * chFolderName)
{
	CString strFolderName;
	strFolderName = chFolderName;

	m_fnSendToCim(SEND_MCC_UPLOAD_REPORT, DATA1_REPORT, strFolderName);
}