#pragma once

#include "CIM_Interface/ClientSocket.h"
#include "IniClass/BIniFile.h"
//#include "InformDlg.h"

#define CIM_INFO_PATH				VS10Master_PARAM_INI_PATH
#define RECIPE_FOLDER_PATH			"D:\\nBiz_InspectStock\\Data\\StickRecipe\\"

#define TIMER_CIM_RECONNECT    1000


//Sequence Check
#define	SEND_LOAD_COMPLETE					    "LOAD_COMPLETE"								
#define	RECV_LOAD_COMPLETE_ACK					"LOAD_COMPLETE_ACK"
												
#define	RECV_MATERIAL_INFO						"MATERIAL_INFO"
#define	SEND_MATERIAL_INFO_ACK					"MATERIAL_INFO_ACK"
												
#define	RECV_JOBSTART_CMD						"JOBSTART_CMD"
#define	SEND_JOBSTART_CMD_ACK					"JOBSTART_CMD_ACK"
												
#define	SEND_INSPECTION_START					"INSPECTION_START"
#define	RECV_INSPECTION_START_ACK				"INSPECTION_START_ACK"
												
#define	SEND_INSPECTION_COMPLETE				"INSPECTION_COMPLETE"
#define	RECV_INSPECTION_COMPLETE_ACK			"INSPECTION_COMPLETE_ACK"
												
#define	SEND_UNLOAD_COMPLETE					"UNLOAD_COMPLETE"
#define	RECV_UNLOAD_COMPLETE_ACK				"UNLOAD_COMPLETE_ACK"
												
#define	RECV_EQST_REQUEST						"EQST_REQUEST"
#define	SEND_EQST_REQUEST_ACK					"EQST_REQUEST_ACK"
#define	SEND_EQST_CHANGE_REPORT					"EQST_CHANGE_REPORT"
#define	RECV_EQST_CHANGE_REPORT_ACK				"EQST_CHANGE_REPORT_ACK"
#define	RECV_EQST_CHANGE_CMD					"EQST_CHANGE_CMD"
#define	SEND_EQST_CAHNGE_CMD_ACK				"EQST_CAHNGE_CMD_ACK"
#define	RECV_PRST_REQUEST						"PRST_REQUEST"
#define	SEND_PRST_REQUEST_ACK					"PRST_REQUEST_ACK"

#define	RECV_PRST_CHANGE_REPORT_ACK				"PRST_CHANGE_REPORT_ACK"
#define	SEND_PRST_CHANGE_REPORT					"PRST_CHANGE_REPORT"


// 사양서 확인 수정 Check
#define	RECV_PRST_CHANGE_CMD					"PRST_CHANGE_CMD"
#define	SEND_PRST_CHANGE_CMD_ACK				"PRST_CHANGE_CMD_ACK"
// 사양서 확인 수정 Check
												
#define	RECV_JOB_PROCESS_CMD					"JOB_PROCESS_CMD"
#define	SEND_JOB_PROCESS_CMD_ACK				"JOB_PROCESS_CMD_ACK"
#define	RECV_CIM_ALIVE							"CIM_ALIVE"
#define	SEND_CIM_ALIVE_ACK						"CIM_ALIVE_ACK"
#define	RECV_TIME_SET							"TIME_SET"
#define	SEND_TIME_SET_ACK						"TIME_SET_ACK"
												
#define	RECV_BUZZER_CMD							"BUZZER_CMD"
												
#define	SEND_BUZZER_CMD_ACK						"BUZZER_CMD_ACK"
#define	SEND_MCMD_REQUEST						"MCMD_REQUEST"
#define	RECV_MCMD_REQUEST_ACK					"MCMD_REQUEST_ACK"
#define	RECV_MCMD_CHANGE_REPORT					"MCMD_CHANGE_REPORT"
#define	SEND_MCMD_CHANGE_REPORT_ACK				"MCMD_CHANGE_REPORT_ACK"
#define	SEND_STICK_EXIST_REPORT					"STICK_EXIST_REPORT"
#define	RECV_STICK_EXIST_REPORT_ACK				"STICK_EXIST_REPORT_ACK"

#define	SEND_ALARM_REPORT								"ALARM_REPORT"												
#define	RECV_ALARM_REPORT_ACK							"ALARM_REPORT_ACK"
#define	RECV_ALARM_WAITLIST_REQUEST				"ALARM_WAITLIST_REQUEST"
#define	SEND_ALARM_WAITLIST_REQUEST_ACK		"ALARM_WAITLIST_REQUEST_ACK"

#define	RECV_ECID_LIST_REQUEST						"ECID_LIST_REQUEST"
#define	SEND_ECID_LIST_REQUEST_ACK					"ECID_LIST_REQUEST_ACK"
#define	RECV_ECID_REQUEST								"ECID_REQUEST"
#define	SEND_ECID_REQUEST_ACK							"ECID_REQUEST_ACK"
												
#define	SEND_ECID_CHANGE_REPORT						"ECID_CHANGE_REPORT"
#define	RECV_ECID_CHANGE_REPORT_ACK				"ECID_CHANGE_REPORT_ACK"
#define	RECV_ECID_CHANGE_CMD							"ECID_CHANGE_CMD"
#define	SEND_ECID_CHANGE_CMD_ACK					"ECID_CHANGE_CMD_ACK"
												
#define	RECV_SVID_REQUEST								"SVID_REQUEST"
#define	SEND_SVID_REQUEST_ACK							"SVID_REQUEST_ACK"

#define	RECV_PPID_LIST_REQUEST							"PPID_LIST_REQUEST"
#define	SEND_PPID_LIST_REQUEST_ACK					"PPID_LIST_REQUEST_ACK"
#define	RECV_PPID_REQUEST								"PPID_REQUEST"
#define	SEND_PPID_REQUEST_ACK							"PPID_REQUEST_ACK"
												
#define	RECV_PPID_CURRENT_REQUEST				"PPID_CURRENT_REQUEST"
#define	SEND_PPID_CURRENT_REQUEST_ACK			"PPID_CURRENT_REQUEST_ACK"

#define	SEND_PPID_REPORT						"PPID_REPORT"
#define	RECV_PPID_REPORT_ACK					"PPID_REPORT_ACK"
#define	RECV_PPID_CMD							"PPID_CMD"
#define	SEND_PPID_CMD_ACK						"PPID_CMD_ACK"
												
#define	RECV_EOID_REQUEST						"EOID_REQUEST"
#define	SEND_EOID_REQUEST_ACK					"EOID_REQUEST_ACK"
												
#define	SEND_EOID_CHANGE_REPORT					"EOID_CHANGE_REPORT"
#define	RECV_EOID_CHANGE_REPORT_ACK				"EOID_CHANGE_REPORT_ACK"
#define	RECV_EOID_CMD							"EOID_CMD"
#define	SEND_EOID_CMD_ACK						"EOID_CMD_ACK"
												
#define	RECV_FILESERVER_UPLOAD_REPORT			"FILESERVER_UPLOAD_REPORT"
#define	SEND_FILESERVER_UPLOAD_REPORT_ACK		"FILESERVER_UPLOAD_REPORT_ACK"

#define	RECV_FILESERVER_DISCONNET_REPORT		"FILESERVER_DISCONNET_REPORT"
#define	SEND_FILESERVER_DISCONNECT_REPORT_ACK	"FILESERVER_DISCONNECT_REPORT_ACK"

#define	RECV_OP_CALL							"OP_CALL"
#define	SEND_OP_CALL_ACK						"OP_CALL_ACK"
#define	RECV_TERMINAL_MESSAGE					"TERMINAL_MESSAGE"
#define	SEND_TERMINAL_MESSAGE_ACK				"TERMINAL_MESSAGE_ACK"

//////////////////////////////////////////////////////////////////////////
//MCC
#define RECV_MCC_CMD							"MCC_CMD"
#define SEND_MCC_CMD_ACK						"MCC_CMD_ACK"

#define SEND_MCC_UPLOAD_REPORT					"MCC_UPLOAD_REPORT"
#define RECV_MCC_UPLOAD_ACK						"MCC_REPORT_ACK"

// static char *G_ByWho[] {
// 	"0",//"BY_HOST",
// 	"1",//"BY_OP",
// 	"2"//"BY_EQP_SELF"
// };

#define DATA1_OK									"OK"
#define DATA1_ERR									"ERR"
#define DATA1_REQ									"REQ"
#define DATA1_ON									"ON"
#define DATA1_OFF									"OFF"
#define DATA1_SET									"SET"
#define DATA1_RESET									"RESET"
#define DATA1_REPORT								"REPORT"

#define DATA2_PAUSE									"PAUSE"
#define DATA2_RESUME								"RESUME"



enum MATERIAL_INFO_TABLE
{
	Material_ID,MATERIAL_SETID,LOTID,BATCHID,JOBID,PORTID,SLOTNO,PRODUCT_TYPE,MATERIAL_KIND,
	PRODUCTID,RUNSPECID,LAYERID,STEPID,PPID,FLOWID,MATERIAL_SIZEX_MATERIAL_SIZEY,
	MATERIAL_THICKNESS,MATERIAL_STATE,MATERIAL_ORDER,COMMENT,USE_COUNT,JUDGEMENT,REASON_CODE,
	INS_FLAG,LIBRARYID,PRERUN_FLAG,TURN_DIR,FLIP_STATE,WORK_STATE,MULTI_USE,STAGE_STATE,LOCATION,
	OPTION_NAME1,OPTION_VALUE1,OPTION_NAME2,OPTION_VALUE2,OPTION_NAME3,OPTION_VALUE3,
	OPTION_NAME4,OPTION_VALUE4,OPTION_NAME5,OPTION_VALUE5,OperatorID
};

enum BY_WHO
{
	BY_HOST = 1,
	BY_OP,
	BY_EQUIP,
};

enum EQP_STATE
{
	EQP_NORMAL = 1,
	EQP_FAULT,
	EQP_PM,
};

enum PRST_STATE
{
	PRST_NONE = -1,
	PRST_IDLE = 1,
	PRST_SETUP,
	PRST_EXECUTE,
	PRST_PAUSE,
	//PRST_DISABLE,

	PRST_RESUME = 8
};

enum PPID_MODE
{
	NEW_PPID = 1,	
	DELETE_PPID,
	MODIFY_PPID,
};

enum CIM_SYSTEM_ERROR_REPLY
{
	ERROR_NORMAL = -1,

	ERROR_NOSTICK = 0,
	ERROR_STAGE_ERROR,
	ERROR_MATERIAL_ID_MISMATCH,
};

enum JOB_PROCESS
{	
	PROCESS_ABORT = 6,
	PROCESS_CANCEL = 7,

};

enum MCMD
{
	OFFLINE = 1,
	ONLINE_LOCAL,
	ONLINE_REMOTE,

};

enum ALARM
{
	ALARM_LIGHT =1,
	ALARM_HEAVY,	
};


//ALCD 

// 1, 2로 쓴다고 함
// Light, Heavy

// 문서에 있던내용

// 0 = not used
// 1 = Personal safety
// 2 = Equipment safety
// 3 = Parameter control warning
// 4 = Parameter control error
// 5 = Irrecoverable error
// 6 = Equipment state warning
// 7 = Attention flags
// 8 = Data integrity

// BY WYO
//1 By Host
//2 By Op
//3 By Eqip Self

//Judge
// OK Good Common
// NG Not Good Common
// RJ Reject Common
// RP Reprocess Common
// RT Pass' Reprocess Common
// RW Rework Common
// SK Skip Common
// HO Glass Hold Common
// * * Reserved
// * * Reserved
#include "..\resource.h"
#include "..\SubDlg_CIMInterface.h"
class CCimInterfaceSocket  : public CWnd
{
	DECLARE_DYNAMIC(CCimInterfaceSocket)

public:
	CCimInterfaceSocket(void);
	~CCimInterfaceSocket(void);

protected:

	DECLARE_MESSAGE_MAP()
public:
	bool m_bRecvMaterialData;
	bool m_bRecvJobStartCMD;
public:
	SubDlg_CIMInterface *m_CIMInfodlg;
private:
	CStringArray m_strArrayMaterialInfo;
public:

	void SendMsgCIMView(ListColor nType, const char* pWritLog, ...);
public:
	CRITICAL_SECTION  m_CriCimInter;
	CClientSocket m_ClientSocket;	
	CString m_strReciveString;
	char m_chServerIP[50];
	char m_chServerPortNum[50];
	CBIniFile iniSocket;
	CInterServerInterface *m_pServerInterface;


	BOOL m_bConnect;

	void	m_fnInitSocket(CInterServerInterface *pServerInterface);
	void	m_fnSendToCim(CString strMessageDefine, CString strData1, CString strData2="");
	void	m_fnGetSystemErrorStatus(int *pIndex);	
	int		m_fnGetSystemErrorCheck(BOOL bReturnTypeCheck);
	void	m_fnDataReset();
	CString m_fnGetMaterialInfo(int nIndex);
	
	CString m_fnGetPPIDLIst();
	CString m_fnGetPPIDValue(CString strPPID, int nMode=0);	/* 0 기본, 1 생성, 2 삭제, 3 수정*/
	void  m_fnSendPPIDCmd(CString strID, int nMode);
	void	m_fnSendAlarmReport(BOOL bSet, int nAlarmType, int nAlarmID, CString strAlarmText, CTime tOccurTime);
	void	m_fnSendAlarmWaitListAck(CString strAlarmList /*,로구분 */);

	void m_fnSendLoadComplete(CString strStickID);	
	void m_fnSendInspectionStart(CString strStickID);
	void m_fnSendInspectionComp(CString strStickID, CString strJudge);
	void m_fnSendUnloadComp(CString strStickID);
	void m_fnSendStickExistReport(BOOL bONOFF);

	// EQST(EqpStatus : Normal=1, Fault=2, PM=3) ,// ByWho(ByHost=1, ByOperator=2, ByEquipment itself=3) ,ALID(alarm ID),  Eqst 가 Fault일 경우만 Alarm ID랑 같이 보고함
	void m_fnSendEqstChangeReport(int nEqpStatus, int nByWho, int nAlarmID=0); 
	// PRST(Process State : IDLE=1, SETUP=2, EXECUTE=3, PAUSE=4, DISABLE=5), ByWho(ByHost=1, ByOperator=2, ByEquipment itself=3)
	void m_fnSendPrstChangeReport(int nPrstStatus, int nByWho);
	void m_fnSendMCMDRequest();
	void m_fnSendStickExistReport();

	//ECID
	//ECID=ECSLL=ECWLL=ECDEF=ECWUL=ECSUL, … (n)
	void m_fnSendECID_ListReqAck(); 
	void m_fnSendECID_ReqAck(CString strECID);
	void m_fnSendECID_ChangeReport();
	bool m_fnSendECID_ChangeCmdAck(CString strNewvalue);

	// SVID 
	//SV_VALUE(#),…,SV_VALUE(n) Value만 보고
	//void m_fnSendSVID_ReqAck(CString strValue);

	//EOID
	//EOID=EOV,…(n)
	void m_fnSendEOID_ReqAck();
	//EOID=EOV
	void m_fnSendEOID_ChangeReport(CString strValue);
	void m_fnSendEOID_ChangeCmdAck();
	void m_fnSendMCCFileUploadReport(char * chFolderName);


	//EOID

	BOOL	m_fnTimeSyncSet(CString strTime);

	int m_nPrOldState;
	int m_nEQSTOldState;
	int m_nOldSignalTower;

	struct CimDataFormat
	{
		char chNumber[10];
		char chMessageName[50];
		char chData1[10];
		char chData2[1500];

		CimDataFormat()
		{
			memset(this, 0x00, sizeof(CimDataFormat));
		}
		void CimDataFormatInit()
		{
			memset(this, 0x00, sizeof(CimDataFormat));
		}

	}m_CimDataFormat;

	struct CimLotInfo
	{
		char chFlowID[20];     
		char chFlowName[20];
		char chStepID[20];
		char chStepName[20];		
		char chLotID[20];
		char chProdType[20];
		char chProdTypeID[20];
		char chOperID[20];
		char chCstID[20];
		char chPPID[20];
		char chGlassID[20];
		char chPortNum[20];
		char chSlotNum[20];
		char chControlMode[20];  // CTRL 1, OFFLINE 2:ONLIE 3, ONLINE REMOTE
		char chTotalGlassCount[20]; // 추가사항
		char chMGlassCount[20];

		CimLotInfo()
		{
			memset(this, 0x00, sizeof(CimLotInfo));
		}
		void CimLotInfoInit()
		{
			memset(this, 0x00, sizeof(CimLotInfo));
		}

	}m_CimLotInfo;


	struct CimDataStruct {	
		unsigned short unEqpStatus;	
		unsigned short unProcStatus;	
		unsigned short unALCD;	
		unsigned short unALID;	
		char chALTX[80];
		unsigned short unALTM;	

		BOOL bUnloadReadyAck;
		BOOL bUnloadComplete;		

		//////////////////////////////////////////////////////////////////////////
		char chOPCallText[80];
		char chTerminalMsg[80];
		char chPPID[20];

		CimDataStruct()
		{
			memset(this, 0x00, sizeof(CimDataStruct));
		}
	}m_CimData;

	struct CimInterface {	
		BOOL bLoadReadySend;
		BOOL bUnloadReadySend;
		BOOL bLoadReady;
		BOOL bLoadComp;
		//////////////////////////////////////////////////////////////////////////
		BOOL bInspectStart;
		char	  chLotStartInfo[10];		
		//////////////////////////////////////////////////////////////////////////



		CimInterface()
		{
			memset(this, 0x00, sizeof(CimInterface));
		}
		void Reset()
		{
			memset(this, 0x00, sizeof(CimInterface));
		}
	}m_CimInterface;



	LRESULT OnClientReceive(WPARAM wParam, LPARAM lClient);
	LRESULT OnClientReConnect(WPARAM wParam, LPARAM lClient);
	LRESULT OnConnectOK(WPARAM wParam, LPARAM lClient);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

private:
	BOOL m_fnRecipeIDCheck(CString strRecipeID);
	BOOL m_fnCreateRecipeFile(CString strRecipeName, int nMode, CStringArray *pArray);
};
	
	

	
	

	
	

	
	

	
	

	
	

	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	

	

	
	
	
	
	
	
	
	

	
	
	
	
	
	
	

	
	
	
	

	
	
	
	
	
	

	
	
	
	
	
	

	
	

	
	
	
	

	


	
	
	
	
	
	
	
