#include "StdAfx.h"
#include "LiveGrab.h"



CvFont G_OvFont;
CvFont G_OvFontSmall;

//int nCntImg = 0;
UINT thfn_LiveThread(LPVOID pParam)
{
	GET_LIVE_THREAD *pData = (GET_LIVE_THREAD*)pParam;
	SMP_LIVE *pLiveMem = pData->pLiveMem;

	CvPoint MLineS, MLineE;
	CvPoint MLineS2, MLineE2;

	MLineS.x = 0; 
	MLineS.y = LIVE_SIZE_H/2;

	MLineE.x = LIVE_SIZE_W; 
	MLineE.y = LIVE_SIZE_H/2;	

	MLineS2.x = LIVE_SIZE_W/2; 
	MLineS2.y = 0;
	MLineE2.x = LIVE_SIZE_W/2; 
	MLineE2.y = LIVE_SIZE_H;

	//DWORD ProcTick = 0;
	//DWORD CalcFpsCam = 0;
	//DWORD CalcFpsCamBuf = 0;

	CvvImage ViewImage;
	char strImgMsg[256];
	
	ViewImage.Create(LIVE_SIZE_W, LIVE_SIZE_H, ((IPL_DEPTH_8U & 255)*3) );
	//ViewImage.Create(LIVE_SIZE_H, LIVE_SIZE_H, ((IPL_DEPTH_8U & 255)*4) );
	//IplImage *pImgBuf = nullptr;
	//pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 1);
	while(pData->LiveGrabCamsRun == true)
	{
		if(pLiveMem->LiveCheck == 2)
		{
			//ProcTick =  GetTickCount();
			//CalcFpsCam = GetTickCount() - CalcFpsCamBuf;
			
			//memcpy(pData->pGrabImg->imageData, pLiveMem->LiveImage, LIVE_IMG_SIZE);
			char *pImgDesPos = pData->pGrabImg->imageData;
			char *pImgSrcPos = &pLiveMem->LiveImage[0];
			for(int hStep=0; hStep<LIVE_SIZE_H; hStep++)
			{
				memcpy(pImgDesPos, pImgSrcPos, (LIVE_SIZE_W-2));
				pImgDesPos = pData->pGrabImg->imageData+(LIVE_SIZE_W*hStep);
				pImgSrcPos = &pLiveMem->LiveImage[((LIVE_SIZE_W-2)*hStep)];
			}


			cvCvtColor(pData->pGrabImg, ViewImage.GetImage(), CV_GRAY2RGB);
			//cvSaveImage("d:\\ThImg.bmp", pData->pGrabImg);
			//////////////////////////////////////////////////////////////////////////
			if(pData->pAlignProcImg == nullptr)
			{
				pData->pAlignProcImg = cvCloneImage(pData->pGrabImg);
				//sprintf_s(strImgMsg, "Align Grab Image");
				//cvPutText(ViewImage.GetImage(), strImgMsg,cvPoint(340, 490), &G_OvFont, CV_RGB(255,0,0)  );
			}

			//nCntImg++;
			//sprintf_s(strImgMsg, "Theta Align Cam (%d)", CalcFpsCamBuf);
			sprintf_s(strImgMsg, "Theta Align Cam");
			cvPutText(ViewImage.GetImage(), strImgMsg,cvPoint(10, 30), &G_OvFont, CV_RGB(255,0,0)  );

			//if(pCtrlObj->m_bViewCenterLine == true)
			{
				cvLine(ViewImage.GetImage(), MLineS, MLineE, CV_RGB(255,0,0), 2);
				cvLine(ViewImage.GetImage(), MLineS2, MLineE2, CV_RGB(255,0,0), 2);	
			}
			if(pData->LiveViewCam_HDC != 0)
				ViewImage.DrawToHDC(pData->LiveViewCam_HDC, pData->LiveViewSize);
				//ViewImage.Show(pData->LiveViewCam_HDC, 0,0,pData->LiveViewSize.Width(), pData->LiveViewSize.Height());

			pLiveMem->LiveCheck = 4;
			//CalcFpsCamBuf  = GetTickCount();
		}
		else
		{
			Sleep(10);
		}
	}

	if(pData->pAlignProcImg !=nullptr)
		cvReleaseImage(&pData->pAlignProcImg );
	pData->pAlignProcImg  = nullptr;
	
	ViewImage.Destroy();
	pData->pLiveThread = nullptr;

	return 0;
}
CLiveGrab::CLiveGrab(void)
{
	m_pImgData = nullptr;
	int MapSize = ((LIVE_SIZE_W*LIVE_SIZE_H)*1) + sizeof(int)*3+(sizeof(double)*2);
	hLIMapping = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, MapSize, CAM_LIVE_IMG_MAP_NAME); 
	hMapRead = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, CAM_LIVE_IMG_MAP_NAME);

	if(hMapRead != nullptr)
		m_pImgData = (SMP_LIVE*)MapViewOfFile(hMapRead, FILE_MAP_ALL_ACCESS, 0, 0, 0);

	m_pCam1Live = nullptr;
	cvInitFont(&G_OvFont, CV_FONT_HERSHEY_TRIPLEX, 1,1,5.0f);
	cvInitFont(&G_OvFontSmall, CV_FONT_HERSHEY_TRIPLEX, 0.5,0.5, 4.0f);
}


CLiveGrab::~CLiveGrab(void)
{
	m_ProcLiveThread.LiveGrabCamsRun = false;
	while(m_ProcLiveThread.pLiveThread != nullptr)
	{
		Sleep(10);
	}
	if(	m_pCam1Live != nullptr)
		cvReleaseImage(&m_pCam1Live);
	m_pCam1Live = nullptr;

	if(hMapRead!=nullptr)
		CloseHandle(hMapRead);

	if(hLIMapping!=nullptr)
		CloseHandle(hLIMapping);
}
bool CLiveGrab::RunLiveProgram()
{
	HWND hWnd = NULL;
	hWnd = ::FindWindowEx(NULL,0,"#32770", "Basler_CamLive");
	if(hWnd == NULL)
	{
		//ShellExecute(NULL, "open", "D:\\nBiz_InspectStock\\Theta_GrabStart.BAT" , NULL, NULL, SW_HIDE);
		//::WinExec("D:\\nBiz_InspectStock\\Theta_GrabStart.BAT", SW_HIDE);
		ShellExecute(NULL, _T("Open"), _T("D:/nBiz_InspectStock/Theta_GrabStart.BAT"), NULL, NULL, SW_SHOW);
		//::WinExec("D:/nBiz_InspectStock/Theta_GrabStart.BAT", SW_SHOW);
	}
	return true;
}
bool CLiveGrab::RunAFTask()
{
	//HWND hWnd = NULL;
	////hWnd = ::FindWindowEx(NULL,0,"#32770", "VS13AutoFocus.exe");
	//hWnd = ::FindWindow("#32770", _T("VS13AutoFocus.exe"));
	//if(hWnd == NULL)
	//{
	//	ShellExecute(NULL, _T("Open"), _T("D:/nBiz_InspectStock/nBiz_AFTask.BAT"), NULL, NULL, SW_SHOW);
	//}
	return true;
}
bool CLiveGrab::InitLiveThread()//HDC ViewCam1_HDC, CRect ViewCam1Size, HDC ViewCam2_HDC, CRect ViewCam2Size)
{
	RunLiveProgram();
	RunAFTask();
	if(m_pImgData == nullptr)
		return false;

	m_ProcLiveThread.LiveViewCam_HDC = nullptr;
	memset(&m_ProcLiveThread.LiveViewSize, 0x00, sizeof(CRect));
	if(m_ProcLiveThread.LiveGrabCamsRun == true)
		return true;

	//m_pCam1Live = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 1);
	m_pCam1Live = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 1);
	m_ProcLiveThread.pGrabImg = m_pCam1Live;
	m_ProcLiveThread.LiveGrabCamsRun = true;
	m_ProcLiveThread.pLiveMem = m_pImgData;
	m_ProcLiveThread.pLiveThread = ::AfxBeginThread(thfn_LiveThread, &m_ProcLiveThread, THREAD_PRIORITY_NORMAL);

	return true;
}
bool CLiveGrab::SetLiveDisplayObj(HDC ViewCam_HDC, CRect ViewCamSize)
{
	m_ProcLiveThread.LiveViewCam_HDC = ViewCam_HDC;
	m_ProcLiveThread.LiveViewSize = ViewCamSize;
	return true;
}

void CLiveGrab::SetCamLiveMode(int LiveMode)
{
	m_pImgData->UpDateMode = LiveMode;
}
int CLiveGrab::GetCamLiveMode()
{
	return m_pImgData->UpDateMode;
}
IplImage * CLiveGrab::GetGrabImg()
{
	//Sleep(300);//Grab Sync 용입니다.
	if(m_ProcLiveThread.pAlignProcImg != nullptr)
	{
		cvReleaseImage(&m_ProcLiveThread.pAlignProcImg);
		m_ProcLiveThread.pAlignProcImg = nullptr;
	}
	int OldGrabMode = m_pImgData->UpDateMode;
	if(OldGrabMode != LIVE_MODE_VIEW && OldGrabMode != LIVE_MODE_AF )
	{
		m_pImgData->UpDateMode= LIVE_MODE_GRAB;
	}
	
	//////////////////////////////////////////////////////////////////////////
	//Grab을 기다린다.
	int TimeOutCnt =0;
	int TimeOutSec =100*5;//5Sec
	while(m_ProcLiveThread.pAlignProcImg == nullptr)
	{
		Sleep(10);
		TimeOutCnt++;
		if(TimeOutCnt>=TimeOutSec)
		{
			G_WriteInfo(Log_Error, "==> Grab Image Fail!!!");
			m_pImgData->UpDateMode= OldGrabMode;
			return nullptr;
		}
	}
	m_pImgData->UpDateMode= OldGrabMode;
	return m_ProcLiveThread.pAlignProcImg;
}




void CLiveGrab::RestGrabProcess()
{
	m_pImgData->LiveGrabCamsRun = 0;
}

