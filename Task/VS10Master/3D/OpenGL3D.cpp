

#include "StdAfx.h"
#include "OpenGL3D.h"
#define Draw3DLine(x1,y1,z1,x2,y2,z2) \
	glBegin(GL_LINES); \
	glVertex3f((x1),(y1),(z1)); \
	glVertex3f((x2),(y2),(z2)); \
	glEnd();


GLfloat GLcolors[][4] = {

	{255/255.0f, 0/255.0f, 0/255.0f, 0.5f},//빨강
	{204/255.0f, 255/255.0f, 0/255.0f, 0.5f},//노랑연두
	{0/255.0f,102/255.0f,102/255.0f, 0.5f},//청색
	{75/255.0f,0/255.0f,153/255.0f, 0.5f},//남보라


	{255/255.0f, 51/255.0f, 0/255.0f, 0.5f},//다홍
	{131/255.0f,234/255.0f,29/255.0f, 0.5f},//연두
	{0/255.0f,153/255.0f,153/255.0f, 0.5f},//바다색
	{128/255.0f,0/255.0f,128/255.0f, 0.5f},//보라

	{255/255.0f, 102/255.0f, 0/255.0f, 0.5f},//주황
	{31/255.0f,200/255.0f,31/255.0f, 0.5f},//풀색
	{0/255.0f,0/255.0f,255/255.0f, 0.5f},//파랑
	{153/255.0f,0/255.0f,102/255.0f, 0.5f},//붉은 보라

	{255/255.0f, 204/255.0f, 0/255.0f, 0.5f},//귤색
	{6/255.0f,157/255.0f,6/255.0f, 0.5f},//녹색
	{0/255.0f,0/255.0f,204/255.0f, 0.5f},//감색
	{204/255.0f,0/255.0f,102/255.0f, 0.5f},//자주

	{255/255.0f, 255/255.0f, 0/255.0f, 0.5f},//노랑
	{0/255.0f,153/255.0f,102/255.0f, 0.5f},//초록
	{51/255.0f,0/255.0f,153/255.0f, 0.5f},//남색
	{255/255.0f,0/255.0f,102/255.0f, 0.5f}//연지



	//{255/255.0f, 0/255.0f, 0/255.0f},//빨강
	//{255/255.0f, 51/255.0f, 0/255.0f},//다홍
	//{255/255.0f, 102/255.0f, 0/255.0f},//주황
	//{255/255.0f, 204/255.0f, 0/255.0f},//귤색
	//{255/255.0f, 255/255.0f, 0/255.0f},//노랑

	//{204/255.0f, 255/255.0f, 0/255.0f},//노랑연두
	//{131/255.0f,234/255.0f,29/255.0f},//연두
	//{31/255.0f,200/255.0f,31/255.0f},//풀색
	//{6/255.0f,157/255.0f,6/255.0f},//녹색
	//{0/255.0f,153/255.0f,102/255.0f},//초록

	//{0/255.0f,102/255.0f,102/255.0f},//청색
	//{0/255.0f,153/255.0f,153/255.0f},//바다색
	//{0/255.0f,0/255.0f,255/255.0f},//파랑
	//{0/255.0f,0/255.0f,204/255.0f},//감색
	//{51/255.0f,0/255.0f,153/255.0f},//남색

	//{75/255.0f,0/255.0f,153/255.0f},//남보라
	//{128/255.0f,0/255.0f,128/255.0f},//보라
	//{153/255.0f,0/255.0f,102/255.0f},//붉은 보라
	//{204/255.0f,0/255.0f,102/255.0f},//자주
	//{255/255.0f,0/255.0f,102/255.0f}//연지
	};

//void glDrawText(float GLPosX, float GLPosY,  float GLPosZ,char* strMsg, void* font)//, double Color[3])
//{
//	double FontWidth = 0.02;
//
//	glColor3f(Color[0], Color[1], Color[2]);//X
//
//	int len = (int)strlen(strMsg);
//	glRasterPos3d( GLPosX , GLPosY, GLPosZ );
//	for ( int i = 0 ; i < len ; ++i )
//	{
//		glutBitmapCharacter( font, strMsg[i] );
//	}
//}

COpenGL3D::COpenGL3D(void)
{
	m_hWnd		= NULL;

	m_CellZDepth		= 0.1f;

	m_DefectZDepth  = m_CellZDepth-0.1;
	m_ScanZDepth	= m_DefectZDepth-0.1f;
	m_HWObjZDepth  = m_ScanZDepth-0.1f;

	m_HoldRender		= false;

	//m_nBlockCnt	= 0;
	//m_pBlockList	= nullptr;
	m_fStickWidth = 0.0f;
	m_fStickEdgePosW = 0.0f;
	m_fStickEdgePosH = 0.0f;

	m_nCellCnt	= 0;
	m_pCellList	= nullptr;

	m_nScanImgCnt = 0;
	m_pScanImgList = nullptr;

	m_nScopePointCnt = 0;
	m_pScopePointList = nullptr;

	m_ViewDrawBase	= true;
	m_ViewDrawAxis	= true;
	m_ViewDrawBox	= true;

	//Draw Object
	m_ViewDrawAOIHWInfor	= true;

	m_ViewDrawBlock	= true;
	m_ViewDrawPad	= true;
	m_ViewDrawCell	= true;
	m_ViewDrawScanImg		 = false;
	m_ViewDrawCDTPPoint		 = false;
	m_ViewDraw3DMeasurPoint = false;
	m_ViewDrawDefectData	= true;

	m_pActiveSX			= nullptr;
	m_pActiveSY			= nullptr;
	m_CenterX = 0;
	m_CenterY = 0;
	//m_ppDefectMappingImg		= nullptr;


	m_MouseXPosFromCenter = 0.0f;
	m_MouseYPosFromCenter = 0.0f;


	m_WinViewSizeW = 1024;
	m_WinViewSizeH = 768;

	m_pCamToObjDis = nullptr;

	m_fReviewShiftMin = 0.0f;
	m_fReviewShiftMax = 0.0f;
	m_fRingLightShiftMin = 0.0f;
	m_fRingLightShiftMax = 0.0f;

	m_pbUpReviewCylinder = nullptr;
	m_pbUpRingLightCylinder = nullptr;
	m_pbScopeSafetyPos = nullptr;

	m_pDefectDataMicQ = nullptr;
	m_pDefectDataMacQ = nullptr;
	m_AutoReviewCntMic = 0;
	m_AutoReviewCntMac = 0;
	m_pGlassReicpeParam = nullptr;
}

COpenGL3D::~COpenGL3D(void)
{
	if(wglGetCurrentContext())
		wglMakeCurrent(NULL, NULL);

	if(m_hGLContext)
	{
		wglDeleteContext(m_hGLContext);
		m_hGLContext = NULL;
	}
	::ReleaseDC(m_hWnd, m_hDC);
}
BOOL COpenGL3D::InitOpenGL(HWND hWnd, int StageXmm, int StageYmm)
{

	if (!::IsWindow(hWnd))
		return FALSE;

	//m_CenterX = (StageXmm*1000)/2;
	//m_CenterY = (StageYmm*1000)/2;

	m_3DViewerInfo.DnXMin =0.0f; //(-1.0f)*((float)(StageXmm/2));
	m_3DViewerInfo.DnXMax =(float)(StageXmm);// (float)(StageXmm/2);

	m_3DViewerInfo.DnYMin = 0;// (float)(StageYmm/2);
	m_3DViewerInfo.DnYMax = (float)(StageYmm);//(-1.0f)*((float)(StageYmm/2));

	//처음 Loding 시에는 합산이 아닌 Data를 뿌린다.
	m_3DViewerInfo.DnZMin = (-1.0f)*100.0;
	m_3DViewerInfo.DnZMax = (1.0f)*100.0;
	m_hWnd = hWnd;
	m_hDC = ::GetDC(m_hWnd);
	if(!SetWindowPixelFormat())
		return FALSE;
	if(!CreateViewGLContext())
		return FALSE;
	glClearDepth(1.0f);

	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_FRONT);

	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LEQUAL);
	//glEnable(GL_DEPTH_TEST | GL_DOUBLEBUFFER);

	glEnable(GL_TEXTURE_2D); //<11>   
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); //<12>  

	RasterFont();
	//////////////////////////////////////////////////////////////////////////
	//glGenTextures(1, &Textuerimg); //<4>
	//////////////////////////////////////////////////////////////////////////
	m_ViewText = false;

	wglMakeCurrent(m_hDC, m_hGLContext);
	return TRUE;
}

void COpenGL3D::DestroyOpenGL()
{
	if(wglGetCurrentContext())
		wglMakeCurrent(NULL, NULL);

	if(m_hGLContext)
	{
		wglDeleteContext(m_hGLContext);
		m_hGLContext = NULL;
	}
	::ReleaseDC(m_hWnd, m_hDC);
}
void COpenGL3D::RasterFont()
{
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	m_nFontOffset = glGenLists(128);
	for (GLuint i = 32; i < 127; i++)
	{
		glNewList(i + m_nFontOffset, GL_COMPILE);
		glBitmap(8, 13, 0.0f, 2.0f, 10.0f, 0.0f, G_RasterFont[i-32]);
		glEndList();
	}
}

void COpenGL3D::PrintString(const char* str)
{
	glPushAttrib(GL_LIST_BIT);
	glListBase(m_nFontOffset);
	glCallLists((int)strlen(str), GL_UNSIGNED_BYTE, (GLubyte*)str);
	glPopAttrib();
}
BOOL COpenGL3D::SetWindowPixelFormat()
{
	PIXELFORMATDESCRIPTOR pfd;

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);	// Size of this pfd
	pfd.nVersion = 1;							// Version number : must be 1
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |			// Support window
		PFD_SUPPORT_OPENGL |			// Support OpenGL
		PFD_DOUBLEBUFFER |			// Double buffered
		PFD_STEREO_DONTCARE;			// Support either monoscopic or stereoscopic
	pfd.iPixelType = PFD_TYPE_RGBA;				// RGBA type
	pfd.cColorBits = 32;						// Specifies the number of color bitplanes in each color buffer
	pfd.cRedBits = 8;							// Specifies the number of red bitplanes in each RGBA color buffer
	pfd.cRedShift = 16;							// Specifies the shift count for red bitplanes in each RGBA color buffer
	pfd.cGreenBits = 8;							// Specifies the number of green bitplanes in each RGBA color buffer
	pfd.cGreenShift = 8;						// Specifies the shift count for green bitplanes in each RGBA color buffer
	pfd.cBlueBits = 8;							// Specifies the number of blue bitplanes in each RGBA color buffer
	pfd.cBlueShift = 0;							// Specifies the shift count for blue bitplanes in each RGBA color buffer
	pfd.cAlphaBits = 0;							// Specifies the number of alpha bitplanes in each RGBA color buffer. Alpha bitplanes are not supported
	pfd.cAlphaShift = 0;						// Specifies the shift count for alpha bitplanes in each RGBA color buffer. Alpha bitplanes are not supported
	pfd.cAccumBits = 64;						// Specifies the total number of bitplanes in the accumulation buffer
	pfd.cAccumRedBits = 16;						// Specifies the number of red bitplanes in the accumulation buffer
	pfd.cAccumGreenBits = 16;					// Specifies the number of green bitplanes in the accumulation buffer
	pfd.cAccumBlueBits = 16;					// Specifies the number of blue bitplanes in the accumulation buffer
	pfd.cAccumAlphaBits = 0;					// Specifies the number of alpha bitplanes in the accumulation buffer
	pfd.cDepthBits = 32;						// Specifies the depth of the depth (z-axis) buffer
	pfd.cStencilBits = 8;						// Specifies the depth of the stencil buffer
	pfd.cAuxBuffers = 0;						// Specifies the number of auxiliary buffers. Auxiliary buffers are not supported
	pfd.iLayerType = PFD_MAIN_PLANE;			// Ignored. Earlier implementations of OpenGL used this member, but it is no longer used
	pfd.bReserved = 0;							// Specifies the number of overlay and underlay planes
	pfd.dwLayerMask = 0;						// Ignored. Earlier implementations of OpenGL used this member, but it is no longer used
	pfd.dwVisibleMask = 0;						// Specifies the transparent color or index of an underlay plane
	pfd.dwDamageMask = 0;						// Ignored. Earlier implementations of OpenGL used this member, but it is no longer used

	int m_GLPixelIndex = ChoosePixelFormat(m_hDC, &pfd);// Attempts to match an appropriate pixel format supported by a device context to a given pixel format specification
	if(m_GLPixelIndex == 0)								// Choose default
	{
		m_GLPixelIndex = 1;
		if(DescribePixelFormat(m_hDC, m_GLPixelIndex,	// Obtains information about the pixel format identified by iPixelFormat of the device associated with hdc
			sizeof(PIXELFORMATDESCRIPTOR), &pfd)==0)
			return FALSE;
	}
	if(!SetPixelFormat(m_hDC, m_GLPixelIndex, &pfd))	//Sets the pixel format of the specified device context to the format specified by the iPixelFormat index
		return FALSE;

	return TRUE;
}
BOOL COpenGL3D::CreateViewGLContext()
{
	m_hGLContext = wglCreateContext(m_hDC); // Create an OpenGL rendering context
	if(!m_hGLContext)
		return FALSE;
	if(!wglMakeCurrent(m_hDC, m_hGLContext)) // Set the current rendering context
		return FALSE;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
void COpenGL3D::ChangeViewSize(int newWindowWidth, int newWindowHeight)
{
	m_WinViewSizeW = newWindowWidth;
	m_WinViewSizeH = newWindowHeight;
	glViewport(0, 0, m_WinViewSizeW, m_WinViewSizeH);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(39.5,   (double)m_WinViewSizeW /(double)m_WinViewSizeH , 1.0f, 300.0f);
	//glShadeModel(GL_SMOOTH);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
	RenderScreen();
}

void COpenGL3D::DrawCamStaticUI()
{
	//DrawCrossLine();
	PositionInfoView();
}
void COpenGL3D::DrawCrossLine()
{
	glLineWidth(0.1f);

	glColor3f(0, 0, 1.0f);//Y
	glBegin(GL_LINES);
	glVertex3f(-10.0f, 0.0f, 0.0f);
	glVertex3f(10.0f, 0.0f, 0.0f);
	glEnd();

	glColor3f(1.0f, 0, 0);//X
	glBegin(GL_LINES);
	glVertex3f(0.0f, -10.0f, 0.0f);
	glVertex3f(0.0f, 10.0f, 0.0f);
	glEnd();

	glColor4f(0.0f, 0.0f,0.0f,1.0f);

	char strDisMsg[100];
	//sprintf_s(strDisMsg, "  X: %0.3f,   Y:%0.3f", m_ActionData.m_xMove, m_ActionData.m_yMove );
	//sprintf_s(strDisMsg, "  X: %0.3f,   Y:%0.3f( X: %0.3f,   Y:%0.3f)", m_ActionData.m_xMove+m_MouseXPosFromCenter, m_ActionData.m_yMove+m_MouseYPosFromCenter, m_ActionData.m_xMove, m_ActionData.m_yMove );
	sprintf_s(strDisMsg, "  X: %0.3f,   Y:%0.3f",m_MouseXPosFromCenter, m_MouseYPosFromCenter );
	//glDrawText(0.00f, 0.00f, 0.0f, strDisMsg, GLUT_BITMAP_HELVETICA_12);
	PrintString(strDisMsg);
	
}
void COpenGL3D::PositionInfoView()
{
	char strDisMsg[100];
	glColor4f(51/255.0f,0/255.0f,153/255.0f, 0.5f);
	sprintf_s(strDisMsg, "  X: %0.3fmm,   Y:%0.3fmm",m_MouseXPosFromCenter, m_MouseYPosFromCenter );
	//glDrawText(-1.70f, 2.05f, 0.0f, strDisMsg, GLUT_BITMAP_HELVETICA_18);
	PrintString(strDisMsg);
}
void COpenGL3D::DrawMouseCrossLine()
{
	float ViewValue = 10.0f/m_ActionData.m_zScale;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glLineWidth(0.1f);

	glColor3f(0, 0, 1.0f);//Y
	glBegin(GL_LINES);
	glVertex3f(m_MouseXPosFromCenter -ViewValue, m_MouseYPosFromCenter, m_ScanZDepth);
	glVertex3f(m_MouseXPosFromCenter + ViewValue, m_MouseYPosFromCenter, m_ScanZDepth);
	glEnd();

	glColor3f(1.0f, 0, 0);//X
	glBegin(GL_LINES);
	glVertex3f(m_MouseXPosFromCenter, m_MouseYPosFromCenter-ViewValue, m_ScanZDepth);
	glVertex3f(m_MouseXPosFromCenter, m_MouseYPosFromCenter+ViewValue, m_ScanZDepth);
	glEnd();
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}
//void COpenGL3D::DrawBox()
//{
//	//glVertex3f(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax, 0.0f);
//	//glVertex3f(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin, 0.0f);
//	//glVertex3f(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin, 0.0f);
//	//glVertex3f(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax, 0.0f);
//	//glColor4f(1.0f, 0.0f, 0.0f,1.0f);
//	//glLineWidth(0.1f);
//	//Draw3DLine(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin,0,
//	//	m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin,0);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin,0,
//	//	m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax,0);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax,0,
//	//	m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax,0);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax,0,
//	//	m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin,0);
//
//
//	//Draw3DLine(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin,30.0f,
//	//	m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin,30.0f);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin,30.0f,
//	//	m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax,30.0f);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax,30.0f,
//	//	m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax,30.0f);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax,30.0f,
//	//	m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin,30.0f);
//
//
//	//Draw3DLine(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin,0,
//	//	m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin,30.0f);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin,0,
//	//	m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin,30.0f);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax,0,
//	//	m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax,30.0f);
//
//	//Draw3DLine(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax,0,
//	//	m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax,30.0f);
//
//}

void COpenGL3D::DrawBase()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glBegin(GL_QUADS);
	glColor4f(0.5f, 0.5f, 0.5f,0.8f);
	glVertex3f(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMax, 0.0f);
	glVertex3f(m_3DViewerInfo.DnXMax, m_3DViewerInfo.DnYMin, 0.0f);
	glVertex3f(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMin, 0.0f);
	glVertex3f(m_3DViewerInfo.DnXMin, m_3DViewerInfo.DnYMax, 0.0f);
	glEnd();

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

void COpenGL3D::ReviewObjectsDraw()
{

	if(m_pCamToObjDis != nullptr)
	{

		glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
		//glLineWidth(2); // 선의 굵기
		glColor4f(1.0f,0.0f,0.0f, 1.0f);
		//////////////////////////////////////////////////////////////////////////
		//Review Camera X, Y Draw
		float Radius = 5.5f;
		glBegin(GL_POLYGON);
		for(float Angle = 0.0 * 3.141592f; Angle <= 2.0 * 3.141592f ; Angle += 3.141592f / 20.0f)
		{
			glVertex3f((Radius) * cos(Angle)+ (m_pCamToObjDis->fReviewCamPosX), 
				(Radius) * sin(Angle) + (m_pCamToObjDis->fReviewCamPosY), m_HWObjZDepth);
		}
		glEnd();
		if(m_pbUpReviewCylinder!=nullptr)
		{
			if(*m_pbUpReviewCylinder == true)
			{
				glColor3f(1.0f, 1.0f, 0);//X
				glLineWidth(2.0f);
				float Radius = 100.0f;
				glBegin(GL_LINE_LOOP);

				for (int i=0; i<360; i++)
				{
					float degInRad=i*(3.14159/180);
					glVertex3f(cos(degInRad)*Radius+ m_pCamToObjDis->fReviewCamPosX, sin(degInRad)*Radius+ m_pCamToObjDis->fReviewCamPosY, m_CellZDepth);
				}
				glEnd();
			}
		}
		//////////////////////////////////////////////////////////////////////////
		//Scan Camera X, Y Draw
		float NewPosSX = m_pCamToObjDis->fReviewCamPosX-m_pCamToObjDis->fReviewCamTo_LineScanX;
		float NewPosSY = m_pCamToObjDis->fReviewCamPosY-m_pCamToObjDis->fReviewCamTo_LineScanY;
		glLineWidth(2.0f);
		glColor3f(1.0f, 0, 0);//X
		glBegin(GL_LINE_LOOP);
		glVertex3f(m_pCamToObjDis->ScanCamArea.left/1000.0f+NewPosSX, m_pCamToObjDis->ScanCamArea.top/1000.0f+NewPosSY, m_HWObjZDepth);
		glVertex3f(m_pCamToObjDis->ScanCamArea.right/1000.0f+NewPosSX, m_pCamToObjDis->ScanCamArea.top/1000.0f+NewPosSY, m_HWObjZDepth);
		glVertex3f(m_pCamToObjDis->ScanCamArea.right/1000.0f+NewPosSX, m_pCamToObjDis->ScanCamArea.bottom/1000.0f+NewPosSY, m_HWObjZDepth);
		glVertex3f(m_pCamToObjDis->ScanCamArea.left/1000.0f+NewPosSX, m_pCamToObjDis->ScanCamArea.bottom/1000.0f+NewPosSY, m_HWObjZDepth);
		glVertex3f(m_pCamToObjDis->ScanCamArea.left/1000.0f+NewPosSX, m_pCamToObjDis->ScanCamArea.top/1000.0f+NewPosSY, m_HWObjZDepth);
		glEnd();
		if(m_pbUpRingLightCylinder!=nullptr)
		{
			if(*m_pbUpRingLightCylinder == true)
			{
				glColor3f(1.0f, 1.0f, 0);//X
				glLineWidth(2.0f);
				float Radius = 100.0f;
				glBegin(GL_LINE_LOOP);

				for (int i=0; i<360; i++)
				{
					float degInRad=i*(3.14159/180);
					glVertex3f(cos(degInRad)*Radius+ NewPosSX, sin(degInRad)*Radius+ NewPosSY, m_CellZDepth);
				}
				glEnd();
			}
		}
		
		//////////////////////////////////////////////////////////////////////////
		//Space Sensor X, Y Draw
		Radius = 5.5f;
		NewPosSX = m_pCamToObjDis->fReviewCamPosX-(m_pCamToObjDis->fReviewCamTo_SpaceSensorDisX);
		NewPosSY = m_pCamToObjDis->fReviewCamPosY-(m_pCamToObjDis->fReviewCamTo_SpaceSensorDisY);
		glLineWidth(2.0f);
		glColor3f(1.0f, 0, 0);//X
		glBegin(GL_LINE_LOOP);

		for (int i=0; i<360; i++)
		{
			float degInRad=i*(3.14159/180);
			glVertex3f(cos(degInRad)*Radius+ NewPosSX, sin(degInRad)*Radius+ NewPosSY, m_CellZDepth);
		}
		glEnd();


		//////////////////////////////////////////////////////////////////////////
		//Scope Camera X, Y Draw
		Radius = 5.5f;
		glColor3f(0, 0, 1.0f);//X
		glBegin(GL_POLYGON);
		for(float Angle = 0.0 * 3.141592f; Angle <= 2.0 * 3.141592f ; Angle += 3.141592f / 20.0f)
		{
			glVertex3f((Radius) * cos(Angle)+ (m_pCamToObjDis->fScopeCamPosX), 
				(Radius) * sin(Angle) + (m_pCamToObjDis->fScopeCamPosY), m_HWObjZDepth);
		}
		glEnd();
		if(m_pbScopeSafetyPos!=nullptr)
		{
			if(*m_pbScopeSafetyPos == false)
			{
				glColor3f(1.0f, 0.0f, 0);//X
				glLineWidth(2.0f);
				float Radius = 17.0f;
				glBegin(GL_LINE_LOOP);

				for (int i=0; i<360; i++)
				{
					float degInRad=i*(3.14159/180);
					glVertex3f(cos(degInRad)*Radius+ m_pCamToObjDis->fScopeCamPosX, 
						sin(degInRad)*Radius+ m_pCamToObjDis->fScopeCamPosY, m_CellZDepth);
				}
				glEnd();
			}
		}
		//////////////////////////////////////////////////////////////////////////
		//AirBlow X, Y Draw
		NewPosSX = m_pCamToObjDis->fScopeCamPosX-m_pCamToObjDis->fScopeCamTo_AirBlowDisX;
		NewPosSY = m_pCamToObjDis->fScopeCamPosY-m_pCamToObjDis->fScopeCamTo_AirBlowDisY;

		float ViewValue = 5.0f;
		glLineWidth(2.0f);
		glBegin(GL_LINES);
		glVertex3f(NewPosSX -ViewValue, NewPosSY, m_HWObjZDepth);
		glVertex3f(NewPosSX + ViewValue, NewPosSY, m_HWObjZDepth);
		glEnd();

		glBegin(GL_LINES);
		glVertex3f(NewPosSX, NewPosSY-ViewValue, m_HWObjZDepth);
		glVertex3f(NewPosSX, NewPosSY+ViewValue, m_HWObjZDepth);
		glEnd();

		//////////////////////////////////////////////////////////////////////////
		//Back Light X, Y Draw
		NewPosSX = m_pCamToObjDis->fScopeCamPosX-m_pCamToObjDis->fScopeCamTo_BackLightDisX;
		NewPosSY = m_pCamToObjDis->fScopeCamPosY-m_pCamToObjDis->fScopeCamTo_BackLightDisY;
		glLineWidth(2.0f);
		glColor3f(0, 0, 1.0f);//X
		glBegin(GL_LINE_LOOP);
		glVertex3f((m_pCamToObjDis->BackLightArea.left/1000.0f+NewPosSX), (m_pCamToObjDis->BackLightArea.top/1000.0f+NewPosSY), m_HWObjZDepth);
		glVertex3f((m_pCamToObjDis->BackLightArea.right/1000.0f+NewPosSX), (m_pCamToObjDis->BackLightArea.top/1000.0f+NewPosSY), m_HWObjZDepth);
		glVertex3f((m_pCamToObjDis->BackLightArea.right/1000.0f+NewPosSX), (m_pCamToObjDis->BackLightArea.bottom/1000.0f+NewPosSY), m_HWObjZDepth);
		glVertex3f((m_pCamToObjDis->BackLightArea.left/1000.0f+NewPosSX), (m_pCamToObjDis->BackLightArea.bottom/1000.0f+NewPosSY), m_ScanZDepth);
		glVertex3f((m_pCamToObjDis->BackLightArea.left/1000.0f+NewPosSX), (m_pCamToObjDis->BackLightArea.top/1000.0f+NewPosSY), m_HWObjZDepth);
		glEnd();

		//////////////////////////////////////////////////////////////////////////
		//Scan Sensor X, Y Draw
		NewPosSX = m_pCamToObjDis->fScopeCamPosX-m_pCamToObjDis->fScopeCamTo_ScanSensorDisX;
		NewPosSY = m_pCamToObjDis->fScopeCamPosY-m_pCamToObjDis->fScopeCamTo_ScanSensorDisY;
		glLineWidth(2.0f);
		glColor3f(0, 0, 1.0f);//X
		glBegin(GL_LINE_LOOP);
		glVertex3f((m_pCamToObjDis->ScanSensortArea.left/1000.0f+NewPosSX), (m_pCamToObjDis->ScanSensortArea.top/1000.0f+NewPosSY), m_HWObjZDepth);
		glVertex3f((m_pCamToObjDis->ScanSensortArea.right/1000.0f+NewPosSX), (m_pCamToObjDis->ScanSensortArea.top/1000.0f+NewPosSY), m_HWObjZDepth);
		glVertex3f((m_pCamToObjDis->ScanSensortArea.right/1000.0f+NewPosSX), (m_pCamToObjDis->ScanSensortArea.bottom/1000.0f+NewPosSY), m_HWObjZDepth);
		glVertex3f((m_pCamToObjDis->ScanSensortArea.left/1000.0f+NewPosSX), (m_pCamToObjDis->ScanSensortArea.bottom/1000.0f+NewPosSY), m_HWObjZDepth);
		glVertex3f((m_pCamToObjDis->ScanSensortArea.left/1000.0f+NewPosSX), (m_pCamToObjDis->ScanSensortArea.top/1000.0f+NewPosSY), m_HWObjZDepth);
		glEnd();


		//////////////////////////////////////////////////////////////////////////
		//Theta Align Camera X, Y Draw
		NewPosSX = m_pCamToObjDis->fScopeCamPosX-m_pCamToObjDis->fScopeCamTo_ThetaAlignCamDisX;
		NewPosSY = m_pCamToObjDis->fScopeCamPosY-m_pCamToObjDis->fScopeCamTo_ThetaAlignCamDisY;
		glLineWidth(2.0f);
		glColor3f( 0, 0, 1.0f);//X
		glBegin(GL_LINE_LOOP);

		for (int i=0; i<360; i++)
		{
			float degInRad=i*(3.14159/180);
			glVertex3f(cos(degInRad)*Radius+ NewPosSX, sin(degInRad)*Radius+ NewPosSY, m_CellZDepth);
		}
		glEnd();

		glDepthMask(GL_TRUE);
		glDisable(GL_BLEND);

	}
}


void COpenGL3D::DrawAxis()
{
	glLineWidth(2.0f);

	glColor3f(1.0f, 0, 0);//X
	glBegin(GL_LINES);
	glVertex3f(0.0f, -10.0f, 0.0f);
	glVertex3f(0.0f, 10.0f, 0.0f);
	glEnd();

	glColor3f(1.0f, 0, 0);//X
	glBegin(GL_LINES);
	glVertex3f(-10.0f, 0.0f, 0.0f);
	glVertex3f(10.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(100.0f, 100-10.0f, 0.0f);
	glVertex3f(100.0f,100+ 10.0f, 0.0f);
	glEnd();

	glColor3f(1.0f, 0, 0);//X
	glBegin(GL_LINES);
	glVertex3f(100-10.0f, 100.0f, 0.0f);
	glVertex3f(100+10.0f, 100.0f, 0.0f);
	glEnd();
}

void COpenGL3D::RenderScreen()
{
	glClearColor(0.0f,	0.0f, 0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	//////////////////////////////////////////////////////////////////////////
	//
	{//형상 변환.
		glTranslated(m_ActionData.m_xTrans, m_ActionData.m_yTrans, m_ActionData.m_zTrans);
		//DrawCamStaticUI();
		glRotatef(m_ActionData.m_xRotat,1.0, 0.0, 0.0);
		glRotatef(m_ActionData.m_yRotat, 0.0, 1.0, 0.0);
		glRotatef(m_ActionData.m_zRotat, 0.0, 0.0, 1.0);

		if(m_ActionData.m_xRotat > 360)		m_ActionData.m_xRotat -= 360;
		if(m_ActionData.m_xRotat < -360)	m_ActionData.m_xRotat += 360;
		if(m_ActionData.m_yRotat > 360)		m_ActionData.m_yRotat -= 360;
		if(m_ActionData.m_yRotat < -360)	m_ActionData.m_yRotat += 360;
		if(m_ActionData.m_zRotat > 360)		m_ActionData.m_zRotat -= 360;
		if(m_ActionData.m_zRotat < -360)	m_ActionData.m_zRotat += 360;

		glScalef(m_ActionData.m_xScale, m_ActionData.m_yScale, m_ActionData.m_zScale);
	}
	{//View 영역 설정.
		//////////////////////////////////////////////////////////////////////////
		glOrtho (m_3DViewerInfo.DnXMin + m_ActionData.m_xMove,	
			m_3DViewerInfo.DnXMax + m_ActionData.m_xMove, 
			m_3DViewerInfo.DnYMin + m_ActionData.m_yMove,
			m_3DViewerInfo.DnYMax + m_ActionData.m_yMove,
			m_3DViewerInfo.DnZMin,
			m_3DViewerInfo.DnZMax);
	}
	{//Object Drawing.
		{
			//전체 View Position 제어.
			float ViewCenter  = abs(m_3DViewerInfo.DnZMin + m_3DViewerInfo.DnZMax);
			glTranslatef(0,0,ViewCenter);//전체
			//SetViewPortArea();
			if(m_ViewDrawBase) DrawBase();
			//if(m_ViewDrawAxis) 
			//DrawAxis();
			//if(m_ViewDrawBox)DrawBox();
			//DrawStick();
			//Draw Object
			
			if(m_ViewDrawCell)DrawStickCell();
			if(m_ViewDrawScanImg)DrawScanImg();
			if(m_ViewDraw3DMeasurPoint)Draw3DScopePos();
			if(m_ViewDrawCDTPPoint)DrawCDTPPos();
			

			ReviewObjectsDraw();
			DrawMouseCrossLine();

			
		}
	}
	//////////////////////////////////////////////////////////////////////////
	glPopMatrix();
	glFinish();
	SwapBuffers(wglGetCurrentDC());
}
void COpenGL3D::MouseDisByView(float xPos, float yPos)
{
	m_MouseXPosFromCenter = m_ActionData.m_xMove + (xPos/(m_ActionData.m_xScale/(1.0f*SCALE_VALUE)));
	m_MouseYPosFromCenter = m_ActionData.m_yMove + (yPos/(m_ActionData.m_yScale/((STAGE_MAX_STROKE_Y/STAGE_MAX_STROKE_X)*SCALE_VALUE)));//*(-1.0f);
	RenderScreen();
}

void COpenGL3D::SetViewPortArea()
{
	m_RenderView.left		= (m_ActionData.m_xMove -(float)(m_WinViewSizeW/2)/(m_ActionData.m_xScale/(1.0f*SCALE_VALUE)))*1000.0f;
	m_RenderView.right		= (m_ActionData.m_xMove +(float)(m_WinViewSizeW/2)/(m_ActionData.m_xScale/(1.0f*SCALE_VALUE)))*1000.0f;
	m_RenderView.top		= (m_ActionData.m_yMove -(float)(m_WinViewSizeH/2)/(m_ActionData.m_yScale/((STAGE_MAX_STROKE_Y/STAGE_MAX_STROKE_X)*SCALE_VALUE)))*1000.0f;
	m_RenderView.bottom	= (m_ActionData.m_yMove +(float)(m_WinViewSizeH/2)/(m_ActionData.m_yScale/((STAGE_MAX_STROKE_Y/STAGE_MAX_STROKE_X)*SCALE_VALUE)))*1000.0f;
}

void COpenGL3D::MoveObject(float xRate, float yRate)
{
	m_ActionData.m_xMove +=(float)xRate/(m_ActionData.m_xScale/(1.0f*SCALE_VALUE));
	m_ActionData.m_yMove +=(float)yRate/(m_ActionData.m_yScale/((STAGE_MAX_STROKE_Y/STAGE_MAX_STROKE_X)*SCALE_VALUE));
	RenderScreen();


}
void COpenGL3D::RotatObject(float xRate, float yRate)
{
	m_ActionData.m_xRotat -= (float)(xRate);
	m_ActionData.m_yRotat += (float)(yRate);
	RenderScreen();
	//::InvalidateRect(m_hWnd, NULL, FALSE);

}
void COpenGL3D::ZoomObject(float UpDownValue)
{
	

	if(m_ActionData.m_ScaleValue+UpDownValue>=0.45f)//1.0f )
	{
		if(m_ActionData.m_ScaleValue>80.0f)
			UpDownValue *=2;
		if(m_ActionData.m_ScaleValue>150.0f)
			UpDownValue *=3;

		m_ActionData.m_ScaleValue+=  UpDownValue;
		m_ActionData.m_xScale =  1.0f * m_ActionData.m_ScaleValue;
		m_ActionData.m_yScale = (STAGE_MAX_STROKE_Y/STAGE_MAX_STROKE_X) * m_ActionData.m_ScaleValue;
		m_ActionData.m_zScale = 0.23f * m_ActionData.m_ScaleValue;
		RenderScreen();
	}
}

void COpenGL3D::SetRendDataCell(int StickWidth, int StickCellEdgeDisW, int StickCellEdgeDisH, int CCnt, CELLINFO *pDataList)
{
	m_fStickWidth		= (float)StickWidth/1000.0f;
	m_fStickEdgePosW = (pDataList[0].m_CellPos.left/1000.0f)- (float)StickCellEdgeDisW/1000.0f;
	m_fStickEdgePosH = (pDataList[0].m_CellPos.top/1000.0f) -  (float)StickCellEdgeDisH/1000.0f;
	m_nCellCnt			= CCnt;
	m_pCellList			= pDataList;
}
void COpenGL3D::DrawStickCell()
{
	if(m_pCellList == nullptr || m_nCellCnt<=0)
		return;

	float OffsetX  = ((float)G_ReviewToDrawXOffset)/1000.0f;
	float OffsetY  = ((float)G_ReviewToDrawYOffset)/1000.0f;
	

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	//Draw Stick.
	glBegin(GL_POLYGON);
	glColor4f(0.988f, 0.839f, 0.403f,0.5f);
	glVertex3f(m_fStickEdgePosW+OffsetX, m_fStickEdgePosH+OffsetY, 0.2f);
	glVertex3f(m_fStickEdgePosW+OffsetX+m_fStickWidth, m_fStickEdgePosH+OffsetY , 0.2);
	glVertex3f(m_fStickEdgePosW+OffsetX+m_fStickWidth, m_fStickEdgePosH+OffsetY+STAGE_MAX_STROKE_Y, 0.2);
	glVertex3f(m_fStickEdgePosW+OffsetX, m_fStickEdgePosH+OffsetY+STAGE_MAX_STROKE_Y, 0.2f);
	glEnd();

	//Draw Cell
	glColor4f(0.8f, 0.8f, 0.8f,0.8f);
	glBegin(GL_QUADS);
	for(int DrwStep = 0; DrwStep<m_nCellCnt; DrwStep++)
	{
		glVertex3f((m_pCellList[DrwStep].m_CellPos.left/1000.0f)+OffsetX, 
			(m_pCellList[DrwStep].m_CellPos.top/1000.0f)+OffsetY, m_CellZDepth);

		glVertex3f((m_pCellList[DrwStep].m_CellPos.right/1000.0f)+OffsetX, 
			(m_pCellList[DrwStep].m_CellPos.top/1000.0f)+OffsetY, m_CellZDepth);

		glVertex3f((m_pCellList[DrwStep].m_CellPos.right/1000.0f)+OffsetX, 
			(m_pCellList[DrwStep].m_CellPos.bottom/1000.0f)+OffsetY, m_CellZDepth);

		glVertex3f((m_pCellList[DrwStep].m_CellPos.left/1000.0f)+OffsetX,
			(m_pCellList[DrwStep].m_CellPos.bottom/1000.0f)+OffsetY, m_CellZDepth);
	}
	glEnd();




	//m_fReviewShiftMin;
	//m_fReviewShiftMax;
	//m_fRingLightShiftMin;
	//m_fRingLightShiftMax;

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
	if(m_pbUpRingLightCylinder != nullptr && m_pbUpReviewCylinder!= nullptr && m_pbScopeSafetyPos !=nullptr)
	{
		if(*m_pbUpReviewCylinder == true)
		{
			glLineWidth(1.0f);
			glColor4f(0.0f, 0.0f, 1.0f,0.0f);
			Draw3DLine((m_fReviewShiftMin-110.0f)+OffsetX, 
				0.1f +OffsetY-100.0f, m_CellZDepth, 
				(m_fReviewShiftMax+110.0f)+OffsetX, 
				0.1f +OffsetY-100.0f, m_CellZDepth);

			Draw3DLine((m_fReviewShiftMin-110.0f)+OffsetX, 
				877.300f +OffsetY+100.0f, m_CellZDepth, 
				(m_fReviewShiftMax+110.0f)+OffsetX, 
				877.300f +OffsetY+100.0f, m_CellZDepth);

			Draw3DLine((m_fReviewShiftMax+110.0f)+OffsetX, 
				0.1f +OffsetY-100.0f, m_CellZDepth, 
				(m_fReviewShiftMax+110.0f)+OffsetX, 
				877.300f +OffsetY+100.0f, m_CellZDepth);

			Draw3DLine((m_fReviewShiftMin-110.0f)+OffsetX, 
				0.1f +OffsetY+-100.0f, m_CellZDepth, 
				(m_fReviewShiftMin-110.0f)+OffsetX, 
				877.300f +OffsetY+100.0f, m_CellZDepth);

			//glLineWidth(2.0f);
			glColor4f(1.0f, 0.0f, 0.0f,0.0f);
			Draw3DLine(m_fReviewShiftMin+OffsetX, 
				0.1f +OffsetY, m_CellZDepth, 
				m_fReviewShiftMax+OffsetX, 
				0.1f +OffsetY, m_CellZDepth);

			Draw3DLine(m_fReviewShiftMin+OffsetX, 
				877.300f +OffsetY, m_CellZDepth, 
				m_fReviewShiftMax+OffsetX, 
				877.300f +OffsetY, m_CellZDepth);

			Draw3DLine(m_fReviewShiftMin+OffsetX, 
				0.1f +OffsetY, m_CellZDepth, 
				m_fReviewShiftMin+OffsetX, 
				877.300f +OffsetY, m_CellZDepth);

			Draw3DLine(m_fReviewShiftMax+OffsetX, 
				0.1f +OffsetY, m_CellZDepth, 
				m_fReviewShiftMax+OffsetX, 
				877.300f +OffsetY, m_CellZDepth);
		}
		if(*m_pbUpRingLightCylinder == true)
		{
			glLineWidth(1.0f);
			glColor4f(0.0f, 0.0f, 1.0f,0.0f);
			Draw3DLine((m_fRingLightShiftMin-100.0f)+OffsetX, 
				0.1f +OffsetY-100.0f, m_CellZDepth, 
				(m_fRingLightShiftMax+100.0f)+OffsetX, 
				0.1f +OffsetY-100.0f, m_CellZDepth);

			Draw3DLine((m_fRingLightShiftMin-100.0f)+OffsetX, 
				877.300f +OffsetY+100.0f, m_CellZDepth, 
				(m_fRingLightShiftMax+100.0f)+OffsetX, 
				877.300f +OffsetY+100.0f, m_CellZDepth);

			Draw3DLine((m_fRingLightShiftMax+100.0f)+OffsetX, 
				0.1f +OffsetY-100.0f, m_CellZDepth, 
				(m_fRingLightShiftMax+100.0f)+OffsetX, 
				877.300f +OffsetY+100.0f, m_CellZDepth);

			Draw3DLine((m_fRingLightShiftMin-100.0f)+OffsetX, 
				0.1f +OffsetY-100.0f, m_CellZDepth, 
				(m_fRingLightShiftMin-100.0f)+OffsetX, 
				877.300f +OffsetY+100.0f, m_CellZDepth);

			//glLineWidth(2.0f);
			glColor4f(1.0f, 0.0f, 0.0f,0.0f);
			Draw3DLine(m_fRingLightShiftMin+OffsetX, 
				0.1f +OffsetY, m_CellZDepth, 
				m_fRingLightShiftMax+OffsetX, 
				0.1f +OffsetY, m_CellZDepth);

			Draw3DLine(m_fRingLightShiftMin+OffsetX, 
				877.300f +OffsetY, m_CellZDepth, 
				m_fRingLightShiftMax+OffsetX, 
				877.300f +OffsetY, m_CellZDepth);

			Draw3DLine(m_fRingLightShiftMin+OffsetX, 
				0.1f +OffsetY, m_CellZDepth, 
				m_fRingLightShiftMin+OffsetX, 
				877.300f +OffsetY, m_CellZDepth);

			Draw3DLine(m_fRingLightShiftMax+OffsetX, 
				0.1f +OffsetY, m_CellZDepth, 
				m_fRingLightShiftMax+OffsetX, 
				877.300f +OffsetY, m_CellZDepth);
		}

		if(*m_pbScopeSafetyPos == false)
		{
			float ScopeOffsetX  = ((float)G_ScopeToDrawXOffset)/1000.0f;
			float ScopeOffsetY  = ((float)G_ScopeToDrawYOffset)/1000.0f;

			glLineWidth(2.0f);
			glColor4f(1.0f, 1.0f, 0.0f,0.0f);
			Draw3DLine(m_fScopeShiftMin+ScopeOffsetX, 
				340.0f +ScopeOffsetY, m_CellZDepth, 
				m_fScopeShiftMax+ScopeOffsetX, 
				340.0f +ScopeOffsetY, m_CellZDepth);

			Draw3DLine(m_fScopeShiftMin+ScopeOffsetX, 
				1320.000f +ScopeOffsetY, m_CellZDepth, 
				m_fScopeShiftMax+ScopeOffsetX, 
				1320.000f +ScopeOffsetY, m_CellZDepth);

			Draw3DLine(m_fScopeShiftMin+ScopeOffsetX, 
				340.0f +ScopeOffsetY, m_CellZDepth, 
				m_fScopeShiftMin+ScopeOffsetX, 
				1320.000f +ScopeOffsetY, m_CellZDepth);

			Draw3DLine(m_fScopeShiftMax+ScopeOffsetX, 
				340.0f +ScopeOffsetY, m_CellZDepth, 
				m_fScopeShiftMax+ScopeOffsetX, 
				1320.000f +ScopeOffsetY, m_CellZDepth);
		}
	}
}


void COpenGL3D::SetRendDataScan(int SCnt, SCANIMGINFO *pDataList, int ReviewCntMic, DefectDataQ * pDefectDataMicQ, int ReviewCntMac, DefectDataQ * pDefectDataMacQ, GLASS_PARAM *pGlassReicpeParam)
{
	//m_nScanActiveIndex[TaskIndex] = -1;
	m_nScanImgCnt		= SCnt;
	m_pScanImgList		= pDataList;

	m_AutoReviewCntMic     = ReviewCntMic;
	m_AutoReviewCntMac     = ReviewCntMac;
	m_pDefectDataMicQ		= pDefectDataMicQ;
	m_pDefectDataMacQ		= pDefectDataMacQ;

	m_pGlassReicpeParam = pGlassReicpeParam;
	m_ViewDrawScanImg = true;
}
void COpenGL3D::DrawScanImg()
{

	
	float OffsetX  = ((float)G_ReviewToDrawXOffset)/1000.0f;
	float OffsetY  = ((float)G_ReviewToDrawYOffset)/1000.0f;

	char PrintData[20]={0,0};

	for(int DrwStep = 0; DrwStep<m_nScanImgCnt; DrwStep++)
	{
		if((m_pScanImgList+DrwStep)->m_ScanLineNum == 0 && (m_pScanImgList+DrwStep)->m_ScanImgIndex ==0)
			break;

		glColor4f(1.0f, 0.8f, 0.8f,0.8f);
		Draw3DLine(m_3DViewerInfo.DnXMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.left/1000.0f)+OffsetX, 
						m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.top/1000.0f)+OffsetY, m_ScanZDepth,
						m_3DViewerInfo.DnXMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.right/1000.0f)+OffsetX, 
						m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.top/1000.0f)+OffsetY, m_ScanZDepth);


		Draw3DLine(m_3DViewerInfo.DnXMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.right/1000.0f)+OffsetX, 
			m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.top/1000.0f)+OffsetY, m_ScanZDepth, 
			m_3DViewerInfo.DnXMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.right/1000.0f)+OffsetX, 
			m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.bottom/1000.0f)+OffsetY, m_ScanZDepth);

		Draw3DLine(m_3DViewerInfo.DnXMin +((m_pScanImgList+DrwStep)->m_ScanImgPos.right/1000.0f)+OffsetX, 
			m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.bottom/1000.0f)+OffsetY, m_ScanZDepth,
			m_3DViewerInfo.DnXMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.left/1000.0f)+OffsetX,
			m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.bottom/1000.0f)+OffsetY, m_ScanZDepth);

		Draw3DLine(m_3DViewerInfo.DnXMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.left/1000.0f)+OffsetX, 
			m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.top/1000.0f)+OffsetY, m_ScanZDepth,
			m_3DViewerInfo.DnXMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.left/1000.0f)+OffsetX,
			m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.bottom/1000.0f)+OffsetY, m_ScanZDepth);

		//if(m_ViewText == true)
		//{
		//	glColor4f(1.0f, 0.0f,1.0f,1.0f);
		//	glRasterPos3f(m_3DViewerInfo.DnXMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.left/1000.0f)+OffsetX, 
		//		m_3DViewerInfo.DnYMin + ((m_pScanImgList+DrwStep)->m_ScanImgPos.top/1000.0f)+OffsetY,
		//		m_ScanZDepth);

		//	sprintf_s(PrintData,"%02d_%02d", (m_pScanImgList+DrwStep)->m_ScanLineNum, (m_pScanImgList+DrwStep)->m_ScanImgIndex);
		//	PrintString(PrintData);
		//}
	}
	//glEnd();
	//glDepthMask(GL_TRUE);
	//glDisable(GL_BLEND);

	//Defect Draw
	if(m_pDefectDataMicQ != nullptr && m_pGlassReicpeParam != nullptr)
	{
		if(m_pDefectDataMicQ->QGetCnt()>0)
		{
			glColor3f(1.0f, 0.0f, 0.0f);
			int DrawIndex = 0;
			CRect DefectInfo;

			DataNodeObj * pNowReviewData = nullptr;
			m_pDefectDataMicQ->QGetMoveFirstNode();
			pNowReviewData = m_pDefectDataMicQ->QGetNextNodeData();
			while (pNowReviewData != nullptr)
			{
				if(DrawIndex < m_AutoReviewCntMic)
					glColor3f(1.0f, 0.0f, 0.0f);
				else
					glColor3f(0.0f, 1.0f, 0.0f);
				//////////////////////////////////////////////////////////////////////////
				//Draw Defect
				DefectInfo.left = (m_pGlassReicpeParam->CenterPointX + pNowReviewData->DefectData.CoodStartX);
				DefectInfo.top = (m_pGlassReicpeParam->CenterPointY + pNowReviewData->DefectData.CoodStartY*(-1));
				DefectInfo.right = DefectInfo.left+3000;//pNowReviewData->DefectData.CoodWidth;
				DefectInfo.bottom = DefectInfo.top+3000;//pNowReviewData->DefectData.CoodHeight;

				glBegin(GL_QUADS);
				for(int DrwStep = 0; DrwStep<m_nCellCnt; DrwStep++)
				{
					glVertex3f((DefectInfo.left/1000.0f)+OffsetX, 
						(DefectInfo.top/1000.0f)+OffsetY, m_CellZDepth);
					glVertex3f((DefectInfo.right/1000.0f)+OffsetX, 
						(DefectInfo.top/1000.0f)+OffsetY, m_CellZDepth);
					glVertex3f((DefectInfo.right/1000.0f)+OffsetX, 
						(DefectInfo.bottom/1000.0f)+OffsetY, m_CellZDepth);
					glVertex3f((DefectInfo.left/1000.0f)+OffsetX,
						(DefectInfo.bottom/1000.0f)+OffsetY, m_CellZDepth);
				}
				glEnd();

				//////////////////////////////////////////////////////////////////////////
				pNowReviewData = m_pDefectDataMicQ->QGetNextNodeData();

				DrawIndex++;
			}
		}
		if(m_pDefectDataMacQ != nullptr && m_pGlassReicpeParam != nullptr)
		{
			if(m_pDefectDataMacQ->QGetCnt()>0)
			{
				glColor3f(0.0f, 0.0f, 1.0f);
				int DrawIndex = 0;
				CRect DefectInfo;

				DataNodeObj * pNowReviewData = nullptr;
				m_pDefectDataMacQ->QGetMoveFirstNode();
				pNowReviewData = m_pDefectDataMacQ->QGetNextNodeData();
				while (pNowReviewData != nullptr)
				{
					if(DrawIndex < m_AutoReviewCntMac)
						glColor3f(0.0f, 0.0f, 1.0f);
					else
						glColor3f(0.0f, 1.0f, 1.0f);
					//////////////////////////////////////////////////////////////////////////
					//Draw Defect
					DefectInfo.left = (m_pGlassReicpeParam->CenterPointX + pNowReviewData->DefectData.CoodStartX);
					DefectInfo.top = (m_pGlassReicpeParam->CenterPointY + pNowReviewData->DefectData.CoodStartY*(-1));
					DefectInfo.right = DefectInfo.left+3000;//pNowReviewData->DefectData.CoodWidth;
					DefectInfo.bottom = DefectInfo.top+3000;//pNowReviewData->DefectData.CoodHeight;

					glBegin(GL_QUADS);
					for(int DrwStep = 0; DrwStep<m_nCellCnt; DrwStep++)
					{
						glVertex3f((DefectInfo.left/1000.0f)+OffsetX, 
							(DefectInfo.top/1000.0f)+OffsetY, m_CellZDepth);
						glVertex3f((DefectInfo.right/1000.0f)+OffsetX, 
							(DefectInfo.top/1000.0f)+OffsetY, m_CellZDepth);
						glVertex3f((DefectInfo.right/1000.0f)+OffsetX, 
							(DefectInfo.bottom/1000.0f)+OffsetY, m_CellZDepth);
						glVertex3f((DefectInfo.left/1000.0f)+OffsetX,
							(DefectInfo.bottom/1000.0f)+OffsetY, m_CellZDepth);
					}
					glEnd();

					//////////////////////////////////////////////////////////////////////////
					pNowReviewData = m_pDefectDataMacQ->QGetNextNodeData();

					DrawIndex++;
				}
			}
		}

	}
}

void COpenGL3D::SetRendData3DScope(int SCnt, SCOPE_3DPoint *pDataList)
{
	m_nScopePointCnt =  SCnt;
	m_pScopePointList = pDataList;
	m_ViewDraw3DMeasurPoint = true;
}

void COpenGL3D::Draw3DScopePos()
{

	float OffsetX  = ((float)G_ReviewToDrawXOffset)/1000.0f;
	float OffsetY  = ((float)G_ReviewToDrawYOffset)/1000.0f;
	float Radius = 3.5f;
	float degInRad=0.0f;
	glLineWidth(2.0f);
	glColor4f(0.0f, 0.0f, 0.0f,0.8f);
	for(int DrwStep = 0; DrwStep<m_nScopePointCnt; DrwStep++)
	{
		//Draw3DLine((m_pScopePointList[DrwStep].nPointX/1000.0f)-5.0f, 
		//				(m_pScopePointList[DrwStep].nPointY/1000.0f)-5.0f, m_ScanZDepth,
		//				(m_pScopePointList[DrwStep].nPointX/1000.0f)+5.0f, 
		//				(m_pScopePointList[DrwStep].nPointY/1000.0f)+5.0f, m_ScanZDepth);
		//Draw3DLine((m_pScopePointList[DrwStep].nPointX/1000.0f)+5.0f, 
		//				(m_pScopePointList[DrwStep].nPointY/1000.0f)-5.0f, m_ScanZDepth,
		//				(m_pScopePointList[DrwStep].nPointX/1000.0f)-5.0f, 
		//				(m_pScopePointList[DrwStep].nPointY/1000.0f)+5.0f,m_ScanZDepth);
		glBegin(GL_LINE_LOOP);

		for (int i=0; i<360; i++)
		{
			degInRad=i*(3.14159/180);
			glVertex3f(cos(degInRad)*Radius+ (m_pScopePointList[DrwStep].nPointX/1000.0f)+OffsetX, sin(degInRad)*Radius+ (m_pScopePointList[DrwStep].nPointY/1000.0f)+OffsetY, m_CellZDepth);
		}
		glEnd();

		//glBegin(GL_POLYGON);
		//for(float Angle = 0.0 * 3.141592f; Angle <= 2.0 * 3.141592f ; Angle += 3.141592f / 20.0f)
		//{
		//	glVertex3f((Radius) * cos(Angle)+ (m_pScopePointList[DrwStep].nPointX/1000.0f), 
		//		(Radius) * sin(Angle) + (m_pScopePointList[DrwStep].nPointY/1000.0f), m_HWObjZDepth);
		//}
		//glEnd();
	}
}
void COpenGL3D::SetRendCDTP(int SCnt, CDTP_Point *pDataList)
{
	m_nCDTPPointCnt = SCnt;
	m_pCDTPPointList = pDataList;
	m_ViewDrawCDTPPoint = true;
}
void COpenGL3D::DrawCDTPPos()
{

	float OffsetX  = ((float)G_ReviewToDrawXOffset)/1000.0f;
	float OffsetY  = ((float)G_ReviewToDrawYOffset)/1000.0f;

	float Radius = 1.5f;
	float degInRad=0.0f;
	glLineWidth(2.0f);
	glColor4f(1.0f, 0.0f, 1.0f,0.8f);
	for(int DrwStep = 0; DrwStep<m_nCDTPPointCnt; DrwStep++)
	{
		//Draw3DLine((m_pScopePointList[DrwStep].nPointX/1000.0f)-5.0f, 
		//				(m_pScopePointList[DrwStep].nPointY/1000.0f)-5.0f, m_ScanZDepth,
		//				(m_pScopePointList[DrwStep].nPointX/1000.0f)+5.0f, 
		//				(m_pScopePointList[DrwStep].nPointY/1000.0f)+5.0f, m_ScanZDepth);
		//Draw3DLine((m_pScopePointList[DrwStep].nPointX/1000.0f)+5.0f, 
		//				(m_pScopePointList[DrwStep].nPointY/1000.0f)-5.0f, m_ScanZDepth,
		//				(m_pScopePointList[DrwStep].nPointX/1000.0f)-5.0f, 
		//				(m_pScopePointList[DrwStep].nPointY/1000.0f)+5.0f,m_ScanZDepth);
		glBegin(GL_LINE_LOOP);

		for (int i=0; i<360; i++)
		{
			degInRad=i*(3.14159/180);
			glVertex3f(cos(degInRad)*Radius+ (m_pCDTPPointList[DrwStep].nPointX/1000.0f)+OffsetX, sin(degInRad)*Radius+ (m_pCDTPPointList[DrwStep].nPointY/1000.0f)+OffsetY, m_CellZDepth);
		}
		glEnd();

		//glBegin(GL_POLYGON);
		//for(float Angle = 0.0 * 3.141592f; Angle <= 2.0 * 3.141592f ; Angle += 3.141592f / 20.0f)
		//{
		//	glVertex3f((Radius) * cos(Angle)+ (m_pScopePointList[DrwStep].nPointX/1000.0f), 
		//		(Radius) * sin(Angle) + (m_pScopePointList[DrwStep].nPointY/1000.0f), m_HWObjZDepth);
		//}
		//glEnd();
	}
}

//void COpenGL3D::SetRendDataDefect(int DCnt, Draw_DEFECTINFO *pDataList)
//{
//	m_nDefCnt	= DCnt;
//	m_pDefList	= pDataList;
//	RenderScreen();
//}
//void COpenGL3D::DrawDefectData()
//{
//	glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//	glDepthMask(GL_FALSE);
//
//	//glBegin(GL_QUADS);
//	//glColor4f(1.0, 0.0f, 0.0f,1.0f);
//	float ViewValue = 3.5f/m_ActionData.m_zScale;
//	if(m_ActionData.m_zScale>100.0f)
//		ViewValue = 0;
//
//	//GLUquadricObj * quadric = gluNewQuadric();
//	//gluDeleteQuadric(quadric); 
//	glEnable(GL_BLEND);
//	glDepthMask(GL_FALSE);
//
//	float Radius = 0;
//	CPoint InViewRect;
//	for(int DrwStep = 0; DrwStep<m_nDefCnt; DrwStep++)
//	{
//
//		if(ViewValue ==0)
//			glColor4f(GLcolors[m_pDefList[DrwStep].m_GlassIndex][0],
//			GLcolors[m_pDefList[DrwStep].m_GlassIndex][1], 
//			GLcolors[m_pDefList[DrwStep].m_GlassIndex][2],
//			1.0f);
//		else
//			glColor4f(GLcolors[m_pDefList[DrwStep].m_GlassIndex][0],
//			GLcolors[m_pDefList[DrwStep].m_GlassIndex][1], 
//			GLcolors[m_pDefList[DrwStep].m_GlassIndex][2],
//			GLcolors[m_pDefList[DrwStep].m_GlassIndex][3]);
//
//		Radius = (m_pDefList[DrwStep].m_DefEndY>m_pDefList[DrwStep].m_DefEndX?m_pDefList[DrwStep].m_DefEndY:m_pDefList[DrwStep].m_DefEndX);
//		
//		InViewRect.x =(int) (m_pDefList[DrwStep].m_DefStartX*1000.0f);
//		InViewRect.y =(int) (m_pDefList[DrwStep].m_DefStartY*1000.0f);
//		if( m_RenderView.PtInRect(InViewRect) == TRUE)
//		{
//			glBegin(GL_POLYGON);
//			for(float Angle = 0.0 * 3.141592f; Angle <= 2.0 * 3.141592f ; Angle += 3.141592f / 20.0f)
//			{
//				glVertex3f((Radius+ViewValue) * cos(Angle)+ (m_pDefList[DrwStep].m_DefStartX), 
//					(Radius+ViewValue) * sin(Angle) + (m_pDefList[DrwStep].m_DefStartY), m_DefectZDepth);
//			}
//			glEnd();
//		}
//		
//	}
//	glDepthMask(GL_TRUE);
//	glDisable(GL_BLEND);
//
//
//	if(m_ActionData.m_zScale > 10.0f)
//	{
//		char strDisMsg[100];
//		float Offset = 0.0f;
//		if(ViewValue == 0)
//		{
//			glColor4f(1.0f,0.0f,0.0f,1.0f);
//			Offset = 0.1f;
//		}
//		else
//		{
//			glColor4f(0.0f,0.0f,0.0f,1.0f);
//			Offset = ViewValue*2.0f;
//		}
//		for(int DrwStep = 0; DrwStep<m_nDefCnt; DrwStep++)
//		{
//			InViewRect.x =(int) (m_pDefList[DrwStep].m_DefStartX*1000.0f);
//			InViewRect.y =(int) (m_pDefList[DrwStep].m_DefStartY*1000.0f);
//			if( m_RenderView.PtInRect(InViewRect) == TRUE  &&  ViewValue == 0)
//			{
//				sprintf_s(strDisMsg, "%s : X : %dum, Y : %dum", m_pDefList[DrwStep].m_CellName,(int) (m_pDefList[DrwStep].m_DefCoorX*1000.0f), (int)(m_pDefList[DrwStep].m_DefCoorY*1000.0f));
//				glDrawText(m_pDefList[DrwStep].m_DefStartX, (m_pDefList[DrwStep].m_DefStartY), m_DefectZDepth, 	strDisMsg, GLUT_BITMAP_8_BY_13);
//
//				sprintf_s(strDisMsg, "      Size:(%dum, %dum)", (int)(m_pDefList[DrwStep].m_DefEndX*1000.0f), (int)(m_pDefList[DrwStep].m_DefEndY*1000.0f));
//				glDrawText(m_pDefList[DrwStep].m_DefStartX, (m_pDefList[DrwStep].m_DefStartY+Offset), m_DefectZDepth, 	strDisMsg, GLUT_BITMAP_8_BY_13);
//			}
//		}
//	}
//}
//void COpenGL3D::SetSelectItemDefect(int CCnt, Draw_DEFECTINFO *pDataList)
//{
//	m_nSelectCnt	= CCnt;
//	m_pSelectList	= pDataList;
//	RenderScreen();
//}
//void COpenGL3D::DrawSelectItem()
//{
//	if(m_pSelectList != NULL && m_nSelectCnt>0)
//	{
//
//		float ViewValue = 3.9f/m_ActionData.m_zScale;
//		if(m_ActionData.m_zScale>100.0f)
//			ViewValue = 0;
//
//		//GLUquadricObj * quadric = gluNewQuadric();
//		//gluDeleteQuadric(quadric); 
//		glEnable(GL_BLEND);
//		glDepthMask(GL_FALSE);
//		glLineWidth(2); // 선의 굵기
//		glColor4f(0.0f,0.0f,0.0f, 1.0f);
//		
//		for(int DrwStep = 0; DrwStep<m_nSelectCnt && ViewValue != 0; DrwStep++)
//		{
//			float radius = abs(m_pSelectList[DrwStep].m_DefEndY - m_pSelectList[DrwStep].m_DefStartY)/2+ViewValue;
//			float DEGINRAD=3.14159/180;
//			int Nums=360;
//			// drawing 명령을 실행
//			glBegin(GL_LINE_LOOP);
//
//			for (int i=0; i<Nums; i++)
//			{
//				float degInRad=i*DEGINRAD;
//				glVertex3f(cos(degInRad)*radius+ (m_pSelectList[DrwStep].m_DefStartX), sin(degInRad)*radius+ (m_pSelectList[DrwStep].m_DefStartY), m_CellZDepth);
//			}
//			glEnd();
//
//		}
//		glDepthMask(GL_TRUE);
//		glDisable(GL_BLEND);
//	}
//}