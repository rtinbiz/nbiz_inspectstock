#if !defined(__ClassArr_H__)
#define __ClassArr_H__

#include <assert.h>
#include <memory.h>
#include <new.h>   //only supports Win32 and Mac
using namespace std;

template<class TYPE, class ARG_TYPE> class CClassArray
{
public:
// Construction & destruction
	CClassArray()									{ m_pData = NULL; m_nSize = m_nMaxSize = m_nGrowBy = 0; }
	~CClassArray();

// Attributes
	int GetSize() const								{ return m_nSize; }
	int GetUpperBound() const						{ return m_nSize-1; }
	void SetSize(int nNewSize, int nGrowBy = -1);

// Operations
	// Clean up
	void FreeExtra();
	void RemoveAll()								{ SetSize(0, -1); }

	// Accessing elements
	TYPE GetAt(int nIndex) const					{ assert(nIndex >= 0 && nIndex < m_nSize); return m_pData[nIndex]; }
	void SetAt(int nIndex, ARG_TYPE newElement)		{ assert(nIndex >= 0 && nIndex < m_nSize); m_pData[nIndex] = newElement; }
	TYPE& ElementAt(int nIndex)						{ assert(nIndex >= 0 && nIndex < m_nSize); return m_pData[nIndex]; }

	// Direct Access to the element data (may return NULL)
	const TYPE* GetData() const						{ return (const TYPE*)m_pData; }
	TYPE* GetData()									{ return (TYPE*)m_pData; }

	// Potentially growing the array
	void SetAtGrow(int nIndex, ARG_TYPE newElement);
	int Add(ARG_TYPE newElement)					{ int nIndex = m_nSize;	SetAtGrow(nIndex, newElement); return nIndex; }
	int Append(const CClassArray& src);
	void Copy(const CClassArray& src);

	// overloaded operator helpers
	TYPE operator[](int nIndex) const				{ return GetAt(nIndex); }
	TYPE& operator[](int nIndex)					{ return ElementAt(nIndex); }

	// Operations that move elements around
	void InsertAt(int nIndex, ARG_TYPE newElement, int nCount = 1);
	void RemoveAt(int nIndex, int nCount = 1);
	void InsertAt(int nStartIndex, CClassArray* pNewArray);

// Implementation
protected:
	void ConstructElements(TYPE* pElements, int nCount);
	void DestructElements(TYPE* pElements, int nCount);
	void CopyElements(TYPE* pDest, const TYPE* pSrc, int nCount);

	TYPE* m_pData;   // the actual array of data
	int m_nSize;     // # of elements (upperBound - 1)
	int m_nMaxSize;  // max allocated
	int m_nGrowBy;   // grow amount
};


#endif
