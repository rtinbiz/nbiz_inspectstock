// MaintDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "MaintDlg.h"
#include "afxdialogex.h"


// CMaintDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMaintDlg, CDialogEx)

CMaintDlg::CMaintDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMaintDlg::IDD, pParent)
{
	m_pReviewPos = nullptr;
	m_pScopePos = nullptr;
	m_pGripperPos = nullptr;

	m_pLiveViewObj = nullptr;
	m_pCamToObjDis = nullptr;

	m_pbUpReviewCylinder = nullptr;
	m_pbUpRingLightCylinder = nullptr;
}

CMaintDlg::~CMaintDlg()
{
}

void CMaintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BT_STANDBY_SCOPE_UPDOWN, m_btScope_UpDown_Standby);
	DDX_Control(pDX, IDC_BT_MEASURE_SCOPE_UPDOWN, m_btScope_UpDown_Measur);
	//	DDX_Control(pDX, IDC_BT_THETA_ALIGN_SCOPE_UPDOWN, m_btScope_UpDown_ThetaAlign);
	DDX_Control(pDX, IDC_BT_STANDBY_SHIFT_DRIVE_SCOPE, m_btScope_ShiftDrive_Standby);
	DDX_Control(pDX, IDC_BT_STANDBY_REVIEW_UPDOWN, m_btReview_UpDown_Standby);
	DDX_Control(pDX, IDC_BT_REVIEW_REVIEW_UPDOWN, m_btReview_UpDown_Review);
	DDX_Control(pDX, IDC_BT_INSPECTION_REVIEW_UPDOWN, m_btReview_UpDown_Inspection);
	DDX_Control(pDX, IDC_BT_STANDBY_SHIFT_DRIVE_SCAN, m_btReview_ShiftDrive_Standby);
	DDX_Control(pDX, IDC_BT_STANDBY_SPACE_UPDOWN, m_btSpace_UpDown_Standby);
	DDX_Control(pDX, IDC_BT_MEASURE_SPACE_UPDOWN, m_btSpace_UpDown_Measure);
	DDX_Control(pDX, IDC_BT_STANDBY_GRIPPER_UPDOWN, m_btGripper_UpDown_Standby);
	DDX_Control(pDX, IDC_BT_GRIP_STANDBY_GRIPPER_UPDOWN, m_btGripper_UpDown_GripStandby);
	DDX_Control(pDX, IDC_BT_GRIP_GRIPPER_UPDOWN, m_btGripper_UpDown_Grip);
	DDX_Control(pDX, IDC_BT_MEASURE_GRIPPER_UPDOWN, m_btGripper_UpDown_Measure);
	DDX_Control(pDX, IDC_BT_TT01OUT_GRIPPER_UPDOWN, m_btGripper_UpDown_TT01OUT);
	DDX_Control(pDX, IDC_BT_STANDBY_TENSION_FBWD, m_btTension_FBward_Standby);
	DDX_Control(pDX, IDC_BT_GRIP_TENSION_FBWD, m_btTension_FBward_TensionStandby);
	DDX_Control(pDX, IDC_BT_GRIP_STANDBY_TENSION_FBWD, m_btTension_FBward_TensionGrip);
	DDX_Control(pDX, IDC_ST_STICK_IN_AM01_1, m_stGripperStickDetect_Left);
	DDX_Control(pDX, IDC_ST_STICK_IN_AM01_2, m_stGripperStickDetect_Right);
	DDX_Control(pDX, IDC_BT_TENSION_FWD, m_btTensionCylinder_FWD);
	DDX_Control(pDX, IDC_BT_TENSION_BWD, m_btTensionCylinder_BWD);
	DDX_Control(pDX, IDC_BT_GRIPPER_MAINT_OPEN, m_btTensionCylinder_GripClose);
	DDX_Control(pDX, IDC_BT_GRIPPER_MAINT_CLOSE, m_btTensionCylinder_GripOpen);
	DDX_Control(pDX, IDC_CH_SYNC_SCOPE_BACK_LIGHT,	m_chSyncAxisSelect_Scope[0]);
	DDX_Control(pDX, IDC_CH_SYNC_SCOPE_CAM,				m_chSyncAxisSelect_Scope[1]);
	DDX_Control(pDX, IDC_CH_SYNC_SCOPE_AIRBLOW,		m_chSyncAxisSelect_Scope[2]);
	DDX_Control(pDX, IDC_CH_SYNC_SCOPE_AREA_SENSOR,	m_chSyncAxisSelect_Scope[3]);

	DDX_Control(pDX, IDC_CH_SYNC_SCAN_REVIEW_CAM, m_chSyncAxisSelect_Scan[0]);
	DDX_Control(pDX, IDC_CH_SYNC_SCAN_SPACE_SENSOR, m_chSyncAxisSelect_Scan[1]);
	DDX_Control(pDX, IDC_CH_SYNC_SCAN_LINE_SCAN_CAM, m_chSyncAxisSelect_Scan[2]);

	DDX_Control(pDX, IDC_BT_MOVE_SCAN_XY_MULTI, m_btReviewMulti_Move);
	DDX_Control(pDX, IDC_BT_MOVE_SCAN_SHIFT_X, m_btReview_ShiftStep_Move);
	DDX_Control(pDX, IDC_BT_MOVE_SCAN_DRIVE_Y, m_btReview_DriveStep_Move);
	DDX_Control(pDX, IDC_BT_MOVE_SCOPE_XY_MULTI, m_btScopeMulti_Move);
	DDX_Control(pDX, IDC_BT_MOVE_SCOPE_SHIFT_X, m_btScope_ShiftStep_Move);
	DDX_Control(pDX, IDC_BT_MOVE_SCOPE_DRIVE_Y, m_btScope_DriveStep_Move);
	DDX_Control(pDX, IDC_BT_STOP_SCAN_XY, m_btReviewActionStop);
	DDX_Control(pDX, IDC_BT_STOP_SCOPE_XY, m_btScopeActionStop);
	DDX_Control(pDX, IDC_BT_EXIT_MAINT, m_btExitMaint);
	//DDX_Control(pDX, IDC_BT_AREA3D_MEASURE_SCOPE_UPDOWN, m_btScope_UpDown_Area3D_Measure);
	DDX_Control(pDX, IDC_BT_MOVE_LSCAN_XY_MULTI, m_btLineScanSync_Move);
	DDX_Control(pDX, IDC_BT_MOVE_LSCAN_SHIFT_X, m_btLineScanStep_Shift);
	DDX_Control(pDX, IDC_BT_MOVE_LSCAN_DRIVE_Y, m_btLineScanStep_Drive);
	DDX_Control(pDX, IDC_BT_STOP_LSCAN_XY, m_btLineScan_ActionStop);
	DDX_Control(pDX, IDC_BT_RING_LIGHT_UP, m_btRingLight_Cylinder_Up);
	DDX_Control(pDX, IDC_BT_RING_LIGHT_DN, m_btRingLight_Cylinder_Down);
	DDX_Control(pDX, IDC_BT_REVIEW_UP, m_btReview_Cylinder_Up);
	DDX_Control(pDX, IDC_BT_REVIEW_DN, m_btReview_Cylinder_Down);
	DDX_Control(pDX, IDC_BT_STICK_JIG_IN, m_btStickJig_Input);
	DDX_Control(pDX, IDC_BT_STICK_JIG_OUT, m_btStickJig_Output);
}


BEGIN_MESSAGE_MAP(CMaintDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_STANDBY_REVIEW_UPDOWN, &CMaintDlg::OnBnClickedBtStandbyReviewUpdown)
	ON_BN_CLICKED(IDC_BT_REVIEW_REVIEW_UPDOWN, &CMaintDlg::OnBnClickedBtReviewReviewUpdown)
	ON_BN_CLICKED(IDC_BT_INSPECTION_REVIEW_UPDOWN, &CMaintDlg::OnBnClickedBtInspectionReviewUpdown)
	ON_BN_CLICKED(IDC_BT_STANDBY_SHIFT_DRIVE_SCAN, &CMaintDlg::OnBnClickedBtStandbyShiftDriveScan)
	ON_BN_CLICKED(IDC_BT_STANDBY_SCOPE_UPDOWN, &CMaintDlg::OnBnClickedBtStandbyScopeUpdown)
	ON_BN_CLICKED(IDC_BT_MEASURE_SCOPE_UPDOWN, &CMaintDlg::OnBnClickedBtMeasureScopeUpdown)
	ON_BN_CLICKED(IDC_BT_THETA_ALIGN_SCOPE_UPDOWN, &CMaintDlg::OnBnClickedBtThetaAlignScopeUpdown)
	ON_BN_CLICKED(IDC_BT_STANDBY_SHIFT_DRIVE_SCOPE, &CMaintDlg::OnBnClickedBtStandbyShiftDriveScope)
	ON_BN_CLICKED(IDC_BT_STANDBY_SPACE_UPDOWN, &CMaintDlg::OnBnClickedBtStandbySpaceUpdown)
	ON_BN_CLICKED(IDC_BT_MEASURE_SPACE_UPDOWN, &CMaintDlg::OnBnClickedBtMeasureSpaceUpdown)
	ON_BN_CLICKED(IDC_BT_STANDBY_TENSION_FBWD, &CMaintDlg::OnBnClickedBtStandbyTensionFbwd)
	ON_BN_CLICKED(IDC_BT_GRIP_TENSION_FBWD, &CMaintDlg::OnBnClickedBtGripTensionFbwd)
	ON_BN_CLICKED(IDC_BT_GRIP_STANDBY_TENSION_FBWD, &CMaintDlg::OnBnClickedBtGripStandbyTensionFbwd)
	ON_BN_CLICKED(IDC_BT_STANDBY_GRIPPER_UPDOWN, &CMaintDlg::OnBnClickedBtStandbyGripperUpdown)
	ON_BN_CLICKED(IDC_BT_GRIP_STANDBY_GRIPPER_UPDOWN, &CMaintDlg::OnBnClickedBtGripStandbyGripperUpdown)
	ON_BN_CLICKED(IDC_BT_GRIP_GRIPPER_UPDOWN, &CMaintDlg::OnBnClickedBtGripGripperUpdown)
	ON_BN_CLICKED(IDC_BT_MEASURE_GRIPPER_UPDOWN, &CMaintDlg::OnBnClickedBtMeasureGripperUpdown)
	ON_BN_CLICKED(IDC_BT_TT01OUT_GRIPPER_UPDOWN, &CMaintDlg::OnBnClickedBtTt01outGripperUpdown)
	ON_BN_CLICKED(IDC_BT_TENSION_FWD, &CMaintDlg::OnBnClickedBtTensionFwd)
	ON_BN_CLICKED(IDC_BT_TENSION_BWD, &CMaintDlg::OnBnClickedBtTensionBwd)
	ON_BN_CLICKED(IDC_BT_GRIPPER_MAINT_OPEN, &CMaintDlg::OnBnClickedBtGripperMaintOpen)
	ON_BN_CLICKED(IDC_BT_GRIPPER_MAINT_CLOSE, &CMaintDlg::OnBnClickedBtGripperMaintClose)

	ON_BN_CLICKED(IDC_CH_SYNC_SCOPE_BACK_LIGHT, &CMaintDlg::OnBnClickedChSyncScope)
	ON_BN_CLICKED(IDC_CH_SYNC_SCOPE_CAM, &CMaintDlg::OnBnClickedChSyncScope)
	ON_BN_CLICKED(IDC_CH_SYNC_SCOPE_AIRBLOW, &CMaintDlg::OnBnClickedChSyncScope)
	ON_BN_CLICKED(IDC_CH_SYNC_SCOPE_AREA_SENSOR, &CMaintDlg::OnBnClickedChSyncScope)
	ON_BN_CLICKED(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM, &CMaintDlg::OnBnClickedChSyncScope)
	ON_BN_CLICKED(IDC_CH_SYNC_SCAN_REVIEW_CAM, &CMaintDlg::OnBnClickedChSyncScan)
	ON_BN_CLICKED(IDC_CH_SYNC_SCAN_SPACE_SENSOR, &CMaintDlg::OnBnClickedChSyncScan)
	ON_BN_CLICKED(IDC_CH_SYNC_SCAN_LINE_SCAN_CAM, &CMaintDlg::OnBnClickedChSyncScan)
	ON_BN_CLICKED(IDC_BT_MOVE_SCAN_XY_MULTI, &CMaintDlg::OnBnClickedBtMoveScanXyMulti)
	ON_BN_CLICKED(IDC_BT_MOVE_SCAN_SHIFT_X, &CMaintDlg::OnBnClickedBtMoveScanShiftX)
	ON_BN_CLICKED(IDC_BT_MOVE_SCAN_DRIVE_Y, &CMaintDlg::OnBnClickedBtMoveScanDriveY)
	ON_BN_CLICKED(IDC_BT_STOP_SCAN_XY, &CMaintDlg::OnBnClickedBtStopScanXy)
	ON_BN_CLICKED(IDC_BT_MOVE_SCOPE_XY_MULTI, &CMaintDlg::OnBnClickedBtMoveScopeXyMulti)
	ON_BN_CLICKED(IDC_BT_MOVE_SCOPE_SHIFT_X, &CMaintDlg::OnBnClickedBtMoveScopeShiftX)
	ON_BN_CLICKED(IDC_BT_MOVE_SCOPE_DRIVE_Y, &CMaintDlg::OnBnClickedBtMoveScopeDriveY)
	ON_BN_CLICKED(IDC_BT_STOP_SCOPE_XY, &CMaintDlg::OnBnClickedBtStopScopeXy)
	ON_BN_CLICKED(IDC_BT_EXIT_MAINT, &CMaintDlg::OnBnClickedBtExitMaint)
	ON_BN_CLICKED(IDC_BT_MOVE_LSCAN_XY_MULTI, &CMaintDlg::OnBnClickedBtMoveLscanXyMulti)
	ON_BN_CLICKED(IDC_BT_MOVE_LSCAN_SHIFT_X, &CMaintDlg::OnBnClickedBtMoveLscanShiftX)
	ON_BN_CLICKED(IDC_BT_MOVE_LSCAN_DRIVE_Y, &CMaintDlg::OnBnClickedBtMoveLscanDriveY)
	ON_BN_CLICKED(IDC_BT_STOP_LSCAN_XY, &CMaintDlg::OnBnClickedBtStopLscanXy)
	ON_BN_CLICKED(IDC_BT_AREA3D_MEASURE_SCOPE_UPDOWN, &CMaintDlg::OnBnClickedBtArea3dMeasureScopeUpdown)
	ON_BN_CLICKED(IDC_BT_RING_LIGHT_UP, &CMaintDlg::OnBnClickedBtRingLightUp)
	ON_BN_CLICKED(IDC_BT_RING_LIGHT_DN, &CMaintDlg::OnBnClickedBtRingLightDn)
	ON_BN_CLICKED(IDC_BT_REVIEW_UP, &CMaintDlg::OnBnClickedBtReviewUp)
	ON_BN_CLICKED(IDC_BT_REVIEW_DN, &CMaintDlg::OnBnClickedBtReviewDn)
	
	ON_BN_CLICKED(IDC_BT_STICK_JIG_IN, &CMaintDlg::OnBnClickedBtStickJigIn)
	ON_BN_CLICKED(IDC_BT_STICK_JIG_OUT, &CMaintDlg::OnBnClickedBtStickJigOut)
END_MESSAGE_MAP()


// CMaintDlg 메시지 처리기입니다.


BOOL CMaintDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMaintDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;

	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btScope_UpDown_Standby.GetTextColor(&tColor);
		m_btScope_UpDown_Standby.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btScope_UpDown_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btScope_UpDown_Standby.SetTextColor(&tColor);
		m_btScope_UpDown_Standby.SetCheckButton(true, true);
		m_btScope_UpDown_Standby.SetFont(&tFont);

		m_btScope_UpDown_Measur.SetRoundButtonStyle(&m_tButtonStyle);
		m_btScope_UpDown_Measur.SetTextColor(&tColor);
		m_btScope_UpDown_Measur.SetCheckButton(true, true);
		m_btScope_UpDown_Measur.SetFont(&tFont);

		//m_btScope_UpDown_ThetaAlign.SetRoundButtonStyle(&m_tButtonStyle);
		//m_btScope_UpDown_ThetaAlign.SetTextColor(&tColor);
		//m_btScope_UpDown_ThetaAlign.SetCheckButton(true, true);
		//m_btScope_UpDown_ThetaAlign.SetFont(&tFont);

		//m_btScope_UpDown_Area3D_Measure.SetRoundButtonStyle(&m_tButtonStyle);
		//m_btScope_UpDown_Area3D_Measure.SetTextColor(&tColor);
		//m_btScope_UpDown_Area3D_Measure.SetCheckButton(true, true);
		//m_btScope_UpDown_Area3D_Measure.SetFont(&tFont);
		

		m_btScope_ShiftDrive_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btScope_ShiftDrive_Standby.SetTextColor(&tColor);
		m_btScope_ShiftDrive_Standby.SetCheckButton(true, true);
		m_btScope_ShiftDrive_Standby.SetFont(&tFont);

		m_btReview_UpDown_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReview_UpDown_Standby.SetTextColor(&tColor);
		m_btReview_UpDown_Standby.SetCheckButton(true, true);
		m_btReview_UpDown_Standby.SetFont(&tFont);

		m_btReview_UpDown_Review.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReview_UpDown_Review.SetTextColor(&tColor);
		m_btReview_UpDown_Review.SetCheckButton(true, true);
		m_btReview_UpDown_Review.SetFont(&tFont);

		m_btReview_UpDown_Inspection.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReview_UpDown_Inspection.SetTextColor(&tColor);
		m_btReview_UpDown_Inspection.SetCheckButton(true, true);
		m_btReview_UpDown_Inspection.SetFont(&tFont);

		m_btReview_ShiftDrive_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReview_ShiftDrive_Standby.SetTextColor(&tColor);
		m_btReview_ShiftDrive_Standby.SetCheckButton(true, true);
		m_btReview_ShiftDrive_Standby.SetFont(&tFont);

		m_btSpace_UpDown_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSpace_UpDown_Standby.SetTextColor(&tColor);
		m_btSpace_UpDown_Standby.SetCheckButton(true, true);
		m_btSpace_UpDown_Standby.SetFont(&tFont);

		m_btSpace_UpDown_Measure.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSpace_UpDown_Measure.SetTextColor(&tColor);
		m_btSpace_UpDown_Measure.SetCheckButton(true, true);
		m_btSpace_UpDown_Measure.SetFont(&tFont);

		m_btGripper_UpDown_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripper_UpDown_Standby.SetTextColor(&tColor);
		m_btGripper_UpDown_Standby.SetCheckButton(true, true);
		m_btGripper_UpDown_Standby.SetFont(&tFont);

		m_btGripper_UpDown_GripStandby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripper_UpDown_GripStandby.SetTextColor(&tColor);
		m_btGripper_UpDown_GripStandby.SetCheckButton(true, true);
		m_btGripper_UpDown_GripStandby.SetFont(&tFont);

		m_btGripper_UpDown_Grip.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripper_UpDown_Grip.SetTextColor(&tColor);
		m_btGripper_UpDown_Grip.SetCheckButton(true, true);
		m_btGripper_UpDown_Grip.SetFont(&tFont);

		m_btGripper_UpDown_Measure.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripper_UpDown_Measure.SetTextColor(&tColor);
		m_btGripper_UpDown_Measure.SetCheckButton(true, true);
		m_btGripper_UpDown_Measure.SetFont(&tFont);

		m_btGripper_UpDown_TT01OUT.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripper_UpDown_TT01OUT.SetTextColor(&tColor);
		m_btGripper_UpDown_TT01OUT.SetCheckButton(true, true);
		m_btGripper_UpDown_TT01OUT.SetFont(&tFont);

		m_btTension_FBward_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btTension_FBward_Standby.SetTextColor(&tColor);
		m_btTension_FBward_Standby.SetCheckButton(true, true);
		m_btTension_FBward_Standby.SetFont(&tFont);

		m_btTension_FBward_TensionStandby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btTension_FBward_TensionStandby.SetTextColor(&tColor);
		m_btTension_FBward_TensionStandby.SetCheckButton(true, true);
		m_btTension_FBward_TensionStandby.SetFont(&tFont);

		m_btTension_FBward_TensionGrip.SetRoundButtonStyle(&m_tButtonStyle);
		m_btTension_FBward_TensionGrip.SetTextColor(&tColor);
		m_btTension_FBward_TensionGrip.SetCheckButton(true, true);
		m_btTension_FBward_TensionGrip.SetFont(&tFont);


		m_btTensionCylinder_FWD.SetRoundButtonStyle(&m_tButtonStyle);
		m_btTensionCylinder_FWD.SetTextColor(&tColor);
		m_btTensionCylinder_FWD.SetCheckButton(true, true);
		m_btTensionCylinder_FWD.SetFont(&tFont);

		m_btTensionCylinder_BWD.SetRoundButtonStyle(&m_tButtonStyle);
		m_btTensionCylinder_BWD.SetTextColor(&tColor);
		m_btTensionCylinder_BWD.SetCheckButton(true, true);
		m_btTensionCylinder_BWD.SetFont(&tFont);

		m_btTensionCylinder_GripClose.SetRoundButtonStyle(&m_tButtonStyle);
		m_btTensionCylinder_GripClose.SetTextColor(&tColor);
		m_btTensionCylinder_GripClose.SetCheckButton(true, true);
		m_btTensionCylinder_GripClose.SetFont(&tFont);

		m_btTensionCylinder_GripOpen.SetRoundButtonStyle(&m_tButtonStyle);
		m_btTensionCylinder_GripOpen.SetTextColor(&tColor);
		m_btTensionCylinder_GripOpen.SetCheckButton(true, true);
		m_btTensionCylinder_GripOpen.SetFont(&tFont);

		m_btReviewMulti_Move.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReviewMulti_Move.SetTextColor(&tColor);
		m_btReviewMulti_Move.SetCheckButton(true, true);
		m_btReviewMulti_Move.SetFont(&tFont);

		m_btReview_ShiftStep_Move.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReview_ShiftStep_Move.SetTextColor(&tColor);
		m_btReview_ShiftStep_Move.SetCheckButton(true, true);
		m_btReview_ShiftStep_Move.SetFont(&tFont);

		m_btReview_DriveStep_Move.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReview_DriveStep_Move.SetTextColor(&tColor);
		m_btReview_DriveStep_Move.SetCheckButton(true, true);
		m_btReview_DriveStep_Move.SetFont(&tFont);

		m_btScopeMulti_Move.SetRoundButtonStyle(&m_tButtonStyle);
		m_btScopeMulti_Move.SetTextColor(&tColor);
		m_btScopeMulti_Move.SetCheckButton(true, true);
		m_btScopeMulti_Move.SetFont(&tFont);

		m_btScope_ShiftStep_Move.SetRoundButtonStyle(&m_tButtonStyle);
		m_btScope_ShiftStep_Move.SetTextColor(&tColor);
		m_btScope_ShiftStep_Move.SetCheckButton(true, true);
		m_btScope_ShiftStep_Move.SetFont(&tFont);

		m_btScope_DriveStep_Move.SetRoundButtonStyle(&m_tButtonStyle);
		m_btScope_DriveStep_Move.SetTextColor(&tColor);
		m_btScope_DriveStep_Move.SetCheckButton(true, true);
		m_btScope_DriveStep_Move.SetFont(&tFont);

		m_btReviewActionStop.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReviewActionStop.SetTextColor(&tColor);
		m_btReviewActionStop.SetCheckButton(true, true);
		m_btReviewActionStop.SetFont(&tFont);

		m_btScopeActionStop.SetRoundButtonStyle(&m_tButtonStyle);
		m_btScopeActionStop.SetTextColor(&tColor);
		m_btScopeActionStop.SetCheckButton(true, true);
		m_btScopeActionStop.SetFont(&tFont);


		m_btLineScanSync_Move.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLineScanSync_Move.SetTextColor(&tColor);
		m_btLineScanSync_Move.SetCheckButton(true, true);
		m_btLineScanSync_Move.SetFont(&tFont);


		m_btLineScanStep_Shift.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLineScanStep_Shift.SetTextColor(&tColor);
		m_btLineScanStep_Shift.SetCheckButton(true, true);
		m_btLineScanStep_Shift.SetFont(&tFont);


		m_btLineScanStep_Drive.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLineScanStep_Drive.SetTextColor(&tColor);
		m_btLineScanStep_Drive.SetCheckButton(true, true);
		m_btLineScanStep_Drive.SetFont(&tFont);

		m_btLineScan_ActionStop.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLineScan_ActionStop.SetTextColor(&tColor);
		m_btLineScan_ActionStop.SetCheckButton(true, true);
		m_btLineScan_ActionStop.SetFont(&tFont);

		m_btExitMaint.SetRoundButtonStyle(&m_tButtonStyle);
		m_btExitMaint.SetTextColor(&tColor);
		m_btExitMaint.SetCheckButton(true, true);
		m_btExitMaint.SetFont(&tFont);

		m_btRingLight_Cylinder_Up.SetRoundButtonStyle(&m_tButtonStyle);
		m_btRingLight_Cylinder_Up.SetTextColor(&tColor);
		m_btRingLight_Cylinder_Up.SetCheckButton(true, true);
		m_btRingLight_Cylinder_Up.SetFont(&tFont);

		m_btRingLight_Cylinder_Down.SetRoundButtonStyle(&m_tButtonStyle);
		m_btRingLight_Cylinder_Down.SetTextColor(&tColor);
		m_btRingLight_Cylinder_Down.SetCheckButton(true, true);
		m_btRingLight_Cylinder_Down.SetFont(&tFont);

		m_btReview_Cylinder_Up.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReview_Cylinder_Up.SetTextColor(&tColor);
		m_btReview_Cylinder_Up.SetCheckButton(true, true);
		m_btReview_Cylinder_Up.SetFont(&tFont);

		m_btReview_Cylinder_Down.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReview_Cylinder_Down.SetTextColor(&tColor);
		m_btReview_Cylinder_Down.SetCheckButton(true, true);
		m_btReview_Cylinder_Down.SetFont(&tFont);


		m_btStickJig_Input.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStickJig_Input.SetTextColor(&tColor);
		m_btStickJig_Input.SetCheckButton(true, true);
		m_btStickJig_Input.SetFont(&tFont);

		m_btStickJig_Output.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStickJig_Output.SetTextColor(&tColor);
		m_btStickJig_Output.SetCheckButton(true, true);
		m_btStickJig_Output.SetFont(&tFont);
	}
	m_stGripperStickDetect_Left.SetFont(&G_SmallFont);
	m_stGripperStickDetect_Left.SetTextColor(RGB(0,0,0));
	m_stGripperStickDetect_Left.SetBkColor(G_Color_Off);

	m_stGripperStickDetect_Right.SetFont(&G_SmallFont);
	m_stGripperStickDetect_Right.SetTextColor(RGB(0,0,0));
	m_stGripperStickDetect_Right.SetBkColor(G_Color_Off);

	SetTimer(TIMER_MAINT_DLG_UPDATE, 300, 0);
}
void CMaintDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


BOOL CMaintDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


HBRUSH CMaintDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(IDC_ST_STICK_IN_AM01_1 == DLG_ID_Number ||
		IDC_ST_STICK_IN_AM01_2 == DLG_ID_Number)
		return hbr;

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 0)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CMaintDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(TIMER_MAINT_DLG_UPDATE == nIDEvent)// && this->IsWindowVisible() == TRUE)
	{
		//////////////////////////////////////////////////////////////////////////
		//Review Z Pos
		if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanUpDn] - m_pReviewPos->nScan_ZPos_Standby)<=PosCheckInpos &&
			m_btReview_UpDown_Standby.m_SettingFlag != 1)
		{
			m_btReview_UpDown_Standby.SetCheck(true);
			m_btReview_UpDown_Standby.m_SettingFlag = 1;
			m_btReview_UpDown_Review.SetCheck(false);
			m_btReview_UpDown_Review.m_SettingFlag = -1;
			m_btReview_UpDown_Inspection.SetCheck(false);
			m_btReview_UpDown_Inspection.m_SettingFlag = -1;

			m_pLiveViewObj->m_bSafetyPos[0] = true;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanUpDn] - m_pReviewPos->nScan_ZPos_Review)<=PosCheckInpos &&
			m_btReview_UpDown_Review.m_SettingFlag != 1)
		{
			m_btReview_UpDown_Standby.SetCheck(false);
			m_btReview_UpDown_Standby.m_SettingFlag = -1;
			m_btReview_UpDown_Review.SetCheck(true);
			m_btReview_UpDown_Review.m_SettingFlag = 1;
			m_btReview_UpDown_Inspection.SetCheck(false);
			m_btReview_UpDown_Inspection.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[0] = false;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanUpDn] - m_pReviewPos->nScan_ZPos_Inspection)<=PosCheckInpos &&
			m_btReview_UpDown_Inspection.m_SettingFlag != 1)
		{
			m_btReview_UpDown_Standby.SetCheck(false);
			m_btReview_UpDown_Standby.m_SettingFlag = -1;
			m_btReview_UpDown_Review.SetCheck(false);
			m_btReview_UpDown_Review.m_SettingFlag = -1;
			m_btReview_UpDown_Inspection.SetCheck(true);
			m_btReview_UpDown_Inspection.m_SettingFlag = 1;
			m_pLiveViewObj->m_bSafetyPos[0] = false;
		}
		else if(
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanUpDn] - m_pReviewPos->nScan_ZPos_Standby)>PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanUpDn] - m_pReviewPos->nScan_ZPos_Review)>PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanUpDn] - m_pReviewPos->nScan_ZPos_Inspection)>PosCheckInpos &&
			(m_btReview_UpDown_Standby.m_SettingFlag ==1 || 
			m_btReview_UpDown_Review.m_SettingFlag == 1 || 
			m_btReview_UpDown_Inspection.m_SettingFlag ==1))
		{
			m_btReview_UpDown_Standby.SetCheck(false);
			m_btReview_UpDown_Standby.m_SettingFlag = -1;
			m_btReview_UpDown_Review.SetCheck(false);
			m_btReview_UpDown_Review.m_SettingFlag = -1;
			m_btReview_UpDown_Inspection.SetCheck(false);
			m_btReview_UpDown_Inspection.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[0] = false;
		}
		//Scope Safety 체크
		m_pLiveViewObj->m_bScopeSafetyPos = G_MotInfo.m_pMotState->m_bScopeSafetyPos;
		//////////////////////////////////////////////////////////////////////////
		//Review X, Y Pos
		if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanShift] - m_pReviewPos->nScan_Standby_Shift)<=PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanDrive] - m_pReviewPos->nScan_Standby_Drive)<=PosCheckInpos &&
			m_btReview_ShiftDrive_Standby.m_SettingFlag != 1)
		{
			m_btReview_ShiftDrive_Standby.SetCheck(true);
			m_btReview_ShiftDrive_Standby.m_SettingFlag = 1;
		}
		else if(
			(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanShift] - m_pReviewPos->nScan_Standby_Shift)>PosCheckInpos ||
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanDrive] - m_pReviewPos->nScan_Standby_Drive)>PosCheckInpos) &&
			m_btReview_ShiftDrive_Standby.m_SettingFlag == 1)
		{
			m_btReview_ShiftDrive_Standby.SetCheck(false);
			m_btReview_ShiftDrive_Standby.m_SettingFlag = -1;
		}



		//////////////////////////////////////////////////////////////////////////
		//Scope Z Pos
		if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn] - m_pScopePos->nScope_ZPos_Standby)<=PosCheckInpos &&
			m_btScope_UpDown_Standby.m_SettingFlag != 1)
		{
			m_btScope_UpDown_Standby.SetCheck(true);
			m_btScope_UpDown_Standby.m_SettingFlag = 1;
			m_btScope_UpDown_Measur.SetCheck(false);
			m_btScope_UpDown_Measur.m_SettingFlag = -1;
			//m_btScope_UpDown_ThetaAlign.SetCheck(false);
			//m_btScope_UpDown_ThetaAlign.m_SettingFlag = -1;
			//m_btScope_UpDown_Area3D_Measure.SetCheck(false);
			//m_btScope_UpDown_Area3D_Measure.m_SettingFlag = -1;

			m_pLiveViewObj->m_bSafetyPos[1] = true;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn] - m_pScopePos->nScope_ZPos_Measure)<=PosCheckInpos &&
			m_btScope_UpDown_Measur.m_SettingFlag != 1)
		{
			m_btScope_UpDown_Standby.SetCheck(false);
			m_btScope_UpDown_Standby.m_SettingFlag = -1;
			m_btScope_UpDown_Measur.SetCheck(true);
			m_btScope_UpDown_Measur.m_SettingFlag = 1;
			//m_btScope_UpDown_ThetaAlign.SetCheck(false);
			//m_btScope_UpDown_ThetaAlign.m_SettingFlag = -1;
			//m_btScope_UpDown_Area3D_Measure.SetCheck(false);
			//m_btScope_UpDown_Area3D_Measure.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[1] = false;
		}
		//else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn] - m_pScopePos->nScope_ZPos_ThetaAlign)<=PosCheckInpos &&
		//	m_btScope_UpDown_ThetaAlign.m_SettingFlag != 1)
		//{
		//	m_btScope_UpDown_Standby.SetCheck(false);
		//	m_btScope_UpDown_Standby.m_SettingFlag = -1;
		//	m_btScope_UpDown_Measur.SetCheck(false);
		//	m_btScope_UpDown_Measur.m_SettingFlag = -1;
		//	//m_btScope_UpDown_ThetaAlign.SetCheck(true);
		//	//m_btScope_UpDown_ThetaAlign.m_SettingFlag = 1;

		//	m_btScope_UpDown_Area3D_Measure.SetCheck(false);
		//	m_btScope_UpDown_Area3D_Measure.m_SettingFlag = -1;
		//	m_pLiveViewObj->m_bSafetyPos[1] = false;
		//}
		//else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn] - m_pScopePos->nScope_ZPos_Area3DMeasure)<=PosCheckInpos /*&&
		//	m_btScope_UpDown_ThetaAlign.m_SettingFlag != 1*/)
		//{
		//	m_btScope_UpDown_Standby.SetCheck(false);
		//	m_btScope_UpDown_Standby.m_SettingFlag = -1;
		//	m_btScope_UpDown_Measur.SetCheck(false);
		//	m_btScope_UpDown_Measur.m_SettingFlag = -1;
		//	//m_btScope_UpDown_ThetaAlign.SetCheck(false);
		//	//m_btScope_UpDown_ThetaAlign.m_SettingFlag = -1;
		//	m_btScope_UpDown_Area3D_Measure.SetCheck(true);
		//	m_btScope_UpDown_Area3D_Measure.m_SettingFlag = 1;
		//	m_pLiveViewObj->m_bSafetyPos[1] = false;
		//}
		else if(
			(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn] - m_pScopePos->nScope_ZPos_Standby)>PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn] - m_pScopePos->nScope_ZPos_Measure)>PosCheckInpos //&&
			//abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn] - m_pScopePos->nScope_ZPos_ThetaAlign)>PosCheckInpos&&
			/*abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn] - m_pScopePos->nScope_ZPos_Area3DMeasure)>PosCheckInpos)*/) &&
			(m_btScope_UpDown_Standby.m_SettingFlag ==1 || 
			m_btScope_UpDown_Measur.m_SettingFlag == 1) /*|| 
			m_btScope_UpDown_ThetaAlign.m_SettingFlag ==1*//*||
			m_btScope_UpDown_Area3D_Measure.m_SettingFlag ==1*/)
		{
			m_btScope_UpDown_Standby.SetCheck(false);
			m_btScope_UpDown_Standby.m_SettingFlag = -1;
			m_btScope_UpDown_Measur.SetCheck(false);
			m_btScope_UpDown_Measur.m_SettingFlag = -1;
			//m_btScope_UpDown_ThetaAlign.SetCheck(false);
			//m_btScope_UpDown_ThetaAlign.m_SettingFlag = -1;
			//m_btScope_UpDown_Area3D_Measure.SetCheck(false);
			//m_btScope_UpDown_Area3D_Measure.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[1] = false;
		}

		//////////////////////////////////////////////////////////////////////////
		//Scope X, Y Pos
		if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeShift] - m_pScopePos->nScope_Standby_Shift)<=PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeDrive] - m_pScopePos->nScope_Standby_Drive)<=PosCheckInpos &&
			m_btScope_ShiftDrive_Standby.m_SettingFlag != 1)
		{
			m_btScope_ShiftDrive_Standby.SetCheck(true);
			m_btScope_ShiftDrive_Standby.m_SettingFlag = 1;

		}
		else if(
			(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeShift] - m_pScopePos->nScope_Standby_Shift)>PosCheckInpos ||
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeDrive] - m_pScopePos->nScope_Standby_Drive)>PosCheckInpos) &&
			m_btScope_ShiftDrive_Standby.m_SettingFlag == 1)
		{
			m_btScope_ShiftDrive_Standby.SetCheck(false);
			m_btScope_ShiftDrive_Standby.m_SettingFlag = -1;
		}

		//////////////////////////////////////////////////////////////////////////
		//Space Z Pos
		if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_SpaceSnUpDn] - m_pScopePos->nSpace_ZPos_Standby)<=PosCheckInpos &&
			m_btSpace_UpDown_Standby.m_SettingFlag != 1)
		{
			m_btSpace_UpDown_Standby.SetCheck(true);
			m_btSpace_UpDown_Standby.m_SettingFlag = 1;
			m_btSpace_UpDown_Measure.SetCheck(false);
			m_btSpace_UpDown_Measure.m_SettingFlag = -1;

			m_pLiveViewObj->m_bSafetyPos[2] = true;
			
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_SpaceSnUpDn] - m_pScopePos->nSpace_ZPos_Measure)<=PosCheckInpos &&
			m_btSpace_UpDown_Measure.m_SettingFlag != 1)
		{
			m_btSpace_UpDown_Standby.SetCheck(false);
			m_btSpace_UpDown_Standby.m_SettingFlag = -1;
			m_btSpace_UpDown_Measure.SetCheck(true);
			m_btSpace_UpDown_Measure.m_SettingFlag = 1;

			m_pLiveViewObj->m_bSafetyPos[2] = false;
		}
		else if((
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_SpaceSnUpDn] - m_pScopePos->nSpace_ZPos_Standby)>PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_SpaceSnUpDn] - m_pScopePos->nSpace_ZPos_Measure)>PosCheckInpos )&&
			(m_btSpace_UpDown_Standby.m_SettingFlag ==1 || 
			m_btSpace_UpDown_Measure.m_SettingFlag ==1))
		{
			m_btSpace_UpDown_Standby.SetCheck(false);
			m_btSpace_UpDown_Standby.m_SettingFlag = -1;
			m_btSpace_UpDown_Measure.SetCheck(false);
			m_btSpace_UpDown_Measure.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[2] = false;
		}


		//////////////////////////////////////////////////////////////////////////
		//Gripper Z Pos
		if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_Standby)<=PosCheckInpos &&
			m_btGripper_UpDown_Standby.m_SettingFlag != 1)
		{
			m_btGripper_UpDown_Standby.SetCheck(true);
			m_btGripper_UpDown_Standby.m_SettingFlag = 1;
			m_btGripper_UpDown_GripStandby.SetCheck(false);
			m_btGripper_UpDown_GripStandby.m_SettingFlag = -1;
			m_btGripper_UpDown_Grip.SetCheck(false);
			m_btGripper_UpDown_Grip.m_SettingFlag = -1;

			m_btGripper_UpDown_Measure.SetCheck(false);
			m_btGripper_UpDown_Measure.m_SettingFlag = -1;
			m_btGripper_UpDown_TT01OUT.SetCheck(false);
			m_btGripper_UpDown_TT01OUT.m_SettingFlag = -1;

			m_pLiveViewObj->m_bSafetyPos[3] = true;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_Grip_Standby)<=PosCheckInpos &&
			m_btGripper_UpDown_GripStandby.m_SettingFlag != 1)
		{
			m_btGripper_UpDown_Standby.SetCheck(false);
			m_btGripper_UpDown_Standby.m_SettingFlag = -1;
			m_btGripper_UpDown_GripStandby.SetCheck(true);
			m_btGripper_UpDown_GripStandby.m_SettingFlag = 1;
			m_btGripper_UpDown_Grip.SetCheck(false);
			m_btGripper_UpDown_Grip.m_SettingFlag = -1;

			m_btGripper_UpDown_Measure.SetCheck(false);
			m_btGripper_UpDown_Measure.m_SettingFlag = -1;
			m_btGripper_UpDown_TT01OUT.SetCheck(false);
			m_btGripper_UpDown_TT01OUT.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[3] = false;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_Grip)<=PosCheckInpos &&
			m_btGripper_UpDown_Grip.m_SettingFlag != 1)
		{
			m_btGripper_UpDown_Standby.SetCheck(false);
			m_btGripper_UpDown_Standby.m_SettingFlag = -1;
			m_btGripper_UpDown_GripStandby.SetCheck(false);
			m_btGripper_UpDown_GripStandby.m_SettingFlag = -1;
			m_btGripper_UpDown_Grip.SetCheck(true);
			m_btGripper_UpDown_Grip.m_SettingFlag = 1;

			m_btGripper_UpDown_Measure.SetCheck(false);
			m_btGripper_UpDown_Measure.m_SettingFlag = -1;
			m_btGripper_UpDown_TT01OUT.SetCheck(false);
			m_btGripper_UpDown_TT01OUT.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[3] = false;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_Measure)<=PosCheckInpos &&
			m_btGripper_UpDown_Measure.m_SettingFlag != 1)
		{
			m_btGripper_UpDown_Standby.SetCheck(false);
			m_btGripper_UpDown_Standby.m_SettingFlag = -1;
			m_btGripper_UpDown_GripStandby.SetCheck(false);
			m_btGripper_UpDown_GripStandby.m_SettingFlag = -1;
			m_btGripper_UpDown_Grip.SetCheck(false);
			m_btGripper_UpDown_Grip.m_SettingFlag = -1;

			m_btGripper_UpDown_Measure.SetCheck(true);
			m_btGripper_UpDown_Measure.m_SettingFlag = 1;
			m_btGripper_UpDown_TT01OUT.SetCheck(false);
			m_btGripper_UpDown_TT01OUT.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[3] = false;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_TT01Out)<=PosCheckInpos &&
			m_btGripper_UpDown_TT01OUT.m_SettingFlag != 1)
		{
			m_btGripper_UpDown_Standby.SetCheck(false);
			m_btGripper_UpDown_Standby.m_SettingFlag = -1;
			m_btGripper_UpDown_GripStandby.SetCheck(false);
			m_btGripper_UpDown_GripStandby.m_SettingFlag = -1;
			m_btGripper_UpDown_Grip.SetCheck(false);
			m_btGripper_UpDown_Grip.m_SettingFlag = -1;

			m_btGripper_UpDown_Measure.SetCheck(false);
			m_btGripper_UpDown_Measure.m_SettingFlag = -1;
			m_btGripper_UpDown_TT01OUT.SetCheck(true);
			m_btGripper_UpDown_TT01OUT.m_SettingFlag = 1;
			m_pLiveViewObj->m_bSafetyPos[3] = false;
		}
		else if((
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_Standby)>PosCheckInpos&&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_Grip_Standby)>PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_Grip)>PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_Measure)>PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_TT01Out)>PosCheckInpos) &&
			(m_btScope_UpDown_Standby.m_SettingFlag ==1 || 
			m_btGripper_UpDown_GripStandby.m_SettingFlag == 1 || 
			m_btGripper_UpDown_Grip.m_SettingFlag == 1 || 
			m_btGripper_UpDown_Measure.m_SettingFlag == 1 || 
			m_btGripper_UpDown_TT01OUT.m_SettingFlag ==1))
		{
			m_btGripper_UpDown_Standby.SetCheck(false);
			m_btGripper_UpDown_Standby.m_SettingFlag = -1;
			m_btGripper_UpDown_GripStandby.SetCheck(false);
			m_btGripper_UpDown_GripStandby.m_SettingFlag = -1;
			m_btGripper_UpDown_Grip.SetCheck(false);
			m_btGripper_UpDown_Grip.m_SettingFlag = -1;
			m_btGripper_UpDown_Measure.SetCheck(false);
			m_btGripper_UpDown_Measure.m_SettingFlag = -1;
			m_btGripper_UpDown_TT01OUT.SetCheck(false);
			m_btGripper_UpDown_TT01OUT.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[3] = false;
		}

		//////////////////////////////////////////////////////////////////////////
		//Tension Forward/Backward Pos
		if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_LTension1] - m_pGripperPos->nGripper_TPos_Standby)<=PosCheckInpos &&
			m_btTension_FBward_Standby.m_SettingFlag != 1)
		{
			m_btTension_FBward_Standby.SetCheck(true);
			m_btTension_FBward_Standby.m_SettingFlag = 1;
			m_btTension_FBward_TensionStandby.SetCheck(false);
			m_btTension_FBward_TensionStandby.m_SettingFlag = -1;
			m_btTension_FBward_TensionGrip.SetCheck(false);
			m_btTension_FBward_TensionGrip.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[4] = true;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_LTension1] - m_pGripperPos->nGripper_TPos_TensionStandby)<=PosCheckInpos &&
			m_btTension_FBward_TensionStandby.m_SettingFlag != 1)
		{
			m_btTension_FBward_Standby.SetCheck(false);
			m_btTension_FBward_Standby.m_SettingFlag = -1;
			m_btTension_FBward_TensionStandby.SetCheck(true);
			m_btTension_FBward_TensionStandby.m_SettingFlag = 1;
			m_btTension_FBward_TensionGrip.SetCheck(false);
			m_btTension_FBward_TensionGrip.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[4] = false;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_LTension1] - m_pGripperPos->nGripper_TPos_Grip)<=PosCheckInpos &&
			m_btTension_FBward_TensionGrip.m_SettingFlag != 1)
		{
			m_btTension_FBward_Standby.SetCheck(false);
			m_btTension_FBward_Standby.m_SettingFlag = -1;
			m_btTension_FBward_TensionStandby.SetCheck(false);
			m_btTension_FBward_TensionStandby.m_SettingFlag = -1;
			m_btTension_FBward_TensionGrip.SetCheck(true);
			m_btTension_FBward_TensionGrip.m_SettingFlag = 1;
			m_pLiveViewObj->m_bSafetyPos[4] = false;
		}
		else if(
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_LTension1] - m_pGripperPos->nGripper_TPos_Standby)<=PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_LTension1] - m_pGripperPos->nGripper_TPos_TensionStandby)<=PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_LTension1] - m_pGripperPos->nGripper_TPos_Grip)<=PosCheckInpos &&
			(m_btTension_FBward_Standby.m_SettingFlag ==1 || 
			m_btScope_UpDown_Measur.m_SettingFlag == 1 || 
			m_btTension_FBward_TensionGrip.m_SettingFlag ==1))
		{
			m_btTension_FBward_Standby.SetCheck(false);
			m_btTension_FBward_Standby.m_SettingFlag = -1;
			m_btTension_FBward_TensionStandby.SetCheck(false);
			m_btTension_FBward_TensionStandby.m_SettingFlag = -1;
			m_btTension_FBward_TensionGrip.SetCheck(false);
			m_btTension_FBward_TensionGrip.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[4] = false;
		}
		//////////////////////////////////////////////////////////////////////////
		//Tension Cylinder Forward/Backward
		int SolStateLeft = SOL_NONE;
		int SolStateRight = SOL_NONE;
		if((G_MotInfo.m_pMotState->stGripperStatus.In08_Sn_LeftTensionCylinderForward == 1 && G_MotInfo.m_pMotState->stGripperStatus.In09_Sn_LeftTensionCylinderBackward == 0))
			SolStateLeft = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In08_Sn_LeftTensionCylinderForward == 0 && G_MotInfo.m_pMotState->stGripperStatus.In09_Sn_LeftTensionCylinderBackward == 1))
			SolStateLeft = SOL_DOWN;
		else
			SolStateLeft = SOL_NONE;

		if((G_MotInfo.m_pMotState->stGripperStatus.In20_Sn_RightTensionCylinderForward == 1 && G_MotInfo.m_pMotState->stGripperStatus.In21_Sn_RightTensionCylinderBackward == 0) )
			SolStateRight = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In20_Sn_RightTensionCylinderForward == 0 && G_MotInfo.m_pMotState->stGripperStatus.In21_Sn_RightTensionCylinderBackward == 1))
			SolStateRight = SOL_DOWN;
		else
		{
			SolStateRight = SOL_NONE;
		}
		if(SolStateLeft == SolStateRight)
		{
			if(SolStateLeft == SOL_UP &&
				m_btTensionCylinder_FWD.m_SettingFlag != 1)
			{
				m_btTensionCylinder_FWD.SetCheck(true);
				m_btTensionCylinder_FWD.m_SettingFlag = 1;
				m_btTensionCylinder_BWD.SetCheck(false);
				m_btTensionCylinder_BWD.m_SettingFlag = -1;
				m_pLiveViewObj->m_bSafetyPos[5] = false;
			}
			else if(SolStateLeft == SOL_DOWN &&
				m_btTensionCylinder_BWD.m_SettingFlag != 1)
			{
				m_btTensionCylinder_FWD.SetCheck(false);
				m_btTensionCylinder_FWD.m_SettingFlag = -1;
				m_btTensionCylinder_BWD.SetCheck(true);
				m_btTensionCylinder_BWD.m_SettingFlag = 1;

				m_pLiveViewObj->m_bSafetyPos[5] = true;
			}
			//else if(m_btTensionCylinder_FWD.m_SettingFlag == 1 || m_btTensionCylinder_BWD.m_SettingFlag == 1)
			//{
			//	m_btTensionCylinder_FWD.SetCheck(false);
			//	m_btTensionCylinder_FWD.m_SettingFlag = -1;
			//	m_btTensionCylinder_BWD.SetCheck(false);
			//	m_btTensionCylinder_BWD.m_SettingFlag = -1;
			//	m_pLiveViewObj->m_bSafetyPos[5] = false;
			//}
		}
		else if(m_btTensionCylinder_FWD.m_SettingFlag == 1 || m_btTensionCylinder_BWD.m_SettingFlag == 1)
		{
			m_btTensionCylinder_FWD.SetCheck(false);
			m_btTensionCylinder_FWD.m_SettingFlag = -1;
			m_btTensionCylinder_BWD.SetCheck(false);
			m_btTensionCylinder_BWD.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[5] = false;
		}
		//////////////////////////////////////////////////////////////////////////
		//Stick Gripper
		int aSolStateLeft[4] = {SOL_NONE,SOL_NONE,SOL_NONE,SOL_NONE};
		int aSolStateRight[4] = {SOL_NONE,SOL_NONE,SOL_NONE,SOL_NONE};

		if((G_MotInfo.m_pMotState->stGripperStatus.In00_Sn_LeftOpenGripper1 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In01_Sn_LeftCloseGripper1 == 0) )
			aSolStateLeft[0] = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In00_Sn_LeftOpenGripper1 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In01_Sn_LeftCloseGripper1 == 1))
			aSolStateLeft[0] = SOL_DOWN;
		else
			aSolStateLeft[0] = SOL_NONE;

		if((G_MotInfo.m_pMotState->stGripperStatus.In12_Sn_RightOpenGripper1 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In13_Sn_RightCloseGripper1 == 0) )
			aSolStateRight[0] = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In12_Sn_RightOpenGripper1 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In13_Sn_RightCloseGripper1 == 1))
			aSolStateRight[0] = SOL_DOWN;
		else
			aSolStateRight[0] = SOL_NONE;


		if((G_MotInfo.m_pMotState->stGripperStatus.In02_Sn_LeftOpenGripper2 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In03_Sn_LeftCloseGripper2 == 0) )
			aSolStateLeft[1] = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In02_Sn_LeftOpenGripper2 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In03_Sn_LeftCloseGripper2 == 1))
			aSolStateLeft[1] = SOL_DOWN;
		else
			aSolStateLeft[1] = SOL_NONE;

		if((G_MotInfo.m_pMotState->stGripperStatus.In14_Sn_RightOpenGripper2 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In15_Sn_RightCloseGripper2 == 0) )
			aSolStateRight[1] = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In14_Sn_RightOpenGripper2 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In15_Sn_RightCloseGripper2 == 1))
			aSolStateRight[1] = SOL_DOWN;
		else
			aSolStateRight[1] = SOL_NONE;


		if((G_MotInfo.m_pMotState->stGripperStatus.In04_Sn_LeftOpenGripper3 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In05_Sn_LeftCloseGripper3 == 0) )
			aSolStateLeft[2] = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In04_Sn_LeftOpenGripper3 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In05_Sn_LeftCloseGripper3 == 1))
			aSolStateLeft[2] = SOL_DOWN;
		else
			aSolStateLeft[2] = SOL_NONE;

		if((G_MotInfo.m_pMotState->stGripperStatus.In16_Sn_RightOpenGripper3 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In17_Sn_RightCloseGripper3 == 0) )
			aSolStateRight[2] = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In16_Sn_RightOpenGripper3 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In17_Sn_RightCloseGripper3 == 1))
			aSolStateRight[2] = SOL_DOWN;
		else
			aSolStateRight[2] = SOL_NONE;

		if((G_MotInfo.m_pMotState->stGripperStatus.In06_Sn_LeftOpenGripper4 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In07_Sn_LeftCloseGripper4 == 0) )
			aSolStateLeft[3] = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In06_Sn_LeftOpenGripper4 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In07_Sn_LeftCloseGripper4 == 1))
			aSolStateLeft[3] = SOL_DOWN;
		else
			aSolStateLeft[3] = SOL_NONE;

		if((G_MotInfo.m_pMotState->stGripperStatus.In18_Sn_RightOpenGripper4 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In19_Sn_RightCloseGripper4 == 0) )
			aSolStateRight[3] = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In18_Sn_RightOpenGripper4 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In19_Sn_RightCloseGripper4 == 1))
			aSolStateRight[3] = SOL_DOWN;
		else
			aSolStateRight[3] = SOL_NONE;



		if(aSolStateLeft[0] == aSolStateRight[0] &&
			aSolStateLeft[1] == aSolStateRight[1] &&
			aSolStateLeft[2] == aSolStateRight[2] &&
			aSolStateLeft[3] == aSolStateRight[3])
		{
			if(aSolStateLeft[0] == SOL_UP &&
				m_btTensionCylinder_GripOpen.m_SettingFlag != 1)
			{
				m_btTensionCylinder_GripOpen.SetCheck(false);
				m_btTensionCylinder_GripOpen.m_SettingFlag = 1;
				m_btTensionCylinder_GripClose.SetCheck(true);
				m_btTensionCylinder_GripClose.m_SettingFlag = -1;
				m_pLiveViewObj->m_bSafetyPos[6] = false;
			}
			else if(aSolStateLeft[0] == SOL_DOWN &&
				m_btTensionCylinder_GripClose.m_SettingFlag != 1)
			{
				m_btTensionCylinder_GripOpen.SetCheck(true );
				m_btTensionCylinder_GripOpen.m_SettingFlag = -1;
				m_btTensionCylinder_GripClose.SetCheck(false);
				m_btTensionCylinder_GripClose.m_SettingFlag = 1;
				m_pLiveViewObj->m_bSafetyPos[6] = true;
			}
			//else if(m_btTensionCylinder_GripOpen.m_SettingFlag == 1 || m_btTensionCylinder_GripClose.m_SettingFlag == 1)
			//{
			//	m_btTensionCylinder_GripOpen.SetCheck(false);
			//	m_btTensionCylinder_GripOpen.m_SettingFlag = -1;
			//	m_btTensionCylinder_GripClose.SetCheck(false);
			//	m_btTensionCylinder_GripClose.m_SettingFlag = -1;
			//	m_pLiveViewObj->m_bSafetyPos[6] = false;
			//}
		}
		else if(m_btTensionCylinder_GripOpen.m_SettingFlag == 1 || m_btTensionCylinder_GripClose.m_SettingFlag == 1)
		{
			m_btTensionCylinder_GripOpen.SetCheck(false);
			m_btTensionCylinder_GripOpen.m_SettingFlag = -1;
			m_btTensionCylinder_GripClose.SetCheck(false);
			m_btTensionCylinder_GripClose.m_SettingFlag = -1;
			m_pLiveViewObj->m_bSafetyPos[6] = false;
		}


		if(m_stGripperStickDetect_Left.m_SettingFlag != G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection  && 
			G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 0)
		{
			m_stGripperStickDetect_Left.m_SettingFlag = G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection;

			m_stGripperStickDetect_Left.SetBkColor(G_Color_Off);
			m_stGripperStickDetect_Left.SetWindowText("Left");

			G_pCIMInterface->m_fnSendStickExistReport(FALSE);
		}
		if(m_stGripperStickDetect_Left.m_SettingFlag != G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection  && 
			G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1)
		{
			m_stGripperStickDetect_Left.m_SettingFlag = G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection;
			m_stGripperStickDetect_Left.SetBkColor(G_Color_On);
			m_stGripperStickDetect_Left.SetWindowText("Left");

			G_pCIMInterface->m_fnSendStickExistReport(TRUE);
		}

		if(m_stGripperStickDetect_Right.m_SettingFlag != G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection  && 
			G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 0)
		{
			m_stGripperStickDetect_Right.m_SettingFlag = G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection;

			m_stGripperStickDetect_Right.SetBkColor(G_Color_Off);
			m_stGripperStickDetect_Right.SetWindowText("Right");
		}
		if(m_stGripperStickDetect_Right.m_SettingFlag != G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection  && 
			G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1)
		{
			m_stGripperStickDetect_Right.m_SettingFlag = G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection;
			m_stGripperStickDetect_Right.SetBkColor(G_Color_On);
			m_stGripperStickDetect_Right.SetWindowText("Right");
		}


		//////////////////////////////////////////////////////////////////////////
		//Ring Light Cylinder Up/Down
		SolStateLeft = SOL_NONE;
		SolStateRight = SOL_NONE;
		if((G_MotInfo.m_pMotState->stEmissionTable.In11_Sn_RingLightCylinderUp == 1 && G_MotInfo.m_pMotState->stEmissionTable.In12_Sn_RingLightCylinderDown == 0))
			SolStateLeft = SOL_UP;
		else if((G_MotInfo.m_pMotState->stEmissionTable.In11_Sn_RingLightCylinderUp == 0 && G_MotInfo.m_pMotState->stEmissionTable.In12_Sn_RingLightCylinderDown == 1))
			SolStateLeft = SOL_DOWN;
		else
			SolStateLeft = SOL_NONE;

		//if(SolStateLeft == SolStateRight)
		{
			if(SolStateLeft == SOL_UP &&
				(m_btRingLight_Cylinder_Up.m_SettingFlag != SOL_UP||m_btRingLight_Cylinder_Down.m_SettingFlag!=SOL_UP))
			{
				m_btRingLight_Cylinder_Up.SetCheck(true);
				m_btRingLight_Cylinder_Up.m_SettingFlag = SOL_UP;
				m_btRingLight_Cylinder_Down.SetCheck(false);
				m_btRingLight_Cylinder_Down.m_SettingFlag = SOL_UP;

				if(m_pbUpRingLightCylinder != nullptr)
					*m_pbUpRingLightCylinder = true;
			}
			else if(SolStateLeft == SOL_DOWN &&
				(m_btRingLight_Cylinder_Up.m_SettingFlag != SOL_DOWN||m_btRingLight_Cylinder_Down.m_SettingFlag!=SOL_DOWN))
			{
				m_btRingLight_Cylinder_Up.SetCheck(false);
				m_btRingLight_Cylinder_Up.m_SettingFlag = SOL_DOWN;
				m_btRingLight_Cylinder_Down.SetCheck(true);
				m_btRingLight_Cylinder_Down.m_SettingFlag = SOL_DOWN;

				if(m_pbUpRingLightCylinder != nullptr)
					*m_pbUpRingLightCylinder = false;
			}
			else if(SolStateLeft == SOL_NONE&&
				(m_btRingLight_Cylinder_Up.m_SettingFlag != SOL_NONE||m_btRingLight_Cylinder_Down.m_SettingFlag!=SOL_NONE))
			{
				m_btRingLight_Cylinder_Up.SetCheck(false);
				m_btRingLight_Cylinder_Up.m_SettingFlag = SOL_NONE;
				m_btRingLight_Cylinder_Down.SetCheck(false);
				m_btRingLight_Cylinder_Down.m_SettingFlag = SOL_NONE;
				if(m_pbUpRingLightCylinder != nullptr)
					*m_pbUpRingLightCylinder = false;
			}
		}
		//////////////////////////////////////////////////////////////////////////
		//Review Cylinder Up/Down
		SolStateLeft = SOL_NONE;
		SolStateRight = SOL_NONE;
		if((G_MotInfo.m_pMotState->stEmissionTable.In13_Sn_ReviewCylinderUp == 1 && G_MotInfo.m_pMotState->stEmissionTable.In14_Sn_ReviewCylinderDown == 0))
			SolStateLeft = SOL_UP;
		else if((G_MotInfo.m_pMotState->stEmissionTable.In13_Sn_ReviewCylinderUp == 0 && G_MotInfo.m_pMotState->stEmissionTable.In14_Sn_ReviewCylinderDown == 1))
			SolStateLeft = SOL_DOWN;
		else
			SolStateLeft = SOL_NONE;

		//if(SolStateLeft == SolStateRight)
		{
			if(SolStateLeft == SOL_UP &&
				(m_btReview_Cylinder_Up.m_SettingFlag != SOL_UP||m_btReview_Cylinder_Down.m_SettingFlag!=SOL_UP))
			{
				m_btReview_Cylinder_Up.SetCheck(true);
				m_btReview_Cylinder_Up.m_SettingFlag = SOL_UP;
				m_btReview_Cylinder_Down.SetCheck(false);
				m_btReview_Cylinder_Down.m_SettingFlag = SOL_UP;

				if(m_pbUpReviewCylinder != nullptr)
					*m_pbUpReviewCylinder = true;
			}
			else if(SolStateLeft == SOL_DOWN &&
				(m_btReview_Cylinder_Up.m_SettingFlag != SOL_DOWN||m_btReview_Cylinder_Down.m_SettingFlag!=SOL_DOWN))
			{
				m_btReview_Cylinder_Up.SetCheck(false);
				m_btReview_Cylinder_Up.m_SettingFlag = SOL_DOWN;
				m_btReview_Cylinder_Down.SetCheck(true);
				m_btReview_Cylinder_Down.m_SettingFlag = SOL_DOWN;

				if(m_pbUpReviewCylinder != nullptr)
					*m_pbUpReviewCylinder = false;
			}
			else if(SolStateLeft == SOL_NONE&&
				(m_btReview_Cylinder_Up.m_SettingFlag != SOL_NONE||m_btReview_Cylinder_Down.m_SettingFlag!=SOL_NONE))
			{
				m_btReview_Cylinder_Up.SetCheck(false);
				m_btReview_Cylinder_Up.m_SettingFlag = SOL_NONE;
				m_btReview_Cylinder_Down.SetCheck(false);
				m_btReview_Cylinder_Down.m_SettingFlag = SOL_NONE;
				if(m_pbUpReviewCylinder != nullptr)
					*m_pbUpReviewCylinder = false;
			}
		}

		//////////////////////////////////////////////////////////////////////////
		//Jig In/Out
		if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_JigInOut] - m_pGripperPos->nStickJig_Standby)<=PosCheckInpos &&
			m_btStickJig_Output.m_SettingFlag != 1)
		{
			m_btStickJig_Output.SetCheck(true);
			m_btStickJig_Output.m_SettingFlag = 1;
			m_btStickJig_Input.SetCheck(false);
			m_btStickJig_Input.m_SettingFlag = -1;
		}
		else if( abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_JigInOut] - m_pGripperPos->nStickJig_Measure)<=PosCheckInpos &&
			m_btStickJig_Input.m_SettingFlag != 1)
		{
			m_btStickJig_Output.SetCheck(false);
			m_btStickJig_Output.m_SettingFlag = -1;
			m_btStickJig_Input.SetCheck(true);
			m_btStickJig_Input.m_SettingFlag = 1;
		}
		else if((
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_JigInOut] - m_pGripperPos->nStickJig_Standby)>PosCheckInpos &&
			abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_JigInOut] - m_pGripperPos->nStickJig_Measure)>PosCheckInpos )&&
			(m_btStickJig_Output.m_SettingFlag ==1 || 
			m_btStickJig_Input.m_SettingFlag ==1))
		{
			m_btStickJig_Output.SetCheck(false);
			m_btStickJig_Output.m_SettingFlag = -1;
			m_btStickJig_Input.SetCheck(false);
			m_btStickJig_Input.m_SettingFlag = -1;
		}
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CMaintDlg::OnBnClickedBtRingLightUp()
{
	G_MotObj.m_fnRingLightUp();
}
void CMaintDlg::OnBnClickedBtRingLightDn()
{
	G_MotObj.m_fnRingLightDown();
}
void CMaintDlg::OnBnClickedBtReviewUp()
{
	G_MotObj.m_fnReviewUp();
}
void CMaintDlg::OnBnClickedBtReviewDn()
{
	G_MotObj.m_fnReviewDown();
}
void CMaintDlg::OnBnClickedBtStandbyReviewUpdown()
{
	G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
}


void CMaintDlg::OnBnClickedBtReviewReviewUpdown()
{
	G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Review);
}


void CMaintDlg::OnBnClickedBtInspectionReviewUpdown()
{
	G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Inspection);
}


void CMaintDlg::OnBnClickedBtStandbyShiftDriveScan()
{
	if(m_btReview_UpDown_Standby.GetCheck() == true&&
		m_btSpace_UpDown_Standby.GetCheck() == true)
	{
		G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
	}
	else
	{
		AfxMessageBox("Scan Up/Down축이 안전 위치에 있어야 합니다.\r\n단차센서 Up/Down축이 안전 위치에 있어야 합니다.");
	}
}


void CMaintDlg::OnBnClickedBtStandbyScopeUpdown()
{
	G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
}


void CMaintDlg::OnBnClickedBtMeasureScopeUpdown()
{
	G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Measure);
}
void CMaintDlg::OnBnClickedBtArea3dMeasureScopeUpdown()
{
	//G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Area3DMeasure);
}


void CMaintDlg::OnBnClickedBtThetaAlignScopeUpdown()
{
	//G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_ThetaAlign);
}


void CMaintDlg::OnBnClickedBtStandbyShiftDriveScope()
{
	if(m_btScope_UpDown_Standby.GetCheck() == true)
	{
		G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
	}
	else
	{
		AfxMessageBox("Scope Up/Down축이 안전 위치에 있어야 합니다.");
	}
}


void CMaintDlg::OnBnClickedBtStandbySpaceUpdown()
{
	G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
}


void CMaintDlg::OnBnClickedBtMeasureSpaceUpdown()
{
	G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Measure);
}


void CMaintDlg::OnBnClickedBtStandbyTensionFbwd()
{
	if(m_stGripperStickDetect_Left.GetBkColor() == G_Color_Off && m_stGripperStickDetect_Right.GetBkColor() == G_Color_Off)
	{
		G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);
		G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);
	}
	else
	{
		AfxMessageBox("Gripper에 Stick이 감지 되었습니다.\r\nStick 제거 후 동작 하세요.");
	}
}


void CMaintDlg::OnBnClickedBtGripTensionFbwd()
{
	G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_TensionStandby);
	G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_TensionStandby);
}


void CMaintDlg::OnBnClickedBtGripStandbyTensionFbwd()
{
	G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Grip);
	G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Grip);
}


void CMaintDlg::OnBnClickedBtStandbyGripperUpdown()
{
	//Gripper가 열려 있으면 움직여도 된다.
	if(m_btTensionCylinder_GripClose.GetCheck() == true ||
		(m_stGripperStickDetect_Left.GetBkColor() == G_Color_Off && m_stGripperStickDetect_Right.GetBkColor() == G_Color_Off))
	{
		G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Standby);
	}
	else
	{
		AfxMessageBox("Gripper에 Stick이 감지 되었습니다.\r\nGripper를 Open하시거나\r\nStick 제거 후 동작 하세요.");
	}
}


void CMaintDlg::OnBnClickedBtGripStandbyGripperUpdown()
{
	//Gripper가 열려 있으면 움직여도 된다.
	if(m_btTensionCylinder_GripClose.GetCheck() == true ||
		(m_stGripperStickDetect_Left.GetBkColor() == G_Color_Off && m_stGripperStickDetect_Right.GetBkColor() == G_Color_Off))
	{
		G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip_Standby);
	}
	else
	{
		AfxMessageBox("Gripper에 Stick이 감지 되었습니다.\r\nGripper를 Open하시거나\r\nStick 제거 후 동작 하세요.");
	}
}


void CMaintDlg::OnBnClickedBtGripGripperUpdown()
{
	G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip);
}


void CMaintDlg::OnBnClickedBtMeasureGripperUpdown()
{
	G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Measure);
}


void CMaintDlg::OnBnClickedBtTt01outGripperUpdown()
{
	if(m_btScope_UpDown_Standby.GetCheck() == true)
	{
		G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_TT01Out);
	}
	else
	{
		AfxMessageBox("3D Scope Up/Down축이 안전 위치에 있어야 합니다.");
	}
}


void CMaintDlg::OnBnClickedBtTensionFwd()
{
	G_MotObj.m_fnStickTentionFWD();
}


void CMaintDlg::OnBnClickedBtTensionBwd()
{
	if(m_btTensionCylinder_GripClose.GetCheck() == true && m_btGripper_UpDown_GripStandby.GetCheck() == true)
	//if(m_stGripperStickDetect_Left.GetBkColor() == G_Color_Off && m_stGripperStickDetect_Right.GetBkColor() == G_Color_Off)
	{
		G_MotObj.m_fnStickTentionBWD();
	}
	else
	{
		AfxMessageBox("지출 동작을 수행 할 수 없습니다.\r\n<그립 대기위치 + 스틱 풀기> 상태에서만 동작 합니다.");
	}
}

void CMaintDlg::OnBnClickedBtGripperMaintClose()
{
	G_MotObj.m_fnStickGripperClose();
}


void CMaintDlg::OnBnClickedBtGripperMaintOpen()
{
	if(m_btScope_UpDown_Standby.GetCheck() == true && m_btScope_ShiftDrive_Standby.GetCheck() == true)
	{
		G_MotObj.m_fnStickGripperOpen();
	}
	else
	{
		AfxMessageBox("3D Scope Up/Down축이 안전 위치에 있어야 합니다.\r\n 3D Scope의 Shift, Drive가 안전위치에 있어야 합니다.");
	}
}



void CMaintDlg::OnBnClickedChSyncScope()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();


	switch(wID)
	{
	case IDC_CH_SYNC_SCOPE_BACK_LIGHT:
		{
			if( IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_BACK_LIGHT) == BST_CHECKED)
			{
				CheckDlgButton(IDC_CH_SYNC_SCOPE_CAM, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_AIRBLOW, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_AREA_SENSOR, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM, BST_UNCHECKED);
			}

		}break;
	case IDC_CH_SYNC_SCOPE_CAM:
		{
			if( IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_CAM) == BST_CHECKED)
			{
				CheckDlgButton(IDC_CH_SYNC_SCOPE_BACK_LIGHT, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_AIRBLOW, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_AREA_SENSOR, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM, BST_UNCHECKED);
			}
		}break;
	case IDC_CH_SYNC_SCOPE_AIRBLOW:
		{
			if( IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_AIRBLOW) == BST_CHECKED)
			{
				CheckDlgButton(IDC_CH_SYNC_SCOPE_BACK_LIGHT, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_CAM, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_AREA_SENSOR, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM, BST_UNCHECKED);
			}
		}break;
	case IDC_CH_SYNC_SCOPE_AREA_SENSOR:
		{
			if( IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_AREA_SENSOR) == BST_CHECKED)
			{
				CheckDlgButton(IDC_CH_SYNC_SCOPE_BACK_LIGHT, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_CAM, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_AIRBLOW, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM, BST_UNCHECKED);
			}
		}break;

	case IDC_CH_SYNC_SCOPE_THETA_ALING_CAM:
		{
			if( IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM) == BST_CHECKED)
			{
				CheckDlgButton(IDC_CH_SYNC_SCOPE_BACK_LIGHT, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_CAM, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_AIRBLOW, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCOPE_AREA_SENSOR, BST_UNCHECKED);
			}
		}break;
	default:
		{
			CheckDlgButton(IDC_CH_SYNC_SCOPE_BACK_LIGHT, BST_UNCHECKED);
			CheckDlgButton(IDC_CH_SYNC_SCOPE_CAM, BST_UNCHECKED);
			CheckDlgButton(IDC_CH_SYNC_SCOPE_AIRBLOW, BST_UNCHECKED);
			CheckDlgButton(IDC_CH_SYNC_SCOPE_AREA_SENSOR, BST_UNCHECKED);
			CheckDlgButton(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM, BST_UNCHECKED);
		}break;
	}
}

void CMaintDlg::OnBnClickedBtMoveScanXyMulti()
{
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_REVIEW_SHIFT)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;
	GetDlgItem(IDC_EDIT_NEW_POS_REVIEW_DRIVE)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int ScanNewPosShift = GetDlgItemInt(IDC_EDIT_NEW_POS_REVIEW_SHIFT);//Editer가 Number 설정 양수만 들어 온다.
	int ScanNewPosDrive = GetDlgItemInt(IDC_EDIT_NEW_POS_REVIEW_DRIVE);

	if( IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_BACK_LIGHT) == BST_CHECKED||
		IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_CAM) == BST_CHECKED||
		IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_AIRBLOW) == BST_CHECKED||
		IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_AREA_SENSOR) == BST_CHECKED||
		IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM) == BST_CHECKED)
	{//Scan to Scope Sync Move.

		SyncScope synScopeID = Sync_Scope_3DScope;
		bool syncCheck = true;
		if(IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_BACK_LIGHT) == BST_CHECKED)
			synScopeID = Sync_Scope_BackLight;
		else if(IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_CAM) == BST_CHECKED)
			synScopeID = Sync_Scope_3DScope;
		else if(IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_AIRBLOW) == BST_CHECKED)
			synScopeID = Sync_Scope_AirBlow;
		else if(IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_AREA_SENSOR) == BST_CHECKED)
			synScopeID = Sync_Scope_ScanSensor;
		else if(IsDlgButtonChecked(IDC_CH_SYNC_SCOPE_THETA_ALING_CAM) == BST_CHECKED)
			synScopeID = Sync_Scope_ThetaAlignCam;
		else
		{
			syncCheck = false;
		}
		if(syncCheck == true)
			G_MotObj.m_fnAM01ReviewSyncMove(ScanNewPosShift, ScanNewPosDrive, synScopeID);
		else
			G_MotObj.m_fnScanMultiMove(ScanNewPosShift, ScanNewPosDrive);
	}
	else
	{//Scan Multi Move
		G_MotObj.m_fnScanMultiMove(ScanNewPosShift, ScanNewPosDrive);
	}
}

void CMaintDlg::OnBnClickedBtMoveScanShiftX()
{
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_REVIEW_OFFSET_SHIFT)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int ScanNewPosShift =G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanShift] + 
		GetDlgItemInt(IDC_EDIT_NEW_POS_REVIEW_OFFSET_SHIFT);
	G_MotObj.m_fnScanShiftMove(ScanNewPosShift);
}


void CMaintDlg::OnBnClickedBtMoveScanDriveY()
{
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_REVIEW_OFFSET_DRIVE)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int ScanNewPosDrive =G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanDrive] + 
		GetDlgItemInt(IDC_EDIT_NEW_POS_REVIEW_OFFSET_DRIVE);
	G_MotObj.m_fnScanDriveMove(ScanNewPosDrive);
}
void CMaintDlg::OnBnClickedBtStopScanXy()
{
	//G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScanDrive);
	//G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScanShift);
	//G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_MultiMoveScan);
	//G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_SyncMoveScanScofe);

	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScopeDrive);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScopeShift);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScanDrive);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScanShift);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_MultiMoveScan);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_SyncMoveScanScofe);

	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_JigInout);
}

void CMaintDlg::OnBnClickedChSyncScan()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();


	switch(wID)
	{
	case IDC_CH_SYNC_SCAN_REVIEW_CAM:
		{
			if( IsDlgButtonChecked(IDC_CH_SYNC_SCAN_REVIEW_CAM) == BST_CHECKED)
			{
				CheckDlgButton(IDC_CH_SYNC_SCAN_SPACE_SENSOR, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCAN_LINE_SCAN_CAM, BST_UNCHECKED);
			}
		}break;
	case IDC_CH_SYNC_SCAN_SPACE_SENSOR:
		{
			if( IsDlgButtonChecked(IDC_CH_SYNC_SCAN_SPACE_SENSOR) == BST_CHECKED)
			{
				CheckDlgButton(IDC_CH_SYNC_SCAN_REVIEW_CAM, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCAN_LINE_SCAN_CAM, BST_UNCHECKED);
			}
		}break;
	case IDC_CH_SYNC_SCAN_LINE_SCAN_CAM:
		{
			if( IsDlgButtonChecked(IDC_CH_SYNC_SCAN_LINE_SCAN_CAM) == BST_CHECKED)
			{
				CheckDlgButton(IDC_CH_SYNC_SCAN_REVIEW_CAM, BST_UNCHECKED);
				CheckDlgButton(IDC_CH_SYNC_SCAN_SPACE_SENSOR, BST_UNCHECKED);
			}
		}break;

	default:
		{
			CheckDlgButton(IDC_CH_SYNC_SCAN_REVIEW_CAM, BST_UNCHECKED);
			CheckDlgButton(IDC_CH_SYNC_SCAN_SPACE_SENSOR, BST_UNCHECKED);
			CheckDlgButton(IDC_CH_SYNC_SCAN_LINE_SCAN_CAM, BST_UNCHECKED);
		}break;
	}
}

void CMaintDlg::OnBnClickedBtMoveScopeXyMulti()
{
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_SCOPE_SHIFT)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;
	GetDlgItem(IDC_EDIT_NEW_POS_SCOPE_DRIVE)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int ScopeNewPosShift = GetDlgItemInt(IDC_EDIT_NEW_POS_SCOPE_SHIFT);//Editer가 Number 설정 양수만 들어 온다.
	int ScopeNewPosDrive = GetDlgItemInt(IDC_EDIT_NEW_POS_SCOPE_DRIVE);

	if( IsDlgButtonChecked(IDC_CH_SYNC_SCAN_REVIEW_CAM) == BST_CHECKED||
		IsDlgButtonChecked(IDC_CH_SYNC_SCAN_SPACE_SENSOR) == BST_CHECKED||
		IsDlgButtonChecked(IDC_CH_SYNC_SCAN_LINE_SCAN_CAM) == BST_CHECKED)
	{//Scan to Scope Sync Move.

		SyncScan synScanID = Sync_Scan_Review;
		bool syncCheck = true;
		if(IsDlgButtonChecked(IDC_CH_SYNC_SCAN_REVIEW_CAM) == BST_CHECKED)
			synScanID = Sync_Scan_Review;
		else if(IsDlgButtonChecked(IDC_CH_SYNC_SCAN_SPACE_SENSOR) == BST_CHECKED)
			synScanID = Sync_Scan_SpaceSensor;
		else if(IsDlgButtonChecked(IDC_CH_SYNC_SCAN_LINE_SCAN_CAM) == BST_CHECKED)
			synScanID = Sync_Scan_LineScan;
		else
		{
			syncCheck = false;
		}
		if(syncCheck == true)
			G_MotObj.m_fnAM01ScopeSyncMove(ScopeNewPosShift, ScopeNewPosDrive, synScanID);
		else
			G_MotObj.m_fnScopeMultiMove(ScopeNewPosShift, ScopeNewPosDrive);
	}
	else
	{//Scan Multi Move
		G_MotObj.m_fnScopeMultiMove(ScopeNewPosShift, ScopeNewPosDrive);
	}
}


void CMaintDlg::OnBnClickedBtMoveScopeShiftX()
{
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_SCOPE_OFFSET_SHIFT)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int ScanNewPosShift =G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeShift] + 
		GetDlgItemInt(IDC_EDIT_NEW_POS_REVIEW_OFFSET_SHIFT);
	G_MotObj.m_fnScopeShiftMove(ScanNewPosShift);
}


void CMaintDlg::OnBnClickedBtMoveScopeDriveY()
{
	//
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_SCOPE_OFFSET_DRIVE)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int ScanNewPosDrive =G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeDrive] + 
		GetDlgItemInt(IDC_EDIT_NEW_POS_REVIEW_OFFSET_DRIVE);
	G_MotObj.m_fnScopeDriveMove(ScanNewPosDrive);
}


void CMaintDlg::OnBnClickedBtStopScopeXy()
{
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScopeDrive);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScopeShift);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_MultiMoveScofe);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_SyncMoveScanScofe);
}


void CMaintDlg::OnBnClickedBtExitMaint()
{
	m_pLiveViewObj->m_bDisplayInterlock =  true;
	this->ShowWindow(SW_HIDE);
}


void CMaintDlg::OnBnClickedBtMoveLscanXyMulti()
{
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_LSCAN_SHIFT)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;
	GetDlgItem(IDC_EDIT_NEW_POS_LSCAN_DRIVE)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int LineScanNewPosShift = GetDlgItemInt(IDC_EDIT_NEW_POS_LSCAN_SHIFT);//Editer가 Number 설정 양수만 들어 온다.
	int LineScanNewPosDrive = GetDlgItemInt(IDC_EDIT_NEW_POS_LSCAN_DRIVE);

	//Scan to Scope Sync Move.
	LineScanNewPosShift = LineScanNewPosShift+m_pCamToObjDis->nReviewCamTo_LineScanX;
	LineScanNewPosDrive = LineScanNewPosDrive+m_pCamToObjDis->nReviewCamTo_LineScanY;

	//m_fnAM01LineScanSyncMove는 Back Light가 Scan Cam을 따라 다닌다.
	G_MotObj.m_fnAM01LineScanSyncMove(LineScanNewPosShift, LineScanNewPosDrive);

}


void CMaintDlg::OnBnClickedBtMoveLscanShiftX()
{
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_LSCAN_OFFSET_SHIFT)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int ScanNewPosShift =G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeShift] + 
		GetDlgItemInt(IDC_EDIT_NEW_POS_LSCAN_OFFSET_SHIFT);
	G_MotObj.m_fnScopeShiftMove(ScanNewPosShift);
}


void CMaintDlg::OnBnClickedBtMoveLscanDriveY()
{
	CString strNewPos;
	GetDlgItem(IDC_EDIT_NEW_POS_LSCAN_OFFSET_DRIVE)->GetWindowText(strNewPos);
	if(strNewPos.GetLength()==0)
		return;

	int ScanNewPosDrive =G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeDrive] + 
		GetDlgItemInt(IDC_EDIT_NEW_POS_LSCAN_OFFSET_DRIVE);
	G_MotObj.m_fnScopeDriveMove(ScanNewPosDrive);
}


void CMaintDlg::OnBnClickedBtStopLscanXy()
{
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScanDrive);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_ScanShift);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_MultiMoveScan);
	G_MotObj.m_fnAM01ActionStop( nBiz_Seq_AM01_SyncMoveScanScofe);
}


void CMaintDlg::OnBnClickedBtStickJigIn()
{
	if(G_MotObj.m_fnStickJigMove(m_pGripperPos->nStickJig_Measure) == false)
	{
		CString strMsg;
		strMsg.Format("");
		strMsg.AppendFormat("Jig In/Out조건을 확인 하세요\r\n");
		strMsg.AppendFormat("   1)스틱이 감지상태이면 Gripper Z는 TT01_Out위치이어야 한다.\r\n");
		strMsg.AppendFormat("   2)스틱이 비감지상태이면 최소한 Gripper Sol이 BWD인지 체크 하자.\r\n");
		strMsg.AppendFormat("   3)TT01위치는 UT02이어야 한다.\r\n");
		strMsg.AppendFormat("   4)Review Sol Down\r\n");
		strMsg.AppendFormat("   5)Ring Light Sol Down\r\n");
		strMsg.AppendFormat("   6)Review(Inspect) Z대기 위치.\r\n");
		strMsg.AppendFormat("   7)Space Sensor Z대기 위치.\r\n");
		strMsg.AppendFormat("   8)Scope Z대기 위치.\r\n");
		strMsg.AppendFormat("   9)Scan Drive, Shift대기 위치.\r\n");
		strMsg.AppendFormat("   10)Scope Drive, Shift대기 위치.\r\n");
		AfxMessageBox(strMsg);
	}
}


void CMaintDlg::OnBnClickedBtStickJigOut()
{
	if(G_MotObj.m_fnStickJigMove(m_pGripperPos->nStickJig_Standby) == false)
	{
		CString strMsg;
		strMsg.Format("");
		strMsg.AppendFormat("Jig In/Out조건을 확인 하세요\r\n");
		strMsg.AppendFormat("   1)스틱이 감지상태이면 Gripper Z는 TT01_Out위치이어야 한다.\r\n");
		strMsg.AppendFormat("   2)스틱이 비감지상태이면 최소한 Gripper Sol이 BWD인지 체크 하자.\r\n");
		strMsg.AppendFormat("   3)TT01위치는 UT02이어야 한다.\r\n");
		strMsg.AppendFormat("   4)Review Sol Down\r\n");
		strMsg.AppendFormat("   5)Ring Light Sol Down\r\n");
		strMsg.AppendFormat("   6)Review(Inspect) Z대기 위치.\r\n");
		strMsg.AppendFormat("   7)Space Sensor Z대기 위치.\r\n");
		strMsg.AppendFormat("   8)Scope Z대기 위치.\r\n");
		strMsg.AppendFormat("   9)Scan Drive, Shift대기 위치.\r\n");
		strMsg.AppendFormat("   10)Scope Drive, Shift대기 위치.\r\n");
		AfxMessageBox(strMsg);
	}
}
