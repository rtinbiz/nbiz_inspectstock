#pragma once


// CPMDlg 대화 상자입니다.

class CPMDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPMDlg)

public:
	CPMDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPMDlg();

	CvvImage cvPMImage;

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_PM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	SubDlg_CIMInterface		*  m_pSubCIMInterfaceView;
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	virtual void PostNcDestroy();

	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btnUnLockPM;
	CRoundButton2 m_btnPMMaint;
	CRoundButton2 m_btnPMStage;

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedMaint();
	afx_msg void OnBnClickedStage();
};