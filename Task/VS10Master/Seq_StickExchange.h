#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
#include "Seq_ThetaAlign.h"

class CSeq_StickExchange: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		//Stick Loading Seq
		SEQ_STEP_STICK_LOADING,
		SEQ_STEP_Stick_Information,
			SEQ_STEP_SPACE_SAFETY_Z,
			SEQ_STEP_SCAN_SAFETY_Z,
			SEQ_STEP_RING_LIGHT_DOWN,
			SEQ_STEP_REVIEW_DOWN,
			SEQ_STEP_SCOPE_SAFETY_Z,
			SEQ_STEP_SCAN_SAFETY_XY,
			SEQ_STEP_SCOPE_SAFETY_XY,
		SEQ_STEP_Wait_Stick_Load_Ready,
			SEQ_STEP_GRIPPER_OPEN_SOL_BWD,
			SEQ_STEP_GRIPPER_SOL_BWD,
			SEQ_STEP_GRIPPER_CLOSE,
			SEQ_STEP_GRIPPER_ALL_STANDBY,
			SEQ_STEP_GRIPPER_Z_STANDBY,
		//SEQ_STEP_Wait_TT01_Pos_AM01,
		SEQ_STEP_Wait_Theta_Align_Start,
		//----------------------------------------
		//Stick 가인장 Sequnce이다.
		SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY_1,
		SEQ_STEP_STICK_CH_GRIPPER_OPEN_1,
		SEQ_STEP_STICK_CH_GRIPPER_SOL_FWD,
		SEQ_STEP_STICK_CH_GRIPPER_ALL_GRIPPOS,
		SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_1,
		SEQ_STEP_STICK_CH_GRIPPER_GRIP,
		SEQ_STEP_STICK_CH_GRIPPER_ALL_TENSION_ZeroSet,
		SEQ_STEP_STICK_CH_VAC_OFF,
		SEQ_STEP_STICK_CH_GRIPPER_Z_TT01_OUT,
		SEQ_STEP_STICK_CH_START_STICK_TENSION,
		SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_2,
		SEQ_STEP_STICK_CH_VAC_ON,
		SEQ_STEP_STICK_CH_GRIPPER_OPEN_2,
		SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY_2,
		SEQ_STEP_STICK_CH_GRIPPER_SOL_BWD,
		SEQ_STEP_STICK_CH_GRIPPER_CLOSE,
		//----------------------------------------
			SEQ_STEP_THETA_ALIGN,
			SEQ_STEP_GRIPPER_Z_GRIP_STANDBY,
			SEQ_STEP_GRIPPER_OPEN,
			SEQ_STEP_GRIPPER_SOL_FWD,
			SEQ_STEP_GRIPPER_ALL_GRIPPOS,
			SEQ_STEP_GRIPPER_Z_GRIP,
			SEQ_STEP_GRIPPER_GRIP,
			SEQ_STEP_GRIPPER_ALL_TENSION_ZeroSet,
		SEQ_STEP_Wait_TT01_VAC_OFF,
				SEQ_STEP_GRIPPER_Z_TT01_OUT,
		SEQ_STEP_Wait_TT01_Pos_UT02,
			SEQ_STEP_GRIPPER_ALL_TENSION_STANDBY,
			SEQ_STEP_START_STICK_TENSION,

			SEQ_STEP_INPUT_JIG,

			SEQ_STEP_GRIPPER_Z_MEASURE,

			SEQ_STEP_SEND_CIM_STICKID,
			SEQ_STEP_RECV_CIM_DATA,


/////////////////////////////////////////////////
//Stick Unloading Seq
		SEQ_STEP_STICK_UNLOADING,
			SEQ_STEP_UNLOAD_SPACE_SAFETY_Z,
			SEQ_STEP_UNLOAD_SCAN_SAFETY_Z,
			SEQ_STEP_UNLOAD_RING_LIGHT_DOWN,
			SEQ_STEP_UNLOAD_REVIEW_DOWN,
			SEQ_STEP_UNLOAD_SCOPE_SAFETY_Z,
			SEQ_STEP_UNLOAD_SCAN_SAFETY_XY,
			SEQ_STEP_UNLOAD_SCOPE_SAFETY_XY,

		SEQ_STEP_STICK_UNLOADING_START,
		SEQ_STEP_Wait_Stick_Result_ACK,

			SEQ_STEP_UNLOAD_GRIPPER_Z_TT01_OUT,
			SEQ_STEP_UNLOAD_OUTPUT_JIG,

			SEQ_STEP_UNLOAD_GRIPPER_ALL_TENSION_STANDBY,//인장 상태로 Unload한다 
		SEQ_STEP_Wait_Stick_Unload_Ready_OK,
			SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP,
		SEQ_STEP_Wait_TT01_VAC_ON,
			SEQ_STEP_UNLOAD_GRIPPER_OPEN,
			SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP_STANDBY,
			SEQ_STEP_UNLOAD_GRIPPER_SOL_BWD,
			SEQ_STEP_UNLOAD_GRIPPER_CLOSE,
			SEQ_STEP_UNLOAD_GRIPPER_ALL_STANDBY,
			SEQ_STEP_UNLOAD_GRIPPER_Z_STANDBY,
		SEQ_STEP_UNLOAD_Wait_TT01_Pos_UT02,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_StickExchange(void);
	~CSeq_StickExchange(void);
	bool fnSeqFlowLoadStart();
	bool fnSeqFlowUnloadStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();

public:
	StickInfoByIndexer  *m_pStickLoadInfo;
	GLASS_PARAM		*m_pGlassReicpeParam;
	CSeq_ThetaAlign	*m_pSeqObj_ThetaAlign;
	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;
	GripperPosition		*m_pGripperPos;
	float					*m_pStickTentionValue;
	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;
	CInterServerInterface  *m_pServerInterface;
};

