// AlarmClearDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "AlarmClearDlg.h"
#include "afxdialogex.h"


// CAlarmClearDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAlarmClearDlg, CDialogEx)

CAlarmClearDlg::CAlarmClearDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAlarmClearDlg::IDD, pParent)
{
	m_AlarmDlg_Brush = ::CreateSolidBrush(RGB(235,0,0));	

	m_pSubCIMInterfaceView = nullptr;
}

CAlarmClearDlg::~CAlarmClearDlg()
{
	DeleteObject(m_AlarmDlg_Brush);
}

void CAlarmClearDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_RECIPE, m_ctrlGridAlarm);
	DDX_Control(pDX, IDC_BTN_ALARM_CLEAR, m_btnAlarmClear);
	DDX_Control(pDX, IDC_BTN_ALARM_BUZ_STOP, m_btnBuzzerStop);
	DDX_Control(pDX, IDC_BTN_ALARM_RESET, m_btnAlarmReset);
}


BEGIN_MESSAGE_MAP(CAlarmClearDlg, CDialog)

	ON_BN_CLICKED(IDC_BTN_ALARM_CLEAR, &CAlarmClearDlg::OnBnClickedBtnAlarmClear)
	ON_WM_CTLCOLOR()	
	ON_BN_CLICKED(IDC_BTN_ALARM_RESET, &CAlarmClearDlg::OnBnClickedBtnAlarmReset)	
	ON_BN_CLICKED(IDC_BTN_ALARM_BUZ_STOP, &CAlarmClearDlg::OnBnClickedBtnAlarmBuzStop)
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CAlarmClearDlg 메시지 처리기입니다.
void CAlarmClearDlg:: InitCtrl_UI()
{
	m_btBackColor = RGB(255, 0, 0);//G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btnAlarmClear.GetTextColor(&tColor);
		m_btnAlarmClear.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,25,"굴림");
			tFont.lfHeight =20;
		}

		m_btnAlarmClear.SetRoundButtonStyle(&m_tButtonStyle);
		m_btnAlarmClear.SetTextColor(&tColor);
		m_btnAlarmClear.SetCheckButton(false);
		m_btnAlarmClear.SetFont(&tFont);

		m_btnBuzzerStop.SetRoundButtonStyle(&m_tButtonStyle);
		m_btnBuzzerStop.SetTextColor(&tColor);
		m_btnBuzzerStop.SetCheckButton(false);
		m_btnBuzzerStop.SetFont(&tFont);

		m_btnAlarmReset.SetRoundButtonStyle(&m_tButtonStyle);
		m_btnAlarmReset.SetTextColor(&tColor);
		m_btnAlarmReset.SetCheckButton(false);
		m_btnAlarmReset.SetFont(&tFont);

	}

	int CellHeight = 26;
	//////////////////////////////////////////////////////////////////////////
	m_ctrlGridAlarm.SetDefCellWidth(50);
	m_ctrlGridAlarm.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();

	m_ctrlGridAlarm.SetRowCount(1000);
	m_ctrlGridAlarm.SetColumnCount(4);
	m_ctrlGridAlarm.SetFixedRowCount(1);		
	m_ctrlGridAlarm.SetFixedColumnCount(1);
	m_ctrlGridAlarm.SetListMode(FALSE);
	m_ctrlGridAlarm.SetEditable(FALSE);
	m_ctrlGridAlarm.SetColumnResize(FALSE);
	m_ctrlGridAlarm.SetRowResize(FALSE);
	m_ctrlGridAlarm.SetFixedTextColor(RGB(255,200,100));
	m_ctrlGridAlarm.SetFixedBkColor(RGB(0,0,0));

	m_ctrlGridAlarm.SetFont(&G_GridFont);
	m_ctrlGridAlarm.SetItemText(0, 0, "ITEM_NUM");			m_ctrlGridAlarm.SetColumnWidth(0, 90);
	m_ctrlGridAlarm.SetItemText(0, 1, "TIME");					m_ctrlGridAlarm.SetColumnWidth(1, 160);
	m_ctrlGridAlarm.SetItemText(0, 2, "ID");					m_ctrlGridAlarm.SetColumnWidth(2, 80);
	m_ctrlGridAlarm.SetItemText(0, 3, "ALARM DEFINE");		m_ctrlGridAlarm.SetColumnWidth(3, 780);
	m_ctrlGridAlarm.SetItemFormat(0, 0, DT_CENTER);
	m_ctrlGridAlarm.SetItemFormat(0, 1, DT_CENTER);
	m_ctrlGridAlarm.SetItemFormat(0, 2, DT_CENTER);
	m_ctrlGridAlarm.SetItemFormat(0, 3, DT_CENTER);

	//try {
	//	m_ctrlGridAlarm.SetRowCount(0);
	//	m_ctrlGridAlarm.SetColumnCount(4);
	//	m_ctrlGridAlarm.SetFixedRowCount(1);		
	//	m_ctrlGridAlarm.SetFixedColumnCount(1);
	//	m_ctrlGridAlarm.SetListMode(TRUE);
	//}
	//catch (CMemoryException* e)
	//{
	//	e->ReportError();
	//	e->Delete();
	//	return;
	//}

	//for (int row = 0; row < m_ctrlGridAlarm.GetRowCount(); row++)
	//{
	//	for (int col = 0; col < m_ctrlGridAlarm.GetColumnCount(); col++)
	//	{
	//		GV_ITEM Item;
	//		Item.mask = GVIF_TEXT|GVIF_FORMAT;
	//		Item.col = col;
	//		Item.row = row;

	//		if(row == 0)
	//		{			
	//			Item.nFormat = DT_VCENTER|GVIF_FORMAT;

	//			if (col == 0)
	//			{
	//				Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
	//				Item.strText.Format(_T("ITEM_NUM"));					
	//			}
	//			else if( col == 1)
	//			{
	//				Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
	//				Item.strText.Format(_T("                    ALARM TIME                    "));
	//			}
	//			else if( col == 2)
	//			{
	//				Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
	//				Item.strText.Format(_T("           ALARM ID           "));					
	//			}
	//			else if( col == 3)
	//			{
	//				Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
	//				Item.strText.Format(_T("                                                                     ALARM DEFINE                                                                     "));
	//			}
	//		}
	//		m_ctrlGridAlarm.SetItem(&Item);
	//	}
	//}

	//m_ctrlGridAlarm.AutoSize(GVS_HEADER);
	//m_ctrlGridAlarm.SetTextColor(RGB(0, 0, 105));
	//m_ctrlGridAlarm.SetEditable(FALSE);
	//m_ctrlGridAlarm.EnableDragAndDrop(FALSE);
}
BOOL CAlarmClearDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	
	InitCtrl_UI();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CAlarmClearDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}
bool CAlarmClearDlg::m_fnAlarmListAdd(int nAlarmID)
{
	CTime tCurrentTime;
	tCurrentTime = CTime::GetCurrentTime();
	CString strCurrentTime;
	strCurrentTime = tCurrentTime.Format("%Y/%m/%d-%H:%M:%S");

	int AlarmIndex = -1;
	for(int FStep = 0; FStep<Alarm_List_Cnt; FStep++)
	{
		if(G_strAlarmList[FStep].nAlarmID == nAlarmID)
		{
			if(G_strAlarmList[FStep].bSetAlarm == true)//이미 발생 한경우.
				return false;

			AlarmIndex = FStep;//신규 추가.
			break;
		}
	}
	if(AlarmIndex<0)
		return false;//해당 알람 코드가 없다.

	if(G_strAlarmList[AlarmIndex].nAlarmType == ALARM_HEAVY && (G_pCIMSendObj->m_EQST_Mode != EQP_FAULT && G_pCIMSendObj->m_PRST_Process != PRST_PAUSE))
	{
		// 상태 변경 보고 Heavy Alarm 시 조건.
		G_pCIMSendObj->m_EQST_Mode_Resume=G_pCIMSendObj->m_EQST_Mode;
		G_pCIMSendObj->m_PRST_Process_Resume = G_pCIMSendObj->m_PRST_Process;

		G_pCIMSendObj->m_EQST_Mode = EQP_FAULT;
		G_pCIMSendObj->m_PRST_Process = PRST_PAUSE;

		G_pCIMSendObj->m_bAlarmHeavyChange = true;

		//모션 구동축 정지
		G_pServerInterface->m_fnSendCommand_Nrs(TASK_24_Motion, nBiz_Func_System, nBiz_Seq_Mot_All_Stop, nBiz_Unit_Zero);
	}
	else  // Alarm 경알람일때
	{
		if(G_pCIMSendObj->m_bAlarmHeavyChange == false) // Havey Alarm 이 아닐때만 
			G_MotObj.m_fnSetSignalTower(STWOER_MODE4_ALARM);
	}


	int NewInputCntNo = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH)+1;
	m_ctrlGridAlarm.SetRowCount(NewInputCntNo+1);

	CString strAlarmIndex;
	CString strKeyValue;

	strAlarmIndex.Format("%d", AlarmIndex);
	strKeyValue.Format("%s%d", Key_AlarmIndex_, NewInputCntNo);
	WritePrivateProfileString(Section_Alarm, strKeyValue, strAlarmIndex,SetAlarmList_PATH);
	strKeyValue.Format("%s%d", Key_AlarmTime_, NewInputCntNo);
	WritePrivateProfileString(Section_Alarm, strKeyValue, strCurrentTime, SetAlarmList_PATH);

	CString strInputListValue;
	strInputListValue.Format("%d", NewInputCntNo);
	m_ctrlGridAlarm.SetItemText(NewInputCntNo, 0, strInputListValue);

	strInputListValue.Format("%d", NewInputCntNo);
	m_ctrlGridAlarm.SetItemText(NewInputCntNo, 1, strCurrentTime);

	G_strAlarmList[AlarmIndex].bSetAlarm = true;
	strInputListValue.Format("%d", G_strAlarmList[AlarmIndex].nAlarmID);
	m_ctrlGridAlarm.SetItemText(NewInputCntNo, 2, strInputListValue);
	strInputListValue.Format("%s", G_strAlarmList[AlarmIndex].strAlarmText);
	m_ctrlGridAlarm.SetItemText(NewInputCntNo, 3, strInputListValue);

	m_ctrlGridAlarm.SetItemFormat(NewInputCntNo, 0, DT_CENTER);
	m_ctrlGridAlarm.SetItemFormat(NewInputCntNo, 1, DT_CENTER);
	m_ctrlGridAlarm.SetItemFormat(NewInputCntNo, 2, DT_CENTER);
	m_ctrlGridAlarm.Refresh();

	CString strTotalCnt;
	strTotalCnt.Format("%d", NewInputCntNo);
	WritePrivateProfileString(Section_Alarm, Key_AlarmTotalCnt, strTotalCnt,  SetAlarmList_PATH);

	G_pCIMSendObj->SendSetCIMAlarm(AlarmIndex);
	this->ShowWindow(SW_SHOW);
	return true;

}



HBRUSH CAlarmClearDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			//return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			//return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return m_AlarmDlg_Brush;
	return hbr;
}

void CAlarmClearDlg::OnBnClickedBtnAlarmReset()
{

}
void CAlarmClearDlg::OnBnClickedBtnAlarmClear()
{
	if(m_pSubCIMInterfaceView != nullptr)
	{
		m_pSubCIMInterfaceView->SendResetAllCIMAlarm();

		for(int RStep = 0; RStep<Alarm_List_Cnt; RStep++)
		{
			G_strAlarmList[RStep].bSetAlarm = false;
		}
		m_fnAlarmRead();
	}
}
void CAlarmClearDlg::OnBnClickedBtnAlarmBuzStop()
{
	G_MotObj.m_fnSetSignalTower_StopBuz();
}

void CAlarmClearDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnClose();
}



void CAlarmClearDlg::m_fnAlarmRead()
{
	DelAllItemGridRow(&m_ctrlGridAlarm);
	m_ctrlGridAlarm.SetDefCellHeight(26);
	//m_ctrlGridAlarm.DeleteAllItems();
	char strReadData[256];
	memset(strReadData, 0x00, 256);
	int nReadAlarmCnt = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH);
	m_ctrlGridAlarm.SetRowCount(nReadAlarmCnt+1);

	CString strKeyValue;
	CString strInputListValue;
	for(int RStep= 1; RStep<=nReadAlarmCnt; RStep++)
	{
		strKeyValue.Format("%s%d", Key_AlarmIndex_, RStep);
		int AlarmCodeIndex = GetPrivateProfileInt(Section_Alarm, strKeyValue.GetBuffer(strKeyValue.GetLength()), 0, SetAlarmList_PATH);
		strKeyValue.Format("%s%d", Key_AlarmTime_, RStep);
		GetPrivateProfileString(Section_Alarm, strKeyValue, "0000/00/00-00:00:00", &strReadData[0], 256, SetAlarmList_PATH);


		G_strAlarmList[AlarmCodeIndex].bSetAlarm = true;//이전 설정 Alarm 복원.(중복 체크시 사용.)
		strInputListValue.Format("%d", RStep);
		m_ctrlGridAlarm.SetItemText(RStep, 0, strInputListValue.GetBuffer(strInputListValue.GetLength()));

		m_ctrlGridAlarm.SetItemText(RStep, 1, strReadData);
		strInputListValue.Format("%d", G_strAlarmList[AlarmCodeIndex].nAlarmID);
		m_ctrlGridAlarm.SetItemText(RStep, 2, strInputListValue.GetBuffer(strInputListValue.GetLength()));
		strInputListValue.Format("%s", G_strAlarmList[AlarmCodeIndex].strAlarmText);
		m_ctrlGridAlarm.SetItemText(RStep, 3, strInputListValue.GetBuffer(strInputListValue.GetLength()));

		m_ctrlGridAlarm.SetItemFormat(RStep, 0, DT_CENTER);
		m_ctrlGridAlarm.SetItemFormat(RStep, 1, DT_CENTER);
		m_ctrlGridAlarm.SetItemFormat(RStep, 2, DT_CENTER);
	}
	m_ctrlGridAlarm.Refresh();
}


void CAlarmClearDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE)
	{
		CRect WinRect;
		this->GetClientRect(&WinRect);
		int AlarmViewPosX	= 1920/2-WinRect.Width()/2;
		int AlarmViewPosY	= 1080/2-WinRect.Height()/2;
		::SetWindowPos(this->m_hWnd, HWND_TOPMOST, AlarmViewPosX, AlarmViewPosY, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);

		m_fnAlarmRead();
	}
}


