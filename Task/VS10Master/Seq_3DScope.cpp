#include "StdAfx.h"
#include "Seq_3DScope.h"


CSeq_3DScope::CSeq_3DScope(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;

	m_RecvCMD_Ret = -1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;

	m_3DMeasureIndex = 0;
	m_pNowLensIndex_3DScope = nullptr;
	m_pScopeLensOffset = nullptr;
	m_pScope3DParam = nullptr;
	m_pScopePos = nullptr;
	m_pNowIndexPos = nullptr;
	m_p3DMeasurPointCnt = nullptr;
	m_pp3DMeasurPointList = nullptr;
}


CSeq_3DScope::~CSeq_3DScope(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
}

bool CSeq_3DScope::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_3DScope::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();
	G_WriteInfo(Log_Error, "CSeq_3DScope Error Step Run");
	return 0;
}
void CSeq_3DScope::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
int CSeq_3DScope::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_3DScope%03d> Sequence Start", ProcStep);

			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_STICK_STATE_CHECK:
		{//Stick이 측정 가능 상태 인지 체크 한다.

			if(G_CheckStickProc_StaticState() == ST_PROC_NG)
			{
				G_WriteInfo(Log_Check, "SEQ_STEP_STICK_STATE_CHECK ST_PROC_NG", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			CString strFromPath;
			strFromPath.Format("%s\\%s\\", VS31Mesure3D_Result_PATH, "VS31Measure3D");
			G_fnCheckDirAndCreate(strFromPath.GetBuffer(strFromPath.GetLength()));
			G_ClearFolder(strFromPath);//이전것을 지운다.

			strFromPath.Format("%s\\%s\\%s\\", VS31Mesure3D_Result_PATH, "VS31Measure3D", "AnalysisData");
			G_fnCheckDirAndCreate(strFromPath.GetBuffer(strFromPath.GetLength()));
			G_ClearFolder(strFromPath);//이전것을 지운다.

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_SCOPE_INIT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_SCOPE_INIT(%03d)>", ProcStep);

			m_pServerInterface->m_fnSendCommand_Nrs(
				TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_AlarmReset, nBiz_Unit_Zero);
			Sleep(200);
			m_pServerInterface->m_fnSendCommand_Nrs(
				TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_ManualModeON, nBiz_Unit_Zero);
			Sleep(200);

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_DATA_CHECK:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_DATA_CHECK(%03d)>", ProcStep);
			if(*m_p3DMeasurPointCnt<=0 && *m_pp3DMeasurPointList == nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_DATA_CHECK%03d> Data Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			//TT01위치 체크.
			if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_DATA_CHECK%03d>TT01 Pos Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			m_3DMeasureIndex = 0;
			fnSeqSetNextStep();//Auto Increment
		}break;

//--------------------------------------
//AF
	case SEQ_STEP_SCOPE_SAFETY_Z_MOVE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z_MOVE(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			Sleep(500);
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SENSOR_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);


			int TimeOutWait = 3000;//About 30sec
			while((G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE ) 
				  || (G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE ))
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z_MOVE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z_MOVE%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			///////////////////////////////////////////////////////////////////////////////////////////////////
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z_MOVE%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_SENSOR_SAFETY_Z:
		{
			//G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SENSOR_SAFETY_Z(%03d)>", ProcStep);
			//G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			//int TimeOutWait = 30000;//About 300sec
			//while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_AF_10XLENS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_AF_10XLENS(%03d)>", ProcStep);
			m_WaitCMD = nBiz_3DSend_SelectLens;
			m_RecvCMD = 0;
			//저 배율 Scope lens로 AF를 수행 한다.
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, SCOPE_LENS_10X);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_AF_10XLENS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			m_WaitCMD = 0;
			m_RecvCMD = 0;
			if(*m_pNowLensIndex_3DScope != SCOPE_LENS_10X)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_AF_10XLENS%03d> Scope Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_AF_MOVE_POS:
		{
			//G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_AF_MOVE_POS(%03d)>", ProcStep);
			//막으면 안됨..
			m_pNowIndexPos =  &(*m_pp3DMeasurPointList)[m_3DMeasureIndex];
			//////Cell의 Left Top만 AF를 하고 바로 측정위치로 가자.
			////if(m_pNowIndexPos->nPosType != CELL_LeftTop)
			////{
			////	G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_AF_MOVE_POS%03d> Skip Scope AF", ProcStep);
			////	fnSeqSetNextStep(SEQ_STEP_3DSCOPE_MOVE_MEASURE_POS);
			////	break;
			////}

			//if(m_pNowIndexPos->nScopeAxisAFPointX == 0 && m_pNowIndexPos->nScopeAxisAFPointY == 0)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_AF_MOVE_POS%03d> Skip AF", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_3DSCOPE_MOVE_MEASURE_POS);//Auto Increment
			//	break;
			//}
			//G_MotObj.m_fnAM01ScopeSyncMove(m_pNowIndexPos->nScopeAxisAFPointX, m_pNowIndexPos->nScopeAxisAFPointY, Sync_Scan_SpaceSensor);
			//int TimeOutWait = 3000;//About 30sec
			//while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_AF_MOVE_POS%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_AF_MOVE_POS%03d> Mot Run Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//미리 계산된 AF 위치로 이동 한다.
			//G_MotObj.m_fnScopeMultiMove(m_pNowIndexPos->nScopeAxisAFPointX, m_pNowIndexPos->nScopeAxisAFPointY);

			//int TimeOutWait = 3000;//About 30sec
			//while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_AF_MOVE_POS%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_AF_MOVE_POS%03d> Mot Run Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_AF_MEASURE_Z:
		{
		/*	G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_AF_MEASURE_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Measure);
			int TimeOutWait = 30000;
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_AF_MEASURE_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_AF_MEASURE_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}*/
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_AF_Z_MEASURE_POS:
		{
			//G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_AF_Z_MEASURE_POS(%03d)>", ProcStep);
			//G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Measure);
			//int TimeOutWait = 3000;//About 30sec
			//while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_AF_Z_MEASURE_POS%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_AF_Z_MEASURE_POS%03d> Mot Run Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_10x_AUTOFOCUS:
		{
			//G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_10x_AUTOFOCUS(%03d)>", ProcStep);
			//m_WaitCMD = nBiz_Recv_AutoFocusEnd;
			//m_RecvCMD = 0;
			//m_RecvCMD_Ret =  -1;
			//m_pServerInterface->m_fnSendCommand_Nrs(TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_AutoFocus, 0);

			//int TimeOutWait = 3000;//About 20sec
			//while(m_WaitCMD != m_RecvCMD)
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		m_RecvCMD_Ret = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

			//		break;
			//	}

			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_10x_AUTOFOCUS%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//m_WaitCMD = 0;
			//m_RecvCMD = 0;

			////결과 무시??????????????????????????????????
			//if(m_RecvCMD_Ret != 1)//AF오류 발생.
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_10x_AUTOFOCUS%03d> AF Error!!!!", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SPACE_SAFETY_Z_MOVE:
		{
			//G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SPACE_SAFETY_Z_MOVE(%03d)>", ProcStep);
			//G_MotObj.m_fnScopeSpaceZSyncMove( m_pScopePos->nScope_ZPos_Standby, m_pScopePos->nSpace_ZPos_Standby, 60);
			//int TimeOutWait = 600;
			//while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_SPACE_MULTI_MOVE].B_MOTCMD_State != IDLE )
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(100);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SPACE_SAFETY_Z_MOVE%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_SPACE_MULTI_MOVE].ActionResult.nResult != VS24MotData::Ret_Ok)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SPACE_SAFETY_Z_MOVE%03d> Mot Run Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_AF_STANDBY_Z:
		{
			/*	G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_AF_STANDBY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
			TimeOutWait --;
			if(TimeOutWait<0 || m_SeqStop == true)
			{
			m_WaitCMD = 0;
			m_RecvCMD = 0;
			if(m_SeqStop== true)
			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			else
			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			break;
			}
			Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
			G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_AF_STANDBY_Z%03d> Stop", ProcStep);
			fnSeqSetNextStep(SEQ_STEP_ERROR);
			break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
			G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_AF_STANDBY_Z%03d> Mot Run Error", ProcStep);
			fnSeqSetNextStep(SEQ_STEP_ERROR);
			break;
			}*/
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z_MOVE2:
		{
			//G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z_MOVE2(%03d)>", ProcStep);
			//G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			//int TimeOutWait = 3000;//About 30sec
			//while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z_MOVE2%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z_MOVE2%03d> Mot Run Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z_MOVE2%03d> Scope Safety Sensor Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
//Space Sensor Check Distance
	case SEQ_STEP_3DSCOPE_MOVE_MEASURE_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_MOVE_MEASURE_POS(%03d)>", ProcStep);

			//G_MotObj.m_fnScopeMultiMove(m_pNowIndexPos->nPointX, m_pNowIndexPos->nPointY);
			G_MotObj.m_fnAM01ScopeSyncMove(m_pNowIndexPos->nScopeAxisPointX, m_pNowIndexPos->nScopeAxisPointY, Sync_Scan_SpaceSensor);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_AF_MOVE_POS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_AF_MOVE_POS%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	//case SEQ_STEP_3DSCOPE_SPACE_AVOIDANCE:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_SPACE_AVOIDANCE(%03d)>", ProcStep);

	//		//Space Laser 회피 위치로 이동 한다.
	//		G_MotObj.m_fnScopeMultiMove(m_pNowIndexPos->nPointX-1000, m_pNowIndexPos->nPointY);//1mm회피
	//		int TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_SPACE_AVOIDANCE%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_SPACE_AVOIDANCE%03d> Mot Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	case SEQ_STEP_SPACE_SENSOR_MEASURE_Z:
		{
			//정의천 추가
			//G_MotObj.m_fnDoonTukLaserOn();
			//////////////////////////////////
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SENSOR_MEASURE_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Measure);
			Sleep(500); // Space sensor 측정 Seq. 정상화 시 재 정리 할 것
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Measure);

			int TimeOutWait = 30000;
			while((G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
				   || (G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE ))
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SENSOR_MEASURE_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SENSOR_MEASURE_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_SENSOR_MEASURE_START:
		{
//			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SENSOR_MEASURE_START(%03d)>", ProcStep);

			//for(int i=0; i<10; i++)
			//{
			//	G_WriteInfo(Log_Check, "====> Simul Space Sensor check(%d)", i+1);
			//	Sleep(300);
			//}
			
			//float targetPos = m_pScope3DParam->fSpaceTargetValue;
			//G_MotObj.m_fnSpaceSensorMeasureStart(true, targetPos);
			//int TimeOutWait = 10000;//About 100sec
			//while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_MEASURE].B_MOTCMD_State != IDLE )
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_MotObj.m_fnSpaceSensorMeasureStart(false, targetPos);
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SENSOR_MEASURE_Z%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_MEASURE].ActionResult.nResult != VS24MotData::Ret_Ok)
			//{
			//	G_MotObj.m_fnSpaceSensorMeasureStart(false, targetPos);
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SENSOR_MEASURE_Z%03d> Mot Run Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
			//G_MotObj.m_fnDoonTukLaserOff();
		}break;

	//case SEQ_STEP_3DSCOPE_SPACE_RETURN:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_SPACE_RETURN(%03d)>", ProcStep);

	//		//원래 측정 위치로 이동 한다.
	//		G_MotObj.m_fnScopeMultiMove(m_pNowIndexPos->nPointX, m_pNowIndexPos->nPointY);
	//		int TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_SPACE_RETURN%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_SPACE_RETURN%03d> Mot Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;

	case SEQ_STEP_3DSCOPE_AF_Z_MEASURE_POS2:
		{
			//G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z(%03d)>", ProcStep);
			//G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Measure);
			//int TimeOutWait = 3000;//About 30sec
			//while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z%03d> Mot Run Error", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_10x_AUTOFOCUS2:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_10x_AUTOFOCUS(%03d)>", ProcStep);
			m_WaitCMD = nBiz_Recv_AutoFocusEnd;
			m_RecvCMD = 0;
			m_RecvCMD_Ret =  -1;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_AutoFocus, 0);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					m_RecvCMD_Ret = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_10x_AUTOFOCUS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			m_WaitCMD = 0;
			m_RecvCMD = 0;

			//결과 무시??????????????????????????????????
			if(m_RecvCMD_Ret != 1)//AF오류 발생.
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_10x_AUTOFOCUS%03d> AF Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_3DSCOPE_MEASURE_BACK_MOVE_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Measure-30000);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_MEASURE_LENS_CHNAGE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_MEASURE_LENS_CHNAGE(%03d)>", ProcStep);
			m_WaitCMD = nBiz_3DSend_SelectLens;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, m_pScope3DParam->nMeasureLensIndex);

			int TimeOutWait = 10000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_MEASURE_LENS_CHNAGE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			m_WaitCMD = 0;
			m_RecvCMD = 0;
			if(*m_pNowLensIndex_3DScope != m_pScope3DParam->nMeasureLensIndex)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_MEASURE_LENS_CHNAGE%03d> Scope Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			Sleep(300);//확인 후 삭제
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Measure);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_MEASURE_START:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_MEASURE_START(%03d)>", ProcStep);


			//for(int i=0; i<10; i++)
			//{
			//	G_WriteInfo(Log_Check, "====> Simul 3DSCOPE_MEASURE check(%d)", i+1);
			//	Sleep(300);
			//}

			SEND_STARTMEASURE_DATA SendData;
			int nMCnt = m_3DMeasureIndex+1;

			SendData.nMaskType		= m_pScope3DParam->nStick_Type;
			SendData.nMeasureAngle	= m_pScope3DParam->nMeasure_Angle;
			SendData.fTargetThick 	= m_pScope3DParam->fStick_Thick;
			SendData.fSpaceGap 		= m_pScope3DParam->fSpaceTargetValue;
			SendData.bMeasureAll	= m_pScope3DParam->nMeasure_Area;

			//int SendData[3];
			//SendData[0] = 2;		//1:MASK_TYPE_STRIPE, 2:MASK_TYPE_DOT
			//SendData[1] = 0;		//0: 0.0Degree, 1:45.0Degree
			//SendData[2] = 45;	//Nano단위로 사용 함으로 um->Nano로 변경.
			CString strSavePath;
			strSavePath.Format(SCOPE_RESULT_SAVE_PATH);
			char *pSendData = new char[sizeof(SEND_STARTMEASURE_DATA) + strSavePath.GetLength()+1];
			memcpy(pSendData, &SendData, sizeof(SEND_STARTMEASURE_DATA) );
			memcpy((pSendData+sizeof(SEND_STARTMEASURE_DATA)), strSavePath.GetBuffer(strSavePath.GetLength()), strSavePath.GetLength());
			
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Recv_StartMeasureEnd;
			m_RecvCMD = 0;

			m_pServerInterface->m_fnSendCommand_Nrs(
				TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_StartMeasure, (USHORT)nMCnt,(USHORT)(sizeof(SEND_STARTMEASURE_DATA) + strSavePath.GetLength()) , (UCHAR*)pSendData);
			delete [] pSendData;


			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_3DSCOPE_MEASURE_WAIT_END:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_MEASURE_WAIT_END(%03d)>", ProcStep);
			//
			int TimeOutWait = 12000;//About 5min
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(100);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_MEASURE_WAIT_END%03d> Time Out Wait Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret == 0)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_MEASURE_WAIT_END%03d> Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_3DSCOPE_NEXT:
		{
			//G_MotObj.m_fnDoonTukLaserOff(); //정의천 추가 // 나중에 위로 올려....

			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_NEXT%03d> %dEnd", ProcStep, m_3DMeasureIndex+1);
			m_3DMeasureIndex++;
			if(m_3DMeasureIndex < *m_p3DMeasurPointCnt)
			{
				fnSeqSetNextStep(SEQ_STEP_SCOPE_SAFETY_Z_MOVE);//Auto Increment
				break;
			}
			fnSeqSetNextStep();//Auto Increment(
		}break;
	//-------------------------------------------------------------
	case SEQ_STEP_END_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_SPACE_SENSOR_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SPACE_SENSOR_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SPACE_SENSOR_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SPACE_SENSOR_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_3DSCOPE_AF_10XLENS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_3DSCOPE_AF_10XLENS(%03d)>", ProcStep);
			m_WaitCMD = nBiz_3DSend_SelectLens;
			m_RecvCMD = 0;
			//저 배율 Scope lens로 AF를 수행 한다.
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, SCOPE_LENS_10X);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_3DSCOPE_AF_10XLENS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			m_WaitCMD = 0;
			m_RecvCMD = 0;

			if(*m_pNowLensIndex_3DScope != SCOPE_LENS_10X)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_3DSCOPE_AF_10XLENS%03d> Scope Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			G_WriteInfo(Log_Normal, "<CSeq_3DScope%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_3DScope> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}