#pragma once
#include "afxwin.h"
#include "d:\nbiz_inspectstock\commonheader\ui\grid64\gridctrl.h"


// SubDlg_CIMInterface 대화 상자입니다.

class SubDlg_CIMInterface : public CDialogEx
{
	DECLARE_DYNAMIC(SubDlg_CIMInterface)

public:
	SubDlg_CIMInterface(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~SubDlg_CIMInterface();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SUB_CIM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	
	void WriteCIMMsg(ListColor nType, char* pWritLog );
	//LRESULT		OnUpdateCIMMsg(WPARAM wParam, LPARAM lParam);
	CXListBox m_listCIMLogView;


	bool m_bAlarmHeavyChange;
	EQP_STATE		m_EQST_Mode;//Normal, Fault, PM
	EQP_STATE		m_EQST_Mode_Resume;//Normal, Fault, PM
	PRST_STATE	m_PRST_Process;//IDLE, Setup, Excute, Pause, Disable
	PRST_STATE	  m_PRST_Process_Resume;//IDLE, Setup, Excute, Pause, Disable

	EQP_STATE		m_EQST_Mode_Old;//Normal, Fault, PM
	PRST_STATE	m_PRST_Process_Old;//IDLE, Setup, Excute, Pause, Disable

	int m_nAlarmListCount;
	
	afx_msg void OnBnClickedBtLoadComplete();
	afx_msg void OnBnClickedBtInspectionStart();
	afx_msg void OnBnClickedBtInspectionEnd();
	afx_msg void OnBnClickedBtUnloadComplete();

	afx_msg void OnBnClickedBtEqpPm();
	afx_msg void OnBnClickedBtEqpFault();
	afx_msg void OnBnClickedBtEqNormal();

	afx_msg void OnBnClickedBtPrstResume();
	afx_msg void OnBnClickedBtPrstDisable();
	afx_msg void OnBnClickedBtPrstPause();
	afx_msg void OnBnClickedBtPrstExecute();
	afx_msg void OnBnClickedBtPrstSetup();
	afx_msg void OnBnClickedBtPrstIdle();
	afx_msg void OnBnClickedBtMcmdRequest();

	void SendSetCIMAlarm(int AlarmIndex);//Alarm List에서 찾은걸을 받야야 한다.
	void SendResetAllCIMAlarm();
	CString SendAlarmList();

	int m_TotalAlarmCnt;
	CComboBox m_cbAlarmList;
	afx_msg void OnBnClickedBtAlarmReportSet();
	afx_msg void OnBnClickedBtAlarmReport2();
	afx_msg void OnCbnSelchangeCbAlarmIndex();
	CGridCtrl m_gridECIDList;
	void ECID_DataInit();
	void ECIDList_ValueUpDate();
	void ECIDList_Write();
	void ECIDList_Load();
	afx_msg void OnBnClickedBtEcidLoad();
	afx_msg void OnBnClickedBtEcidSave();

	LRESULT		ECID_Update(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedBtMccCreateTestData();
	afx_msg void OnBnClickedBtMccUploadReport();
};
