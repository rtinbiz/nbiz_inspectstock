#include "StdAfx.h"
#include "Seq_ReviewCalc.h"


CSeq_ReviewCalc::CSeq_ReviewCalc(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;

	m_RecvCMD_Ret = -1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;
	m_pScopePos = nullptr;
	m_pReviewPos = nullptr;

	m_pReviewLensOffset = nullptr;
	m_pLiveViewObj = nullptr;

	m_pNowGrabImg = nullptr;
	m_pProcGrayImgBuf = nullptr;

	m_pGripperPos = nullptr;
}


CSeq_ReviewCalc::~CSeq_ReviewCalc(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;

	if(m_pNowGrabImg != nullptr)
		cvReleaseImage(&m_pNowGrabImg);
	m_pNowGrabImg = nullptr;

	if(m_pProcGrayImgBuf != nullptr)
		cvReleaseImage(&m_pProcGrayImgBuf);
	m_pProcGrayImgBuf = nullptr;
}


bool CSeq_ReviewCalc::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_ReviewCalc::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();

	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();

	G_WriteInfo(Log_Error, "CSeq_ReviewCalc Error Step Run( Z Axis Go to a safe position)");
	return 0;
}
void CSeq_ReviewCalc::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
int CSeq_ReviewCalc::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_Inspect_Mic%03d> Sequence Start", ProcStep);

			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_INPUT_JIG:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_INPUT_JIG(%03d)>", ProcStep);
			if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 0 &&
			   G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 0 &&
			   G_MotInfo.m_pMotState->stEmissionTable.In15_Sn_MaskJigBWD == 1 &&
			   G_MotObj.m_TT01_NowPos == CMotCtrlObj::TT01_UT02_Pos)//
			{
				if(G_MotObj.m_fnStickJigMove(m_pGripperPos->nStickJig_Measure) == true)
				{
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_INPUT_JIG%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_INPUT_JIG%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
				}
				else
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_INPUT_JIG%03d> Jig Interlock Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				
			}
			else
			{
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_REVIEW_POS_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_REVIEW_POS_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nCalLens_ZUpDnPos);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_REVIEW_POS_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_REVIEW_POS_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_LENS_CALC_2X_MOVE_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_2X_MOVE_POS(%03d)>", ProcStep);
			//TT01위치 체크.
			if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_2X_MOVE_POS%03d>TT01 Pos Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nCalLens2X_Shift, m_pReviewPos->nCalLens2X_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_2X_MOVE_POS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_2X_MOVE_POS%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_2X_LENS_CHANGE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_2X_LENS_CHANGE(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Func_ReviewLens;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, REVIEW_LENS_2X, nBiz_Unit_Zero);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_2X_LENS_CHANGE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			if(m_RecvCMD_Ret != REVIEW_LENS_2X)
			{//TT01이 들어온 상태가 아니다.
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_2X_LENS_CHANGE%03d> Reve Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			
			fnSeqSetNextStep();//Auto Increment
		}break;
	
	case SEQ_STEP_LENS_CALC_2X_REVIEW_LIGHT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_2X_REVIEW_LIGHT(%03d)>", ProcStep);

			//Live View를 On한다.
			m_pLiveViewObj->LiveView(true);

			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_SetReviewLight;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, 
				nBiz_Seq_SetReviewLight, m_pReviewPos->nCalLens2X_ReflectLight);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_2X_REVIEW_LIGHT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//if(m_RecvCMD_Ret != m_pReviewPos->nCalLens2X_ReflectLight)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_2X_REVIEW_LIGHT%03d> Scope Lens Check Error!!!!", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_2X_REVIEW_AF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_2X_REVIEW_AF(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_AF;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 3);//One AF Try

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_2X_REVIEW_AF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)//AF On
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_2X_REVIEW_AF%03d> AF On!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_2X_REVIEW_GRAB:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_2X_REVIEW_GRAB(%03d)>", ProcStep);
			//Live View를 On한다.
			if(m_pNowGrabImg != nullptr)
				cvReleaseImage(&m_pNowGrabImg);
			m_pNowGrabImg = nullptr;

			bool bLiveOverlay = m_pLiveViewObj->m_bSaveOverlayImage;
			m_pLiveViewObj->m_bSaveOverlayImage = false;
			m_pLiveViewObj->SaveImage(&m_pNowGrabImg);

			int TimeOutWait = 3000;//About 20sec
			while(m_pNowGrabImg == nullptr)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			m_pLiveViewObj->m_bSaveOverlayImage= bLiveOverlay;
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_2X_REVIEW_GRAB%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pNowGrabImg  ==  nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_2X_REVIEW_GRAB%03d> Grab Image Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			//-------------------------------------------------------------
			float fpixelSizeX = m_pReviewLensOffset->Lens02xPixelSizeX;
			float fpixelSizeY = m_pReviewLensOffset->Lens02xPixelSizeY;
			//double Angle = 	getObjectAngle(pTest1);
			IplImage *pTest2= cvCloneImage(m_pNowGrabImg);
			//rotateImage(pTest1, pTest2, -0.5);
			ScaleMeasuer(pTest2, REVIEW_LENS_2X, fpixelSizeX, fpixelSizeY);
			cvSaveImage("D:\\CalcLens2x_Org.bmp", m_pNowGrabImg);
			if(pTest2 != nullptr)
				cvReleaseImage(&pTest2);
			pTest2 = nullptr;
			//-------------------------------------------------------------
			

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_10X_MOVE_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_10X_MOVE_POS(%03d)>", ProcStep);
			//TT01위치 체크.
			if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_10X_MOVE_POS%03d>TT01 Pos Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nCalLens10X_Shift, m_pReviewPos->nCalLens10X_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_10X_MOVE_POS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_10X_MOVE_POS%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_10X_LENS_CHANGE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_10X_LENS_CHANGE(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Func_ReviewLens;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, REVIEW_LENS_10X, nBiz_Unit_Zero);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_10X_LENS_CHANGE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			if(m_RecvCMD_Ret != REVIEW_LENS_10X)
			{//TT01이 들어온 상태가 아니다.
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_10X_LENS_CHANGE%03d> Reve Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_LENS_CALC_10X_REVIEW_LIGHT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_10X_REVIEW_LIGHT(%03d)>", ProcStep);

			//Live View를 On한다.
			m_pLiveViewObj->LiveView(true);

			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_SetReviewLight;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, 
				nBiz_Seq_SetReviewLight, m_pReviewPos->nCalLens10X_ReflectLight);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_10X_REVIEW_LIGHT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//if(m_RecvCMD_Ret != m_pReviewPos->nCalLens10X_ReflectLight)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_10X_REVIEW_LIGHT%03d> Scope Lens Check Error!!!!", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_10X_REVIEW_AF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_10X_REVIEW_AF(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_AF;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 3);//One AF Try

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_10X_REVIEW_AF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)//AF On
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_10X_REVIEW_AF%03d> AF On!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_10X_REVIEW_GRAB:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_10X_REVIEW_GRAB(%03d)>", ProcStep);
			//Live View를 On한다.
			if(m_pNowGrabImg != nullptr)
				cvReleaseImage(&m_pNowGrabImg);
			m_pNowGrabImg = nullptr;

			bool bLiveOverlay = m_pLiveViewObj->m_bSaveOverlayImage;
			m_pLiveViewObj->m_bSaveOverlayImage = false;
			m_pLiveViewObj->SaveImage(&m_pNowGrabImg);

			int TimeOutWait = 3000;//About 20sec
			while(m_pNowGrabImg == nullptr)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			m_pLiveViewObj->m_bSaveOverlayImage= bLiveOverlay;
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_10X_REVIEW_GRAB%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pNowGrabImg  ==  nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_10X_REVIEW_GRAB%03d> Grab Image Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			//-------------------------------------------------------------
			float fpixelSizeX = m_pReviewLensOffset->Lens10xPixelSizeX;
			float fpixelSizeY = m_pReviewLensOffset->Lens10xPixelSizeY;
			//double Angle = 	getObjectAngle(pTest1);
			IplImage *pTest2= cvCloneImage(m_pNowGrabImg);
			//rotateImage(pTest1, pTest2, -0.5);
			ScaleMeasuer(pTest2, REVIEW_LENS_10X, fpixelSizeX, fpixelSizeY);
			cvSaveImage("D:\\CalcLens10x_Org.bmp", m_pNowGrabImg);
			if(pTest2 != nullptr)
				cvReleaseImage(&pTest2);
			pTest2 = nullptr;
			//-------------------------------------------------------------
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_LENS_CALC_20X_MOVE_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_20X_MOVE_POS(%03d)>", ProcStep);
			//TT01위치 체크.
			if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_20X_MOVE_POS%03d>TT01 Pos Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nCalLens20X_Shift, m_pReviewPos->nCalLens20X_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_20X_MOVE_POS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_20X_MOVE_POS%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_20X_LENS_CHANGE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_20X_LENS_CHANGE(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Func_ReviewLens;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, REVIEW_LENS_20X, nBiz_Unit_Zero);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_20X_LENS_CHANGE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			if(m_RecvCMD_Ret != REVIEW_LENS_20X)
			{//TT01이 들어온 상태가 아니다.
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_20X_LENS_CHANGE%03d> Reve Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_LENS_CALC_20X_REVIEW_LIGHT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_20X_REVIEW_LIGHT(%03d)>", ProcStep);

			//Live View를 On한다.
			m_pLiveViewObj->LiveView(true);

			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_SetReviewLight;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, 
				nBiz_Seq_SetReviewLight, m_pReviewPos->nCalLens20X_ReflectLight);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_20X_REVIEW_LIGHT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//if(m_RecvCMD_Ret != m_pReviewPos->nCalLens20X_ReflectLight)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_20X_REVIEW_LIGHT%03d> Scope Lens Check Error!!!!", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_20X_REVIEW_AF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_20X_REVIEW_AF(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_AF;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 3);//One AF Try

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_20X_REVIEW_AF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)//AF On
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_20X_REVIEW_AF%03d> AF On!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_20X_REVIEW_GRAB:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_20X_REVIEW_GRAB(%03d)>", ProcStep);
			//Live View를 On한다.
			if(m_pNowGrabImg != nullptr)
				cvReleaseImage(&m_pNowGrabImg);
			m_pNowGrabImg = nullptr;

			bool bLiveOverlay = m_pLiveViewObj->m_bSaveOverlayImage;
			m_pLiveViewObj->m_bSaveOverlayImage = false;
			m_pLiveViewObj->SaveImage(&m_pNowGrabImg);

			int TimeOutWait = 3000;//About 20sec
			while(m_pNowGrabImg == nullptr)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			m_pLiveViewObj->m_bSaveOverlayImage= bLiveOverlay;
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_20X_REVIEW_GRAB%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pNowGrabImg  ==  nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_20X_REVIEW_GRAB%03d> Grab Image Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			//-------------------------------------------------------------
			float fpixelSizeX = m_pReviewLensOffset->Lens20xPixelSizeX;
			float fpixelSizeY = m_pReviewLensOffset->Lens20xPixelSizeY;
			//double Angle = 	getObjectAngle(pTest1);
			IplImage *pTest2= cvCloneImage(m_pNowGrabImg);
			//rotateImage(pTest1, pTest2, -0.5);
			ScaleMeasuer(pTest2, REVIEW_LENS_20X, fpixelSizeX, fpixelSizeY);
			cvSaveImage("D:\\CalcLens20x_Org.bmp", m_pNowGrabImg);
			if(pTest2 != nullptr)
				cvReleaseImage(&pTest2);
			pTest2 = nullptr;
			//-------------------------------------------------------------
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_LENS_CALC_END_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_END_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_END_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_END_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_END_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_END_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_END_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_END_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LENS_CALC_END_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LENS_CALC_END_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LENS_CALC_END_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LENS_CALC_END_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_OUTPUT_JIG:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_OUTPUT_JIG(%03d)>", ProcStep);
			if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 0 &&
				G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 0 &&
				G_MotInfo.m_pMotState->stEmissionTable.In15_Sn_MaskJigBWD == 0 &&
				G_MotObj.m_TT01_NowPos == CMotCtrlObj::TT01_UT02_Pos)//
			{

	
				if(G_MotObj.m_fnStickJigMove(m_pGripperPos->nStickJig_Standby) == true)
				{
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_OUTPUT_JIG%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_OUTPUT_JIG%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					//Jig Out일때는 센서도 본다.
					if(G_MotInfo.m_pMotState->stEmissionTable.In15_Sn_MaskJigBWD != 1)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_OUTPUT_JIG%03d> Mask Jig BWD Detect Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
				}
				else
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_OUTPUT_JIG%03d> Jig Interlock Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			G_WriteInfo(Log_Normal, "<CSeq_Inspect_Mic%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_Inspect_Mic> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}