
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원


#include <afxsock.h>            // MFC 소켓 확장



#define  USE_CAM_DEVICE
//#define SIMUL_CDTP_CALC_VIEW
//#define SIMUL_CELL_ALIGN_CALC_VIEW
#define SIMUL_REVIEW_LENS_CALC
#define DIRECT_TT01_ALINGN_MOVE


static int G_ReviewToDrawXOffset = -310838;//-331500+(20662);
static int G_ReviewToDrawYOffset = 156835;//107000+(49835);

static int G_ScopeToDrawXOffset = -73000;//-73000;
static int G_ScopeToDrawYOffset = -235000;


//Image Process Lib Loading
#include "..\..\CommonHeader\ImgProcLink.h"
//VS64 Interface Lib Loading
#include "..\..\CommonHeader\VS_ServerLink.h"
//UI용 Header File Link
#include "..\..\CommonHeader\UI_LinkHeader.h"


#include "LiveGrab.h"


#include "..\..\CommonHeader\Motion\MotionIO.h"
#include "..\..\CommonHeader\Motion\MotionAxisDef.h"
#include "..\..\CommonHeader\Inspection\CalculateCoordinateDefine.h"
#include "..\..\CommonHeader\Inspection\DefineStaticValue.h"

#include "..\..\CommonHeader\Motion\MotionAxisDef.h"
#include "..\..\CommonHeader\Motion\MotionIO.h"

#include "..\..\..\CommonHeader\MCC_Command.h"

#include "..\..\CommonHeader\Sequence\SeqBase.h"

#include "BlobLabeling.h"
#include "SystemMasterParam.h"
#include "MotCtrlObj.h"


#define  LOG_TEXT_MAX_SIZE 1024
void G_WriteInfo(ListColor nColorType, const char* pWritLog, ...);
void G_WriteInfo_FromCIM(ListColor nColorType, const char* pWritLog, ...);

void G_AlarmAdd(int AlamID);


extern CMCC_Command G_MccCommand;
extern CInterServerInterface  *G_pServerInterface;

//#include "DataFormat\DataFormat.h"
//extern CDataFormat G_MeasureStickResultObj;
#include "CIM_Interface\CimInterfaceSocket.h"
#include "..\..\CommonHeader\Static_List_VS10Master.h"

extern int Alarm_List_Cnt;
extern AlarmInfo G_strAlarmList[];

extern int ECID_List_Cnt;
//extern ECIDInfo G_strECIDList[] ;
extern ECIDInfo *G_strECIDList;

extern int EOID_List_Cnt;
extern EOIDInfo G_strEOIDList[] ;

extern int PPID_List_Cnt;
extern PPIDInfo G_strPPIDList[] ;

extern int SVID_List_Cnt;
extern SVIDInfo G_strSVIDList[] ;

extern CCimInterfaceSocket *G_pCIMInterface;

#include "SubDlg_CIMInterface.h"
#include "AlarmClearDlg.h"
extern CAlarmClearDlg *G_pAlarmSendObj;
extern SubDlg_CIMInterface *G_pCIMSendObj;
////////////////////////////////////////////////
// Color Define.
#define SMART_RED			RGB(255,128,128)
#define SMART_BLUE			RGB(128,128,255)
#define SMART_GREEN			RGB(128,255,128)
#define SMART_YELLOW		RGB(255,255,128)
#define SMART_MAGENTA		RGB(255,128,255)
#define SMART_BRIGHT_BLUE	RGB(128,255,255)

#define DARK_GRAY			RGB(128,128,128)
#define BRIGHT_GRAY			RGB(200,200,200)
#define GRAY				RGB(84, 84, 84)

#define WHITE				RGB(255,255,255)
#define BLACK				RGB(0,0,0)
#define GREEN				RGB(0,255,0)
#define BLUE				RGB(0,0,255)
#define RED					RGB(255,0,0)
#define YELLOW				RGB(255,255,0)
#define MAGENTA				RGB(255,0,255)

#define DEEP_BLUE			RGB(0,0,125)


extern COLORREF G_Color_WinBack;
extern COLORREF G_Color_On;
extern COLORREF G_Color_Off;
extern COLORREF G_Color_LightOn;
extern COLORREF G_Color_Lock;
extern COLORREF G_Color_Unlock;
extern COLORREF G_Color_Open;
extern COLORREF G_Color_Close;


extern COLORREF G_CSignalTower_Red_ON;
extern COLORREF G_CSignalTower_Red_OFF;
extern COLORREF G_CSignalTower_Yellow_ON;
extern COLORREF G_CSignalTower_Yellow_OFF;
extern COLORREF G_CSignalTower_Green_ON;
extern COLORREF G_CSignalTower_Green_OFF;
extern COLORREF G_CSignalTower_Buzzer_ON;
extern COLORREF G_CSignalTower_Buzzer_OFF;


extern CFont	G_TaskStateFont;
extern CFont	G_TaskStateSmallFont;
extern CFont	G_RecipeFileFont;
extern CFont	G_NormalFont;
extern CFont	G_GridFont;
extern CFont	G_GridSmallFont;
extern CFont	G_NumberFont;
extern CFont	G_SmallFont;
extern CFont	G_MidFont;

#include "MotionCMDState.h"

//전역 변수로 설정했다.
extern CMotCtrlObj G_MotObj;


extern TASK_State G_VS10TASK_STATE;
extern TASK_State G_VS11TASK_STATE;
extern TASK_State G_VS13TASK_STATE;
extern TASK_State G_VS21TASK_STATE;
extern TASK_State G_VS24TASK_STATE;
extern TASK_State G_VS31TASK_STATE;
extern TASK_State G_VS32TASK_STATE;

#include "LiveViewCtrl.h"
#include "PMDlg.h"

#define TIMER_MOT_POS_UPDATE				10101
#define TIMER_INSPECT_INFO_UPDATE		10102
#define TIMER_TASK_STATE_UPDATE			10103
#define TIMER_MAINT_DLG_UPDATE			10104
#define TIMER_CIM_DLG_UPDATE				10105
#define TIMER_PASSWORD_INPUT				10106
#define TIMER_TIME_UPDATE					10107

#define  WM_USER_RECIPE_CHANGE			 (WM_USER + 1036)   
//#define  WM_USER_CIM_VIEW_MSG 			 (WM_USER + 1037)   
#define  WM_USER_WRITE_INSPECT_RET		 (WM_USER + 1038)
enum
{
	Write_None= 0,
	Write_Reset = 1,
	Write_ResultAll = 2,
	Write_SetStartTime,
	Write_SetEndTime,
};
#define  WM_USER_RECV_HOST_MSG		 (WM_USER + 1039)


extern HWND G_CIM_I_value;
#define  WM_USER_ECID_CHANGE			 (WM_USER + 1039)   

#define  REVIEW_RULER_SCALE_02x			100.0f
#define  REVIEW_RULER_SCALE_10x			50.0f
#define  REVIEW_RULER_SCALE_20x			10.0f
#define INSPECT_SCAN_OVER_um  0
void DelAllItemGridRow(CGridCtrl *pTargetGrid);
void NextCellEditMove(CGridCtrl *pTargetGrid);
void DelSelectedItemGridRow(CGridCtrl *pTargetGrid);

#define  MOT_MEM_SHARE   "MOT_MEMORY"
typedef struct tag_Machine_Status
{
	ALIO_A_In stNormalStatus;  //Door, Temperature, 장비 일반 사항.
	ALIO_B_In stBoxTableSensor; //Box Table, Box Detect Sensor
	ALIO_C_In stModuleStatus; //TM01, LT01, BO01, UT01
	ALIO_D_In stGripperStatus; //Gripper
	ALIO_E_In stPickerSensor;  //TM02
	ALIO_F_In stPickerCylinder;  //TM02 Cylinder
	ALIO_G_In stTurnTableSensor; //TT01 
	ALIO_H_In stReserve;
	ALIO_I_In stEmissionTable; //UT02 Status

	ALIO_A_Out		m_IO_OutA;
	ALIO_B_Out			m_IO_OutB;
	ALIO_C_Out		m_IO_OutC;
	ALIO_D_Out		m_IO_OutD;
	ALIO_E_Out			m_IO_OutE;
	ALIO_F_Out			m_IO_OutF;
	ALIO_G_Out		m_IO_OutG;
	ALIO_H_Out		m_IO_OutH;
	ALIO_I_Out			m_IO_OutI;

	int			  nMotionPos[AXIS_Cnt];

	bool			m_bScopeSafetyPos;
	tag_Machine_Status()
	{
		memset(this, 0x00, sizeof(tag_Machine_Status));
	}
}STMACHINE_STATUS;
typedef struct SMem_Handle_Tag
{	
	HANDLE hFile;
	HANDLE hFMap;
	int nTSize;
	STMACHINE_STATUS *m_pMotState;
	SMem_Handle_Tag()
	{
		//memset(this, 0x00, sizeof(SFile_Handle));
		hFile = INVALID_HANDLE_VALUE;
		hFMap = INVALID_HANDLE_VALUE;

		nTSize = sizeof(STMACHINE_STATUS);
		m_pMotState = nullptr;
	}
}SMem_Handle;
extern SMem_Handle	 G_MotInfo;
bool OpenMotionMem(SMem_Handle *pTargetMem);
bool CloseMotionMem(SMem_Handle *pTargetMem);

//Scale Check
#define MaxDetectCNT			20
#define SCAILE_SMALL_LINE_CNT	28
#define SCAILE_LONG_LINE_CNT	6
#define RESULT_SAVE_SIZE		500

#define HOSTID_NAME           "     "
#define EQP_NAME_A3MSI01N "A3MSI01N"
#define EQP_FLOW_ID	"MSK"

#define ST_PROC_OK true
#define ST_PROC_NG false
bool G_CheckStickProc_StaticState();
bool G_CheckStickJig_InOut();


enum EQ_RUN_MODE
{
	Master_NONE = 0,
	Master_AUTO = 1,
	Master_MANUAL,
	Master_PM,
};
extern EQ_RUN_MODE G_NowEQRunMode;
typedef struct TaskLinkCheck_Tag
{	
	int m_TaskCheckCnt_Inspector;
	int m_TaskCheckCnt_Indexer;
	int m_TaskCheckCnt_Motion;
	int m_TaskCheckCnt_Mesure3D;
	int m_TaskCheckCnt_Area3DScan;
	int m_TaskCheckCnt_AutoFocus;
	TaskLinkCheck_Tag()
	{
		memset(this, 0x00, sizeof(TaskLinkCheck_Tag));
	}
}TLCheck;

#define CALL_BUZZER2_STICK_INFO 2000
#define CALL_BUZZER2_STICK_LOAD_END 4000
#define CALL_BUZZER2_AUTO_RUN_END 6000

#pragma comment (lib, "version.lib")
#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


