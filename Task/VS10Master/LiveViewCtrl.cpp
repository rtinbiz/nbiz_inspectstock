#include "StdAfx.h"
#include "LiveViewCtrl.h"

CvFont G_cvDrawVBigFont; 
CvFont G_cvDrawBigFont; 
CvFont G_cvDrawFont; 
CvFont G_cvDrawSmallFont; 
char G_strProcMsg[128];
//CvFont		G_ImageInfoBFont;
void PutImage_TextBig(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor, CvScalar BackColor, CvScalar RectColor)
{
	//	extern CvFont		G_ImageInfoFont;
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_cvDrawBigFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= NamePos.x-5;
	StartTxt.y	= NamePos.y+baseline;
	EndTxt.x	= NamePos.x+text_size.width+5;
	EndTxt.y	= NamePos.y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, BackColor);

	cvPutText(pMainMap, text, NamePos, &G_cvDrawBigFont, FontColor);

	cvRectangle(pMainMap, StartTxt, EndTxt, RectColor);
}

#include <math.h>
double getObjectAngle(IplImage *src)
{
	CvMoments cm;
	//try
	//{
	//	cvMoments(src, &cm, true);
	//	double th = 0.5 * atan((2 * cm.m11) / (cm.m20 - cm.m02));
	//	return th * (180 / 3.14);
	//}
	//catch
	{
		IplImage *tmp = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
		cvCvtColor(src, tmp, CV_RGB2GRAY);
		

		//cvAdaptiveThreshold(tmp, tmp, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 11, 5);
		cvThreshold(tmp, tmp, 128, 255, CV_THRESH_BINARY);
		//cvErode(tmp, tmp, NULL,4);
		//cvDilate(tmp, tmp, NULL,4);
		
		//cvDilate(tmp, tmp, NULL,20);
		//cvNamedWindow("pTest2", CV_WINDOW_NORMAL);
		//cvShowImage("pTest2", tmp);
		//cvWaitKey(0);
		//IplImage *pCrossImg = cvLoadImage("D:\\nBiz_InspectStock\\Data\\Cross.bmp", CV_LOAD_IMAGE_GRAYSCALE);
		//IplImage* C = cvCreateImage( cvSize( tmp->width - pCrossImg->width+1, tmp->height - pCrossImg->height+1 ), IPL_DEPTH_32F, 1 ); // 상관계수를 구할 이미지(C)
		//double min=0.0, max=0.0;
		//CvPoint left_top;
		//cvMatchTemplate(tmp, pCrossImg, C, CV_TM_CCOEFF_NORMED); // 상관계수를 구하여 C 에 그린다.
		//cvNamedWindow("C", CV_WINDOW_NORMAL);
		//do
		//{
		//	min=0.0;
		//	max=0.0;

		//	cvMinMaxLoc(C, &min, &max, NULL, &left_top); // 상관계수가 최대값을 값는 위치 찾기 
		//	//cvRectangle(tmp, left_top, cvPoint(left_top.x + pCrossImg->width, left_top.y + pCrossImg->height), CV_RGB(255,0,0)); // 
		//	//cvDrawRect(tmp, left_top, cvPoint(left_top.x + pCrossImg->width, left_top.y + pCrossImg->height), CV_RGB(255,255,255), -1);
		//	cvDrawRect(tmp, left_top, cvPoint(left_top.x + pCrossImg->width, left_top.y + pCrossImg->height), CV_RGB(0,0,0), -1);
		//	cvDrawRect(C, left_top, cvPoint(left_top.x + pCrossImg->width, left_top.y + pCrossImg->height), CV_RGB(0,0,0), -1);

		//	cvShowImage("C",C);   // 상관계수 이미지 보기
		//	cvWaitKey(200);
		//}while(max>0.7);
		//
		//
		//
		//cvNamedWindow("T9-result", CV_WINDOW_NORMAL);
		////cvNamedWindow("T9-sample");
		////cvNamedWindow("C");
		//cvShowImage("T9-result",tmp); // 결과 보기
		////cvShowImage("T9-sample",pCrossImg); // 스테이플러(B) 보기
		////cvShowImage("C",C);   // 상관계수 이미지 보기
		//cvWaitKey(0);

		//// 모든 이미지 릴리즈
		//
		//cvReleaseImage(&pCrossImg);
		//cvReleaseImage(&C);


		cvMoments(tmp, &cm, 0);
		double th = 0.5 * atan((2.0 * cm.m11) / (cm.m20 - cm.m02));



		cvReleaseImage(&tmp);
		return th* (180 / 3.14);

	}
}
void rotateImage(const IplImage* src, IplImage* dst, double degree)
{
	// Only 1-Channel
	//if(src->nChannels != 1)
	//	return;
	CvPoint2D32f    centralPoint    = cvPoint2D32f(src->width/2, src->height/2);            // 회전 기준점 설정(이미지의 중심점)
	CvMat*            rotationMatrix    = cvCreateMat(2, 3, CV_32FC1);                        // 회전 기준 행렬
	// Rotation 기준 행렬 연산 및 저장(90도에서 기울어진 각도를 빼야 본래이미지(필요시 수정))
	cv2DRotationMatrix(centralPoint, degree, 1, rotationMatrix);
	// Image Rotation
	cvWarpAffine(src, dst, rotationMatrix, CV_INTER_LINEAR + CV_WARP_FILL_OUTLIERS);
	// Memory 해제
	cvReleaseMat(&rotationMatrix);
}
DWORD ScaleMeasuer(IplImage* srcClone, int LensIndex, double fPixelSizeX, double fPixelSizeY, int CheckRectSize, bool isLive)
{
	DWORD ProcTime = GetTickCount();
	//이진화 영상 처리.
	//IplImage *srcClone = cvCloneImage(src);
	IplImage *tmp = cvCreateImage(cvSize(srcClone->width, srcClone->height), IPL_DEPTH_8U, 1);
	cvCvtColor(srcClone, tmp, CV_RGB2GRAY);
	cvThreshold(tmp, tmp, 128, 255, CV_THRESH_BINARY_INV);

	int RectSize =CheckRectSize/2; 
	BYTE *pDataPointLT= nullptr;
	BYTE *pDataPointRT= nullptr;
	BYTE *pDataPointLB= nullptr;
	BYTE *pDataPointRB= nullptr;

	BYTE *pDataPointSLT= nullptr;
	BYTE *pDataPointSRT= nullptr;
	BYTE *pDataPointSLB= nullptr;
	BYTE *pDataPointSRB= nullptr;

	BYTE *pDataPointNoneTC= nullptr;
	BYTE *pDataPointNoneBC= nullptr;
	BYTE *pDataPointNoneLC= nullptr;
	BYTE *pDataPointNoneRC= nullptr;

	int DtRectCnt = 0;

	CRect DetectRect[MaxDetectCNT];
	memset(DetectRect, 0x00, sizeof(CRect)*MaxDetectCNT);

	for(int HStep= RectSize; HStep<tmp->height-RectSize; HStep++)
	{
		for(int WStep= RectSize; WStep<tmp->width-RectSize; WStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*tmp->width));
			if(*pDataPointC>0)
			{
				//기준거리
				pDataPointLT =  (BYTE *)(tmp->imageData+(WStep-RectSize)+((HStep-RectSize)*tmp->width));
				pDataPointRT =  (BYTE *)(tmp->imageData+(WStep+RectSize)+((HStep-RectSize)*tmp->width));
				pDataPointLB =  (BYTE *)(tmp->imageData+(WStep-RectSize)+((HStep+RectSize)*tmp->width));
				pDataPointRB =  (BYTE *)(tmp->imageData+(WStep+RectSize)+((HStep+RectSize)*tmp->width));
				//기준거리에 반
				pDataPointSLT =  (BYTE *)(tmp->imageData+(WStep-RectSize/2)+((HStep-RectSize/2)*tmp->width));
				pDataPointSRT =  (BYTE *)(tmp->imageData+(WStep+RectSize/2)+((HStep-RectSize/2)*tmp->width));
				pDataPointSLB =  (BYTE *)(tmp->imageData+(WStep-RectSize/2)+((HStep+RectSize/2)*tmp->width));
				pDataPointSRB =  (BYTE *)(tmp->imageData+(WStep+RectSize/2)+((HStep+RectSize/2)*tmp->width));
				//0이 나와야 하는 위치
				pDataPointNoneTC =  (BYTE *)(tmp->imageData+(WStep)+((HStep-RectSize)*tmp->width));
				pDataPointNoneBC =  (BYTE *)(tmp->imageData+(WStep)+((HStep+RectSize)*tmp->width));
				pDataPointNoneLC =  (BYTE *)(tmp->imageData+(WStep-RectSize)+((HStep)*tmp->width));
				pDataPointNoneRC =  (BYTE *)(tmp->imageData+(WStep+RectSize)+((HStep)*tmp->width));

				if((*pDataPointLT>0 &&*pDataPointRT>0 &&*pDataPointLB>0 &&*pDataPointRB>0)&&
					(*pDataPointSLT>0 &&*pDataPointSRT>0 &&*pDataPointSLB>0 &&*pDataPointSRB>0)&&
					(*pDataPointNoneTC==0 &&*pDataPointNoneBC==0 &&*pDataPointNoneLC==0 &&*pDataPointNoneRC==0))
				{
					bool bInsert = false;
					for(int SCRStep = 0; SCRStep<MaxDetectCNT ; SCRStep++)
					{
						CPoint Ch;
						Ch.x = WStep;
						Ch.y = HStep;
						//내부에 속하면
						if(DetectRect[SCRStep].PtInRect(Ch) == TRUE)
						{
							DetectRect[SCRStep].left		= (DetectRect[SCRStep].left <		(WStep-RectSize))?DetectRect[SCRStep].left:(WStep-RectSize);
							DetectRect[SCRStep].top		= (DetectRect[SCRStep].top <		(HStep-RectSize))?DetectRect[SCRStep].top:(HStep-RectSize);
							DetectRect[SCRStep].right		= (DetectRect[SCRStep].right >		(WStep+RectSize))?DetectRect[SCRStep].right:WStep+RectSize;
							DetectRect[SCRStep].bottom	= (DetectRect[SCRStep].bottom >	(HStep+RectSize))?DetectRect[SCRStep].bottom:HStep+RectSize;
							bInsert = true;
							break;
						}
					}
					if(bInsert==false && DtRectCnt<MaxDetectCNT)
					{//포함 사각이 없었다는 것 임으로.
						DetectRect[DtRectCnt].left = WStep-RectSize;
						DetectRect[DtRectCnt].top = HStep-RectSize;
						DetectRect[DtRectCnt].right = WStep+RectSize;
						DetectRect[DtRectCnt].bottom = HStep+RectSize;
						DtRectCnt++;
					}
				}
			}
		}
	}
	////잡음이 가장 적은 것을 찾는다.
	//CvRect FindRect;
	//double ValueSumMin = 999999999998.0;
	//double ValueSumBuf = 999999999999.0;
	//for(int DrawStep = 0; DrawStep<DtRectCnt; DrawStep++)
	//{
	//	CvRect RectSumValue;
	//	RectSumValue.x = DetectRect[DrawStep].left;
	//	RectSumValue.y = DetectRect[DrawStep].top;
	//	RectSumValue.width = DetectRect[DrawStep].Width();
	//	RectSumValue.height = DetectRect[DrawStep].Height();

	//	cvSetImageROI(tmp, RectSumValue);
	//	ValueSumBuf = cvSum(tmp).val[0];
	//	cvResetImageROI(tmp);

	//	if(ValueSumMin>ValueSumBuf)
	//	{
	//		ValueSumMin = ValueSumBuf;
	//		FindRect = RectSumValue;
	//	}

	//	cvDrawRect(srcClone, cvPoint(DetectRect[DrawStep].left, DetectRect[DrawStep].top), cvPoint(DetectRect[DrawStep].right, DetectRect[DrawStep].bottom), CV_RGB(0,255,0), 2);
	//	
	//}

	double SDistance = 99999999.0;
	double BufDis= SDistance;
	
	//Center에서 가장 가까운것. 찾기.
	CvRect FindRect;
	for(int DrawStep = 0; DrawStep<DtRectCnt; DrawStep++)
	{

		BufDis = sqrt(pow((double)((DetectRect[DrawStep].left+DetectRect[DrawStep].Width()/2) - (srcClone->width/2)), 2) + 
						  pow((double)((DetectRect[DrawStep].top+DetectRect[DrawStep].Height()/2) - (srcClone->height/2)), 2)); 
		if(SDistance>BufDis)
		{
			SDistance = BufDis;
			
			FindRect.x = DetectRect[DrawStep].left;
			FindRect.y = DetectRect[DrawStep].top;
			FindRect.width = DetectRect[DrawStep].Width();
			FindRect.height = DetectRect[DrawStep].Height();
		}

		//cvDrawRect(srcClone, cvPoint(DetectRect[DrawStep].left, DetectRect[DrawStep].top), cvPoint(DetectRect[DrawStep].right, DetectRect[DrawStep].bottom), CV_RGB(0,255,0), 2);
	}
	if(FindRect.width<0 || FindRect.height<0)
	{
		G_WriteInfo(Log_Worrying,"Call Error");
		if(tmp != nullptr)
			cvReleaseImage(&tmp);
		tmp = nullptr;

		//if(srcClone != nullptr)
		//	cvReleaseImage(&srcClone);
		//srcClone = nullptr;
		return -1;
	}

	//Center Draw
	cvLine(srcClone, cvPoint(FindRect.x, FindRect.y),cvPoint(FindRect.x +FindRect.width, FindRect.y+FindRect.height), CV_RGB(255,0,0), 1);
	cvLine(srcClone, cvPoint(FindRect.x, FindRect.y+FindRect.height),cvPoint(FindRect.x +FindRect.width, FindRect.y), CV_RGB(255,0,0), 1);
	cvCircle(srcClone, cvPoint(FindRect.x+FindRect.width/2, FindRect.y+FindRect.height/2), FindRect.width/2,CV_RGB(0,255,0), 2);
#ifdef SIMUL_REVIEW_LENS_CALC
	cvDrawRect(srcClone, cvPoint(FindRect.x, FindRect.y), cvPoint(FindRect.x +FindRect.width, FindRect.y+FindRect.height), CV_RGB(0,255,0), 2);
#endif
	//cvNamedWindow("Measure", CV_WINDOW_NORMAL);
	//cvShowImage("Measure",srcClone);   // 상관계수 이미지 보기
	//cvWaitKey(0);

	CvRect MeaserOutRect;
	MeaserOutRect = FindRect;
	//1) Up Limit찾기.
	for(int HStep= FindRect.y; HStep >= 0 ; HStep--)
	{
		bool FindAllBlackPos = true;
		for(int WStep= FindRect.x; WStep<FindRect.x + FindRect.width; WStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*srcClone->width));
			if(*pDataPointC <128)
			{
				FindAllBlackPos = false;
				break;
			}
		}
		if(FindAllBlackPos == true)
		{
			MeaserOutRect.y = HStep;
			break;
		}
		if(HStep == 0)
			MeaserOutRect.y = 0;
	}
	//2) Down Limit찾기.
	for(int HStep= FindRect.y+FindRect.height; HStep <srcClone->height ; HStep++)
	{
		bool FindAllBlackPos = true;
		for(int WStep= FindRect.x; WStep<FindRect.x + FindRect.width; WStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*srcClone->width));
			if(*pDataPointC <128)
			{
				FindAllBlackPos = false;
				break;
			}
		}
		if(FindAllBlackPos == true)
		{
			MeaserOutRect.height = HStep-MeaserOutRect.y;
			break;
		}
		if(HStep == 0)
			MeaserOutRect.height = FindRect.height;
	}
	//3) Left Limit찾기.
	for(int WStep= FindRect.x; WStep >= 0; WStep--)
	{
		bool FindAllBlackPos = true;
		for(int HStep= FindRect.y; HStep<FindRect.y+FindRect.height; HStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*srcClone->width));
			if(*pDataPointC <128)
			{
				FindAllBlackPos = false;
				break;
			}
		}
		if(FindAllBlackPos == true)
		{
			MeaserOutRect.x = WStep;
			break;
		}
		if(WStep == 0)
			MeaserOutRect.x  = 0;
	}
	//4) Right Limit찾기.
	for(int WStep= FindRect.x+FindRect.width; WStep < srcClone->width; WStep++)
	{
		bool FindAllBlackPos = true;
		for(int HStep= FindRect.y; HStep<FindRect.y+FindRect.height; HStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*srcClone->width));
			if(*pDataPointC <128)
			{
				FindAllBlackPos = false;
				break;
			}
		}
		if(FindAllBlackPos == true)
		{
			MeaserOutRect.width = WStep-MeaserOutRect.x;
			break;
		}
		if(WStep == 0)
			MeaserOutRect.width = FindRect.width;
	}

	//최 외각 Line Draw
#ifdef SIMUL_REVIEW_LENS_CALC
	cvDrawRect(srcClone, cvPoint(MeaserOutRect.x, MeaserOutRect.y), cvPoint(MeaserOutRect.x +MeaserOutRect.width, MeaserOutRect.y+MeaserOutRect.height), CV_RGB(0,0,255), 2);
#endif

	//////////////////////////////////////////////////////////////////////////
	//측정 범위 찾기 전부 화이트인것
	CvRect MeaserInRect;
	MeaserInRect = MeaserOutRect;
	//1) Up Limit찾기.
	for(int HStep= MeaserOutRect.y; HStep < FindRect.y ; HStep++)
	{
		bool FindAllBlackPos = true;
		for(int WStep= FindRect.x; WStep<FindRect.x + (MeaserOutRect.width/2); WStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*srcClone->width));
			if(*pDataPointC >128)
			{
				FindAllBlackPos = false;
				break;
			}
		}
		if(FindAllBlackPos == true)
		{
			MeaserInRect.y = HStep;
			break;
		}
		if(HStep == 0)
			MeaserInRect.y = 0;
	}
	//2) Down Limit찾기.
	for(int HStep= MeaserOutRect.y+MeaserOutRect.height; HStep > FindRect.y ; HStep--)
	{
		bool FindAllBlackPos = true;
		for(int WStep= FindRect.x; WStep<FindRect.x + (MeaserOutRect.width/2); WStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*srcClone->width));
			if(*pDataPointC >128)
			{
				FindAllBlackPos = false;
				break;
			}
		}
		if(FindAllBlackPos == true)
		{
			MeaserInRect.height = HStep-MeaserInRect.y;
			break;
		}
		if(HStep == 0)
			MeaserInRect.height = FindRect.height;
	}
	//3) Left Limit찾기.
	for(int WStep= MeaserOutRect.x; WStep<FindRect.x; WStep++)
	{
		bool FindAllBlackPos = true;
		for(int HStep= FindRect.y; HStep<FindRect.y+(MeaserOutRect.height/2); HStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*srcClone->width));
			if(*pDataPointC > 128)
			{
				FindAllBlackPos = false;
				break;
			}
		}
		if(FindAllBlackPos == true)
		{
			MeaserInRect.x = WStep;
			break;
		}
		if(WStep == 0)
			MeaserInRect.x  = 0;
	}
	//4) Right Limit찾기.
	for(int WStep= MeaserOutRect.x+MeaserOutRect.width; WStep > FindRect.x; WStep--)
	{
		bool FindAllBlackPos = true;
		for(int HStep= FindRect.y; HStep<FindRect.y+(MeaserOutRect.height/2); HStep++)
		{
			BYTE *pDataPointC =  (BYTE *)(tmp->imageData+WStep+(HStep*srcClone->width));
			if(*pDataPointC > 128)
			{
				FindAllBlackPos = false;
				break;
			}
		}
		if(FindAllBlackPos == true)
		{
			MeaserInRect.width = WStep-MeaserInRect.x;
			break;
		}
		if(WStep == 0)
			MeaserInRect.width = FindRect.width;
	}
	//측정 범위 안쪽
#ifdef SIMUL_REVIEW_LENS_CALC
	cvDrawRect(srcClone, cvPoint(MeaserInRect.x, MeaserInRect.y), cvPoint(MeaserInRect.x +MeaserInRect.width, MeaserInRect.y+MeaserInRect.height), CV_RGB(255,0,255), 1);
#endif
	
	//측정 시작.
	//측정라인을 구하기위한 거리값이다.
	int MOffset = ((MeaserOutRect.height - MeaserInRect.height)/2)/4;
	if(MOffset<0 || (MeaserInRect.y-MOffset)<=0)
	{
		G_WriteInfo(Log_Worrying,"Call Error2");
		if(tmp != nullptr)
			cvReleaseImage(&tmp);
		tmp = nullptr;
		return -1;
	}
#ifdef SIMUL_REVIEW_LENS_CALC
	//cvDrawRect(srcClone, cvPoint(MeaserOutRect.x+MOffset, MeaserOutRect.y+MOffset), cvPoint(MeaserOutRect.x +MeaserOutRect.width-MOffset, MeaserOutRect.y+MeaserOutRect.height-MOffset), CV_RGB(0,0,0), 1);
	cvDrawRect(srcClone, cvPoint(MeaserInRect.x-MOffset, MeaserInRect.y-MOffset), cvPoint(MeaserInRect.x +MeaserInRect.width+MOffset, MeaserInRect.y+MeaserInRect.height+MOffset),  CV_RGB(0,0,0), 2);
#endif

	

//////////////////////////////////////////////////////////////////////////
//신규 측정
	int MStartXPos = MeaserOutRect.x+MeaserOutRect.width/2;//	Up/Down Line측정 Start Pos
	int MStartYPos = MeaserOutRect.y+MeaserOutRect.height/2;//	Left/Right Line측정 Start Pos

	int NewPosLongLineN[3][2];//좌로 
	int NewPosLongLineP[3][2];//우로 

	for(int IStep = 0; IStep<3; IStep++)
	{
		for(int IStep2 = 0; IStep2<2; IStep2++)
		{
			NewPosLongLineP[IStep][IStep2] =-1;
			NewPosLongLineN[IStep][IStep2] =-1;
		}
	}

	int NewPosLongLineN_Left[3][2];//좌로 
	int NewPosLongLineP_Left[3][2];//우로 

	for(int IStep = 0; IStep<3; IStep++)
	{
		for(int IStep2 = 0; IStep2<2; IStep2++)
		{
			NewPosLongLineP_Left[IStep][IStep2] =-1;
			NewPosLongLineN_Left[IStep][IStep2] =-1;
		}
	}

	int NewPosLongLineN_Down[3][2];//좌로 
	int NewPosLongLineP_Down[3][2];//우로 

	for(int IStep = 0; IStep<3; IStep++)
	{
		for(int IStep2 = 0; IStep2<2; IStep2++)
		{
			NewPosLongLineP_Down[IStep][IStep2] =-1;
			NewPosLongLineN_Down[IStep][IStep2] =-1;
		}
	}

	int NewPosLongLineN_Right[3][2];//좌로 
	int NewPosLongLineP_Right[3][2];//우로 

	for(int IStep = 0; IStep<3; IStep++)
	{
		for(int IStep2 = 0; IStep2<2; IStep2++)
		{
			NewPosLongLineP_Right[IStep][IStep2] =-1;
			NewPosLongLineN_Right[IStep][IStep2] =-1;
		}
	}

	int PointFind = 0;
	int UpDownFind = 0;
	int PointFind_N = 0;
	int UpDownFind_N = 0;

	//상부
	for(int FStep = 1; FStep<(MeaserInRect.width/2)-1; FStep++ )
	{
		//오른쪽 상승 Edge찾기
		//BYTE *pDataPN =  (BYTE *)(tmp->imageData+(MStartXPos+FStep-1)+(MeaserOutRect.y*srcClone->width));
		BYTE *pDataPC =  (BYTE *)(tmp->imageData+(MStartXPos+FStep   )+((MeaserInRect.y-MOffset)*srcClone->width));
		BYTE *pDataPP =  (BYTE *)(tmp->imageData+(MStartXPos+FStep+1)+((MeaserInRect.y-MOffset)*srcClone->width));
		if(*pDataPC == 0  && *pDataPP>128 && PointFind<3)//상승찾기
		{
			NewPosLongLineP[PointFind][UpDownFind++] = (MStartXPos+FStep);
		}
		if(*pDataPC > 128  && *pDataPP<=128 && PointFind<3)//하강찾기
		{
			NewPosLongLineP[PointFind++][UpDownFind] = (MStartXPos+FStep);
			UpDownFind = 0;
		}
		//왼쪽 
		//BYTE *pDataNP =  (BYTE *)(tmp->imageData+(MStartXPos-FStep+1)+(MeaserOutRect.y*srcClone->width));
		BYTE *pDataNC =  (BYTE *)(tmp->imageData+(MStartXPos-FStep   )+((MeaserInRect.y-MOffset)*srcClone->width));
		BYTE *pDataNN =  (BYTE *)(tmp->imageData+(MStartXPos-FStep-1)+((MeaserInRect.y-MOffset)*srcClone->width));
		if(*pDataNC <=128  && *pDataNN>128 && PointFind_N<3)//상승찾기
		{
			NewPosLongLineN[PointFind_N][UpDownFind_N++] = (MStartXPos-FStep);
		}
		if(*pDataNC > 128  && *pDataNN<=128 && PointFind_N<3)//하강찾기
		{
			NewPosLongLineN[PointFind_N++][UpDownFind_N] = (MStartXPos-FStep);
			UpDownFind_N = 0;
		}
	}

	//PointFind = 0;
	//UpDownFind = 0;
	//PointFind_N = 0;
	//UpDownFind_N = 0;
	////하단부 측정
	//for(int FStep = 1; FStep<(MeaserInRect.width/2)-1; FStep++ )
	//{
	//	//오른쪽 상승 Edge찾기
	//	//BYTE *pDataPN =  (BYTE *)(tmp->imageData+(MStartXPos+FStep-1)+(MeaserOutRect.y*srcClone->width));
	//	BYTE *pDataPC =  (BYTE *)(tmp->imageData+(MStartXPos+FStep   )+((MeaserInRect.height+MOffset)*srcClone->width));
	//	BYTE *pDataPP =  (BYTE *)(tmp->imageData+(MStartXPos+FStep+1)+((MeaserInRect.height+MOffset)*srcClone->width));
	//	if(*pDataPC == 0  && *pDataPP>128 && PointFind<3)//상승찾기
	//	{
	//		NewPosLongLineP_Down[PointFind][UpDownFind++] = (MStartXPos+FStep);
	//	}
	//	if(*pDataPC > 128  && *pDataPP<=128 && PointFind<3)//하강찾기
	//	{
	//		NewPosLongLineP_Down[PointFind++][UpDownFind] = (MStartXPos+FStep);
	//		UpDownFind = 0;
	//	}
	//	//왼쪽 
	//	//BYTE *pDataNP =  (BYTE *)(tmp->imageData+(MStartXPos-FStep+1)+(MeaserOutRect.y*srcClone->width));
	//	BYTE *pDataNC =  (BYTE *)(tmp->imageData+(MStartXPos-FStep   )+((MeaserInRect.height+MOffset)*srcClone->width));
	//	BYTE *pDataNN =  (BYTE *)(tmp->imageData+(MStartXPos-FStep-1)+((MeaserInRect.height+MOffset)*srcClone->width));
	//	if(*pDataNC <=128  && *pDataNN>128 && PointFind_N<3)//상승찾기
	//	{
	//		NewPosLongLineN_Down[PointFind_N][UpDownFind_N++] = (MStartXPos-FStep);
	//	}
	//	if(*pDataNC > 128  && *pDataNN<=128 && PointFind_N<3)//하강찾기
	//	{
	//		NewPosLongLineN_Down[PointFind_N++][UpDownFind_N] = (MStartXPos-FStep);
	//		UpDownFind_N = 0;
	//	}
	//}
	//


	PointFind = 0;
	UpDownFind = 0;
	PointFind_N = 0;
	UpDownFind_N = 0;
	//1) Left Pos 
	for(int FStep = 1; FStep<(MeaserInRect.height/2)-1; FStep++ )
	{
		//증가 상승 Edge찾기
		//BYTE *pDataPN =  (BYTE *)(tmp->imageData+(MStartXPos+FStep-1)+(MeaserOutRect.y*srcClone->width));
		BYTE *pDataPC =  (BYTE *)(tmp->imageData+(MeaserInRect.x-MOffset)+((MStartYPos+FStep)*srcClone->width));
		BYTE *pDataPP =  (BYTE *)(tmp->imageData+(MeaserInRect.x-MOffset)+((MStartYPos+FStep+1)*srcClone->width));
		if(*pDataPC == 0  && *pDataPP>128 && PointFind<3)//상승찾기
		{
			NewPosLongLineP_Left[PointFind][UpDownFind++] = (MStartYPos+FStep);
		}
		if(*pDataPC > 128  && *pDataPP<=128 && PointFind<3)//하강찾기
		{
			NewPosLongLineP_Left[PointFind++][UpDownFind] = (MStartYPos+FStep);
			UpDownFind = 0;
		}
		//감소 방향
		//BYTE *pDataNP =  (BYTE *)(tmp->imageData+(MStartXPos-FStep+1)+(MeaserOutRect.y*srcClone->width));
		BYTE *pDataNC =  (BYTE *)(tmp->imageData+(MeaserInRect.x-MOffset)+((MStartYPos-FStep)*srcClone->width));
		BYTE *pDataNN =  (BYTE *)(tmp->imageData+(MeaserInRect.x-MOffset)+((MStartYPos-FStep-1)*srcClone->width));
		if(*pDataNC <=128  && *pDataNN>128 && PointFind_N<3)//상승찾기
		{
			NewPosLongLineN_Left[PointFind_N][UpDownFind_N++] = (MStartYPos-FStep);
		}
		if(*pDataNC > 128  && *pDataNN<=128 && PointFind_N<3)//하강찾기
		{
			NewPosLongLineN_Left[PointFind_N++][UpDownFind_N] = (MStartYPos-FStep);
			UpDownFind_N = 0;
		}
	}
	PointFind = 0;
	UpDownFind = 0;
	PointFind_N = 0;
	UpDownFind_N = 0;
	//1) Right Pos 
	for(int FStep = 1; FStep<(MeaserInRect.height/2)-1; FStep++ )
	{
		//증가 상승 Edge찾기
		//BYTE *pDataPN =  (BYTE *)(tmp->imageData+(MStartXPos+FStep-1)+(MeaserOutRect.y*srcClone->width));
		BYTE *pDataPC =  (BYTE *)(tmp->imageData+(MeaserInRect.width+MOffset)+((MStartYPos+FStep)*srcClone->width));
		BYTE *pDataPP =  (BYTE *)(tmp->imageData+(MeaserInRect.width+MOffset)+((MStartYPos+FStep+1)*srcClone->width));
		if(*pDataPC == 0  && *pDataPP>128 && PointFind<3)//상승찾기
		{
			NewPosLongLineP_Right[PointFind][UpDownFind++] = (MStartYPos+FStep);
		}
		if(*pDataPC > 128  && *pDataPP<=128 && PointFind<3)//하강찾기
		{
			NewPosLongLineP_Right[PointFind++][UpDownFind] = (MStartYPos+FStep);
			UpDownFind = 0;
		}
		//감소 방향
		//BYTE *pDataNP =  (BYTE *)(tmp->imageData+(MStartXPos-FStep+1)+(MeaserOutRect.y*srcClone->width));
		BYTE *pDataNC =  (BYTE *)(tmp->imageData+(MeaserInRect.width+MOffset)+((MStartYPos-FStep)*srcClone->width));
		BYTE *pDataNN =  (BYTE *)(tmp->imageData+(MeaserInRect.width+MOffset)+((MStartYPos-FStep-1)*srcClone->width));
		if(*pDataNC <=128  && *pDataNN>128 && PointFind_N<3)//상승찾기
		{
			NewPosLongLineN_Right[PointFind_N][UpDownFind_N++] = (MStartYPos-FStep);
		}
		if(*pDataNC > 128  && *pDataNN<=128 && PointFind_N<3)//하강찾기
		{
			NewPosLongLineN_Right[PointFind_N++][UpDownFind_N] = (MStartYPos-FStep);
			UpDownFind_N = 0;
		}
	}
	char strPrintMsg[128];
	memset(strPrintMsg, 0x00, 128);
	double LineSizeUp  =0.0;
	double LineSizeLeft =0.0;


	//CTime time = CTime::GetCurrentTime();
	//if(isLive == false)
	//{
	//	CString strRetSavePath;
	//	strRetSavePath.Format("%s\\StandardScaler_%04d%02d%02d%02d%02d%02d.jpg", VS10Master_Result_PATH,time.GetYear(),time.GetMonth(), time.GetDay(), time.GetHour(),time.GetMinute(), time.GetSecond());
	//	G_fnCheckDirAndCreate(strRetSavePath.GetBuffer(strRetSavePath.GetLength()));
	//	cvSaveImage(strRetSavePath.GetBuffer(strRetSavePath.GetLength()), srcClone);
	//}

	//if(tmp != nullptr)
	//	cvReleaseImage(&tmp);
	//tmp = nullptr;


	//ProcTime = GetTickCount() - ProcTime;
	//return ProcTime;
//////////////////////////////////////////////////////////////////////////
	

	//int MStartXPos = MeaserOutRect.x+MeaserOutRect.width/2;//	Up/Down Line측정 Start Pos
	//int MStartYPos = MeaserOutRect.y+MeaserOutRect.height/2;//	Left/Right Line측정 Start Pos
	//중간에서 Serche시작 한다.
	//왼쪽과 업 방향은 하강 Edge를 오른쪽과 Down은 상승 Edge를 찾는다.
	int PosShortLineP[2][15];//좌우로 29 개 찾기.
	int PosShortLineN[2][15];//좌우로 29 개 찾기.
	//int PosLongLineN[2][3];//좌로 3 개 찾기.
	//int PosLongLineP[2][3];//우로 3 개 찾기.

	for(int IStep = 0; IStep<2; IStep++)
	{
		for(int IStep2 = 0; IStep2<15; IStep2++)
		{
			PosShortLineP[IStep][IStep2] =-1;
			PosShortLineN[IStep][IStep2] =-1;
		}
	}

	//for(int IStep = 0; IStep<2; IStep++)
	//{
	//	for(int IStep2 = 0; IStep2<3; IStep2++)
	//	{
	//		PosLongLineP[IStep][IStep2] =-1;
	//		PosLongLineN[IStep][IStep2] =-1;
	//	}
	//}

	int PLNCnt = 0;
	int PLPCnt = 0;
	int PLNSCnt = 0;
	int PLPSCnt = 0;
	//1) Up Pos 
	for(int FStep = 1; FStep<(MeaserInRect.width/2)-1; FStep++ )
	{
		//오른쪽 상승 Edge찾기
		//BYTE *pDataPN =  (BYTE *)(tmp->imageData+(MStartXPos+FStep-1)+(MeaserOutRect.y*srcClone->width));
		//BYTE *pDataPC =  (BYTE *)(tmp->imageData+(MStartXPos+FStep   )+((MeaserInRect.y-MOffset)*srcClone->width));
		//BYTE *pDataPP =  (BYTE *)(tmp->imageData+(MStartXPos+FStep+1)+((MeaserInRect.y-MOffset)*srcClone->width));
		//if(*pDataPC == 0  && *pDataPP>128 && PLPCnt<3)
		//{
		//	PosLongLineP[0][PLPCnt] = (MStartXPos+FStep+1);
		//	PLPCnt++;
		//}
		BYTE *pDataSPC =  (BYTE *)(tmp->imageData+(MStartXPos+FStep   )+((MeaserOutRect.y+MOffset)*srcClone->width));
		BYTE *pDataSPP =  (BYTE *)(tmp->imageData+(MStartXPos+FStep+1)+((MeaserOutRect.y+MOffset)*srcClone->width));
		if(*pDataSPC == 0  && *pDataSPP>128 && PLPSCnt<15-1)
		{
			PosShortLineP[0][PLPSCnt] = (MStartXPos+FStep+1);
			PLPSCnt++;
		}
		//왼쪽
		//BYTE *pDataNP =  (BYTE *)(tmp->imageData+(MStartXPos-FStep+1)+(MeaserOutRect.y*srcClone->width));
		//BYTE *pDataNC =  (BYTE *)(tmp->imageData+(MStartXPos-FStep   )+((MeaserInRect.y-MOffset)*srcClone->width));
		//BYTE *pDataNN =  (BYTE *)(tmp->imageData+(MStartXPos-FStep-1)+((MeaserInRect.y-MOffset)*srcClone->width));
		//if(*pDataNC >128  && *pDataNN==0 && PLNCnt<3)
		//{
		//	PosLongLineN[0][PLNCnt] = (MStartXPos-FStep-1);
		//	PLNCnt++;
		//}
		BYTE *pDataSNC =  (BYTE *)(tmp->imageData+(MStartXPos-FStep   )+((MeaserOutRect.y+MOffset)*srcClone->width));
		BYTE *pDataSNP =  (BYTE *)(tmp->imageData+(MStartXPos-FStep-1)+((MeaserOutRect.y+MOffset)*srcClone->width));
		if(*pDataSNC >128  && *pDataSNP==0 && PLNSCnt<15)
		{
			PosShortLineN[0][PLPSCnt] = (MStartXPos-FStep-1);
			PLNSCnt++;
		}
#ifdef SIMUL_REVIEW_LENS_CALC
		//cvLine(srcClone, cvPoint((MStartXPos+FStep   ), MeaserInRect.y), cvPoint((MStartXPos+FStep+1), MeaserInRect.y), CV_RGB(255,0,255), 2);
		//cvLine(srcClone, cvPoint((MStartXPos-FStep   ), MeaserInRect.y), cvPoint((MStartXPos-FStep-1), MeaserInRect.y), CV_RGB(255,0,255), 2);
		//왼쪽 하강 Edge 찾기.
		//cvSetImageROI(srcClone, MeaserOutRect);
		//cvNamedWindow("Measure", CV_WINDOW_NORMAL);
		//cvShowImage("Measure",srcClone);   // 상관계수 이미지 보기
		//cvResetImageROI(srcClone);
		//cvWaitKey(10);
#endif
	}
	PLNCnt = 0;
	PLPCnt = 0;
	PLNSCnt = 0;
	PLPSCnt = 0;
	//1) Left Pos 
	for(int FStep = 1; FStep<(MeaserInRect.height/2)-1; FStep++ )
	{
		//오른쪽 상승 Edge찾기
		//BYTE *pDataPN =  (BYTE *)(tmp->imageData+(MStartXPos+FStep-1)+(MeaserOutRect.y*srcClone->width));
		//BYTE *pDataPC =  (BYTE *)(tmp->imageData+(MeaserInRect.x-MOffset)+((MStartYPos+FStep)*srcClone->width));
		//BYTE *pDataPP =  (BYTE *)(tmp->imageData+(MeaserInRect.x-MOffset)+((MStartYPos+FStep+1)*srcClone->width));
		//if(*pDataPC == 0  && *pDataPP>128 && PLPCnt<3)
		//{
		//	PosLongLineP[1][PLPCnt] = (MStartYPos+FStep+1);
		//	PLPCnt++;
		//}
		BYTE *pDataSPC =  (BYTE *)(tmp->imageData+(MeaserOutRect.x+MOffset)+((MStartYPos+FStep)*srcClone->width));
		BYTE *pDataSPP =  (BYTE *)(tmp->imageData+(MeaserOutRect.x+MOffset)+((MStartYPos+FStep+1)*srcClone->width));
		if(*pDataSPC == 0  && *pDataSPP>128 && PLPSCnt<15-1)
		{
			PosShortLineP[1][PLPSCnt] = (MStartYPos+FStep+1);
			PLPSCnt++;
		}
		//BYTE *pDataNP =  (BYTE *)(tmp->imageData+(MStartXPos-FStep+1)+(MeaserOutRect.y*srcClone->width));
		//BYTE *pDataNC =  (BYTE *)(tmp->imageData+(MeaserInRect.x-MOffset)+((MStartYPos-FStep)*srcClone->width));
		//BYTE *pDataNN =  (BYTE *)(tmp->imageData+(MeaserInRect.x-MOffset)+((MStartYPos-FStep-1)*srcClone->width));
		//if(*pDataNC >128  && *pDataNN==0 && PLNCnt<3)
		//{
		//	PosLongLineN[1][PLNCnt] = (MStartYPos-FStep-1);
		//	PLNCnt++;
		//}
		BYTE *pDataSNC =  (BYTE *)(tmp->imageData+(MeaserOutRect.x+MOffset)+((MStartYPos-FStep)*srcClone->width));
		BYTE *pDataSNP =  (BYTE *)(tmp->imageData+(MeaserOutRect.x+MOffset)+((MStartYPos-FStep-1)*srcClone->width));
		if(*pDataSNC >128  && *pDataSNP==0 && PLNSCnt<15)
		{
			PosShortLineN[1][PLNSCnt] = (MStartYPos-FStep-1);
			PLNSCnt++;
		}
#ifdef SIMUL_REVIEW_LENS_CALC
		cvLine(srcClone, cvPoint(MeaserInRect.x, (MStartYPos+FStep   )), cvPoint(MeaserInRect.x, (MStartYPos+FStep+1)), CV_RGB(255,0,255), 2);
		cvLine(srcClone, cvPoint(MeaserInRect.x, (MStartYPos-FStep   )), cvPoint(MeaserInRect.x, (MStartYPos-FStep-1)), CV_RGB(255,0,255), 2);
		//왼쪽 하강 Edge 찾기.
		//cvSetImageROI(srcClone, MeaserOutRect);
		//cvNamedWindow("Measure", CV_WINDOW_NORMAL);
		//cvShowImage("Measure",srcClone);   // 상관계수 이미지 보기
		//cvResetImageROI(srcClone);
		//cvWaitKey(10);
#endif
	}
	//char strPrintMsg[128];
	memset(strPrintMsg, 0x00, 128);
	int nDrawIndex= 1;
	CvPoint CenterPos;
	CenterPos.x =  FindRect.x + FindRect.width/2;
	CenterPos.y =  FindRect.y + FindRect.height/2;
	//Long Line Draw
	//for(int DStep = 0; DStep<3;DStep++)
	//{
	//	cvLine(srcClone, cvPoint(PosLongLineN[0][DStep], MeaserInRect.y), cvPoint(PosLongLineN[0][DStep], MeaserInRect.y+FindRect.height/4), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, cvPoint(PosLongLineP[0][DStep], MeaserInRect.y), cvPoint(PosLongLineP[0][DStep], MeaserInRect.y+FindRect.height/4), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, CenterPos, cvPoint(PosLongLineN[0][DStep], MeaserInRect.y+FindRect.height/4), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, CenterPos, cvPoint(PosLongLineP[0][DStep], MeaserInRect.y+FindRect.height/4), CV_RGB(255,0,0), 2);

	//	cvLine(srcClone, cvPoint(MeaserInRect.x, PosLongLineN[1][DStep]), cvPoint(MeaserInRect.x+FindRect.width/4, PosLongLineN[1][DStep]), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, cvPoint(MeaserInRect.x, PosLongLineP[1][DStep]), cvPoint(MeaserInRect.x+FindRect.width/4, PosLongLineP[1][DStep]), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, CenterPos, cvPoint(MeaserInRect.x+FindRect.width/4, PosLongLineN[1][DStep]), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, CenterPos, cvPoint(MeaserInRect.x+FindRect.width/4, PosLongLineP[1][DStep]), CV_RGB(255,0,0), 2);
	//}
	//Small Line Draw
	for(int DStep = 0; DStep<12;DStep++)
	{
		cvLine(srcClone, cvPoint(PosShortLineN[0][DStep], MeaserOutRect.y), cvPoint(PosShortLineN[0][DStep], MeaserOutRect.y+FindRect.height/4), CV_RGB(0,0,255), 1);
		cvLine(srcClone, cvPoint(PosShortLineP[0][DStep], MeaserOutRect.y), cvPoint(PosShortLineP[0][DStep], MeaserOutRect.y+FindRect.height/4), CV_RGB(0,0,255), 1);

		cvLine(srcClone, CenterPos, cvPoint(PosShortLineN[0][DStep], MeaserOutRect.y+FindRect.height/4), CV_RGB(0,0,255), 1);
		cvLine(srcClone, CenterPos, cvPoint(PosShortLineP[0][DStep], MeaserOutRect.y+FindRect.height/4), CV_RGB(0,0,255), 1);

		cvLine(srcClone, cvPoint(MeaserOutRect.x, PosShortLineN[1][DStep]), cvPoint(MeaserOutRect.x+FindRect.width/4, PosShortLineN[1][DStep]), CV_RGB(0,0,255), 1);
		cvLine(srcClone, cvPoint(MeaserOutRect.x, PosShortLineP[1][DStep]), cvPoint(MeaserOutRect.x+FindRect.width/4, PosShortLineP[1][DStep]), CV_RGB(0,0,255), 1);

		cvLine(srcClone, CenterPos, cvPoint(MeaserOutRect.x+FindRect.width/4, PosShortLineN[1][DStep]), CV_RGB(0,0,255), 1);
		cvLine(srcClone, CenterPos, cvPoint(MeaserOutRect.x+FindRect.width/4, PosShortLineP[1][DStep]), CV_RGB(0,0,255), 1);
	}	

	int MaxLenPixelDisUp = 0;
	if(PosShortLineN[0][14]>0 && PosShortLineP[0][14-1]>0)
		MaxLenPixelDisUp = abs(PosShortLineN[0][14] - PosShortLineP[0][14-1]);

	int MaxLenPixelDisLeft = 0;
	if(PosShortLineN[1][14]>0 && PosShortLineP[1][14-1]>0)
		MaxLenPixelDisLeft = abs(PosShortLineN[1][14] - PosShortLineP[1][14-1]);
#ifdef SIMUL_REVIEW_LENS_CALC
	//IplImage *pSaveImgSize = cvCreateImage(cvSize(RESULT_SAVE_SIZE,RESULT_SAVE_SIZE), IPL_DEPTH_8U, 3);
	//cvSetImageROI(srcClone, MeaserOutRect);
	//cvResize(srcClone,  pSaveImgSize);
	//cvResetImageROI(srcClone);
#endif
	//측정 정보 표시.
	//sprintf_s(strPrintMsg, "Pixel SizeX: %0.3fum", fPixelSizeX);
	//cvPutText(pSaveImgSize, strPrintMsg, cvPoint(10,  pSaveImgSize->height-120), &G_cvDrawSmallFont, CV_RGB(0,0,0));
	sprintf_s(strPrintMsg, "Pixel Size X: %0.3fum, Y: %0.3fum", fPixelSizeX, fPixelSizeY);
	cvPutText(srcClone, strPrintMsg, cvPoint(10, srcClone->height-90), &G_cvDrawSmallFont, CV_RGB(0,0,0));
	
	/*double*/ LineSizeUp   = ((double)MaxLenPixelDisUp * fPixelSizeX)/(SCAILE_SMALL_LINE_CNT*2.0);
	/*double*/ LineSizeLeft = ((double)MaxLenPixelDisLeft * fPixelSizeY)/(SCAILE_SMALL_LINE_CNT*2.0);

	if(LineSizeUp>0.0)
		sprintf_s(strPrintMsg, "Up Result : %0.3fum", LineSizeUp);
	else
		sprintf_s(strPrintMsg, "Up Result : -----");
	//cvPutText(srcClone, strPrintMsg, cvPoint(MeaserOutRect.x+MeaserOutRect.width/2,  MeaserOutRect.y+MeaserOutRect.height-80), &G_cvDrawFont, CV_RGB(255,0,0));
	cvPutText(srcClone, strPrintMsg, cvPoint(10,  srcClone->height-60), &G_cvDrawFont, CV_RGB(255,0,0));

	if(LineSizeLeft>0.0)
		sprintf_s(strPrintMsg, "Left Result: %0.3fum", LineSizeLeft);
	else
		sprintf_s(strPrintMsg, "Left Result: -----");
	//cvPutText(srcClone, strPrintMsg, cvPoint(MeaserOutRect.x+MeaserOutRect.width/2, MeaserOutRect.y+MeaserOutRect.height-40), &G_cvDrawFont, CV_RGB(255,0,0));
	cvPutText(srcClone, strPrintMsg, cvPoint(10, srcClone->height-30), &G_cvDrawFont, CV_RGB(255,0,0));
	
	//상단 그리기
	double SingleSize =0.0;
	for(int DStep = 0; DStep<3;DStep++)
	{

		cvLine(srcClone, cvPoint(NewPosLongLineN[DStep][0], MeaserInRect.y), cvPoint(NewPosLongLineN[DStep][0], MeaserInRect.y+FindRect.height/4), CV_RGB(255,0,0), 2);
		cvLine(srcClone, cvPoint(NewPosLongLineN[DStep][1], MeaserInRect.y), cvPoint(NewPosLongLineN[DStep][1], MeaserInRect.y+FindRect.height/4), CV_RGB(255,0,0), 2);
		SingleSize   = ((double)abs(NewPosLongLineN[DStep][0]- NewPosLongLineN[DStep][1]) * fPixelSizeX);
		
		//int nTemp1;
		//int nTemp2;
		//int nTemp3;
		
		if(LineSizeUp>0.0)
		{
			double dTemp1;
			double dTemp2;
			dTemp1 = SingleSize - ((double)((int)(SingleSize * 10))/10.0);
			dTemp2 = (double)((int)(LineSizeUp*10.0))/10.0;
			sprintf_s(strPrintMsg, "%0.3fum", dTemp1+dTemp2);
		}
		else
			sprintf_s(strPrintMsg, "%0.3fum", SingleSize);
		
		cvPutText(srcClone, strPrintMsg, cvPoint(NewPosLongLineN[DStep][0], MeaserInRect.y), &G_cvDrawFont, CV_RGB(255,0,0));

		cvLine(srcClone, cvPoint(NewPosLongLineP[DStep][0], MeaserInRect.y), cvPoint(NewPosLongLineP[DStep][0], MeaserInRect.y+FindRect.height/4), CV_RGB(255,0,0), 2);
		cvLine(srcClone, cvPoint(NewPosLongLineP[DStep][1], MeaserInRect.y), cvPoint(NewPosLongLineP[DStep][1], MeaserInRect.y+FindRect.height/4), CV_RGB(255,0,0), 2);
		SingleSize   = ((double)abs(NewPosLongLineP[DStep][0]- NewPosLongLineP[DStep][1]) * fPixelSizeX);
		if(LineSizeUp>0.0)
		{
			double dTemp1;
			double dTemp2;
			dTemp1 = SingleSize - ((double)((int)(SingleSize * 10))/10.0);
			dTemp2 = (double)((int)(LineSizeUp*10.0))/10.0;
			sprintf_s(strPrintMsg, "%0.3fum", dTemp1+dTemp2);
		}
		else
			sprintf_s(strPrintMsg, "%0.3fum", SingleSize);
		cvPutText(srcClone, strPrintMsg, cvPoint(NewPosLongLineP[DStep][0], MeaserInRect.y), &G_cvDrawFont, CV_RGB(255,0,0));

	}
	////하단 그리기
	//SingleSize =0.0;
	//for(int DStep = 0; DStep<3;DStep++)
	//{

	//	cvLine(srcClone, cvPoint(NewPosLongLineN_Down[DStep][0], MeaserOutRect.height), cvPoint(NewPosLongLineN_Down[DStep][0], MeaserOutRect.height+FindRect.height/4), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, cvPoint(NewPosLongLineN_Down[DStep][1], MeaserOutRect.height), cvPoint(NewPosLongLineN_Down[DStep][1], MeaserOutRect.height+FindRect.height/4), CV_RGB(255,0,0), 2);
	//	SingleSize   = ((double)abs(NewPosLongLineN_Down[DStep][0]- NewPosLongLineN_Down[DStep][1]) * fPixelSizeX);

	//	int nTemp1;
	//	int nTemp2;
	//	int nTemp3;

	//	if(LineSizeUp>0.0)
	//	{
	//		double dTemp1;
	//		double dTemp2;
	//		dTemp1 = SingleSize - ((double)((int)(SingleSize * 10))/10.0);
	//		dTemp2 = (double)((int)(LineSizeUp*10.0))/10.0;
	//		sprintf_s(strPrintMsg, "%0.3fum", dTemp1+dTemp2);
	//	}
	//	else
	//		sprintf_s(strPrintMsg, "%0.3fum", SingleSize);

	//	cvPutText(srcClone, strPrintMsg, cvPoint(NewPosLongLineN_Down[DStep][0], MeaserOutRect.height), &G_cvDrawFont, CV_RGB(255,0,0));

	//	cvLine(srcClone, cvPoint(NewPosLongLineP_Down[DStep][0], MeaserOutRect.height), cvPoint(NewPosLongLineP_Down[DStep][0], MeaserOutRect.height+FindRect.height/4), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, cvPoint(NewPosLongLineP_Down[DStep][1], MeaserOutRect.height), cvPoint(NewPosLongLineP_Down[DStep][1], MeaserOutRect.height+FindRect.height/4), CV_RGB(255,0,0), 2);
	//	SingleSize   = ((double)abs(NewPosLongLineP_Down[DStep][0]- NewPosLongLineP_Down[DStep][1]) * fPixelSizeX);
	//	if(LineSizeUp>0.0)
	//	{
	//		double dTemp1;
	//		double dTemp2;
	//		dTemp1 = SingleSize - ((double)((int)(SingleSize * 10))/10.0);
	//		dTemp2 = (double)((int)(LineSizeUp*10.0))/10.0;
	//		sprintf_s(strPrintMsg, "%0.3fum", dTemp1+dTemp2);
	//	}
	//	else
	//		sprintf_s(strPrintMsg, "%0.3fum", SingleSize);
	//	cvPutText(srcClone, strPrintMsg, cvPoint(NewPosLongLineP_Down[DStep][0], MeaserInRect.height), &G_cvDrawFont, CV_RGB(255,0,0));

	//}

	//Left
	for(int DStep = 0; DStep<3;DStep++)
	{
		cvLine(srcClone, cvPoint(MeaserOutRect.x, NewPosLongLineN_Left[DStep][0]), cvPoint(MeaserOutRect.x+FindRect.width/4, NewPosLongLineN_Left[DStep][0]), CV_RGB(255,0,0), 2);
		cvLine(srcClone, cvPoint(MeaserOutRect.x, NewPosLongLineN_Left[DStep][1]), cvPoint(MeaserOutRect.x+FindRect.width/4, NewPosLongLineN_Left[DStep][1]), CV_RGB(255,0,0), 2);
		SingleSize   = ((double)abs(NewPosLongLineN_Left[DStep][0]- NewPosLongLineN_Left[DStep][1]) * fPixelSizeY);
		if(LineSizeLeft>0.0)
		{
			double dTemp1;
			double dTemp2;
			dTemp1 = SingleSize - ((double)((int)(SingleSize * 10))/10.0);
			dTemp2 = (double)((int)(LineSizeLeft*10.0))/10.0;
			sprintf_s(strPrintMsg, "%0.3fum", dTemp1+dTemp2);
		}
		else
			sprintf_s(strPrintMsg, "%0.3fum", SingleSize);
		cvPutText(srcClone, strPrintMsg, cvPoint( MeaserOutRect.x+FindRect.width/4, NewPosLongLineN_Left[DStep][0]), &G_cvDrawFont, CV_RGB(255,0,0));

		cvLine(srcClone, cvPoint(MeaserOutRect.x, NewPosLongLineP_Left[DStep][0]), cvPoint(MeaserOutRect.x+FindRect.width/4, NewPosLongLineP_Left[DStep][0]), CV_RGB(255,0,0), 2);
		cvLine(srcClone, cvPoint(MeaserOutRect.x, NewPosLongLineP_Left[DStep][1]), cvPoint(MeaserOutRect.x+FindRect.width/4, NewPosLongLineP_Left[DStep][1]), CV_RGB(255,0,0), 2);
		
		SingleSize   = ((double)abs(NewPosLongLineN_Left[DStep][0]- NewPosLongLineN_Left[DStep][1]) * fPixelSizeY);
		if(LineSizeLeft>0.0)
		{
			double dTemp1;
			double dTemp2;
			dTemp1 = SingleSize - ((double)((int)(SingleSize * 10))/10.0);
			dTemp2 = (double)((int)(LineSizeLeft*10.0))/10.0;
			sprintf_s(strPrintMsg, "%0.3fum", dTemp1+dTemp2);
		}
		else
			sprintf_s(strPrintMsg, "%0.3fum", SingleSize);
		cvPutText(srcClone, strPrintMsg, cvPoint( MeaserOutRect.x+FindRect.width/4, NewPosLongLineP_Left[DStep][0]), &G_cvDrawFont, CV_RGB(255,0,0));
	}
	//Right
	//for(int DStep = 0; DStep<3;DStep++)
	//{
	//	cvLine(srcClone, cvPoint(MeaserOutRect.width, NewPosLongLineN_Right[DStep][0]), cvPoint(MeaserOutRect.width+FindRect.width/4, NewPosLongLineN_Right[DStep][0]), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, cvPoint(MeaserOutRect.width, NewPosLongLineN_Right[DStep][1]), cvPoint(MeaserOutRect.width+FindRect.width/4, NewPosLongLineN_Right[DStep][1]), CV_RGB(255,0,0), 2);
	//	SingleSize   = ((double)abs(NewPosLongLineN_Right[DStep][0]- NewPosLongLineN_Right[DStep][1]) * fPixelSizeY);
	//	if(LineSizeLeft>0.0)
	//	{
	//		double dTemp1;
	//		double dTemp2;
	//		dTemp1 = SingleSize - ((double)((int)(SingleSize * 10))/10.0);
	//		dTemp2 = (double)((int)(LineSizeLeft*10.0))/10.0;
	//		sprintf_s(strPrintMsg, "%0.3fum", dTemp1+dTemp2);
	//	}
	//	else
	//		sprintf_s(strPrintMsg, "%0.3fum", SingleSize);
	//	cvPutText(srcClone, strPrintMsg, cvPoint( MeaserOutRect.width+FindRect.width/4, NewPosLongLineN_Left[DStep][0]), &G_cvDrawFont, CV_RGB(255,0,0));

	//	cvLine(srcClone, cvPoint(MeaserOutRect.width, NewPosLongLineP_Right[DStep][0]), cvPoint(MeaserOutRect.width+FindRect.width/4, NewPosLongLineP_Right[DStep][0]), CV_RGB(255,0,0), 2);
	//	cvLine(srcClone, cvPoint(MeaserOutRect.width, NewPosLongLineP_Right[DStep][1]), cvPoint(MeaserOutRect.width+FindRect.width/4, NewPosLongLineP_Right[DStep][1]), CV_RGB(255,0,0), 2);

	//	SingleSize   = ((double)abs(NewPosLongLineN_Right[DStep][0]- NewPosLongLineN_Right[DStep][1]) * fPixelSizeY);
	//	if(LineSizeLeft>0.0)
	//	{
	//		double dTemp1;
	//		double dTemp2;
	//		dTemp1 = SingleSize - ((double)((int)(SingleSize * 10))/10.0);
	//		dTemp2 = (double)((int)(LineSizeLeft*10.0))/10.0;
	//		sprintf_s(strPrintMsg, "%0.3fum", dTemp1+dTemp2);
	//	}
	//	else
	//		sprintf_s(strPrintMsg, "%0.3fum", SingleSize);
	//	cvPutText(srcClone, strPrintMsg, cvPoint( MeaserOutRect.width+FindRect.width/4, NewPosLongLineP_Right[DStep][0]), &G_cvDrawFont, CV_RGB(255,0,0));
	//}

	
	//cvSetImageROI(tmp, MeaserOutRect);
	//cvNamedWindow("MeasureT", CV_WINDOW_NORMAL);
	//cvShowImage("MeasureT",tmp);   // 상관계수 이미지 보기
	//cvResetImageROI(tmp);

	//cvNamedWindow("Measure", CV_WINDOW_NORMAL);
	//cvNamedWindow("Measure");
	//cvShowImage("Measure",srcClone);   // 상관계수 이미지 보기
	//cvWaitKey(10);

	CTime time = CTime::GetCurrentTime();
	if(isLive == false)
	{
		CString strRetSavePath;
		switch(LensIndex)
		{
		case REVIEW_LENS_2X:
			strRetSavePath.Format("%s\\Standard_Lens02x_%04d%02d%02d%02d%02d%02d.jpg", VS10Master_Result_PATH,time.GetYear(),time.GetMonth(), time.GetDay(), time.GetHour(),time.GetMinute(), time.GetSecond());
			break;
		case REVIEW_LENS_10X:
			strRetSavePath.Format("%s\\Standard_Lens10x_%04d%02d%02d%02d%02d%02d.jpg", VS10Master_Result_PATH,time.GetYear(),time.GetMonth(), time.GetDay(), time.GetHour(),time.GetMinute(), time.GetSecond());
			break;
		case REVIEW_LENS_20X:
			strRetSavePath.Format("%s\\Standard_Lens20x_%04d%02d%02d%02d%02d%02d.jpg", VS10Master_Result_PATH,time.GetYear(),time.GetMonth(), time.GetDay(), time.GetHour(),time.GetMinute(), time.GetSecond());
			break;
		}
		G_fnCheckDirAndCreate(strRetSavePath.GetBuffer(strRetSavePath.GetLength()));
		cvSaveImage(strRetSavePath.GetBuffer(strRetSavePath.GetLength()), srcClone);
	}

	//if(pSaveImgSize != nullptr)
	//	cvReleaseImage(&pSaveImgSize);
	//pSaveImgSize = nullptr;

	if(tmp != nullptr)
		cvReleaseImage(&tmp);
	tmp = nullptr;

	//if(srcClone != nullptr)
	//	cvReleaseImage(&srcClone);
	//srcClone = nullptr;
	
	ProcTime = GetTickCount() - ProcTime;
	return ProcTime;

}

#ifdef USE_CAM_DEVICE
MIL_INT MFTYPE DisplayErrorExt(MIL_INT HookType, MIL_ID EventId, void MPTYPE *UserDataPtr)
{
/*
	char 	ErrorMessageFunction_A[M_ERROR_MESSAGE_SIZE] = "";
	char 	ErrorMessageFunction_B[M_ERROR_MESSAGE_SIZE] = "";
	char 	ErrorMessageFunction_C[M_ERROR_MESSAGE_SIZE] = "";
	char 	ErrorMessageFunction_D[M_ERROR_MESSAGE_SIZE] = "";
	char 	ErrorMessageFunction_E[M_ERROR_MESSAGE_SIZE] = "";
	int 	ErrorMessageFunction_F;
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT_FCT , ErrorMessageFunction_A);
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT , ErrorMessageFunction_B);
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT_SUB_1 , ErrorMessageFunction_C);
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT_SUB_2 , ErrorMessageFunction_D);
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT_SUB_3 , ErrorMessageFunction_E);
	MappGetHookInfo(EventId, M_CURRENT_SUB_NB , &ErrorMessageFunction_F);
	//M_CURRENT_SUB_NB 
*/

//	CSoliosXCL* pDig = (CSoliosXCL *)UserDataPtr;
//	pDig->SetTrigger();
/*
	CString str_a, str_b, str_c="", str_d="", str_e=""; 
	str_a = ErrorMessageFunction_A;
	str_b = ErrorMessageFunction_B;
	if(ErrorMessageFunction_F<=1)
		str_c = ErrorMessageFunction_C;
	else if(ErrorMessageFunction_F<=2)
		str_d = ErrorMessageFunction_D;
	else if(ErrorMessageFunction_F<=3)
		str_e = ErrorMessageFunction_E;
	theLog.AddLog("Mil Error : "+str_a+" = "+str_b+" "+str_c+" "+str_d+" "+str_e);
	//theLog.Flush();
*/

	return M_NULL;
}
#endif

int CntDraw = 0;
void DrawInterlock(IplImage *pTarget, IplImage *pStateImg, bool *pState)
{
	if(pTarget == nullptr || pStateImg == nullptr)
		return;

	CvScalar StandbyColor = CV_RGB(0,255,0);
	CvScalar OrderColor = CV_RGB(255,0,0);
	CvScalar OrderColorTog = CV_RGB(0,0,255);

	CvPoint RectStart;
	RectStart.x = 80;
	RectStart.y = 28;
	sprintf_s(G_strProcMsg,128,"Review");
	if(pState[0] == true)
		cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, StandbyColor);
	else
	{
		if(CntDraw<3)
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColor);
		else
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColorTog);
	}
	RectStart.x = 65;
	RectStart.y += 44;
	sprintf_s(G_strProcMsg,128,"3D Scope");

	if(pState[1] == true)
		cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, StandbyColor);
	else
	{
		if(CntDraw<3)
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColor);
		else
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColorTog);
	}
	RectStart.x = 15;
	RectStart.y += 44;
	sprintf_s(G_strProcMsg,128,"Space Sensor");
	if(pState[2] == true)
		cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, StandbyColor);
	else
	{
		if(CntDraw<3)
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColor);
		else
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColorTog);
	}
	RectStart.x = 60;
	RectStart.y += 44;
	sprintf_s(G_strProcMsg,128,"Gripper Z");
	if(pState[3] == true)
		cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, StandbyColor);
	else
	{
		if(CntDraw<3)
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColor);
		else
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColorTog);
	}
	RectStart.x = 70;
	RectStart.y += 44;
	sprintf_s(G_strProcMsg,128,"Tension");
	if(pState[4] == true)
		cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, StandbyColor);
	else
	{
		if(CntDraw<3)
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColor);
		else
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColorTog);
	}
	RectStart.x = 15;
	RectStart.y += 44;
	if(pState[5] == true)
	{
		sprintf_s(G_strProcMsg,128,"<Tension Cyl>");
		cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, StandbyColor);
	}
	else
	{
		sprintf_s(G_strProcMsg,128,">Tension Cyl<");
		if(CntDraw<3)
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColor);
		else
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColorTog);
	}
	RectStart.x = 60;
	RectStart.y += 44;
	if(pState[6] == true)
	{
		sprintf_s(G_strProcMsg,128,"Grip Close");
		cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, StandbyColor);
	}
	else
	{
		sprintf_s(G_strProcMsg,128,"Grip Open");
		if(CntDraw<3)
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColor);
		else
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColorTog);
	}
	RectStart.x = 40;
	RectStart.y += 44;
	sprintf_s(G_strProcMsg,128,"------");
	if(pState[7] == true)
		cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, StandbyColor);
	else
	{
		if(CntDraw<3)
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColor);
		else
			cvPutText(pStateImg, G_strProcMsg, RectStart , &G_cvDrawFont, OrderColorTog);
	}
	//cvDrawRect(pTarget,RectStart, RectEnd,   CV_RGB(0, 255, 0), -1);
	//cvDrawRect(pTarget,RectStart, RectEnd,   CV_RGB(0, 0, 0), 2);
	//sprintf_s(G_strProcMsg,128,"test");
	//cvPutText(pTarget, G_strProcMsg, cvPoint(RectStart.x+5, RectEnd.y-10) , &G_cvDrawBigFont, CV_RGB(255,0,0));

	CvRect DestPos;
	DestPos.x =pTarget->width - pStateImg->width-3;
	DestPos.y = 3;
	DestPos.width = pStateImg->width;
	DestPos.height = pStateImg->height;
	cvSetImageROI(pTarget, DestPos);
	cvCopyImage(pStateImg, pTarget);
	cvResetImageROI(pTarget);
	//if(CntDraw<3)
	//	PutImage_TextBig(pTarget, cvPoint(pTarget->width-300,200), "Scope Pos", CV_RGB(255, 0, 0), CV_RGB(0, 0, 255), CV_RGB(0, 0, 255));
	//else
	//	PutImage_TextBig(pTarget, cvPoint(pTarget->width-300,200), "Scope Pos", CV_RGB(0, 0, 255), CV_RGB(255, 0, 0),  CV_RGB(0, 0, 255));

	//if(CntDraw<3)
	//	PutImage_TextBig(pTarget, cvPoint(pTarget->width-300,100), "Tension Pos", CV_RGB(255, 0, 0), CV_RGB(0, 0, 255), CV_RGB(0, 0, 255));
	//else
	//	PutImage_TextBig(pTarget, cvPoint(pTarget->width-300,100), "Tension Pos", CV_RGB(0, 0, 255), CV_RGB(255, 0, 0),  CV_RGB(0, 0, 255));

	CntDraw++;
	if(CntDraw>6)
		CntDraw =0;
}

void OverlayAutoReviewInfo(IplImage *pTargetView, CLiveViewCtrl *pCtrlObj)
{
	CvRect AutoReviewArea;
	AutoReviewArea.x =0;
	AutoReviewArea.y = pCtrlObj->m_pLiveViewImg->height - (pCtrlObj->m_pAutoReviewImg->height+pCtrlObj->m_pAutoReviewImg->height/5);
	AutoReviewArea.width = pCtrlObj->m_pAutoReviewImg->width;
	AutoReviewArea.height = pCtrlObj->m_pAutoReviewImg->height;

	cvSetImageROI(pTargetView, AutoReviewArea);
	cvCopyImage(pCtrlObj->m_pAutoReviewImg, pTargetView);
	cvResetImageROI(pTargetView);

	if(pCtrlObj->m_pAutoReviewSmallImg!=nullptr)
	{
		CvRect SmallImgArea;
		SmallImgArea.x = pTargetView->width - (pCtrlObj->m_pAutoReviewSmallImg->width+2);
		SmallImgArea.y = pTargetView->height - (pCtrlObj->m_pAutoReviewSmallImg->height+2);
		SmallImgArea.width = pCtrlObj->m_pAutoReviewSmallImg->width;
		SmallImgArea.height = pCtrlObj->m_pAutoReviewSmallImg->height;

		cvSetImageROI(pTargetView, SmallImgArea);
		cvCopyImage(pCtrlObj->m_pAutoReviewSmallImg, pTargetView);
		cvResetImageROI(pTargetView);

		cvDrawRect(pTargetView, 
			cvPoint(SmallImgArea.x, SmallImgArea.y), 
			cvPoint(SmallImgArea.x+SmallImgArea.width, SmallImgArea.y+SmallImgArea.height),
			CV_RGB(255,255,0), 4);
	}

}
void OverlayActionInfo(IplImage *pTargetView)
{
}
void OverlayCDTPInfo(IplImage *pTargetView, CLiveViewCtrl *pCtrlObj)
{
	CvPoint DrawTxtPt;
	if(pCtrlObj->m_CDPos.width>0 && pCtrlObj->m_CDPos.height>0)
	{
		cvDrawRect(pTargetView, 
		cvPoint(pCtrlObj->m_CDPos.x, pCtrlObj->m_CDPos.y), 
		cvPoint(pCtrlObj->m_CDPos.x+pCtrlObj->m_CDPos.width, pCtrlObj->m_CDPos.y+pCtrlObj->m_CDPos.height),
		CV_RGB(255,0,255), 4);

		DrawTxtPt = cvPoint(pCtrlObj->m_CDPos.x, pCtrlObj->m_CDPos.y);
	}

	if(pCtrlObj->m_TPPos.width>0 && pCtrlObj->m_TPPos.height>0)
	{
		cvDrawRect(pTargetView, 
		cvPoint(pCtrlObj->m_TPPos.x, pCtrlObj->m_TPPos.y), 
		cvPoint(pCtrlObj->m_TPPos.x+pCtrlObj->m_TPPos.width, pCtrlObj->m_TPPos.y+pCtrlObj->m_TPPos.height),
		CV_RGB(0,255,255), 4);
		DrawTxtPt = cvPoint(pCtrlObj->m_TPPos.x, pCtrlObj->m_TPPos.y);
	}

	sprintf_s(G_strProcMsg,128,"%s", pCtrlObj->m_strCDTPInfo.GetBuffer(pCtrlObj->m_strCDTPInfo.GetLength()));
	cvPutText(pTargetView, G_strProcMsg, DrawTxtPt , &G_cvDrawBigFont, CV_RGB(255,255,0));
}

UINT Thread_ReviewGrabProc(LPVOID pParam)
{
	IplImage *pStateView = cvLoadImage("D:\\nBiz_InspectStock\\Data\\SysHWParam\\MotStateView.bmp");
	IplImage *pStateViewBuf = cvCloneImage(pStateView);//cvLoadImage("D:\\nBiz_InspectStock\\Data\\SysHWParam\\MotStateView.bmp");
	CLiveViewCtrl *pCtrlObj = (CLiveViewCtrl *)pParam;

	CvPoint MLineS, MLineE;
	CvPoint MLineS2, MLineE2;

	MLineS.x = 0; 
	MLineS.y = pCtrlObj->m_pLiveViewImg->height/2;

	MLineE.x = pCtrlObj->m_pLiveViewImg->width; 
	MLineE.y = pCtrlObj->m_pLiveViewImg->height/2;	

	MLineS2.x = pCtrlObj->m_pLiveViewImg->width/2; 
	MLineS2.y = 0;
	MLineE2.x = pCtrlObj->m_pLiveViewImg->width/2; 
	MLineE2.y = pCtrlObj->m_pLiveViewImg->height;

	CRect nViewRect;
	CWnd* pWnd = CWnd::FromHandle(pCtrlObj->m_UpdateWindHandle);
	pWnd->GetClientRect(nViewRect);		
		
	float hscale      = 1.0f;
	float vscale      = 1.0f;
	float italicscale = 0.0f;
	int  thickness    = 2;
	cvInitFont (&G_cvDrawFont, CV_FONT_HERSHEY_SIMPLEX , hscale, vscale, italicscale, thickness, CV_AA);

	hscale      = 0.4f;
	vscale      = 0.4f;
	italicscale = 0.0f;
	thickness    = 1;
	cvInitFont (&G_cvDrawSmallFont, CV_FONT_HERSHEY_COMPLEX , hscale, vscale, italicscale, thickness, CV_AA);
	
	hscale      = 1.4f;
	vscale      = 1.4f;
	italicscale = 0.0f;
	thickness    = 2;
	cvInitFont (&G_cvDrawBigFont, CV_FONT_HERSHEY_COMPLEX , hscale, vscale, italicscale, thickness, CV_AA);

	hscale      = 2.4f;
	vscale      = 2.4f;
	italicscale = 0.5f;
	thickness    = 2;
	cvInitFont (&G_cvDrawVBigFont, CV_FONT_HERSHEY_COMPLEX , hscale, vscale, italicscale, thickness, CV_AA);
	
	//cvInitFont (&G_ImageInfoBFont, CV_FONT_HERSHEY_SIMPLEX , 1.0f, 1.2f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	int nGrabCnt = 0;

	HDC pDisplayhDC	= ::GetDC(pCtrlObj->m_UpdateWindHandle);	
	CDC *pDisplayDc	= CDC::FromHandle(pDisplayhDC);	
	IplImage *pGrabBufferBayer = cvCreateImage(cvSize(pCtrlObj->m_pLiveViewImg->width, pCtrlObj->m_pLiveViewImg->height), IPL_DEPTH_8U, 1);
	cvZero(pGrabBufferBayer);
	CvvImage ViewImage;
	DWORD GrabTime = GetTickCount();
	DWORD GrabCnt = GrabTime;

	CvScalar ScopeSafetyColor= CV_RGB(255,0,0);
	int ScopeSafetyCntView =0;

	//DWORD SizeProctTime = 0;
	while (pCtrlObj->m_RunThread == true)
	{
//#ifdef USE_CAM_DEVICE
		if(pCtrlObj->m_bLiveViewOnOff == true)
//#endif
		{
#ifdef USE_CAM_DEVICE
			pCtrlObj->CamGrab((char*)pGrabBufferBayer->imageData);
			cvCvtColor( pGrabBufferBayer, pCtrlObj->m_pLiveViewImg, CV_BayerRG2BGR);
#else
			if(pCtrlObj->m_pLiveViewImg != nullptr)
				cvReleaseImage(&pCtrlObj->m_pLiveViewImg);
			pCtrlObj->m_pLiveViewImg = cvLoadImage("D:\\nBiz_InspectStock\\Data\\Resource\\VS10Master\\TestImgReview.bmp");
#endif
			
			//Count View
			ViewImage.CopyOf(pCtrlObj->m_pLiveViewImg);
			
			if(pCtrlObj->m_bViewReviewCalc == true)
			{
				ScaleMeasuer(ViewImage.GetImage(), 1,pCtrlObj->m_fPixelSizeX, pCtrlObj->m_fPixelSizeY, 300, false);
			}
			if(pCtrlObj->m_bViewCenterLine == true)
			{
				cvLine(ViewImage.GetImage(), MLineS, MLineE, CV_RGB(255,0,0), 2);
				cvLine(ViewImage.GetImage(), MLineS2, MLineE2, CV_RGB(255,0,0), 2);	
			}
			//Display Ruler Line
			if(pCtrlObj->m_bViewRulerLine == true)
				pCtrlObj->DrawRuler(ViewImage.GetImage());
			
			sprintf_s(G_strProcMsg,128,"Shift:[%dum], Drive:[%dum]", pCtrlObj->m_ReviewStagePos.x, pCtrlObj->m_ReviewStagePos.y);
			//sprintf_s(G_strProcMsg,128,"Shift:[%dum], Drive:[%dum][ProcTime: %d]", pCtrlObj->m_ReviewStagePos.x, pCtrlObj->m_ReviewStagePos.y, SizeProctTime);
			cvPutText(ViewImage.GetImage(), G_strProcMsg, cvPoint(10, 50), &G_cvDrawBigFont, CV_RGB(255,0,0));
			//SizeProctTime = 0;
			//Draw Interlock
			if(pCtrlObj->m_bDisplayInterlock == true)
			{
				DrawInterlock(ViewImage.GetImage(), pStateView, &pCtrlObj->m_bSafetyPos[0]);
				cvCopyImage(pStateViewBuf, pStateView);
			}

			//Image Save
			if(pCtrlObj->m_bSaveImage == true)
			{
				if(pCtrlObj->m_ppNewImg !=nullptr)
				{
					//cvLine(pCtrlObj->m_pLiveViewImg, MLineS, MLineE, CV_RGB(255,0,0), 1);
					//cvLine(pCtrlObj->m_pLiveViewImg, MLineS2, MLineE2, CV_RGB(255,0,0), 1);	

					if(pCtrlObj->m_bSaveOverlayImage == true)
						*pCtrlObj->m_ppNewImg = cvCloneImage(ViewImage.GetImage());
					else
						*pCtrlObj->m_ppNewImg = cvCloneImage(pCtrlObj->m_pLiveViewImg);

					pCtrlObj->m_ppNewImg = nullptr;
				}
				if(strlen(pCtrlObj->m_strSavePath)>5)
				{
					cvSaveImage(pCtrlObj->m_strSavePath, pCtrlObj->m_pLiveViewImg);
					memset(pCtrlObj->m_strSavePath, 0x00, MAX_PATH);
				}
				pCtrlObj->m_bSaveImage = false;
			}
			//이후로는 File로 저장 할수 없다.
			//Mouse Point View
			cvLine(ViewImage.GetImage(),	cvPoint(pCtrlObj->m_MousePos.x-200, pCtrlObj->m_MousePos.y), 
				cvPoint(pCtrlObj->m_MousePos.x+200, pCtrlObj->m_MousePos.y),CV_RGB(255,0,0), 2);
			cvLine(ViewImage.GetImage(),	cvPoint(pCtrlObj->m_MousePos.x, pCtrlObj->m_MousePos.y-200), 
				cvPoint(pCtrlObj->m_MousePos.x, pCtrlObj->m_MousePos.y+200),CV_RGB(255,0,0), 2);

			if(pCtrlObj->m_bAutoReviewInfo == true)
				OverlayAutoReviewInfo(ViewImage.GetImage(), pCtrlObj);
			if(pCtrlObj->m_bCDTPReviewInfo == true)
				OverlayCDTPInfo(ViewImage.GetImage(), pCtrlObj);

			if(pCtrlObj->m_bScopeSafetyPos == false)
			{
				ScopeSafetyCntView++;
				if(ScopeSafetyCntView<5)
				{
					ScopeSafetyColor = CV_RGB(255,0,0);
				}
				else if(ScopeSafetyCntView<10)
				{
					ScopeSafetyColor = CV_RGB(255,255,0);
				}
				else 
					ScopeSafetyCntView=0;

				cvPutText(ViewImage.GetImage(), "Scope Not Safety Position!!", 
					cvPoint((ViewImage.GetImage()->width/2)-550, ViewImage.GetImage()->height/2+100), 
					&G_cvDrawVBigFont, ScopeSafetyColor);
			}

			if(pCtrlObj->m_bLiveViewOnOff == true)
			{
				//ViewImage.Show(pDisplayDc->m_hDC, 0, 0, nViewRect.Width(), nViewRect.Height());
				ViewImage.DrawToHDC(pDisplayDc->m_hDC, nViewRect);	
			}
		}
#ifdef USE_CAM_DEVICE
		Sleep(10);
#else
		Sleep(60);
#endif
	}
	ViewImage.Destroy();	
	ReleaseDC(pCtrlObj->m_UpdateWindHandle, pDisplayhDC);

	if(pGrabBufferBayer != nullptr)
		cvReleaseImage(&pGrabBufferBayer);

	if(pStateView != nullptr)
		cvReleaseImage(&pStateView);
	
	if(pStateViewBuf != nullptr)
		cvReleaseImage(&pStateViewBuf);
	
	pCtrlObj->m_RunThread = false;
	pCtrlObj->m_pHandleThread = nullptr;
	return 1;
}

CLiveViewCtrl::CLiveViewCtrl(void)
{
	m_RunThread = false;
	m_pHandleThread = nullptr;

	m_pLiveViewImg = nullptr;
	m_UpdateWindHandle = 0;

	m_bViewCenterLine = true;
	m_bViewReviewCalc = false;
	m_bViewRulerLine = false;

	m_bAutoReviewInfo = false;
	m_bCDTPReviewInfo = false;
	m_pAutoReviewImg = nullptr;
	m_pAutoReviewSmallImg = nullptr;

	m_NowLensIndex = REVIEW_LENS_2X;
	m_fPixelSizeX = 0.175f;
	m_fPixelSizeY = 0.175f;
	m_fRulerScale = 5.0f;

	m_bSaveImage = false;
	m_bSaveOverlayImage =  false;
	memset(m_strSavePath, 0x00, MAX_PATH);
	m_ppNewImg = nullptr;
	m_bLiveViewOnOff = true;

	m_MousePos.x = 0;
	m_MousePos.y = 0;
	m_ReviewStagePos.x = 0;
	m_ReviewStagePos.y = 0;
	
	m_bDisplayInterlock = true;
	for(int SStep= 0; SStep<8; SStep++)
		m_bSafetyPos[SStep] = false;

	m_bScopeSafetyPos = false;
}


CLiveViewCtrl::~CLiveViewCtrl(void)
{
	m_RunThread = false;
	while(m_pHandleThread != nullptr)
	{
		Sleep(10);
	}
	cvReleaseImage(&m_pLiveViewImg);
	m_pLiveViewImg = nullptr;

	cvReleaseImage(&m_pAutoReviewImg);
	m_pAutoReviewImg = nullptr;

	cvReleaseImage(&m_pAutoReviewSmallImg);
	m_pAutoReviewSmallImg = nullptr;
}
void CLiveViewCtrl::SaveImage(char *pSaveFullPath)
{
	sprintf_s(m_strSavePath, "%s", pSaveFullPath);
	m_bSaveImage = true;
	
}
void CLiveViewCtrl::SaveImage(IplImage **ppNewImg)
{
	m_ppNewImg = ppNewImg;
	m_bSaveImage = true;
	
}
bool CLiveViewCtrl::InitCamCtrl(int nBoardId, int nDeviceId, HWND UpdateWindHandle, char *pDcfFile)
{
	m_UpdateWindHandle = UpdateWindHandle;
#ifdef USE_CAM_DEVICE
	MappAlloc(M_DEFAULT, &MilApplication);
	MappHookFunction(M_ERROR_CURRENT, DisplayErrorExt,this);
	MsysAlloc(M_SYSTEM_SOLIOS, nBoardId, M_SETUP, &MilSystem);//SOLIOS보드를 사용하는 경우 -jscarlet
	//MsysAlloc(M_SYSTEM_HELIOS, nBoardId, M_SETUP, &MilSystem);//HELIOS보드를 사용하는 경우 
	//MsysControl(MilSystem,M_PROCESSING_SYSTEM ,M_DEFAULT_HOST);

	if ( pDcfFile == nullptr )
		MdigAlloc(MilSystem,M_DEFAULT,M_CAMERA_SETUP,M_DEFAULT,&MilDigitizer);
	else 
		MdigAlloc(MilSystem,nDeviceId,(LPSTR)(LPCTSTR)pDcfFile, M_DEFAULT, &MilDigitizer);

	m_nDeviceId = nDeviceId;

	MdigControl(MilDigitizer, M_GRAB_MODE, M_ASYNCHRONOUS);


	MdigInquire(MilDigitizer,M_SIZE_X,&DigSizeX);
	MdigInquire(MilDigitizer,M_SIZE_Y,&DigSizeY);  	
	MdigInquire(MilDigitizer,M_SIZE_BAND,&Band);

	m_pLiveViewImg = cvCreateImage(cvSize((int)DigSizeX,(int)DigSizeY), IPL_DEPTH_8U, 3);
	cvZero(m_pLiveViewImg);

	m_pAutoReviewImg = cvCreateImage(cvSize((int)DigSizeX, 400), IPL_DEPTH_8U, 3);
	cvZero(m_pAutoReviewImg);



	for (int i = 0 ; i < 2 ; i++)
	{
		MbufAllocColor(MilSystem, 1, DigSizeX, DigSizeY,
			M_DEF_IMAGE_TYPE,
			M_IMAGE + M_GRAB + M_PROC,
			&MilImage[i]);
	}

	//if (MilImageDisplay == M_NULL)
	//{
	//	MbufAllocColor(MilSystem, 1, DigSizeX, DigSizeY,
	//		M_DEF_IMAGE_TYPE,
	//		M_IMAGE + M_GRAB + M_PROC + M_DISP ,
	//		&MilImageDisplay);
	//}
	//else 
	//	return false;

	MbufClear(MilImage[0],0);
	MbufClear(MilImage[1],0);
	//MbufClear(MilImageDisplay,0);

	//if (MilDisplay == M_NULL)
	//{
	//	MdispAlloc(MilSystem,M_DEFAULT, M_DISPLAY_SETUP, M_WINDOWED, &MilDisplay);
	//}
	//else
	//	return false;
#else
	//m_pLiveViewImg = cvCreateImage(cvSize((int)REAL_IMAGE_SIZE_X,(int)REAL_IMAGE_SIZE_Y), IPL_DEPTH_8U, 3);
	m_pLiveViewImg = cvLoadImage("D:\\nBiz_InspectStock\\Data\\Resource\\VS10Master\\TestImgReview.bmp");

#endif


	//SetAutoRevewInfo(0, 0, 	"None", "None", nullptr, cvPoint(0, 0));
	//Grab Thread Run.
	m_RunThread = true;
	m_pHandleThread = ::AfxBeginThread(Thread_ReviewGrabProc, this,	 THREAD_PRIORITY_NORMAL);
	return true;
}
void CLiveViewCtrl::CamGrab(char* chImage)
{
#ifdef USE_CAM_DEVICE
	MdigControl(MilDigitizer, M_GRAB_MODE, M_SYNCHRONOUS);
	MdigGrab(MilDigitizer, MilImage[0]);
	MbufGet(MilImage[0], (BYTE*)chImage);
#else
	m_pLiveViewImg = cvLoadImage("D:\\nBiz_InspectStock\\Data\\Resource\\VS10Master\\TestImgReview.bmp");
#endif
}
void CLiveViewCtrl::ReleaseCamCtrl()
{
#ifdef USE_CAM_DEVICE
	MdigControl(MilDigitizer, M_GRAB_ABORT, M_DEFAULT);
	MbufClear(MilImage[0],0);
	MbufClear(MilImage[1],0);

	MdigHalt(MilDigitizer);
	::Sleep(50);
	for ( int i = 0; i < 2; i++) 
	{
		if ( MilImage[i] != M_NULL ) 
		{
			MbufFree(MilImage[i]);
			MilImage[i] = M_NULL;
		}
	}
	////	TRACE("MilByerCoef\n");
	//if(MilDisplay != M_NULL && MilImageDisplay!= M_NULL)
	//{
	//	MdispDeselect(MilDisplay, MilImageDisplay);
	//	//		TRACE("MilDisplay_MilImageDisplay\n");
	//}
	//if(MilImageDisplay != M_NULL )
	//{
	//	MbufFree(MilImageDisplay);
	//	MilImageDisplay = M_NULL;
	//	//		TRACE("MilImageDisplay\n");
	//}
	//if(MilDisplay != M_NULL )
	//{
	//	MdispFree(MilDisplay);
	//	MilDisplay = M_NULL;
	//	//		TRACE("MilDisplay\n");
	//}
	if(MilDigitizer != M_NULL)
	{
		MdigFree(MilDigitizer);
		MilDigitizer = M_NULL;
		//		TRACE("MilDigitizer\n");
	}

	MsysFree(MilSystem);
	MilSystem = M_NULL;
	if(MilApplication != M_NULL)
	{
		MappFree(MilApplication);
		MilApplication = M_NULL;
	}
#endif
}	

void CLiveViewCtrl::DrawRuler(IplImage *pTarget)
{

	int StartX = pTarget->width/2;
	int StartY = pTarget->height/2;
	CvPoint LineStart, LineEnd;
	LineStart.x = StartX;
	LineStart.y = StartY-10;
	LineEnd.x = StartX;
	LineEnd.y = StartY+10;
	int nDrawCnt = 0;
	for (int DStep = 0; DStep<pTarget->width/2; DStep++)
	{
		if((int)((m_fPixelSizeX*DStep)/m_fRulerScale)==nDrawCnt)
		{
			nDrawCnt++;
			LineStart.x =StartX+DStep;
			LineEnd.x =StartX+DStep;
			cvDrawLine(pTarget, LineStart, LineEnd, CV_RGB(255,0,0),2);

			LineStart.x =StartX-DStep;
			LineEnd.x =StartX-DStep;
			cvDrawLine(pTarget, LineStart, LineEnd, CV_RGB(255,0,0),2);
		}
	}
	LineStart.x = StartX-10;
	LineStart.y = StartY;
	LineEnd.x = StartX+10;
	LineEnd.y = StartY;

	nDrawCnt = 0;
	for (int DStep = 0; DStep<pTarget->height/2; DStep++)
	{
		if((int)((m_fPixelSizeY*DStep)/m_fRulerScale)==nDrawCnt)
		{
			nDrawCnt++;
			LineStart.y =StartY+DStep;
			LineEnd.y =StartY+DStep;
			cvDrawLine(pTarget, LineStart, LineEnd, CV_RGB(255,0,0),2);

			LineStart.y =StartY-DStep;
			LineEnd.y =StartY-DStep;
			cvDrawLine(pTarget, LineStart, LineEnd, CV_RGB(255,0,0),2);
		}
	}
	//단위 표시
	LineStart.x = pTarget->width-120;
	LineStart.y = pTarget->height -50;
	LineEnd.x = pTarget->width-20;
	LineEnd.y = pTarget->height -50;
	cvDrawLine(pTarget, LineStart, LineEnd, CV_RGB(255,0,0),5);
	cvDrawLine(pTarget, cvPoint(LineStart.x ,LineStart.y-10), cvPoint(LineStart.x ,LineStart.y+10), CV_RGB(255,0,0),5);
	cvDrawLine(pTarget, cvPoint(LineEnd.x, LineEnd.y-10), cvPoint(LineEnd.x, LineEnd.y+10), CV_RGB(255,0,0),5);
	
	sprintf_s(G_strProcMsg,128,"%0.1fum", m_fRulerScale);
	cvPutText(pTarget, G_strProcMsg, cvPoint(LineStart.x-5, LineStart.y+40), &G_cvDrawFont, CV_RGB(255,0,0));
}

void CLiveViewCtrl::ChangeRuler(bool OnOff, float fPixelSizeX, float fPixelSizeY, int LensIndex)//, float fRulerScale)
{
	m_bViewRulerLine = OnOff;
	m_fPixelSizeX = fPixelSizeX;
	m_fPixelSizeY = fPixelSizeY;
	m_NowLensIndex = LensIndex;
	switch(m_NowLensIndex)
	{
	case REVIEW_LENS_2X:
		m_fRulerScale = REVIEW_RULER_SCALE_02x;		
		break;
	case REVIEW_LENS_10X:
		m_fRulerScale = REVIEW_RULER_SCALE_10x;	
		break;
	case REVIEW_LENS_20X:
		m_fRulerScale = REVIEW_RULER_SCALE_20x;	
		break;
	}
}

void CLiveViewCtrl::SetAutoRevewInfo(int nTotalcnt, int CntIndex, CString strBoxID, CString strStickID, COORD_DINFO *pNowReviewData, CvPoint MachinePos, IplImage*pSmallImg)
{
	if(m_pAutoReviewImg == nullptr)
		return;
	IplImage *pWriteBuf=cvCloneImage(m_pAutoReviewImg);
	cvSetZero(pWriteBuf);
	static char strData[256];
	memset(strData, 0x00, 256);

	CvPoint nTextPos;
	nTextPos.x = 10;
	nTextPos.y = 70;


	if(pNowReviewData != nullptr)
	{
		sprintf_s(strData,128,"(%d/%d)Box ID:%s , StickID:%s", CntIndex, nTotalcnt, strBoxID.GetBuffer(strBoxID.GetLength()), strStickID.GetBuffer(strStickID.GetLength()));
		cvPutText(pWriteBuf, strData, nTextPos, &G_cvDrawBigFont, CV_RGB(255,255,0));
		nTextPos.y += 60;
		sprintf_s(strData,128,"Type:%d", pNowReviewData->DefectType);
		cvPutText(pWriteBuf, strData, nTextPos, &G_cvDrawBigFont, CV_RGB(255,255,0));
		nTextPos.y += 60;
		sprintf_s(strData,128,"Cood=> X:%d, Y:%d, W:%d, H:%d", pNowReviewData->CoodStartX, pNowReviewData->CoodStartY, pNowReviewData->CoodWidth, pNowReviewData->CoodHeight);
		cvPutText(pWriteBuf, strData, nTextPos, &G_cvDrawBigFont, CV_RGB(255,255,0));
		nTextPos.y += 60;
		sprintf_s(strData,128,"Machine=> X:%d, Y:%d", MachinePos.x, MachinePos.y);
		cvPutText(pWriteBuf, strData, nTextPos, &G_cvDrawBigFont, CV_RGB(255,255,0));
		nTextPos.y += 60;
		sprintf_s(strData,128,"AVG:%d, Min:%d, Max:%d - Cnt(%d)", pNowReviewData->nDValueAvg, pNowReviewData->nDValueMin, pNowReviewData->nDValueMax, pNowReviewData->nDefectCount);
		cvPutText(pWriteBuf, strData, nTextPos, &G_cvDrawBigFont, CV_RGB(255,255,0));

		if(m_pAutoReviewSmallImg!= nullptr)
			cvReleaseImage(&m_pAutoReviewSmallImg);
		m_pAutoReviewSmallImg = nullptr;
		if(pSmallImg!= nullptr)
		{
			m_pAutoReviewSmallImg = cvCreateImage(cvSize(pSmallImg->width*2, pSmallImg->height*2), IPL_DEPTH_8U, 3);
			cvZero(m_pAutoReviewSmallImg);
			
			IplImage*pSizebuf = cvCreateImage(cvSize(pSmallImg->width*2, pSmallImg->height*2), IPL_DEPTH_8U, 1);
			
			cvResize(pSmallImg, pSizebuf);
			cvConvertImage(pSizebuf, m_pAutoReviewSmallImg);

			cvReleaseImage(&pSizebuf);
			pSizebuf = nullptr;
			/*m_pAutoReviewSmallImg
			CvRect SmallImgArea;
			SmallImgArea.x =0;
			SmallImgArea.y =0;
			SmallImgArea.width = pSmallImg->width;
			SmallImgArea.height = pSmallImg->height;

			if(pSmallImg->height<=pWriteBuf->height)
			{
			SmallImgArea.x =pWriteBuf->width - pSmallImg->width;
			SmallImgArea.y = abs(pWriteBuf->height - pSmallImg->height)/2;

			cvSetImageROI(pWriteBuf, SmallImgArea);
			cvCopyImage(pSmallImg, pWriteBuf);
			cvResetImageROI(pWriteBuf);
			}
			cvDrawRect(pWriteBuf, 
			cvPoint(SmallImgArea.x, SmallImgArea.y), 
			cvPoint(SmallImgArea.x+SmallImgArea.width, SmallImgArea.y+SmallImgArea.height),
			CV_RGB(255,255,0),2);*/
		}

	}
	else
	{
		if(m_pAutoReviewSmallImg!= nullptr)
			cvReleaseImage(&m_pAutoReviewSmallImg);
		m_pAutoReviewSmallImg = nullptr;

		nTextPos.x = pWriteBuf->width/2-500;
		nTextPos.y = 150;
		sprintf_s(strData,128,"Inspection Auto Review");
		cvPutText(pWriteBuf, strData, nTextPos, &G_cvDrawVBigFont, CV_RGB(255,255,0));
		nTextPos.x -= 100;
		nTextPos.y = 250;
		sprintf_s(strData,128,"Box ID:%s , StickID:%s",strBoxID.GetBuffer(strBoxID.GetLength()), strStickID.GetBuffer(strStickID.GetLength()));
		cvPutText(pWriteBuf, strData, nTextPos, &G_cvDrawBigFont, CV_RGB(255,255,0));
	}
	cvCopyImage(pWriteBuf, m_pAutoReviewImg);
	
	if(pWriteBuf!=nullptr)
		cvReleaseImage(&pWriteBuf);
	pWriteBuf = nullptr;
}
void CLiveViewCtrl::SetCDTPReviewInfo(int nTotalcnt, int CntIndex, CString strStickID, CvRect CDPos, CvRect TPPos)
{
	//m_strCDTPInfo.Format("%s(%d/%d)", strStickID, CntIndex, nTotalcnt);
	m_strCDTPInfo.Format("<%d/%d>", CntIndex, nTotalcnt);
	m_CDPos = CDPos;
	m_TPPos = TPPos;
	DrawCDTPReviewInfo(true);
}