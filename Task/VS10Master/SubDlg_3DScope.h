#pragma once
#include "afxwin.h"




// CSubDlg_3DScope 대화 상자입니다.

class CSubDlg_3DScope : public CDialogEx
{
	DECLARE_DYNAMIC(CSubDlg_3DScope)

public:
	CSubDlg_3DScope(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubDlg_3DScope();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SUB_3D_SCOPE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	ObsAppState m_ScopeAppState;
	ObsActionState m_ScopeActionState;
	CInterServerInterface  *m_pServerInterface;
	SCOPE_3DPara				*m_pScope3DParam;
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	afx_msg void OnBnClickedBtMeasureInit();

	//Task Status 갱신 UI Control
	CColorStaticST m_ctrStaticTask3DMeasure;
	CColorStaticST m_ctrStaticProc3DMeasure;
	CColorStaticST m_stMeasureCnt3D;
	CColorStaticST m_stSpaceSensorTargetZPos;

	COLORREF	m_TaskStateColor;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButton2 m_btMeasure3DInit;
	CRoundButton2 m_btStartMeasure3D;
	CRoundButton2 m_bt3DLensSelect[4];

	CRoundButton2 m_btSPSensorMoveScopePos;
	CRoundButton2 m_btSPSensorMoveTargetZPos;
	CRoundButton2 m_btSpaceSensorSafetyPosMove;

	afx_msg void OnBnClickedBt3dLens();
	afx_msg void OnBnClickedBtMeasureStart();

	afx_msg void OnBnClickedBtMoveScopePos();
	afx_msg void OnBnClickedBtMoveTargetZpos();
	afx_msg void OnBnClickedBtMoveZeroPos();
};
