#pragma once
#include "afxwin.h"


// CSubDlg_StickExchange 대화 상자입니다.

class CSubDlg_StickExchange : public CDialogEx
{
	DECLARE_DYNAMIC(CSubDlg_StickExchange)

public:
	CSubDlg_StickExchange(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubDlg_StickExchange();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SUB_STICK_EXCHANGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	StickInfoByIndexer	*m_pStickLoadInfo;
	GLASS_PARAM			*m_pGlassReicpeParam;
	Scope3DPosition		*m_pScopePos;
	GripperPosition			*m_pGripperPos;
	float						*m_pStickTentionValue;

	CInterServerInterface  *m_pServerInterface;
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	CColorStaticST m_stTT01_AM01_Pos;
	CColorStaticST m_stTT01_Turn_Pos;
	CColorStaticST m_stTT01_UT02_Pos;
	CColorStaticST m_stDetectionStickLeft;
	CColorStaticST m_stDetectionStickRight;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButton2 m_btGripperStandbyPos;
	CRoundButton2 m_btGripperGripPos;
	CRoundButton2 m_btGripperTensionStandbyPos;
	CRoundButton2 m_btGripperSolFWD;
	CRoundButton2 m_btGripperSolBWD;
	CRoundButton2 m_btGripperSolOpen;
	CRoundButton2 m_btGripperSolClose;
	CRoundButton2 m_btGripperStickTentionStart;
	
	CRoundButton2 m_btGripZPos_TT01_out;
	CRoundButton2 m_btGripZPos_Grip_Standby;
	CRoundButton2 m_btGripZPos_Grip;
	CRoundButton2 m_btGripZPos_Measure;
	CRoundButton2 m_btGripZPos_Standby;

	afx_msg void OnBnClickedBtGripTenStandbyPos();
	afx_msg void OnBnClickedBtGripTenGripPos();
	afx_msg void OnBnClickedBtGripTentionStep1();
	afx_msg void OnBnClickedBtGripperSolFwd();
	afx_msg void OnBnClickedBtGripperSolBwd();
	afx_msg void OnBnClickedBtGripperOpen();
	afx_msg void OnBnClickedBtGripperClose();
	afx_msg void OnBnClickedBtGripTentionStart();

	afx_msg void OnBnClickedBtGripZposTt01Out();
	afx_msg void OnBnClickedBtGripZposGrip();
	afx_msg void OnBnClickedBtGripZposMeasure();
	afx_msg void OnBnClickedBtGripZposStandby();
	
	afx_msg void OnBnClickedBtGripZposGripStandby();
};
