// SubDlg_CellAlign.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SubDlg_StickExchange.h"
#include "afxdialogex.h"


// CSubDlg_StickExchange 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSubDlg_StickExchange, CDialogEx)

CSubDlg_StickExchange::CSubDlg_StickExchange(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSubDlg_StickExchange::IDD, pParent)
{
	m_pStickLoadInfo = nullptr;
	m_pGlassReicpeParam = nullptr;
	m_pScopePos = nullptr;
	m_pGripperPos = nullptr;
	m_pStickTentionValue = nullptr;

	m_pServerInterface = nullptr;
}

CSubDlg_StickExchange::~CSubDlg_StickExchange()
{
}

void CSubDlg_StickExchange::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ST_TT01_AM01, m_stTT01_AM01_Pos);
	DDX_Control(pDX, IDC_ST_TT01_TURN, m_stTT01_Turn_Pos);
	DDX_Control(pDX, IDC_ST_TT01_UT02, m_stTT01_UT02_Pos);
	DDX_Control(pDX, IDC_BT_GRIP_TEN_STANDBY_POS, m_btGripperStandbyPos);
	DDX_Control(pDX, IDC_BT_GRIP_TEN_GRIP_POS, m_btGripperGripPos);
	DDX_Control(pDX, IDC_BT_GRIP_TENTION_STEP1, m_btGripperTensionStandbyPos);
	DDX_Control(pDX, IDC_BT_GRIPPER_SOL_FWD, m_btGripperSolFWD);
	DDX_Control(pDX, IDC_BT_GRIPPER_SOL_BWD, m_btGripperSolBWD);
	DDX_Control(pDX, IDC_BT_GRIPPER_OPEN, m_btGripperSolOpen);
	DDX_Control(pDX, IDC_BT_GRIPPER_CLOSE, m_btGripperSolClose);
	DDX_Control(pDX, IDC_BT_GRIP_TENTION_START, m_btGripperStickTentionStart);
	DDX_Control(pDX, IDC_ST_STICK_DETECT_LEFT, m_stDetectionStickLeft);
	DDX_Control(pDX, IDC_ST_STICK_DETECT_RIGHT, m_stDetectionStickRight);
	DDX_Control(pDX, IDC_BT_GRIP_ZPOS_TT01_OUT, m_btGripZPos_TT01_out);
	DDX_Control(pDX, IDC_BT_GRIP_ZPOS_GRIP, m_btGripZPos_Grip);
	DDX_Control(pDX, IDC_BT_GRIP_ZPOS_MEASURE, m_btGripZPos_Measure);
	DDX_Control(pDX, IDC_BT_GRIP_ZPOS_STANDBY, m_btGripZPos_Standby);
	DDX_Control(pDX, IDC_BT_GRIP_ZPOS_GRIP_STANDBY, m_btGripZPos_Grip_Standby);
}


BEGIN_MESSAGE_MAP(CSubDlg_StickExchange, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_GRIP_TEN_STANDBY_POS, &CSubDlg_StickExchange::OnBnClickedBtGripTenStandbyPos)
	ON_BN_CLICKED(IDC_BT_GRIP_TEN_GRIP_POS, &CSubDlg_StickExchange::OnBnClickedBtGripTenGripPos)
	ON_BN_CLICKED(IDC_BT_GRIP_TENTION_STEP1, &CSubDlg_StickExchange::OnBnClickedBtGripTentionStep1)
	ON_BN_CLICKED(IDC_BT_GRIPPER_SOL_FWD, &CSubDlg_StickExchange::OnBnClickedBtGripperSolFwd)
	ON_BN_CLICKED(IDC_BT_GRIPPER_SOL_BWD, &CSubDlg_StickExchange::OnBnClickedBtGripperSolBwd)
	ON_BN_CLICKED(IDC_BT_GRIPPER_OPEN, &CSubDlg_StickExchange::OnBnClickedBtGripperOpen)
	ON_BN_CLICKED(IDC_BT_GRIPPER_CLOSE, &CSubDlg_StickExchange::OnBnClickedBtGripperClose)
	ON_BN_CLICKED(IDC_BT_GRIP_TENTION_START, &CSubDlg_StickExchange::OnBnClickedBtGripTentionStart)
	ON_BN_CLICKED(IDC_BT_GRIP_ZPOS_TT01_OUT, &CSubDlg_StickExchange::OnBnClickedBtGripZposTt01Out)
	ON_BN_CLICKED(IDC_BT_GRIP_ZPOS_GRIP, &CSubDlg_StickExchange::OnBnClickedBtGripZposGrip)
	ON_BN_CLICKED(IDC_BT_GRIP_ZPOS_MEASURE, &CSubDlg_StickExchange::OnBnClickedBtGripZposMeasure)
	ON_BN_CLICKED(IDC_BT_GRIP_ZPOS_STANDBY, &CSubDlg_StickExchange::OnBnClickedBtGripZposStandby)
	ON_BN_CLICKED(IDC_BT_GRIP_ZPOS_GRIP_STANDBY, &CSubDlg_StickExchange::OnBnClickedBtGripZposGripStandby)
END_MESSAGE_MAP()


// CSubDlg_CDTP 메시지 처리기입니다.


BOOL CSubDlg_StickExchange::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSubDlg_StickExchange::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;

	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btGripperStandbyPos.GetTextColor(&tColor);
		m_btGripperStandbyPos.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btGripperStandbyPos.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripperStandbyPos.SetTextColor(&tColor);
		m_btGripperStandbyPos.SetCheckButton(true, true);
		m_btGripperStandbyPos.SetFont(&tFont);


		m_btGripperGripPos.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripperGripPos.SetTextColor(&tColor);
		m_btGripperGripPos.SetCheckButton(true, true);
		m_btGripperGripPos.SetFont(&tFont);

		m_btGripperTensionStandbyPos.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripperTensionStandbyPos.SetTextColor(&tColor);
		m_btGripperTensionStandbyPos.SetCheckButton(true, true);
		m_btGripperTensionStandbyPos.SetFont(&tFont);

		m_btGripperSolFWD.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripperSolFWD.SetTextColor(&tColor);
		m_btGripperSolFWD.SetCheckButton(true, true);
		m_btGripperSolFWD.SetFont(&tFont);

		m_btGripperSolBWD.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripperSolBWD.SetTextColor(&tColor);
		m_btGripperSolBWD.SetCheckButton(true, true);
		m_btGripperSolBWD.SetFont(&tFont);

		m_btGripperSolOpen.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripperSolOpen.SetTextColor(&tColor);
		m_btGripperSolOpen.SetCheckButton(true, true);
		m_btGripperSolOpen.SetFont(&tFont);

		m_btGripperSolClose.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripperSolClose.SetTextColor(&tColor);
		m_btGripperSolClose.SetCheckButton(true, true);
		m_btGripperSolClose.SetFont(&tFont);

		m_btGripperStickTentionStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripperStickTentionStart.SetTextColor(&tColor);
		m_btGripperStickTentionStart.SetCheckButton(true, true);
		m_btGripperStickTentionStart.SetFont(&tFont);

		m_btGripZPos_TT01_out.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripZPos_TT01_out.SetTextColor(&tColor);
		m_btGripZPos_TT01_out.SetCheckButton(true, true);
		m_btGripZPos_TT01_out.SetFont(&tFont);
		
		m_btGripZPos_Grip_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripZPos_Grip_Standby.SetTextColor(&tColor);
		m_btGripZPos_Grip_Standby.SetCheckButton(true, true);
		m_btGripZPos_Grip_Standby.SetFont(&tFont);

		m_btGripZPos_Grip.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripZPos_Grip.SetTextColor(&tColor);
		m_btGripZPos_Grip.SetCheckButton(true, true);
		m_btGripZPos_Grip.SetFont(&tFont);

		m_btGripZPos_Measure.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripZPos_Measure.SetTextColor(&tColor);
		m_btGripZPos_Measure.SetCheckButton(true, true);
		m_btGripZPos_Measure.SetFont(&tFont);

		m_btGripZPos_Standby.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGripZPos_Standby.SetTextColor(&tColor);
		m_btGripZPos_Standby.SetCheckButton(true, true);
		m_btGripZPos_Standby.SetFont(&tFont);
	}
	m_stTT01_AM01_Pos.SetFont(&G_NormalFont);
	m_stTT01_AM01_Pos.SetTextColor(RGB(0, 0, 0));
	m_stTT01_AM01_Pos.SetBkColor(G_Color_Off);

	m_stTT01_Turn_Pos.SetFont(&G_NormalFont);
	m_stTT01_Turn_Pos.SetTextColor(RGB(0, 0, 0));
	m_stTT01_Turn_Pos.SetBkColor(G_Color_Off);

	m_stTT01_UT02_Pos.SetFont(&G_NormalFont);
	m_stTT01_UT02_Pos.SetTextColor(RGB(0, 0, 0));
	m_stTT01_UT02_Pos.SetBkColor(G_Color_On);

	m_stDetectionStickLeft.SetFont(&G_NormalFont);
	m_stDetectionStickLeft.SetTextColor(RGB(0, 0, 0));
	m_stDetectionStickLeft.SetBkColor(G_Color_On);

	m_stDetectionStickRight.SetFont(&G_NormalFont);
	m_stDetectionStickRight.SetTextColor(RGB(0, 0, 0));
	m_stDetectionStickRight.SetBkColor(G_Color_On);

}
void CSubDlg_StickExchange::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


HBRUSH CSubDlg_StickExchange::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(IDC_ST_TT01_AM01 == DLG_ID_Number ||
		IDC_ST_TT01_TURN == DLG_ID_Number ||
		IDC_ST_TT01_UT02 == DLG_ID_Number ||
		IDC_ST_STICK_DETECT_LEFT ==DLG_ID_Number ||
		IDC_ST_STICK_DETECT_RIGHT ==DLG_ID_Number)
		return hbr;

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CSubDlg_StickExchange::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSubDlg_StickExchange::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}




void CSubDlg_StickExchange::OnBnClickedBtGripTenStandbyPos()
{
	G_WriteInfo(Log_Normal, "<OnBnClickedBtGripTenStandbyPos(%03d)>");
	//1)Grip Tension Axis (4*4 Axis 후퇴)
	//Left 4 Axis, Right 4Axis를 Standby Pos Move 시킨다.
	G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);
	G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);
	m_btGripperStandbyPos.SetCheck(true);
}


void CSubDlg_StickExchange::OnBnClickedBtGripTenGripPos()
{
	G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Grip);
	G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Grip);
	m_btGripperStandbyPos.SetCheck(true);
}


void CSubDlg_StickExchange::OnBnClickedBtGripTentionStep1()
{
	G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_TensionStandby);
	G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_TensionStandby);
	m_btGripperStandbyPos.SetCheck(true);
}

void CSubDlg_StickExchange::OnBnClickedBtGripTentionStart()
{
	G_WriteInfo(Log_Normal, "OnBnClickedBtGripTentionStart");
	//상위에서 받은것
	int StickLoadOffset = m_pStickLoadInfo->nStickLoadOffset;
	int StickLoadSize= StickLoadOffset+m_pGlassReicpeParam->STICK_WIDTH;
	//인장 Mot 선택.
	int SelectMotCnt = StickLoadSize/(TENSION_CLEE_WIDTH*TENSION_CLEE_CNT);
	SelectMotCnt += (StickLoadSize%(TENSION_CLEE_WIDTH*TENSION_CLEE_CNT)>0)?1:0;

	int SelectMot = 0;
	switch(SelectMotCnt)
	{
	case 1: SelectMot = 0x01; break;
	case 2: SelectMot = (0x01 | 0x02); break;
	case 3: SelectMot = (0x01 | 0x02 | 0x04); break;
	case 4: SelectMot = (0x01 | 0x02 | 0x04 | 0x08); break;
	default: 
		SelectMot = 0;
		break;
	}
	if(*m_pStickTentionValue<0.0)
	{
		G_WriteInfo(Log_Check, "OnBnClickedBtGripTentionStart Stick Tension Value Error");
		return;
	}
		
	if(SelectMot>0)
		//1)Grip Tension Start
		G_MotObj.m_fnAM01GripperTensionStart(true, SelectMot, *m_pStickTentionValue);
	else
		G_WriteInfo(Log_Error, "<SEQ_STEP_START_STICK_TENSION> Tension Axis Select Error");
}


void CSubDlg_StickExchange::OnBnClickedBtGripperSolFwd()
{
	G_WriteInfo(Log_Normal, "OnBnClickedBtGripperSolFwd");
	//2)Grip Tension Sol BWD
	G_MotObj.m_fnStickTentionFWD();
}


void CSubDlg_StickExchange::OnBnClickedBtGripperSolBwd()
{
	G_WriteInfo(Log_Normal, "OnBnClickedBtGripperSolBwd");
	//2)Grip Tension Sol BWD
	G_MotObj.m_fnStickTentionBWD();
}


void CSubDlg_StickExchange::OnBnClickedBtGripperOpen()
{
	G_MotObj.m_fnStickGripperOpen();
}


void CSubDlg_StickExchange::OnBnClickedBtGripperClose()
{
	G_MotObj.m_fnStickGripperClose();
}



void CSubDlg_StickExchange::OnBnClickedBtGripZposTt01Out()
{
	G_WriteInfo(Log_Normal, "OnBnClickedBtGripZposTt01Out");
	G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_TT01Out);
	m_btGripZPos_TT01_out.SetCheck(true);
}


void CSubDlg_StickExchange::OnBnClickedBtGripZposGrip()
{
	G_WriteInfo(Log_Normal, "OnBnClickedBtGripZposGrip");
	G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip);
	m_btGripZPos_Grip.SetCheck(true);
}


void CSubDlg_StickExchange::OnBnClickedBtGripZposMeasure()
{
	G_WriteInfo(Log_Normal, "OnBnClickedBtGripZposMeasure");
	G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Measure);
	m_btGripZPos_Measure.SetCheck(true);
}


void CSubDlg_StickExchange::OnBnClickedBtGripZposStandby()
{
	G_WriteInfo(Log_Normal, "OnBnClickedBtGripZposStandby");
	G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Standby);
	m_btGripZPos_Standby.SetCheck(true);
}


void CSubDlg_StickExchange::OnBnClickedBtGripZposGripStandby()
{
	G_WriteInfo(Log_Normal, "OnBnClickedBtGripZposGripStandby");
	G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip_Standby);
	m_btGripZPos_Grip_Standby.SetCheck(true);
}
