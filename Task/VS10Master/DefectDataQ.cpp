#include "StdAfx.h"
#include "DefectDataQ.h"


//////////////////////////////////////////////////////////////////////
// Defect Write 시간Format Create를 줄이기 위해서.(Overflow 일수도....)
//////////////////////////////////////////////////////////////////////

/*
*	Module Name		:	QCMDCtrlList
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

DefectDataQ::DefectDataQ()
{
	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_pReadPos						= &m_HeadNode;

	m_CntNode					= 0;
	m_bDataUpdateEnd		= false;
	InitializeCriticalSection(&m_CrtSection);
}
DefectDataQ::~DefectDataQ()
{
	EnterCriticalSection(&m_CrtSection);
	//QClean();
	{
		DataNodeObj * pDeleteNode = NULL;

		if(m_HeadNode.m_pNextNode != &m_TailNode)
		{
			pDeleteNode =  m_HeadNode.m_pNextNode;

			DataNodeObj * pNextNodeBuf = NULL;
			while (pDeleteNode != &m_TailNode)
			{

				pNextNodeBuf = pDeleteNode->m_pNextNode;

				delete pDeleteNode;

				pDeleteNode = pNextNodeBuf;
				pNextNodeBuf = NULL;
			}
			m_HeadNode.m_pFrontNode		= NULL;
			m_HeadNode.m_pNextNode		= &m_TailNode;

			m_TailNode.m_pFrontNode		= &m_HeadNode;
			m_TailNode.m_pNextNode		= NULL;
		}
	}
	LeaveCriticalSection(&m_CrtSection);

	DeleteCriticalSection(&m_CrtSection);
}


void DefectDataQ::QPutNode(COORD_DINFO	*pNewObj)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj *pNewNode = new DataNodeObj;

	pNewNode->DefectData = *pNewObj;//Data저장.
	
	pNewNode->m_pFrontNode					= &m_HeadNode;
	pNewNode->m_pNextNode					= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode					= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
}
void DefectDataQ::QPutBackNode(COORD_DINFO	*pNewObj)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj *pNewNode = new DataNodeObj;

	pNewNode->DefectData = *pNewObj;//Data저장.

	pNewNode->m_pFrontNode					= m_TailNode.m_pFrontNode;
	pNewNode->m_pNextNode					= &m_TailNode;

	(pNewNode->m_pFrontNode)->m_pNextNode	= pNewNode;
	m_TailNode.m_pFrontNode					= pNewNode;
	

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
}

bool	DefectDataQ::QPutMergeNode(COORD_DINFO	*pNewObj, IplImage *pSmImg, double MergeDis)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pCheckNode = nullptr;
	pCheckNode =  m_HeadNode.m_pNextNode;

	int InputCenterX = pNewObj->CoodStartX+(pNewObj->CoodWidth/2);
	int InputCenterY = pNewObj->CoodStartY+(pNewObj->CoodHeight/2);
	int BufCenterX = 0;
	int BufCenterY = 0;
	double DisCenter = 0;
	while (pCheckNode != &m_TailNode)
	{
		BufCenterX = pCheckNode->DefectData.CoodStartX+(pCheckNode->DefectData.CoodWidth/2);
		BufCenterY = pCheckNode->DefectData.CoodStartY+(pCheckNode->DefectData.CoodHeight/2);
		//Defect 거리 구하기.
		DisCenter = sqrt((double)((InputCenterX-BufCenterX)*(InputCenterX-BufCenterX)+(InputCenterY-BufCenterY)*(InputCenterY-BufCenterY)));
		if(DisCenter<MergeDis)
		{
			LeaveCriticalSection(&m_CrtSection);
			return false;
		}
		pCheckNode =  pCheckNode->m_pNextNode;
	}

	DataNodeObj *pNewNode = new DataNodeObj;

	
	pNewNode->DefectData = *pNewObj;//Data저장.
	if(pSmImg != nullptr)
		pNewNode->m_pSmallImg = cvCloneImage(pSmImg);

	pNewNode->m_pFrontNode						= &m_HeadNode;
	pNewNode->m_pNextNode							= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode						= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
	return true;
}
COORD_DINFO	 DefectDataQ::QGetNode(IplImage **ppSmalImg)
{
	EnterCriticalSection(&m_CrtSection);
	COORD_DINFO	retData;
	
	DataNodeObj *pReturnNode;
	pReturnNode = m_TailNode.m_pFrontNode;

	if(pReturnNode ==  &m_HeadNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return retData;
	}

	m_TailNode.m_pFrontNode = pReturnNode->m_pFrontNode;
	(pReturnNode->m_pFrontNode)->m_pNextNode = &m_TailNode;

	retData = pReturnNode->DefectData;
	if(pReturnNode->m_pSmallImg != nullptr)
		*ppSmalImg = cvCloneImage(pReturnNode->m_pSmallImg);
	delete pReturnNode;
	m_CntNode --;

	LeaveCriticalSection(&m_CrtSection);
	return retData;
}
bool DefectDataQ::QGetMoveFirstNode()
{
	EnterCriticalSection(&m_CrtSection);
	bool retValue = false;
	m_pReadPos = &m_HeadNode;
	if(m_pReadPos->m_pNextNode != &m_TailNode)
	{
		m_pReadPos= m_pReadPos->m_pNextNode;
		retValue = true;
	}
	LeaveCriticalSection(&m_CrtSection);
	return retValue;
}
bool DefectDataQ::QSetNextNodeImage( IplImage *pSmallImg)
{
	EnterCriticalSection(&m_CrtSection);
	bool retValue = false;
	if(m_pReadPos != &m_TailNode)
	{
		if(pSmallImg != nullptr)
			m_pReadPos->m_pSmallImg = cvCloneImage(pSmallImg);

		m_pReadPos= m_pReadPos->m_pNextNode;

		retValue = true;
	}
	LeaveCriticalSection(&m_CrtSection);
	return retValue;
}
bool DefectDataQ::QSetNextNodePath( CString strReviewPathBuf)
{
	EnterCriticalSection(&m_CrtSection);
	bool retValue = false;
	if(m_pReadPos != &m_TailNode)
	{
		m_pReadPos->m_strReviewFileName.Format("%s", strReviewPathBuf);
		m_pReadPos= m_pReadPos->m_pNextNode;
		retValue = true;
	}
	LeaveCriticalSection(&m_CrtSection);
	return retValue;
}

bool DefectDataQ::QGetNextNodeData(COORD_DINFO*pDataBuf, IplImage ***pppSmallImg, CString **ppstrReviewPathBuf)
{
	EnterCriticalSection(&m_CrtSection);
	bool retValue = false;
	if(m_pReadPos != &m_TailNode)
	{
		*pDataBuf = m_pReadPos->DefectData;
		m_pReadPos->m_strReviewFileName.Format("");

		if(ppstrReviewPathBuf != nullptr)
			*ppstrReviewPathBuf = &m_pReadPos->m_strReviewFileName;

		if(pppSmallImg != nullptr)
			*pppSmallImg = &m_pReadPos->m_pSmallImg;
		
		m_pReadPos= m_pReadPos->m_pNextNode;
		
		retValue = true;
	}
	LeaveCriticalSection(&m_CrtSection);
	return retValue;
}
DataNodeObj* DefectDataQ::QGetNextNodeData()
{
	DataNodeObj* pRetObj = nullptr;
	EnterCriticalSection(&m_CrtSection);
	if(m_pReadPos != &m_TailNode)
	{
		pRetObj = m_pReadPos;
		m_pReadPos= m_pReadPos->m_pNextNode;
	}
	LeaveCriticalSection(&m_CrtSection);
	return pRetObj;
}

bool	 DefectDataQ::QGetMoveLastNode()
{
	EnterCriticalSection(&m_CrtSection);
	bool retValue = false;
	m_pReadPos = &m_TailNode;
	if(m_pReadPos->m_pFrontNode != &m_HeadNode)
	{
		m_pReadPos= m_pReadPos->m_pFrontNode;
		retValue = true;
	}
	LeaveCriticalSection(&m_CrtSection);
	return retValue;
}
bool	 DefectDataQ::QGetFrontNodeData(COORD_DINFO*pDataBuf)
{
	EnterCriticalSection(&m_CrtSection);
	bool retValue = false;
	if(m_pReadPos != &m_HeadNode)
	{
		*pDataBuf = m_pReadPos->DefectData;
		m_pReadPos= m_pReadPos->m_pFrontNode;
		retValue = true;
	}
	LeaveCriticalSection(&m_CrtSection);
	return retValue;
}
void DefectDataQ::NodeDataChange(DataNodeObj*pNode_A, DataNodeObj*pNode_B)
{
	int					Buf_SortSizeIndex;
	int					Buf_MachinePosX;
	int					Buf_MachinePosY;
	COORD_DINFO			Buf_DefectData;
	IplImage			*Buf_pSmallImg;
	CString				Buf_strReviewFileName;

	Buf_SortSizeIndex	= pNode_A->m_SizeSortIndex;
	Buf_MachinePosX		= pNode_A->m_MachinePosX;
	Buf_MachinePosY		= pNode_A->m_MachinePosY;
	Buf_DefectData		= pNode_A->DefectData;
	Buf_pSmallImg		= pNode_A->m_pSmallImg;
	Buf_strReviewFileName.Format("%s", pNode_A->m_strReviewFileName);

	pNode_A->m_SizeSortIndex		= pNode_B->m_SizeSortIndex;
	pNode_A->m_MachinePosX		= pNode_B->m_MachinePosX;
	pNode_A->m_MachinePosY		= pNode_B->m_MachinePosY;
	pNode_A->DefectData			= pNode_B->DefectData;
	pNode_A->m_pSmallImg			= pNode_B->m_pSmallImg;
	pNode_A->m_strReviewFileName.Format("%s", pNode_B->m_strReviewFileName);

	pNode_B->m_SizeSortIndex		= Buf_SortSizeIndex;
	pNode_B->m_MachinePosX		= Buf_MachinePosX;
	pNode_B->m_MachinePosY		= Buf_MachinePosY;
	pNode_B->DefectData			= Buf_DefectData;
	pNode_B->m_pSmallImg			= Buf_pSmallImg;
	pNode_B->m_strReviewFileName.Format("%s", Buf_strReviewFileName);
}
bool DefectDataQ::QSortBySize(bool updateIndex)
{
	EnterCriticalSection(&m_CrtSection);

	DataNodeObj * pCMPNode = nullptr;
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return false;
	}

	int nSortIndex = 0;
	pCMPNode = m_HeadNode.m_pNextNode;
	//버블 Sort를 하자.
	while(pCMPNode != &m_TailNode && pCMPNode != nullptr )
	{
		DataNodeObj * pChkBuf = pCMPNode->m_pNextNode;
		while(pChkBuf != &m_TailNode && pChkBuf != nullptr )
		{
			if(pChkBuf->DefectData.nDefectCount > pCMPNode->DefectData.nDefectCount)
			{//크거나 같으면 교환.(Link는 두고 Data를 교환한다.)
				NodeDataChange(pCMPNode, pChkBuf);
			}
			//다음 Node로 이동
			pChkBuf =  pChkBuf->m_pNextNode;
		}

		//여기서 처음으로 크기별 Index가 정해진다.
		if(updateIndex == true)
			pCMPNode->m_SizeSortIndex = nSortIndex++;
		//다음 Node로 이동
		pCMPNode =  pCMPNode->m_pNextNode;
	}

	LeaveCriticalSection(&m_CrtSection);
	return true;
}

bool DefectDataQ::QSortByDistance(int nCount, int BasePosX, int BasePosY)
{
	EnterCriticalSection(&m_CrtSection);

	DataNodeObj * pCMPNode = nullptr;
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return false;
	}

	int nDisCntIndex = 0;
	pCMPNode = m_HeadNode.m_pNextNode;

	double checkPosX = (double)BasePosX;
	double checkPosY = (double)BasePosY;
	double dXLth = 0.0;
	double dYLth = 0.0;
	double dDisOrg = 0.0;

	//버블 Sort를 하자.
	while(pCMPNode != &m_TailNode && pCMPNode != nullptr &&  nDisCntIndex<nCount)
	{
		//dXLth = (checkPosX - (double)pCMPNode->DefectData.CoodStartX);
		//dYLth = (checkPosY - (double)pCMPNode->DefectData.CoodStartY );

		dXLth = (checkPosX - (double)pCMPNode->m_MachinePosX);
		dYLth = (checkPosY - (double)pCMPNode->m_MachinePosY );
	
		dDisOrg = sqrt(dXLth * dXLth + dYLth * dYLth);

		DataNodeObj * pChkBuf = pCMPNode->m_pNextNode;
		int nChkDisCntIndex = 1;
		while(pChkBuf != &m_TailNode && pChkBuf != nullptr   && nChkDisCntIndex < (nCount-nDisCntIndex))
		{
			//double dchkXLth = (checkPosX - (double)pChkBuf->DefectData.CoodStartX);
			//double dchkYLth = (checkPosY - (double)pChkBuf->DefectData.CoodStartY);

			double dchkXLth = (checkPosX - (double)pChkBuf->m_MachinePosX);
			double dchkYLth = (checkPosY - (double)pChkBuf->m_MachinePosY);
			double dDischk = sqrt(dchkXLth * dchkXLth + dchkYLth * dchkYLth);

			if(dDischk < dDisOrg)
			{//크거나 같으면 교환.(Link는 두고 Data를 교환한다.)
				dDisOrg = dDischk;
				NodeDataChange(pCMPNode, pChkBuf);
			}
			//다음 Node로 이동
			pChkBuf =  pChkBuf->m_pNextNode;
			nChkDisCntIndex++;
		}
		//새로운 기준점을 기록
		//checkPosX = (double)pCMPNode->DefectData.CoodStartX;
		//checkPosY = (double)pCMPNode->DefectData.CoodStartY;

		checkPosX = (double)pCMPNode->m_MachinePosX;
		checkPosY = (double)pCMPNode->m_MachinePosY;

		//다음 Node로 이동
		pCMPNode =  pCMPNode->m_pNextNode;
		nDisCntIndex++;
	}

	LeaveCriticalSection(&m_CrtSection);
	return true;
}
void DefectDataQ::QClean(void)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pDeleteNode = nullptr;

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pDeleteNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = nullptr;
	while (pDeleteNode != &m_TailNode)
	{
		pNextNodeBuf = pDeleteNode->m_pNextNode;
		delete pDeleteNode;

		pDeleteNode = pNextNodeBuf;
		pNextNodeBuf = nullptr;
	}

	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_bDataUpdateEnd		= false;
	m_CntNode = 0;
	LeaveCriticalSection(&m_CrtSection);
}

