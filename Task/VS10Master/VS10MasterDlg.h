
// VS10MasterDlg.h : 헤더 파일
//

#pragma once
#include "DefectDataQ.h"

#include "MapViewDlg.h"
#include "afxwin.h"
#include "StickReicpeDlg.h"
#include "SysParamDlg.h"
#include "MaintDlg.h"


#include "SubDlg_3DScope.h"	
#include "SubDlg_InspectScan.h"
#include "SubDlg_AreaScan3D.h"
#include "SubDlg_CDTP.h"
#include "SubDlg_StickExchange.h"
#include "SubDlg_ThetaAlign.h"
//#include "SubDlg_CIMInterface.h"

#include "Seq_InsAutoReview.h"

#include "Seq_StickExchange.h"
#include "Seq_ThetaAlign.h"
#include "Seq_CellAlign.h"
#include "Seq_ReviewCalc.h"
#include "Seq_AreaScan3D.h"
#include "Seq_3DScope.h"
#include "Seq_Inspection_Mic.h"
#include "Seq_Inspection_Mac.h"
#include "Seq_AutoRun.h"
#include "Seq_InitAM01.h"

//#include "AlarmClearDlg.h"

#include "DataFormat\DataFormat.h"
#include "SubStickLoadDlg.h"

#include "afxcmn.h"
#include "CIMMsgViewDlg.h"
#include "PassWord.h"

UINT Thread_InspectDefectMergeProc_Mic(LPVOID pParam);
UINT Thread_InspectDefectMergeProc_Mac(LPVOID pParam);
// CVS10MasterDlg 대화 상자
class CVS10MasterDlg : public CDialogEx, public CProcInitial//VS64 Interface
{
// 생성입니다.
public:
	CVS10MasterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~CVS10MasterDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VS10MASER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
public:
	IplImage *m_pSampleView;
	HDC			m_HDCView;
	CRect			m_RectiewArea;
	void DrawImageView();
public://CIM Interfac
	CCimInterfaceSocket m_CIMInterface;

public://Live View
	HDC			m_TheataAlignHDCView;
	CRect			m_TheataAlignRectiewArea;
	CLiveGrab	m_ThetaAlignLiveObj;

	CLiveViewCtrl m_LiveViewObj;


	CStickReicpeDlg *m_pStickReicpeView;
	//////////////////////////////////////////////////////////////////////////
	CRect								m_MapViewArea;
	CMapViewDlg	*				m_p3DMapViewer;
	CSysParamDlg 	*				m_pSysParamView;

	bool m_bUpReviewCylinder;
	bool m_bUpRingLightCylinder;
	CMaintDlg		*				m_pMaintenanceView;

	CRect								m_SubDlgArea;

	CSubDlg_3DScope				* m_Sub3DScopeView;
	CSubDlg_InspectScan			* m_SubInspectView;
	CSubDlg_AreaScan3D			* m_SubAreaScanView;
	CSubDlg_CDTP				* m_SubCDTPView;

	CSubDlg_StickExchange		* m_SubStickExchangeView;
	CSubDlg_ThetaAlign			* m_SubThetaAlignView;

	SubDlg_CIMInterface			* m_SubCIMInterfaceView;
	CAlarmClearDlg				* m_SubAlarmClearView;

	CPMDlg						* m_SubPMView;
public:
	int							m_AlignCellStartX;
	int							m_AlignCellStartY;
	float						m_StickTentionValue;

	HW_PARAM					m_HWRecipeParam;//Inspector에서 받아 서 저장 하는것.
	GLASS_PARAM					m_GlassReicpeParam;

	SAOI_IMAGE_INFO				m_stMaskImageInfo_Mic;
	SAOI_COMMON_PARA			m_stCommonPara_Mic;	

	SAOI_IMAGE_INFO				m_stMaskImageInfo_Mac;
	SAOI_COMMON_PARA			m_stCommonPara_Mac;	

	SCOPE_3DPara				m_Scope3DParam;
	SCAN_3DPara					m_Scan3DParam;
	SCAN_3DResult				m_Scan3DResult;
	CD_TPParam					m_CDTPParam;
//////////////////////////////////////////////////////////////////////////
public://Inspect Process Data.
	bool								m_BlockEnd;
	DWORD							m_StartBlockTick;
	DWORD							m_StartLineTick;
	int									m_CamScnaLineCnt;
	int									m_CamScnaImgCnt;
	int									m_StartScanLineNum;
//////////////////////////////////////////////////////////////////////////
public://CD TP
	int									m_CDTPPointCnt;
	CDTP_Point					*  m_pCDTPPointList;
	void InputCDTPPointTreeItem(int CntItem, CDTP_Point*  pCDTPPointList);

	TP_Result m_TP_Result;

public://3D Scope Process Data.
	RESULT3DScope_DATA		m_NowScopeRetData;
	int									m_3DMeasurPointCnt;
	SCOPE_3DPoint			*  m_p3DMeasurPointList;

	float	m_f3DScope_Ret_DOON_TUK;
	float	m_f3DScope_Ret_STICK_THICK;
	float	m_f3DScope_Ret_TAPERANGLE;

	void InputScope3DPointTreeItem(int CntItem, SCOPE_3DPoint*  p3DMeasurPointList);
public:
	//////////////////////////////////////////////////////////////////////////
	//Map 좌표 그리기.(1차로 Recipe Load시 그리고 Inspection시에 다시 그린다.)
	int									m_nCellCnt;
	CELLINFO				*		m_pCellList;
	int									m_nScanImgCnt;
	SCANIMGINFO			*		m_pScanImgList;
	int									m_nRetDefectCnt;
	DEFECT_DATA			*		m_pRetDefectList;


	bool						m_bInspectMicOrMac;//true : Mic, false:Mac

	DefectDataQ					m_MergeData_MicQ;
	DefectDataQ					m_DefectData_MicQ;
	DefectDataQ					m_DefectPADData_MicQ;
	DefectDataQ					m_MapDisplayData_MicQ;

	DefectDataQ					m_MergeData_MacQ;
	DefectDataQ					m_DefectData_MacQ;
	DefectDataQ					m_DefectPADData_MacQ;
	DefectDataQ					m_MapDisplayData_MacQ;
	
//System Value
public:
	FDC_Data						m_FdcData;
	int									m_AirPressureValue_Air;
	int									m_AirPressureValue_N2;
//System Param
public:
	//Lens Offset
	REVIEW_LENS_OFFSET		m_ReviewLensOffset;
	SCOPE_LENS_OFFSET		m_ScopeLensOffset;
	ReviewPosition					m_ReviewPos;
	Scope3DPosition				m_ScopePos;
	GripperPosition					m_GripperPos;
public://UI Object
	CGridCtrl m_gridStickInfo;
	CGridCtrl m_gridResultList;
public://result Proc
	CImageList m_ImageList;
	HTREEITEM m_ResultRoot_3DScope;	
	HTREEITEM m_ResultRoot_InsMicScan;
	HTREEITEM m_ResultRoot_InsMacScan;
	HTREEITEM m_ResultRoot_AreaScan3D;
	HTREEITEM m_ResultRoot_CDTP;	


	CTreeCtrl m_treeResult;
public://Sequence
	//측정 결과.
	bool m_ResultMeasurement[4];
	StickInfoByIndexer  m_StickLoadInfo;
	CSeq_StickExchange		m_SeqObj_StickExchange;
	CSeq_ThetaAlign			m_SeqObj_ThetaAlign;
	CSeq_CellAlign				m_SeqObj_CellAlign;
	CSeq_ReviewCalc				m_SeqObj_ReviewCalc;
	CSeq_CDTP					m_SeqObj_CDTP;
	CSeq_3DScope			m_SeqObj_3DScope;
	CSeq_AreaScan3D		m_SeqObj_AreaScan3D;
	CSeq_InsAutoReview		m_SeqObj_InsAutoReview;
	CSeq_Inspect_Mic			m_SeqObj_InsMic;
	CSeq_Inspect_Mac			m_SeqObj_InsMac;
	CSeq_AutoRun				m_SeqObj_AutoRun;
	CSeq_InitAM01			m_SeqObj_InitAM01;

	void SequenceObjectInit();
	static int SeqFlow_InitAM01(void *pObj, int NowFlowStep);
	static int SeqFlow_StickExchange(void *pObj, int NowFlowStep);
	static int SeqFlow_CellAlign(void *pObj, int NowFlowStep);
	static int SeqFlow_ReviewCalc(void *pObj, int NowFlowStep);
	static int SeqFlow_ThetaAlign(void *pObj, int NowFlowStep);
	static int SeqFlow_CDTP(void *pObj, int NowFlowStep);
	static int SeqFlow_AreaScan3D(void *pObj, int NowFlowStep);
	static int SeqFlow_3DScope(void *pObj, int NowFlowStep);
	static int SeqFlow_InsMic(void *pObj, int NowFlowStep);
	static int SeqFlow_InsMac(void *pObj, int NowFlowStep);
	static int SeqFlow_InsAutoReview(void *pObj, int NowFlowStep);
	static int SeqFlow_AutoRun(void *pObj, int NowFlowStep);
	CRoundButton2 m_btSeqStart[10];
	CRoundButton2 m_btSeqStop[10];
	CTextProgressCtrl m_progSeqViewBar[10];
	bool CheckAllRunSequence();

	//모든 결과 Data를 File에 기록 한다.
	void m_fnMeasureInputData(CDataFormat *pWriteResultObj);
	void m_fnInspectInputData(CDataFormat *pWriteResultObj);
	bool WriteMeasureAllResult();

public:

	CColorStaticST m_EQState[3];
	CColorStaticST m_EQProcessState[4];


	CInterServerInterface  m_ServerInterface;
	void m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...);
	bool Sys_fnInitVS64Interface();
	int				Sys_fnAnalyzeMsg(CMDMSG* cmdMsg);//Message Process Function.
	LRESULT		Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam);
	//////////////////////////////////////////////////////////////////////////
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	TLCheck m_TaskLinkCheck;
	//int m_TaskCheckCnt_Inspector;
	//int m_TaskCheckCnt_Indexer;
	//int m_TaskCheckCnt_Motion;
	//int m_TaskCheckCnt_Mesure3D;
	//int m_TaskCheckCnt_Area3DScan;
	//int m_TaskCheckCnt_AutoFocus;

	int m_NowLensIndex_Review;
	int m_NowLensIndex_3DScope;
	CXListBox m_listLogView;
	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_bLiveViewOnOff;
	CRoundButton2 m_btReviewLens[3];

	CRoundButton2 m_btRecipeSetting;
	CRoundButton2 m_btSysParameter;
	CRoundButton2 m_btMaintenance;
	CRoundButton2 m_btCIMInterface;
	CRoundButton2 m_btAlarmInterface;
	afx_msg void OnBnClickedBtCimInterface();
	afx_msg void OnBnClickedBtAlarmView();
	afx_msg void OnBnClickedBtLiveViewOnoff();
	AFModuleData	m_AfModuleData;
	CColorStaticST m_stTaskState[6];
	CRoundButton2 m_btReviewAFOn;
	CRoundButton2 m_btReviewAFOff;
	CRoundButton2 m_btSetRevewLightValue;
	CRoundButton2 m_btAutoFocusInit;
	CButton m_CheckAFInRange;
	CButton m_CheckAFInfocus;

	CEdit m_edReviewLightValue;
	ObjDistance m_CamToObjDis;
	void LoadHWParam();
	bool CheckProcRecipeName(char *pNewRecipe);
	void LoadRecipeParam(char *pFilePath, bool bAlignPosSet = false);
	void LoadNowProcRecipe(bool bAlignPosSet = false);
	LRESULT		OnRecipeChange(WPARAM wParam, LPARAM lParam);


	CCIMMsgViewDlg *m_pHostMsgViewDlg;
	LRESULT		OnHostMessage(WPARAM wParam, LPARAM lParam);

	void SetResult_Folder(CString strStickID);
	void CopyResult_Folder(CString strStickID);


	CString		m_CIM_Inspection_StartTime;
	CString		m_CIM_Inspection_EndTime;
	LRESULT		OnWriteInspectResult(WPARAM wParam, LPARAM lParam);
	
	afx_msg void OnBnClickedBtMainRecipe();
	afx_msg void OnBnClickedBtSystemRecipeView();
	afx_msg void OnBnClickedBtMainMaint();

	afx_msg void OnBnClickedBt3dLensId1();
	afx_msg void OnBnClickedBt3dLensId2();
	afx_msg void OnBnClickedBt3dLensId3();
	afx_msg void OnBnClickedBtnReviewLightSet();
	afx_msg void OnBnClickedBtAfOn();
	afx_msg void OnBnClickedBtAfOff();
	afx_msg void OnBnClickedChCrossOn();
	afx_msg void OnBnClickedChRollerOn();

	CRoundButton2 m_btSubView[6];
	afx_msg void OnBnClickedBtSubView();
	afx_msg void OnBnClickedBtInitAf();




	afx_msg void OnBnClickedBtSeqStart();
	afx_msg void OnBnClickedBtSeqStop();

	afx_msg void OnBnClickedButton2();

	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);


	CColorStaticST m_stOpenCloseDoor[8];
	CColorStaticST m_stLockCheckDoor[8];
	CRoundButton2 m_btUnlockFrontDoor;
	CRoundButton2 m_btUnlockSideDoor;
	CRoundButton2 m_btUnlockBackDoor;
	afx_msg void OnBnClickedBtDoorFront();
	afx_msg void OnBnClickedBtDoorEqSide();
	afx_msg void OnBnClickedBtDoorBack();
	CColorStaticST m_stKeyState;
	CColorStaticST m_stEnableKeyOnOff;
	CColorStaticST m_stTeachKeyLock;

	CRoundButtonStyle	m_tStyleAlarmBT;
	CRoundButtonStyle	m_tStyleCIMBT;
	CRoundButtonStyle	m_tStyleOn;	
	CRoundButtonStyle	m_tStyleOff;
	CRoundButton2 m_btSystemOnOff;
	CRoundButton2 m_btEMOState[4];
	CRoundButton2 m_btSaveReviewImage;
	CColorStaticST m_stStickDetect[2];
	CColorStaticST m_stTT01_AM01_pos;
	CColorStaticST m_stTT01_TURN_pos;
	CColorStaticST m_stTT01_UT02_pos;
	afx_msg void OnBnClickedRsetMotCmdAll();

	CRoundButton2 m_btChangeAutoMode;
	CRoundButton2 m_btChangeManualMode;
	CRoundButton2 m_btChangePMMode;
	afx_msg void OnBnClickedBtEqPmMode();
	afx_msg void OnBnClickedBtEqManualMode();
	afx_msg void OnBnClickedBtEqAutoMode();

	CColorStaticST m_stSignalTower[4];
	CRoundButton2 m_btBuzzerStop;
	afx_msg void OnBnClickedBtBuzzerStop();
	afx_msg void OnBnClickedButton3();
	
	afx_msg void OnBnClickedBtSaveReviewImg();
	afx_msg void OnBnClickedChDrawOverlay();
	BYTE *m_pSmallImgReadBuf;
	bool SetSmallDefectImg(CString strTargetPath, IplImage *pLoadImg = nullptr, DefectDataQ *pReadRetFileDataQ = nullptr);
	afx_msg void OnNMDblclkTrResult(NMHDR *pNMHDR, LRESULT *pResult);


	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();


	afx_msg void OnStnDblclickStTask_Inspector();
	afx_msg void OnStnDblclickStTask_Indexer();
	afx_msg void OnStnDblclickStTask_Measure();
	afx_msg void OnStnDblclickStTask_Motion();
	afx_msg void OnStnDblclickStTask_3DScan();
	afx_msg void OnStnDblclickStTask_AF();



	afx_msg void OnBnClickedChRevewCalc();
	afx_msg void OnBnClickedButton7();
};
