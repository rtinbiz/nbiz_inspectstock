#include "StdAfx.h"
#include "Seq_AutoRun.h"


CSeq_AutoRun::CSeq_AutoRun(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;

	m_pServerInterface = nullptr;
	m_pSeqObj_StickExchange = nullptr;
	m_pSeqObj_InitAM01 = nullptr;
	m_pSeqObj_CellAlign = nullptr;
	m_pSeqObj_CDTP = nullptr;
	m_pSeqObj_AreaScan3D = nullptr;
	m_pSeqObj_3DScope = nullptr;
	
	m_pSeqObj_InsMic = nullptr;
	m_pSeqObj_InsMac = nullptr;
}


CSeq_AutoRun::~CSeq_AutoRun(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
}

bool CSeq_AutoRun::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_AutoRun::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();
	G_WriteInfo(Log_Error, "CSeq_AutoRun Error Step Run( Z Axis Go to a safe position)");
	return 0;
}
void CSeq_AutoRun::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
int CSeq_AutoRun::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_AutoRun%03d> Sequence Start", ProcStep);

			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_SEQ_INIT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SEQ_INIT(%03d)>", ProcStep);
			m_pSeqObj_InitAM01->fnSeqFlowStart();
			int TimeOutWait = 18000;//About 30Min
			while(m_pSeqObj_InitAM01->fnSeqGetNowStep() != CSeq_InitAM01::SEQ_STEP_IDLE &&
				m_pSeqObj_InitAM01->fnSeqGetNowStep() != CSeq_InitAM01::SEQ_STEP_ERROR)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(100);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				m_pSeqObj_InitAM01->fnSeqSetStop();
				G_WriteInfo(Log_Check, "<SEQ_STEP_SEQ_INIT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pSeqObj_InitAM01->fnSeqGetNowStep() == CSeq_InitAM01::SEQ_STEP_ERROR)
			{
				G_WriteInfo(Log_Worrying, "<SEQ_STEP_SEQ_INIT> Seq Error");
				m_pSeqObj_InitAM01->fnSeqErrorReset();
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SEQ_CELL_ALIGN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SEQ_CELL_ALIGN(%03d)>");
			m_pSeqObj_CellAlign->fnSeqFlowStart();
			int TimeOutWait = 18000;//About 30Min
			while(m_pSeqObj_CellAlign->fnSeqGetNowStep() != CSeq_CellAlign::SEQ_STEP_IDLE &&
				m_pSeqObj_CellAlign->fnSeqGetNowStep() != CSeq_CellAlign::SEQ_STEP_ERROR)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(100);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				m_pSeqObj_CellAlign->fnSeqSetStop();
				G_WriteInfo(Log_Check, "<SEQ_STEP_SEQ_CELL_ALIGN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pSeqObj_CellAlign->fnSeqGetNowStep() == CSeq_CellAlign::SEQ_STEP_ERROR)
			{
				G_WriteInfo(Log_Worrying, "<SEQ_STEP_SEQ_CELL_ALIGN> Seq Error");
				m_pSeqObj_CellAlign->fnSeqErrorReset();
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			//Cell Align이 완료 되면 Host에 Inspection Start를 알린다.
			G_pCIMInterface->m_CIMInfodlg->OnBnClickedBtInspectionStart();

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SEQ_AREA_3DSCAN:
		{
			//Skip
			fnSeqSetNextStep();
			break;

			G_WriteInfo(Log_Normal, "<SEQ_STEP_SEQ_AREA_3DSCAN(%03d)>");
			m_pSeqObj_AreaScan3D->fnSeqFlowStart();
			int TimeOutWait = 30000;//About 3Min
			while(m_pSeqObj_AreaScan3D->fnSeqGetNowStep() != CSeq_AreaScan3D::SEQ_STEP_IDLE &&
				m_pSeqObj_AreaScan3D->fnSeqGetNowStep() != CSeq_AreaScan3D::SEQ_STEP_ERROR)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(100);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				m_pSeqObj_AreaScan3D->fnSeqSetStop();
				G_WriteInfo(Log_Check, "<SEQ_STEP_SEQ_AREA_3DSCAN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pSeqObj_AreaScan3D->fnSeqGetNowStep() == CSeq_AreaScan3D::SEQ_STEP_ERROR)
			{
				G_WriteInfo(Log_Worrying, "<SEQ_STEP_SEQ_AREA_3DSCAN> Seq Error");
				m_pSeqObj_AreaScan3D->fnSeqErrorReset();
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SEQ_CDTP_MEASURE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SEQ_CDTP_MEASURE(%03d)>");
			m_pSeqObj_CDTP->fnSeqFlowStart();
			int TimeOutWait = 18000;//About 30Min
			while(m_pSeqObj_CDTP->fnSeqGetNowStep() != CSeq_CDTP::SEQ_STEP_IDLE &&
				m_pSeqObj_CDTP->fnSeqGetNowStep() != CSeq_CDTP::SEQ_STEP_ERROR)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(100);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				m_pSeqObj_CDTP->fnSeqSetStop();
				G_WriteInfo(Log_Check, "<SEQ_STEP_SEQ_CDTP_MEASURE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pSeqObj_CDTP->fnSeqGetNowStep() == CSeq_CDTP::SEQ_STEP_ERROR)
			{
				G_WriteInfo(Log_Worrying, "<SEQ_STEP_SEQ_CDTP_MEASURE> Seq Error");
				m_pSeqObj_CDTP->fnSeqErrorReset();
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SEQ_3DSCOPE_MEASURE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SEQ_3DSCOPE_MEASURE(%03d)>");
			m_pSeqObj_3DScope->fnSeqFlowStart();
			int TimeOutWait = 30000;//About 30Min
			while(m_pSeqObj_3DScope->fnSeqGetNowStep() != CSeq_3DScope::SEQ_STEP_IDLE &&
				m_pSeqObj_3DScope->fnSeqGetNowStep() != CSeq_3DScope::SEQ_STEP_ERROR)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(100);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				m_pSeqObj_3DScope->fnSeqSetStop();
				G_WriteInfo(Log_Check, "<SEQ_STEP_SEQ_3DSCOPE_MEASURE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pSeqObj_3DScope->fnSeqGetNowStep() == CSeq_3DScope::SEQ_STEP_ERROR)
			{
				G_WriteInfo(Log_Worrying, "<SEQ_STEP_SEQ_3DSCOPE_MEASURE> Seq Error");
				m_pSeqObj_3DScope->fnSeqErrorReset();
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SEQ_INSPECT_MIC_DEFECT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SEQ_INSPECT_MIC_DEFECT(%03d)>");
			m_pSeqObj_InsMic->fnSeqFlowStart();
			int TimeOutWait = 180000;//About 30Min
			while(m_pSeqObj_InsMic->fnSeqGetNowStep() != CSeq_Inspect_Mic::SEQ_STEP_IDLE &&
				m_pSeqObj_InsMic->fnSeqGetNowStep() != CSeq_Inspect_Mic::SEQ_STEP_ERROR)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				m_pSeqObj_InsMic->fnSeqSetStop();
				G_WriteInfo(Log_Check, "<SEQ_STEP_SEQ_INSPECT_MIC_DEFECT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pSeqObj_InsMic->fnSeqGetNowStep() == CSeq_Inspect_Mic::SEQ_STEP_ERROR)
			{
				G_WriteInfo(Log_Worrying, "<SEQ_STEP_SEQ_INSPECT_MIC_DEFECT> Seq Error");
				m_pSeqObj_InsMic->fnSeqErrorReset();
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SEQ_INSPECT_MAC_DEFECT:
		{
			//Skip
			fnSeqSetNextStep();
			break;

			G_WriteInfo(Log_Normal, "<SEQ_STEP_SEQ_INSPECT_MAC_DEFECT(%03d)>");
			m_pSeqObj_InsMac->fnSeqFlowStart();
			int TimeOutWait = 180000;//About 30Min
			while(m_pSeqObj_InsMac->fnSeqGetNowStep() != CSeq_Inspect_Mac::SEQ_STEP_IDLE &&
				m_pSeqObj_InsMac->fnSeqGetNowStep() != CSeq_Inspect_Mac::SEQ_STEP_ERROR)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				m_pSeqObj_InsMac->fnSeqSetStop();
				G_WriteInfo(Log_Check, "<SEQ_STEP_SEQ_INSPECT_MAC_DEFECT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pSeqObj_InsMac->fnSeqGetNowStep() == CSeq_Inspect_Mac::SEQ_STEP_ERROR)
			{
				G_WriteInfo(Log_Worrying, "<SEQ_STEP_SEQ_INSPECT_MAC_DEFECT> Seq Error");
				m_pSeqObj_InsMac->fnSeqErrorReset();
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_UPLOAD_DATA:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_UPLOAD_DATA(%03d)>");

			//Data Write
			//::SendMessage(AfxGetMainWnd()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 9929);

			//Stick 판정 결과를 쓰자.

			WritePrivateProfileString(Section_Process_Data, Key_NowProcessResult, "OK", VS10Master_PARAM_INI_PATH);
			//WritePrivateProfileString(Section_Process_Data, Key_NowProcessResult, "NG", VS10Master_PARAM_INI_PATH);

			//검사가 완료 되면 Host에 Inspection End를 알린다.
			G_pCIMInterface->m_CIMInfodlg->OnBnClickedBtInspectionEnd();

			fnSeqSetNextStep();//Auto Increment
		}break;
			//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_SEQ_END_INIT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SEQ_END_INIT(%03d)>");
			m_pSeqObj_InitAM01->fnSeqFlowStart();
			int TimeOutWait = 180000;//About 30Min
			while(m_pSeqObj_InitAM01->fnSeqGetNowStep() != CSeq_InitAM01::SEQ_STEP_IDLE &&
				m_pSeqObj_InitAM01->fnSeqGetNowStep() != CSeq_InitAM01::SEQ_STEP_ERROR)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				m_pSeqObj_InitAM01->fnSeqSetStop();
				G_WriteInfo(Log_Check, "<SEQ_STEP_SEQ_INIT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pSeqObj_InitAM01->fnSeqGetNowStep() == CSeq_InitAM01::SEQ_STEP_ERROR)
			{
				G_WriteInfo(Log_Worrying, "<SEQ_STEP_SEQ_INIT> Seq Error");
				m_pSeqObj_InitAM01->fnSeqErrorReset();
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	//case SEQ_STEP_UNLOAD_STICK:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_STICK(%03d)>");
	//		m_pSeqObj_StickExchange->fnSeqFlowUnloadStart();
	//		int TimeOutWait = 180000;//About 30Min
	//		while(m_pSeqObj_StickExchange->fnSeqGetNowStep() != CSeq_Inspect_Mic::SEQ_STEP_IDLE &&
	//			m_pSeqObj_StickExchange->fnSeqGetNowStep() != CSeq_Inspect_Mic::SEQ_STEP_ERROR)
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			m_pSeqObj_StickExchange->fnSeqSetStop();
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_STICK%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(m_pSeqObj_StickExchange->fnSeqGetNowStep() == CSeq_Inspect_Mic::SEQ_STEP_ERROR)
	//		{
	//			G_WriteInfo(Log_Worrying, "<SEQ_STEP_UNLOAD_STICK> Seq Error");
	//			m_pSeqObj_StickExchange->fnSeqErrorReset();
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;

		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			G_MotObj.m_fnBuzzer2On(CALL_BUZZER2_AUTO_RUN_END);
			G_WriteInfo(Log_Normal, "<CSeq_AutoRun%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;
	default:
		{
			G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
			G_WriteInfo(Log_Normal, "<CSeq_AutoRun> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}