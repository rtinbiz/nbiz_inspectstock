#pragma once


// CCIMMsgViewDlg 대화 상자입니다.

class CCIMMsgViewDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCIMMsgViewDlg)

public:
	CCIMMsgViewDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCIMMsgViewDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MSG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	//CInterServerInterface  *m_pServerInterface;
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	COLORREF m_btBackColor;
	void InitCtrl_UI();
	CRoundButtonStyle m_tButtonStyle;	

	CString m_strCIMMsg[10];

	void SetMessageData(int MsgType, char *strpMsg);
};
