#pragma once
#include "afxwin.h"


// CSubDlg_InspectScan 대화 상자입니다.

class CSubDlg_InspectScan : public CDialogEx
{
	DECLARE_DYNAMIC(CSubDlg_InspectScan)

public:
	CSubDlg_InspectScan(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubDlg_InspectScan();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SUB_SCAN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	int									*m_pAlignCellStartX;
	int									*m_pAlignCellStartY;

	GLASS_PARAM					*m_pGlassReicpeParam;

	SAOI_IMAGE_INFO			*m_pstMaskImageInfo_Mic;
	SAOI_COMMON_PARA		*m_pstCommonPara_Mic;	

	SAOI_IMAGE_INFO			*m_pstMaskImageInfo_Mac;
	SAOI_COMMON_PARA		*m_pstCommonPara_Mac;	

	CInterServerInterface  *m_pServerInterface;
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	//Inspection Process
	bool								*m_pBlockEnd;
	DWORD							*m_pStartBlockTick;
	DWORD							*m_pStartLineTick;
	int									*m_pCamScnaLineCnt;
	int									*m_pCamScnaImgCnt;
	int									*m_pStartScanLineNum;
public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButton2 m_btGetNowPosCellStart;
	CRoundButton2 m_btAlignCellStartPos;
	CRoundButton2 m_btInitInspector;
	CRoundButton2 m_btSendRecipeParam;
	CRoundButton2 m_btSendStart;
	CRoundButton2 m_btSendStop;

	CRoundButton2 m_bt_MoveScanStart;
	CRoundButton2 m_btMoveOddScanStart;
	CRoundButton2 m_btMoveEvnScanStart;
	CRoundButton2 m_btMoveShift;

	CColorStaticST m_stScanInfo[5];
	CColorStaticST m_stNowScanLine;
	afx_msg void OnBnClickedBtUnitInit();
	afx_msg void OnBnClickedBtSendRecipeParam();
	
	afx_msg void OnBnClickedBtGetNowPos();
	
	afx_msg void OnBnClickedBtCellAlign();
	CStatic m_stScanLineCnt;
	CStatic m_stScanImgCnt;
	CStatic m_stTotalDefectCnt;
	CStatic m_stProcEndCnt;
	CStatic m_stProcTime;
	CStatic m_stBackup1_S;
	CStatic m_stWriteFile1_S;
	afx_msg void OnBnClickedBtSendStart();
	afx_msg void OnBnClickedBtSendStop();
	CStatic m_stInspectTaskState;

	

	afx_msg void OnBnClickedBtMoveScanStart();
	afx_msg void OnBnClickedBtMoveScanStartOdd();
	afx_msg void OnBnClickedBtMoveScanShift();
	afx_msg void OnBnClickedBtMoveScanStartEven();
};
