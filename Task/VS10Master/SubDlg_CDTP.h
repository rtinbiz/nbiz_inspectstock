#pragma once
#include "afxwin.h"


// CSubDlg_CDTP 대화 상자입니다.

class CSubDlg_CDTP : public CDialogEx
{
	DECLARE_DYNAMIC(CSubDlg_CDTP)

public:
	CSubDlg_CDTP(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubDlg_CDTP();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SUB_CDTP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CInterServerInterface  *m_pServerInterface;
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	CRoundButtonStyle m_tButtonStyle;	

	int m_CDTPNowIndex;
	int m_CDTPTotalCnt;
	CColorStaticST m_stIndex_CDTP;
	CColorStaticST m_stTotal_CDTP;
	CRoundButton2 m_btStartMeasure_CDTP;
	CRoundButton2 m_btStopMeasure_CDTP;
	
	int m_AutoReviewNowIndex;
	int m_AutoReviewTotalCnt;
	CColorStaticST m_stIndex_AutoReview;
	CColorStaticST m_stTotal_AutoReview;
	CRoundButton2 m_btStartAutoReview;
	CRoundButton2 m_btStopAutoReview;
	afx_msg void OnBnClickedBtStartCdTp();
	afx_msg void OnBnClickedBtStopCdTp();
	afx_msg void OnBnClickedBtStartAutoReview();
	afx_msg void OnBnClickedBtStopAutoReview();
};
