#ifndef _STATUS_STRUCTURE_H_
#define _STATUS_STRUCTURE_H_

#include "GlobalParam.h"
#include "GlobalEnumList.h"
#include "..\..\CommonHeader\Motion\MotionIO.h"


typedef struct tag_Machine_Status
{
	ALIO_A_In stNormalStatus;  //Door, Temperature, 장비 일반 사항.
	ALIO_B_In stBoxTableSensor; //Box Table, Box Detect Sensor
	ALIO_C_In stModuleStatus; //TM01, LT01, BO01, UT01
	ALIO_D_In stGripperStatus; //Gripper
	ALIO_E_In stPickerSensor;  //TM02
	ALIO_F_In stPickerCylinder;  //TM02 Cylinder
	ALIO_G_In stTurnTableSensor; //TT01 
	ALIO_H_In stReserve;
	ALIO_I_In stEmissionTable; //UT02 Status
	int			  nMotionPos[AXIS_Cnt];
	tag_Machine_Status()
	{
		memset(this, 0x00, sizeof(tag_Machine_Status));
	}
}STMACHINE_STATUS;



/**
@struct
@brief
@remark
-
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:49
*/
typedef struct tag_Motion_Status
{
	EL_MOTION_STATUS	 B_MOTION_STATUS;
	EL_CMD_STATUS	B_COMMAND_STATUS;
	UINT	UN_CMD_COUNT;
	UINT	UN_TIME_OUT_VAL;

	tag_Motion_Status()
	{
		memset(this, 0x00, sizeof(tag_Motion_Status));
	}
}STMOTION_STATUS;


/**
@struct
@brief
@remark
-
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:49
*/
typedef struct tag_LT01_StickStatus
{	
	UINT	unObjectType;
	BOOL	unObjectStatus;

	tag_LT01_StickStatus()
	{
		memset(this, 0x00, sizeof(tag_LT01_StickStatus));
	}
}SLT01_STICK_STATUS;


typedef struct tag_BoxTableStatus
{
	BOX_SIZE elBoxSize;
	BOX_STATUS elBoxStatus;
}ST_BOX_TABLE_STATUS;


#endif