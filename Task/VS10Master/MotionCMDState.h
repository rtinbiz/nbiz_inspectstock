#pragma once



//Observation Application Link State.
typedef enum
{
	ObserveSW_Init =-1,				//<<< 초기 상태
	ObserveSW_None =0,				//<<< Observation APP이 구동되지 않았다.
	ObserveSW_OK ,					//<<< Observation APP이 정상 구동중이다.
	ObserveSW_CtrlPowerOff,			//<<< Observation APP는 구동중이나 Controller가 Off 상태 이다.
	ObserveSW_NoneRemote,			//<<< Observation APP와 Controller는 구동중 이지만 Remote Mode가 아니다.
	////////
	ObserveSW_ManualMode			//<<< Main UI에서 Manual Mode로 변환 한것이다.
}ObsAppState;

//Observation App 동작상태 (ObserveSW_OK APP상태에서만 Check.)
typedef enum
{
	ActionObs_Init =-2,						//<<< 초기 상태
	ActionObs_Manual =-1,					//<<< ObserveSW_ManualMode일때는 상태 Check를 하지 않는다.
	ActionObs_None  =0,						//<<< 동작 명령 수행중이 아닐때.

	//ActionObs_AutoGain,						//<<< Auto Gain 동작 수행시.
	ActionObs_AutoFocus,					//<<< Auto Focus 동작 수행시.
	//ActionObs_AutoUpDownPosSetting,			//<<< 측정 Upper/Lower 위치 선정 수행시.
	ActionObs_AutoUpDownPosSetByImg,		//<<< 측정 Upper/Lower 위치 선정 수행시.
	ActionObs_StartMeasurement,				//<<< 3D Scan 수행시.
	ActionObs_AutoSaveData,					//<<< 측정 Data 저장 수행시.

	//ActionObs_AutoGain_Error,				//<<< Auto Gain 동작 수행시 Error.
	ActionObs_AutoFocus_Error,				//<<< Auto Focus 동작 수행시 Error.
	//ActionObs_AutoUpDownPosSet_Error,		//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
	ActionObs_AutoUpDownPosSetByImg_Error,	//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
	ActionObs_StartMeasurement_Error,		//<<< 3D Scan 수행시 Error.
	ActionObs_AutoSaveData_Error,			//<<< 측정 Data 저장 수행시 Error.

	ActionObs_RemoteMode_Error//<<< 측정 가능 Remote Mode가 아니다.
}ObsActionState;