#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"

#include "Seq_StickExchange.h"
#include "Seq_ThetaAlign.h"
#include "Seq_CDTP.h"
#include "Seq_AreaScan3D.h"
#include "Seq_3DScope.h"
#include "Seq_Inspection_Mic.h"
#include "Seq_Inspection_Mac.h"
#include "Seq_InitAM01.h"
#include "Seq_CellAlign.h"
class CSeq_AutoRun: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_SEQ_INIT,
		SEQ_STEP_SEQ_CELL_ALIGN,
		SEQ_STEP_SEQ_AREA_3DSCAN,
		SEQ_STEP_SEQ_CDTP_MEASURE,
		SEQ_STEP_SEQ_3DSCOPE_MEASURE,
		SEQ_STEP_SEQ_INSPECT_MIC_DEFECT,
		SEQ_STEP_SEQ_INSPECT_MAC_DEFECT,
		SEQ_STEP_UPLOAD_DATA,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_SEQ_END_INIT,
		//SEQ_STEP_UNLOAD_STICK,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_AutoRun(void);
	~CSeq_AutoRun(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
public:
	CInterServerInterface  *m_pServerInterface;
	CSeq_StickExchange	*m_pSeqObj_StickExchange;
	CSeq_InitAM01		*m_pSeqObj_InitAM01;
	CSeq_CellAlign			*m_pSeqObj_CellAlign;
	CSeq_CDTP				*m_pSeqObj_CDTP;
	CSeq_AreaScan3D	*m_pSeqObj_AreaScan3D;
	CSeq_3DScope		*m_pSeqObj_3DScope;
	CSeq_Inspect_Mic		*m_pSeqObj_InsMic;
	CSeq_Inspect_Mac		*m_pSeqObj_InsMac;
};

