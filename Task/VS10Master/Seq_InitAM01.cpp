#include "StdAfx.h"
#include "Seq_InitAM01.h"


CSeq_InitAM01::CSeq_InitAM01(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;

	m_RecvCMD_Ret =-1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;

	m_pNowLensIndex_3DScope = nullptr;
	m_pReviewPos =  nullptr;
	m_pScopePos = nullptr;
	m_pGripperPos = nullptr;
	m_pTaskLinkCheck = nullptr;
	m_pSubCIMInterfaceView = nullptr;
}


CSeq_InitAM01::~CSeq_InitAM01(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
}

bool CSeq_InitAM01::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_InitAM01::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();
	G_WriteInfo(Log_Error, "CSeq_InitAM01 Error Step Run");
	return 0;
}
void CSeq_InitAM01::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
int CSeq_InitAM01::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_InitAM01%03d> Sequence Start", ProcStep);

			//AfxMessageBox("Jig Output Plz...!");

			//if(AfxMessageBox("Jig Output ???",MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDNO)
			//{		
			//	G_WriteInfo(Log_Worrying,"SEQ_STEP_UNLOAD_OUTPUT_JIG", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			//////////////////////////////////////////////////////////////////////////

			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_DATA_CHECK:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_DATA_CHECK(%03d)>", ProcStep);
			G_MotObj.m_fnResetAllCMDRun();
			

			//G_MotObj.m_fnSetSignalTower(STWOER_MODE7_PM);
			fnSeqSetNextStep();//Auto Increment
		}break;
	//case SEQ_STEP_DOOR_LOCK:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_DOOR_LOCK(%03d)>", ProcStep);
	//		G_MotObj.m_fnDoorLockFront();
	//		int TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_DOOR_FRONT_LOCK].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_DOOR_LOCK%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_DOOR_FRONT_LOCK].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_DOOR_LOCK%03d> Mot Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}

	//		G_MotObj.m_fnDoorLockSide();
	//		TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_DOOR_SIDE_LOCK].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_DOOR_LOCK%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_DOOR_SIDE_LOCK].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_DOOR_LOCK%03d> Mot Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		G_MotObj.m_fnDoorLockBack();
	//		TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_DOOR_BACK_LOCK].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_DOOR_LOCK%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_DOOR_BACK_LOCK].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_DOOR_LOCK%03d> Mot Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_SENSOR_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SENSOR_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SENSOR_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SENSOR_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			Sleep(500);//위치이동 후 살짝 기다린다.
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_TASK_LINK_CHECK:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_TASK_LINK_CHECK(%03d)>", ProcStep);

			
			if(m_pTaskLinkCheck!= nullptr)
			{
				bool checkLink = true;
				if(m_pTaskLinkCheck->m_TaskCheckCnt_Inspector >= 5)
				{//3)Inspect Task Check
					checkLink = false;
					G_WriteInfo(Log_Error,"<m_TaskCheckCnt_Inspector> Task Obj Link Error(CNT:%d)", m_pTaskLinkCheck->m_TaskCheckCnt_Inspector);
				}
				if(m_pTaskLinkCheck->m_TaskCheckCnt_Indexer >= 5)
				{//6)Indexer Task Check
					checkLink = false;
					G_WriteInfo(Log_Error,"<m_TaskCheckCnt_Indexer> Task Obj Link Error(CNT:%d)", m_pTaskLinkCheck->m_TaskCheckCnt_Indexer);
				}
				if(m_pTaskLinkCheck->m_TaskCheckCnt_Motion >= 5)
				{//1)Motion Task Check
					checkLink = false;
					G_WriteInfo(Log_Error,"<m_TaskCheckCnt_Motion> Task Obj Link Error(CNT:%d)", m_pTaskLinkCheck->m_TaskCheckCnt_Motion);
				}
				if(m_pTaskLinkCheck->m_TaskCheckCnt_Mesure3D >= 5)
				{//5)Scope Task Check
					checkLink = false;
					G_WriteInfo(Log_Error,"<m_TaskCheckCnt_Mesure3D> Task Obj Link Error(CNT:%d)", m_pTaskLinkCheck->m_TaskCheckCnt_Mesure3D);
				}
				if(m_pTaskLinkCheck->m_TaskCheckCnt_Area3DScan >= 5)
				{//4)Area 2D Task Check
					checkLink = false;
					G_WriteInfo(Log_Error,"<m_TaskCheckCnt_Area3DScan> Task Obj Link Error(CNT:%d)", m_pTaskLinkCheck->m_TaskCheckCnt_Area3DScan);
				}
				if(m_pTaskLinkCheck->m_TaskCheckCnt_AutoFocus >= 5)
				{//2)AF Task Check
					checkLink = false;
					G_WriteInfo(Log_Error,"<m_TaskCheckCnt_AutoFocus> Task Obj Link Error(CNT:%d)", m_pTaskLinkCheck->m_TaskCheckCnt_AutoFocus);
				}

				if(checkLink == false)
				{
					G_WriteInfo(Log_Error,"<SEQ_STEP_TASK_LINK_CHECK%03d> Task Link Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			else
			{
				G_WriteInfo(Log_Error,"<SEQ_STEP_TASK_LINK_CHECK%03d> Task Obj Link Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;

		
	//case SEQ_STEP_REVIEW_LENS_INIT:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_LENS_INIT(%03d)>", ProcStep);
	//		m_RecvCMD_Ret = -1;
	//		m_WaitCMD = nBiz_Seq_Init;
	//		m_RecvCMD = 0;
	//		m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_Init, nBiz_Unit_Zero);

	//		int TimeOutWait = 3000;//About 20sec
	//		while(m_WaitCMD != m_RecvCMD)
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_RecvCMD_Ret = -1;
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;

	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_LENS_INIT%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(m_RecvCMD_Ret >0)
	//		{//TT01이 들어온 상태가 아니다.
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_LENS_INIT%03d> Review Lens Init Error!!!!", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	case SEQ_STEP_REVIEW_LENS_CHANGE_2xLens://Lens가 바뀔때 Light도 바뀐다.
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_LENS_CHANGE_2xLens(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Func_ReviewLens;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, REVIEW_LENS_2X, nBiz_Unit_Zero);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_LENS_CHANGE_2xLens(%03d)> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != REVIEW_LENS_2X)
			{//TT01이 들어온 상태가 아니다.
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_LENS_CHANGE_2xLens%03d> Review Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			m_RecvCMD_Ret = 0;
			m_WaitCMD = 0;
			m_RecvCMD = 0;
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_LENS_CHANGE_10xLens:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_LENS_CHANGE_10xLens(%03d)>", ProcStep);
			m_WaitCMD = nBiz_3DSend_SelectLens;
			m_RecvCMD = 0;
			//저 배율 Scope lens로 AF를 수행 한다.
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, SCOPE_LENS_10X);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_LENS_CHANGE_10xLens%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			m_WaitCMD = 0;
			m_RecvCMD = 0;
			if(*m_pNowLensIndex_3DScope != SCOPE_LENS_10X)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_LENS_CHANGE_10xLens%03d> Scope Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_LIGHT_RING_BACK_REFLECT_OFF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_LIGHT_RING_BACK_REFLECT_SET(%03d)>", ProcStep);
			G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, 0 ,0);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_LIGHT_RING_BACK_REFLECT_SET%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_LIGHT_RING_BACK_REFLECT_SET%03d> Light Off Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	//case SEQ_STEP_LOADCELL_LINK_OPEN:
	//	{
	//		//G_WriteInfo(Log_Normal, "<SEQ_STEP_LOADCELL_LINK_OPEN(%03d)>", ProcStep);
	//		////2)Grip Close
	//		//G_MotObj.m_fnStickTensionZeroSet();
	//		//int TimeOutWait = 3000;//About 30sec
	//		//while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_ZERO].B_MOTCMD_State != IDLE )
	//		//{
	//		//	TimeOutWait --;
	//		//	if(TimeOutWait<0 || m_SeqStop == true)
	//		//	{
	//		//		m_WaitCMD = 0;
	//		//		m_RecvCMD = 0;
	//		//		if(m_SeqStop== true)
	//		//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//		//		else
	//		//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//		//		break;
	//		//	}
	//		//	Sleep(10);
	//		//}
	//		//if(TimeOutWait<0 || m_SeqStop == true)
	//		//{
	//		//	G_WriteInfo(Log_Check, "<SEQ_STEP_LOADCELL_LINK_OPEN%03d> Stop", ProcStep);
	//		//	fnSeqSetNextStep(SEQ_STEP_ERROR);
	//		//	break;
	//		//}
	//		//if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_ZERO].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		//{
	//		//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_LOADCELL_LINK_OPEN%03d> ACC Run Error", ProcStep);
	//		//	fnSeqSetNextStep(SEQ_STEP_ERROR);
	//		//	break;
	//		//}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	//case SEQ_STEP_QRCODE_LINK_OPEN:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_QRCODE_LINK_OPEN(%03d)>", ProcStep);
	//		//2)Grip Close
	//		G_MotObj.m_fnQRCodeRead();
	//		int TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_QR_READ].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_QRCODE_LINK_OPEN%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		//if(G_MotObj.m_MotCMD_STATUS[AM01_QR_READ].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		//{
	//		//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_QRCODE_LINK_OPEN%03d> ACC Run Error", ProcStep);
	//		//	fnSeqSetNextStep(SEQ_STEP_ERROR);
	//		//	break;
	//		//}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	//case SEQ_STEP_AIRE_LINK_OPEN:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_AIRE_LINK_OPEN(%03d)>", ProcStep);
	//		//2)Grip Close
	//		G_MotObj.m_fnQRCodeRead();
	//		int TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_AIR_READ].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_AIRE_LINK_OPEN%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_AIR_READ].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_QRCODE_LINK_OPEN%03d> ACC Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		int AirValue = G_MotObj.m_MotCMD_STATUS[AM01_AIR_READ].ActionResult.newPosition1;
	//		int N2Value = G_MotObj.m_MotCMD_STATUS[AM01_AIR_READ].ActionResult.newPosition2;

	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	//case SEQ_STEP_SPACE_SENSOR_LINK_OPEN:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SENSOR_LINK_OPEN(%03d)>", ProcStep);
	//		float targetPos = 0.0f;
	//		G_MotObj.m_fnSpaceSensorMeasureStart(false, targetPos);
	//		int TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_MEASURE].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SENSOR_LINK_OPEN%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_MEASURE].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SENSOR_LINK_OPEN%03d> Mot Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	case SEQ_STEP_CHECK_STICK:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CHECK_STICK(%03d)>", ProcStep);
			if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1 ||
				G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1)
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_CHECK_STICK%03d> Stick Exist In Gripper", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_EQ_RUN_STATE_SET);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	//case SEQ_STEP_GRIPPER_OPEN:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_OPEN(%03d)>", ProcStep);
	//		//2)Grip Close
	//		G_MotObj.m_fnStickGripperOpen();
	//		int TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_OPEN%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_OPEN%03d> Mot Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	case SEQ_STEP_GRIPPER_SOL_BWD:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_SOL_BWD(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnStickTentionBWD();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_BWD].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_SOL_BWD%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_BWD].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_SOL_BWD%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_GRIPPER_CLOSE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_CLOSE(%03d)>", ProcStep);
			//2)Grip Close
			G_MotObj.m_fnStickGripperClose();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_CLOSE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_CLOSE%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_GRIPPER_ALL_STANDBY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_ALL_STANDBY(%03d)>", ProcStep);
			//1)Grip Tension Axis (4*4 Axis 후퇴)
			//Left 4 Axis, Right 4Axis를 Standby Pos Move 시킨다.
			G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);
			G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);

			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State != IDLE ||
				G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_ALL_STANDBY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok ||
				G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_ALL_STANDBY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_GRIPPER_Z_STANDBY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_Z_STANDBY(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_Z_STANDBY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_Z_STANDBY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_GRIPPER_JIG_STANDBY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_JIG_STANDBY(%03d)>", ProcStep);

			if(G_MotObj.m_fnStickJigMove(m_pGripperPos->nStickJig_Standby) == true)
			{
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_JIG_STANDBY%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_JIG_STANDBY%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				//Jig Out일때는 센서도 본다.
				if(G_MotInfo.m_pMotState->stEmissionTable.In15_Sn_MaskJigBWD != 1)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_JIG_STANDBY%03d> Mask Jig BWD Detect Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			else
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_JIG_STANDBY%03d> Jig Interlock Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_EQ_RUN_STATE_SET:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_EQ_RUN_STATE_SET(%03d)>", ProcStep);

			::SendMessage(AfxGetMainWnd()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 7751);

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CNT:
		{
			//G_MotObj.m_fnSetSignalTower(STWOER_MODE2_IDLE1);

			if(m_pSubCIMInterfaceView != nullptr)
			{
				m_pSubCIMInterfaceView->OnBnClickedBtEqNormal();
				m_pSubCIMInterfaceView->OnBnClickedBtPrstIdle();
			}
			

			G_WriteInfo(Log_Normal, "<CSeq_InitAM01%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_InitAM01> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}