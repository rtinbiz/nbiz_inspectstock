#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
class CSeq_CellAlign: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_STICK_STATE_CHECK,
		//========================
		SEQ_STEP_SPACE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_CELL_ALIGN_LENS_CHANGE,
		SEQ_STEP_CELL_ALIGN_REVIEW_LIGHT,
		SEQ_STEP_CELL_ALIGN_LIGHT,
		//----------------------------------------
		SEQ_STEP_CELL_ALIGN_MOVE_POS,
		SEQ_STEP_CELL_ALIGN_REVIEW_POS_Z,
		SEQ_STEP_CELL_ALIGN_REVIEW_UP,
		SEQ_STEP_CELL_ALIGN_REVIEW_AF,
		SEQ_STEP_CELL_ALIGN_REVIEW_GRAB,
		SEQ_STEP_CELL_ALIGN_POINT_FIND,
		SEQ_STEP_CELL_ALIGN_SET_VALUE,
		SEQ_STEP_CELL_ALIGN_POS_MOVE,
		SEQ_STEP_CELL_ALIGN_END_LIGHT,
		//----------------------------------------
		SEQ_STEP_CELL_ALIGN_END_LIGHT_DOWN,
		SEQ_STEP_CELL_ALIGN_END_REVIEW_DOWN,
		SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_Z,
		SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_XY,
		SEQ_STEP_CELL_ALIGN_END_SCOPE_SAFETY_XY,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_CellAlign(void);
	~CSeq_CellAlign(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:
	int									*m_pAlignCellStartX;
	int									*m_pAlignCellStartY;

	REVIEW_LENS_OFFSET		*m_pReviewLensOffset;
	CLiveViewCtrl		*m_pLiveViewObj;
	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;

	IplImage *m_pNowGrabImg;
	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;
	CInterServerInterface  *m_pServerInterface;

	IplImage *m_pProcGrayImgBuf;

	CvPoint Find_CELL_LeftTopEdge( IplImage *pTarget, int Cnt, CvRect *pBuf, double avrW, double avrH);
	CvPoint AlignImgProc(IplImage *pTarget);
};

