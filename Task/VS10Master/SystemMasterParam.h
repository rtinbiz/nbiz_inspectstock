#pragma once
//Measur Task에 공유가 걸린 폴더를 나타낸다.

//#define THEATA_ALIGN_GRAB1_IMG_Name "Theta_Align1_Img.jpg"
//#define THEATA_ALIGN_GRAB2_IMG_Name "Theta_Align2_Img.jpg"
//#define THEATA_ALIGN_GRABT_IMG_Name "Theta_AlignT_Img.jpg"
//#define THEATA_ALIGN_GRAB1_IMG_PATH		"Z:\\Measer3D\\Theta_Align1_Img.jpg"
//#define THEATA_ALIGN_GRAB2_IMG_PATH		"Z:\\Measer3D\\Theta_Align2_Img.jpg"
//#define THEATA_ALIGN_GRABT_IMG_PATH		"Z:\\Measer3D\\Theta_AlignT_Img.jpg"
#define SCOPE_RESULT_SAVE_PATH				"Z:\\[05_Measure]\\VS31Measure3D\\"


//////////////////////////////////////////////////////////////////////////
//Master의 Local Drive이다.(각Task에서 아래 경로에 Upload를 한다.)

#define VS11Indexer_Result_PATH		"E:\\MasterShared\\[02_Indexer]"
#define VS21Inspector_Result_PATH	"E:\\MasterShared\\[03_Inspector]"
#define VS10Master_Result_PATH		"E:\\MasterShared\\[04_Master]"
#define VS31Mesure3D_Result_PATH	"E:\\MasterShared\\[05_Measure]"
#define VS32Mesure2D_Result_PATH	"E:\\MasterShared\\[05_Measure]"

#define MasterToCIM_Result_PATH_Ret			"R:\\"//CIM PC 공유 Folder Result *.dat
#define MasterToCIM_RName_IMAGE		"Image"//CIM PC 공유 Folder 이물검사 Review Image *.jpg
//#define MasterToCIM_RName_CDTP			"CDTP"//CIM PC 공유 Folder CDTP Image *.jpg
//#define MasterToCIM_RName_WAVE		"WAVE"//CIM PC 공유 Folder WAVE Result Image *.jpg
//#define MasterToCIM_RName_DOONTUK	"DOONTUK"//CIM PC 공유 Folder WAVE DoonTuk *.jpg

//#define TP_ALIGN_PATH_2X "D:\\nBiz_InspectStock\\Data\\Resource\\VS10Master\\TP_Align_2x.bmp"
#define TP_ALIGN_PATH_10X "D:\\nBiz_InspectStock\\Data\\Resource\\VS10Master\\TP_Align_10x.bmp"
#define TP_ALIGN_PATH_20X "D:\\nBiz_InspectStock\\Data\\Resource\\VS10Master\\TP_Align_20x.bmp"

#define	LOCAL_RESULT_FOLDER			"AOI_Result\\UpDate"//VS21Inspector_Result_PATH

#define SetECIDList_PATH "D:\\nBiz_InspectStock\\Data\\Task_INI\\SetSysECID_List.ini"
#define  Section_ECID_INFO		"ECID_INFO"
#define  Section_ECID_CNT		"ECID_CNT"
#define  Section_ECID_		"ECID_"
#define  Key_ECName	"ECName"
#define  Key_ECSLL		"ECSLL"
#define  Key_ECWLL		"ECWLL"
#define  Key_ECDEF		"ECDEF"
#define  Key_ECWUL		"ECWUL"
#define  Key_ECSUL		"ECSUL"

#define SetAlarmList_PATH "D:\\nBiz_InspectStock\\Data\\Task_INI\\SetSysAlarm_List.ini"
#define  Section_Alarm		"AlarmInfo"
#define  Key_AlarmTotalCnt	"AlarmTotalCnt"
#define  Key_AlarmIndex_		"AlarmIndex_"
#define  Key_AlarmTime_		"AlarmTime_"

#define PosCheckInpos	3 //+, - 6um이내에 있어야 한다.
#define Interlock_PARAM_INI_PATH "D:\\nBiz_InspectStock\\Data\\Task_INI\\Interlock_Sys_Param.ini"
#define  Section_SCOPE		"SCOPE"

#define  Key_Z_Down_Max						"Z_Down_Max"
#define  Key_Z_Down_Shift_Min				"Z_Down_Shift_Min"
#define  Key_Z_Down_Shift_Max				"Z_Down_Shift_Max"
#define  Key_Z_Down_Drive_Min				"Z_Down_Drive_Min"
#define  Key_Z_Down_Drive_Max				"Z_Down_Drive_Max"

#define  Section_SCAN		"SCAN"
#define  Key_Drive_Min			"Drive_Min"
#define  Key_Drive_Max			"Drive_Max"
#define  Key_Shift_Min			"Shift_Min"
#define  Key_Shift_Max			"Shift_Max"

#define  Key_Space_Shift_Min "Space_Shift_Min"
#define  Key_Space_Shift_Max "Space_Shift_Max"



#define  Section_JIG_INTERLOCK		"JIG_INTERLOCK"
#define  Key_RingLightSol_Scan_Shift_Min				"RingLightSol_Scan_Shift_Min"
#define  Key_RingLightSol_Scan_Shift_Max				"RingLightSol_Scan_Shift_Max"
#define  Key_ReviewSol_Scan_Shift_Min				"ReviewSol_Scan_Shift_Min"
#define  Key_ReviewSol_Scan_Shift_Max				"ReviewSol_Scan_Shift_Max"

//Indexer Interlock 
#define  Section_BT01		"BT01"
#define  Key_Table_UpDn_Min		"Table_UpDn_Min"
#define  Key_Table_UpDn_Max		"Table_UpDn_Max"
#define  Key_UpDn_SafeZone_TM01_Min		"UpDn_SafeZone_TM01_Min"
#define  Key_UpDn_SafeZone_TM01_Max		"UpDn_SafeZone_TM01_Max"
//#define  Key_UpDn_TM02DriveLimit		"UpDn_TM02DriveLimit"


#define  Section_TM01		"TM01"
#define  Key_Fork_Drive_Min		"Fork_Drive_Min"
#define  Key_Fork_Drive_Max		"Fork_Drive_Max"

#define  Section_LT01		"LT01"
#define  Key_Pusher_Min		"Pusher_Min"
#define  Key_Pusher_Max		"Pusher_Max"

#define  Section_BO01		"BO01"
#define  Key_Drive_Min			"Drive_Min"
#define  Key_Drive_Max			"Drive_Max"
#define  Key_UpDn_Min 			"UpDn_Min"
#define  Key_UpDn_Max 			"UpDn_Max"
#define  Key_Rotate_Min 		"Rotate_Min"
#define  Key_Rotate_Max 		"Rotate_Max"
#define  Key_UpDn_SafeZone_One_Min 		"UpDn_SafeZone_One_Min"
#define  Key_UpDn_SafeZone_One_Max 		"UpDn_SafeZone_One_Max"
#define  Key_UpDn_SafeZone_Two_Min 		"UpDn_SafeZone_Two_Min"
#define  Key_UpDn_SafeZone_Two_Max 		"UpDn_SafeZone_Two_Max"
#define  Key_Drive_SafeZone_Min 					"Drive_SafeZone_Min"
#define  Key_Drive_SafeZone_Max 				"Drive_SafeZone_Max"
#define  Key_Rotate_SafeZone_Min 				"Rotate_SafeZone_Min"
#define  Key_Rotate_SafeZone_Max 				"Rotate_SafeZone_Max"

#define  Section_UT01		"UT01"
#define  Key_Pusher_Min		"Pusher_Min"
#define  Key_Pusher_Max		"Pusher_Max"

#define  Section_TM02	"TM02"
#define  Key_Drive_Min		"Drive_Min"
#define  Key_Drive_Max		"Drive_Max"
#define  Key_UpDn_Min 		"UpDn_Min"
#define  Key_UpDn_Max 		"UpDn_Max"
#define  Key_Tilt_Min 			"Tilt_Min"
#define  Key_Tilt_Max 			"Tilt_Max"

#define  Key_Drive_TT01_Pos_Low 		"Drive_TT01_Pos_Low"
#define  Key_Drive_TT01_Pos_High 		"Drive_TT01_Pos_High"
#define  Key_UpDn_TT01_Max 		"UpDn_TT01_Max"
#define  Key_Drive_UT02_Pos_Low 		"Drive_UT02_Pos_Low"
#define  Key_Drive_UT02_Pos_High 		"Drive_UT02_Pos_High"
#define  Key_UpDn_UT02_Max 		"UpDn_UT02_Max"
#define  Key_Drive_UT01_Pos_Low 		"Drive_UT01_Pos_Low"
#define  Key_Drive_UT01_Pos_High 		"Drive_UT01_Pos_High"
#define  Key_UpDn_UT01_Max 		"UpDn_UT01_Max"
#define  Key_Drive_LT01_Pos_Low 		"Drive_LT01_Pos_Low"
#define  Key_Drive_LT01_Pos_High 		"Drive_LT01_Pos_High"
#define  Key_UpDn_LT01_Max 		"UpDn_LT01_Max"
#define  Key_Drive_BT01_Pos_Low 		"Drive_BT01_Pos_Low"
#define  Key_Drive_BT01_Pos_High 		"Drive_BT01_Pos_High"
#define  Key_UpDn_BT01_Max 		"UpDn_BT01_Max"




#define  Section_TT01		"TT01"
#define  Key_Drive_Min			"Drive_Min"
#define  Key_Drive_Max			"Drive_Max"

#define  Key_Rotate_Min 		"Rotate_Min"
#define  Key_Rotate_Max 		"Rotate_Max"
#define  Key_Tilt_Min 			"Tilt_Min"
#define  Key_Tilt_Max 			"Tilt_Max"
//Indexer Interlock 
//////////////////////////////////////////////////////////////////////////
#define VS10Master_RECIPE_INI_PATH "D:\\nBiz_InspectStock\\Data\\StickRecipe"
//////////////////////////////////////////////////////////////////////////
#define VS10Master_PARAM_INI_PATH "D:\\nBiz_InspectStock\\Data\\Task_INI\\VS10Master_Sys_Param.ini"

//기준 Camera 대비 거리를 나타 낸다.
#define  Section_Process_Data		"Process_Data"
#define  Key_NowRunBoxID				"NowRunBoxID"
#define  Key_NowRunStickID				"NowRunStickID"
#define  Key_NowRunRecipeName		"NowRunRecipeName"
#define  Key_NowProcessResult		"NowProcessResult"
#define  Key_NowCellAlignPosShift		"NowCellAlignPosShift"
#define  Key_NowCellAlignPosDrive		"NowCellAlignPosDrive"


#define  Section_InspectionInfo		"InspectionInfo"
#define  Key_ScanLineShiftDistance		"ScanLineShiftDistance"
#define  Key_ScanImgResolution_W		"ScanImgResolution_W"
#define  Key_ScanImgResolution_H		"ScanImgResolution_H"


#define  Section_GripperPos		"GripperPos"
#define  Key_Gripper_ZPos_TT01Out		"Gripper_ZPos_TT01Out"
#define  Key_Gripper_ZPos_Grip_Standby	"Gripper_ZPos_Grip_Standby"
#define  Key_Gripper_ZPos_Grip				"Gripper_ZPos_Grip"
#define  Key_Gripper_ZPos_Measure		"Gripper_ZPos_Measure"
#define  Key_Gripper_ZPos_Standby		"Gripper_ZPos_Standby"

#define  Key_Gripper_TPos_Standby				"Gripper_TPos_Standby"
#define  Key_Gripper_TPos_Grip						"Gripper_TPos_Grip"
#define  Key_Gripper_TPos_TensionStandby		"Gripper_TPos_TensionStandby"


#define  Key_Gripper_Jig_StandbyPos		"Gripper_Jig_StandbyPos"
#define  Key_Gripper_Jig_MeasurePos		"Gripper_Jig_MeasurePos"


#define  Section_ScopePos		"ScopePos"
#define  Key_Scope_ZPos_Measure		"Scope_ZPos_Measure"
#define  Key_Scope_ZPos_Standby		"Scope_ZPos_Standby"
//#define  Key_Scope_ZPos_ThetaAlign		"Scope_ZPos_ThetaAlign"
//#define  Key_Scope_ZPos_Area3DMeasure		"Scope_ZPos_Area3DMeasure"

#define  Key_Space_ZPos_Measure		"Space_ZPos_Measure"
#define  Key_Space_ZPos_Standby		"Space_ZPos_Standby"

#define  Key_Scope_Standby_Shift		"Scope_Standby_Shift"
#define  Key_Scope_Standby_Drive		"Scope_Standby_Drive"
//#define  Key_Scope_ThetaAlignLensIndex	"Scope_ThetaAlignLensIndex"
#define  Key_Scope_ThetaAlignDrivePos1	"Scope_ThetaAlignDrivePos1"
#define  Key_Scope_ThetaAlignShiftPos1		"Scope_ThetaAlignShiftPos1"
#define  Key_Scope_ThetaAlignDrivePos2	"Scope_ThetaAlignDrivePos2"
#define  Key_Scope_ThetaAlignShiftPos2		"Scope_ThetaAlignShiftPos2"

#define  Key_Scope_ThetaAlignLightVale		"Scope_ThetaAlignLightVale"
#define  Key_Scope_ThetaAlignPos2Offset		"Scope_ThetaAlignPos2Offset"
//#define  Key_Scope_ThetaAlignAFDistance		"Scope_ThetaAlignAFDistance"

#define  Section_ScanPos		"ScanPos"

#define  Key_Scan_ZPos_Review		"Scan_ZPos_Review"
#define  Key_Scan_ZPos_Standby		"Scan_ZPos_Standby"
#define  Key_Scan_ZPos_Inspection		"Scan_ZPos_Inspection"
#define  Key_Scan_Standby_Shift		"Scan_Standby_Shift"
#define  Key_Scan_Standby_Drive		"Scan_Standby_Drive"
#define  Key_CellAlign_LensIndex	"CellAlign_LensIndex"
#define  Key_CellAlign_BackLight	"CellAlign_BackLight"


//#define  Key_Review_CDTP_LensIndex	"Review_CDTP_LensIndex"
#define  Key_Review_AutoReview_LensIndex	"Review_AutoReview_LensIndex"
#define  Key_Review_AutoReview_ReflectLight	"Review_AutoReview_ReflectLight"
#define  Key_Review_AutoReview_MaxCnt		"Review_AutoReview_MaxCnt"
#define  Key_Review_AutoReview_BackLight	"Review_AutoReview_BackLight"//0보다 크면 Back Light가 따라다닌다.

#define  Key_Review_AutoReviewMac_LensIndex		"Review_AutoReviewMac_LensIndex"
#define  Key_Review_AutoReviewMac_ReflectLight	"Review_AutoReviewMac_ReflectLight"
#define  Key_Review_AutoReviewMac_MaxCnt		"Review_AutoReviewMac_MaxCnt"
#define  Key_Review_AutoReviewMac_BackLight		"Review_AutoReviewMac_BackLight"//0보다 크면 Back Light가 따라다닌다.

#define  Key_Review_CalLens_ZUpDnPos		"Review_CalLens_ZUpDnPos"
#define  Key_Review_CalLens2X_ReflectLight	"Review_CalLens2X_ReflectLight"
#define  Key_Review_CalLens2X_Shift			"Review_CalLens2X_Shift"
#define  Key_Review_CalLens2X_Drive			"Review_CalLens2X_Drive"
#define  Key_Review_CalLens10X_ReflectLight	"Review_CalLens10X_ReflectLight"
#define  Key_Review_CalLens10X_Shift		"Review_CalLens10X_Shift"
#define  Key_Review_CalLens10X_Drive		"Review_CalLens10X_Drive"
#define  Key_Review_CalLens20X_ReflectLight	"Review_CalLens20X_ReflectLight"
#define  Key_Review_CalLens20X_Shift		"Review_CalLens20X_Shift"
#define  Key_Review_CalLens20X_Drive		"Review_CalLens20X_Drive"



//기준 Camera 대비 거리를 나타 낸다.
#define  Section_ScanScopeCamDis		"Section_ScanScopeCamDis"
#define  Key_ReviewToScopeDisOffsetShift				"ReviewToScopeDisOffsetShift"
#define  Key_ReviewToScopeDisOffsetDrive				"ReviewToScopeDisOffsetDrive"

//기준 Camera 대비 거리를 나타 낸다.
#define  Section_ReviewCamToObjDis		"ReviewCamToObjDis"
	//Review Camera 대비 거리를 나타 낸다.
	#define  Key_LineScanDisX					"LineScanDisX"
	#define  Key_LineScanDisY					"LineScanDisY"

	#define  Key_SpaceSensorDisX			"SpaceSensorDisX"
	#define  Key_SpaceSensorDisY			"SpaceSensorDisY"

//기준 Camera 대비 거리를 나타 낸다.
#define  Section_ScopeCamToObjDis		"ScopeCamToObjDis"
	//Scope Camera 대비 거리를 나타 낸다.
	#define  Key_AirBlowDisX							"AirBlowDisX"
	#define  Key_AirBlowDisY							"AirBlowDisY"

	#define  Key_BackLightDisX						"BackLightDisX"
	#define  Key_BackLightDisY						"BackLightDisY"

	#define  Key_ScanSensorDisX						"ScanSensorDisX"
	#define  Key_ScanSensorDisY						"ScanSensorDisY"

	#define  Key_ThetaAlignCamDisX					"ThetaAlignCamDisX"
	#define  Key_ThetaAlignCamDisY					"ThetaAlignCamDisY"

#define  Section_ReviewLens		"ReviewLens"
#define  Section_ScopeLens		"ScopeLens"
#define  Key_Lens02x_PixelSizeX		"Lens02x_PixelSizeX"
#define  Key_Lens02x_PixelSizeY		"Lens02x_PixelSizeY"
#define  Key_Lens02x_OffsetX		"Lens02x_OffsetX"
#define  Key_Lens02x_OffsetY		"Lens02x_OffsetY"
#define  Key_Lens02x_Light			"Lens02x_Light"

#define  Key_Lens10x_PixelSizeX		"Lens10x_PixelSizeX"
#define  Key_Lens10x_PixelSizeY		"Lens10x_PixelSizeY"
#define  Key_Lens10x_OffsetX		"Lens10x_OffsetX"
#define  Key_Lens10x_OffsetY		"Lens10x_OffsetY"
#define  Key_Lens10x_Light			"Lens10x_Light"

#define  Key_Lens20x_PixelSizeX		"Lens20x_PixelSizeX"
#define  Key_Lens20x_PixelSizeY		"Lens20x_PixelSizeY"
#define  Key_Lens20x_OffsetX		"Lens20x_OffsetX"
#define  Key_Lens20x_OffsetY		"Lens20x_OffsetY"
#define  Key_Lens20x_Light			"Lens20x_Light"

#define  Key_Lens50x_PixelSizeX		"Lens50x_PixelSizeX"
#define  Key_Lens50x_PixelSizeY		"Lens50x_PixelSizeY"
#define  Key_Lens50x_OffsetX		"Lens50x_OffsetX"
#define  Key_Lens50x_OffsetY		"Lens50x_OffsetY"
#define  Key_Lens50x_Light			"Lens50x_Light"

#define  Key_Lens150x_PixelSizeX	"Lens150x_PixelSizeX"
#define  Key_Lens150x_PixelSizeY	"Lens150x_PixelSizeY"
#define  Key_Lens150x_OffsetX		"Lens150x_OffsetX"
#define  Key_Lens150x_OffsetY		"Lens150x_OffsetY"
#define  Key_Lens150x_Light			"Lens150x_Light"

//////////////////////////////////////////////////////////////////////////
#define  Section_CELL_INFO "CELL_INFO"

#define  Key_Scan_CellAlign_Pos_Shift	"CellAlign_Pos_Shift"
#define  Key_Scan_CellAlign_Pos_Drive	"CellAlign_Pos_Drive"
#define  Key_Scan_Cell_Tension_Value "Cell_Tension_Value"

#define  Key_Stick_Width "Stick_Width"
#define  Key_Stick_Edge_Distance_W "Stick_Edge_Dis_W"
#define  Key_Stick_Edge_Distance_H "Stick_Edge_Dis_H"
#define  Key_Cell_Width "Cell_Width"
#define  Key_Cell_Height "Cell_Height"
#define  Key_Cell_Horizontal_Count "Cell_Horizontal_Count"
#define  Key_Cell_Vertical_Count "Cell_Vertical_Count "
#define  Key_Cell_Next_Horizontal_Distance "Cell_Next_Horizontal_Distance"
#define  Key_Cell_Next_Vertical_Distance "Cell_Next_Vertical_Distance" 

#define  Section_SCAN_3D  Section_CELL_INFO//"SCAN_3D"
#define  Key_Trigger_Step				"Trigger_Step"
//#define  Key_Scan_Count				"Scan_Count"
//#define  Key_Scan_Distance			"Scan_Distance"
#define  Key_Pass_Min_Height		"Pass_Min_Height"
#define  Key_Pass_Max_Height		"Pass_Max_Height"

#define  Section_MICRO_SCAN  Section_CELL_INFO//"MICRO_SCAN"
#define  Key_MIC_Scale_Width			"MIC_Scale_Width"
#define  Key_MIC_Scale_Height			"MIC_Scale_Height"
#define  Key_MIC_Detect_Min			"MIC_Detect_Min"
#define  Key_MIC_Detect_Max			"MIC_Detect_Max"
#define  Key_MIC_Skip_Out_Line		"MIC_Skip_Out_Line"
#define  Key_MIC_Size_Filter_Minimum_Width "MIC_Size_Filter_Minimum_Width" 
#define  Key_MIC_Size_Filter_Minimum_Height "MIC_Size_Filter_Minimum_Height" 
#define  Key_MIC_Size_Filter_Maximum_Width "MIC_Size_Filter_Maximum_Width" 
#define  Key_MIC_Size_Filter_Maximum_Height "MIC_Size_Filter_Maximum_Height" 
#define  Key_MIC_Defect_Distance	"MIC_Defect_Distance" 
#define  Key_MIC_Filter_Mask_Type	"MIC_Filter_Mask_Type" 
#define  Key_MIC_Light_Back				"MIC_Light_Back" 
#define  Key_MIC_Light_Ring				"MIC_Light_Ring" 

#define  Section_MACRO_SCAN   Section_CELL_INFO //"MACRO_SCAN"
#define  Key_MAC_Scale_Width			"MAC_Scale_Width"
#define  Key_MAC_Scale_Height			"MAC_Scale_Height"
#define  Key_MAC_Detect_Min			"MAC_Detect_Min"
#define  Key_MAC_Detect_Max			"MAC_Detect_Max"
#define  Key_MAC_Skip_Out_Line		"MAC_Skip_Out_Line"
#define  Key_MAC_Size_Filter_Minimum_Width "MAC_Size_Filter_Minimum_Width" 
#define  Key_MAC_Size_Filter_Minimum_Height "MAC_Size_Filter_Minimum_Height" 
#define  Key_MAC_Size_Filter_Maximum_Width "MAC_Size_Filter_Maximum_Width" 
#define  Key_MAC_Size_Filter_Maximum_Height "MAC_Size_Filter_Maximum_Height" 
#define  Key_MAC_Defect_Distance	"MAC_Defect_Distance" 
#define  Key_MAC_Filter_Mask_Type	"MAC_Filter_Mask_Type" 
#define  Key_MAC_Light_Upper				"MAC_Light_Upper" 
#define  Key_MAC_Light_Ring				"MAC_Light_Ring" 

#define  Section_SCOPE_3D  Section_CELL_INFO//"SCOPE_3D"
#define  Key_Stick_Type		"Stick_Type"
#define  Key_Stick_Thick		"Stick_Thick"
#define  Key_Measure_Angle	"Measure_Angle"
#define  Key_Measure_Area	"Measure_Area"
#define  Key_Space_Distance 	"Space_Distance"

#define  Key_Measure_Lens_Index 	"Measure_Lens_Index"

#define  Key_Measure_Point_Type	"Measure_Point_Type"
//#define  Key_Measure_Cell_Type 	"Measure_Cell_Type"
#define  Key_Measure_Cell_List 	"Measure_Cell_List"



#define  Section_CD_TP  Section_CELL_INFO//"CD_TP"
#define  Key_Measure_Lens	"Measure_Lens"
#define  Key_Edge_Pos_Count_X		"Edge_Pos_Count_X"
#define  Key_Edge_Pos_Count_Y		"Edge_Pos_Count_Y"
#define  Key_Light_Upper			"Light_Upper" 
#define  Key_Light_Back				"Light_Back" 
#define  Key_CD_Light_Threshold		"CD_Light_Threshold"
#define  Key_CD_Measure_Direction	"CD_Measure_Direction"

#define  Key_TP_Mark_Distance_X		"TP_Mark_Distance_X"
#define  Key_TP_Mark_Distance_Y		"TP_Mark_Distance_Y"



enum SCOPE_LENS_INDEX
{
	SCOPE_LENS_NONE =0,
	SCOPE_LENS_10X =1,
	SCOPE_LENS_20X,
	SCOPE_LENS_50X,
	SCOPE_LENS_150X,
};

enum REVIEW_LENS_INDEX
{
	REVIEW_LENS_NONE =0,
	REVIEW_LENS_2X =1,
	REVIEW_LENS_10X,
	REVIEW_LENS_20X,
};


typedef struct StickInfoByIndexer_Tag
{
	char strBoxID[64];
	char strStickID[64];
	int  nStickLoadOffset;

	StickInfoByIndexer_Tag()
	{
		memset(this, 0x00, sizeof(StickInfoByIndexer_Tag));
	}
}StickInfoByIndexer;	

typedef struct GripperPosition_Tag
{
	int nGripper_ZPos_TT01Out;
	int nGripper_ZPos_Grip_Standby;
	int nGripper_ZPos_Grip;//오른쪽 11000Cts(1.1mm차이가 있다) 보정필요.
	int nGripper_ZPos_Measure;
	int nGripper_ZPos_Standby;

	int nGripper_TPos_Standby;
	int nGripper_TPos_Grip;
	int nGripper_TPos_TensionStandby;

	//Stick 지그를 추가 한다.
	int nStickJig_Standby;
	int nStickJig_Measure;
	GripperPosition_Tag()
	{
		memset(this, 0x00, sizeof(GripperPosition_Tag));
	}
}GripperPosition;	


typedef struct ReviewPosition_Tag
{
	int nScan_ZPos_Standby;
	int nScan_ZPos_Review	;
	int nScan_ZPos_Inspection;

	int nScan_Standby_Shift;
	int nScan_Standby_Drive;


	int nCellAlign_LensIndex;
	int nCellAlign_Backlight;


	//int nCDTP_LensIndex;
	int nAutoReviewMic_LensIdex;
	int nAutoReviewMic_Light;
	int nAutoReviewMic_Cnt;
	int nAutoReviewMic_Backlight;

	int nAutoReviewMac_LensIdex;
	int nAutoReviewMac_Light;
	int nAutoReviewMac_Cnt;
	int nAutoReviewMac_Backlight;

	int nInspection_MicBackLight;
	int nInspection_MicRingLight;

	int nInspection_MacReflectLight;
	int nInspection_MacRingLight;


	//Review Calc Data
	int nCalLens_ZUpDnPos;
	int nCalLens2X_ReflectLight;
	int nCalLens2X_Shift;
	int nCalLens2X_Drive;

	int nCalLens10X_ReflectLight;
	int nCalLens10X_Shift;
	int nCalLens10X_Drive;

	int nCalLens20X_ReflectLight;
	int nCalLens20X_Shift;
	int nCalLens20X_Drive;
	ReviewPosition_Tag()
	{
		memset(this, 0x00, sizeof(ReviewPosition_Tag));
	}
}ReviewPosition;	

typedef struct Scope3DPosition_Tag
{
	int nScope_ZPos_Standby;
	int nScope_ZPos_Measure	;
	//int nScope_ZPos_ThetaAlign;

	//int nScope_ZPos_Area3DMeasure;

		
	int nSpace_ZPos_Standby;
	int nSpace_ZPos_Measure;

	int nScope_Standby_Shift;
	int nScope_Standby_Drive;

	//int nThetaAlignLensIndex;
	int nThetaAlignPos1_Drive;
	int nThetaAlignPos1_Shift;
	int nThetaAlignPos2_Drive;
	int nThetaAlignPos2_Shift;

	int nThetaAlignLightVale;
	int nThetaAlignPos2Offset;
	//int nThetaAlignPosAFDis_Shift;
	Scope3DPosition_Tag()
	{
		memset(this, 0x00, sizeof(Scope3DPosition_Tag));
	}
}Scope3DPosition;	

typedef struct AFModuleData_Tag
{
	int		 nMiv;
	BOOL     bIn_focus;		
	BOOL     bLaser_enabled;		
	int		 nPosition;  
	int      nFocusOldPos;
	int      nFocusNowPos;

	unsigned short  nNowPwm;

	float nPos;
	int nErrorCode;
	AFModuleData_Tag()
	{
		memset(this, 0x00, sizeof(AFModuleData_Tag));
	}

}AFModuleData;	
typedef struct SCOPE_LENS_OFFSET_TAG
{
	float Lens10xPixelSizeX;
	float Lens10xPixelSizeY;
	int Lens10xOffsetX;
	int Lens10xOffsetY;

	float Lens20xPixelSizeX;
	float Lens20xPixelSizeY;
	int Lens20xOffsetX;
	int Lens20xOffsetY;

	float Lens50xPixelSizeX;
	float Lens50xPixelSizeY;
	int Lens50xOffsetX;
	int Lens50xOffsetY;

	float Lens150xPixelSizeX;
	float Lens150xPixelSizeY;
	int Lens150xOffsetX;
	int Lens150xOffsetY;
}SCOPE_LENS_OFFSET;

typedef struct REVIEW_LENS_OFFSET_TAG
{
	float Lens02xPixelSizeX;
	float Lens02xPixelSizeY;
	int Lens02xOffsetX;
	int Lens02xOffsetY;
	USHORT Lens02xLight;

	float Lens10xPixelSizeX;
	float Lens10xPixelSizeY;
	int Lens10xOffsetX;
	int Lens10xOffsetY;
	USHORT Lens10xLight;

	float Lens20xPixelSizeX;
	float Lens20xPixelSizeY;
	int Lens20xOffsetX;
	int Lens20xOffsetY;
	USHORT Lens20xLight;
}REVIEW_LENS_OFFSET;

#define  CDTP_CELL_EDGE_OFFSET 60//um단위
enum
{
	CELL_None = 0,
	CELL_LeftTop = 1,
	CELL_RightTop,
	CELL_RightBottom, 
	CELL_LeftBottom, 
	CELL_Center,
};


typedef struct SCOPE_3D_TAG
{
	int nStick_Type;
	float fStick_Thick;
	int nMeasure_Angle;
	int nMeasure_Area;
	float fSpaceTargetValue;

	int nMeasurPointType;
	//int nMeasurCelType;

	int nMeasurCellCnt;
	int nMeasurCellList[50];

	int nMeasureLensIndex;
}SCOPE_3DPara;

#define  THETA_MOT_INPOS_PULSE_CNT 5

//1회전에 1mm ==> 0.06도 회전.
//1회전 10000pulse
#define  THETA_MOT_PULS_PER_ANGLE (0.06/10000.0)
#define  THETA_ALIGN_ROTATE_CENTER_OFFSET 135000 //um

#define  TENSION_CLEE_CNT			4
#define  TENSION_CLEE_WIDTH		100000//100mm
#define  SCANER_SENSOR_WIDTH	((50000*2)-1000)//50mmSensor 2ea(두센서의 겸침 1mm) 
#define  SCANER_SENSOR_SHIFT	(SCANER_SENSOR_WIDTH - 1000)//Scan 회수에 대한 겹침 
typedef struct SCAN_3D_TAG
{
	char strFileName[64];
	int nScan_Count;
	int nScan_Distance;
	float fTrigger_Step;
	int nPass_Min_Height;
	int nPass_Max_Height;
}SCAN_3DPara;
typedef struct SCAN_3DResult_TAG
{
	double fAvr;
	double fMin;
	double fMax;
}SCAN_3DResult;
typedef struct CD_TP_TAG
{
	int nMeasure_Lens;
	int nEdge_Pos_Count_X;
	int nEdge_Pos_Count_Y;
	int nLight_Upper;
	int nLight_Back;
	int nLight_Threshold;
	int nMeasure_Direction; //1:'+'수직 2:'X'대각
	int nTP_Mark_Distance_X;
	int nTP_Mark_Distance_Y;
}CD_TPParam;

typedef struct ObjDistance_TAG
{
	//int nReviewCamPosX;
	//int nReviewCamPosY;
	int nReviewCamTo_LineScanX;
	int nReviewCamTo_LineScanY;
	int nReviewCamTo_SpaceSensorDisX;
	int nReviewCamTo_SpaceSensorDisY;

	int nReviewToScopeDisOffsetShift;
	int nReviewToScopeDisOffsetDrive;

	int nScopeCamTo_AirBlowDisX;
	int nScopeCamTo_AirBlowDisY;

	int nScopeCamTo_BackLightDisX;
	int nScopeCamTo_BackLightDisY;
	int nScopeCamTo_ScanSensorDisX;
	int nScopeCamTo_ScanSensorDisY;

	int nScopeCamTo_ThetaAlignCamDisX;
	int nScopeCamTo_ThetaAlignCamDisY;
//////////////////////////////////////////////////////////////////////////
	//Draw용.mm단위.
	float fReviewCamPosX;
	float fReviewCamPosY;
	float fReviewCamTo_LineScanX;
	float fReviewCamTo_LineScanY;

	CRect ScanCamArea;

	float fReviewCamTo_SpaceSensorDisX;
	float fReviewCamTo_SpaceSensorDisY;
	CRect BackLightArea;

	float fScopeCamPosX;
	float fScopeCamPosY;

	float fScopeCamTo_AirBlowDisX;
	float fScopeCamTo_AirBlowDisY;
	float fScopeCamTo_BackLightDisX;
	float fScopeCamTo_BackLightDisY;
	float fScopeCamTo_ScanSensorDisX;
	float fScopeCamTo_ScanSensorDisY;

	float fScopeCamTo_ThetaAlignCamDisX;
	float fScopeCamTo_ThetaAlignCamDisY;
	CRect ScanSensortArea;

	ObjDistance_TAG()
	{
		//nReviewCamPosX = 0;
		//nReviewCamPosY = 0;
		nReviewCamTo_LineScanX = 0;
		nReviewCamTo_LineScanY = 0;

		ScanCamArea.left				= (int)(((12000.0f*5.25f)/2.0f)*(-1.0f));
		ScanCamArea.top				= (int)(((6000.0f*5.25f)/2.0f)*(-1.0f));
		ScanCamArea.right			= ScanCamArea.left	+ (int)(12000.0f*5.25f);//12k TDI Type.
		ScanCamArea.bottom		= ScanCamArea.top	+ (int)(6000.0f*5.25f);//6000Line Scan.

		BackLightArea.left				= (int)(((100000.0f)/2.0f)*(-1.0f));
		BackLightArea.top				= (int)(((30000.0f)/2.0f)*(-1.0f));
		BackLightArea.right			= BackLightArea.left	+ (int)(100000.0f);//100000um.
		BackLightArea.bottom		= BackLightArea.top	+ (int)(30000.0f);//30000um

		nReviewCamTo_SpaceSensorDisX = 0;
		nReviewCamTo_SpaceSensorDisY = 0;

		//nScopeCamPosX = 0;
		//nScopeCamPosY = 0;
		nScopeCamTo_AirBlowDisX = 0;
		nScopeCamTo_AirBlowDisY = 0;
		nScopeCamTo_BackLightDisX = 0;
		nScopeCamTo_BackLightDisY = 0;
		nScopeCamTo_ScanSensorDisX = 0;
		nScopeCamTo_ScanSensorDisY = 0;

		nScopeCamTo_ThetaAlignCamDisX = 0;
		nScopeCamTo_ThetaAlignCamDisY = 0;

		ScanSensortArea.left				= (int)(((120000.0f)/2.0f)*(-1.0f));
		ScanSensortArea.top				= (int)(((5.0f)/2.0f)*(-1.0f));
		ScanSensortArea.right			= ScanSensortArea.left	+ (int)(120000.0f);//120000um.
		ScanSensortArea.bottom		= ScanSensortArea.top	+ (int)(5.0f);//5um

		//Draw용.mm단위.
		fReviewCamPosX = 0.0f;
		fReviewCamPosY = 0.0f;
		fReviewCamTo_LineScanX = 0.0f;
		fReviewCamTo_LineScanY = 0.0f;
		fReviewCamTo_SpaceSensorDisX = 0.0f;
		fReviewCamTo_SpaceSensorDisY = 0.0f;

		fScopeCamPosX = 0.0f;
		fScopeCamPosY = 0.0f;
		fScopeCamTo_AirBlowDisX = 0.0f;
		fScopeCamTo_AirBlowDisY = 0.0f;
		fScopeCamTo_BackLightDisX = 0.0f;
		fScopeCamTo_BackLightDisY = 0.0f;
		fScopeCamTo_ScanSensorDisX = 0.0f;
		fScopeCamTo_ScanSensorDisY = 0.0f;

		fScopeCamTo_ThetaAlignCamDisX = 0.0f;
		fScopeCamTo_ThetaAlignCamDisY = 0.0f;
	}

}ObjDistance;

struct FDC_Data
{          
	int INS_HDD_C_CAPA;
	int INS_HDD_D_CAPA;
	int INS_CPU_SHARE;
	int INS_MEMORY;
	FDC_Data()
	{
		memset(this, 0x00, sizeof(FDC_Data));
	}
};

struct SEND_STARTMEASURE_DATA {
	int nMaskType;
	int nMeasureAngle;
	float fTargetThick;
	float fSpaceGap;
	BOOL bMeasureAll;
};

typedef struct Result3DScopeDataFormat_ATG
{//기본 거리 단위nm Nano
	int P_Z_UPPER_L;
	int P_Z_LOWER_L;
	int P_PITCH;
	int P_Z_MEA_DIS;

	int P_ND_FILTER;
	int P_LASER_BR;

	int RIB_WIDTH;
	int RIB_WIDTH_C;
	int RIB_WIDTH_L;
	int RIB_WIDTH_R;
	int RIB_HEIGHT_L;
	int RIB_HEIGHT_E_L;
	int RIB_HEIGHT_R;
	int RIB_HEIGHT_E_R;
	float RIB_ANGLE_L;
	float RIB_ANGLE_R;
	int RIB_HEIGHT;

	int RIB_WIDTH2;
	int RIB_WIDTH_C2;
	int RIB_WIDTH_L2;
	int RIB_WIDTH_R2;
	int RIB_HEIGHT_L2;
	int RIB_HEIGHT_E_L2;
	int RIB_HEIGHT_R2;
	int RIB_HEIGHT_E_R2;
	float RIB_ANGLE_L2;
	float RIB_ANGLE_R2;
	int RIB_HEIGHT2;

}RESULT3DScope_DATA;

typedef struct SCOPE_3DPoint_TAG
{
	int nPosType;//Cell에대해서 1:좌상, 2: 우상, 3: 우하, 4: 좌하, 5:중앙.
	int nPointX;
	int nPointY;
	
	int nScopeAxisPointX;
	int nScopeAxisPointY;

	//SCOPE_3DPoint 에서는 AF Position이 미리 계산되어 들어간다.
	int nAFPointX;
	int nAFPointY;

	int nScopeAxisAFPointX;
	int nScopeAxisAFPointY;

//	RESULT3DScope_DATA sScopeRet;
	int nScopeRetCount;
	RESULT3DScope_DATA *pScopeRet;

	SCOPE_3DPoint_TAG()
	{
		memset(this, 0x00, sizeof(SCOPE_3DPoint_TAG));
	}
	~SCOPE_3DPoint_TAG()
	{
		if(pScopeRet) {
			delete [] pScopeRet;
		}
	}
} SCOPE_3DPoint;

typedef struct TP_Result_TAG
{
	float	fCDTP_Ret_TP;

	int  nCell_TPCnt;
	float	*pfCDTP_Ret_CELL_TP;
	TP_Result_TAG()
	{
		fCDTP_Ret_TP = 0;
		nCell_TPCnt = 0;
		pfCDTP_Ret_CELL_TP = nullptr;
	}
	~TP_Result_TAG()
	{
		if(pfCDTP_Ret_CELL_TP!=nullptr)
			delete [] pfCDTP_Ret_CELL_TP;
		pfCDTP_Ret_CELL_TP = nullptr;
	}
	void CreateCellTPBuf(int CellCnt)
	{
		if(pfCDTP_Ret_CELL_TP!=nullptr)
			delete [] pfCDTP_Ret_CELL_TP;
		pfCDTP_Ret_CELL_TP = nullptr;
		nCell_TPCnt = 0;
		if(CellCnt>0)
		{
			nCell_TPCnt = CellCnt;
			pfCDTP_Ret_CELL_TP = new float[nCell_TPCnt];
			memset(pfCDTP_Ret_CELL_TP, 0x00, sizeof(float)*nCell_TPCnt);
		}
	}
	void ResetCellTPBuf()
	{
		if(pfCDTP_Ret_CELL_TP!=nullptr)
			delete [] pfCDTP_Ret_CELL_TP;
		pfCDTP_Ret_CELL_TP = nullptr;
		nCell_TPCnt = 0;
	}
} TP_Result;



typedef struct CDTP_Point_TAG
{
	int nPosType;//Cell에대해서 1:좌상, 2: 우상, 3: 우하, 4: 좌하, 5:중앙.
	int nPointX;
	int nPointY;

	//nPost Type의 최 외곽 좌표를 기록 한다.
	int nCellEdgePointX;
	int nCellEdgePointY;


	int nTP_GrabShiftPos;
	int nTP_GrabDrivePos;
	//nPost Type의 최 외곽 좌표를 기록 한다.
	int nTP_MeasurePointX;
	int nTP_MeasurePointY;

	//측정 홀을 중앙 위치.(M좌표)
	int nMCenterPointX;
	int nMCenterPointY;

	//Result Write 
	float fSlitSizeW;
	float fSlitSizeH;
	float fRibSizeW;
	float fRibSizeH;

//	TP_Result fTP_Result;

	CDTP_Point_TAG()
	{
		memset(this,0x00, sizeof(CDTP_Point_TAG));
	}
} CDTP_Point;



#define SVID_LIST_PATH "E:\\MasterShared\\[04_Master]\\SVID_List.ini"
#define  Section_SVID		"SVID"
#define  Key_STICK_TENSION_GRIP	"STICK_TENSION_GRIP"
#define  Key_STICK_TENSION_FINE "STICK_TENSION_FINE"
#define  Key_STICK_INSPEC_CAM_GRAY_MAX "STICK_INSPEC_CAM_GARY_MAX"
#define  Key_STICK_INSPEC_CAM_GRAY_MIN "STICK_INSPEC_CAM_GARY_MIN"
#define  Key_STICK_INSPEC_CAM_VOLTAGE  "STICK_INSPEC_CAM_VOLTAGE"
#define  Key_STICK_INSPEC_CAM_TEMPERATURE "STICK_INSPEC_CAM_TEMPERATURE"
#define  Key_STICK_INSPEC_DARKFIELD_VALUE "STICK_INSPEC_DARKFIELD_VALUE"
#define  Key_STICK_INSPEC_BACKLIGHT_VALUE "STICK_INSPEC_BACKLIGHT_VALUE"
#define  Key_STICK_INSPEC_MACRO_LIGHT_VALUE "STICK_INSPEC_MACRO_LIGHT_VALUE"
#define  Key_STICK_REVIEW_IMG_CONTRAST "STICK_REVIEW_IMG_CONTRAST"
#define  Key_STICK_REVIEWAF_TIME "STICK_REVIEWAF_TIME"
#define  Key_MOT_AXIS_MEASURE_X1_OVERLOAD "MOT_AXIS_MEASURE_X1_OVERLOAD"
#define  Key_MOT_AXIS_MEASURE_X2_OVERLOAD "MOT_AXIS_MEASURE_X2_OVERLOAD"
#define  Key_MOT_AXIS_MEASURE_Y_OVERLOAD "MOT_AXIS_MEASURE_Y_OVERLOAD"
#define  Key_MOT_AXIS_INSPECT_X_OVERLOAD "MOT_AXIS_INSPECT_X_OVERLOAD"
#define  Key_MOT_AXIS_INSPECT_Y_OVERLOAD "MOT_AXIS_INSPECT_Y_OVERLOAD"
#define  Key_TEMPERATURE_STATE_PC_BOX "TEMPERATURE_STATE_PC_BOX"
#define  Key_TEMPERATURE_STATE_ELECTRICAL_BOX "TEMPERATURE_STATE_ELECTRICAL_BOX"
#define  Key_PNEUMATIC_AIR "PNEUMATIC_AIR"
#define  Key_PNEUMATIC_N2 "PNEUMATIC_N2"