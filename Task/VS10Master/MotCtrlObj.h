#pragma once

enum MOTION_STATUS
{
	//Command Start
	AM01_SCAN_DRIVE = 0,
	AM01_SCAN_SHIFT,
	AM01_SCAN_UPDN,
	AM01_SCAN_MULTI_MOVE_XY,
	AM01_SPACE_UPDN,
	AM01_SPACE_MEASURE,

	AM01_SCOPE_DRIVE,
	AM01_SCOPE_SHIFT,
	AM01_SCOPE_UPDN,	
	AM01_SCOPE_MULTI_MOVE_XY,
	AM01_SCOPE_LENS_CHNAGE,
	AM01_SCAN_SCOPE_SYNC_MOVE_XY,
	AM01_TENTION_LEFT_MULTI,
	AM01_TENTION_RIGHT_MULTI,	
	AM01_GRIPPER_UPDN,	
	AM01_TENTION_START,	

	AM01_REVIEW_LENS_OFFSET,
	AM01_SCOPE_LENS_OFFSET,

	TT01_THETA_ALIGN_MOVE,
	//Command End
	AM01_TENTION_FWD,
	AM01_TENTION_BWD,
	AM01_GRIPPER_OPEN,
	AM01_GRIPPER_CLOSE,

	AM01_DOONTUK_LASER_ON,
	AM01_DOONTUK_LASER_OFF,

	AM01_RING_LIGHT_UP,
	AM01_RING_LIGHT_DOWN,

	AM01_REVIEW_UP,
	AM01_REVIEW_DOWN,

	AM01_JIG_INOUT,
	
	AM01_DOOR_FRONT_RELEASE,
	AM01_DOOR_FRONT_LOCK,
	AM01_DOOR_SIDE_RELEASE,
	AM01_DOOR_SIDE_LOCK,
	AM01_DOOR_BACK_RELEASE,
	AM01_DOOR_BACK_LOCK,

	AM01_SCOPE_SPACE_MULTI_MOVE,

	AM01_TENTION_ZERO,
	AM01_QR_READ,
	AM01_AIR_READ,

	AM01_LIGHT,

	//Status Start
	ALL_STATUS,
	//Status End
	MOTION_COUNT
};

enum TIME_OUT_LIST
{	
	BT01_UPDN_TIMEOUT = 10,
	TM01_DRIVE_TIMEOUT = 10,
	NORMAL_TIMEOUT = 30

};

typedef enum tag_MOTION_STATUS
{
	IDLE = 0,
	RUNNING
}EL_MOTION_STATUS;




/**
@struct
@brief
@remark
-
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:49
*/
typedef struct tag_Motion_Status
{
	EL_MOTION_STATUS	 B_MOTCMD_State;
	UINT	UN_TIME_OUT_VAL;
	VS24MotData ActionResult;
	tag_Motion_Status()
	{
		memset(this, 0x00, sizeof(tag_Motion_Status));
	}
}STMOTION_STATUS;

static const int	RETRY_COUNT = 1;

typedef enum SyncScan_Tag
{
	Sync_Scan_Review = 1,
	Sync_Scan_LineScan = 2,
	Sync_Scan_SpaceSensor,
}SyncScan;
typedef enum SyncScope_Tag
{
	Sync_Scope_3DScope = 1,
	Sync_Scope_AirBlow = 2,
	Sync_Scope_BackLight,
	Sync_Scope_ScanSensor,
	Sync_Scope_ThetaAlignCam,
}SyncScope;

typedef enum ScanLight_Tag
{
	Light_Ring = 0x01,
	Light_Back = 0x02,
	Light_Reflect= 0x04,
}ScanLight;

class CMotCtrlObj
{
public:
	CMotCtrlObj(void);
	~CMotCtrlObj(void);
public:
	CInterServerInterface  *m_pServerInterface;
	STMOTION_STATUS m_MotCMD_STATUS[MOTION_COUNT];

	enum
	{
		TT01_AM01_Unknown = 0,
		TT01_AM01_Pos =1,
		TT01_Turn_Pos =2,
		TT01_UT02_Pos,
	}m_TT01_NowPos;
public:


	void m_fnResetAllCMDRun();
	void m_fnSetSignalTower(STWOER_MODE NewSettingMode);
	void m_fnSetSignalTower_BuzOn();
	void m_fnSetSignalTower_StopBuz();
	//////////////////////////////////////////////////////////////////////////
	//VS24MotData m_stAM01_Mess;
	void m_fnAM01MotAllStop();

	void m_fnScanDriveMove(int nPosValue);
	int m_fn_Scan_Drive_Return(VS24MotData stReturn_Msg);
	void m_fnScanShiftMove(int nPosValue);
	int m_fn_ScanShift_Return(VS24MotData stReturn_Msg);
	void m_fnScanUpDnMove(int nPosValue);
	int m_fnScanUpDn_Return(VS24MotData stReturn_Msg);
	void m_fnSpaceSensorMove(int nPosValue);
	int m_fnSpaceSensorUpDn_Return(VS24MotData stReturn_Msg);
	void m_fnAM01MultiMoveLTension(int nSelect, int nPosValue,  int nTimeOut=0); 
	int m_fnAM01TensionMultiMoveL_Return(VS24MotData stReturn_Msg);
	void m_fnAM01MultiMoveRTension(int nSelect, int nPosValue,  int nTimeOut=0); 
	int m_fnAM01TensionMultiMoveR_Return(VS24MotData stReturn_Msg);



	void m_fnScanMultiMove(int nPosValuesShift, int nPosValuesDrive);				
	int m_fnScanMultiMove_Return(VS24MotData stReturn_Msg);

	void m_fnAM01GripperUpDown(int nPosValue);
	int m_fnAM01GripperUpDown_Return(VS24MotData stReturn_Msg);

	void m_fnAM01GripperTensionStart(bool OnOff, int AxisSelect, float fTargetValue);				
	int m_fnAM01GripperTensionStart_Return(VS24MotData stReturn_Msg);


	void m_fnScanSetLensOffset(int nReviewLensIndex);
	int m_fnScanSetLensOffset_Return(VS24MotData stReturn_Msg);
	void m_fnScopeSetLensOffset(int nScopeLensIndex);
	int m_fnScopeSetLensOffset_Return(VS24MotData stReturn_Msg);

	//Scofe
	void m_fnScopeDriveMove(int nPosValue);				
	int m_fn_Scope_Drive_Return(VS24MotData stReturn_Msg);
	void m_fnScopeShiftMove(int nPosValue);					
	int m_fn_Scope_Shift_Return(VS24MotData stReturn_Msg);
	void m_fnScopeUpDnMove(int nPosValue);				
	int m_fnScopeUpDn_Return(VS24MotData stReturn_Msg);

	void m_fnScopeMultiMove(int nPosValuesShift, int nPosValuesDrive);				
	int m_fnScopeMultiMove_Return(VS24MotData stReturn_Msg);
	

	void m_fnAM01PositionMove(int nPosValue, int nMotionType, int nTimeOut=0);
	void m_fnAM01MultiPositionMove(int nPosValueShift, int nPosValueDrive, int nMotionType, int nTimeOut = 0);
	void m_fnAM01ActionStop(int nMotionType, int nTimeOut=0);


	void m_fnAM01MultiScopeSpaceZ(int nPosScope, int nPosSpace, int nTimeOut = NORMAL_TIMEOUT);

	void m_fnScopeSpaceZSyncMove(int nPosScope, int nPosSpace, int nTimeOut = NORMAL_TIMEOUT);
	int m_fnScopeSpaceZSyncMove_Return(VS24MotData stReturn_Msg);


	void m_fnAM01ScanScopeSyncMove(int nPosScanShift, int nPosScanDrive, int nPosScopeShift, int nPosScopeDrive, int nTimeOut = 0);


	void m_fnAM01ReviewSyncMove(int nPosValueShift, int nPosValueDrive, SyncScope ScopeSyncModule, int nTimeOut = NORMAL_TIMEOUT);
	void m_fnAM01LineScanSyncMove(int nPosValueShift, int nPosValueDrive, int nTimeOut = NORMAL_TIMEOUT);
	void m_fnAM01ScopeSyncMove(int nPosValueShift, int nPosValueDrive, SyncScan ScanSyncModule, int nTimeOut = NORMAL_TIMEOUT);
	int m_fnAM01ScanScopeSyncMove_Return(VS24MotData stReturn_Msg);


	void m_fnStickTentionFWD();
	int m_fnStickTentionFWD_Return(VS24MotData stReturn_Msg);
	void m_fnStickTentionBWD();
	int m_fnStickTentionBWD_Return(VS24MotData stReturn_Msg);

	void m_fnStickGripperOpen();
	int m_fnStickGripperOpen_Return(VS24MotData stReturn_Msg);
	void m_fnStickGripperClose();
	int m_fnStickGripperClose_Return(VS24MotData stReturn_Msg);
	
	///2015,04,14 정의천 추가
	void m_fnDoonTukLaserOn();
	int m_fnDoonTukLaserOn_Return(VS24MotData stReturn_Msg);
	void m_fnDoonTukLaserOff();
	int m_fnDoonTukLaserOff_Return(VS24MotData stReturn_Msg);
	///2015,04,14 정의천 추가

	void m_fnRingLightUp();
	int m_fnRingLightUp_Return(VS24MotData stReturn_Msg);
	void m_fnRingLightDown();
	int m_fnRingLightDown_Return(VS24MotData stReturn_Msg);

	void m_fnReviewUp();
	int m_fnReviewUp_Return(VS24MotData stReturn_Msg);
	void m_fnReviewDown();
	int m_fnReviewDown_Return(VS24MotData stReturn_Msg);


	bool m_fnStickJigMove(int nPosValue);
	int m_fnStickJigMove_Return(VS24MotData stReturn_Msg);

	void m_fnStickTensionZeroSet();
	int m_fnStickTensionZeroSet_Return(VS24MotData stReturn_Msg);

	void m_fnQRCodeRead();
	int m_fnQRCodeRead_Return(VS24MotData stReturn_Msg);

	void m_fnAIRRead();
	int m_fnAIRRead_Return(VS24MotData stReturn_Msg);

	void m_fnBuzzer2On(int OnTime_msec = 5000);
	void m_fnBuzzer2Off();
	void m_fnTT01PositionMove(int nPosValue, int nTimeOut=0);
	void m_fnTT01AlignMove(int nPosValue);
	int m_fnTT01AlignMove_Return(VS24MotData stReturn_Msg);


	void m_fnSetLight(int Select, int ValueRing, int ValueBack, int ValueReflect);
	int m_fnSetLight_Return(VS24MotData stReturn_Msg);


	void m_fnSpaceSensorMeasureStart(bool OnOff, float fTargetValue);	
	int m_fnSpaceSensorMeasureStart_Return(VS24MotData stReturn_Msg);


	void m_fnDoorReleasFront();
	void m_fnDoorReleasSide();
	void m_fnDoorReleasBack();
	void m_fnDoorLockFront();
	void m_fnDoorLockSide();
	void m_fnDoorLockBack();

	void m_fnDoorReleasFront_Return(VS24MotData stReturn_Msg);
	void m_fnDoorReleasSide_Return(VS24MotData stReturn_Msg);
	void m_fnDoorReleasBack_Return(VS24MotData stReturn_Msg);
	void m_fnDoorLockFront_Return(VS24MotData stReturn_Msg);
	void m_fnDoorLockSide_Return(VS24MotData stReturn_Msg);
	void m_fnDoorLockBack_Return(VS24MotData stReturn_Msg);


	void m_fnInspectTriggerSet(float SetValue_um);
	void m_fnAreaScanTriggerSet(float SetValue_um);


	//Gripper상태를 체크 하기위해 2중 (통신과, I/O)
	int m_fnGetAM01State_CySol_TEN_GRIPPER1();
	int m_fnGetAM01State_CySol_TEN_GRIPPER2();
	int m_fnGetAM01State_CySol_TEN_GRIPPER3();
	int m_fnGetAM01State_CySol_TEN_GRIPPER4();
};

