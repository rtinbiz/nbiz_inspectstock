// SysParamDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SysParamDlg.h"
#include "afxdialogex.h"


// CSysParamDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSysParamDlg, CDialogEx)

CSysParamDlg::CSysParamDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSysParamDlg::IDD, pParent)
{

}

CSysParamDlg::~CSysParamDlg()
{
}

void CSysParamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_SYSTEM_PARAM, m_gridSystemParam);
	DDX_Control(pDX, IDC_GRID_REVIE_CAM_OFFSET, m_gridReviewOffset);
	DDX_Control(pDX, IDC_GRID_SCOPE_OFFSET, m_gridScopeOffset);
	DDX_Control(pDX, IDC_GRID_REVIEW_LENS_OFFSET, m_gridReviewLensOffset);
	DDX_Control(pDX, IDC_GRID_SCOPE_LENS_OFFSET, m_gridScopeLensOffset);
	DDX_Control(pDX, IDC_BT_SAVE, m_btSysRecipeSave);
	DDX_Control(pDX, IDC_BT_EXIT, m_btSysRecipeExit);
	DDX_Control(pDX, IDC_BT_MCC_PARAM_SAVE, m_btSysMCCParamSave);
	DDX_Control(pDX, IDC_BT_REVIEW_LENS_CALC_START, m_btSysReviewLensCalcStart);
	DDX_Control(pDX, IDC_BT_REVIEW_LENS_CALC_START2, m_btSysReviewLensCalcStop);
}


BEGIN_MESSAGE_MAP(CSysParamDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BT_SAVE, &CSysParamDlg::OnBnClickedBtSave)
	ON_BN_CLICKED(IDC_BT_EXIT, &CSysParamDlg::OnBnClickedBtExit)
	ON_BN_CLICKED(IDC_BT_MCC_PARAM_SAVE, &CSysParamDlg::OnBnClickedBtMccParamSave)
	ON_BN_CLICKED(IDC_CH_MCC_WRITE_ON, &CSysParamDlg::OnBnClickedChMccWriteOn)
	ON_BN_CLICKED(IDC_CH_MCC_WRITE_OFF, &CSysParamDlg::OnBnClickedChMccWriteOff)
	ON_BN_CLICKED(IDC_BT_REVIEW_LENS_CALC_START, &CSysParamDlg::OnBnClickedBtReviewLensCalcStart)
	ON_BN_CLICKED(IDC_BT_REVIEW_LENS_CALC_START2, &CSysParamDlg::OnBnClickedBtReviewLensCalcStart2)
END_MESSAGE_MAP()


// CSysParamDlg 메시지 처리기입니다.


BOOL CSysParamDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CSysParamDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE)
	{
		LoadSystemParam();

		int I_RefreshTime;
		int FileMakeTime;
		int WriteOnOff;
		G_MccCommand.m_fnReadMCC_Param(&I_RefreshTime, &FileMakeTime, &WriteOnOff );
		if(WriteOnOff>0)
		{
			CheckDlgButton(IDC_CH_MCC_WRITE_ON, BST_CHECKED);
			CheckDlgButton(IDC_CH_MCC_WRITE_OFF, BST_UNCHECKED);
		}
		else
		{
			CheckDlgButton(IDC_CH_MCC_WRITE_ON, BST_UNCHECKED);
			CheckDlgButton(IDC_CH_MCC_WRITE_OFF, BST_CHECKED);
		}
		CString strNewData;
		strNewData.Format("%d", FileMakeTime);
		GetDlgItem(IDC_ED_MCC_MAKE_TIME)->SetWindowText(strNewData);
		strNewData.Format("%d", I_RefreshTime);
		GetDlgItem(IDC_ED_MCC_REFRESH_TIME)->SetWindowText(strNewData);
	}
}
void CSysParamDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btSysRecipeSave.GetTextColor(&tColor);
		m_btSysRecipeSave.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btSysMCCParamSave.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSysMCCParamSave.SetTextColor(&tColor);
		m_btSysMCCParamSave.SetCheckButton(true, true);
		m_btSysMCCParamSave.SetFont(&tFont);

		m_btSysRecipeSave.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSysRecipeSave.SetTextColor(&tColor);
		m_btSysRecipeSave.SetCheckButton(true, true);
		m_btSysRecipeSave.SetFont(&tFont);

		m_btSysRecipeExit.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSysRecipeExit.SetTextColor(&tColor);
		m_btSysRecipeExit.SetCheckButton(true, true);
		m_btSysRecipeExit.SetFont(&tFont);


		m_btSysReviewLensCalcStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSysReviewLensCalcStart.SetTextColor(&tColor);
		m_btSysReviewLensCalcStart.SetCheckButton(true, true);
		m_btSysReviewLensCalcStart.SetFont(&tFont);

		m_btSysReviewLensCalcStop.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSysReviewLensCalcStop.SetTextColor(&tColor);
		m_btSysReviewLensCalcStop.SetCheckButton(true, true);
		m_btSysReviewLensCalcStop.SetFont(&tFont);
	}

	GetDlgItem(IDC_ED_MCC_MAKE_TIME)->SetFont(&G_MidFont);
	GetDlgItem(IDC_ED_MCC_REFRESH_TIME)->SetFont(&G_MidFont);


	int CellHeight = 20;
	//////////////////////////////////////////////////////////////////////////
	m_gridSystemParam.SetDefCellWidth(50);
	m_gridSystemParam.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();

	m_gridSystemParam.SetRowCount(6+43+4+1);
	m_gridSystemParam.SetColumnCount(4);
	m_gridSystemParam.SetFixedRowCount(1);		
	m_gridSystemParam.SetFixedColumnCount(1);
	m_gridSystemParam.SetListMode(FALSE);
	//m_gridSystemParam.SetEditable(FALSE);
	m_gridSystemParam.SetColumnResize(FALSE);
	m_gridSystemParam.SetRowResize(FALSE);
	m_gridSystemParam.SetFixedTextColor(RGB(255,100,100));
	m_gridSystemParam.SetFixedBkColor(RGB(0,0,0));

	m_gridSystemParam.SetFont(&G_GridSmallFont);
	m_gridSystemParam.SetItemText(0, 0, "System Param");		m_gridSystemParam.SetColumnWidth(0, 180);
	m_gridSystemParam.SetItemText(0, 1, "Value");				m_gridSystemParam.SetColumnWidth(1, 90);
	m_gridSystemParam.SetItemText(0, 2, "Unit");					m_gridSystemParam.SetColumnWidth(2, 45);
	m_gridSystemParam.SetItemText(0, 3, "Comment");			m_gridSystemParam.SetColumnWidth(3, 226);

	int InputIndex = 1;
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_NowRunBoxID);				
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_NowRunStickID);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_NowRunRecipeName);

	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_ScanLineShiftDistance);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_ScanImgResolution_W);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_ScanImgResolution_H);

	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_ZPos_TT01Out);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_ZPos_Grip_Standby);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_ZPos_Grip);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_ZPos_Measure);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_ZPos_Standby);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_TPos_Standby);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_TPos_Grip);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_TPos_TensionStandby);

	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_Jig_StandbyPos);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Gripper_Jig_MeasurePos);

	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ZPos_Measure);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ZPos_Standby);
	//m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ZPos_ThetaAlign);
	//m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ZPos_Area3DMeasure);

	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Space_ZPos_Measure);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Space_ZPos_Standby);

	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_Standby_Shift);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_Standby_Drive);
	//m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ThetaAlignLensIndex);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ThetaAlignDrivePos1);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ThetaAlignShiftPos1);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ThetaAlignDrivePos2);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ThetaAlignShiftPos2);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ThetaAlignLightVale);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ThetaAlignPos2Offset);
	//m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scope_ThetaAlignAFDistance);

	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scan_ZPos_Review);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scan_ZPos_Standby);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scan_ZPos_Inspection);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scan_Standby_Shift);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Scan_Standby_Drive);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_CellAlign_LensIndex);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_CellAlign_BackLight);

	//m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CDTP_LensIndex);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_AutoReview_LensIndex);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_AutoReview_ReflectLight);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_AutoReview_MaxCnt);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_AutoReview_BackLight);

	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_AutoReviewMac_LensIndex);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_AutoReviewMac_ReflectLight);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_AutoReviewMac_MaxCnt);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_AutoReviewMac_BackLight);

	
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens_ZUpDnPos);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens2X_ReflectLight);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens2X_Shift);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens2X_Drive);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens10X_ReflectLight);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens10X_Shift);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens10X_Drive);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens20X_ReflectLight);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens20X_Shift);
	m_gridSystemParam.SetItemText(InputIndex++, 0, Key_Review_CalLens20X_Drive);




	InputIndex = 1;
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);

	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);

	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridSystemParam.SetItemFormat(InputIndex++, 1, DT_CENTER);
	InputIndex = 1;
	m_gridSystemParam.SetItemText(InputIndex++, 2, "str");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "str");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "str");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	//m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	//m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	//m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "Pixel");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	//m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	//m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");

	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");

	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "val");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");
	m_gridSystemParam.SetItemText(InputIndex++, 2, "um");

	InputIndex = 1;
	m_gridSystemParam.SetItemText(InputIndex++, 3, "현재 Box ID");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "현재 Skick ID");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "현재 Recipe ID");

	m_gridSystemParam.SetItemText(InputIndex++, 3, "Inspection Shift Distance");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Inspection Resolution Width");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Inspection Resolution Height");

	m_gridSystemParam.SetItemText(InputIndex++, 3, "Up/Down: Turn Table In/Out 가능 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Up/Down: Grip 대기 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Up/Down: Grip 가능 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Up/Down: Measure 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Up/Down: 대기위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "인장 대기 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "인장 Grip 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "초기 인장 Start 위치");

	m_gridSystemParam.SetItemText(InputIndex++, 3, "Stick 지그 대기 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Stick 지그 측정 위치");

	m_gridSystemParam.SetItemText(InputIndex++, 3, "3D Scope 측정 Up Down위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "3D Scope 대기 Up Down위치");
	//m_gridSystemParam.SetItemText(InputIndex++, 3, "3D Scope Theta Align 위치");
	//m_gridSystemParam.SetItemText(InputIndex++, 3, "Area 3D Scan 측정 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "단차 센서 측정 Up Down위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "단차 센서 대기 Up Down위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "3D Scope Shift 축 대기 위치(세로)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "3D Scope Drive 축 대기 위치(가로)");
	//m_gridSystemParam.SetItemText(InputIndex++, 3, "Theta Align Lens");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Theta Align기준위치 1(Drive축)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Theta Align기준위치 1(Shift축)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Theta Align기준위치 2(Drive축)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Theta Align기준위치 2(Shift축)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Theta Align 조명 값");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Theta Align Pos2 Offset");
	//m_gridSystemParam.SetItemText(InputIndex++, 3, "Theta Align AF위치(Shift축방향)");

	m_gridSystemParam.SetItemText(InputIndex++, 3, "Review-Cam Review Up Down위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Review-Cam 대기  Up Down위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Inspection-Cam 검사  Up Down위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Review Shift 축 대기 위치(세로)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Review Drive 축 대기 위치(가로)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Cell Align Lens");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Cell Align Back Light");
	//m_gridSystemParam.SetItemText(InputIndex++, 3, "CD, TP 측정 Lens");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Micro Auto Review Lens");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Micro Auto Review Light");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Micro Auto Review Count");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Micro Auto Review Backlight");

	m_gridSystemParam.SetItemText(InputIndex++, 3, "Macro Auto Review Lens");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Macro Auto Review Light");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Macro Auto Review Count");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "Macro Auto Review Backlight");

	m_gridSystemParam.SetItemText(InputIndex++, 3, "Lens Calibration 업,다운 위치");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "2X Lens Cal 조명");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "2X Lens Cal Shift 위치(X Axis)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "2X Lens Cal Drive 위치(Y Axis)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "10X Lens Cal 조명");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "10X Lens Cal Shift 위치(X Axis)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "10X Lens Cal Drive 위치(Y Axis)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "20X Lens Cal 조명");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "20X Lens Cal Shift 위치(X Axis)");
	m_gridSystemParam.SetItemText(InputIndex++, 3, "20X Lens Cal Drive 위치(Y Axis)");

	CGridCellCombo *pCell = NULL;
	CStringArray options;
	//if (!m_gridSystemParam.SetCellType(23,1, RUNTIME_CLASS(CGridCellCombo)))
	//	return ;
	//options.RemoveAll();
	//options.Add("10x");
	//options.Add("20x");//Align은 10, 20에서만 하자.
	////options.Add("50x");
	////options.Add("150x");
	//pCell = (CGridCellCombo*) m_gridSystemParam.GetCell(23, 1);
	//pCell->SetOptions(options);
	//pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

	if (!m_gridSystemParam.SetCellType(34,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("02x");
	options.Add("10x");
	options.Add("20x");//Align은 10, 20에서만 하자.
	pCell = (CGridCellCombo*) m_gridSystemParam.GetCell(34, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

	if (!m_gridSystemParam.SetCellType(36,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("02x");
	options.Add("10x");
	options.Add("20x");//Align은 10, 20에서만 하자.
	pCell = (CGridCellCombo*) m_gridSystemParam.GetCell(36, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	


	if (!m_gridSystemParam.SetCellType(40,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("02x");
	options.Add("10x");
	options.Add("20x");//Align은 10, 20에서만 하자.
	pCell = (CGridCellCombo*) m_gridSystemParam.GetCell(40, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	
	//if (!m_gridSystemParam.SetCellType(37,1, RUNTIME_CLASS(CGridCellCombo)))
	//	return ;
	//options.RemoveAll();
	////options.Add("02x");
	//options.Add("10x");
	//options.Add("20x");//Align은 10, 20에서만 하자.

	//pCell = (CGridCellCombo*) m_gridSystemParam.GetCell(37, 1);
	//pCell->SetOptions(options);
	//pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	
	//////////////////////////////////////////////////////////////////////////

	m_gridReviewOffset.SetDefCellWidth(50);
	m_gridReviewOffset.SetDefCellHeight(CellHeight);
	//m_gridReviewOffset.AutoFill();

	m_gridReviewOffset.SetRowCount(4+2+1);
	m_gridReviewOffset.SetColumnCount(4);
	m_gridReviewOffset.SetFixedRowCount(1);		
	m_gridReviewOffset.SetFixedColumnCount(1);
	m_gridReviewOffset.SetListMode(FALSE);
	//m_gridReviewOffset.SetEditable(FALSE);
	m_gridReviewOffset.SetColumnResize(FALSE);
	m_gridReviewOffset.SetRowResize(FALSE);
	m_gridReviewOffset.SetFixedTextColor(RGB(100,255,200));
	m_gridReviewOffset.SetFixedBkColor(RGB(0,0,0));

	m_gridReviewOffset.SetFont(&G_GridSmallFont);
	m_gridReviewOffset.SetItemText(0, 0, "Review Obj Offset");		m_gridReviewOffset.SetColumnWidth(0, 180);
	m_gridReviewOffset.SetItemText(0, 1, "Value");						m_gridReviewOffset.SetColumnWidth(1, 90);
	m_gridReviewOffset.SetItemText(0, 2, "Unit");						m_gridReviewOffset.SetColumnWidth(2, 45);
	m_gridReviewOffset.SetItemText(0, 3, "Comment");				m_gridReviewOffset.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridReviewOffset.SetItemText(InputIndex++, 0, Key_ReviewToScopeDisOffsetShift);
	m_gridReviewOffset.SetItemText(InputIndex++, 0, Key_ReviewToScopeDisOffsetDrive);

	m_gridReviewOffset.SetItemText(InputIndex++, 0, Key_LineScanDisX);
	m_gridReviewOffset.SetItemText(InputIndex++, 0, Key_LineScanDisY);
	m_gridReviewOffset.SetItemText(InputIndex++, 0, Key_SpaceSensorDisX);
	m_gridReviewOffset.SetItemText(InputIndex++, 0, Key_SpaceSensorDisY);
	InputIndex = 1;
	m_gridReviewOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	InputIndex = 1;
	m_gridReviewOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewOffset.SetItemText(InputIndex++, 2, "um");
	InputIndex = 1;
	m_gridReviewOffset.SetItemText(InputIndex++, 3, "Review에서3DScope Cam까지 거리 X(shift)");
	m_gridReviewOffset.SetItemText(InputIndex++, 3, "Review에서3DScope Cam까지 거리 Y(drive)");

	m_gridReviewOffset.SetItemText(InputIndex++, 3, "Review에서 Line Scan Cam까지 거리 X");
	m_gridReviewOffset.SetItemText(InputIndex++, 3, "Review에서 Line Scan Cam까지 거리 Y");
	m_gridReviewOffset.SetItemText(InputIndex++, 3, "Review에서 Space Sensor까지 거리 X");
	m_gridReviewOffset.SetItemText(InputIndex++, 3, "Review에서 Space Sensor까지 거리 Y");
	//////////////////////////////////////////////////////////////////////////

	m_gridScopeOffset.SetDefCellWidth(50);
	m_gridScopeOffset.SetDefCellHeight(CellHeight);
	//m_gridScopeOffset.AutoFill();

	m_gridScopeOffset.SetRowCount(6+2+1);
	m_gridScopeOffset.SetColumnCount(4);
	m_gridScopeOffset.SetFixedRowCount(1);		
	m_gridScopeOffset.SetFixedColumnCount(1);
	m_gridScopeOffset.SetListMode(FALSE);
	//m_gridScopeOffset.SetEditable(FALSE);
	m_gridScopeOffset.SetColumnResize(FALSE);
	m_gridScopeOffset.SetRowResize(FALSE);
	m_gridScopeOffset.SetFixedTextColor(RGB(255,200,100));
	m_gridScopeOffset.SetFixedBkColor(RGB(0,0,0));

	m_gridScopeOffset.SetFont(&G_GridSmallFont);
	m_gridScopeOffset.SetItemText(0, 0, "Scope Obj Offset");		m_gridScopeOffset.SetColumnWidth(0, 180);
	m_gridScopeOffset.SetItemText(0, 1, "Value");						m_gridScopeOffset.SetColumnWidth(1, 90);
	m_gridScopeOffset.SetItemText(0, 2, "Unit");						m_gridScopeOffset.SetColumnWidth(2, 45);
	m_gridScopeOffset.SetItemText(0, 3, "Comment");					m_gridScopeOffset.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridScopeOffset.SetItemText(InputIndex++, 0, Key_AirBlowDisX);
	m_gridScopeOffset.SetItemText(InputIndex++, 0, Key_AirBlowDisY);
	m_gridScopeOffset.SetItemText(InputIndex++, 0, Key_BackLightDisX);
	m_gridScopeOffset.SetItemText(InputIndex++, 0, Key_BackLightDisY);
	m_gridScopeOffset.SetItemText(InputIndex++, 0, Key_ScanSensorDisX);
	m_gridScopeOffset.SetItemText(InputIndex++, 0, Key_ScanSensorDisY);
	m_gridScopeOffset.SetItemText(InputIndex++, 0, Key_ThetaAlignCamDisX);
	m_gridScopeOffset.SetItemText(InputIndex++, 0, Key_ThetaAlignCamDisY);
	InputIndex = 1;
	m_gridScopeOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	InputIndex = 1;
	m_gridScopeOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeOffset.SetItemText(InputIndex++, 2, "um");
	InputIndex = 1;
	m_gridScopeOffset.SetItemText(InputIndex++, 3, "Scope에서 AirBlow까지 거리 X");
	m_gridScopeOffset.SetItemText(InputIndex++, 3, "Scope에서 AirBlow까지 거리 Y");
	m_gridScopeOffset.SetItemText(InputIndex++, 3, "Scope에서 BackLight까지 거리 X");
	m_gridScopeOffset.SetItemText(InputIndex++, 3, "Scope에서 BackLight까지 거리 Y");
	m_gridScopeOffset.SetItemText(InputIndex++, 3, "Scope에서 Scan3D Sensor까지 거리 X");
	m_gridScopeOffset.SetItemText(InputIndex++, 3, "Scope에서 Scan3D Sensor까지 거리 Y");
	m_gridScopeOffset.SetItemText(InputIndex++, 3, "Scope에서 Theta Align Camera까지 거리 X");
	m_gridScopeOffset.SetItemText(InputIndex++, 3, "Scope에서 Theta Align Camera까지 거리 Y");
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	m_gridReviewLensOffset.SetDefCellWidth(50);
	m_gridReviewLensOffset.SetDefCellHeight(CellHeight);
	//m_gridReviewLensOffset.AutoFill();
	m_gridReviewLensOffset.SetRowCount(15+1);
	m_gridReviewLensOffset.SetColumnCount(4);
	m_gridReviewLensOffset.SetFixedRowCount(1);		
	m_gridReviewLensOffset.SetFixedColumnCount(1);
	m_gridReviewLensOffset.SetListMode(FALSE);
	//m_gridReviewOffset.SetEditable(FALSE);
	m_gridReviewLensOffset.SetColumnResize(FALSE);
	m_gridReviewLensOffset.SetRowResize(FALSE);
	m_gridReviewLensOffset.SetFixedTextColor(RGB(200,155,255));
	m_gridReviewLensOffset.SetFixedBkColor(RGB(0,0,0));

	m_gridReviewLensOffset.SetFont(&G_GridSmallFont);
	m_gridReviewLensOffset.SetItemText(0, 0, "Review Lens Offset");			m_gridReviewLensOffset.SetColumnWidth(0, 180);
	m_gridReviewLensOffset.SetItemText(0, 1, "Value");						m_gridReviewLensOffset.SetColumnWidth(1, 90);
	m_gridReviewLensOffset.SetItemText(0, 2, "Unit");						m_gridReviewLensOffset.SetColumnWidth(2, 45);
	m_gridReviewLensOffset.SetItemText(0, 3, "Comment");				m_gridReviewLensOffset.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens02x_PixelSizeX);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens02x_PixelSizeY);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens02x_OffsetX);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens02x_OffsetY);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens02x_Light);

	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_PixelSizeX);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_PixelSizeY);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_OffsetX);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_OffsetY);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_Light);

	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_PixelSizeX);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_PixelSizeY);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_OffsetX);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_OffsetY);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_Light);

	InputIndex = 1;
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridReviewLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	InputIndex = 1;
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "val");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "val");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 2, "val");
	InputIndex = 1;
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 2X Pixel Size");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 2X Pixel Size");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 2X Lens Offset X");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 2X Lens Offset Y");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 2X Lens 조명");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 10X Pixel Size");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 10X Pixel Size");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 10X Lens Offset X");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 10X Lens Offset Y");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 10X Lens 조명");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 20X Pixel Size");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 20X Pixel Size");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 20X Lens Offset X");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 20X Lens Offset Y");
	m_gridReviewLensOffset.SetItemText(InputIndex++, 3, "Review 20X Lens 조명");

	//기준 Lens 고정
	m_gridReviewLensOffset.SetItemBkColour(13,1,RGB(255,100,100));
	m_gridReviewLensOffset.SetItemBkColour(14,1,RGB(255,100,100));
	//////////////////////////////////////////////////////////////////////////
	m_gridScopeLensOffset.SetDefCellWidth(50);
	m_gridScopeLensOffset.SetDefCellHeight(CellHeight);
	//m_gridScopeLensOffset.AutoFill();
	m_gridScopeLensOffset.SetRowCount(16+1);
	m_gridScopeLensOffset.SetColumnCount(4);
	m_gridScopeLensOffset.SetFixedRowCount(1);		
	m_gridScopeLensOffset.SetFixedColumnCount(1);
	m_gridScopeLensOffset.SetListMode(FALSE);
	//m_gridScopeLensOffset.SetEditable(FALSE);
	m_gridScopeLensOffset.SetColumnResize(FALSE);
	m_gridScopeLensOffset.SetRowResize(FALSE);
	m_gridScopeLensOffset.SetFixedTextColor(RGB(200,255,200));
	m_gridScopeLensOffset.SetFixedBkColor(RGB(0,0,0));

	m_gridScopeLensOffset.SetFont(&G_GridSmallFont);
	m_gridScopeLensOffset.SetItemText(0, 0, "Scope Lens Offset");	m_gridScopeLensOffset.SetColumnWidth(0, 180);
	m_gridScopeLensOffset.SetItemText(0, 1, "Value");					m_gridScopeLensOffset.SetColumnWidth(1, 90);
	m_gridScopeLensOffset.SetItemText(0, 2, "Unit");						m_gridScopeLensOffset.SetColumnWidth(2, 45);
	m_gridScopeLensOffset.SetItemText(0, 3, "Comment");				m_gridScopeLensOffset.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_PixelSizeX);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_PixelSizeY);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_OffsetX);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens10x_OffsetY);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_PixelSizeX);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_PixelSizeY);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_OffsetX);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens20x_OffsetY);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens50x_PixelSizeX);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens50x_PixelSizeY);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens50x_OffsetX);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens50x_OffsetY);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens150x_PixelSizeX);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens150x_PixelSizeY);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens150x_OffsetX);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 0, Key_Lens150x_OffsetY);
	InputIndex = 1;
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridScopeLensOffset.SetItemFormat(InputIndex++, 1, DT_CENTER);
	InputIndex = 1;
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 2, "um");
	InputIndex = 1;
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 10X Pixel Size");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 10X Pixel Size");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 10X Lens Offset X");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 10X Lens Offset Y");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 20X Pixel Size");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 20X Pixel Size");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 20X Lens Offset X");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 20X Lens Offset Y");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 50X Pixel Size");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 50X Pixel Size");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 50X Lens Offset X");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 50X Lens Offset Y");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 150X Pixel Size");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 150X Pixel Size");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 150X Lens Offset X");
	m_gridScopeLensOffset.SetItemText(InputIndex++, 3,  "Scope 150X Lens Offset Y");

	//기준 Lens 고정
	m_gridScopeLensOffset.SetItemBkColour(11,1,RGB(255,100,100));
	m_gridScopeLensOffset.SetItemBkColour(12,1,RGB(255,100,100));
}
void CSysParamDlg::LoadSystemParam()
{
	COLORREF crSysInfo =					RGB(255,255,0);
	COLORREF crLineScanInfo =			RGB(128,255,0);
	COLORREF crGripperZInfo =			RGB(255,128,0);
	COLORREF crGripperTentionInfo =	RGB(255,255,128);
	COLORREF crScopeZInfo =				RGB(255,128,255);
	COLORREF crSpaceZInfo =				RGB(128,128,0);
	COLORREF crScopeXYInfo =			RGB(128,128,128);
	COLORREF crThetaAlignInfo =			RGB(128,128,255);
	COLORREF crScanZInfo =				RGB(255,128,128);
	COLORREF crScanXYInfo =				RGB(128,128,128);
	COLORREF crCellAlignInfo =			RGB(255,200,200);
	COLORREF crAutoReviewInfo =		RGB(200,200,255);
	//////////////////////////////////////////////////////////////////////////
	char strReadData[256];
	int InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunBoxID, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crSysInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crSysInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crSysInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanLineShiftDistance, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crLineScanInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_W, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crLineScanInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_H, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crLineScanInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_TT01Out, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Grip_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Grip, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Measure, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_TPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_TPos_Grip, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_TPos_TensionStandby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_Jig_StandbyPos, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_Jig_MeasurePos, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Measure, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScopeZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScopeZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopePos, Key_Scope_ZPos_ThetaAlign, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScopeZInfo);
	//m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Area3DMeasure, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScopeZInfo);
	//m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Space_ZPos_Measure, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crSpaceZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Space_ZPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crSpaceZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_Standby_Shift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScopeXYInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_Standby_Drive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScopeXYInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignLensIndex, "10x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	//m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignDrivePos1, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignShiftPos1, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignDrivePos2, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignShiftPos2, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignLightVale, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignPos2Offset, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignAFDistance, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	//m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Review, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScanZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScanZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Inspection, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScanZInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_Standby_Shift, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScanXYInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_Standby_Drive, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crScanXYInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_CellAlign_LensIndex, "10x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_CellAlign_BackLight, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScanPos, Key_Review_CDTP_LensIndex, "20x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReview_LensIndex, "20x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crAutoReviewInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReview_ReflectLight, "100", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crAutoReviewInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReview_MaxCnt, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crAutoReviewInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReview_BackLight, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crAutoReviewInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_LensIndex, "20x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crAutoReviewInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_ReflectLight, "100", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crAutoReviewInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_MaxCnt, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crAutoReviewInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_BackLight, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crAutoReviewInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens_ZUpDnPos, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_ReflectLight, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_Shift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_Drive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_ReflectLight, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_Shift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_Drive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_ReflectLight, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_Shift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_Drive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridSystemParam.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridSystemParam.SetItemText(InputIndex++, 1, strReadData);
	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetShift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetDrive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewOffset.SetItemText(InputIndex++, 1, strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewCamToObjDis, Key_SpaceSensorDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewCamToObjDis, Key_SpaceSensorDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewOffset.SetItemText(InputIndex++, 1, strReadData);
	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_AirBlowDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_AirBlowDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ScanSensorDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ScanSensorDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeOffset.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ThetaAlignCamDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ThetaAlignCamDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeOffset.SetItemText(InputIndex++, 1, strReadData);

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_Light, "10", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_Light, "10", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, "기준");
	memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, "기준");
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_Light, "10", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridReviewLensOffset.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridReviewLensOffset.SetItemText(InputIndex++, 1, strReadData);


	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crThetaAlignInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crCellAlignInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crGripperTentionInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, "기준");
	memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, "기준");

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crLineScanInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crLineScanInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crLineScanInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_gridScopeLensOffset.SetItemBkColour(InputIndex, 1, crLineScanInfo);
	m_gridScopeLensOffset.SetItemText(InputIndex++, 1, strReadData);
}
void CSysParamDlg::SaveSystemParam()
{
	CString strNewData;
	//Cell Info Save
	int InputIndex = 1;
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_Process_Data, Key_NowRunBoxID, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_Process_Data, Key_NowRunStickID, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	//Default OK
	WritePrivateProfileString(Section_Process_Data, Key_NowProcessResult, "OK", VS10Master_PARAM_INI_PATH);


	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_InspectionInfo, Key_ScanLineShiftDistance, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_W, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_H, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_TT01Out, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Grip_Standby, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Grip, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Measure, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Standby, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_TPos_Standby, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_TPos_Grip, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_TPos_TensionStandby, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_Jig_StandbyPos, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_GripperPos, Key_Gripper_Jig_MeasurePos, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Measure, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Standby, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	//strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	//WritePrivateProfileString(Section_ScopePos, Key_Scope_ZPos_ThetaAlign, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	//strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	//WritePrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Area3DMeasure, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Space_ZPos_Measure, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Space_ZPos_Standby, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_Standby_Shift, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_Standby_Drive, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	//strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	//WritePrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignLensIndex, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignDrivePos1, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignShiftPos1, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignDrivePos2, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignShiftPos2, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignLightVale, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignPos2Offset, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	//strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	//WritePrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignAFDistance, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Review, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Standby, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Inspection, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Scan_Standby_Shift, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Scan_Standby_Drive, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_CellAlign_LensIndex, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_CellAlign_BackLight, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	//strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	//WritePrivateProfileString(Section_ScanPos, Key_Review_CDTP_LensIndex, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_AutoReview_LensIndex, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_AutoReview_ReflectLight, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_AutoReview_MaxCnt, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_AutoReview_BackLight, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_LensIndex, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_ReflectLight, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_MaxCnt, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_BackLight, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens_ZUpDnPos, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_ReflectLight, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_Shift, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_Drive, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_ReflectLight, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_Shift, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_Drive, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_ReflectLight, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_Shift, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridSystemParam.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_Drive, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridReviewOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetShift, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetDrive, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridReviewOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewCamToObjDis, Key_SpaceSensorDisX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewCamToObjDis, Key_SpaceSensorDisY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridScopeOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeCamToObjDis, Key_AirBlowDisX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeCamToObjDis, Key_AirBlowDisY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeCamToObjDis, Key_ScanSensorDisX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeCamToObjDis, Key_ScanSensorDisY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridScopeOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeCamToObjDis, Key_ThetaAlignCamDisX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeCamToObjDis, Key_ThetaAlignCamDisY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens02x_PixelSizeX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens02x_PixelSizeY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens02x_OffsetX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens02x_OffsetY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens02x_Light, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens10x_PixelSizeX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens10x_PixelSizeY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens10x_OffsetX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens10x_OffsetY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens10x_Light, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens20x_PixelSizeX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens20x_PixelSizeY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens20x_OffsetX, "0.0", VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens20x_OffsetY, "0.0", VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridReviewLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ReviewLens, Key_Lens20x_Light, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens10x_PixelSizeX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens10x_PixelSizeY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens10x_OffsetX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens10x_OffsetY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens20x_PixelSizeX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens20x_PixelSizeY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens20x_OffsetX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens20x_OffsetY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens50x_PixelSizeX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens50x_PixelSizeY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens50x_OffsetX, "0.0", VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens50x_OffsetY, "0.0", VS10Master_PARAM_INI_PATH);

	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens150x_PixelSizeX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens150x_PixelSizeY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens150x_OffsetX, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
	strNewData.Format("%s", m_gridScopeLensOffset.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_ScopeLens, Key_Lens150x_OffsetY, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
}
void CSysParamDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


HBRUSH CSysParamDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CSysParamDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSysParamDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


void CSysParamDlg::OnBnClickedBtSave()
{
	SaveSystemParam();
	::SendMessage(this->GetParent()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 1);//System Param
}


void CSysParamDlg::OnBnClickedBtExit()
{
	this->ShowWindow(SW_HIDE);
}


void CSysParamDlg::OnBnClickedChMccWriteOn()
{
	if( IsDlgButtonChecked(IDC_CH_MCC_WRITE_ON) ==BST_CHECKED)
	{
		CheckDlgButton(IDC_CH_MCC_WRITE_OFF, BST_UNCHECKED);
	}
}


void CSysParamDlg::OnBnClickedChMccWriteOff()
{
	if( IsDlgButtonChecked(IDC_CH_MCC_WRITE_OFF) ==BST_CHECKED)
	{
		CheckDlgButton(IDC_CH_MCC_WRITE_ON, BST_UNCHECKED);
	}
}


void CSysParamDlg::OnBnClickedBtMccParamSave()
{
	int RefreshTime;
	int FileMakeTime;
	int WriteOnOff;
	CString strReadData;
	GetDlgItem(IDC_ED_MCC_MAKE_TIME)->GetWindowText(strReadData);
	FileMakeTime = atoi(strReadData);
	GetDlgItem(IDC_ED_MCC_REFRESH_TIME)->GetWindowText(strReadData);
	RefreshTime = atoi(strReadData);
	if( IsDlgButtonChecked(IDC_CH_MCC_WRITE_ON) ==BST_CHECKED)
		WriteOnOff = 1;
	else
		WriteOnOff = 0;
	G_MccCommand.m_fnSendMCC_Parm(RefreshTime, FileMakeTime, WriteOnOff);
}




void CSysParamDlg::OnBnClickedBtReviewLensCalcStart()
{
	if(AfxMessageBox("Review Lens 수치 측정을 시작 할 까요?\r\n(기존 동작은 모두 멈춥니다.)",
		MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDYES)
	{	
		::SendMessage(this->GetParent()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 101);
	}
}


void CSysParamDlg::OnBnClickedBtReviewLensCalcStart2()
{
	::SendMessage(this->GetParent()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 102);
}
