#pragma once



class CBoxControl
{
public:
	CBoxControl();
	~CBoxControl();
	
	int m_fnSetParameter(
		SBT01_POSITION* stpointer1, 
		STM01_POSITION* stpointer2,
		SLT01_POSITION* stpointer3,
		SBO01_POSITION* stpointer4,
		SUT01_POSITION* stpointer5);

	int m_fnBoxMove_F_BT01_T_LT01();

	int m_fnBoxCoverOpen();

	int m_fnBoxCoverClose();

	int m_fnBoxMove_F_UT01_T_LT01();

	int m_fnBoxMove_F_LT01_T_BT01();	
	
	bool m_fnLoadingTableBoxSizeCheck();

	int m_fnGetNextBoxIndex(BOX_STATUS elBoxStatus);

	int m_fnGetNextBoxPos(int nBoxIndex);

	int m_fnGetPusherPos();
public:
	EL_BOX_LOADING m_elBoxLoadStep;
	EL_COVER_OPEN  m_elCoverOpenStep;

	SEQUENCE_ERROR m_elSeq_Error;
	BOX_SIZE		m_elBoxSize;

	const SBT01_POSITION*  m_st_BT01_Pos;
	const STM01_POSITION* m_st_TM01_Pos;
	const SLT01_POSITION* m_st_LT01_Pos;
	const SBO01_POSITION* m_st_BO01_Pos;
	const SUT01_POSITION* m_st_UT01_Pos;
};

