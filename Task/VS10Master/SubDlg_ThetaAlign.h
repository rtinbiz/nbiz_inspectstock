#pragma once
#include "afxwin.h"


// CSubDlg_ThetaAlign 대화 상자입니다.

class CSubDlg_ThetaAlign : public CDialogEx
{
	DECLARE_DYNAMIC(CSubDlg_ThetaAlign)

public:
	CSubDlg_ThetaAlign(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubDlg_ThetaAlign();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SUB_THETA_ALIGN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	SCOPE_LENS_OFFSET		*m_pScopeLensOffset;
	Scope3DPosition	*m_pScopePos;
	CInterServerInterface  *m_pServerInterface;
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:

	//CLiveGrab* m_pThetaAlignLiveObj;
	IplImage *m_pAlignImage1;
	IplImage *m_pAlignImage2;

	int m_GrabIndex;
	int m_Align1ImgPos;
	int m_Align2ImgPos;
	int m_ThetaMot_Pulse;
	HDC			m_HDCView[2];
	CRect			m_RectiewArea[2];
	void DrawImageView();

public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	CRoundButtonStyle m_tButtonStyle;	
	CRoundButton2 m_btMoveAlignPos1;
	CRoundButton2 m_btMoveAlignPos2;
	CRoundButton2 m_btGrabThetaAlignImg;
	CRoundButton2 m_btStickThetaCorrect;
	CRoundButton2 m_btMoveAFPos1;
	CRoundButton2 m_btMoveAFPos2;
	CRoundButton2 m_btScopeAutofocus;

	CColorStaticST m_stThetaAlignCorrectValue;
	afx_msg void OnBnClickedBtMoveAlign1pos();
	afx_msg void OnBnClickedBtMoveAlign2pos();
	afx_msg void OnBnClickedBtGrabAlignImg();
	afx_msg void OnBnClickedBtThetaCorrect();
	
	afx_msg void OnBnClickedBtMoveAf1pos();
	afx_msg void OnBnClickedBtMoveAf2pos();
	afx_msg void OnBnClickedBtAutoFocus();
	afx_msg void OnPaint();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
