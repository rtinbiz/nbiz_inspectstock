// SubDlg_AreaScan3D.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SubDlg_AreaScan3D.h"
#include "afxdialogex.h"


// CSubDlg_AreaScan3D 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSubDlg_AreaScan3D, CDialogEx)

CSubDlg_AreaScan3D::CSubDlg_AreaScan3D(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSubDlg_AreaScan3D::IDD, pParent)
{
	m_pServerInterface = nullptr;
	m_pScan3DParam = nullptr;
	m_pAlignCellStartX = nullptr;
	m_pAlignCellStartY = nullptr;
	m_pGlassReicpeParam = nullptr;
}

CSubDlg_AreaScan3D::~CSubDlg_AreaScan3D()
{
}

void CSubDlg_AreaScan3D::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ST_3DSCAN_INDEX, m_st3DScanIndex);
	DDX_Control(pDX, IDC_BT_3DSCAN_INIT, m_bt3DScanInit);
	DDX_Control(pDX, IDC_BT_3DSCAN_ALARM_RESET, m_bt3DScanAlarmReset);
	DDX_Control(pDX, IDC_BT_3DSCAN_PROCESS_START, m_bt3DScanProcessStart);
	DDX_Control(pDX, IDC_BT_3DSCAN_SCAN_START, m_bt3DScanScanStart);
	DDX_Control(pDX, IDC_BT_3DSCAN_SCAN_END, m_bt3DScanScanEnd);
	DDX_Control(pDX, IDC_BT_3DSCAN_PROCESS_END, m_bt3DScanProcessEnd);
	DDX_Control(pDX, IDC_ST_3DSCAN_TOTAL_CNT, m_st3DScanTotalCnt);
}


BEGIN_MESSAGE_MAP(CSubDlg_AreaScan3D, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_3DSCAN_INIT, &CSubDlg_AreaScan3D::OnBnClickedBt3dscanInit)
	ON_BN_CLICKED(IDC_BT_3DSCAN_ALARM_RESET, &CSubDlg_AreaScan3D::OnBnClickedBt3dscanAlarmReset)
	ON_BN_CLICKED(IDC_BT_3DSCAN_PROCESS_START, &CSubDlg_AreaScan3D::OnBnClickedBt3dscanProcessStart)
	ON_BN_CLICKED(IDC_BT_3DSCAN_SCAN_START, &CSubDlg_AreaScan3D::OnBnClickedBt3dscanScanStart)
	ON_BN_CLICKED(IDC_BT_3DSCAN_SCAN_END, &CSubDlg_AreaScan3D::OnBnClickedBt3dscanScanEnd)
	ON_BN_CLICKED(IDC_BT_3DSCAN_PROCESS_END, &CSubDlg_AreaScan3D::OnBnClickedBt3dscanProcessEnd)
END_MESSAGE_MAP()


// CSubDlg_AreaScan3D 메시지 처리기입니다.


BOOL CSubDlg_AreaScan3D::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSubDlg_AreaScan3D::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_bt3DScanInit.GetTextColor(&tColor);
		m_bt3DScanInit.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_bt3DScanInit.SetRoundButtonStyle(&m_tButtonStyle);
		m_bt3DScanInit.SetTextColor(&tColor);
		m_bt3DScanInit.SetCheckButton(true, true);
		m_bt3DScanInit.SetFont(&tFont);

		m_bt3DScanAlarmReset.SetRoundButtonStyle(&m_tButtonStyle);
		m_bt3DScanAlarmReset.SetTextColor(&tColor);
		m_bt3DScanAlarmReset.SetCheckButton(true, true);
		m_bt3DScanAlarmReset.SetFont(&tFont);

		m_bt3DScanProcessStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_bt3DScanProcessStart.SetTextColor(&tColor);
		m_bt3DScanProcessStart.SetCheckButton(true, true);
		m_bt3DScanProcessStart.SetFont(&tFont);

		m_bt3DScanScanStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_bt3DScanScanStart.SetTextColor(&tColor);
		m_bt3DScanScanStart.SetCheckButton(true, true);
		m_bt3DScanScanStart.SetFont(&tFont);

		m_bt3DScanScanEnd.SetRoundButtonStyle(&m_tButtonStyle);
		m_bt3DScanScanEnd.SetTextColor(&tColor);
		m_bt3DScanScanEnd.SetCheckButton(true, true);
		m_bt3DScanScanEnd.SetFont(&tFont);

		m_bt3DScanProcessEnd.SetRoundButtonStyle(&m_tButtonStyle);
		m_bt3DScanProcessEnd.SetTextColor(&tColor);
		m_bt3DScanProcessEnd.SetCheckButton(true, true);
		m_bt3DScanProcessEnd.SetFont(&tFont);

	}
	m_st3DScanIndex.SetFont(&G_TaskStateFont);
	m_st3DScanIndex.SetTextColor(RGB(255, 255, 0));
	m_st3DScanIndex.SetBkColor(RGB(0, 0, 0));

	m_st3DScanTotalCnt.SetFont(&G_TaskStateSmallFont);
	m_st3DScanTotalCnt.SetTextColor(RGB(255, 255, 0));
	m_st3DScanTotalCnt.SetBkColor(RGB(0, 0, 0));
	
}
void CSubDlg_AreaScan3D::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


HBRUSH CSubDlg_AreaScan3D::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(IDC_ST_3DSCAN_INDEX == DLG_ID_Number||
		IDC_ST_3DSCAN_TOTAL_CNT == DLG_ID_Number )
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CSubDlg_AreaScan3D::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSubDlg_AreaScan3D::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}

void CSubDlg_AreaScan3D::OnBnClickedBt3dscanInit()
{
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_32_Mesure2D,	nBiz_3DScan_System, nBiz_3DScan_UnitInitialize, nBiz_Unit_Zero);
	m_bt3DScanInit.SetCheck(true);
}

void CSubDlg_AreaScan3D::OnBnClickedBt3dscanAlarmReset()
{
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_32_Mesure2D,	nBiz_3DScan_System, nBiz_3DScan_AlarmReset, nBiz_Unit_Zero);
	m_bt3DScanAlarmReset.SetCheck(true);
}

void CSubDlg_AreaScan3D::OnBnClickedBt3dscanProcessStart()
{

	if(m_pScan3DParam != nullptr)
	{
		G_WriteInfo(Log_Normal, "3D Scan Process Start");
		m_pServerInterface->m_fnSendCommand_Nrs(	TASK_32_Mesure2D,	nBiz_3DScan_System, nBiz_3DScan_ProcessStart, nBiz_Unit_Zero,
			(USHORT)sizeof(SCAN_3DPara), (UCHAR*)m_pScan3DParam);
		m_bt3DScanProcessStart.SetCheck(true);
		m_st3DScanIndex.SetWindowText("1");

		//Prameter 전달과. Scan Start 위치로 간다.
	}
	else
		G_WriteInfo(Log_Error, "3D Scan Parameter Link Error");
}


void CSubDlg_AreaScan3D::OnBnClickedBt3dscanScanStart()
{
	CString strNewValue;
	m_st3DScanIndex.GetWindowText(strNewValue);
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_32_Mesure2D,	nBiz_3DScan_System, nBiz_3DScan_ScanStart, (USHORT)atoi(strNewValue));
	m_bt3DScanScanStart.SetCheck(true);
	//Ack가 들어오면 바로 Move를 한다.
}


void CSubDlg_AreaScan3D::OnBnClickedBt3dscanScanEnd()
{
	CString strNewValue;
	m_st3DScanIndex.GetWindowText(strNewValue);
	int nNowScanIndex = atoi(strNewValue);
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_32_Mesure2D,	nBiz_3DScan_System, nBiz_3DScan_ScanEnd, (USHORT)nNowScanIndex);
	m_bt3DScanScanEnd.SetCheck(true);

	//Ack가 들어오면 바로 Shift Move를 한다.
	//다음 Scan을 위해 증가 시킨다.
	if(nNowScanIndex == m_pScan3DParam->nScan_Count)
	{
		G_WriteInfo(Log_Normal, "3D Scan End");
		m_st3DScanIndex.SetWindowText("0");
	}
	else
	{
		strNewValue.Format("%d", nNowScanIndex+1);
		m_st3DScanIndex.SetWindowText(strNewValue);
	}

}


void CSubDlg_AreaScan3D::OnBnClickedBt3dscanProcessEnd()
{
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_32_Mesure2D,	nBiz_3DScan_System, nBiz_3DScan_ProcessEnd, nBiz_Unit_Zero);
	m_bt3DScanProcessEnd.SetCheck(true);
}
