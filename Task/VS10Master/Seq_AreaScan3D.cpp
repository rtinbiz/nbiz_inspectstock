#include "StdAfx.h"
#include "Seq_AreaScan3D.h"


CSeq_AreaScan3D::CSeq_AreaScan3D(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;
	
	m_RecvCMD_Ret = -1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;

	m_pScopePos = nullptr;
	m_pReviewPos = nullptr;
	m_pScan3DParam = nullptr;
	m_pScan3DResult = nullptr;
	m_pCamToObjDis = nullptr;
	m_pAlignCellStartX = nullptr;
	m_pAlignCellStartY = nullptr;

	m_AreaScanStartShift = 0;
	m_AreaScanStartDrive = 0;

	m_NowScanIndex =0;
	m_pAreaScanStartPosList = nullptr;
}


CSeq_AreaScan3D::~CSeq_AreaScan3D(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;

	if(m_pAreaScanStartPosList != nullptr)
		delete [] m_pAreaScanStartPosList;
	m_pAreaScanStartPosList = nullptr;
}

bool CSeq_AreaScan3D::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_AreaScan3D::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();
	G_WriteInfo(Log_Error, "CSeq_AreaScan3D Error Step Run( Z Axis Go to a safe position)");
	return 0;
}
void CSeq_AreaScan3D::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
int CSeq_AreaScan3D::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_AreaScan3D%03d> Sequence Start", ProcStep);

			m_NowScanIndex = 0;
			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_STICK_STATE_CHECK:
		{//Stick이 측정 가능 상태 인지 체크 한다.

			if(G_CheckStickProc_StaticState() == ST_PROC_NG)
			{
				G_WriteInfo(Log_Check, "SEQ_STEP_STICK_STATE_CHECK ST_PROC_NG", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			CString strFromPath;
			strFromPath.Format("%s\\%s\\", VS31Mesure3D_Result_PATH, "VS32Measure2D");
			G_fnCheckDirAndCreate(strFromPath.GetBuffer(strFromPath.GetLength()));
			G_ClearFolder(strFromPath);//이전것을 지운다.

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_AREASCAN_DATA_CALC:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_AREASCAN_DATA_CALC(%03d)>", ProcStep);
			//*m_pScan3DParam;
			//*m_pCamToObjDis;
			//1) Review의 Cell Start 좌표.
			//*m_pAlignCellStartX;
			//*m_pAlignCellStartY;
			//2) Scope Cell Start 좌표 변환.

			m_AreaScanStartShift = *m_pAlignCellStartX+m_pCamToObjDis->nReviewToScopeDisOffsetShift;
			m_AreaScanStartDrive = *m_pAlignCellStartY+m_pCamToObjDis->nReviewToScopeDisOffsetDrive;
			//3) Area 3D Sensor Offset 추가.
			m_AreaScanStartShift = m_AreaScanStartShift+m_pCamToObjDis->nScopeCamTo_ScanSensorDisX;
			m_AreaScanStartDrive = m_AreaScanStartDrive+m_pCamToObjDis->nScopeCamTo_ScanSensorDisY;
			//4) Area 3D Sensor H Dis Add.
			//m_AreaScanStartShift = m_AreaScanStartShift + (SCANER_SENSOR_WIDTH/2); //정의천
			//m_AreaScanStartDrive = m_AreaScanStartDrive;//정의천

			m_AreaScanStartShift = m_AreaScanStartShift + (SCANER_SENSOR_WIDTH/2)-20000; //정의천
			m_AreaScanStartDrive = m_AreaScanStartDrive-20000;//정의천


			m_pScan3DParam->nScan_Distance;
			m_pScan3DParam->nScan_Count;
			//좌표 Data 생성.
			if(m_pAreaScanStartPosList != nullptr)
				delete [] m_pAreaScanStartPosList;
			m_pAreaScanStartPosList = new CPoint[m_pScan3DParam->nScan_Count*2];

			for(int PStep = 1; PStep<=m_pScan3DParam->nScan_Count; PStep++)
			{
				if(PStep%2==1)
				{//Odd Scan
					m_pAreaScanStartPosList[PStep-1].y = m_AreaScanStartDrive;
				}
				else
				{//even Scan
					m_pAreaScanStartPosList[PStep-1].y = m_AreaScanStartDrive + m_pScan3DParam->nScan_Distance+40000;//정의천
				}
				m_pAreaScanStartPosList[PStep-1].x = m_AreaScanStartShift+ (SCANER_SENSOR_SHIFT*(PStep-1));
			}

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_AREASCAN_MEASURE_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_AREASCAN_MEASURE_Z(%03d)>", ProcStep);
			//G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Area3DMeasure);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_AREASCAN_MEASURE_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_AREASCAN_MEASURE_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	
	case SEQ_STEP_AREASCAN_STANDBY_POS_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_AREASCAN_STANDBY_POS_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_AreaScanStartShift, m_AreaScanStartDrive);
			int TimeOutWait = 30000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_AREASCAN_STANDBY_POS_XYSEQ_STEP_AREASCAN_STANDBY_POS_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_AREASCAN_STANDBY_POS_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			
			//Trigger Setting(시작 위치에서 수행 하자.)
			G_MotObj.m_fnAreaScanTriggerSet(m_pScan3DParam->fTrigger_Step);

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_AREASCAN_PARAM_SET:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_AREASCAN_PARAM_SET(%03d)>", ProcStep);
			m_WaitCMD = nBiz_3DScan_UnitInitialize;
			m_RecvCMD = 0;
			m_RecvCMD_Ret = -1;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_32_Mesure2D, nBiz_3DScan_System, nBiz_3DScan_UnitInitialize, nBiz_Unit_Zero);
			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					m_RecvCMD_Ret = -1;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_AREASCAN_PARAM_SET%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)
			{
				G_WriteInfo(Log_Error,"<SEQ_STEP_AREASCAN_PARAM_SET%03d> Unit Init Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			G_WriteInfo(Log_Check,"FileName: %s", m_pScan3DParam->strFileName);
			G_WriteInfo(Log_Check,"Scan_Count: %d", m_pScan3DParam->nScan_Count);
			G_WriteInfo(Log_Check,"Scan_Distance: %d", m_pScan3DParam->nScan_Distance);
			G_WriteInfo(Log_Check,"Trigger_Step: %0.1f", m_pScan3DParam->fTrigger_Step);
			G_WriteInfo(Log_Check,"Min_Height: %d", m_pScan3DParam->nPass_Min_Height);
			G_WriteInfo(Log_Check,"Max_Height: %d", m_pScan3DParam->nPass_Max_Height);

			//Param Set

			m_pScan3DParam->nScan_Distance+=40000;
			m_WaitCMD = nBiz_3DScan_ProcessStart;
			m_RecvCMD = 0;
			m_RecvCMD_Ret = -1;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_32_Mesure2D, nBiz_3DScan_System, nBiz_3DScan_ProcessStart, nBiz_Unit_Zero, 
				sizeof(SCAN_3DPara), (UCHAR*)m_pScan3DParam);
			TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_AREASCAN_PARAM_SET%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)
			{
				G_WriteInfo(Log_Error,"<SEQ_STEP_AREASCAN_PARAM_SET%03d> Param Set Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			
			m_pScan3DParam->nScan_Distance-=40000;
			//Scan 시작 Index를 설정 하자.
			m_NowScanIndex = 1;
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_AREASCAN_START_POS_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_AREASCAN_START_POS_XY(%03d)>", ProcStep);
			G_MotObj.m_fnScopeMultiMove(m_pAreaScanStartPosList[m_NowScanIndex-1].x, m_pAreaScanStartPosList[m_NowScanIndex-1].y);
			int TimeOutWait = 30000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_AREASCAN_START_POS_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_AREASCAN_START_POS_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_AREASCAN_START:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_AREASCAN_START(%03d)>", ProcStep);
			//Send Scan Start
			m_WaitCMD = nBiz_3DScan_ScanStart;
			m_RecvCMD = 0;
			m_RecvCMD_Ret = -1;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_32_Mesure2D, nBiz_3DScan_System, nBiz_3DScan_ScanStart, (USHORT)m_NowScanIndex);
			int TimeOutWait = 300000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_AREASCAN_START%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != m_NowScanIndex)
			{
				G_WriteInfo(Log_Error,"<SEQ_STEP_AREASCAN_START%03d> Param Set Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//끝나는 위치로 보낸다.
			int EndShiftPos = m_pAreaScanStartPosList[m_NowScanIndex-1].x;
			int EndDrivePos = 0;
			if(m_NowScanIndex%2 ==1)//Odd Scan
				EndDrivePos = m_pAreaScanStartPosList[m_NowScanIndex-1].y + m_pScan3DParam->nScan_Distance+40000;//졍의천
			else//EvenScan
				EndDrivePos = m_pAreaScanStartPosList[m_NowScanIndex-1].y - m_pScan3DParam->nScan_Distance-40000;//정의천


			G_WriteInfo(Log_Check,"Start Pos ==> Shift(%d), Drive(%d)", m_pAreaScanStartPosList[m_NowScanIndex-1].x, m_pAreaScanStartPosList[m_NowScanIndex-1].y);
			G_WriteInfo(Log_Check,"End Pos ==> Shift(%d), Drive(%d)", EndShiftPos, EndDrivePos);
			G_MotObj.m_fnScopeMultiMove(EndShiftPos, EndDrivePos);

			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_AREASCAN_END:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_AREASCAN_END(%03d)>", ProcStep);
			int TimeOutWait = 300000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_AREASCAN_END%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_AREASCAN_END%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			m_WaitCMD = nBiz_3DScan_ScanEnd;
			m_RecvCMD = 0;
			m_RecvCMD_Ret = -1;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_32_Mesure2D, nBiz_3DScan_System, nBiz_3DScan_ScanEnd, (USHORT)m_NowScanIndex);
			TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_AREASCAN_END%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != m_NowScanIndex)
			{
				G_WriteInfo(Log_Error,"<SEQ_STEP_AREASCAN_END%03d> 3DScan_ScanEnd Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//Next Scan
			m_NowScanIndex++;
			if(m_NowScanIndex<=m_pScan3DParam->nScan_Count)
			{
				fnSeqSetNextStep(SEQ_STEP_AREASCAN_START_POS_XY);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
		
	case SEQ_STEP_WAIT_RESUT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_WAIT_RESUT(%03d)>", ProcStep);
			//Send Scan Start
			m_WaitCMD = nBiz_3DScan_ProcessEnd;
			m_RecvCMD = 0;
			m_RecvCMD_Ret = -1;

			CString str3DScanRetSavePath;
			str3DScanRetSavePath.Format("AreaScan3D_%d", GetTickCount());
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_32_Mesure2D, nBiz_3DScan_System, nBiz_3DScan_ProcessEnd, nBiz_Unit_Zero);
			(USHORT)str3DScanRetSavePath.GetLength(), (UCHAR*)str3DScanRetSavePath.GetBuffer(str3DScanRetSavePath.GetLength());

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_WAIT_RESUT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)
			{
				G_WriteInfo(Log_Error,"<SEQ_STEP_WAIT_RESUT%03d> Process End Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//m_WaitCMD = nBiz_3DScan_FileBackupStart;
			//m_RecvCMD = 0;
			//m_RecvCMD_Ret = -1;
			//int TimeOutWait = 3000;//About 20sec
			//while(m_WaitCMD != m_RecvCMD)
			//{
			//	TimeOutWait --;
			//	if(TimeOutWait<0 || m_SeqStop == true)
			//	{
			//		m_WaitCMD = 0;
			//		m_RecvCMD = 0;
			//		if(m_SeqStop== true)
			//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
			//		else
			//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
			//		break;
			//	}
			//	Sleep(10);
			//}
			//if(TimeOutWait<0 || m_SeqStop == true)
			//{
			//	G_WriteInfo(Log_Check, "<SEQ_STEP_WAIT_RESUT%03d> Stop", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			m_WaitCMD = nBiz_3DScan_FileBackupEnd;
			m_RecvCMD = 0;
			m_RecvCMD_Ret = -1;
			TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_WAIT_RESUT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_32_Mesure2D, nBiz_3DScan_System, nBiz_3DScan_FileBackupEnd, nBiz_Unit_Zero);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			G_WriteInfo(Log_Normal, "<CSeq_AreaScan3D%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;
	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_AreaScan3D> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}