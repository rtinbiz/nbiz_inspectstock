#pragma once
#include "afxwin.h"


// CSubDlg_AreaScan3D 대화 상자입니다.

class CSubDlg_AreaScan3D : public CDialogEx
{
	DECLARE_DYNAMIC(CSubDlg_AreaScan3D)

public:
	CSubDlg_AreaScan3D(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubDlg_AreaScan3D();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SUB_AREA_SCAN3D };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CInterServerInterface  *m_pServerInterface;
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:

	int						*m_pAlignCellStartX;
	int						*m_pAlignCellStartY;
	GLASS_PARAM		*m_pGlassReicpeParam;
	SCAN_3DPara		*m_pScan3DParam;

	COLORREF m_btBackColor;
	void InitCtrl_UI();
	CColorStaticST m_st3DScanIndex;
	CColorStaticST m_st3DScanTotalCnt;
	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_bt3DScanInit;
	CRoundButton2 m_bt3DScanAlarmReset;
	CRoundButton2 m_bt3DScanProcessStart;
	CRoundButton2 m_bt3DScanScanStart;
	CRoundButton2 m_bt3DScanScanEnd;
	CRoundButton2 m_bt3DScanProcessEnd;
	afx_msg void OnBnClickedBt3dscanInit();
	afx_msg void OnBnClickedBt3dscanAlarmReset();
	afx_msg void OnBnClickedBt3dscanProcessStart();
	afx_msg void OnBnClickedBt3dscanScanStart();
	afx_msg void OnBnClickedBt3dscanScanEnd();
	afx_msg void OnBnClickedBt3dscanProcessEnd();
	
};
