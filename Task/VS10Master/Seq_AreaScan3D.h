#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
class CSeq_AreaScan3D: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////
		SEQ_STEP_STICK_STATE_CHECK,
		//========================
		SEQ_STEP_SPACE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_XY,
		SEQ_STEP_AREASCAN_DATA_CALC,
		SEQ_STEP_AREASCAN_MEASURE_Z,
		SEQ_STEP_AREASCAN_STANDBY_POS_XY,
		SEQ_STEP_AREASCAN_PARAM_SET,
		//--------------------------------------
		SEQ_STEP_AREASCAN_START_POS_XY,
		SEQ_STEP_AREASCAN_START,//Scan 시작 
		SEQ_STEP_AREASCAN_END,//Scan 종료 위치 도착.
		//--------------------------------------
		SEQ_STEP_WAIT_RESUT,
		SEQ_STEP_END_SCOPE_SAFETY_Z,
		SEQ_STEP_END_SCOPE_SAFETY_XY,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_AreaScan3D(void);
	~CSeq_AreaScan3D(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:

	int						*m_pAlignCellStartX;
	int						*m_pAlignCellStartY;
	SCAN_3DPara		*m_pScan3DParam;
	SCAN_3DResult   *m_pScan3DResult;
	ObjDistance			*m_pCamToObjDis;

	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;
	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;


	int m_AreaScanStartShift;
	int m_AreaScanStartDrive;

	int m_NowScanIndex;
	CPoint *m_pAreaScanStartPosList;


	CInterServerInterface  *m_pServerInterface;
};

