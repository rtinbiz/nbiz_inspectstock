#pragma once
#define  LIVE_SIZE_W 1628 // 1626
#define  LIVE_SIZE_H  1236

#define  IMG_PIXEL_SIZE_W  5//um
#define  IMG_PIXEL_SIZE_H  5//um

#define  LIVE_IMG_SIZE ((LIVE_SIZE_W-2)*LIVE_SIZE_H)*1

#define CAM_LIVE_IMG_MAP_NAME		"LIVE_IMG"

enum
{
	LIVE_MODE_NONE = 0,
	LIVE_MODE_VIEW = 1,
	LIVE_MODE_AF,
	LIVE_MODE_GRAB,
};

#define PI_V 3.141592
#define PI_ATR (PI_V /180.0)
#define PI_RTA ( 180.0/PI_V)
#define AngleTORadian(Angle) (Angle * PI_ATR )
#define RadianTOAngle(radian) (radian * PI_RTA)

#define CamROIStartPos ((LIVE_SIZE_W/2) - (LIVE_SIZE_H/2))

#pragma pack(1)
typedef struct SMP_LIVE_TAG
{
	char LiveImage[LIVE_IMG_SIZE];
	int LiveGrabCamsRun;
	int UpDateMode;
	int LiveCheck;
}SMP_LIVE;



#pragma pack()

typedef struct AngleCheckData_TAG
{
	double AreaPointS[4];
	double AreaPointE[4];
	double ImgSumValue[2];
	AngleCheckData_TAG()
	{
		memset(this, 0x00, sizeof(AngleCheckData_TAG));
	}
}AngleCheckD;

typedef struct GET_LIVE_THREAD_TAG
{
	bool LiveGrabCamsRun;
	CWinThread *pLiveThread;
	SMP_LIVE *pLiveMem;
	//////////////////////////////////////////////////////////////////////////
	HDC LiveViewCam_HDC;
	CRect LiveViewSize;
	IplImage *pGrabImg;
	IplImage *pAlignProcImg;

	GET_LIVE_THREAD_TAG()
	{
		LiveGrabCamsRun				= false;
		pLiveThread					= nullptr;
		pLiveMem						= nullptr;

		LiveViewCam_HDC			= nullptr;

		pGrabImg						= nullptr;
		pAlignProcImg					= nullptr;
		memset(&LiveViewSize, 0x00, sizeof(CRect));
	}
}GET_LIVE_THREAD;


typedef struct PinContectInfo_TAG
{
	bool	 bUseNowData;
	double DispAngle;
	long CalcAngleOffset;
	double CalcPosOffsetX;
	double CalcPosOffsetY;
	

	PinContectInfo_TAG()
	{
		memset(this, 0x00, sizeof(PinContectInfo_TAG));
	}
}DispPinJetPosInfo;

typedef struct ALIGN_RET_TAG
{
	CvPoint CenterXY;//Image상의 Align Center
	double DispPinAngleOffset;//Disp 용 위치 Offset값이 저장된다.
	double ConePinAngleOffset;//Cone의 회전 Offset이 저장된다.
	long ConePinPulseOffset;//회전 Offset이 Motion Pulse값으로 환산 저장 된다.

	CvPoint AxisMoveOffsetXY;//Center에 대한 Offset이 저장 된다.
	

	DispPinJetPosInfo *pDispFindPos;
	ALIGN_RET_TAG()
	{
		CenterXY.x = -1000;
		CenterXY.y = -1000;

		DispPinAngleOffset = 0.0;
		ConePinAngleOffset = 0.0;

		AxisMoveOffsetXY.x = 0;
		AxisMoveOffsetXY.y = 0;
		ConePinPulseOffset = 0;

		pDispFindPos = nullptr;
	}
	~ALIGN_RET_TAG()
	{
		CenterXY.x = -1000;
		CenterXY.y = -1000;
		DispPinAngleOffset = 0.0;
		ConePinAngleOffset = 0.0;

		AxisMoveOffsetXY.x = 0;
		AxisMoveOffsetXY.y = 0;
		ConePinPulseOffset= 0;

		if(pDispFindPos != nullptr)
			delete [] pDispFindPos;
		pDispFindPos = nullptr;
	}
	void Reset()
	{
		CenterXY.x = -1000;
		CenterXY.y = -1000;
		DispPinAngleOffset = 0.0;
		ConePinAngleOffset = 0.0;

		AxisMoveOffsetXY.x = 0;
		AxisMoveOffsetXY.y = 0;
		ConePinPulseOffset= 0;

		if(pDispFindPos != nullptr)
			delete [] pDispFindPos;
		pDispFindPos = nullptr;
	}
}ALIGN_RET;

class CLiveGrab
{
public:
	CLiveGrab(void);
	~CLiveGrab(void);
private:
	HANDLE hLIMapping;
	HANDLE hMapRead;
	SMP_LIVE *m_pImgData;
public:
	GET_LIVE_THREAD m_ProcLiveThread;

public:
	bool RunLiveProgram();
	bool RunAFTask();
	bool InitLiveThread();//HDC ViewCam1_HDC, CRect ViewCam1Size, HDC ViewCam2_HDC, CRect ViewCam2Size);
	void SetCamLiveMode(int LiveMode);
	int GetCamLiveMode();

	IplImage * GetGrabImg();
	//bool CenterAlignValue(IplImage *pTargetImg, ALIGN_RET *pRetAlign, IplImage *pAlignResult = nullptr);

	bool GetCenterAlignValue(ALIGN_RET *pRetAlign, IplImage *pAlignResult = nullptr);
	IplImage *m_pCam1Live;


	bool SetLiveDisplayObj(HDC ViewCam_HDC, CRect ViewCamSize);
public:
	void RestGrabProcess();

};
UINT thfn_LiveThread(LPVOID pParam);