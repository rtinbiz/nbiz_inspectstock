
// VS11IndexerDlg.h : 헤더 파일
//

#pragma once

#include "afxwin.h"




// CVS11IndexerDlg 대화 상자
class CVS11IndexerDlg : public CDialogEx, public CProcInitial//VS64 Interface
{
// 생성입니다.
public:
	CVS11IndexerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~CVS11IndexerDlg();	
	CInterServerInterface  m_cpServerInterface;
	
// 대화 상자 데이터입니다.
	enum { IDD = IDD_VS11INDEXER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnNcDestroy();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	
	void m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...);
	void Sys_fnInitVS64Interface();
	int				Sys_fnAnalyzeMsg(CMDMSG* cmdMsg);//Message Process Function.
	LRESULT		Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam);
	LRESULT		Sys_fnBarcodeCallback(WPARAM wParam, LPARAM lParam);
				
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	void m_fnAppendList(CString strLog, LOG_INFO nColorType);
	afx_msg void OnBnClickedOk();
	void m_fnCreateButton(void);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	int m_fnReadSystemFile(void);
		
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	void m_fnTaskStatusChage(int nTaskNumber, TASK_State elTempStatus);
		
	int m_fnThreadCreator();		

public:
	IplImage*	m_iplBackGround;
	IplImage*	m_iplBackGroundOrg;

	COLORREF	m_btBackColor;
	HDC			m_HDCView;
	CRect			m_RectiewArea;
	
	CXListBox m_ctrListLogView;
	
	CRoundButton2 m_ctrBtnExit;	
	CRoundButton2 m_ctrBtnLotStart;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;
	CRoundButtonStyle	m_tStyle2;
	CRoundButtonStyle	m_tStyle3;
	
	CRoundButton2 m_btMeasure3DInit;
	CRoundButton2 m_btStartMeasure3D;
	CRoundButton2 m_bt3DLensSelect[4];
	
		
	CRoundButton2 m_ctrBtnEpInit;
	CRoundButton2 m_ctrBtnEpReset;
	CRoundButton2 m_ctrBtnEpProcStart;
	CRoundButton2 m_ctrBtnEpScanStart;
	CRoundButton2 m_ctrBtnEpScanEnd;
	CRoundButton2 m_ctrBtnEpProcEnd;




	STHREAD_PARAM	m_sThreadParam;
	
	//Task Status 갱신 UI Control
	CColorStaticST m_ctrStaticTaskInfo;
	CColorStaticST m_ctrStaticTask3DMeasure;
	CColorStaticST m_ctrStaticProc3DMeasure;
	CColorStaticST m_stMeasureCnt3D;
	CFont			m_TaskStateFont;
	CFont			m_TaskStateSmallFont;
	COLORREF	m_TaskStateColor;

	//메인 스레드 인터벌.
	static const int	STATUS_DISP_TIMER = 1000;
	static const int	THREAD_INTERVAL = 500;
	static const int	TASK_STATUS_TIMER = THREAD_INTERVAL * 10;
	static const int	TASK_STATUS_DISCONNECT = THREAD_INTERVAL * 20;
		
	
	int m_nMasterStatusCount;  //마스터 상태 갱신에 대한 시간 체크
	int m_nMotionStatusCount; //모션타스크 상태 갱신에 대한 시간 체크
	

	//메인 시퀀스 스텝.
	EL_MAIN_SEQ m_elMainSequence;
		
	
	afx_msg void OnBnClickedBtMeasureStart();
	afx_msg void OnBnClickedBt3dLens();
	
	afx_msg void OnBnClickedBtMeasureInit();
	
	afx_msg void OnBnClickedBtnImageSave();

	void m_fnSet_CDTP_Data(int nWidth, int nHeight, int nPosX, int nPosY);
	afx_msg void OnBnClickedBtnLotStart();
	
};

extern CVS11IndexerDlg*				g_cpMainDlg;
extern CInterServerInterface*		g_cpServerInterface;