#include "StdAfx.h"
#include "Seq_CDTP.h"

#define ALL_LENGTH 1
#define BLACK_LENGTH 2
#define WHITE_LENGTH 3


double TwoPoint_Distance(CvPoint StartPt, CvPoint EndPt)
{
	double dXLength = (double)(EndPt.x - StartPt.x);
	double dYLength = (double)(EndPt.y - StartPt.y);
	return sqrt((dXLength * dXLength) + (dYLength * dYLength));
}

CSeq_CDTP::CSeq_CDTP(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;

	m_RecvCMD_Ret = -1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;
	m_pScopePos = nullptr;
	m_pReviewPos = nullptr;

	m_pReviewLensOffset = nullptr;
	m_pLiveViewObj = nullptr;
	m_pCDTPParam = nullptr;
	m_pCDTPPointCnt = nullptr;
	m_ppCDTPPointList = nullptr;
	m_pTP_Result = nullptr;
	
	m_pNowGrabImg = nullptr;
	m_pProcGrayImgBuf = nullptr;
	m_CDTPMeasureIndex =0;

	m_bCD_FindOk = false;

	m_pGlassReicpeParam = nullptr;

	m_pTp_GrayBuf = nullptr;
	m_pTp_GrayBufCopy = nullptr;
	m_pTp_GrayAlign = nullptr;
	m_pMatchBuffer = nullptr;
}


CSeq_CDTP::~CSeq_CDTP(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;

	if(m_pNowGrabImg != nullptr)
		cvReleaseImage(&m_pNowGrabImg);
	m_pNowGrabImg = nullptr;

	if(m_pProcGrayImgBuf != nullptr)
		cvReleaseImage(&m_pProcGrayImgBuf);
	m_pProcGrayImgBuf = nullptr;

	if(m_pTp_GrayAlign != nullptr)
		cvReleaseImage(&m_pTp_GrayAlign);
	m_pTp_GrayAlign = nullptr;

	if(m_pMatchBuffer != nullptr)
		cvReleaseImage(&m_pMatchBuffer);
	m_pMatchBuffer = nullptr;
	
	if(m_pTp_GrayBuf != nullptr)
		cvReleaseImage(&m_pTp_GrayBuf);
	m_pTp_GrayBuf = nullptr;

	if(m_pTp_GrayBufCopy != nullptr)
		cvReleaseImage(&m_pTp_GrayBufCopy);
	m_pTp_GrayBufCopy = nullptr;
	
}

bool CSeq_CDTP::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_CDTP::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();
	G_WriteInfo(Log_Error, "CSeq_CDTP Error Step Run( Z Axis Go to a safe position)");

	m_pLiveViewObj->DrawCDTPReviewInfo(false);
	return 0;
}
void CSeq_CDTP::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}

double CSeq_CDTP::m_fnGetLengthData(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType)
{
	if((abs(nStart.x - nEnd.x) - abs(nStart.y - nEnd.y)) >= 0)
	{
		return m_fnGetLengthDataLongX(gray_image, nStart, nEnd, nType);
	}
	else
	{
		return m_fnGetLengthDataLongY(gray_image, nStart, nEnd, nType);
	}
}
double CSeq_CDTP::m_fnGetLengthDataLongX(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType)
{
	CvPoint nPoint1; CvPoint nPoint2; CvPoint nTemp;

	if(nStart.x > nEnd.x)
	{
		nTemp = nStart;
		nStart = nEnd;
		nEnd = nTemp;
	}

	if((nEnd.x - nStart.x) == 0)
		return 0;

	double dTemp1 = (double)(nEnd.y - nStart.y) / (double)(nEnd.x - nStart.x);

	int nStepY = 0;
	double dValue = 0.0;

	for (int nStepX = nStart.x; nStepX < nEnd.x; nStepX++)
	{
		nStepY = (int)(nStart.y + (nStepX - nStart.x) * dTemp1);
		dValue = cvGetReal2D(gray_image, nStepY, nStepX);
		nPoint1.x = nStepX; nPoint1.y = nStepY;

		if ((int)dValue == 0)
		{
			//cvCircle(rgb_image, nPoint1, 10, CV_RGB(0, 255, 0), 2);
			break;
		}
	}

	for (int nStepX = nEnd.x; nStepX > nStart.x; nStepX--)
	{
		nStepY = (int)(nStart.y + (nStepX - nStart.x) * dTemp1);
		dValue = cvGetReal2D(gray_image, nStepY, nStepX);
		nPoint2.x = nStepX; nPoint2.y = nStepY;

		if ((int)dValue == 0)
		{
			//cvCircle(rgb_image, nPoint2, 10, CV_RGB(0, 255, 0), 2);
			break;
		}
	}

	double dXLength = (double)(nEnd.x - nStart.x);
	double dYLength = (double)(nEnd.y - nStart.y);
	double dLengthAll = sqrt(dXLength * dXLength + dYLength * dYLength);

	if (nType == ALL_LENGTH)
		return dLengthAll;

	dXLength = (double)(nPoint2.x - nPoint1.x);
	dYLength = (double)(nPoint2.y - nPoint1.y);
	double dLengthBlack = sqrt(dXLength * dXLength + dYLength * dYLength);

	if (nType == BLACK_LENGTH)
		return dLengthBlack;

	double dLengthWhite = dLengthAll - dLengthBlack;

	if (nType == WHITE_LENGTH)
		return dLengthWhite;

	return dLengthBlack;
}

double CSeq_CDTP::m_fnGetLengthDataLongY(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType)
{
	CvPoint nPoint1; CvPoint nPoint2; CvPoint nTemp;

	if(nStart.y > nEnd.y)
	{
		nTemp = nStart;
		nStart = nEnd;
		nEnd = nTemp;
	}

	if((nEnd.y - nStart.y) == 0)
		return 0;

	double dTemp1 = (double)(nEnd.x - nStart.x) / (double)(nEnd.y - nStart.y);

	int nStepX = 0;
	double dValue = 0.0;

	for (int nStepY = nStart.y; nStepY < nEnd.y; nStepY++)
	{
		nStepX = (int)(nStart.x + (nStepY - nStart.y) * dTemp1);
		dValue = cvGetReal2D(gray_image, nStepY, nStepX);
		nPoint1.y = nStepY; nPoint1.x = nStepX;

		if ((int)dValue == 0)
		{
			//cvCircle(rgb_image, nPoint1, 10, CV_RGB(0, 255, 0), 2);
			break;
		}
	}

	for (int nStepY = nEnd.y; nStepY > nStart.y; nStepY--)
	{
		nStepX = (int)(nStart.x + (nStepY - nStart.y) * dTemp1);
		dValue = cvGetReal2D(gray_image, nStepY, nStepX);
		nPoint2.y = nStepY; nPoint2.x = nStepX;

		if ((int)dValue == 0)
		{
			//cvCircle(rgb_image, nPoint2, 10, CV_RGB(0, 255, 0), 2);
			break;
		}
	}

	double dXLength = (double)(nEnd.x - nStart.x);
	double dYLength = (double)(nEnd.y - nStart.y);
	double dLengthAll = sqrt(dXLength * dXLength + dYLength * dYLength);

	if (nType == ALL_LENGTH)
		return dLengthAll;

	dXLength = (double)(nPoint2.x - nPoint1.x);
	dYLength = (double)(nPoint2.y - nPoint1.y);
	double dLengthBlack = sqrt(dXLength * dXLength + dYLength * dYLength);

	if (nType == BLACK_LENGTH)
		return dLengthBlack;

	double dLengthWhite = dLengthAll - dLengthBlack;

	if (nType == WHITE_LENGTH)
		return dLengthWhite;

	return dLengthBlack;
}



CvRect CSeq_CDTP::CDTPImgProc(CD_TPParam *pCDTPParam, CDTP_Point*pNowPos, IplImage *pTarget, CvPoint *pEdgeCell, CvRect *pFindIndexRib_LR , CvRect *pFindIndexRib_UD)
{
#ifdef SIMUL_CDTP_CALC_VIEW
	DWORD ProcTime = ::GetTickCount();
#endif
	CvRect retValue;
	retValue.x = -1;
	retValue.y = -1;
	retValue.width = -1;
	retValue.height = -1;
	if(pTarget == nullptr )
	return retValue;

	if(m_pProcGrayImgBuf == nullptr)
		m_pProcGrayImgBuf = cvCreateImage(cvSize(pTarget->width, pTarget->height), IPL_DEPTH_8U, 1);

	//cvZero(m_pProcGrayImgBuf);

	cvCvtColor(pTarget, m_pProcGrayImgBuf, CV_RGB2GRAY);
	cvThreshold(m_pProcGrayImgBuf, m_pProcGrayImgBuf, pCDTPParam->nLight_Threshold, 255, CV_THRESH_BINARY); 

	//CString strNewName;
	//strNewName.Format("D:\\CD_%d.bmp", GetTickCount());
	//cvSaveImage(strNewName.GetBuffer(strNewName.GetLength()),m_pProcGrayImgBuf);

	CBlobLabeling blob;
	//영상 노이즈 제거.
	cvErode(m_pProcGrayImgBuf, m_pProcGrayImgBuf, NULL, 2);
	cvDilate(m_pProcGrayImgBuf, m_pProcGrayImgBuf, NULL, 2);
//	blob.SetParam(m_pProcGrayImgBuf,50, 100000 );
	blob.SetParam(m_pProcGrayImgBuf,1 );
	blob.DoLabeling();
	int AvrCnt = 0;
	double AvrWidth = 0.0;
	double AvrHieght = 0.0;
	if(blob.m_nBlobs==0)
		return retValue;

	for(int i = 0; i<blob.m_nBlobs; i++)
	{
		if((blob.m_recBlobs[i].x < 10) ||
		(blob.m_recBlobs[i].y < 10) ||
		(blob.m_recBlobs[i].x+blob.m_recBlobs[i].width > m_pProcGrayImgBuf->width-10)||
		(blob.m_recBlobs[i].y+blob.m_recBlobs[i].height > m_pProcGrayImgBuf->height-10))
			continue;
		AvrCnt++;
		AvrWidth += (double)blob.m_recBlobs[i].width;
		AvrHieght += (double)blob.m_recBlobs[i].height;
	}
	AvrWidth = AvrWidth/(double)AvrCnt;
	AvrHieght=AvrHieght/(double)AvrCnt;
	//blob.BlobSmallSizeConstraint((int)AvrWidth, (int)AvrHieght);
	int FindIndex =  -1;
	int FindIndexRib_LR =  -1;
	int FindIndexRib_UD =  -1;
	switch(pNowPos->nPosType )
	{
	case CELL_LeftTop:
		FindIndex = Find_CELL_LeftTop(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X, pCDTPParam->nEdge_Pos_Count_Y, AvrWidth, AvrHieght, pEdgeCell);
		if(pCDTPParam->nMeasure_Direction == 1)//수직
		{
			FindIndexRib_LR = Find_CELL_LeftTop(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X+1, pCDTPParam->nEdge_Pos_Count_Y, AvrWidth, AvrHieght, pEdgeCell);
			FindIndexRib_UD = Find_CELL_LeftTop(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X, pCDTPParam->nEdge_Pos_Count_Y+1, AvrWidth, AvrHieght, pEdgeCell);
		}
		else//2:대각
		{
			CvPoint FindCrossPt;
			FindCrossPt = Find_CELL_Left_Cross(pTarget, FindIndex, blob.m_nBlobs, blob.m_recBlobs, AvrWidth, AvrHieght);
			FindIndexRib_LR = FindCrossPt.x; 
			FindIndexRib_UD = FindCrossPt.y; 
		}
		break;
	case CELL_RightTop:
		FindIndex = Find_CELL_RightTop(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X, pCDTPParam->nEdge_Pos_Count_Y, AvrWidth, AvrHieght, pEdgeCell);
		if(pCDTPParam->nMeasure_Direction == 1)//수직
		{
			FindIndexRib_LR = Find_CELL_RightTop(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X+1, pCDTPParam->nEdge_Pos_Count_Y, AvrWidth, AvrHieght, pEdgeCell);
			FindIndexRib_UD = Find_CELL_RightTop(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X, pCDTPParam->nEdge_Pos_Count_Y+1, AvrWidth, AvrHieght, pEdgeCell);
		}
		else//2:대각
		{
			CvPoint FindCrossPt;
			FindCrossPt = Find_CELL_Right_Cross(pTarget, FindIndex, blob.m_nBlobs, blob.m_recBlobs, AvrWidth, AvrHieght);
			FindIndexRib_LR = FindCrossPt.x; 
			FindIndexRib_UD = FindCrossPt.y; 
		}
		break;
	case CELL_RightBottom:
		FindIndex = Find_CELL_RightBottom(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X, pCDTPParam->nEdge_Pos_Count_Y, AvrWidth, AvrHieght, pEdgeCell);
		if(pCDTPParam->nMeasure_Direction == 1)//수직
		{
			FindIndexRib_LR = Find_CELL_RightBottom(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X+1, pCDTPParam->nEdge_Pos_Count_Y, AvrWidth, AvrHieght, pEdgeCell);
			FindIndexRib_UD = Find_CELL_RightBottom(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X, pCDTPParam->nEdge_Pos_Count_Y+1, AvrWidth, AvrHieght, pEdgeCell);
		}
		else//2:대각
		{
			CvPoint FindCrossPt;
			FindCrossPt = Find_CELL_Right_Cross(pTarget, FindIndex, blob.m_nBlobs, blob.m_recBlobs, AvrWidth, AvrHieght);
			FindIndexRib_LR = FindCrossPt.x; 
			FindIndexRib_UD = FindCrossPt.y; 
		}
		break;
	case CELL_LeftBottom:
		FindIndex = Find_CELL_LeftBottom(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X, pCDTPParam->nEdge_Pos_Count_Y, AvrWidth, AvrHieght, pEdgeCell);
		if(pCDTPParam->nMeasure_Direction == 1)//수직
		{
			FindIndexRib_LR = Find_CELL_LeftBottom(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X+1, pCDTPParam->nEdge_Pos_Count_Y, AvrWidth, AvrHieght, pEdgeCell);
			FindIndexRib_UD = Find_CELL_LeftBottom(pTarget, blob.m_nBlobs, blob.m_recBlobs, pCDTPParam->nEdge_Pos_Count_X, pCDTPParam->nEdge_Pos_Count_Y+1, AvrWidth, AvrHieght, pEdgeCell);
		}
		else//2:대각
		{
			CvPoint FindCrossPt;
			FindCrossPt = Find_CELL_Left_Cross(pTarget, FindIndex, blob.m_nBlobs, blob.m_recBlobs, AvrWidth, AvrHieght);
			FindIndexRib_LR = FindCrossPt.x; 
			FindIndexRib_UD = FindCrossPt.y; 
		}
		break;
	case CELL_Center:
		//FindIndex = Find_CELL_Center(pTarget, blob.m_nBlobs, blob.m_recBlobs, AvrWidth, AvrHieght);
		break;
	}
	if(FindIndex>=0)
	{
#ifdef SIMUL_CDTP_CALC_VIEW
		cvDrawRect(pTarget, cvPoint(blob.m_recBlobs[FindIndex].x , blob.m_recBlobs[FindIndex].y), 
		cvPoint(blob.m_recBlobs[FindIndex].x+blob.m_recBlobs[FindIndex].width , blob.m_recBlobs[FindIndex].y+blob.m_recBlobs[FindIndex].height), CV_RGB(255,0,0), 2);
#endif

		retValue = blob.m_recBlobs[FindIndex];
		if(FindIndexRib_LR>=0 && pFindIndexRib_LR != nullptr)
		{
			*pFindIndexRib_LR= blob.m_recBlobs[FindIndexRib_LR];
		}
		if(FindIndexRib_UD>=0 && pFindIndexRib_LR != nullptr)
		{
			*pFindIndexRib_UD= blob.m_recBlobs[FindIndexRib_UD];
		}
	}
	else
		return retValue;
#ifdef SIMUL_CDTP_CALC_VIEW
	ProcTime = ::GetTickCount()-ProcTime;
	extern CvFont G_cvDrawFont; 
	char G_strProcMsg[128];
	sprintf_s(G_strProcMsg,128,"[%d Tick]",ProcTime);
	cvPutText(pTarget, G_strProcMsg,  cvPoint(blob.m_recBlobs[FindIndex].x , blob.m_recBlobs[FindIndex].y), &G_cvDrawFont, CV_RGB(255,0,0));

	//cvNamedWindow("TEST_V", CV_WINDOW_NORMAL);
	//cvShowImage("TEST_V", pTarget);
	//cvWaitKey();
#endif


	return retValue;
}
void CSeq_CDTP::CDTPDrawResult(IplImage *pTarget, CvRect ResultRect, CvRect FindIndexRib_LR , CvRect FindIndexRib_UD, CvPoint EdgeCell, CDTP_Point *pNowIndexPos)
{
	extern CvFont G_cvDrawFont; 
	char strProcMsg[128];

	cvLine(pTarget,	cvPoint(EdgeCell.x ,0), 
		cvPoint(EdgeCell.x,	pTarget->height-1), CV_RGB(0,0,255), 2);
	cvLine(pTarget,	cvPoint(0, EdgeCell.y), 
		cvPoint(pTarget->width-1, EdgeCell.y), CV_RGB(0,0,255), 2);
	
	CvPoint DrawStrPt;
	DrawStrPt.x = 30;
	DrawStrPt.y = 30;

	switch(pNowIndexPos->nPosType)
	{
	case CELL_LeftTop:
		{
			sprintf_s(strProcMsg,128,"CELL_LeftTop");
			cvPutText(pTarget, strProcMsg,  DrawStrPt, &G_cvDrawFont, CV_RGB(0,255,255));
		}break;
	case CELL_RightTop:
		{
			sprintf_s(strProcMsg,128,"CELL_RightTop");
			cvPutText(pTarget, strProcMsg,  DrawStrPt, &G_cvDrawFont, CV_RGB(0,255,255));
		}break;
	case CELL_RightBottom:
		{
			sprintf_s(strProcMsg,128,"CELL_RightBottom");
			cvPutText(pTarget, strProcMsg,  DrawStrPt, &G_cvDrawFont, CV_RGB(0,255,255));
		}break;
	case CELL_LeftBottom:
		{
			sprintf_s(strProcMsg,128,"CELL_LeftBottom");
			cvPutText(pTarget, strProcMsg,  DrawStrPt, &G_cvDrawFont, CV_RGB(0,255,255));
		}break;
	}


	sprintf_s(strProcMsg,128,"Cell Edge[S:%dum , D:%dum]", pNowIndexPos->nCellEdgePointX, pNowIndexPos->nCellEdgePointY);
	cvPutText(pTarget, strProcMsg,  cvPoint(DrawStrPt.x ,DrawStrPt.y+35), &G_cvDrawFont, CV_RGB(255,0,255));
	G_WriteInfo(Log_Exception, strProcMsg);
	
	sprintf_s(strProcMsg,128,"Slit Size [W:%0.3fum, H:%0.3fum]", pNowIndexPos->fSlitSizeW, pNowIndexPos->fSlitSizeH);
	cvPutText(pTarget, strProcMsg,  cvPoint(DrawStrPt.x ,DrawStrPt.y+75), &G_cvDrawFont, CV_RGB(0,255,0));
	G_WriteInfo(Log_Exception, strProcMsg);

	sprintf_s(strProcMsg,128,"Rib Size [W:%0.3fum, H:%0.3fum]", pNowIndexPos->fRibSizeW, pNowIndexPos->fRibSizeH);
	cvPutText(pTarget, strProcMsg,  cvPoint(DrawStrPt.x ,DrawStrPt.y+115), &G_cvDrawFont, CV_RGB(0,0,255));
	G_WriteInfo(Log_Exception, strProcMsg);

	CvPoint CrossPt;
	CrossPt.x =ResultRect.x+ResultRect.width/2;
	CrossPt.y =ResultRect.y+ResultRect.height/2;
	cvLine(pTarget, cvPoint(CrossPt.x-10,CrossPt.y),cvPoint(CrossPt.x+10 ,CrossPt.y), CV_RGB(255,0,0), 2);
	cvLine(pTarget, cvPoint(CrossPt.x ,CrossPt.y-10),cvPoint(CrossPt.x ,CrossPt.y+10), CV_RGB(255,0,0), 2);

	CrossPt.x =FindIndexRib_LR.x+FindIndexRib_LR.width/2;
	CrossPt.y =FindIndexRib_LR.y+FindIndexRib_LR.height/2;
	cvLine(pTarget, cvPoint(CrossPt.x-10,CrossPt.y),cvPoint(CrossPt.x+10 ,CrossPt.y), CV_RGB(0,255,0), 1);
	cvLine(pTarget, cvPoint(CrossPt.x ,CrossPt.y-10),cvPoint(CrossPt.x ,CrossPt.y+10), CV_RGB(0,255,0), 1);

	CrossPt.x =FindIndexRib_UD.x+FindIndexRib_UD.width/2;
	CrossPt.y =FindIndexRib_UD.y+FindIndexRib_UD.height/2;
	cvLine(pTarget, cvPoint(CrossPt.x-10,CrossPt.y),cvPoint(CrossPt.x+10 ,CrossPt.y), CV_RGB(0,255,0), 1);
	cvLine(pTarget, cvPoint(CrossPt.x ,CrossPt.y-10),cvPoint(CrossPt.x ,CrossPt.y+10), CV_RGB(0,255,0), 1);


	//cvDrawRect(pTarget, cvPoint(ResultRect.x ,ResultRect.y), cvPoint(ResultRect.x+ResultRect.width , ResultRect.y+ResultRect.height), CV_RGB(255,0,0), 2);
	//cvDrawRect(pTarget, cvPoint(FindIndexRib_LR.x ,FindIndexRib_LR.y), cvPoint(FindIndexRib_LR.x+FindIndexRib_LR.width , FindIndexRib_LR.y+FindIndexRib_LR.height), CV_RGB(0,255,0), 1);
	//cvDrawRect(pTarget, cvPoint(FindIndexRib_UD.x ,FindIndexRib_UD.y), cvPoint(FindIndexRib_UD.x+FindIndexRib_UD.width , FindIndexRib_UD.y+FindIndexRib_UD.height), CV_RGB(0,255,0), 1);
	//switch(pNowIndexPos->nPosType)
	//{
	//case CELL_LeftTop:
	//	{
	//		cvLine(pTarget,	cvPoint(ResultRect.x-10 , ResultRect.y), 
	//			cvPoint(ResultRect.x-10, ResultRect.y+ResultRect.height), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x-5 , ResultRect.y), 
	//			cvPoint(ResultRect.x-15, ResultRect.y), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x-5 , ResultRect.y+ResultRect.height), 
	//			cvPoint(ResultRect.x-15, ResultRect.y+ResultRect.height), CV_RGB(255,0,255), 2);
	//		sprintf_s(strProcMsg,128,"[%0.3fum / %0.3fum]", pNowIndexPos->fSlitSizeH, pNowIndexPos->fRibSizeH);
	//		cvPutText(pTarget, strProcMsg,  cvPoint(ResultRect.x-ResultRect.width , ResultRect.y+ResultRect.height/2), &G_cvDrawSmallFont, CV_RGB(255,0,0));

	//		cvLine(pTarget,	cvPoint(ResultRect.x ,							ResultRect.y-10), 
	//								cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y-10), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x,							ResultRect.y-5), 
	//								cvPoint(ResultRect.x,							ResultRect.y-15), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y-5), 
	//								cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y-15), CV_RGB(255,0,255), 2);
	//		sprintf_s(strProcMsg,128,"[%0.3fum / %0.3fum]", pNowIndexPos->fSlitSizeW, pNowIndexPos->fRibSizeW);
	//		cvPutText(pTarget, strProcMsg,  cvPoint(ResultRect.x+10 , ResultRect.y-20), &G_cvDrawSmallFont, CV_RGB(255,0,0));
	//	}break;
	//case CELL_RightTop:
	//	{
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width+10 , ResultRect.y), 
	//			cvPoint(ResultRect.x+ResultRect.width+10, ResultRect.y+ResultRect.height), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width+5 , ResultRect.y), 
	//			cvPoint(ResultRect.x+ResultRect.width+15, ResultRect.y), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width+5 , ResultRect.y+ResultRect.height), 
	//			cvPoint(ResultRect.x+ResultRect.width+15, ResultRect.y+ResultRect.height), CV_RGB(255,0,255), 2);
	//		sprintf_s(strProcMsg,128,"[%0.3fum / %0.3fum]", pNowIndexPos->fSlitSizeH, pNowIndexPos->fRibSizeH);
	//		cvPutText(pTarget, strProcMsg,  cvPoint(ResultRect.x+ResultRect.width+10 , ResultRect.y+ResultRect.height/2), &G_cvDrawSmallFont, CV_RGB(255,0,0));

	//		cvLine(pTarget,	cvPoint(ResultRect.x ,							ResultRect.y-10), 
	//			cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y-10), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x,							ResultRect.y-5), 
	//			cvPoint(ResultRect.x,							ResultRect.y-15), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y-5), 
	//			cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y-15), CV_RGB(255,0,255), 2);
	//		sprintf_s(strProcMsg,128,"[%0.3fum / %0.3fum]", pNowIndexPos->fSlitSizeW, pNowIndexPos->fRibSizeW);
	//		cvPutText(pTarget, strProcMsg,  cvPoint(ResultRect.x+10 , ResultRect.y-20), &G_cvDrawSmallFont, CV_RGB(255,0,0));
	//	}break;
	//case CELL_RightBottom:
	//	{
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width+10 , ResultRect.y), 
	//			cvPoint(ResultRect.x+ResultRect.width+10, ResultRect.y+ResultRect.height), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width+5 , ResultRect.y), 
	//			cvPoint(ResultRect.x+ResultRect.width+15, ResultRect.y), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width+5 , ResultRect.y+ResultRect.height), 
	//			cvPoint(ResultRect.x+ResultRect.width+15, ResultRect.y+ResultRect.height), CV_RGB(255,0,255), 2);
	//		sprintf_s(strProcMsg,128,"[%0.3fum / %0.3fum]", pNowIndexPos->fSlitSizeH, pNowIndexPos->fRibSizeH);
	//		cvPutText(pTarget, strProcMsg,  cvPoint(ResultRect.x+ResultRect.width+10 , ResultRect.y+ResultRect.height/2), &G_cvDrawSmallFont, CV_RGB(255,0,0));

	//		cvLine(pTarget,	cvPoint(ResultRect.x ,							ResultRect.y+ResultRect.height+10), 
	//			cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y+ResultRect.height+10), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x,							ResultRect.y+ResultRect.height+5), 
	//			cvPoint(ResultRect.x,							ResultRect.y+ResultRect.height+15), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y+ResultRect.height+5), 
	//			cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y+ResultRect.height+15), CV_RGB(255,0,255), 2);
	//		sprintf_s(strProcMsg,128,"[%0.3fum / %0.3fum]", pNowIndexPos->fSlitSizeW, pNowIndexPos->fRibSizeW);
	//		cvPutText(pTarget, strProcMsg,  cvPoint(ResultRect.x , ResultRect.y+ResultRect.height+30), &G_cvDrawSmallFont, CV_RGB(255,0,0));
	//	}break;
	//case CELL_LeftBottom:
	//	{
	//		cvLine(pTarget,	cvPoint(ResultRect.x-10 , ResultRect.y), 
	//			cvPoint(ResultRect.x-10, ResultRect.y+ResultRect.height), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x-5 , ResultRect.y), 
	//			cvPoint(ResultRect.x-15, ResultRect.y), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x-5 , ResultRect.y+ResultRect.height), 
	//			cvPoint(ResultRect.x-15, ResultRect.y+ResultRect.height), CV_RGB(255,0,255), 2);
	//		sprintf_s(strProcMsg,128,"[%0.3fum / %0.3fum]", pNowIndexPos->fSlitSizeH, pNowIndexPos->fRibSizeH);
	//		cvPutText(pTarget, strProcMsg,  cvPoint(ResultRect.x-ResultRect.width , ResultRect.y+ResultRect.height/2), &G_cvDrawSmallFont, CV_RGB(255,0,0));

	//		cvLine(pTarget,	cvPoint(ResultRect.x ,							ResultRect.y+ResultRect.height+10), 
	//			cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y+ResultRect.height+10), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x,							ResultRect.y+ResultRect.height+5), 
	//			cvPoint(ResultRect.x,							ResultRect.y+ResultRect.height+15), CV_RGB(255,0,255), 2);
	//		cvLine(pTarget,	cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y+ResultRect.height+5), 
	//			cvPoint(ResultRect.x+ResultRect.width,	ResultRect.y+ResultRect.height+15), CV_RGB(255,0,255), 2);
	//		sprintf_s(strProcMsg,128,"[%0.3fum / %0.3fum]", pNowIndexPos->fSlitSizeW, pNowIndexPos->fRibSizeW);
	//		cvPutText(pTarget, strProcMsg,  cvPoint(ResultRect.x , ResultRect.y+ResultRect.height+30), &G_cvDrawSmallFont, CV_RGB(255,0,0));
	//	}break;
	//}
}
int CSeq_CDTP::Find_CELL_LeftTop( IplImage *pTarget, int Cnt, CvRect *pBuf, int FCntX, int FCntY, double avrW, double avrH, CvPoint *pEdgeCell)
{
	int FindIndex = -1;
	CvRect FindRectObj_X;
	CvRect FindRectObj_Y;
	memset(&FindRectObj_X,0x00, sizeof(CvRect));
	memset(&FindRectObj_Y,0x00, sizeof(CvRect));
	int MinPosX = pTarget->width;//pBuf[0].x+pBuf[0].width;
	int MinPosY = pTarget->height;//pBuf[0].y+pBuf[0].height;
	int Minbuf = 0;

	//double MinW= avrW*0.8;
	//double MaxW= avrW*1.2;
	//double MinH= avrH*0.8;
	//double MaxH= avrH*1.2;
	double MinW= avrW*0.5;
	double MaxW= avrW*1.2;
	double MinH= avrH*0.5;
	double MaxH= avrH*1.2;
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
	//		(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
	//	{
	//		cvDrawRect(pTarget, cvPoint(pBuf[FStep].x , pBuf[FStep].y), 
	//			cvPoint(pBuf[FStep].x+pBuf[FStep].width , pBuf[FStep].y+pBuf[FStep].height), CV_RGB(0,0,0), 2);
	//	}
	//}


	//기준점 찾기.
	//X방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MinPosX > pBuf[FStep].x  )
			{
				MinPosX = pBuf[FStep].x;
				FindRectObj_X = pBuf[FStep];
			}
		}
	}
	//X방향 제일 작은것중에 Y방향 제일 작은것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{
				if( FindRectObj_X.y > pBuf[FStep].y  )
				{
					FindRectObj_X = pBuf[FStep];
				}
			}
		}
	}

	int YDisMin= pTarget->height;
	int YDisBuf = 0;
	CvRect StpeRectObj_X;
	memset(&StpeRectObj_X,0x00, sizeof(CvRect));
	//X방향 제일 작은것중에 Y방향 제일 작은것.에서 가장 가까운것.

	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{
				if( FindRectObj_X.y != pBuf[FStep].y  )//자기 자신을 빼기위해.
				{
					YDisBuf = abs(FindRectObj_X.y - pBuf[FStep].y);
					if(YDisMin>YDisBuf)
					{
						YDisMin=YDisBuf;
						StpeRectObj_X = pBuf[FStep];
					}
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,0,255), 3);
	cvDrawRect(pTarget, cvPoint(StpeRectObj_X.x , StpeRectObj_X.y), 
		cvPoint(StpeRectObj_X.x+StpeRectObj_X.width , StpeRectObj_X.y+StpeRectObj_X.height), CV_RGB(0,255,255), 1);
#endif
	if(FCntY>2)
	{
		int DisFindY= FindRectObj_X.y +(abs(FindRectObj_X.y - StpeRectObj_X.y)*(FCntY-1));
		//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
					(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
				{
					if((DisFindY > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
						(DisFindY < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.
					{
						FindRectObj_X=pBuf[FStep];
					}

				}
			}
		}
	}
	else if(FCntY==2)
	{
		FindRectObj_X = StpeRectObj_X;
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,255,255), 5);
#endif
	
	//Y방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MinPosY > pBuf[FStep].y )
			{
				MinPosY = pBuf[FStep].y;
				FindRectObj_Y = pBuf[FStep];
			}
		}
	}
	//Y방향 제일 작은것중에 X방향 제일 작은것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{

				if( FindRectObj_Y.x > pBuf[FStep].x  )
				{
					FindRectObj_Y = pBuf[FStep];
				}
			}
		}
	}

	int XDisMin= pTarget->width;
	int XDisBuf = 0;
	CvRect StpeRectObj_Y;
	memset(&StpeRectObj_Y,0x00, sizeof(CvRect));
	//Y방향 제일 작은것중에 X방향 제일 작은것.에서 가장 가까운것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{
				if( FindRectObj_Y.x != pBuf[FStep].x  )//자기 자신을 빼기위해.
				{
					XDisBuf = abs(FindRectObj_Y.x - pBuf[FStep].x);
					if(XDisMin>XDisBuf)
					{
						XDisMin=XDisBuf;
						StpeRectObj_Y = pBuf[FStep];
					}
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,0), 1);
	cvDrawRect(pTarget, cvPoint(StpeRectObj_Y.x , StpeRectObj_Y.y), 
		cvPoint(StpeRectObj_Y.x+StpeRectObj_Y.width , StpeRectObj_Y.y+StpeRectObj_Y.height), CV_RGB(0,255,0), 1);
#endif
	if(FCntX>2)
	{
		int DisFindX= FindRectObj_Y.x +(abs(FindRectObj_Y.x - StpeRectObj_Y.x)*(FCntX-1));
		//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
					(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
				{
					if((DisFindX > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
						(DisFindX < (pBuf[FStep].x + pBuf[FStep].width/2 )))//같은 선상에서.
					{
						FindRectObj_Y=pBuf[FStep];
					}
				}
			}
		}
	}
	else if(FCntX==2)
	{
		FindRectObj_Y = StpeRectObj_Y;
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,255), 5);
#endif
	//Cell Center
	//cvLine(pTarget, cvPoint(0 , FindRectObj_Y.y), 
	//			cvPoint(pTarget->width-1, FindRectObj_Y.y), CV_RGB(255,0,255), 1);
	//cvLine(pTarget, cvPoint(FindRectObj_X.x , 0), 
	//	cvPoint(FindRectObj_X.x, pTarget->height-1), CV_RGB(0,255,0), 1);

	pEdgeCell->x = FindRectObj_X.x;
	pEdgeCell->y = FindRectObj_Y.y;

	CvRect FindRectObj;
	FindRectObj.width =0;
	FindRectObj.height =0;
	FindRectObj.x =FindRectObj_Y.x;
	FindRectObj.y =FindRectObj_X.y;
#ifdef SIMUL_CDTP_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , FindRectObj.y),  cvPoint(pTarget->width-1, FindRectObj.y), CV_RGB(255,255,255), 3);
	cvLine(pTarget, cvPoint(FindRectObj.x , 0),  cvPoint(FindRectObj.x, pTarget->height-1), CV_RGB(255,255,255), 3);
#endif
	FindIndex = -1;
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
				(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
				(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
				(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)
			{
				FindIndex = FStep;
				FindRectObj =pBuf[FStep];
				break;
			}
		}
	}
	//중앙에 발견된것임으로.X축 안쪽으로 이동시켜서 찾는다.
	if(FindIndex<0) 
	{
		//FindRectObj.x += XDisMin/2;
		FindRectObj.y -= YDisMin/2;
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
					(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
					(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
					(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)
				{
					FindIndex =FStep;
					FindRectObj =pBuf[FStep];
					break;
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , FindRectObj_X.y+FindRectObj_X.height/2), 
		cvPoint(pTarget->width-1, FindRectObj_X.y+FindRectObj_X.height/2), CV_RGB(255,0,255), 2);
	cvLine(pTarget, cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2 , 0), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2, pTarget->height-1), CV_RGB(0,255,0), 2);
#endif
	return FindIndex;
}
int CSeq_CDTP::Find_CELL_RightTop(IplImage *pTarget, int Cnt, CvRect *pBuf, int FCntX, int FCntY, double avrW, double avrH, CvPoint *pEdgeCell)
{
	int FindIndex = -1;
	CvRect FindRectObj_X;
	CvRect FindRectObj_Y;
	memset(&FindRectObj_X,0x00, sizeof(CvRect));
	memset(&FindRectObj_Y,0x00, sizeof(CvRect));
	int MaxPosX = 0;//pBuf[0].x+pBuf[0].width;
	int MinPosY =  pTarget->height;//pBuf[0].y+pBuf[0].height;
	int Minbuf = 0;

	double MinW= avrW*0.5;
	double MaxW= avrW*1.2;
	double MinH= avrH*0.5;
	double MaxH= avrH*1.2;

	//기준점 찾기.
	//X방향 제일 큰것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MaxPosX < pBuf[FStep].x  )
			{
				MaxPosX = pBuf[FStep].x;
				FindRectObj_X = pBuf[FStep];
			}
		}
	}
	//X방향 제일 큰것중에 Y방향 제일 작은것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{

				if( FindRectObj_X.y > pBuf[FStep].y  )
				{
					FindRectObj_X = pBuf[FStep];
				}
			}
		}
	}

	int YDisMin= pTarget->height;
	int YDisBuf = 0;
	CvRect StpeRectObj_X;
	memset(&StpeRectObj_X,0x00, sizeof(CvRect));
	//X방향 제일 작은것중에 Y방향 제일 작은것.에서 가장 가까운것.

	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{
				if( FindRectObj_X.y != pBuf[FStep].y  )//자기 자신을 빼기위해.
				{
					YDisBuf = abs(FindRectObj_X.y - pBuf[FStep].y);
					if(YDisMin>YDisBuf)
					{
						YDisMin=YDisBuf;
						StpeRectObj_X = pBuf[FStep];
					}
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,0,255), 1);
	cvDrawRect(pTarget, cvPoint(StpeRectObj_X.x , StpeRectObj_X.y), 
		cvPoint(StpeRectObj_X.x+StpeRectObj_X.width , StpeRectObj_X.y+StpeRectObj_X.height), CV_RGB(0,0,255), 1);
#endif
	if(FCntY>2)
	{
		int DisFindY= FindRectObj_X.y +(abs(FindRectObj_X.y - StpeRectObj_X.y)*(FCntY-1));
		//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
					(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
				{
					if((DisFindY > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
						(DisFindY < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.
					{
						FindRectObj_X=pBuf[FStep];
					}

				}
			}
		}
	}
	else if(FCntY==2)
	{
		FindRectObj_X = StpeRectObj_X;
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,255,255), 2);
#endif


	//Y방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MinPosY > pBuf[FStep].y )
			{
				MinPosY = pBuf[FStep].y;
				FindRectObj_Y = pBuf[FStep];
			}
		}
	}
	//y방향 제일 작은것중에 X방향 제일 큰것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{

				if( FindRectObj_Y.x < pBuf[FStep].x  )
				{
					FindRectObj_Y = pBuf[FStep];
				}
			}
		}
	}

	int XDisMin= pTarget->width;
	int XDisBuf = 0;
	CvRect StpeRectObj_Y;
	memset(&StpeRectObj_Y,0x00, sizeof(CvRect));
	//Y방향 제일 작은것중에 X방향 제일 작은것.에서 가장 가까운것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{
				if( FindRectObj_Y.x != pBuf[FStep].x  )//자기 자신을 빼기위해.
				{
					XDisBuf = abs(FindRectObj_Y.x - pBuf[FStep].x);
					if(XDisMin>XDisBuf)
					{
						XDisMin=XDisBuf;
						StpeRectObj_Y = pBuf[FStep];
					}
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,0), 1);
	cvDrawRect(pTarget, cvPoint(StpeRectObj_Y.x , StpeRectObj_Y.y), 
		cvPoint(StpeRectObj_Y.x+StpeRectObj_Y.width , StpeRectObj_Y.y+StpeRectObj_Y.height), CV_RGB(0,255,0), 1);
#endif
	if(FCntX>2)
	{
		int DisFindX= FindRectObj_Y.x -(abs(FindRectObj_Y.x - StpeRectObj_Y.x)*(FCntX-1));
		//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
					(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
				{
					if((DisFindX > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
						(DisFindX < (pBuf[FStep].x + pBuf[FStep].width/2 )))//같은 선상에서.
					{
						FindRectObj_Y=pBuf[FStep];
					}
				}
			}
		}
	}
	else if(FCntX==2)
	{
		FindRectObj_Y = StpeRectObj_Y;
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,255), 2);
#endif
	//Cell Center
	//cvLine(pTarget, cvPoint(0 , FindRectObj_Y.y), 
	//			cvPoint(pTarget->width-1, FindRectObj_Y.y), CV_RGB(255,0,255), 1);
	//cvLine(pTarget, cvPoint(FindRectObj_X.x , 0), 
	//	cvPoint(FindRectObj_X.x, pTarget->height-1), CV_RGB(0,255,0), 1);
	pEdgeCell->x = FindRectObj_X.x +FindRectObj_X.width;
	pEdgeCell->y = FindRectObj_Y.y;

	CvRect FindRectObj;
	FindRectObj.width =0;
	FindRectObj.height =0;
	FindRectObj.x =FindRectObj_Y.x;
	FindRectObj.y =FindRectObj_X.y;
#ifdef SIMUL_CDTP_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , FindRectObj.y), 
		cvPoint(pTarget->width-1, FindRectObj.y), CV_RGB(255,0,255), 1);
	cvLine(pTarget, cvPoint(FindRectObj.x , 0), 
		cvPoint(FindRectObj.x, pTarget->height-1), CV_RGB(0,255,0), 1);
#endif
	FindIndex = -1;
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
				(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
				(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
				(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)
			{
				FindIndex = FStep;
				FindRectObj =pBuf[FStep];
				break;
			}
		}
	}
	//중앙에 발견된것임으로.X축 안쪽으로 이동시켜서 찾는다.
	if(FindIndex<0) 
	{
		//FindRectObj.x -= XDisMin/2;
		FindRectObj.y -= YDisMin/2;
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
					(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
					(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
					(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)
				{
					FindIndex =FStep;
					FindRectObj =pBuf[FStep];
					break;
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , FindRectObj_X.y+FindRectObj_X.height/2), 
		cvPoint(pTarget->width-1, FindRectObj_X.y+FindRectObj_X.height/2), CV_RGB(255,0,255), 2);
	cvLine(pTarget, cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2 , 0), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2, pTarget->height-1), CV_RGB(0,255,0), 2);
#endif
	return FindIndex;
}
int CSeq_CDTP::Find_CELL_RightBottom(IplImage *pTarget, int Cnt, CvRect *pBuf,int FCntX, int FCntY, double avrW, double avrH, CvPoint *pEdgeCell)
{
	int FindIndex = -1;
	CvRect FindRectObj_X;
	CvRect FindRectObj_Y;
	memset(&FindRectObj_X,0x00, sizeof(CvRect));
	memset(&FindRectObj_Y,0x00, sizeof(CvRect));
	int MaxPosX = 0;//pBuf[0].x+pBuf[0].width;
	int MaxPosY = 0;//pBuf[0].y+pBuf[0].height;
	int Minbuf = 0;

	double MinW= avrW*0.5;
	double MaxW= avrW*1.2;
	double MinH= avrH*0.5;
	double MaxH= avrH*1.2;
	//기준점 찾기.
	//X방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MaxPosX < pBuf[FStep].x  )
			{
				MaxPosX = pBuf[FStep].x;
				FindRectObj_X = pBuf[FStep];
			}
		}
	}
	//X방향 제일 작은것중에 Y방향 제일 큰것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{

				if( FindRectObj_X.y < pBuf[FStep].y  )
				{
					FindRectObj_X = pBuf[FStep];
				}
			}
		}
	}

	int YDisMin= pTarget->height;
	int YDisBuf = 0;
	CvRect StpeRectObj_X;
	memset(&StpeRectObj_X,0x00, sizeof(CvRect));
	//X방향 제일 작은것중에 Y방향 제일 작은것.에서 가장 가까운것.

	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{
				if( FindRectObj_X.y != pBuf[FStep].y  )//자기 자신을 빼기위해.
				{
					YDisBuf = abs(FindRectObj_X.y - pBuf[FStep].y);
					if(YDisMin>YDisBuf)
					{
						YDisMin=YDisBuf;
						StpeRectObj_X = pBuf[FStep];
					}
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,0,255), 1);
	cvDrawRect(pTarget, cvPoint(StpeRectObj_X.x , StpeRectObj_X.y), 
		cvPoint(StpeRectObj_X.x+StpeRectObj_X.width , StpeRectObj_X.y+StpeRectObj_X.height), CV_RGB(0,0,255), 1);
#endif
	if(FCntY>2)
	{
		int DisFindY= FindRectObj_X.y -(abs(FindRectObj_X.y - StpeRectObj_X.y)*(FCntY-1));
		//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
					(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
				{
					if((DisFindY > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
						(DisFindY < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.
					{
						FindRectObj_X=pBuf[FStep];
					}

				}
			}
		}
	}
	else if(FCntY==2)
	{
		FindRectObj_X = StpeRectObj_X;
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,255,255), 2);
#endif
	//Y방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MaxPosY < pBuf[FStep].y )
			{
				MaxPosY = pBuf[FStep].y;
				FindRectObj_Y = pBuf[FStep];
			}
		}
	}
	//ㅛ방향 제일 작은것중에 X방향 제일 작은것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{

				if( FindRectObj_Y.x < pBuf[FStep].x  )
				{
					FindRectObj_Y = pBuf[FStep];
				}
			}
		}
	}

	int XDisMin= pTarget->width;
	int XDisBuf = 0;
	CvRect StpeRectObj_Y;
	memset(&StpeRectObj_Y,0x00, sizeof(CvRect));
	//Y방향 제일 작은것중에 X방향 제일 작은것.에서 가장 가까운것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{
				if( FindRectObj_Y.x != pBuf[FStep].x  )//자기 자신을 빼기위해.
				{
					XDisBuf = abs(FindRectObj_Y.x - pBuf[FStep].x);
					if(XDisMin>XDisBuf)
					{
						XDisMin=XDisBuf;
						StpeRectObj_Y = pBuf[FStep];
					}
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,0), 1);
	cvDrawRect(pTarget, cvPoint(StpeRectObj_Y.x , StpeRectObj_Y.y), 
		cvPoint(StpeRectObj_Y.x+StpeRectObj_Y.width , StpeRectObj_Y.y+StpeRectObj_Y.height), CV_RGB(0,255,0), 1);
#endif
	if(FCntX>2)
	{
		int DisFindX= FindRectObj_Y.x -(abs(FindRectObj_Y.x - StpeRectObj_Y.x)*(FCntX-1));
		//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
					(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
				{
					if((DisFindX > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
						(DisFindX < (pBuf[FStep].x + pBuf[FStep].width/2 )))//같은 선상에서.
					{
						FindRectObj_Y=pBuf[FStep];
					}
				}
			}
		}
	}
	else if(FCntX==2)
	{
		FindRectObj_Y = StpeRectObj_Y;
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,255), 2);
#endif
	//Cell Center
	//cvLine(pTarget, cvPoint(0 , FindRectObj_Y.y), 
	//			cvPoint(pTarget->width-1, FindRectObj_Y.y), CV_RGB(255,0,255), 1);
	//cvLine(pTarget, cvPoint(FindRectObj_X.x , 0), 
	//	cvPoint(FindRectObj_X.x, pTarget->height-1), CV_RGB(0,255,0), 1);
	pEdgeCell->x = FindRectObj_X.x+FindRectObj_X.width;
	pEdgeCell->y = FindRectObj_Y.y+FindRectObj_Y.height;
	CvRect FindRectObj;
	FindRectObj.width =0;
	FindRectObj.height =0;
	FindRectObj.x =FindRectObj_Y.x;
	FindRectObj.y =FindRectObj_X.y;
#ifdef SIMUL_CDTP_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , FindRectObj.y), 
		cvPoint(pTarget->width-1, FindRectObj.y), CV_RGB(255,0,255), 1);
	cvLine(pTarget, cvPoint(FindRectObj.x , 0), 
		cvPoint(FindRectObj.x, pTarget->height-1), CV_RGB(0,255,0), 1);
#endif
	FindIndex = -1;
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
				(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
				(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
				(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)
			{
				FindIndex = FStep;
				FindRectObj =pBuf[FStep];
				break;
			}
		}
	}
	//중앙에 발견된것임으로.X축 안쪽으로 이동시켜서 찾는다.
	if(FindIndex<0) 
	{
		//FindRectObj.x -= XDisMin/2;
		FindRectObj.y += YDisMin/2;
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
					(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
					(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
					(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)


				{
					FindIndex =FStep;
					FindRectObj =pBuf[FStep];
					break;
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , FindRectObj_X.y+FindRectObj_X.height/2), 
		cvPoint(pTarget->width-1, FindRectObj_X.y+FindRectObj_X.height/2), CV_RGB(255,0,255), 2);
	cvLine(pTarget, cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2 , 0), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2, pTarget->height-1), CV_RGB(0,255,0), 2);
#endif
	return FindIndex;
}
int CSeq_CDTP::Find_CELL_LeftBottom(IplImage *pTarget, int Cnt, CvRect *pBuf, int FCntX, int FCntY, double avrW, double avrH, CvPoint *pEdgeCell)
{
	int FindIndex = -1;
	CvRect FindRectObj_X;
	CvRect FindRectObj_Y;
	memset(&FindRectObj_X,0x00, sizeof(CvRect));
	memset(&FindRectObj_Y,0x00, sizeof(CvRect));
	int MinPosX = pTarget->width;
	int MaxPosY = 0;
	int Minbuf = 0;

	double MinW= avrW*0.5;
	double MaxW= avrW*1.2;
	double MinH= avrH*0.5;
	double MaxH= avrH*1.2;
	//기준점 찾기.
	//X방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MinPosX > pBuf[FStep].x  )
			{
				MinPosX = pBuf[FStep].x;
				FindRectObj_X = pBuf[FStep];
			}
		}
	}
	//X방향 제일 작은것중에 Y방향 제일 큰것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{

				if( FindRectObj_X.y < pBuf[FStep].y  )
				{
					FindRectObj_X = pBuf[FStep];
				}
			}
		}
	}

	int YDisMin= pTarget->height;
	int YDisBuf = 0;
	CvRect StpeRectObj_X;
	memset(&StpeRectObj_X,0x00, sizeof(CvRect));
	//X방향 제일 작은것중에 Y방향 제일 큰것. 가장 가까운것.

	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{
				if( FindRectObj_X.y != pBuf[FStep].y  )//자기 자신을 빼기위해.
				{
					YDisBuf = abs(FindRectObj_X.y - pBuf[FStep].y);
					if(YDisMin>YDisBuf)
					{
						YDisMin=YDisBuf;
						StpeRectObj_X = pBuf[FStep];
					}
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,0,255), 1);
	cvDrawRect(pTarget, cvPoint(StpeRectObj_X.x , StpeRectObj_X.y), 
		cvPoint(StpeRectObj_X.x+StpeRectObj_X.width , StpeRectObj_X.y+StpeRectObj_X.height), CV_RGB(0,0,255), 1);
#endif
	if(FCntY>2)
	{
		int DisFindY= FindRectObj_X.y -(abs(FindRectObj_X.y - StpeRectObj_X.y)*(FCntY-1));
		//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
					(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
				{
					if((DisFindY > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
						(DisFindY < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.
					{
						FindRectObj_X=pBuf[FStep];
					}

				}
			}
		}
	}
	else if(FCntY==2)
	{
		FindRectObj_X = StpeRectObj_X;
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,255,255), 2);
#endif
	//Y방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MaxPosY < pBuf[FStep].y )
			{
				MaxPosY = pBuf[FStep].y;
				FindRectObj_Y = pBuf[FStep];
			}
		}
	}
	//ㅛ방향 제일 작은것중에 X방향 제일 작은것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{

				if( FindRectObj_Y.x > pBuf[FStep].x  )
				{
					FindRectObj_Y = pBuf[FStep];
				}
			}
		}
	}

	int XDisMin= pTarget->width;
	int XDisBuf = 0;
	CvRect StpeRectObj_Y;
	memset(&StpeRectObj_Y,0x00, sizeof(CvRect));
	//Y방향 제일 작은것중에 X방향 제일 작은것.에서 가장 가까운것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{
				if( FindRectObj_Y.x != pBuf[FStep].x  )//자기 자신을 빼기위해.
				{
					XDisBuf = abs(FindRectObj_Y.x - pBuf[FStep].x);
					if(XDisMin>XDisBuf)
					{
						XDisMin=XDisBuf;
						StpeRectObj_Y = pBuf[FStep];
					}
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,0), 1);
	cvDrawRect(pTarget, cvPoint(StpeRectObj_Y.x , StpeRectObj_Y.y), 
		cvPoint(StpeRectObj_Y.x+StpeRectObj_Y.width , StpeRectObj_Y.y+StpeRectObj_Y.height), CV_RGB(0,255,0), 1);
#endif
	if(FCntX>2)
	{
		int DisFindX= FindRectObj_Y.x +(abs(FindRectObj_Y.x - StpeRectObj_Y.x)*(FCntX-1));
		//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
					(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
				{
					if((DisFindX > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
						(DisFindX < (pBuf[FStep].x + pBuf[FStep].width/2 )))//같은 선상에서.
					{
						FindRectObj_Y=pBuf[FStep];
					}
				}
			}
		}
	}
	else if(FCntX==2)
	{
		FindRectObj_Y = StpeRectObj_Y;
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,255), 2);
#endif
	//Cell Center
	//cvLine(pTarget, cvPoint(0 , FindRectObj_Y.y), 
	//			cvPoint(pTarget->width-1, FindRectObj_Y.y), CV_RGB(255,0,255), 1);
	//cvLine(pTarget, cvPoint(FindRectObj_X.x , 0), 
	//	cvPoint(FindRectObj_X.x, pTarget->height-1), CV_RGB(0,255,0), 1);
	pEdgeCell->x = FindRectObj_X.x;
	pEdgeCell->y = FindRectObj_Y.y+FindRectObj_Y.height;

	CvRect FindRectObj;
	FindRectObj.width =0;
	FindRectObj.height =0;
	FindRectObj.x =FindRectObj_Y.x;
	FindRectObj.y =FindRectObj_X.y;
#ifdef SIMUL_CDTP_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , FindRectObj.y), 
		cvPoint(pTarget->width-1, FindRectObj.y), CV_RGB(255,0,255), 1);
	cvLine(pTarget, cvPoint(FindRectObj.x , 0), 
		cvPoint(FindRectObj.x, pTarget->height-1), CV_RGB(0,255,0), 1);
#endif
	FindIndex = -1;
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
				(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
				(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
				(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)
			{
				FindIndex = FStep;
				FindRectObj =pBuf[FStep];
				break;
			}
		}
	}
	//중앙에 발견된것임으로.X축 안쪽으로 이동시켜서 찾는다.
	if(FindIndex<0) 
	{
		//FindRectObj.x += XDisMin/2;
		FindRectObj.y += YDisMin/2;
		for(int FStep = 0; FStep<Cnt; FStep++)
		{
			if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
				(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
			{
				if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
					(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
					(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
					(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)


				{
					FindIndex =FStep;
					FindRectObj =pBuf[FStep];
					break;
				}
			}
		}
	}
#ifdef SIMUL_CDTP_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , FindRectObj_X.y+FindRectObj_X.height/2), 
		cvPoint(pTarget->width-1, FindRectObj_X.y+FindRectObj_X.height/2), CV_RGB(255,0,255), 2);
	cvLine(pTarget, cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2 , 0), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2, pTarget->height-1), CV_RGB(0,255,0), 2);
#endif
	return FindIndex;
}
int CSeq_CDTP::Find_CELL_Center(IplImage *pTarget, int Cnt, CvRect *pBuf, double avrW, double avrH)
{
	int FindIndex = -1;
	//CvRect FindRectObj_X;
	//CvRect FindRectObj_Y;
	//memset(&FindRectObj_X,0x00, sizeof(CvRect));
	//memset(&FindRectObj_Y,0x00, sizeof(CvRect));
	//int MinPosX = pTarget->width;//pBuf[0].x+pBuf[0].width;
	//int MinPosY = pTarget->height;//pBuf[0].y+pBuf[0].height;
	//int Minbuf = 0;

	////기준점 찾기.
	////X방향 제일 작은것
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//	{
	//		if( MinPosX > pBuf[FStep].x  )
	//		{
	//			MinPosX = pBuf[FStep].x;
	//			FindRectObj_X = pBuf[FStep];
	//		}
	//	}
	//}
	////X방향 제일 작은것중에 Y방향 제일 작은것.
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//	{
	//		if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
	//			(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
	//		{

	//			if( FindRectObj_X.y > pBuf[FStep].y  )
	//			{
	//				FindRectObj_X = pBuf[FStep];
	//			}
	//		}
	//	}
	//}

	//int YDisMin= pTarget->height;
	//int YDisBuf = 0;
	//CvRect StpeRectObj_X;
	//memset(&StpeRectObj_X,0x00, sizeof(CvRect));
	////X방향 제일 작은것중에 Y방향 제일 작은것.에서 가장 가까운것.

	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//	{
	//		if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
	//			(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
	//		{
	//			if( FindRectObj_X.y != pBuf[FStep].y  )//자기 자신을 빼기위해.
	//			{
	//				YDisBuf = abs(FindRectObj_X.y - pBuf[FStep].y);
	//				if(YDisMin>YDisBuf)
	//				{
	//					YDisMin=YDisBuf;
	//					StpeRectObj_X = pBuf[FStep];
	//				}
	//			}
	//		}
	//	}
	//}

	//cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
	//	cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,0,255), 1);
	//cvDrawRect(pTarget, cvPoint(StpeRectObj_X.x , StpeRectObj_X.y), 
	//	cvPoint(StpeRectObj_X.x+StpeRectObj_X.width , StpeRectObj_X.y+StpeRectObj_X.height), CV_RGB(0,0,255), 1);
	//if(FCntY>2)
	//{
	//	int DisFindY= FindRectObj_X.y +(abs(FindRectObj_X.y - StpeRectObj_X.y)*(FCntY-1));
	//	//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
	//	for(int FStep = 0; FStep<Cnt; FStep++)
	//	{
	//		if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//		{
	//			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
	//				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
	//			{
	//				if((DisFindY > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
	//					(DisFindY < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.
	//				{
	//					FindRectObj_X=pBuf[FStep];
	//				}

	//			}
	//		}
	//	}
	//}
	//else if(FCntY==2)
	//{
	//	FindRectObj_X = StpeRectObj_X;
	//}
	//cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
	//	cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,255,255), 2);



	////Y방향 제일 작은것
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//	{
	//		if( MinPosY > pBuf[FStep].y )
	//		{
	//			MinPosY = pBuf[FStep].y;
	//			FindRectObj_Y = pBuf[FStep];
	//		}
	//	}
	//}
	////ㅛ방향 제일 작은것중에 X방향 제일 작은것.
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//	{
	//		if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
	//			(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
	//		{

	//			if( FindRectObj_Y.x > pBuf[FStep].x  )
	//			{
	//				FindRectObj_Y = pBuf[FStep];
	//			}
	//		}
	//	}
	//}

	//int XDisMin= pTarget->width;
	//int XDisBuf = 0;
	//CvRect StpeRectObj_Y;
	//memset(&StpeRectObj_Y,0x00, sizeof(CvRect));
	////Y방향 제일 작은것중에 X방향 제일 작은것.에서 가장 가까운것.
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//	{
	//		if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
	//			(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
	//		{
	//			if( FindRectObj_Y.x != pBuf[FStep].x  )//자기 자신을 빼기위해.
	//			{
	//				XDisBuf = abs(FindRectObj_Y.x - pBuf[FStep].x);
	//				if(XDisMin>XDisBuf)
	//				{
	//					XDisMin=XDisBuf;
	//					StpeRectObj_Y = pBuf[FStep];
	//				}
	//			}
	//		}
	//	}
	//}
	//cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
	//	cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,0), 1);
	//cvDrawRect(pTarget, cvPoint(StpeRectObj_Y.x , StpeRectObj_Y.y), 
	//	cvPoint(StpeRectObj_Y.x+StpeRectObj_Y.width , StpeRectObj_Y.y+StpeRectObj_Y.height), CV_RGB(0,255,0), 1);

	//if(FCntX>2)
	//{
	//	int DisFindX= FindRectObj_Y.x +(abs(FindRectObj_Y.x - StpeRectObj_Y.x)*(FCntX-1));
	//	//X방향 제일 작은것중에 Y방향 제일 작은것.에서 FCntX만큼 떨어진것
	//	for(int FStep = 0; FStep<Cnt; FStep++)
	//	{
	//		if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//		{
	//			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
	//				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
	//			{
	//				if((DisFindX > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
	//					(DisFindX < (pBuf[FStep].x + pBuf[FStep].width/2 )))//같은 선상에서.
	//				{
	//					FindRectObj_Y=pBuf[FStep];
	//				}
	//			}
	//		}
	//	}
	//}
	//else if(FCntX==2)
	//{
	//	FindRectObj_Y = StpeRectObj_Y;
	//}
	//cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
	//	cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,255), 2);

	////Cell Center
	////cvLine(pTarget, cvPoint(0 , FindRectObj_Y.y), 
	////			cvPoint(pTarget->width-1, FindRectObj_Y.y), CV_RGB(255,0,255), 1);
	////cvLine(pTarget, cvPoint(FindRectObj_X.x , 0), 
	////	cvPoint(FindRectObj_X.x, pTarget->height-1), CV_RGB(0,255,0), 1);

	//CvRect FindRectObj;
	//FindRectObj.width =0;
	//FindRectObj.height =0;
	//FindRectObj.x =FindRectObj_Y.x;
	//FindRectObj.y =FindRectObj_X.y;

	//cvLine(pTarget, cvPoint(0 , FindRectObj.y), 
	//	cvPoint(pTarget->width-1, FindRectObj.y), CV_RGB(255,0,255), 1);
	//cvLine(pTarget, cvPoint(FindRectObj.x , 0), 
	//	cvPoint(FindRectObj.x, pTarget->height-1), CV_RGB(0,255,0), 1);

	//FindIndex = -1;
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//	{
	//		if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
	//			(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
	//			(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
	//			(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)
	//		{
	//			FindIndex = FStep;
	//			FindRectObj =pBuf[FStep];
	//			break;
	//		}
	//	}
	//}
	////중앙에 발견된것임으로.X축 안쪽으로 이동시켜서 찾는다.
	//if(FindIndex<0) 
	//{
	//	FindRectObj.x += XDisMin/2;
	//	for(int FStep = 0; FStep<Cnt; FStep++)
	//	{
	//		if(pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )//최소 Size보다 큰것들 중에
	//		{
	//			if((FindRectObj.x > (pBuf[FStep].x - pBuf[FStep].width/2 ))&&
	//				(FindRectObj.x < (pBuf[FStep].x + pBuf[FStep].width/2 ))&&
	//				(FindRectObj.y > (pBuf[FStep].y - pBuf[FStep].height/2 ))&&
	//				(FindRectObj.y < (pBuf[FStep].y + pBuf[FStep].height/2 )))//같은 선상에서.(X, Y)


	//			{
	//				FindIndex =FStep;
	//				FindRectObj =pBuf[FStep];
	//				break;
	//			}
	//		}
	//	}
	//}

	//cvLine(pTarget, cvPoint(0 , FindRectObj_X.y+FindRectObj_X.height/2), 
	//	cvPoint(pTarget->width-1, FindRectObj_X.y+FindRectObj_X.height/2), CV_RGB(255,0,255), 2);
	//cvLine(pTarget, cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2 , 0), 
	//	cvPoint(FindRectObj_Y.x+FindRectObj_Y.width/2, pTarget->height-1), CV_RGB(0,255,0), 2);

	return FindIndex;
}
CvPoint CSeq_CDTP::Find_CELL_Left_Cross( IplImage *pTarget, int FindIndex, int Cnt, CvRect *pBuf, double avrW, double avrH)
{
	CvPoint retValue;
	retValue.x = -1;
	retValue.y = -1;

	if(FindIndex<0 || FindIndex>Cnt || pBuf == nullptr)
		return retValue;

	CvRect FindRectObj = pBuf[FindIndex];

	double MinW= avrW*0.5;
	double MaxW= avrW*1.2;
	double MinH= avrH*0.5;
	double MaxH= avrH*1.2;
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
	//		(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
	//	{
	//		cvDrawRect(pTarget, cvPoint(pBuf[FStep].x , pBuf[FStep].y), 
	//			cvPoint(pBuf[FStep].x+pBuf[FStep].width , pBuf[FStep].y+pBuf[FStep].height), CV_RGB(0,0,0), 2);
	//	}
	//}

	int XDisMin= pTarget->width;
	int XDisBuf = 0;
	//Y방향 제일 작은것중에 X방향 제일 작은것.에서 가장 가까운것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj.y - FindRectObj.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj.y + FindRectObj.height/2 )))//같은 선상에서.
			{
				if( FindRectObj.x != pBuf[FStep].x  )//자기 자신을 빼기위해.
				{
					XDisBuf = abs(FindRectObj.x - pBuf[FStep].x);
					if(XDisMin>XDisBuf)
					{
						XDisMin=XDisBuf;
					}
				}
			}
		}
	}
	int YDisMin= pTarget->height;
	int YDisBuf = 0;
	//X방향 제일 작은것중에 Y방향 제일 작은것.에서 가장 가까운것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj.x - FindRectObj.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj.x + FindRectObj.width/2 )))//같은 선상에서.
			{
				if( FindRectObj.y != pBuf[FStep].y  )//자기 자신을 빼기위해.
				{
					YDisBuf = abs(FindRectObj.y - pBuf[FStep].y);
					if(YDisMin>YDisBuf)
					{
						YDisMin=YDisBuf;
					}
				}
			}
		}
	}
	
	int Right_Up_Index = -1;
	CPoint Right_Up_Check;
	Right_Up_Check.x = FindRectObj.x+(FindRectObj.width/2) + XDisMin/2;
	Right_Up_Check.y = FindRectObj.y+(FindRectObj.height/2)- YDisMin/2;

	int Right_Down_Index = -1;
	CPoint Right_Down_Check;
	Right_Down_Check.x = FindRectObj.x+(FindRectObj.width/2) + XDisMin/2;
	Right_Down_Check.y = FindRectObj.y+(FindRectObj.height/2)+ YDisMin/2;

	
	//포인트를 포함하는 것을 찾자.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			CRect ChkRect;
			ChkRect.left = pBuf[FStep].x;
			ChkRect.top = pBuf[FStep].y;
			ChkRect.right = pBuf[FStep].x + pBuf[FStep].width;
			ChkRect.bottom = pBuf[FStep].y + pBuf[FStep].height;

			if(ChkRect.PtInRect(Right_Up_Check) == TRUE)
				Right_Up_Index = FStep;

			if(ChkRect.PtInRect(Right_Down_Check) == TRUE)
				Right_Down_Index = FStep;

		}
	}
	retValue.x = Right_Up_Index;
	retValue.y = Right_Down_Index;

	return retValue;
}
CvPoint CSeq_CDTP::Find_CELL_Right_Cross( IplImage *pTarget, int FindIndex, int Cnt, CvRect *pBuf, double avrW, double avrH)
{
	CvPoint retValue;
	retValue.x = -1;
	retValue.y = -1;

	if(FindIndex<0 || FindIndex>Cnt || pBuf == nullptr)
		return retValue;

	CvRect FindRectObj = pBuf[FindIndex];

	double MinW= avrW*0.5;
	double MaxW= avrW*1.2;
	double MinH= avrH*0.5;
	double MaxH= avrH*1.2;
	//for(int FStep = 0; FStep<Cnt; FStep++)
	//{
	//	if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
	//		(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
	//	{
	//		cvDrawRect(pTarget, cvPoint(pBuf[FStep].x , pBuf[FStep].y), 
	//			cvPoint(pBuf[FStep].x+pBuf[FStep].width , pBuf[FStep].y+pBuf[FStep].height), CV_RGB(0,0,0), 2);
	//	}
	//}

	int XDisMin= pTarget->width;
	int XDisBuf = 0;
	//Y방향 제일 작은것중에 X방향 제일 작은것.에서 가장 가까운것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj.y - FindRectObj.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj.y + FindRectObj.height/2 )))//같은 선상에서.
			{
				if( FindRectObj.x != pBuf[FStep].x  )//자기 자신을 빼기위해.
				{
					XDisBuf = abs(FindRectObj.x - pBuf[FStep].x);
					if(XDisMin>XDisBuf)
					{
						XDisMin=XDisBuf;
					}
				}
			}
		}
	}
	int YDisMin= pTarget->height;
	int YDisBuf = 0;
	//X방향 제일 작은것중에 Y방향 제일 작은것.에서 가장 가까운것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj.x - FindRectObj.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj.x + FindRectObj.width/2 )))//같은 선상에서.
			{
				if( FindRectObj.y != pBuf[FStep].y  )//자기 자신을 빼기위해.
				{
					YDisBuf = abs(FindRectObj.y - pBuf[FStep].y);
					if(YDisMin>YDisBuf)
					{
						YDisMin=YDisBuf;
					}
				}
			}
		}
	}

	int Right_Up_Index = -1;
	CPoint Right_Up_Check;
	Right_Up_Check.x = FindRectObj.x+(FindRectObj.width/2) - XDisMin/2;
	Right_Up_Check.y = FindRectObj.y+(FindRectObj.height/2)- YDisMin/2;

	int Right_Down_Index = -1;
	CPoint Right_Down_Check;
	Right_Down_Check.x = FindRectObj.x+(FindRectObj.width/2) - XDisMin/2;
	Right_Down_Check.y = FindRectObj.y+(FindRectObj.height/2)+ YDisMin/2;


	//포인트를 포함하는 것을 찾자.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			CRect ChkRect;
			ChkRect.left = pBuf[FStep].x;
			ChkRect.top = pBuf[FStep].y;
			ChkRect.right = pBuf[FStep].x + pBuf[FStep].width;
			ChkRect.bottom = pBuf[FStep].y + pBuf[FStep].height;

			if(ChkRect.PtInRect(Right_Up_Check) == TRUE)
				Right_Up_Index = FStep;

			if(ChkRect.PtInRect(Right_Down_Check) == TRUE)
				Right_Down_Index = FStep;

		}
	}
	retValue.x = Right_Up_Index;
	retValue.y = Right_Down_Index;

	return retValue;
}

CvPoint CSeq_CDTP::Find_TP_Center(CD_TPParam *pCDTPParam, CDTP_Point*pNowPos, IplImage *pTarget, REVIEW_LENS_OFFSET	*pReviewLensOffset)
{
	if(m_pTp_GrayAlign != nullptr)
		cvReleaseImage(&m_pTp_GrayAlign);
	m_pTp_GrayAlign = nullptr;

	int ROI_W = 0;
	int ROI_H = 0;
	switch(m_pCDTPParam->nMeasure_Lens)
	{
	//case REVIEW_LENS_2X:
	//	{
	//		m_pTp_GrayAlign = cvLoadImage(TP_ALIGN_PATH_2X, CV_LOAD_IMAGE_GRAYSCALE);
	//	}break;
	case REVIEW_LENS_10X:
		{
			m_pTp_GrayAlign = cvLoadImage(TP_ALIGN_PATH_10X, CV_LOAD_IMAGE_GRAYSCALE);

			ROI_W = (int)(pCDTPParam->nTP_Mark_Distance_X/pReviewLensOffset->Lens10xPixelSizeX);
			ROI_H = (int)(pCDTPParam->nTP_Mark_Distance_Y/pReviewLensOffset->Lens10xPixelSizeX);
		}break;
	case REVIEW_LENS_20X:
		{
			m_pTp_GrayAlign = cvLoadImage(TP_ALIGN_PATH_20X, CV_LOAD_IMAGE_GRAYSCALE);
			ROI_W = (int)(pCDTPParam->nTP_Mark_Distance_X/pReviewLensOffset->Lens20xPixelSizeX);
			ROI_H = (int)(pCDTPParam->nTP_Mark_Distance_Y/pReviewLensOffset->Lens20xPixelSizeX);
		}break;
	default:
		{
			m_pTp_GrayAlign = cvLoadImage(TP_ALIGN_PATH_20X, CV_LOAD_IMAGE_GRAYSCALE);
			ROI_W = (int)(pCDTPParam->nTP_Mark_Distance_X/pReviewLensOffset->Lens20xPixelSizeX);
			ROI_H = (int)(pCDTPParam->nTP_Mark_Distance_Y/pReviewLensOffset->Lens20xPixelSizeX);
		}break;
	}

	CvPoint RetPont;
	RetPont.x = -1;
	RetPont.y = -1;

	if(m_pTp_GrayBuf == nullptr)
		m_pTp_GrayBuf = cvCreateImage(cvSize(pTarget->width, pTarget->height), IPL_DEPTH_8U, 1);
	else
		cvSetZero(m_pTp_GrayBuf);

	if(m_pTp_GrayBufCopy == nullptr)
		m_pTp_GrayBufCopy = cvCreateImage(cvSize(pTarget->width, pTarget->height), IPL_DEPTH_8U, 1);
	else
		cvSetZero(m_pTp_GrayBufCopy);


	//cvCvtColor(pTarget, m_pTp_GrayBuf, CV_RGB2GRAY);
	cvCvtColor(pTarget, m_pTp_GrayBufCopy, CV_RGB2GRAY);
	CvRect rROI_Area;
	rROI_Area.x = pTarget->width/2 - ROI_W;
	rROI_Area.y = pTarget->height/2- ROI_H;
	rROI_Area.width =  (ROI_W*2);
	rROI_Area.height = (ROI_H*2);

	cvSetImageROI(m_pTp_GrayBufCopy, rROI_Area);
	cvSetImageROI(m_pTp_GrayBuf, rROI_Area);
	cvCopyImage(m_pTp_GrayBufCopy, m_pTp_GrayBuf);
	cvResetImageROI(m_pTp_GrayBuf);
	cvResetImageROI(m_pTp_GrayBufCopy);

	cvThreshold(m_pTp_GrayBuf, m_pTp_GrayBuf, 128, 255, CV_THRESH_BINARY_INV); 
	//CBlobLabeling blob;
	//영상 노이즈 제거.
	switch(m_pCDTPParam->nMeasure_Lens)
	{
	case REVIEW_LENS_10X:
		{
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 2);
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 2);
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 2);
		}break;
	case REVIEW_LENS_20X:
		{
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 4);
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 4);
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 4);
		}break;
	default:
		{
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 4);
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 4);
			cvErode(m_pTp_GrayBuf, m_pTp_GrayBuf, NULL, 4);
		}break;
	}

	//cvErode(pImgBuf, pImgBuf, NULL, 4);
	//cvErode(pImgBuf, pImgBuf, NULL, 4);
	//cvErode(pImgBuf, pImgBuf, NULL, 4);
	//cvDilate(pImgBuf, pImgBuf, NULL, 2);

	if(m_pMatchBuffer != nullptr)
	{
		cvReleaseImage(&m_pMatchBuffer);
		m_pMatchBuffer = nullptr;
	}

	m_pMatchBuffer = cvCreateImage(cvSize(pTarget->width - m_pTp_GrayAlign->width + 1,
		pTarget->height - m_pTp_GrayAlign->height + 1),	IPL_DEPTH_32F, 1);

	double dMin = 0.0, dMax = 0.0;
	double dLimit = 0.90;
	CvPoint left_top;
	cvSetZero(m_pMatchBuffer); 
	cvMatchTemplate(m_pTp_GrayAlign, m_pTp_GrayBuf, m_pMatchBuffer, CV_TM_CCOEFF_NORMED);
	cvMinMaxLoc(m_pMatchBuffer, &dMin, &dMax, NULL, &left_top);
	RetPont.x = left_top.x +m_pTp_GrayAlign->width/2;
	RetPont.y = left_top.y +m_pTp_GrayAlign->height/2;
	return RetPont;
}
bool CSeq_CDTP::Calc_TP_Value(GLASS_PARAM *pGlassReicpeParam, int nListCnt, CDTP_Point *pCDTPPointList, TP_Result *pTP_Result)
{
	double Cell_TP_W_Total = 0.0;
	double Cell_TP_H_Total = 0.0;
	double Stick_TP_W= 0.0;
	double Stick_TP_H= 0.0;

	pTP_Result->ResetCellTPBuf();
	pTP_Result->CreateCellTPBuf(pGlassReicpeParam->GLASS_CELL_COUNT_Y);

	CvPoint StartPt, EndPt;

	//Total TP
	CDTP_Point *pTPPoint_Start = &pCDTPPointList[0];
	CDTP_Point *pTPPoint_End = &pCDTPPointList[(pGlassReicpeParam->GLASS_CELL_COUNT_Y-1)*4+3];

	StartPt.x = pTPPoint_Start->nTP_MeasurePointX;
	StartPt.y = pTPPoint_Start->nTP_MeasurePointY;
	EndPt.x = pTPPoint_End->nTP_MeasurePointX;
	EndPt.y = pTPPoint_End->nTP_MeasurePointY;
	pTP_Result->fCDTP_Ret_TP = (float)TwoPoint_Distance(StartPt,	EndPt);


	int CellTP_Index = 0;
	//Cell TP계산
	for(int TPStep = 0; TPStep<(pGlassReicpeParam->GLASS_CELL_COUNT_Y*4); TPStep++)
	{
		if(pCDTPPointList[TPStep].nPosType == CELL_LeftTop)
		{
			pCDTPPointList[TPStep];
			EndPt.x = pCDTPPointList[TPStep].nTP_MeasurePointX;
			EndPt.y = pCDTPPointList[TPStep].nTP_MeasurePointY;
			if(CellTP_Index<pGlassReicpeParam->GLASS_CELL_COUNT_Y)
			{
				pTP_Result->pfCDTP_Ret_CELL_TP[CellTP_Index++] = (float)TwoPoint_Distance(StartPt, EndPt);
			}
		}
	}
	return true;
}
int CSeq_CDTP::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_CDTP%03d> Sequence Start", ProcStep);

			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_STICK_STATE_CHECK:
		{//Stick이 측정 가능 상태 인지 체크 한다.
			
			if(G_CheckStickProc_StaticState() == ST_PROC_NG)
			{
				G_WriteInfo(Log_Check, "SEQ_STEP_STICK_STATE_CHECK ST_PROC_NG", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}



			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_DATA_CHECK:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_DATA_CHECK(%03d)>", ProcStep);
			if(*	m_pCDTPPointCnt<=0 && *m_ppCDTPPointList == nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_DATA_CHECK%03d> Data Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			char strReadData[256];

			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_Process_Data, Key_NowRunBoxID, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_strBoxID.Format("%s", strReadData);
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_strStickID.Format("%s", strReadData);
			if(m_strBoxID.GetLength()<5)
				m_strBoxID.Format("None");
			if(m_strStickID.GetLength()<5)
				m_strStickID.Format("None");

			
			//m_strCDTPSavePath.Format("%s\\%s_%s_CDTP\\", VS10Master_Result_PATH,m_strBoxID, m_strStickID);
			m_strCDTPSavePath.Format("%s\\%s_CDTP\\", VS10Master_Result_PATH, m_strStickID);
			G_fnCheckDirAndCreate(m_strCDTPSavePath.GetBuffer(m_strCDTPSavePath.GetLength()));
			G_ClearFolder(m_strCDTPSavePath.GetBuffer(m_strCDTPSavePath.GetLength()));

			//TT01위치 체크.
			if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_DATA_CHECK%03d>TT01 Pos Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_CDTP_LENS_CHANGE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_LENS_CHANGE(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Func_ReviewLens;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, m_pCDTPParam->nMeasure_Lens, nBiz_Unit_Zero);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_LENS_CHANGE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			
			if(m_RecvCMD_Ret != m_pCDTPParam->nMeasure_Lens)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_LENS_CHANGE%03d> Scope Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			m_RecvCMD_Ret = -1;
			m_WaitCMD = 0;
			m_RecvCMD = 0;
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_CDTP_REVIEW_POS_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_REVIEW_POS_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Review);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_REVIEW_POS_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_REVIEW_POS_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//Start Index를 Reset을 한다.
			m_CDTPMeasureIndex = 0;
			//memset(&m_NowIndexPos, 0x00, sizeof(CDTP_Point));
			m_pNowIndexPos = nullptr;
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_CDTP_MOVE_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_MOVE_POS(%03d)>", ProcStep);
			m_pNowIndexPos =  &(*m_ppCDTPPointList)[m_CDTPMeasureIndex];

			m_pNowIndexPos->nCellEdgePointX = 0;
			m_pNowIndexPos->nCellEdgePointY = 0;

			m_pNowIndexPos->nMCenterPointX = 0;
			m_pNowIndexPos->nMCenterPointY = 0;

			m_pNowIndexPos->fSlitSizeW =  0.0f;
			m_pNowIndexPos->fSlitSizeH =  0.0f;

			m_pNowIndexPos->fRibSizeW =  0.0f;
			m_pNowIndexPos->fRibSizeH =  0.0f;

			//첫Cell은 그냥 가고 두번째 부터 Cell.위치 계산.
			//int NextPosShift = m_pNowIndexPos->nPointX;
			//int NextPosDrive = m_pNowIndexPos->nPointY;

			if(m_CDTPMeasureIndex==0)
			{//첫 Cell인 경우 Align좌표를 이용한다.(Cell Align시에 이미 적용 되어 계산 된다.)
				m_pNowIndexPos->nPointX += CDTP_CELL_EDGE_OFFSET; 
				m_pNowIndexPos->nPointY += CDTP_CELL_EDGE_OFFSET;
			}
			else if(m_CDTPMeasureIndex>0 &&m_pGlassReicpeParam !=nullptr)
			{
				//첫 Cell 위치에서 지정위치들을 찾아 가자.(화면안에 안들어온다.--위치 오류 있다.
				CDTP_Point *OldpNowIndexPos = &(*m_ppCDTPPointList)[m_CDTPMeasureIndex-1];
				if((OldpNowIndexPos->nCellEdgePointX>0 && OldpNowIndexPos->nCellEdgePointX>0 ))
				{//이전 Cell의 CD를 정상적으로 찾았을경우.
					switch(OldpNowIndexPos->nPosType)
					{
						case CELL_LeftTop:
							{
								m_pNowIndexPos->nPointX = OldpNowIndexPos->nCellEdgePointX+m_pGlassReicpeParam->CELL_SIZE_X;
								m_pNowIndexPos->nPointY = OldpNowIndexPos->nCellEdgePointY;

								m_pNowIndexPos->nPointX -= CDTP_CELL_EDGE_OFFSET; 
								m_pNowIndexPos->nPointY += CDTP_CELL_EDGE_OFFSET;
							}break;
						case CELL_RightTop:
							{
								m_pNowIndexPos->nPointX = OldpNowIndexPos->nCellEdgePointX;
								m_pNowIndexPos->nPointY = OldpNowIndexPos->nCellEdgePointY+m_pGlassReicpeParam->CELL_SIZE_Y;

								m_pNowIndexPos->nPointX -= CDTP_CELL_EDGE_OFFSET; 
								m_pNowIndexPos->nPointY -= CDTP_CELL_EDGE_OFFSET;
							}break;
						case CELL_RightBottom:
							{
								m_pNowIndexPos->nPointX = OldpNowIndexPos->nCellEdgePointX-m_pGlassReicpeParam->CELL_SIZE_X;
								m_pNowIndexPos->nPointY = OldpNowIndexPos->nCellEdgePointY;

								m_pNowIndexPos->nPointX += CDTP_CELL_EDGE_OFFSET; 
								m_pNowIndexPos->nPointY -= CDTP_CELL_EDGE_OFFSET;
							}break;
						case CELL_LeftBottom:
							{
								m_pNowIndexPos->nPointX = OldpNowIndexPos->nCellEdgePointX;
								m_pNowIndexPos->nPointY = OldpNowIndexPos->nCellEdgePointY+(m_pGlassReicpeParam->CELL_DISTANCE_Y-m_pGlassReicpeParam->CELL_SIZE_Y);
							
								m_pNowIndexPos->nPointX += CDTP_CELL_EDGE_OFFSET; 
								m_pNowIndexPos->nPointY += CDTP_CELL_EDGE_OFFSET;
							}break;
					}
					G_WriteInfo(Log_Normal, "Find New Cell Pos(S: %d, D:%d)", m_pNowIndexPos->nPointX, m_pNowIndexPos->nPointY);
				}
			}

			G_MotObj.m_fnAM01ReviewSyncMove(m_pNowIndexPos->nPointX, m_pNowIndexPos->nPointY, Sync_Scope_BackLight);
			m_pLiveViewObj->DrawCDTPReviewInfo(false);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_MOVE_POS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_MOVE_POS%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_REVIEW_LIGHT_OFF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_REVIEW_LIGHT_OFF(%03d)>", ProcStep);
			//Live View를 On한다.
			m_pLiveViewObj->LiveView(true);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_SetReviewLight;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, 0/*m_pCDTPParam->nLight_Upper*/);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_REVIEW_LIGHT_OFF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//if(m_RecvCMD_Ret != 0)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_REVIEW_LIGHT_OFF%03d> Review Light Off!!!!", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_LIGHT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_LIGHT(%03d)>", ProcStep);
			G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, m_pCDTPParam->nLight_Back ,0);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_LIGHT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_LIGHT%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_REVIEW_UP:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_REVIEW_UP(%03d)>", ProcStep);
			G_MotObj.m_fnReviewUp();
			int TimeOutWait = 6000;//About 60sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_UP].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_REVIEW_UP%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_UP].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_REVIEW_UP%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_REVIEW_AF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_REVIEW_AF(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_AF;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 3);//One AF Try

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_REVIEW_AF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)//AF On
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_REVIEW_AF%03d> AF On!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_CDTP_REVIEW_GRAB:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_REVIEW_GRAB(%03d)>", ProcStep);
			//Live View를 On한다.
			if(m_pNowGrabImg != nullptr)
				cvReleaseImage(&m_pNowGrabImg);
			m_pNowGrabImg = nullptr;

			bool bLiveOverlay = m_pLiveViewObj->m_bSaveOverlayImage;
			m_pLiveViewObj->m_bSaveOverlayImage = false;
			m_pLiveViewObj->SaveImage(&m_pNowGrabImg);

			//CString strNewName;
			//strNewName.Format("D:\\CD_Grab_%d.bmp", GetTickCount());
			//cvSaveImage(strNewName.GetBuffer(strNewName.GetLength()),m_pNowGrabImg);

			int TimeOutWait = 3000;//About 20sec
			while(m_pNowGrabImg == nullptr)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			m_pLiveViewObj->m_bSaveOverlayImage= bLiveOverlay;
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_REVIEW_GRAB%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pNowGrabImg  ==  nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_REVIEW_GRAB%03d> Grab Image Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_MEASURE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_MEASURE(%03d)>", ProcStep);

			CvPoint CellEdge;
			CvRect FindTargetRect;
			CvRect FindIndexRib_LR;
			CvRect FindIndexRib_UD;

			FindTargetRect = CDTPImgProc(m_pCDTPParam, m_pNowIndexPos, m_pNowGrabImg, &CellEdge, &FindIndexRib_LR, &FindIndexRib_UD);

			double Pixel_SizeSlit_W = 0.0;
			double Pixel_SizeSlit_H = 0.0;
							
			double Pixel_SizeRib_W = 0.0;
			double Pixel_SizeRib_H = 0.0;


			if(FindTargetRect.x<0 || FindTargetRect.y<0 ||FindTargetRect.width<0 || FindTargetRect.height<0)
			{
				//G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_MEASURE%03d> Find CD TP Point", ProcStep);
				//fnSeqSetNextStep(SEQ_STEP_ERROR);

				m_bCD_FindOk = false;
				G_WriteInfo(Log_Check, "측정 오류 Skip(Main Pos Find Error)");
				fnSeqSetNextStep(SEQ_STEP_CDTP_SAVE_RESULT);
				break;
			}
			m_pLiveViewObj->SetCDTPReviewInfo(*m_pCDTPPointCnt, m_CDTPMeasureIndex+1, m_strStickID, FindTargetRect, cvRect(0,0,0,0));

			if(FindIndexRib_LR.x<0 || FindIndexRib_LR.y<0 ||FindIndexRib_LR.width<0 || FindIndexRib_LR.height<0)
			{
				m_bCD_FindOk = false;
				G_WriteInfo(Log_Check, "측정 오류 Skip(Left/Right Pos Find Error)");
				fnSeqSetNextStep(SEQ_STEP_CDTP_SAVE_RESULT);
				break;
			}
			if(FindIndexRib_UD.x<0 || FindIndexRib_UD.y<0 ||FindIndexRib_UD.width<0 || FindIndexRib_UD.height<0)
			{
				m_bCD_FindOk = false;
				G_WriteInfo(Log_Check, "측정 오류 Skip(Up/Down Pos Find Error)");
				fnSeqSetNextStep(SEQ_STEP_CDTP_SAVE_RESULT);
				break;
			}
			CvPoint StartPt, EndPt_W, EndPt_H;
			StartPt.x = FindTargetRect.x + (FindTargetRect.width/2);
			StartPt.y = FindTargetRect.y + (FindTargetRect.height/2);
			EndPt_W.x = FindIndexRib_LR.x + (FindIndexRib_LR.width/2);
			EndPt_W.y = FindIndexRib_LR.y + (FindIndexRib_LR.height/2);
			EndPt_H.x = FindIndexRib_UD.x + (FindIndexRib_UD.width/2);
			EndPt_H.y = FindIndexRib_UD.y + (FindIndexRib_UD.height/2);

			Pixel_SizeSlit_W = m_fnGetLengthData(m_pProcGrayImgBuf,StartPt, EndPt_W,  WHITE_LENGTH);//CD
			Pixel_SizeRib_W = m_fnGetLengthData(m_pProcGrayImgBuf,StartPt, EndPt_W,  BLACK_LENGTH);//Rib

			Pixel_SizeSlit_H = m_fnGetLengthData(m_pProcGrayImgBuf,StartPt, EndPt_H,  WHITE_LENGTH);//CD
			Pixel_SizeRib_H = m_fnGetLengthData(m_pProcGrayImgBuf,StartPt, EndPt_H,  BLACK_LENGTH);//Rib

			m_bCD_FindOk = true;
			//Cell Corner
			float CellCornerPosX = (float)( CellEdge.x  - (m_pNowGrabImg->width/2));
			float CellCornerPosY = (float)( CellEdge.y - (m_pNowGrabImg->height/2));
			//측정위치.
			float ImagePosX = (float)( (FindTargetRect.x+(FindTargetRect.width/2))  - (m_pNowGrabImg->width/2));
			float ImagePosY = (float)( (FindTargetRect.y+(FindTargetRect.height/2)) - (m_pNowGrabImg->height/2));
			switch(m_pCDTPParam->nMeasure_Lens)
			{
			case REVIEW_LENS_2X:
				{
					m_pNowIndexPos->nCellEdgePointX = (int)((float)m_pNowIndexPos->nPointX + (CellCornerPosX*m_pReviewLensOffset->Lens02xPixelSizeX));
					m_pNowIndexPos->nCellEdgePointY = (int)((float)m_pNowIndexPos->nPointY + (CellCornerPosY*m_pReviewLensOffset->Lens02xPixelSizeY));

					m_pNowIndexPos->fSlitSizeW = (float)Pixel_SizeSlit_W*(float)m_pReviewLensOffset->Lens02xPixelSizeX;
					m_pNowIndexPos->fSlitSizeH = (float)Pixel_SizeSlit_H*(float)m_pReviewLensOffset->Lens02xPixelSizeY;

					m_pNowIndexPos->fRibSizeW = (float)Pixel_SizeRib_W*(float)m_pReviewLensOffset->Lens02xPixelSizeX;
					m_pNowIndexPos->fRibSizeH = (float)Pixel_SizeRib_H*(float)m_pReviewLensOffset->Lens02xPixelSizeY;

					m_pNowIndexPos->nMCenterPointX = (int)((float)m_pNowIndexPos->nPointX + (ImagePosX*m_pReviewLensOffset->Lens02xPixelSizeX));
					m_pNowIndexPos->nMCenterPointY = (int)((float)m_pNowIndexPos->nPointY + (ImagePosY*m_pReviewLensOffset->Lens02xPixelSizeY));

				}break;
			case REVIEW_LENS_10X:
				{
					m_pNowIndexPos->nCellEdgePointX = (int)((float)m_pNowIndexPos->nPointX + (CellCornerPosX*m_pReviewLensOffset->Lens10xPixelSizeX));
					m_pNowIndexPos->nCellEdgePointY = (int)((float)m_pNowIndexPos->nPointY + (CellCornerPosY*m_pReviewLensOffset->Lens10xPixelSizeY));

					m_pNowIndexPos->fSlitSizeW = (float)Pixel_SizeSlit_W*(float)m_pReviewLensOffset->Lens10xPixelSizeX;
					m_pNowIndexPos->fSlitSizeH = (float)Pixel_SizeSlit_H*(float)m_pReviewLensOffset->Lens10xPixelSizeY;
					
					m_pNowIndexPos->fRibSizeW = (float)Pixel_SizeRib_W*(float)m_pReviewLensOffset->Lens10xPixelSizeX;
					m_pNowIndexPos->fRibSizeH = (float)Pixel_SizeRib_H*(float)m_pReviewLensOffset->Lens10xPixelSizeY;

					m_pNowIndexPos->nMCenterPointX = (int)((float)m_pNowIndexPos->nPointX + (ImagePosX*m_pReviewLensOffset->Lens10xPixelSizeX));
					m_pNowIndexPos->nMCenterPointY = (int)((float)m_pNowIndexPos->nPointY + (ImagePosY*m_pReviewLensOffset->Lens10xPixelSizeY));

				}break;
			case REVIEW_LENS_20X:
				{
					m_pNowIndexPos->nCellEdgePointX = (int)((float)m_pNowIndexPos->nPointX + (CellCornerPosX*m_pReviewLensOffset->Lens20xPixelSizeX));
					m_pNowIndexPos->nCellEdgePointY = (int)((float)m_pNowIndexPos->nPointY + (CellCornerPosY*m_pReviewLensOffset->Lens20xPixelSizeY));

					m_pNowIndexPos->fSlitSizeW = (float)Pixel_SizeSlit_W*m_pReviewLensOffset->Lens20xPixelSizeX;
					m_pNowIndexPos->fSlitSizeH = (float)Pixel_SizeSlit_H*m_pReviewLensOffset->Lens20xPixelSizeY;

					m_pNowIndexPos->fRibSizeW = (float)Pixel_SizeRib_W*(float)m_pReviewLensOffset->Lens20xPixelSizeX;
					m_pNowIndexPos->fRibSizeH = (float)Pixel_SizeRib_H*(float)m_pReviewLensOffset->Lens20xPixelSizeY;

					m_pNowIndexPos->nMCenterPointX = (int)((float)m_pNowIndexPos->nPointX + (ImagePosX*m_pReviewLensOffset->Lens20xPixelSizeX));
					m_pNowIndexPos->nMCenterPointY = (int)((float)m_pNowIndexPos->nPointY + (ImagePosY*m_pReviewLensOffset->Lens20xPixelSizeY));
				}break;
			default:
				{
					m_pNowIndexPos->nCellEdgePointX = (int)((float)m_pNowIndexPos->nPointX + (CellCornerPosX*m_pReviewLensOffset->Lens10xPixelSizeX));
					m_pNowIndexPos->nCellEdgePointY = (int)((float)m_pNowIndexPos->nPointY + (CellCornerPosY*m_pReviewLensOffset->Lens10xPixelSizeY));

					m_pNowIndexPos->fSlitSizeW = (float)Pixel_SizeSlit_W*m_pReviewLensOffset->Lens10xPixelSizeX;
					m_pNowIndexPos->fSlitSizeH = (float)Pixel_SizeSlit_H*m_pReviewLensOffset->Lens10xPixelSizeY;

					m_pNowIndexPos->fRibSizeW = (float)Pixel_SizeRib_W*m_pReviewLensOffset->Lens10xPixelSizeX;
					m_pNowIndexPos->fRibSizeH = (float)Pixel_SizeRib_H*m_pReviewLensOffset->Lens10xPixelSizeY;

					m_pNowIndexPos->nMCenterPointX = (int)((float)m_pNowIndexPos->nPointX + (ImagePosX*m_pReviewLensOffset->Lens10xPixelSizeX));
					m_pNowIndexPos->nMCenterPointY = (int)((float)m_pNowIndexPos->nPointY + (ImagePosY*m_pReviewLensOffset->Lens10xPixelSizeY));
				}break;
			}
			
			CDTPDrawResult(m_pNowGrabImg, FindTargetRect, FindIndexRib_LR , FindIndexRib_UD, CellEdge, m_pNowIndexPos);

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_SAVE_RESULT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_SAVE_RESULT%03d> %d", ProcStep, m_CDTPMeasureIndex);
			CTime time = CTime::GetCurrentTime();
			CString strRetFileName;
			//strRetFileName.Format("%s.%03d_%04d%02d%02d%02d%02d%02d.jpg", m_strStickID, m_CDTPMeasureIndex+1, 
			//	time.GetYear(),time.GetMonth(), time.GetDay(), time.GetHour(),time.GetMinute(), time.GetSecond());
			strRetFileName.Format("CD_%s.%03d.jpg", m_strStickID, m_CDTPMeasureIndex+1);

			CString strCDTPSavePathName;
			strCDTPSavePathName.Format("%s\\%s", m_strCDTPSavePath, strRetFileName);
			cvSaveImage(strCDTPSavePathName.GetBuffer(strCDTPSavePathName.GetLength()), m_pNowGrabImg);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_TP_MOVE_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_TP_MOVE_POS(%03d)>", ProcStep);
			

			int TP_ShiftPos = m_pNowIndexPos->nPointX;
			int TP_DrivePos = m_pNowIndexPos->nPointY;
			if(m_bCD_FindOk == true)
			{//CD 찾기 성공 했다면 기준점 위치에서 TP위치를 찾는다.
				TP_ShiftPos = m_pNowIndexPos->nCellEdgePointX;
				TP_DrivePos = m_pNowIndexPos->nCellEdgePointY;
			}
			switch(m_pNowIndexPos->nPosType)
			{
			case CELL_LeftTop:
				{
					TP_ShiftPos -= m_pCDTPParam->nTP_Mark_Distance_X;
					TP_DrivePos -= m_pCDTPParam->nTP_Mark_Distance_Y;
				}break;
			case CELL_RightTop:
				{
					TP_ShiftPos += m_pCDTPParam->nTP_Mark_Distance_X;
					TP_DrivePos -= m_pCDTPParam->nTP_Mark_Distance_Y;
				}break;
			case CELL_RightBottom:
				{
					TP_ShiftPos += m_pCDTPParam->nTP_Mark_Distance_X;
					TP_DrivePos += m_pCDTPParam->nTP_Mark_Distance_Y;
				}break;
			case CELL_LeftBottom:
				{
					TP_ShiftPos -= m_pCDTPParam->nTP_Mark_Distance_X;
					TP_DrivePos += m_pCDTPParam->nTP_Mark_Distance_Y;
				}break;
			}
			m_pNowIndexPos->nTP_GrabShiftPos = TP_ShiftPos;
			m_pNowIndexPos->nTP_GrabDrivePos = TP_DrivePos;

			G_MotObj.m_fnAM01ReviewSyncMove(TP_ShiftPos, TP_DrivePos, Sync_Scope_BackLight);
			
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_TP_MOVE_POS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_TP_MOVE_POS%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			m_pLiveViewObj->DrawCDTPReviewInfo(false);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_TP_REVIEW_LIGHT_ON:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_TP_REVIEW_LIGHT_ON(%03d)>", ProcStep);
			//Live View를 On한다.
			m_pLiveViewObj->LiveView(true);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_SetReviewLight;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, m_pCDTPParam->nLight_Upper);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_TP_REVIEW_LIGHT_ON%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//if(m_RecvCMD_Ret != 0)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_TP_REVIEW_LIGHT_ON%03d> Review Light Off!!!!", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_TP_LIGHT_OFF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_TP_LIGHT_OFF(%03d)>", ProcStep);
			G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, 0 ,0);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_TP_LIGHT_OFF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_TP_LIGHT_OFF%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	
	case SEQ_STEP_TP_REVIEW_AF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_TP_REVIEW_AF(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_AF;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 3);//One AF Try

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_TP_REVIEW_AF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)//AF On
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_TP_REVIEW_AF%03d> AF On!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_TP_REVIEW_GRAB:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_TP_REVIEW_GRAB(%03d)>", ProcStep);
			//Live View를 On한다.
			if(m_pNowGrabImg != nullptr)
				cvReleaseImage(&m_pNowGrabImg);
			m_pNowGrabImg = nullptr;

			bool bLiveOverlay = m_pLiveViewObj->m_bSaveOverlayImage;
			m_pLiveViewObj->m_bSaveOverlayImage = false;
			m_pLiveViewObj->SaveImage(&m_pNowGrabImg);

			int TimeOutWait = 3000;//About 20sec
			while(m_pNowGrabImg == nullptr)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			m_pLiveViewObj->m_bSaveOverlayImage= bLiveOverlay;
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_TP_REVIEW_GRAB%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pNowGrabImg  ==  nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_TP_REVIEW_GRAB%03d> Grab Image Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_TP_MEASURE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_TP_MEASURE(%03d)>", ProcStep);

			CvPoint TPCenter;
			TPCenter = Find_TP_Center(m_pCDTPParam, m_pNowIndexPos, m_pNowGrabImg, m_pReviewLensOffset);
			if(TPCenter.x<0 || TPCenter.y<0 )
			{
				//G_WriteInfo(Log_Check, "<SEQ_STEP_TP_MEASURE%03d> Find CD TP Point", ProcStep);
				//fnSeqSetNextStep(SEQ_STEP_ERROR);

				G_WriteInfo(Log_Check, "TP 측정 오류 Skip");
				fnSeqSetNextStep(SEQ_STEP_TP_SAVE_RESULT);

				break;
			}
			m_pLiveViewObj->SetCDTPReviewInfo(*m_pCDTPPointCnt, m_CDTPMeasureIndex+1, m_strStickID,
				cvRect(0,0,0,0),
				cvRect(TPCenter.x-(m_pTp_GrayAlign->width/2),TPCenter.y-(m_pTp_GrayAlign->height/2),m_pTp_GrayAlign->width,m_pTp_GrayAlign->height));


			CvPoint CDisTPCenter;
			CDisTPCenter.x = TPCenter.x - m_pNowGrabImg->width/2;
			CDisTPCenter.y = TPCenter.y - m_pNowGrabImg->height/2;


			switch(m_pCDTPParam->nMeasure_Lens)
			{
			case REVIEW_LENS_2X:
				{
					m_pNowIndexPos->nTP_MeasurePointX = (int)((float)m_pNowIndexPos->nTP_GrabShiftPos + (CDisTPCenter.x*m_pReviewLensOffset->Lens02xPixelSizeX));
					m_pNowIndexPos->nTP_MeasurePointY = (int)((float)m_pNowIndexPos->nTP_GrabDrivePos + (CDisTPCenter.y*m_pReviewLensOffset->Lens02xPixelSizeY));
				}break;
			case REVIEW_LENS_10X:
				{
					m_pNowIndexPos->nTP_MeasurePointX = (int)((float)m_pNowIndexPos->nTP_GrabShiftPos + (CDisTPCenter.x*m_pReviewLensOffset->Lens10xPixelSizeX));
					m_pNowIndexPos->nTP_MeasurePointY = (int)((float)m_pNowIndexPos->nTP_GrabDrivePos + (CDisTPCenter.y*m_pReviewLensOffset->Lens10xPixelSizeY));
				}break;
			case REVIEW_LENS_20X:
				{
					m_pNowIndexPos->nTP_MeasurePointX = (int)((float)m_pNowIndexPos->nTP_GrabShiftPos + (CDisTPCenter.x*m_pReviewLensOffset->Lens20xPixelSizeX));
					m_pNowIndexPos->nTP_MeasurePointY = (int)((float)m_pNowIndexPos->nTP_GrabDrivePos + (CDisTPCenter.y*m_pReviewLensOffset->Lens20xPixelSizeY));
				}break;
			default:
				{
					m_pNowIndexPos->nTP_MeasurePointX = (int)((float)m_pNowIndexPos->nTP_GrabShiftPos + (CDisTPCenter.x*m_pReviewLensOffset->Lens10xPixelSizeX));
					m_pNowIndexPos->nTP_MeasurePointY = (int)((float)m_pNowIndexPos->nTP_GrabDrivePos + (CDisTPCenter.y*m_pReviewLensOffset->Lens10xPixelSizeY));
				}break;
			}
			
	
			cvLine(m_pNowGrabImg,	cvPoint(TPCenter.x ,0),	cvPoint(TPCenter.x,	m_pNowGrabImg->height-1), CV_RGB(255,0,0), 2);
			cvLine(m_pNowGrabImg,	cvPoint(0, TPCenter.y), cvPoint(m_pNowGrabImg->width-1, TPCenter.y), CV_RGB(255,0,0), 2);
			char strProcMsg[128];
			extern CvFont G_cvDrawFont;
			sprintf_s(strProcMsg,128,"TP Center [S:%dum , D:%dum]", m_pNowIndexPos->nTP_MeasurePointX, m_pNowIndexPos->nTP_MeasurePointY);
			cvPutText(m_pNowGrabImg, strProcMsg,  TPCenter, &G_cvDrawFont, CV_RGB(255,0,255));


			CString strCalcInfo;
			switch(m_pNowIndexPos->nPosType)
			{
			case CELL_LeftTop:
				{
					strCalcInfo.Format("%d) LeftTop: TP Center [S:%dum , D:%dum]", 
						m_CDTPMeasureIndex, m_pNowIndexPos->nTP_MeasurePointX, m_pNowIndexPos->nTP_MeasurePointY);
				}break;
			case CELL_RightTop:
				{
					strCalcInfo.Format("%d) RightTop: TP Center [S:%dum , D:%dum]", 
						m_CDTPMeasureIndex, m_pNowIndexPos->nTP_MeasurePointX, m_pNowIndexPos->nTP_MeasurePointY);
				}break;
			case CELL_RightBottom:
				{
					strCalcInfo.Format("%d) RightBottom: TP Center [S:%dum , D:%dum]", 
						m_CDTPMeasureIndex, m_pNowIndexPos->nTP_MeasurePointX, m_pNowIndexPos->nTP_MeasurePointY);
				}break;
			case CELL_LeftBottom:
				{
					strCalcInfo.Format("%d) LeftBottom: TP Center [S:%dum , D:%dum]", 
						m_CDTPMeasureIndex, m_pNowIndexPos->nTP_MeasurePointX, m_pNowIndexPos->nTP_MeasurePointY);
				}break;
			default:
				{
				}break;
			}
			G_WriteInfo(Log_Normal, "TPData : %s", strCalcInfo.GetBuffer(strCalcInfo.GetLength()));
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_TP_SAVE_RESULT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_TP_SAVE_RESULT%03d> %d", ProcStep, m_CDTPMeasureIndex);


			CTime time = CTime::GetCurrentTime();
			CString strRetFileName;
			//strRetFileName.Format("%s.%03d_%04d%02d%02d%02d%02d%02d.jpg", m_strStickID, m_CDTPMeasureIndex+1, 
			//	time.GetYear(),time.GetMonth(), time.GetDay(), time.GetHour(),time.GetMinute(), time.GetSecond());
			strRetFileName.Format("TP_%s.%03d.jpg", m_strStickID, m_CDTPMeasureIndex+1);

			CString strCDTPSavePathName;
			strCDTPSavePathName.Format("%s\\%s", m_strCDTPSavePath, strRetFileName);
			cvSaveImage(strCDTPSavePathName.GetBuffer(strCDTPSavePathName.GetLength()), m_pNowGrabImg);

			fnSeqSetNextStep();//Auto Increment
		}break;
	//case SEQ_STEP_CDTP_REVIEW_DOWN:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_REVIEW_DOWN(%03d)>", ProcStep);
	//		G_MotObj.m_fnReviewDown();
	//		int TimeOutWait = 3000;//About 30sec
	//		while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
	//		{
	//			TimeOutWait --;
	//			if(TimeOutWait<0 || m_SeqStop == true)
	//			{
	//				m_WaitCMD = 0;
	//				m_RecvCMD = 0;
	//				if(m_SeqStop== true)
	//					G_WriteInfo(Log_Worrying, "Wait CMD Stop");
	//				else
	//					G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//				break;
	//			}
	//			Sleep(10);
	//		}
	//		if(TimeOutWait<0 || m_SeqStop == true)
	//		{
	//			G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_REVIEW_DOWN%03d> Stop", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
	//		{
	//			G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
	//			fnSeqSetNextStep(SEQ_STEP_ERROR);
	//			break;
	//		}
	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
	case SEQ_STEP_CDTP_NEXT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_NEXT%03d> %dEnd", ProcStep, m_CDTPMeasureIndex+1);
			m_CDTPMeasureIndex++;
			if(m_CDTPMeasureIndex < *m_pCDTPPointCnt)
			{
				fnSeqSetNextStep(SEQ_STEP_CDTP_MOVE_POS);//Auto Increment
				break;
			}
			fnSeqSetNextStep();//Auto Increment(
		}break;
	case SEQ_STEP_CDTP_CALC_TP_VALUE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_CALC_TP_VALUE%03d> %dEnd", ProcStep, m_CDTPMeasureIndex+1);
			if(Calc_TP_Value(m_pGlassReicpeParam, *m_pCDTPPointCnt, &(*m_ppCDTPPointList)[0], m_pTP_Result)== false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_CALC_TP_VALUE%03d> Calc TP Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_END_RING_LIGHT_DOWN:
		{
			m_pLiveViewObj->DrawCDTPReviewInfo(false);
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_END_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_END_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_END_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_END_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_END_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_END_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_END_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_END_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_END_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_END_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_END_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_END_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_END_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_END_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_END_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CDTP_END_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CDTP_END_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CDTP_END_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CDTP_END_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			m_pLiveViewObj->DrawCDTPReviewInfo(false);
			G_WriteInfo(Log_Normal, "<CSeq_CDTP%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;

	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_CDTP> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}