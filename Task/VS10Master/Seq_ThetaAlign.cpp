#include "StdAfx.h"
#include "Seq_ThetaAlign.h"


CSeq_ThetaAlign::CSeq_ThetaAlign(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;

	m_RecvCMD_Ret = 0;
	m_RecvCMD = 0;
	m_WaitCMD = 0;

	m_ThetaAlignOK = false;
	m_pScopePos = nullptr;
	m_pReviewPos = nullptr;
	//m_pScopeLensOffset = nullptr;
	m_pNowLensIndex_3DScope = nullptr;

	m_GrabActionCnt= 0;
	m_pAlignImg[0] = nullptr;
	m_pAlignImg[1] = nullptr;

	m_pLiveViewObj = nullptr;
	m_pThetaAlignLiveObj = nullptr;

	m_ThetaAlignCnt = 0;
}


CSeq_ThetaAlign::~CSeq_ThetaAlign(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;

	if(m_pAlignImg[0] != nullptr)
		cvReleaseImage(&m_pAlignImg[0]);
	m_pAlignImg[0] = nullptr;

	if(m_pAlignImg[1] != nullptr)
		cvReleaseImage(&m_pAlignImg[1]);
	m_pAlignImg[1] = nullptr;
}
bool CSeq_ThetaAlign::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_ThetaAlign::fnSeqErrorStepProc()
{
	//Live View를 On한다.
	m_pLiveViewObj->LiveView(true);
	m_pThetaAlignLiveObj->SetCamLiveMode(LIVE_MODE_GRAB);

	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();

	G_WriteInfo(Log_Error, "CSeq_ThetaAlign Error Step Run( Z Axis Go to a safe position)");
	return 0;
}
void CSeq_ThetaAlign::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}

int CSeq_ThetaAlign::AlignImgProc_DetectLine(IplImage *pTarget)
{
	if(pTarget == nullptr )
		return false;

	IplImage *pGrayAlign = cvCloneImage(pTarget);//cvCreateImage(cvSize(pTarget->width, pTarget->height), IPL_DEPTH_8U, 1);
	//cvCvtColor(pTarget, pGrayAlign, CV_RGB2GRAY);
	cvThreshold(pGrayAlign, pGrayAlign, 50, 255, CV_THRESH_BINARY); 
	//cvNamedWindow("TEST_V");

	//기준 위치 구하기.(오른쪽 에서 왼쪽으로 찾는다. 블랙에서 화이트가 50%이상 감지 되는 위치.)
	int retValue = 0;
	int white_Cnt = 0;
	for(int WStep = pGrayAlign->width-1;WStep>=0 ; WStep--)
	{
		white_Cnt = 0;
		for(int HStep= 0; HStep<pGrayAlign->height; HStep++)
		{
			if(*((BYTE*)pGrayAlign->imageData+(WStep)+(HStep*pGrayAlign->width)) >= 128)
				white_Cnt++;

			//*((BYTE*)pGrayAlign->imageData+(WStep)+(HStep*pGrayAlign->width)) = 128;
		}
		if(white_Cnt>=(pGrayAlign->height/2))
		{
			retValue = WStep;
			break;
		}
		//cvShowImage("TEST_V", pGrayAlign);
		//cvWaitKey(10);
	}
	cvReleaseImage(&pGrayAlign);
	pGrayAlign = nullptr;
	return retValue;
}
int CSeq_ThetaAlign::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_ThetaAlign%03d> Sequence Start", ProcStep);
			m_ThetaAlignOK = false;


			m_ThetaAlignCnt = 0;
			//Live View를 On한다.
			m_pLiveViewObj->LiveView(false);
			m_pThetaAlignLiveObj->SetCamLiveMode(LIVE_MODE_VIEW);
			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_ALIGN_LIGHT_ON:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_ALIGN_LIGHT_ON(%03d)>", ProcStep);
			G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, m_pScopePos->nThetaAlignLightVale ,0);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_ALIGN_LIGHT_ON%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_ALIGN_LIGHT_ON%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//영상 초기화
			if(m_pAlignImg[0] != nullptr)
				cvReleaseImage(&m_pAlignImg[0]);
			m_pAlignImg[0] = nullptr;

			if(m_pAlignImg[1] != nullptr)
				cvReleaseImage(&m_pAlignImg[1]);
			m_pAlignImg[1] = nullptr;

			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_SCOPE_ALIGN_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_ALIGN_POS1(%03d)>", ProcStep);
			if(m_pAlignImg[0] == nullptr && m_pAlignImg[1] == nullptr)
			{//둘다 없을 때 Pos 1으로
				m_GrabActionCnt = 1;
				G_MotObj.m_fnScopeMultiMove(m_pScopePos->nThetaAlignPos1_Shift, m_pScopePos->nThetaAlignPos1_Drive);
			}
			else if(m_pAlignImg[0] != nullptr && m_pAlignImg[1] == nullptr)
			{//Pos1은 있고 Pos2가 없으면 2로.
				m_GrabActionCnt = 2;
				G_MotObj.m_fnScopeMultiMove(m_pScopePos->nThetaAlignPos2_Shift, m_pScopePos->nThetaAlignPos2_Drive);
			}
			else if(m_pAlignImg[0] == nullptr && m_pAlignImg[1] != nullptr)
			{//Pos2은 있고 Pos1가 없으면 1로.
				m_GrabActionCnt = 3;
				G_MotObj.m_fnScopeMultiMove(m_pScopePos->nThetaAlignPos1_Shift, m_pScopePos->nThetaAlignPos1_Drive);
			}
			else
			{//둘다 있으면  적용 하러 가자
				fnSeqSetNextStep(SEQ_STEP_Theata_Align_Verdict);//검사 하러 간다.
				break;
			}
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_ALIGN_POS1%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_ALIGN_POS1%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_GRAB:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_GRAB1(%03d)>", ProcStep);

			if(m_pThetaAlignLiveObj == nullptr)
			{
				G_WriteInfo(Log_Error,"<SEQ_STEP_SCOPE_GRAB%03d> Grabber Link Error");
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//m_SubThetaAlignView->ShowWindow(SW_SHOW);
			CString strNewImagePath;
			if(m_GrabActionCnt == 1 || m_GrabActionCnt == 3 )
			{
				//Grab Image 가져 오기
				if(m_pAlignImg[0] != nullptr)
					cvReleaseImage(&m_pAlignImg[0]);
				m_pAlignImg[0] = nullptr;
				IplImage *pNowGrabImg = 	m_pThetaAlignLiveObj->GetGrabImg();
				if(pNowGrabImg != nullptr)
				{
					m_pAlignImg[0] = cvCloneImage(pNowGrabImg);
					cvSaveImage("D:\\ThetaAlignImg_1.bmp", pNowGrabImg);
					pNowGrabImg = nullptr;
				}
				else
				{
					G_WriteInfo(Log_Error,"<SEQ_STEP_SCOPE_GRAB%03d> Grabber Error");
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			else if(m_GrabActionCnt ==2)
			{
				//Grab Image 가져 오기
				if(m_pAlignImg[1] != nullptr)
					cvReleaseImage(&m_pAlignImg[1]);
				m_pAlignImg[1] = nullptr;
				IplImage *pNowGrabImg = 	m_pThetaAlignLiveObj->GetGrabImg();
				if(pNowGrabImg != nullptr)
				{
					m_pAlignImg[1] = cvCloneImage(pNowGrabImg);
					cvSaveImage("D:\\ThetaAlignImg_2.bmp", pNowGrabImg);
					pNowGrabImg = nullptr;
				}
				else
				{
					G_WriteInfo(Log_Error,"<SEQ_STEP_SCOPE_GRAB%03d> Grabber Error");
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			fnSeqSetNextStep(SEQ_STEP_SCOPE_ALIGN_POS);//SEQ_STEP_SCOPE_AF_POS);
			break;
		}break;
	case SEQ_STEP_Theata_Align_Verdict:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_Theata_Align_Verdict(%03d)>", ProcStep);
			m_ThetaAlignCnt++;
			int Align1ImgPos = AlignImgProc_DetectLine(m_pAlignImg[0]);
			int Align2ImgPos = AlignImgProc_DetectLine(m_pAlignImg[1]);
			G_WriteInfo(Log_Check, "%d) Org ==> Img1(%d),Img2(%d)", m_ThetaAlignCnt, Align1ImgPos, Align2ImgPos);
			Align2ImgPos+=m_pScopePos->nThetaAlignPos2Offset;
			G_WriteInfo(Log_Check, "%d) Offset ==> Img1(%d),Img2(%d)", m_ThetaAlignCnt, Align1ImgPos, Align2ImgPos);

			int DisPixel = Align2ImgPos-Align1ImgPos;
			G_WriteInfo(Log_Check, "Img1(%d),Img2(%d)==>%d", Align1ImgPos, Align2ImgPos, DisPixel);
			double PixelSize = 4;
			double DistanceOffset = DisPixel*PixelSize;
			if(Align1ImgPos == Align2ImgPos)
			{
				//이미지상에 정확한 위치이다.
				fnSeqSetNextStep(SEQ_STEP_ALIGN_END_LIGHT_OFF);//완료.
				break;
			}
			if(m_pScopePos->nThetaAlignPos1_Drive>0  && m_pScopePos->nThetaAlignPos2_Drive>0)
			{
				double CorrectionsOffset = 0.0;
				double TanA = (DistanceOffset/(abs(m_pScopePos->nThetaAlignPos2_Drive - m_pScopePos->nThetaAlignPos1_Drive)-THETA_ALIGN_ROTATE_CENTER_OFFSET));//밑변(거리).

				double CorrectionsAngle = (180.0/3.1415926535897)*TanA;
				int ThetaMot_Pulse = (int)(CorrectionsAngle/THETA_MOT_PULS_PER_ANGLE);
				//지정 차이보다 작으면 성공으로 간주
				if(abs(ThetaMot_Pulse)<THETA_MOT_INPOS_PULSE_CNT)
				{
					fnSeqSetNextStep(SEQ_STEP_ALIGN_END_LIGHT_OFF);//완료.
					break;
				}
				else
				{
#ifdef  DIRECT_TT01_ALINGN_MOVE
					//////////////////////////////////////////////////////////////////////////
					int NowThetaPos = G_MotInfo.m_pMotState->nMotionPos[AXIS_TT01_ThetaAlign];
					G_MotObj.m_fnTT01AlignMove(NowThetaPos+ThetaMot_Pulse);
#else
					m_WaitCMD = nBiz_Seq_Stick_Align_Value_Ack;
					m_RecvCMD = 0;
					//보정치 전달.
					m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Align_Value, 1,
						(USHORT)sizeof(int), (UCHAR*)&ThetaMot_Pulse);//정상처리.
#endif			
				}
			}
			else
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_Theata_Align_Verdict%03d> Calc Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;

		case SEQ_STEP_Stick_Align_Value_Ack:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_Stick_Align_Value_Ack(%03d)>", ProcStep);
				int TimeOutWait = 3000;//About 20sec
#ifdef  DIRECT_TT01_ALINGN_MOVE
				//////////////////////////////////////////////////////////////////////////
				while(G_MotObj.m_MotCMD_STATUS[TT01_THETA_ALIGN_MOVE].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<TT01_THETA_ALIGN_MOVE%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[TT01_THETA_ALIGN_MOVE].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<TT01_THETA_ALIGN_MOVE%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
#else
				
				//////////////////////////////////////////////////////////////////////////
				while(m_WaitCMD != m_RecvCMD)
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						m_RecvCMD_Ret = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_AF%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
#endif
				
				//영상 초기화
				if(m_pAlignImg[0] != nullptr)
					cvReleaseImage(&m_pAlignImg[0]);
				m_pAlignImg[0] = nullptr;

				if(m_pAlignImg[1] != nullptr)
					cvReleaseImage(&m_pAlignImg[1]);
				m_pAlignImg[1] = nullptr;
				//////////////////////////////////////////////////////////////////////////
				//영상을 취득하자.
				fnSeqSetNextStep(SEQ_STEP_SCOPE_GRAB);
			}break;


		case SEQ_STEP_ALIGN_END_LIGHT_OFF:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_ALIGN_END_LIGHT_OFF(%03d)>", ProcStep);
				G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, 0 ,0);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_ALIGN_END_LIGHT_OFF%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_ALIGN_END_LIGHT_OFF%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_ALIGN_END_SCOPE_SAFETY_Z:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_ALIGN_END_SCOPE_SAFETY_Z(%03d)>", ProcStep);
				//m_SubThetaAlignView->ShowWindow(SW_HIDE);
				//2)Grip Tension Sol BWD
				G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_ALIGN_END_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_ALIGN_END_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;

		case SEQ_STEP_ALIGN_END_SCOPE_SAFETY_XY:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_ALIGN_END_SCOPE_SAFETY_XY(%03d)>", ProcStep);

				G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_ALIGN_POS1%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_ALIGN_END_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				//마지막 Result 기록.
				m_ThetaAlignOK = true;
				//Gripper로 Stick을 물고 있자.
				m_WaitCMD = 0;
				m_RecvCMD = 0;
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Align_End, 1);//정상처리.
				fnSeqSetNextStep();//Auto Increment
			}break;
			
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			G_WriteInfo(Log_Normal, "<CSeq_ThetaAlign%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");

			//Live View를 On한다.
			m_pLiveViewObj->LiveView(true);
			m_pThetaAlignLiveObj->SetCamLiveMode(LIVE_MODE_GRAB);
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_ThetaAlign> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}