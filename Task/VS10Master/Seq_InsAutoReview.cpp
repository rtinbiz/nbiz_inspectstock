#include "StdAfx.h"
#include "Seq_InsAutoReview.h"

static CvFont G_DefectFont; 

void PutImage_TextDefect(IplImage *pMainMap, CvPoint NamePos,char*text, CvScalar FontColor, CvScalar BackColor, CvScalar RectColor)
{
	//	extern CvFont		G_ImageInfoFont;
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_DefectFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= NamePos.x-5;
	StartTxt.y	= NamePos.y+baseline;
	EndTxt.x	= NamePos.x+text_size.width+5;
	EndTxt.y	= NamePos.y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, BackColor);

	cvPutText(pMainMap, text, NamePos, &G_DefectFont, FontColor);

	cvRectangle(pMainMap, StartTxt, EndTxt, RectColor);
}
CSeq_InsAutoReview::CSeq_InsAutoReview(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;

	m_RecvCMD_Ret =-1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;

	m_pScopePos = nullptr;
	m_pReviewPos = nullptr;

	m_pbInspectMicOrMac = nullptr;
	m_pMergeData_MicQ = nullptr;
	m_pMergeData_MacQ = nullptr;

	m_AutoReviewIndex = 0;
	m_AutoReviewTotal = 0;

	//m_AutoReviewPosShift = 0;
	//m_AutoReviewPosDrive = 0;
	m_pGlassReicpeParam = nullptr;
	m_pHWRecipeParam = nullptr;

	m_pNowGrabImg = nullptr;
	//m_pNowSmallImg = nullptr;
	m_pLiveViewObj = nullptr;

	m_pNowReviewData = nullptr;

	float hscale      = 0.8f;
	float vscale      = 0.8f;
	float italicscale = 0.0f;
	int  thickness    = 1;
	cvInitFont (&G_DefectFont, CV_FONT_HERSHEY_SIMPLEX , hscale, vscale, italicscale, thickness, CV_AA);


}

CSeq_InsAutoReview::~CSeq_InsAutoReview(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;

	if(m_pNowGrabImg != nullptr)
		cvReleaseImage(&m_pNowGrabImg);
	m_pNowGrabImg = nullptr;

	//if(m_pNowSmallImg != nullptr)
	//	cvReleaseImage(&m_pNowSmallImg);
	//m_pNowSmallImg = nullptr;
}

bool CSeq_InsAutoReview::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_InsAutoReview::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();

	m_pLiveViewObj->DrawAutoReviewInfo(false);

	G_WriteInfo(Log_Error, "CSeq_InsAutoReview Error Step Run");
	return 0;
}
void CSeq_InsAutoReview::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}

void CSeq_InsAutoReview::WriteDefectInfo(int nTotalcnt, int CntIndex, CString strBoxID, CString strStickID, IplImage *pTarget, COORD_DINFO *pNowReviewData, CvPoint MachinePos)
{
	char strData[128];
	memset(strData, 0x00, 128);

	CvPoint nTextPos;
	nTextPos.x = 10;
	nTextPos.y = 35;
	sprintf_s(strData,128,"(%d/%d)Box ID:%s , StickID:%s", CntIndex, nTotalcnt, strBoxID.GetBuffer(strBoxID.GetLength()), strStickID.GetBuffer(strStickID.GetLength()));
	PutImage_TextDefect(pTarget, nTextPos, strData, CV_RGB(0,0,0), CV_RGB(255,255,0), CV_RGB(255,255,0));
	nTextPos.y += 35;
	sprintf_s(strData,128,"Type:%d", pNowReviewData->DefectType);
	PutImage_TextDefect(pTarget, nTextPos, strData, CV_RGB(0,0,0), CV_RGB(255,255,0), CV_RGB(255,255,0));
	nTextPos.y += 35;
	sprintf_s(strData,128,"Cood=> X:%d, Y:%d, W:%d, H:%d", pNowReviewData->CoodStartX, pNowReviewData->CoodStartY, pNowReviewData->CoodWidth, pNowReviewData->CoodHeight);
	PutImage_TextDefect(pTarget, nTextPos, strData, CV_RGB(0,0,0), CV_RGB(255,255,0), CV_RGB(255,255,0));
	nTextPos.y += 35;
	sprintf_s(strData,128,"Machine=> X:%d, Y:%d", MachinePos.x, MachinePos.y);
	PutImage_TextDefect(pTarget, nTextPos, strData, CV_RGB(0,0,0), CV_RGB(255,255,0), CV_RGB(255,255,0));
	nTextPos.y += 35;
	sprintf_s(strData,128,"AVG:%d, Min:%d, Max:%d - Cnt(%d)", pNowReviewData->nDValueAvg, pNowReviewData->nDValueMin, pNowReviewData->nDValueMax, pNowReviewData->nDefectCount);
	PutImage_TextDefect(pTarget, nTextPos, strData, CV_RGB(0,0,0), CV_RGB(255,255,0), CV_RGB(255,255,0));
}

int CSeq_InsAutoReview::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_InsAutoReview%03d> Sequence Start", ProcStep);


			char strReadData[256];
			
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_Process_Data, Key_NowRunBoxID, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_strBoxID.Format("%s", strReadData);
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_strStickID.Format("%s", strReadData);
			if(m_strBoxID.GetLength()<5)
				m_strBoxID.Format("None");
			if(m_strStickID.GetLength()<5)
				m_strStickID.Format("None");
			//임시저장 위치 지정.
			
			//m_strAutoReviewSavePath.Format("%s\\%s_%s_AutoReview\\", VS10Master_Result_PATH, m_strBoxID, m_strStickID);
			if(*m_pbInspectMicOrMac == true)
				m_strAutoReviewSavePath.Format("%s\\%s_AReviewMic\\", VS10Master_Result_PATH, m_strStickID);
			else
				m_strAutoReviewSavePath.Format("%s\\%s_AReviewMac\\", VS10Master_Result_PATH, m_strStickID);

			G_fnCheckDirAndCreate(m_strAutoReviewSavePath.GetBuffer(m_strAutoReviewSavePath.GetLength()));
			G_ClearFolder(m_strAutoReviewSavePath.GetBuffer(m_strAutoReviewSavePath.GetLength()));

			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_STICK_STATE_CHECK:
		{//Stick이 측정 가능 상태 인지 체크 한다.

			if(G_CheckStickProc_StaticState() == ST_PROC_NG)
			{
				G_WriteInfo(Log_Check, "SEQ_STEP_STICK_STATE_CHECK ST_PROC_NG", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			m_pLiveViewObj->SetAutoRevewInfo(0, 0, 
				m_strBoxID, m_strStickID, nullptr, 
				cvPoint(0, 0));

			m_pLiveViewObj->DrawAutoReviewInfo(true);


			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SET_LENS_AUTORIVEW:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SET_LENS_AUTORIVEW(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Func_ReviewLens;
			m_RecvCMD = 0;

			if(*m_pbInspectMicOrMac == true)
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, m_pReviewPos->nAutoReviewMic_LensIdex, nBiz_Unit_Zero);
			else
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, m_pReviewPos->nAutoReviewMac_LensIdex, nBiz_Unit_Zero);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SET_LENS_AUTORIVEW%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(*m_pbInspectMicOrMac == true)
			{
				//if(m_RecvCMD_Ret != m_pReviewPos->nAutoReviewMic_LensIdex)
				//{//TT01이 들어온 상태가 아니다.
				//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SET_LENS_AUTORIVEW%03d> Reve Lens Check Error!!!!", ProcStep);
				//	fnSeqSetNextStep(SEQ_STEP_ERROR);
				//	break;
				//}
			}
			else
			{
				//if(m_RecvCMD_Ret != m_pReviewPos->nAutoReviewMac_LensIdex)
				//{//TT01이 들어온 상태가 아니다.
				//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SET_LENS_AUTORIVEW%03d> Reve Lens Check Error!!!!", ProcStep);
				//	fnSeqSetNextStep(SEQ_STEP_ERROR);
				//	break;
				//}
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SET_LIGHT_AUTORIVEW:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SET_LIGHT_AUTORIVEW(%03d)>", ProcStep);

			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_SetReviewLight;
			m_RecvCMD = 0;
			
			if(*m_pbInspectMicOrMac == true)
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, m_pReviewPos->nAutoReviewMic_Light);
			else
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, m_pReviewPos->nAutoReviewMac_Light);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SET_LIGHT_AUTORIVEW%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//조명값 변경은 신경 Off
			if(*m_pbInspectMicOrMac == true)
			{
				//if(m_RecvCMD_Ret != m_pReviewPos->nAutoReviewMic_Light)
				//{
				//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SET_LIGHT_AUTORIVEW%03d> Review On(%d)!!!!", m_RecvCMD_Ret);
				//	fnSeqSetNextStep(SEQ_STEP_ERROR);
				//	break;
				//}
			}
			else
			{
				//if(m_RecvCMD_Ret != m_pReviewPos->nAutoReviewMac_Light)
				//{
				//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_SET_LIGHT_AUTORIVEW%03d> Review On(%d)!!!!", m_RecvCMD_Ret);
				//	fnSeqSetNextStep(SEQ_STEP_ERROR);
				//	break;
				//}
			}
			
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SET_BACK_LIGHT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SET_BACK_LIGHT(%03d)>", ProcStep);
			if(*m_pbInspectMicOrMac == true)
				G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, m_pReviewPos->nAutoReviewMic_Backlight ,0);
			else
				G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, m_pReviewPos->nAutoReviewMac_Backlight ,0);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SET_BACK_LIGHT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SET_BACK_LIGHT%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_DFECT_DATA_CHECK:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_DFECT_DATA_CHECK(%03d)>", ProcStep);
			if(*m_pbInspectMicOrMac == true)
			{
				if(m_pMergeData_MicQ->m_bDataUpdateEnd == false)//Data를 받는 중이라면.대기.
				{
					int TimeOutWait = 3000;//About 30sec
					while(m_pMergeData_MicQ->m_bDataUpdateEnd == false )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_DFECT_DATA_CHECK%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
				}

				if(m_pMergeData_MicQ->QGetCnt() == 0)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_DFECT_DATA_CHECK%03d> None Defect", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_CNT);
					break;
				}
				//첫 위치로 이동.
				m_AutoReviewIndex = 0;
				m_AutoReviewTotal = m_pMergeData_MicQ->QGetCnt();
				m_pMergeData_MicQ->QGetMoveFirstNode();
			}
			else
			{
				if(m_pMergeData_MacQ->m_bDataUpdateEnd == false)//Data를 받는 중이라면.대기.
				{
					int TimeOutWait = 3000;//About 30sec
					while(m_pMergeData_MacQ->m_bDataUpdateEnd == false )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_DFECT_DATA_CHECK%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
				}

				if(m_pMergeData_MacQ->QGetCnt() == 0)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_DFECT_DATA_CHECK%03d> None Defect", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_CNT);
					break;
				}
				//첫 위치로 이동.
				m_AutoReviewIndex = 0;
				m_AutoReviewTotal = m_pMergeData_MacQ->QGetCnt();
				m_pMergeData_MacQ->QGetMoveFirstNode();
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	
	

	case SEQ_STEP_DFECT_MOVE_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_DFECT_MOVE_POS(%03d)>", ProcStep);
			//첫 위치로 이동.
			//IplImage **ppSmallImgeLoad = nullptr;
			if(*m_pbInspectMicOrMac == true)
				m_pNowReviewData = m_pMergeData_MicQ->QGetNextNodeData();
			else
				m_pNowReviewData = m_pMergeData_MacQ->QGetNextNodeData();

			if( m_pNowReviewData == nullptr)
			{
				G_WriteInfo(Log_Normal, "Auto Review End", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_END_REVIEW_DATA_RESORT);
				break;
			}
			if(*m_pbInspectMicOrMac == true)
			{
				if(m_pReviewPos->nAutoReviewMic_Cnt > 0)
				{//Cnt가 0보다 작거나 같으면 All Review이다.
					if(m_AutoReviewIndex>=m_pReviewPos->nAutoReviewMic_Cnt)
					{//지정 개수만큼 취득 하였으면 나가자.
						G_WriteInfo(Log_Normal, "Auto Review End", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_END_REVIEW_DATA_RESORT);
						break;
					}
				}
			}
			else
			{
				if(m_pReviewPos->nAutoReviewMac_Cnt > 0)
				{//Cnt가 0보다 작거나 같으면 All Review이다.
					if(m_AutoReviewIndex>=m_pReviewPos->nAutoReviewMac_Cnt)
					{//지정 개수만큼 취득 하였으면 나가자.
						G_WriteInfo(Log_Normal, "Auto Review End", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_END_REVIEW_DATA_RESORT);
						break;
					}
				}
			}
			

			m_AutoReviewIndex++;

			//중심 좌표계에서 머신 좌표계로 변환.
			//m_AutoReviewPosShift = (m_pGlassReicpeParam->CenterPointX + m_pNowReviewData->DefectData.CoodStartX)+(m_pNowReviewData->DefectData.CoodWidth/2);
			//m_AutoReviewPosDrive = (m_pGlassReicpeParam->CenterPointY + m_pNowReviewData->DefectData.CoodStartY*(-1))+(m_pNowReviewData->DefectData.CoodHeight/2);

			//화면에 정보를 표시 하자.
			//m_pLiveViewObj->SetAutoRevewInfo(m_pReviewPos->nAutoReviewMic_Cnt, m_AutoReviewIndex,
			//	m_strBoxID, m_strStickID, &m_pNowReviewData->DefectData, 
			//	cvPoint(m_AutoReviewPosShift, m_AutoReviewPosDrive), m_pNowReviewData->m_pSmallImg);
			if(*m_pbInspectMicOrMac == true)
			{
				m_pLiveViewObj->SetAutoRevewInfo((m_AutoReviewTotal<m_pReviewPos->nAutoReviewMic_Cnt?m_AutoReviewTotal:m_pReviewPos->nAutoReviewMic_Cnt), 
					m_AutoReviewIndex,
					m_strBoxID, m_strStickID, &m_pNowReviewData->DefectData, 
					cvPoint(m_pNowReviewData->m_MachinePosX, m_pNowReviewData->m_MachinePosY), m_pNowReviewData->m_pSmallImg);
			}
			else
			{
				m_pLiveViewObj->SetAutoRevewInfo((m_AutoReviewTotal<m_pReviewPos->nAutoReviewMac_Cnt?m_AutoReviewTotal:m_pReviewPos->nAutoReviewMac_Cnt), 
					m_AutoReviewIndex,
					m_strBoxID, m_strStickID, &m_pNowReviewData->DefectData, 
					cvPoint(m_pNowReviewData->m_MachinePosX, m_pNowReviewData->m_MachinePosY), m_pNowReviewData->m_pSmallImg);
			}

			/////////////////////////////////////////////////////////////////////////////////////
			int TimeOutWait = 3000;//About 30sec
			if((*m_pbInspectMicOrMac == true && m_pReviewPos->nAutoReviewMic_Backlight > 0) ||
				(*m_pbInspectMicOrMac == false && m_pReviewPos->nAutoReviewMac_Backlight > 0))
			{//0이 아니면 사용 하겠다는 것임으로 Backlight를 따라다니게 한다.
				//G_MotObj.m_fnAM01ReviewSyncMove(m_AutoReviewPosShift, m_AutoReviewPosDrive, Sync_Scope_BackLight);
				G_MotObj.m_fnAM01ReviewSyncMove(m_pNowReviewData->m_MachinePosX, m_pNowReviewData->m_MachinePosY, Sync_Scope_BackLight);
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_DFECT_MOVE_POS%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_DFECT_MOVE_POS%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			else
			{
				G_MotObj.m_fnScanMultiMove(m_pNowReviewData->m_MachinePosX, m_pNowReviewData->m_MachinePosY);
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_DFECT_MOVE_POS%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_DFECT_MOVE_POS%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_DFECT_REVIEW_POS_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_DFECT_REVIEW_POS_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Review);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_DFECT_REVIEW_POS_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_DFECT_REVIEW_POS_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_UP:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_UP(%03d)>", ProcStep);
			G_MotObj.m_fnReviewUp();
			int TimeOutWait = 6000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_UP].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_UP%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_UP].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_UP%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_DFECT_AF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_DFECT_AF(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_AF;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 3);//One AF Try

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_DFECT_AF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//if(m_RecvCMD_Ret != 1)//AF OK
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_DFECT_AF%03d> AF Ok Error!!!!", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_DFECT_IMG_GRAB:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_DFECT_IMG_GRAB(%03d)>", ProcStep);
			//Live View를 On한다.
			if(m_pNowGrabImg != nullptr)
				cvReleaseImage(&m_pNowGrabImg);
			m_pNowGrabImg = nullptr;

			bool bLiveOverlay = m_pLiveViewObj->m_bSaveOverlayImage;
			m_pLiveViewObj->m_bSaveOverlayImage = false;
			m_pLiveViewObj->SaveImage(&m_pNowGrabImg);

			int TimeOutWait = 3000;//About 20sec
			while(m_pNowGrabImg == nullptr)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			m_pLiveViewObj->m_bSaveOverlayImage= bLiveOverlay;
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_DFECT_IMG_GRAB%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pNowGrabImg  ==  nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_DFECT_IMG_GRAB%03d> Grab Image Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_DFECT_SAVE_RESULT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_DFECT_SAVE_RESULT%03d> %d", ProcStep, m_AutoReviewIndex);
			//Review Image Defect Data Write(In Image)
			WriteDefectInfo(m_AutoReviewTotal, m_pNowReviewData->m_SizeSortIndex+1,/*m_AutoReviewIndex,*/ m_strBoxID, m_strStickID, m_pNowGrabImg, &m_pNowReviewData->DefectData, 
				cvPoint(m_pNowReviewData->m_MachinePosX, m_pNowReviewData->m_MachinePosY));
			//cvNamedWindow("SaveSmallImg");
			//Small Image Write
			if(m_pNowReviewData->m_pSmallImg != nullptr)
			{
				IplImage *pSmallImg = cvCreateImage(cvSize(m_pNowReviewData->m_pSmallImg->width, m_pNowReviewData->m_pSmallImg->height), IPL_DEPTH_8U, 3);
				cvCvtColor(m_pNowReviewData->m_pSmallImg, pSmallImg, CV_GRAY2BGR);
				CvRect DestPos;
				DestPos.x =m_pNowGrabImg->width - m_pNowReviewData->m_pSmallImg->width-3;
				DestPos.y =m_pNowGrabImg->height - m_pNowReviewData->m_pSmallImg->height-3;
				DestPos.width = m_pNowReviewData->m_pSmallImg->width;
				DestPos.height = m_pNowReviewData->m_pSmallImg->height;
				cvSetImageROI(m_pNowGrabImg, DestPos);
				cvCopyImage(pSmallImg,m_pNowGrabImg);
				cvResetImageROI(m_pNowGrabImg);
				//cvShowImage("SaveSmallImg", pSmallImg);
				//cvWaitKey(300);
				cvReleaseImage(&pSmallImg);
				pSmallImg= nullptr;
			}
			//if(m_pNowSmallImg != nullptr)
			//	cvReleaseImage(&m_pNowSmallImg);
			//m_pNowSmallImg = nullptr;
			//strCDTPSavePathName.Format("%s\\AutoReview_%s_%s_%03d.jpg", strAutoReviewSavePath,strBoxID, strStickID, m_AutoReviewIndex );
			CTime time = CTime::GetCurrentTime();
			CString strRetFileName;
			if(*m_pbInspectMicOrMac == true)//Image File명은 Micro 다음 Macro 순으로 지정 한다.
			{
				strRetFileName.Format("%s.%03d_%04d%02d%02d%02d%02d%02d.jpg", m_strStickID, 
					m_pNowReviewData->m_SizeSortIndex+1,
					time.GetYear(),time.GetMonth(), time.GetDay(), time.GetHour(),time.GetMinute(), time.GetSecond());
			}
			else
			{
				strRetFileName.Format("%s.%03d_%04d%02d%02d%02d%02d%02d.jpg", m_strStickID, 
					m_pNowReviewData->m_SizeSortIndex+1 +m_pMergeData_MicQ->QGetCnt(),
					time.GetYear(),time.GetMonth(), time.GetDay(), time.GetHour(),time.GetMinute(), time.GetSecond());
			}

			//Review Image File Path Write
			m_pNowReviewData->m_strReviewFileName.Format("%s", strRetFileName);

			CString strRetSavePath;
			strRetSavePath.Format("%s\\%s", 	m_strAutoReviewSavePath, strRetFileName);
			cvSaveImage(strRetSavePath.GetBuffer(strRetSavePath.GetLength()), m_pNowGrabImg);

			m_pNowReviewData = nullptr;
			fnSeqSetNextStep(SEQ_STEP_DFECT_MOVE_POS);//Auto Increment
		}break;
	case SEQ_STEP_END_REVIEW_DATA_RESORT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_REVIEW_DATA_RESORT(%03d)>", ProcStep);
			if(*m_pbInspectMicOrMac == true)
			{
				if(m_pMergeData_MicQ != nullptr)
				{
					//File에 기록 할때 순서를 정렬 시키기위해서.
					m_pMergeData_MicQ->QSortBySize(false);
				}
				else
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_END_REVIEW_DATA_RESORT%03d> Data Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			else
			{
				if(m_pMergeData_MacQ != nullptr)
				{
					//File에 기록 할때 순서를 정렬 시키기위해서.
					m_pMergeData_MacQ->QSortBySize(false);
				}
				else
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_END_REVIEW_DATA_RESORT%03d> Data Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_END_REVIEW_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_END_SET_LIGHT_AUTORIVEW:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SET_LIGHT_AUTORIVEW(%03d)>", ProcStep);
			G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, 0 ,0);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_END_SET_LIGHT_AUTORIVEW%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SET_LIGHT_AUTORIVEW%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			if(m_pLiveViewObj->m_pAutoReviewSmallImg !=nullptr)
				cvReleaseImage(&m_pLiveViewObj->m_pAutoReviewSmallImg);
			m_pLiveViewObj->m_pAutoReviewSmallImg = nullptr;

			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			m_pLiveViewObj->DrawAutoReviewInfo(false);
			G_MotObj.m_fnSetSignalTower(STWOER_MODE2_IDLE1);
			G_WriteInfo(Log_Normal, "<CSeq_InsAutoReview%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_InsAutoReview> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}