#pragma once
//#pragma comment (lib, "opengl32.lib")
//#pragma comment (lib, "glu32.lib")
//#pragma comment (lib, ".\\3D\\glut32.lib")

#include ".\3D\gl\gl.h"
#include ".\3D\gl\glu.h"
#include ".\3D\OpenGL3D.h"
// CMapViewDlg 대화 상자입니다.


class CMapViewDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMapViewDlg)

public:
	CMapViewDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMapViewDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MAP_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);


	COpenGL3D m_3DViewObj;
	//////////////////////////////////////////////////////////////////////////
	BOOL m_LButtonDown;
	BOOL m_RButtonDown;
	CPoint OldMovePoint;

	//////////////////////////////////////////////////////////////////////////
	//Obj Pos Draw
	void SetDeviceDistance(ObjDistance *pCamToObjDis);

	//Real to Draw 변환
	//////////////////////////////////////////////////////////////////////////
	//Offset(review Cam To Draw)

	void SetReviewToDraw(int ShiftAxis, int DriveAxis);
	void SetDrawToReview(int *pShiftAxis, int *pDriveAxis);
	void SetReview_DrawToHWPos(float ShiftAxis, float DriveAxis, int *pShiftAxis, int *pDriveAxis);
	//Offset(Scope Cam To Draw)

	void SetScopeToDraw(int ShiftAxis, int DriveAxis);
	void SetDrawToScope(int *pShiftAxis, int *pDriveAxis);
	void SetScope_DrawToHWPos(float ShiftAxis, float DriveAxis, int *pShiftAxis, int *pDriveAxis);
	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);

	virtual void PostNcDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnStnClickedStViewMap();
	afx_msg void OnBnClickedBtViewDefault();



	int m_ReviewShiftMin;
	int m_ReviewShiftMax;
	int m_RingLightShiftMin;
	int m_RingLightShiftMax;

	int m_ScopeShiftMin;
	int m_ScopeShiftMax;
};
