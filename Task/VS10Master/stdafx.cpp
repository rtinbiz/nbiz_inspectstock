
// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// VS10Master.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

CMCC_Command G_MccCommand;

CXListBox *G_pLogList = nullptr;
CInterServerInterface  *G_pServerInterface = nullptr;


HWND G_CIM_I_value = 0;
//CIM Interfac
CCimInterfaceSocket *G_pCIMInterface = nullptr;
////측정 Data 합산.
//CDataFormat G_MeasureStickResultObj;

void  G_WriteInfo(ListColor nColorType, const char* pWritLog, ...)
{

	char bufLog[LOG_TEXT_MAX_SIZE];
	memset(bufLog, 0x00, LOG_TEXT_MAX_SIZE);
	//////////////////////////////////////////////////////////////////////////
	va_list vargs;
	va_start(vargs, pWritLog);
	int LenStr = vsprintf_s((char*)bufLog, LOG_TEXT_MAX_SIZE-1,pWritLog, (va_list)vargs);
	va_end(vargs);

	if(G_pServerInterface != nullptr)
	{
		int uLogLevel = 0;
		switch(nColorType)
		{
		case Log_Normal:		uLogLevel = LOG_LEVEL_4; 	break;
		case Log_Check:		uLogLevel = LOG_LEVEL_2; 	break;
		case Log_Worrying:	uLogLevel = LOG_LEVEL_3; 	break;
		case Log_Error:		
			uLogLevel = LOG_LEVEL_1; 	
			//Alarm Set
			break;
		case Log_Info:			uLogLevel = LOG_LEVEL_2; 	break;
		case Log_Exception:	uLogLevel = LOG_LEVEL_3; 	break;
		}
		G_pServerInterface->m_fnPrintLog(TASK_10_Master, uLogLevel, bufLog);
	}

	if(G_pLogList != nullptr)
	{
		if(G_pLogList->GetCount() > LIST_ALARM_COUNT)
			G_pLogList->DeleteString(0);
		CXListBoxAddString(G_pLogList, nColorType, bufLog);
		G_pLogList->SetScrollPos(SB_VERT , G_pLogList->GetCount(), TRUE);
		G_pLogList->SetTopIndex(G_pLogList->GetCount() - 1);	
	}
}

void  G_WriteInfo_FromCIM(ListColor nColorType, const char* pWritLog, ...)
{

	char bufLog[LOG_TEXT_MAX_SIZE];
	memset(bufLog, 0x00, LOG_TEXT_MAX_SIZE);
	//////////////////////////////////////////////////////////////////////////
	va_list vargs;
	va_start(vargs, pWritLog);
	int LenStr = vsprintf_s((char*)bufLog, LOG_TEXT_MAX_SIZE-1,pWritLog, (va_list)vargs);
	va_end(vargs);

	if(G_pServerInterface != nullptr)
	{
		int uLogLevel = 0;
		switch(nColorType)
		{
		case Log_Normal:		uLogLevel = LOG_LEVEL_4; 	break;
		case Log_Check:		uLogLevel = LOG_LEVEL_2; 	break;
		case Log_Worrying:	uLogLevel = LOG_LEVEL_3; 	break;
		case Log_Error:		uLogLevel = LOG_LEVEL_1; 	break;
		case Log_Info:			uLogLevel = LOG_LEVEL_2; 	break;
		case Log_Exception:	uLogLevel = LOG_LEVEL_3; 	break;
		}
		G_pServerInterface->m_fnPrintLog(TASK_10_Master, uLogLevel, bufLog);
	}
}
SubDlg_CIMInterface *G_pCIMSendObj = nullptr;
CAlarmClearDlg *G_pAlarmSendObj = nullptr;

void G_AlarmAdd(int AlamID)
{
	if(G_pAlarmSendObj!= nullptr && G_pCIMSendObj != nullptr)
	{


		G_pAlarmSendObj->m_fnAlarmListAdd(AlamID);
	}
}
COLORREF G_Color_WinBack = RGB(64, 64, 64);//= RGB(192, 192, 192);

COLORREF	G_Color_On = RGB(128,240,128);
COLORREF	G_Color_Off = RGB(128,128,128);
COLORREF	G_Color_LightOn = RGB(250,128,128);

COLORREF G_Color_Lock = RGB(240,128,128);
COLORREF G_Color_Unlock = RGB(128,128,128);

COLORREF G_Color_Open = RGB(128,240,128);
COLORREF G_Color_Close= RGB(128,128,240);



COLORREF G_CSignalTower_Red_ON = RGB(255,0,0);
COLORREF G_CSignalTower_Red_OFF=RGB(150,80,80);

COLORREF G_CSignalTower_Yellow_ON = RGB(255,255,0);
COLORREF G_CSignalTower_Yellow_OFF= RGB(100,100,50);

COLORREF G_CSignalTower_Green_ON = RGB(0,255,0);
COLORREF G_CSignalTower_Green_OFF= RGB(80,150,80);

COLORREF G_CSignalTower_Buzzer_ON = RGB(240,128,128);
COLORREF G_CSignalTower_Buzzer_OFF= RGB(180,180,180);


EQ_RUN_MODE G_NowEQRunMode = Master_NONE;
STMOTION_STATUS G_MOTION_STATUS[MOTION_COUNT];
CMotCtrlObj G_MotObj;

TASK_State G_VS10TASK_STATE = TASK_STATE_NONE;
TASK_State G_VS11TASK_STATE = TASK_STATE_NONE;
TASK_State G_VS13TASK_STATE = TASK_STATE_NONE;
TASK_State G_VS21TASK_STATE = TASK_STATE_NONE;
TASK_State G_VS24TASK_STATE = TASK_STATE_NONE;
TASK_State G_VS31TASK_STATE = TASK_STATE_NONE;
TASK_State G_VS32TASK_STATE = TASK_STATE_NONE;

CFont			G_TaskStateFont;
CFont			G_TaskStateSmallFont;
CFont			G_RecipeFileFont;
CFont			G_NormalFont;
CFont			G_GridFont;
CFont			G_GridSmallFont;
CFont			G_NumberFont;
CFont			G_SmallFont;
CFont			G_MidFont;

SMem_Handle					G_MotInfo;
bool OpenMotionMem(SMem_Handle *pTargetMem)
{
	if(pTargetMem == nullptr)
		return false;
	pTargetMem->hFMap = ::OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, MOT_MEM_SHARE);
	if(!pTargetMem->hFMap) {

		pTargetMem->hFMap = CreateFileMapping(pTargetMem->hFile, NULL, PAGE_READWRITE,	0, pTargetMem->nTSize, MOT_MEM_SHARE);
		if(!pTargetMem->hFMap) 
		{
			G_WriteInfo(Log_Error, "1) Mot_SMP를 생성할 수 없습니다.");
			return false;
		}
		else
			G_WriteInfo(Log_Check, "1) Mot_SMP를 신규 생성 하였습니다.");
	}
	else if(GetLastError() == ERROR_ALREADY_EXISTS)
	{
		return true;
	}
	pTargetMem->m_pMotState = (STMACHINE_STATUS *)::MapViewOfFile(pTargetMem->hFMap,FILE_MAP_ALL_ACCESS,0,0, pTargetMem->nTSize);
	if(!pTargetMem->m_pMotState)
	{
		CloseHandle(pTargetMem->hFMap);
		G_WriteInfo(Log_Error, "2) Mot_SMP를 생성할 수 없습니다.");
		return false;
	}
	return true;
}
bool CloseMotionMem(SMem_Handle *pTargetMem)
{
	if(pTargetMem == nullptr)
		return false;

	if(pTargetMem->m_pMotState)
		UnmapViewOfFile(pTargetMem->m_pMotState);
	if(pTargetMem->hFMap)
		CloseHandle(pTargetMem->hFMap);
	pTargetMem->hFile = INVALID_HANDLE_VALUE;
	pTargetMem->hFMap = INVALID_HANDLE_VALUE;
	pTargetMem->m_pMotState = nullptr;
	return true;
}



void DelAllItemGridRow(CGridCtrl *pTargetGrid)
{
	int DelItemCnt = pTargetGrid->GetRowCount();
	if(DelItemCnt>1)
	{
		for(int DelIndex = DelItemCnt-1; DelIndex > 0; DelIndex--)
		{
			pTargetGrid->DeleteRow(DelIndex);
		}
	}
	pTargetGrid->Refresh();
}
void NextCellEditMove(CGridCtrl *pTargetGrid)
{
	CCellRange GSelectItem = pTargetGrid->GetSelectedCellRange();
	int RowMin = GSelectItem.GetMinRow();
	int ColPos = -1;

	if(RowMin>0 && RowMin<pTargetGrid->GetRowCount())
	{
		for(int ColStep =0;ColStep<pTargetGrid->GetColumnCount();ColStep++)
		{
			if(pTargetGrid->IsItemEditing(RowMin, ColStep) == TRUE)
			{
				ColPos = ColStep;
				break;
			}

		}
		if(ColPos == -1)
			return;
		RowMin +=1;
		if(RowMin<pTargetGrid->GetRowCount())
		{
			pTargetGrid->SetSelectedRange(RowMin,ColPos,RowMin,ColPos,FALSE,TRUE);
			pTargetGrid->SetFocusCell(RowMin,ColPos);
		}

		pTargetGrid->Refresh();
	}
}

void DelSelectedItemGridRow(CGridCtrl *pTargetGrid)
{
	if(pTargetGrid!=NULL)
	{
		int TotalCnt = pTargetGrid->GetRowCount();
		if(TotalCnt>pTargetGrid->GetFixedColumnCount())
		{

			int *pDelItems = new int[TotalCnt];
			memset(pDelItems, 0x00, sizeof(int)*TotalCnt);

			CCellRange SelectItem = pTargetGrid->GetSelectedCellRange();
			int DelMin = SelectItem.GetMinRow();
			int DelMax = SelectItem.GetMaxRow();
			int DelCnt = 0;
			//Select Cell을 찾는다.(큰 Index부터 찾는다.)
			for(int DelIndex = DelMax;DelIndex>=DelMin; DelIndex--)
			{
				if(((pTargetGrid->GetItemState(DelIndex, 0)) & GVIS_SELECTED) == GVIS_SELECTED)
				{
					pDelItems[DelCnt++] = DelIndex;
				}
			}
			//찾은 Cell을 일괄 삭제 한다.
			for(int DelIndex = 0; DelIndex<DelCnt; DelIndex++)
			{
				pTargetGrid->DeleteRow(pDelItems[DelIndex]);
			}

			if(pDelItems != NULL)
				delete [] pDelItems;
			pTargetGrid->Refresh();
		}
	}
}

bool G_CheckStickProc_StaticState()
{
	CTime ptrNowTime = CTime::GetCurrentTime();
	G_WriteInfo(Log_Check, ">>>====SEQ_STEP_STICK_STATE_CHECK=====Start");
	G_WriteInfo(Log_Check, ">>>===== %04d/%02d/%02d - %02d:%02d:%02d", ptrNowTime.GetYear(), ptrNowTime.GetMonth(),ptrNowTime.GetDay(),
		ptrNowTime.GetHour(),ptrNowTime.GetMinute(),ptrNowTime.GetSecond());
	//1)Stick 유무 체크
	if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 0 && 
		G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 0 )
	{
		G_WriteInfo(Log_Error, ">>>1) Error Stick Check");
		return ST_PROC_NG;
	}
	else
	{
		char strReadData[256];
		CString strBoxID, strStickID;
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_Process_Data, Key_NowRunBoxID, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		strBoxID.Format("%s", strReadData);
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		strStickID.Format("%s", strReadData);
		G_WriteInfo(Log_Check, ">>> 1) Box ID :%s", strBoxID);
		G_WriteInfo(Log_Check, ">>> 2) Stick ID :%s", strStickID);
	}

	//2) Jig는 나와 있어야 하고(측정위치에)
	int JigMeasurePos = GetPrivateProfileInt(Section_GripperPos, Key_Gripper_Jig_MeasurePos, 0, VS10Master_PARAM_INI_PATH);
	if(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_JigInOut] - JigMeasurePos) > PosCheckInpos)
	{
		G_WriteInfo(Log_Error, ">>> 3, 4) Error JigMeasurePos Pos");
		return ST_PROC_NG;
	}
	else
	{
		G_WriteInfo(Log_Check, ">>> 3) Jig Measure Pos :%d", JigMeasurePos);
		G_WriteInfo(Log_Check, ">>> 4) Jig Now Pos :%d", G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_JigInOut]);
	}

	//3) Gripper Up/Down은 측정 위치에 있어야 한다.
	int GripperMeasurePos = GetPrivateProfileInt(Section_GripperPos, Key_Gripper_ZPos_Measure, 0, VS10Master_PARAM_INI_PATH);
	if(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - GripperMeasurePos) > PosCheckInpos)
	{
		G_WriteInfo(Log_Error, ">>> 5, 6) Error GripperMeasurePos Pos");
		return ST_PROC_NG;
	}
	else
	{
		G_WriteInfo(Log_Check, ">>> 5) Gripper(Up/Down) Measure Pos :%d", JigMeasurePos);
		G_WriteInfo(Log_Check, ">>> 6) Gripper(Up/Down) Now Pos :%d", G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn]);
	}

	//4) TT01 Drive위치 확인.
	if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
	{
		
		G_WriteInfo(Log_Error, ">>> 7) Error m_TT01_NowPos Posz");
		return ST_PROC_NG;
	}
	else
		G_WriteInfo(Log_Check, ">>> 7) Turn Table Now Pos :%d", G_MotInfo.m_pMotState->nMotionPos[AXIS_TT01_TableDrive]);

	//5) Gripper Open상태 체크
	if(G_MotObj.m_fnGetAM01State_CySol_TEN_GRIPPER1() != SOL_DOWN ||
		G_MotObj.m_fnGetAM01State_CySol_TEN_GRIPPER2() != SOL_DOWN ||
		G_MotObj.m_fnGetAM01State_CySol_TEN_GRIPPER3() != SOL_DOWN ||
		G_MotObj.m_fnGetAM01State_CySol_TEN_GRIPPER4() != SOL_DOWN)
	{
		G_WriteInfo(Log_Error, ">>> 8) Error Gripper Open Posz");
		return ST_PROC_NG;
	}
	else
	{
		G_WriteInfo(Log_Check, ">>> 8) Stick Gripper All Close");
	}

	G_WriteInfo(Log_Check, ">>>=====SEQ_STEP_STICK_STATE_CHECK=====End");
	return ST_PROC_OK;
}

bool G_CheckStickJig_InOut()
{
	CTime ptrNowTime = CTime::GetCurrentTime();
	G_WriteInfo(Log_Check, ">>>====SEQ_STEP_STICK_JIG_INOUT_CHECK=====Start");
	G_WriteInfo(Log_Check, ">>>===== %04d/%02d/%02d - %02d:%02d:%02d", ptrNowTime.GetYear(), ptrNowTime.GetMonth(),ptrNowTime.GetDay(),
		ptrNowTime.GetHour(),ptrNowTime.GetMinute(),ptrNowTime.GetSecond());
	
	//1)스틱 감지 상태체크(스틱이 감지 상태이면)
	if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1 || 
		G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1 )
	{
		//Gripper Z축 높이는 올라가 있어야 한다.
		int GripperTT01OutPos = GetPrivateProfileInt(Section_GripperPos, Key_Gripper_ZPos_TT01Out, 0, VS10Master_PARAM_INI_PATH);
		if(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - GripperTT01OutPos) > PosCheckInpos)
		{
			G_WriteInfo(Log_Error, ">>> 2) Error Stick Detect");
			return ST_PROC_NG;
		}
	}
	else
	//2)스틱 감지 상태체크스틱이 감지 되어 있지 않다면.(최소한 Gripper Sol은 BWD상태이어야 한다.
	{
		int SolStateLeft = SOL_NONE;
		int SolStateRight = SOL_NONE;
		if((G_MotInfo.m_pMotState->stGripperStatus.In08_Sn_LeftTensionCylinderForward == 1 && G_MotInfo.m_pMotState->stGripperStatus.In09_Sn_LeftTensionCylinderBackward == 0))
			SolStateLeft = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In08_Sn_LeftTensionCylinderForward == 0 && G_MotInfo.m_pMotState->stGripperStatus.In09_Sn_LeftTensionCylinderBackward == 1))
			SolStateLeft = SOL_DOWN;
		else
			SolStateLeft = SOL_NONE;

		if((G_MotInfo.m_pMotState->stGripperStatus.In20_Sn_RightTensionCylinderForward == 1 && G_MotInfo.m_pMotState->stGripperStatus.In21_Sn_RightTensionCylinderBackward == 0) )
			SolStateRight = SOL_UP;
		else if((G_MotInfo.m_pMotState->stGripperStatus.In20_Sn_RightTensionCylinderForward == 0 && G_MotInfo.m_pMotState->stGripperStatus.In21_Sn_RightTensionCylinderBackward == 1))
			SolStateRight = SOL_DOWN;
		else
		{
			SolStateRight = SOL_NONE;
		}
		if(SolStateLeft == SolStateRight &&  SolStateLeft!= SOL_DOWN)//Sol BWD상태
		{//Sol상태 이상.
			G_WriteInfo(Log_Error, ">>> 2) Error Stick Cylinder FWD");
			return ST_PROC_NG;
		}

	}
	//3) TT01 Drive위치 확인.
	if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
	{
		G_WriteInfo(Log_Error, ">>> 3) Error m_TT01_NowPos");
		return ST_PROC_NG;
	}

	//4)Review Sol Down
	{
		int SolState = SOL_NONE;
		if((G_MotInfo.m_pMotState->stEmissionTable.In13_Sn_ReviewCylinderUp == 1 && G_MotInfo.m_pMotState->stEmissionTable.In14_Sn_ReviewCylinderDown == 0) )
			SolState = SOL_UP;
		else if((G_MotInfo.m_pMotState->stEmissionTable.In13_Sn_ReviewCylinderUp == 0 && G_MotInfo.m_pMotState->stEmissionTable.In14_Sn_ReviewCylinderDown == 1))
			SolState = SOL_DOWN;
		else if((G_MotInfo.m_pMotState->stEmissionTable.In13_Sn_ReviewCylinderUp == 1 && G_MotInfo.m_pMotState->stEmissionTable.In14_Sn_ReviewCylinderDown == 1))
			SolState = SOL_ERROR_BOTH_ON;
		else if((G_MotInfo.m_pMotState->stEmissionTable.In13_Sn_ReviewCylinderUp == 0 && G_MotInfo.m_pMotState->stEmissionTable.In14_Sn_ReviewCylinderDown == 0))
			SolState = SOL_ERROR_BOTH_OFF;
		else
			SolState = SOL_NONE;

		if(SolState != SOL_DOWN)
		{
			G_WriteInfo(Log_Error, ">>> 4) Error Review Sol Down");
			return ST_PROC_NG;
		}
	}
	//5)Ring Light Sol Down
	{
		int SolState = SOL_NONE;
		if((G_MotInfo.m_pMotState->stEmissionTable.In11_Sn_RingLightCylinderUp == 1 && G_MotInfo.m_pMotState->stEmissionTable.In12_Sn_RingLightCylinderDown == 0) )
			SolState = SOL_UP;
		else if((G_MotInfo.m_pMotState->stEmissionTable.In11_Sn_RingLightCylinderUp == 0 && G_MotInfo.m_pMotState->stEmissionTable.In12_Sn_RingLightCylinderDown == 1))
			SolState = SOL_DOWN;
		else if((G_MotInfo.m_pMotState->stEmissionTable.In11_Sn_RingLightCylinderUp == 1 && G_MotInfo.m_pMotState->stEmissionTable.In12_Sn_RingLightCylinderDown == 1))
			SolState = SOL_ERROR_BOTH_ON;
		else if((G_MotInfo.m_pMotState->stEmissionTable.In11_Sn_RingLightCylinderUp == 0 && G_MotInfo.m_pMotState->stEmissionTable.In12_Sn_RingLightCylinderDown == 0))
			SolState = SOL_ERROR_BOTH_OFF;
		else
		{
			SolState = SOL_NONE;
		}
		if(SolState != SOL_DOWN)
		{
			G_WriteInfo(Log_Error, ">>> 5) Error Ring Light Sol Down");
			return ST_PROC_NG;
		}
	}
	//6)Review(Inspect) Z대기 위치.
	int ScanReviewStandBy = GetPrivateProfileInt(Section_ScanPos, Key_Scan_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
	if(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanUpDn]-ScanReviewStandBy)>PosCheckInpos)
	{
		G_WriteInfo(Log_Error, ">>> 6) Error ScanReviewStandBy Pos");
		return ST_PROC_NG;
	}
	//7)Space Sensor Z대기 위치.
	int SpaceSensorZPos = GetPrivateProfileInt(Section_ScopePos, Key_Space_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
	if(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_SpaceSnUpDn]-SpaceSensorZPos)>PosCheckInpos)
	{
		G_WriteInfo(Log_Error, ">>> 6) Error SpaceSensorZPos Pos");
		return ST_PROC_NG;
	}

	//8)Scope Z대기 위치.
	int ScopeUpDownStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_ZPos_Standby, 0, VS10Master_PARAM_INI_PATH);
	if(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeUpDn]-ScopeUpDownStandby) > PosCheckInpos)
	{
		G_WriteInfo(Log_Error, ">>> 6) Error ScopeUpDownStandby Pos");
		return ST_PROC_NG;
	}

	//9)Scan Drive, Shift대기 위치.
	int ScanShiftStandby = GetPrivateProfileInt(Section_ScanPos, Key_Scan_Standby_Shift, 0, VS10Master_PARAM_INI_PATH);
	int ScanDriveStandby = GetPrivateProfileInt(Section_ScanPos, Key_Scan_Standby_Drive, 0, VS10Master_PARAM_INI_PATH);
	if((abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanShift]-ScanShiftStandby) > PosCheckInpos) ||
		(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanDrive]-ScanDriveStandby) > PosCheckInpos) )
	{
		G_WriteInfo(Log_Error, ">>> 6) Error ScanShiftStandby, ScanDriveStandby Pos");
		return ST_PROC_NG;
	}

	//10)Scope Drive, Shift대기 위치.
	int ScopeShiftStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_Standby_Shift, 0, VS10Master_PARAM_INI_PATH);
	int ScopeDriveStandby = GetPrivateProfileInt(Section_ScopePos, Key_Scope_Standby_Drive, 0, VS10Master_PARAM_INI_PATH);
	if((abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeShift]-ScopeShiftStandby) > PosCheckInpos) ||
		(abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeDrive]-ScopeDriveStandby) > PosCheckInpos) )
	{
		G_WriteInfo(Log_Error, ">>> 6) Error ScopeShiftStandby, ScopeDriveStandby Pos");
		return ST_PROC_NG;
	}

	G_WriteInfo(Log_Check, ">>>=====SEQ_STEP_STICK_JIG_INOUT_CHECK=====End");
	return ST_PROC_OK;
}