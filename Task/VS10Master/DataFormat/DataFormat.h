#pragma once
using namespace std;

#include <map>
#include <vector>
#include <list>

#define TOTAL_TABLE_COUNT						4
#define INFO_TABLE_COUNT						2

#define TABLE_MAX_COUNT							10
#define ITEM_MAX_COUNT							100
#define ITEM_NAME_LENGTH						50

enum
{
	KEY_HEADER_INFO =						    0,
	KEY_STICK_INFO	=					        1,
	KEY_MEASURE_INFO,
	KEY_INSPECTION_INFO,
};

#define TABLE_END	 "END_TABLE"

////////////////////////////////////////////////////////////////////////// HeadInfo
#define H_ITEM				"ITEM,10"
#define H_HEADERITEM		"HEADERITEM,10"
#define H_ROW_CNT			"ROW_CNT,16"

#define D_ITEM				"ITEM"
#define D_DATA				"DATA"
#define D_HEADERITEM		"HEADERITEM"
#define D_HEADERDATA		"HEADERDATA"
#define D_ROW_CNT			"ROW_CNT"
////////////////////////////////////////////////////////////////////////// Stick Info
#define H_STICKITEM			"STICKITEM,10"
#define H_STICK_ID			"STICK_ID,30"
#define H_JIG_ID			"JIG_ID,20"
#define H_BOX_ID			"BOX_ID,40"
#define H_FLOW_ID			"FLOW_ID,10"
#define H_FLOW_NAME			"FLOW_NAME,20"
#define H_STEPID			"STEP_ID,10"
#define H_STEP_NAME			"STEP_NAME,20"
#define H_PRODTYPE			"PRODTYPE,10"
#define H_MASK_TYPE			"MASK_TYPE,10"
#define H_MASK_MODEL		"MASK_MODEL,10"
#define H_PPID				"PPID,20"
#define H_STICK_CNT			"STICK_CNT,10"
#define H_MSTICK_CNT		"MSTICK_CNT,10"
#define H_JUDGE				"JUDGE,10"
#define H_INJIG_ID			"INJIG_ID,10"
#define H_OUTJIG_ID			"OUTJIG_ID,10"
#define H_EQP_ID			"EQP_ID,20"
#define H_EQP_STATE			"EQP_STATE,10"
#define H_OPER_ID			"OPER_ID,10"
#define H_COORD				"COORD,5"
#define H_JOB_STARTTIME		"JOB_STARTTIME,15"
#define H_JOB_ENDTIME		"JOB_ENDTIME,15"

#define D_STICKITEM			"STICKITEM"
#define D_STICKDATA			"STICKDATA"
#define D_STICK_ID			"STICK_ID"
#define D_JIG_ID			"JIG_ID"
#define D_BOX_ID			"BOX_ID"
#define D_FLOW_ID			"FLOW_ID"
#define D_FLOW_NAME			"FLOW_NAME"
#define D_STEPID			"STEP_ID"
#define D_STEP_NAME			"STEP_NAME"
#define D_PRODTYPE			"PRODTYPE"
#define D_MASK_TYPE			"MASK_TYPE"
#define D_MASK_MODEL		"MASK_MODEL"
#define D_PPID				"PPID"
#define D_STICK_CNT			"STICK_CNT"
#define D_MSTICK_CNT		"MSTICK_CNT"
#define D_JUDGE				"JUDGE"
#define D_INJIG_ID			"INJIG_ID"
#define D_OUTJIG_ID			"OUTJIG_ID"
#define D_EQP_ID			"EQP_ID"
#define D_EQP_STATE			"EQP_STATE"
#define D_OPER_ID			"OPER_ID"
#define D_COORD				"COORD"
#define D_JOB_STARTTIME		"JOB_STARTTIME"
#define D_JOB_ENDTIME		"JOB_ENDTIME"

////////////////////////////////////////////////////////////////////////// Measure Info
#define H_MEASITEM			"MEASITEM,10"
#define H_POINT_NO			"POINT_NO,8"
#define H_STICK_ID			"STICK_ID,30"
#define H_BOX_ID			"BOX_ID,40"
#define H_LAYER				"LAYER,10"
#define H_COORD_X1			"COORD_X1,8"
#define H_COORD_Y1			"COORD_Y1,8"
#define H_COORD_X2			"COORD_X2,8"
#define H_COORD_Y2			"COORD_Y2,8"
#define H_JUDGE				"JUDGE,10"
#define H_RESULT			"RESULT,10"
#define H_TENSION			"TENSION,10"
#define H_MEASURE_01		"MIN,15"
#define H_MEASURE_02		"AVG,15"
#define H_MEASURE_03		"MAX,15"
#define H_MEASURE_04		"DOON_TUK,15"
#define H_MEASURE_05		"STICK_THICK,15"
#define H_MEASURE_06		"TAPERANGLE,15"
#define H_MEASURE_07		"SLIT_WIDTH_X,15"
#define H_MEASURE_08		"SLIT_WIDTH_Y,15"
#define H_MEASURE_09		"RIB_WIDTH_X,15"
#define H_MEASURE_10		"RIB_WIDTH_Y,15"
#define H_MEASURE_11		"CELL_T/P,15"
#define H_MEASURE_12		"T/P,15"
#define H_MEASURE_13		"STRAIGHTNESS_X,15"
#define H_MEASURE_14		"STRAIGHTNESS_Y,15"
#define H_MEASURE_15		"WAVE,15"
#define H_MEASURE_16		"DOON_TUK_X_L,15"
#define H_MEASURE_17		"DOON_TUK_X_R,15"
#define H_MEASURE_18		"DOON_TUK_Y_L,15"
#define H_MEASURE_19		"DOON_TUK_Y_R,15"
#define H_MEASURE_20		"STICK_THIK_X_L,15"
#define H_MEASURE_21		"STICK_THIK_X_R,15"
#define H_MEASURE_22		"STICK_THIK_Y_L,15"
#define H_MEASURE_23		"STICK_THIK_Y_R,15"
#define H_MEASURE_24		"TAPERANGLE_X_L,15"
#define H_MEASURE_25		"TAPERANGLE_X_R,15"
#define H_MEASURE_26		"TAPERANGLE_Y_L,15"
#define H_MEASURE_27		"TAPERANGLE_Y_R,15"
#define H_MEASURE_28		"MEASURE_28,15"
#define H_MEASURE_29		"MEASURE_29,15"
#define H_MEASURE_30		"MEASURE_30,15"

#define D_MEASDATA			"MEASDATA"
#define D_POINT_NO			"POINT_NO"
#define D_STICK_ID			"STICK_ID"
#define D_BOX_ID			"BOX_ID"
#define D_LAYER				"LAYER"
#define D_COORD_X1			"COORD_X1"
#define D_COORD_Y1			"COORD_Y1"
#define D_COORD_X2			"COORD_X2"
#define D_COORD_Y2			"COORD_Y2"
#define D_JUDGE				"JUDGE"
#define D_RESULT			"RESULT"
#define D_TENSION			"TENSION"
#define D_MEASURE_01		"MIN"
#define D_MEASURE_02		"AVG"
#define D_MEASURE_03		"MAX"
#define D_MEASURE_04		"DOON_TUK"
#define D_MEASURE_05		"STICK_THICK"
#define D_MEASURE_06		"TAPERANGLE"
#define D_MEASURE_07		"SLIT_WIDTH_X"
#define D_MEASURE_08		"SLIT_WIDTH_Y"
#define D_MEASURE_09		"RIB_WIDTH_X"
#define D_MEASURE_10		"RIB_WIDTH_Y"
#define D_MEASURE_11		"CELL_T/P"
#define D_MEASURE_12		"T/P"
#define D_MEASURE_13		"STRAIGHTNESS_X"
#define D_MEASURE_14		"STRAIGHTNESS_Y"
#define D_MEASURE_15		"WAVE"
#define D_MEASURE_16		"DOON_TUK_X_L"
#define D_MEASURE_17		"DOON_TUK_X_R"
#define D_MEASURE_18		"DOON_TUK_Y_L"
#define D_MEASURE_19		"DOON_TUK_Y_R"
#define D_MEASURE_20		"STICK_THIK_X_L"
#define D_MEASURE_21		"STICK_THIK_X_R"
#define D_MEASURE_22		"STICK_THIK_Y_L"
#define D_MEASURE_23		"STICK_THIK_Y_R"
#define D_MEASURE_24		"TAPERANGLE_X_L"
#define D_MEASURE_25		"TAPERANGLE_X_R"
#define D_MEASURE_26		"TAPERANGLE_Y_L"
#define D_MEASURE_27		"TAPERANGLE_Y_R"
#define D_MEASURE_28		"MEASURE_28"
#define D_MEASURE_29		"MEASURE_29"
#define D_MEASURE_30		"MEASURE_30"



////////////////////////////////////////////////////////////////////////// Inspection Info
#define H_INSITEM			"INSITEM,10"
	// #define H_POINT_NO			"POINT_NO,8"   Measure Information과 상동으로 주석처리
	// #define H_STICK_ID			"STICK_ID,30"
	// #define H_LAYER				"LAYER,10"
	// #define H_COORD_X1			"COORD_X1,8"
	// #define H_COORD_Y1			"COORD_Y1,8"
	// #define H_COORD_X2			"COORD_X2,8"
	// #define H_COORD_Y2			"COORD_Y2,8"
	// #define H_JUDGE				"JUDGE,10"
	// #define H_RESULT				"RESULT,10"
#define H_DEFECT_NAME		"DEFECT_NAME,15"
#define H_DEFECT_CODE		"DEFECT_CODE,15"
#define H_DEFECT_CNT		"DEFECT_CNT,15"
#define H_IMAGE_FILE		"IMAGE_FILE,55"
#define H_INSPECT_01		"INSPECT_01,15"
#define H_INSPECT_02		"INSPECT_02,15"
#define H_INSPECT_03		"INSPECT_03,15"
#define H_INSPECT_04		"INSPECT_04,15"
#define H_INSPECT_05		"INSPECT_05,15"
#define H_INSPECT_06		"INSPECT_06,15"
#define H_INSPECT_07		"INSPECT_07,15"
#define H_INSPECT_08		"INSPECT_08,15"
#define H_INSPECT_09		"INSPECT_09,15"
#define H_INSPECT_10		"INSPECT_10,15"
#define H_INSPECT_11		"INSPECT_11,15"
#define H_INSPECT_12		"INSPECT_12,15"
#define H_INSPECT_13		"INSPECT_13,15"
#define H_INSPECT_14		"INSPECT_14,15"
#define H_INSPECT_15		"INSPECT_15,15"
#define H_INSPECT_16		"INSPECT_16,15"
#define H_INSPECT_17		"INSPECT_17,15"
#define H_INSPECT_18		"INSPECT_18,15"
#define H_INSPECT_19		"INSPECT_19,15"
#define H_INSPECT_20		"INSPECT_20,15"
#define H_INSPECT_21		"INSPECT_21,15"
#define H_INSPECT_22		"INSPECT_22,15"
#define H_INSPECT_23		"INSPECT_23,15"
#define H_INSPECT_24		"INSPECT_24,15"
#define H_INSPECT_25		"INSPECT_25,15"
#define H_INSPECT_26		"INSPECT_26,15"
#define H_INSPECT_27		"INSPECT_27,15"
#define H_INSPECT_28		"INSPECT_28,15"
#define H_INSPECT_29		"INSPECT_29,15"
#define H_INSPECT_30		"INSPECT_30,15"

#define D_INSDATA			"INSDATA"
// #define D_POINT_NO			"POINT_NO,8" 
// #define D_STICK_ID			"STICK_ID,30"
// #define D_LAYER				"LAYER,10"
// #define D_COORD_X1			"COORD_X1,8"
// #define D_COORD_Y1			"COORD_Y1,8"
// #define D_COORD_X2			"COORD_X2,8"
// #define D_COORD_Y2			"COORD_Y2,8"
// #define D_JUDGE				"JUDGE,10"
// #define D_RESULT				"RESULT,10"
#define D_DEFECT_NAME		"DEFECT_NAME"
#define D_DEFECT_CODE		"DEFECT_CODE"
#define D_DEFECT_CNT		"DEFECT_CNT"
#define D_IMAGE_FILE		"IMAGE_FILE"
#define D_INSPECT_01		"INSPECT_01"
#define D_INSPECT_02		"INSPECT_02"
#define D_INSPECT_03		"INSPECT_03"
#define D_INSPECT_04		"INSPECT_04"
#define D_INSPECT_05		"INSPECT_05"
#define D_INSPECT_06		"INSPECT_06"
#define D_INSPECT_07		"INSPECT_07"
#define D_INSPECT_08		"INSPECT_08"
#define D_INSPECT_09		"INSPECT_09"
#define D_INSPECT_10		"INSPECT_10"
#define D_INSPECT_11		"INSPECT_11"
#define D_INSPECT_12		"INSPECT_12"
#define D_INSPECT_13		"INSPECT_13"
#define D_INSPECT_14		"INSPECT_14"
#define D_INSPECT_15		"INSPECT_15"
#define D_INSPECT_16		"INSPECT_16"
#define D_INSPECT_17		"INSPECT_17"
#define D_INSPECT_18		"INSPECT_18"
#define D_INSPECT_19		"INSPECT_19"
#define D_INSPECT_20		"INSPECT_20"
#define D_INSPECT_21		"INSPECT_21"
#define D_INSPECT_22		"INSPECT_22"
#define D_INSPECT_23		"INSPECT_23"
#define D_INSPECT_24		"INSPECT_24"
#define D_INSPECT_25		"INSPECT_25"
#define D_INSPECT_26		"INSPECT_26"
#define D_INSPECT_27		"INSPECT_27"
#define D_INSPECT_28		"INSPECT_28"
#define D_INSPECT_29		"INSPECT_29"
#define D_INSPECT_30		"INSPECT_30"

const CString G_strHeadTable[] = 
{
	_T(H_ITEM),
	_T(D_DATA),

	_T(H_HEADERITEM),
	_T(D_HEADERDATA),

	_T(H_ROW_CNT),
	_T(""),

	_T(TABLE_END),
	_T(TABLE_END),
};

const CString G_strStickTable[] = 
{
	_T(H_ITEM),
	_T(D_DATA),

	_T(H_STICKITEM),
	_T(D_STICKDATA),

	_T(H_STICK_ID),
	_T(""),	

	_T(H_JIG_ID),
	_T(""),

	_T(H_BOX_ID),
	_T(""),

	_T(H_FLOW_ID),
	_T(""),

	_T(H_FLOW_NAME),
	_T(""),

	_T(H_STEPID),
	_T(""),

	_T(H_STEP_NAME),
	_T(""),	

	_T(H_PRODTYPE),
	_T(""),

	_T(H_MASK_TYPE),
	_T(""),

	_T(H_MASK_MODEL),
	_T(""),

	_T(H_PPID),
	_T(""),

	_T(H_STICK_CNT),
	_T(""),

	_T(H_MSTICK_CNT),
	_T(""),

	_T(H_JUDGE),
	_T(""),

	_T(H_INJIG_ID),
	_T(""),

	_T(H_OUTJIG_ID),
	_T(""),

	_T(H_EQP_ID),
	_T(""),

	_T(H_EQP_STATE),
	_T(""),

	_T(H_OPER_ID),
	_T(""),

	_T(H_COORD),
	_T(""),

	_T(H_JOB_STARTTIME),
	_T(""),

	_T(H_JOB_ENDTIME),
	_T(""),

	_T(TABLE_END),
	_T(TABLE_END),
};

const CString G_strMeasureTable[] = 
{
	_T(H_ITEM),
	_T(D_DATA),

	_T(H_MEASITEM),
	_T(D_MEASDATA),

	_T(H_POINT_NO),
	_T(D_POINT_NO),

	_T(H_STICK_ID),
	_T(""),

	_T(H_BOX_ID),
	_T(""),

	_T(H_LAYER),
	_T(""),

	_T(H_COORD_X1),
	_T(""),

	_T(H_COORD_X2),
	_T(""),

	_T(H_COORD_Y1),
	_T(""),

	_T(H_COORD_Y2),
	_T(""),

	_T(H_JUDGE),
	_T(""),

	_T(H_RESULT),
	_T(""),

	_T(H_TENSION),
	_T(""),

	_T(H_MEASURE_01),
	_T(""),

	_T(H_MEASURE_02),
	_T(""),

	_T(H_MEASURE_03),
	_T(""),

	_T(H_MEASURE_04),
	_T(""),

	_T(H_MEASURE_05),
	_T(""),

	_T(H_MEASURE_06),
	_T(""),

	_T(H_MEASURE_07),
	_T(""),

	_T(H_MEASURE_08),
	_T(""),

	_T(H_MEASURE_09),
	_T(""),

	_T(H_MEASURE_10),
	_T(""),

	_T(H_MEASURE_11),
	_T(""),

	_T(H_MEASURE_12),
	_T(""),

	_T(H_MEASURE_13),
	_T(""),

	_T(H_MEASURE_14),
	_T(""),

	_T(H_MEASURE_15),
	_T(""),

	_T(H_MEASURE_16),
	_T(""),

	_T(H_MEASURE_17),
	_T(""),

	_T(H_MEASURE_18),
	_T(""),

	_T(H_MEASURE_19),
	_T(""),

	_T(H_MEASURE_20),
	_T(""),

	_T(H_MEASURE_21),
	_T(""),

	_T(H_MEASURE_22),
	_T(""),

	_T(H_MEASURE_23),
	_T(""),

	_T(H_MEASURE_24),
	_T(""),

	_T(H_MEASURE_25),
	_T(""),

	_T(H_MEASURE_26),
	_T(""),

	_T(H_MEASURE_27),
	_T(""),

	_T(H_MEASURE_28),
	_T(""),

	_T(H_MEASURE_29),
	_T(""),

	_T(H_MEASURE_30),
	_T(""),

	_T(TABLE_END),
	_T(TABLE_END),
};

const CString G_strInspectionTable[] = 
{
	_T(H_ITEM),
	_T(D_DATA),

	_T(H_INSITEM),
	_T(D_INSDATA),

	//_T(H_POINT_NO),//2015.02.05 GJJ
	//_T(D_POINT_NO),//2015.02.05 GJJ

	_T(H_STICK_ID),
	_T(""),

	_T(H_BOX_ID),
	_T(""),

	_T(H_LAYER),
	_T(""),

	_T(H_COORD_X1),
	_T(""),

	_T(H_COORD_X2),
	_T(""),

	_T(H_COORD_Y1),
	_T(""),

	_T(H_COORD_Y2),
	_T(""),

	_T(H_JUDGE),
	_T(""),

	_T(H_RESULT),
	_T(""),

	_T(H_DEFECT_NAME),
	_T(""),

	_T(H_DEFECT_CODE),
	_T(""),

	_T(H_DEFECT_CNT),
	_T(""),

	_T(H_IMAGE_FILE),
	_T(""),

	_T(H_INSPECT_01),
	_T(""),

	_T(H_INSPECT_02),
	_T(""),

	_T(H_INSPECT_03),
	_T(""),

	_T(H_INSPECT_04),
	_T(""),

	_T(H_INSPECT_05),
	_T(""),

	_T(H_INSPECT_06),
	_T(""),

	_T(H_INSPECT_07),
	_T(""),

	_T(H_INSPECT_08),
	_T(""),

	_T(H_INSPECT_09),
	_T(""),

	_T(H_INSPECT_10),
	_T(""),

	_T(H_INSPECT_11),
	_T(""),

	_T(H_INSPECT_12),
	_T(""),

	_T(H_INSPECT_13),
	_T(""),

	_T(H_INSPECT_14),
	_T(""),

	_T(H_INSPECT_15),
	_T(""),

	_T(H_INSPECT_16),
	_T(""),

	_T(H_INSPECT_17),
	_T(""),

	_T(H_INSPECT_18),
	_T(""),

	_T(H_INSPECT_19),
	_T(""),

	_T(H_INSPECT_20),
	_T(""),

	_T(H_INSPECT_21),
	_T(""),

	_T(H_INSPECT_22),
	_T(""),

	_T(H_INSPECT_23),
	_T(""),

	_T(H_INSPECT_24),
	_T(""),

	_T(H_INSPECT_25),
	_T(""),

	_T(H_INSPECT_26),
	_T(""),

	_T(H_INSPECT_27),
	_T(""),

	_T(H_INSPECT_28),
	_T(""),

	_T(H_INSPECT_29),
	_T(""),

	_T(H_INSPECT_30),
	_T(""),

	_T(TABLE_END),
	_T(TABLE_END),
};

////////////////////////////////////////////////////////////////////////// Input
const CString G_InputMeasureData[] = 
{
	_T(D_COORD_X1),
	_T(D_COORD_Y1),
	_T(D_COORD_X2),
	_T(D_COORD_Y2),

	_T(D_MEASURE_01),  //MIN
	_T(D_MEASURE_02),  //AVG
	_T(D_MEASURE_03),  //MAX
	_T(D_MEASURE_04),  //DOON_TUK
	_T(D_MEASURE_05),  //STICK_THICK
	_T(D_MEASURE_06),  //TAPERANGLE
	_T(D_MEASURE_07),  //SLIT_WIDTH_X
	_T(D_MEASURE_08),  //SLIT_WIDTH_Y
	_T(D_MEASURE_09),  //RIB_WIDTH_X
	_T(D_MEASURE_10),  //RIB_WIDTH_Y
	_T(D_MEASURE_11),  //CELL_T/P
	_T(D_MEASURE_12),  //T/P
	_T(D_MEASURE_13),  //STRAIGHTNESS_X
	_T(D_MEASURE_14),  //STRAIGHTNESS_Y
	_T(D_MEASURE_15),  //WAVE
	_T(D_MEASURE_16),  //DOON_TUK_X_L
	_T(D_MEASURE_17),  //DOON_TUK_X_R
	_T(D_MEASURE_18),  //DOON_TUK_Y_L
	_T(D_MEASURE_19),  //DOON_TUK_Y_R
	_T(D_MEASURE_20),  //STICK_THIK_X_L
	_T(D_MEASURE_21),  //STICK_THIK_X_R
	_T(D_MEASURE_22),  //STICK_THIK_Y_L
	_T(D_MEASURE_23),  //STICK_THIK_Y_R
	_T(D_MEASURE_24),  //TAPERANGLE_X_L
	_T(D_MEASURE_25),  //TAPERANGLE_X_R
	_T(D_MEASURE_26),  //TAPERANGLE_Y_L
	_T(D_MEASURE_27),  //TAPERANGLE_Y_R
};

enum MeasureInput
{
	M_COORD_X1 = 0,
	M_COORD_Y1,
	M_COORD_X2,
	M_COORD_Y2,

	//굴곡
	M_WAVE_MIN,////MIN
	M_WAVE_AVG,//AVG
	M_WAVE_MAX,//MAX

	//둔턱 측정 Data
	M_DOON_TUK,//DOON_TUK
	M_STICK_THICK,//STICK_THICK
	M_TAPERANGLE,//TAPERANGLE

	//CD TP
	M_SLIT_WIDTH_X,//SLIT_WIDTH_X
	M_SLIT_WIDTH_Y,//SLIT_WIDTH_Y
	M_RIB_WIDTH_X,//RIB_WIDTH_X
	M_RIB_WIDTH_Y,//RIB_WIDTH_Y

	M_CELL_TP,//CELL_T/P
	M_TP,//T/P
	M_STRAIGHTNESS_X,//STRAIGHTNESS_X
	M_STRAIGHTNESS_Y,//STRAIGHTNESS_Y
	
	M_WAVE,//WAVE

	//둔턱 측정 Data
	M_DOON_TUK_X_L,//DOON_TUK_X_L
	M_DOON_TUK_X_R,//DOON_TUK_X_R
	M_DOON_TUK_Y_L,//DOON_TUK_Y_L
	M_DOON_TUK_Y_R,//DOON_TUK_Y_R
	M_STICK_THIK_X_L,//STICK_THIK_X_L
	M_STICK_THIK_X_R,//STICK_THIK_X_R
	M_STICK_THIK_Y_L,//STICK_THIK_Y_L
	M_STICK_THIK_Y_R,//STICK_THIK_Y_R
	M_TAPERANGLE_X_L,//TAPERANGLE_X_L
	M_TAPERANGLE_X_R,//TAPERANGLE_X_R
	M_TAPERANGLE_Y_L,//TAPERANGLE_Y_L
	M_TAPERANGLE_Y_R,//TAPERANGLE_Y_R
};

const CString G_InputInspectData[] = 
{
	_T(D_COORD_X1),
	_T(D_COORD_Y1),
	_T(D_COORD_X2),
	_T(D_COORD_Y2),

	_T(D_DEFECT_NAME),
	_T(D_DEFECT_CODE),
	_T(D_DEFECT_CNT),	
	_T(D_IMAGE_FILE),
};

enum InpsectInput
{
	I_COORD_X1 = 0,
	I_COORD_Y1,
	I_COORD_X2,
	I_COORD_Y2,

	I_DEFECT_NAME,
	I_DEFECT_CODE,
	I_DEFECT_CNT,	
	I_IMAGE_FILE,
};


extern vector <char*> G_vecPtrObject; 

struct DataStruct
{	
	int nSize;
	char* chInfoValue;	
	vector<char*> vecDataValue;
	CString strKeyValue;
	char chItemName[ITEM_NAME_LENGTH];			

	DataStruct()
	{
		nSize = 0;
		chInfoValue = NULL;
		memset(chItemName, 0x00, ITEM_NAME_LENGTH);		
	}	
	~DataStruct()
	{	
	}

	void SetValueAlloc(int nSize, char* chData=NULL)
	{
		chInfoValue = new char[nSize+1];
		memset(chInfoValue, 0x00, nSize+1);

		if(chData != NULL)
		{
			memcpy(chInfoValue, chData, nSize);
		}
		
		G_vecPtrObject.push_back(chInfoValue);
	}

	void SetMeasureValueAlloc(int nSize, int nMeasureCount, char* chData=NULL)
	{
		CString strPoint;
 		for(int i=0; i < nMeasureCount; i++)
 		{	
			chInfoValue = new char[nSize+1];
			memset(chInfoValue, 0x00, nSize+1);

			if(chData != NULL)
			{
				
				if(!strcmp(chData,D_POINT_NO))
				{
					strPoint.Format("%d", i+1);
					memcpy(chInfoValue, strPoint, nSize);
				}
				else
					memcpy(chInfoValue, chData, nSize);
			}
		
 			vecDataValue.push_back(chInfoValue);
 		}
	}

};

/**
 *@brief SDC A3 입고검사기 Data Foramt
 *@author 최재원
 *@brief  Format 생성 순서 1. Create 함수 생성, 2. InsertDataValue함수
                           3. DataToMem 함수 생성 4. Data File Save 함수

 *@remark 1. Create함수는 모든 측정 및 검사가 끝났을 때 생성한다.
          2. InsertDataValue 함수를 써서 결과값을 넣는다.
		  3. DataToMem 함수를 호출해 Memory에 쓴다.
		  4. 결과값을 파일로 저장한다.

 *@todo   


 *@date 2014/12/08
*/
class CDataFormat
{
public:
	CDataFormat(void);
	~CDataFormat(void);	

	//! Header 및 Stick Info Data 생성 Table 0번, 1번
	void m_fnCreateInfoData();	

	//! Inspect Data 와 측정 Data 생성 Table 2번, 3번
	//! nWhere : Table Number 0 ~ 3
	//! nMeasureDataCount : MeasureData 총 Count
	void m_fnCreateInspectAndMeasureData(int nWhere, int nMeasureDataCount, CString strStikcID, CString strBoxID, CString strTension = "");	

	//! Info Data 및 결과 Data를 삽입한다.
	//! nWhere : Table Number 0 ~ 3
	//! strFindItem : 넣을 아이템 ex) ROW_CNT
	//! strValue    : 넣을 값     ex) 33
	BOOL m_fnInsertDataValue(int nWhere, CString strFindItem, CString strValue);
	
	//! Table 0, 1의 InfoData 를 StringMemory에 넣음
	void m_fnWriteInfoDataToMem();			
	//! Table 2, 3의 Measure Data와 Inspect Data를 StringMemeory에 넣음
	void m_fnWriteInsPectAndMeasureDataToMem(int nWhere);		
	//! File TotalLine
	//int m_fnGetSumFileCount();

	
	//! 결과 파일 저장
	void m_fnDataFileSave(CString strSaveRoot, CString strFlowID, CString strStepID, CString strStickID);

	map<CString, DataStruct>		 m_MapData[TOTAL_TABLE_COUNT];
	list<pair<CString, DataStruct>>  m_ListData[TOTAL_TABLE_COUNT];

	int	m_nInspectCount;
	int m_nMeasureCount;

private:
	//! 최종 File을 저장하기 위한 String Memory
	CStringArray m_strArrayMem;
	//! Data 초기화 함수 : m_fnCreateInfoData(); 안에서 호출됨
	void m_fnMapDataClear();
	//! String 빈공간을 채워주는 함수 (bFinalItem == TRUE 이면 , 를 제외한다. 현재는 적용하지 않음)
	CString m_fnGetStringRule(char* chSrcData, int nSize, BOOL bFinalItem=FALSE);	

};



