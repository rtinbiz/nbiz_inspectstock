#include "StdAfx.h"
#include "DataFormat.h"

//////////////////////////////////////////////////////////////////////////
vector <char*> G_vecPtrObject; 
//////////////////////////////////////////////////////////////////////////

CDataFormat::CDataFormat(void)
{
}


CDataFormat::~CDataFormat(void)
{
	m_fnMapDataClear();
}

void CDataFormat::m_fnMapDataClear()
{	
	vector <char*>::iterator iter;
	
	for(iter = G_vecPtrObject.begin(); iter != G_vecPtrObject.end(); ++iter)
		delete [](*iter);
	
	G_vecPtrObject.clear();

	//////////////////////////////////////////////////////////////////////////

	list< pair<CString, DataStruct>>::iterator Iter_Pos;     // 정 방향
	map<CString, DataStruct>:: iterator iter_MapPos;

	int nSizeTest = 0;

	for(int i=0; i < TOTAL_TABLE_COUNT; i++)
	{
		if(m_MapData[i].size() > 0)
		{			
			vector <char *>::iterator iter;
			for(iter_MapPos=m_MapData[i].begin(); iter_MapPos != m_MapData[i].end(); ++iter_MapPos)
			{
 				for(iter = iter_MapPos->second.vecDataValue.begin(); iter != iter_MapPos->second.vecDataValue.end(); ++iter)
 					delete [](*iter);
			}
			m_MapData[i].begin()->second.vecDataValue.clear();

		}
		
		m_MapData[i].erase( m_MapData[i].begin(), m_MapData[i].end() );
		m_MapData[i].clear();

		for(Iter_Pos=m_ListData[i].begin(); Iter_Pos!=m_ListData[i].end();){
			m_ListData[i].erase(Iter_Pos++);//
		}
 	
 		m_ListData[i].clear();
	}
}

void CDataFormat::m_fnCreateInfoData()
{		
	m_fnMapDataClear();

	DataStruct Item[ITEM_MAX_COUNT];
	int		   nItemCount = 0;

	typedef pair < CString, DataStruct> data_Pair;	
	//////////////////////////////////////////////////////////////////////////	
	int nInfoTable = INFO_TABLE_COUNT;
	int nMapCount = 0;

	CString strGetString; 
	CString strItemName, strDataName;
	int		nItemSize = 0;
	int     nFindCount = 0;	

	for(int nTable = 0; nTable < nInfoTable; nTable++)
	{
		if(nTable == KEY_HEADER_INFO)
		{
			nMapCount = sizeof(G_strHeadTable) / 4;
		}
		else if(nTable == KEY_STICK_INFO)
		{
			nMapCount = sizeof(G_strStickTable) / 4;
		}			

		for(int i=0; i < nMapCount; i++)
		{
			if(nTable == KEY_HEADER_INFO)
			{
				strGetString = G_strHeadTable[i];
			}
			else if(nTable == KEY_STICK_INFO)
			{
				strGetString = G_strStickTable[i];
			}
	
			nFindCount = strGetString.Find(",");

			if(nFindCount > 0)  // 첫번째 아이템은 ITem과 Size로 입력되어있다, 콤마로 구분
			{
				strItemName = strGetString.Left(nFindCount);
				nItemSize   = atoi( strGetString.Mid(nFindCount + 1) );

				Item[nItemCount].nSize = nItemSize;
				sprintf_s(Item[nItemCount].chItemName, "%s", strItemName);
				Item[nItemCount].strKeyValue.Format("%d_%s", nTable, Item[nItemCount].chItemName);				
			}
			else if(strGetString.Find(TABLE_END) >= 0)    // Table의 마지막일 경우
			{
				strItemName = TABLE_END;	
				sprintf_s(Item[nItemCount].chItemName, "%s", strItemName);
				Item[nItemCount].strKeyValue.Format("%d_%s", nTable, Item[nItemCount].chItemName);				
				
				Item[nItemCount].SetValueAlloc(10, strItemName.GetBuffer()); // TABLE_END, 10
				m_MapData[nTable].insert(data_Pair(Item[nItemCount].strKeyValue, Item[nItemCount]));	
				m_ListData[nTable].push_back(data_Pair(Item[nItemCount].strKeyValue, Item[nItemCount]));
				nItemCount++;	
				break;
			}
			else  // 두번째 Data
			{			
				strDataName = strGetString;		
				Item[nItemCount].SetValueAlloc(Item[nItemCount].nSize, strDataName.GetBuffer());
				m_MapData[nTable].insert(data_Pair(Item[nItemCount].strKeyValue, Item[nItemCount]));	
				m_ListData[nTable].push_back(data_Pair(Item[nItemCount].strKeyValue, Item[nItemCount]));
		
				nItemCount++;		
			}			
		}
	}	
}

void CDataFormat::m_fnCreateInspectAndMeasureData(int nWhere, int nMeasureDataCount, CString strStikcID, CString strBoxID, CString strTension)
{

	DataStruct Item[ITEM_MAX_COUNT];
	int		   nItemCount = 0;

	typedef pair < CString, DataStruct> data_Pair;	
	//////////////////////////////////////////////////////////////////////////	
	int nMapCount = 0;

	CString strGetString; 
	CString strItemName, strDataName;
	int		nItemSize = 0;
	int     nFindCount = 0;	

	nItemCount = 0;
	if(nWhere == KEY_MEASURE_INFO)
	{
		m_nMeasureCount= nMeasureDataCount;
		nMapCount = sizeof(G_strMeasureTable) / 4;
	}
	else if(nWhere == KEY_INSPECTION_INFO)
	{
		m_nInspectCount = nMeasureDataCount;
		nMapCount = sizeof(G_strInspectionTable) / 4;
	}
	else
	{
		AfxMessageBox("Error Where Index");
		return;
	}
	int nPointNum = 0;

	for(int i=0; i < nMapCount; i++)
	{

		if(nWhere == KEY_MEASURE_INFO)
		{
			strGetString = G_strMeasureTable[i];
		}
		else if(nWhere == KEY_INSPECTION_INFO)
		{
			strGetString = G_strInspectionTable[i];
		}
		
		nFindCount = strGetString.Find(",");

		if(nFindCount > 0)  // 첫번째 아이템은 ITem과 Size로 입력되어있다, 콤마로 구분
		{
			strItemName = strGetString.Left(nFindCount);
			nItemSize   = atoi( strGetString.Mid(nFindCount + 1) );

			Item[nItemCount].nSize = nItemSize;
			sprintf_s(Item[nItemCount].chItemName, "%s", strItemName);
			Item[nItemCount].strKeyValue.Format("%d_%s", nWhere, Item[nItemCount].chItemName);
		}
		else if(strGetString.Find(TABLE_END) >= 0)    // Table의 마지막일 경우
		{
			strItemName = TABLE_END;	
			Item[nItemCount].strKeyValue.Format("%d_%s", nWhere, strItemName);	
			sprintf_s(Item[nItemCount].chItemName, "%s", strItemName);
			Item[nItemCount].SetValueAlloc(10, strItemName.GetBuffer());
			m_MapData[nWhere].insert(data_Pair(Item[nItemCount].strKeyValue, Item[nItemCount]));	
			m_ListData[nWhere].push_back(data_Pair(Item[nItemCount].strKeyValue, Item[nItemCount]));
			nItemCount++;	
			break;
		}
		else  // 두번째 Data
		{			
			if(strItemName == D_STICK_ID)
			{
				strGetString = strStikcID;				
			}
			else if(strItemName == D_BOX_ID)
			{
				strGetString = strBoxID;
			}
			else if(strItemName == D_TENSION)
			{
				strGetString = strTension;
			}			

			Item[nItemCount].SetMeasureValueAlloc(Item[nItemCount].nSize, nMeasureDataCount, strGetString.GetBuffer());			

			m_MapData[nWhere].insert(data_Pair(Item[nItemCount].strKeyValue, Item[nItemCount]));	
			m_ListData[nWhere].push_back(data_Pair(Item[nItemCount].strKeyValue, Item[nItemCount]));
			nItemCount++;	
		}			
	}
	

}

void CDataFormat::m_fnWriteInfoDataToMem()
{
	list< pair<CString, DataStruct>>::iterator Iter_Pos;     // 정 방향
 
	int nLineCount = 0;
	CString strTemp, strWrite;
	BOOL	bFinalItem = FALSE;

	int nChangeIndex = 0;
	for(int nTable=0 ; nTable<INFO_TABLE_COUNT; nTable++)
	{
		for(int nChangeIndex = 0; nChangeIndex < 2; nChangeIndex++)
		{		
			nLineCount = 0;		
			strWrite = "";

			for( Iter_Pos = m_ListData[nTable].begin(); Iter_Pos != m_ListData[nTable].end(); ++Iter_Pos)
			{			
				bFinalItem = FALSE;

				int nItemColCount = 0;

				if(nTable == 0)          // Final Col Item 일 경우 , 를 제외한다.
				   nItemColCount = (sizeof(G_strHeadTable) / (sizeof(CString) * 2)) - 2;				
				else
				   nItemColCount = (sizeof(G_strStickTable) / (sizeof(CString) * 2)) - 2;				

				if(nLineCount == nItemColCount)
				{
					bFinalItem = TRUE;					
				}
				// 규칙 좌측정렬, Item 빈공간은 Space로 채운다, 데이터 항목은 ','로 분리한다.	
				// 1. 먼저 Item Name으로 Write 한다.				
				if(nChangeIndex == 0)       // Item 항목
				{
					strTemp = m_fnGetStringRule(Iter_Pos->second.chItemName, Iter_Pos->second.nSize, bFinalItem);					
				}
				else if(nChangeIndex == 1)  // Data 항목
				{
					strTemp = m_fnGetStringRule(Iter_Pos->second.chInfoValue, Iter_Pos->second.nSize, bFinalItem);
				}
					
				if(strTemp.Find(TABLE_END) > -1)    // 줄바꿈
				{
					strWrite += "\n";
					m_strArrayMem.Add(strWrite);	
			
					if(nChangeIndex == 1)					// 테이블의 종료 부분일때 
					{
					//	m_strArrayMem.Add("\n\n");					
					}
				}			
				else
					m_strArrayMem.Add(strTemp);
			
				nLineCount++;
			}		
		}
	}
}

void CDataFormat::m_fnWriteInsPectAndMeasureDataToMem(int nWhere)
{
	list<pair<CString, DataStruct>>::iterator Iter_Pos;     // 정 방향

	int nLineCount = 0;
	CString strTemp, strWrite;
	BOOL	bFinalItem = FALSE;

	int *pDataSizeFormat = NULL;	
	int nItemColCount = 0;
	int nMeasureDataCount;

	if(nWhere == KEY_MEASURE_INFO)
	{
		nMeasureDataCount = m_nMeasureCount;
		pDataSizeFormat = new int[sizeof(G_strMeasureTable) / 8 * sizeof(int)];
		memset(pDataSizeFormat, 0x00, sizeof(G_strMeasureTable) / 8 * sizeof(int));

		nItemColCount = (sizeof(G_strMeasureTable) / (sizeof(CString) * 2)) - 2;	
	}
	else if(nWhere == KEY_INSPECTION_INFO)
	{
		nMeasureDataCount = m_nInspectCount;
		pDataSizeFormat = new int[sizeof(G_strInspectionTable) / 8 * sizeof(int)];
		memset(pDataSizeFormat, 0x00, sizeof(G_strInspectionTable) / 8 * sizeof(int));

		nItemColCount = (sizeof(G_strInspectionTable) / (sizeof(CString) * 2)) - 2;	
	}
	else
	{
		AfxMessageBox("Error Where Index 확인");
		return;
	}	

	nLineCount = 0;		
	strWrite = "";

	for( Iter_Pos = m_ListData[nWhere].begin(); Iter_Pos != m_ListData[nWhere].end(); ++Iter_Pos)
	{			
		// 규칙 좌측정렬, Item 빈공간은 Space로 채운다, 데이터 항목은 ','로 분리한다.	
		// 1. 먼저 Item Name으로 Write 한다.			
		bFinalItem = FALSE;
		
		if(nLineCount == nItemColCount) // Final Col Item 일 경우 , 를 제외한다.
		{
			bFinalItem = TRUE;					
		}

		strTemp = m_fnGetStringRule(Iter_Pos->second.chItemName, Iter_Pos->second.nSize, bFinalItem);
		pDataSizeFormat[nLineCount] = Iter_Pos->second.nSize;


		if(strTemp.Find(TABLE_END) > -1)    // 줄바꿈
		{
			strWrite += "\n";
			m_strArrayMem.Add(strWrite);			
		}			
		else
			m_strArrayMem.Add(strTemp);

		nLineCount++;
	}
			
	int nItemNameCount = 0;
	
 	for(int nCount = 0; nCount < nMeasureDataCount/*EndTable 제외*/; nCount++)							// Item Count 세로
 	{
		nItemNameCount = 0;
		for( Iter_Pos = m_ListData[nWhere].begin(); Iter_Pos != m_ListData[nWhere].end(); ++Iter_Pos)		// Item Name 세로
		{
			bFinalItem = FALSE;

			if(nItemNameCount == nItemColCount)    // Final Col Item 일 경우 , 를 제외한다.
			{
				bFinalItem = TRUE;					
			}

			//for(UINT i=0; i < Iter_Pos->second.vecDataValue.size(); i++)
			//{						
				if(Iter_Pos->second.vecDataValue.size() == 0)
					continue;

				CString strData;
				strData = Iter_Pos->second.vecDataValue[nCount];
				strTemp = m_fnGetStringRule(strData.GetBuffer(), pDataSizeFormat[nItemNameCount], bFinalItem);

				m_strArrayMem.Add(strTemp);
			//}
			nItemNameCount++;			
		}
		m_strArrayMem.Add("\n");
	}
	// Data 입력 종료
	//m_strArrayMem.Add("\n\n");					

	if(pDataSizeFormat != NULL)
		delete []pDataSizeFormat;
}



CString CDataFormat::m_fnGetStringRule(char* chSrcData, int nSize, BOOL bFinalItem) 
{
	CString strRuleData;	

	strRuleData = chSrcData;
	if(nSize > 0)
	{
		if(nSize - strlen(chSrcData) >= 0)
		{
			for(UINT i =0; i< nSize - strlen(chSrcData); i++)	
				strRuleData += " ";
		}	
		else
		{
			AfxMessageBox("Size Error");
			return "Error";
		}
	}
	
	if(bFinalItem == FALSE)
		strRuleData += ",";

	return strRuleData;	
}

void CDataFormat::m_fnDataFileSave(CString strSaveRoot, CString strFlowID, CString strStepID, CString strStickID)
{
	CStdioFile stdioFile;
	CFileException ex;	

	CTime cTime = CTime::GetCurrentTime();
	CString strTime = cTime.Format(_T("%Y%m%d%H%M%S"));
	//strStickID = strStickID.Remove("_999");
	CString strStickID2 =strStickID.Left(16); //2015.02.05 GJJ
	CString strFileSavePath;
	strFileSavePath.Format("%s%s\\%s_%s.dat", strSaveRoot, strStickID, strStickID2, strTime);//2015.02.05 GJJ

	if (!stdioFile.Open(strFileSavePath, CFile::modeWrite | CFile::modeCreate| CFile::typeText, &ex))
	{		
		ex.ReportError();
		stdioFile.Close();
	}	

	for(int i=0; i< m_strArrayMem.GetSize(); i++)
	{
		stdioFile.WriteString(m_strArrayMem.GetAt(i));

	}

	m_strArrayMem.RemoveAll();

	stdioFile.Close();

	////////////////////////////////////////////////////////////////////////// Signal File 생성
	CString strSignalFileSavePath;
	strSignalFileSavePath.Format("%s%s\\%s.%s_Stick_%s_%s.dat", 
		strSaveRoot, 
		strStickID,
		strFlowID, 
		strStepID, 
		strStickID2, //2015.02.05 GJJ
		strTime);

	if (!stdioFile.Open(strSignalFileSavePath, CFile::modeWrite | CFile::modeCreate| CFile::typeBinary, &ex))
	{
		//Log("CD File Write Error");
		//ex.ReportError();		
	}			

	stdioFile.Close();

}

BOOL CDataFormat::m_fnInsertDataValue(int nWhere, CString strFindItem, CString strValue)
{	
	CString strFindIndex;

	strFindIndex.Format("%d_%s", nWhere, strFindItem);

	map< CString, DataStruct >::iterator IterPos;
	IterPos = m_MapData[nWhere].find(strFindIndex);

	if(IterPos == m_MapData[nWhere].end())	
	{
		AfxMessageBox("Error Insert Data");
		return FALSE;
	}	

	memcpy(IterPos->second.chInfoValue, strValue, strValue.GetLength()+1);
	return TRUE;
}

//int CDataFormat::m_fnGetSumFileCount()
//{
//	return m_strArrayMem.GetSize();
//}