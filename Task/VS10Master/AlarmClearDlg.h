#pragma once


// CAlarmClearDlg 대화 상자입니다.

class CAlarmClearDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CAlarmClearDlg)

public:
	CAlarmClearDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAlarmClearDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_ALARM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	HBRUSH m_AlarmDlg_Brush;
	COLORREF m_btBackColor;
	void InitCtrl_UI();


	SubDlg_CIMInterface		*  m_pSubCIMInterfaceView;
public:
	virtual BOOL OnInitDialog();	
	virtual void PostNcDestroy();
	bool m_fnAlarmListAdd(int nAlarmID);
	void m_fnAlarmRead();



	CGridCtrl m_ctrlGridAlarm;
	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btnAlarmClear;
	CRoundButton2 m_btnBuzzerStop;
	CRoundButton2 m_btnAlarmReset;
	
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnAlarmReset();
	afx_msg void OnBnClickedBtnAlarmBuzStop();
	afx_msg void OnBnClickedBtnAlarmClear();
	afx_msg void OnClose();

	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);




};
