// MapViewDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "MapViewDlg.h"
#include "afxdialogex.h"


// CMapViewDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMapViewDlg, CDialogEx)

CMapViewDlg::CMapViewDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMapViewDlg::IDD, pParent)
{
	m_ReviewShiftMin = 0;
	m_ReviewShiftMax = 0;
	m_RingLightShiftMin = 0;
	m_RingLightShiftMax = 0;

	m_ScopeShiftMin = 0;
	m_ScopeShiftMax = 0;
}

CMapViewDlg::~CMapViewDlg()
{
}

void CMapViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMapViewDlg, CDialogEx)
	ON_WM_CTLCOLOR()

	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_STN_CLICKED(IDC_ST_VIEW_MAP, &CMapViewDlg::OnStnClickedStViewMap)
	ON_BN_CLICKED(IDC_BT_VIEW_DEFAULT, &CMapViewDlg::OnBnClickedBtViewDefault)
END_MESSAGE_MAP()


// CMapViewDlg 메시지 처리기입니다.


BOOL CMapViewDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CRect ViewMapSize;
	GetDlgItem(IDC_ST_VIEW_MAP)->GetWindowRect(&ViewMapSize);
	ScreenToClient(&ViewMapSize);

	float WinH = (ViewMapSize.Width()*STAGE_MAX_STROKE_Y)/STAGE_MAX_STROKE_X;
	GetDlgItem(IDC_ST_VIEW_MAP)->SetWindowPos(nullptr, ViewMapSize.left, ViewMapSize.top,ViewMapSize.Width(),(int)WinH, SWP_NOMOVE);
	GetDlgItem(IDC_ST_VIEW_MAP)->GetWindowRect(&ViewMapSize);
	ScreenToClient(&ViewMapSize);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	//CRect WinSize;
	//this->GetWindowRect(&WinSize);
	m_3DViewObj.InitOpenGL(GetDlgItem(IDC_ST_VIEW_MAP)->GetSafeHwnd(), 440, 1190);//ViewMapSize.Width(), ViewMapSize.Height());//OpenGL Init.
	m_3DViewObj.ChangeViewSize(ViewMapSize.Width(), ViewMapSize.Height());
	m_LButtonDown = FALSE;
	m_RButtonDown = FALSE;

	m_ReviewShiftMin = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_ReviewSol_Scan_Shift_Min, 311894, Interlock_PARAM_INI_PATH);
	m_ReviewShiftMax = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_ReviewSol_Scan_Shift_Max, 412756, Interlock_PARAM_INI_PATH);
	m_RingLightShiftMin = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_RingLightSol_Scan_Shift_Min, 301894, Interlock_PARAM_INI_PATH);
	m_RingLightShiftMax = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_RingLightSol_Scan_Shift_Max, 422756, Interlock_PARAM_INI_PATH);

	m_ScopeShiftMin = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_Z_Down_Shift_Min, 22000, Interlock_PARAM_INI_PATH);
	m_ScopeShiftMax = GetPrivateProfileInt(Section_JIG_INTERLOCK, Key_Z_Down_Shift_Max, 230000, Interlock_PARAM_INI_PATH);

	int LineScanSiftOffset = GetPrivateProfileInt(Section_ReviewCamToObjDis, Key_LineScanDisX, 0, VS10Master_PARAM_INI_PATH);
	//m_RingLightShiftMin+=(LineScanSiftOffset);
	//m_RingLightShiftMax+=(LineScanSiftOffset);



	m_3DViewObj.m_fReviewShiftMin = (float)m_ReviewShiftMin/1000.0f;
	m_3DViewObj.m_fReviewShiftMax = (float)m_ReviewShiftMax/1000.0f;
	m_3DViewObj.m_fRingLightShiftMin = (float)m_RingLightShiftMin/1000.0f;
	m_3DViewObj.m_fRingLightShiftMax = (float)m_RingLightShiftMax/1000.0f;

	m_3DViewObj.m_fScopeShiftMin = (float)m_ScopeShiftMin/1000.0f;
	m_3DViewObj.m_fScopeShiftMax = (float)m_ScopeShiftMax/1000.0f;
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CMapViewDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
void CMapViewDlg::SetDeviceDistance(ObjDistance *pCamToObjDis)
{ 
	m_3DViewObj.m_pCamToObjDis = pCamToObjDis;  

	//Draw용.mm단위.
	SetReviewToDraw(0,0);
	pCamToObjDis->fReviewCamTo_LineScanX = (float)pCamToObjDis->nReviewCamTo_LineScanX/1000.0f;
	pCamToObjDis->fReviewCamTo_LineScanY = (float)pCamToObjDis->nReviewCamTo_LineScanY/1000.0f;
	pCamToObjDis->fReviewCamTo_SpaceSensorDisX = (float)pCamToObjDis->nReviewCamTo_SpaceSensorDisX/1000.0f;
	pCamToObjDis->fReviewCamTo_SpaceSensorDisY = (float)pCamToObjDis->nReviewCamTo_SpaceSensorDisY/1000.0f;

	SetScopeToDraw(0, 0);
	pCamToObjDis->fScopeCamTo_AirBlowDisX = (float)pCamToObjDis->nScopeCamTo_AirBlowDisX/1000.0f;
	pCamToObjDis->fScopeCamTo_AirBlowDisY = (float)pCamToObjDis->nScopeCamTo_AirBlowDisY/1000.0f;
	pCamToObjDis->fScopeCamTo_BackLightDisX = (float)pCamToObjDis->nScopeCamTo_BackLightDisX/1000.0f;
	pCamToObjDis->fScopeCamTo_BackLightDisY = (float)pCamToObjDis->nScopeCamTo_BackLightDisY/1000.0f;
	pCamToObjDis->fScopeCamTo_ScanSensorDisX = (float)pCamToObjDis->nScopeCamTo_ScanSensorDisX/1000.0f;
	pCamToObjDis->fScopeCamTo_ScanSensorDisY = (float)pCamToObjDis->nScopeCamTo_ScanSensorDisY/1000.0f;

	pCamToObjDis->fScopeCamTo_ThetaAlignCamDisX = (float)pCamToObjDis->nScopeCamTo_ThetaAlignCamDisX/1000.0f;
	pCamToObjDis->fScopeCamTo_ThetaAlignCamDisY = (float)pCamToObjDis->nScopeCamTo_ThetaAlignCamDisY/1000.0f;
}

void CMapViewDlg::SetReview_DrawToHWPos(float ShiftAxis, float DriveAxis, int *pShiftAxis, int *pDriveAxis)
{
	*pShiftAxis =  (int)(ShiftAxis*1000.0f) -  G_ReviewToDrawXOffset;
	*pDriveAxis =  (int)(DriveAxis*1000.0f) -  G_ReviewToDrawYOffset;
}
void CMapViewDlg::SetReviewToDraw(int ShiftAxis, int DriveAxis)
{
	m_3DViewObj.m_pCamToObjDis->fReviewCamPosX = (float)(ShiftAxis + G_ReviewToDrawXOffset)/1000.0f;
	m_3DViewObj.m_pCamToObjDis->fReviewCamPosY = (float)(DriveAxis + G_ReviewToDrawYOffset)/1000.0f;
	m_3DViewObj.RenderScreen();
}
void CMapViewDlg::SetDrawToReview(int *pShiftAxis, int *pDriveAxis)
{
	*pShiftAxis =  (int)(m_3DViewObj.m_pCamToObjDis->fReviewCamPosX*1000.0f) -  G_ReviewToDrawXOffset;
	*pDriveAxis =  (int)(m_3DViewObj.m_pCamToObjDis->fReviewCamPosY*1000.0f) -  G_ReviewToDrawYOffset;
}

void CMapViewDlg::SetScope_DrawToHWPos(float ShiftAxis, float DriveAxis, int *pShiftAxis, int *pDriveAxis)
{
	*pShiftAxis =  (int)(ShiftAxis*1000.0f) -  G_ScopeToDrawXOffset;
	*pDriveAxis =  (int)(DriveAxis*1000.0f) -  G_ScopeToDrawYOffset;
}
void CMapViewDlg::SetScopeToDraw(int ShiftAxis, int DriveAxis)
{
	m_3DViewObj.m_pCamToObjDis->fScopeCamPosX = (float)(ShiftAxis + G_ScopeToDrawXOffset)/1000.0f;
	m_3DViewObj.m_pCamToObjDis->fScopeCamPosY = (float)(DriveAxis + G_ScopeToDrawYOffset)/1000.0f;
	m_3DViewObj.RenderScreen();
}

void CMapViewDlg::SetDrawToScope(int *pShiftAxis, int *pDriveAxis)
{
	*pShiftAxis =  (int)(m_3DViewObj.m_pCamToObjDis->fScopeCamPosX*1000.0f) -  G_ScopeToDrawXOffset;
	*pDriveAxis =  (int)(m_3DViewObj.m_pCamToObjDis->fScopeCamPosY*1000.0f) -  G_ScopeToDrawYOffset;
}

HBRUSH CMapViewDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
BOOL OnlyOneMove = TRUE;
void CMapViewDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CRect ViewMapSize;
	GetDlgItem(IDC_ST_VIEW_MAP)->GetWindowRect(&ViewMapSize);
	ScreenToClient(&ViewMapSize);

	CString strNowPos;
	if(ViewMapSize.PtInRect(point) == TRUE)
	{
		OnlyOneMove = TRUE;
		//HCURSOR hCursor = AfxGetApp()->LoadStandardCursor(NULL); 
		//SetCursor(hCursor); 
		GetDlgItem(IDC_BT_TEST)->SetFocus();
		point.x -= ViewMapSize.left;
		point.y -= ViewMapSize.top;
		//strNowPos.Format("X: %0.3fum", m_3DViewObj.m_MouseXPosFromCenter);
		//GetDlgItem(IDC_ST_POS_X)->SetWindowText(strNowPos);
		//strNowPos.Format("Y: %0.3fum",m_3DViewObj.m_MouseYPosFromCenter  );
		//GetDlgItem(IDC_ST_POS_Y)->SetWindowText(strNowPos);
		int NowHWPosX = 0;
		int NowHWPosY = 0;
		SetReview_DrawToHWPos(m_3DViewObj.m_MouseXPosFromCenter, m_3DViewObj.m_MouseYPosFromCenter,
			&NowHWPosX, &NowHWPosY);
		strNowPos.Format("X: %0.3fum",(float)(NowHWPosX/1000.0f));
		GetDlgItem(IDC_ST_POS_X)->SetWindowText(strNowPos);
		strNowPos.Format("Y: %0.3fum",(float)(NowHWPosY/1000.0f));
		GetDlgItem(IDC_ST_POS_Y)->SetWindowText(strNowPos);
		SetScope_DrawToHWPos(m_3DViewObj.m_MouseXPosFromCenter, m_3DViewObj.m_MouseYPosFromCenter,
			&NowHWPosX, &NowHWPosY);
		strNowPos.Format("X: %0.3fum",(float)(NowHWPosX/1000.0f));
		GetDlgItem(IDC_ST_POS_X2)->SetWindowText(strNowPos);
		strNowPos.Format("Y: %0.3fum",(float)(NowHWPosY/1000.0f));
		GetDlgItem(IDC_ST_POS_Y2)->SetWindowText(strNowPos);


		if(m_RButtonDown || m_LButtonDown)
		{
			GLfloat x=0, y=0;
			//변활량을 전달한다.
			
			x = (float)(OldMovePoint.x - point.x);
			y = (float)(OldMovePoint.y - point.y);//*(-1.0f);
			if(m_LButtonDown)
			{
				m_3DViewObj.MoveObject(x,y);
				//float MovPosX = (((float)x *STAGE_MAX_STROKE_X)/(float)ViewMapSize.Width());
				//float MovPosY = (((float)y*STAGE_MAX_STROKE_Y)/(float)ViewMapSize.Height());
				//m_3DViewObj.MoveObject(MovPosX,MovPosY);
				OldMovePoint = point;
			}
		}
		else
		{
			//float MovPosX = (((float)point.x*STAGE_MAX_STROKE_X)/(float)ViewMapSize.Width());
			//float MovPosY = (((float)point.y*STAGE_MAX_STROKE_Y)/(float)ViewMapSize.Height());
			
			float MovPosX = (((float)point.x*STAGE_MAX_STROKE_X)/(float)ViewMapSize.Width());
			float MovPosY = (((float)point.y*STAGE_MAX_STROKE_Y)/(float)ViewMapSize.Height());
			m_3DViewObj.MouseDisByView(MovPosX,MovPosY);
			//m_3DViewObj.MouseDisByView((float)point.x - (float)ViewMapSize.Width(), (float)point.y - (float)ViewMapSize.Height() );
		}
	}
	else
	{
		//HCURSOR hCursor= AfxGetApp()->LoadStandardCursor(IDC_ARROW); 
		//SetCursor(hCursor); 

		m_LButtonDown = FALSE;
		m_RButtonDown = FALSE;
	}
	
	CDialog::OnMouseMove(nFlags, point);
}

void CMapViewDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//////////////////////////////////////////////////////////////////////////
	CRect ViewMapSize;
	GetDlgItem(IDC_ST_VIEW_MAP)->GetWindowRect(&ViewMapSize);
	ScreenToClient(&ViewMapSize);

	m_LButtonDown = TRUE;
	//OldMovePoint = point;

	//CRect ViewMapSize;
	//GetDlgItem(IDC_ST_VIEW_MAP)->GetWindowRect(&ViewMapSize);
	//ScreenToClient(&ViewMapSize);
	OldMovePoint.x = point.x -ViewMapSize.left;
	OldMovePoint.y = point.y -ViewMapSize.top;


	CString strNowPos;
	if(ViewMapSize.PtInRect(point) == TRUE)
	{
		HCURSOR hCursor = AfxGetApp()->LoadStandardCursor(NULL); 
		SetCursor(hCursor); 
		strNowPos.Format("%0.3f", m_3DViewObj.m_MouseXPosFromCenter+(STAGE_MAX_STROKE_X/2) );
		GetDlgItem(IDC_ED_POS_X)->SetWindowText(strNowPos);
		strNowPos.Format("%0.3f", m_3DViewObj.m_MouseYPosFromCenter+(STAGE_MAX_STROKE_Y/2)  );
		GetDlgItem(IDC_ED_POS_Y)->SetWindowText(strNowPos);
	}
	else
	{
		HCURSOR hCursor= AfxGetApp()->LoadStandardCursor(IDC_ARROW); 
		SetCursor(hCursor); 
	}
	CDialog::OnLButtonDown(nFlags, point);
}

void CMapViewDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_RButtonDown = TRUE;
	CDialog::OnRButtonDown(nFlags, point);
}

BOOL CMapViewDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//CRect ViewMapSize;
	//GetDlgItem(IDC_ST_VIEW_MAP)->GetWindowRect(&ViewMapSize);
	//
	//if(zDelta>0)
	//	::SetCursorPos(ViewMapSize.left+(ViewMapSize.Width()/2), ViewMapSize.top+(ViewMapSize.Height()/2));

	//ScreenToClient(&ViewMapSize);
	//CPoint NowPoint = pt;
	//ScreenToClient(&NowPoint);

	//if(ViewMapSize.PtInRect(NowPoint) == TRUE)
	//{
	//	GetDlgItem(IDC_BT_TEST)->SetFocus();
	//	if(OnlyOneMove == TRUE && zDelta>0)
	//	{
	//		NowPoint.x -= ViewMapSize.left;
	//		NowPoint.y -= ViewMapSize.top;

	//		GLfloat x=0, y=0;
	//		//변활량을 전달한다.

	//		x = (float)( NowPoint.x - (ViewMapSize.Width()/2));
	//		y = (float)(NowPoint.y -(ViewMapSize.Height()/2));//*(-1.0f);

	//		m_3DViewObj.MoveObject(x,y);

	//		OldMovePoint = NowPoint;
	//		OnlyOneMove = FALSE;
	//	}
	//	if(zDelta>0)
	//		m_3DViewObj.ZoomObject(0.5f);
	//	else
	//		m_3DViewObj.ZoomObject(-0.5f);

	//	//m_3DViewObj.MouseDisByView(NowPoint.x - ViewMapSize.Width()/2, NowPoint.y - ViewMapSize.Height()/2 );
	//}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void CMapViewDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_LButtonDown = FALSE;
	OldMovePoint = point;
	CDialog::OnLButtonUp(nFlags, point);
}

void CMapViewDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_RButtonDown = FALSE;
	CDialog::OnRButtonUp(nFlags, point);
}



void CMapViewDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


void CMapViewDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	//m_3DViewObj.ChangeViewSize(cx, cy);
	//m_3DViewObj.RenderScreen();
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CMapViewDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
	m_3DViewObj.RenderScreen();
}


void CMapViewDlg::OnStnClickedStViewMap()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMapViewDlg::OnBnClickedBtViewDefault()
{
	m_3DViewObj.SetDefValue();
}
