#pragma once
#include "d:\nbiz_inspectstock\commonheader\ui\grid64\gridctrl.h"
#include "afxwin.h"


// CSysParamDlg 대화 상자입니다.

class CSysParamDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSysParamDlg)

public:
	CSysParamDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSysParamDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SYS_PARM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	CGridCtrl m_gridSystemParam;
	CGridCtrl m_gridReviewOffset;
	CGridCtrl m_gridScopeOffset;
	CGridCtrl m_gridReviewLensOffset;
	CGridCtrl m_gridScopeLensOffset;

	void LoadSystemParam();
	void SaveSystemParam();
	afx_msg void OnBnClickedBtSave();
	afx_msg void OnBnClickedBtExit();

	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btSysRecipeSave;
	CRoundButton2 m_btSysRecipeExit;
	CRoundButton2 m_btSysMCCParamSave;
	afx_msg void OnBnClickedBtMccParamSave();
	afx_msg void OnBnClickedChMccWriteOn();
	afx_msg void OnBnClickedChMccWriteOff();
	CRoundButton2 m_btSysReviewLensCalcStart;
	CRoundButton2 m_btSysReviewLensCalcStop;
	afx_msg void OnBnClickedBtReviewLensCalcStart();
	afx_msg void OnBnClickedBtReviewLensCalcStart2();
};
