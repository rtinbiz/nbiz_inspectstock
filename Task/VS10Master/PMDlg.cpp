// PMDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "PMDlg.h"
#include "afxdialogex.h"


// CPMDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPMDlg, CDialogEx)

CPMDlg::CPMDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPMDlg::IDD, pParent)
{
	m_pSubCIMInterfaceView = nullptr;
}

CPMDlg::~CPMDlg()
{
}

void CPMDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_btnUnLockPM);
	DDX_Control(pDX, IDC_PM_MAINT, m_btnPMMaint);
	DDX_Control(pDX, IDC_PM_STAGE, m_btnPMStage);
}


BEGIN_MESSAGE_MAP(CPMDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDOK, &CPMDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_PM_MAINT, &CPMDlg::OnBnClickedMaint)
	ON_BN_CLICKED(IDC_PM_STAGE, &CPMDlg::OnBnClickedStage)
END_MESSAGE_MAP()



// CPMDlg 메시지 처리기입니다.

BOOL CPMDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	InitCtrl_UI();
	cvPMImage.Load("D:\\nBiz_InspectStock\\Data\\Resource\\VS10Master\\PM_IMAGE.jpg");

	 CRect ButtonRect;

	m_btnUnLockPM.GetWindowRect(ButtonRect);
	m_btnUnLockPM.SetWindowPos(nullptr, 1920/2-ButtonRect.Width()/2, 1080/2+ButtonRect.Height()*1, 0, 0, SWP_NOSIZE);
	m_btnPMMaint.SetWindowPos(nullptr, 1920/2-ButtonRect.Width()/2, 1080/2+ButtonRect.Height()*3, 0, 0, SWP_NOSIZE);
	m_btnPMStage.SetWindowPos(nullptr, 1920/2-ButtonRect.Width()/2, 1080/2+ButtonRect.Height()*4, 0, 0, SWP_NOSIZE);
	this->MoveWindow(0, 0, 1920,1080);
	CenterWindow();
	//CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	//pMainWnd->m_StageControlDlg.m_bStageInterlock = TRUE;
	//Invalidate();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CPMDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;

	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btnUnLockPM.GetTextColor(&tColor);
		m_btnUnLockPM.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,25,"굴림");
			tFont.lfHeight =25;
		}

		m_btnUnLockPM.SetRoundButtonStyle(&m_tButtonStyle);
		m_btnUnLockPM.SetTextColor(&tColor);
		m_btnUnLockPM.SetCheckButton(false);
		m_btnUnLockPM.SetFont(&tFont);

		m_btnPMMaint.SetRoundButtonStyle(&m_tButtonStyle);
		m_btnPMMaint.SetTextColor(&tColor);
		m_btnPMMaint.SetCheckButton(false);
		m_btnPMMaint.SetFont(&tFont);

		m_btnPMStage.SetRoundButtonStyle(&m_tButtonStyle);
		m_btnPMStage.SetTextColor(&tColor);
		m_btnPMStage.SetCheckButton(false);
		m_btnPMStage.SetFont(&tFont);

	}
}

void CPMDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.	
	int nXpos = (1920-cvPMImage.Width()) / 2;
	int nYpos = (1080-cvPMImage.Height()) / 2;

	CRect rect;
	rect.left = nXpos;
	rect.top = nYpos-200;
	rect.right = rect.left + cvPMImage.Width();
	rect.bottom = rect.top + cvPMImage.Height();	

	cvPMImage.DrawToHDC(dc, &rect);

}

HBRUSH CPMDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CPMDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}
void CPMDlg::OnBnClickedOk()
{
	if(m_pSubCIMInterfaceView != nullptr)
	{
		m_pSubCIMInterfaceView->OnBnClickedBtEqNormal();
	}
	OnOK();
}
void CPMDlg::OnBnClickedMaint()
{

}

void CPMDlg::OnBnClickedStage()
{
	//CMainWnd *pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;

	//pMainWnd->m_ServerDlg.OnBnClickedBtnMainStageControl();
}
