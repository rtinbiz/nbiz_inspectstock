// SubDlg_3DScope.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SubDlg_3DScope.h"
#include "afxdialogex.h"


// CSubDlg_3DScope 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSubDlg_3DScope, CDialogEx)

CSubDlg_3DScope::CSubDlg_3DScope(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSubDlg_3DScope::IDD, pParent)
{
	m_pServerInterface = nullptr;
	m_pScope3DParam = nullptr;
}

CSubDlg_3DScope::~CSubDlg_3DScope()
{
}

void CSubDlg_3DScope::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BT_MEASURE_INIT, m_btMeasure3DInit);
	DDX_Control(pDX, IDC_BT_MEASURE_START, m_btStartMeasure3D);
	DDX_Control(pDX, IDC_BT_3D_LENS_1, m_bt3DLensSelect[0]);
	DDX_Control(pDX, IDC_BT_3D_LENS_2, m_bt3DLensSelect[1]);
	DDX_Control(pDX, IDC_BT_3D_LENS_3, m_bt3DLensSelect[2]);
	DDX_Control(pDX, IDC_BT_3D_LENS_4, m_bt3DLensSelect[3]);
	DDX_Control(pDX, IDC_ST_TASK_STATE_3D, m_ctrStaticTask3DMeasure);
	DDX_Control(pDX, IDC_ST_PROC_STATE_3D, m_ctrStaticProc3DMeasure);
	DDX_Control(pDX, IDC_ST_MEASURE_INDEX, m_stMeasureCnt3D);
	DDX_Control(pDX, IDC_ST_SPACE_SENSOR_TARGET_POS, m_stSpaceSensorTargetZPos);
	DDX_Control(pDX, IDC_BT_MOVE_SCOPE_POS, m_btSPSensorMoveScopePos);
	DDX_Control(pDX, IDC_BT_MOVE_TARGET_ZPOS, m_btSPSensorMoveTargetZPos);
	DDX_Control(pDX, IDC_BT_MOVE_ZERO_POS, m_btSpaceSensorSafetyPosMove);
}


BEGIN_MESSAGE_MAP(CSubDlg_3DScope, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_MEASURE_INIT, &CSubDlg_3DScope::OnBnClickedBtMeasureInit)
	ON_BN_CLICKED(IDC_BT_3D_LENS_1, &CSubDlg_3DScope::OnBnClickedBt3dLens)
	ON_BN_CLICKED(IDC_BT_3D_LENS_2, &CSubDlg_3DScope::OnBnClickedBt3dLens)
	ON_BN_CLICKED(IDC_BT_3D_LENS_3, &CSubDlg_3DScope::OnBnClickedBt3dLens)
	ON_BN_CLICKED(IDC_BT_3D_LENS_4, &CSubDlg_3DScope::OnBnClickedBt3dLens)
	ON_BN_CLICKED(IDC_BT_MEASURE_START, &CSubDlg_3DScope::OnBnClickedBtMeasureStart)
	ON_BN_CLICKED(IDC_BT_MOVE_SCOPE_POS, &CSubDlg_3DScope::OnBnClickedBtMoveScopePos)
	ON_BN_CLICKED(IDC_BT_MOVE_TARGET_ZPOS, &CSubDlg_3DScope::OnBnClickedBtMoveTargetZpos)
	ON_BN_CLICKED(IDC_BT_MOVE_ZERO_POS, &CSubDlg_3DScope::OnBnClickedBtMoveZeroPos)
END_MESSAGE_MAP()


// CSubDlg_3DScope 메시지 처리기입니다.


BOOL CSubDlg_3DScope::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();

	m_ScopeAppState = ObserveSW_None;
	m_ScopeActionState = ActionObs_None;
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSubDlg_3DScope::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;

	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btMeasure3DInit.GetTextColor(&tColor);
		m_btMeasure3DInit.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btMeasure3DInit.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMeasure3DInit.SetTextColor(&tColor);
		m_btMeasure3DInit.SetCheckButton(true, true);
		m_btMeasure3DInit.SetFont(&tFont);

		m_btStartMeasure3D.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStartMeasure3D.SetTextColor(&tColor);
		m_btStartMeasure3D.SetCheckButton(true, true);
		m_btStartMeasure3D.SetFont(&tFont);

		for(int nStep = 0; nStep < 4; nStep++)
		{
			m_bt3DLensSelect[nStep].SetTextColor(&tColor);
			m_bt3DLensSelect[nStep].SetCheckButton(true,true);		
			m_bt3DLensSelect[nStep].SetFont(&tFont);
			m_bt3DLensSelect[nStep].SetRoundButtonStyle(&m_tButtonStyle);
		}
		m_btSPSensorMoveScopePos.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSPSensorMoveScopePos.SetTextColor(&tColor);
		m_btSPSensorMoveScopePos.SetCheckButton(true, true);
		m_btSPSensorMoveScopePos.SetFont(&tFont);

		m_btSPSensorMoveTargetZPos.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSPSensorMoveTargetZPos.SetTextColor(&tColor);
		m_btSPSensorMoveTargetZPos.SetCheckButton(true, true);
		m_btSPSensorMoveTargetZPos.SetFont(&tFont);


		m_btSpaceSensorSafetyPosMove.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSpaceSensorSafetyPosMove.SetTextColor(&tColor);
		m_btSpaceSensorSafetyPosMove.SetCheckButton(true, true);
		m_btSpaceSensorSafetyPosMove.SetFont(&tFont);
		

	}
	m_TaskStateColor = RGB(128, 128, 128);

	m_ctrStaticTask3DMeasure.SetFont(&G_TaskStateSmallFont);
	m_ctrStaticTask3DMeasure.SetTextColor(RGB(0, 0, 0));
	m_ctrStaticTask3DMeasure.SetBkColor(m_TaskStateColor);

	m_ctrStaticProc3DMeasure.SetFont(&G_TaskStateSmallFont);
	m_ctrStaticProc3DMeasure.SetTextColor(RGB(0, 0, 0));
	m_ctrStaticProc3DMeasure.SetBkColor(m_TaskStateColor);

	m_stMeasureCnt3D.SetFont(&G_TaskStateFont);
	m_stMeasureCnt3D.SetTextColor(RGB(0, 0, 0));
	m_stMeasureCnt3D.SetBkColor(m_TaskStateColor);

	m_stSpaceSensorTargetZPos.SetFont(&G_TaskStateSmallFont);
	m_stSpaceSensorTargetZPos.SetTextColor(RGB(0, 0, 0));
	m_stSpaceSensorTargetZPos.SetBkColor(m_TaskStateColor);
	
}
void CSubDlg_3DScope::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


HBRUSH CSubDlg_3DScope::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	switch (DLG_ID_Number)
	{
	case IDC_ST_TASK_STATE_3D:
	case IDC_ST_PROC_STATE_3D:
		return hbr;		
	default:
		break;
	}
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CSubDlg_3DScope::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSubDlg_3DScope::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


void CSubDlg_3DScope::OnBnClickedBtMeasureInit()
{
	if(m_pServerInterface != nullptr)
	{
		G_WriteInfo(Log_Info,"Measur Task Init Send");
		//m_btMeasure3DInit.SetCheck(true);
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_AlarmReset, nBiz_Unit_Zero);
		Sleep(200);
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_ManualModeON, nBiz_Unit_Zero);
			//TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_ManualModeOFF, nBiz_Unit_Zero);
		//m_btMeasure3DInit.SetCheck(false);
	}
	else
		G_WriteInfo(Log_Error,"Server Obj Link Error!!!!");

}


void CSubDlg_3DScope::OnBnClickedBt3dLens()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();

	SHORT nLensIndex=0;
	switch(wID)
	{
	case IDC_BT_3D_LENS_1: nLensIndex =1; break;
	case IDC_BT_3D_LENS_2: nLensIndex =2;  break;
	case IDC_BT_3D_LENS_3: nLensIndex =3; break;
	case IDC_BT_3D_LENS_4: nLensIndex =4;  break;

	}
	if(nLensIndex>0 && nLensIndex<=4 && m_pServerInterface != nullptr)
	{
		G_WriteInfo(Log_Info,"Select Scope Lens Index:%d", nLensIndex);

		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, nLensIndex);
	}
}
void CSubDlg_3DScope::OnBnClickedBtMeasureStart()
{
	if(m_pServerInterface != nullptr)
	{
		CString strMeasureCnt;
		m_stMeasureCnt3D.GetWindowText(strMeasureCnt);

		if(m_btStartMeasure3D.GetCheck() == false)
		{
			SEND_STARTMEASURE_DATA SendData;
			int nMCnt = atoi(strMeasureCnt)+1;
			strMeasureCnt.Format("%d", nMCnt);
			m_stMeasureCnt3D.SetWindowText(strMeasureCnt);
			
			SendData.nMaskType		= m_pScope3DParam->nStick_Type;
			SendData.nMeasureAngle	= m_pScope3DParam->nMeasure_Angle;
			SendData.fTargetThick 		= m_pScope3DParam->fStick_Thick;
			SendData.fSpaceGap 		= m_pScope3DParam->fSpaceTargetValue;
			SendData.bMeasureAll		= m_pScope3DParam->nMeasure_Area;

			//int SendData[3];
			//SendData[0] = 2;		//1:MASK_TYPE_STRIPE, 2:MASK_TYPE_DOT
			//SendData[1] = 0;		//0: 0.0Degree, 1:45.0Degree
			//SendData[2] = 45;	//Nano단위로 사용 함으로 um->Nano로 변경.
			CString strSavePath;
			strSavePath.Format(SCOPE_RESULT_SAVE_PATH);
			char *pSendData = new char[sizeof(SEND_STARTMEASURE_DATA) + strSavePath.GetLength()+1];
			memcpy(pSendData, &SendData, sizeof(SEND_STARTMEASURE_DATA) );
			memcpy((pSendData+sizeof(SEND_STARTMEASURE_DATA)), strSavePath.GetBuffer(strSavePath.GetLength()), strSavePath.GetLength());
			m_pServerInterface->m_fnSendCommand_Nrs(
				TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_StartMeasure, (USHORT)nMCnt,(USHORT)(sizeof(SEND_STARTMEASURE_DATA) + strSavePath.GetLength()) , (UCHAR*)pSendData);
			delete [] pSendData;
			m_btStartMeasure3D.SetCheck(true);
		}
		else
			G_WriteInfo(Log_Check,"Measure Run Now~~~!!!(%s)", strMeasureCnt);
	}
	
}


void CSubDlg_3DScope::OnBnClickedBtMoveScopePos()
{
	if(m_pScope3DParam!= nullptr)
	{
		if(m_btSPSensorMoveScopePos.GetCheck() == false)
		{
			CString strNewTargetValue;
			strNewTargetValue.Format("%0.3fum", m_pScope3DParam->fSpaceTargetValue);
			m_stSpaceSensorTargetZPos.SetWindowText(strNewTargetValue);

			//Send Move Command.


			m_btSPSensorMoveScopePos.SetCheck(true);
		}
		else
			G_WriteInfo(Log_Worrying,"Now Moving Axis Space Sensor ");
	
	}
	else
		G_WriteInfo(Log_Error,"Measure Parameter Link Error");
}


void CSubDlg_3DScope::OnBnClickedBtMoveTargetZpos()
{
	if(m_pScope3DParam!= nullptr)
	{
		if(m_btSPSensorMoveTargetZPos.GetCheck() == false && m_btSPSensorMoveScopePos.GetCheck() == false)
		{
			CString strNewTargetValue;
			strNewTargetValue.Format("%0.3fum", m_pScope3DParam->fSpaceTargetValue);
			m_stSpaceSensorTargetZPos.SetWindowText(strNewTargetValue);

			//Send Move Command.


			m_btSPSensorMoveTargetZPos.SetCheck(true);
		}
		else
			G_WriteInfo(Log_Worrying,"Now Moving Axis Space Sensor ");

	}
	else
		G_WriteInfo(Log_Error,"Measure Parameter Link Error");
}


void CSubDlg_3DScope::OnBnClickedBtMoveZeroPos()
{
	//1) Z축 Stop이 나가고.

	//2) Zero Pos까지 내린다.
}
