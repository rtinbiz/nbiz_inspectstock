#pragma once
#include "afxwin.h"


// CSubStickLoadDlg 대화 상자입니다.

class CSubStickLoadDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSubStickLoadDlg)

public:
	CSubStickLoadDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubStickLoadDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SUB_STICK_LOAING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	//CInterServerInterface  *m_pServerInterface;
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void SetStickInfo(CString strBoxID, CString strStickID, CString strRecipeID);
public:
	CString m_strBoxID;
	CString m_strStickID;
	CString m_strRecipeID;

	int m_SelectLoadingType;
	CString m_SelectLoadingPPID;
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	CRoundButtonStyle m_tButtonStyle;

	CRoundButton2 m_btLoading_OnlyStick;
	CRoundButton2 m_btLoading_FullInspection;
	CRoundButton2 m_btLoading_Cancel;

	afx_msg void OnBnClickedBtStickOnlyLoading();
	afx_msg void OnBnClickedBtStickLoadingAutoRun();
	afx_msg void OnBnClickedBtStickLoadingCancel();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	CComboBox m_cbRecipeSelect;
};
