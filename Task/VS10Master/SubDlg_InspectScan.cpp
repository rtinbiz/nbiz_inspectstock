// SubDlg_AreaScan3D.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SubDlg_InspectScan.h"
#include "afxdialogex.h"


// CSubDlg_InspectScan 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSubDlg_InspectScan, CDialogEx)

CSubDlg_InspectScan::CSubDlg_InspectScan(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSubDlg_InspectScan::IDD, pParent)
{
	m_pServerInterface = nullptr;

	m_pAlignCellStartX= nullptr;
	m_pAlignCellStartY= nullptr;
	m_pGlassReicpeParam = nullptr;
	m_pstMaskImageInfo_Mic = nullptr;
	m_pstCommonPara_Mic = nullptr;	
	m_pstMaskImageInfo_Mac = nullptr;
	m_pstCommonPara_Mac = nullptr;


	m_pBlockEnd= nullptr;
	m_pStartBlockTick= nullptr;
	m_pStartLineTick= nullptr;
	m_pCamScnaLineCnt= nullptr;
	m_pCamScnaImgCnt= nullptr;
	m_pStartScanLineNum= nullptr;

	
}

CSubDlg_InspectScan::~CSubDlg_InspectScan()
{
}

void CSubDlg_InspectScan::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BT_GET_NOW_POS, m_btGetNowPosCellStart);
	DDX_Control(pDX, IDC_ST_SCAN_INFO1, m_stScanInfo[0]);
	DDX_Control(pDX, IDC_ST_SCAN_INFO2, m_stScanInfo[1]);
	DDX_Control(pDX, IDC_ST_SCAN_INFO3, m_stScanInfo[2]);
	DDX_Control(pDX, IDC_ST_SCAN_INFO4, m_stScanInfo[3]);
	DDX_Control(pDX, IDC_ST_SCAN_INFO5, m_stScanInfo[4]);
	DDX_Control(pDX, IDC_ST_SCAN_LINE_CNT_11, m_stScanLineCnt);
	DDX_Control(pDX, IDC_ST_SCAN_IMG_CNT_11, m_stScanImgCnt);
	DDX_Control(pDX, IDC_ST_TOTAL_DEFECT_CNT, m_stTotalDefectCnt);
	DDX_Control(pDX, IDC_ST_PROC_END_CNT_11, m_stProcEndCnt);
	DDX_Control(pDX, IDC_ST_PROC_TICK_11, m_stProcTime);
	DDX_Control(pDX, IDC_ST_BACKUP1_S, m_stBackup1_S);
	DDX_Control(pDX, IDC_ST_WRITE_FILE1_S, m_stWriteFile1_S);
	DDX_Control(pDX, IDC_ST_TASK_11, m_stInspectTaskState);
	DDX_Control(pDX, IDC_BT_CELL_ALIGN, m_btAlignCellStartPos);
	DDX_Control(pDX, IDC_BT_UNIT_INIT, m_btInitInspector);
	DDX_Control(pDX, IDC_BT_SEND_RECIPE_PARAM, m_btSendRecipeParam);
	DDX_Control(pDX, IDC_BT_SEND_START, m_btSendStart);
	DDX_Control(pDX, IDC_BT_SEND_STOP, m_btSendStop);
	DDX_Control(pDX, IDC_ST_SCAN_INDEX, m_stNowScanLine);
	DDX_Control(pDX, IDC_BT_MOVE_SCAN_START, m_bt_MoveScanStart);
	DDX_Control(pDX, IDC_BT_MOVE_SCAN_START_ODD, m_btMoveOddScanStart);
	DDX_Control(pDX, IDC_BT_MOVE_SCAN_START_EVEN, m_btMoveEvnScanStart);
	DDX_Control(pDX, IDC_BT_MOVE_SCAN_SHIFT, m_btMoveShift);
}


BEGIN_MESSAGE_MAP(CSubDlg_InspectScan, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_UNIT_INIT, &CSubDlg_InspectScan::OnBnClickedBtUnitInit)
	ON_BN_CLICKED(IDC_BT_SEND_RECIPE_PARAM, &CSubDlg_InspectScan::OnBnClickedBtSendRecipeParam)
	ON_BN_CLICKED(IDC_BT_GET_NOW_POS, &CSubDlg_InspectScan::OnBnClickedBtGetNowPos)
	ON_BN_CLICKED(IDC_BT_CELL_ALIGN, &CSubDlg_InspectScan::OnBnClickedBtCellAlign)
	ON_BN_CLICKED(IDC_BT_SEND_START, &CSubDlg_InspectScan::OnBnClickedBtSendStart)
	ON_BN_CLICKED(IDC_BT_SEND_STOP, &CSubDlg_InspectScan::OnBnClickedBtSendStop)
	ON_BN_CLICKED(IDC_BT_MOVE_SCAN_START, &CSubDlg_InspectScan::OnBnClickedBtMoveScanStart)
	ON_BN_CLICKED(IDC_BT_MOVE_SCAN_START_ODD, &CSubDlg_InspectScan::OnBnClickedBtMoveScanStartOdd)
	ON_BN_CLICKED(IDC_BT_MOVE_SCAN_SHIFT, &CSubDlg_InspectScan::OnBnClickedBtMoveScanShift)
	ON_BN_CLICKED(IDC_BT_MOVE_SCAN_START_EVEN, &CSubDlg_InspectScan::OnBnClickedBtMoveScanStartEven)
END_MESSAGE_MAP()


// CSubDlg_InspectScan 메시지 처리기입니다.


BOOL CSubDlg_InspectScan::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSubDlg_InspectScan::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btGetNowPosCellStart.GetTextColor(&tColor);
		m_btGetNowPosCellStart.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btGetNowPosCellStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_btGetNowPosCellStart.SetTextColor(&tColor);
		m_btGetNowPosCellStart.SetCheckButton(true, true);
		m_btGetNowPosCellStart.SetFont(&tFont);

		m_btAlignCellStartPos.SetRoundButtonStyle(&m_tButtonStyle);
		m_btAlignCellStartPos.SetTextColor(&tColor);
		m_btAlignCellStartPos.SetCheckButton(true, true);
		m_btAlignCellStartPos.SetFont(&tFont);

		m_btInitInspector.SetRoundButtonStyle(&m_tButtonStyle);
		m_btInitInspector.SetTextColor(&tColor);
		m_btInitInspector.SetCheckButton(true, true);
		m_btInitInspector.SetFont(&tFont);

		m_btSendRecipeParam.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSendRecipeParam.SetTextColor(&tColor);
		m_btSendRecipeParam.SetCheckButton(true, true);
		m_btSendRecipeParam.SetFont(&tFont);

		m_btSendStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSendStart.SetTextColor(&tColor);
		m_btSendStart.SetCheckButton(true, true);
		m_btSendStart.SetFont(&tFont);

		m_btSendStop.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSendStop.SetTextColor(&tColor);
		m_btSendStop.SetCheckButton(true, true);
		m_btSendStop.SetFont(&tFont);


		m_bt_MoveScanStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_bt_MoveScanStart.SetTextColor(&tColor);
		m_bt_MoveScanStart.SetCheckButton(true, true);
		m_bt_MoveScanStart.SetFont(&tFont);

		m_btMoveOddScanStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMoveOddScanStart.SetTextColor(&tColor);
		m_btMoveOddScanStart.SetCheckButton(true, true);
		m_btMoveOddScanStart.SetFont(&tFont);

		m_btMoveEvnScanStart.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMoveEvnScanStart.SetTextColor(&tColor);
		m_btMoveEvnScanStart.SetCheckButton(true, true);
		m_btMoveEvnScanStart.SetFont(&tFont);

		m_btMoveShift.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMoveShift.SetTextColor(&tColor);
		m_btMoveShift.SetCheckButton(true, true);
		m_btMoveShift.SetFont(&tFont);

	}

	for(int SStep = 0; SStep<5; SStep++)
	{
		//m_stScanInfo[SStep].SetFont(&m_TaskStateSmallFont);
		m_stScanInfo[SStep].SetTextColor(RGB(255, 255, 0));
		m_stScanInfo[SStep].SetBkColor(RGB(0, 0, 0));
	}
	m_stNowScanLine.SetFont(&G_TaskStateSmallFont);
	m_stNowScanLine.SetTextColor(RGB(255, 255, 0));
	m_stNowScanLine.SetBkColor(RGB(0, 0, 0));

	
	SetTimer(TIMER_INSPECT_INFO_UPDATE, 500, 0);
}
void CSubDlg_InspectScan::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


HBRUSH CSubDlg_InspectScan::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(	IDC_ST_SCAN_INFO1 == DLG_ID_Number ||
		IDC_ST_SCAN_INFO2 == DLG_ID_Number ||
		IDC_ST_SCAN_INFO3 == DLG_ID_Number ||
		IDC_ST_SCAN_INFO4 == DLG_ID_Number ||
		IDC_ST_SCAN_INFO5 == DLG_ID_Number ||
		IDC_ST_SCAN_INDEX == DLG_ID_Number)
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CSubDlg_InspectScan::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSubDlg_InspectScan::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(TIMER_INSPECT_INFO_UPDATE ==nIDEvent && this->IsWindowVisible() == TRUE)
	{
		CString strNewData;
		strNewData.Format("%d", *m_pStartScanLineNum);
		m_stNowScanLine.SetWindowText(strNewData);
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CSubDlg_InspectScan::OnBnClickedBtCellAlign()
{
	//Cell Align을 수행 하고 좌표를 가져오자.
	//*m_pAlignCellStartX = 35000;
	//*m_pAlignCellStartY = 175000;

	//////////////////////////////////////////////////////////////////////////
	CString strNewValue;
	strNewValue.Format("Cell Start X: %dum", *m_pAlignCellStartX);
	m_stScanInfo[0].SetWindowText(strNewValue);

	strNewValue.Format("Cell Start Y: %dum", *m_pAlignCellStartY);
	m_stScanInfo[1].SetWindowText(strNewValue);

	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanLineShiftDistance, "60000", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Shift Distance: %sum", strReadData);
	m_stScanInfo[2].SetWindowText(strNewValue);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_W, "5.25", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Resolution_W: %sum", strReadData);
	m_stScanInfo[3].SetWindowText(strNewValue);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_H, "5.25", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Resolution_H: %sum", strReadData);
	m_stScanInfo[4].SetWindowText(strNewValue);

}

void CSubDlg_InspectScan::OnBnClickedBtGetNowPos()
{
	//여기서는 현제 좌표를 입력하고 설정을 수행 한다.
	//*m_pAlignCellStartX = 18001;
	//*m_pAlignCellStartY = 175002;
	//////////////////////////////////////////////////////////////////////////
	CString strNewValue;
	strNewValue.Format("Cell Start X: %dum", *m_pAlignCellStartX);
	m_stScanInfo[0].SetWindowText(strNewValue);

	strNewValue.Format("Cell Start Y: %dum", *m_pAlignCellStartY);
	m_stScanInfo[1].SetWindowText(strNewValue);

	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanLineShiftDistance, "60000", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Shift Distance: %sum", strReadData);
	m_stScanInfo[2].SetWindowText(strNewValue);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_W, "5.25", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Resolution_W: %sum", strReadData);
	m_stScanInfo[3].SetWindowText(strNewValue);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_H, "5.25", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Resolution_H: %sum", strReadData);
	m_stScanInfo[4].SetWindowText(strNewValue);
}


void CSubDlg_InspectScan::OnBnClickedBtUnitInit()
{
	if(m_pServerInterface != nullptr)
	{
		m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, nBiz_AOI_System, nBiz_Send_UnitInitialize, 0);
		G_WriteInfo(Log_Normal, "Master to Inspector UnitInitialize");
		m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, nBiz_AOI_System, nBiz_Send_AlarmReset, 0);
		G_WriteInfo(Log_Normal, "Master to Inspector AlarmReset");
	}
	else
		G_WriteInfo(Log_Error, "Inspector Server Link Error");

	//Scan Line Reset
	*m_pStartScanLineNum = 0;

	m_btSendRecipeParam.SetCheck(false);
	m_btSendStart.SetCheck(false);
}


void CSubDlg_InspectScan::OnBnClickedBtSendRecipeParam()
{
	if(m_btSendRecipeParam.GetCheck() == true)
	{
		G_WriteInfo(Log_Worrying, "Already Param Set!!");
		return;
	}
	m_btSendRecipeParam.SetCheck(true);

	//////////////////////////////////////////////////////////////////////////
	CString strNewValue;
	strNewValue.Format("Cell Start X: %dum", *m_pAlignCellStartX);
	m_stScanInfo[0].SetWindowText(strNewValue);

	strNewValue.Format("Cell Start Y: %dum", *m_pAlignCellStartY);
	m_stScanInfo[1].SetWindowText(strNewValue);

	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanLineShiftDistance, "60000", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Shift Distance: %sum", strReadData);
	m_stScanInfo[2].SetWindowText(strNewValue);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_W, "5.25", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Resolution_W: %sum", strReadData);
	m_stScanInfo[3].SetWindowText(strNewValue);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_H, "5.25", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("Resolution_H: %sum", strReadData);
	m_stScanInfo[4].SetWindowText(strNewValue);


	SYSTEMTIME NowTime;
	GetLocalTime(&NowTime);
	char ImgFolderName[128];
	memset(ImgFolderName, 0x00, 128);
	sprintf(ImgFolderName,"I_%04d%02d%02d",NowTime.wYear, NowTime.wMonth, NowTime.wDay);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("%s", strReadData);
	if(strNewValue.GetLength()==0)
		strNewValue.Format("None");
	sprintf_s(m_pGlassReicpeParam->m_GlassName, "%s\\%s", ImgFolderName, strNewValue);
	
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanLineShiftDistance, "60000", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_pGlassReicpeParam->m_ScanLineShiftDistance = atoi(strReadData);//<<< Scna Line Shift 거리.(um단위)
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_W, "5.00", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_pGlassReicpeParam->m_ScanImgResolution_W = (float)atof(strReadData);//<<검사 Pixel Size.(um단위)검사 Resolution이 된다.
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_H, "5.00", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_pGlassReicpeParam->m_ScanImgResolution_H = (float)atof(strReadData);//<<검사 Pixel Size.(um단위)

	////////////////////////////////////////////////////////////////////////////
	//Cell Start 좌표를 이용 하여. Center 좌표와 Edge 좌표를 구한다.
	//Align을 할경우 실제 좌표가 입력 되어야 한다.
	int CellStartX = *m_pAlignCellStartX;
	int CellStartY = *m_pAlignCellStartY;

	if(m_pGlassReicpeParam->GLASS_CELL_COUNT_X>0 && m_pGlassReicpeParam->GLASS_CELL_COUNT_Y>0)
	{
		int CellGapDisX = abs(m_pGlassReicpeParam->CELL_DISTANCE_X-m_pGlassReicpeParam->CELL_SIZE_X);
		int CellGapDisY = abs(m_pGlassReicpeParam->CELL_DISTANCE_Y-m_pGlassReicpeParam->CELL_SIZE_Y);

		int AllCellDisX = (m_pGlassReicpeParam->GLASS_CELL_COUNT_X*m_pGlassReicpeParam->CELL_SIZE_X) +
			((m_pGlassReicpeParam->GLASS_CELL_COUNT_X-1)*CellGapDisX);
		int AllCellDisY = (m_pGlassReicpeParam->GLASS_CELL_COUNT_Y*m_pGlassReicpeParam->CELL_SIZE_Y) +
			((m_pGlassReicpeParam->GLASS_CELL_COUNT_Y-1)*CellGapDisY);




		//Recipe Load Cell 좌표.
		//DrawCellStartX = (m_pGlassReicpeParam->STICK_WIDTH/2)-(AllCellDisX/2);//um
		//DrawCellStartY = (1190000/2)-(AllCellDisY/2);//um
		m_pGlassReicpeParam->CenterPointX =  CellStartX+(AllCellDisX/2);
		m_pGlassReicpeParam->CenterPointY =  CellStartY+(AllCellDisY/2);

		m_pGlassReicpeParam->CELL_EDGE_X = abs(AllCellDisX/2)*(-1);
		m_pGlassReicpeParam->CELL_EDGE_Y = abs(AllCellDisY/2);
	}
	//Glass Param Send.
	int SendDataSize  = 0;
	//////////////////////////////////////////////////////////////////////////
	//Active Param Send
	SendDataSize = sizeof(SAOI_IMAGE_INFO)+sizeof(SAOI_COMMON_PARA);
	char *pSnedActiveParam = new char[SendDataSize];
	memcpy(&pSnedActiveParam[0],m_pstMaskImageInfo_Mic, sizeof(SAOI_IMAGE_INFO));
	memcpy(&pSnedActiveParam[sizeof(SAOI_IMAGE_INFO)],m_pstCommonPara_Mic, sizeof(SAOI_COMMON_PARA));

	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, 
			nBiz_AOI_Parameter, nBiz_Send_ActiveAreaParam, 0, 
			(USHORT)SendDataSize, 
			(UCHAR*)&pSnedActiveParam[0]);
	delete [] pSnedActiveParam;
	//////////////////////////////////////////////////////////////////////////
	//Pad Param Send(Stick에는 의미 없다.== Dumy Cell에 써볼까?)
	SendDataSize = (sizeof(SAOI_IMAGE_INFO)*4)+(sizeof(SAOI_COMMON_PARA)*4);
	char *pSnedPadParam = new char[SendDataSize];
	int JumpSize =  sizeof(SAOI_IMAGE_INFO) + sizeof(SAOI_COMMON_PARA);

	for(int SetStep =0; SetStep<4; SetStep++)
	{
		memcpy(&pSnedPadParam[(JumpSize*SetStep)],										 m_pstMaskImageInfo_Mic, sizeof(SAOI_IMAGE_INFO));
		memcpy(&pSnedPadParam[(JumpSize*SetStep) + sizeof(SAOI_IMAGE_INFO)], m_pstCommonPara_Mic, sizeof(SAOI_COMMON_PARA));
	}

	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, 
			nBiz_AOI_Parameter, nBiz_Send_PadAreaParam, 0, 
			(USHORT)SendDataSize, 
			(UCHAR*)&pSnedPadParam[0]);
	delete [] pSnedPadParam;
	//////////////////////////////////////////////////////////////////////////
	//Cell Real Pos Data Setting. 나중에 필요하면 추가 하자.(Simul에 있어 ㅡ.ㅡ;)


	//////////////////////////////////////////////////////////////////////////
	//Glass(Stick)정보를 전달 하자.
	SendDataSize  = sizeof(GLASS_PARAM)+1;
	char *pSnedHWGlassParam = new char[SendDataSize];
	memcpy(&pSnedHWGlassParam[0], m_pGlassReicpeParam, sizeof(GLASS_PARAM));

	pSnedHWGlassParam[SendDataSize-1] = 0;//Micro
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, 
		nBiz_AOI_Parameter, nBiz_Send_GlassParamSet, 0, 
		(USHORT)SendDataSize, 
		(UCHAR*)&pSnedHWGlassParam[0]);
	delete [] pSnedHWGlassParam;

	//히스토그램 그리는 정보 ㅡ.ㅡ 기억이 잘 ㅡ,ㅡ;;;;;;;
	int HistoSkipIndex = 3;
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, 
			nBiz_AOI_Inspection, nBiz_Send_IllValueStartEndIndex, (USHORT)HistoSkipIndex);
	//첫 Scan Line Setting
	*m_pStartScanLineNum = 1;
}

void CSubDlg_InspectScan::OnBnClickedBtSendStart()
{
	if(m_btSendRecipeParam.GetCheck() == false)
	{
		G_WriteInfo(Log_Worrying, "Need to Parameter Set!!");
		return;
	}

	if(m_btSendStart.GetCheck() == true)
	{
		G_WriteInfo(Log_Worrying, "Now Running Inspector");
		return;
	}

	CString strCamMsg;
	if((*m_pStartScanLineNum) %2 == 0)
		strCamMsg.Format("scd %d", 1);
	else
		strCamMsg.Format("scd %d", 0);
	//Scan Direction Select
	m_pServerInterface->m_fnSendCommand_Nrs(TASK_21_Inspector, nBiz_AOI_System, nBiz_Send_CamCommand, 0, strCamMsg.GetLength(), (UCHAR*)strCamMsg.GetBuffer());
	Sleep(50);

	m_stProcTime.SetWindowText("");
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, nBiz_AOI_Inspection, nBiz_Send_ScanStart, *m_pStartScanLineNum);
	*m_pStartLineTick = GetTickCount();
	*m_pStartBlockTick= *m_pStartLineTick;

	m_btSendStart.SetCheck(true);
}

void CSubDlg_InspectScan::OnBnClickedBtSendStop()
{
	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, nBiz_AOI_Inspection, nBiz_Send_ScanStop, 0);

	m_btSendRecipeParam.SetCheck(false);
	m_btSendStart.SetCheck(false);
}


void CSubDlg_InspectScan::OnBnClickedBtMoveScanStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CSubDlg_InspectScan::OnBnClickedBtMoveScanStartOdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CSubDlg_InspectScan::OnBnClickedBtMoveScanShift()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CSubDlg_InspectScan::OnBnClickedBtMoveScanStartEven()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
