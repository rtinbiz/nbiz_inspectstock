#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
class CSeq_3DScope: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_STICK_STATE_CHECK,
		//========================
		SEQ_STEP_SPACE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_3DSCOPE_SCOPE_INIT,
		SEQ_STEP_3DSCOPE_DATA_CHECK,

		//--------------------------------------
		
		SEQ_STEP_SCOPE_SAFETY_Z_MOVE,//Point마다 할것임으로
		SEQ_STEP_SPACE_SENSOR_SAFETY_Z,	
		SEQ_STEP_3DSCOPE_AF_10XLENS,

		//AF
		SEQ_STEP_3DSCOPE_AF_MOVE_POS,
		SEQ_STEP_SPACE_AF_MEASURE_Z,//자석 스틱근처로 보내기 위해서
		SEQ_STEP_3DSCOPE_AF_Z_MEASURE_POS,
		SEQ_STEP_3DSCOPE_10x_AUTOFOCUS,
		
		SEQ_STEP_SCOPE_SPACE_SAFETY_Z_MOVE,

		SEQ_STEP_SPACE_AF_STANDBY_Z,//이동 할것임으로.
		SEQ_STEP_SCOPE_SAFETY_Z_MOVE2,

		

		//Space Sensor Check Distance
		SEQ_STEP_3DSCOPE_MOVE_MEASURE_POS,
		  //SEQ_STEP_3DSCOPE_SPACE_AVOIDANCE,
		SEQ_STEP_SPACE_SENSOR_MEASURE_Z,
		SEQ_STEP_SPACE_SENSOR_MEASURE_START,
		  //SEQ_STEP_3DSCOPE_SPACE_RETURN,
		  //정의천 추가
		SEQ_STEP_3DSCOPE_AF_Z_MEASURE_POS2,
		SEQ_STEP_3DSCOPE_10x_AUTOFOCUS2,
		SEQ_STEP_3DSCOPE_MEASURE_BACK_MOVE_Z,
		//Measure 3D Scope
		SEQ_STEP_3DSCOPE_MEASURE_LENS_CHNAGE,
		SEQ_STEP_3DSCOPE_MEASURE_MOVE_Z,
		SEQ_STEP_3DSCOPE_MEASURE_START,
		SEQ_STEP_3DSCOPE_MEASURE_WAIT_END,
		SEQ_STEP_3DSCOPE_NEXT,//다음으로 갈때 랜즈정리 한다.
		//--------------------------------------
		SEQ_STEP_END_SCOPE_SAFETY_Z,
		SEQ_STEP_END_SPACE_SENSOR_SAFETY_Z,
		SEQ_STEP_END_3DSCOPE_AF_10XLENS,
		SEQ_STEP_END_SCAN_SAFETY_XY,
		SEQ_STEP_END_SCOPE_SAFETY_XY,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_3DScope(void);
	~CSeq_3DScope(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:
	SCOPE_3DPara		*m_pScope3DParam;
	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;
	SCOPE_LENS_OFFSET		*m_pScopeLensOffset;
	int *m_pNowLensIndex_3DScope;
	int						  m_3DMeasureIndex;
	SCOPE_3DPoint			*m_pNowIndexPos;
	int								*m_p3DMeasurPointCnt;
	SCOPE_3DPoint			**	m_pp3DMeasurPointList;

	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;
	CInterServerInterface  *m_pServerInterface;
};

