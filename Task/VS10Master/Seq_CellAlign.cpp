#include "StdAfx.h"
#include "Seq_CellAlign.h"


CSeq_CellAlign::CSeq_CellAlign(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;

	m_RecvCMD_Ret = -1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;
	m_pScopePos = nullptr;
	m_pReviewPos = nullptr;

	m_pAlignCellStartX = nullptr;
	m_pAlignCellStartY = nullptr;

	m_pReviewLensOffset = nullptr;
	m_pLiveViewObj = nullptr;

	m_pNowGrabImg = nullptr;
	m_pProcGrayImgBuf = nullptr;
}


CSeq_CellAlign::~CSeq_CellAlign(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;

	if(m_pNowGrabImg != nullptr)
		cvReleaseImage(&m_pNowGrabImg);
	m_pNowGrabImg = nullptr;

	if(m_pProcGrayImgBuf != nullptr)
		cvReleaseImage(&m_pProcGrayImgBuf);
	m_pProcGrayImgBuf = nullptr;
}


bool CSeq_CellAlign::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_CellAlign::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();

	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();

	G_WriteInfo(Log_Error, "CSeq_CellAlign Error Step Run( Z Axis Go to a safe position)");
	return 0;
}
void CSeq_CellAlign::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
CvPoint CSeq_CellAlign::Find_CELL_LeftTopEdge( IplImage *pTarget, int Cnt, CvRect *pBuf, double avrW, double avrH)
{
	CvPoint RetEdgePos;
	RetEdgePos.x = -1;
	RetEdgePos.y = -1;

	int FindIndex = -1;
	CvRect FindRectObj_X;
	CvRect FindRectObj_Y;
	memset(&FindRectObj_X,0x00, sizeof(CvRect));
	memset(&FindRectObj_Y,0x00, sizeof(CvRect));
	int MinPosX = pTarget->width;//pBuf[0].x+pBuf[0].width;
	int MinPosY = pTarget->height;//pBuf[0].y+pBuf[0].height;
	int Minbuf = 0;

	double MinW= avrW*0.8;
	double MaxW= avrW*1.2;
	double MinH= avrH*0.8;
	double MaxH= avrH*1.2;
	//X방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MinPosX > pBuf[FStep].x  )
			{
				MinPosX = pBuf[FStep].x;
				FindRectObj_X = pBuf[FStep];
			}
		}
	}
	//X방향 제일 작은것중에 Y방향 제일 작은것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].x > (FindRectObj_X.x - FindRectObj_X.width/2 ))&&
				(pBuf[FStep].x < (FindRectObj_X.x + FindRectObj_X.width/2 )))//같은 선상에서.
			{

				if( FindRectObj_X.y > pBuf[FStep].y  )
				{
					FindRectObj_X = pBuf[FStep];
				}
			}
		}
	}

#ifdef SIMUL_CELL_ALIGN_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_X.x , FindRectObj_X.y), 
		cvPoint(FindRectObj_X.x+FindRectObj_X.width , FindRectObj_X.y+FindRectObj_X.height), CV_RGB(0,255,255), 2);
#endif


	//Y방향 제일 작은것
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if( MinPosY > pBuf[FStep].y )
			{
				MinPosY = pBuf[FStep].y;
				FindRectObj_Y = pBuf[FStep];
			}
		}
	}
	//ㅛ방향 제일 작은것중에 X방향 제일 작은것.
	for(int FStep = 0; FStep<Cnt; FStep++)
	{
		if((pBuf[FStep].width>MinW &&pBuf[FStep].height>MinH )&&//최소 Size보다 큰것들 중에
			(pBuf[FStep].width<MaxW &&pBuf[FStep].height<MaxH ))//최대 Size보다 작은들 중에
		{
			if((pBuf[FStep].y > (FindRectObj_Y.y - FindRectObj_Y.height/2 ))&&
				(pBuf[FStep].y < (FindRectObj_Y.y + FindRectObj_Y.height/2 )))//같은 선상에서.
			{

				if( FindRectObj_Y.x > pBuf[FStep].x  )
				{
					FindRectObj_Y = pBuf[FStep];
				}
			}
		}
	}
	
#ifdef SIMUL_CELL_ALIGN_CALC_VIEW
	cvDrawRect(pTarget, cvPoint(FindRectObj_Y.x , FindRectObj_Y.y), 
		cvPoint(FindRectObj_Y.x+FindRectObj_Y.width , FindRectObj_Y.y+FindRectObj_Y.height), CV_RGB(0,255,255), 2);
#endif

	RetEdgePos.x = FindRectObj_X.x;
	RetEdgePos.y = FindRectObj_Y.y;


//#ifdef SIMUL_CELL_ALIGN_CALC_VIEW
	cvLine(pTarget, cvPoint(0 , RetEdgePos.y), 
		cvPoint(pTarget->width-1, RetEdgePos.y), CV_RGB(255,0,255), 2);
	cvLine(pTarget, cvPoint(RetEdgePos.x , 0), 
		cvPoint(RetEdgePos.x, pTarget->height-1), CV_RGB(0,255,0), 2);
//#endif
	return RetEdgePos;
}
CvPoint CSeq_CellAlign::AlignImgProc(IplImage *pTarget)
{
	CvPoint retAlignPoint;
#ifdef SIMUL_CELL_ALIGN_CALC_VIEW
	DWORD ProcTime = ::GetTickCount();
#endif
	retAlignPoint.x = -1;
	retAlignPoint.y = -1;

	if(pTarget == nullptr )
		return retAlignPoint;

	if(m_pProcGrayImgBuf == nullptr)
		m_pProcGrayImgBuf = cvCreateImage(cvSize(pTarget->width, pTarget->height), IPL_DEPTH_8U, 1);

	cvCvtColor(pTarget, m_pProcGrayImgBuf, CV_RGB2GRAY);
	cvThreshold(m_pProcGrayImgBuf, m_pProcGrayImgBuf, 128, 255, CV_THRESH_BINARY); 

	CBlobLabeling blob;
	blob.SetParam(m_pProcGrayImgBuf,1 );
	blob.DoLabeling();
	int AvrCnt = 0;
	double AvrWidth = 0.0;
	double AvrHieght = 0.0;
	if(blob.m_nBlobs==0)
		return retAlignPoint;

	for(int i = 0; i<blob.m_nBlobs; i++)
	{
		AvrCnt++;
		AvrWidth += (double)blob.m_recBlobs[i].width;
		AvrHieght += (double)blob.m_recBlobs[i].height;
	}
	AvrWidth = AvrWidth/(double)AvrCnt;
	AvrHieght=AvrHieght/(double)AvrCnt;


	retAlignPoint = Find_CELL_LeftTopEdge( pTarget, blob.m_nBlobs, blob.m_recBlobs, AvrWidth, AvrHieght);


#ifdef SIMUL_CELL_ALIGN_CALC_VIEW
	ProcTime = ::GetTickCount()-ProcTime;
	extern CvFont G_cvDrawFont; 
	char G_strProcMsg[128];
	sprintf_s(G_strProcMsg,128,"[%d Tick]",ProcTime);
	cvPutText(pTarget, G_strProcMsg,  cvPoint(retAlignPoint.x , retAlignPoint.y), &G_cvDrawFont, CV_RGB(255,0,0));

	cvNamedWindow("TEST_x", CV_WINDOW_NORMAL);
	cvShowImage("TEST_x", pTarget);
	cvWaitKey();
#endif
	cvZero(m_pProcGrayImgBuf);
	return retAlignPoint;
}
int CSeq_CellAlign::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_CellAlign%03d> Sequence Start", ProcStep);

			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_STICK_STATE_CHECK:
		{//Stick이 측정 가능 상태 인지 체크 한다.

			if(G_CheckStickProc_StaticState() == ST_PROC_NG)
			{
				G_WriteInfo(Log_Check, "SEQ_STEP_STICK_STATE_CHECK ST_PROC_NG", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			CString strNewValue;
			strNewValue.Format("%d", 0);
			WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosShift, strNewValue.GetBuffer(strNewValue.GetLength()), VS10Master_PARAM_INI_PATH);
			strNewValue.Format("%d", 0);
			WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosDrive, strNewValue.GetBuffer(strNewValue.GetLength()), VS10Master_PARAM_INI_PATH);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_LENS_CHANGE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_LENS_CHANGE(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Func_ReviewLens;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, m_pReviewPos->nCellAlign_LensIndex, nBiz_Unit_Zero);

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_LENS_CHANGE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			if(m_RecvCMD_Ret != m_pReviewPos->nCellAlign_LensIndex)//nBiz_Seq_TT01_Pos_AM01가 도킹 했는지를 먼저 본다.
			{//TT01이 들어온 상태가 아니다.
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_LENS_CHANGE%03d> Reve Lens Check Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//TT01위치 체크.
			if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_LENS_CHANGE%03d>TT01 Pos Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	
	case SEQ_STEP_CELL_ALIGN_REVIEW_LIGHT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_REVIEW_LIGHT(%03d)>", ProcStep);

			//Live View를 On한다.
			m_pLiveViewObj->LiveView(true);

			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_SetReviewLight;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, 0);//Align때는 Review 조명을 끈다.

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_REVIEW_LIGHT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//if(m_RecvCMD_Ret != 0)
			//{
			//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_REVIEW_LIGHT%03d> Scope Lens Check Error!!!!", ProcStep);
			//	fnSeqSetNextStep(SEQ_STEP_ERROR);
			//	break;
			//}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_LIGHT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_LIGHT(%03d)>", ProcStep);
			G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, m_pReviewPos->nCellAlign_Backlight ,0);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_LIGHT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_LIGHT%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_MOVE_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_MOVE_POS(%03d)>", ProcStep);
			G_MotObj.m_fnAM01ReviewSyncMove(*m_pAlignCellStartX, *m_pAlignCellStartY, Sync_Scope_BackLight);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_MOVE_POS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_MOVE_POS%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_REVIEW_POS_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_REVIEW_POS_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Review);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_REVIEW_POS_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_REVIEW_POS_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_REVIEW_UP:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_REVIEW_UP(%03d)>", ProcStep);
			G_MotObj.m_fnReviewUp();
			int TimeOutWait = 6000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_UP].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_REVIEW_UP%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_UP].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_REVIEW_UP%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			Sleep(300);//Cylinder안정화
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_REVIEW_AF:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_REVIEW_AF(%03d)>", ProcStep);
			m_RecvCMD_Ret = -1;
			m_WaitCMD = nBiz_Seq_AF;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 3);//One AF Try

			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_REVIEW_AF%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret != 1)//AF On
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_REVIEW_AF%03d> AF On!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_REVIEW_GRAB:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_REVIEW_GRAB(%03d)>", ProcStep);
			//Live View를 On한다.
			if(m_pNowGrabImg != nullptr)
				cvReleaseImage(&m_pNowGrabImg);
			m_pNowGrabImg = nullptr;

			bool bLiveOverlay = m_pLiveViewObj->m_bSaveOverlayImage;
			m_pLiveViewObj->m_bSaveOverlayImage = false;
			m_pLiveViewObj->SaveImage(&m_pNowGrabImg);

			int TimeOutWait = 3000;//About 20sec
			while(m_pNowGrabImg == nullptr)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_RecvCMD_Ret = -1;
					m_WaitCMD = 0;
					m_RecvCMD = 0;

					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			m_pLiveViewObj->m_bSaveOverlayImage= bLiveOverlay;
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_REVIEW_GRAB%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_pNowGrabImg  ==  nullptr)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_REVIEW_GRAB%03d> Grab Image Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_POINT_FIND:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_POINT_FIND(%03d)>", ProcStep);
			CvPoint CellEdge;
			CellEdge = AlignImgProc(m_pNowGrabImg);

			if(CellEdge.x<0 || CellEdge.y<0)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_POINT_FIND%03d> Align Fail.!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			CvPoint CenterToDis;
			CenterToDis.x = CellEdge.x-(REAL_IMAGE_SIZE_X/2);
			CenterToDis.y = CellEdge.y-(REAL_IMAGE_SIZE_Y/2);

			int CellEdgePosShift = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanShift];
			int CellEdgePosDrive = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanDrive];
			bool bSettingCheckOk= true;
			switch(m_pReviewPos->nCellAlign_LensIndex)
			{
			case REVIEW_LENS_2X:
				{
					CellEdgePosShift+=(int)(m_pReviewLensOffset->Lens02xPixelSizeX*(float)CenterToDis.x);
					CellEdgePosDrive+=(int)(m_pReviewLensOffset->Lens02xPixelSizeY*(float)CenterToDis.y);
				}break;
			case REVIEW_LENS_10X:
				{
					CellEdgePosShift+=(int)(m_pReviewLensOffset->Lens10xPixelSizeX*(float)CenterToDis.x);
					CellEdgePosDrive+=(int)(m_pReviewLensOffset->Lens10xPixelSizeY*(float)CenterToDis.y);
				}break;
			case REVIEW_LENS_20X:
				{
					CellEdgePosShift+=(int)(m_pReviewLensOffset->Lens20xPixelSizeX*(float)CenterToDis.x);
					CellEdgePosDrive+=(int)(m_pReviewLensOffset->Lens20xPixelSizeY*(float)CenterToDis.y);
				}break;
			default:
				{
					bSettingCheckOk = false;
				}break;
			}
			if(bSettingCheckOk == true)
			{
				*m_pAlignCellStartX = CellEdgePosShift;
				*m_pAlignCellStartY = CellEdgePosDrive;
			}
			else
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_POINT_FIND%03d> Lens Value Setting Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			extern CvFont G_cvDrawFont; 
			char G_strProcMsg[128];
			sprintf_s(G_strProcMsg,128,"Img X:%d, Y:%d",CellEdge.x, CellEdge.y);
			cvPutText(m_pNowGrabImg, G_strProcMsg,  cvPoint(CellEdge.x+100 , CellEdge.y+100), &G_cvDrawFont, CV_RGB(255,0,0));

			sprintf_s(G_strProcMsg,128,"Cell X:%d, Y:%d",CellEdgePosShift, CellEdgePosDrive);
			cvPutText(m_pNowGrabImg, G_strProcMsg,  cvPoint(CellEdge.x+100 , CellEdge.y+200), &G_cvDrawFont, CV_RGB(255,0,255));
			cvSaveImage("D:\\CellAlign.bmp", m_pNowGrabImg);

			CString strNewValue;
			strNewValue.Format("%d", CellEdgePosShift);
			WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosShift, strNewValue.GetBuffer(strNewValue.GetLength()), VS10Master_PARAM_INI_PATH);
			strNewValue.Format("%d", CellEdgePosDrive);
			WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosDrive, strNewValue.GetBuffer(strNewValue.GetLength()), VS10Master_PARAM_INI_PATH);

			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_CELL_ALIGN_SET_VALUE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_SET_VALUE(%03d)>", ProcStep);


			//*m_pAlignCellStartX = CellEdgePosShift;
			//*m_pAlignCellStartY = CellEdgePosDrive;
			//Align 좌표 적용.
			::SendMessage(AfxGetMainWnd()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 2);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_POS_MOVE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_POS_MOVE(%03d)>", ProcStep);
			G_MotObj.m_fnAM01ReviewSyncMove(*m_pAlignCellStartX, *m_pAlignCellStartY, Sync_Scope_BackLight);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_POS_MOVE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_POS_MOVE%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_END_LIGHT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_END_LIGHT(%03d)>", ProcStep);
			G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, 0 ,0);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_END_LIGHT%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_END_LIGHT%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;

	case SEQ_STEP_CELL_ALIGN_END_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_END_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_END_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_END_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_END_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_END_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_END_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_END_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_END_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_CELL_ALIGN_END_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_CELL_ALIGN_END_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_CELL_ALIGN_END_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_CELL_ALIGN_END_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			G_WriteInfo(Log_Normal, "<CSeq_Inspect_Mic%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_Inspect_Mic> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}