#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
class CSeq_CDTP: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_STICK_STATE_CHECK,
		//========================

		SEQ_STEP_SPACE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_CDTP_DATA_CHECK,
		SEQ_STEP_CDTP_LENS_CHANGE,
		SEQ_STEP_CDTP_REVIEW_POS_Z,
	
		//CD측정------------------------------
		SEQ_STEP_CDTP_MOVE_POS,
		SEQ_STEP_CDTP_REVIEW_LIGHT_OFF,
		SEQ_STEP_CDTP_LIGHT,
		SEQ_STEP_CDTP_REVIEW_UP,
		SEQ_STEP_CDTP_REVIEW_AF,
		SEQ_STEP_CDTP_REVIEW_GRAB,
		SEQ_STEP_CDTP_MEASURE,
		SEQ_STEP_CDTP_SAVE_RESULT,

		//TP측정------------------------------
		SEQ_STEP_TP_MOVE_POS,
		SEQ_STEP_TP_REVIEW_LIGHT_ON,
		SEQ_STEP_TP_LIGHT_OFF,
		SEQ_STEP_TP_REVIEW_AF,
		SEQ_STEP_TP_REVIEW_GRAB,
		SEQ_STEP_TP_MEASURE,
		SEQ_STEP_TP_SAVE_RESULT,

		//SEQ_STEP_CDTP_REVIEW_DOWN,
		SEQ_STEP_CDTP_NEXT,
		//------------------------------
		SEQ_STEP_CDTP_CALC_TP_VALUE,
		SEQ_STEP_CDTP_END_RING_LIGHT_DOWN,
		SEQ_STEP_CDTP_END_REVIEW_DOWN,
		SEQ_STEP_CDTP_END_SCAN_SAFETY_Z,
		SEQ_STEP_CDTP_END_SCAN_SAFETY_XY,
		SEQ_STEP_CDTP_END_SCOPE_SAFETY_XY,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_CDTP(void);
	~CSeq_CDTP(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:
	CString m_strBoxID;
	CString m_strStickID;
	CString m_strCDTPSavePath;
	GLASS_PARAM			*m_pGlassReicpeParam;
	REVIEW_LENS_OFFSET	*m_pReviewLensOffset;
	CLiveViewCtrl		*m_pLiveViewObj;
	CD_TPParam			*m_pCDTPParam;
	Scope3DPosition		*m_pScopePos;
	ReviewPosition		*m_pReviewPos;
	int					m_CDTPMeasureIndex;
	CDTP_Point			*m_pNowIndexPos;
	int					*m_pCDTPPointCnt;
	CDTP_Point			**m_ppCDTPPointList;
	TP_Result			*m_pTP_Result;

	IplImage *m_pNowGrabImg;

	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;
	CInterServerInterface  *m_pServerInterface;

	IplImage *m_pProcGrayImgBuf;
	CvRect CDTPImgProc(CD_TPParam *pCDTPParam, CDTP_Point*pNowPos, IplImage *pTarget, CvPoint *pEdgeCell, CvRect *pFindIndexRib_LR , CvRect *pFindIndexRib_UD);
	void CDTPDrawResult(IplImage *pTarget, CvRect ResultRect, CvRect FindIndexRib_LR , CvRect FindIndexRib_UD, CvPoint EdgeCell, CDTP_Point *pNowIndexPos);

	double m_fnGetLengthData(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType);
	double m_fnGetLengthDataLongY(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType);
	double m_fnGetLengthDataLongX(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType);

	int Find_CELL_LeftTop( IplImage *pTarget, int Cnt, CvRect *pBuf, int FCntX, int FCntY, double avrW, double avrH, CvPoint *pEdgeCell);
	int Find_CELL_RightTop(IplImage *pTarget, int Cnt, CvRect *pBuf, int FCntX, int FCntY, double avrW, double avrH, CvPoint *pEdgeCell);
	int Find_CELL_RightBottom(IplImage *pTarget, int Cnt, CvRect *pBuf, int FCntX, int FCntY, double avrW, double avrH, CvPoint *pEdgeCell);
	int Find_CELL_LeftBottom(IplImage *pTarget, int Cnt, CvRect *pBuf, int FCntX, int FCntY,double avrW, double avrH, CvPoint *pEdgeCell);
	int Find_CELL_Center(IplImage *pTarget, int Cnt, CvRect *pBuf,double avrW, double avrH );

	CvPoint Find_CELL_Left_Cross( IplImage *pTarget, int FindIndex, int Cnt, CvRect *pBuf, double avrW, double avrH);
	CvPoint Find_CELL_Right_Cross(IplImage *pTarget, int FindIndex, int Cnt, CvRect *pBuf, double avrW, double avrH);


	IplImage *m_pTp_GrayBuf;
	IplImage *m_pTp_GrayBufCopy;
	IplImage *m_pTp_GrayAlign;
	IplImage *m_pMatchBuffer;
	CvPoint Find_TP_Center(CD_TPParam *pCDTPParam, CDTP_Point*pNowPos, IplImage *pTarget, REVIEW_LENS_OFFSET	*pReviewLensOffset);
	bool Calc_TP_Value(GLASS_PARAM *pGlassReicpeParam, int nListCnt, CDTP_Point *pCDTPPointList, TP_Result *pTP_Result);
	bool m_bCD_FindOk;
};

