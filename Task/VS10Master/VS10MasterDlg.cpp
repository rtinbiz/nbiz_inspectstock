// VS10MasterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VS10Master.h"
#include "VS10MasterDlg.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CvFont		G_DefectFont;
void PutTextInImageDefect(IplImage *pMainMap, CvPoint *pNamePos,char*text)
{
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_DefectFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= pNamePos->x-5;
	StartTxt.y	= pNamePos->y+baseline;
	EndTxt.x	= pNamePos->x+text_size.width+5;
	EndTxt.y	= pNamePos->y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	//CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
	//	cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
	//	cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
	//	cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	//CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	//int PolyVertexNumber[2] = { 4 , 0 } ;
	//int PolyNumber = 1 ;
	//cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, CV_RGB(0, 0, 0));
	cvPutText(pMainMap, text, *pNamePos, &G_DefectFont, CV_RGB(255,0,0));
	cvRectangle(pMainMap, StartTxt, EndTxt, CV_RGB(255, 0, 0));
}
void InputInspectDefectTreeItem(CTreeCtrl *pInputTree, HTREEITEM *pResultRoot, int DIndex,  CvPoint StickCenter, COORD_DINFO *pNewData, IplImage *pSmallImg)
{
	CString strNewData;
	strNewData.Format("%03d)Defect");
	HTREEITEM SubResultItem = pInputTree->InsertItem(strNewData, 5, 5, *pResultRoot);
	
	pInputTree->SetItemData(SubResultItem, (DWORD_PTR)pSmallImg);
	
	int PosShift = (StickCenter.x + pNewData->CoodStartX)+(pNewData->CoodWidth/2);
	int PosDrive = (StickCenter.y + pNewData->CoodStartY*(-1))+(pNewData->CoodHeight/2);

	strNewData.Format("Shift Pos :%d", PosShift);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);
	strNewData.Format("Drive Pos :%d", PosDrive);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);


	strNewData.Format("Defect Type :%d", pNewData->DefectType);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);

	strNewData.Format("StartX :%d", pNewData->CoodStartX);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);
	strNewData.Format("StartY :%d", pNewData->CoodStartY);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);

	strNewData.Format("Width :%d", pNewData->CoodWidth);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);
	strNewData.Format("Height :%d", pNewData->CoodHeight);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);
	strNewData.Format("Pixel Cnt :%d", pNewData->nDefectCount);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);
	strNewData.Format("Min/Max/Avg :%d/%d/%d", pNewData->nDValueMin, pNewData->nDValueMax, pNewData->nDValueAvg);
	pInputTree->InsertItem(strNewData, 8, 8, SubResultItem);
	
}
UINT Thread_InspectDefectMergeProc_Mic(LPVOID pParam)
{
	CVS10MasterDlg *pProcData = (CVS10MasterDlg *)pParam;


	pProcData->m_MergeData_MicQ.m_bDataUpdateEnd = false;
	//Defect Merge (중복위치 Defect를 합산한다.)
	if(pProcData->m_DefectData_MicQ.QGetCnt()>0)
	{
		int InPutTotalCnt =pProcData->m_DefectData_MicQ.QGetCnt();
		int MergeDefecCnt = 0;
		int ResultBufCnt = 0;
		COORD_DINFO NewData;
		double MotDistance =(double) (pProcData->m_stCommonPara_Mic.nDefectDistance/2.0);
		IplImage *pSmalImg = nullptr;
		for(int IStep = 0; IStep<InPutTotalCnt; IStep++)
		{
			NewData = pProcData->m_DefectData_MicQ.QGetNode(&pSmalImg);



			if(pProcData->m_MergeData_MicQ.QPutMergeNode(&NewData, pSmalImg, MotDistance) == false)
				MergeDefecCnt++;//Merge되어 입력 되지 않은 겟수.
			Sleep(0);

			if(pSmalImg != nullptr)
				cvReleaseImage(&pSmalImg);
			pSmalImg = nullptr;
		}
		ResultBufCnt = pProcData->m_MergeData_MicQ.QGetCnt();

		HTREEITEM *pResultRoot = &pProcData->m_ResultRoot_InsMicScan;
		COORD_DINFO InputData;
		IplImage **ppSmallImg = nullptr;
		if(pProcData->m_MergeData_MicQ.QGetMoveFirstNode() == true)
		{
			for(int IStep = 0; IStep<ResultBufCnt; IStep++)
			{
				if(pProcData->m_MergeData_MicQ.QGetNextNodeData(&InputData, &ppSmallImg)== true)
				{
					InputInspectDefectTreeItem(&pProcData->m_treeResult, 
						pResultRoot, 
						IStep+1,
						cvPoint(pProcData->m_GlassReicpeParam.CenterPointX, pProcData->m_GlassReicpeParam.CenterPointY),
						&InputData, 
						*ppSmallImg);
				}
				else
					G_WriteInfo(Log_Error, "Defect Merge Count Error!!!");
			}
		}
		//pProcData->m_treeResult.Expand(*pResultRoot, TVE_EXPAND);

		CString strNewValue;
		strNewValue.Format("Inspect Micro(Total:%d, (%d-%d)", pProcData->m_MergeData_MicQ.QGetCnt(), InPutTotalCnt, MergeDefecCnt);
		pProcData->m_treeResult.SetItemText(pProcData->m_ResultRoot_InsMicScan, strNewValue);

		//Auto Review에 정보 전달.
		pProcData->m_SubCDTPView->m_AutoReviewNowIndex = 0;
		pProcData->m_SubCDTPView->m_AutoReviewTotalCnt = pProcData->m_MergeData_MicQ.QGetCnt();

		strNewValue.Format("%d", pProcData->m_SubCDTPView->m_AutoReviewTotalCnt);
		pProcData->m_SubCDTPView->m_stTotal_AutoReview.SetWindowText(strNewValue);

		strNewValue.Format("%d", pProcData->m_SubCDTPView->m_AutoReviewNowIndex);
		pProcData->m_SubCDTPView->m_stIndex_AutoReview.SetWindowText(strNewValue);
	}
	//Machine좌표 입력
	if(pProcData->m_MergeData_MicQ.QGetCnt()>0)
	{
		DataNodeObj * pNowReviewData = nullptr;
		pProcData->m_MergeData_MicQ.QGetMoveFirstNode();
		pNowReviewData = pProcData->m_MergeData_MicQ.QGetNextNodeData();
		while (pNowReviewData != nullptr)
		{
			//중심 좌표계에서 머신 좌표계로 변환.
			pNowReviewData->m_MachinePosX = (pProcData->m_GlassReicpeParam.CenterPointX + pNowReviewData->DefectData.CoodStartX)+(pNowReviewData->DefectData.CoodWidth/2);
			pNowReviewData->m_MachinePosY = (pProcData->m_GlassReicpeParam.CenterPointY + pNowReviewData->DefectData.CoodStartY*(-1))+(pNowReviewData->DefectData.CoodHeight/2);
			pNowReviewData = pProcData->m_MergeData_MicQ.QGetNextNodeData();
		}
	}
	pProcData->m_MapDisplayData_MicQ.QClean();
	//////////////////////////////////////////////////////////////////////////
	pProcData->m_MergeData_MicQ.QSortBySize(true);//Defect Sort By Size
	//////////////////////////////////////////////////////////////////////////
	pProcData->m_MergeData_MicQ.QSortByDistance(pProcData->m_ReviewPos.nAutoReviewMic_Cnt, 
		pProcData->m_AlignCellStartX, pProcData->m_AlignCellStartY);//Sort Auto Review Short Distance
	
	
	//Auto Review 화면 갱신(Map View)
	if(pProcData->m_MergeData_MicQ.QGetCnt()>0)
	{
		DataNodeObj * pNowReviewData = nullptr;
		pProcData->m_MergeData_MicQ.QGetMoveFirstNode();
		pNowReviewData = pProcData->m_MergeData_MicQ.QGetNextNodeData();
		while (pNowReviewData != nullptr)
		{
			pProcData->m_MapDisplayData_MicQ.QPutBackNode(&pNowReviewData->DefectData);
			pNowReviewData = pProcData->m_MergeData_MicQ.QGetNextNodeData();
		}
	}
	pProcData->m_p3DMapViewer->m_3DViewObj.RenderScreen();

	pProcData->m_MergeData_MicQ.m_bDataUpdateEnd = true;
	return 0;
}

UINT Thread_InspectDefectMergeProc_Mac(LPVOID pParam)
{
	CVS10MasterDlg *pProcData = (CVS10MasterDlg *)pParam;


	pProcData->m_MergeData_MacQ.m_bDataUpdateEnd = false;
	//Defect Merge (중복위치 Defect를 합산한다.)
	if(pProcData->m_DefectData_MacQ.QGetCnt()>0)
	{
		int InPutTotalCnt =pProcData->m_DefectData_MacQ.QGetCnt();
		int MergeDefecCnt = 0;
		int ResultBufCnt = 0;
		COORD_DINFO NewData;
		double MotDistance =(double) (pProcData->m_stCommonPara_Mac.nDefectDistance/2.0);
		IplImage *pSmalImg = nullptr;
		for(int IStep = 0; IStep<InPutTotalCnt; IStep++)
		{
			NewData = pProcData->m_DefectData_MacQ.QGetNode(&pSmalImg);

			if(pProcData->m_MergeData_MacQ.QPutMergeNode(&NewData, pSmalImg, MotDistance) == false)
				MergeDefecCnt++;//Merge되어 입력 되지 않은 겟수.
			Sleep(0);

			if(pSmalImg != nullptr)
				cvReleaseImage(&pSmalImg);
			pSmalImg = nullptr;
		}
		ResultBufCnt = pProcData->m_MergeData_MacQ.QGetCnt();

		HTREEITEM *pResultRoot = &pProcData->m_ResultRoot_InsMacScan;
		COORD_DINFO InputData;
		IplImage **ppSmallImg = nullptr;
		if(pProcData->m_MergeData_MacQ.QGetMoveFirstNode() == true)
		{
			for(int IStep = 0; IStep<ResultBufCnt; IStep++)
			{
				if(pProcData->m_MergeData_MacQ.QGetNextNodeData(&InputData, &ppSmallImg)== true)
				{
					InputInspectDefectTreeItem(&pProcData->m_treeResult, 
						pResultRoot, 
						IStep+1,
						cvPoint(pProcData->m_GlassReicpeParam.CenterPointX, pProcData->m_GlassReicpeParam.CenterPointY),
						&InputData, 
						*ppSmallImg);
				}
				else
					G_WriteInfo(Log_Error, "Defect Merge Count Error!!!");
			}
		}
		//pProcData->m_treeResult.Expand(*pResultRoot, TVE_EXPAND);

		CString strNewValue;
		strNewValue.Format("Inspect Macro(Total:%d, (%d-%d)", pProcData->m_MergeData_MacQ.QGetCnt(), InPutTotalCnt, MergeDefecCnt);
		pProcData->m_treeResult.SetItemText(pProcData->m_ResultRoot_InsMacScan, strNewValue);

		//Auto Review에 정보 전달.
		pProcData->m_SubCDTPView->m_AutoReviewNowIndex = 0;
		pProcData->m_SubCDTPView->m_AutoReviewTotalCnt = pProcData->m_MergeData_MacQ.QGetCnt();

		strNewValue.Format("%d", pProcData->m_SubCDTPView->m_AutoReviewTotalCnt);
		pProcData->m_SubCDTPView->m_stTotal_AutoReview.SetWindowText(strNewValue);

		strNewValue.Format("%d", pProcData->m_SubCDTPView->m_AutoReviewNowIndex);
		pProcData->m_SubCDTPView->m_stIndex_AutoReview.SetWindowText(strNewValue);
	}
	//Machine좌표 입력
	if(pProcData->m_MergeData_MacQ.QGetCnt()>0)
	{
		DataNodeObj * pNowReviewData = nullptr;
		pProcData->m_MergeData_MacQ.QGetMoveFirstNode();
		pNowReviewData = pProcData->m_MergeData_MacQ.QGetNextNodeData();
		while (pNowReviewData != nullptr)
		{
			//중심 좌표계에서 머신 좌표계로 변환.
			pNowReviewData->m_MachinePosX = (pProcData->m_GlassReicpeParam.CenterPointX + pNowReviewData->DefectData.CoodStartX)+(pNowReviewData->DefectData.CoodWidth/2);
			pNowReviewData->m_MachinePosY = (pProcData->m_GlassReicpeParam.CenterPointY + pNowReviewData->DefectData.CoodStartY*(-1))+(pNowReviewData->DefectData.CoodHeight/2);
			pNowReviewData = pProcData->m_MergeData_MacQ.QGetNextNodeData();
		}
	}
	pProcData->m_MapDisplayData_MacQ.QClean();
	//////////////////////////////////////////////////////////////////////////
	pProcData->m_MergeData_MacQ.QSortBySize(true);//Defect Sort By Size
	//////////////////////////////////////////////////////////////////////////
	pProcData->m_MergeData_MacQ.QSortByDistance(pProcData->m_ReviewPos.nAutoReviewMic_Cnt, 
		pProcData->m_AlignCellStartX, pProcData->m_AlignCellStartY);//Sort Auto Review Short Distance


	//Auto Review 화면 갱신(Map View)
	if(pProcData->m_MergeData_MacQ.QGetCnt()>0)
	{
		DataNodeObj * pNowReviewData = nullptr;
		pProcData->m_MergeData_MacQ.QGetMoveFirstNode();
		pNowReviewData = pProcData->m_MergeData_MacQ.QGetNextNodeData();
		while (pNowReviewData != nullptr)
		{
			pProcData->m_MapDisplayData_MacQ.QPutBackNode(&pNowReviewData->DefectData);
			pNowReviewData = pProcData->m_MergeData_MacQ.QGetNextNodeData();
		}
	}
	pProcData->m_p3DMapViewer->m_3DViewObj.RenderScreen();

	pProcData->m_MergeData_MacQ.m_bDataUpdateEnd = true;
	return 0;
}
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CVS10MasterDlg 대화 상자
CVS10MasterDlg::CVS10MasterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVS10MasterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_pSampleView = nullptr;
	m_pStickReicpeView = nullptr;
	m_pSysParamView = nullptr;
	m_pMaintenanceView = nullptr;


	m_Sub3DScopeView = nullptr;
	m_SubInspectView = nullptr;
	m_SubAreaScanView = nullptr;
	m_SubCDTPView = nullptr;

	m_SubStickExchangeView = nullptr;
	m_SubThetaAlignView = nullptr;


	m_nCellCnt = 0;
	m_pCellList = nullptr;
	m_nScanImgCnt = 0;
	m_pScanImgList = nullptr;
	m_nRetDefectCnt = 0;
	m_pRetDefectList = nullptr;


	m_CamScnaLineCnt = 0;
	m_CamScnaImgCnt = 0;
	m_StartScanLineNum = 0;
	m_BlockEnd = false;

	m_ResultRoot_3DScope = 0;	
	m_ResultRoot_InsMicScan = 0;
	m_ResultRoot_AreaScan3D = 0;
	m_ResultRoot_CDTP = 0;		

	m_CDTPPointCnt = 0;
	m_pCDTPPointList = nullptr;

	m_3DMeasurPointCnt = 0;
	m_p3DMeasurPointList = nullptr;


	cvInitFont (&G_DefectFont, CV_FONT_HERSHEY_TRIPLEX , 0.6f, 0.7f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX


	//측정 결과.
	m_ResultMeasurement[0] = false;
	m_ResultMeasurement[1] = false;
	m_ResultMeasurement[2] = false;
	m_ResultMeasurement[3] = false;

	//Now Lens Index
	m_NowLensIndex_Review = 0;
	m_NowLensIndex_3DScope = 0;

	//이 값은 음수가 없다.
	m_AirPressureValue_Air = -1;
	m_AirPressureValue_N2 = -1;


	//m_TaskCheckCnt_Inspector = 0;
	//m_TaskCheckCnt_Indexer = 0;
	//m_TaskCheckCnt_Motion = 0;
	//m_TaskCheckCnt_Mesure3D = 0;
	//m_TaskCheckCnt_Area3DScan = 0;
	//m_TaskCheckCnt_AutoFocus = 0;

	m_pSmallImgReadBuf = nullptr;


	m_f3DScope_Ret_DOON_TUK= 0.0f;
	m_f3DScope_Ret_STICK_THICK= 0.0f;
	m_f3DScope_Ret_TAPERANGLE= 0.0f;


	m_pHostMsgViewDlg = nullptr;

	m_bUpRingLightCylinder = false;
	m_bUpReviewCylinder = false;

	m_AlignCellStartX = 0;
	m_AlignCellStartY = 0;

	m_bInspectMicOrMac =  true;//true : Mic, false:Mac
}
CVS10MasterDlg::~CVS10MasterDlg()
{
	if(m_pSampleView != nullptr)
		cvReleaseImage(&m_pSampleView);
	m_pSampleView = nullptr;

	m_nCellCnt = 0;
	if(m_pCellList != nullptr)
		delete []m_pCellList;
	m_pCellList = nullptr;

	m_nScanImgCnt = 0;
	if(m_pScanImgList != nullptr)
		delete []m_pScanImgList;
	m_pScanImgList = nullptr;

	m_nRetDefectCnt = 0;
	if(m_pRetDefectList != nullptr)
		delete []m_pRetDefectList;
	m_pRetDefectList = nullptr;

	m_CDTPPointCnt = 0;
	if(m_pCDTPPointList != nullptr)
		delete []m_pCDTPPointList;
	m_pCDTPPointList = nullptr;

	m_3DMeasurPointCnt = 0;
	if(m_p3DMeasurPointList != nullptr)
		delete []m_p3DMeasurPointList;
	m_p3DMeasurPointList = nullptr;


	if(m_pSmallImgReadBuf != nullptr)
		delete [] m_pSmallImgReadBuf;
	m_pSmallImgReadBuf = nullptr;

	CloseMotionMem(&G_MotInfo);
}



void CVS10MasterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_STICK_INFO, m_gridStickInfo);
	DDX_Control(pDX, IDC_GRID_RESULT_LIST, m_gridResultList);
	DDX_Control(pDX, IDC_LIST_MOT_LOG, m_listLogView);
	DDX_Control(pDX, IDC_BT_LIVE_VIEW_ONOFF, m_bLiveViewOnOff);
	DDX_Control(pDX, IDC_ST_TASK_STATE1, m_stTaskState[0]);
	DDX_Control(pDX, IDC_ST_TASK_STATE2, m_stTaskState[1]);
	DDX_Control(pDX, IDC_ST_TASK_STATE3, m_stTaskState[2]);
	DDX_Control(pDX, IDC_ST_TASK_STATE4, m_stTaskState[3]);
	DDX_Control(pDX, IDC_ST_TASK_STATE5, m_stTaskState[4]);
	DDX_Control(pDX, IDC_ST_TASK_STATE6, m_stTaskState[5]);
	DDX_Control(pDX, IDC_BT_MAIN_RECIPE, m_btRecipeSetting);
	DDX_Control(pDX, IDC_BT_SYSTEM_RECIPE_VIEW, m_btSysParameter);
	DDX_Control(pDX, IDC_BT_MAIN_MAINT, m_btMaintenance);
	DDX_Control(pDX, IDC_BT_SUB_VIEW1, m_btSubView[0]);
	DDX_Control(pDX, IDC_BT_SUB_VIEW2, m_btSubView[1]);
	DDX_Control(pDX, IDC_BT_SUB_VIEW3, m_btSubView[2]);
	DDX_Control(pDX, IDC_BT_SUB_VIEW4, m_btSubView[3]);
	DDX_Control(pDX, IDC_BT_SUB_VIEW5, m_btSubView[4]);
	DDX_Control(pDX, IDC_BT_SUB_VIEW6, m_btSubView[5]);
	DDX_Control(pDX, IDC_TR_RESULT, m_treeResult);
	DDX_Control(pDX, IDC_BT_3D_LENS_ID1, m_btReviewLens[0]);
	DDX_Control(pDX, IDC_BT_3D_LENS_ID2, m_btReviewLens[1]);
	DDX_Control(pDX, IDC_BT_3D_LENS_ID3, m_btReviewLens[2]);
	DDX_Control(pDX, IDC_BT_AF_ON, m_btReviewAFOn);
	DDX_Control(pDX, IDC_BT_AF_OFF, m_btReviewAFOff);
	DDX_Control(pDX, IDC_BTN_REVIEW_LIGHT_SET, m_btSetRevewLightValue);
	DDX_Control(pDX, IDC_EDIT_REVIEW_LIGHT_CMD, m_edReviewLightValue);

	DDX_Control(pDX, IDC_CH_AF_INRANGE, m_CheckAFInRange);
	DDX_Control(pDX, IDC_CH_AF_INFOCUS, m_CheckAFInfocus);
	DDX_Control(pDX, IDC_BT_INIT_AF, m_btAutoFocusInit);
	DDX_Control(pDX, IDC_PRO_SEQ_1, m_progSeqViewBar[0]);
	DDX_Control(pDX, IDC_PRO_SEQ_2, m_progSeqViewBar[1]);
	DDX_Control(pDX, IDC_PRO_SEQ_3, m_progSeqViewBar[2]);
	DDX_Control(pDX, IDC_PRO_SEQ_4, m_progSeqViewBar[3]);
	DDX_Control(pDX, IDC_PRO_SEQ_5, m_progSeqViewBar[4]);
	DDX_Control(pDX, IDC_PRO_SEQ_6, m_progSeqViewBar[5]);
	DDX_Control(pDX, IDC_PRO_SEQ_7, m_progSeqViewBar[6]);
	DDX_Control(pDX, IDC_PRO_SEQ_8, m_progSeqViewBar[7]);
	DDX_Control(pDX, IDC_PRO_SEQ_9, m_progSeqViewBar[8]);
	DDX_Control(pDX, IDC_PRO_SEQ_10, m_progSeqViewBar[9]);
	DDX_Control(pDX, IDC_BT_SEQ_START_1, m_btSeqStart[0]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_1, m_btSeqStop[0]);
	DDX_Control(pDX, IDC_BT_SEQ_START_2, m_btSeqStart[1]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_2, m_btSeqStop[1]);
	DDX_Control(pDX, IDC_BT_SEQ_START_3, m_btSeqStart[2]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_3, m_btSeqStop[2]);
	DDX_Control(pDX, IDC_BT_SEQ_START_4, m_btSeqStart[3]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_4, m_btSeqStop[3]);
	DDX_Control(pDX, IDC_BT_SEQ_START_5, m_btSeqStart[4]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_5, m_btSeqStop[4]);
	DDX_Control(pDX, IDC_BT_SEQ_START_6, m_btSeqStart[5]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_6, m_btSeqStop[5]);
	DDX_Control(pDX, IDC_BT_SEQ_START_7, m_btSeqStart[6]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_7, m_btSeqStop[6]);
	DDX_Control(pDX, IDC_BT_SEQ_START_8, m_btSeqStart[7]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_8, m_btSeqStop[7]);
	DDX_Control(pDX, IDC_BT_SEQ_START_9, m_btSeqStart[8]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_9, m_btSeqStop[8]);
	DDX_Control(pDX, IDC_BT_SEQ_START_10, m_btSeqStart[9]);
	DDX_Control(pDX, IDC_BT_SEQ_STOP_10, m_btSeqStop[9]);

	DDX_Control(pDX, IDC_BT_DOOR_FRONT, m_btUnlockFrontDoor);
	DDX_Control(pDX, IDC_BT_DOOR_EQ_SIDE, m_btUnlockSideDoor);
	DDX_Control(pDX, IDC_BT_DOOR_BACK, m_btUnlockBackDoor);

	DDX_Control(pDX, IDC_ST_OPENCLOSE_FRONT_LEFT,				m_stOpenCloseDoor[0]);
	DDX_Control(pDX, IDC_ST_FRONT_LEFT_LOCK,						m_stLockCheckDoor[0]);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_FRONT_RIGHT,			m_stOpenCloseDoor[1]);
	DDX_Control(pDX, IDC_ST_FRONT_RIGHT_LOCK,					m_stLockCheckDoor[1]);

	DDX_Control(pDX, IDC_ST_OPENCLOSE_INDEX_SID_LEFT,		m_stOpenCloseDoor[2]);
	DDX_Control(pDX, IDC_ST_SIDE_LEFT_INDEX_LOCK,				m_stLockCheckDoor[2]);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_INDEX_SID_RIGHT,		m_stOpenCloseDoor[3]);
	DDX_Control(pDX, IDC_ST_SIDE_RIGHT_INDEX_LOCK,				m_stLockCheckDoor[3]);

	DDX_Control(pDX, IDC_ST_OPENCLOSE_INSPECT_SID_LEFT,	m_stOpenCloseDoor[4]);
	DDX_Control(pDX, IDC_ST_SIDE_LEFT_INSPECT_LOCK,			m_stLockCheckDoor[4]);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_INSPECT_SID_RIGHT,	m_stOpenCloseDoor[5]);
	DDX_Control(pDX, IDC_ST_SIDE_RIGHT_INSPECT_LOCK,			m_stLockCheckDoor[5]);

	DDX_Control(pDX, IDC_ST_OPENCLOSE_BACK_LEFT,				m_stOpenCloseDoor[6]);
	DDX_Control(pDX, IDC_ST_BACK_LEFT_LOCK,						m_stLockCheckDoor[6]);
	DDX_Control(pDX, IDC_ST_OPENCLOSE_BACK_RIGHT,				m_stOpenCloseDoor[7]);
	DDX_Control(pDX, IDC_ST_BACK_RIGHT_LOCK,						m_stLockCheckDoor[7]);

	DDX_Control(pDX, IDC_ST_KEY_STATE, m_stKeyState);
	DDX_Control(pDX, IDC_ST_ENABLE_KEY, m_stEnableKeyOnOff);
	DDX_Control(pDX, IDC_ST_KEY_LOCK, m_stTeachKeyLock);
	DDX_Control(pDX, IDC_BT_SYSTEM_ONOFF, m_btSystemOnOff);
	DDX_Control(pDX, IDC_BT_SYSTEM_EMO_1, m_btEMOState[0]);
	DDX_Control(pDX, IDC_BT_SYSTEM_EMO_2, m_btEMOState[1]);
	DDX_Control(pDX, IDC_BT_SYSTEM_EMO_3, m_btEMOState[2]);
	DDX_Control(pDX, IDC_BT_SYSTEM_EMO_4, m_btEMOState[3]);
	DDX_Control(pDX, IDC_ST_STICK_IN_AM01_1, m_stStickDetect[0]);
	DDX_Control(pDX, IDC_ST_STICK_IN_AM01_2, m_stStickDetect[1]);
	DDX_Control(pDX, IDC_ST_TT01_AM01_P0S, m_stTT01_AM01_pos);
	DDX_Control(pDX, IDC_ST_TT01_TURN_P0S, m_stTT01_TURN_pos);
	DDX_Control(pDX, IDC_ST_TT01_UT02_P0S, m_stTT01_UT02_pos);
	DDX_Control(pDX, IDC_ST_SIGNAL_RED,			m_stSignalTower[0]);
	DDX_Control(pDX, IDC_ST_SIGNAL_YELLOW,		m_stSignalTower[1]);
	DDX_Control(pDX, IDC_ST_SIGNAL_GREEN,		m_stSignalTower[2]);
	DDX_Control(pDX, IDC_ST_SIGNAL_BUZZER,		m_stSignalTower[3]);
	DDX_Control(pDX, IDC_BT_BUZZER_STOP, m_btBuzzerStop);
	DDX_Control(pDX, IDC_BT_SAVE_REVIEW_IMG, m_btSaveReviewImage);
	DDX_Control(pDX, IDC_BT_CIM_INTERFACE, m_btCIMInterface);
	DDX_Control(pDX, IDC_BT_EQ_AUTO_MODE, m_btChangeAutoMode);
	DDX_Control(pDX, IDC_BT_EQ_MANUAL_MODE, m_btChangeManualMode);
	DDX_Control(pDX, IDC_BT_EQ_PM_MODE, m_btChangePMMode);
	DDX_Control(pDX, IDC_BT_ALARM_VIEW, m_btAlarmInterface);

	DDX_Control(pDX, IDC_ST_EQP_NORMAL, m_EQState[0]);
	DDX_Control(pDX, IDC_ST_EQP_FAULT, m_EQState[1]);
	DDX_Control(pDX, IDC_ST_EQP_PM, m_EQState[2]);

	DDX_Control(pDX, IDC_ST_PROC_IDLE, m_EQProcessState[0]);
	DDX_Control(pDX, IDC_ST_PROC_SETUP, m_EQProcessState[1]);
	DDX_Control(pDX, IDC_ST_PROC_EXCUTE, m_EQProcessState[2]);
	DDX_Control(pDX, IDC_ST_PROC_PAUSE, m_EQProcessState[3]);
}

BEGIN_MESSAGE_MAP(CVS10MasterDlg, CDialogEx)
	ON_MESSAGE(WM_VS64USER_CALLBACK, &CVS10MasterDlg::Sys_fnMessageCallback)//VS64 Interface
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_LIVE_VIEW_ONOFF, &CVS10MasterDlg::OnBnClickedBtLiveViewOnoff)
	ON_BN_CLICKED(IDC_BT_MAIN_RECIPE, &CVS10MasterDlg::OnBnClickedBtMainRecipe)

	ON_MESSAGE(WM_USER_RECIPE_CHANGE, &CVS10MasterDlg::OnRecipeChange)
	ON_MESSAGE(WM_USER_RECV_HOST_MSG, &CVS10MasterDlg::OnHostMessage)

	ON_MESSAGE(WM_USER_WRITE_INSPECT_RET, &CVS10MasterDlg::OnWriteInspectResult)
	ON_BN_CLICKED(IDC_BT_SYSTEM_RECIPE_VIEW, &CVS10MasterDlg::OnBnClickedBtSystemRecipeView)
	ON_BN_CLICKED(IDC_BT_MAIN_MAINT, &CVS10MasterDlg::OnBnClickedBtMainMaint)
	ON_BN_CLICKED(IDC_BT_3D_LENS_ID1, &CVS10MasterDlg::OnBnClickedBt3dLensId1)
	ON_BN_CLICKED(IDC_BT_3D_LENS_ID2, &CVS10MasterDlg::OnBnClickedBt3dLensId2)
	ON_BN_CLICKED(IDC_BT_3D_LENS_ID3, &CVS10MasterDlg::OnBnClickedBt3dLensId3)
	ON_BN_CLICKED(IDC_BTN_REVIEW_LIGHT_SET, &CVS10MasterDlg::OnBnClickedBtnReviewLightSet)
	ON_BN_CLICKED(IDC_BT_AF_ON, &CVS10MasterDlg::OnBnClickedBtAfOn)
	ON_BN_CLICKED(IDC_BT_AF_OFF, &CVS10MasterDlg::OnBnClickedBtAfOff)
	ON_BN_CLICKED(IDC_CH_CROSS_ON, &CVS10MasterDlg::OnBnClickedChCrossOn)
	ON_BN_CLICKED(IDC_CH_ROLLER_ON, &CVS10MasterDlg::OnBnClickedChRollerOn)
	ON_BN_CLICKED(IDC_BT_SUB_VIEW1, &CVS10MasterDlg::OnBnClickedBtSubView)
	ON_BN_CLICKED(IDC_BT_SUB_VIEW2, &CVS10MasterDlg::OnBnClickedBtSubView)
	ON_BN_CLICKED(IDC_BT_SUB_VIEW3, &CVS10MasterDlg::OnBnClickedBtSubView)
	ON_BN_CLICKED(IDC_BT_SUB_VIEW4, &CVS10MasterDlg::OnBnClickedBtSubView)
	ON_BN_CLICKED(IDC_BT_SUB_VIEW5, &CVS10MasterDlg::OnBnClickedBtSubView)
	ON_BN_CLICKED(IDC_BT_SUB_VIEW6, &CVS10MasterDlg::OnBnClickedBtSubView)
	ON_BN_CLICKED(IDC_BT_INIT_AF, &CVS10MasterDlg::OnBnClickedBtInitAf)
	ON_BN_CLICKED(IDC_BT_SEQ_START_1, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_1, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_2, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_2, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_3, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_3, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_4, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_4, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_5, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_5, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_6, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_6, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_7, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_7, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_8, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_8, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_9, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_9, &CVS10MasterDlg::OnBnClickedBtSeqStop)
	ON_BN_CLICKED(IDC_BT_SEQ_START_10, &CVS10MasterDlg::OnBnClickedBtSeqStart)
	ON_BN_CLICKED(IDC_BT_SEQ_STOP_10, &CVS10MasterDlg::OnBnClickedBtSeqStop)

	ON_BN_CLICKED(IDC_BUTTON2, &CVS10MasterDlg::OnBnClickedButton2)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDBLCLK()
	ON_BN_CLICKED(IDC_BT_DOOR_FRONT, &CVS10MasterDlg::OnBnClickedBtDoorFront)
	ON_BN_CLICKED(IDC_BT_DOOR_EQ_SIDE, &CVS10MasterDlg::OnBnClickedBtDoorEqSide)
	ON_BN_CLICKED(IDC_BT_DOOR_BACK, &CVS10MasterDlg::OnBnClickedBtDoorBack)
	ON_BN_CLICKED(IDC_RSET_MOT_CMD_ALL, &CVS10MasterDlg::OnBnClickedRsetMotCmdAll)
	ON_BN_CLICKED(IDC_BT_BUZZER_STOP, &CVS10MasterDlg::OnBnClickedBtBuzzerStop)
	ON_BN_CLICKED(IDC_BUTTON3, &CVS10MasterDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BT_SAVE_REVIEW_IMG, &CVS10MasterDlg::OnBnClickedBtSaveReviewImg)
	ON_BN_CLICKED(IDC_CH_DRAW_OVERLAY, &CVS10MasterDlg::OnBnClickedChDrawOverlay)
	ON_NOTIFY(NM_DBLCLK, IDC_TR_RESULT, &CVS10MasterDlg::OnNMDblclkTrResult)
	ON_BN_CLICKED(IDC_BUTTON4, &CVS10MasterDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CVS10MasterDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CVS10MasterDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BT_CIM_INTERFACE, &CVS10MasterDlg::OnBnClickedBtCimInterface)
	ON_BN_CLICKED(IDC_BT_EQ_PM_MODE, &CVS10MasterDlg::OnBnClickedBtEqPmMode)
	ON_BN_CLICKED(IDC_BT_EQ_MANUAL_MODE, &CVS10MasterDlg::OnBnClickedBtEqManualMode)
	ON_BN_CLICKED(IDC_BT_EQ_AUTO_MODE, &CVS10MasterDlg::OnBnClickedBtEqAutoMode)
	ON_BN_CLICKED(IDC_BT_ALARM_VIEW, &CVS10MasterDlg::OnBnClickedBtAlarmView)

	ON_STN_DBLCLK(IDC_ST_TASK_STATE1, &CVS10MasterDlg::OnStnDblclickStTask_Inspector)
	ON_STN_DBLCLK(IDC_ST_TASK_STATE2, &CVS10MasterDlg::OnStnDblclickStTask_Indexer)
	ON_STN_DBLCLK(IDC_ST_TASK_STATE3, &CVS10MasterDlg::OnStnDblclickStTask_Measure)
	ON_STN_DBLCLK(IDC_ST_TASK_STATE4, &CVS10MasterDlg::OnStnDblclickStTask_Motion)
	ON_STN_DBLCLK(IDC_ST_TASK_STATE5, &CVS10MasterDlg::OnStnDblclickStTask_3DScan)
	ON_STN_DBLCLK(IDC_ST_TASK_STATE6, &CVS10MasterDlg::OnStnDblclickStTask_AF)
	ON_BN_CLICKED(IDC_CH_REVEW_CALC, &CVS10MasterDlg::OnBnClickedChRevewCalc)
	ON_BN_CLICKED(IDC_BUTTON7, &CVS10MasterDlg::OnBnClickedButton7)
	
END_MESSAGE_MAP()


// CVS10MasterDlg 메시지 처리기

BOOL CVS10MasterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	extern CXListBox *G_pLogList;
	G_pLogList = &m_listLogView;
	OpenMotionMem(&G_MotInfo);
	//////////////////////////////////////////////////////////////////////////
	//1) Server Interface Connection.
	bool bLinkVS64Server = Sys_fnInitVS64Interface();
	CString NewWindowsName;
	NewWindowsName.Format("Visual Station 64 - Master Task(No.%02d)", m_ProcInitVal.m_nTaskNum);
	SetWindowText(NewWindowsName);

	if(bLinkVS64Server == true)
	{
		G_WriteInfo(Log_Normal, "Server Link OK");
	}
	G_pServerInterface = &m_ServerInterface;
	G_MotObj.m_pServerInterface = &m_ServerInterface;
	//CIM Init
	m_CIMInterface.m_fnInitSocket(&m_ServerInterface);
	G_pCIMInterface = &m_CIMInterface;

	GetDlgItem(IDC_ST_MAP_VIEW_AREA)->GetWindowRect(&m_MapViewArea);
	//ClientToScreen(&m_AccDlgArea);
	ScreenToClient(&m_MapViewArea);

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	GetDlgItem(IDC_ST_IMG_VIEW)->GetClientRect(&m_RectiewArea);
	m_HDCView	=  GetDlgItem(IDC_ST_IMG_VIEW)->GetDC()->m_hAttribDC;
	m_pSampleView = cvLoadImage("./DSC_0078.jpg");
	DrawImageView();


	
	//////////////////////////////////////////////////////////////////////////
	//0)
	G_MotObj.m_fnSetSignalTower(STWOER_MODE0_NONE);
	//1)
	InitCtrl_UI();
	//2)
	SequenceObjectInit();
	//3)
	LoadHWParam();
	//4)
	LoadNowProcRecipe();
	//////////////////////////////////////////////////////////////////////////

	this->CheckDlgButton(IDC_CH_CROSS_ON, BST_CHECKED);

	SetTimer(TIMER_MOT_POS_UPDATE, 100, 0);
	SetTimer(TIMER_TASK_STATE_UPDATE, 3000, 0);
	SetTimer(TIMER_TIME_UPDATE, 1000, 0);
	
	CString strCheckPath;
	strCheckPath.Format("%s\\", VS10Master_Result_PATH);
	G_fnCheckDirAndCreate(strCheckPath.GetBuffer(strCheckPath.GetLength()));
	strCheckPath.Format("%s\\", VS11Indexer_Result_PATH);
	G_fnCheckDirAndCreate(strCheckPath.GetBuffer(strCheckPath.GetLength()));
	strCheckPath.Format("%s\\", VS21Inspector_Result_PATH);
	G_fnCheckDirAndCreate(strCheckPath.GetBuffer(strCheckPath.GetLength()));
	strCheckPath.Format("%s\\", VS31Mesure3D_Result_PATH);
	G_fnCheckDirAndCreate(strCheckPath.GetBuffer(strCheckPath.GetLength()));
	strCheckPath.Format("%s\\", VS32Mesure2D_Result_PATH);
	G_fnCheckDirAndCreate(strCheckPath.GetBuffer(strCheckPath.GetLength()));

	
	GetDlgItem(IDC_ST_EQP_TIME)->SetFont(&G_MidFont);

	CString strEQPName;
	strEQPName.Format("%s", EQP_NAME_A3MSI01N);
	GetDlgItem(IDC_ST_EQP_NAME)->SetFont(&G_RecipeFileFont);
	GetDlgItem(IDC_ST_EQP_NAME)->SetWindowText(strEQPName);

	///////////////////////////////////////////////////////////////////////////////////////////////
	// 현재 실행된 프로그램의 경로를 저장할 변수이다.
	char temp_path[MAX_PATH];

	// 현재 실행된 프로그램의 경로를 얻는다.
	GetModuleFileName(AfxGetInstanceHandle(), temp_path, sizeof(temp_path)); 
	// 버전 정보를 얻기 위해 사용할 핸들값을 저장하는 변수이다.
	DWORD h_version_handle;
	// 버전정보는 항목을 사용자가 추가/삭제 할수 있기 때문에 고정된 크기가 아니다. 
	// 따라서 현재 프로그램의 버전정보에 대한 크기를 얻어서 그 크기에 맞는 메모리를 할당하고 작업해야한다.
	DWORD version_info_size = GetFileVersionInfoSize(temp_path, &h_version_handle);

	// 버전정보를 저장하기 위한 시스템 메모리를 생성한다. ( 핸들 형식으로 생성 )
	HANDLE h_memory = GlobalAlloc(GMEM_MOVEABLE, version_info_size); 
	// 핸들 형식의 메모리를 사용하기 위해서 해당 핸들에 접근할수 있는 주소를 얻는다.
	LPVOID p_info_memory = GlobalLock(h_memory);

	// 현재 프로그램의 버전 정보를 가져온다.
	GetFileVersionInfo(temp_path, h_version_handle, version_info_size, p_info_memory);
	// 버전 정보에 포함된 각 항목별 정보 위치를 저장할 변수이다. 이 포인터에 전달된 주소는 
	// p_info_memory 의 내부 위치이기 때문에 해제하면 안됩니다. 
	// ( p_info_memory 를 참조하는 형식의 포인터 입니다. )
	char *p_data = NULL;
	// 실제로 읽은 정보의 크기를 저장할 변수이다.
	UINT data_size = 0;

	// 세부항목 명시에 사용된 041204b0 는 언어코드이고 "Korean"를 의미합니다.
	// 버전정보에 포함된 Comments 정보를 얻어서 출력합니다.
	//if(VerQueryValue(p_info_memory, "\\StringFileInfo\\041204b0\\Comments", (void **)&p_data, &data_size)){ 
	// SetDlgItemText(IDC_COMMENTS_EDIT, p_data);
	//}

	//// 버전정보에 포함된 CompanyName 정보를 얻어서 출력한다.
	//if(VerQueryValue(p_info_memory, "\\StringFileInfo\\041204b0\\CompanyName", (void **)&p_data, &data_size)){ 
	// SetDlgItemText(IDC_COMPANY_NAME_EDIT, p_data);
	//} 

	//// 버전정보에 포함된 FileDescript-xion 정보를 얻어서 출력한다.
	//if(VerQueryValue(p_info_memory, "\\StringFileInfo\\041204b0\\FileDescript-xion", (void **)&p_data, &data_size)){ 
	// SetDlgItemText(IDC_FILE_DESCRIPT-xION_EDIT, p_data);
	//} 

	// 버전정보에 포함된 FileVersion 정보를 얻어서 출력한다.
	GetDlgItem(IDC_ST_EQP_VERSION)->SetFont(&G_GridFont);
	CString VersionInfo;
	if(VerQueryValue(p_info_memory, "\\StringFileInfo\\041204b0\\FileVersion", (void **)&p_data, &data_size))
	{ 
		VersionInfo.Format("%s", p_data);
		GetDlgItem(IDC_ST_EQP_VERSION)->SetWindowText(VersionInfo);
	} 
	// 버전 정보를 저장하기 위해 사용했던 메모리를 해제한다.
	GlobalUnlock(h_memory); 
	GlobalFree(h_memory);
	//////////////////////////////////////////////////////////////////////////

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}
bool CVS10MasterDlg::CheckProcRecipeName(char *pNewRecipe)
{
	CString tpath;
	tpath.Format("%s\\%s.ini", VS10Master_RECIPE_INI_PATH,pNewRecipe);
	//검색 클래스
	CFileFind finder;
	//CFileFind는 파일, 디렉터리가 존재하면 TRUE 를 반환함
	BOOL bWorking = finder.FindFile(tpath);
	return bWorking?true:false;
}
void CVS10MasterDlg::InputCDTPPointTreeItem(int CntItem, CDTP_Point*  pCDTPPointList)
{
	//Clear Data
	CString strNewName;
	strNewName.Format("CD, TP(%d)", CntItem);
	m_treeResult.SetItemText(m_ResultRoot_CDTP, strNewName);
	HTREEITEM ChildItem = m_treeResult.GetChildItem(m_ResultRoot_CDTP);
	while(ChildItem >0)
	{
		m_treeResult.DeleteItem(ChildItem);
		ChildItem = m_treeResult.GetChildItem(m_ResultRoot_CDTP);
	}
	//////////////////////////////////////////////////////////////////////////

	CString strNewData;
	HTREEITEM SubResultItem = 0;
	for(int iStep = 0; iStep<CntItem; iStep++)
	{
		strNewData.Format("%03d)Point",iStep+1);
		SubResultItem = m_treeResult.InsertItem(strNewData, 5, 5, m_ResultRoot_CDTP);

		switch(pCDTPPointList[iStep].nPosType)
		{
		case CELL_None:				strNewData.Format("Type :None");	break;
		case CELL_LeftTop:			strNewData.Format("Type :LeftTop");	break;
		case CELL_RightTop:			strNewData.Format("Type :RightTop");	break;
		case CELL_RightBottom:		strNewData.Format("Type :RightBottom");	break;
		case CELL_LeftBottom:		strNewData.Format("Type :LeftBottom");	break;
		case CELL_Center:			strNewData.Format("Type :Center");	break;
		}
		m_treeResult.InsertItem(strNewData, 8, 8, SubResultItem);
		strNewData.Format("StartX :%d", pCDTPPointList[iStep].nPointX);
		m_treeResult.InsertItem(strNewData, 8, 8, SubResultItem);
		strNewData.Format("StartY :%d", pCDTPPointList[iStep].nPointY);
		m_treeResult.InsertItem(strNewData, 8, 8, SubResultItem);
	}
}
void CVS10MasterDlg::InputScope3DPointTreeItem(int CntItem, SCOPE_3DPoint*  p3DMeasurPointList)
{
	//Clear Data
	CString strNewName;
	strNewName.Format("3D_Scope(%d)", CntItem);
	m_treeResult.SetItemText(m_ResultRoot_3DScope, strNewName);
	HTREEITEM ChildItem = m_treeResult.GetChildItem(m_ResultRoot_3DScope);
	while(ChildItem >0)
	{
		m_treeResult.DeleteItem(ChildItem);
		ChildItem = m_treeResult.GetChildItem(m_ResultRoot_3DScope);
	}
	//////////////////////////////////////////////////////////////////////////

	CString strNewData;
	HTREEITEM SubResultItem = 0;
	for(int iStep = 0; iStep<CntItem; iStep++)
	{
		strNewData.Format("%03d)Point",iStep+1);
		SubResultItem = m_treeResult.InsertItem(strNewData, 5, 5, m_ResultRoot_3DScope);

		switch(p3DMeasurPointList[iStep].nPosType)
		{
		case CELL_None:				strNewData.Format("Type :None");	break;
		case CELL_LeftTop:			strNewData.Format("Type :LeftTop");	break;
		case CELL_RightTop:			strNewData.Format("Type :RightTop");	break;
		case CELL_RightBottom:	strNewData.Format("Type :RightBottom");	break;
		case CELL_LeftBottom:		strNewData.Format("Type :LeftBottom");	break;
		case CELL_Center:			strNewData.Format("Type :Center");	break;
		}
		m_treeResult.InsertItem(strNewData, 8, 8, SubResultItem);
		strNewData.Format("StartX :%d", p3DMeasurPointList[iStep].nPointX);
		m_treeResult.InsertItem(strNewData, 8, 8, SubResultItem);
		strNewData.Format("StartY :%d", p3DMeasurPointList[iStep].nPointY);
		m_treeResult.InsertItem(strNewData, 8, 8, SubResultItem);
	}
}
void CVS10MasterDlg::LoadNowProcRecipe(bool bAlignPosSet)
{
	//진행 중이었던 Recipe를 로드 한다.
	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	G_WriteInfo(Log_Info, "Recipe Reload(%s)", strReadData);

	CString strProcRecipePath;
	strProcRecipePath.Format("%s\\%s.ini", VS10Master_RECIPE_INI_PATH, strReadData);
	LoadRecipeParam(strProcRecipePath.GetBuffer(strProcRecipePath.GetLength()), bAlignPosSet);
	//////////////////////////////////////////////////////////////////////////
	int InputIndex = 1;
	CString strNewValue;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunBoxID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("%s", strReadData);
	if(strNewValue.GetLength()==0)
		strNewValue.Format("None");
	m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("%s", strReadData);
	if(strNewValue.GetLength()==0)
		strNewValue.Format("None");
	m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strNewValue.Format("%s", strReadData);
	m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);

	//strNewValue.Format("%dum", m_GlassReicpeParam.CELL_SIZE_X);
	//m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//strNewValue.Format("%dum", m_GlassReicpeParam.CELL_SIZE_Y);
	//m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//strNewValue.Format("%d", m_GlassReicpeParam.GLASS_CELL_COUNT_X);
	//m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//strNewValue.Format("%d", m_GlassReicpeParam.GLASS_CELL_COUNT_Y);
	//m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//strNewValue.Format("%dum", m_GlassReicpeParam.CELL_DISTANCE_X);
	//m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//strNewValue.Format("%dum", m_GlassReicpeParam.CELL_DISTANCE_Y);
	//m_gridStickInfo.SetItemText(InputIndex, 1, strNewValue); m_gridStickInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);

	m_gridStickInfo.Refresh();
	m_nCellCnt = 0;
	if(m_pCellList != nullptr)
		delete [] m_pCellList;
	m_pCellList = nullptr;

	//Align을 할경우 실제 좌표가 입력 되어야 한다.
	//int DrawCellStartX = 0;
	//int DrawCellStartY = 0;

	if(m_GlassReicpeParam.GLASS_CELL_COUNT_X>0 && m_GlassReicpeParam.GLASS_CELL_COUNT_Y>0)
	{
		//Inspection Cell List 
		int CellGapDisX = abs(m_GlassReicpeParam.CELL_DISTANCE_X-m_GlassReicpeParam.CELL_SIZE_X);
		int CellGapDisY = abs(m_GlassReicpeParam.CELL_DISTANCE_Y-m_GlassReicpeParam.CELL_SIZE_Y);

		int AllCellDisX = (m_GlassReicpeParam.GLASS_CELL_COUNT_X*m_GlassReicpeParam.CELL_SIZE_X) +
							 ((m_GlassReicpeParam.GLASS_CELL_COUNT_X-1)*CellGapDisX);
		int AllCellDisY = (m_GlassReicpeParam.GLASS_CELL_COUNT_Y*m_GlassReicpeParam.CELL_SIZE_Y) +
							 ((m_GlassReicpeParam.GLASS_CELL_COUNT_Y-1)*CellGapDisY);


		//3D Scan Area Setting
		int OverScan = 0;
		m_Scan3DParam.nScan_Count = m_GlassReicpeParam.STICK_WIDTH;
		if(m_Scan3DParam.nScan_Count%SCANER_SENSOR_SHIFT>0)
			OverScan = 1;
		m_Scan3DParam.nScan_Count /= SCANER_SENSOR_SHIFT;
		m_Scan3DParam.nScan_Count+=OverScan;
		m_Scan3DParam.nScan_Distance = AllCellDisY;
		strNewValue.Format("%d", m_Scan3DParam.nScan_Count);
		m_SubAreaScanView->m_st3DScanTotalCnt.SetWindowText(strNewValue);

		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		memset(m_Scan3DParam.strFileName,0x00, 64);
		sprintf_s(m_Scan3DParam.strFileName,"%s", strReadData);
		
		memset(&m_Scan3DResult, 0x00, sizeof(SCAN_3DResult));
		//Recipe Load Cell 좌표.
		//m_AlignCellStartX = (m_GlassReicpeParam.STICK_WIDTH/2)-(AllCellDisX/2);//um
		//m_AlignCellStartY = (1190000/2)-(AllCellDisY/2);//um
		//m_AlignCellStartX += 25500;
		//m_AlignCellStartY += 25500;
		m_GlassReicpeParam.CenterPointX =  m_AlignCellStartX+(AllCellDisX/2);
		m_GlassReicpeParam.CenterPointY =  m_AlignCellStartY+(AllCellDisY/2);

		m_GlassReicpeParam.CELL_EDGE_X = abs(AllCellDisX/2)*(-1);
		m_GlassReicpeParam.CELL_EDGE_Y = abs(AllCellDisY/2);

		m_nCellCnt = (m_GlassReicpeParam.GLASS_CELL_COUNT_X*m_GlassReicpeParam.GLASS_CELL_COUNT_Y);
		if(m_nCellCnt>0)
		{
			m_pCellList = new CELLINFO[m_nCellCnt];
			int InputCnt =0;
			for(int CWStep = 0; CWStep<m_GlassReicpeParam.GLASS_CELL_COUNT_X; CWStep++)
			{
				for(int CHStep = 0; CHStep<m_GlassReicpeParam.GLASS_CELL_COUNT_Y; CHStep++)
				{
					m_pCellList[InputCnt].m_CellName[0]= 'C';

					m_pCellList[InputCnt].m_CellPos.left		= (m_GlassReicpeParam.CenterPointX+m_GlassReicpeParam.CELL_EDGE_X)+(m_GlassReicpeParam.CELL_DISTANCE_X*CWStep);
					m_pCellList[InputCnt].m_CellPos.top		= (m_GlassReicpeParam.CenterPointY+m_GlassReicpeParam.CELL_EDGE_Y*(-1))+(m_GlassReicpeParam.CELL_DISTANCE_Y*CHStep);
					m_pCellList[InputCnt].m_CellPos.right		= m_pCellList[InputCnt].m_CellPos.left+m_GlassReicpeParam.CELL_SIZE_X;
					m_pCellList[InputCnt].m_CellPos.bottom	= m_pCellList[InputCnt].m_CellPos.top+m_GlassReicpeParam.CELL_SIZE_Y;
					InputCnt++;
				}
			}
			m_p3DMapViewer->m_3DViewObj.SetRendDataCell(
				m_GlassReicpeParam.STICK_WIDTH,
				m_GlassReicpeParam.STICK_EDGE_DIS_WIDTH, 
				m_GlassReicpeParam.STICK_EDGE_DIS_HEIGHT, 
				m_nCellCnt, m_pCellList);
		}
		//////////////////////////////////////////////////////////////////////////
		//CD TP Point List.
		m_p3DMapViewer->m_3DViewObj.m_ViewDrawCDTPPoint = false;
		m_p3DMapViewer->m_3DViewObj.RenderScreen();
		m_CDTPPointCnt = 0;
		if(m_pCDTPPointList != nullptr)
			delete [] m_pCDTPPointList;
		m_pCDTPPointList = nullptr;
		

		//Cell 당 4Point 이다.
		m_CDTPPointCnt =  m_GlassReicpeParam.GLASS_CELL_COUNT_X*m_GlassReicpeParam.GLASS_CELL_COUNT_Y*4;
		m_pCDTPPointList = new CDTP_Point[m_CDTPPointCnt];
		//TP결과 Buf Reset;
		m_TP_Result.ResetCellTPBuf();

		int InputCnt = 0;
		int CDTPPosIndex = 0;
		for(int CWStep = 1; CWStep<=m_GlassReicpeParam.GLASS_CELL_COUNT_X; CWStep++)
		{
			for(int CHStep = 1; CHStep<=m_GlassReicpeParam.GLASS_CELL_COUNT_Y; CHStep++)
			{
				m_pCDTPPointList[CDTPPosIndex].nPosType = CELL_LeftTop;
				m_pCDTPPointList[CDTPPosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left;
				m_pCDTPPointList[CDTPPosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top;
				CDTPPosIndex++;
				m_pCDTPPointList[CDTPPosIndex].nPosType = CELL_RightTop;
				m_pCDTPPointList[CDTPPosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.right;
				m_pCDTPPointList[CDTPPosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top;
				CDTPPosIndex++;
				m_pCDTPPointList[CDTPPosIndex].nPosType = CELL_RightBottom;
				m_pCDTPPointList[CDTPPosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.right;
				m_pCDTPPointList[CDTPPosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.bottom;
				CDTPPosIndex++;
				m_pCDTPPointList[CDTPPosIndex].nPosType = CELL_LeftBottom;
				m_pCDTPPointList[CDTPPosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left;
				m_pCDTPPointList[CDTPPosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.bottom;
				CDTPPosIndex++;
				InputCnt++;
			}
		}
		//Tree 항목 Clear

		InputCDTPPointTreeItem(m_CDTPPointCnt, m_pCDTPPointList);
		m_p3DMapViewer->m_3DViewObj.SetRendCDTP(m_CDTPPointCnt, m_pCDTPPointList);
		m_p3DMapViewer->m_3DViewObj.RenderScreen();
		//////////////////////////////////////////////////////////////////////////
		//Scope Measur Point List.
		m_p3DMapViewer->m_3DViewObj.m_ViewDraw3DMeasurPoint = false;
		m_p3DMapViewer->m_3DViewObj.RenderScreen();

		m_3DMeasurPointCnt = 0;
		if(m_p3DMeasurPointList != nullptr)
			delete [] m_p3DMeasurPointList;
		m_p3DMeasurPointList = nullptr;

		int MeasurCellCnt = 0;
		bool bNowInsertCell =  false;
		for(int CWStep = 1; CWStep<=m_GlassReicpeParam.GLASS_CELL_COUNT_X; CWStep++)
		{
			for(int CHStep = 1; CHStep<=m_GlassReicpeParam.GLASS_CELL_COUNT_Y; CHStep++)
			{
				bNowInsertCell =  false;
				//switch(m_Scope3DParam.nMeasurCelType)
				//{
				//case 1: bNowInsertCell = true; break; //All
				//case 2: bNowInsertCell =(/*(CWStep%2 ==1?true:false) && */(CHStep%2 ==1?true:false)); break;//Odd
				//case 3: bNowInsertCell =(/*(CWStep%2 ==0?true:false) && */(CHStep%2 ==0?true:false)); break;//even
				//}

				for(int nStep =0; nStep<m_Scope3DParam.nMeasurCellCnt; nStep++)
				{
					if(m_Scope3DParam.nMeasurCellList[nStep] == CHStep)
					{
						bNowInsertCell = true;
						break;
					}
				}


				if(bNowInsertCell == true)
					MeasurCellCnt++;
			}
		}
		switch(m_Scope3DParam.nMeasurPointType)
		{
		case 1: MeasurCellCnt *= 1; break;//Cell Edge 1Point
		case 2: MeasurCellCnt *= 1; break;//Cell Center 1Point
		case 3: MeasurCellCnt *= 2; break;//Cell Edge 대각 2Point
		case 4: MeasurCellCnt *= 4; break;//Cell Edge All 4Point 
		case 5: MeasurCellCnt *= 5; break;//Cell All 5Point 
		case 6: MeasurCellCnt *= 3; break;//Cell Cell Center 3Point
		}
		
		m_3DMeasurPointCnt =  MeasurCellCnt;
		m_p3DMeasurPointList = new SCOPE_3DPoint[m_3DMeasurPointCnt];
		int MeasurePosIndex = 0;
		int CellLineIndex = 0;
		InputCnt =0;


		int nPosScopeShiftOffset = 0;
		int nPosScopeDriveOffset = 0;

		char strReadData[256];
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetShift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeShiftOffset = atoi(strReadData);
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetDrive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeDriveOffset = atoi(strReadData);

		for(int CWStep = 1; CWStep<=m_GlassReicpeParam.GLASS_CELL_COUNT_X; CWStep++)
		{
			for(int CHStep = 1; CHStep<=m_GlassReicpeParam.GLASS_CELL_COUNT_Y; CHStep++)
			{
				bNowInsertCell =  false;
				//switch(m_Scope3DParam.nMeasurCelType)
				//{
				//case 1: //All
				//		bNowInsertCell = true;
				//	break;
				//case 2: //Odd
				//		bNowInsertCell =(/*(CWStep%2 ==1?true:false) && */(CHStep%2 ==1?true:false));
				//	break;
				//case 3: //even
				//		bNowInsertCell =(/*(CWStep%2 ==0?true:false) && */(CHStep%2 ==0?true:false));
				//	break;
				//}
				for(int nStep =0; nStep<m_Scope3DParam.nMeasurCellCnt; nStep++)
				{
					if(m_Scope3DParam.nMeasurCellList[nStep] == CHStep)
					{
						bNowInsertCell = true;
						break;
					}
				}


				//모든 좌표는 Review 기준 좌표 임으로 Scope 좌표로 변환 한다.
				if(bNowInsertCell == true)
				{
					switch(m_Scope3DParam.nMeasurPointType)
					{
					case 1: 
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_LeftTop;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top; 

						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left+110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top+80; 

						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 

						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top; 
						MeasurePosIndex++;
						break;//Cell Edge 1Point
					case 2: 
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_Center;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2;
						
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2;

						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 
						
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top; 

						MeasurePosIndex++;
						break;//Cell Center 1Point
					case 3: 
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_LeftTop;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left+110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top+80;
						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top; 


						MeasurePosIndex++;
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_RightBottom;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.right-110;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.bottom-80;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.right;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.bottom;

						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.bottom; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.bottom; 

						MeasurePosIndex++;
						break;//Cell Edge 대각 2Point
					case 4:// ] 로 그리며 이동 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_LeftTop;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left+110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top+80;
						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top; 
						MeasurePosIndex++;
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_RightTop;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.right;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.right-110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top+80;
						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top; 
						MeasurePosIndex++;
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_RightBottom;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.right;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.bottom;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.right-110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.bottom-80;

						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.bottom; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.bottom; 
						MeasurePosIndex++;
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_LeftBottom;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.bottom;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left+110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.bottom-80;
						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.bottom; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.bottom; 
						MeasurePosIndex++;
						break;//Cell Edge All 4Point 
					case 5:// 3을 그린다.
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_LeftTop;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.left+110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.top+80;
						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.top; 
						MeasurePosIndex++;
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_RightTop;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.right;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.right-110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.top+80;
						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.top; 
						MeasurePosIndex++;
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_Center;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2;

						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_p3DMeasurPointList[MeasurePosIndex].nPointY; 
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.left + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = m_p3DMeasurPointList[MeasurePosIndex].nPointY; 
						MeasurePosIndex++;
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_RightBottom;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.right;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.bottom;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.right-110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.bottom-80;
						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.bottom; 

						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.right + 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.bottom; 
						MeasurePosIndex++;
						m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_LeftBottom;
						m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left;
						m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.bottom;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.left+110;
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.bottom-80;
						//AF위치를 기록 한다.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.bottom; 

						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset +m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
						m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset +m_pCellList[InputCnt].m_CellPos.bottom; 
						MeasurePosIndex++;
						break;//Cell All 5Point 
					case 6:
						{
							int nCellHeightOffset = m_pCellList[InputCnt].m_CellPos.Height()/4;
							m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_Center;
							m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
							m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2;

							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2;
							//AF위치를 기록 한다.
							m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
							m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 

							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top; 
							MeasurePosIndex++;

							m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_Center;
							m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
							m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2 - nCellHeightOffset;

							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2- nCellHeightOffset;
							//AF위치를 기록 한다.
							//m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = 0;//Skip AF
							//m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = 0;//Skip AF

							//m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = 0;//Skip AF
							//m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = 0;//Skip AF
							m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
							m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 

							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top; 
							MeasurePosIndex++;

							m_p3DMeasurPointList[MeasurePosIndex].nPosType = CELL_Center;
							m_p3DMeasurPointList[MeasurePosIndex].nPointX = m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
							m_p3DMeasurPointList[MeasurePosIndex].nPointY = m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2 + nCellHeightOffset;

							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left + m_pCellList[InputCnt].m_CellPos.Width()/2;
							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top + m_pCellList[InputCnt].m_CellPos.Height()/2 + nCellHeightOffset;
							//AF위치를 기록 한다.
							m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
							m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = m_pCellList[InputCnt].m_CellPos.top; 

							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = nPosScopeShiftOffset + m_pCellList[InputCnt].m_CellPos.left - 1000;//1mm 왼쪽(Cell Out 위치.
							m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = nPosScopeDriveOffset + m_pCellList[InputCnt].m_CellPos.top; 
							//m_p3DMeasurPointList[MeasurePosIndex].nAFPointX = 0;//Skip AF
							//m_p3DMeasurPointList[MeasurePosIndex].nAFPointY = 0;//Skip AF

							//m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointX = 0;//Skip AF
							//m_p3DMeasurPointList[MeasurePosIndex].nScopeAxisAFPointY = 0;//Skip AF
							MeasurePosIndex++;


							
						}break;
					}
				}
				InputCnt++;
			}
		}
		InputScope3DPointTreeItem(m_3DMeasurPointCnt, m_p3DMeasurPointList);
		m_p3DMapViewer->m_3DViewObj.SetRendData3DScope(m_3DMeasurPointCnt, m_p3DMeasurPointList);
		m_p3DMapViewer->m_3DViewObj.RenderScreen();
	}
	else
		G_WriteInfo(Log_Error, "Recipe Cell Cnt Setting Error!!");
}
void CVS10MasterDlg::InitCtrl_UI()
{
	//////////////////////////////////////////////////////////////////////////
	G_TaskStateFont.CreateFont(70, 0, 0, 0, FW_BOLD,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Arial Black");

	G_TaskStateSmallFont.CreateFont(40, 0, 0, 0, FW_THIN,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Arial Black");

	G_RecipeFileFont.CreateFont(35, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Consolas");

	G_NormalFont.CreateFont(15, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	G_GridFont.CreateFont(13, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	G_NumberFont.CreateFont(18, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Times New Roman");

	G_SmallFont.CreateFont(10, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	G_GridSmallFont.CreateFont(12, 0, 0, 0, FW_DONTCARE ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	G_MidFont.CreateFont(22, 0, 0, 0, FW_DONTCARE ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "굴림");

	//////////////////////////////////////////////////////////////////////////

	m_btBackColor = G_Color_WinBack;
	//////////////////////////////////////////////////////////////////////////
	//Review Cam 2448X2048

	CRect LiveViewArea;
	float ViewWidthValue = (float)REAL_IMAGE_SIZE_Y/(float)REAL_IMAGE_SIZE_X;
	GetDlgItem(IDC_ST_LIVE_VIEW_AREA)->GetClientRect(&LiveViewArea);
	GetDlgItem(IDC_ST_LIVE_VIEW_AREA)->SetWindowPos(nullptr, 0, 0, LiveViewArea.Width(),(int)(LiveViewArea.Width()*ViewWidthValue), SWP_NOMOVE);
	GetDlgItem(IDC_ST_LIVE_VIEW_AREA)->GetClientRect(&LiveViewArea);
	

	CheckDlgButton(IDC_CH_DRAW_OVERLAY, BST_UNCHECKED);

	m_LiveViewObj.InitCamCtrl(0, 0, GetDlgItem(IDC_ST_LIVE_VIEW_AREA)->GetSafeHwnd(),"D:\\nBiz_InspectStock\\DCFs\\CISG20U20A_0.dcf");
	

	GetDlgItem(IDC_ST_LIVE_VIEW_AREA)->GetClientRect(&m_TheataAlignRectiewArea);
	m_TheataAlignHDCView	=  GetDlgItem(IDC_ST_LIVE_VIEW_AREA)->GetDC()->m_hAttribDC;
	m_ThetaAlignLiveObj.InitLiveThread();
	m_ThetaAlignLiveObj.SetLiveDisplayObj(m_TheataAlignHDCView, &m_TheataAlignRectiewArea);
	m_ThetaAlignLiveObj.SetCamLiveMode(LIVE_MODE_GRAB);
	//////////////////////////////////////////////////////////////////////////
	m_p3DMapViewer = new CMapViewDlg(this);

	m_p3DMapViewer->m_3DViewObj.m_pbUpReviewCylinder = &m_bUpReviewCylinder;
	m_p3DMapViewer->m_3DViewObj.m_pbUpRingLightCylinder = &m_bUpRingLightCylinder;
	m_p3DMapViewer->m_3DViewObj.m_pbScopeSafetyPos = &m_LiveViewObj.m_bScopeSafetyPos;
	m_p3DMapViewer->Create(IDD_DLG_MAP_VIEW);
	m_p3DMapViewer->SetWindowPos(nullptr,  m_MapViewArea.left, m_MapViewArea.top, m_MapViewArea.Width(), m_MapViewArea.Height(), SWP_SHOWWINDOW);
	m_p3DMapViewer->ShowWindow(SW_SHOW);//SW_SHOW
	//////////////////////////////////////////////////////////////////////////
	SIZE Screen;
	memset(&Screen, 0x00, sizeof(SIZE));
	Screen.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN);
	Screen.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN);
	this->SetWindowPos(nullptr, 0, 0, Screen.cx-205, 1080, SWP_SHOWWINDOW);

	m_edReviewLightValue.SetFont(&G_NumberFont);

	tButtonStyle tStyle;
	// Get default Style Dark
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);


	m_tStyleAlarmBT.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0xFF, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0xFF, 0x60, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xFF, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xFF, 0x40, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleAlarmBT.SetButtonStyle(&tStyle);
	
	m_tStyleCIMBT.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleCIMBT.SetButtonStyle(&tStyle);

	
	// Get default Style Dark Gray
	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 12.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x20, 0x20, 0x20);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyleOff.SetButtonStyle(&tStyle);
	
	tColorScheme tColorOnOff;
	LOGFONT tFontOnOff;
	m_bLiveViewOnOff.GetTextColor(&tColorOnOff);
	m_bLiveViewOnOff.GetFont(&tFontOnOff);
	{
		// Change Color
		// Button is disabled
		//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
		// Button is enabled but not clicked
		tColorOnOff.m_tEnabled	= RGB(0x00, 0x00, 0xFF);
		// Button is clicked, meaning checked as check box or selected as radio button
		tColorOnOff.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
		// Button is pressed, Mouse is on button and left mouse button pressed
		tColorOnOff.m_tPressed	= RGB(0x00, 0x00, 0xAF);

		strcpy_s(tFontOnOff.lfFaceName,20,"굴림");
		tFontOnOff.lfHeight =12;
	}
	m_btSystemOnOff.SetRoundButtonStyle(&m_tStyleOn);
	m_btSystemOnOff.SetTextColor(&tColorOnOff);
	m_btSystemOnOff.SetCheckButton(true, true);
	m_btSystemOnOff.SetFont(&tFontOnOff);
	

	for(int iStep = 0; iStep<4; iStep++)
	{
		m_btEMOState[iStep].SetRoundButtonStyle(&m_tStyleOn);
		m_btEMOState[iStep].SetTextColor(&tColorOnOff);
		m_btEMOState[iStep].SetCheckButton(true, true);
		m_btEMOState[iStep].SetFont(&tFontOnOff);
	}

	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_bLiveViewOnOff.GetTextColor(&tColor);
		m_bLiveViewOnOff.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_bLiveViewOnOff.SetRoundButtonStyle(&m_tButtonStyle);
		m_bLiveViewOnOff.SetTextColor(&tColor);
		m_bLiveViewOnOff.SetCheckButton(true, true);
		m_bLiveViewOnOff.SetFont(&tFont);

		m_bLiveViewOnOff.SetCheck(true);
		m_bLiveViewOnOff.SetWindowText("Live Off");

		m_btRecipeSetting.SetRoundButtonStyle(&m_tButtonStyle);
		m_btRecipeSetting.SetTextColor(&tColor);
		m_btRecipeSetting.SetCheckButton(true, true);
		m_btRecipeSetting.SetFont(&tFont);

		m_btSysParameter.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSysParameter.SetTextColor(&tColor);
		m_btSysParameter.SetCheckButton(true, true);
		m_btSysParameter.SetFont(&tFont);

		m_btMaintenance.SetRoundButtonStyle(&m_tButtonStyle);
		m_btMaintenance.SetTextColor(&tColor);
		m_btMaintenance.SetCheckButton(true, true);
		m_btMaintenance.SetFont(&tFont);

		m_btReviewAFOn.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReviewAFOn.SetTextColor(&tColor);
		m_btReviewAFOn.SetCheckButton(true, true);
		m_btReviewAFOn.SetFont(&tFont);

		m_btReviewAFOff.SetRoundButtonStyle(&m_tButtonStyle);
		m_btReviewAFOff.SetTextColor(&tColor);
		m_btReviewAFOff.SetCheckButton(true, true);
		m_btReviewAFOff.SetFont(&tFont);
		m_btReviewAFOff.SetCheck(true);

		m_btSetRevewLightValue.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSetRevewLightValue.SetTextColor(&tColor);
		m_btSetRevewLightValue.SetCheckButton(true, true);
		m_btSetRevewLightValue.SetFont(&tFont);

		m_btAutoFocusInit.SetRoundButtonStyle(&m_tButtonStyle);
		m_btAutoFocusInit.SetTextColor(&tColor);
		m_btAutoFocusInit.SetCheckButton(true, true);
		m_btAutoFocusInit.SetFont(&tFont);
		
		m_btCIMInterface.SetRoundButtonStyle(&m_tStyleCIMBT);
		m_btCIMInterface.SetTextColor(&tColorOnOff);
		m_btCIMInterface.SetCheckButton(true, true);
		m_btCIMInterface.SetFont(&tFont);

		m_btAlarmInterface.SetRoundButtonStyle(&m_tStyleAlarmBT);
		m_btAlarmInterface.SetTextColor(&tColorOnOff);
		m_btAlarmInterface.SetCheckButton(true, true);
		m_btAlarmInterface.SetFont(&tFont);
		
		for(int iStep = 0; iStep<6; iStep++)
		{
			m_btSubView[iStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btSubView[iStep].SetTextColor(&tColor);
			m_btSubView[iStep].SetCheckButton(true, true);
			m_btSubView[iStep].SetFont(&tFont);
		}

		for(int iStep = 0; iStep<3; iStep++)
		{
			m_btReviewLens[iStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btReviewLens[iStep].SetTextColor(&tColor);
			m_btReviewLens[iStep].SetCheckButton(true, true);
			m_btReviewLens[iStep].SetFont(&tFont);
		}

		for(int iStep = 0; iStep<10; iStep++)
		{
			m_btSeqStart[iStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btSeqStart[iStep].SetTextColor(&tColor);
			m_btSeqStart[iStep].SetCheckButton(true, true);
			m_btSeqStart[iStep].SetFont(&tFont);
		}

		for(int iStep = 0; iStep<10; iStep++)
		{
			m_btSeqStop[iStep].SetRoundButtonStyle(&m_tButtonStyle);
			m_btSeqStop[iStep].SetTextColor(&tColor);
			m_btSeqStop[iStep].SetCheckButton(true, true);
			m_btSeqStop[iStep].SetFont(&tFont);
		}

		m_btUnlockFrontDoor.SetRoundButtonStyle(&m_tButtonStyle);
		m_btUnlockFrontDoor.SetTextColor(&tColor);
		m_btUnlockFrontDoor.SetCheckButton(true, true);
		m_btUnlockFrontDoor.SetFont(&tFont);

		m_btUnlockSideDoor.SetRoundButtonStyle(&m_tButtonStyle);
		m_btUnlockSideDoor.SetTextColor(&tColor);
		m_btUnlockSideDoor.SetCheckButton(true, true);
		m_btUnlockSideDoor.SetFont(&tFont);

		m_btUnlockBackDoor.SetRoundButtonStyle(&m_tButtonStyle);
		m_btUnlockBackDoor.SetTextColor(&tColor);
		m_btUnlockBackDoor.SetCheckButton(true, true);
		m_btUnlockBackDoor.SetFont(&tFont);

		m_btBuzzerStop.SetRoundButtonStyle(&m_tButtonStyle);
		m_btBuzzerStop.SetTextColor(&tColor);
		m_btBuzzerStop.SetCheckButton(true, true);
		m_btBuzzerStop.SetFont(&tFont);

		m_btSaveReviewImage.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSaveReviewImage.SetTextColor(&tColor);
		m_btSaveReviewImage.SetCheckButton(true, true);
		m_btSaveReviewImage.SetFont(&tFont);

		
		m_btChangeAutoMode.SetRoundButtonStyle(&m_tButtonStyle);
		m_btChangeAutoMode.SetTextColor(&tColor);
		m_btChangeAutoMode.SetCheckButton(true, true);
		m_btChangeAutoMode.SetFont(&tFont);

		m_btChangeManualMode.SetRoundButtonStyle(&m_tButtonStyle);
		m_btChangeManualMode.SetTextColor(&tColor);
		m_btChangeManualMode.SetCheckButton(true, true);
		m_btChangeManualMode.SetFont(&tFont);

		m_btChangePMMode.SetRoundButtonStyle(&m_tButtonStyle);
		m_btChangePMMode.SetTextColor(&tColor);
		m_btChangePMMode.SetCheckButton(true, true);
		m_btChangePMMode.SetFont(&tFont);
		
	}
	m_stKeyState.SetFont(&G_NormalFont);
	m_stKeyState.SetTextColor(RGB(0,0,0));
	m_stKeyState.SetBkColor(G_Color_Off);

	m_stStickDetect[0].SetFont(&G_SmallFont);
	m_stStickDetect[0].SetTextColor(RGB(0,0,0));
	m_stStickDetect[0].SetBkColor(G_Color_Off);

	m_stStickDetect[1].SetFont(&G_SmallFont);
	m_stStickDetect[1].SetTextColor(RGB(0,0,0));
	m_stStickDetect[1].SetBkColor(G_Color_Off);
	

	m_stSignalTower[0].SetFont(&G_SmallFont);
	m_stSignalTower[0].SetTextColor(RGB(0,0,0));
	m_stSignalTower[0].SetBkColor(G_CSignalTower_Red_OFF);

	m_stSignalTower[1].SetFont(&G_SmallFont);
	m_stSignalTower[1].SetTextColor(RGB(0,0,0));
	m_stSignalTower[1].SetBkColor(G_CSignalTower_Yellow_OFF);

	m_stSignalTower[2].SetFont(&G_SmallFont);
	m_stSignalTower[2].SetTextColor(RGB(0,0,0));
	m_stSignalTower[2].SetBkColor(G_CSignalTower_Green_OFF);

	m_stSignalTower[3].SetFont(&G_SmallFont);
	m_stSignalTower[3].SetTextColor(RGB(0,0,0));
	m_stSignalTower[3].SetBkColor(G_CSignalTower_Buzzer_OFF);

	m_stTT01_AM01_pos.SetFont(&G_SmallFont);
	m_stTT01_AM01_pos.SetTextColor(RGB(0,0,0));
	m_stTT01_AM01_pos.SetBkColor(G_Color_Off);

	m_stTT01_TURN_pos.SetFont(&G_SmallFont);
	m_stTT01_TURN_pos.SetTextColor(RGB(0,0,0));
	m_stTT01_TURN_pos.SetBkColor(G_Color_Off);

	m_stTT01_UT02_pos.SetFont(&G_SmallFont);
	m_stTT01_UT02_pos.SetTextColor(RGB(0,0,0));
	m_stTT01_UT02_pos.SetBkColor(G_Color_Off);


	m_stEnableKeyOnOff.SetFont(&G_NormalFont);
	m_stEnableKeyOnOff.SetTextColor(RGB(0,0,0));
	m_stEnableKeyOnOff.SetBkColor(G_Color_Off);

	m_stTeachKeyLock.SetFont(&G_SmallFont);
	m_stTeachKeyLock.SetTextColor(RGB(0,0,0));
	m_stTeachKeyLock.SetBkColor(G_Color_Lock);
	
	for(int StStep =0; StStep<8; StStep++)
	{
		m_stOpenCloseDoor[StStep].SetFont(&G_SmallFont);
		m_stOpenCloseDoor[StStep].SetTextColor(RGB(0,0,0));
		m_stOpenCloseDoor[StStep].SetBkColor(G_Color_WinBack);
	}

	for(int StStep =0; StStep<8; StStep++)
	{
		m_stLockCheckDoor[StStep].SetFont(&G_SmallFont);
		m_stLockCheckDoor[StStep].SetTextColor(RGB(0,0,0));
		m_stLockCheckDoor[StStep].SetBkColor(G_Color_WinBack);
	}
	for(int TStep =0; TStep<6; TStep++)
	{
		//m_stTaskState[TStep].SetFont(&G_PosEditFont);
		m_stTaskState[TStep].SetTextColor(RGB(0,0,0));
		m_stTaskState[TStep].SetBkColor(G_Color_Off);
	}

	G_TaskStateChange(this, &m_stTaskState[0], TASK_STATE_NONE);
	G_TaskStateChange(this, &m_stTaskState[1], TASK_STATE_NONE);
	G_TaskStateChange(this, &m_stTaskState[2], TASK_STATE_NONE);
	G_TaskStateChange(this, &m_stTaskState[3], TASK_STATE_NONE);
	G_TaskStateChange(this, &m_stTaskState[4], TASK_STATE_NONE);
	G_TaskStateChange(this, &m_stTaskState[5], TASK_STATE_NONE);

	for(int TStep =0; TStep<3; TStep++)
	{
		m_EQState[TStep].SetFont(&G_NormalFont);
		m_EQState[TStep].SetTextColor(RGB(0,0,0));
		m_EQState[TStep].SetBkColor(G_Color_Off);
	}

	for(int TStep =0; TStep<4; TStep++)
	{
		m_EQProcessState[TStep].SetFont(&G_NormalFont);
		m_EQProcessState[TStep].SetTextColor(RGB(0,0,0));
		m_EQProcessState[TStep].SetBkColor(G_Color_Off);
	}


	m_pHostMsgViewDlg	=  new CCIMMsgViewDlg (this);//1
	m_pHostMsgViewDlg->Create(IDD_DLG_MSG);
	m_pHostMsgViewDlg->ShowWindow(SW_HIDE);
	m_pHostMsgViewDlg->CenterWindow();


	m_pStickReicpeView	=  new CStickReicpeDlg(this);//1
	m_pStickReicpeView->m_pAlignCellStartX = &m_AlignCellStartX;
	m_pStickReicpeView->m_pAlignCellStartY = &m_AlignCellStartY;
	m_pStickReicpeView->Create(IDD_DLG_STICK_RECIPE);
	m_pStickReicpeView->ShowWindow(SW_HIDE);

	m_pSysParamView	=  new CSysParamDlg (this);//1
	m_pSysParamView->Create(IDD_DLG_SYS_PARM);
	m_pSysParamView->ShowWindow(SW_HIDE);

	m_pMaintenanceView	=  new CMaintDlg (this);//1
	m_pMaintenanceView->m_pReviewPos = &m_ReviewPos;
	m_pMaintenanceView->m_pScopePos = &m_ScopePos;
	m_pMaintenanceView->m_pGripperPos = &m_GripperPos;
	m_pMaintenanceView->m_pLiveViewObj = &m_LiveViewObj;
	m_pMaintenanceView->m_pCamToObjDis = &m_CamToObjDis;

	m_pMaintenanceView->m_pbUpReviewCylinder = &m_bUpReviewCylinder;
	m_pMaintenanceView->m_pbUpRingLightCylinder = &m_bUpRingLightCylinder;
	m_pMaintenanceView->Create(IDD_DLG_MAINTENANCE);
	m_pMaintenanceView->ShowWindow(SW_HIDE);

	//GetDlgItem(IDC_ST_SUB_VIEW_AREA)->GetWindowRect(m_SubDlgArea);
	//ScreenToClient(m_SubDlgArea);
	m_SubDlgArea.left = 0; 
	m_SubDlgArea.top = 0;
	m_SubDlgArea.right = m_SubDlgArea.left+ 680;
	m_SubDlgArea.bottom = m_SubDlgArea.top+220;
	m_Sub3DScopeView	=  new CSubDlg_3DScope (this);//1
	m_Sub3DScopeView->m_pServerInterface = &m_ServerInterface;
	m_Sub3DScopeView->m_pScope3DParam = &m_Scope3DParam;
	m_Sub3DScopeView->Create(IDD_DLG_SUB_3D_SCOPE);
	m_Sub3DScopeView->ShowWindow(SW_HIDE);
	m_Sub3DScopeView->MoveWindow(m_SubDlgArea);
	m_Sub3DScopeView->CenterWindow();
	m_btSubView[0].SetCheck(false);

	m_SubInspectView	=  new CSubDlg_InspectScan (this);//1
	m_SubInspectView->m_pServerInterface = &m_ServerInterface;
	m_SubInspectView->m_pAlignCellStartX = &m_AlignCellStartX;
	m_SubInspectView->m_pAlignCellStartY = &m_AlignCellStartY;
	m_SubInspectView->m_pGlassReicpeParam = &m_GlassReicpeParam;
	m_SubInspectView->m_pstMaskImageInfo_Mic = &m_stMaskImageInfo_Mic;
	m_SubInspectView->m_pstCommonPara_Mic = &m_stCommonPara_Mic;	
	m_SubInspectView->m_pstMaskImageInfo_Mac = &m_stMaskImageInfo_Mac;
	m_SubInspectView->m_pstCommonPara_Mac = &m_stCommonPara_Mac;

	m_SubInspectView->m_pBlockEnd = &m_BlockEnd;
	m_SubInspectView->m_pStartBlockTick = &m_StartBlockTick;
	m_SubInspectView->m_pStartLineTick = &m_StartLineTick;
	m_SubInspectView->m_pCamScnaLineCnt = &m_CamScnaLineCnt;
	m_SubInspectView->m_pCamScnaImgCnt = &m_CamScnaImgCnt;
	m_SubInspectView->m_pStartScanLineNum = &m_StartScanLineNum;

	m_SubInspectView->Create(IDD_DLG_SUB_SCAN);
	m_SubInspectView->ShowWindow(SW_HIDE);
	m_SubInspectView->MoveWindow(m_SubDlgArea);
	m_SubInspectView->CenterWindow();

	m_SubInspectView->m_pAlignCellStartX = &m_AlignCellStartX;
	m_SubInspectView->m_pAlignCellStartY = &m_AlignCellStartY;
	m_btSubView[1].SetCheck(false);

	m_SubAreaScanView	=  new CSubDlg_AreaScan3D(this);//1
	m_SubAreaScanView->m_pServerInterface = &m_ServerInterface;
	m_SubAreaScanView->m_pAlignCellStartX = &m_AlignCellStartX;
	m_SubAreaScanView->m_pAlignCellStartY = &m_AlignCellStartY;
	m_SubAreaScanView->m_pGlassReicpeParam = &m_GlassReicpeParam;
	m_SubAreaScanView->m_pScan3DParam = &m_Scan3DParam;
	m_SubAreaScanView->Create(IDD_DLG_SUB_AREA_SCAN3D);
	m_SubAreaScanView->ShowWindow(SW_HIDE);
	m_SubAreaScanView->MoveWindow(m_SubDlgArea);
	m_SubAreaScanView->CenterWindow();
	m_btSubView[2].SetCheck(false);

	m_SubCDTPView	=  new CSubDlg_CDTP (this);//1
	m_SubCDTPView->m_pServerInterface = &m_ServerInterface;
	m_SubCDTPView->Create(IDD_DLG_SUB_CDTP);
	m_SubCDTPView->ShowWindow(SW_HIDE);
	m_SubCDTPView->MoveWindow(m_SubDlgArea);
	m_SubCDTPView->CenterWindow();
	m_btSubView[3].SetCheck(false);

	CRect ThAlignView;

	ThAlignView.left = 0; 
	ThAlignView.top = 0;
	ThAlignView.right = 865;
	ThAlignView.bottom  =450;
	m_SubThetaAlignView	=  new CSubDlg_ThetaAlign (this);//1
	
	//m_SubThetaAlignView->m_pThetaAlignLiveObj = &m_ThetaAlignLiveObj;
	m_SubThetaAlignView->m_pServerInterface = &m_ServerInterface;
	m_SubThetaAlignView->m_pScopeLensOffset = &m_ScopeLensOffset;
	m_SubThetaAlignView->m_pScopePos = &m_ScopePos;
	m_SubThetaAlignView->Create(IDD_DLG_SUB_THETA_ALIGN);
	m_SubThetaAlignView->ShowWindow(SW_HIDE);
	m_SubThetaAlignView->MoveWindow(ThAlignView);
	m_SubThetaAlignView->CenterWindow();
	m_btSubView[4].SetCheck(false);

	m_SubStickExchangeView	=  new CSubDlg_StickExchange (this);//1
	m_SubStickExchangeView->m_pServerInterface = &m_ServerInterface;
	m_SubStickExchangeView->m_pStickLoadInfo = &m_StickLoadInfo;
	m_SubStickExchangeView->m_pGlassReicpeParam = &m_GlassReicpeParam;
	m_SubStickExchangeView->m_pScopePos = &m_ScopePos;
	m_SubStickExchangeView->m_pGripperPos = &m_GripperPos;
	m_SubStickExchangeView->m_pStickTentionValue = &m_StickTentionValue;
	m_SubStickExchangeView->Create(IDD_DLG_SUB_STICK_EXCHANGE);
	m_SubStickExchangeView->ShowWindow(SW_HIDE);
	m_SubStickExchangeView->MoveWindow(m_SubDlgArea);
	m_SubStickExchangeView->CenterWindow();
	m_btSubView[5].SetCheck(true);


	m_SubCIMInterfaceView	=  new SubDlg_CIMInterface (this);//1
	m_SubCIMInterfaceView->Create(IDD_DLG_SUB_CIM);
	m_SubCIMInterfaceView->ShowWindow(SW_HIDE);
	//m_SubCIMInterfaceView->MoveWindow(m_SubDlgArea);
	m_SubCIMInterfaceView->CenterWindow();
	m_CIMInterface.m_CIMInfodlg = m_SubCIMInterfaceView;
	G_pCIMSendObj = m_SubCIMInterfaceView;

	G_CIM_I_value = m_SubCIMInterfaceView->GetSafeHwnd();


	m_SubAlarmClearView	=  new CAlarmClearDlg (this);//1
	m_SubAlarmClearView->m_pSubCIMInterfaceView = m_SubCIMInterfaceView;
	m_SubAlarmClearView->Create(IDD_DLG_ALARM);
	m_SubAlarmClearView->ShowWindow(SW_HIDE);
	m_SubAlarmClearView->CenterWindow();
	G_pAlarmSendObj = m_SubAlarmClearView;

	m_SubPMView	=  new CPMDlg (this);//1
	m_SubPMView->m_pSubCIMInterfaceView = m_SubCIMInterfaceView;
	m_SubPMView->Create(IDD_DLG_PM);
	m_SubPMView->ShowWindow(SW_HIDE);
	m_SubPMView->CenterWindow();
	
	//////////////////////////////////////////////////////////////////////////
	//m_gridStickInfo
	int CellHeight = 26;
	//////////////////////////////////////////////////////////////////////////
	m_gridStickInfo.SetDefCellWidth(50);
	m_gridStickInfo.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();
	m_gridStickInfo.SetRowCount(3+1);//9+1);
	m_gridStickInfo.SetColumnCount(2);
	m_gridStickInfo.SetFixedRowCount(1);		
	m_gridStickInfo.SetFixedColumnCount(1);
	m_gridStickInfo.SetListMode(TRUE);
	m_gridStickInfo.SetEditable(FALSE);
	m_gridStickInfo.SetColumnResize(FALSE);
	m_gridStickInfo.SetRowResize(FALSE);
	m_gridStickInfo.SetFixedTextColor(RGB(255,200,100));
	m_gridStickInfo.SetFixedBkColor(RGB(0,0,0));

	m_gridStickInfo.SetFont(&G_NormalFont);
	m_gridStickInfo.SetItemText(0, 0, "스틱정보");		m_gridStickInfo.SetColumnWidth(0, 87);m_gridStickInfo.SetItemFormat(0, 0, DT_CENTER);
	m_gridStickInfo.SetItemText(0, 1, "Value");			m_gridStickInfo.SetColumnWidth(1, 300);m_gridStickInfo.SetItemFormat(0, 1, DT_CENTER);


	int InputIndex = 1;
	m_gridStickInfo.SetItemText(InputIndex, 0, "BoxID");					m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	m_gridStickInfo.SetItemText(InputIndex, 0, "StickID");				m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	m_gridStickInfo.SetItemText(InputIndex, 0, "Recipe");				m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	//m_gridStickInfo.SetItemText(InputIndex, 0, "Width");				    m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	//m_gridStickInfo.SetItemText(InputIndex, 0, "Height");					m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	//m_gridStickInfo.SetItemText(InputIndex, 0, "H Count");				m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	//m_gridStickInfo.SetItemText(InputIndex, 0, "V Count");			    m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	//m_gridStickInfo.SetItemText(InputIndex, 0, "Next_H Dis");			m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	//m_gridStickInfo.SetItemText(InputIndex, 0, "Next_V Dis");			m_gridStickInfo.SetItemFormat(InputIndex++, 0, DT_CENTER);
	////////////////////////////////////////////////////////////////////////// 
	//Result Tree Setting.
	CBitmap m_bitmap; 
	// Create a 32bits ImageList
	m_ImageList.Create (16, 16, ILC_COLOR32 , 1,1);

	//Add the bitmap to the ImageList
	m_bitmap.LoadBitmap(IDB_ITEM_IMAGE);
	m_ImageList.Add(&m_bitmap, RGB(255,255,255));

	m_treeResult.SetTextColor(RGB(255,255,255));
	m_treeResult.SetBkColor(RGB(0,0,0));

	//m_ImageList.Create(MAKEINTRESOURCE(IDB_ITEM_IMAGE), 16, 1, RGB(255,255,255));
	m_treeResult.SetImageList(&m_ImageList,TVSIL_NORMAL);
	m_treeResult.DeleteAllItems();
	m_ResultRoot_CDTP				= m_treeResult.InsertItem("CD,TP",				0, 6, NULL);
	m_ResultRoot_3DScope			= m_treeResult.InsertItem("3D_Scope",		0, 6, NULL);
	m_ResultRoot_AreaScan3D		= m_treeResult.InsertItem("Area Scan 3D",	0, 6, NULL);
	m_ResultRoot_InsMicScan	= m_treeResult.InsertItem("Inspect Micro",			0, 6, NULL);
	m_ResultRoot_InsMacScan	= m_treeResult.InsertItem("Inspect Macro",			0, 6, NULL);

	for(int pStep = 0; pStep<6; pStep++)
	{
		m_progSeqViewBar[pStep].SetTextColor(RGB(255,255,255));
		m_progSeqViewBar[pStep].SetTextBkColor(RGB(0,0,0));
		m_progSeqViewBar[pStep].SetBarColor(RGB(255,200,100));
		m_progSeqViewBar[pStep].SetBarBkColor(RGB(0,0,0));
		m_progSeqViewBar[pStep].AlignText(DT_CENTER);
		m_progSeqViewBar[pStep].SetShowPercent(true);

		m_progSeqViewBar[pStep].SetFont(&G_NormalFont);
		m_progSeqViewBar[pStep].SetWindowText("Progress-");
		// start marquee style progress bars
		//m_ProgressNormalMarquee.SetMarquee(true, 30);
		// stop marquee style progress bars
		//m_ProgressNormalMarquee.SetMarquee(false, 0);
		//m_nMarqueeSliderValue = (int)pScrollBar->SendMessage(TBM_GETPOS, 0, 0);
		//m_ProgressNormalMarquee.SetMarqueeOptions(m_nMarqueeSliderValue);
		//m_progSeqViewBar.Invalidate();
	}
	m_progSeqViewBar[0].SetRange(CSeq_StickExchange::SEQ_STEP_RUN, CSeq_StickExchange::SEQ_STEP_CNT);
	m_progSeqViewBar[0].SetPos(1);

	m_progSeqViewBar[1].SetRange(CSeq_ThetaAlign::SEQ_STEP_RUN, CSeq_ThetaAlign::SEQ_STEP_CNT);
	m_progSeqViewBar[1].SetPos(1);

	m_progSeqViewBar[2].SetRange(CSeq_CDTP::SEQ_STEP_RUN, CSeq_CDTP::SEQ_STEP_CNT);
	m_progSeqViewBar[2].SetPos(1);

	m_progSeqViewBar[3].SetRange(CSeq_AreaScan3D::SEQ_STEP_RUN, CSeq_AreaScan3D::SEQ_STEP_CNT);
	m_progSeqViewBar[3].SetPos(1);

	m_progSeqViewBar[4].SetRange(CSeq_3DScope::SEQ_STEP_RUN, CSeq_3DScope::SEQ_STEP_CNT);
	m_progSeqViewBar[4].SetPos(1);

	m_progSeqViewBar[5].SetRange(CSeq_Inspect_Mic::SEQ_STEP_RUN, CSeq_Inspect_Mic::SEQ_STEP_CNT);
	m_progSeqViewBar[5].SetPos(1);

	m_progSeqViewBar[9].SetTextColor(RGB(255,255,255));
	m_progSeqViewBar[9].SetTextBkColor(RGB(0,0,0));
	m_progSeqViewBar[9].SetBarColor(RGB(255,200,100));
	m_progSeqViewBar[9].SetBarBkColor(RGB(0,0,0));
	m_progSeqViewBar[9].AlignText(DT_CENTER);
	m_progSeqViewBar[9].SetShowPercent(true);

	m_progSeqViewBar[9].SetFont(&G_NormalFont);
	m_progSeqViewBar[9].SetWindowText("Progress-");
	m_progSeqViewBar[9].SetRange(CSeq_Inspect_Mac::SEQ_STEP_RUN, CSeq_Inspect_Mac::SEQ_STEP_CNT);
	m_progSeqViewBar[9].SetPos(1);


	m_progSeqViewBar[6].SetTextColor(RGB(255,255,255));
	m_progSeqViewBar[6].SetTextBkColor(RGB(0,0,0));
	m_progSeqViewBar[6].SetBarColor(RGB(255,80,80));
	m_progSeqViewBar[6].SetBarBkColor(RGB(100,100,200));
	m_progSeqViewBar[6].AlignText(DT_CENTER);
	m_progSeqViewBar[6].SetShowPercent(true);
	int AutoRunRange = (CSeq_AutoRun::SEQ_STEP_CNT);//+CSeq_StickExchange::SEQ_STEP_CNT+CSeq_ThetaAlign::SEQ_STEP_CNT+CSeq_3DScope::SEQ_STEP_CNT+CSeq_AreaScan3D::SEQ_STEP_CNT+CSeq_Inspect_Mic::SEQ_STEP_CNT);
	m_progSeqViewBar[6].SetRange(CSeq_AutoRun::SEQ_STEP_RUN, AutoRunRange);
	m_progSeqViewBar[6].SetPos(1);
	m_progSeqViewBar[6].SetFont(&G_NormalFont);
	m_progSeqViewBar[6].SetWindowText("Progress-");


	m_progSeqViewBar[7].SetTextColor(RGB(255,255,255));
	m_progSeqViewBar[7].SetTextBkColor(RGB(0,0,0));
	m_progSeqViewBar[7].SetBarColor(RGB(255,200,100));
	m_progSeqViewBar[7].SetBarBkColor(RGB(0,0,0));
	m_progSeqViewBar[7].AlignText(DT_CENTER);
	m_progSeqViewBar[7].SetShowPercent(true);

	m_progSeqViewBar[7].SetFont(&G_NormalFont);
	m_progSeqViewBar[7].SetWindowText("Progress-");
	m_progSeqViewBar[7].SetRange(CSeq_CellAlign::SEQ_STEP_RUN, CSeq_CellAlign::SEQ_STEP_CNT);
	m_progSeqViewBar[7].SetPos(1);


	m_progSeqViewBar[8].SetTextColor(RGB(255,255,255));
	m_progSeqViewBar[8].SetTextBkColor(RGB(0,0,0));
	m_progSeqViewBar[8].SetBarColor(RGB(255,200,100));
	m_progSeqViewBar[8].SetBarBkColor(RGB(0,0,0));
	m_progSeqViewBar[8].AlignText(DT_CENTER);
	m_progSeqViewBar[8].SetShowPercent(true);

	m_progSeqViewBar[8].SetFont(&G_NormalFont);
	m_progSeqViewBar[8].SetWindowText("Progress-");
	m_progSeqViewBar[8].SetRange(CSeq_InitAM01::SEQ_STEP_RUN, CSeq_InitAM01::SEQ_STEP_CNT);
	m_progSeqViewBar[8].SetPos(1);


	

}
void CVS10MasterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		if(0x0000f012 == nID)
		{
			SIZE Screen;
			memset(&Screen, 0x00, sizeof(SIZE));
			Screen.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN);
			Screen.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN);
			this->SetWindowPos(nullptr, 0, 0, Screen.cx-205, 1080, SWP_SHOWWINDOW);
		}

		if(SC_CLOSE == nID)
		{
			if(AfxMessageBox("System Exit ?",MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDNO)
			{
				return;
			}
		}

		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVS10MasterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		DrawImageView();
		CDialogEx::OnPaint();
	}
}
void CVS10MasterDlg::DrawImageView()
{
	if(m_pSampleView != nullptr)
	{
		CvvImage	ViewImage;
		ViewImage.Create(m_pSampleView->width, m_pSampleView->height, ((IPL_DEPTH_8U & 255)*3) );
		ViewImage.CopyOf(m_pSampleView);
		ViewImage.DrawToHDC(m_HDCView, m_RectiewArea);
		ViewImage.Destroy();
	}
}
// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVS10MasterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CVS10MasterDlg::m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...)
{
	char	cLogBuffer[MAX_LOG_BUFF];
	va_list vargs;

	va_start(vargs, pErrLog);

	vsprintf_s(cLogBuffer, pErrLog, (va_list)vargs);

	va_end(vargs);

	m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nTaskNum ,uLogLevel, cLogBuffer);

	//strcpy_s(&cLogBuffer[strlen(cLogBuffer)], sizeof("\r\n"),"\r\n");

	//printf(cLogBuffer);
}

void CVS10MasterDlg::LoadHWParam()
{
	G_WriteInfo(Log_Normal, "HW Parameter Load");
	char strReadData[256];

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetShift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nReviewToScopeDisOffsetShift = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetDrive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nReviewToScopeDisOffsetDrive = atoi(strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nReviewCamTo_LineScanX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nReviewCamTo_LineScanY = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewCamToObjDis, Key_SpaceSensorDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nReviewCamTo_SpaceSensorDisX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewCamToObjDis, Key_SpaceSensorDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nReviewCamTo_SpaceSensorDisY = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_AirBlowDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nScopeCamTo_AirBlowDisX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_AirBlowDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nScopeCamTo_AirBlowDisY = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nScopeCamTo_BackLightDisX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nScopeCamTo_BackLightDisY = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ScanSensorDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nScopeCamTo_ScanSensorDisX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ScanSensorDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nScopeCamTo_ScanSensorDisY = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ThetaAlignCamDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nScopeCamTo_ThetaAlignCamDisX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ThetaAlignCamDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_CamToObjDis.nScopeCamTo_ThetaAlignCamDisY = atoi(strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens02xPixelSizeX = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens02xPixelSizeY = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens02xOffsetX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens02xOffsetY = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_Light, "10", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens02xLight = (USHORT)atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens10xPixelSizeX = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens10xPixelSizeY = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens10xOffsetX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens10xOffsetY = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_Light, "10", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens10xLight = (USHORT)atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens20xPixelSizeX = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens20xPixelSizeY = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens20xOffsetX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens20xOffsetY = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_Light, "10", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewLensOffset.Lens20xLight = (USHORT)atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens10xPixelSizeX = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens10xPixelSizeY = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens10xOffsetX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens10xOffsetY = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens20xPixelSizeX = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens20xPixelSizeY = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens20xOffsetX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens20xOffsetY = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens50xPixelSizeX = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens50xPixelSizeY = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens50xOffsetX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens50xOffsetY = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_PixelSizeX, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens150xPixelSizeX = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_PixelSizeY, "0.0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens150xPixelSizeY = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens150xOffsetX = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopeLensOffset.Lens150xOffsetY = atoi(strReadData);

	m_p3DMapViewer->SetDeviceDistance(&m_CamToObjDis);


	//////////////////////////////////////////////////////////////////////////
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_TT01Out, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nGripper_ZPos_TT01Out = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Grip_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nGripper_ZPos_Grip_Standby = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Grip, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nGripper_ZPos_Grip = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Measure, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nGripper_ZPos_Measure = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_ZPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nGripper_ZPos_Standby = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_TPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nGripper_TPos_Standby = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_TPos_Grip, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nGripper_TPos_Grip = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_TPos_TensionStandby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nGripper_TPos_TensionStandby = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_Jig_StandbyPos, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nStickJig_Standby = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_GripperPos, Key_Gripper_Jig_MeasurePos, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GripperPos.nStickJig_Measure = atoi(strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Measure, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nScope_ZPos_Measure = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nScope_ZPos_Standby = atoi(strReadData);

	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopePos, Key_Scope_ZPos_ThetaAlign, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//m_ScopePos.nScope_ZPos_ThetaAlign = atoi(strReadData);

	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopePos, Key_Scope_ZPos_Area3DMeasure, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//m_ScopePos.nScope_ZPos_Area3DMeasure = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Space_ZPos_Measure, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nSpace_ZPos_Measure = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Space_ZPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nSpace_ZPos_Standby = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_Standby_Shift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nScope_Standby_Shift = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_Standby_Drive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nScope_Standby_Drive = atoi(strReadData);

	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignLensIndex, "10x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//if(strReadData[0] == '1' && strReadData[1] =='0')
	//	m_ScopePos.nThetaAlignLensIndex = SCOPE_LENS_10X;
	//else if(strReadData[0] =='2' && strReadData[1] =='0')
	//	m_ScopePos.nThetaAlignLensIndex = SCOPE_LENS_20X;
	////else if(strReadData[0] =='5' && strReadData[1] =='0')
	////	m_ScopePos.nThetaAlignLensIndex = SCOPE_LENS_50X;
	////else if(strReadData[0] =='1' && strReadData[1] =='5')
	////	m_ScopePos.nThetaAlignLensIndex = SCOPE_LENS_150X;
	//else
	//	m_ScopePos.nThetaAlignLensIndex = SCOPE_LENS_10X;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignDrivePos1, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nThetaAlignPos1_Drive = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignShiftPos1, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nThetaAlignPos1_Shift = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignDrivePos2, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nThetaAlignPos2_Drive = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignShiftPos2, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nThetaAlignPos2_Shift = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignLightVale, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nThetaAlignLightVale = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignPos2Offset, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ScopePos.nThetaAlignPos2Offset = atoi(strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScopePos, Key_Scope_ThetaAlignAFDistance, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//m_ScopePos.nThetaAlignPosAFDis_Shift = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Review, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nScan_ZPos_Review = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Standby, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nScan_ZPos_Standby = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_ZPos_Inspection, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nScan_ZPos_Inspection = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_Standby_Shift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nScan_Standby_Shift = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Scan_Standby_Drive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nScan_Standby_Drive = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_CellAlign_LensIndex, "10x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	if(strReadData[0] == '0' && strReadData[1] =='2')
		m_ReviewPos.nCellAlign_LensIndex  = REVIEW_LENS_2X;
	else
	if(strReadData[0] =='1' && strReadData[1] =='0')
		m_ReviewPos.nCellAlign_LensIndex  = REVIEW_LENS_10X;
	else if(strReadData[0] =='2' && strReadData[1] =='0')
		m_ReviewPos.nCellAlign_LensIndex  = REVIEW_LENS_20X;
	else
		m_ReviewPos.nCellAlign_LensIndex = REVIEW_LENS_20X;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_CellAlign_BackLight, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCellAlign_Backlight = atoi(strReadData);



	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_ScanPos, Key_Review_CDTP_LensIndex, "20x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	////if(strReadData[0] == '0' && strReadData[1] =='2')
	////	m_ReviewPos.nAutoReviewMic_LensIdex  = REVIEW_LENS_2X;
	////else
	//if(strReadData[0] =='1' && strReadData[1] =='0')
	//	m_ReviewPos.nCDTP_LensIndex  = REVIEW_LENS_10X;
	//else if(strReadData[0] =='2' && strReadData[1] =='0')
	//	m_ReviewPos.nCDTP_LensIndex  = REVIEW_LENS_20X;
	//else
	//	m_ReviewPos.nCDTP_LensIndex = REVIEW_LENS_20X;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReview_LensIndex, "20x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	if(strReadData[0] == '0' && strReadData[1] =='2')
		m_ReviewPos.nAutoReviewMic_LensIdex  = REVIEW_LENS_2X;
	else if(strReadData[0] =='1' && strReadData[1] =='0')
		m_ReviewPos.nAutoReviewMic_LensIdex  = REVIEW_LENS_10X;
	else if(strReadData[0] =='2' && strReadData[1] =='0')
		m_ReviewPos.nAutoReviewMic_LensIdex  = REVIEW_LENS_20X;
	else
		m_ReviewPos.nAutoReviewMic_LensIdex = REVIEW_LENS_20X;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReview_ReflectLight, "100", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nAutoReviewMic_Light = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReview_MaxCnt, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nAutoReviewMic_Cnt = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReview_BackLight, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nAutoReviewMic_Backlight = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_LensIndex, "20x", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	if(strReadData[0] == '0' && strReadData[1] =='2')
		m_ReviewPos.nAutoReviewMac_LensIdex  = REVIEW_LENS_2X;
	else if(strReadData[0] =='1' && strReadData[1] =='0')
		m_ReviewPos.nAutoReviewMac_LensIdex  = REVIEW_LENS_10X;
	else if(strReadData[0] =='2' && strReadData[1] =='0')
		m_ReviewPos.nAutoReviewMac_LensIdex  = REVIEW_LENS_20X;
	else
		m_ReviewPos.nAutoReviewMac_LensIdex = REVIEW_LENS_20X;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_ReflectLight, "100", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nAutoReviewMac_Light = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_MaxCnt, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nAutoReviewMac_Cnt = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_AutoReviewMac_BackLight, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nAutoReviewMac_Backlight = atoi(strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens_ZUpDnPos, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens_ZUpDnPos = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_ReflectLight, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens2X_ReflectLight = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_Shift, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens2X_Shift = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens2X_Drive, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens2X_Drive = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_ReflectLight, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens10X_ReflectLight = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_Shift, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens10X_Shift = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens10X_Drive, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens10X_Drive = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_ReflectLight, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens20X_ReflectLight = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_Shift, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens20X_Shift = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_ScanPos, Key_Review_CalLens20X_Drive, "20", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_ReviewPos.nCalLens20X_Drive = atoi(strReadData);

}
void CVS10MasterDlg::LoadRecipeParam(char *pFilePath, bool bAlignPosSet)
{
	G_WriteInfo(Log_Normal, "Recipe Parameter Load");
	//////////////////////////////////////////////////////////////////////////
	char strReadData[256];
	if(bAlignPosSet == false)
	{//True이면 Cell Align값이 m_AlignCellStartX, m_AlignCellStartY에 설정 되어 있다.
		
		
		int NowCellAlignShift= GetPrivateProfileInt(Section_Process_Data, Key_NowCellAlignPosShift, 0,  VS10Master_PARAM_INI_PATH);
		int NowCellAlignDrive= GetPrivateProfileInt(Section_Process_Data, Key_NowCellAlignPosDrive, 0,  VS10Master_PARAM_INI_PATH);

		if(NowCellAlignShift>0 && NowCellAlignDrive>0)
		{//로딩 이후 찾아놓은(Cell Align) 정보가 있다면.
			m_AlignCellStartX = NowCellAlignShift;
			m_AlignCellStartY = NowCellAlignDrive;
		}
		else
		{
			//Recipe이 저장되어 있는 정보 가져 오기.
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_CELL_INFO, Key_Scan_CellAlign_Pos_Shift, "0", &strReadData[0], 256, pFilePath);
			m_AlignCellStartX = atoi(strReadData);
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_CELL_INFO, Key_Scan_CellAlign_Pos_Drive, "0", &strReadData[0], 256, pFilePath);
			m_AlignCellStartY = atoi(strReadData);
		}
	}
	

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Scan_Cell_Tension_Value, "0.0", &strReadData[0], 256, pFilePath);
	m_StickTentionValue = (float)atof(strReadData);
	
	//System 정보 가져오기.
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanLineShiftDistance, "60000", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GlassReicpeParam.m_ScanLineShiftDistance = atoi(strReadData);//<<< Scna Line Shift 거리.(um단위)
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_W, "5.25", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GlassReicpeParam.m_ScanImgResolution_W = (float)atof(strReadData);//<<검사 Pixel Size.(um단위)검사 Resolution이 된다.
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_InspectionInfo, Key_ScanImgResolution_H, "5.25", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	m_GlassReicpeParam.m_ScanImgResolution_H = (float)atof(strReadData);//<<검사 Pixel Size.(um단위)


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Stick_Width, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.STICK_WIDTH = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Stick_Edge_Distance_W, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.STICK_EDGE_DIS_WIDTH = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Stick_Edge_Distance_H, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.STICK_EDGE_DIS_HEIGHT = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Width, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.CELL_SIZE_X = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Height, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.CELL_SIZE_Y = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Horizontal_Count, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.GLASS_CELL_COUNT_X = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Vertical_Count, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.GLASS_CELL_COUNT_Y = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Next_Horizontal_Distance, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.CELL_DISTANCE_X = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Next_Vertical_Distance, "0", &strReadData[0], 256, pFilePath);
	m_GlassReicpeParam.CELL_DISTANCE_Y = atoi(strReadData);

	//나머지는 기본값으로 가자~~~;
	m_GlassReicpeParam.PadStartDistanceX			= 0;						//<<CELL_EDGE_X에서 거리.(Pad부는 Active 보다 항상 크다.)
	m_GlassReicpeParam.PadStartDistanceY			= 0;						//<<CELL_EDGE_Y에서 거리.(Pad부는 Active 보다 항상 크다.)
	m_GlassReicpeParam.PadEndDistanceX				= 0;						//
	m_GlassReicpeParam.PadEndDistanceY				= 0;						//
	
	//m_GlassReicpeParam.ActiveDeadZoneSize			= 100;
	//m_GlassReicpeParam.ActivePadDeadZone			= 0;//m_GlassReicpeParam.ActiveDeadZoneSize	+100;

	//Block 갯수를 나타낸다.
	m_GlassReicpeParam.BLOCK_DISTANCE_X			= 0;					//<<um단위 이다.
	m_GlassReicpeParam.BLOCK_DISTANCE_Y			= 0;					//<<um단위 이다.

	//축 기준으로 몇개의 Block이 있는지 나타 낸다.
	m_GlassReicpeParam.GLASS_BLOCK_COUNT_X	= 1;				//항상 1이상이다.
	m_GlassReicpeParam.GLASS_BLOCK_COUNT_Y	= 1;				//항상 1이상이다.

	m_GlassReicpeParam.ActiveDeadZoneSize			= 100;
	m_GlassReicpeParam.ActivePadDeadZone			= 0;//m_GlassReicpeParam.ActiveDeadZoneSize	+100;

	//////////////////////////////////////////////////////////////////////////
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCAN_3D, Key_Trigger_Step, "0.0", &strReadData[0], 256, pFilePath);
	m_Scan3DParam.fTrigger_Step = atof(strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_SCAN_3D, Key_Scan_Count, "0", &strReadData[0], 256, pFilePath);
	//m_Scan3DParam.nScan_Count = atoi(strReadData);
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_SCAN_3D, Key_Scan_Distance, "0", &strReadData[0], 256, pFilePath);
	//m_Scan3DParam.nScan_Distance = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCAN_3D, Key_Pass_Min_Height, "0", &strReadData[0], 256, pFilePath);
	m_Scan3DParam.nPass_Min_Height = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCAN_3D, Key_Pass_Max_Height, "0", &strReadData[0], 256, pFilePath);
	m_Scan3DParam.nPass_Max_Height = atoi(strReadData);

	//////////////////////////////////////////////////////////////////////////
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Scale_Width, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nScaleX1 = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Scale_Height, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nScaleY1 = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Detect_Min, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nMinRangeMain = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Detect_Max, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nMaxRangeMain = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Skip_Out_Line, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nOutLineLeft			= atoi(strReadData);
	m_stMaskImageInfo_Mic.nOutLineRight			= m_stMaskImageInfo_Mic.nOutLineLeft;
	m_stMaskImageInfo_Mic.nOutLineUp			= m_stMaskImageInfo_Mic.nOutLineLeft;
	m_stMaskImageInfo_Mic.nOutLineDown		= m_stMaskImageInfo_Mic.nOutLineLeft;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Minimum_Width, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nMinimumSizeX	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Minimum_Height, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nMinimumSizeY	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Maximum_Width, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nMaximumSizeX	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Maximum_Height, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mic.nMaximumSizeY	= atoi(strReadData);

	//Inspector H/W Parameter에서 설정 한다.
	m_stCommonPara_Mic.nImageSizeX			= 0;
	m_stCommonPara_Mic.nImageSizeY			= 0;

	//m_stMaskImageInfo_Mic.nScaleX1;                                                               ///<마스크 패턴의 단위(Active)
	//m_stMaskImageInfo_Mic.nScaleY1;                                                               ///<마스크 패턴의 단위(Active)
	m_stMaskImageInfo_Mic.nScaleX2= 0;                                                             ///<마스크 패턴의 단위(Left Pad)
	m_stMaskImageInfo_Mic.nScaleY2= 0;                                                               ///<마스크 패턴의 단위(Left Pad)
	m_stMaskImageInfo_Mic.nScaleX3= 0;                                                              ///<마스크 패턴의 단위(Right Pad)
	m_stMaskImageInfo_Mic.nScaleY3= 0;                                                               ///<마스크 패턴의 단위(Right Pad)
	//m_stMaskImageInfo_Mic.nMinRangeMain;                                           ///<검출 허용치 Default : 60(Active)
	//m_stMaskImageInfo_Mic.nMaxRangeMain;                                ///<검출 허용치 Default : 60(Active)
	m_stMaskImageInfo_Mic.nMinRangeLPad= 0;                                 ///<검출 허용치 Default : 60(Left Pad)
	m_stMaskImageInfo_Mic.nMaxRangeLPad= 0;                                 ///<검출 허용치 Default : 60(Left Pad)
	m_stMaskImageInfo_Mic.nMinRangeRPad= 0;                               ///<검출 허용치 Default : 60(Right Pad)
	m_stMaskImageInfo_Mic.nMaxRangeRPad= 0;                                ///<검출 허용치 Default : 60(Right Pad)
	//m_stMaskImageInfo_Mic.nOutLineLeft;                                                ///<제거할 테두리 두께. Default : 1
	//m_stMaskImageInfo_Mic.nOutLineRight;                                              ///<제거할 테두리 두께. Default : 1
	//m_stMaskImageInfo_Mic.nOutLineUp;                                                 ///<제거할 테두리 두께. Default : 1
	//m_stMaskImageInfo_Mic.nOutLineDown;                                  ///<제거할 테두리 두께. Default : 1
	m_stMaskImageInfo_Mic.nBlockCount = 64;                                                ///<_ACC Block, Default : 128
	m_stMaskImageInfo_Mic.nThreadCount = 192;                                              ///<_ACC Thread, Default : 64
	//m_stMaskImageInfo_Mic.nMinimumSizeX;                                 ///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
	//m_stMaskImageInfo_Mic.nMinimumSizeY;                                  ///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.
	//m_stMaskImageInfo_Mic.nMaximumSizeX;                                 ///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
	//m_stMaskImageInfo_Mic.nMaximumSizeY;                                 ///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Defect_Distance, "0", &strReadData[0], 256, pFilePath);
	m_stCommonPara_Mic.nDefectDistance	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Filter_Mask_Type, "100", &strReadData[0], 256, pFilePath);
	m_stCommonPara_Mic.nMaskType	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Light_Back, "100", &strReadData[0], 256, pFilePath);
	m_ReviewPos.nInspection_MicBackLight = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Light_Ring, "100", &strReadData[0], 256, pFilePath);
	m_ReviewPos.nInspection_MicRingLight = atoi(strReadData);
	//////////////////////////////////////////////////////////////////////////
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Scale_Width, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nScaleX1 = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Scale_Height, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nScaleY1 = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Detect_Min, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nMinRangeMain = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Detect_Max, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nMaxRangeMain = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Skip_Out_Line, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nOutLineLeft			= atoi(strReadData);
	m_stMaskImageInfo_Mac.nOutLineRight		= m_stMaskImageInfo_Mac.nOutLineLeft;
	m_stMaskImageInfo_Mac.nOutLineUp			= m_stMaskImageInfo_Mac.nOutLineLeft;
	m_stMaskImageInfo_Mac.nOutLineDown		= m_stMaskImageInfo_Mac.nOutLineLeft;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Minimum_Width, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nMinimumSizeX	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Minimum_Height, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nMinimumSizeY	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Maximum_Width, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nMaximumSizeX	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Maximum_Height, "0", &strReadData[0], 256, pFilePath);
	m_stMaskImageInfo_Mac.nMaximumSizeY	= atoi(strReadData);

	//Inspector H/W Parameter에서 설정 한다.
	m_stCommonPara_Mac.nImageSizeX			= 0;
	m_stCommonPara_Mac.nImageSizeY			= 0;

	//m_stMaskImageInfo_Mac.nScaleX1;                                                               ///<마스크 패턴의 단위(Active)
	//m_stMaskImageInfo_Mac.nScaleY1;                                                               ///<마스크 패턴의 단위(Active)
	m_stMaskImageInfo_Mac.nScaleX2= 0;                                                             ///<마스크 패턴의 단위(Left Pad)
	m_stMaskImageInfo_Mac.nScaleY2= 0;                                                               ///<마스크 패턴의 단위(Left Pad)
	m_stMaskImageInfo_Mac.nScaleX3= 0;                                                              ///<마스크 패턴의 단위(Right Pad)
	m_stMaskImageInfo_Mac.nScaleY3= 0;                                                               ///<마스크 패턴의 단위(Right Pad)
	//m_stMaskImageInfo_Mac.nMinRangeMain;                                           ///<검출 허용치 Default : 60(Active)
	//m_stMaskImageInfo_Mac.nMaxRangeMain;                                ///<검출 허용치 Default : 60(Active)
	m_stMaskImageInfo_Mac.nMinRangeLPad= 0;                                 ///<검출 허용치 Default : 60(Left Pad)
	m_stMaskImageInfo_Mac.nMaxRangeLPad= 0;                                 ///<검출 허용치 Default : 60(Left Pad)
	m_stMaskImageInfo_Mac.nMinRangeRPad= 0;                               ///<검출 허용치 Default : 60(Right Pad)
	m_stMaskImageInfo_Mac.nMaxRangeRPad= 0;                                ///<검출 허용치 Default : 60(Right Pad)
	//m_stMaskImageInfo_Mac.nOutLineLeft;                                                ///<제거할 테두리 두께. Default : 1
	//m_stMaskImageInfo_Mac.nOutLineRight;                                              ///<제거할 테두리 두께. Default : 1
	//m_stMaskImageInfo_Mac.nOutLineUp;                                                 ///<제거할 테두리 두께. Default : 1
	//m_stMaskImageInfo_Mac.nOutLineDown;                                  ///<제거할 테두리 두께. Default : 1
	m_stMaskImageInfo_Mac.nBlockCount = 64;                                                ///<_ACC Block, Default : 128
	m_stMaskImageInfo_Mac.nThreadCount = 192;                                              ///<_ACC Thread, Default : 64
	//m_stMaskImageInfo_Mac.nMinimumSizeX;                                 ///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
	//m_stMaskImageInfo_Mac.nMinimumSizeY;                                  ///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.
	//m_stMaskImageInfo_Mac.nMaximumSizeX;                                 ///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
	//m_stMaskImageInfo_Mac.nMaximumSizeY;                                 ///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Defect_Distance, "0", &strReadData[0], 256, pFilePath);
	m_stCommonPara_Mac.nDefectDistance	= atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Filter_Mask_Type, "100", &strReadData[0], 256, pFilePath);
	m_stCommonPara_Mac.nMaskType	= atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Light_Upper, "100", &strReadData[0], 256, pFilePath);
	m_ReviewPos.nInspection_MacReflectLight =  atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Light_Ring, "100", &strReadData[0], 256, pFilePath);
	m_ReviewPos.nInspection_MacRingLight =  atoi(strReadData);
	//////////////////////////////////////////////////////////////////////////
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Stick_Type, "Dot", &strReadData[0], 256, pFilePath);
	m_Scope3DParam.nStick_Type = strReadData[0] == 'S'?1:2;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Stick_Thick, "0.0", &strReadData[0], 256, pFilePath);
	m_Scope3DParam.fStick_Thick = atof(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Angle, "R_Angle", &strReadData[0], 256, pFilePath);
	m_Scope3DParam.nMeasure_Angle = strReadData[0] == 'R'?0:1;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Area, "Point", &strReadData[0], 256, pFilePath);
	m_Scope3DParam.nMeasure_Area = strReadData[0] == 'P'?0:1;

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Space_Distance, "20.0", &strReadData[0], 256, pFilePath);
	m_Scope3DParam.fSpaceTargetValue = atof(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Lens_Index, "20X", &strReadData[0], 256, pFilePath);

	m_Scope3DParam.nMeasureLensIndex = atof(strReadData);
	if(strReadData[0] == '1' && strReadData[1] =='0')
		m_Scope3DParam.nMeasureLensIndex = SCOPE_LENS_10X;
	else if(strReadData[0] =='2' && strReadData[1] =='0')
		m_Scope3DParam.nMeasureLensIndex = SCOPE_LENS_20X;
	else if(strReadData[0] =='5' && strReadData[1] =='0')
		m_Scope3DParam.nMeasureLensIndex = SCOPE_LENS_50X;
	else if(strReadData[0] =='1' && strReadData[1] =='5')
		m_Scope3DParam.nMeasureLensIndex = SCOPE_LENS_150X;
	else
		m_Scope3DParam.nMeasureLensIndex = SCOPE_LENS_10X;


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Point_Type, "TYPE_1", &strReadData[0], 256, pFilePath);
	switch(strReadData[5])
	{
	case '1' :m_Scope3DParam.nMeasurPointType = 1; break;
	case '2' :m_Scope3DParam.nMeasurPointType = 2; break;
	case '3' :m_Scope3DParam.nMeasurPointType = 3; break;
	case '4' :m_Scope3DParam.nMeasurPointType = 4; break;
	case '5' :m_Scope3DParam.nMeasurPointType = 5; break;
	case '6' :m_Scope3DParam.nMeasurPointType = 6; break;
	default:
		m_Scope3DParam.nMeasurPointType = 1; break;
	}
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Cell_Type, "ALL", &strReadData[0], 256, pFilePath);
	//switch(strReadData[0])
	//{
	//case 'A' :m_Scope3DParam.nMeasurCelType = 1; break;
	//case 'O' :m_Scope3DParam.nMeasurCelType = 2; break;
	//case 'E' :m_Scope3DParam.nMeasurCelType = 3; break;
	//default:
	//	m_Scope3DParam.nMeasurCelType = 1; break;
	//}
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Cell_List, "1, 4, 7", &strReadData[0], 256, pFilePath);

	CString strTData;
	strTData.Format("%s", strReadData);
	strTData.Replace(" ", "");
	CStringArray strReadValues;
	CString resToken; 
	int curPos= 0; 
	resToken= strTData.Tokenize(",",curPos); 
	int nAddCntCell = 0;
	while (resToken != "")
	{ 
		strReadValues.Add(resToken);
		resToken= strTData.Tokenize(",",curPos);
		nAddCntCell++;
	}
	m_Scope3DParam.nMeasurCellCnt = nAddCntCell;
	memset(m_Scope3DParam.nMeasurCellList, 0x00, sizeof(int)*50);

	for(int nStep = 0; nStep<nAddCntCell; nStep++)
	{
		m_Scope3DParam.nMeasurCellList[nStep] = atoi(strReadValues.GetAt(nStep));
	}
	
	//////////////////////////////////////////////////////////////////////////
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Measure_Lens, "10x", &strReadData[0], 256, pFilePath);
	switch(strReadData[0])
	{
	case '0'://2x
		m_CDTPParam.nMeasure_Lens = REVIEW_LENS_2X;//2x
		break;
	case '1'://10x
		m_CDTPParam.nMeasure_Lens = REVIEW_LENS_10X;//10x
		break;
	case '2'://20x
		m_CDTPParam.nMeasure_Lens = REVIEW_LENS_20X;//20x
		break;
	default:
		m_CDTPParam.nMeasure_Lens = REVIEW_LENS_10X;//10x
		break;
	}
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Edge_Pos_Count_X, "5", &strReadData[0], 256, pFilePath);
	m_CDTPParam.nEdge_Pos_Count_X = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Edge_Pos_Count_Y, "5", &strReadData[0], 256, pFilePath);
	m_CDTPParam.nEdge_Pos_Count_Y = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Light_Upper, "100", &strReadData[0], 256, pFilePath);
	m_CDTPParam.nLight_Upper = atoi(strReadData);
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Light_Back, "100", &strReadData[0], 256, pFilePath);
	m_CDTPParam.nLight_Back = atoi(strReadData);

	GetPrivateProfileString(Section_CD_TP, Key_CD_Light_Threshold, "60", &strReadData[0], 256, pFilePath);
	m_CDTPParam.nLight_Threshold = atoi(strReadData);

	GetPrivateProfileString(Section_CD_TP, Key_CD_Measure_Direction, "+", &strReadData[0], 256, pFilePath);
	switch(strReadData[0])
	{
	case '+'://
		m_CDTPParam.nMeasure_Direction = 1;//수직
		break;
	case 'X'://
		m_CDTPParam.nMeasure_Direction = 2;//대각
		break;
	default:
		m_CDTPParam.nMeasure_Lens = 1;
		break;
	}

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_TP_Mark_Distance_X, "100", &strReadData[0], 256, pFilePath);
	m_CDTPParam.nTP_Mark_Distance_X = atoi(strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_TP_Mark_Distance_Y, "100", &strReadData[0], 256, pFilePath);
	m_CDTPParam.nTP_Mark_Distance_Y= atoi(strReadData);
}

bool CVS10MasterDlg::SetSmallDefectImg(CString strTargetPath, IplImage *pLoadImg, DefectDataQ *pReadRetFileDataQ)
{
	CFile fileObj;
	CFileException except;
	if(!fileObj.Open(strTargetPath, CFile::shareDenyRead, &except))
	{
		TRACE(_T("File could not be opened %d\n"), except.m_cause);
		return false;
	}
	int FileHeader[3];
	DWORD dwRead = 0;
	fileObj.SeekToBegin();
	dwRead = fileObj.Read((BYTE*)&FileHeader[0], 12);
	if(dwRead != 12)
	{
		fileObj.Close();
		return false;
	}
	// 헤더 사이즈 12바이트
	// 0은 Count 개수, 1은 Width , 2는 Height
	int ImgSize = (FileHeader[1]*FileHeader[2]);//1체널임으로. 크기가 Size이다.

	if(m_pSmallImgReadBuf == nullptr)
		m_pSmallImgReadBuf = new BYTE[ImgSize];
	memset(m_pSmallImgReadBuf, 0x00, ImgSize);


	//if(m_bInspectMicOrMac == true)
	{
		if(m_DefectData_MicQ.QGetCnt() != FileHeader[0])
			return false;//수치가 안맞는다.
	}
	//else
	//{
	//	if(m_DefectData_MacQ.QGetCnt() != FileHeader[0])
	//		return false;//수치가 안맞는다.
	//}

	

	//COORD_DINFO DataBuf;
	//IplImage **ppSmallImg = nullptr;
	IplImage *pSmallImg = cvCreateImage(cvSize(FileHeader[1],FileHeader[2]), IPL_DEPTH_8U, 1);
	//cvNamedWindow("SMALL_IMG");
	//if(m_bInspectMicOrMac == true)
	{
		m_DefectData_MicQ.QGetMoveFirstNode();
		do
		{
			dwRead = fileObj.Read(m_pSmallImgReadBuf, ImgSize);
			memcpy(pSmallImg->imageData, m_pSmallImgReadBuf, ImgSize);
			m_DefectData_MicQ.QSetNextNodeImage(pSmallImg);

			//cvShowImage("SMALL_IMG", (*ppSmallImg));
			//cvWaitKey(100);
		}
		while (dwRead == ImgSize);
	}
	//else
	//{
	//	m_DefectData_MacQ.QGetMoveFirstNode();
	//	do
	//	{
	//		dwRead = fileObj.Read(m_pSmallImgReadBuf, ImgSize);
	//		memcpy(pSmallImg->imageData, m_pSmallImgReadBuf, ImgSize);
	//		m_DefectData_MacQ.QSetNextNodeImage(pSmallImg);

	//		//cvShowImage("SMALL_IMG", (*ppSmallImg));
	//		//cvWaitKey(100);
	//	}
	//	while (dwRead == ImgSize);
	//}

	if(pSmallImg !=nullptr)
		cvReleaseImage(&pSmallImg);

	fileObj.Close();
	return true;
}

bool CVS10MasterDlg::Sys_fnInitVS64Interface()
{
	if(1 < __argc)
	{
		int nInputTaskNum = atoi(__argv[1]);
		char *pLinkPath = NULL;

		char path_buffer[_MAX_PATH];
		char chFilePath[_MAX_PATH] = {0,};
		char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		//실행 파일 이름을 포함한 Full path 가 얻어진다.
		::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
		//디렉토리만 구해낸다.
		_splitpath_s(path_buffer, drive, dir, fname, ext);
		strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
		strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));



		switch(nInputTaskNum)
		{

		case TASK_10_Master		: pLinkPath = MSG_TASK_INI_FullPath_TN10; break;
		case TASK_11_Indexer		: pLinkPath = MSG_TASK_INI_FullPath_TN11; break;
		case TASK_21_Inspector	: pLinkPath = MSG_TASK_INI_FullPath_TN21; break;
		case TASK_24_Motion		: pLinkPath = MSG_TASK_INI_FullPath_TN24; break;
		case TASK_31_Mesure3D	: pLinkPath = MSG_TASK_INI_FullPath_TN31; break;
		case TASK_32_Mesure2D	: pLinkPath = MSG_TASK_INI_FullPath_TN32; break;
		case TASK_33_SerialPort	: pLinkPath = MSG_TASK_INI_FullPath_TN33; break;
		case TASK_60_Log			: pLinkPath = MSG_TASK_INI_FullPath_TN60; break;
		case TASK_90_TaskLoader: pLinkPath = MSG_TASK_INI_FullPath_TN90; break;


		case TASK_99_Sample: pLinkPath = MSG_TASK_INI_FullPath_TN99; break;
		default:
			{
				CString ErrorMessage;
				ErrorMessage.Format("Unkown TaskNum(%d)", nInputTaskNum);
				AfxMessageBox(ErrorMessage);
				SendMessage(WM_CLOSE, 0,  0);	
			}return false;
		}

		strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

		m_fnReadDatFile(chFilePath);
		m_ProcInitVal.m_nTaskNum = nInputTaskNum;
		//m_ProcInitVal.m_nLogFileID = nInputTaskNum;

	}
	else
	{
		AfxMessageBox("Input Run Task Number");
		SendMessage(WM_CLOSE, 0,  0);
		return false;
	}
	m_ServerInterface.m_fnSetLogTaskInfo(m_ProcInitVal.m_nLogTaskNo, m_ProcInitVal.m_nLogBlockLovel);	
	if(m_ServerInterface.m_fnRegisterTask(m_ProcInitVal.m_nTaskNum, m_ProcInitVal.m_sServerIP, m_ProcInitVal.m_nPortNo) != 0)
	{
		AfxMessageBox("Client Regist Fail");
		SendMessage(WM_CLOSE, 0,  0);
		return false;
	}
	else
	{
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, "Client Regist OK : %d", m_ProcInitVal.m_nTaskNum);
	}

	m_ServerInterface.m_fnSetAppWindowHandle(m_hWnd);
	m_ServerInterface.m_fnSetAppUserMessage(WM_VS64USER_CALLBACK);
	return true;
}

/*
*	Module Name		:	AOI_fnAnalyzeMsg
*	Parameter		:	Massage Parameter Pointer
*	Return			:	없음
*	Function		:	Sever로 부터 저달 되는 Command를 내부 Function과 Link 한다.(처리)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
DWORD StatusCMDOldDelay = 0;
int CVS10MasterDlg::Sys_fnAnalyzeMsg(CMDMSG* RcvCmdMsg)
{
	VS24MotData stReturn_Msg;	
	try
	{
		//AOI_AppendList("RcvComMsg:T(%d), F(%d), S(%d)",RcvCmdMsg->uTask_Dest, RcvCmdMsg->uFunID_Dest, RcvCmdMsg->uSeqID_Dest) ;
		if(RcvCmdMsg->uTask_Dest == m_ProcInitVal.m_nTaskNum) 
		{	
			switch(RcvCmdMsg->uFunID_Dest)
			{
				//////////////////////////////////////////////////////////////////////////
				//Project Default Command.
			case nBiz_Func_System:
				{
					switch (RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Seq_Alive_Question:
						{
							m_ServerInterface.m_fnSendCommand_Nrs(RcvCmdMsg->uTask_Src, nBiz_Func_System, nBiz_Seq_Alive_Response, G_VS10TASK_STATE);
						}break;
					case nBiz_Seq_Alive_Response:
						{
							TASK_State elTempState;
							elTempState = (TASK_State)RcvCmdMsg->uUnitID_Dest;

							switch (RcvCmdMsg->uTask_Src)
							{
							
							case TASK_21_Inspector:
								{
									if (G_VS21TASK_STATE != elTempState)
									{
										G_TaskStateChange(this, &m_stTaskState[0], elTempState);
									}
									G_VS21TASK_STATE = elTempState;
									//상태 갱신
									m_TaskLinkCheck.m_TaskCheckCnt_Inspector = 0;
								}break;
							case TASK_11_Indexer:	
								{
									if (G_VS11TASK_STATE != elTempState)
									{
										G_TaskStateChange(this, &m_stTaskState[1], elTempState);
									}
									G_VS11TASK_STATE = elTempState;
									//상태 갱신
									m_TaskLinkCheck.m_TaskCheckCnt_Indexer = 0;
								}break;	
							case TASK_31_Mesure3D:	
								{
									if (G_VS31TASK_STATE != elTempState)
									{
										G_TaskStateChange(this, &m_stTaskState[2], elTempState);
									}
									G_VS31TASK_STATE = elTempState;
									m_TaskLinkCheck.m_TaskCheckCnt_Mesure3D =0;
								}break;
							case TASK_24_Motion:	
								{
									if (G_VS24TASK_STATE != elTempState)
									{
										G_TaskStateChange(this, &m_stTaskState[3], elTempState);
									}
									G_VS24TASK_STATE = elTempState;
									//상태 갱신
									if(m_TaskLinkCheck.m_TaskCheckCnt_Motion>3)//Motion Task가 늦게 시작 한것임으로 CMD Reset
									{
										G_WriteInfo(Log_Normal, "TASK_24_Motion : Motion Task가 늦게 시작");
										G_MotObj.m_fnResetAllCMDRun();
									}
									m_TaskLinkCheck.m_TaskCheckCnt_Motion= 0;
								}break;
								
							case TASK_13_AutoFocus:	
								{
									if(RcvCmdMsg->uMsgSize == sizeof(AFModuleData))
										memcpy(&m_AfModuleData, RcvCmdMsg->cMsgBuf, sizeof(AFModuleData));
									{
										if(m_AfModuleData.nMiv == TRUE)
										{
											m_CheckAFInRange.SetCheck(1);
										}
										else
										{
											m_CheckAFInRange.SetCheck(0);
										}

										if(m_AfModuleData.bIn_focus)
										{
											m_CheckAFInfocus.SetCheck(1);
										}
										else
										{
											m_CheckAFInfocus.SetCheck(0);
										}
									}
									if (G_VS13TASK_STATE != elTempState)
									{
										G_TaskStateChange(this, &m_stTaskState[4], elTempState);
									}
									G_VS13TASK_STATE = elTempState;
									//상태 갱신
									m_TaskLinkCheck.m_TaskCheckCnt_AutoFocus =0;
								}break;
							case TASK_32_Mesure2D:	
								{
									if (G_VS32TASK_STATE != elTempState)
									{
										G_TaskStateChange(this, &m_stTaskState[5], elTempState);
									}
									G_VS32TASK_STATE = elTempState;
									m_TaskLinkCheck.m_TaskCheckCnt_Area3DScan =0;
								}break;
							default:					
								throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
								break;					
							}//End Switch	
						}break;

					default:
						throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
	//////////////////////////////////////////////////////////////////////////
			case nBiz_Func_IndexerToMaster:
				{
					switch (RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Seq_Recipe_Change:
						{//공정 전체 Recipe가 선택 된다.
							//G_WriteInfo(Log_Normal, "nBiz_Seq_Recipe_Change");
							//if(RcvCmdMsg->uMsgSize >=7 && RcvCmdMsg->uMsgSize<=40)
							//{
							//	char strNewReciep[128];
							//	memset(strNewReciep, 0x00, 128);
							//	memcpy(strNewReciep, RcvCmdMsg->cMsgBuf, RcvCmdMsg->uMsgSize);
							//	if(CheckProcRecipeName(strNewReciep) == true)
							//	{
							//		WritePrivateProfileString(Section_Process_Data, Key_NowRunRecipeName,strNewReciep, VS10Master_PARAM_INI_PATH);
							//		//LoadNowProcRecipe();

							//		m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Recipe_Change_Ack, 1);//정상처리.
							//	}
							//	else
							//	{
							//		G_WriteInfo(Log_Error, "Recipe_Change : No Recipe!!!!");
							//		m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Recipe_Change_Ack, 0);//비정상.
							//	}
							//}
							//else
							//{
							//	G_WriteInfo(Log_Worrying, "Recipe_Change : Recipe ID Error!!!!");
							//	m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Recipe_Change_Ack, 0);//비정상.
							//}
						}break;
						//-->Loading Sequence
					case nBiz_Seq_Stick_Information			: //Load Offset
						{//Load Offset, Box ID, Stick ID를 받고 기록 한다.
							G_WriteInfo(Log_Normal, "nBiz_Seq_Stick_Information");

							if(RcvCmdMsg->uMsgSize == sizeof(StickInfoByIndexer))
							{
								G_MotObj.m_fnBuzzer2On(CALL_BUZZER2_STICK_INFO);
								if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 0 &&
									G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection ==  0 && 
									(m_SubCIMInterfaceView->m_EQST_Mode == EQP_NORMAL && m_SubCIMInterfaceView->m_PRST_Process == PRST_IDLE ))//대기 상태이어야 한다.
								{//Stick이 없는 상태에서 Indexer로 부터 Stick 정보를 받으면 Loading 준비를 한다.
									memcpy(&m_StickLoadInfo, RcvCmdMsg->cMsgBuf, RcvCmdMsg->uMsgSize);

									CString strNewData;
									strNewData.Format("%s",m_StickLoadInfo.strBoxID);
									WritePrivateProfileString(Section_Process_Data, Key_NowRunBoxID, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
									strNewData.Format("%s", m_StickLoadInfo.strStickID);
									WritePrivateProfileString(Section_Process_Data, Key_NowRunStickID, strNewData.GetBuffer(strNewData.GetLength()), VS10Master_PARAM_INI_PATH);
									
									//Default OK
									WritePrivateProfileString(Section_Process_Data, Key_NowProcessResult, "OK", VS10Master_PARAM_INI_PATH);

									//이전 스틱 정보를 지우자.
									WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosShift, "0", VS10Master_PARAM_INI_PATH);
									WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosDrive, "0", VS10Master_PARAM_INI_PATH);

									if(G_NowEQRunMode == Master_AUTO)
									{//Auto Run상태 이면 자동으로 Auto Seq를 실행 한다.
										G_WriteInfo(Log_Check, "nBiz_Seq_Stick_Information Auto Run Stick Process");
										//m_SeqObj_AutoRun.fnSeqFlowStart();
									}
									else if(G_NowEQRunMode == Master_MANUAL)
									{//Loading 할지를 물어 본다.(자동 모드로  변환 혹은 Loading만 할지 UI에서 선태 시킨다.
										CSubStickLoadDlg LoadSelectDlg;
										LoadSelectDlg.SetStickInfo(CString(m_StickLoadInfo.strBoxID), CString(m_StickLoadInfo.strStickID), CString(""));
										LoadSelectDlg.DoModal();
										if(LoadSelectDlg.m_SelectLoadingPPID.GetLength()>0)
										{
											CString strSelectRecipeName;
											strSelectRecipeName.Format("%s", LoadSelectDlg.m_SelectLoadingPPID);
											WritePrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, strSelectRecipeName.GetBuffer(strSelectRecipeName.GetLength()), VS10Master_PARAM_INI_PATH);
											::SendMessage(this->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 0);
										}

										switch(LoadSelectDlg.m_SelectLoadingType)
										{
										case 1:
											G_WriteInfo(Log_Check, "nBiz_Seq_Stick_Information Only Stick Loading");
											m_SeqObj_StickExchange.fnSeqFlowLoadStart();
											break;
										case 2:
											G_WriteInfo(Log_Check, "nBiz_Seq_Stick_Information Loading And Inspection Start");
											//m_SeqObj_AutoRun.fnSeqFlowStart();
											break;
										case 3:
											G_WriteInfo(Log_Check, "nBiz_Seq_Stick_Information Loading Cancel 1");
											m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);
											break;
										default:
											G_WriteInfo(Log_Check, "nBiz_Seq_Stick_Information Loading Cancel 2");
											m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);
											break;
										}
									}
									else
									{
										OnBnClickedBtEqManualMode();
										G_WriteInfo(Log_Check, "Def Load Manual ");
										m_SeqObj_StickExchange.fnSeqFlowLoadStart();
									}
								}
								else
								{
									G_WriteInfo(Log_Worrying, "nBiz_Seq_Stick_Information  Now Stick Exist ~~~!!!");
									//비정상. 상태를 알린다. ( Unload가 먼저 일어나야 한다.)
									m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);
								}
								//else
								//	m_SeqObj_StickExchange.fnSeqFlowLoadStart();

							}

							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 1);//정상처리.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);//비정상.
						}break;
					case nBiz_Seq_Stick_Load_Ready		://턴테이블 이동 준비 완료.
						{//Turn Table이 진입 할것이다. 준비 Sequence를 실행 하자.
							G_WriteInfo(Log_Normal, "nBiz_Seq_Stick_Load_Ready");
							if(m_SeqObj_StickExchange.m_WaitCMD == nBiz_Seq_Stick_Load_Ready)
								m_SeqObj_StickExchange.m_RecvCMD = nBiz_Seq_Stick_Load_Ready;

							//준비 상태가 완료 되면 Ack를 전달.(Seq 종단에 추가 하자.)
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Ready_OK, 1);//정상처리.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);//비정상.
						}break;

					case nBiz_Seq_Theta_Align_Start		:
						{//Turn Table이 진입하였고 Theta Align을 시작 하라고 한다.
						  //Scope Align Seq를 하고 보정 치를 전달 하자.
							G_WriteInfo(Log_Normal, "nBiz_Seq_Theta_Align_Start");
							if(m_SeqObj_StickExchange.m_WaitCMD == nBiz_Seq_Theta_Align_Start)
								m_SeqObj_StickExchange.m_RecvCMD = nBiz_Seq_Theta_Align_Start;
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Align_Value, 1);//정상처리.
						}break;
					case nBiz_Seq_Stick_Align_Value_Ack	:
						{//Theta 보정치가 적용 되었다 다시 Align을 수행하여 허용치에 들어 갔는지 확인 하자.
							G_WriteInfo(Log_Normal, "nBiz_Seq_Stick_Align_Value_Ack");
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Align_End, 1);//정상처리.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Align_End, 0);//비정상.
							if(m_SeqObj_ThetaAlign.m_WaitCMD == nBiz_Seq_Stick_Align_Value_Ack)
								m_SeqObj_ThetaAlign.m_RecvCMD = nBiz_Seq_Stick_Align_Value_Ack;
							//Gripper로 Stick을 물고 있자.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Standby, 1);//정상처리.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Standby, 0);//비정상.
						}break;
					case nBiz_Seq_TT01_VAC_OFF			:
						{//TurnTable이 나갈수 있게 Stick을 당기거나 올려 준다.
							G_WriteInfo(Log_Normal, "nBiz_Seq_TT01_VAC_OFF");

							if(m_SeqObj_StickExchange.m_WaitCMD == nBiz_Seq_TT01_VAC_OFF)
								m_SeqObj_StickExchange.m_RecvCMD = nBiz_Seq_TT01_VAC_OFF;
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Comp, 1);//정상처리.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Comp, 0);//비정상.
						}break;

					//-->UnLoading Sequence
					case nBiz_Seq_Stick_Result_ACK			  :
						{//Indexer가 결과를 잘받았음으로 
						//Turn Table이 진입 할 수 있는 상태로 만들자.
							G_WriteInfo(Log_Normal, "nBiz_Seq_Stick_Result_ACK");
							if(m_SeqObj_StickExchange.m_WaitCMD == nBiz_Seq_Stick_Result_ACK)
								m_SeqObj_StickExchange.m_RecvCMD = nBiz_Seq_Stick_Result_ACK;
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Ready, 1);//정상처리.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Ready, 0);//비정상.
						}break;
					case nBiz_Seq_Stick_Unload_Ready_OK  :
						{//진입을 시작 하였고.nBiz_Seq_TT01_Pos_AM01가 들오는지를 보자.
							
							//이전 스틱 정보를 지우자.
							WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosShift, "0", VS10Master_PARAM_INI_PATH);
							WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosDrive, "0", VS10Master_PARAM_INI_PATH);

							G_WriteInfo(Log_Normal, "nBiz_Seq_Stick_Unload_Ready_OK");
							if(m_SeqObj_StickExchange.m_WaitCMD == nBiz_Seq_Stick_Unload_Ready_OK)
								m_SeqObj_StickExchange.m_RecvCMD = nBiz_Seq_Stick_Unload_Ready_OK;
							//nBiz_Seq_TT01_Pos_AM01가 들어오면, Stick을 붙일수 있는 위치로 이동 하자.
							//그리고 
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Standby, 1);//정상처리.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Standby, 0);//비정상.
						}break;
					case nBiz_Seq_TT01_VAC_ON				  :
						{//Turn Table이 Vacumm을 On하고 Stick을 흡착 하였다.
							//Gripper를 풀고.
						//turn Talbe이 나갈수 있는 위치로 이동 한다.
							G_WriteInfo(Log_Normal, "nBiz_Seq_TT01_VAC_ON");
							if(m_SeqObj_StickExchange.m_WaitCMD == nBiz_Seq_TT01_VAC_ON)
								m_SeqObj_StickExchange.m_RecvCMD = nBiz_Seq_TT01_VAC_ON;
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Comp, 1);//정상처리.
							//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Comp, 0);//비정상.
							//를 보내고 다음 것을 받을 준비를 하자.
						}break;

					//-->Position Sequence
					
					case nBiz_Seq_TT01_Pos_Turn			:
						{
							if(m_SubStickExchangeView->m_stTT01_Turn_Pos.m_SettingFlag != 1 )
							{
								m_SubStickExchangeView->m_stTT01_AM01_Pos.m_SettingFlag = -1;
								m_SubStickExchangeView->m_stTT01_Turn_Pos.m_SettingFlag = 1;
								m_SubStickExchangeView->m_stTT01_UT02_Pos.m_SettingFlag = -1;
								G_WriteInfo(Log_Normal, "nBiz_Seq_TT01_Pos_Turn");
								m_SubStickExchangeView->m_stTT01_AM01_Pos.SetBkColor(G_Color_Off);
								m_SubStickExchangeView->m_stTT01_Turn_Pos.SetBkColor(G_Color_On);
								m_SubStickExchangeView->m_stTT01_UT02_Pos.SetBkColor(G_Color_Off);
							}
							G_MotObj.m_TT01_NowPos = CMotCtrlObj::TT01_Turn_Pos;
							
						}break;
					case nBiz_Seq_TT01_Pos_AM01			:
						{//Turn Table이 진입완료 신호.
							if(m_SubStickExchangeView->m_stTT01_AM01_Pos.m_SettingFlag != 1 )
							{
								m_SubStickExchangeView->m_stTT01_AM01_Pos.m_SettingFlag = 1;
								m_SubStickExchangeView->m_stTT01_Turn_Pos.m_SettingFlag = -1;
								m_SubStickExchangeView->m_stTT01_UT02_Pos.m_SettingFlag = -1;
								G_WriteInfo(Log_Normal, "nBiz_Seq_TT01_Pos_AM01");
								
								m_SubStickExchangeView->m_stTT01_AM01_Pos.SetBkColor(G_Color_On);
								m_SubStickExchangeView->m_stTT01_Turn_Pos.SetBkColor(G_Color_Off);
								m_SubStickExchangeView->m_stTT01_UT02_Pos.SetBkColor(G_Color_Off);
							}
							G_MotObj.m_TT01_NowPos = CMotCtrlObj::TT01_AM01_Pos;
						}break;

					case nBiz_Seq_TT01_Pos_UT02			:
						{//Turn Table이 완전히 나간 상태 임으로 검사를 시작 하자.
							if(m_SubStickExchangeView->m_stTT01_UT02_Pos.m_SettingFlag != 1 )
							{
								m_SubStickExchangeView->m_stTT01_AM01_Pos.m_SettingFlag = -1;
								m_SubStickExchangeView->m_stTT01_Turn_Pos.m_SettingFlag = -1;
								m_SubStickExchangeView->m_stTT01_UT02_Pos.m_SettingFlag = 1;
								G_WriteInfo(Log_Normal, "nBiz_Seq_TT01_Pos_UT02");
								
								m_SubStickExchangeView->m_stTT01_AM01_Pos.SetBkColor(G_Color_Off);
								m_SubStickExchangeView->m_stTT01_Turn_Pos.SetBkColor(G_Color_Off);
								m_SubStickExchangeView->m_stTT01_UT02_Pos.SetBkColor(G_Color_On);
							}
							G_MotObj.m_TT01_NowPos = CMotCtrlObj::TT01_UT02_Pos;
						}break;


					case nBiz_Seq_Load_Vac_Off_Ack:
						{							
							G_WriteInfo(Log_Normal, "nBiz_Seq_Load_Vac_Off_Ack");
							if(m_SeqObj_StickExchange.m_WaitCMD == nBiz_Seq_Load_Vac_Off_Ack)
								m_SeqObj_StickExchange.m_RecvCMD = nBiz_Seq_Load_Vac_Off_Ack;
						}break;

					case nBiz_Seq_Load_Vac_On_Ack				  :
						{
							G_WriteInfo(Log_Normal, "nBiz_Seq_Load_Vac_On_Ack");
							if(m_SeqObj_StickExchange.m_WaitCMD == nBiz_Seq_Load_Vac_On_Ack)
								m_SeqObj_StickExchange.m_RecvCMD = nBiz_Seq_Load_Vac_On_Ack;
						}break;
					default:
						break;
					}
				}break;
			//case nBiz_Func_MasterToIndexer:
			//	{

			//	}break;
	//////////////////////////////////////////////////////////////////////////
			case nBiz_Func_MotionAct: //모션에 대한 리턴 케이스
				{					
					stReturn_Msg.Reset();				
					int nMessSize = sizeof(VS24MotData);
					memcpy((char*)&stReturn_Msg, (char*)RcvCmdMsg->cMsgBuf, nMessSize);
					switch(RcvCmdMsg->uSeqID_Dest)
					{				
					//case nBiz_Seq_AM01_ScanDrive: //						
					//	G_MotObj.m_fn_Scan_Drive_Return(stReturn_Msg);
					//	break;	
					//case nBiz_Seq_AM01_ScanShift: //						
					//	G_MotObj.m_fn_ScanShift_Return(stReturn_Msg);
					//	break;	
					case nBiz_Seq_AM01_ScanUpDn: //						
						G_MotObj.m_fnScanUpDn_Return(stReturn_Msg);
						break;	
					case nBiz_Seq_AM01_SpaceSnUpDn: //
						G_MotObj.m_fnSpaceSensorUpDn_Return(stReturn_Msg);
						break;
					case nBiz_Seq_AM01_ScopeDrive: //	
						G_MotObj.m_fn_Scope_Drive_Return(stReturn_Msg);
						break;
					case nBiz_Seq_AM01_ScopeShift: //		
						G_MotObj.m_fn_Scope_Shift_Return(stReturn_Msg);
						break;
					case nBiz_Seq_AM01_ScopeUpDn: //	
						G_MotObj.m_fnScopeUpDn_Return(stReturn_Msg);
						break;

					case nBiz_Seq_AM01_MultiLTension: //	
						G_MotObj.m_fnAM01TensionMultiMoveL_Return(stReturn_Msg);
						if(m_SubStickExchangeView->m_btGripperStandbyPos.GetCheck() == true)
							m_SubStickExchangeView->m_btGripperStandbyPos.SetCheck(false);
						break;
					case nBiz_Seq_AM01_MultiRTension: //	
						G_MotObj.m_fnAM01TensionMultiMoveR_Return(stReturn_Msg);
						if(m_SubStickExchangeView->m_btGripperStandbyPos.GetCheck() == true)
							m_SubStickExchangeView->m_btGripperStandbyPos.SetCheck(false);
						break;
					case nBiz_Seq_AM01_TensionUpDn: //	
						G_MotObj.m_fnAM01GripperUpDown_Return(stReturn_Msg);
						if(m_SubStickExchangeView->m_btGripZPos_TT01_out.GetCheck() == true)
							m_SubStickExchangeView->m_btGripZPos_TT01_out.SetCheck(false);
						if(m_SubStickExchangeView->m_btGripZPos_Grip_Standby.GetCheck() == true)
							m_SubStickExchangeView->m_btGripZPos_Grip_Standby.SetCheck(false);
						if(m_SubStickExchangeView->m_btGripZPos_Grip.GetCheck() == true)
							m_SubStickExchangeView->m_btGripZPos_Grip.SetCheck(false);
						if(m_SubStickExchangeView->m_btGripZPos_Measure.GetCheck() == true)
							m_SubStickExchangeView->m_btGripZPos_Measure.SetCheck(false);
						if(m_SubStickExchangeView->m_btGripZPos_Standby.GetCheck() == true)
							m_SubStickExchangeView->m_btGripZPos_Standby.SetCheck(false);
						break;
					case nBiz_Seq_AM01_TensionStart: //	
						G_MotObj.m_fnAM01GripperTensionStart_Return(stReturn_Msg);
						if(m_SubStickExchangeView->m_btGripperStickTentionStart.GetCheck() == true)
							m_SubStickExchangeView->m_btGripperStickTentionStart.SetCheck(false);
						break;
					case nBiz_Seq_AM01_MultiMoveScan: //	
						G_MotObj.m_fnScanMultiMove_Return(stReturn_Msg);
						break;

					case nBiz_Seq_AM01_MultiMoveScofe: //	
						G_MotObj.m_fnScopeMultiMove_Return(stReturn_Msg);
						if(m_SubThetaAlignView->m_btMoveAFPos1.GetCheck() == true)
							m_SubThetaAlignView->m_btMoveAFPos1.SetCheck(false);
						if(m_SubThetaAlignView->m_btMoveAFPos2.GetCheck() == true)
							m_SubThetaAlignView->m_btMoveAFPos2.SetCheck(false);
						if(m_SubThetaAlignView->m_btMoveAlignPos1.GetCheck() == true)
							m_SubThetaAlignView->m_btMoveAlignPos1.SetCheck(false);
						if(m_SubThetaAlignView->m_btMoveAlignPos2.GetCheck() == true)
							m_SubThetaAlignView->m_btMoveAlignPos2.SetCheck(false);

						break;
					case nBiz_Seq_AM01_SyncMoveScanScofe: //	
						G_MotObj.m_fnAM01ScanScopeSyncMove_Return(stReturn_Msg);
						break;
					case nBiz_Seq_AM01_SyncMoveScopeSpaceZ:
						G_MotObj.m_fnScopeSpaceZSyncMove_Return(stReturn_Msg);
						break;

					case nBiz_Seq_TT01_ThetaAlign: //	
						G_MotObj.m_fnTT01AlignMove_Return(stReturn_Msg);
						if(m_SubThetaAlignView->m_btStickThetaCorrect.GetCheck() == true)
							m_SubThetaAlignView->m_btStickThetaCorrect.SetCheck(false);

						break;
					case nBiz_Seq_AM01_SpaceMeasurStart: //	
						G_MotObj.m_fnSpaceSensorMeasureStart_Return(stReturn_Msg);
						break;
					case nBiz_Seq_AM01_ReviewLensOffset: //	
						G_MotObj.m_fnScanSetLensOffset_Return(stReturn_Msg);
						break;
					case nBiz_Seq_AM01_ScopeLensOffset: //	
						G_MotObj.m_fnScopeSetLensOffset_Return(stReturn_Msg);
						break;
					case nBiz_Seq_AM01_JigInout: //	
						G_MotObj.m_fnStickJigMove_Return(stReturn_Msg);
						break;
					default:						
						throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
			case nBiz_Func_CylinderAct: //실린더에 대한 리턴 케이스
				{					
					stReturn_Msg.Reset();				

					int nMessSize = sizeof(VS24MotData);
					memcpy((char*)&stReturn_Msg, (char*)RcvCmdMsg->cMsgBuf, nMessSize);

					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Seq_AM01_TENTION_FWD: 						
						G_MotObj.m_fnStickTentionFWD_Return(stReturn_Msg);
						if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
						{
							m_SubStickExchangeView->m_btGripperSolFWD.SetCheck(true);
							m_SubStickExchangeView->m_btGripperSolBWD.SetCheck(false);
						}
						else
						{
							m_SubStickExchangeView->m_btGripperSolFWD.SetCheck(false);
						}
						break;
					case nBiz_Seq_AM01_TENTION_BWD: 						
						G_MotObj.m_fnStickTentionBWD_Return(stReturn_Msg);

						if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
						{
							m_SubStickExchangeView->m_btGripperSolFWD.SetCheck(false);
							m_SubStickExchangeView->m_btGripperSolBWD.SetCheck(true);
						}
						else
						{
							m_SubStickExchangeView->m_btGripperSolBWD.SetCheck(false);
						}
						break;
					case nBiz_Seq_AM01_OPEN_TEN_GRIPPER: 						
						G_MotObj.m_fnStickGripperOpen_Return(stReturn_Msg);
						if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
						{
							m_SubStickExchangeView->m_btGripperSolClose.SetCheck(false);
							m_SubStickExchangeView->m_btGripperSolOpen.SetCheck(true);
						}
						else
						{
							m_SubStickExchangeView->m_btGripperSolOpen.SetCheck(false);
						}
						break;
					case nBiz_Seq_AM01_CLOSE_TEN_GRIPPER: 						
						G_MotObj.m_fnStickGripperClose_Return(stReturn_Msg);
						if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
						{
							m_SubStickExchangeView->m_btGripperSolClose.SetCheck(true);
							m_SubStickExchangeView->m_btGripperSolOpen.SetCheck(false);
						}
						else
						{
							m_SubStickExchangeView->m_btGripperSolClose.SetCheck(false);
						}
						break;
					case nBiz_Seq_Door_Release_Front: 					
						{
							G_MotObj.m_fnDoorReleasFront_Return(stReturn_Msg);
							if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
							{
								//Lock Button으로 변환.
								//m_btUnlockFrontDoor.SetCheck(true);
								//m_btUnlockFrontDoor.SetWindowText("Front Door Lock");
								G_WriteInfo(Log_Check, "Front Door Lock");
							}
						}break;
					case nBiz_Seq_Door_Release_Side: 		
						{
							G_MotObj.m_fnDoorReleasSide_Return(stReturn_Msg);
							if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
							{
								//Lock Button으로 변환.
								//m_btUnlockSideDoor.SetCheck(true);
								//m_btUnlockSideDoor.SetWindowText("Side Door Lock");
								G_WriteInfo(Log_Check, "Side Door Lock");
							}
						}break;
					case nBiz_Seq_Door_Release_Back: 						
						{
							G_MotObj.m_fnDoorReleasBack_Return(stReturn_Msg);
							if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
							{
								//Lock Button으로 변환.
								//m_btUnlockBackDoor.SetCheck(true);
								//m_btUnlockBackDoor.SetWindowText("Back Door Lock");
								G_WriteInfo(Log_Check, "Back Door Lock");
							}
						}break;
					case nBiz_Seq_Door_Lock_Front:
						{
							G_MotObj.m_fnDoorLockFront_Return(stReturn_Msg);
							if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
							{
								//Unlock Button으로 변환.
								//m_btUnlockFrontDoor.SetCheck(false);
								//m_btUnlockFrontDoor.SetWindowText("Front Door Unlock");
								G_WriteInfo(Log_Check, "Front Door Unlock");
							}
						}break;
					case nBiz_Seq_Door_Lock_Side: 						
						{
							G_MotObj.m_fnDoorLockSide_Return(stReturn_Msg);
							if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
							{
								//Unlock Button으로 변환.
								//m_btUnlockSideDoor.SetCheck(false);
								//m_btUnlockSideDoor.SetWindowText("Side Door Unlock");
								G_WriteInfo(Log_Check, "Side Door Unlock");
							}
						}break;
					case nBiz_Seq_Door_Lock_Back: 						
						{
							G_MotObj.m_fnDoorLockBack_Return(stReturn_Msg);
							if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
							{
								//Unlock Button으로 변환.
								//m_btUnlockBackDoor.SetCheck(false);
								//m_btUnlockBackDoor.SetWindowText("Back Door Unlock");
								G_WriteInfo(Log_Check, "Back Door Unlock");
							}
						}break;
					case nBiz_Seq_AM01_Doontuk_ON:
						{
							G_MotObj.m_fnDoonTukLaserOn_Return(stReturn_Msg);
						}break;
					case nBiz_Seq_AM01_Doontuk_OFF:
						{
							G_MotObj.m_fnDoonTukLaserOff_Return(stReturn_Msg);
						}break;
					case nBiz_Seq_AM01_RingLight_UP:
						{
							G_MotObj.m_fnRingLightUp_Return(stReturn_Msg);
						}break;
					case nBiz_Seq_AM01_RingLight_DN:
						{
							G_MotObj.m_fnRingLightDown_Return(stReturn_Msg);
						}break;
					case nBiz_Seq_AM01_Review_UP:
						{
							G_MotObj.m_fnReviewUp_Return(stReturn_Msg);
						}break;
					case nBiz_Seq_AM01_Review_DN:
						{
							G_MotObj.m_fnReviewDown_Return(stReturn_Msg);
						}break;
						
					default:						
						throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
			case nBiz_Func_ACC: //VAC에 대한 리턴 케이스
				{					
					stReturn_Msg.Reset();							
					memcpy((char*)&stReturn_Msg, (char*)RcvCmdMsg->cMsgBuf, sizeof(VS24MotData));

					switch(RcvCmdMsg->uSeqID_Dest)
					{

					case nBiz_Seq_Light_Scan:
							G_MotObj.m_fnSetLight_Return(stReturn_Msg);
						break;
					case nBiz_Seq_Light_Align:					
						break;	
					case nBiz_Seq_LoadCell_L_ZeroSet:
						G_MotObj.m_fnStickTensionZeroSet_Return(stReturn_Msg);
						break;
					case nBiz_Seq_Read_QR:
						G_MotObj.m_fnQRCodeRead_Return(stReturn_Msg);
						break;
					case nBiz_Seq_Read_AirN2:
						{
							G_MotObj.m_fnAIRRead_Return(stReturn_Msg);
							if(stReturn_Msg.nResult == VS24MotData::Ret_Ok)
							{
								m_AirPressureValue_Air = stReturn_Msg.newPosition1;
								m_AirPressureValue_N2 = stReturn_Msg.newPosition2;

								CString strNewValue;
								strNewValue.Format("%0.1f", (float)m_AirPressureValue_Air);
								WritePrivateProfileString(Section_SVID, Key_PNEUMATIC_AIR, strNewValue.GetBuffer(strNewValue.GetLength()), SVID_LIST_PATH);
								strNewValue.Format("%0.1f", (float)m_AirPressureValue_N2);
								WritePrivateProfileString(Section_SVID, Key_PNEUMATIC_N2, strNewValue.GetBuffer(strNewValue.GetLength()), SVID_LIST_PATH);
							}
						}break;
					case nBiz_Seq_Buzzer2_On:
						break;
					case nBiz_Seq_Buzzer2_Off:
						break;
						
					default:					
						throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
			case nBiz_Func_Vac: //VAC에 대한 리턴 케이스
				{					
					//stReturn_Msg.Reset();				

					//int nMessSize = sizeof(VS24MotData);
					//memcpy((char*)&stReturn_Msg, (char*)RcvCmdMsg->cMsgBuf, nMessSize);

					//switch(RcvCmdMsg->uSeqID_Dest)
					//{
					//case nBiz_Seq_TM01_Vac_ON:						
					//	break;				

					//default:
					//	{
					//		throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					//	}break;
					//}//End Switch
				}break;
			case nBiz_Func_Status: //VAC에 대한 리턴 케이스
				{
					//stReturn_Msg.Reset();				
					//memcpy((char*)&stReturn_Msg, (char*)RcvCmdMsg->cMsgBuf, sizeof(VS24MotData));

					//switch (RcvCmdMsg->uSeqID_Dest)
					//{
					//case nBiz_Seq_ALL:
					//	memcpy((char*)&G_STMACHINE_STATUS, (char*)RcvCmdMsg->cMsgBuf + sizeof(VS24MotData), sizeof(STMACHINE_STATUS));
					//	m_fnAllStatusRelease(stReturn_Msg);	  //전체 IO 상태 갱신 부터 한다.

					//	m_fnDrawImageView();  //전체 상태 갱신 시점에 한다. 박스 시퀀스 이벤트와 동기화 해야 한다.
					//	break;				
					//default:				
					//	throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					//	break;
					//}//End Switch
				}break;
			case nBiz_3DScan_System: //VAC에 대한 리턴 케이스
				{					
					switch (RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_3DScan_UnitInitialize:
						{
							if(m_SeqObj_AreaScan3D.m_WaitCMD == nBiz_3DScan_UnitInitialize)
							{
								m_SeqObj_AreaScan3D.m_RecvCMD = nBiz_3DScan_UnitInitialize;
								m_SeqObj_AreaScan3D.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;	
					case nBiz_3DScan_AlarmReset:
						{
							if(m_SeqObj_AreaScan3D.m_WaitCMD == nBiz_3DScan_AlarmReset)
							{
								m_SeqObj_AreaScan3D.m_RecvCMD = nBiz_3DScan_AlarmReset;
								m_SeqObj_AreaScan3D.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;	
					case nBiz_3DScan_ProcessStart:
						{
							if(m_SeqObj_AreaScan3D.m_WaitCMD == nBiz_3DScan_ProcessStart)
							{
								m_SeqObj_AreaScan3D.m_RecvCMD = nBiz_3DScan_ProcessStart;
								m_SeqObj_AreaScan3D.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;	
					case nBiz_3DScan_ScanStart:
						{
							if(m_SeqObj_AreaScan3D.m_WaitCMD == nBiz_3DScan_ScanStart)
							{
								m_SeqObj_AreaScan3D.m_RecvCMD = nBiz_3DScan_ScanStart;
								m_SeqObj_AreaScan3D.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;	
					case nBiz_3DScan_ScanEnd:
						{
							if(m_SeqObj_AreaScan3D.m_WaitCMD == nBiz_3DScan_ScanEnd)
							{
								m_SeqObj_AreaScan3D.m_RecvCMD = nBiz_3DScan_ScanEnd;
								m_SeqObj_AreaScan3D.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;	
					case nBiz_3DScan_ProcessEnd:
						{
							if(m_SeqObj_AreaScan3D.m_WaitCMD == nBiz_3DScan_ProcessEnd)
							{
								m_SeqObj_AreaScan3D.m_RecvCMD = nBiz_3DScan_ProcessEnd;
								m_SeqObj_AreaScan3D.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;	
					case nBiz_3DScan_FileBackupStart:
						{
							if(m_SeqObj_AreaScan3D.m_WaitCMD == nBiz_3DScan_FileBackupStart)
							{
								m_SeqObj_AreaScan3D.m_RecvCMD = nBiz_3DScan_FileBackupStart;
								m_SeqObj_AreaScan3D.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;
					case nBiz_3DScan_FileBackupEnd:
						{
							if(m_SeqObj_AreaScan3D.m_WaitCMD == nBiz_3DScan_FileBackupEnd)
							{
								//////////////////////////////////////////////////////////////////////////
								//Min Max Avr 값 받기.
								if(RcvCmdMsg->uMsgSize  == sizeof(SCAN_3DResult))
								{
									memcpy((char*)&m_Scan3DResult, (char*)RcvCmdMsg->cMsgBuf, sizeof(SCAN_3DResult));
									G_WriteInfo(Log_Check, "Area 3D Scan Min : %0.3fum", m_Scan3DResult.fMin);
									G_WriteInfo(Log_Check, "Area 3D Scan Max : %0.3fum", m_Scan3DResult.fMax);
									G_WriteInfo(Log_Check, "Area 3D Scan Avr : %0.3fum", m_Scan3DResult.fAvr);
								}
								else
								{
									G_WriteInfo(Log_Error, "Area 3D Scan Result Error");
								}
								

								m_SeqObj_AreaScan3D.m_RecvCMD = nBiz_3DScan_FileBackupEnd;
								m_SeqObj_AreaScan3D.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;
					default:				
						throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
			case nBiz_Func_ReviewLens:
				{
					G_WriteInfo(Log_Normal, "AF nBiz_Func_ReviewLens End");

					if(RcvCmdMsg->uSeqID_Dest>=nBiz_Seq_Lens1 && RcvCmdMsg->uSeqID_Dest<=nBiz_Seq_Lens4)
					{
						m_btReviewLens[0].SetCheck(false);
						m_btReviewLens[1].SetCheck(false);
						m_btReviewLens[2].SetCheck(false);
					}
					//Lens가 변경됨을 알린다.(Seq에서 보내고 구동 중이면.
					if(m_SeqObj_CDTP.m_WaitCMD == nBiz_Func_ReviewLens)
					{
						m_SeqObj_CDTP.m_RecvCMD_Ret = RcvCmdMsg->uSeqID_Dest;
						m_SeqObj_CDTP.m_RecvCMD = nBiz_Func_ReviewLens;
					}
					if(m_SeqObj_CellAlign.m_WaitCMD == nBiz_Func_ReviewLens)
					{
						m_SeqObj_CellAlign.m_RecvCMD_Ret = RcvCmdMsg->uSeqID_Dest;
						m_SeqObj_CellAlign.m_RecvCMD = nBiz_Func_ReviewLens;
					}
					if(m_SeqObj_ReviewCalc.m_WaitCMD == nBiz_Func_ReviewLens)
					{
						m_SeqObj_ReviewCalc.m_RecvCMD_Ret = RcvCmdMsg->uSeqID_Dest;
						m_SeqObj_ReviewCalc.m_RecvCMD = nBiz_Func_ReviewLens;
					}
					if(m_SeqObj_InitAM01.m_WaitCMD == nBiz_Func_ReviewLens)
					{
						m_SeqObj_InitAM01.m_RecvCMD_Ret = RcvCmdMsg->uSeqID_Dest;
						m_SeqObj_InitAM01.m_RecvCMD = nBiz_Func_ReviewLens;
						
					}
					if(m_SeqObj_InsAutoReview.m_WaitCMD == nBiz_Func_ReviewLens)
					{
						m_SeqObj_InsAutoReview.m_RecvCMD_Ret = RcvCmdMsg->uSeqID_Dest;
						m_SeqObj_InsAutoReview.m_RecvCMD = nBiz_Func_ReviewLens;
						
					}
					int TimeOutWait = 3000;//About 5sec
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Seq_Lens1:
						{
							if(RcvCmdMsg->uUnitID_Dest == 0)
							{
								G_WriteInfo(Log_Error, "Review Lens 2x Error");
								TimeOutWait = 0;
							}
							else if(RcvCmdMsg->uUnitID_Dest == nBiz_Seq_Lens1)
							{
								m_btReviewLens[0].SetCheck(true);
								m_btSetRevewLightValue.EnableWindow(TRUE);
								if( IsDlgButtonChecked(IDC_CH_ROLLER_ON) ==BST_CHECKED)
									m_LiveViewObj.ChangeRuler(true, m_ReviewLensOffset.Lens02xPixelSizeX, m_ReviewLensOffset.Lens02xPixelSizeY, REVIEW_LENS_2X);
								else
									m_LiveViewObj.ChangeRuler(false, m_ReviewLensOffset.Lens02xPixelSizeX, m_ReviewLensOffset.Lens02xPixelSizeY, REVIEW_LENS_2X);

								G_WriteInfo(Log_Normal, "AF Task Lens 2X");
								//Lens를 바꾸고 나면 조명 값도 바꾸자.
								m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, m_ReviewLensOffset.Lens02xLight);
								m_NowLensIndex_Review = REVIEW_LENS_2X;
								//Lens가 바뀌었음으로 Lens Offset을 적용 하자.(기준점 이동은 Lens Change 명령이 나갈때 먼저 보낸다.
								G_MotObj.m_fnScanSetLensOffset(REVIEW_LENS_2X);
							}
							else
							{
								m_btSetRevewLightValue.EnableWindow(FALSE);
								G_WriteInfo(Log_Error, "unknown Lens");
								TimeOutWait = 0;
							}

							
						}break;
					case nBiz_Seq_Lens2:
						{
							if(RcvCmdMsg->uUnitID_Dest == 0)
							{
								G_WriteInfo(Log_Error, "Review Lens 10x Error");
								TimeOutWait = 0;
							}
							else if(RcvCmdMsg->uUnitID_Dest == nBiz_Seq_Lens2)
							{
								m_btReviewLens[1].SetCheck(true);
								m_btSetRevewLightValue.EnableWindow(TRUE);
								if( IsDlgButtonChecked(IDC_CH_ROLLER_ON) ==BST_CHECKED)
									m_LiveViewObj.ChangeRuler(true, m_ReviewLensOffset.Lens10xPixelSizeX, m_ReviewLensOffset.Lens10xPixelSizeY, REVIEW_LENS_10X);
								else
									m_LiveViewObj.ChangeRuler(false, m_ReviewLensOffset.Lens10xPixelSizeX, m_ReviewLensOffset.Lens10xPixelSizeY, REVIEW_LENS_10X);

								G_WriteInfo(Log_Normal, "AF Task Lens 10X");
								//Lens를 바꾸고 나면 조명 값도 바꾸자.
								m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, m_ReviewLensOffset.Lens10xLight);
								m_NowLensIndex_Review = REVIEW_LENS_10X;
								G_MotObj.m_fnScanSetLensOffset(REVIEW_LENS_10X);

							}
							else
							{
								m_btSetRevewLightValue.EnableWindow(FALSE);
								G_WriteInfo(Log_Error, "unknown Lens");
								TimeOutWait = 0;
							}
						}break;
					case nBiz_Seq_Lens3:
						{
							if(RcvCmdMsg->uUnitID_Dest == 0)
							{
								G_WriteInfo(Log_Error, "Review Lens 20x Error");
								TimeOutWait = 0;
							}
							else if(RcvCmdMsg->uUnitID_Dest == nBiz_Seq_Lens3)
							{
								m_btReviewLens[2].SetCheck(true);
								m_btSetRevewLightValue.EnableWindow(TRUE);
								if( IsDlgButtonChecked(IDC_CH_ROLLER_ON) ==BST_CHECKED)
									m_LiveViewObj.ChangeRuler(true, m_ReviewLensOffset.Lens20xPixelSizeX, m_ReviewLensOffset.Lens20xPixelSizeY, REVIEW_LENS_20X);
								else
									m_LiveViewObj.ChangeRuler(false, m_ReviewLensOffset.Lens20xPixelSizeX, m_ReviewLensOffset.Lens20xPixelSizeY, REVIEW_LENS_20X);
								G_WriteInfo(Log_Normal, "AF Task Lens 20X");
								//Lens를 바꾸고 나면 조명 값도 바꾸자.
								m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, m_ReviewLensOffset.Lens20xLight);
								m_NowLensIndex_Review = REVIEW_LENS_20X;
								G_MotObj.m_fnScanSetLensOffset(REVIEW_LENS_20X);
							}
							else
							{
								m_btSetRevewLightValue.EnableWindow(FALSE);
								G_WriteInfo(Log_Error, "unknown Lens");
								TimeOutWait = 0;
							}
						}break;
					case nBiz_Seq_Lens4:
						{
							G_WriteInfo(Log_Error, "unknown Lens");
							TimeOutWait = 0;
						}break;
					
					default:
						break;
					}

					//MSG msg;
					//BOOL bRet; 
					////Lens가 바뀌고 Offset이 적용되었는지를 봐야 한다.(시점이 달라서 이중 동작이 나갈수 있다)
					//while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_LENS_OFFSET].B_MOTCMD_State != IDLE )
					//{
					//	TimeOutWait --;
					//	if(TimeOutWait<0)
					//		break;
					//	Sleep(10);
					//	bRet = PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
					//	if (bRet != -1)
					//	{
					//		TranslateMessage(&msg); 
					//		DispatchMessage(&msg); 
					//	}
					//}
				}break;
			case nBiz_Func_AF:
				{
					G_WriteInfo(Log_Normal, "AF nBiz_Func_AF End");
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Seq_Init:
						{
							if(m_SeqObj_InitAM01.m_WaitCMD == nBiz_Seq_Init)
							{
								m_SeqObj_InitAM01.m_RecvCMD = nBiz_Seq_Init;
								m_SeqObj_InitAM01.m_RecvCMD_Ret = RcvCmdMsg->uSeqID_Dest;
							}

							if(RcvCmdMsg->uUnitID_Dest>0)
								G_WriteInfo(Log_Error, "AF Task Init Error");

							G_WriteInfo(Log_Normal, "AF Task Init End");
						}break;
					case nBiz_Seq_AF:
						{
							G_WriteInfo(Log_Check, "nBiz_Seq_AF end(%s)",*((int*)RcvCmdMsg->cMsgBuf)>0?"OK":"NG");
							//Lens가 변경됨을 알린다.(Seq에서 보내고 구동 중이면.
							if(RcvCmdMsg->uMsgSize == sizeof(int))
							{
								if(m_SeqObj_CDTP.m_WaitCMD == nBiz_Seq_AF)
								{
									m_SeqObj_CDTP.m_RecvCMD = nBiz_Seq_AF;
									m_SeqObj_CDTP.m_RecvCMD_Ret = *((int*)RcvCmdMsg->cMsgBuf);
								}
								if(m_SeqObj_CellAlign.m_WaitCMD == nBiz_Seq_AF)
								{
									m_SeqObj_CellAlign.m_RecvCMD = nBiz_Seq_AF;
									m_SeqObj_CellAlign.m_RecvCMD_Ret =*((int*)RcvCmdMsg->cMsgBuf);
								}
								if(m_SeqObj_ReviewCalc.m_WaitCMD == nBiz_Seq_AF)
								{
									m_SeqObj_ReviewCalc.m_RecvCMD = nBiz_Seq_AF;
									m_SeqObj_ReviewCalc.m_RecvCMD_Ret =*((int*)RcvCmdMsg->cMsgBuf);
								}
								if(m_SeqObj_InsAutoReview.m_WaitCMD == nBiz_Seq_AF)
								{
									m_SeqObj_InsAutoReview.m_RecvCMD = nBiz_Seq_AF;
									m_SeqObj_InsAutoReview.m_RecvCMD_Ret = *((int*)RcvCmdMsg->cMsgBuf);
								}
							}
							
							if(RcvCmdMsg->uUnitID_Dest == 1)//AF On
							{
								m_btReviewAFOn.SetCheck(true);
								m_btReviewAFOff.SetCheck(false);
								G_WriteInfo(Log_Normal, "AF Task AF On");
							}
							else//AF Off
							{
								m_btReviewAFOn.SetCheck(false);
								m_btReviewAFOff.SetCheck(true);
								G_WriteInfo(Log_Normal, "AF Task AF Off");
							}
							m_btAutoFocusInit.SetCheck(false);
						}break;
					case nBiz_Seq_AFNowPos:
						{
							if(RcvCmdMsg->uMsgSize == sizeof(int))
							{
								int ZPos_um = (*((int*)RcvCmdMsg->cMsgBuf))/3;
								G_WriteInfo(Log_Check, "nBiz_Seq_AF Pos(%d um)",ZPos_um);
								CString strAFPos;
								strAFPos.Format("%d um", ZPos_um);
								SetDlgItemText(IDC_ST_AF_POS,strAFPos);
							}
						}break;
					default:
						break;
					}
				}break;
				case nBiz_Func_Light:
					{
						G_WriteInfo(Log_Normal, "AF nBiz_Func_Light End");

						switch(RcvCmdMsg->uSeqID_Dest)
						{
						case nBiz_Seq_SetReviewLight:
							{
								CString strLightValue;
								strLightValue.Format("%d", RcvCmdMsg->uUnitID_Dest);
								if(m_SeqObj_CDTP.m_WaitCMD == nBiz_Seq_SetReviewLight)
								{
									m_SeqObj_CDTP.m_RecvCMD = nBiz_Seq_SetReviewLight;
									m_SeqObj_CDTP.m_RecvCMD_Ret= RcvCmdMsg->uUnitID_Dest;
								}
								if(m_SeqObj_CellAlign.m_WaitCMD == nBiz_Seq_SetReviewLight)
								{
									m_SeqObj_CellAlign.m_RecvCMD = nBiz_Seq_SetReviewLight;
									m_SeqObj_CellAlign.m_RecvCMD_Ret= RcvCmdMsg->uUnitID_Dest;
								}
								if(m_SeqObj_ReviewCalc.m_WaitCMD == nBiz_Seq_SetReviewLight)
								{
									m_SeqObj_ReviewCalc.m_RecvCMD = nBiz_Seq_SetReviewLight;
									m_SeqObj_ReviewCalc.m_RecvCMD_Ret= RcvCmdMsg->uUnitID_Dest;
								}
								if(m_SeqObj_InsAutoReview.m_WaitCMD == nBiz_Seq_SetReviewLight)
								{
									m_SeqObj_InsAutoReview.m_RecvCMD = nBiz_Seq_SetReviewLight;
									m_SeqObj_InsAutoReview.m_RecvCMD_Ret= RcvCmdMsg->uUnitID_Dest;
								}
								if(m_btReviewLens[0].GetCheck() ==true)//2x가 선택된 상태.
								{
									m_ReviewLensOffset.Lens02xLight = RcvCmdMsg->uUnitID_Dest;
									WritePrivateProfileString(Section_ReviewLens, Key_Lens02x_Light, strLightValue.GetBuffer(strLightValue.GetLength()), VS10Master_PARAM_INI_PATH);
								}
								else if(m_btReviewLens[1].GetCheck() ==true)//10x가 선택된 상태.
								{
									m_ReviewLensOffset.Lens10xLight = RcvCmdMsg->uUnitID_Dest;
									WritePrivateProfileString(Section_ReviewLens, Key_Lens10x_Light, strLightValue.GetBuffer(strLightValue.GetLength()), VS10Master_PARAM_INI_PATH);
								}
								else if(m_btReviewLens[2].GetCheck() ==true)//20x가 선택된 상태.
								{
									m_ReviewLensOffset.Lens20xLight = RcvCmdMsg->uUnitID_Dest;
									WritePrivateProfileString(Section_ReviewLens, Key_Lens20x_Light, strLightValue.GetBuffer(strLightValue.GetLength()), VS10Master_PARAM_INI_PATH);
								}
								else 
								{
									G_WriteInfo(Log_Normal, "AF Task No Setting Light Selected Lens");
									break;
								}
								m_edReviewLightValue.SetWindowText(strLightValue);
								m_btSetRevewLightValue.SetCheck(false);
								G_WriteInfo(Log_Normal, "AF Task Light Value Set(%d)", RcvCmdMsg->uUnitID_Dest);
							}break;

						default:
							break;
						}
					}break;
			case nBiz_3DMeasure_System:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Recv_AutoFocusEnd:	
						{
							G_WriteInfo(Log_Normal, "3D Scope AF End");
							//theta Align에서 보낸것이면.
							if(m_SubThetaAlignView->m_btScopeAutofocus.GetCheck() ==true)
								m_SubThetaAlignView->m_btScopeAutofocus.SetCheck(false);
							
							//if(m_SeqObj_ThetaAlign.m_WaitCMD == nBiz_Recv_AutoFocusEnd)
							//{
							//	if(RcvCmdMsg->uUnitID_Dest == ActionObs_AutoFocus_Error)
							//		m_SeqObj_ThetaAlign.m_RecvCMD_Ret = 0;//NG
							//	else
							//		m_SeqObj_ThetaAlign.m_RecvCMD_Ret = 1;//OK

							//	m_SeqObj_ThetaAlign.m_RecvCMD = nBiz_Recv_AutoFocusEnd;
							//}

							if(m_SeqObj_3DScope.m_WaitCMD == nBiz_Recv_AutoFocusEnd)
							{
								if(RcvCmdMsg->uUnitID_Dest == ActionObs_AutoFocus_Error)
									m_SeqObj_3DScope.m_RecvCMD_Ret = 0;//NG
								else
									m_SeqObj_3DScope.m_RecvCMD_Ret = 1;//OK

								m_SeqObj_3DScope.m_RecvCMD = nBiz_Recv_AutoFocusEnd;
							}
		
						}break;
					case nBiz_Recv_StartMeasureEnd:		
						{
							if(m_SeqObj_3DScope.m_WaitCMD == nBiz_Recv_StartMeasureEnd)
							{
								m_SeqObj_3DScope.m_RecvCMD_Ret = 0;
								if(RcvCmdMsg->uUnitID_Dest > 0)
								{
									int &nScopeRetCount = m_p3DMeasurPointList[m_SeqObj_3DScope.m_3DMeasureIndex].nScopeRetCount;
									nScopeRetCount = 0;
									RESULT3DScope_DATA *&pScopeRet = m_p3DMeasurPointList[m_SeqObj_3DScope.m_3DMeasureIndex].pScopeRet;
									if(pScopeRet) {
										delete [] pScopeRet;
										pScopeRet = 0x00;
									}

									if(RcvCmdMsg->uMsgSize >= sizeof(int)) {
										memcpy(&nScopeRetCount, RcvCmdMsg->cMsgBuf, sizeof(int));
									}
									if(RcvCmdMsg->uMsgSize == (sizeof(int) + sizeof(RESULT3DScope_DATA)*nScopeRetCount) && 
										m_p3DMeasurPointList!=nullptr && m_3DMeasurPointCnt>m_SeqObj_3DScope.m_3DMeasureIndex)
									{
										pScopeRet = new RESULT3DScope_DATA[nScopeRetCount];
										if(pScopeRet) {
											memcpy(pScopeRet, RcvCmdMsg->cMsgBuf+sizeof(int), sizeof(RESULT3DScope_DATA)*nScopeRetCount);
											m_SeqObj_3DScope.m_RecvCMD_Ret = 1;  // OK
										}
									}
								}
								m_SeqObj_3DScope.m_RecvCMD = nBiz_Recv_StartMeasureEnd;
							}

							m_Sub3DScopeView->m_btStartMeasure3D.SetCheck(false);
							G_WriteInfo(Log_Normal, "3D Scope Measure End");
						}break;					
					case nBiz_3DSend_SelectLens:
						{
							m_Sub3DScopeView->m_bt3DLensSelect[0].SetCheck(false);
							m_Sub3DScopeView->m_bt3DLensSelect[1].SetCheck(false);
							m_Sub3DScopeView->m_bt3DLensSelect[2].SetCheck(false);
							m_Sub3DScopeView->m_bt3DLensSelect[3].SetCheck(false);

							G_MotObj.m_fnScopeSetLensOffset(RcvCmdMsg->uUnitID_Dest);
							//Lens가 바뀌고 Offset이 적용되었는지를 봐야 한다.(시점이 달라서 이중 동작이 나갈수 있다)
							//int TimeOutWait = 500;//About 5sec
							//MSG msg;
							//BOOL bRet; 
							////Lens가 바뀌고 Offset이 적용되었는지를 봐야 한다.(시점이 달라서 이중 동작이 나갈수 있다)
							//while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_LENS_OFFSET].B_MOTCMD_State != IDLE )
							//{
							//	TimeOutWait --;
							//	if(TimeOutWait<0)
							//		break;
							//	Sleep(10);
							//	bRet = PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
							//	if (bRet != -1)
							//	{
							//		TranslateMessage(&msg); 
							//		DispatchMessage(&msg); 
							//	}
							//}
							m_NowLensIndex_3DScope = RcvCmdMsg->uUnitID_Dest;
							//m_SeqObj_3DScope.m_pNowLensIndex_3DScope = &m_NowLensIndex_3DScope; //정의천
							//Theta Align에서 보낸것이면 전달 하자.
							if(m_SeqObj_3DScope.m_WaitCMD == nBiz_3DSend_SelectLens)
							{
								m_SeqObj_3DScope.m_RecvCMD = nBiz_3DSend_SelectLens;
							}
							if(m_SeqObj_ThetaAlign.m_WaitCMD == nBiz_3DSend_SelectLens)
							{
								m_SeqObj_ThetaAlign.m_RecvCMD = nBiz_3DSend_SelectLens;
							}
							if(m_SeqObj_InitAM01.m_WaitCMD == nBiz_3DSend_SelectLens)
							{
								m_SeqObj_InitAM01.m_RecvCMD = nBiz_3DSend_SelectLens;
							}

							switch(RcvCmdMsg->uUnitID_Dest)
							{
							case 1:m_Sub3DScopeView->m_bt3DLensSelect[0].SetCheck(true);break;
							case 2:m_Sub3DScopeView->m_bt3DLensSelect[1].SetCheck(true);break;
							case 3:m_Sub3DScopeView->m_bt3DLensSelect[2].SetCheck(true);break;
							case 4:m_Sub3DScopeView->m_bt3DLensSelect[3].SetCheck(true);break;
							default:
								break;
							}
							
							G_WriteInfo(Log_Normal, "3D Scope Lens Change End(%d)", RcvCmdMsg->uUnitID_Dest);
						}break;	
					case nBiz_Recv_AlignGrabEnd:
						{
							//if(RcvCmdMsg->uUnitID_Dest == 1)
							//{
							//	CString strNewImagePath;
							//	if(m_SubThetaAlignView->m_GrabIndex == 1)
							//	{//Align 1Pos Image //Theta_Align1_Img.jpg
							//		if(m_SubThetaAlignView->m_pAlignImage1 != nullptr)
							//			cvReleaseImage(&m_SubThetaAlignView->m_pAlignImage1);
							//		m_SubThetaAlignView->m_pAlignImage1 = nullptr;

							//		strNewImagePath.Format("%s\\%s", VS31Mesure3D_Result_PATH, THEATA_ALIGN_GRAB1_IMG_Name);
							//		m_SubThetaAlignView->m_pAlignImage1 = cvLoadImage(strNewImagePath);

							//		m_SubThetaAlignView->m_Align1ImgPos = m_SeqObj_ThetaAlign.AlignImgProc_DetectLine(m_SubThetaAlignView->m_pAlignImage1);
							//	}
							//	else if(m_SubThetaAlignView->m_GrabIndex == 2)
							//	{//Align 2Pos Image  //Theta_Align2_Img.jpg
							//		if(m_SubThetaAlignView->m_pAlignImage2 != nullptr)
							//			cvReleaseImage(&m_SubThetaAlignView->m_pAlignImage2);
							//		m_SubThetaAlignView->m_pAlignImage2 = nullptr;

							//		strNewImagePath.Format("%s\\%s", VS31Mesure3D_Result_PATH, THEATA_ALIGN_GRAB2_IMG_Name);
							//		m_SubThetaAlignView->m_pAlignImage2 = cvLoadImage(strNewImagePath);
							//		m_SubThetaAlignView->m_Align2ImgPos = m_SeqObj_ThetaAlign.AlignImgProc_DetectLine(m_SubThetaAlignView->m_pAlignImage2);

							//		//////////////////////////////////////////////////////////////////////////
							//		int DisPixel = m_SubThetaAlignView->m_Align2ImgPos-m_SubThetaAlignView->m_Align1ImgPos;
							//		double DistanceOffset = 0.0;
							//		switch(m_ScopePos.nThetaAlignLensIndex)
							//		{
							//		case SCOPE_LENS_10X:
							//			DistanceOffset = DisPixel*m_ScopeLensOffset.Lens10xPixelSizeX;
							//			break;
							//		case SCOPE_LENS_20X:
							//			DistanceOffset = DisPixel*m_ScopeLensOffset.Lens20xPixelSizeX;
							//			break;
							//		case SCOPE_LENS_50X:
							//			DistanceOffset = DisPixel*m_ScopeLensOffset.Lens50xPixelSizeX;
							//			break;
							//		case SCOPE_LENS_150X:
							//			DistanceOffset = DisPixel*m_ScopeLensOffset.Lens150xPixelSizeX;
							//			break;
							//		}
							//		if(m_ScopePos.nThetaAlignPos2_Drive>0  && m_ScopePos.nThetaAlignPos2_Drive>0)
							//		{
							//			double CorrectionsOffset = 0.0;
							//			double TanA = (DistanceOffset/abs(m_ScopePos.nThetaAlignPos2_Drive - m_ScopePos.nThetaAlignPos1_Drive));//밑변(거리).

							//			double CorrectionsAngle = (180.0/3.1415926535897)*TanA;
							//			m_SubThetaAlignView->m_ThetaMot_Pulse = (int)(CorrectionsAngle/THETA_MOT_PULS_PER_ANGLE);
							//			CString strNewData;
							//			strNewData.Format("Angle:%f, Pulse:%d",CorrectionsAngle, m_SubThetaAlignView->m_ThetaMot_Pulse);
							//			m_SubThetaAlignView->m_stThetaAlignCorrectValue.SetWindowText(strNewData);
							//		}

							//		m_SubThetaAlignView->m_GrabIndex = 0;
							//	}
							//	else
							//	{//Grab Test Image//Theta_Align_Test.jpg

							//		if(m_SubThetaAlignView->m_pAlignImage1 != nullptr)
							//			cvReleaseImage(&m_SubThetaAlignView->m_pAlignImage1);
							//		m_SubThetaAlignView->m_pAlignImage1 = nullptr;

							//		strNewImagePath.Format("%s\\%s", VS31Mesure3D_Result_PATH, THEATA_ALIGN_GRABT_IMG_Name);
							//		m_SubThetaAlignView->m_pAlignImage1 = cvLoadImage(strNewImagePath);
							//		m_SubThetaAlignView->m_Align1ImgPos = m_SeqObj_ThetaAlign.AlignImgProc_DetectLine(m_SubThetaAlignView->m_pAlignImage1);
							//	}
							//	

							//	m_SubThetaAlignView->DrawImageView();
							//	if(m_SeqObj_ThetaAlign.m_WaitCMD == nBiz_Recv_AlignGrabEnd)
							//	{
							//		m_SeqObj_ThetaAlign.m_RecvCMD = nBiz_Recv_AlignGrabEnd;
							//	}
							//}
							//else
							//{
							//	G_WriteInfo(Log_Check, "3D Scope Align Img Grab Error");
							//}
							//m_SubThetaAlignView->m_btMoveAlignPos1.SetCheck(false);
							//m_SubThetaAlignView->m_btMoveAlignPos2.SetCheck(false);
							//m_SubThetaAlignView->m_btGrabThetaAlignImg.SetCheck(false);
							//G_WriteInfo(Log_Normal, "3D Scope Align Img Grab End");
						}break;	
							
					default:					
						throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
			case nBiz_3DMeasure_Status:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_3DRecv_Status:	
						{
							if(m_Sub3DScopeView->m_ScopeAppState  != RcvCmdMsg->uUnitID_Dest)
							{
								CString nNowState;
								switch(RcvCmdMsg->uUnitID_Dest)
								{
								case ObserveSW_Init://<<< 초기 상태
									{
										nNowState.Format("ObserveSW_Init");
										m_Sub3DScopeView->m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
									}break;
								case ObserveSW_None://<<< Observation APP이 구동되지 않았다.
									{
										nNowState.Format("ObserveSW_None");
										m_Sub3DScopeView->m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
									}break;
								case ObserveSW_OK://<<< Observation APP이 정상 구동중이다.
									{
										nNowState.Format("ObserveSW_OK");
										m_Sub3DScopeView->m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
									}break;
								case ObserveSW_CtrlPowerOff://<<< Observation APP는 구동중이나 Controller가 Off 상태 이다.
									{
										nNowState.Format("ObserveSW_CtrlPowerOff");
										m_Sub3DScopeView->m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
									}break;
								case ObserveSW_NoneRemote://<<< Observation APP와 Controller는 구동중 이지만 Remote Mode가 아니다.
									{
										nNowState.Format("ObserveSW_NoneRemote");
										m_Sub3DScopeView->m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
									}break;
								case ObserveSW_ManualMode://<<< Main UI에서 Manual Mode로 변환 한것이다.
									{
										nNowState.Format("ObserveSW_ManualMode");
										m_Sub3DScopeView->m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
									}break;
								default:
									break;
								}
								G_WriteInfo(Log_Normal, "3D Recv_Status Change : %s ", nNowState);
								m_Sub3DScopeView->m_ScopeAppState  = (ObsAppState)RcvCmdMsg->uUnitID_Dest;
							}
							
						}break;

					case nBiz_3DRecv_ActionStatus:					
						{
							if(m_Sub3DScopeView->m_ScopeActionState  != RcvCmdMsg->uUnitID_Dest)
							{
								CString nNowState;
								switch(RcvCmdMsg->uUnitID_Dest)
								{
								case ActionObs_Init://<<< 초기 상태
									{
										nNowState.Format("ActionObs_Init");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);

									}break;
								case ActionObs_Manual://<<< ObserveSW_ManualMode일때는 상태 Check를 하지 않는다.
									{
										nNowState.Format("ActionObs_Manual");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_None://<<< 동작 명령 수행중이 아닐때.
									{
										nNowState.Format("ActionObs_None");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_AutoFocus://<<< Auto Focus 동작 수행시.
									{
										nNowState.Format("ActionObs_AutoFocus");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_AutoUpDownPosSetByImg://<<< 측정 Upper/Lower 위치 선정 수행시.
									{
										nNowState.Format("ActionObs_AutoUpDownPosSetByImg");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_StartMeasurement://<<< 3D Scan 수행시.
									{
										nNowState.Format("ActionObs_StartMeasurement");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_AutoSaveData://<<< 측정 Data 저장 수행시.
									{
										nNowState.Format("ActionObs_AutoSaveData");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_AutoFocus_Error://<<< Auto Focus 동작 수행시 Error.
									{
										nNowState.Format("ActionObs_AutoFocus_Error");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_AutoUpDownPosSetByImg_Error://<<< 측정 Upper/Lower 위치 선정 수행시 Error.
									{
										nNowState.Format("ActionObs_AutoUpDownPosSetByImg_Error");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_StartMeasurement_Error://<<< 3D Scan 수행시 Error.
									{
										nNowState.Format("ActionObs_StartMeasurement_Error");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_AutoSaveData_Error://<<< 측정 Data 저장 수행시 Error.
									{
										nNowState.Format("ActionObs_AutoSaveData_Error");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								case ActionObs_RemoteMode_Error://<<< 측정 가능 Remote Mode가 아니다.
									{
										nNowState.Format("ActionObs_RemoteMode_Error");
										m_Sub3DScopeView->m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									}break;
								}
								G_WriteInfo(Log_Normal, "3D Action Status Change : %s ", nNowState);
								m_Sub3DScopeView->m_ScopeActionState  = (ObsActionState)RcvCmdMsg->uUnitID_Dest;
							}
						}break;					
					default:					
						throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;

//////////////////////////////////////////////////////////////////////////
//Inspection Command
			case nBiz_AOI_System:
				{
					DWORD StatusCMDDelay=0;
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Recv_Status:
						{
							//G_WriteInfo(Log_Info, "nBiz_Recv_Status");
							
							memcpy(&m_FdcData, RcvCmdMsg->cMsgBuf, sizeof(FDC_Data));

							StatusCMDDelay =  GetTickCount() - StatusCMDOldDelay;
							StatusCMDOldDelay= GetTickCount();
							CString strTaskState;
							switch(RcvCmdMsg->uUnitID_Dest)
							{
							case nBiz_AOI_STATE_ERROR	:strTaskState.Format("ERROR(%0.2f)", StatusCMDDelay/1000.0f); break;
							case nBiz_AOI_STATE_IDLE	:strTaskState.Format("IDLE(%0.2f)", StatusCMDDelay/1000.0f); break;
							case nBiz_AOI_STATE_READY	:strTaskState.Format("READY(%0.2f)", StatusCMDDelay/1000.0f); break;
							case nBiz_AOI_STATE_BUSY	:strTaskState.Format("BUSY(%0.2f)", StatusCMDDelay/1000.0f); break;
							case nBiz_AOI_STATE_ALARM	:strTaskState.Format("ALARM(%0.2f)", StatusCMDDelay/1000.0f); break;
							}
							m_SubInspectView->m_stInspectTaskState.SetWindowText(strTaskState);

						}break;
					case nBiz_Recv_FileBackUp_Start:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_FileBackUp_Start");
						}break;
					case nBiz_Recv_FileBackUp_End:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_FileBackUp_End");
						}break;
					case nBiz_Recv_Alarm:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_Alarm");
						}break;

					case nBiz_Recv_CamCommand: //20120408 sts
						{//
							CString strMessage;
							CString strResult;		

							strMessage = RcvCmdMsg->cMsgBuf;							

							if(strMessage.Find("Temperature:") > 0)
							{
								strResult = strMessage.Mid(strMessage.Find(":") + 2, 4);
								WritePrivateProfileString(Section_SVID, Key_STICK_INSPEC_CAM_TEMPERATURE, strResult, SVID_LIST_PATH);
							}
							else if(strMessage.Find("ADC") > 0)
							{
								strResult = strMessage.Mid(strMessage.Find(":") + 2, 4);
								WritePrivateProfileString(Section_SVID, Key_STICK_INSPEC_CAM_VOLTAGE, strResult, SVID_LIST_PATH);
							}
						}break;
					default:
						{
							throw CVs64Exception(_T("CVS10MasterDlg::AOI_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
							break;
						}
					}
				}break;
			case nBiz_AOI_Parameter:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Recv_CalculateInfo:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_CalculateInfo");
							if(RcvCmdMsg->uMsgSize>0)
							{
								m_CamScnaLineCnt	= *(int*)(RcvCmdMsg->cMsgBuf);
								m_CamScnaImgCnt	= *(int*)(RcvCmdMsg->cMsgBuf+sizeof(int));
								CString strNewData;
								strNewData.Format("%d", m_CamScnaLineCnt);
								m_SubInspectView->m_stScanLineCnt.SetWindowText(strNewData);
								strNewData.Format("%d", m_CamScnaImgCnt);
								m_SubInspectView->m_stScanImgCnt.SetWindowText(strNewData);
							}

							CString strNewName;
							if(m_bInspectMicOrMac ==  true)
							{
								strNewName.Format("Inspect Micro");
								m_treeResult.SetItemText(m_ResultRoot_InsMicScan, strNewName);
								HTREEITEM ChildItem = m_treeResult.GetChildItem(m_ResultRoot_InsMicScan);
								while(ChildItem >0)
								{
									m_treeResult.DeleteItem(ChildItem);
									ChildItem = m_treeResult.GetChildItem(m_ResultRoot_InsMicScan);
								}
							}
							else
							{
								strNewName.Format("Inspect Macro");
								m_treeResult.SetItemText(m_ResultRoot_InsMacScan, strNewName);
								HTREEITEM ChildItem = m_treeResult.GetChildItem(m_ResultRoot_InsMacScan);
								while(ChildItem >0)
								{
									m_treeResult.DeleteItem(ChildItem);
									ChildItem = m_treeResult.GetChildItem(m_ResultRoot_InsMacScan);
								}
							}
							
							//Auto Review에 정보 전달.
							m_SubCDTPView->m_AutoReviewNowIndex = 0;
							m_SubCDTPView->m_AutoReviewTotalCnt = 0;
							m_SubCDTPView->m_stTotal_AutoReview.SetWindowText("0");
							m_SubCDTPView->m_stIndex_AutoReview.SetWindowText("0");
							
						}break;
					case nBiz_Recv_BlockDrawInfo:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_BlockDrawInfo");
							//int TaskIndex = -1;
							//switch(RcvCmdMsg->uTask_Src)
							//{
							//case TASK_21_Inspector: TaskIndex = 0; break;
							//	//case INSPECT_TASK_AOI12: TaskIndex = 1; break;
							//	//case INSPECT_TASK_AOI13: TaskIndex = 2; break;
							//	//case INSPECT_TASK_AOI14: TaskIndex = 3; break;
							//}

							//if(m_pBlockList[TaskIndex] != nullptr)
							//	delete [] m_pBlockList[TaskIndex];
							//m_pBlockList[TaskIndex] = nullptr;

							//if(RcvCmdMsg->uMsgSize>0 && RcvCmdMsg->uUnitID_Dest>0)
							//{
							//	m_nBlockCnt[TaskIndex] = 	RcvCmdMsg->uUnitID_Dest;
							//	m_pBlockList[TaskIndex] = new BLOCKINFO[m_nBlockCnt[TaskIndex]];
							//	memcpy(m_pBlockList[TaskIndex], RcvCmdMsg->cMsgBuf, RcvCmdMsg->uMsgSize);
							//	if(m_p3DMapViewer != NULL)
							//	{
							//		m_p3DMapViewer->m_3DViewObj.SetRendDataBlock(m_nBlockCnt[TaskIndex], m_pBlockList[TaskIndex]);
							//		m_p3DMapViewer->m_3DViewObj.RenderScreen();
							//	}
							//}
						}break;
					case nBiz_Recv_CellDrawInfo:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_CellDrawInfo");
							if(m_pCellList != nullptr)
								delete [] m_pCellList;
							m_pCellList= nullptr;

							if(RcvCmdMsg->uMsgSize>0 && RcvCmdMsg->uUnitID_Dest>0)
							{
								m_nCellCnt = 	RcvCmdMsg->uUnitID_Dest;
								m_pCellList = new CELLINFO[m_nCellCnt];
								memcpy(m_pCellList, RcvCmdMsg->cMsgBuf, RcvCmdMsg->uMsgSize);
								//m_p3DMapViewer->m_3DViewObj.RenderFree();
								if(m_p3DMapViewer != NULL)
								{
									m_p3DMapViewer->m_3DViewObj.SetRendDataCell(
										m_GlassReicpeParam.STICK_WIDTH,
										m_GlassReicpeParam.STICK_EDGE_DIS_WIDTH, 
										m_GlassReicpeParam.STICK_EDGE_DIS_HEIGHT, 
										m_nCellCnt, m_pCellList);
									m_p3DMapViewer->m_3DViewObj.RenderScreen();
								}
							}
						}break;
					case nBiz_Recv_PADDrawInfo:
						{
							//G_WriteInfo(Log_Info, "nBiz_Recv_PADDrawInfo");
							//int TaskIndex = -1;
							//switch(RcvCmdMsg->uTask_Src)
							//{
							//case TASK_21_Inspector: TaskIndex = 0; break;
							//	//case INSPECT_TASK_AOI12: TaskIndex = 1; break;
							//	//case INSPECT_TASK_AOI13: TaskIndex = 2; break;
							//	//case INSPECT_TASK_AOI14: TaskIndex = 3; break;
							//}
							//if(m_pPadCellList[TaskIndex] != nullptr)
							//	delete [] m_pPadCellList[TaskIndex];
							//m_pPadCellList[TaskIndex] = nullptr;

							//if(RcvCmdMsg->uMsgSize>0 && RcvCmdMsg->uUnitID_Dest>0)
							//{
							//	m_nPadCellCnt[TaskIndex] = 	RcvCmdMsg->uUnitID_Dest;
							//	m_pPadCellList[TaskIndex] = new PADINFO[m_nPadCellCnt[TaskIndex]];
							//	memcpy(m_pPadCellList[TaskIndex], RcvCmdMsg->cMsgBuf, RcvCmdMsg->uMsgSize);
							//	//m_p3DMapViewer->m_3DViewObj.RenderFree();
							//	if(m_p3DMapViewer != NULL)
							//	{
							//		m_p3DMapViewer->m_3DViewObj.SetRendDataPAD(m_nPadCellCnt[TaskIndex], m_pPadCellList[TaskIndex]);
							//		m_p3DMapViewer->m_3DViewObj.RenderScreen();
							//	}
							//}
						}break;
					case nBiz_Recv_HWParam:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_HWParam");
							if(RcvCmdMsg->uMsgSize>0)
							{							
								memcpy(&m_HWRecipeParam, RcvCmdMsg->cMsgBuf ,sizeof(HW_PARAM));
							}
						}break;
					case nBiz_Recv_ScanImgDrawInfo:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_ScanImgDrawInfo");
							if(RcvCmdMsg->uMsgSize == 0)//Seq Num(Block Number)가 0인것은  Calculate정보이다.
							{
								if(RcvCmdMsg->uUnitID_Dest > 0)//Scan Image Total Count가 기록 된다.
								{
									//m_ParamSetEnd = true;
									if(m_pScanImgList != nullptr)
										delete [] m_pScanImgList;
									m_pScanImgList = nullptr;

									m_nScanImgCnt = 	RcvCmdMsg->uUnitID_Dest;
									m_pScanImgList = new SCANIMGINFO[m_nScanImgCnt];
									memset(m_pScanImgList, 0x00, sizeof(SCANIMGINFO)*m_nScanImgCnt);
									m_p3DMapViewer->m_3DViewObj.SetRendDataScan(
										m_nScanImgCnt, 
										m_pScanImgList, 
										m_ReviewPos.nAutoReviewMic_Cnt,
										&m_MapDisplayData_MicQ,//&m_MergeData_MicQ, 
										m_ReviewPos.nAutoReviewMac_Cnt,
										&m_MapDisplayData_MacQ,
										&m_GlassReicpeParam);
									
									m_p3DMapViewer->m_3DViewObj.RenderScreen();
									CString strNewData;
									if(m_bInspectMicOrMac == true)
									{
										m_MergeData_MicQ.QClean();
										m_DefectData_MicQ.QClean();
										m_DefectPADData_MicQ.QClean();

										m_MapDisplayData_MicQ.QClean();
										strNewData.Format("%d", m_DefectData_MicQ.QGetCnt());
									}
									else
									{
										m_MergeData_MacQ.QClean();
										m_DefectData_MacQ.QClean();
										m_DefectPADData_MacQ.QClean();

										m_MapDisplayData_MacQ.QClean();
										strNewData.Format("%d", m_DefectData_MacQ.QGetCnt());
									}
									m_SubInspectView->m_stTotalDefectCnt.SetWindowText(strNewData);
								}
							}	
							if(m_SeqObj_InsMic.m_WaitCMD == nBiz_Recv_ScanImgDrawInfo)
							{
								m_SeqObj_InsMic.m_RecvCMD = nBiz_Recv_ScanImgDrawInfo;
								m_SeqObj_InsMic.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}

							if(m_SeqObj_InsMac.m_WaitCMD == nBiz_Recv_ScanImgDrawInfo)
							{
								m_SeqObj_InsMac.m_RecvCMD = nBiz_Recv_ScanImgDrawInfo;
								m_SeqObj_InsMac.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
						}break;
					default:
						{
							throw CVs64Exception(_T("CNBiz_AOI_InspectorDlg::AOI_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
							break;
						}
					}
				}break;
			case nBiz_AOI_Inspection:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Recv_ScanReady:
						{//H/W가 Move을 해도 된다.

							if(m_SeqObj_InsMic.m_WaitCMD == nBiz_Recv_ScanReady)
							{
								m_SeqObj_InsMic.m_RecvCMD = nBiz_Recv_ScanReady;
								m_SeqObj_InsMic.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
							if(m_SeqObj_InsMac.m_WaitCMD == nBiz_Recv_ScanReady)
							{
								m_SeqObj_InsMac.m_RecvCMD = nBiz_Recv_ScanReady;
								m_SeqObj_InsMac.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
							G_WriteInfo(Log_Info, "nBiz_Recv_ScanReady");
						}break;
					case nBiz_Recv_ScanStop:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_ScanStop");
						}break;
					case nBiz_Recv_InspectRetInfo:
						{
							

							int DefectCount  = *((int*)RcvCmdMsg->cMsgBuf);
							COORD_DINFO * pDefectList  = (COORD_DINFO*)(RcvCmdMsg->cMsgBuf+sizeof(int));//검사 결과를 가져 온것이다. Count 갯수만큼 복사 하자.
							
							if(m_bInspectMicOrMac == true)
								G_WriteInfo(Log_Info, "nBiz_Recv_InspectRetInfo(%d)", DefectCount);

							int GrayMin = 255;
							int GrayMax = 0;
							
							for(int DefStep= 0; DefStep<DefectCount; DefStep++)
							{
								if(m_bInspectMicOrMac == true)
								{
									//m_DefectData_MicQ.QPutNode((pDefectList+DefStep));
									m_DefectData_MicQ.QPutBackNode((pDefectList+DefStep));
									m_MapDisplayData_MicQ.QPutBackNode((pDefectList+DefStep));
								}
								//else
								//{
								//	//m_DefectData_MacQ.QPutNode((pDefectList+DefStep));
								//	m_DefectData_MacQ.QPutBackNode((pDefectList+DefStep));
								//	m_MapDisplayData_MacQ.QPutBackNode((pDefectList+DefStep));
								//}
								if((pDefectList+DefStep)->nDValueMin<GrayMin)
									GrayMin = (pDefectList+DefStep)->nDValueMin;
								if((pDefectList+DefStep)->nDValueMax>GrayMax)
									GrayMax = (pDefectList+DefStep)->nDValueMax;
							}

							CString strNewValue;
							strNewValue.Format("%d", GrayMin);
							WritePrivateProfileString(Section_SVID, Key_STICK_INSPEC_CAM_GRAY_MIN, strNewValue, SVID_LIST_PATH);
							strNewValue.Format("%d", GrayMax);
							WritePrivateProfileString(Section_SVID, Key_STICK_INSPEC_CAM_GRAY_MAX, strNewValue, SVID_LIST_PATH);
							//////////////////////////////////////////////////////////////////////////
							SCANIMGINFO*pRcvData =(SCANIMGINFO*)(RcvCmdMsg->cMsgBuf+sizeof(int)+(sizeof(DEFECT_DATA)*DefectCount));
							int ImageIndex = ((pRcvData->m_ScanLineNum -1)*(m_CamScnaImgCnt))+(pRcvData->m_ScanImgIndex-1);//배열은 0 Index이다.
							if(ImageIndex<0)
								break;

							*(m_pScanImgList+ImageIndex) = *pRcvData;

							//활성 GrabImage 표시.(Motion이 움직임으로 랜더링은 자동으로 된다.)
							//m_p3DMapViewer->m_3DViewObj.m_nScanActiveIndex= ImageIndex;
							//m_p3DMapViewer->m_3DViewObj.RenderScreen();
							CString strNewData;
							strNewData.Format("%d/%d/%d", m_StartScanLineNum, RcvCmdMsg->uUnitID_Dest, DefectCount);
							m_SubInspectView->m_stProcEndCnt.SetWindowText(strNewData);
							CString strNewData2;

							if(m_bInspectMicOrMac == true)
								strNewData2.Format("[%d/%d]", m_DefectData_MicQ.QGetCnt(), m_DefectPADData_MicQ.QGetCnt());
							//else
							//	strNewData2.Format("[%d/%d]", m_DefectData_MacQ.QGetCnt(), m_DefectPADData_MacQ.QGetCnt());
							m_SubInspectView->m_stTotalDefectCnt.SetWindowText(strNewData2);

						}break;
					case nBiz_Recv_InspectPADRetInfo:
						{
							//G_WriteInfo(Log_Info, "nBiz_Recv_InspectPADRetInfo");
							int DefectCnt  = *((int*)RcvCmdMsg->cMsgBuf);
							COORD_DINFO * pDefectList  = (COORD_DINFO*)(RcvCmdMsg->cMsgBuf+sizeof(int));
							for(int DefStep= 0; DefStep<DefectCnt; DefStep++)
							{
								if(m_bInspectMicOrMac == true)
									m_DefectPADData_MicQ.QPutNode((pDefectList+DefStep));
								//else
								//	m_DefectPADData_MacQ.QPutNode((pDefectList+DefStep));
							}
							CString strNewData2;
							if(m_bInspectMicOrMac == true)
								strNewData2.Format("[%d/%d]",	m_DefectData_MicQ.QGetCnt(), m_DefectPADData_MicQ.QGetCnt());
							//else
							//	strNewData2.Format("[%d/%d]",	m_DefectData_MacQ.QGetCnt(), m_DefectPADData_MacQ.QGetCnt());
							m_SubInspectView->m_stTotalDefectCnt.SetWindowText(strNewData2);
						}break;
					case nBiz_Recv_ScanActEnd:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_ScanActEnd");
						}break;
					case nBiz_Recv_ScanPADEnd:
						{
							if(m_SeqObj_InsMic.m_WaitCMD == nBiz_Recv_ScanPADEnd)
							{
								m_SeqObj_InsMic.m_RecvCMD = nBiz_Recv_ScanPADEnd;
								m_SeqObj_InsMic.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
							if(m_SeqObj_InsMac.m_WaitCMD == nBiz_Recv_ScanPADEnd)
							{
								m_SeqObj_InsMac.m_RecvCMD = nBiz_Recv_ScanPADEnd;
								m_SeqObj_InsMac.m_RecvCMD_Ret = RcvCmdMsg->uUnitID_Dest;
							}
							//G_WriteInfo(Log_Info, "nBiz_Recv_ScanPADEnd");
							G_WriteInfo(Log_Normal, "Line Process End");
							//자동 Scan Start를 날리는 것이사 실제 사용 X
							CString strTickTime;
							if(m_StartScanLineNum<m_CamScnaLineCnt)
							{
								strTickTime.Format("%0.2f",(GetTickCount()-m_StartLineTick)/1000.0f);
								m_SubInspectView->m_stProcTime.SetWindowText(strTickTime);
								m_StartScanLineNum++;
								m_SubInspectView->m_btSendStart.SetCheck(false);
								//m_ServerInterface.m_fnSendCommand_Nrs(	TASK_21_Inspector, 		nBiz_AOI_Inspection, nBiz_Send_ScanStart, m_StartScanLineNum);
								//m_StartLineTick = GetTickCount();
							}
						}break;
					case nBiz_Recv_BlockEnd:
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_BlockEnd");
							CString strTickTime;
							m_StartBlockTick = GetTickCount()-m_StartBlockTick;
							strTickTime.Format("%0.2f",m_StartBlockTick/1000.0f);
							m_SubInspectView->m_stProcTime.SetWindowText(strTickTime);

							m_StartScanLineNum = 0;

							m_SubInspectView->m_btSendRecipeParam.SetCheck(false);
							m_SubInspectView->m_btSendStart.SetCheck(false);

						}break;

					case nBiz_Send_BigInspect:
						{
							if(m_SeqObj_InsMac.m_WaitCMD == nBiz_Send_BigInspect)
							{
								m_SeqObj_InsMac.m_RecvCMD = nBiz_Send_BigInspect;
								m_SeqObj_InsMac.m_RecvCMD_Ret = 0;
							}
						}break;
					case nBiz_Recv_BigInspectResult:
						{
							if(m_SeqObj_InsMac.m_WaitCMD == nBiz_Recv_BigInspectResult)
							{
								int  DefectCnt  =*((int*)(RcvCmdMsg->cMsgBuf));//검사 결과를 가져 온것이다. Count 갯수만큼 복사 하자.
								COORD_DINFO * pDefectList  = (COORD_DINFO*)(RcvCmdMsg->cMsgBuf+sizeof(int));//검사 결과를 가져 온것이다. Count 갯수만큼 복사 하자.
								G_WriteInfo(Log_Check, "nBiz_Recv_BigInspectResult(%d)", DefectCnt);

								COORD_DINFO * pDefectCheck = &pDefectList[0]; 
								for(int DefStep= 0; DefStep<DefectCnt; DefStep++)
								{
									pDefectCheck = &pDefectList[DefStep];
									if(m_bInspectMicOrMac == false)
									{
										//m_DefectData_MacQ.QPutNode((pDefectList+DefStep));
										m_DefectData_MacQ.QPutBackNode((pDefectList+DefStep));
										m_MapDisplayData_MacQ.QPutBackNode((pDefectList+DefStep));
									}
								}
								m_DefectData_MacQ.m_bDataUpdateEnd = false;
								::AfxBeginThread(Thread_InspectDefectMergeProc_Mac, this,	 THREAD_PRIORITY_HIGHEST);
								//////////////////////////////////////////////////////////////////////////
								m_SeqObj_InsMac.m_RecvCMD = nBiz_Recv_BigInspectResult;
								m_SeqObj_InsMac.m_RecvCMD_Ret = 0;
							}
						}break;
					case nBiz_Send_BigSaveImg:
						{
							if(m_SeqObj_InsMac.m_WaitCMD == nBiz_Send_BigSaveImg)
							{
								m_SeqObj_InsMac.m_RecvCMD = nBiz_Send_BigSaveImg;
								m_SeqObj_InsMac.m_RecvCMD_Ret = 0;
							}
						}break;
					case nBiz_Recv_IlluminationValue:
						{
							//G_WriteInfo(Log_Info, "nBiz_Recv_IlluminationValue");
							//CString strValue;
							//strValue.Format("%d", RcvCmdMsg->uUnitID_Dest);
							//cvNamedWindow("Histogram");

							//float Histogaram_Data[256];
							//IplImage	* hist_img = cvCreateImage( cvSize(300, 190), IPL_DEPTH_8U, 3);
							//if(RcvCmdMsg->uMsgSize>0 && RcvCmdMsg->uMsgSize == sizeof(float)*256)
							//{
							//	char s_text[50];
							//	cvZero(hist_img);
							//	int axis_point =  (int)(hist_img->height*0.9);
							//	float hist_val = 0;
							//	int intensity = 0;
							//	memcpy(Histogaram_Data, RcvCmdMsg->cMsgBuf,  sizeof(float)*256);

							//	float max_bin_value = 0;
							//	for ( int i = 0; i < 255; i++)
							//	{
							//		hist_val  = Histogaram_Data[i];

							//		if(max_bin_value<hist_val)
							//			max_bin_value = hist_val;
							//	}

							//	int MaxIntensity = -1;
							//	int MinValue = -1;
							//	int MaxValue = -1;
							//	for ( int i = 0; i < 255; i++)
							//	{
							//		hist_val  = Histogaram_Data[i];
							//		intensity = cvRound( hist_val*255/max_bin_value);
							//		cvLine(hist_img, cvPoint(i+20, axis_point), cvPoint(i+20, abs(axis_point - intensity) ), CV_RGB(255,255,0), 1);
							//		if(intensity>0)// && 3.0f < (hist_val/max_bin_value)*100)
							//		{
							//			MaxValue = i;
							//			MaxIntensity = intensity;
							//		}
							//		if(intensity>0 && MinValue == -1)
							//			MinValue = i;

							//		Histogaram_Data[i] = hist_val;
							//	}

							//	cvLine(hist_img, cvPoint(20, (axis_point - MaxIntensity)), cvPoint(275, (axis_point - MaxIntensity)), CV_RGB(255,0,255), 1);
							//	cvLine(hist_img, cvPoint(20+MaxValue, 30), cvPoint(20+MaxValue, axis_point), CV_RGB(255,0,255), 1);

							//	cvLine(hist_img, cvPoint(20, axis_point+3), cvPoint(275, axis_point+3), CV_RGB(0,255,0), 2);

							//	cvLine(hist_img, cvPoint(20, 0), cvPoint(20, axis_point), CV_RGB(255,0,0), 1);
							//	cvLine(hist_img, cvPoint(275, 0), cvPoint(275, axis_point), CV_RGB(255,0,0), 1);


							//	sprintf_s( s_text, "%d", MaxValue);
							//	cvPutText( hist_img, s_text, cvPoint(MaxValue+15, axis_point+15), &G_DefectFont, CV_RGB(255,0,255));

							//	sprintf_s( s_text, "%d", MinValue);
							//	cvPutText( hist_img, s_text, cvPoint(MinValue+15, axis_point+15), &G_DefectFont, CV_RGB(255,0,255));

							//	//sprintf_s( s_text, "Active Image Histogram");
							//	//cvPutText( hist_img, s_text, cvPoint(70, 10), &G_DefectFont, CV_RGB(0,255,255));

							//	sprintf_s( s_text, "Max Value:%3d",  RcvCmdMsg->uUnitID_Dest);
							//	cvPutText( hist_img, s_text, cvPoint(10, 60), &G_DefectFont, CV_RGB(255,0,0));


							//	sprintf_s( s_text, "TaskNum:%3d", RcvCmdMsg->uTask_Src);
							//	cvPutText( hist_img, s_text, cvPoint(10, 20), &G_DefectFont, CV_RGB(255,0,255));
							//}
							//cvShowImage("Histogram", hist_img);
							//cvReleaseImage(&hist_img);

						}break;
					case nBiz_Recv_IllValueRealTime:
						{
							//G_WriteInfo(Log_Info, "nBiz_Recv_IllValueRealTime");
							//LightValueRecvCountByRealTime++;

							//CString strValue;
							//strValue.Format("%d", RcvCmdMsg->uUnitID_Dest);
							//cvNamedWindow("Histogram_RealTime");

							//switch(RcvCmdMsg->uTask_Src)
							//{
							//case TASK_21_Inspector: 
							//	{
							//		GetDlgItem(IDC_ST_HISTOGRAM1)->SetWindowText(strValue);
							//	}break;
							//}
							//float Histogaram_Data[256];
							//IplImage	* hist_img = cvCreateImage( cvSize(300, 190), IPL_DEPTH_8U, 3);
							//if(RcvCmdMsg->uMsgSize>0 && RcvCmdMsg->uMsgSize == sizeof(float)*256)
							//{
							//	char s_text[50];
							//	cvZero(hist_img);
							//	int axis_point =  (int)(hist_img->height*0.9);
							//	float hist_val = 0;
							//	int intensity = 0;
							//	memcpy(Histogaram_Data, RcvCmdMsg->cMsgBuf,  sizeof(float)*256);

							//	float max_bin_value = 0;
							//	for ( int i = 0; i < 255; i++)
							//	{
							//		hist_val  = Histogaram_Data[i];

							//		if(max_bin_value<hist_val)
							//			max_bin_value = hist_val;
							//	}

							//	int MaxIntensity = -1;
							//	int MinValue = -1;
							//	int MaxValue = -1;
							//	for ( int i = 0; i < 255; i++)
							//	{
							//		hist_val  = Histogaram_Data[i];
							//		intensity = cvRound( hist_val*255/max_bin_value);
							//		cvLine(hist_img, cvPoint(i+20, axis_point), cvPoint(i+20, abs(axis_point - intensity) ), CV_RGB(255,255,0), 1);
							//		if(intensity>0)// && 3.0f < (hist_val/max_bin_value)*100)
							//		{
							//			MaxValue = i;
							//			MaxIntensity = intensity;
							//		}
							//		if(intensity>0 && MinValue == -1)
							//			MinValue = i;

							//		Histogaram_Data[i] = hist_val;
							//	}

							//	cvLine(hist_img, cvPoint(20, (axis_point - MaxIntensity)), cvPoint(275, (axis_point - MaxIntensity)), CV_RGB(255,0,255), 1);
							//	cvLine(hist_img, cvPoint(20+MaxValue, 30), cvPoint(20+MaxValue, axis_point), CV_RGB(255,0,255), 1);

							//	cvLine(hist_img, cvPoint(20, axis_point+3), cvPoint(275, axis_point+3), CV_RGB(0,255,0), 2);

							//	cvLine(hist_img, cvPoint(20, 0), cvPoint(20, axis_point), CV_RGB(255,0,0), 1);
							//	cvLine(hist_img, cvPoint(275, 0), cvPoint(275, axis_point), CV_RGB(255,0,0), 1);


							//	sprintf_s( s_text, "%d", MaxValue);
							//	cvPutText( hist_img, s_text, cvPoint(MaxValue+15, axis_point+15), &G_DefectFont, CV_RGB(255,0,255));

							//	sprintf_s( s_text, "%d", MinValue);
							//	cvPutText( hist_img, s_text, cvPoint(MinValue+15, axis_point+15), &G_DefectFont, CV_RGB(255,0,255));

							//	//sprintf_s( s_text, "Cnt:%d", LightValueRecvCountByRealTime);
							//	//cvPutText( hist_img, s_text, cvPoint(10, 100), &G_DefectFont, CV_RGB(255,0,0));

							//	sprintf_s( s_text, "Max Value:%3d",  RcvCmdMsg->uUnitID_Dest);
							//	cvPutText( hist_img, s_text, cvPoint(10, 60), &G_DefectFont, CV_RGB(255,0,0));


							//	sprintf_s( s_text, "TaskNum:%3d", RcvCmdMsg->uTask_Src);
							//	cvPutText( hist_img, s_text, cvPoint(10, 20), &G_DefectFont, CV_RGB(255,0,255));
							//}
							//cvShowImage("Histogram_RealTime", hist_img);
							//cvReleaseImage(&hist_img);
						}break;
					default:
						{
							throw CVs64Exception(_T("CVS10MasterDlg::AOI_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
							break;
						}
					}
				}break;
			case nBiz_AOI_FileAccess:
				{
					
					CString newStateFile;
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Recv_DefectWriteStart: 
						{

							G_WriteInfo(Log_Info, "nBiz_Recv_DefectWriteStart");
							newStateFile.Format("WDef Start");
							m_SubInspectView->m_stWriteFile1_S.SetWindowText(newStateFile);
						}break;
					case nBiz_Recv_DefectWriteEnd: 
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_DefectWriteEnd");
							newStateFile.Format("WDef End");
							m_SubInspectView->m_stWriteFile1_S.SetWindowText(newStateFile);
							//Small Image Read
							CString strImageOpenPath;
							strImageOpenPath.Format("%s\\%s\\D_Insp_T%d.img", VS21Inspector_Result_PATH, LOCAL_RESULT_FOLDER, TASK_21_Inspector);
							SetSmallDefectImg(strImageOpenPath, nullptr);

							//결과를 Merge하여 Tree에 추가 한다.
							if(m_bInspectMicOrMac == true)
								::AfxBeginThread(Thread_InspectDefectMergeProc_Mic, this,	 THREAD_PRIORITY_HIGHEST);
							//else
							//	::AfxBeginThread(Thread_InspectDefectMergeProc_Mac, this,	 THREAD_PRIORITY_HIGHEST);
							
						}break;
					case nBiz_Recv_BackupStart: 
						{
							G_WriteInfo(Log_Info, "nBiz_Recv_BackupStart");
							newStateFile.Format("Backup Start");
							m_SubInspectView->m_stBackup1_S.SetWindowText(newStateFile);
						}break;
					case nBiz_Recv_BackupEnd: 
						{
							if(m_SeqObj_InsMic.m_WaitCMD == nBiz_Recv_BackupEnd)
							{
								m_SeqObj_InsMic.m_RecvCMD_Ret = 1;//OK
								m_SeqObj_InsMic.m_RecvCMD = nBiz_Recv_BackupEnd;
							}
							if(m_SeqObj_InsMac.m_WaitCMD == nBiz_Recv_BackupEnd)
							{
								m_SeqObj_InsMac.m_RecvCMD_Ret = 1;//OK
								m_SeqObj_InsMac.m_RecvCMD = nBiz_Recv_BackupEnd;
							}
							G_WriteInfo(Log_Info, "nBiz_Recv_BackupEnd");
							newStateFile.Format("Backup End");
							m_SubInspectView->m_stBackup1_S.SetWindowText(newStateFile);
							m_BlockEnd = true;

							//완료되면 Reader를 중지 한다.
							//Auto Review완료 시 하자.
							//m_p3DMapViewer->m_3DViewObj.m_ViewDrawScanImg = false;
						}break;
					default:
						{
							throw CVs64Exception(_T("CVS10MasterDlg::AOI_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
							break;
						}
					}
				}break;
			case nBiz_MCC_Func:
				{

					CString newStateFile;
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_MCC_SeqSendCommand: 
						{
							if(RcvCmdMsg->uUnitID_Dest == nBiz_MCC_File_Write_End)
							{
								char chFolderName[512] = {0};
								memcpy(chFolderName, RcvCmdMsg->cMsgBuf, RcvCmdMsg->uMsgSize);

								m_CIMInterface.m_fnSendMCCFileUploadReport(chFolderName);
							}

						}break;
					}					
				}break;
			default:
				{
					throw CVs64Exception(_T("CVS10MasterDlg::AOI_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
					break;
				}
			}//End Switch
		}
		else
		{
			throw CVs64Exception(_T("CVS10MasterDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
		}
	}
	catch (...)
	{
		throw; 
	}
	return OKAY;
}

/*
*	Module Name		:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function		:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS10MasterDlg::Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	CMDMSG* CmdMsg = (CMDMSG*)lParam;		//SmartPointer 버전으로 개조가 필요하다... Work Item..

	try
	{
		if(CmdMsg != NULL)
		{
			nRet = Sys_fnAnalyzeMsg(CmdMsg);

			if(OKAY != nRet)
			{
				//Client 메시지 처리에대한 에러처리...
			}

			m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
			m_ServerInterface.m_fnFreeMemory(CmdMsg);
			return OKAY;
		}
		else
		{
			throw CVs64Exception(_T("CVS19MWDlg::m_fnVS64MsgProc, Message did not become well."));
		}

	}
	catch (CVs64Exception& e)
	{
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS,  e.m_fnGetErrorMsg());

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, exception -> %s"), e.what());
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, CException -> %s"), wszErr);
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_ServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_ServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_ServerInterface.m_fnFreeMemory(CmdMsg);
	}
	return OKAY;
}


BOOL CVS10MasterDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return __super::PreTranslateMessage(pMsg);
}

HBRUSH CVS10MasterDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(IDC_ST_TASK_STATE1 == DLG_ID_Number ||
		IDC_ST_TASK_STATE2 == DLG_ID_Number ||
		IDC_ST_TASK_STATE3 == DLG_ID_Number ||
		IDC_ST_TASK_STATE4 == DLG_ID_Number ||
		IDC_ST_TASK_STATE5 == DLG_ID_Number ||
		IDC_ST_TASK_STATE6 == DLG_ID_Number ||
		IDC_ST_KEY_STATE == DLG_ID_Number ||
		IDC_ST_ENABLE_KEY== DLG_ID_Number ||
		IDC_ST_KEY_LOCK== DLG_ID_Number ||
		IDC_ST_STICK_IN_AM01_1 == DLG_ID_Number ||
		IDC_ST_STICK_IN_AM01_2 == DLG_ID_Number ||
		IDC_ST_TT01_AM01_P0S == DLG_ID_Number ||
		IDC_ST_TT01_TURN_P0S == DLG_ID_Number ||
		IDC_ST_TT01_UT02_P0S == DLG_ID_Number ||
		IDC_ST_SIGNAL_RED == DLG_ID_Number ||
		IDC_ST_SIGNAL_YELLOW == DLG_ID_Number ||
		IDC_ST_SIGNAL_GREEN == DLG_ID_Number ||
		IDC_ST_SIGNAL_BUZZER == DLG_ID_Number||
		IDC_ST_EQP_NORMAL == DLG_ID_Number ||
		IDC_ST_EQP_FAULT == DLG_ID_Number ||
		IDC_ST_EQP_PM == DLG_ID_Number ||
		IDC_ST_PROC_IDLE == DLG_ID_Number ||
		IDC_ST_PROC_SETUP == DLG_ID_Number ||
		IDC_ST_PROC_EXCUTE == DLG_ID_Number ||
		IDC_ST_PROC_PAUSE == DLG_ID_Number )
		return hbr;
	if(	DLG_ID_Number == IDC_ST_OPENCLOSE_FRONT_LEFT ||
		DLG_ID_Number == IDC_ST_FRONT_LEFT_LOCK ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_FRONT_RIGHT ||
		DLG_ID_Number == IDC_ST_FRONT_RIGHT_LOCK ||

		DLG_ID_Number == IDC_ST_OPENCLOSE_INDEX_SID_LEFT ||
		DLG_ID_Number == IDC_ST_SIDE_LEFT_INDEX_LOCK ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_INDEX_SID_RIGHT ||
		DLG_ID_Number == IDC_ST_SIDE_RIGHT_INDEX_LOCK ||

		DLG_ID_Number == IDC_ST_OPENCLOSE_INSPECT_SID_LEFT ||
		DLG_ID_Number == IDC_ST_SIDE_LEFT_INSPECT_LOCK ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_INSPECT_SID_RIGHT ||
		DLG_ID_Number == IDC_ST_SIDE_RIGHT_INSPECT_LOCK ||

		DLG_ID_Number == IDC_ST_OPENCLOSE_BACK_LEFT ||
		DLG_ID_Number == IDC_ST_BACK_LEFT_LOCK ||
		DLG_ID_Number == IDC_ST_OPENCLOSE_BACK_RIGHT ||
		DLG_ID_Number == IDC_ST_BACK_RIGHT_LOCK)
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}
	if(IDC_ST_EQP_NAME == DLG_ID_Number)
	{
		//pDC->SetTextColor(RGB(rand()%255,rand()%255,rand()%255)); 
		pDC->SetTextColor(RGB(100, 255, 100)); 
		pDC->SetBkColor(m_btBackColor);
		return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	}
	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

int ScanStageX = 0;
int ScanStageY= 0;
int ScopeStageX= 0;
int ScopeStageY= 0;

int ScanOldStageX = 0;
int ScanOldStageY= 0;
int ScopeOldStageX= 0;
int ScopeOldStageY= 0;

bool GripperStickDetectLeft = false;
bool GripperStickDetectRight = false;

int ReadCntCamState = 0;
bool ReadInspectCamState = true;

void CVS10MasterDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(TIMER_MOT_POS_UPDATE == nIDEvent)// && IsWindowVisible() == TRUE)
	{	
		//ScanStageX+=rand()%1000;
		//ScanStageY+=rand()%1000;
		//ScopeStageX+=rand()%1000;
		//ScopeStageY+=rand()%1000;

		//ScanStageX = 630000;
		//ScanStageY = 357244;
		//ScopeStageX = 530055;
		//ScopeStageY = 279244;

		//for(int i=0; i<7;i++)
		//	m_progSeqViewBar[i].SetPos(rand()%5);

		if(G_MotInfo.m_pMotState != nullptr)
		{
				//AXIS_AM01_ScanDrive,					//Cruiser Axis #6
				//AXIS_AM01_ScanShift,					//Cruiser Axis #2
				//AXIS_AM01_ScanUpDn,					//AxisLink_D X
				//AXIS_AM01_SpaceSnUpDn,			//AxisLink_D Y

				//AXIS_AM01_ScopeDrive,				//Cruiser Axis #5
				//AXIS_AM01_ScopeShift,				//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
				//AXIS_AM01_ScopeUpDn,				//AxisLink_D Z
				//AXIS_AM01_AirBlowLeftRight,			//AxisLink_D U

				//AXIS_AM01_TensionLeftUpDn,		//AxisLink_A Z
				//AXIS_AM01_TensionRightUpDn,		//AxisLink_A U
				//AXIS_AM01_LTension1,					//AxisLink_B X
				//AXIS_AM01_LTension2,					//AxisLink_B Y
				//AXIS_AM01_LTension3,					//AxisLink_B Z
				//AXIS_AM01_LTension4,					//AxisLink_B U
				//AXIS_AM01_RTension1,					//AxisLink_C X
				//AXIS_AM01_RTension2,					//AxisLink_C Y
				//AXIS_AM01_RTension3,					//AxisLink_C Z
				//AXIS_AM01_RTension4,					//AxisLink_C U
			ScanStageX = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanShift];//Cruiser Axis #2
			ScanStageY = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanDrive];//Cruiser Axis #6
			if((ScanStageX != ScanOldStageX)  || (ScanStageY != ScanOldStageY))
			{
				m_p3DMapViewer->SetReviewToDraw(ScanStageX, ScanStageY);

				ScanOldStageX = ScanStageX;
				ScanOldStageY= ScanStageY;
			}
			
			ScopeStageX = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeShift];//Cruiser Axis #3, #4 //좌우 Gantry구동 #30가상축 
			ScopeStageY = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeDrive];//Cruiser Axis #5
			if((ScopeStageX != ScopeOldStageX)  || (ScopeStageY != ScopeOldStageY))
			{
				m_p3DMapViewer->SetScopeToDraw(ScopeStageX, ScopeStageY);

				ScopeOldStageX = ScopeStageX;
				ScopeOldStageY= ScopeStageY;
			}
			//////////////////////////////////////////////////////////////////////////
			if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1 && 
				m_SubStickExchangeView->m_stDetectionStickLeft.GetBkColor() == G_Color_Off)
			{
				m_SubStickExchangeView->m_stDetectionStickLeft.SetBkColor(G_Color_On);
			}
			else if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 0 && 
				m_SubStickExchangeView->m_stDetectionStickLeft.GetBkColor() == G_Color_On)
			{
				m_SubStickExchangeView->m_stDetectionStickLeft.SetBkColor(G_Color_Off);
			}
			//////////////////////////////////////////////////////////////////////////
			if(G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1 && 
				m_SubStickExchangeView->m_stDetectionStickRight.GetBkColor() == G_Color_Off)
			{
				m_SubStickExchangeView->m_stDetectionStickRight.SetBkColor(G_Color_On);
			}
			else if(G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 0 && 
				m_SubStickExchangeView->m_stDetectionStickRight.GetBkColor() == G_Color_On)
			{
				m_SubStickExchangeView->m_stDetectionStickRight.SetBkColor(G_Color_Off);
			}
		}
		
		if(G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode == 1)//Auto Mode일때는 Door Open 을 막는다.
		{
			//if(G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose == 1 && (G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock == 0 ||
			//	G_MotInfo.m_pMotState->m_IO_OutA.Out00_IndexerFrontDoorRelease != 1))
			if(G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock == 1 && G_MotInfo.m_pMotState->m_IO_OutA.Out00_IndexerFrontDoorRelease != 1)
			{
				//강재 문열림 Alarm
				G_AlarmAdd(AL_1101);
			}
			//if(G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose == 1 && (G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock == 0 ||
			//	G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease != 1))
			if(G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock == 1 && G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease != 1)
			{
				//강재 문열림 Alarm
				G_AlarmAdd(AL_1101);
			}
			//if(G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose == 1 && (G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock == 0 ||
			//	G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease != 1))
			if(G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock == 1 && G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease != 1)
			{
				//강재 문열림 Alarm
				G_AlarmAdd(AL_1101);
			}
			//if(G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose == 1 && (G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock == 0 ||
			//	G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease != 1))
			if(G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock == 1 && G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease != 1)
			{
				//강재 문열림 Alarm
				G_AlarmAdd(AL_1101);
			}
			//if(G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose == 1 && (G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock == 0 ||
			//	G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease != 1))
			if(G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock == 1 && G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease != 1)
			{
				//강재 문열림 Alarm
				G_AlarmAdd(AL_1101);
			}
			//if(G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose == 1 && (G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock == 0 ||
			//	G_MotInfo.m_pMotState->m_IO_OutA.Out02_InspectorBackDoorRelease != 1))
			if(G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock == 1 && G_MotInfo.m_pMotState->m_IO_OutA.Out02_InspectorBackDoorRelease != 1)
			{
				//강재 문열림 Alarm
				G_AlarmAdd(AL_1101);
			}
			//if(G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose == 1 && (G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock == 0 ||
			//	G_MotInfo.m_pMotState->m_IO_OutA.Out02_InspectorBackDoorRelease != 1))
			if(G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock == 1 && G_MotInfo.m_pMotState->m_IO_OutA.Out02_InspectorBackDoorRelease != 1)
			{
				//강재 문열림 Alarm
				G_AlarmAdd(AL_1101);
			}
		}

		if(G_MotInfo.m_pMotState->stNormalStatus.In00_EMO1 == 1 || G_MotInfo.m_pMotState->stNormalStatus.In01_EMO2 == 1 ||
			G_MotInfo.m_pMotState->stNormalStatus.In02_EMO3 == 1 || G_MotInfo.m_pMotState->stNormalStatus.In28_EMO4 == 1)
		{
			//EMO Alarm
			G_AlarmAdd(AL_1107);
		}
		//EQ State Upate
		{
			if(m_SubCIMInterfaceView != nullptr)
			{
				//m_EQProcessState[0].SetTextColor(RGB(0,0,0));
				//m_EQProcessState[0].SetBkColor(G_Color_Off);
				//m_EQProcessState[1].SetTextColor(RGB(0,0,0));
				//m_EQProcessState[1].SetBkColor(G_Color_Off);
				//m_EQProcessState[2].SetTextColor(RGB(0,0,0));
				//m_EQProcessState[2].SetBkColor(G_Color_Off);
				//m_EQProcessState[3].SetTextColor(RGB(0,0,0));
				//m_EQProcessState[3].SetBkColor(G_Color_Off);

				switch(m_SubCIMInterfaceView->m_EQST_Mode)
				{
				case EQP_NORMAL:
					{
						//m_EQState[0].set
						if(m_btChangeAutoMode.IsWindowEnabled() == FALSE)
							m_btChangeAutoMode.EnableWindow(TRUE);
						//if(m_btChangeAutoMode.GetCheck() == true)
						//	m_btChangeAutoMode.SetCheck(false);

						if(m_btChangeManualMode.IsWindowEnabled() == FALSE)
							m_btChangeManualMode.EnableWindow(TRUE);
						//if(m_btChangeManualMode.GetCheck() == true)
						//	m_btChangeManualMode.SetCheck(false);

						if(m_btChangePMMode.IsWindowEnabled() == FALSE)
							m_btChangePMMode.EnableWindow(TRUE);
						if(m_btChangePMMode.GetCheck() == true)
							m_btChangePMMode.SetCheck(false);


						m_EQState[0].SetBkColor(GREEN);
						m_EQState[1].SetBkColor(G_Color_Off);
						m_EQState[2].SetBkColor(G_Color_Off);

					}break;
				case EQP_FAULT:
					{
						if(m_btChangeAutoMode.IsWindowEnabled() == TRUE)
							m_btChangeAutoMode.EnableWindow(FALSE);
						if(m_btChangeAutoMode.GetCheck() == true)
							m_btChangeAutoMode.SetCheck(false);

						if(m_btChangeManualMode.IsWindowEnabled() == FALSE)
							m_btChangeManualMode.EnableWindow();
						if(m_btChangeManualMode.GetCheck() == false)
							m_btChangeManualMode.SetCheck(true);

						if(m_btChangePMMode.IsWindowEnabled() == FALSE)
							m_btChangePMMode.EnableWindow(TRUE);
						if(m_btChangePMMode.GetCheck() == true)
							m_btChangePMMode.SetCheck(false);

						m_EQState[0].SetBkColor(G_Color_Off);
						m_EQState[1].SetBkColor(RED);
						m_EQState[2].SetBkColor(G_Color_Off);
					}break;
				case EQP_PM:
					{
						if(m_btChangeAutoMode.IsWindowEnabled() == TRUE)
							m_btChangeAutoMode.EnableWindow(FALSE);
						if(m_btChangeAutoMode.GetCheck() == true)
							m_btChangeAutoMode.SetCheck(false);

						if(m_btChangeManualMode.IsWindowEnabled() == TRUE)
							m_btChangeManualMode.EnableWindow(FALSE);
						if(m_btChangeManualMode.GetCheck() == true)
							m_btChangeManualMode.SetCheck(false);

						if(m_btChangePMMode.IsWindowEnabled() == FALSE)
							m_btChangePMMode.EnableWindow(TRUE);
						if(m_btChangePMMode.GetCheck() == false)
							m_btChangePMMode.SetCheck(true);

						m_EQState[0].SetBkColor(G_Color_Off);
						m_EQState[1].SetBkColor(G_Color_Off);
						m_EQState[2].SetBkColor(BLUE);

						if(m_SubPMView != nullptr)
						{
							if(m_SubPMView->IsWindowVisible() == FALSE)
							{
								m_SubPMView->CenterWindow();
								m_SubPMView->ShowWindow(SW_SHOW);
							}
						}
					}break;
				default:
					{
					}break;
				}

				if(m_SubPMView != nullptr)
				{
					if(m_SubPMView->IsWindowVisible() == TRUE  && m_SubCIMInterfaceView->m_EQST_Mode != EQP_PM)
						m_SubPMView->ShowWindow(SW_HIDE);
				}

				switch(m_SubCIMInterfaceView->m_PRST_Process)
				{
				case PRST_IDLE:
					m_EQProcessState[0].SetBkColor(GREEN);
					m_EQProcessState[1].SetBkColor(G_Color_Off);
					m_EQProcessState[2].SetBkColor(G_Color_Off);
					m_EQProcessState[3].SetBkColor(G_Color_Off);

					{//Seq 구동정지 상태 Init만 가능 하게 한다.
						if(m_btSeqStart[0].IsWindowEnabled()==FALSE && m_btChangeManualMode.GetCheck() ==true)
						{
							for(int iStep = 0; iStep<8; iStep++)
							{
								m_btSeqStart[iStep].EnableWindow(TRUE);
							}
							m_btSeqStart[9].EnableWindow(TRUE);
						}

					}
					break;
				case PRST_SETUP:
					m_EQProcessState[0].SetBkColor(G_Color_Off);
					m_EQProcessState[1].SetBkColor(YELLOW);
					m_EQProcessState[2].SetBkColor(G_Color_Off);
					m_EQProcessState[3].SetBkColor(G_Color_Off);

					{//Seq 구동정지 상태 Init만 가능 하게 한다.
						if(m_btChangeAutoMode.GetCheck() == true)
						{//Setup상태가 되면 Manual로 변경 해야 한다. 자동.
							OnBnClickedBtEqManualMode();
						}
						if(m_btSeqStart[0].IsWindowEnabled()==TRUE)
						{
							for(int iStep = 0; iStep<8; iStep++)
							{
								m_btSeqStart[iStep].EnableWindow(FALSE);
							}
							m_btSeqStart[9].EnableWindow(FALSE);
						}

					}
					break;
				case PRST_EXECUTE:
					m_EQProcessState[0].SetBkColor(G_Color_Off);
					m_EQProcessState[1].SetBkColor(G_Color_Off);
					m_EQProcessState[2].SetBkColor(GREEN);
					m_EQProcessState[3].SetBkColor(G_Color_Off);
					break;
				case PRST_PAUSE:
					m_EQProcessState[0].SetBkColor(G_Color_Off);
					m_EQProcessState[1].SetBkColor(G_Color_Off);
					m_EQProcessState[2].SetBkColor(G_Color_Off);
					m_EQProcessState[3].SetBkColor(BLUE);
					break;
				default:
					break;
				}
			}
		}
		//////////////////////////////////////////////////////////////////////////
		//Door Lock 감지.
		//////////////////////////////////////////////////////////////////////////
		if(G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose == 0 &&
			m_stOpenCloseDoor[0].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose)
		{
			m_stOpenCloseDoor[0].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose;
			m_stOpenCloseDoor[0].SetWindowText("Close");
			m_stOpenCloseDoor[0].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[0].SetBkColor(G_Color_Close);
			m_stOpenCloseDoor[1].SetWindowText("Close");
			m_stOpenCloseDoor[1].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[1].SetBkColor(G_Color_Close);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose == 1&&
			m_stOpenCloseDoor[0].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose)
		{
			m_stOpenCloseDoor[0].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose;
			m_stOpenCloseDoor[0].SetWindowText("Open");
			m_stOpenCloseDoor[0].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[0].SetBkColor(G_Color_Open);
			m_stOpenCloseDoor[1].SetWindowText("Open");
			m_stOpenCloseDoor[1].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[1].SetBkColor(G_Color_Open);
		}
		if(G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock == 0&&
			m_stLockCheckDoor[0].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock)
		{
			m_stLockCheckDoor[0].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock;
			m_stLockCheckDoor[0].SetWindowText("Lock");
			m_stLockCheckDoor[0].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[0].SetBkColor(G_Color_Lock);
			m_stLockCheckDoor[1].SetWindowText("Lock");
			m_stLockCheckDoor[1].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[1].SetBkColor(G_Color_Lock);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock == 1&&
			m_stLockCheckDoor[0].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock)
		{
			m_stLockCheckDoor[0].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock;
			m_stLockCheckDoor[0].SetWindowText("Unlock");
			m_stLockCheckDoor[0].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[0].SetBkColor(G_Color_Unlock);
			m_stLockCheckDoor[1].SetWindowText("Unlock");
			m_stLockCheckDoor[1].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[1].SetBkColor(G_Color_Unlock);
		}

		//////////////////////////////////////////////////////////////////////////
		if(G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose == 0&&
			m_stOpenCloseDoor[2].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose)
		{
			m_stOpenCloseDoor[2].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose;
			m_stOpenCloseDoor[2].SetWindowText("Close");
			m_stOpenCloseDoor[2].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[2].SetBkColor(G_Color_Close);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose == 1 &&
			m_stOpenCloseDoor[2].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose)
		{
			m_stOpenCloseDoor[2].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose;
			m_stOpenCloseDoor[2].SetWindowText("Open");
			m_stOpenCloseDoor[2].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[2].SetBkColor(G_Color_Open);
		}
		if(G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock == 0 &&
			m_stLockCheckDoor[2].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock)
		{
			m_stLockCheckDoor[2].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock;
			m_stLockCheckDoor[2].SetWindowText("Lock");
			m_stLockCheckDoor[2].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[2].SetBkColor(G_Color_Lock);
		}
		else  if(G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock == 1 &&
			m_stLockCheckDoor[2].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock)
		{
			m_stLockCheckDoor[2].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock;
			m_stLockCheckDoor[2].SetWindowText("Unlock");
			m_stLockCheckDoor[2].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[2].SetBkColor(G_Color_Unlock);
		}
		//////////////////////////////////////////////////////////////////////////
		if(G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose == 0 &&
			m_stOpenCloseDoor[3].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose)
		{
			m_stOpenCloseDoor[3].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose;
			m_stOpenCloseDoor[3].SetWindowText("Close");
			m_stOpenCloseDoor[3].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[3].SetBkColor(G_Color_Close);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose == 1 &&
			m_stOpenCloseDoor[3].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose)
		{
			m_stOpenCloseDoor[3].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose;
			m_stOpenCloseDoor[3].SetWindowText("Open");
			m_stOpenCloseDoor[3].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[3].SetBkColor(G_Color_Open);
		}
		if(G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock == 0 &&
			m_stLockCheckDoor[3].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock)
		{
			m_stLockCheckDoor[3].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock;
			m_stLockCheckDoor[3].SetWindowText("Lock");
			m_stLockCheckDoor[3].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[3].SetBkColor(G_Color_Lock);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock == 1 &&
			m_stLockCheckDoor[3].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock)
		{
			m_stLockCheckDoor[3].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock;
			m_stLockCheckDoor[3].SetWindowText("Unlock");
			m_stLockCheckDoor[3].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[3].SetBkColor(G_Color_Unlock);
		}
		//////////////////////////////////////////////////////////////////////////
		if(G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose == 0&&
			m_stOpenCloseDoor[4].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose)
		{
			m_stOpenCloseDoor[4].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose;
			m_stOpenCloseDoor[4].SetWindowText("Close");
			m_stOpenCloseDoor[4].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[4].SetBkColor(G_Color_Close);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose == 1&&
			m_stOpenCloseDoor[4].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose)
		{
			m_stOpenCloseDoor[4].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose;
			m_stOpenCloseDoor[4].SetWindowText("Open");
			m_stOpenCloseDoor[4].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[4].SetBkColor(G_Color_Open);
		}
		if(G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock == 0 &&
			m_stLockCheckDoor[4].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock)
		{
			m_stLockCheckDoor[4].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock;
			m_stLockCheckDoor[4].SetWindowText("Lock");
			m_stLockCheckDoor[4].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[4].SetBkColor(G_Color_Lock);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock == 1 &&
			m_stLockCheckDoor[4].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock)
		{
			m_stLockCheckDoor[4].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock;
			m_stLockCheckDoor[4].SetWindowText("Unlock");
			m_stLockCheckDoor[4].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[4].SetBkColor(G_Color_Unlock);
		}
		//////////////////////////////////////////////////////////////////////////
		if(G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose == 0 &&
			m_stOpenCloseDoor[5].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose)
		{
			m_stOpenCloseDoor[5].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose;
			m_stOpenCloseDoor[5].SetWindowText("Close");
			m_stOpenCloseDoor[5].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[5].SetBkColor(G_Color_Close);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose == 1&&
			m_stOpenCloseDoor[5].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose)
		{
			m_stOpenCloseDoor[5].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose;
			m_stOpenCloseDoor[5].SetWindowText("Open");
			m_stOpenCloseDoor[5].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[5].SetBkColor(G_Color_Open);
		}
		if(G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock == 0 &&
			m_stLockCheckDoor[5].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock)
		{
			m_stLockCheckDoor[5].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock;
			m_stLockCheckDoor[5].SetWindowText("Lock");
			m_stLockCheckDoor[5].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[5].SetBkColor(G_Color_Lock);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock == 1 &&
			m_stLockCheckDoor[5].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock)
		{
			m_stLockCheckDoor[5].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock;
			m_stLockCheckDoor[5].SetWindowText("Unlock");
			m_stLockCheckDoor[5].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[5].SetBkColor(G_Color_Unlock);
		}
		//////////////////////////////////////////////////////////////////////////
		if(G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose == 0 &&
			m_stOpenCloseDoor[6].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose)
		{
			m_stOpenCloseDoor[6].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose;
			m_stOpenCloseDoor[6].SetWindowText("Close");
			m_stOpenCloseDoor[6].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[6].SetBkColor(G_Color_Close);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose == 1 &&
			m_stOpenCloseDoor[6].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose)
		{
			m_stOpenCloseDoor[6].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose;
			m_stOpenCloseDoor[6].SetWindowText("Open");
			m_stOpenCloseDoor[6].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[6].SetBkColor(G_Color_Open);
		}
		if(G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock == 0&&
			m_stLockCheckDoor[6].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock)
		{
			m_stLockCheckDoor[6].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock;
			m_stLockCheckDoor[6].SetWindowText("Lock");
			m_stLockCheckDoor[6].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[6].SetBkColor(G_Color_Lock);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock == 1 &&
			m_stLockCheckDoor[6].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock)
		{
			m_stLockCheckDoor[6].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock;
			m_stLockCheckDoor[6].SetWindowText("Unlock");
			m_stLockCheckDoor[6].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[6].SetBkColor(G_Color_Unlock);
		}
		//////////////////////////////////////////////////////////////////////////

		if(G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose == 0 &&
			m_stOpenCloseDoor[7].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose)
		{
			m_stOpenCloseDoor[7].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose;
			m_stOpenCloseDoor[7].SetWindowText("Close");
			m_stOpenCloseDoor[7].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[7].SetBkColor(G_Color_Close);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose ==1 &&
			m_stOpenCloseDoor[7].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose)
		{
			m_stOpenCloseDoor[7].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose;
			m_stOpenCloseDoor[7].SetWindowText("Open");
			m_stOpenCloseDoor[7].SetTextColor(RGB(0,0,0));
			m_stOpenCloseDoor[7].SetBkColor(G_Color_Open);
		}
		if(G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock ==0  &&
			m_stLockCheckDoor[7].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock)
		{
			m_stLockCheckDoor[7].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock;
			m_stLockCheckDoor[7].SetWindowText("Lock");
			m_stLockCheckDoor[7].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[7].SetBkColor(G_Color_Lock);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock == 1&&
			m_stLockCheckDoor[7].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock)
		{
			m_stLockCheckDoor[7].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock;
			m_stLockCheckDoor[7].SetWindowText("Unlock");
			m_stLockCheckDoor[7].SetTextColor(RGB(0,0,0));
			m_stLockCheckDoor[7].SetBkColor(G_Color_Unlock);
		}
		
		//2015.01.15 GJJ S
		if(G_MotInfo.m_pMotState->stNormalStatus.In03_Sn_IDXFrontDoorOpenClose == 1 ||
			G_MotInfo.m_pMotState->stNormalStatus.In07_Sn_IDXSideDoorLeftOpenClose == 1 ||
			G_MotInfo.m_pMotState->stNormalStatus.In09_Sn_IDXSideDoorRightOpenClose == 1 ||
			G_MotInfo.m_pMotState->stNormalStatus.In11_Sn_InspSideDoorLeftOpenClose == 1 ||
			G_MotInfo.m_pMotState->stNormalStatus.In13_Sn_InspSideDoorRightOpenClose == 1 ||
			G_MotInfo.m_pMotState->stNormalStatus.In15_Sn_InspBackDoorLeftOpenClose == 1 ||
			G_MotInfo.m_pMotState->stNormalStatus.In17_Sn_InspBackDoorRightOpenClose == 1)
		{

			if(m_SubCIMInterfaceView->m_EQST_Mode != EQP_PM)
			{
				m_SubCIMInterfaceView->OnBnClickedBtEqpPm();
				G_WriteInfo(Log_Normal, "Door Close, PM Mode Release!");
			}
			//if(m_SubPMView->IsWindowVisible()== TRUE)
			//{
			//	m_SubPMView->ShowWindow(SW_HIDE);
			//	G_WriteInfo(Log_Normal, "Door Close, PM Mode Release!");	
			//}
			//if(m_bAutoPMOn)
			//{
			//	G_WriteInfo(Log_Normal, "Door Close, PM Mode Release!");	
			//	m_bAutoPMOn = FALSE;
			//}
		}
		//else
		//{
		//	if(!m_bAutoPMOn)
		//	{
		//		G_WriteInfo(Log_Worrying, "Door Open Auto PM Mode!");	
		//		m_bAutoPMOn = TRUE;

		//		m_pModeLockView->m_fnDisplayTitle("도어 오픈 감지", "장비 조작 금지");
		//		m_pModeLockView->ShowWindow(SW_SHOW);				
		//	}
		//	
		//}	
		//2015.01.15 GJJ E

		if(G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode == 1)//Auto Mode일때는 Door Open 을 막는다.
		{
			if(m_btUnlockFrontDoor.IsWindowEnabled() == TRUE)
				m_btUnlockFrontDoor.EnableWindow(FALSE);

			if(m_btUnlockSideDoor.IsWindowEnabled() == TRUE)
				m_btUnlockSideDoor.EnableWindow(FALSE);

			if(m_btUnlockBackDoor.IsWindowEnabled() == TRUE)
				m_btUnlockBackDoor.EnableWindow(FALSE);
			
		}
		else
		{
			if(m_btUnlockFrontDoor.IsWindowEnabled() == FALSE)
				m_btUnlockFrontDoor.EnableWindow(TRUE);

			if(m_btUnlockSideDoor.IsWindowEnabled() == FALSE)
				m_btUnlockSideDoor.EnableWindow(TRUE);

			if(m_btUnlockBackDoor.IsWindowEnabled() == FALSE)
				m_btUnlockBackDoor.EnableWindow(TRUE);
		}

		if(G_MotInfo.m_pMotState->m_IO_OutA.Out00_IndexerFrontDoorRelease == 1 && m_btUnlockFrontDoor.GetCheck() == false)
		{
			m_btUnlockFrontDoor.SetWindowText("Front Door Lock");
			m_btUnlockFrontDoor.SetCheck(true);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In04_Sn_IDXFrontDoorLock == 0 && 
			m_btUnlockFrontDoor.GetCheck() == true)
		{
			m_btUnlockFrontDoor.SetWindowText("Front Door Unlock");
			m_btUnlockFrontDoor.SetCheck(false);
		}
		if(G_MotInfo.m_pMotState->m_IO_OutA.Out01_SideDoorAllRelease == 1 && m_btUnlockSideDoor.GetCheck() == false)
		{
			m_btUnlockSideDoor.SetWindowText("Side Door Lock");
			m_btUnlockSideDoor.SetCheck(true);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In08_Sn_IDXSideDoorLeftLock == 0 && 
			G_MotInfo.m_pMotState->stNormalStatus.In10_Sn_IDXSideDoorRightLock == 0  && 
			G_MotInfo.m_pMotState->stNormalStatus.In12_Sn_InspSideDoorLeftLock == 0  && 
			G_MotInfo.m_pMotState->stNormalStatus.In14_Sn_InspSideDoorRightLock == 0  && 
			m_btUnlockSideDoor.GetCheck() == true)
		{
			m_btUnlockSideDoor.SetWindowText("Side Door Unlock");
			m_btUnlockSideDoor.SetCheck(false);
		}

		if(G_MotInfo.m_pMotState->m_IO_OutA.Out02_InspectorBackDoorRelease == 1 && m_btUnlockBackDoor.GetCheck() == false) 
		{
			m_btUnlockBackDoor.SetWindowText("Back Door Lock");
			m_btUnlockBackDoor.SetCheck(true);
		}
		else if(G_MotInfo.m_pMotState->stNormalStatus.In16_Sn_InspBackDoorLeftLock == 0 && 
			G_MotInfo.m_pMotState->stNormalStatus.In18_Sn_InspBackDoorRightLock == 0  && 
			m_btUnlockBackDoor.GetCheck() == true)
		{
			m_btUnlockBackDoor.SetWindowText("Back Door Unlock");
			m_btUnlockBackDoor.SetCheck(false);
		}




		if(m_stKeyState.m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In29_TeachMoveEnable && 
			G_MotInfo.m_pMotState->stNormalStatus.In29_TeachMoveEnable == 0)
		{
			m_stEnableKeyOnOff.m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In29_TeachMoveEnable;
			m_stEnableKeyOnOff.SetWindowText("Enable Key Off");
			m_stEnableKeyOnOff.SetBkColor(G_Color_Off);
		}
		if(m_stKeyState.m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In29_TeachMoveEnable && 
			G_MotInfo.m_pMotState->stNormalStatus.In29_TeachMoveEnable == 1)
		{
			m_stEnableKeyOnOff.m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In29_TeachMoveEnable;
			m_stEnableKeyOnOff.SetWindowText("Enable Key On");
			m_stEnableKeyOnOff.SetBkColor(G_Color_On);
		}

		if(m_stTeachKeyLock.m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out03_TeachKeyUnlock && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out03_TeachKeyUnlock == 0)
		{
			m_stTeachKeyLock.m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out03_TeachKeyUnlock;
			m_stTeachKeyLock.SetWindowText("Lock");
			m_stTeachKeyLock.SetBkColor(G_Color_Off);
		}
		if(m_stTeachKeyLock.m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out03_TeachKeyUnlock && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out03_TeachKeyUnlock == 1)
		{
			m_stTeachKeyLock.m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out03_TeachKeyUnlock;
			m_stTeachKeyLock.SetWindowText("Unlock");
			m_stTeachKeyLock.SetBkColor(G_Color_On);
		}
		
		

		if(m_stKeyState.m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode && 
			G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode == 1)
		{
			m_stKeyState.m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode;

			m_stKeyState.SetWindowText("Key: Auto Mode");
			m_stKeyState.SetBlinkTextColors(RGB(255,0,0), RGB(0,0,255));
			m_stKeyState.SetBlinkBkColors(RGB(0, 255, 0), RGB(0,255,0));
			m_stKeyState.StartBkBlink(TRUE, CColorStaticST::ST_FLS_FAST);	
			m_stKeyState.StartTextBlink(TRUE, CColorStaticST::ST_FLS_FAST);
			m_stKeyState.EnableNotify(this, WM_USER + 10);
		}
		if(m_stKeyState.m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode&& 
			G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode == 0)
		{
			m_stKeyState.m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In19_AutoTeachMode;

			m_stKeyState.SetWindowText("Key : Teach Mode");
			m_stKeyState.SetBlinkTextColors(RGB(255,0,0), RGB(0,0,255));
			m_stKeyState.SetBlinkBkColors(RGB(0, 0, 255), RGB(255,0,0));
			m_stKeyState.StartBkBlink(TRUE, CColorStaticST::ST_FLS_FAST);	
			m_stKeyState.StartTextBlink(TRUE, CColorStaticST::ST_FLS_FAST);
			m_stKeyState.EnableNotify(this, WM_USER + 10);
		}


		//1단
		if (G_MotInfo.m_pMotState->stNormalStatus.In24_SystemOn == 1 && m_btSystemOnOff.m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In24_SystemOn )
		{
			m_btSystemOnOff.m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In24_SystemOn;
			m_btSystemOnOff.SetRoundButtonStyle(&m_tStyleOn);
			m_btSystemOnOff.SetWindowText("Sys 0n");
		}
		if (G_MotInfo.m_pMotState->stNormalStatus.In24_SystemOn == 0 && m_btSystemOnOff.m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In24_SystemOn )
		{
			m_btSystemOnOff.m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In24_SystemOn;
			m_btSystemOnOff.SetRoundButtonStyle(&m_tStyleOff);
			m_btSystemOnOff.SetWindowText("Sys Off");
		}

		if (G_MotInfo.m_pMotState->stNormalStatus.In00_EMO1 == 1 && 
			m_btEMOState[0].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In00_EMO1 )
		{
			m_btEMOState[0].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In00_EMO1;
			m_btEMOState[0].SetRoundButtonStyle(&m_tStyleOn);
			m_btEMOState[0].SetWindowText("1");
		}
		if (G_MotInfo.m_pMotState->stNormalStatus.In01_EMO2 == 1 && 
			m_btEMOState[1].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In01_EMO2 )
		{
			m_btEMOState[1].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In01_EMO2;
			m_btEMOState[1].SetRoundButtonStyle(&m_tStyleOn);
			m_btEMOState[1].SetWindowText("2");
		}
		if (G_MotInfo.m_pMotState->stNormalStatus.In02_EMO3 == 1 && 
			m_btEMOState[2].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In02_EMO3 )
		{
			m_btEMOState[2].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In02_EMO3;
			m_btEMOState[2].SetRoundButtonStyle(&m_tStyleOn);
			m_btEMOState[2].SetWindowText("3");
		}
		if (G_MotInfo.m_pMotState->stNormalStatus.In28_EMO4 == 1 && 
			m_btEMOState[3].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In28_EMO4 )
		{
			m_btEMOState[3].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In28_EMO4;
			m_btEMOState[3].SetRoundButtonStyle(&m_tStyleOn);
			m_btEMOState[3].SetWindowText("4");
		}

		if (G_MotInfo.m_pMotState->stNormalStatus.In00_EMO1 == 0 && 
			m_btEMOState[0].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In00_EMO1 )
		{
			m_btEMOState[0].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In00_EMO1;
			m_btEMOState[0].SetRoundButtonStyle(&m_tStyleOff);
			m_btEMOState[0].SetWindowText("1");
		}
		if (G_MotInfo.m_pMotState->stNormalStatus.In01_EMO2 == 0 && 
			m_btEMOState[1].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In01_EMO2 )
		{
			m_btEMOState[1].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In01_EMO2;
			m_btEMOState[1].SetRoundButtonStyle(&m_tStyleOff);
			m_btEMOState[1].SetWindowText("2");
		}
		if (G_MotInfo.m_pMotState->stNormalStatus.In02_EMO3 == 0 && 
			m_btEMOState[2].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In02_EMO3 )
		{
			m_btEMOState[2].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In02_EMO3;
			m_btEMOState[2].SetRoundButtonStyle(&m_tStyleOff);
			m_btEMOState[2].SetWindowText("3");
		}
		if (G_MotInfo.m_pMotState->stNormalStatus.In28_EMO4 == 0 && 
			m_btEMOState[3].m_SettingFlag != G_MotInfo.m_pMotState->stNormalStatus.In28_EMO4 )
		{
			m_btEMOState[3].m_SettingFlag = G_MotInfo.m_pMotState->stNormalStatus.In28_EMO4;
			m_btEMOState[3].SetRoundButtonStyle(&m_tStyleOff);
			m_btEMOState[3].SetWindowText("4");
		}
		
		//2015.01.15 GJJ S
		//PC Rack Alarm
		if (G_MotInfo.m_pMotState->stNormalStatus.In22_PCRackTempAlarm1 == 1)
		{	
			G_AlarmAdd(AL_1122);//PCBOX Temp Error Alarm
		}


		if (G_MotInfo.m_pMotState->stNormalStatus.In23_PCRackTempAlarm2 == 1)
		{
			G_AlarmAdd(AL_1120);//PCBOX Temp Error Alarm
		}
		

		//Elec Box Alarm
		if (G_MotInfo.m_pMotState->stNormalStatus.In20_ElectronicBoxTempAlarm1 == 1)
		{
			G_AlarmAdd(AL_1121);//{ false, 	AL_1121,2,ALARM_LIGHT ,"ELBOX Temp Worrying"},
		}


		if (G_MotInfo.m_pMotState->stNormalStatus.In21_ElectronicBoxTempAlarm2 == 1)
		{
			G_AlarmAdd(AL_1119);//{ false, 	AL_1119,2,ALARM_HEAVY ,"ELBOX Temp Error Alarm"},
		}
		//2015.01.15 GJJ E

		if(m_stStickDetect[0].m_SettingFlag != G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection  && 
			G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 0)
		{
			m_stStickDetect[0].m_SettingFlag = G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection;
			
			m_stStickDetect[0].SetBkColor(G_Color_Off);
			m_stStickDetect[0].SetWindowText("Left");
		}
		if(m_stStickDetect[0].m_SettingFlag != G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection  && 
			G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1)
		{
			m_stStickDetect[0].m_SettingFlag = G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection;
			m_stStickDetect[0].SetBkColor(G_Color_On);
			m_stStickDetect[0].SetWindowText("Left");
		}

		if(m_stStickDetect[1].m_SettingFlag != G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection  && 
			G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 0)
		{
			m_stStickDetect[1].m_SettingFlag = G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection;

			m_stStickDetect[1].SetBkColor(G_Color_Off);
			m_stStickDetect[1].SetWindowText("Right");
		}
		if(m_stStickDetect[1].m_SettingFlag != G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection  && 
			G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1)
		{
			m_stStickDetect[1].m_SettingFlag = G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection;
			m_stStickDetect[1].SetBkColor(G_Color_On);
			m_stStickDetect[1].SetWindowText("Right");
		}


		//G_MotInfo.m_pMotState->m_IO_OutA.Out08_SignalTower_Red = rand()%2>0?1:0;
		//G_MotInfo.m_pMotState->m_IO_OutA.Out09_SignalTower_Yellow = rand()%2>0?1:0;
		//G_MotInfo.m_pMotState->m_IO_OutA.Out10_SignalTower_Green = rand()%2>0?1:0;
		//G_MotInfo.m_pMotState->m_IO_OutA.Out11_Buzzer1 = rand()%2>0?1:0;

		if(m_stSignalTower[0].m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out08_SignalTower_Red  && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out08_SignalTower_Red == 0)
		{
			m_stSignalTower[0].m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out08_SignalTower_Red;
			m_stSignalTower[0].SetBkColor(G_CSignalTower_Red_OFF);
			m_stSignalTower[0].SetWindowText("Red");
		}
		if(m_stSignalTower[0].m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out08_SignalTower_Red  && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out08_SignalTower_Red == 1)
		{
			m_stSignalTower[0].m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out08_SignalTower_Red;
			m_stSignalTower[0].SetBkColor(G_CSignalTower_Red_ON);
			m_stSignalTower[0].SetWindowText("Red");
		}
		//////////////////////////////////////////////////////////////////////////
		if(m_stSignalTower[1].m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out09_SignalTower_Yellow  && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out09_SignalTower_Yellow == 0)
		{
			m_stSignalTower[1].m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out09_SignalTower_Yellow;
			m_stSignalTower[1].SetBkColor(G_CSignalTower_Yellow_OFF);
			m_stSignalTower[1].SetWindowText("Yellow");
		}
		if(m_stSignalTower[1].m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out09_SignalTower_Yellow  && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out09_SignalTower_Yellow == 1)
		{
			m_stSignalTower[1].m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out09_SignalTower_Yellow;
			m_stSignalTower[1].SetBkColor(G_CSignalTower_Yellow_ON);
			m_stSignalTower[1].SetWindowText("Yellow");
		}
		//////////////////////////////////////////////////////////////////////////
		if(m_stSignalTower[2].m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out10_SignalTower_Green  && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out10_SignalTower_Green == 0)
		{
			m_stSignalTower[2].m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out10_SignalTower_Green;
			m_stSignalTower[2].SetBkColor(G_CSignalTower_Green_OFF);
			m_stSignalTower[2].SetWindowText("Green");
		}
		if(m_stSignalTower[2].m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out10_SignalTower_Green  && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out10_SignalTower_Green == 1)
		{
			m_stSignalTower[2].m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out10_SignalTower_Green;
			m_stSignalTower[2].SetBkColor(G_CSignalTower_Green_ON);
			m_stSignalTower[2].SetWindowText("Green");
		}
		//////////////////////////////////////////////////////////////////////////
		if(m_stSignalTower[3].m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out11_Buzzer1  && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out11_Buzzer1 == 0)
		{
			m_stSignalTower[3].m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out11_Buzzer1;
			m_stSignalTower[3].SetBkColor(G_CSignalTower_Buzzer_OFF);
			m_stSignalTower[3].SetWindowText("Buz");
			m_btBuzzerStop.EnableWindow(FALSE);
		}
		if(m_stSignalTower[3].m_SettingFlag != G_MotInfo.m_pMotState->m_IO_OutA.Out11_Buzzer1  && 
			G_MotInfo.m_pMotState->m_IO_OutA.Out11_Buzzer1 == 1)
		{
			m_stSignalTower[3].m_SettingFlag = G_MotInfo.m_pMotState->m_IO_OutA.Out11_Buzzer1;
			m_stSignalTower[3].SetBkColor(G_CSignalTower_Buzzer_ON);
			m_stSignalTower[3].SetWindowText("Buz");
			m_btBuzzerStop.EnableWindow(TRUE);
		}

		switch(G_MotObj.m_TT01_NowPos)
		{
		case CMotCtrlObj::TT01_AM01_Pos:
			{
				if(m_stTT01_AM01_pos.m_SettingFlag != G_MotObj.m_TT01_NowPos)
				{
					m_stTT01_AM01_pos.m_SettingFlag = G_MotObj.m_TT01_NowPos;
					m_stTT01_AM01_pos.SetBkColor(G_Color_On);
					m_stTT01_TURN_pos.SetBkColor(G_Color_Off);
					m_stTT01_UT02_pos.SetBkColor(G_Color_Off);
				}
				
			}break;
		case CMotCtrlObj::TT01_Turn_Pos:
			{
				if(m_stTT01_AM01_pos.m_SettingFlag != G_MotObj.m_TT01_NowPos)
				{
					m_stTT01_AM01_pos.m_SettingFlag = G_MotObj.m_TT01_NowPos;
					m_stTT01_AM01_pos.SetBkColor(G_Color_Off);
					m_stTT01_TURN_pos.SetBkColor(G_Color_On);
					m_stTT01_UT02_pos.SetBkColor(G_Color_Off);
				}
			}break;
		case CMotCtrlObj::TT01_UT02_Pos:
			{	
				if(m_stTT01_AM01_pos.m_SettingFlag != G_MotObj.m_TT01_NowPos)
				{
					m_stTT01_AM01_pos.m_SettingFlag = G_MotObj.m_TT01_NowPos;
					m_stTT01_AM01_pos.SetBkColor(G_Color_Off);
					m_stTT01_TURN_pos.SetBkColor(G_Color_Off);
					m_stTT01_UT02_pos.SetBkColor(G_Color_On);
				}
			}break;
		default:
			{
				if(m_stTT01_AM01_pos.m_SettingFlag != G_MotObj.m_TT01_NowPos)
				{
					m_stTT01_AM01_pos.m_SettingFlag = G_MotObj.m_TT01_NowPos;
					m_stTT01_AM01_pos.SetBkColor(G_Color_Off);
					m_stTT01_TURN_pos.SetBkColor(G_Color_Off);
					m_stTT01_UT02_pos.SetBkColor(G_Color_Off);
				}
			}break;
		}

		if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1 || 
			G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1)
		{
			if(m_btSeqStart[1].IsWindowVisible() == TRUE)
			{
				m_btSeqStart[1].ShowWindow(SW_HIDE);
			}
			for(int i = 2; i<8; i++)
			{
				if(m_btSeqStart[i].IsWindowVisible() == FALSE)
					m_btSeqStart[i].ShowWindow(SW_SHOW);
			}
			if(m_btSeqStart[9].IsWindowVisible() == FALSE)
				m_btSeqStart[9].ShowWindow(SW_SHOW);
			//for(int i = 2; i<8; i++)
			//{
			//	if(m_btSeqStart[i].IsWindowVisible() == FALSE && i != 6)
			//		m_btSeqStart[i].ShowWindow(SW_SHOW);
			//}

			////Manual의 Auto Run은 Host로 부터 PPID를 받아야 한다.
			//if(G_NowEQRunMode == Master_MANUAL && 
			//	(G_pCIMInterface->m_bRecvJobStartCMD == false || G_pCIMInterface->m_bRecvMaterialData == false))
			//{
			//	if(m_btSeqStart[6].IsWindowVisible() == FALSE)
			//		m_btSeqStart[6].ShowWindow(SW_SHOW);
			//}
			//else
			//{
			//	if(m_btSeqStart[6].IsWindowVisible() == TRUE)
			//		m_btSeqStart[6].ShowWindow(SW_HIDE);
			//}

			
		}
		else
		{
			if(m_btSeqStart[1].IsWindowVisible() == FALSE)
			{
				m_btSeqStart[1].ShowWindow(SW_SHOW);
			}
			for(int i = 2; i<8; i++)
			{
				if(m_btSeqStart[i].IsWindowVisible() == TRUE)
					m_btSeqStart[i].ShowWindow(SW_HIDE);
			}
			if(m_btSeqStart[9].IsWindowVisible() == TRUE)
				m_btSeqStart[9].ShowWindow(SW_HIDE);
		}


		CString strCamMsg;
		ReadCntCamState++;//500ms마다 읽기. 그럼 1초마다 갱신
		if((m_SeqObj_InsMic.m_NowStep < CSeq_Inspect_Mic::SEQ_STEP_RUN && ReadCntCamState == 10 )&&
			(m_SeqObj_InsMac.m_NowStep < CSeq_Inspect_Mac::SEQ_STEP_RUN && ReadCntCamState == 10 ))
		{
			if(ReadInspectCamState == true)
			{
				strCamMsg.Format("%s", "VV");  //Volt
				m_ServerInterface.m_fnSendCommand_Nrs(TASK_21_Inspector, nBiz_AOI_System, nBiz_Send_CamCommand, 0, strCamMsg.GetLength(), (UCHAR*)strCamMsg.GetBuffer(strCamMsg.GetLength()));
				ReadInspectCamState = false;
			}
			else
			{
				strCamMsg.Format("%s", "VT");  //Temp
				m_ServerInterface.m_fnSendCommand_Nrs(TASK_21_Inspector, nBiz_AOI_System, nBiz_Send_CamCommand, 0, strCamMsg.GetLength(), (UCHAR*)strCamMsg.GetBuffer(strCamMsg.GetLength()));
				ReadInspectCamState = true;
			}

			ReadCntCamState = 0;
		}



	}

	if(TIMER_TASK_STATE_UPDATE == nIDEvent)
	{

		//Task Alive Check
		m_ServerInterface.m_fnSendCommand_Nrs(TASK_21_Inspector, nBiz_Func_System, nBiz_Seq_Alive_Question, nBiz_Unit_Zero);
		m_TaskLinkCheck.m_TaskCheckCnt_Inspector++;
		m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_System, nBiz_Seq_Alive_Question, nBiz_Unit_Zero);
		m_TaskLinkCheck.m_TaskCheckCnt_Indexer++;
		m_ServerInterface.m_fnSendCommand_Nrs(TASK_24_Motion, nBiz_Func_System, nBiz_Seq_Alive_Question, nBiz_Unit_Zero);
		m_TaskLinkCheck.m_TaskCheckCnt_Motion++;
		m_ServerInterface.m_fnSendCommand_Nrs(TASK_31_Mesure3D, nBiz_Func_System, nBiz_Seq_Alive_Question, nBiz_Unit_Zero);
		m_TaskLinkCheck.m_TaskCheckCnt_Mesure3D++;
		m_ServerInterface.m_fnSendCommand_Nrs(TASK_32_Mesure2D, nBiz_Func_System, nBiz_Seq_Alive_Question, nBiz_Unit_Zero);
		m_TaskLinkCheck.m_TaskCheckCnt_Area3DScan++;
		m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_System, nBiz_Seq_Alive_Question, nBiz_Unit_Zero);
		m_TaskLinkCheck.m_TaskCheckCnt_AutoFocus++;

		CString strNewData;
		strNewData.Format("Inspector(%d)", m_TaskLinkCheck.m_TaskCheckCnt_Inspector);
		GetDlgItem(IDC_ST_INS1_STATUS1)->SetWindowText(strNewData);
		strNewData.Format("Indexer(%d)", m_TaskLinkCheck.m_TaskCheckCnt_Indexer);
		GetDlgItem(IDC_ST_INS1_STATUS2)->SetWindowText(strNewData);
		strNewData.Format("Measure(%d)", m_TaskLinkCheck.m_TaskCheckCnt_Mesure3D);
		GetDlgItem(IDC_ST_INS1_STATUS3)->SetWindowText(strNewData);
		strNewData.Format("Motion(%d)", m_TaskLinkCheck.m_TaskCheckCnt_Motion);
		GetDlgItem(IDC_ST_INS1_STATUS4)->SetWindowText(strNewData);
		strNewData.Format("AF(%d)", m_TaskLinkCheck.m_TaskCheckCnt_AutoFocus);
		GetDlgItem(IDC_ST_INS1_STATUS5)->SetWindowText(strNewData);
		strNewData.Format("3D Scan(%d)", m_TaskLinkCheck.m_TaskCheckCnt_Area3DScan);
		GetDlgItem(IDC_ST_INS1_STATUS6)->SetWindowText(strNewData);
		//TT01의 위치를 물어 보자.(3초에 한번정도?) ㅎㅎ
		m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_TT01_Pos_Req, nBiz_Unit_Zero);


		if(m_TaskLinkCheck.m_TaskCheckCnt_Inspector>2 && G_VS21TASK_STATE != TASK_STATE_NONE)
		{
			G_TaskStateChange(this, &m_stTaskState[0], TASK_STATE_NONE);
			G_VS21TASK_STATE = TASK_STATE_NONE;
		}
		if(m_TaskLinkCheck.m_TaskCheckCnt_Indexer>2 && G_VS11TASK_STATE != TASK_STATE_NONE)
		{
			G_TaskStateChange(this, &m_stTaskState[1], TASK_STATE_NONE);
			G_VS11TASK_STATE = TASK_STATE_NONE;
		}
		if(m_TaskLinkCheck.m_TaskCheckCnt_Mesure3D>2 && G_VS31TASK_STATE != TASK_STATE_NONE)
		{
			G_TaskStateChange(this, &m_stTaskState[2], TASK_STATE_NONE);
			G_VS31TASK_STATE = TASK_STATE_NONE;
		}
		if(m_TaskLinkCheck.m_TaskCheckCnt_Motion>2 && G_VS24TASK_STATE != TASK_STATE_NONE)
		{
			G_TaskStateChange(this, &m_stTaskState[3], TASK_STATE_NONE);
			G_VS24TASK_STATE = TASK_STATE_NONE;
		}
		if(m_TaskLinkCheck.m_TaskCheckCnt_AutoFocus>2 && G_VS13TASK_STATE != TASK_STATE_NONE)
		{
			G_TaskStateChange(this, &m_stTaskState[4], TASK_STATE_NONE);
			G_VS13TASK_STATE = TASK_STATE_NONE;
		}
		if(m_TaskLinkCheck.m_TaskCheckCnt_Area3DScan>2 && G_VS32TASK_STATE != TASK_STATE_NONE)
		{
			G_TaskStateChange(this, &m_stTaskState[5], TASK_STATE_NONE);
			G_VS32TASK_STATE = TASK_STATE_NONE;
		}

		//Air Press Value
		G_MotObj.m_fnAIRRead();
	}

	if(TIMER_TIME_UPDATE == nIDEvent)
	{

		CString NowTime;
		CTime mNowTime;
		mNowTime = CTime::GetCurrentTime();

		NowTime.Format("%04d/%02d/%02d - %02d:%02d:%02d",mNowTime.GetYear(), mNowTime.GetMonth(), mNowTime.GetDay(), mNowTime.GetHour(), mNowTime.GetMinute(), mNowTime.GetSecond());
		GetDlgItem(IDC_ST_EQP_TIME)->SetWindowText(NowTime);
	}
	__super::OnTimer(nIDEvent);
}


void CVS10MasterDlg::OnBnClickedBtLiveViewOnoff()
{
	if(m_bLiveViewOnOff.GetCheck() == false)
	{
		m_bLiveViewOnOff.SetWindowText("Live Off");
		m_LiveViewObj.LiveView(true);
		m_bLiveViewOnOff.SetCheck(true);

		G_WriteInfo(Log_Check, "Review Live View On");

		m_ThetaAlignLiveObj.SetCamLiveMode(LIVE_MODE_GRAB);
	}
	else
	{
		m_bLiveViewOnOff.SetWindowText("Live On");
		m_LiveViewObj.LiveView(false);
		m_bLiveViewOnOff.SetCheck(false);
		G_WriteInfo(Log_Check, "Review Live View Off");

		m_ThetaAlignLiveObj.SetCamLiveMode(LIVE_MODE_VIEW);
	}
}

void CVS10MasterDlg::OnBnClickedBtMainRecipe()
{
	//CPassWord InputData;
	//if(InputData.DoModal() != IDOK)
	//	return;

	m_pStickReicpeView->ShowWindow(SW_SHOW);
	//m_pSysParamView->ShowWindow(SW_HIDE);
	//m_pMaintenanceView->ShowWindow(SW_HIDE);

	m_pStickReicpeView->CenterWindow();

	//m_btRecipeSetting.SetCheck(true);
	//m_btSysParameter.SetCheck(false);
	//m_btMaintenance.SetCheck(false);
}
void CVS10MasterDlg::OnBnClickedBtSystemRecipeView()
{
	//CPassWord InputData;
	//if(InputData.DoModal() != IDOK)
	//	return;

	//m_pStickReicpeView->ShowWindow(SW_HIDE);
	m_pSysParamView->ShowWindow(SW_SHOW);
	//m_pMaintenanceView->ShowWindow(SW_HIDE);
	m_pSysParamView->CenterWindow();

	//m_btRecipeSetting.SetCheck(false);
	//m_btSysParameter.SetCheck(true);
	//m_btMaintenance.SetCheck(false);
}


void CVS10MasterDlg::OnBnClickedBtMainMaint()
{
	//CPassWord InputData;
	//if(InputData.DoModal() != IDOK)
	//	return;

	//m_pStickReicpeView->ShowWindow(SW_HIDE);
	//m_pSysParamView->ShowWindow(SW_HIDE);
	m_pMaintenanceView->ShowWindow(SW_SHOW);
	m_pMaintenanceView->CenterWindow();
	m_LiveViewObj.m_bDisplayInterlock =  false;

	CRect ViewPos;
	ViewPos =  m_MapViewArea;
	ViewPos.top = ViewPos.top+65;
	ViewPos.left = ViewPos.left +10;
	ViewPos.right = ViewPos.left +355;
	ViewPos.bottom = ViewPos.top +960;//55;
	m_pMaintenanceView->MoveWindow(ViewPos);

	//m_btRecipeSetting.SetCheck(false);
	//m_btSysParameter.SetCheck(false);
	//m_btMaintenance.SetCheck(true);
}

void CVS10MasterDlg::OnBnClickedBtCimInterface()
{
	m_SubCIMInterfaceView->ShowWindow(SW_SHOW);
}

void CVS10MasterDlg::OnBnClickedBtAlarmView()
{
	m_SubAlarmClearView->ShowWindow(SW_SHOW);
}

LRESULT CVS10MasterDlg::OnRecipeChange(WPARAM wParam, LPARAM lParam)
{

	if(lParam == 0)//Recipe 변경.
	{
		CString strNowActiveRecipe;
		char strReadData[256];
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		strNowActiveRecipe.Format("%s", strReadData);
		//Host 보고
		CString strSendCim;
		strSendCim.Format("%s,%d",strNowActiveRecipe, BY_OP);
		G_pCIMInterface->m_fnSendToCim(SEND_PPID_CURRENT_REQUEST_ACK, DATA1_OK, strSendCim);

		LoadNowProcRecipe();
	}
	if(lParam == 1)//System Param 변경.
	{
		LoadHWParam();
		LoadNowProcRecipe();
	}

	if(lParam == 2)//Align 좌표 변경.
		LoadNowProcRecipe(true);


	//////////////////////////////////////////////////////////////////////////
	if(lParam == 101)//Review Lens Calc Start
	{
		//모두 정지.
		if(CheckAllRunSequence() == true)//Sequenc가 돌고 있으면.. 구동 하지 않느다.
		{//모든 구동 Sequence를 정지 한다.
			//m_SeqObj_StickExchange.fnSeqSetStop();
			m_SeqObj_ThetaAlign.fnSeqSetStop();
			m_SeqObj_CellAlign.fnSeqSetStop();
			//m_SeqObj_ReviewCalc.fnSeqSetStop();
			m_SeqObj_CDTP.fnSeqSetStop();
			m_SeqObj_3DScope.fnSeqSetStop();
			m_SeqObj_AreaScan3D.fnSeqSetStop();
			m_SeqObj_InsAutoReview.fnSeqSetStop();
			m_SeqObj_InsMic.fnSeqSetStop();
			m_SeqObj_InsMac.fnSeqSetStop();
			m_SeqObj_AutoRun.fnSeqSetStop();
			m_SeqObj_InitAM01.fnSeqSetStop();
			G_MotObj.m_fnAM01MotAllStop();
		}

		m_SeqObj_ReviewCalc.fnSeqFlowStart();
	}
	if(lParam == 102)//Review Lens Calc Stop
	{
		m_SeqObj_ReviewCalc.fnSeqSetStop();
	}

	if(lParam == 9919)//Review m_ViewDrawScanImg
	{
		m_p3DMapViewer->m_3DViewObj.m_ViewDrawScanImg = false;
	}
	//if(lParam == 9929)//Write Measure All Result();
	//{
	//	WriteMeasureAllResult();
	//}

	if(lParam == 7751)//Now  =>Manual Select.
	{
		if(m_btChangeAutoMode.GetCheck() == false && 
			m_btChangeManualMode.GetCheck() == false &&
			m_btChangePMMode.GetCheck() == false)
		{
			OnBnClickedBtEqManualMode();
		}
	}
	
	return 0;
}
LRESULT CVS10MasterDlg::OnHostMessage(WPARAM wParam, LPARAM lParam)
{
	if(m_pHostMsgViewDlg != nullptr)
	{
		m_pHostMsgViewDlg->SetMessageData((int)wParam, (char*)lParam);
		m_pHostMsgViewDlg->ShowWindow(SW_SHOW);
	}
	
	return 0;
}

void CVS10MasterDlg::SetResult_Folder(CString strStickID)
{
	G_WriteInfo(Log_Check, "Start SetResult_Folder(%s)", strStickID);
	//////////////////////////////////////////////////////////////////////////
	//Result 경로 생성.
	CString NewPath;
	NewPath.Format("%s%s\\", MasterToCIM_Result_PATH_Ret, strStickID.GetBuffer(strStickID.GetLength()));
	G_fnCheckDirAndCreate(NewPath.GetBuffer(NewPath.GetLength()));
	G_ClearFolder(NewPath);//이전것을 지운다.

	NewPath.Format("%s%s\\%s\\", MasterToCIM_Result_PATH_Ret, strStickID.GetBuffer(strStickID.GetLength()),MasterToCIM_RName_IMAGE);
	G_fnCheckDirAndCreate(NewPath.GetBuffer(NewPath.GetLength()));

	G_WriteInfo(Log_Check, "End SetResult_Folder(%s)", strStickID);
}
void CVS10MasterDlg::CopyResult_Folder(CString strStickID)
{
	G_WriteInfo(Log_Check, "Start CopyResult_Folder(%s)", strStickID);
	//////////////////////////////////////////////////////////////////////////
	//Result 경로 생성.
	CString strResultPath;
	strResultPath.Format("%s%s\\", MasterToCIM_Result_PATH_Ret, strStickID.GetBuffer(strStickID.GetLength()));
	G_fnCheckDirAndCreate(strResultPath.GetBuffer(strResultPath.GetLength()));

	CString strImgPath;
	strImgPath.Format("%s%s\\%s\\", MasterToCIM_Result_PATH_Ret, strStickID.GetBuffer(strStickID.GetLength()),MasterToCIM_RName_IMAGE);
	G_fnCheckDirAndCreate(strImgPath.GetBuffer(strImgPath.GetLength()));
	
	//1)Result Data And Index File은 지정경로에 생성된다.
	CString strFromPath;
	//2)굴곡 측정 Image저장
	strFromPath.Format("%s\\%s\\", VS31Mesure3D_Result_PATH, "VS32Measure2D");
	G_WriteInfo(Log_Check, "1)Area Scan ==>From (%s)", strFromPath);
	G_WriteInfo(Log_Check, "1)Area Scan ==>To (%s)", strImgPath);
	if(FileCopyFromAToB_InFolder(strFromPath, strImgPath, CString()/*CString(".jpg")*/) == false)
	{
		G_WriteInfo(Log_Error, "굴곡(WAVE) 측정 Image Data Copy Error!");
		return;
	}
	//G_ClearFolder(strFromPath);//이전것을 지운다.
	//3)둔턱 측정 Image저장
	strFromPath.Format("%s\\%s\\", VS31Mesure3D_Result_PATH, "VS31Measure3D");
	G_WriteInfo(Log_Check, "2)Scope ==>From (%s)", strFromPath);
	G_WriteInfo(Log_Check, "2)Scope ==>To (%s)", strImgPath);
	if(FileCopyFromAToB_InFolder(strFromPath, strImgPath, CString()/*CString(".jpg")*/) == false)
	{
		G_WriteInfo(Log_Error, "DOONTUK 측정 Vk4 Data Copy Error!");
		return;
	}
	strFromPath.Format("%s\\%s\\%s\\", VS31Mesure3D_Result_PATH, "VS31Measure3D", "AnalysisData");
	G_WriteInfo(Log_Check, "2)Scope Image ==>From (%s)", strFromPath);
	G_WriteInfo(Log_Check, "2)Scope Image ==>To (%s)", strImgPath);
	if(FileCopyFromAToB_InFolder(strFromPath, strImgPath, CString()/*CString(".jpg")*/) == false)
	{
		G_WriteInfo(Log_Error, "DOONTUK 측정 Result Image Data Copy Error!");
		return;
	}
	//G_ClearFolder(strFromPath);//이전것을 지운다.
	//4)CD,TP 측정 Image저장
	strFromPath.Format("%s\\%s_CDTP\\", VS10Master_Result_PATH, strStickID);
	G_WriteInfo(Log_Check, "3)CD,TP ==>From (%s)", strFromPath);
	G_WriteInfo(Log_Check, "3)CD,TP ==>To (%s)", strImgPath);
	if(FileCopyFromAToB_InFolder(strFromPath, strImgPath, CString()/*CString(".jpg")*/) == false)
	{
		G_WriteInfo(Log_Error, "CDTP 측정 Image Data Copy Error!");
		return;
	}
	//G_ClearFolder(strFromPath);//이전것을 지운다.
	//4)Auto Review 측정 Image저장
	strFromPath.Format("%s\\%s_AReviewMic\\", VS10Master_Result_PATH, strStickID);
	G_WriteInfo(Log_Check, "4)AutoReview Micro ==>From (%s)", strFromPath);
	G_WriteInfo(Log_Check, "4)AutoReview Micro ==>To (%s)", strImgPath);
	if(FileCopyFromAToB_InFolder(strFromPath, strImgPath, CString()/*CString(".jpg")*/) == false)
	{
		G_WriteInfo(Log_Error, "ReviewMic 측정 Image Data Copy Error!");
		return;
	}
	strFromPath.Format("%s\\%s_AReviewMac\\", VS10Master_Result_PATH, strStickID);
	G_WriteInfo(Log_Check, "4)AutoReview Macro ==>From (%s)", strFromPath);
	G_WriteInfo(Log_Check, "4)AutoReview Macro ==>To (%s)", strImgPath);
	if(FileCopyFromAToB_InFolder(strFromPath, strImgPath, CString()/*CString(".jpg")*/) == false)
	{
		G_WriteInfo(Log_Error, "ReviewMac 측정 Image Data Copy Error!");
		return;
	}
	CString strImgPathFull;
	strImgPathFull.Format("%sMacroImage.bmp", strImgPath);
	strFromPath.Format("%s\\MacroImage.bmp", VS10Master_Result_PATH);
	G_WriteInfo(Log_Check, "5)Macro Bmp ==>From (%s)", strFromPath);
	G_WriteInfo(Log_Check, "5)Macro Bmp ==>To (%s)", strImgPathFull);

	
	if(CopyFile(strFromPath, strImgPathFull,FALSE) == FALSE)
	{
		G_WriteInfo(Log_Error, "sMacroImage Image Data Copy Error!");
		return;
	}
	//G_ClearFolder(strFromPath);//이전것을 지운다.
	G_WriteInfo(Log_Check, "End CopyResult_Folder(%s)", strStickID);
}

LRESULT CVS10MasterDlg::OnWriteInspectResult(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case Write_None:
		{

		}break;
	case Write_Reset:
		{

		}break;
	case Write_ResultAll:
		{
			WriteMeasureAllResult();
		}break;
	case Write_SetStartTime:
		{
			char strReadData[256];
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			SetResult_Folder(CString(strReadData));
			CTime tCurrentTime;
			tCurrentTime = CTime::GetCurrentTime();
			m_CIM_Inspection_StartTime.Format("%s", tCurrentTime.Format("%Y%m%d%H%M%S"));
		}break;
	case Write_SetEndTime:
		{
			CTime tCurrentTime;
			tCurrentTime = CTime::GetCurrentTime();
			m_CIM_Inspection_EndTime.Format("%s", tCurrentTime.Format("%Y%m%d%H%M%S"));
		}break;
	default:
		break;
	}
	return 0;
}



void CVS10MasterDlg::OnBnClickedBt3dLensId1()
{
	if(m_btAutoFocusInit.GetCheck() == true)
	{
		G_WriteInfo(Log_Worrying, "AF Init Now~!!!");
		return;
	}
	m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, nBiz_Seq_Lens1, nBiz_Unit_Zero);
}


void CVS10MasterDlg::OnBnClickedBt3dLensId2()
{
	if(m_btAutoFocusInit.GetCheck() == true)
	{
		G_WriteInfo(Log_Worrying, "AF Init Now~!!!");
		return;
	}
	m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, nBiz_Seq_Lens2, nBiz_Unit_Zero);
}


void CVS10MasterDlg::OnBnClickedBt3dLensId3()
{
	if(m_btAutoFocusInit.GetCheck() == true)
	{
		G_WriteInfo(Log_Worrying, "AF Init Now~!!!");
		return;
	}
	m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_ReviewLens, nBiz_Seq_Lens3, nBiz_Unit_Zero);
}


void CVS10MasterDlg::OnBnClickedBtnReviewLightSet()
{
	if(m_btAutoFocusInit.GetCheck() == true)
	{
		G_WriteInfo(Log_Worrying, "AF Init Now~!!!");
		return;
	}
	CString strLightValue;
	m_edReviewLightValue.GetWindowText(strLightValue);
	m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_Light, nBiz_Seq_SetReviewLight, (USHORT)atoi(strLightValue));
	m_btSetRevewLightValue.SetCheck(true);
}


void CVS10MasterDlg::OnBnClickedBtAfOn()
{
	if(m_btAutoFocusInit.GetCheck() == true)
	{
		G_WriteInfo(Log_Worrying, "AF Init Now~!!!");
		return;
	}
	G_WriteInfo(Log_Worrying, "OnBnClickedBtAfOn~!!!");
	m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 1);
}


void CVS10MasterDlg::OnBnClickedBtAfOff()
{
	if(m_btAutoFocusInit.GetCheck() == true)
	{
		G_WriteInfo(Log_Worrying, "AF Init Now~!!!");
		return;
	}
	G_WriteInfo(Log_Worrying, "OnBnClickedBtAfOff~!!!");
	m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 0);
}


void CVS10MasterDlg::OnBnClickedChCrossOn()
{
	if( IsDlgButtonChecked(IDC_CH_CROSS_ON) ==BST_CHECKED)

		m_LiveViewObj.DrawCenterLine(true);
	else
	{
		if( IsDlgButtonChecked(IDC_CH_ROLLER_ON) ==BST_CHECKED)
		{
			m_LiveViewObj.ChangeRuler(false);
			CheckDlgButton(IDC_CH_ROLLER_ON, BST_UNCHECKED);
		}

		m_LiveViewObj.DrawCenterLine(false);

	}
	m_LiveViewObj.DrawReviewCalc(false);
	CheckDlgButton(IDC_CH_REVEW_CALC, BST_UNCHECKED);
}

void CVS10MasterDlg::OnBnClickedChRevewCalc()
{
	if( IsDlgButtonChecked(IDC_CH_REVEW_CALC) ==BST_CHECKED)
	{
		m_LiveViewObj.DrawReviewCalc(true);

		m_LiveViewObj.DrawCenterLine(false);
		CheckDlgButton(IDC_CH_CROSS_ON, BST_UNCHECKED);

		m_LiveViewObj.ChangeRuler(false);
		CheckDlgButton(IDC_CH_ROLLER_ON, BST_UNCHECKED);
	}
	else
	{
		m_LiveViewObj.DrawReviewCalc(false);

		m_LiveViewObj.DrawCenterLine(true);
		CheckDlgButton(IDC_CH_CROSS_ON, BST_CHECKED);

		m_LiveViewObj.ChangeRuler(false);
		CheckDlgButton(IDC_CH_ROLLER_ON, BST_UNCHECKED);
	}
}

void CVS10MasterDlg::OnBnClickedChRollerOn()
{
	if( IsDlgButtonChecked(IDC_CH_ROLLER_ON) ==BST_CHECKED)
	{
		if(m_btReviewLens[0].GetCheck() == true)
			m_LiveViewObj.ChangeRuler(true, m_ReviewLensOffset.Lens02xPixelSizeX, m_ReviewLensOffset.Lens02xPixelSizeY, REVIEW_LENS_2X);
		else if(m_btReviewLens[1].GetCheck() == true)
			m_LiveViewObj.ChangeRuler(true, m_ReviewLensOffset.Lens10xPixelSizeX, m_ReviewLensOffset.Lens10xPixelSizeY, REVIEW_LENS_10X);
		else if(m_btReviewLens[2].GetCheck() == true)
			m_LiveViewObj.ChangeRuler(true, m_ReviewLensOffset.Lens20xPixelSizeX, m_ReviewLensOffset.Lens20xPixelSizeY, REVIEW_LENS_20X);
		else
		{
			G_WriteInfo(Log_Worrying, "No Selected Lens");
			m_LiveViewObj.ChangeRuler(false);
			CheckDlgButton(IDC_CH_ROLLER_ON, BST_UNCHECKED);
		}
	}
	else
	{
		m_LiveViewObj.ChangeRuler(false);
	}
	m_LiveViewObj.DrawReviewCalc(false);
	CheckDlgButton(IDC_CH_REVEW_CALC, BST_UNCHECKED);

}

void CVS10MasterDlg::OnBnClickedBtInitAf()
{
	if(m_btAutoFocusInit.GetCheck() ==false)
	{
		m_btAutoFocusInit.SetCheck(true);
		//m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_Init, 0);
		m_ServerInterface.m_fnSendCommand_Nrs(TASK_13_AutoFocus, nBiz_Func_AF, nBiz_Seq_AF, 3);
	}
}

void CVS10MasterDlg::OnBnClickedBtEqAutoMode()
{
	if(m_SeqObj_StickExchange.m_NowStep != CSeq_StickExchange::SEQ_STEP_IDLE )
	{
		AfxMessageBox("Now Stick Exchange ~ !!!!!");
		return;
	}
	if(m_SubCIMInterfaceView->m_PRST_Process ==  PRST_SETUP)
	{
		AfxMessageBox("Now Process Setup State\r\n\r\nInit AM01 Sequence Run");
		return;
	}
	//모두 정지.
	if(CheckAllRunSequence() == true)//Sequenc가 돌고 있으면.. 구동 하지 않느다.
	{//모든 구동 Sequence를 정지 한다.
		//m_SeqObj_StickExchange.fnSeqSetStop();
		m_SeqObj_ThetaAlign.fnSeqSetStop();
		m_SeqObj_CellAlign.fnSeqSetStop();
		m_SeqObj_ReviewCalc.fnSeqSetStop();
		m_SeqObj_CDTP.fnSeqSetStop();
		m_SeqObj_3DScope.fnSeqSetStop();
		m_SeqObj_AreaScan3D.fnSeqSetStop();
		m_SeqObj_InsAutoReview.fnSeqSetStop();
		m_SeqObj_InsMic.fnSeqSetStop();
		m_SeqObj_InsMac.fnSeqSetStop();
		m_SeqObj_AutoRun.fnSeqSetStop();
		m_SeqObj_InitAM01.fnSeqSetStop();

		G_MotObj.m_fnAM01MotAllStop();
	}

	
	//////////////////////////////////////////////////////////////////////////
	m_SubCIMInterfaceView->OnBnClickedBtEqNormal();


	for(int iStep = 0; iStep<10; iStep++)
	{
		m_btSeqStart[iStep].EnableWindow(FALSE);
	}


	//AutoRun Seq를 구동하여.. 자동 모드로 진입하자.
	m_btChangeAutoMode.SetCheck(true);
	m_btChangeManualMode.SetCheck(false);
	m_btChangePMMode.SetCheck(false);

	G_NowEQRunMode = Master_AUTO;
}

void CVS10MasterDlg::OnBnClickedBtEqManualMode()
{

	if(m_SubCIMInterfaceView->m_EQST_Mode == EQP_FAULT )
	{
		AfxMessageBox("Now EQ STATE FAULT");
		return;
	}
	if(m_SeqObj_StickExchange.m_NowStep != CSeq_StickExchange::SEQ_STEP_IDLE )
	{
		AfxMessageBox("Now Stick Exchange ~ !!!!!");
		return;
	}

	//모두 정지.
	if(CheckAllRunSequence() == true)//Sequenc가 돌고 있으면.. 구동 하지 않느다.
	{//모든 구동 Sequence를 정지 한다.
		//m_SeqObj_StickExchange.fnSeqSetStop();

		m_SeqObj_ThetaAlign.fnSeqSetStop();
		m_SeqObj_CellAlign.fnSeqSetStop();
		m_SeqObj_ReviewCalc.fnSeqSetStop();
		m_SeqObj_CDTP.fnSeqSetStop();
		m_SeqObj_3DScope.fnSeqSetStop();
		m_SeqObj_AreaScan3D.fnSeqSetStop();
		m_SeqObj_InsAutoReview.fnSeqSetStop();
		m_SeqObj_InsMic.fnSeqSetStop();
		m_SeqObj_InsMac.fnSeqSetStop();
		m_SeqObj_AutoRun.fnSeqSetStop();
		m_SeqObj_InitAM01.fnSeqSetStop();
		//////////////////////////////////////////////////////////////////////////
		G_MotObj.m_fnAM01MotAllStop();
	}
	
	//////////////////////////////////////////////////////////////////////////
	m_SubCIMInterfaceView->OnBnClickedBtEqNormal();


	for(int iStep = 0; iStep<10; iStep++)
	{
		m_btSeqStart[iStep].EnableWindow(TRUE);
	}
	m_btChangeAutoMode.SetCheck(false);
	m_btChangeManualMode.SetCheck(true);
	m_btChangePMMode.SetCheck(false);

	G_NowEQRunMode = Master_MANUAL;
}

void CVS10MasterDlg::OnBnClickedBtEqPmMode()
{
	if(m_SeqObj_StickExchange.m_NowStep != CSeq_StickExchange::SEQ_STEP_IDLE )
	{
		AfxMessageBox("Now Stick Exchange ~ !!!!!");
		return;
	}
	//모두 정지.
	if(CheckAllRunSequence() == true)//Sequenc가 돌고 있으면.. 구동 하지 않느다.
	{//모든 구동 Sequence를 정지 한다.
		//m_SeqObj_StickExchange.fnSeqSetStop();
		m_SeqObj_ThetaAlign.fnSeqSetStop();
		m_SeqObj_CellAlign.fnSeqSetStop();
		m_SeqObj_ReviewCalc.fnSeqSetStop();
		m_SeqObj_CDTP.fnSeqSetStop();
		m_SeqObj_3DScope.fnSeqSetStop();
		m_SeqObj_AreaScan3D.fnSeqSetStop();
		m_SeqObj_InsAutoReview.fnSeqSetStop();
		m_SeqObj_InsMic.fnSeqSetStop();
		m_SeqObj_InsMac.fnSeqSetStop();
		m_SeqObj_AutoRun.fnSeqSetStop();
		m_SeqObj_InitAM01.fnSeqSetStop();
		G_MotObj.m_fnAM01MotAllStop();
	}
	

	//////////////////////////////////////////////////////////////////////////
	m_SubCIMInterfaceView->OnBnClickedBtEqpPm();
	m_btChangeAutoMode.SetCheck(false);
	m_btChangeManualMode.SetCheck(false);
	m_btChangePMMode.SetCheck(true);

	G_NowEQRunMode = Master_PM;
}





void CVS10MasterDlg::OnBnClickedBtSubView()
{
	for(int iStep = 0; iStep<6; iStep++)
		m_btSubView[iStep].SetCheck(false);


	m_Sub3DScopeView->ShowWindow(SW_HIDE);
	m_SubInspectView->ShowWindow(SW_HIDE);
	m_SubAreaScanView->ShowWindow(SW_HIDE);
	m_SubCDTPView->ShowWindow(SW_HIDE);
	m_SubStickExchangeView->ShowWindow(SW_HIDE);
	m_SubThetaAlignView->ShowWindow(SW_HIDE);

	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();
	switch(wID)
	{
	case IDC_BT_SUB_VIEW1:
		m_Sub3DScopeView->ShowWindow(SW_SHOW);
		m_btSubView[0].SetCheck(true);
		break;
	case IDC_BT_SUB_VIEW2:
		m_SubInspectView->ShowWindow(SW_SHOW);
		m_btSubView[1].SetCheck(true);
		break;
	case IDC_BT_SUB_VIEW3:
		m_SubAreaScanView->ShowWindow(SW_SHOW);
		m_btSubView[2].SetCheck(true);
		break;
	case IDC_BT_SUB_VIEW4:
		m_SubCDTPView->ShowWindow(SW_SHOW);
		m_btSubView[3].SetCheck(true);
		break;
	case IDC_BT_SUB_VIEW5:
		m_SubThetaAlignView->ShowWindow(SW_SHOW);
		m_btSubView[4].SetCheck(true);
		break;
	case IDC_BT_SUB_VIEW6:
		m_SubStickExchangeView->ShowWindow(SW_SHOW);
		m_btSubView[5].SetCheck(true);
		break;

	default:
		m_Sub3DScopeView->ShowWindow(SW_SHOW);
		m_btSubView[0].SetCheck(true);
		break;
	}
}

void CVS10MasterDlg::SequenceObjectInit()
{
	m_SeqObj_CellAlign.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_CellAlign.m_pAlignCellStartX = &m_AlignCellStartX;
	m_SeqObj_CellAlign.m_pAlignCellStartY = &m_AlignCellStartY;
	m_SeqObj_CellAlign.m_pScopePos = &m_ScopePos;
	m_SeqObj_CellAlign.m_pReviewPos =&m_ReviewPos;
	m_SeqObj_CellAlign.m_pReviewLensOffset = &m_ReviewLensOffset;
	m_SeqObj_CellAlign.m_pLiveViewObj = &m_LiveViewObj;
	m_SeqObj_CellAlign.fnSeqInit((void*)this, SeqFlow_CellAlign);


	m_SeqObj_ReviewCalc.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_ReviewCalc.m_pGripperPos = &m_GripperPos;
	m_SeqObj_ReviewCalc.m_pScopePos = &m_ScopePos;
	m_SeqObj_ReviewCalc.m_pReviewPos =&m_ReviewPos;
	m_SeqObj_ReviewCalc.m_pReviewLensOffset = &m_ReviewLensOffset;
	m_SeqObj_ReviewCalc.m_pLiveViewObj = &m_LiveViewObj;
	m_SeqObj_ReviewCalc.fnSeqInit((void*)this, SeqFlow_ReviewCalc);
	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_ThetaAlign.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_ThetaAlign.m_pScopePos = &m_ScopePos;
	m_SeqObj_ThetaAlign.m_pReviewPos = &m_ReviewPos;
	//m_SeqObj_ThetaAlign.m_pScopeLensOffset = &m_ScopeLensOffset;
	m_SeqObj_ThetaAlign.m_pLiveViewObj = &m_LiveViewObj;
	m_SeqObj_ThetaAlign.m_pThetaAlignLiveObj = &m_ThetaAlignLiveObj;
	m_SeqObj_ThetaAlign.m_pNowLensIndex_3DScope = &m_NowLensIndex_3DScope;
	m_SeqObj_ThetaAlign.fnSeqInit((void*)this, SeqFlow_ThetaAlign);
	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_CDTP.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_CDTP.m_pGlassReicpeParam = &m_GlassReicpeParam;
	m_SeqObj_CDTP.m_pScopePos = &m_ScopePos;
	m_SeqObj_CDTP.m_pReviewPos =&m_ReviewPos;
	m_SeqObj_CDTP.m_pReviewLensOffset = &m_ReviewLensOffset;
	m_SeqObj_CDTP.m_pLiveViewObj = &m_LiveViewObj;
	m_SeqObj_CDTP.m_pCDTPParam = &m_CDTPParam;
	m_SeqObj_CDTP.m_pCDTPPointCnt = &m_CDTPPointCnt;
	m_SeqObj_CDTP.m_ppCDTPPointList = &m_pCDTPPointList;
	m_SeqObj_CDTP.m_pTP_Result = &m_TP_Result;
	m_SeqObj_CDTP.fnSeqInit((void*)this, SeqFlow_CDTP);
	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_3DScope.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_3DScope.m_pNowLensIndex_3DScope = &m_NowLensIndex_3DScope;
	m_SeqObj_3DScope.m_pScopeLensOffset = &m_ScopeLensOffset;
	m_SeqObj_3DScope.m_pScope3DParam = &m_Scope3DParam;
	m_SeqObj_3DScope.m_pScopePos = &m_ScopePos;
	m_SeqObj_3DScope.m_pReviewPos = &m_ReviewPos;
	m_SeqObj_3DScope.m_p3DMeasurPointCnt = &m_3DMeasurPointCnt;
	m_SeqObj_3DScope.m_pp3DMeasurPointList = &m_p3DMeasurPointList;
	m_SeqObj_3DScope.fnSeqInit((void*)this, SeqFlow_3DScope);
	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_AreaScan3D.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_AreaScan3D.m_pScan3DParam = &m_Scan3DParam;
	m_SeqObj_AreaScan3D.m_pScan3DResult = &m_Scan3DResult;
	m_SeqObj_AreaScan3D.m_pCamToObjDis = &m_CamToObjDis;
	m_SeqObj_AreaScan3D.m_pAlignCellStartX = &m_AlignCellStartX;
	m_SeqObj_AreaScan3D.m_pAlignCellStartY = &m_AlignCellStartY;
	m_SeqObj_AreaScan3D.m_pScopePos = &m_ScopePos;
	m_SeqObj_AreaScan3D.m_pReviewPos = &m_ReviewPos;
	m_SeqObj_AreaScan3D.fnSeqInit((void*)this, SeqFlow_AreaScan3D);

	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_InsAutoReview.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_InsAutoReview.m_pGlassReicpeParam = &m_GlassReicpeParam;
	m_SeqObj_InsAutoReview.m_pHWRecipeParam = &m_HWRecipeParam;
	m_SeqObj_InsAutoReview.m_pLiveViewObj = &m_LiveViewObj;
	m_SeqObj_InsAutoReview.m_pbInspectMicOrMac = &m_bInspectMicOrMac;
	m_SeqObj_InsAutoReview.m_pMergeData_MicQ = &m_MergeData_MicQ;
	m_SeqObj_InsAutoReview.m_pMergeData_MacQ = &m_MergeData_MacQ;
	m_SeqObj_InsAutoReview.m_pScopePos = &m_ScopePos;
	m_SeqObj_InsAutoReview.m_pReviewPos = &m_ReviewPos;
	m_SeqObj_InsAutoReview.fnSeqInit((void*)this, SeqFlow_InsAutoReview);

	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_InsMic.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_InsMic.m_pBlockEnd = &m_BlockEnd;
	m_SeqObj_InsMic.m_pStartBlockTick = &m_StartBlockTick;
	m_SeqObj_InsMic.m_pStartLineTick = &m_StartLineTick;
	m_SeqObj_InsMic.m_pCamScnaLineCnt = &m_CamScnaLineCnt;
	m_SeqObj_InsMic.m_pCamScnaImgCnt = &m_CamScnaImgCnt;
	m_SeqObj_InsMic.m_pStartScanLineNum = &m_StartScanLineNum;
	m_SeqObj_InsMic.m_pbInspectMicOrMac = &m_bInspectMicOrMac;
	m_SeqObj_InsMic.m_pAlignCellStartX = &m_AlignCellStartX;
	m_SeqObj_InsMic.m_pAlignCellStartY = &m_AlignCellStartY;
	m_SeqObj_InsMic.m_pGlassReicpeParam = &m_GlassReicpeParam;
	m_SeqObj_InsMic.m_pstMaskImageInfo_Mic = &m_stMaskImageInfo_Mic;
	m_SeqObj_InsMic.m_pstCommonPara_Mic = &m_stCommonPara_Mic;	
	//m_SeqObj_InsMic.m_pstMaskImageInfo_Mac = &m_stMaskImageInfo_Mac;
	//m_SeqObj_InsMic.m_pstCommonPara_Mac = &m_stCommonPara_Mac;
	m_SeqObj_InsMic.m_pHWRecipeParam = &m_HWRecipeParam;
	m_SeqObj_InsMic.m_pCamToObjDis = &m_CamToObjDis;
	m_SeqObj_InsMic.m_pScopePos = &m_ScopePos;
	m_SeqObj_InsMic.m_pReviewPos = &m_ReviewPos;

	m_SeqObj_InsMic.m_pSeqObj_InsAutoReview =&m_SeqObj_InsAutoReview;
	m_SeqObj_InsMic.fnSeqInit((void*)this, SeqFlow_InsMic);

	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_InsMac.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_InsMac.m_pBlockEnd = &m_BlockEnd;
	m_SeqObj_InsMac.m_pStartBlockTick = &m_StartBlockTick;
	m_SeqObj_InsMac.m_pStartLineTick = &m_StartLineTick;
	m_SeqObj_InsMac.m_pCamScnaLineCnt = &m_CamScnaLineCnt;
	m_SeqObj_InsMac.m_pCamScnaImgCnt = &m_CamScnaImgCnt;
	m_SeqObj_InsMac.m_pStartScanLineNum = &m_StartScanLineNum;
	m_SeqObj_InsMac.m_pbInspectMicOrMac = &m_bInspectMicOrMac;
	m_SeqObj_InsMac.m_pAlignCellStartX = &m_AlignCellStartX;
	m_SeqObj_InsMac.m_pAlignCellStartY = &m_AlignCellStartY;
	m_SeqObj_InsMac.m_pGlassReicpeParam = &m_GlassReicpeParam;
	m_SeqObj_InsMac.m_pMergeData_MacQ = &m_MergeData_MacQ;
	//m_SeqObj_InsMac.m_pstMaskImageInfo_Mic = &m_stMaskImageInfo_Mic;
	//m_SeqObj_InsMac.m_pstCommonPara_Mic = &m_stCommonPara_Mic;	
	m_SeqObj_InsMac.m_pstMaskImageInfo_Mac = &m_stMaskImageInfo_Mac;
	m_SeqObj_InsMac.m_pstCommonPara_Mac = &m_stCommonPara_Mac;
	m_SeqObj_InsMac.m_pHWRecipeParam = &m_HWRecipeParam;
	m_SeqObj_InsMac.m_pCamToObjDis = &m_CamToObjDis;
	m_SeqObj_InsMac.m_pScopePos = &m_ScopePos;
	m_SeqObj_InsMac.m_pReviewPos = &m_ReviewPos;

	m_SeqObj_InsMac.m_pSeqObj_InsAutoReview =&m_SeqObj_InsAutoReview;
	m_SeqObj_InsMac.fnSeqInit((void*)this, SeqFlow_InsMac);
	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_StickExchange.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_StickExchange.m_pStickLoadInfo = &m_StickLoadInfo;
	m_SeqObj_StickExchange.m_pSeqObj_ThetaAlign		= &m_SeqObj_ThetaAlign;
	m_SeqObj_StickExchange.m_pGlassReicpeParam = &m_GlassReicpeParam;
	m_SeqObj_StickExchange.m_pScopePos = &m_ScopePos;
	m_SeqObj_StickExchange.m_pReviewPos = &m_ReviewPos;
	m_SeqObj_StickExchange.m_pGripperPos = &m_GripperPos;
	m_SeqObj_StickExchange.m_pStickTentionValue = &m_StickTentionValue;
	m_SeqObj_StickExchange.fnSeqInit((void*)this, SeqFlow_StickExchange);
	//////////////////////////////////////////////////////////////////////////

	m_SeqObj_InitAM01.m_pServerInterface = &m_ServerInterface;
	m_SeqObj_InitAM01.m_pTaskLinkCheck  = &m_TaskLinkCheck;
	m_SeqObj_InitAM01.m_pSubCIMInterfaceView = m_SubCIMInterfaceView;
	m_SeqObj_InitAM01.m_pScopePos = &m_ScopePos;
	m_SeqObj_InitAM01.m_pReviewPos = &m_ReviewPos;
	m_SeqObj_InitAM01.m_pGripperPos = &m_GripperPos;
	m_SeqObj_InitAM01.m_pNowLensIndex_3DScope = &m_NowLensIndex_3DScope;
	m_SeqObj_InitAM01.fnSeqInit((void*)this, SeqFlow_InitAM01);
	//////////////////////////////////////////////////////////////////////////
	m_SeqObj_AutoRun.m_pSeqObj_StickExchange	= &m_SeqObj_StickExchange;
	m_SeqObj_AutoRun.m_pSeqObj_InitAM01			= &m_SeqObj_InitAM01;
	m_SeqObj_AutoRun.m_pSeqObj_CellAlign			= &m_SeqObj_CellAlign;
	m_SeqObj_AutoRun.m_pSeqObj_CDTP				= &m_SeqObj_CDTP;
	m_SeqObj_AutoRun.m_pSeqObj_AreaScan3D		= &m_SeqObj_AreaScan3D;
	m_SeqObj_AutoRun.m_pSeqObj_3DScope			= &m_SeqObj_3DScope;
	m_SeqObj_AutoRun.m_pSeqObj_InsMic			= &m_SeqObj_InsMic;
	m_SeqObj_AutoRun.m_pSeqObj_InsMac			= &m_SeqObj_InsMac;
	m_SeqObj_AutoRun.fnSeqInit((void*)this, SeqFlow_AutoRun);
}




int CVS10MasterDlg::SeqFlow_StickExchange(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;

	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_StickExchange.m_StartTech)/1000;
	strNewValue.Format("StickExchange(%d)-", FlowNowSec);
	//strNewValue.Format("StickExchange-");

	int BarIndex = 0;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;
	
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);


	switch(NowFlowStep)
	{
	case CSeq_StickExchange::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_StickExchange::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_StickExchange::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_StickExchange::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}

	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_CellAlign(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_CellAlign.m_StartTech)/1000;
	strNewValue.Format("Cell Align(%d)-", FlowNowSec);
	//strNewValue.Format("Cell Align-");
	int BarIndex = 7;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;
	
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_CellAlign::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_CellAlign::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_CellAlign::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_CellAlign::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_ReviewCalc(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_ReviewCalc.m_StartTech)/1000;
	strNewValue.Format("Review Calc(%d)-", FlowNowSec);
	//strNewValue.Format("Review Calc-");
	switch(NowFlowStep)
	{
	case CSeq_ReviewCalc::SEQ_STEP_ERROR:
		break;
	case CSeq_ReviewCalc::SEQ_STEP_IDLE:
		break;
	case CSeq_ReviewCalc::SEQ_STEP_RUN:
		break;
	case CSeq_ReviewCalc::SEQ_STEP_CNT:
		break;
	}
	return 0;
}
int CVS10MasterDlg::SeqFlow_ThetaAlign(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_ThetaAlign.m_StartTech)/1000;
	strNewValue.Format("ThetaAlign(%d)-", FlowNowSec);
	//strNewValue.Format("ThetaAlign-");
	int BarIndex = 1;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;
	
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_ThetaAlign::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_ThetaAlign::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_ThetaAlign::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_ThetaAlign::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_CDTP(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_CDTP.m_StartTech)/1000;
	strNewValue.Format("CD, TP(%d)-", FlowNowSec);
	//strNewValue.Format("CD, TP-");
	int BarIndex = 2;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;
	
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_CDTP::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_CDTP::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_CDTP::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_CDTP::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_AreaScan3D(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_AreaScan3D.m_StartTech)/1000;
	strNewValue.Format("AreaScan3D(%d)-", FlowNowSec);
	//strNewValue.Format("Area Scan 3D-");
	int BarIndex = 3;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;
	
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_AreaScan3D::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_AreaScan3D::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_AreaScan3D::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_AreaScan3D::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_3DScope(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_3DScope.m_StartTech)/1000;
	strNewValue.Format("3D Scope(%d)-", FlowNowSec);
	//strNewValue.Format("3D Scope-");
	int BarIndex = 4;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;
	
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_3DScope::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_3DScope::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_3DScope::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_3DScope::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_InsMic(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_InsMic.m_StartTech)/1000;
	strNewValue.Format("Inspection Micro(%d)-", FlowNowSec);
	//strNewValue.Format("Inspection-");
	int BarIndex = 5;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;
	
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_Inspect_Mic::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_Inspect_Mic::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_Inspect_Mic::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_Inspect_Mic::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_InsMac(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_InsMac.m_StartTech)/1000;
	strNewValue.Format("Inspection Macro(%d)-", FlowNowSec);
	//strNewValue.Format("Inspection-");
	int BarIndex = 9;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;

	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_Inspect_Mac::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_Inspect_Mac::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_Inspect_Mac::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_Inspect_Mac::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_InsAutoReview(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	switch(NowFlowStep)
	{
	case CSeq_InsAutoReview::SEQ_STEP_ERROR:
		break;
	case CSeq_InsAutoReview::SEQ_STEP_IDLE:
		break;
	case CSeq_InsAutoReview::SEQ_STEP_RUN:
		break;
	case CSeq_InsAutoReview::SEQ_STEP_CNT:
		break;
	}
	return 0;
}
int CVS10MasterDlg::SeqFlow_AutoRun(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_AutoRun.m_StartTech)/1000;
	strNewValue.Format("AutoRun(%d)-", FlowNowSec);
	int BarIndex = 6;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;
	
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_AutoRun::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_AutoRun::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_AutoRun::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_AutoRun::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}
int CVS10MasterDlg::SeqFlow_InitAM01(void *pObj, int NowFlowStep)
{
	CVS10MasterDlg*pCtrOBJ = (CVS10MasterDlg*)pObj;
	CString strNewValue;
	int FlowNowSec =(::GetTickCount()- pCtrOBJ->m_SeqObj_InitAM01.m_StartTech)/1000;
	strNewValue.Format("InitAM01(%d)-", FlowNowSec);
	//strNewValue.Format("InitAM01-");
	int BarIndex = 8;
	int NewPos = NowFlowStep;
	if(NewPos<0)
		NewPos = 0;

	pCtrOBJ->m_progSeqViewBar[BarIndex].SetWindowText(strNewValue);
	switch(NowFlowStep)
	{
	case CSeq_StickExchange::SEQ_STEP_ERROR:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(255,0,0));
		break;
	case CSeq_StickExchange::SEQ_STEP_IDLE:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBkColor(RGB(0,0,0));
		break;
	case CSeq_StickExchange::SEQ_STEP_RUN:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,200,0));
		break;
	case CSeq_StickExchange::SEQ_STEP_CNT:
		pCtrOBJ->m_progSeqViewBar[BarIndex].SetBarBkColor(RGB(0,0,0));
		break;
	}
	pCtrOBJ->m_progSeqViewBar[BarIndex].SetPos(NewPos);
	return 0;
}


//모든 결과 Data를 File에 기록 한다.
void CVS10MasterDlg::m_fnMeasureInputData(CDataFormat *pWriteResultObj)
{
	CString strData;
	int nDataSize = sizeof(G_InputMeasureData) / 8;

	map< CString, DataStruct >::iterator IterPos[100];

	for(int i=0; i < nDataSize; i++)
	{
		strData.Format("%d_%s", KEY_MEASURE_INFO, G_InputMeasureData[i]);
		IterPos[i] = pWriteResultObj->m_MapData[KEY_MEASURE_INFO].find(strData);
	}	


	int nLineIndex = 0;
	//////////////////////////////////////////////////////////////////////////

	// 굴곡 Start
	//CString strData1 = "111";
	//CString strData2 = "222";
	//CString strData3 = "333";
	//CString strData4 = "444";
	//CString strData5 = "굴곡";
	//CString strData6 = "둔턱";
	//CString strData7 = "CD";


	//굴곡 1
	CString strRet_STRAIGHTNESS_X;
	CString strRet_STRAIGHTNESS_Y;
	CString strRet_M_WAVE;
	strRet_STRAIGHTNESS_X.Format("%0.2f", 0.0f);
	strRet_STRAIGHTNESS_Y.Format("%0.2f", 0.0f);
	strRet_M_WAVE.Format("%0.2f", 0.0f);
	memcpy(IterPos[M_STRAIGHTNESS_X]->second.vecDataValue[nLineIndex], strRet_STRAIGHTNESS_X, strRet_STRAIGHTNESS_X.GetLength()+1);
	memcpy(IterPos[M_STRAIGHTNESS_Y]->second.vecDataValue[nLineIndex], strRet_STRAIGHTNESS_Y, strRet_STRAIGHTNESS_Y.GetLength()+1);
	memcpy(IterPos[M_WAVE]->second.vecDataValue[nLineIndex], strRet_M_WAVE, strRet_M_WAVE.GetLength()+1);

	CString strCoord_X1;
	CString strCoord_Y1;
	CString strCoord_X2;
	CString strCoord_Y2;
	strCoord_X1.Format("%d", m_AlignCellStartX);
	strCoord_Y1.Format("%d", m_AlignCellStartY);
	strCoord_X2.Format("%d", m_Scan3DParam.nScan_Count*SCANER_SENSOR_WIDTH);
	strCoord_Y2.Format("%d", m_Scan3DParam.nScan_Distance);
	memcpy(IterPos[M_COORD_X1]->second.vecDataValue[nLineIndex], strCoord_X1, strCoord_X1.GetLength()+1);//X위치
	memcpy(IterPos[M_COORD_Y1]->second.vecDataValue[nLineIndex], strCoord_Y1, strCoord_Y1.GetLength()+1);//Y위치
	memcpy(IterPos[M_COORD_X2]->second.vecDataValue[nLineIndex], strCoord_X2, strCoord_X2.GetLength()+1);//Width
	memcpy(IterPos[M_COORD_Y2]->second.vecDataValue[nLineIndex], strCoord_Y2, strCoord_Y2.GetLength()+1);//Hight


	CString strMIN;
	CString strAVG;
	CString strMAX;
	strMIN.Format("%0.3f", m_Scan3DResult.fMin);
	strAVG.Format("%0.3f", m_Scan3DResult.fAvr);
	strMAX.Format("%0.3f", m_Scan3DResult.fMax);
	memcpy(IterPos[M_WAVE_MIN]->second.vecDataValue[nLineIndex], strMIN, strMIN.GetLength()+1);
	memcpy(IterPos[M_WAVE_AVG]->second.vecDataValue[nLineIndex], strAVG, strAVG.GetLength()+1);
	memcpy(IterPos[M_WAVE_MAX]->second.vecDataValue[nLineIndex], strMAX, strMAX.GetLength()+1);
	nLineIndex++;
	// 굴곡 End


	//CD  n개
	// Cd Start

	CString strfSlitSizeW;
	CString strfSlitSizeH;
	CString strfRibSizeW;
	CString strfRibSizeH;
	for(int i=0; i<m_CDTPPointCnt; i++)
	{

		strCoord_X1.Format("%d", m_pCDTPPointList[i].nPointX);//Now Machine Coordinates
		strCoord_Y1.Format("%d", m_pCDTPPointList[i].nPointY);//Now Machine Coordinates
		strCoord_X2.Format("%d", 0);
		strCoord_Y2.Format("%d", 0);

		memcpy(IterPos[M_COORD_X1]->second.vecDataValue[nLineIndex], strCoord_X1, strCoord_X1.GetLength()+1);
		memcpy(IterPos[M_COORD_Y1]->second.vecDataValue[nLineIndex], strCoord_Y1, strCoord_Y1.GetLength()+1);
		memcpy(IterPos[M_COORD_X2]->second.vecDataValue[nLineIndex], strCoord_X2, strCoord_X2.GetLength()+1);
		memcpy(IterPos[M_COORD_Y2]->second.vecDataValue[nLineIndex], strCoord_Y2, strCoord_Y2.GetLength()+1);

		strfSlitSizeW.Format("%0.2f", m_pCDTPPointList[i].fSlitSizeW);
		strfSlitSizeH.Format("%0.2f", m_pCDTPPointList[i].fSlitSizeH);
		strfRibSizeW.Format("%0.2f", m_pCDTPPointList[i].fRibSizeW);
		strfRibSizeH.Format("%0.2f", m_pCDTPPointList[i].fRibSizeH);
		memcpy(IterPos[M_SLIT_WIDTH_X]->second.vecDataValue[nLineIndex], strfSlitSizeW, strfSlitSizeW.GetLength()+1);
		memcpy(IterPos[M_SLIT_WIDTH_Y]->second.vecDataValue[nLineIndex], strfSlitSizeH, strfSlitSizeH.GetLength()+1);
		memcpy(IterPos[M_RIB_WIDTH_X]->second.vecDataValue[nLineIndex], strfRibSizeW, strfRibSizeW.GetLength()+1);
		memcpy(IterPos[M_RIB_WIDTH_Y]->second.vecDataValue[nLineIndex], strfRibSizeH, strfRibSizeH.GetLength()+1);
		nLineIndex++;
	}
	//Cell TP  n(Cell Cout)
	CString strRet_CELL_TP;
	for(int i=0; i<m_TP_Result.nCell_TPCnt; i++)
	{
		strCoord_X1.Format("%d", m_pCDTPPointList[i*4].nPointX);//Now Machine Coordinates
		strCoord_Y1.Format("%d", m_pCDTPPointList[i*4].nPointY);//Now Machine Coordinates
		memcpy(IterPos[M_COORD_X1]->second.vecDataValue[nLineIndex], strCoord_X1, strCoord_X1.GetLength()+1);
		memcpy(IterPos[M_COORD_Y1]->second.vecDataValue[nLineIndex], strCoord_Y1, strCoord_Y1.GetLength()+1);


		strRet_CELL_TP.Format("%0.2f", m_TP_Result.pfCDTP_Ret_CELL_TP[i]);
		memcpy(IterPos[M_CELL_TP]->second.vecDataValue[nLineIndex], strRet_CELL_TP, strRet_CELL_TP.GetLength()+1);
		nLineIndex++;
	}
	//TP 1
	CString strRet_TP;
	strRet_TP.Format("%0.2f", m_TP_Result.fCDTP_Ret_TP);
	memcpy(IterPos[M_TP]->second.vecDataValue[nLineIndex], strRet_TP, strRet_TP.GetLength()+1);
	nLineIndex++;
	// CD End

	// 둔턱 Start
	//둔턱  n+1
	CString strRet_DOON_TUK;
	CString strRet_STICK_THICK;
	CString strRet_TAPERANGLE;
	strRet_DOON_TUK.Format("%0.2f", m_f3DScope_Ret_DOON_TUK);
	strRet_STICK_THICK.Format("%0.2f", m_f3DScope_Ret_STICK_THICK);
	strRet_TAPERANGLE.Format("%0.2f", m_f3DScope_Ret_TAPERANGLE);
	memcpy(IterPos[M_DOON_TUK]->second.vecDataValue[nLineIndex], strRet_DOON_TUK, strRet_DOON_TUK.GetLength()+1);
	memcpy(IterPos[M_STICK_THICK]->second.vecDataValue[nLineIndex], strRet_STICK_THICK, strRet_STICK_THICK.GetLength()+1);
	memcpy(IterPos[M_TAPERANGLE]->second.vecDataValue[nLineIndex], strRet_TAPERANGLE, strRet_TAPERANGLE.GetLength()+1);
	nLineIndex++;



	CString strDOON_TUK_X_L			;
	CString strDOON_TUK_X_R			;
	CString strDOON_TUK_Y_L			;
	CString strDOON_TUK_Y_R			;
	CString strSTICK_THIK_X_L		;
	CString strSTICK_THIK_X_R		;
	CString strSTICK_THIK_Y_L		;
	CString strSTICK_THIK_Y_R		;
	CString strTAPERANGLE_X_L		;
	CString strTAPERANGLE_X_R		;
	CString strTAPERANGLE_Y_L		;
	CString strTAPERANGLE_Y_R		;
	//둔턱  n개
	for(int i=0; i<m_3DMeasurPointCnt; i++)
	{
		for(int j=0; m_p3DMeasurPointList[i].nScopeRetCount; j++) {
			strCoord_X1.Format("%d", m_p3DMeasurPointList[i].nPointX);//Now Machine Coordinates
			strCoord_Y1.Format("%d", m_p3DMeasurPointList[i].nPointY);//Now Machine Coordinates
			strCoord_X2.Format("%d", 0);
			strCoord_Y2.Format("%d", 0);
			memcpy(IterPos[M_COORD_X1]->second.vecDataValue[nLineIndex], strCoord_X1, strCoord_X1.GetLength()+1);
			memcpy(IterPos[M_COORD_Y1]->second.vecDataValue[nLineIndex], strCoord_Y1, strCoord_Y1.GetLength()+1);
			memcpy(IterPos[M_COORD_X2]->second.vecDataValue[nLineIndex], strCoord_X2, strCoord_X2.GetLength()+1);
			memcpy(IterPos[M_COORD_Y2]->second.vecDataValue[nLineIndex], strCoord_Y2, strCoord_Y2.GetLength()+1);

			strDOON_TUK_X_L.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_HEIGHT_E_L/1000.0f);
			strDOON_TUK_X_R.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_HEIGHT_E_R/1000.0f);	
			strDOON_TUK_Y_L.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_HEIGHT_E_L2/1000.0f);
			strDOON_TUK_Y_R.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_HEIGHT_E_R2/1000.0f);	

			strSTICK_THIK_X_L.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_HEIGHT_L/1000.0f);
			strSTICK_THIK_X_R.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_HEIGHT_R/1000.0f);
			strSTICK_THIK_Y_L.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_HEIGHT_L2/1000.0f);
			strSTICK_THIK_Y_R.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_HEIGHT_R2/1000.0f);

			strTAPERANGLE_X_L.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_ANGLE_L);
			strTAPERANGLE_X_R.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_ANGLE_R);
			strTAPERANGLE_Y_L.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_ANGLE_L2);
			strTAPERANGLE_Y_R.Format("%0.2f", m_p3DMeasurPointList[i].pScopeRet[j].RIB_ANGLE_R2);

			memcpy(IterPos[M_DOON_TUK_X_L]->second.vecDataValue[nLineIndex],		strDOON_TUK_X_L,		strDOON_TUK_X_L.GetLength()+1);
			memcpy(IterPos[M_DOON_TUK_X_R]->second.vecDataValue[nLineIndex],		strDOON_TUK_X_R,		strDOON_TUK_X_R.GetLength()+1);
			memcpy(IterPos[M_DOON_TUK_Y_L]->second.vecDataValue[nLineIndex],		strDOON_TUK_Y_L,		strDOON_TUK_Y_L.GetLength()+1);
			memcpy(IterPos[M_DOON_TUK_Y_R]->second.vecDataValue[nLineIndex],		strDOON_TUK_Y_R,		strDOON_TUK_Y_R.GetLength()+1);
			memcpy(IterPos[M_STICK_THIK_X_L]->second.vecDataValue[nLineIndex],		strSTICK_THIK_X_L,		strSTICK_THIK_X_L.GetLength()+1);
			memcpy(IterPos[M_STICK_THIK_X_R]->second.vecDataValue[nLineIndex],		strSTICK_THIK_X_R,		strSTICK_THIK_X_R.GetLength()+1);
			memcpy(IterPos[M_STICK_THIK_Y_L]->second.vecDataValue[nLineIndex],		strSTICK_THIK_Y_L,		strSTICK_THIK_Y_L.GetLength()+1);
			memcpy(IterPos[M_STICK_THIK_Y_R]->second.vecDataValue[nLineIndex],		strSTICK_THIK_Y_R,		strSTICK_THIK_Y_R.GetLength()+1);
			memcpy(IterPos[M_TAPERANGLE_X_L]->second.vecDataValue[nLineIndex],	strTAPERANGLE_X_L,		strTAPERANGLE_X_L.GetLength()+1);
			memcpy(IterPos[M_TAPERANGLE_X_R]->second.vecDataValue[nLineIndex],	strTAPERANGLE_X_R,	strTAPERANGLE_X_R.GetLength()+1);
			memcpy(IterPos[M_TAPERANGLE_Y_L]->second.vecDataValue[nLineIndex],	strTAPERANGLE_Y_L,		strTAPERANGLE_Y_L.GetLength()+1);
			memcpy(IterPos[M_TAPERANGLE_Y_R]->second.vecDataValue[nLineIndex],	strTAPERANGLE_Y_R,	strTAPERANGLE_Y_R.GetLength()+1);
			nLineIndex++;		
		}
	}
	// 둔턱 End
	//////////////////////////////////////////////////////////////////////////
}

void CVS10MasterDlg::m_fnInspectInputData(CDataFormat *pWriteResultObj)
{
	CString strData;
	int nDataSize = sizeof(G_InputInspectData) / 8;

	map< CString, DataStruct >::iterator IterPos[100];

	for(int i=0; i < nDataSize; i++)
	{
		strData.Format("%d_%s", KEY_INSPECTION_INFO, G_InputInspectData[i]);
		IterPos[i] = pWriteResultObj->m_MapData[KEY_INSPECTION_INFO].find(strData);
	}	

	CString strCoodX ;
	CString strCoodY ;
	CString strCoodW ;
	CString strCoodH ;

	CString strName;
	CString strCode;
	CString strCnt;
	CString strReviewFile;

	DataNodeObj * pNowReviewData = nullptr;


	


	m_MergeData_MicQ.QGetMoveFirstNode();
	pNowReviewData = m_MergeData_MicQ.QGetNextNodeData();
	for(int i=0; i<m_MergeData_MicQ.QGetCnt() && pNowReviewData != nullptr; i++)
	{


		strCoodX.Format("%d",pNowReviewData->DefectData.CoodStartX);
		strCoodY.Format("%d",pNowReviewData->DefectData.CoodStartY);
		strCoodW.Format("%d",pNowReviewData->DefectData.CoodWidth);
		strCoodH.Format("%d",pNowReviewData->DefectData.CoodHeight);

		strName.Format("0");
		strCode.Format("0");
		strCnt.Format("%d", 1);
		strReviewFile.Format("%s",pNowReviewData->m_strReviewFileName);

		memcpy(IterPos[I_COORD_X1]->second.vecDataValue[i], strCoodX, strCoodX.GetLength()+1);
		memcpy(IterPos[I_COORD_Y1]->second.vecDataValue[i], strCoodY, strCoodY.GetLength()+1);
		memcpy(IterPos[I_COORD_X2]->second.vecDataValue[i], strCoodW, strCoodW.GetLength()+1);
		memcpy(IterPos[I_COORD_Y2]->second.vecDataValue[i], strCoodH, strCoodH.GetLength()+1);

		memcpy(IterPos[I_DEFECT_NAME]->second.vecDataValue[i], strName, strName.GetLength()+1);
		memcpy(IterPos[I_DEFECT_CODE]->second.vecDataValue[i], strCode, strCode.GetLength()+1);
		memcpy(IterPos[I_DEFECT_CNT]->second.vecDataValue[i], strCnt, strCnt.GetLength()+1);
		memcpy(IterPos[I_IMAGE_FILE]->second.vecDataValue[i], strReviewFile, strReviewFile.GetLength()+1);

		pNowReviewData = m_MergeData_MicQ.QGetNextNodeData();
	}

	m_MergeData_MacQ.QGetMoveFirstNode();
	pNowReviewData = m_MergeData_MacQ.QGetNextNodeData();
	for(int i=m_MergeData_MicQ.QGetCnt() ; i<pWriteResultObj->m_nInspectCount && pNowReviewData != nullptr; i++)
	{


		strCoodX.Format("%d",pNowReviewData->DefectData.CoodStartX);
		strCoodY.Format("%d",pNowReviewData->DefectData.CoodStartY);
		strCoodW.Format("%d",pNowReviewData->DefectData.CoodWidth);
		strCoodH.Format("%d",pNowReviewData->DefectData.CoodHeight);

		strName.Format("0");
		strCode.Format("0");
		strCnt.Format("%d", 1);
		strReviewFile.Format("%s",pNowReviewData->m_strReviewFileName);

		memcpy(IterPos[I_COORD_X1]->second.vecDataValue[i], strCoodX, strCoodX.GetLength()+1);
		memcpy(IterPos[I_COORD_Y1]->second.vecDataValue[i], strCoodY, strCoodY.GetLength()+1);
		memcpy(IterPos[I_COORD_X2]->second.vecDataValue[i], strCoodW, strCoodW.GetLength()+1);
		memcpy(IterPos[I_COORD_Y2]->second.vecDataValue[i], strCoodH, strCoodH.GetLength()+1);

		memcpy(IterPos[I_DEFECT_NAME]->second.vecDataValue[i], strName, strName.GetLength()+1);
		memcpy(IterPos[I_DEFECT_CODE]->second.vecDataValue[i], strCode, strCode.GetLength()+1);
		memcpy(IterPos[I_DEFECT_CNT]->second.vecDataValue[i], strCnt, strCnt.GetLength()+1);
		memcpy(IterPos[I_IMAGE_FILE]->second.vecDataValue[i], strReviewFile, strReviewFile.GetLength()+1);

		pNowReviewData = m_MergeData_MacQ.QGetNextNodeData();
	}
}
bool CVS10MasterDlg::WriteMeasureAllResult()//(D:\\TEST1.dat)
{
	CDataFormat WriteResultObj;
	//////////////////////////////////////////////////////////////////////////
	// Info Data Memory 생성 Start
	WriteResultObj.m_fnCreateInfoData();					// Info Data 
	// Info Data Memory 생성 End
	//////////////////////////////////////////////////////////////////////////
	// Stick Information 자료 입력 Start
	CString strBoxID;
	CString strStickID;
	CString strRecipeID;

	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunBoxID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strBoxID.Format("%s",  strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strStickID.Format("%s",  strReadData);
	

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	strRecipeID.Format("%s",  strReadData);


	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_STICK_ID, strStickID);
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_PPID,	   strRecipeID);
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_BOX_ID,	strBoxID);
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_FLOW_ID,	CString(EQP_FLOW_ID));//m_CIMInterface.m_fnGetMaterialInfo(FLOWID));
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_STEPID,	m_CIMInterface.m_fnGetMaterialInfo(STEPID));
	//WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_FLOW_NAME,	m_CIMInterface.m_fnGetMaterialInfo(FLOWID));
	//WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_STEP_NAME,	m_CIMInterface.m_fnGetMaterialInfo(STEPID));

	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_STICK_CNT,"10");
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_MSTICK_CNT,"10");
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_JUDGE,"B");  // S, A, B, C, X
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_OPER_ID, "OP_ID");
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_JOB_STARTTIME, m_CIM_Inspection_StartTime);
	WriteResultObj.m_fnInsertDataValue(KEY_STICK_INFO, D_JOB_ENDTIME, m_CIM_Inspection_EndTime);
	// Stick Information 자료 입력 End
	//////////////////////////////////////////////////////////////////////////	

	//////////////////////////////////////////////////////////////////////////
	// Measure Data Memory 생성 Start
	CString strTentionValue;
	strTentionValue.Format("%0.2fg", m_StickTentionValue);

	//Measure Total Cnt (굴곡-1, 둔턱-n, CD-n)
	int nMeasureTotalCnt = 1 + m_3DMeasurPointCnt+1 + m_CDTPPointCnt+m_TP_Result.nCell_TPCnt+1;
	int nInspectTotalCnt = m_MergeData_MicQ.QGetCnt() + m_MergeData_MacQ.QGetCnt();
	WriteResultObj.m_fnCreateInspectAndMeasureData(KEY_MEASURE_INFO, nMeasureTotalCnt, strStickID, strBoxID, strTentionValue);
	WriteResultObj.m_fnCreateInspectAndMeasureData(KEY_INSPECTION_INFO, nInspectTotalCnt, strStickID, strBoxID);
	// Measure Data Memory 생성 End
	//////////////////////////////////////////////////////////////////////////	
	m_fnMeasureInputData(&WriteResultObj);
	m_fnInspectInputData(&WriteResultObj);
	//////////////////////////////////////////////////////////////////////////
	// Inspect 자료 입력 Start

	// Inspect 자료 입력 End
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// 	// Header 자료 입력 Start	
	const int nHeaderLineCount = 6;
	int nLineCount = nHeaderLineCount + nMeasureTotalCnt +nInspectTotalCnt;
	CString strCount;
	strCount.Format("%d", nLineCount);
	WriteResultObj.m_fnInsertDataValue(KEY_HEADER_INFO, D_ROW_CNT, strCount);
	// Header 자료 입력 End
	//////////////////////////////////////////////////////////////////////////


	// Memory 입력 Start
	WriteResultObj.m_fnWriteInfoDataToMem();
	WriteResultObj.m_fnWriteInsPectAndMeasureDataToMem(KEY_MEASURE_INFO);	
	WriteResultObj.m_fnWriteInsPectAndMeasureDataToMem(KEY_INSPECTION_INFO);
	// Memory 입력 End

	//int test = WriteResultObj.m_fnGetSumFileCount();

	
	//////////////////////////////////////////////////////////////////////////
	// File Save Start
	WriteResultObj.m_fnDataFileSave(MasterToCIM_Result_PATH_Ret, 
		CString(EQP_FLOW_ID)/*m_CIMInterface.m_fnGetMaterialInfo(FLOWID)*/, 
		m_CIMInterface.m_fnGetMaterialInfo(STEPID),
		strStickID);	
	// File Save End
	//////////////////////////////////////////////////////////////////////////
	//Copy Result File
	CopyResult_Folder(strStickID);
	/////////////////////////////////////////////////////////////////////////
	return true;
}
void CVS10MasterDlg::OnBnClickedBtSeqStart()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();

	if(CheckAllRunSequence() ==  true)
	{
		AfxMessageBox("Now Run Sequence Exist!!!");
		return;
	}

	switch(wID)
	{
	case IDC_BT_SEQ_START_1:
		{
			if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1 ||
				G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1)
			{//Stick이 Gripper에 

				if(CheckAllRunSequence() ==  true)
				{
					AfxMessageBox("Now Run Sequence Exist!!!");
					return;
				}
				if(AfxMessageBox("Stick을 Unload하시겠습니까?", MB_YESNO) ==IDYES)
					m_SeqObj_StickExchange.fnSeqFlowUnloadStart();
				else
					return;
			}
			else if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 0 ||
				G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 0)
			{
				m_SeqObj_StickExchange.fnSeqFlowLoadStart();
			}
		}break;
	case IDC_BT_SEQ_START_2:
		{
			if(m_SubCIMInterfaceView->m_EQST_Mode ==  EQP_NORMAL)
				m_SeqObj_ThetaAlign.fnSeqFlowStart();
		}break;
	case IDC_BT_SEQ_START_3:
		{
			m_SeqObj_CDTP.fnSeqFlowStart();
		}break;
	case IDC_BT_SEQ_START_4:
		{
			m_SeqObj_AreaScan3D.fnSeqFlowStart();
		}break;
	case IDC_BT_SEQ_START_5:
		{
			m_SeqObj_3DScope.fnSeqFlowStart();
		}break;
	case IDC_BT_SEQ_START_6:
		{
			m_SeqObj_InsMic.fnSeqFlowStart();
		}break;
	case IDC_BT_SEQ_START_7:
		{
			if(m_SubCIMInterfaceView->m_EQST_Mode ==  EQP_NORMAL)//Manual Mode에서 Semi Auto로 사용하자.
				m_SeqObj_AutoRun.fnSeqFlowStart();
		}break;
	case IDC_BT_SEQ_START_8:
		{
			m_SeqObj_CellAlign.fnSeqFlowStart();
		}break;
	case IDC_BT_SEQ_START_9:
		{
			m_SeqObj_InitAM01.fnSeqFlowStart();
		}break;
	case IDC_BT_SEQ_START_10:
		{
			m_SeqObj_InsMac.fnSeqFlowStart();
		}break;
	}
}


void CVS10MasterDlg::OnBnClickedBtSeqStop()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();
	switch(wID)
	{
	case IDC_BT_SEQ_STOP_1:
		{
			if(m_SeqObj_StickExchange.m_NowStep == CSeq_StickExchange::SEQ_STEP_ERROR)
				m_SeqObj_StickExchange.fnSeqErrorReset();
			else
				m_SeqObj_StickExchange.fnSeqSetStop();
		}break;
	case IDC_BT_SEQ_STOP_2:
		{
			if(m_SeqObj_ThetaAlign.m_NowStep == CSeq_ThetaAlign::SEQ_STEP_ERROR)
				m_SeqObj_ThetaAlign.fnSeqErrorReset();
			else
				m_SeqObj_ThetaAlign.fnSeqSetStop();
		}break;
	case IDC_BT_SEQ_STOP_3:
		{
			if(m_SeqObj_CDTP.m_NowStep == CSeq_CDTP::SEQ_STEP_ERROR)
				m_SeqObj_CDTP.fnSeqErrorReset();
			else
				m_SeqObj_CDTP.fnSeqSetStop();

			m_LiveViewObj.DrawCDTPReviewInfo(false);
		}break;
	case IDC_BT_SEQ_STOP_4:
		{
			if(m_SeqObj_AreaScan3D.m_NowStep == CSeq_AreaScan3D::SEQ_STEP_ERROR)
				m_SeqObj_AreaScan3D.fnSeqErrorReset();
			else
				m_SeqObj_AreaScan3D.fnSeqSetStop();
		}break;
	case IDC_BT_SEQ_STOP_5:
		{
			if(m_SeqObj_3DScope.m_NowStep == CSeq_3DScope::SEQ_STEP_ERROR)
				m_SeqObj_3DScope.fnSeqErrorReset();
			else
				m_SeqObj_3DScope.fnSeqSetStop();
		}break;
	case IDC_BT_SEQ_STOP_6:
		{
			if(m_SeqObj_InsMic.m_NowStep == CSeq_Inspect_Mic::SEQ_STEP_ERROR)
				m_SeqObj_InsMic.fnSeqErrorReset();
			else
				m_SeqObj_InsMic.fnSeqSetStop();
			//Auto Review중이었다면 화면 정리도
			m_LiveViewObj.DrawAutoReviewInfo(false);
			//Defect Review End
			::SendMessage(AfxGetMainWnd()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 9919);
		}break;
	case IDC_BT_SEQ_STOP_7:
		{
			if(m_SeqObj_AutoRun.m_NowStep == CSeq_AutoRun::SEQ_STEP_ERROR)
				m_SeqObj_AutoRun.fnSeqErrorReset();
			else
				m_SeqObj_AutoRun.fnSeqSetStop();
		}break;
	case IDC_BT_SEQ_STOP_8:
		{
			if(m_SeqObj_CellAlign.m_NowStep == CSeq_CellAlign::SEQ_STEP_ERROR)
				m_SeqObj_CellAlign.fnSeqErrorReset();
			else
				m_SeqObj_CellAlign.fnSeqSetStop();
		}break;

	case IDC_BT_SEQ_STOP_9:
		{
			if(m_SeqObj_InitAM01.m_NowStep == CSeq_InitAM01::SEQ_STEP_ERROR)
				m_SeqObj_InitAM01.fnSeqErrorReset();
			else
				m_SeqObj_InitAM01.fnSeqSetStop();
		}break;
	case IDC_BT_SEQ_STOP_10:
		{
			if(m_SeqObj_InsMac.m_NowStep == CSeq_Inspect_Mac::SEQ_STEP_ERROR)
				m_SeqObj_InsMac.fnSeqErrorReset();
			else
				m_SeqObj_InsMac.fnSeqSetStop();
			//Auto Review중이었다면 화면 정리도
			m_LiveViewObj.DrawAutoReviewInfo(false);
			//Defect Review End
			::SendMessage(AfxGetMainWnd()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 9919);
		}break;
	}
}






void CVS10MasterDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	__super::OnLButtonDown(nFlags, point);
}


void CVS10MasterDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	__super::OnLButtonUp(nFlags, point);
}


void CVS10MasterDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CRect ViewMapSize;
	GetDlgItem(IDC_ST_LIVE_VIEW_AREA)->GetWindowRect(&ViewMapSize);
	ScreenToClient(&ViewMapSize);

	CString strNowPos;
	if(ViewMapSize.PtInRect(point) == TRUE)
	{


		//HCURSOR hCursor = AfxGetApp()->LoadStandardCursor(NULL); 
		//SetCursor(hCursor); 

		//GetDlgItem(IDC_BT_TEST)->SetFocus();
		point.x -= ViewMapSize.left;
		point.y -= ViewMapSize.top;
		CvPoint NewDrawPos;
		NewDrawPos.x = (((float)point.x*REAL_IMAGE_SIZE_X)/(float)ViewMapSize.Width());
		NewDrawPos.y = (((float)point.y*REAL_IMAGE_SIZE_Y)/(float)ViewMapSize.Height());

		m_LiveViewObj.m_MousePos = NewDrawPos;

		CvPoint CenterToDis;
		CenterToDis.x = NewDrawPos.x-(REAL_IMAGE_SIZE_X/2);
		CenterToDis.y = NewDrawPos.y-(REAL_IMAGE_SIZE_Y/2);

		int MovePosShift = 0;// G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanShift];
		int MovePosDrive = 0;//G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanDrive];

		switch(m_NowLensIndex_Review)
		{
		case REVIEW_LENS_2X:
			{
				MovePosShift+=(int)(m_ReviewLensOffset.Lens02xPixelSizeX*(float)CenterToDis.x);
				MovePosDrive+=(int)(m_ReviewLensOffset.Lens02xPixelSizeY*(float)CenterToDis.y);
			}break;
		case REVIEW_LENS_10X:
			{
				MovePosShift+=(int)(m_ReviewLensOffset.Lens10xPixelSizeX*(float)CenterToDis.x);
				MovePosDrive+=(int)(m_ReviewLensOffset.Lens10xPixelSizeY*(float)CenterToDis.y);
			}break;
		case REVIEW_LENS_20X:
			{
				MovePosShift+=(int)(m_ReviewLensOffset.Lens20xPixelSizeX*(float)CenterToDis.x);
				MovePosDrive+=(int)(m_ReviewLensOffset.Lens20xPixelSizeY*(float)CenterToDis.y);
			}break;
		case REVIEW_LENS_NONE:
		default:
			MovePosShift+=(int)(m_ReviewLensOffset.Lens20xPixelSizeX*(float)CenterToDis.x);
			MovePosDrive+=(int)(m_ReviewLensOffset.Lens20xPixelSizeY*(float)CenterToDis.y);
			break;
		}
		m_LiveViewObj.m_ReviewStagePos.x = MovePosShift;
		m_LiveViewObj.m_ReviewStagePos.y = MovePosDrive;
	}
	else
	{
		//HCURSOR hCursor= AfxGetApp()->LoadStandardCursor(IDC_ARROW); 
		//SetCursor(hCursor); 
	}
	__super::OnMouseMove(nFlags, point);
}
bool CVS10MasterDlg::CheckAllRunSequence()
{

	if(m_SeqObj_StickExchange.m_NowStep != CSeq_StickExchange::SEQ_STEP_IDLE )
		return true;

	if(m_SeqObj_ThetaAlign.m_NowStep != CSeq_ThetaAlign::SEQ_STEP_IDLE )
		return true;

	if(m_SeqObj_CDTP.m_NowStep != CSeq_CDTP::SEQ_STEP_IDLE )
		return true;

	if(m_SeqObj_AreaScan3D.m_NowStep != CSeq_AreaScan3D::SEQ_STEP_IDLE  )
		return true;

	if(m_SeqObj_3DScope.m_NowStep != CSeq_3DScope::SEQ_STEP_IDLE )
		return true;

	if(m_SeqObj_InsMic.m_NowStep != CSeq_Inspect_Mic::SEQ_STEP_IDLE  )
		return true;

	if(m_SeqObj_InsMac.m_NowStep != CSeq_Inspect_Mac::SEQ_STEP_IDLE  )
		return true;

	if(m_SeqObj_AutoRun.m_NowStep != CSeq_AutoRun::SEQ_STEP_IDLE )
		return true;

	return false;
}

void CVS10MasterDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CRect ViewMapSize;
	GetDlgItem(IDC_ST_LIVE_VIEW_AREA)->GetWindowRect(&ViewMapSize);
	ScreenToClient(&ViewMapSize);

	CString strNowPos;
	if(ViewMapSize.PtInRect(point) == TRUE)
	{
		//HCURSOR hCursor = AfxGetApp()->LoadStandardCursor(NULL); 
		//SetCursor(hCursor); 

		//GetDlgItem(IDC_BT_TEST)->SetFocus();
		point.x -= ViewMapSize.left;
		point.y -= ViewMapSize.top;
		CvPoint NewDrawPos;
		if(m_LiveViewObj.m_bLiveViewOnOff == true)//Review로 보고 있을 때
		{
			NewDrawPos.x = (((float)point.x*REAL_IMAGE_SIZE_X)/(float)ViewMapSize.Width());
			NewDrawPos.y = (((float)point.y*REAL_IMAGE_SIZE_Y)/(float)ViewMapSize.Height());
			//m_LiveViewObj.m_MousePos = NewDrawPos;


			CvPoint CenterToDis;
			CenterToDis.x = NewDrawPos.x-(REAL_IMAGE_SIZE_X/2);
			CenterToDis.y = NewDrawPos.y-(REAL_IMAGE_SIZE_Y/2);

			int MovePosShift = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanShift];
			int MovePosDrive = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScanDrive];

			bool bMovePosCheckOk= true;
			switch(m_NowLensIndex_Review)
			{
			case REVIEW_LENS_2X:
				{
					MovePosShift+=(int)(m_ReviewLensOffset.Lens02xPixelSizeX*(float)CenterToDis.x);
					MovePosDrive+=(int)(m_ReviewLensOffset.Lens02xPixelSizeY*(float)CenterToDis.y);
				}break;
			case REVIEW_LENS_10X:
				{
					MovePosShift+=(int)(m_ReviewLensOffset.Lens10xPixelSizeX*(float)CenterToDis.x);
					MovePosDrive+=(int)(m_ReviewLensOffset.Lens10xPixelSizeY*(float)CenterToDis.y);
				}break;
			case REVIEW_LENS_20X:
				{
					MovePosShift+=(int)(m_ReviewLensOffset.Lens20xPixelSizeX*(float)CenterToDis.x);
					MovePosDrive+=(int)(m_ReviewLensOffset.Lens20xPixelSizeY*(float)CenterToDis.y);
				}break;

			case REVIEW_LENS_NONE:
			default:
				bMovePosCheckOk = false;
				break;
			}
			if(CheckAllRunSequence() == true)//Sequenc가 돌고 있으면.. 구동 하지 않느다.
				bMovePosCheckOk = false;

			if(bMovePosCheckOk == true)
			{
				G_MotObj.m_fnScanMultiMove(MovePosShift, MovePosDrive);
				//G_MotObj.m_fnAM01ReviewSyncMove(MovePosShift, MovePosDrive, Sync_Scope_3DScope);
			}
		}
		else if(m_ThetaAlignLiveObj.GetCamLiveMode() == LIVE_MODE_VIEW)
		{
			NewDrawPos.x = (((float)point.x*LIVE_SIZE_W)/(float)ViewMapSize.Width());
			NewDrawPos.y = (((float)point.y*LIVE_SIZE_H)/(float)ViewMapSize.Height());
			//m_LiveViewObj.m_MousePos = NewDrawPos;


			CvPoint CenterToDis;
			CenterToDis.x = (LIVE_SIZE_W/2)-NewDrawPos.x;//리뷰와 계산 방향 반대
			CenterToDis.y = NewDrawPos.y-(LIVE_SIZE_H/2);

			int MovePosShift = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeShift];
			int MovePosDrive = G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_ScopeDrive];

			bool bMovePosCheckOk= true;

			MovePosShift+=(int)(IMG_PIXEL_SIZE_W*(float)CenterToDis.x);
			MovePosDrive+=(int)(IMG_PIXEL_SIZE_H*(float)CenterToDis.y);

			if(CheckAllRunSequence() == true)//Sequenc가 돌고 있으면.. 구동 하지 않느다.
				bMovePosCheckOk = false;

			if(bMovePosCheckOk == true)
			{
				G_MotObj.m_fnScopeMultiMove(MovePosShift, MovePosDrive);
			}
		}
	}
	else
	{
		//HCURSOR hCursor= AfxGetApp()->LoadStandardCursor(IDC_ARROW); 
		//SetCursor(hCursor); 
	}
	__super::OnLButtonDblClk(nFlags, point);
}


void CVS10MasterDlg::OnBnClickedBtDoorFront()
{
	
		if(m_btUnlockFrontDoor.GetCheck() == true)//체크가 되어있으면 잠금 버튼 
		{
			G_MotObj.m_fnDoorLockFront();
		}
		else//체크가 풀려있으면 해지 버튼 
		{
			CPassWord InputData;
			if(InputData.DoModal() == IDOK)
				G_MotObj.m_fnDoorReleasFront();
		}
}


void CVS10MasterDlg::OnBnClickedBtDoorEqSide()
{
	if(m_btUnlockSideDoor.GetCheck() == true)//체크가 되어있으면 잠금 버튼 
	{
		G_MotObj.m_fnDoorLockSide();
	}
	else//체크가 풀려있으면 해지 버튼 
	{
		CPassWord InputData;
		if(InputData.DoModal() == IDOK)
			G_MotObj.m_fnDoorReleasSide();
	}
}


void CVS10MasterDlg::OnBnClickedBtDoorBack()
{
	if(m_btUnlockBackDoor.GetCheck() == true)//체크가 되어있으면 잠금 버튼 
	{
		G_MotObj.m_fnDoorLockBack();
	}
	else//체크가 풀려있으면 해지 버튼 
	{
		CPassWord InputData;
		if(InputData.DoModal() == IDOK)
			G_MotObj.m_fnDoorReleasBack();
	}
}

void CVS10MasterDlg::OnNMDblclkTrResult(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	HTREEITEM hItem;
	CString strName;

	hItem = m_treeResult.GetNextItem(NULL, TVGN_CARET); // 현재 선택되어진 놈의 핸들을 가져온다.

	if(m_treeResult.GetParentItem(hItem) == m_ResultRoot_InsMicScan)
	{//Inspection Defect 항목을 더블 클릭 한경우 이동.?
		//COORD_DINFO DataBuf;
		//IplImage *pSmallImg = (IplImage *)m_treeResult.GetItemData(hItem);
		
		//if(pSmallImg != nullptr)
	//	{
			//cvNamedWindow("SMALL_Defect_Review");
			//cvShowImage("SMALL_Defect_Review", (pSmallImg));
			//cvWaitKey(100);
		//}
	}

	else if(m_treeResult.GetParentItem(hItem) == m_ResultRoot_InsMacScan)
	{//Inspection Defect 항목을 더블 클릭 한경우 이동.?
		//COORD_DINFO DataBuf;
		//IplImage *pSmallImg = (IplImage *)m_treeResult.GetItemData(hItem);

		//if(pSmallImg != nullptr)
		//	{
		//cvNamedWindow("SMALL_Defect_Review");
		//cvShowImage("SMALL_Defect_Review", (pSmallImg));
		//cvWaitKey(100);
		//}
	}

}


void CVS10MasterDlg::OnBnClickedRsetMotCmdAll()
{
	G_WriteInfo(Log_Normal, "OnBnClickedRsetMotCmdAll()");
	G_MotObj.m_fnResetAllCMDRun();
}

void CVS10MasterDlg::OnBnClickedBtBuzzerStop()
{
	G_MotObj.m_fnSetSignalTower_StopBuz();
}
void CVS10MasterDlg::OnBnClickedChDrawOverlay()
{
	if( IsDlgButtonChecked(IDC_CH_DRAW_OVERLAY) ==BST_CHECKED)
		m_LiveViewObj.m_bSaveOverlayImage = true;
	else
		m_LiveViewObj.m_bSaveOverlayImage = false;
}
void CVS10MasterDlg::OnBnClickedBtSaveReviewImg()
{
	if(m_LiveViewObj.m_bLiveViewOnOff == true)//Review로 보고 있을 때
	{
		IplImage *pTest1 = nullptr;
		m_LiveViewObj.SaveImage(&pTest1);
		while(pTest1 == nullptr)
		{
			Sleep(100);
		}
		CString strName;
		strName.Format("D:\\SaveReview_Img%d.bmp", GetTickCount());
		cvSaveImage(strName.GetBuffer(strName.GetLength()), pTest1);
		if(pTest1 != nullptr)
			cvReleaseImage(&pTest1);
		pTest1 = nullptr;
	}
	else if(m_ThetaAlignLiveObj.GetCamLiveMode() == LIVE_MODE_VIEW)
	{
		IplImage *pTest1 = m_ThetaAlignLiveObj.GetGrabImg();
		if(pTest1 != nullptr)
		{
			CString strName;
			strName.Format("D:\\SaveThAlign_Img%d.bmp", GetTickCount());
			cvSaveImage(strName.GetBuffer(strName.GetLength()), pTest1);
		}
		
	}
}

void CVS10MasterDlg::OnBnClickedButton3()
{

	//m_ServerInterface.m_fnSendCommand_Nrs(	TASK_21_Inspector,  nBiz_AOI_Inspection, nBiz_Send_BigInspect, 0);
	//m_ServerInterface.m_fnSendCommand_Nrs(	TASK_21_Inspector,  nBiz_AOI_Inspection, nBiz_Send_BigSaveImg, 0);
	G_MotObj.m_fnBuzzer2On(3000);
	//CSubStickLoadDlg LoadSelectDlg;
	////LoadSelectDlg.SetStickInfo(CString(m_StickLoadInfo.strBoxID), CString(m_StickLoadInfo.strStickID), CString(""));
	//LoadSelectDlg.SetStickInfo(CString("AAAAAAAAAAAAA"), CString("BBBBBBBBBBBBB"), CString(""));
	//LoadSelectDlg.DoModal();

/////////////////////////////////////////////////////////////////////////////
	//OnBnClickedBtLiveViewOnoff();
//////////////////////////////////////////////////////////////////////////////
	//char strReadData[256];
	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	//SetResult_Folder(CString(strReadData));
	//CopyResult_Folder(CString(strReadData));
/////////////////////////////////////////////////////////////////////////
	//char MsgBuf[1500];
	//memset(MsgBuf, 0x00, 1500);
	//::SendMessage(this->GetSafeHwnd(), WM_USER_RECV_HOST_MSG, 2,(LPARAM)MsgBuf);
//////////////////////////////////////////////////////////////////////////
	//cvNamedWindow("TEST1", CV_WINDOW_NORMAL);
	//cvShowImage("TEST1", m_ThetaAlignLiveObj.GetGrabImg());
	//cvWaitKey(0);
/////////////////////////////////////////////////////////////////////////

	//CPassWord InputData;
	//InputData.DoModal();
	//m_SubThetaAlignView->ShowWindow(SW_SHOW);
//////////////////////////////////////////////////////////////////////////
	//m_CIMInterface.m_fnSendECID_ChangeCmdAck("4=50=100=150=200=400");
	//::SendMessageA(G_CIM_I_value, WM_USER_ECID_CHANGE,0,0);

//////////////////////////////////////////////////////////////////////////
	//WriteMeasureAllResult();
//////////////////////////////////////////////////////////////////////////
	//return;
	//CPMDlg dlg;
	//dlg.DoModal();
	
	//m_SubAlarmClearView->m_fnAlarmListAdd(AL_1009);
	//m_SubAlarmClearView->m_fnAlarmListAdd(AL_1002);
//m_AlarmClearDlg.m_fnInit();

//		
//////////////////////////////////////////////////////////////////////////
	//float fpixelSizeX = m_ReviewLensOffset.Lens02xPixelSizeX;
	//float fpixelSizeY = m_ReviewLensOffset.Lens02xPixelSizeY;
	//if(m_btReviewLens[0].GetCheck() == true)//2x
	//{
	//	fpixelSizeX = m_ReviewLensOffset.Lens02xPixelSizeX;
	//	fpixelSizeY = m_ReviewLensOffset.Lens02xPixelSizeY;
	//}
	//else if(m_btReviewLens[1].GetCheck() == true)//10x
	//{
	//	fpixelSizeX = m_ReviewLensOffset.Lens02xPixelSizeX;
	//	fpixelSizeY = m_ReviewLensOffset.Lens02xPixelSizeY;
	//}
	//else if(m_btReviewLens[2].GetCheck() == true)//20x
	//{
	//	fpixelSizeX = m_ReviewLensOffset.Lens02xPixelSizeX;
	//	fpixelSizeY = m_ReviewLensOffset.Lens02xPixelSizeY;
	//}
	//else
	//	return;//Lens 선택오류

	//IplImage *pTest1 = nullptr;
	//m_LiveViewObj.SaveImage(&pTest1);
	//while(pTest1 == nullptr)
	//{
	//	Sleep(100);
	//}	
	////double Angle = 	getObjectAngle(pTest1);
	//IplImage *pTest2= cvCloneImage(pTest1);
	////rotateImage(pTest1, pTest2, -0.5);
	//ScaleMeasuer(pTest2, fpixelSizeX, fpixelSizeY);

	//if(pTest2 != nullptr)
	//	cvReleaseImage(&pTest2);
	//pTest2 = nullptr;

	//if(pTest1 != nullptr)
	//	cvReleaseImage(&pTest1);
	//pTest1 = nullptr;
//////////////////////////////////////////////////////////////////////////
	//cvNamedWindow("TEST1", CV_WINDOW_NORMAL);
	//IplImage *pTest1 = cvLoadImage("D:\\CD_Test.bmp");

	//
	//CvRect FindResult;
	//CvPoint CellRibPixel;
	//CvPoint CellEdge;
	//FindResult= m_SeqObj_CDTP.CDTPImgProc(&m_CDTPParam, &m_pCDTPPointList[0], pTest1, &CellRibPixel, &CellEdge);//Left Top
	//m_SeqObj_CDTP.CDTPDrawResult(pTest1, FindResult, CellEdge, &m_pCDTPPointList[0]);

	//cvSaveImage("D:\\CD_Test_Result.bmp", pTest1);
	////cvShowImage("TEST1", pTest1);
	////cvWaitKey(0);
	//if(pTest1 != nullptr)
	//	cvReleaseImage(&pTest1);
	//pTest1 = nullptr;

	//G_MotObj.m_fnQRCodeRead();
	//////////////////////////////////////////////////////////////////////////

	//CString strImageOpenPath;
	//strImageOpenPath.Format("%s\\%s\\D_Insp_T%d.img", VS21Inspector_Result_PATH, LOCAL_RESULT_FOLDER, TASK_21_Inspector);
	//SetSmallDefectImg(strImageOpenPath, nullptr);
	//IplImage *pTest1 = nullptr;
	//m_LiveViewObj.SaveImage(&pTest1);
	//while(pTest1 == nullptr)
	//{
	//	Sleep(100);
	//}
	//COORD_DINFO m_NowReviewData;
	//m_SeqObj_InsAutoReview.WriteDefectInfo(100, 1, "123", "dsdfsdfs", pTest1,&m_NowReviewData, cvPoint(100,100) );

	//cvNamedWindow("TEST1", CV_WINDOW_NORMAL);
	//cvShowImage("TEST1", pTest1);
	//cvWaitKey();
	//CString strName;
	//strName.Format("D:\\Save_Img%d.bmp", GetTickCount());
	//cvSaveImage(strName.GetBuffer(strName.GetLength()), pTest1);
	//if(pTest1 != nullptr)
	//	cvReleaseImage(&pTest1);
	//pTest1 = nullptr;
//////////////////////////////////////////////////////////////////////////
	//IplImage *pTest1 = nullptr;
	//pTest1= cvCreateImage(cvSize(577280, 5000), IPL_DEPTH_8U, 3);
	//cvZero(pTest1);
	//CString strName;
	//strName.Format("D:\\Save_Img%d.bmp", GetTickCount());
	//cvSaveImage(strName.GetBuffer(strName.GetLength()), pTest1);
	//if(pTest1 != nullptr)
	//	cvReleaseImage(&pTest1);
	//pTest1 = nullptr;
	//////////////////////////////////////////////////////////////////////////

	
}

void CVS10MasterDlg::OnBnClickedButton2()
{ 
	//IplImage *pSmallImg = cvCreateImage(cvSize(300,300), IPL_DEPTH_8U, 1);
	//IplImage *AutoReviewSmallImg= cvCreateImage(cvSize(600,600), IPL_DEPTH_8U, 3);
	//cvZero(pSmallImg);
	//cvZero(AutoReviewSmallImg);
	//cvConvertImage(pSmallImg, AutoReviewSmallImg);
	//return;
	//m_LiveViewObj.SetAutoRevewInfo(0, 0, 
	//	"AAAAAAAAA", "BBBBBBBBBBBBBBBBBB", nullptr, 
	//	cvPoint(0, 0));

	//m_LiveViewObj.DrawAutoReviewInfo(true);

	//return;
	//G_MotObj.m_fnSetSignalTower(STWOER_MODE2_IDLE1);

	//G_MotObj.m_fnSetSignalTower_StopBuz();
#ifdef SIMUL_CELL_ALIGN_CALC_VIEW

	IplImage *pTest1 = nullptr;
	CvRect FindResult;
	m_LiveViewObj.SaveImage(&pTest1);
	while(pTest1 == nullptr)
	{
		Sleep(100);
	}
	CvPoint CellEdge;
	CellEdge = m_SeqObj_CellAlign.AlignImgProc(pTest1);
	cvShowImage("TEST1", pTest1);
	cvWaitKey(0);
	if(pTest1 != nullptr)
		cvReleaseImage(&pTest1);
	pTest1 = nullptr;
#endif
//#ifdef SIMUL_CDTP_CALC_VIEW
	cvNamedWindow("TEST1", CV_WINDOW_NORMAL);
	IplImage *pTest1 = nullptr;
	CvRect FindResult;
	CvRect FindIndexRib_LR;
	CvRect FindIndexRib_UD;
	//m_LiveViewObj.SaveImage(&pTest1);
	//while(pTest1 == nullptr)
	//{
	//	Sleep(100);
	//}
	pTest1 = cvLoadImage("D:\\Left_top.jpg");
	//pTest1 = cvLoadImage("D:\\Right_top.jpg");
	//pTest1 = cvLoadImage("D:\\TP_DCW1L18GRN150129_999.001.jpg");
	
	//m_SeqObj_CDTP.Find_TP_Center(&m_CDTPParam, &m_pCDTPPointList[0], pTest1, &m_ReviewLensOffset);
	CvPoint CellEdge;
	FindResult = m_SeqObj_CDTP.CDTPImgProc(&m_CDTPParam, &m_pCDTPPointList[0], pTest1, &CellEdge, &FindIndexRib_LR, &FindIndexRib_UD);
	if(FindResult.width>0 && FindResult.height>0)
		m_SeqObj_CDTP.CDTPDrawResult(pTest1, FindResult, FindIndexRib_LR, FindIndexRib_UD,CellEdge, &m_pCDTPPointList[0]);
	cvShowImage("TEST1", pTest1);
	cvWaitKey(0);
	if(pTest1 != nullptr)
		cvReleaseImage(&pTest1);
	pTest1 = nullptr;
	//m_LiveViewObj.SaveImage(&pTest1);
	//while(pTest1 == nullptr)
	//{
	//	Sleep(100);
	//}
	//pTest1 = cvLoadImage("E:\\MasterShared\\[04_Master]\\CDTP_A3MSI01N_CIM_TEST_BOX_DCW1L18GRN150129_999\\CD_DCW1L18GRN150129_999.001.jpg");

	//FindResult= m_SeqObj_CDTP.CDTPImgProc(&m_CDTPParam, &m_pCDTPPointList[1], pTest1, &CellRibPixel, &CellEdge);//Right Top
	//if(FindResult.width>0 && FindResult.height>0)
	//	m_SeqObj_CDTP.CDTPDrawResult(pTest1, FindResult, CellEdge, &m_pCDTPPointList[1]);
	//cvShowImage("TEST1", pTest1);
	//cvWaitKey(0);

	//if(pTest1 != nullptr)
	//	cvReleaseImage(&pTest1);
	//pTest1 = nullptr;
	////m_LiveViewObj.SaveImage(&pTest1);
	////while(pTest1 == nullptr)
	////{
	////	Sleep(100);
	////}
	//pTest1 = cvLoadImage("E:\\MasterShared\\[04_Master]\\CDTP_A3MSI01N_CIM_TEST_BOX_DCW1L18GRN150129_999\\CD_DCW1L18GRN150129_999.001.jpg");
	//FindResult= m_SeqObj_CDTP.CDTPImgProc(&m_CDTPParam, &m_pCDTPPointList[2], pTest1, &CellRibPixel, &CellEdge);//Right buttom
	//if(FindResult.width>0 && FindResult.height>0)
	//	m_SeqObj_CDTP.CDTPDrawResult(pTest1, FindResult, CellEdge, &m_pCDTPPointList[2]);
	//cvShowImage("TEST1", pTest1);
	//cvWaitKey(0);
	//if(pTest1 != nullptr)
	//	cvReleaseImage(&pTest1);
	//pTest1 = nullptr;
	//m_LiveViewObj.SaveImage(&pTest1);
	//while(pTest1 == nullptr)
	//{
	//	Sleep(100);
	//}
	//pTest1 = cvLoadImage("E:\\MasterShared\\[04_Master]\\CDTP_A3MSI01N_CIM_TEST_BOX_DCW1L18GRN150129_999\\CD_DCW1L18GRN150129_999.001.jpg");
	//FindResult= m_SeqObj_CDTP.CDTPImgProc(&m_CDTPParam, &m_pCDTPPointList[3], pTest1, &CellRibPixel, &CellEdge);//left buttom
	//if(FindResult.width>0 && FindResult.height>0)
	//	m_SeqObj_CDTP.CDTPDrawResult(pTest1, FindResult, CellEdge, &m_pCDTPPointList[3]);
	//cvShowImage("TEST1", pTest1);
	//cvWaitKey(0);
	//////cvSaveImage("D:\\TestImg.bmp",pTest1);

	//cvShowImage("TEST1", pTest1);
	//cvWaitKey(0);

	//if(pTest1 != nullptr)
	//	cvReleaseImage(&pTest1);
	//pTest1 = nullptr;
//#endif


	//IplImage *pTest1 = cvLoadImage("D:\\20150222_ThetaAlignImg_1.bmp",CV_LOAD_IMAGE_GRAYSCALE);
	//IplImage *pTest2 = cvLoadImage("D:\\20150222_ThetaAlignImg_2.bmp",CV_LOAD_IMAGE_GRAYSCALE);
	//int Pos1 = m_SeqObj_ThetaAlign.AlignImgProc_DetectLine(pTest1);
	//int Pos2 = m_SeqObj_ThetaAlign.AlignImgProc_DetectLine(pTest2);


	//cvDrawLine(pTest1,cvPoint(Pos1, 0), cvPoint(Pos1, pTest1->height-1),CV_RGB(255, 0, 0));
	//cvDrawLine(pTest2,cvPoint(Pos2, 0), cvPoint(Pos2, pTest2->height-1),CV_RGB(255, 0, 0));


	//cvNamedWindow("TEST1");
	//cvNamedWindow("TEST2");
	//cvShowImage("TEST1", pTest1);
	//cvShowImage("TEST2", pTest2);
	//cvWaitKey(0);
	//cvReleaseImage(&pTest1);
	//cvReleaseImage(&pTest2);
}

void CVS10MasterDlg::OnBnClickedButton6()
{
	if(AfxMessageBox("Tension Set Zero?", MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDNO)
		return;
	G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_ALL_TENSION_ZeroSet>");
	//2)Grip Close
	G_MotObj.m_fnStickTensionZeroSet();
	//int TimeOutWait = 300;//About 30sec
	//while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_ZERO].B_MOTCMD_State != IDLE )
	//{
	//	TimeOutWait --;
	//	if(TimeOutWait<0)
	//	{
	//		G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//		break;
	//	}

	//	Sleep(10);
	//}
	//if(TimeOutWait<0)
	//{
	//	G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_ALL_TENSION_ZeroSet> Stop", );
	//}
	//if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_ZERO].ActionResult.nResult != VS24MotData::Ret_Ok)
	//{
	//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_ALL_TENSION_ZeroSet> ACC Run Error", );
	//}
	
}

void CVS10MasterDlg::OnBnClickedButton4()
{
	if(AfxMessageBox("Stick Tension Start?", MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDNO)
		return;
		
	G_WriteInfo(Log_Normal, "<STICK_TENSION Test>" );
	//상위에서 받은것
	int StickLoadOffset = m_StickLoadInfo.nStickLoadOffset;
	int StickLoadSize= StickLoadOffset+m_GlassReicpeParam.STICK_WIDTH;
	//인장 Mot 선택.
	int SelectMotCnt = StickLoadSize/(TENSION_CLEE_WIDTH*TENSION_CLEE_CNT);
	SelectMotCnt += (StickLoadSize%(TENSION_CLEE_WIDTH*TENSION_CLEE_CNT)>0)?1:0;

	int SelectMot = 0;
	switch(SelectMotCnt)
	{
	case 1: SelectMot = 0x01; break;
	case 2: SelectMot = (0x01 | 0x02); break;
	case 3: SelectMot = (0x01 | 0x02 | 0x04); break;
	case 4: SelectMot = (0x01 | 0x02 | 0x04 | 0x08); break;
	default: 
		SelectMot = 0;
		break;
	}
	if(m_StickTentionValue<0.0)
	{
		G_WriteInfo(Log_Check, "<SEQ_STEP_START_STICK_TENSION%03d> Stick Value Error");
		return;
	}
	if(SelectMot>0)
		//1)Grip Tension Start
		G_MotObj.m_fnAM01GripperTensionStart(true, SelectMot, m_StickTentionValue);
	else
		G_WriteInfo(Log_Error, "<SEQ_STEP_START_STICK_TENSION%03d> Tension Axis Select Error");
	//int TimeOutWait = 3000;//About 30sec
	//while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State != IDLE ||
	//	G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State != IDLE )
	//{
	//	TimeOutWait --;
	//	if(TimeOutWait<0)
	//	{
	//		G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
	//		break;
	//	}
	//	Sleep(10);
	//}
	//if(TimeOutWait<0)
	//{
	//	G_MotObj.m_fnAM01GripperTensionStart(false, m_StickTentionValue);//오류로 인한 인장 중지.
	//	G_WriteInfo(Log_Check, "<SEQ_STEP_START_STICK_TENSION> Stop");
	//}
	//if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].ActionResult.nResult != VS24MotData::Ret_Ok ||
	//	G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].ActionResult.nResult != VS24MotData::Ret_Ok)
	//{
	//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_START_STICK_TENSION> Mot Run Error");
	//}
}


void CVS10MasterDlg::OnBnClickedButton5()
{
	G_MotObj.m_fnAM01GripperTensionStart(false, 0, m_StickTentionValue);//오류로 인한 인장 중지.
	G_WriteInfo(Log_Exception, "<SEQ_STEP_START_STICK_TENSION> Stop");
}

void CVS10MasterDlg::OnStnDblclickStTask_Inspector()
{
	G_WriteInfo(Log_Exception, "Restart OnStnDblclickStTask_Inspector()");
}
void CVS10MasterDlg::OnStnDblclickStTask_Indexer()
{
	G_WriteInfo(Log_Exception, "Restart OnStnDblclickStTask_Indexer()");
}
void CVS10MasterDlg::OnStnDblclickStTask_Measure()
{
	G_WriteInfo(Log_Exception, "Restart OnStnDblclickStTask_Measure()");
}
void CVS10MasterDlg::OnStnDblclickStTask_Motion()
{
	G_WriteInfo(Log_Exception, "Restart OnStnDblclickStTask_Motion()");
}
void CVS10MasterDlg::OnStnDblclickStTask_3DScan()
{
	G_WriteInfo(Log_Exception, "Restart OnStnDblclickStTask_3DScan()");
}
void CVS10MasterDlg::OnStnDblclickStTask_AF()
{
	G_WriteInfo(Log_Exception, "Restart OnStnDblclickStTask_AF()");
}



void CVS10MasterDlg::OnBnClickedButton7()
{
	WriteMeasureAllResult();
}
