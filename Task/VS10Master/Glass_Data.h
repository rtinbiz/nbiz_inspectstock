#pragma once



#define	MASTER_TASK_NUM	10


#define  Section_Options		"Options"	
	#define  Key_USE_Mask_GorR		"USE_Mask_GorR"//1: Global, 2: Recipe
	#define  Key_USE_Mask_GSkip		"USE_Mask_GSkip"//0: G Off, 1: G On
	#define  Key_USE_Mask_RSkip		"USE_Mask_RSkip"//0: R Off, 1: R On

	#define  Key_USE_Comm_GorR		"USE_Comm_GorR"//1: Global, 2: Recipe
	#define  Key_USE_Comm_GSkip		"USE_Comm_GSkip"//0: G Off, 1: G On
	#define  Key_USE_Comm_RSkip		"USE_Comm_RSkip"//0: R Off, 1: R On

#define  Section_Value		"Value"	
#define  Key_USE_Mask_CODE		"Mask_CODE"
#define  Key_USE_Mask_Dis		"Mask_Dis"
#define  Key_USE_Mask_Cnt		"Mask_Cnt"
#define  Key_USE_Comm_CODE		"Comm_CODE"
#define  Key_USE_Comm_Dis		"Comm_Dis"
#define  Key_USE_Comm_Cnt		"Comm_Cnt"
#define  Key_USE_DevName	"DevName"

#define  Section_GLOBAL		"Global"	
#define  Section_Recipe		""
	#define  Key_ItemCnt		"ItemCnt"
	#define  Key_PosX_		"PosX_"
	#define  Key_PosY_	"PosY_"
	#define  Key_Radius_	"Radius_"

#define  SECTION_CELL_INFO										"CELL_INFO"
#define  KEYNAME_CENTER_TO_EDGE_X_UM				"Center to Edge X_um"
#define  KEYNAME_CENTER_TO_EDGE_Y_UM				"Center to Edge Y_um"
#define  KEYNAME_CENTER_TO_DIS_X_UM					"Center to Dis X_um"
#define  KEYNAME_CENTER_TO_DIS_Y_UM					"Center to Dis Y_um"
#define  KEYNAME_CELL_SIZE_WIDTH_UM					"Cell_Size_Width_um"
#define  KEYNAME_CELL_SIZE_HEIGHT_UM					"Cell_Size_Height_um"
#define  KEYNAME_CELL_COUNT_WIDTH						"Cell_Count_Width"
#define  KEYNAME_CELL_COUNT_HEIGHT					"Cell_Count_Heigh"
#define  KEYNAME_BLOCK_DIS_X_UM							"Block_Dis_X_um"
#define  KEYNAME_BLOCK_DIS_Y_UM							"Block_Dis_Y_um"
#define  KEYNAME_BLOCK_COUNT_X							"Block_Count X"
#define  KEYNAME_BLOCK_COUNT_Y							"Block_Count Y"


////////////////////////////////////////////////////////////////////////////
////Command
//#define nBiz_DMP_System		61
////Master to DDMP
//#define nBiz_Send_LotStart				1
//#define nBiz_Send_LoadTDData			2
//
////DDMP To Master
//#define nBiz_Recv_Mask_Error				10
//#define nBiz_Recv_Common_Error			11
//
//#define nBiz_Recv_TDLoad_Error				12
//#define nBiz_Recv_Alive						40

/** 
@brief		Glass 내의 Cell 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
//typedef struct Draw_CELLINFO_TAG
//{
//	char	 m_CellName[10];
//	float		 m_CellStartX;
//	float		 m_CellStartY;
//	float		 m_CellEndX;
//	float		 m_CellEndY;
//
//	Draw_CELLINFO_TAG()
//	{
//		memset(this,0x00, sizeof(Draw_CELLINFO_TAG));
//	}
//}Draw_CELLINFO;

/** 
@brief		Glass 내의 Defect 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
//typedef struct Draw_DEFECTINFO_TAG
//{
//	BOOL	m_Select;
//	int		m_GlassIndex;
//	char	 m_CellName[10];
//
//	float	 m_DefStartX;
//	float	 m_DefStartY;
//	float	 m_DefEndX;
//	float	 m_DefEndY;
//
//	float	 m_DefCoorX;
//	float	 m_DefCoorY;
//
//	Draw_DEFECTINFO_TAG()
//	{
//		memset(this,0x00, sizeof(Draw_DEFECTINFO_TAG));
//	}
//}Draw_DEFECTINFO;


/** 
@brief		Glass 내의 Cell Recipe 정보.

@remark
-			
-			
@author		최지형
@date		2012.00.00
*/
//typedef struct Draw_CELLRecipeINFO_TAG
//{
//	int CELL_EDGE_X;							//<<um단위 이다.
//	int CELL_EDGE_Y;							//<<um단위 이다.
//	int CELL_DISTANCE_X;						//<<um단위 이다.
//	int CELL_DISTANCE_Y;						//<<um단위 이다.
//	int CELL_SIZE_X;							//<<um단위 이다.
//	int CELL_SIZE_Y;							//<<um단위 이다.
//	//축 기준으로 몇개의 Cell이 있는지 나타 낸다.
//	int GLASS_CELL_COUNT_X;				
//	int GLASS_CELL_COUNT_Y;	
//
//	int BLOCK_DISTANCE_X;						//<<um단위 이다.
//	int BLOCK_DISTANCE_Y;						//<<um단위 이다.
//	//축 기준으로 몇개의 Block이 있는지 나타 낸다.
//	int GLASS_BLOCK_COUNT_X;				
//	int GLASS_BLOCK_COUNT_Y;
//
//	
//	Draw_CELLRecipeINFO_TAG()
//	{
//		memset(this,0x00, sizeof(Draw_CELLRecipeINFO_TAG));
//	}
//}Draw_CellRecipe;


/** 
@brief		Glass 내의 Defect 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
//typedef struct Draw_SKIP_AREA_TAG
//{
//	BOOL	m_Select;
//	int		m_SkipType;
//
//	float	m_CenterX;
//	float	m_CenterY;
//
//	float	m_Radius;
//
//
//	Draw_SKIP_AREA_TAG()
//	{
//		memset(this,0x00, sizeof(Draw_SKIP_AREA_TAG));
//	}
//} Draw_SkipAREA;