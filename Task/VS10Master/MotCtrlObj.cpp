#include "StdAfx.h"
#include "MotCtrlObj.h"


CMotCtrlObj::CMotCtrlObj(void)
{
	m_pServerInterface = nullptr;
	m_TT01_NowPos = TT01_AM01_Unknown;

}


CMotCtrlObj::~CMotCtrlObj(void)
{
}

void CMotCtrlObj::m_fnResetAllCMDRun()
{
	for(int RStep = 0; RStep<MOTION_COUNT; RStep++)
	{
		if(m_MotCMD_STATUS[RStep].B_MOTCMD_State != IDLE)
		{
			m_MotCMD_STATUS[RStep].B_MOTCMD_State = IDLE;
			G_WriteInfo(Log_Check,"B_MOTCMD_State(%d) : Reset(IDLE)", RStep);
		}
	}
}
void CMotCtrlObj::m_fnSetSignalTower(STWOER_MODE NewSettingMode)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.newPosition1 = (int)NewSettingMode;
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	G_WriteInfo(Log_Normal,"SetSignalTower : %d.", NewSettingMode);
	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_SetSignalTower, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);
}
void CMotCtrlObj::m_fnSetSignalTower_BuzOn()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Home;//Buzzer On에 사용 된다.
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	G_WriteInfo(Log_Normal,"Tower_OnBuz : On.");
	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_SetSignalTower, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);
}
void CMotCtrlObj::m_fnSetSignalTower_StopBuz()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Stop;//Buzzer Stop에 사용 된다.
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	G_WriteInfo(Log_Normal,"Tower_StopBuz : Stop.");
	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_SetSignalTower, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);
}
void CMotCtrlObj::m_fnAM01MotAllStop()
{
	G_WriteInfo(Log_Check, "Motion AM01 All Stop");
	m_fnAM01ActionStop( nBiz_Seq_AM01_ScanUpDn);			
	m_fnAM01ActionStop( nBiz_Seq_AM01_SpaceSnUpDn	);	
	m_fnAM01ActionStop( nBiz_Seq_AM01_ScopeUpDn);		
	m_fnAM01ActionStop(nBiz_Seq_AM01_SpaceMeasurStart);

	m_fnAM01ActionStop( nBiz_Seq_AM01_ScanDrive);			
	m_fnAM01ActionStop( nBiz_Seq_AM01_ScanShift);	
	m_fnAM01ActionStop( nBiz_Seq_AM01_ScopeDrive	);		
	m_fnAM01ActionStop( nBiz_Seq_AM01_ScopeShift	);		

	m_fnAM01ActionStop(nBiz_Seq_AM01_MultiMoveScan	);
	m_fnAM01ActionStop(nBiz_Seq_AM01_MultiMoveScofe);		
	m_fnAM01ActionStop(nBiz_Seq_AM01_SyncMoveScanScofe	);
	
	m_fnAM01ActionStop( nBiz_Seq_AM01_AirBlowLeftRight);		

	m_fnAM01ActionStop( nBiz_Seq_AM01_TensionUpDn);

	m_fnAM01ActionStop( nBiz_Seq_AM01_TensionStart);
	m_fnAM01ActionStop( nBiz_Seq_AM01_MultiLTension);
	m_fnAM01ActionStop( nBiz_Seq_AM01_MultiRTension);

	m_fnAM01ActionStop( nBiz_Seq_AM01_SyncMoveScopeSpaceZ);

	//m_fnAM01ActionStop( nBiz_Seq_AM01_LTension1				);
	//m_fnAM01ActionStop( nBiz_Seq_AM01_LTension2				);
	//m_fnAM01ActionStop( nBiz_Seq_AM01_LTension3				);	
	//m_fnAM01ActionStop( nBiz_Seq_AM01_LTension4				);	
	//m_fnAM01ActionStop( nBiz_Seq_AM01_RTension1				);	
	//m_fnAM01ActionStop( nBiz_Seq_AM01_RTension2				);	
	//m_fnAM01ActionStop( nBiz_Seq_AM01_RTension3				);	
	//m_fnAM01ActionStop( nBiz_Seq_AM01_RTension4				);	




}
void CMotCtrlObj::m_fnScanDriveMove(int nPosValue)
{
	m_MotCMD_STATUS[AM01_SCAN_DRIVE].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCAN_DRIVE].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCAN_DRIVE].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_ScanDrive, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCAN_DRIVE : [%d]위치로 이동 명령.", nPosValue);
		//m_MotCMD_STATUS[AM01_SCAN_DRIVE].UN_CMD_COUNT++;
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCAN_DRIVE : 해당 축 BUSY 상태.");
	}

}

void CMotCtrlObj::m_fnScanShiftMove(int nPosValue)
{
	m_MotCMD_STATUS[AM01_SCAN_SHIFT].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCAN_SHIFT].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCAN_SHIFT].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_ScanShift, NORMAL_TIMEOUT);

		G_WriteInfo(Log_Normal,"AM01_SCAN_SHIFT : [%d]위치로 이동 명령.", nPosValue);

		
		//m_MotCMD_STATUS[AM01_SCAN_SHIFT].UN_CMD_COUNT++;
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCAN_SHIFT : 해당 축 BUSY 상태.");
	}

}

void CMotCtrlObj::m_fnScanUpDnMove(int nPosValue)
{
	m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_ScanUpDn, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCAN_UPDN : [%d]위치로 이동 명령.", nPosValue);
		//m_MotCMD_STATUS[AM01_SCAN_UPDN].UN_CMD_COUNT++;
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCAN_UPDN : 해당 축 BUSY 상태.");
	}
}

void CMotCtrlObj::m_fnAM01PositionMove(int nPosValue, int nMotionType, int nTimeOut)
{	
	VS24MotData m_stAM01_Mess;

	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;
	m_stAM01_Mess.SpeedMode = Speed_Normal;
	m_stAM01_Mess.newPosition1 = nPosValue;
	m_stAM01_Mess.TimeOut = nTimeOut;

	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);

}

void CMotCtrlObj::m_fnAM01MultiPositionMove(int nPosValueShift, int nPosValueDrive, int nMotionType, int nTimeOut)
{	

	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;
	m_stAM01_Mess.SpeedMode = Speed_Normal;
	m_stAM01_Mess.newPosition1 = nPosValueShift;
	m_stAM01_Mess.newPosition2 = nPosValueDrive;
	m_stAM01_Mess.TimeOut = nTimeOut;

	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);

}


void CMotCtrlObj::m_fnAM01ScanScopeSyncMove(int nPosScanShift, int nPosScanDrive, int nPosScopeShift, int nPosScopeDrive, int nTimeOut)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;
	m_stAM01_Mess.SpeedMode = Speed_Normal;
	m_stAM01_Mess.newPosition1 = nPosScanShift;
	m_stAM01_Mess.newPosition2 = nPosScanDrive;
	m_stAM01_Mess.newPosition3 = nPosScopeShift;
	m_stAM01_Mess.newPosition4 = nPosScopeDrive;
	m_stAM01_Mess.TimeOut = nTimeOut;

	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_AM01_SyncMoveScanScofe, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);
}
void CMotCtrlObj::m_fnAM01MultiMoveLTension(int nSelect, int nPosValue, int nTimeOut)
{	
		m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.Reset();
		if (m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State == IDLE)
		{
			VS24MotData m_stAM01_Mess;
			m_stAM01_Mess.Reset();
			m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
			m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;
			m_stAM01_Mess.SpeedMode = Speed_Normal;
			m_stAM01_Mess.SelectPoint = nSelect;
			m_stAM01_Mess.newPosition1 = nPosValue;
			m_stAM01_Mess.newPosition2 = nPosValue;
			m_stAM01_Mess.newPosition3 = nPosValue;
			m_stAM01_Mess.newPosition4 = nPosValue;
			m_stAM01_Mess.TimeOut = nTimeOut;

			m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State = RUNNING;
			m_pServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_AM01_MultiLTension, nBiz_Unit_Zero,
				(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);

			G_WriteInfo(Log_Normal,"AM01_TENTION_LEFT_MULTI : [%d]위치로 이동 명령.", nPosValue);

			
		}
		else
		{
			G_WriteInfo(Log_Worrying,"AM01_TENTION_LEFT_MULTI : 해당 축 BUSY 상태.");
		}

}
void CMotCtrlObj::m_fnAM01MultiMoveRTension(int nSelect, int nPosValue, int nTimeOut)
{	
	m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State == IDLE)
	{
		VS24MotData m_stAM01_Mess;
		m_stAM01_Mess.Reset();
		m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
		m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;
		m_stAM01_Mess.SpeedMode = Speed_Normal;
		m_stAM01_Mess.SelectPoint = nSelect;
		m_stAM01_Mess.newPosition1 = nPosValue;
		m_stAM01_Mess.newPosition2 = nPosValue;
		m_stAM01_Mess.newPosition3 = nPosValue;
		m_stAM01_Mess.newPosition4 = nPosValue;
		m_stAM01_Mess.TimeOut = nTimeOut;
		m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State = RUNNING;
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_AM01_MultiRTension, nBiz_Unit_Zero,
			(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);

		G_WriteInfo(Log_Normal,"AM01_TENTION_RIGHT_MULTI : [%d]위치로 이동 명령.", nPosValue);

		
		//m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].UN_CMD_COUNT++;
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_TENTION_RIGHT_MULTI : 해당 축 BUSY 상태.");
	}

}
void CMotCtrlObj::m_fnAM01ActionStop(int nMotionType, int nTimeOut)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Stop;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;
	m_stAM01_Mess.TimeOut = nTimeOut;
	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData), (UCHAR*)&m_stAM01_Mess);
}

int CMotCtrlObj::m_fn_Scan_Drive_Return(VS24MotData stReturn_Msg)
{
	
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCAN_DRIVE : 수행 완료.");
			//m_MotCMD_STATUS[AM01_SCAN_DRIVE].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_DRIVE : 에러 발생.");
			//m_MotCMD_STATUS[AM01_SCAN_DRIVE].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCAN_DRIVE : 이동 명령에 대한 타임아웃 발생.");//m_MotCMD_STATUS[AM01_SCAN_DRIVE].UN_CMD_COUNT);
			//m_fnScanDriveMove(stReturn_Msg.newPosition1);
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_DRIVE : 리턴 값 에러[%d].", stReturn_Msg.nResult);
			//m_MotCMD_STATUS[AM01_SCAN_DRIVE].UN_CMD_COUNT = 0;
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCAN_DRIVE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCAN_DRIVE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

int CMotCtrlObj::m_fn_ScanShift_Return(VS24MotData stReturn_Msg)
{


	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCAN_SHIFT : 수행 완료.");
			//m_MotCMD_STATUS[AM01_SCAN_SHIFT].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_SHIFT : 에러 발생.");
			//m_MotCMD_STATUS[AM01_SCAN_SHIFT].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCAN_SHIFT : 이동 명령에 대한 타임아웃 발생.");//, m_MotCMD_STATUS[AM01_SCAN_SHIFT].UN_CMD_COUNT);
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_SHIFT : 리턴 값 에러[%d].", stReturn_Msg.nResult);
			//m_MotCMD_STATUS[AM01_SCAN_SHIFT].UN_CMD_COUNT = 0;
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCAN_SHIFT].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCAN_SHIFT].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	
	return 0;
}
int CMotCtrlObj::m_fnAM01TensionMultiMoveL_Return(VS24MotData stReturn_Msg)
{


	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_TENTION_LEFT_MULTI : 수행 완료.");
			//m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_TENTION_LEFT_MULTI : 에러 발생.");
			//m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_TENTION_LEFT_MULTI : 이동 명령에 대한 타임아웃 발생.");//, m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].UN_CMD_COUNT);
			//m_fnBO01RotateMove(stReturn_Msg.newPosition1);
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_TENTION_LEFT_MULTI : 리턴 값 에러[%d].", stReturn_Msg.nResult);
			//m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].UN_CMD_COUNT = 0;
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	
	return 0;
}
int CMotCtrlObj::m_fnAM01TensionMultiMoveR_Return(VS24MotData stReturn_Msg)
{


	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_TENTION_RIGHT_MULTI : 수행 완료.");
			//m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_TENTION_RIGHT_MULTI : 에러 발생.");
			//m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_TENTION_RIGHT_MULTI : 이동 명령에 대한  타임아웃 발생.");//, m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].UN_CMD_COUNT);
			//m_fnBO01RotateMove(stReturn_Msg.newPosition1);
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_TENTION_RIGHT_MULTI : 리턴 값 에러[%d].", stReturn_Msg.nResult);
			//m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].UN_CMD_COUNT = 0;
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	
	return 0;
}
int CMotCtrlObj::m_fnScanUpDn_Return(VS24MotData stReturn_Msg)
{


	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCAN_UPDN : 수행 완료.");
			//m_MotCMD_STATUS[AM01_SCAN_UPDN].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_UPDN : 에러 발생.");
			//m_MotCMD_STATUS[AM01_SCAN_UPDN].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCAN_UPDN : 타임아웃 발생.");
			//m_fnBO01RotateMove(stReturn_Msg.newPosition1);
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_UPDN : 리턴 값 에러[%d].", stReturn_Msg.nResult);
			//m_MotCMD_STATUS[AM01_SCAN_UPDN].UN_CMD_COUNT = 0;
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	

	return 0;
}


void CMotCtrlObj::m_fnSpaceSensorMove(int nPosValue)
{
	m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_SpaceSnUpDn, 300);
		G_WriteInfo(Log_Normal,"AM01_SPACE_UPDN : [%d]위치로 이동 명령.", nPosValue);
		//m_MotCMD_STATUS[AM01_SPACE_UPDN].UN_CMD_COUNT++;
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SPACE_UPDN : 해당 축 BUSY 상태.");
	}

}

int CMotCtrlObj::m_fnSpaceSensorUpDn_Return(VS24MotData stReturn_Msg)
{


	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SPACE_UPDN : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SPACE_UPDN : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SPACE_UPDN : 이동 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SPACE_UPDN : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	
	return 0;
}
void CMotCtrlObj::m_fnScopeDriveMove(int nPosValue)
{
	//Scope의 Z축이 대기 위치에서 벗어나 있는경우 Scope Drive 범위는 제한된다.
	//Motion Task에서 Z축위치에 따른 Inter Lock을 잡는다.
	m_MotCMD_STATUS[AM01_SCOPE_DRIVE].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCOPE_DRIVE].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCOPE_DRIVE].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_ScopeDrive, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCOPE_DRIVE : [%d]위치로 이동 명령.", nPosValue);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCOPE_DRIVE : 해당 축 BUSY 상태.");
	}
}

int CMotCtrlObj::m_fn_Scope_Drive_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCOPE_DRIVE : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_DRIVE : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCOPE_DRIVE : 타임아웃 발생.");
			//m_fnScanDriveMove(stReturn_Msg.newPosition1);
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_DRIVE : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCOPE_DRIVE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCOPE_DRIVE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnScopeShiftMove(int nPosValue)
{
	if (m_MotCMD_STATUS[AM01_SCOPE_SHIFT].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCOPE_SHIFT].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_ScopeShift, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCOPE_SHIFT : [%d]위치로 이동 명령.", nPosValue);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCOPE_SHIFT : 해당 축 BUSY 상태.");
	}
}

int CMotCtrlObj::m_fn_Scope_Shift_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCOPE_SHIFT : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_SHIFT : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCOPE_SHIFT : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_SHIFT : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCOPE_SHIFT].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCOPE_SHIFT].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

void CMotCtrlObj::m_fnScopeUpDnMove(int nPosValue)
{
	m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_ScopeUpDn, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCOPE_UPDN : [%d]위치로 이동 명령.", nPosValue);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCOPE_UPDN : 해당 축 BUSY 상태.");
	}
}

int CMotCtrlObj::m_fnScopeUpDn_Return(VS24MotData stReturn_Msg)
{
	

	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCOPE_UPDN : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_UPDN : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCOPE_UPDN : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_UPDN : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnTT01PositionMove(int nPosValue, int nTimeOut)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;
	m_stAM01_Mess.SpeedMode = Speed_Normal;
	m_stAM01_Mess.newPosition1 = nPosValue;
	m_stAM01_Mess.TimeOut = nTimeOut;

	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_TT01_ThetaAlign, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);
}
void CMotCtrlObj::m_fnTT01AlignMove(int nPosValue)
{
	m_MotCMD_STATUS[TT01_THETA_ALIGN_MOVE].ActionResult.Reset();
	if (m_MotCMD_STATUS[TT01_THETA_ALIGN_MOVE].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[TT01_THETA_ALIGN_MOVE].B_MOTCMD_State = RUNNING;
		m_fnTT01PositionMove(nPosValue,  NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"TT01AlignMove : [%d]위치로 이동 명령.", nPosValue);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"TT01AlignMove : 해당 축 BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnTT01AlignMove_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"TT01AlignMove : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"TT01AlignMove : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"TT01AlignMove : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"TT01AlignMove : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[TT01_THETA_ALIGN_MOVE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[TT01_THETA_ALIGN_MOVE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnAM01MultiScopeSpaceZ(int nPosScope, int nPosSpace, int nTimeOut)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;
	m_stAM01_Mess.SpeedMode = Speed_Normal;
	m_stAM01_Mess.newPosition1 = nPosScope;
	m_stAM01_Mess.newPosition2 = nPosSpace;
	m_stAM01_Mess.TimeOut = nTimeOut;

	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_AM01_SyncMoveScopeSpaceZ, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);
}
void CMotCtrlObj::m_fnScopeSpaceZSyncMove(int nPosScope, int nPosSpace, int nTimeOut)
{
	m_MotCMD_STATUS[AM01_SCOPE_SPACE_MULTI_MOVE].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCOPE_SPACE_MULTI_MOVE].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCOPE_SPACE_MULTI_MOVE].B_MOTCMD_State = RUNNING;
		m_fnAM01MultiScopeSpaceZ(nPosScope, nPosSpace, nTimeOut);
		G_WriteInfo(Log_Normal,"AM01_SCOPE_SPACE_MULTI_MOVE : [%d][%d]위치로 이동 명령.", nPosScope, nPosSpace);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCOPE_SPACE_MULTI_MOVE : 해당 축 BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnScopeSpaceZSyncMove_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCOPE_SPACE_MULTI_MOVE : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_SPACE_MULTI_MOVE : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCOPE_SPACE_MULTI_MOVE : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_SPACE_MULTI_MOVE : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCOPE_SPACE_MULTI_MOVE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCOPE_SPACE_MULTI_MOVE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

void CMotCtrlObj::m_fnScanMultiMove(int nPosValuesShift, int nPosValuesDrive)
{
	m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State = RUNNING;
		m_fnAM01MultiPositionMove(nPosValuesShift, nPosValuesDrive, nBiz_Seq_AM01_MultiMoveScan, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCAN_MULTI_MOVE_XY : [%d][%d]위치로 이동 명령.", nPosValuesShift, nPosValuesDrive);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCAN_MULTI_MOVE_XY : 해당 축 BUSY 상태.");
	}
}				
int CMotCtrlObj::m_fnScanMultiMove_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCAN_MULTI_MOVE_XY : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_MULTI_MOVE_XY : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCAN_MULTI_MOVE_XY : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_MULTI_MOVE_XY : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

void CMotCtrlObj::m_fnScopeMultiMove(int nPosValuesShift, int nPosValuesDrive)
{
	m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State = RUNNING;
		m_fnAM01MultiPositionMove(nPosValuesShift, nPosValuesDrive, nBiz_Seq_AM01_MultiMoveScofe, 500);//NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCOPE_MULTI_MOVE_XY : [%d][%d]위치로 이동 명령.", nPosValuesShift, nPosValuesDrive);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCOPE_MULTI_MOVE_XY : 해당 축 BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnScopeMultiMove_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCOPE_MULTI_MOVE_XY : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_MULTI_MOVE_XY : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCOPE_MULTI_MOVE_XY : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_MULTI_MOVE_XY : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}



void CMotCtrlObj::m_fnStickTentionFWD()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	CString strLogMessage;

	m_MotCMD_STATUS[AM01_TENTION_FWD].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_TENTION_FWD].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_TENTION_FWD].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_TENTION_FWD, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_AM01_TENTION_FWD, nBiz_Unit_Zero
			);

		G_WriteInfo(Log_Normal,"AM01_TENTION_FWD : FWD.");

	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_TENTION_FWD : CYL. BUSY 상태.");
	}

	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnStickTentionFWD_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_TENTION_FWD : 가이드 후진 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_TENTION_FWD : 가이드 후진 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_TENTION_FWD : 가이드 후진 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_TENTION_FWD : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_TENTION_FWD].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_TENTION_FWD].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnStickTentionBWD()
{

	//if(G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection ==1 ||
	//	G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection ==1)
	//{
	//	G_WriteInfo(Log_Worrying,"m_fnStickTentionBWD : Stick Detection Sensor Check Plz");
	//	return;
	//}
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	
	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_TENTION_BWD].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_TENTION_BWD].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_TENTION_BWD].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_TENTION_BWD, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_AM01_TENTION_BWD, nBiz_Unit_Zero
			);
		G_WriteInfo(Log_Normal,"AM01_TENTION_BWD : BWD.");
		
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_TENTION_BWD : CYL. BUSY 상태.");
	}
	
	delete []chMsgBuf;	
}

int CMotCtrlObj::m_fnStickTentionBWD_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_TENTION_BWD : 가이드 후진 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_TENTION_BWD : 가이드 후진 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_TENTION_BWD : 가이드 후진 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_TENTION_BWD : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_TENTION_BWD].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_TENTION_BWD].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnStickGripperOpen()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;
	m_stAM01_Mess.SelectPoint = 0x0F;//All

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_GRIPPER_OPEN].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_OPEN_TEN_GRIPPER, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_AM01_OPEN_TEN_GRIPPER, nBiz_Unit_Zero
			);

		G_WriteInfo(Log_Normal,"AM01_GRIPPER_OPEN : Open.");
		
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_GRIPPER_OPEN : CYL. BUSY 상태.");
	}
	delete []chMsgBuf;	
}

int CMotCtrlObj::m_fnStickGripperOpen_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_GRIPPER_OPEN : 가이드 후진 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_GRIPPER_OPEN : 가이드 후진 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_GRIPPER_OPEN : 가이드 후진 명령에 대한  타임아웃 발생." );
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_GRIPPER_OPEN : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_GRIPPER_OPEN].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnStickGripperClose()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;
	m_stAM01_Mess.SelectPoint = 0x0F;//All
	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_CLOSE_TEN_GRIPPER, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_AM01_CLOSE_TEN_GRIPPER, nBiz_Unit_Zero
			);

		G_WriteInfo(Log_Normal,"AM01_GRIPPER_CLOSE : Close.");
		
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_GRIPPER_CLOSE : CYL. BUSY 상태.");
	}
	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnStickGripperClose_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_GRIPPER_CLOSE : 가이드 후진 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_GRIPPER_CLOSE : 가이드 후진 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_GRIPPER_CLOSE : 가이드 후진 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_GRIPPER_CLOSE : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

///2015,04,14 정의천 추가
void CMotCtrlObj::m_fnDoonTukLaserOn()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	m_MotCMD_STATUS[AM01_DOONTUK_LASER_ON].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_DOONTUK_LASER_ON].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_DOONTUK_LASER_ON].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_Doontuk_ON, nBiz_Unit_Zero,
			(USHORT)sizeof(VS24MotData), (UCHAR*)&m_stAM01_Mess);
		G_WriteInfo(Log_Normal,"AM01_DOONTUK_LASER_ON : ON.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_DOONTUK_LASER_ON : ON BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnDoonTukLaserOn_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_DoonTuk_Laser_ON : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_DoonTuk_Laser_ON : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_DoonTuk_Laser_ON : 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_DoonTuk_Laser_ON : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_DOONTUK_LASER_ON].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_DOONTUK_LASER_ON].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnDoonTukLaserOff()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	m_MotCMD_STATUS[AM01_DOONTUK_LASER_OFF].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_DOONTUK_LASER_OFF].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_DOONTUK_LASER_OFF].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_Doontuk_OFF, nBiz_Unit_Zero,
			(USHORT)sizeof(VS24MotData), (UCHAR*)&m_stAM01_Mess);
		G_WriteInfo(Log_Normal,"AM01_DOONTUK_LASER_OFF : OFF.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_DOONTUK_LASER_OFF : OFF BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnDoonTukLaserOff_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_DoonTuk_Laser_OFF : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_DoonTuk_Laser_OFF : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_DoonTuk_Laser_OFF : 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_DoonTuk_Laser_OFF : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_DOONTUK_LASER_OFF].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_DOONTUK_LASER_OFF].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
///2015,04,14 정의천 추가

void CMotCtrlObj::m_fnRingLightUp()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	m_MotCMD_STATUS[AM01_RING_LIGHT_UP].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_RING_LIGHT_UP].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_RING_LIGHT_UP].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_RingLight_UP, nBiz_Unit_Zero,
			(USHORT)sizeof(VS24MotData), (UCHAR*)&m_stAM01_Mess);
		G_WriteInfo(Log_Normal,"AM01_RING_LIGHT_UP : Up.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_RING_LIGHT_UP : CYL. BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnRingLightUp_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_RING_LIGHT_UP : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_RING_LIGHT_UP : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_RING_LIGHT_UP : 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_RING_LIGHT_UP : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_RING_LIGHT_UP].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_RING_LIGHT_UP].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnRingLightDown()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_RingLight_DN, nBiz_Unit_Zero,
			(USHORT)sizeof(VS24MotData), (UCHAR*)&m_stAM01_Mess);
		G_WriteInfo(Log_Normal,"AM01_RING_LIGHT_DOWN : Down.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_RING_LIGHT_DOWN : CYL. BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnRingLightDown_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_RING_LIGHT_DOWN : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_RING_LIGHT_DOWN : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_RING_LIGHT_DOWN : 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_RING_LIGHT_DOWN : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

void CMotCtrlObj::m_fnReviewUp()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = 60; //NORMAL_TIMEOUT;

	m_MotCMD_STATUS[AM01_REVIEW_UP].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_REVIEW_UP].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_REVIEW_UP].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_Review_UP, nBiz_Unit_Zero,
			(USHORT)sizeof(VS24MotData), (UCHAR*)&m_stAM01_Mess);
		G_WriteInfo(Log_Normal,"AM01_REVIEW_UP : Up.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_REVIEW_UP : CYL. BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnReviewUp_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_REVIEW_UP : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_REVIEW_UP : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_REVIEW_UP : 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_REVIEW_UP : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_REVIEW_UP].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_REVIEW_UP].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnReviewDown()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_AM01_Review_DN, nBiz_Unit_Zero,
			(USHORT)sizeof(VS24MotData), (UCHAR*)&m_stAM01_Mess);
		G_WriteInfo(Log_Normal,"AM01_REVIEW_DOWN : Down.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_REVIEW_DOWN : CYL. BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnReviewDown_Return(VS24MotData stReturn_Msg)
{

	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_REVIEW_DOWN : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_REVIEW_DOWN : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_REVIEW_DOWN : 명령에 대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_REVIEW_DOWN : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

bool CMotCtrlObj::m_fnStickJigMove(int nPosValue)
{

	if(G_CheckStickJig_InOut() == ST_PROC_NG)
	{//워낙 많이 걸리고 움직일때마다 체크 해야 함으로.
		G_WriteInfo(Log_Worrying,"AM01_JIG_INOUT : Interlock [%d]위치로 이동 명령.", nPosValue);
		return false;
	}
	m_MotCMD_STATUS[AM01_JIG_INOUT].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_JIG_INOUT].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_JIG_INOUT].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_JigInout, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_JIG_INOUT : [%d]위치로 이동 명령.", nPosValue);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_JIG_INOUT : 해당 축 BUSY 상태.");
		return false;
	}
	return true;
}

int CMotCtrlObj::m_fnStickJigMove_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_JIG_INOUT : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_JIG_INOUT : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_JIG_INOUTAM01_JIG_INOUT : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_JIG_INOUT : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_JIG_INOUT].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_JIG_INOUT].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

void CMotCtrlObj::m_fnAM01GripperUpDown(int nPosValue)
{
	m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State = RUNNING;
		m_fnAM01PositionMove(nPosValue, nBiz_Seq_AM01_TensionUpDn, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_GRIPPER_UPDN : [%d]위치로 이동 명령.", nPosValue);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_GRIPPER_UPDN : 해당 축 BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnAM01GripperUpDown_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_GRIPPER_UPDN : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_GRIPPER_UPDN : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_GRIPPER_UPDN : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_GRIPPER_UPDN : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

void CMotCtrlObj::m_fnAM01GripperTensionStart(bool OnOff, int AxisSelect, float fTargetValue)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	if(OnOff)
		m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	else
		m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Stop;

	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	m_stAM01_Mess.SelectPoint = AxisSelect;
	//1000단위 값으로 변환
	m_stAM01_Mess.newPosition1 = (int)(fTargetValue*1000.0f);
	//m_stAM01_Mess.newPosition1 = (int)((fTargetValue+4.0f)*1000.0f);
	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_TENTION_START].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State == IDLE || OnOff == false)//Stop은 바로 내보내자
	{
		m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_AM01_TensionStart, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_MotionAct, nBiz_Seq_AM01_TensionStart, nBiz_Unit_Zero	);

		G_WriteInfo(Log_Normal,"AM01_TENTION_START : Start.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_TENTION_START : BUSY 상태.");
	}
	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnAM01GripperTensionStart_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"TensionStart : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"TensionStart : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"TensionStart : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"TensionStart : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_TENTION_START].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnScanSetLensOffset(int nReviewLensIndex)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;
	char strReadData[256];
	switch(nReviewLensIndex)
	{
	case REVIEW_LENS_2X:
		{
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition1 = atoi(strReadData);
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ReviewLens, Key_Lens02x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition2 = atoi(strReadData);
		}break;
	case REVIEW_LENS_10X:
		{
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition1 = atoi(strReadData);
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ReviewLens, Key_Lens10x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition2 = atoi(strReadData);
		}break;
	case REVIEW_LENS_20X:
		{
			m_stAM01_Mess.newPosition1 = 0;
			m_stAM01_Mess.newPosition2 = 0;
			//memset(strReadData, 0x00, 256);
			//GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			//m_stAM01_Mess.newPosition1 = atoi(strReadData);
			//memset(strReadData, 0x00, 256);
			//GetPrivateProfileString(Section_ReviewLens, Key_Lens20x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			//m_stAM01_Mess.newPosition2  = atoi(strReadData);
		}break;
	default:
		return;
	}

	//////////////////////////////////////////////////////////////////////////
	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_REVIEW_LENS_OFFSET].ActionResult.Reset();
	//if(m_MotCMD_STATUS[AM01_REVIEW_LENS_OFFSET].B_MOTCMD_State == IDLE)//Stop은 바로 내보내자
	{
		m_MotCMD_STATUS[AM01_REVIEW_LENS_OFFSET].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_AM01_ReviewLensOffset, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_MotionAct, nBiz_Seq_AM01_ReviewLensOffset, nBiz_Unit_Zero	);

		G_WriteInfo(Log_Normal,"AM01_REVIEW_LENS_OFFSET : Start.");
	}
	//else
	//{
	//	G_WriteInfo(Log_Worrying,"AM01_REVIEW_LENS_OFFSET : BUSY 상태.");
	//}
	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnScanSetLensOffset_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_REVIEW_LENS_OFFSET : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_REVIEW_LENS_OFFSET : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_REVIEW_LENS_OFFSET : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_REVIEW_LENS_OFFSET : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_REVIEW_LENS_OFFSET].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_REVIEW_LENS_OFFSET].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnScopeSetLensOffset(int nScopeLensIndex)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;
	char strReadData[256];

	switch(nScopeLensIndex)
	{
	case SCOPE_LENS_10X:
		{
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition1 = atoi(strReadData);
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ScopeLens, Key_Lens10x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition2 = atoi(strReadData);
		}break;
	case SCOPE_LENS_20X:
		{
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition1 = atoi(strReadData);
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ScopeLens, Key_Lens20x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition2= atoi(strReadData);
		}break;
	case SCOPE_LENS_50X:
		{
			m_stAM01_Mess.newPosition1 = 0;
			m_stAM01_Mess.newPosition2 = 0;
			//memset(strReadData, 0x00, 256);
			//GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			//m_stAM01_Mess.newPosition1 = atoi(strReadData);
			//memset(strReadData, 0x00, 256);
			//GetPrivateProfileString(Section_ScopeLens, Key_Lens50x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			//m_stAM01_Mess.newPosition2 = atoi(strReadData);
		}break;
	case SCOPE_LENS_150X:
		{
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_OffsetX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition1 = atoi(strReadData);
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_ScopeLens, Key_Lens150x_OffsetY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			m_stAM01_Mess.newPosition2 = atoi(strReadData);
		}break;
	default:
		return;
	}
	//////////////////////////////////////////////////////////////////////////
	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_SCOPE_LENS_OFFSET].ActionResult.Reset();
	//if(m_MotCMD_STATUS[AM01_SCOPE_LENS_OFFSET].B_MOTCMD_State == IDLE)//Stop은 바로 내보내자
	//{
		m_MotCMD_STATUS[AM01_SCOPE_LENS_OFFSET].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_AM01_ScopeLensOffset, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_MotionAct, nBiz_Seq_AM01_ScopeLensOffset, nBiz_Unit_Zero	);

		G_WriteInfo(Log_Normal,"AM01_SCOPE_LENS_OFFSET : Start.");
	//}
	//else
	//{
	//	G_WriteInfo(Log_Worrying,"AM01_SCOPE_LENS_OFFSET : BUSY 상태.");
	//}
	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnScopeSetLensOffset_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCOPE_LENS_OFFSET : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_LENS_OFFSET : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCOPE_LENS_OFFSET : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCOPE_LENS_OFFSET : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCOPE_LENS_OFFSET].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCOPE_LENS_OFFSET].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}





void CMotCtrlObj::m_fnSpaceSensorMeasureStart(bool OnOff, float fTargetValue)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	if(OnOff)
		m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Move;
	else
		m_stAM01_Mess.nAction = m_stAM01_Mess.Act_Stop;

	m_stAM01_Mess.newPosition1 = (int)(fTargetValue*1000.0f);

	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_SPACE_MEASURE].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_SPACE_MEASURE].B_MOTCMD_State == IDLE || OnOff == false)//Stop은 바로 내보내자
	{
		m_MotCMD_STATUS[AM01_SPACE_MEASURE].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_MotionAct, nBiz_Seq_AM01_SpaceMeasurStart, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_MotionAct, nBiz_Seq_AM01_SpaceMeasurStart, nBiz_Unit_Zero	);

		G_WriteInfo(Log_Normal,"AM01_SPACE_MEASURE : Start.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SPACE_MEASURE : BUSY 상태.");
	}
	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnSpaceSensorMeasureStart_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SPACE_MEASURE : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SPACE_MEASURE : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SPACE_MEASURE : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SPACE_MEASURE : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SPACE_MEASURE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SPACE_MEASURE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}




void CMotCtrlObj::m_fnStickTensionZeroSet()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.SelectPoint = (0x01|0x02|0x04|0x08);//All Zero Set
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_TENTION_ZERO].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_TENTION_ZERO].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_TENTION_ZERO].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_ACC, nBiz_Seq_LoadCell_L_ZeroSet, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_ACC, nBiz_Seq_LoadCell_L_ZeroSet, nBiz_Unit_Zero	);

		G_WriteInfo(Log_Normal,"AM01_TENTION_ZERO : Start.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_TENTION_ZERO : BUSY 상태.");
	}
	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnStickTensionZeroSet_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"TensionZero : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"TensionZero : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"TensionZero : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"TensionZero : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_TENTION_ZERO].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_TENTION_ZERO].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnQRCodeRead()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_QR_READ].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_QR_READ].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_QR_READ].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_ACC, nBiz_Seq_Read_QR, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_ACC, nBiz_Seq_Read_QR, nBiz_Unit_Zero	);

		G_WriteInfo(Log_Normal,"AM01_QR_READ : Start.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_QR_READ : BUSY 상태.");
	}
	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnQRCodeRead_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_QR_READ : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_QR_READ : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_QR_READ : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_QR_READ : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_QR_READ].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_QR_READ].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnAIRRead()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_AIR_READ].ActionResult.Reset();
	//if(m_MotCMD_STATUS[AM01_AIR_READ].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_AIR_READ].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_ACC, nBiz_Seq_Read_AirN2, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_ACC, nBiz_Seq_Read_AirN2, nBiz_Unit_Zero	);

		//G_WriteInfo(Log_Normal,"AM01_AIR_READ : Start.");
	}
	//else
	//{
	//	G_WriteInfo(Log_Worrying,"AM01_AIR_READ : BUSY 상태.");
	//}
	delete []chMsgBuf;	
}

void CMotCtrlObj::m_fnBuzzer2On(int OnTime_msec)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.ActionTime = OnTime_msec;

	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_ACC, nBiz_Seq_Buzzer2_On, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);
	
}
void CMotCtrlObj::m_fnBuzzer2Off()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.ActionTime = 0;

	m_pServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_ACC, nBiz_Seq_Buzzer2_Off, nBiz_Unit_Zero,
		(USHORT)sizeof(VS24MotData),	(UCHAR*)&m_stAM01_Mess);
}
int CMotCtrlObj::m_fnAIRRead_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			//G_WriteInfo(Log_Normal,"AM01_AIR_READ : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_AIR_READ : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_AIR_READ :  타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_AIR_READ : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_AIR_READ].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_AIR_READ].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}
void CMotCtrlObj::m_fnAM01ReviewSyncMove(int nPosValueShift, int nPosValueDrive, SyncScope ScopeSyncModule, int nTimeOut)
{
	m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State == IDLE)
	{
		int nPosScopeShift = nPosValueShift;
		int nPosScopeDrive = nPosValueDrive;

		char strReadData[256];
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetShift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeShift += atoi(strReadData);
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetDrive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeDrive += atoi(strReadData);

		//Review기준 Sync이다.
		switch(ScopeSyncModule)
		{
		case Sync_Scope_3DScope:
			{

			}break;
		case Sync_Scope_AirBlow:
			{
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ScopeCamToObjDis, Key_AirBlowDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScopeShift += atoi(strReadData);
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ScopeCamToObjDis, Key_AirBlowDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScopeDrive += atoi(strReadData);
			}break;
		case Sync_Scope_BackLight:
			{
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScopeShift += atoi(strReadData);
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScopeDrive +=  atoi(strReadData);
			}break;
		case Sync_Scope_ScanSensor:
			{
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ScanSensorDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScopeShift += atoi(strReadData);
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ScanSensorDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScopeDrive += atoi(strReadData);
			}break;
		case Sync_Scope_ThetaAlignCam:
			{
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ThetaAlignCamDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScopeShift += atoi(strReadData);
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ScopeCamToObjDis, Key_ThetaAlignCamDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScopeDrive += atoi(strReadData);
			}break;
		}

		m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State = RUNNING;
		m_fnAM01ScanScopeSyncMove(nPosValueShift, nPosValueDrive, nPosScopeShift, nPosScopeDrive, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : Scan[%d][%d]위치로 이동 명령.", nPosValueShift, nPosValueDrive);
		G_WriteInfo(Log_Normal,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : Scope[%d][%d]위치로 이동 명령.", nPosScopeShift, nPosScopeDrive);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : 해당 축 BUSY 상태.");
	}
}
void CMotCtrlObj::m_fnAM01LineScanSyncMove(int nPosValueShift, int nPosValueDrive, int nTimeOut)
{
	m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State == IDLE)
	{
		int nPosScopeShift = nPosValueShift;
		int nPosScopeDrive = nPosValueDrive;

		char strReadData[256];
		//Scope Lens의 위치를 더하고
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetShift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeShift += atoi(strReadData);
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetDrive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeDrive += atoi(strReadData);

		//Line Scan의 위치를 더하면 Scan Cam에 스코프가 가게 된다.
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeShift -= atoi(strReadData);
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeDrive += atoi(strReadData);
		
		//거기에 Back Light의 거리를 더하면 Scope Axis의 위치가 정해 진다.
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeShift += atoi(strReadData);
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScopeCamToObjDis, Key_BackLightDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScopeDrive +=  atoi(strReadData);

		m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State = RUNNING;
		m_fnAM01ScanScopeSyncMove(nPosValueShift, nPosValueDrive, nPosScopeShift, nPosScopeDrive, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : Scan[%d][%d]위치로 이동 명령.", nPosValueShift, nPosValueDrive);
		G_WriteInfo(Log_Normal,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : Scope[%d][%d]위치로 이동 명령.", nPosScopeShift, nPosScopeDrive);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : 해당 축 BUSY 상태.");
	}
}
void CMotCtrlObj::m_fnAM01ScopeSyncMove(int nPosValueShift, int nPosValueDrive, SyncScan ScanSyncModule, int nTimeOut)
{
	m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.Reset();
	if (m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State == IDLE)
	{
		int nPosScanShift = nPosValueShift;
		int nPosScanDrive = nPosValueDrive;

		char strReadData[256];
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetShift, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScanShift -= atoi(strReadData);
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_ScanScopeCamDis, Key_ReviewToScopeDisOffsetDrive, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		nPosScanDrive -= atoi(strReadData);
		//Scope기준 Sync이다.
		switch(ScanSyncModule)
		{
		case Sync_Scan_Review:
			{

			}break;
		case Sync_Scan_LineScan:
			{
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScanShift += atoi(strReadData);
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ReviewCamToObjDis, Key_LineScanDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScanDrive += atoi(strReadData);
			}break;
		case Sync_Scan_SpaceSensor:
			{
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ReviewCamToObjDis, Key_SpaceSensorDisX, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScanShift += atoi(strReadData);
				memset(strReadData, 0x00, 256);
				GetPrivateProfileString(Section_ReviewCamToObjDis, Key_SpaceSensorDisY, "0", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
				nPosScanDrive +=  atoi(strReadData);
			}break;
		}

		m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State = RUNNING;
		m_fnAM01ScanScopeSyncMove(nPosScanShift, nPosScanDrive, nPosValueShift, nPosValueDrive, NORMAL_TIMEOUT);
		G_WriteInfo(Log_Normal,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : Scope[%d][%d]위치로 이동 명령.", nPosValueShift, nPosValueDrive);
		G_WriteInfo(Log_Normal,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : Scan[%d][%d]위치로 이동 명령.", nPosScanShift, nPosScanDrive);
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : 해당 축 BUSY 상태.");
	}
}
int CMotCtrlObj::m_fnAM01ScanScopeSyncMove_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	if(stReturn_Msg.nAction == stReturn_Msg.Act_Stop && stReturn_Msg.nResult == stReturn_Msg.Ret_Ok)
		G_WriteInfo(Log_Worrying,"Stop 완료.");
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_SCAN_SCOPE_SYNC_MOVE_XY : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}

void CMotCtrlObj::m_fnSetLight(int Select, int ValueRing, int ValueBack, int ValueReflect)
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.SelectPoint = Select;
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	m_stAM01_Mess.newPosition1 = ValueRing;
	m_stAM01_Mess.newPosition2 = ValueBack;
	m_stAM01_Mess.newPosition3 = ValueReflect;
	m_stAM01_Mess.newPosition4 = 0;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	CString strLogMessage;

	m_MotCMD_STATUS[AM01_LIGHT].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_ACC, nBiz_Seq_Light_Scan, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_ACC, nBiz_Seq_Light_Scan, nBiz_Unit_Zero
			);

		G_WriteInfo(Log_Normal,"AM01_LIGHT : Setting Ring(%d), Back(%d), Reflect(%d).",
			ValueRing, ValueBack, ValueReflect);

	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_LIGHT : CYL. BUSY 상태.");
	}

	delete []chMsgBuf;	
}
int CMotCtrlObj::m_fnSetLight_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;

	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_LIGHT : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_LIGHT : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_LIGHT : 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_LIGHT : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_LIGHT].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	return 0;
}



void CMotCtrlObj::m_fnDoorReleasFront()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_DOOR_FRONT_RELEASE].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_DOOR_FRONT_RELEASE].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_DOOR_FRONT_RELEASE].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Front, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Front, nBiz_Unit_Zero
			);
		G_WriteInfo(Log_Normal,"AM01_DOOR_FRONT_RELEASE : Release.");

	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_DOOR_FRONT_RELEASE : BUSY 상태.");
	}

	delete []chMsgBuf;	
}
void CMotCtrlObj::m_fnDoorReleasSide()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_DOOR_SIDE_RELEASE].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_DOOR_SIDE_RELEASE].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_DOOR_SIDE_RELEASE].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Side, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Side, nBiz_Unit_Zero
			);
		G_WriteInfo(Log_Normal,"AM01_DOOR_SIDE_RELEASE : Release.");

	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_DOOR_SIDE_RELEASE : BUSY 상태.");
	}

	delete []chMsgBuf;	
}
void CMotCtrlObj::m_fnDoorReleasBack()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_DOOR_BACK_RELEASE].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_DOOR_BACK_RELEASE].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_DOOR_BACK_RELEASE].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Back, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Back, nBiz_Unit_Zero
			);
		G_WriteInfo(Log_Normal,"AM01_DOOR_BACK_RELEASE : Release.");

	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_DOOR_BACK_RELEASE : BUSY 상태.");
	}

	delete []chMsgBuf;	
}
void CMotCtrlObj::m_fnDoorLockFront()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_DOOR_FRONT_LOCK].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_DOOR_FRONT_LOCK].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_DOOR_FRONT_LOCK].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Front, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Front, nBiz_Unit_Zero
			);
		G_WriteInfo(Log_Normal,"AM01_DOOR_FRONT_LOCK : Release.");

	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_DOOR_FRONT_LOCK : BUSY 상태.");
	}
	delete []chMsgBuf;	
}
void CMotCtrlObj::m_fnDoorLockSide()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_DOOR_SIDE_LOCK].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_DOOR_SIDE_LOCK].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_DOOR_SIDE_LOCK].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Side, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Side, nBiz_Unit_Zero
			);
		G_WriteInfo(Log_Normal,"AM01_DOOR_SIDE_LOCK : Release.");

	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_DOOR_SIDE_LOCK : BUSY 상태.");
	}
	delete []chMsgBuf;	
}
void CMotCtrlObj::m_fnDoorLockBack()
{
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();

	m_stAM01_Mess.nAction = m_stAM01_Mess.Act_None;
	m_stAM01_Mess.nResult = m_stAM01_Mess.Ret_None;	
	m_stAM01_Mess.TimeOut = NORMAL_TIMEOUT;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stAM01_Mess, nMessSize);

	m_MotCMD_STATUS[AM01_DOOR_BACK_LOCK].ActionResult.Reset();
	if(m_MotCMD_STATUS[AM01_DOOR_BACK_LOCK].B_MOTCMD_State == IDLE)
	{
		m_MotCMD_STATUS[AM01_DOOR_BACK_LOCK].B_MOTCMD_State = RUNNING;	
		m_pServerInterface->m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Back, nBiz_Unit_Zero,
			nMessSize,	chMsgBuf,
			TASK_10_Master, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Back, nBiz_Unit_Zero);
		G_WriteInfo(Log_Normal,"AM01_DOOR_BACK_LOCK : Release.");
	}
	else
	{
		G_WriteInfo(Log_Worrying,"AM01_DOOR_BACK_LOCK : BUSY 상태.");
	}
	delete []chMsgBuf;	
}

void CMotCtrlObj::m_fnDoorReleasFront_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_DOOR_FRONT_RELEASE : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_FRONT_RELEASE : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_DOOR_FRONT_RELEASE :  대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_FRONT_RELEASE : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_DOOR_FRONT_RELEASE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_DOOR_FRONT_RELEASE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
}
void CMotCtrlObj::m_fnDoorReleasSide_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_DOOR_SIDE_RELEASE : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_SIDE_RELEASE : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_DOOR_SIDE_RELEASE :  대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_SIDE_RELEASE : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_DOOR_SIDE_RELEASE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_DOOR_SIDE_RELEASE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
}
void CMotCtrlObj::m_fnDoorReleasBack_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_DOOR_BACK_RELEASE : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_BACK_RELEASE : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_DOOR_BACK_RELEASE :  대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_BACK_RELEASE : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_DOOR_BACK_RELEASE].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_DOOR_BACK_RELEASE].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
}
void CMotCtrlObj::m_fnDoorLockFront_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_DOOR_FRONT_LOCK : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_FRONT_LOCK : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_DOOR_FRONT_LOCK :  대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_FRONT_LOCK : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_DOOR_FRONT_LOCK].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_DOOR_FRONT_LOCK].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
}
void CMotCtrlObj::m_fnDoorLockSide_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_DOOR_SIDE_LOCK : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_SIDE_LOCK : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_DOOR_SIDE_LOCK :  대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_SIDE_LOCK : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_DOOR_SIDE_LOCK].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_DOOR_SIDE_LOCK].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
}
void CMotCtrlObj::m_fnDoorLockBack_Return(VS24MotData stReturn_Msg)
{
	CString strLogMessage;
	switch (stReturn_Msg.nResult)
	{
	case stReturn_Msg.Ret_Ok:
		{
			G_WriteInfo(Log_Normal,"AM01_DOOR_BACK_LOCK : 수행 완료.");
		}break;
	case stReturn_Msg.Ret_Error:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_BACK_LOCK : 에러 발생.");
		}break;
	case stReturn_Msg.Ret_TimeOut:
		{
			G_WriteInfo(Log_Worrying,"AM01_DOOR_BACK_LOCK :  대한 타임아웃 발생.");
		}break;
	default:
		{
			G_WriteInfo(Log_Error,"AM01_DOOR_BACK_LOCK : 리턴 값 에러[%d].", stReturn_Msg.nResult);
		}break;
	}
	memcpy(&m_MotCMD_STATUS[AM01_DOOR_BACK_LOCK].ActionResult, &stReturn_Msg, sizeof(VS24MotData));
	m_MotCMD_STATUS[AM01_DOOR_BACK_LOCK].B_MOTCMD_State = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
}

void CMotCtrlObj::m_fnInspectTriggerSet(float SetValue_um)
{
	G_WriteInfo(Log_Check,"m_fnInspectTriggerSet(%0.1fum)", SetValue_um);
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.newPosition1 = (int)(SetValue_um*10);
	m_pServerInterface->m_fnSendCommand_Nrs(	
		TASK_24_Motion, 
		nBiz_Func_MotionAct, 
		nBiz_Seq_AM01_SetTrigger_Inspect, 
		nBiz_Unit_Zero,	
		sizeof(VS24MotData),(UCHAR*)&m_stAM01_Mess);

}
void CMotCtrlObj::m_fnAreaScanTriggerSet(float SetValue_um)
{
	G_WriteInfo(Log_Check,"m_fnAreaScanTriggerSet(%0.1fum)", SetValue_um);
	VS24MotData m_stAM01_Mess;
	m_stAM01_Mess.Reset();
	m_stAM01_Mess.newPosition1 = (int)(SetValue_um*10);
	m_pServerInterface->m_fnSendCommand_Nrs(	
		TASK_24_Motion, 
		nBiz_Func_MotionAct, 
		nBiz_Seq_AM01_SetTrigger_AreaScan, 
		nBiz_Unit_Zero,	
		sizeof(VS24MotData),(UCHAR*)&m_stAM01_Mess);

}


int CMotCtrlObj::m_fnGetAM01State_CySol_TEN_GRIPPER1()
{
	
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((G_MotInfo.m_pMotState->stGripperStatus.In00_Sn_LeftOpenGripper1 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In01_Sn_LeftCloseGripper1 == 0) )
		SolStateLeft = SOL_UP;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In00_Sn_LeftOpenGripper1 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In01_Sn_LeftCloseGripper1 == 1))
		SolStateLeft = SOL_DOWN;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In00_Sn_LeftOpenGripper1 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In01_Sn_LeftCloseGripper1 == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In00_Sn_LeftOpenGripper1 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In01_Sn_LeftCloseGripper1 == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((G_MotInfo.m_pMotState->stGripperStatus.In12_Sn_RightOpenGripper1 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In13_Sn_RightCloseGripper1 == 0) )
		SolStateRight = SOL_UP;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In12_Sn_RightOpenGripper1 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In13_Sn_RightCloseGripper1 == 1))
		SolStateRight = SOL_DOWN;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In12_Sn_RightOpenGripper1 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In13_Sn_RightCloseGripper1 == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In12_Sn_RightOpenGripper1 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In13_Sn_RightCloseGripper1 == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}
int CMotCtrlObj::m_fnGetAM01State_CySol_TEN_GRIPPER2()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((G_MotInfo.m_pMotState->stGripperStatus.In02_Sn_LeftOpenGripper2 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In03_Sn_LeftCloseGripper2 == 0) )
		SolStateLeft = SOL_UP;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In02_Sn_LeftOpenGripper2 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In03_Sn_LeftCloseGripper2 == 1))
		SolStateLeft = SOL_DOWN;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In02_Sn_LeftOpenGripper2 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In03_Sn_LeftCloseGripper2 == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In02_Sn_LeftOpenGripper2 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In03_Sn_LeftCloseGripper2 == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((G_MotInfo.m_pMotState->stGripperStatus.In14_Sn_RightOpenGripper2 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In15_Sn_RightCloseGripper2 == 0) )
		SolStateRight = SOL_UP;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In14_Sn_RightOpenGripper2 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In15_Sn_RightCloseGripper2 == 1))
		SolStateRight = SOL_DOWN;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In14_Sn_RightOpenGripper2 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In15_Sn_RightCloseGripper2 == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In14_Sn_RightOpenGripper2 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In15_Sn_RightCloseGripper2 == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;

}
int CMotCtrlObj::m_fnGetAM01State_CySol_TEN_GRIPPER3()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((G_MotInfo.m_pMotState->stGripperStatus.In04_Sn_LeftOpenGripper3 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In05_Sn_LeftCloseGripper3 == 0) )
		SolStateLeft = SOL_UP;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In04_Sn_LeftOpenGripper3 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In05_Sn_LeftCloseGripper3 == 1))
		SolStateLeft = SOL_DOWN;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In04_Sn_LeftOpenGripper3 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In05_Sn_LeftCloseGripper3 == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In04_Sn_LeftOpenGripper3 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In05_Sn_LeftCloseGripper3 == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((G_MotInfo.m_pMotState->stGripperStatus.In16_Sn_RightOpenGripper3 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In17_Sn_RightCloseGripper3 == 0) )
		SolStateRight = SOL_UP;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In16_Sn_RightOpenGripper3 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In17_Sn_RightCloseGripper3 == 1))
		SolStateRight = SOL_DOWN;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In16_Sn_RightOpenGripper3 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In17_Sn_RightCloseGripper3 == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In16_Sn_RightOpenGripper3 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In17_Sn_RightCloseGripper3 == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;

}
int CMotCtrlObj::m_fnGetAM01State_CySol_TEN_GRIPPER4()
{
	int SolStateLeft = SOL_NONE;
	int SolStateRight = SOL_NONE;
	if((G_MotInfo.m_pMotState->stGripperStatus.In06_Sn_LeftOpenGripper4 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In07_Sn_LeftCloseGripper4 == 0) )
		SolStateLeft = SOL_UP;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In06_Sn_LeftOpenGripper4 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In07_Sn_LeftCloseGripper4 == 1))
		SolStateLeft = SOL_DOWN;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In06_Sn_LeftOpenGripper4 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In07_Sn_LeftCloseGripper4 == 1))
		SolStateLeft = SOL_ERROR_BOTH_ON;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In06_Sn_LeftOpenGripper4 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In07_Sn_LeftCloseGripper4 == 0))
		SolStateLeft = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateLeft = SOL_NONE;
	}
	if((G_MotInfo.m_pMotState->stGripperStatus.In18_Sn_RightOpenGripper4 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In19_Sn_RightCloseGripper4 == 0) )
		SolStateRight = SOL_UP;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In18_Sn_RightOpenGripper4 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In19_Sn_RightCloseGripper4 == 1))
		SolStateRight = SOL_DOWN;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In18_Sn_RightOpenGripper4 == 1 && G_MotInfo.m_pMotState->stGripperStatus.In19_Sn_RightCloseGripper4 == 1))
		SolStateRight = SOL_ERROR_BOTH_ON;
	else if((G_MotInfo.m_pMotState->stGripperStatus.In18_Sn_RightOpenGripper4 == 0 && G_MotInfo.m_pMotState->stGripperStatus.In19_Sn_RightCloseGripper4 == 0))
		SolStateRight = SOL_ERROR_BOTH_OFF;
	else
	{
		SolStateRight = SOL_NONE;
	}
	if(SolStateLeft != SolStateRight)
	{
		SolStateLeft = SOL_ERROR_SENSOR;
	}
	return SolStateLeft;
}