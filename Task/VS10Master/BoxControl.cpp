#include "StdAfx.h"
#include "BoxControl.h"
#include "VS11Indexer.h"
#include "VS11IndexerDlg.h"

CBoxControl::CBoxControl()
{
	m_elBoxLoadStep = BL_MAIN_SEQUENCE_IDLE;
	m_elCoverOpenStep = CO_MAIN_SEQUENCE_IDLE;

	m_elSeq_Error = ERR_SEQ_NONE;
	m_elBoxSize = BOX_SIZE_NONE;
}


CBoxControl::~CBoxControl()
{
}

int CBoxControl::m_fnSetParameter(
	SBT01_POSITION* stpointer1,
	STM01_POSITION* stpointer2,
	SLT01_POSITION* stpointer3,
	SBO01_POSITION* stpointer4,
	SUT01_POSITION* stpointer5)
{
	m_st_BT01_Pos = stpointer1;
	m_st_TM01_Pos = stpointer2;
	m_st_LT01_Pos = stpointer3;
	m_st_BO01_Pos = stpointer4;
	m_st_UT01_Pos = stpointer5;

	return 0;
}

int CBoxControl::m_fnBoxMove_F_BT01_T_LT01()
{
	

	return 0;
}

int CBoxControl::m_fnBoxCoverOpen()
{
	return 0;
}

int CBoxControl::m_fnBoxCoverClose()
{
	return 0;
}

int CBoxControl::m_fnBoxMove_F_UT01_T_LT01()
{
	return 0;
}


int CBoxControl::m_fnBoxMove_F_LT01_T_BT01()
{
	return 0;
}

bool CBoxControl::m_fnLoadingTableBoxSizeCheck()
{
	if (G_STMACHINE_STATUS.stModuleStatus.In07_Sn_LoadBoxSmallDetection == ON)
		m_elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stModuleStatus.In08_Sn_LoadBoxMediumDetection == ON)
		m_elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stModuleStatus.In09_Sn_LoadBoxLargeDetection == ON)
		m_elBoxSize = BOX_SIZE_LARGE;

	if (G_BOX_STATUS[BAD_BOX].elBoxSize == m_elBoxSize) //둘이 동일 하다면, 정상.
	{
		return true;
	}

	return false;
}

int CBoxControl::m_fnGetNextBoxIndex(BOX_STATUS elBoxStatus)
{
	int nBoxIndex = 0;

	for (int bl = (int)BOX1; bl < (int)BOX_COUNT; bl++)
	{
		if (G_BOX_STATUS[bl].elBoxStatus == elBoxStatus)
		{
			nBoxIndex = bl;
		}
	}	

	return nBoxIndex;
}

int CBoxControl::m_fnGetNextBoxPos(int nBoxIndex)
{
	int nBoxPosition = 0;
	switch (nBoxIndex)
	{
	case 1:
		nBoxPosition = m_st_BT01_Pos->nBox1Position;
		break;
	case 2:
		nBoxPosition = m_st_BT01_Pos->nBox1Position;
		break;
	case 3:
		nBoxPosition = m_st_BT01_Pos->nBox1Position;
		break;
	case 4:
		nBoxPosition = m_st_BT01_Pos->nBox1Position;
		break;
	case 5:
		nBoxPosition = m_st_BT01_Pos->nBox1Position;
		break;
	case 6:
		nBoxPosition = m_st_BT01_Pos->nBox1Position;
		break;
	case 7:
		nBoxPosition = m_st_BT01_Pos->nBox1Position;
		break;
	default:
		break;
	}

	return nBoxPosition;
}

int CBoxControl::m_fnGetPusherPos()
{
	switch (m_elBoxSize)
	{
	case BOX_SIZE_NONE:
		break;
	case BOX_SIZE_SMALL:
		return m_st_LT01_Pos->nGuide_Small_Pos;
		break;
	case BOX_SIZE_MEDIUM:
		return m_st_LT01_Pos->nGuide_Mid_Pos;
		break;
	case BOX_SIZE_LARGE:
		return m_st_LT01_Pos->nGuide_Large_Pos;
		break;
	default:
		break;
	}

	return m_st_LT01_Pos->nGuide_Ready_Pos;
}