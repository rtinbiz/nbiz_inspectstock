#ifndef _STRUCTURE_H_
#define _STRUCTURE_H_

#include "GlobalParam.h"

/**
@struct	SBOX_TABLE_POSITION
@brief	박스 테이블 포지션 정보
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/14  16:32
*/
typedef struct tag_BT01_Position
{
	int		nBadBoxPosition;
	int		nBox1Position;
	int		nBox2Position;
	int		nBox3Position;
	int		nBox4Position;
	int		nBox5Position;
	int		nBox6Position;
	int		nBox7Position;
	int		nReadyPosition;
	//int		nTargetPosition;
	
	tag_BT01_Position()
	{
		memset(this, 0x00, sizeof(tag_BT01_Position));
	}
}SBT01_POSITION;


/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:12
*/
typedef struct tag_TM01_Position
{
	int		nBT01_FwdPos;
	int		nLT01_BwdPos;	
	//int		nTargetPosition;
	tag_TM01_Position()
	{
		memset(this, 0x00, sizeof(tag_TM01_Position));
	}
}STM01_POSITION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:49
*/
typedef struct tag_LT01_Position
{
	int		nGuide_Small_Pos;
	int		nGuide_Mid_Pos;	
	int		nGuide_Large_Pos;
	int		nGuide_Ready_Pos;
	int		nPurge_Time;
	//int		nTargetPosition;
	tag_LT01_Position()
	{
		memset(this, 0x00, sizeof(tag_LT01_Position));
	}
}SLT01_POSITION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:50
*/
typedef struct tag_BO01_Position
{
	int		nDrive_LT01_Small_Pos;
	int		nDrive_LT01_Mid_Pos;	
	int		nDrive_LT01_Large_Pos;
	int		nDrive_UT01_Small_Pos;
	int		nDrive_UT01_Mid_Pos;	
	int		nDrive_UT01_Large_Pos;
	int		nZ_Ready_Pos;
	int		nZ_LT01_Up_Cover_Pos;
	int		nZ_LT01_Dn_Cover_Pos;
	int		nZ_UT01_Up_Cover_Pos;
	int		nZ_UT01_Dn_Cover_Pos;
	int		nR_Pad_Up_Pos;
	int		nR_Pad_Dn_Pos;
	int		nPurge_Time;

// 	int		nDriveTarget;
// 	int		nZTarget;
// 	int		nRTarget;

	tag_BO01_Position()
	{
		memset(this, 0x00, sizeof(tag_BO01_Position));
	}
}SBO01_POSITION;


/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:49
*/
typedef struct tag_UT01_Position
{
	int		nGuide_Small_Pos;
	int		nGuide_Mid_Pos;	
	int		nGuide_Large_Pos;
	int		nGuide_Ready_Pos;
	int		nPurge_Time;
	//int		nTargetPosition;
	tag_UT01_Position()
	{
		memset(this, 0x00, sizeof(tag_UT01_Position));
	}
}SUT01_POSITION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:54
*/
typedef struct tag_TM02_Position
{
	int		nDrive_BT01_Pos;
	int		nDrive_LT01_Pos;
	int		nDrive_UT01_Pos;
	int		nDrive_TT01_Pos;
	int		nDrive_UT02_Pos;
	int		nDrive_QR_Pos;

	int		nT_BT01_Pos;
	int		nT_LT01_Pos;
	int		nT_UT01_Pos;
	int		nT_TT01_Pos;
	int		nT_UT02_Pos;
	int		nT_QR_Pos;

	int		nShift_BT01_Pos;
	int		nShift_LT01_Pos;
	int		nShift_UT01_Pos;
	int		nShift_TT01_Pos;
	int		nShift_UT02_Pos;
	int		nShift_QR_Pos;

	int		nStickZ_Ready_Pos;
	int		nStickZ_BT01_Pos;
	int		nStickZ_LT01_Pos;
	int		nStickZ_UT01_Pos;
	int		nStickZ_TT01_Pos;
	int		nStickZ_UT02_Pos;
	int		nStickZ_QR_Pos;
	int		nStickZ_Align_Pos;

	int		nPaperZ_BT01_Pos;
	int		nPaperZ_LT01_Pos;
	int		nPaperZ_UT01_Pos;

	int		nStickPurge_Time;
	int		nPaperPurge_Time;

	int		nLightValue1_1;
	int		nLightValue1_2;
	int		nLightValue2_1;
	int		nLightValue2_2;

// 	int		nDriveTarget;
// 	int		nTTarget;
// 	int		nZTarget;
// 	int		nShiftTarget;

	tag_TM02_Position()
	{
		memset(this, 0x00, sizeof(tag_TM02_Position));
	}
}STM02_POSITION;


/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/27  15:26
*/
typedef struct tag_TT01_Position
{
	int		nDrive_Ready_Pos;
	int		nDrive_Turn_Pos;
	int		nDrive_AM01_Pos;	
	
	int		nRotate_SideUp_Pos;
	int		nRotate_SideDn_Pos;
	
	int		nT_Ready_Pos;

	int		nUpPurge_Time;
	int		nDnPurge_Time;

// 	int		nDriveTarget;
// 	int		nTTarget;

	tag_TT01_Position()
	{
		memset(this, 0x00, sizeof(tag_TT01_Position));
	}
}STT01_POSITION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/27  15:30
*/
typedef struct tag_UT02_Position
{
	int		nPurge_Time;

	tag_UT02_Position()
	{
		memset(this, 0x00, sizeof(tag_UT02_Position));
	}
}SUT02_POSITION;


typedef union
{
	int nAirSelection;
	struct
	{
		int m_btStep1:1;  
		int m_btStep2:1;
		int m_btStep3:1; 
		int m_btStep4:1;
		int m_btStep5:1;
		int m_btStep6:1;
		int m_btStep7:1;		
		int m_btStep8:1;		
		int m_btStep9:1;		
		int m_btStep10:1;		
		int m_btStep11:1;		
		int m_btStep12:1;
		int m_btStep13:1;
		int m_btStep14:1;
		int m_btStep15:1;
		int m_btStep16:1;
		int m_btStep32:16;
	}nAirStep;
}AIR_SELECTION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/27  23:25
*/
struct STHREAD_PARAM
{
	void*			cObjectPointer1;
	void*			cObjectPointer2;
	void*			cObjectPointer3;
	void*			cObjectPointer4;
	int				nScanInterval;
	BOOL			bThreadFlag;
	STHREAD_PARAM()
	{
		cObjectPointer1 = nullptr;
		cObjectPointer2 = nullptr;
		cObjectPointer3 = nullptr;
		cObjectPointer4 = nullptr;
		nScanInterval = 500;
		bThreadFlag = TRUE;
	}
};

#endif