// SubDlg_CDTP.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SubDlg_CDTP.h"
#include "afxdialogex.h"


// CSubDlg_CDTP 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSubDlg_CDTP, CDialogEx)

CSubDlg_CDTP::CSubDlg_CDTP(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSubDlg_CDTP::IDD, pParent)
{
	m_pServerInterface = nullptr;
	m_CDTPTotalCnt = 0;
	m_CDTPNowIndex = 0;
	m_AutoReviewNowIndex = 0;
	m_AutoReviewTotalCnt = 0;
}

CSubDlg_CDTP::~CSubDlg_CDTP()
{
}

void CSubDlg_CDTP::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ST_INDEX_CD_TP, m_stIndex_CDTP);
	DDX_Control(pDX, IDC_ST_INDEX_AUTO_RIEVW, m_stIndex_AutoReview);
	DDX_Control(pDX, IDC_ST_TOTAL_CD_TP, m_stTotal_CDTP);
	DDX_Control(pDX, IDC_ST_TOTAL_AUTO_REVIEW, m_stTotal_AutoReview);
	DDX_Control(pDX, IDC_BT_START_CD_TP, m_btStartMeasure_CDTP);
	DDX_Control(pDX, IDC_BT_STOP_CD_TP, m_btStopMeasure_CDTP);
	DDX_Control(pDX, IDC_BT_START_AUTO_REVIEW, m_btStartAutoReview);
	DDX_Control(pDX, IDC_BT_STOP_AUTO_REVIEW, m_btStopAutoReview);
}


BEGIN_MESSAGE_MAP(CSubDlg_CDTP, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_START_CD_TP, &CSubDlg_CDTP::OnBnClickedBtStartCdTp)
	ON_BN_CLICKED(IDC_BT_STOP_CD_TP, &CSubDlg_CDTP::OnBnClickedBtStopCdTp)
	ON_BN_CLICKED(IDC_BT_START_AUTO_REVIEW, &CSubDlg_CDTP::OnBnClickedBtStartAutoReview)
	ON_BN_CLICKED(IDC_BT_STOP_AUTO_REVIEW, &CSubDlg_CDTP::OnBnClickedBtStopAutoReview)
END_MESSAGE_MAP()


// CSubDlg_CDTP 메시지 처리기입니다.


BOOL CSubDlg_CDTP::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSubDlg_CDTP::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btStartMeasure_CDTP.GetTextColor(&tColor);
		m_btStartMeasure_CDTP.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btStartMeasure_CDTP.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStartMeasure_CDTP.SetTextColor(&tColor);
		m_btStartMeasure_CDTP.SetCheckButton(true, true);
		m_btStartMeasure_CDTP.SetFont(&tFont);

		m_btStopMeasure_CDTP.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStopMeasure_CDTP.SetTextColor(&tColor);
		m_btStopMeasure_CDTP.SetCheckButton(true, true);
		m_btStopMeasure_CDTP.SetFont(&tFont);

		m_btStartAutoReview.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStartAutoReview.SetTextColor(&tColor);
		m_btStartAutoReview.SetCheckButton(true, true);
		m_btStartAutoReview.SetFont(&tFont);

		m_btStopAutoReview.SetRoundButtonStyle(&m_tButtonStyle);
		m_btStopAutoReview.SetTextColor(&tColor);
		m_btStopAutoReview.SetCheckButton(true, true);
		m_btStopAutoReview.SetFont(&tFont);

	}
	m_stIndex_CDTP.SetFont(&G_TaskStateSmallFont);
	m_stIndex_CDTP.SetTextColor(RGB(255, 255, 0));
	m_stIndex_CDTP.SetBkColor(RGB(0, 0, 0));

	m_stTotal_CDTP.SetFont(&G_NumberFont);
	m_stTotal_CDTP.SetTextColor(RGB(255, 255, 0));
	m_stTotal_CDTP.SetBkColor(RGB(0, 0, 0));

	m_stIndex_AutoReview.SetFont(&G_TaskStateSmallFont);
	m_stIndex_AutoReview.SetTextColor(RGB(255, 255, 0));
	m_stIndex_AutoReview.SetBkColor(RGB(0, 0, 0));

	m_stTotal_AutoReview.SetFont(&G_NumberFont);
	m_stTotal_AutoReview.SetTextColor(RGB(255, 255, 0));
	m_stTotal_AutoReview.SetBkColor(RGB(0, 0, 0));
	

}
void CSubDlg_CDTP::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}


HBRUSH CSubDlg_CDTP::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(IDC_ST_INDEX_CD_TP == DLG_ID_Number ||
		IDC_ST_TOTAL_CD_TP == DLG_ID_Number ||
		IDC_ST_INDEX_AUTO_RIEVW == DLG_ID_Number ||
		IDC_ST_TOTAL_AUTO_REVIEW == DLG_ID_Number)
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CSubDlg_CDTP::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSubDlg_CDTP::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}



void CSubDlg_CDTP::OnBnClickedBtStartCdTp()
{
	if(m_btStartAutoReview.GetCheck() ==  false)
	{
		if(m_btStartMeasure_CDTP.GetCheck() == false)
		{
			if(m_AutoReviewTotalCnt>0)
			{
				m_CDTPNowIndex = 0;
				m_btStartMeasure_CDTP.SetCheck(true);
				m_btStopMeasure_CDTP.EnableWindow(TRUE);
			}
			else
				G_WriteInfo(Log_Worrying, "CD, TP Measure Data None!");
		}
		else
			G_WriteInfo(Log_Worrying, "Already Auto CD, TP Measure Start!");
	}
	else
		G_WriteInfo(Log_Worrying, "Now Auto Review Run!");

}


void CSubDlg_CDTP::OnBnClickedBtStopCdTp()
{
	m_btStartMeasure_CDTP.SetCheck(false);
	m_btStopMeasure_CDTP.EnableWindow(FALSE);
}


void CSubDlg_CDTP::OnBnClickedBtStartAutoReview()
{
	if(m_btStartMeasure_CDTP.GetCheck() == false)
	{
		if(m_btStartAutoReview.GetCheck() == false)
		{
			if(m_AutoReviewTotalCnt>0)
			{
				m_AutoReviewNowIndex = 0;
				m_btStartAutoReview.SetCheck(true);
				m_btStopAutoReview.EnableWindow(TRUE);
			}
			else
				G_WriteInfo(Log_Worrying, "Auto Review Data None!");
		}
		else
			G_WriteInfo(Log_Worrying, "Already Auto Review Start!");
	}
	else
		G_WriteInfo(Log_Worrying, "Now CD, TP Measure Run!");

}


void CSubDlg_CDTP::OnBnClickedBtStopAutoReview()
{
	m_btStartAutoReview.SetCheck(false);
	m_btStopAutoReview.EnableWindow(FALSE);
}
