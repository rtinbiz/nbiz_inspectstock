#pragma once
#include "afxwin.h"


// CMaintDlg 대화 상자입니다.

class CMaintDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMaintDlg)

public:
	CMaintDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMaintDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MAINTENANCE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
public:
	COLORREF m_btBackColor;
	void InitCtrl_UI();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	ReviewPosition		*m_pReviewPos;
	Scope3DPosition	*m_pScopePos;
	GripperPosition		*m_pGripperPos;
	CLiveViewCtrl		*m_pLiveViewObj;
	//Obj Pos Draw
	ObjDistance *m_pCamToObjDis;


	CRoundButtonStyle m_tButtonStyle;	

	CRoundButton2 m_btReview_UpDown_Standby;
	CRoundButton2 m_btReview_UpDown_Review;
	CRoundButton2 m_btReview_UpDown_Inspection;
	CRoundButton2 m_btReview_ShiftDrive_Standby;

	CRoundButton2 m_btScope_UpDown_Standby;
	CRoundButton2 m_btScope_UpDown_Measur;
	//CRoundButton2 m_btScope_UpDown_ThetaAlign;
	//CRoundButton2 m_btScope_UpDown_Area3D_Measure;

	CRoundButton2 m_btScope_ShiftDrive_Standby;

	CRoundButton2 m_btSpace_UpDown_Standby;
	CRoundButton2 m_btSpace_UpDown_Measure;

	CColorStaticST m_stGripperStickDetect_Left;
	CColorStaticST m_stGripperStickDetect_Right;

	CRoundButton2 m_btGripper_UpDown_Standby;
	CRoundButton2 m_btGripper_UpDown_GripStandby;
	CRoundButton2 m_btGripper_UpDown_Grip;
	CRoundButton2 m_btGripper_UpDown_Measure;
	CRoundButton2 m_btGripper_UpDown_TT01OUT;


	CRoundButton2 m_btTension_FBward_Standby;
	CRoundButton2 m_btTension_FBward_TensionStandby;
	CRoundButton2 m_btTension_FBward_TensionGrip;

	CRoundButton2 m_btTensionCylinder_FWD;
	CRoundButton2 m_btTensionCylinder_BWD;
	CRoundButton2 m_btTensionCylinder_GripOpen;
	CRoundButton2 m_btTensionCylinder_GripClose;
	

	CButton m_chSyncAxisSelect_Scope[4];
	CRoundButton2 m_btReviewMulti_Move;
	CRoundButton2 m_btReview_ShiftStep_Move;
	CRoundButton2 m_btReview_DriveStep_Move;


	CButton m_chSyncAxisSelect_Scan[3];
	CRoundButton2 m_btScopeMulti_Move;
	CRoundButton2 m_btScope_ShiftStep_Move;
	CRoundButton2 m_btScope_DriveStep_Move;

	CRoundButton2 m_btReviewActionStop;
	CRoundButton2 m_btScopeActionStop;
	CRoundButton2 m_btExitMaint;

	CRoundButton2 m_btLineScanSync_Move;
	CRoundButton2 m_btLineScanStep_Shift;
	CRoundButton2 m_btLineScanStep_Drive;
	CRoundButton2 m_btLineScan_ActionStop;


	CRoundButton2 m_btRingLight_Cylinder_Up;
	CRoundButton2 m_btRingLight_Cylinder_Down;
	CRoundButton2 m_btReview_Cylinder_Up;
	CRoundButton2 m_btReview_Cylinder_Down;
	CRoundButton2 m_btStickJig_Input;
	CRoundButton2 m_btStickJig_Output;


	bool *m_pbUpReviewCylinder;
	bool *m_pbUpRingLightCylinder;

	afx_msg void OnBnClickedBtStandbyReviewUpdown();
	afx_msg void OnBnClickedBtReviewReviewUpdown();
	afx_msg void OnBnClickedBtInspectionReviewUpdown();
	afx_msg void OnBnClickedBtStandbyShiftDriveScan();
	afx_msg void OnBnClickedBtStandbyScopeUpdown();
	afx_msg void OnBnClickedBtMeasureScopeUpdown();
	afx_msg void OnBnClickedBtThetaAlignScopeUpdown();
	afx_msg void OnBnClickedBtStandbyShiftDriveScope();
	afx_msg void OnBnClickedBtStandbySpaceUpdown();
	afx_msg void OnBnClickedBtMeasureSpaceUpdown();
	afx_msg void OnBnClickedBtStandbyTensionFbwd();
	afx_msg void OnBnClickedBtGripTensionFbwd();
	afx_msg void OnBnClickedBtGripStandbyTensionFbwd();
	afx_msg void OnBnClickedBtStandbyGripperUpdown();
	afx_msg void OnBnClickedBtGripStandbyGripperUpdown();
	afx_msg void OnBnClickedBtGripGripperUpdown();
	afx_msg void OnBnClickedBtMeasureGripperUpdown();
	afx_msg void OnBnClickedBtTt01outGripperUpdown();
	afx_msg void OnBnClickedBtTensionFwd();
	afx_msg void OnBnClickedBtTensionBwd();
	afx_msg void OnBnClickedBtGripperMaintOpen();
	afx_msg void OnBnClickedBtGripperMaintClose();
	afx_msg void OnBnClickedChSyncScope();
	afx_msg void OnBnClickedChSyncScan();
	afx_msg void OnBnClickedBtMoveScanXyMulti();
	afx_msg void OnBnClickedBtMoveScanShiftX();
	afx_msg void OnBnClickedBtMoveScanDriveY();
	afx_msg void OnBnClickedBtStopScanXy();
	afx_msg void OnBnClickedBtMoveScopeXyMulti();
	afx_msg void OnBnClickedBtMoveScopeShiftX();
	afx_msg void OnBnClickedBtMoveScopeDriveY();
	afx_msg void OnBnClickedBtStopScopeXy();
	afx_msg void OnBnClickedBtMoveLscanXyMulti();
	afx_msg void OnBnClickedBtMoveLscanShiftX();
	afx_msg void OnBnClickedBtMoveLscanDriveY();
	afx_msg void OnBnClickedBtStopLscanXy();
	afx_msg void OnBnClickedBtExitMaint();

	afx_msg void OnBnClickedBtArea3dMeasureScopeUpdown();

	afx_msg void OnBnClickedBtRingLightUp();
	afx_msg void OnBnClickedBtRingLightDn();
	afx_msg void OnBnClickedBtReviewUp();
	afx_msg void OnBnClickedBtReviewDn();

	afx_msg void OnBnClickedBtStickJigIn();
	afx_msg void OnBnClickedBtStickJigOut();
};
