#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
class CSeq_ReviewCalc: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_SPACE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_XY,
		SEQ_STEP_SCOPE_SAFETY_XY,

		//----------------------------------------
		SEQ_STEP_INPUT_JIG,
		//----------------------------------------
		SEQ_STEP_LENS_CALC_REVIEW_POS_Z,
		//2x----------------------------------------
		SEQ_STEP_LENS_CALC_2X_MOVE_POS,
		SEQ_STEP_LENS_CALC_2X_LENS_CHANGE,
		SEQ_STEP_LENS_CALC_2X_REVIEW_LIGHT,
		SEQ_STEP_LENS_CALC_2X_REVIEW_AF,
		SEQ_STEP_LENS_CALC_2X_REVIEW_GRAB,
		//10x----------------------------------------
		SEQ_STEP_LENS_CALC_10X_MOVE_POS,
		SEQ_STEP_LENS_CALC_10X_LENS_CHANGE,
		SEQ_STEP_LENS_CALC_10X_REVIEW_LIGHT,
		SEQ_STEP_LENS_CALC_10X_REVIEW_AF,
		SEQ_STEP_LENS_CALC_10X_REVIEW_GRAB,
		//20x----------------------------------------
		SEQ_STEP_LENS_CALC_20X_MOVE_POS,
		SEQ_STEP_LENS_CALC_20X_LENS_CHANGE,
		SEQ_STEP_LENS_CALC_20X_REVIEW_LIGHT,
		SEQ_STEP_LENS_CALC_20X_REVIEW_AF,
		SEQ_STEP_LENS_CALC_20X_REVIEW_GRAB,

		//----------------------------------------
		SEQ_STEP_LENS_CALC_END_LIGHT_DOWN,
		SEQ_STEP_LENS_CALC_END_REVIEW_DOWN,
		SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_Z,
		SEQ_STEP_LENS_CALC_END_SCAN_SAFETY_XY,
		SEQ_STEP_LENS_CALC_END_SCOPE_SAFETY_XY,

		SEQ_STEP_OUTPUT_JIG,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_ReviewCalc(void);
	~CSeq_ReviewCalc(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:

	REVIEW_LENS_OFFSET		*m_pReviewLensOffset;
	CLiveViewCtrl		*m_pLiveViewObj;
	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;
	GripperPosition		*m_pGripperPos;

	IplImage *m_pNowGrabImg;
	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;
	CInterServerInterface  *m_pServerInterface;

	IplImage *m_pProcGrayImgBuf;

};

