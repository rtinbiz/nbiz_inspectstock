#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
#include "DefectDataQ.h"

class CSeq_InsAutoReview: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_STICK_STATE_CHECK,
		//========================
		SEQ_STEP_SPACE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_XY,
		SEQ_STEP_SCOPE_SAFETY_XY,
		//==============================
		SEQ_STEP_SET_LENS_AUTORIVEW,
		SEQ_STEP_SET_LIGHT_AUTORIVEW,
		SEQ_STEP_SET_BACK_LIGHT,
		SEQ_STEP_DFECT_DATA_CHECK,

		//--------------------------------------
		SEQ_STEP_DFECT_MOVE_POS,
		SEQ_STEP_DFECT_REVIEW_POS_Z,
		SEQ_STEP_REVIEW_UP,
		SEQ_STEP_DFECT_AF,
		SEQ_STEP_DFECT_IMG_GRAB,
		SEQ_STEP_DFECT_SAVE_RESULT,
		//--------------------------------------
		SEQ_STEP_END_REVIEW_DATA_RESORT,
		SEQ_STEP_END_REVIEW_SAFETY_Z,
		SEQ_STEP_END_RING_LIGHT_DOWN,
		SEQ_STEP_END_REVIEW_DOWN,
		SEQ_STEP_END_SCAN_SAFETY_XY,
		SEQ_STEP_END_SCOPE_SAFETY_XY,
		SEQ_STEP_END_SET_LIGHT_AUTORIVEW,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_InsAutoReview(void);
	~CSeq_InsAutoReview(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:
	CString m_strBoxID;
	CString m_strStickID;
	CString m_strAutoReviewSavePath;

	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;
	GLASS_PARAM		*m_pGlassReicpeParam;
	HW_PARAM			*m_pHWRecipeParam;
	CLiveViewCtrl		*m_pLiveViewObj;

	bool			*m_pbInspectMicOrMac;//true : Mic, false:Mac
	DefectDataQ		*m_pMergeData_MicQ;
	DefectDataQ		*m_pMergeData_MacQ;
	IplImage *m_pNowGrabImg;

	//IplImage *m_pNowSmallImg;
	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;

	int m_AutoReviewIndex;
	int m_AutoReviewTotal;
	//int m_AutoReviewPosShift;
	//int m_AutoReviewPosDrive;
	//COORD_DINFO m_NowReviewData;
	DataNodeObj * m_pNowReviewData;
	CInterServerInterface  *m_pServerInterface;
	void WriteDefectInfo(int nTotalcnt, int CntIndex, CString strBoxID, CString strStickID,IplImage *pTarget, COORD_DINFO *pNowReviewData, CvPoint MachinePos);
	
};


