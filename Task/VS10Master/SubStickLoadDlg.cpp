// SubStickLoadDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SubStickLoadDlg.h"
#include "afxdialogex.h"


// CSubStickLoadDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSubStickLoadDlg, CDialogEx)

CSubStickLoadDlg::CSubStickLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSubStickLoadDlg::IDD, pParent)
{
	//m_pServerInterface = nullptr;
	m_SelectLoadingType = 0;
}

CSubStickLoadDlg::~CSubStickLoadDlg()
{
}

void CSubStickLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BT_STICK_ONLY_LOADING, m_btLoading_OnlyStick);
	DDX_Control(pDX, IDC_BT_STICK_LOADING_AUTO_RUN, m_btLoading_FullInspection);
	DDX_Control(pDX, IDC_BT_STICK_LOADING_CANCEL, m_btLoading_Cancel);
	DDX_Control(pDX, IDC_CB_LOADING_RECIPE, m_cbRecipeSelect);
}


BEGIN_MESSAGE_MAP(CSubStickLoadDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_STICK_ONLY_LOADING, &CSubStickLoadDlg::OnBnClickedBtStickOnlyLoading)
	ON_BN_CLICKED(IDC_BT_STICK_LOADING_AUTO_RUN, &CSubStickLoadDlg::OnBnClickedBtStickLoadingAutoRun)
	ON_BN_CLICKED(IDC_BT_STICK_LOADING_CANCEL, &CSubStickLoadDlg::OnBnClickedBtStickLoadingCancel)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CSubStickLoadDlg 메시지 처리기입니다.

BOOL CSubStickLoadDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSubStickLoadDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	m_btBackColor = G_Color_WinBack;
	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btLoading_OnlyStick.GetTextColor(&tColor);
		m_btLoading_OnlyStick.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,15,"굴림");
			tFont.lfHeight =12;
		}

		m_btLoading_OnlyStick.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLoading_OnlyStick.SetTextColor(&tColor);
		m_btLoading_OnlyStick.SetCheckButton(true, true);
		m_btLoading_OnlyStick.SetFont(&tFont);

		m_btLoading_FullInspection.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLoading_FullInspection.SetTextColor(&tColor);
		m_btLoading_FullInspection.SetCheckButton(true, true);
		m_btLoading_FullInspection.SetFont(&tFont);

		m_btLoading_Cancel.SetRoundButtonStyle(&m_tButtonStyle);
		m_btLoading_Cancel.SetTextColor(&tColor);
		m_btLoading_Cancel.SetCheckButton(true, true);
		m_btLoading_Cancel.SetFont(&tFont);

	}
	GetDlgItem(IDC_ST_LOADING_BOXID)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_LOADING_STICKID)->SetFont(&G_TaskStateSmallFont);
	//GetDlgItem(IDC_ST_LOADING_RECIPE)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_CB_LOADING_RECIPE)->SetFont(&G_TaskStateSmallFont);
}
void CSubStickLoadDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	//delete this;
}

HBRUSH CSubStickLoadDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CSubStickLoadDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSubStickLoadDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}

void CSubStickLoadDlg::SetStickInfo(CString strBoxID, CString strStickID, CString strRecipeID)
{
	m_strBoxID.Format("%s", strBoxID);
	m_strStickID.Format("%s", strStickID);
	m_strRecipeID.Format("%s", strRecipeID);
	m_SelectLoadingPPID.Format("");
	m_SelectLoadingType = 0;
}

void CSubStickLoadDlg::OnBnClickedBtStickOnlyLoading()
{
	m_SelectLoadingType = 1;
	m_cbRecipeSelect.GetWindowText(m_SelectLoadingPPID);
	CDialogEx::OnOK();
}


void CSubStickLoadDlg::OnBnClickedBtStickLoadingAutoRun()
{
	m_SelectLoadingType = 2;
	m_cbRecipeSelect.GetWindowText(m_SelectLoadingPPID);
	CDialogEx::OnOK();
}


void CSubStickLoadDlg::OnBnClickedBtStickLoadingCancel()
{
	m_SelectLoadingType = 3;
	m_cbRecipeSelect.GetWindowText(m_SelectLoadingPPID);
	CDialogEx::OnOK();
}


void CSubStickLoadDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow ==  TRUE)
	{
		GetDlgItem(IDC_ST_LOADING_BOXID)->SetWindowText(m_strBoxID);
		GetDlgItem(IDC_ST_LOADING_STICKID)->SetWindowText(m_strStickID);
		//GetDlgItem(IDC_ST_LOADING_RECIPE)->SetWindowText(m_strRecipeID);

		m_SelectLoadingPPID.Format("");
		m_cbRecipeSelect.ResetContent();
		CString tpath;
		tpath.Format("%s\\*.ini", VS10Master_RECIPE_INI_PATH);
		//검색 클래스
		CFileFind finder;

		//CFileFind는 파일, 디렉터리가 존재하면 TRUE 를 반환함
		BOOL bWorking = finder.FindFile(tpath); //
		CString fileName;
		CString DirName;


		CString strNowActiveRecipe;
		char strReadData[256];
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, "None", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		strNowActiveRecipe.Format("%s", strReadData);
		int nNowSelectIndex = -1;

		while (bWorking)
		{
			//다음 파일 / 폴더 가 존재하면다면 TRUE 반환
			bWorking = finder.FindNextFile();
			//파일 일때
			if (finder.IsArchived())
			{
				//파일의 이름
				CString _fileName =  finder.GetFilePath();

				// 현재폴더 상위폴더 썸네일파일은 제외
				if(_fileName.Find(".ini") <= 0 ) continue;

				fileName =  finder.GetFileTitle();
				if(strNowActiveRecipe.CompareNoCase(fileName) == 0 )
				{
					nNowSelectIndex = m_cbRecipeSelect.AddString(fileName); 
					m_SelectLoadingPPID.Format("%s", fileName);
				}
				else
					m_cbRecipeSelect.AddString(fileName);   
			}
		}
		m_cbRecipeSelect.SetCurSel(nNowSelectIndex);
	}
}
