#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
class CSeq_ThetaAlign: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_SPACE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_ALIGN_LIGHT_ON,
		//-----------------------------------------
		SEQ_STEP_SCOPE_ALIGN_POS,
		SEQ_STEP_SCOPE_GRAB,
		//-----------------------------------------
		SEQ_STEP_Theata_Align_Verdict,
		SEQ_STEP_Stick_Align_Value_Ack,
		//----------------------------------------
		SEQ_STEP_ALIGN_END_LIGHT_OFF,
		SEQ_STEP_ALIGN_END_SCOPE_SAFETY_Z,
		SEQ_STEP_ALIGN_END_SCOPE_SAFETY_XY,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_ThetaAlign(void);
	~CSeq_ThetaAlign(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:

	int m_ThetaAlignCnt;
	IplImage *m_pAlignImg[2];
	int m_GrabActionCnt;
	bool		m_ThetaAlignOK;
	//SCOPE_LENS_OFFSET		*m_pScopeLensOffset;
	CLiveViewCtrl		*m_pLiveViewObj;
	CLiveGrab	*m_pThetaAlignLiveObj;
	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;

	
	int *m_pNowLensIndex_3DScope;
	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;
	CInterServerInterface  *m_pServerInterface;

	int AlignImgProc_DetectLine(IplImage *pTarget);
};

