#pragma once
#include "afxwin.h"


// CStickReicpeDlg 대화 상자입니다.

class CStickReicpeDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CStickReicpeDlg)

public:
	CStickReicpeDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStickReicpeDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_STICK_RECIPE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	
	COLORREF m_btBackColor;
	void InitCtrl_UI();
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	CListBox m_listRecipeFile;

	void LoadRecipeFileList();
	void LoadRecipe_CellInfo(char *pFilePath);

	int						*m_pAlignCellStartX;
	int						*m_pAlignCellStartY;

	CGridCtrl m_gridRecipe_CellInfo;
	CGridCtrl m_gridRecipe_Scan3D;
	CGridCtrl m_gridRecipe_MicroScan;
	CGridCtrl m_gridRecipe_MacroScan;
	CGridCtrl m_gridRecipe_3DScope;
	CGridCtrl m_gridRecipe_CDTP;

	CString	m_strNowEditRecipeFile;
	afx_msg void OnLbnSelchangeListRecipeFile();
	void SaveRecipe(CString strNewPath);
	afx_msg void OnBnClickedBtSave();
	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btSaveRecipe;
	CRoundButton2 m_btExitRecipe;
	CRoundButton2 m_btCopyRecipe;
	CRoundButton2 m_btSetNowRecipe;
	CRoundButton2 m_btOpenRecipeFolder;
	afx_msg void OnBnClickedBtCopyRecipe();
	afx_msg void OnBnClickedBtExit();

	afx_msg void OnBnClickedBtSetRecipe();
	afx_msg void OnBnClickedBtOpenRecipeFolder();
	afx_msg void OnBnClickedChResetCellAlignValue();
};
