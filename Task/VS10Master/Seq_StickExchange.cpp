#include "StdAfx.h"
#include "Seq_StickExchange.h"


CSeq_StickExchange::CSeq_StickExchange(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;
	m_RecvCMD_Ret =-1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;

	m_pStickLoadInfo = nullptr;
	m_pGlassReicpeParam = nullptr;
	m_pScopePos	= nullptr;
	m_pReviewPos	= nullptr;
	m_pGripperPos = nullptr;

	m_pStickTentionValue = nullptr;
}


CSeq_StickExchange::~CSeq_StickExchange(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
}

bool CSeq_StickExchange::fnSeqFlowLoadStart()
{
	fnSeqErrorReset();

	
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
bool CSeq_StickExchange::fnSeqFlowUnloadStart()
{
	fnSeqErrorReset();


	fnSeqSetNextStep(SEQ_STEP_STICK_UNLOADING);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_StickExchange::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();
	G_WriteInfo(Log_Normal, "CSeq_StickExchange Error Step Run( Z Axis Go to a safe position)");
	return 0;
}
void CSeq_StickExchange::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
int CSeq_StickExchange::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_StickExchange%03d> Sequence Start", ProcStep);

			
			m_WaitCMD = 0;
			m_RecvCMD = 0;
			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_STICK_LOADING:
		{
			G_WriteInfo(Log_Normal, "▼▼▼=========================▼▼▼");
			G_WriteInfo(Log_Normal, "Stick Load Start");
			G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_LOADING%03d>BoxID:%s, Stick ID:%s", ProcStep, m_pStickLoadInfo->strBoxID, m_pStickLoadInfo->strStickID);



			

			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_Stick_Information:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_Stick_Information(%03d)>", ProcStep);
			//받은 정보를 기록 하자.
			
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//다음 CMD정의
			m_WaitCMD = nBiz_Seq_Stick_Load_Ready;
			m_RecvCMD = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 1);//정상처리.
			//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);//비정상.

			fnSeqSetNextStep();//Auto Increment
		}break;



	case SEQ_STEP_Wait_Stick_Load_Ready:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_Wait_Stick_Load_Ready(%03d)>", ProcStep);
			
			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					
					break;
				}
				
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_Wait_Stick_Load_Ready%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			//TT01이 진입 한다. 동작 체크 하고 대기 상태로 만들자.
			//1) Stick Check.
			m_WaitCMD = 0;
			m_RecvCMD = 0;
			if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1 ||
				G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1)
			{//Stick이 Gripper에 있다. Error다.
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_Wait_Stick_Load_Ready%03d> Grip Stick Now!!!!", ProcStep);
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);//비정상.
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_GRIPPER_OPEN_SOL_BWD:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_OPEN_SOL_BWD(%03d)>", ProcStep);
			//2)Grip Close
			G_MotObj.m_fnStickGripperOpen();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}

				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_OPEN_SOL_BWD%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_OPEN_SOL_BWD%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_GRIPPER_SOL_BWD:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_SOL_BWD(%03d)>", ProcStep);
			//2)Grip Tension Sol BWD
			G_MotObj.m_fnStickTentionBWD();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_BWD].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_SOL_BWD%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_BWD].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_SOL_BWD%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_GRIPPER_CLOSE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_CLOSE(%03d)>", ProcStep);
			//2)Grip Close
			G_MotObj.m_fnStickGripperClose();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_CLOSE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_CLOSE%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_GRIPPER_ALL_STANDBY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_ALL_STANDBY(%03d)>", ProcStep);
			//1)Grip Tension Axis (4*4 Axis 후퇴)
			//Left 4 Axis, Right 4Axis를 Standby Pos Move 시킨다.
			G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);
			G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);

			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State != IDLE ||
				G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_ALL_STANDBY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok ||
				G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_ALL_STANDBY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			
			fnSeqSetNextStep();//Auto Increment
		}break;
		
		case SEQ_STEP_GRIPPER_Z_STANDBY:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_Z_STANDBY(%03d)>", ProcStep);
				//2)Grip Tension Sol BWD
				G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Standby);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						
						break;
					}
					
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_Z_STANDBY%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_Z_STANDBY%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				//준비 상태가 완료 되면 Ack를 전달.(Seq 종단에 추가 하자.)
				m_WaitCMD = nBiz_Seq_Theta_Align_Start;
				m_RecvCMD = 0;
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Ready_OK, 1);//정상처리.
				//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);//비정상.

				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_Wait_Theta_Align_Start:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_Wait_Theta_Align_Start(%03d)>", ProcStep);

				int TimeOutWait = 3000;//About 20sec
				while(m_WaitCMD != m_RecvCMD)
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						
						break;
					}
					
					Sleep(20);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_Wait_Theta_Align_Start%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				//nBiz_Seq_TT01_Pos_AM01가 먼저 들어 오고 
				//nBiz_Seq_Theta_Align_Start가 들어 온다.
				m_WaitCMD = 0;
				m_RecvCMD = 0;
				//1) TT01 Docking Check.
				if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_AM01_Pos)//nBiz_Seq_TT01_Pos_AM01가 도킹 했는지를 먼저 본다.
				{//TT01이 들어온 상태가 아니다.
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_Wait_Theta_Align_Start%03d> TT01 Docking Check Error!!!!", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;

		case SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY_1:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY(%03d)>", ProcStep);
				//2)Grip Tension Sol BWD
				G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip_Standby);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}

				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_OPEN_1:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_OPEN(%03d)>", ProcStep);
				//2)Grip Close
				G_MotObj.m_fnStickGripperOpen();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_OPEN%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_OPEN%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_SOL_FWD:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_SOL_FWD(%03d)>", ProcStep);
				//2)Grip Tension Sol BWD
				G_MotObj.m_fnStickTentionFWD();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_FWD].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_SOL_FWD%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_FWD].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_SOL_FWD%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_ALL_GRIPPOS:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_ALL_GRIPPOS(%03d)>", ProcStep);
				//1)Grip Tension Axis (4*4 Axis 후퇴)
				//Left 4 Axis, Right 4Axis를 Standby Pos Move 시킨다.
				G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Grip);
				G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Grip);

				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State != IDLE ||
					G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_ALL_GRIPPOS%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok ||
					G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_ALL_GRIPPOS%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}

				fnSeqSetNextStep();//Auto Increment
			}break;

		case SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_1:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_1(%03d)>", ProcStep);
				//2)Grip Tension Sol BWD
				G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_1%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_1%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}

				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_GRIP:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_GRIP(%03d)>", ProcStep);
				//2)Grip Close
				G_MotObj.m_fnStickGripperClose();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_GRIP%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_GRIP%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_ALL_TENSION_ZeroSet:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_ALL_TENSION_ZeroSet(%03d)>", ProcStep);
				//2)Grip Close
				G_MotObj.m_fnStickTensionZeroSet();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_ZERO].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_ALL_TENSION_ZeroSet%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_ZERO].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_ALL_TENSION_ZeroSet%03d> ACC Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				//Gripper로 Stick을 물고 있자.
				m_WaitCMD = nBiz_Seq_Load_Vac_Off_Ack;
				m_RecvCMD = 0;
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Load_Vac_Off, 0);//정상처리.
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_VAC_OFF:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_VAC_OFF(%03d)>", ProcStep);

				int TimeOutWait = 3000;//About 20sec
				while(m_WaitCMD != m_RecvCMD)
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_VAC_OFF%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_Z_TT01_OUT:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_Z_TT01_OUT(%03d)>", ProcStep);

				G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_TT01Out);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_Z_TT01_OUT%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_Z_TT01_OUT%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;

		case SEQ_STEP_STICK_CH_START_STICK_TENSION:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_START_STICK_TENSION(%03d)>", ProcStep);
				//상위에서 받은것
				int StickLoadOffset = m_pStickLoadInfo->nStickLoadOffset;
				int StickLoadSize= StickLoadOffset+m_pGlassReicpeParam->STICK_WIDTH;
				//인장 Mot 선택.
				int SelectMotCnt = StickLoadSize/(TENSION_CLEE_WIDTH*TENSION_CLEE_CNT);
				SelectMotCnt += (StickLoadSize%(TENSION_CLEE_WIDTH*TENSION_CLEE_CNT)>0)?1:0;

				int SelectMot = 0;
				switch(SelectMotCnt)
				{
				case 1: SelectMot = 0x01; break;
				case 2: SelectMot = (0x01 | 0x02); break;
				case 3: SelectMot = (0x01 | 0x02 | 0x04); break;
				case 4: SelectMot = (0x01 | 0x02 | 0x04 | 0x08); break;
				}
				if(*m_pStickTentionValue<0.0)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_START_STICK_TENSION%03d> Stick Tention Value Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				//1)Grip Tension Start
				//G_MotObj.m_fnAM01GripperTensionStart(true, SelectMot, *m_pStickTentionValue);
				if(SelectMot>0)
					//1)Grip Tension Start
					G_MotObj.m_fnAM01GripperTensionStart(true, SelectMot, *m_pStickTentionValue);
				else
				{
					G_WriteInfo(Log_Error, "<SEQ_STEP_STICK_CH_START_STICK_TENSION%03d> Tension Axis Select Error");
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}

				int TimeOutWait = 16000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State != IDLE ||
					G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_MotObj.m_fnAM01GripperTensionStart(false, 0, *m_pStickTentionValue);//오류로 인한 인장 중지.
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_START_STICK_TENSION%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_MotObj.m_fnAM01GripperTensionStart(false, 0, *m_pStickTentionValue);//오류로 인한 인장 중지.
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_START_STICK_TENSION%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_2:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_2(%03d)>", ProcStep);
				//2)Grip Tension Sol BWD
				G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_2%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_2%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				m_WaitCMD = nBiz_Seq_Load_Vac_On_Ack;
				m_RecvCMD = 0;
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Load_Vac_On, 0);//정상처리.
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_VAC_ON:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_VAC_ON(%03d)>", ProcStep);
				int TimeOutWait = 3000;//About 60sec
				while(m_WaitCMD != m_RecvCMD)
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_VAC_ON%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}

				fnSeqSetNextStep();//Auto Increment
			}break;

		case SEQ_STEP_STICK_CH_GRIPPER_OPEN_2:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_OPEN_2(%03d)>", ProcStep);
				//2)Grip Close
				G_MotObj.m_fnStickGripperOpen();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_OPEN_2%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_OPEN_2%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY_2:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY_2(%03d)>", ProcStep);
				//2)Grip Tension Sol BWD
				G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip_Standby);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY_2%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_Z_GRIP_STANDBY_2%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_SOL_BWD:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_SOL_BWD(%03d)>", ProcStep);
				//2)Grip Tension Sol BWD
				G_MotObj.m_fnStickTentionBWD();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_BWD].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_SOL_BWD%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_BWD].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_SOL_BWD%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_STICK_CH_GRIPPER_CLOSE:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_CH_GRIPPER_CLOSE(%03d)>", ProcStep);
				//2)Grip Close
				G_MotObj.m_fnStickGripperClose();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

						break;
					}

					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_STICK_CH_GRIPPER_CLOSE%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_STICK_CH_GRIPPER_CLOSE%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;


			case SEQ_STEP_THETA_ALIGN:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_THETA_ALIGN(%03d)>", ProcStep);
					//Align Seq를 실행 한다.
					m_pSeqObj_ThetaAlign->fnSeqFlowStart();
					int TimeOutWait = 90000;//About 3Min
					Sleep(200);
					while(m_pSeqObj_ThetaAlign->fnSeqGetNowStep() != CSeq_ThetaAlign::SEQ_STEP_IDLE &&
						m_pSeqObj_ThetaAlign->fnSeqGetNowStep() != CSeq_ThetaAlign::SEQ_STEP_ERROR)
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
						
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_pSeqObj_ThetaAlign->fnSeqSetStop();
						G_WriteInfo(Log_Check, "<SEQ_STEP_THETA_ALIGN%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(m_pSeqObj_ThetaAlign->fnSeqGetNowStep() == CSeq_ThetaAlign::SEQ_STEP_ERROR)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						G_WriteInfo(Log_Worrying, "Theta Align Seq Error");
						m_pSeqObj_ThetaAlign->fnSeqErrorReset();
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					else if(m_pSeqObj_ThetaAlign->m_ThetaAlignOK == false)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						G_WriteInfo(Log_Worrying, "Theta Align Fail");
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_Z_GRIP_STANDBY:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_Z_GRIP_STANDBY(%03d)>", ProcStep);
					//2)Grip Tension Sol BWD
					G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip_Standby);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
						
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_Z_GRIP_STANDBY%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_Z_GRIP_STANDBY%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
				
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_OPEN:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_OPEN(%03d)>", ProcStep);
					//2)Grip Close
					G_MotObj.m_fnStickGripperOpen();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
						
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_OPEN%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_OPEN%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_SOL_FWD:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_SOL_FWD(%03d)>", ProcStep);
					//2)Grip Tension Sol BWD
					G_MotObj.m_fnStickTentionFWD();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_FWD].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
						
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_SOL_FWD%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_FWD].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_SOL_FWD%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_ALL_GRIPPOS:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_ALL_GRIPPOS(%03d)>", ProcStep);
					//1)Grip Tension Axis (4*4 Axis 후퇴)
					//Left 4 Axis, Right 4Axis를 Standby Pos Move 시킨다.
					G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Grip);
					G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Grip);

					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State != IDLE ||
						G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
					
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_ALL_GRIPPOS%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok ||
						G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_ALL_GRIPPOS%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}

					fnSeqSetNextStep();//Auto Increment
				}break;

			case SEQ_STEP_GRIPPER_Z_GRIP:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_Z_GRIP(%03d)>", ProcStep);
					//2)Grip Tension Sol BWD
					G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
					
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_Z_GRIP%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_Z_GRIP%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}

					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_GRIP:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_GRIP(%03d)>", ProcStep);
					//2)Grip Close
					G_MotObj.m_fnStickGripperClose();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
						
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_GRIP%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_GRIP%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_ALL_TENSION_ZeroSet:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_ALL_TENSION_ZeroSet(%03d)>", ProcStep);
					//2)Grip Close
					G_MotObj.m_fnStickTensionZeroSet();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_ZERO].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_ALL_TENSION_ZeroSet%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_ZERO].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_ALL_TENSION_ZeroSet%03d> ACC Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					//Gripper로 Stick을 물고 있자.
					m_WaitCMD = nBiz_Seq_TT01_VAC_OFF;
					m_RecvCMD = 0;
					m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Standby, 1);//정상처리.
					//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Standby, 0);//비정상.
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_Wait_TT01_VAC_OFF:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_Wait_TT01_VAC_OFF(%03d)>", ProcStep);

					int TimeOutWait = 3000;//About 20sec
					while(m_WaitCMD != m_RecvCMD)
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
					
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_Wait_TT01_VAC_OFF%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					//VAC OFF추가 대기 하자.
					Sleep(100);
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_Z_TT01_OUT:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_Z_TT01_OUT(%03d)>", ProcStep);

					G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_TT01Out);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
					
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_Z_TT01_OUT%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_Z_TT01_OUT%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					//TT01이 UT01로 갈때 까지 기다리자.
					m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Comp, 1);//정상처리.
					//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Load_Comp, 0);//비정상.
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_Wait_TT01_Pos_UT02:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_Wait_TT01_Pos_UT02z_(%03d)>", ProcStep);
					//TT01이 UT01로 갈때 까지 기다리자.
					int TimeOutWait = 10000;//About 60sec
					while(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							
							break;
						}
					
						Sleep(10);
					}
					//nBiz_Seq_TT01_Pos_UT02
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_Wait_TT01_Pos_UT02%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					////1) TT01 Docking Check.
					//if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)//TT01이 대기 위치로 간것을 확인한다.
					//{//TT01이 들어온 상태가 아니다.
					//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_Wait_TT01_Pos_UT02%03d> TT01 Pos Check Error!!!!", ProcStep);
					//	fnSeqSetNextStep(SEQ_STEP_ERROR);
					//	break;
					//}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_ALL_TENSION_STANDBY:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_ALL_TENSION_STANDBY(%03d)>", ProcStep);
					//1)Grip Tension Axis (4*4 Axis 후퇴)
					G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_TensionStandby);
					G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_TensionStandby);

					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State != IDLE ||
						G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_ALL_TENSION_STANDBY%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok ||
						G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_ALL_TENSION_STANDBY%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}

					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_START_STICK_TENSION:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_START_STICK_TENSION(%03d)>", ProcStep);
					//상위에서 받은것
					int StickLoadOffset = m_pStickLoadInfo->nStickLoadOffset;
					int StickLoadSize= StickLoadOffset+m_pGlassReicpeParam->STICK_WIDTH;
					//인장 Mot 선택.
					int SelectMotCnt = StickLoadSize/(TENSION_CLEE_WIDTH*TENSION_CLEE_CNT);
					SelectMotCnt += (StickLoadSize%(TENSION_CLEE_WIDTH*TENSION_CLEE_CNT)>0)?1:0;

					int SelectMot = 0;
					switch(SelectMotCnt)
					{
					case 1: SelectMot = 0x01; break;
					case 2: SelectMot = (0x01 | 0x02); break;
					case 3: SelectMot = (0x01 | 0x02 | 0x04); break;
					case 4: SelectMot = (0x01 | 0x02 | 0x04 | 0x08); break;
					}
					if(*m_pStickTentionValue<0.0)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_START_STICK_TENSION%03d> Stick Tention Value Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					//1)Grip Tension Start
					//G_MotObj.m_fnAM01GripperTensionStart(true, SelectMot, *m_pStickTentionValue);
					if(SelectMot>0)
						//1)Grip Tension Start
						G_MotObj.m_fnAM01GripperTensionStart(true, SelectMot, *m_pStickTentionValue);
					else
					{
						G_WriteInfo(Log_Error, "<SEQ_STEP_START_STICK_TENSION%03d> Tension Axis Select Error");
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}

					int TimeOutWait = 16000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State != IDLE ||
						G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_MotObj.m_fnAM01GripperTensionStart(false, 0, *m_pStickTentionValue);//오류로 인한 인장 중지.
						G_WriteInfo(Log_Check, "<SEQ_STEP_START_STICK_TENSION%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_START].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_MotObj.m_fnAM01GripperTensionStart(false, 0, *m_pStickTentionValue);//오류로 인한 인장 중지.
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_START_STICK_TENSION%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;

			case SEQ_STEP_INPUT_JIG:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_INPUT_JIG(%03d)>", ProcStep);
					if(G_MotObj.m_fnStickJigMove(m_pGripperPos->nStickJig_Measure) == true)
					{
						int TimeOutWait = 3000;//About 30sec
						while(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].B_MOTCMD_State != IDLE )
						{
							TimeOutWait --;
							if(TimeOutWait<0 || m_SeqStop == true)
							{
								m_WaitCMD = 0;
								m_RecvCMD = 0;
								if(m_SeqStop== true)
									G_WriteInfo(Log_Worrying, "Wait CMD Stop");
								else
									G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
								break;
							}
							Sleep(10);
						}
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							G_WriteInfo(Log_Check, "<SEQ_STEP_INPUT_JIG%03d> Stop", ProcStep);
							fnSeqSetNextStep(SEQ_STEP_ERROR);
							break;
						}
						if(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].ActionResult.nResult != VS24MotData::Ret_Ok)
						{
							G_WriteInfo(Log_Worrying,"<SEQ_STEP_INPUT_JIG%03d> Mot Run Error", ProcStep);
							fnSeqSetNextStep(SEQ_STEP_ERROR);
							break;
						}
					}
					else
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_INPUT_JIG%03d> Jig Interlock Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_GRIPPER_Z_MEASURE:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_Z_MEASURE(%03d)>", ProcStep);
					G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Measure);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						
							break;
						}
						
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_GRIPPER_Z_MEASURE%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_GRIPPER_Z_MEASURE%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					
					//Stick 검사 시작 할수 있는 상태가 되었다.

					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_SEND_CIM_STICKID:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_SEND_CIM_STICKID(%03d)>", ProcStep);
					//CIM Interface 
					//G_pCIMInterface->m_fnSendLoadComplete(CString(m_pStickLoadInfo->strStickID));
					if(G_NowEQRunMode == Master_AUTO)
						G_pCIMInterface->m_CIMInfodlg->OnBnClickedBtLoadComplete();

					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_RECV_CIM_DATA:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_RECV_CIM_DATA(%03d)>", ProcStep);
					if(G_NowEQRunMode == Master_AUTO)
					{
						int TimeOutWait = 3000;//About 30sec
						while(G_pCIMInterface->m_bRecvJobStartCMD == false || G_pCIMInterface->m_bRecvMaterialData == false)
						{
							TimeOutWait --;
							if(TimeOutWait<0 || m_SeqStop == true)
							{
								m_WaitCMD = 0;
								m_RecvCMD = 0;
								if(m_SeqStop== true)
									G_WriteInfo(Log_Worrying, "Wait CMD Stop");
								else
									G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
								break;
							}
							Sleep(10);
						}
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							G_WriteInfo(Log_Check, "<SEQ_STEP_RECV_CIM_DATA%03d> Stop", ProcStep);
							//fnSeqSetNextStep(SEQ_STEP_ERROR);
							//break;
						}
						if(G_pCIMInterface->m_bRecvJobStartCMD == false || G_pCIMInterface->m_bRecvMaterialData == false)
						{//Host로 부터 Data를 못받은경우.
							if(G_pCIMInterface->m_bRecvJobStartCMD == false)
								G_WriteInfo(Log_Check, "<SEQ_STEP_RECV_CIM_DATA%03d> Job Start CMD error", ProcStep);
							if(G_pCIMInterface->m_bRecvMaterialData == false)
								G_WriteInfo(Log_Check, "<SEQ_STEP_RECV_CIM_DATA%03d> Material Data error", ProcStep);
						}
						else
						{
							G_WriteInfo(Log_Check, "<SEQ_STEP_RECV_CIM_DATA%03d> Material Data And Job Start OK", ProcStep);
						}
					}
					
					G_WriteInfo(Log_Normal, "Stick Load End");
					G_WriteInfo(Log_Normal, "▲▲▲=========================▲▲▲");
					fnSeqSetNextStep(SEQ_STEP_CNT);//정상 로딩 완료.
				}break;
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
			case SEQ_STEP_STICK_UNLOADING:
				{
					G_WriteInfo(Log_Normal, "▼▼▼=========================▼▼▼");
					G_WriteInfo(Log_Normal, "Stick Unload Start");
					G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_UNLOADING(%03d)>", ProcStep);
					
					fnSeqSetNextStep();//Auto Increment
				}break;

			case SEQ_STEP_UNLOAD_SPACE_SAFETY_Z:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_SPACE_SAFETY_Z(%03d)>", ProcStep);
					G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
					int TimeOutWait = 30000;//About 300sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_SPACE_SAFETY_Z%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_SCAN_SAFETY_Z:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_SCAN_SAFETY_Z(%03d)>", ProcStep);
					G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_SCAN_SAFETY_Z%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_RING_LIGHT_DOWN:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_RING_LIGHT_DOWN(%03d)>", ProcStep);
					G_MotObj.m_fnRingLightDown();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_RING_LIGHT_DOWN%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_REVIEW_DOWN:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_REVIEW_DOWN(%03d)>", ProcStep);
					G_MotObj.m_fnReviewDown();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_REVIEW_DOWN%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_SCOPE_SAFETY_Z:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_SCOPE_SAFETY_Z(%03d)>", ProcStep);
					//2)Grip Tension Sol BWD
					G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_SCAN_SAFETY_XY:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_SCAN_SAFETY_XY(%03d)>", ProcStep);

					G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_SCAN_SAFETY_XY%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_SCOPE_SAFETY_XY:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_SCOPE_SAFETY_XY(%03d)>", ProcStep);

					G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;

			case SEQ_STEP_STICK_UNLOADING_START:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_STICK_UNLOADING_START(%03d)>", ProcStep);
					//처리 결과를 Return 하자
					m_WaitCMD = nBiz_Seq_Stick_Result_ACK;
					m_RecvCMD = 0;
					m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Result, 1);//정상처리.
					//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Ready, 0);//비정상.

					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_Wait_Stick_Result_ACK:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_Wait_Stick_Result_ACK(%03d)>", ProcStep);
					int TimeOutWait = 3000;//About 60sec
					while(m_WaitCMD != m_RecvCMD)
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_Wait_Stick_Result_ACK%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;

			case SEQ_STEP_UNLOAD_GRIPPER_Z_TT01_OUT:
				{//Turn Table이 진입 할 수 있는 상태로 만들자.
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_Z_TT01_OUT(%03d)>", ProcStep);

					G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_TT01Out);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE 
						|| abs(G_MotInfo.m_pMotState->nMotionPos[AXIS_AM01_TensionLeftUpDn] - m_pGripperPos->nGripper_ZPos_TT01Out) > PosCheckInpos)//위치 이동 확인
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_Z_TT01_OUT%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_Z_TT01_OUT%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}

					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_OUTPUT_JIG:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_OUTPUT_JIG(%03d)>", ProcStep);
					if(G_MotObj.m_fnStickJigMove(m_pGripperPos->nStickJig_Standby) == true)
					{
						int TimeOutWait = 3000;//About 30sec
						while(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].B_MOTCMD_State != IDLE )
						{
							TimeOutWait --;
							if(TimeOutWait<0 || m_SeqStop == true)
							{
								m_WaitCMD = 0;
								m_RecvCMD = 0;
								if(m_SeqStop== true)
									G_WriteInfo(Log_Worrying, "Wait CMD Stop");
								else
									G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
								break;
							}
							Sleep(10);
						}
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_OUTPUT_JIG%03d> Stop", ProcStep);
							fnSeqSetNextStep(SEQ_STEP_ERROR);
							break;
						}
						if(G_MotObj.m_MotCMD_STATUS[AM01_JIG_INOUT].ActionResult.nResult != VS24MotData::Ret_Ok)
						{
							G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_OUTPUT_JIG%03d> Mot Run Error", ProcStep);
							fnSeqSetNextStep(SEQ_STEP_ERROR);
							break;
						}
						//Jig Out일때는 센서도 본다.
						if(G_MotInfo.m_pMotState->stEmissionTable.In15_Sn_MaskJigBWD != 1)
						{
							G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_OUTPUT_JIG%03d> Mask Jig BWD Detect Error", ProcStep);
							fnSeqSetNextStep(SEQ_STEP_ERROR);
							break;
						}
					}
					else
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_OUTPUT_JIG%03d> Jig Interlock Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_GRIPPER_ALL_TENSION_STANDBY:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_ALL_TENSION_STANDBY(%03d)>", ProcStep);
					////1)Grip Tension Axis (4*4 Axis 후퇴)
					////Left 4 Axis, Right 4Axis를 Standby Pos Move 시킨다.
					//G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_TensionStandby);
					//G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_TensionStandby);

					//int TimeOutWait = 3000;//About 30sec
					//while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State != IDLE ||
					//	G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State != IDLE )
					//{
					//	TimeOutWait --;
					//	if(TimeOutWait<0 || m_SeqStop == true)
					//	{
					//		m_WaitCMD = 0;
					//		m_RecvCMD = 0;
					//		if(m_SeqStop== true)
					//			G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					//		else
					//			G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

					//		break;
					//	}
					//	Sleep(10);
					//}
					//if(TimeOutWait<0 || m_SeqStop == true)
					//{
					//	G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_ALL_TENSION_STANDBY%03d> Stop", ProcStep);
					//	fnSeqSetNextStep(SEQ_STEP_ERROR);
					//	break;
					//}
					//if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok ||
					//	G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok)
					//{
					//	G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_ALL_TENSION_STANDBY%03d> Mot Run Error", ProcStep);
					//	fnSeqSetNextStep(SEQ_STEP_ERROR);
					//	break;
					//}

					m_WaitCMD = nBiz_Seq_Stick_Unload_Ready_OK;
					m_RecvCMD = 0;
					m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Ready, 1);//정상처리.
					//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Ready, 0);//비정상.
					fnSeqSetNextStep();//Auto Increment
				}break;

			case SEQ_STEP_Wait_Stick_Unload_Ready_OK:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_Wait_Stick_Unload_Ready_OK(%03d)>", ProcStep);
					int TimeOutWait = 3000;//About 60sec
					while(m_WaitCMD != m_RecvCMD)
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_Wait_Stick_Unload_Ready_OK%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					//1) TT01 Docking Check.
					if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_AM01_Pos)//TT01이 Docking위치 인지 확인
					{//TT01이 들어온 상태가 아니다.
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_Wait_TT01_Pos_UT02%03d> TT01 Pos Check Error!!!!", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;

			case SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP(%03d)>", ProcStep);
					//2)Grip Tension Sol BWD
					G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					m_WaitCMD = nBiz_Seq_TT01_VAC_ON;
					m_RecvCMD = 0;
					m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Standby, 1);//정상처리.
					//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Standby, 0);//비정상.
					fnSeqSetNextStep();//Auto Increment
				}break;

			case SEQ_STEP_Wait_TT01_VAC_ON:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_Wait_TT01_VAC_ON(%03d)>", ProcStep);
					int TimeOutWait = 3000;//About 60sec
					while(m_WaitCMD != m_RecvCMD)
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_Wait_TT01_VAC_ON%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}

					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_GRIPPER_OPEN:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_OPEN(%03d)>", ProcStep);
					//2)Grip Close
					G_MotObj.m_fnStickGripperOpen();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_OPEN%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_OPEN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_OPEN%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP_STANDBY:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP_STANDBY(%03d)>", ProcStep);
					//2)Grip Tension Sol BWD
					G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Grip_Standby);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP_STANDBY%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_Z_GRIP_STANDBY%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_GRIPPER_SOL_BWD:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_SOL_BWD(%03d)>", ProcStep);
					//2)Grip Tension Sol BWD
					G_MotObj.m_fnStickTentionBWD();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_BWD].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_SOL_BWD%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_BWD].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_SOL_BWD%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_GRIPPER_CLOSE:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_CLOSE(%03d)>", ProcStep);
					//2)Grip Close
					G_MotObj.m_fnStickGripperClose();
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_CLOSE%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_CLOSE].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_CLOSE%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_GRIPPER_ALL_STANDBY:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_ALL_STANDBY(%03d)>", ProcStep);
					//1)Grip Tension Axis (4*4 Axis 후퇴)
					//Left 4 Axis, Right 4Axis를 Standby Pos Move 시킨다.
					G_MotObj.m_fnAM01MultiMoveLTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);
					G_MotObj.m_fnAM01MultiMoveRTension((0x01 |0x02 |0x04 |0x08), m_pGripperPos->nGripper_TPos_Standby);

					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].B_MOTCMD_State != IDLE ||
						G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_ALL_STANDBY%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_TENTION_LEFT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok ||
						G_MotObj.m_MotCMD_STATUS[AM01_TENTION_RIGHT_MULTI].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_ALL_STANDBY%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_GRIPPER_Z_STANDBY:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_GRIPPER_Z_STANDBY(%03d)>", ProcStep);
					//2)Grip Tension Sol BWD
					G_MotObj.m_fnAM01GripperUpDown(m_pGripperPos->nGripper_ZPos_Standby);
					int TimeOutWait = 3000;//About 30sec
					while(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].B_MOTCMD_State != IDLE )
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_GRIPPER_Z_STANDBY%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					if(G_MotObj.m_MotCMD_STATUS[AM01_GRIPPER_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
					{
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_GRIPPER_Z_STANDBY%03d> Mot Run Error", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					//준비 상태가 완료 되면 Ack를 전달.(Seq 종단에 추가 하자.)
					//m_WaitCMD = nBiz_Seq_TT01_Pos_Turn;
					//m_RecvCMD = 0;
					m_pServerInterface->m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Unload_Comp, 1);//정상처리.
					//m_ServerInterface.m_fnSendCommand_Nrs(TASK_11_Indexer, nBiz_Func_MasterToIndexer, nBiz_Seq_Stick_Information_Ack, 0);//비정상.

					fnSeqSetNextStep();//Auto Increment
				}break;
			case SEQ_STEP_UNLOAD_Wait_TT01_Pos_UT02:
				{
					G_WriteInfo(Log_Normal, "<SEQ_STEP_UNLOAD_Wait_TT01_Pos_UT02(%03d)>", ProcStep);

					int TimeOutWait = 16000;//About 120sec
					while(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)//(m_WaitCMD != m_RecvCMD)
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							//m_WaitCMD = 0;
							//m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");

							break;
						}

						Sleep(10);
					}
					//nBiz_Seq_TT01_Pos_UT02
					//m_WaitCMD = 0;
					//m_RecvCMD = 0;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_UNLOAD_Wait_TT01_Pos_UT02%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
					//1) TT01 Docking Check.
					if(G_MotObj.m_TT01_NowPos != CMotCtrlObj::TT01_UT02_Pos)//TT01이 대기 위치로 간것을 확인한다.
					{//TT01이 들어온 상태가 아니다.
						G_WriteInfo(Log_Worrying,"<SEQ_STEP_UNLOAD_Wait_TT01_Pos_UT02%03d> TT01 Pos Check Error!!!!", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}

					G_WriteInfo(Log_Normal, "Stick Unload End");
					G_WriteInfo(Log_Normal, "▲▲▲=========================▲▲▲");

					//CIM Interface 
					//G_pCIMInterface->m_fnSendUnloadComp(CString(m_pStickLoadInfo->strStickID));
					G_pCIMInterface->m_CIMInfodlg->OnBnClickedBtUnloadComplete();

					fnSeqSetNextStep(SEQ_STEP_CNT);//Unload End
				}break;
	//case SEQ_STEP_GRIPPER_ALL_BWD:
	//	{
	//		G_WriteInfo(Log_Normal, "<SEQ_STEP_GRIPPER_ALL_BWD(%03d)>", ProcStep);


	//		fnSeqSetNextStep();//Auto Increment
	//	}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			G_MotObj.m_fnBuzzer2On(CALL_BUZZER2_STICK_LOAD_END);
			G_WriteInfo(Log_Normal, "<CSeq_StickExchange%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_StickExchange> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}