#pragma once


// CStickTransperSub 대화 상자입니다.

class CStickTransperSub : public CDialogEx
{
	DECLARE_DYNAMIC(CStickTransperSub)

public:
	CStickTransperSub(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStickTransperSub();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_TM02_2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
};
