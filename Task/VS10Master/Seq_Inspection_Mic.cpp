#include "StdAfx.h"
#include "Seq_Inspection_Mic.h"


CSeq_Inspect_Mic::CSeq_Inspect_Mic(void)
{
	m_StartTech = 0;
	m_SeqTechTime = 0;
	m_pServerInterface = nullptr;

	m_pAlignCellStartX = nullptr;
	m_pAlignCellStartY = nullptr;
	m_pGlassReicpeParam = nullptr;
	m_pstMaskImageInfo_Mic = nullptr;
	m_pstCommonPara_Mic = nullptr;
	//m_pstMaskImageInfo_Mac = nullptr;
	//m_pstCommonPara_Mac = nullptr;

	m_pBlockEnd =nullptr;
	m_pStartBlockTick =nullptr;
	m_pStartLineTick =nullptr;
	m_pCamScnaLineCnt =nullptr;
	m_pCamScnaImgCnt =nullptr;
	m_pStartScanLineNum =nullptr;

	m_pScopePos = nullptr;
	m_pReviewPos = nullptr;

	m_InspectStartShift = 0;
	m_InspectStartDrive = 0;

	m_RecvCMD_Ret = -1;
	m_RecvCMD = 0;
	m_WaitCMD = 0;

	m_NowScanIndex = 0;
	m_pLineScanStartPosList = nullptr;

	m_pSeqObj_InsAutoReview = nullptr;

	m_pbInspectMicOrMac = nullptr;//true : Mic, false:Mac
}


CSeq_Inspect_Mic::~CSeq_Inspect_Mic(void)
{
	fnSeqSetNextStep(SEQ_STEP_IDLE);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;

	if(m_pLineScanStartPosList != nullptr)
		delete [] m_pLineScanStartPosList;
	m_pLineScanStartPosList = nullptr;
}

bool CSeq_Inspect_Mic::fnSeqFlowStart()
{
	fnSeqErrorReset();
	fnSeqSetNextStep(SEQ_STEP_RUN);
	m_StartTech = ::GetTickCount();
	m_SeqTechTime = 0;
	return true;
}
int CSeq_Inspect_Mic::fnSeqErrorStepProc()
{
	G_MotObj.m_fnAM01MotAllStop();
	//Seq Error 시에는 cylinder를 무조건 내린다.
	G_MotObj.m_fnReviewDown();
	G_MotObj.m_fnRingLightDown();

	m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, nBiz_AOI_Inspection, nBiz_Send_ScanStop, 0);
	G_WriteInfo(Log_Error, "CSeq_Inspect_Mic Error Step Run( Z Axis Go to a safe position)");

	//Defect Review End
	::SendMessage(AfxGetMainWnd()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 9919);
	return 0;
}
void CSeq_Inspect_Mic::fnSeqUpdateTech()
{
	int nowSeqIndex = fnSeqGetNowStep();
	if(nowSeqIndex>SEQ_STEP_RUN && nowSeqIndex < SEQ_STEP_CNT)
		m_SeqTechTime = ::GetTickCount()- m_StartTech;
}
int CSeq_Inspect_Mic::fnSeqFlowProc(int ProcStep)
{
	switch(ProcStep)
	{
	case SEQ_STEP_ERROR:
		break;
	case SEQ_STEP_IDLE:
		break;
	case SEQ_STEP_RUN:
		{
			G_WriteInfo(Log_Normal, "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
			G_WriteInfo(Log_Normal, "<CSeq_Inspect_Mic%03d> Sequence Start", ProcStep);
			*m_pbInspectMicOrMac = true;//true : Mic, false:Mac
			fnSeqSetNextStep();//Auto Increment
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_STICK_STATE_CHECK:
		{//Stick이 측정 가능 상태 인지 체크 한다.

			if(G_CheckStickProc_StaticState() == ST_PROC_NG)
			{
				G_WriteInfo(Log_Check, "SEQ_STEP_STICK_STATE_CHECK ST_PROC_NG", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SPACE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SPACE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnSpaceSensorMove(m_pScopePos->nSpace_ZPos_Standby);
			int TimeOutWait = 30000;//About 300sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SPACE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SPACE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SPACE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_RING_LIGHT_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnRingLightDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_REVIEW_DOWN:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_REVIEW_DOWN(%03d)>", ProcStep);
			G_MotObj.m_fnReviewDown();
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_REVIEW_DOWN%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_Z:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCOPE_SAFETY_Z(%03d)>", ProcStep);
			G_MotObj.m_fnScopeUpDnMove(m_pScopePos->nScope_ZPos_Standby);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_Z%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotInfo.m_pMotState->m_bScopeSafetyPos == false)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_Z%03d> Scope Safety Sensor Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	
	case SEQ_STEP_SCAN_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCOPE_SAFETY_XY:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SAFETY_XY(%03d)>", ProcStep);

			G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
		
		
	case SEQ_STEP_SCAN_INSPECT_Z_POS:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_INSPECT_Z_POS(%03d)>", ProcStep);
			G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Inspection);
			int TimeOutWait = 3000;//About 30sec
			while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_INSPECT_Z_POS%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
			{
				G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_INSPECT_Z_POS%03d> Mot Run Error", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_INSPECTION_INIT:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_INSPECTION_INIT(%03d)>", ProcStep);
			m_WaitCMD = 0;
			m_RecvCMD = 0;
			m_RecvCMD_Ret = -1;
			m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, nBiz_AOI_System, nBiz_Send_UnitInitialize, 0);
			G_WriteInfo(Log_Normal, "Master to Inspector UnitInitialize");
			m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, nBiz_AOI_System, nBiz_Send_AlarmReset, 0);
			G_WriteInfo(Log_Normal, "Master to Inspector AlarmReset");
			Sleep(500);
			fnSeqSetNextStep();//Auto Increment
		}break;
	case SEQ_STEP_SCAN_SEND_RECIPE:
		{
			G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_SEND_RECIPE(%03d)>", ProcStep);
			//////////////////////////////////////////////////////////////////////////
			SYSTEMTIME NowTime;
			GetLocalTime(&NowTime);
			char ImgFolderName[128];
			memset(ImgFolderName, 0x00, 128);
			sprintf(ImgFolderName,"I_%04d%02d%02d",NowTime.wYear, NowTime.wMonth, NowTime.wDay);

			CString strNewValue;
			char strReadData[256];
			memset(strReadData, 0x00, 256);
			GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
			strNewValue.Format("%s", strReadData);
			if(strNewValue.GetLength()==0)
				strNewValue.Format("None");
			sprintf_s(m_pGlassReicpeParam->m_GlassName, "%s\\%s", ImgFolderName, strNewValue);

			////////////////////////////////////////////////////////////////////////////
			//Cell Start 좌표를 이용 하여. Center 좌표와 Edge 좌표를 구한다.
			//Align을 할 경우 실제 좌표가 입력 되어야 한다.
			int CellStartX = *m_pAlignCellStartX;
			int CellStartY = *m_pAlignCellStartY;

			if(m_pGlassReicpeParam->GLASS_CELL_COUNT_X>0 && m_pGlassReicpeParam->GLASS_CELL_COUNT_Y>0)
			{
				int CellGapDisX = abs(m_pGlassReicpeParam->CELL_DISTANCE_X-m_pGlassReicpeParam->CELL_SIZE_X);
				int CellGapDisY = abs(m_pGlassReicpeParam->CELL_DISTANCE_Y-m_pGlassReicpeParam->CELL_SIZE_Y);

				int AllCellDisX = (m_pGlassReicpeParam->GLASS_CELL_COUNT_X*m_pGlassReicpeParam->CELL_SIZE_X) +
					((m_pGlassReicpeParam->GLASS_CELL_COUNT_X-1)*CellGapDisX);
				int AllCellDisY = (m_pGlassReicpeParam->GLASS_CELL_COUNT_Y*m_pGlassReicpeParam->CELL_SIZE_Y) +
					((m_pGlassReicpeParam->GLASS_CELL_COUNT_Y-1)*CellGapDisY);
				//Recipe Load Cell 좌표.
				//DrawCellStartX = (m_pGlassReicpeParam->STICK_WIDTH/2)-(AllCellDisX/2);//um
				//DrawCellStartY = (1190000/2)-(AllCellDisY/2);//um
				m_pGlassReicpeParam->CenterPointX =  CellStartX+(AllCellDisX/2);
				m_pGlassReicpeParam->CenterPointY =  CellStartY+(AllCellDisY/2);

				m_pGlassReicpeParam->CELL_EDGE_X = abs(AllCellDisX/2)*(-1);
				m_pGlassReicpeParam->CELL_EDGE_Y = abs(AllCellDisY/2);
			}
			//Glass Param Send.
			int SendDataSize  = 0;
			//////////////////////////////////////////////////////////////////////////
			//Active Param Send
			SendDataSize = sizeof(SAOI_IMAGE_INFO)+sizeof(SAOI_COMMON_PARA);
			char *pSnedActiveParam = new char[SendDataSize];
			memcpy(&pSnedActiveParam[0],m_pstMaskImageInfo_Mic, sizeof(SAOI_IMAGE_INFO));
			memcpy(&pSnedActiveParam[sizeof(SAOI_IMAGE_INFO)],m_pstCommonPara_Mic, sizeof(SAOI_COMMON_PARA));

			m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, 
				nBiz_AOI_Parameter, nBiz_Send_ActiveAreaParam, 0, 
				(USHORT)SendDataSize, 
				(UCHAR*)&pSnedActiveParam[0]);
			delete [] pSnedActiveParam;
			//////////////////////////////////////////////////////////////////////////
			//Pad Param Send(Stick에는 의미 없다.== Dumy Cell에 써볼까?)
			SendDataSize = (sizeof(SAOI_IMAGE_INFO)*4)+(sizeof(SAOI_COMMON_PARA)*4);
			char *pSnedPadParam = new char[SendDataSize];
			int JumpSize =  sizeof(SAOI_IMAGE_INFO) + sizeof(SAOI_COMMON_PARA);

			for(int SetStep =0; SetStep<4; SetStep++)
			{
				memcpy(&pSnedPadParam[(JumpSize*SetStep)],										 m_pstMaskImageInfo_Mic, sizeof(SAOI_IMAGE_INFO));
				memcpy(&pSnedPadParam[(JumpSize*SetStep) + sizeof(SAOI_IMAGE_INFO)], m_pstCommonPara_Mic, sizeof(SAOI_COMMON_PARA));
			}

			m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, 
				nBiz_AOI_Parameter, nBiz_Send_PadAreaParam, 0, 
				(USHORT)SendDataSize, 
				(UCHAR*)&pSnedPadParam[0]);
			delete [] pSnedPadParam;
			//////////////////////////////////////////////////////////////////////////
			//Cell Real Pos Data Setting. 나중에 필요하면 추가 하자.(Simul에 있어 ㅡ.ㅡ;)


			//////////////////////////////////////////////////////////////////////////
			//Glass(Stick)정보를 전달 하자.
			SendDataSize  = sizeof(GLASS_PARAM)+1;
			char *pSnedHWGlassParam = new char[SendDataSize];
			memcpy(&pSnedHWGlassParam[0], m_pGlassReicpeParam, sizeof(GLASS_PARAM));

			pSnedHWGlassParam[SendDataSize-1] = 0;
			m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, 
				nBiz_AOI_Parameter, nBiz_Send_GlassParamSet, 0, 
				(USHORT)SendDataSize, 
				(UCHAR*)&pSnedHWGlassParam[0]);
			delete [] pSnedHWGlassParam;

			//히스토그램 그리는 정보 ㅡ.ㅡ 기억이 잘 ㅡ,ㅡ;;;;;;;
			int HistoSkipIndex = 3;
			m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, 
				nBiz_AOI_Inspection, nBiz_Send_IllValueStartEndIndex, (USHORT)HistoSkipIndex);

			//Inspector에서 Param Set후 마지막으로 보내는 CMD를 체크 하자.
			m_WaitCMD = nBiz_Recv_ScanImgDrawInfo;
			m_RecvCMD = 0;
			m_RecvCMD_Ret = -1;
			int TimeOutWait = 3000;//About 20sec
			while(m_WaitCMD != m_RecvCMD)
			{
				TimeOutWait --;
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					m_RecvCMD_Ret = -1;
					if(m_SeqStop== true)
						G_WriteInfo(Log_Worrying, "Wait CMD Stop");
					else
						G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
					break;
				}
				Sleep(10);
			}
			if(TimeOutWait<0 || m_SeqStop == true)
			{
				G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_SEND_RECIPE%03d> Stop", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}
			if(m_RecvCMD_Ret <=0)
			{
				G_WriteInfo(Log_Error,"<SEQ_STEP_SCAN_SEND_RECIPE%03d> Scan Image Cnt Error!!!!", ProcStep);
				fnSeqSetNextStep(SEQ_STEP_ERROR);
				break;
			}

			fnSeqSetNextStep();//Auto Increment
		}break;

		case SEQ_STEP_SCAN_INSPECT_START_XY:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_INSPECT_START_XY(%03d)>", ProcStep);
				//1)Scan Start Pos(Review).
				m_InspectStartShift = m_pHWRecipeParam->m_AOI_BLOCK_SCAN_START_X;
				m_InspectStartDrive = m_pHWRecipeParam->m_AOI_BLOCK_SCAN_START_Y;
				////2) Scan Cam위치로 옮긴다.
				//m_InspectStartShift = m_InspectStartShift+m_pCamToObjDis->nReviewCamTo_LineScanX;
				//m_InspectStartDrive = m_InspectStartDrive+m_pCamToObjDis->nReviewCamTo_LineScanY;

				//m_fnAM01LineScanSyncMove는 Back Light가 Scan Cam을 따라 다닌다.
				G_MotObj.m_fnAM01LineScanSyncMove(m_InspectStartShift, m_InspectStartDrive);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_INSPECT_START_XY%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_INSPECT_START_XY%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_SCAN_LIGHT:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_LIGHT(%03d)>", ProcStep);
				G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, m_pReviewPos->nInspection_MicRingLight, m_pReviewPos->nInspection_MicBackLight ,0);

				CString strNewValue;
				strNewValue.Format("%0.1f", (float)m_pReviewPos->nInspection_MicRingLight);
				WritePrivateProfileString(Section_SVID, Key_STICK_INSPEC_DARKFIELD_VALUE, strNewValue, SVID_LIST_PATH);
				strNewValue.Format("%0.1f", (float)m_pReviewPos->nInspection_MicBackLight);
				WritePrivateProfileString(Section_SVID, Key_STICK_INSPEC_BACKLIGHT_VALUE, strNewValue, SVID_LIST_PATH);
				strNewValue.Format("%0.1f", (float)m_pReviewPos->nInspection_MacReflectLight);
				WritePrivateProfileString(Section_SVID, Key_STICK_INSPEC_MACRO_LIGHT_VALUE, strNewValue, SVID_LIST_PATH);

				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_LIGHT%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_LIGHT%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_RING_LIGHT_UP:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_RING_LIGHT_UP(%03d)>", ProcStep);
				G_MotObj.m_fnRingLightUp();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_UP].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_RING_LIGHT_UP%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_UP].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_RING_LIGHT_UP%03d> Sol Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_SCAN_INSPECT_DATA_CALC:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_INSPECT_DATA_CALC(%03d)>", ProcStep);
				//좌표 Data 생성.
				if(m_pLineScanStartPosList != nullptr)
					delete [] m_pLineScanStartPosList;
				m_pLineScanStartPosList = new CPoint[(*m_pCamScnaLineCnt)*2];

				for(int PStep = 1; PStep<=(*m_pCamScnaLineCnt); PStep++)
				{
					if(PStep%2==1)
					{//Odd Scan
						m_pLineScanStartPosList[PStep-1].y = m_InspectStartDrive;
					}
					else
					{//even Scan
						m_pLineScanStartPosList[PStep-1].y = m_InspectStartDrive + m_pHWRecipeParam->m_INSPECTION_SCAN_HEIGHT;
					}
					m_pLineScanStartPosList[PStep-1].x = m_InspectStartShift+ (m_pGlassReicpeParam->m_ScanLineShiftDistance*(PStep-1));
				}

				//Trigger Setting(시작 위치에서 수행 하자.)
				//G_MotObj.m_fnInspectTriggerSet(4.985f);//5um로 하자
				G_MotObj.m_fnInspectTriggerSet(5.0f);//5um로 하자
				//Scan 시작 Index를 설정 하자.
				m_NowScanIndex = 1;
				*m_pStartScanLineNum = m_NowScanIndex;
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_SCAN_INSPECT_SCAN_START_POS_XY:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_INSPECT_SCAN_START_POS_XY(%03d)>", ProcStep);
				//m_fnAM01LineScanSyncMove는 Back Light가 Scan Cam을 따라 다닌다.
				G_MotObj.m_fnAM01LineScanSyncMove(m_pLineScanStartPosList[m_NowScanIndex-1].x, m_pLineScanStartPosList[m_NowScanIndex-1].y);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_INSPECT_SCAN_START_POS_XY%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_INSPECT_SCAN_START_POS_XY%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_SCAN_INSPECT_SCAN_START:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_INSPECT_SCAN_START(%03d)>", ProcStep);
				//끝나는 위치계산.
				int EndShiftPos = m_pLineScanStartPosList[m_NowScanIndex-1].x;
				int EndDrivePos = 0;
				if(m_NowScanIndex%2 ==1)//Odd Scan
					EndDrivePos = m_pLineScanStartPosList[m_NowScanIndex-1].y + m_pHWRecipeParam->m_INSPECTION_SCAN_HEIGHT;
				else//EvenScan
					EndDrivePos = m_pLineScanStartPosList[m_NowScanIndex-1].y - m_pHWRecipeParam->m_INSPECTION_SCAN_HEIGHT;
				G_WriteInfo(Log_Check,"Start Pos ==> Shift(%d), Drive(%d)", m_pLineScanStartPosList[m_NowScanIndex-1].x, m_pLineScanStartPosList[m_NowScanIndex-1].y);
				G_WriteInfo(Log_Check,"End Pos ==> Shift(%d), Drive(%d)", EndShiftPos, EndDrivePos);


				//Scan Direction Check.
				CString strCamMsg;
				if(m_NowScanIndex %2 == 0)
					strCamMsg.Format("scd %d", 1);
				else
					strCamMsg.Format("scd %d", 0);
				//Scan Direction Select
				m_pServerInterface->m_fnSendCommand_Nrs(TASK_21_Inspector, nBiz_AOI_System, nBiz_Send_CamCommand, 0, strCamMsg.GetLength(), (UCHAR*)strCamMsg.GetBuffer(strCamMsg.GetLength()));
				Sleep(100);
				*m_pStartLineTick = GetTickCount();
				*m_pStartBlockTick= *m_pStartLineTick;

				//Send Scan Start
				m_WaitCMD = nBiz_Recv_ScanReady;
				m_RecvCMD = 0;
				m_RecvCMD_Ret = -1;
				m_pServerInterface->m_fnSendCommand_Nrs(	TASK_21_Inspector, nBiz_AOI_Inspection, nBiz_Send_ScanStart, m_NowScanIndex);
				int TimeOutWait = 3000;//About 20sec
				while(m_WaitCMD != m_RecvCMD)
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_INSPECT_SCAN_START%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				G_MotObj.m_fnAM01LineScanSyncMove(EndShiftPos, EndDrivePos+INSPECT_SCAN_OVER_um);
				fnSeqSetNextStep();//Auto Increment
			}break;

		case SEQ_STEP_SCAN_INSPECT_SCAN_END:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_SCAN_INSPECT_SCAN_END(%03d)>", ProcStep);
				
				bool bInspectorProcessEnd = false;
				m_WaitCMD = nBiz_Recv_ScanPADEnd;
				m_RecvCMD = 0;
				m_RecvCMD_Ret = -1;
				//모션이 끝나기를 기다리고.
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					if(m_WaitCMD == m_RecvCMD)//감속 구간에 Cmd가 들어 올 수 있다.
					{
						bInspectorProcessEnd = true;
						m_WaitCMD = 0;
						m_RecvCMD = 0;
					}
					
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_INSPECT_SCAN_END%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_SCOPE_SYNC_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_SCAN_INSPECT_SCAN_END%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(bInspectorProcessEnd == false)//아직도 안들어왔다면 더기다리자.
				{
					TimeOutWait = 3000;//About 20sec
					while(m_WaitCMD != m_RecvCMD)
					{
						TimeOutWait --;
						if(TimeOutWait<0 || m_SeqStop == true)
						{
							m_WaitCMD = 0;
							m_RecvCMD = 0;
							if(m_SeqStop== true)
								G_WriteInfo(Log_Worrying, "Wait CMD Stop");
							else
								G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
							break;
						}
						Sleep(10);
					}
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						G_WriteInfo(Log_Check, "<SEQ_STEP_SCAN_INSPECT_SCAN_END%03d> Stop", ProcStep);
						fnSeqSetNextStep(SEQ_STEP_ERROR);
						break;
					}
				}
				

				//Next Scan
				m_NowScanIndex++;
				*m_pStartScanLineNum = m_NowScanIndex;
				if(m_NowScanIndex<=(*m_pCamScnaLineCnt))
				{
					fnSeqSetNextStep(SEQ_STEP_SCAN_INSPECT_SCAN_START_POS_XY);
					break;
				}
				//종료되면 Block End는 Main에서 받는다.(File도)
				//Backup이 완료되기를 기다린다.
				m_WaitCMD = nBiz_Recv_BackupEnd;
				m_RecvCMD = 0;
				m_RecvCMD_Ret = -1;
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_END_SCAN_LIGHT:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCAN_LIGHT(%03d)>", ProcStep);
				G_MotObj.m_fnSetLight(Light_Ring|Light_Back|Light_Reflect, 0, 0 ,0);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCAN_LIGHT%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_LIGHT].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCAN_LIGHT%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;

		case SEQ_STEP_END_RING_LIGHT_DOWN:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_END_RING_LIGHT_DOWN(%03d)>", ProcStep);
				G_MotObj.m_fnRingLightDown();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_END_RING_LIGHT_DOWN%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_RING_LIGHT_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_RING_LIGHT_DOWN%03d> Sol Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_END_REVIEW_DOWN:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_END_REVIEW_DOWN(%03d)>", ProcStep);
				G_MotObj.m_fnReviewDown();
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_END_REVIEW_DOWN%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_REVIEW_DOWN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_REVIEW_DOWN%03d> Sol Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_AUTO_REVEW_SEQ:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_AUTO_REVEW_SEQ(%03d)>", ProcStep);
				//Backup이 완료되기를 기다린다.
				int TimeOutWait = 3000;//About 30sec
				while(m_WaitCMD != m_RecvCMD)
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_AUTO_REVEW_SEQ%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}

				//Auto Review를 시작 한다.
				m_pSeqObj_InsAutoReview->fnSeqFlowStart();
				Sleep(200);
				TimeOutWait =500000;//About 3Min
				while(m_pSeqObj_InsAutoReview->fnSeqGetNowStep() != CSeq_InsAutoReview::SEQ_STEP_IDLE &&
					m_pSeqObj_InsAutoReview->fnSeqGetNowStep() != CSeq_InsAutoReview::SEQ_STEP_ERROR)
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					m_pSeqObj_InsAutoReview->fnSeqSetStop();
					G_WriteInfo(Log_Check, "<SEQ_STEP_AUTO_REVEW_SEQ%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(m_pSeqObj_InsAutoReview->fnSeqGetNowStep() == CSeq_InsAutoReview::SEQ_STEP_ERROR)
				{
					m_WaitCMD = 0;
					m_RecvCMD = 0;
					G_WriteInfo(Log_Worrying, "Auto Review Error");
					m_pSeqObj_InsAutoReview->fnSeqErrorReset();
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_END_SCAN_SAFETY_Z:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_AUTO_REVEW_SEQ(%03d)>", ProcStep);
				G_MotObj.m_fnScanUpDnMove(m_pReviewPos->nScan_ZPos_Standby);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCAN_SAFETY_Z%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_UPDN].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCAN_SAFETY_Z%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_END_SCAN_SAFETY_XY:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCAN_SAFETY_XY(%03d)>", ProcStep);

				G_MotObj.m_fnScanMultiMove(m_pReviewPos->nScan_Standby_Shift, m_pReviewPos->nScan_Standby_Drive);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCAN_SAFETY_XY%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCAN_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCAN_SAFETY_XY%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		case SEQ_STEP_END_SCOPE_SAFETY_XY:
			{
				G_WriteInfo(Log_Normal, "<SEQ_STEP_END_SCOPE_SAFETY_XY(%03d)>", ProcStep);

				G_MotObj.m_fnScopeMultiMove(m_pScopePos->nScope_Standby_Shift, m_pScopePos->nScope_Standby_Drive);
				int TimeOutWait = 3000;//About 30sec
				while(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].B_MOTCMD_State != IDLE )
				{
					TimeOutWait --;
					if(TimeOutWait<0 || m_SeqStop == true)
					{
						m_WaitCMD = 0;
						m_RecvCMD = 0;
						if(m_SeqStop== true)
							G_WriteInfo(Log_Worrying, "Wait CMD Stop");
						else
							G_WriteInfo(Log_Worrying, "Wait CMD Time Out");
						break;
					}
					Sleep(10);
				}
				if(TimeOutWait<0 || m_SeqStop == true)
				{
					G_WriteInfo(Log_Check, "<SEQ_STEP_END_SCOPE_SAFETY_XY%03d> Stop", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				if(G_MotObj.m_MotCMD_STATUS[AM01_SCOPE_MULTI_MOVE_XY].ActionResult.nResult != VS24MotData::Ret_Ok)
				{
					G_WriteInfo(Log_Worrying,"<SEQ_STEP_END_SCOPE_SAFETY_XY%03d> Mot Run Error", ProcStep);
					fnSeqSetNextStep(SEQ_STEP_ERROR);
					break;
				}
				fnSeqSetNextStep();//Auto Increment
			}break;
		//////////////////////////////////////////////////////////////////////////
	case SEQ_STEP_CNT:
		{
			//Defect Review End
			::SendMessage(AfxGetMainWnd()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 9919);

			G_WriteInfo(Log_Normal, "<CSeq_Inspect_Mic%03d> Sequence End", ProcStep);
			G_WriteInfo(Log_Normal, "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
			fnSeqSetNextStep(SEQ_STEP_IDLE);
		}
		break;


	default:
		G_WriteInfo(Log_Normal, "◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆");
		G_WriteInfo(Log_Normal, "<CSeq_Inspect_Mic> Unknown Old Step(%d)->Step(%d), SEQ IDLE", fnSeqGetOldStep(), ProcStep);
		fnSeqSetNextStep(SEQ_STEP_IDLE);
		break;
	}
	fnSeqUpdateTech();
	return fnSeqGetNowStep();
}