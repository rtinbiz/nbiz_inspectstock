#pragma once
#include "..\..\CommonHeader\Inspection\DefineStaticValue.h"
typedef struct DataNodeObj_AGT
{
	DataNodeObj_AGT *	m_pFrontNode;
	DataNodeObj_AGT *	m_pNextNode;


	int m_SizeSortIndex;
	int m_MachinePosX;
	int m_MachinePosY;
	COORD_DINFO			DefectData;
	IplImage *m_pSmallImg;
	CString	m_strReviewFileName;

	DataNodeObj_AGT()
	{
		m_pFrontNode	= NULL;
		m_pNextNode		= NULL;

		m_pSmallImg = nullptr;
		m_SizeSortIndex = -1;
		m_strReviewFileName.Format("");

		m_MachinePosX = 0;
		m_MachinePosY = 0;
	}
	~DataNodeObj_AGT()
	{
		if(m_pSmallImg!= nullptr)
			cvReleaseImage(&m_pSmallImg);
		m_pSmallImg = nullptr;
	}
}DataNodeObj;
//////////////////////////////////////////////////////////////////////////
//Sen Command Queue./ Receive Data Queue.
class DefectDataQ  
{
public:
	DefectDataQ();
	~DefectDataQ();
private:
	DataNodeObj m_HeadNode;
	DataNodeObj m_TailNode; 

	DataNodeObj *m_pReadPos; 
	CRITICAL_SECTION m_CrtSection;
	int		m_CntNode;
	
public:
	bool  m_bDataUpdateEnd;


	int		QGetCnt(){return m_CntNode;};
	void	QPutNode(COORD_DINFO	*pNewObj);
	void	QPutBackNode(COORD_DINFO	*pNewObj);

	bool	QPutMergeNode(COORD_DINFO	*pNewObj, IplImage *pSmImg, double MergeDis);

	bool	QGetMoveFirstNode();
	
	bool	QSetNextNodeImage( IplImage *pSmallImg);

	bool	QGetNextNodeData(COORD_DINFO*pDataBuf, IplImage ***pppSmallImg = nullptr, CString **ppstrReviewPathBuf = nullptr);
	bool	QSetNextNodePath( CString strReviewPathBuf);
	DataNodeObj * QGetNextNodeData();

	COORD_DINFO	QGetNode(IplImage **ppSmalImg);

	bool	QGetMoveLastNode();
	bool	QGetFrontNodeData(COORD_DINFO*pDataBuf);

	void    NodeDataChange(DataNodeObj*pNode_A, DataNodeObj*pNode_B);
	bool	QSortBySize(bool updateIndex = true);
	bool	QSortByDistance(int nCount, int BasePosX=0, int BasePosY=0);
	void	QClean(void);
};