// SubDlg_CIMInterface.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "SubDlg_CIMInterface.h"
#include "afxdialogex.h"


// SubDlg_CIMInterface 대화 상자입니다.

IMPLEMENT_DYNAMIC(SubDlg_CIMInterface, CDialogEx)

SubDlg_CIMInterface::SubDlg_CIMInterface(CWnd* pParent /*=NULL*/)
	: CDialogEx(SubDlg_CIMInterface::IDD, pParent)
{


	m_EQST_Mode = EQP_NORMAL;//Normal, Fault, PM
	m_PRST_Process = PRST_SETUP;//IDLE, Setup, Excute, Pause, Disable
	G_VS10TASK_STATE = TASK_STATE_INIT;

	m_nAlarmListCount = 0;

	m_bAlarmHeavyChange = false;

	ECID_List_Cnt = GetPrivateProfileInt(Section_ECID_INFO, Section_ECID_CNT, 14, SetECIDList_PATH);
	G_strECIDList =new ECIDInfo[ECID_List_Cnt];
	memset((char*)G_strECIDList, 0x00, sizeof(ECIDInfo)*ECID_List_Cnt);
	
}

SubDlg_CIMInterface::~SubDlg_CIMInterface()
{
	ECID_List_Cnt = 0;
	delete [] G_strECIDList;
	G_strECIDList = nullptr;
}

void SubDlg_CIMInterface::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CIM_LOG, m_listCIMLogView);
	DDX_Control(pDX, IDC_CB_ALARM_INDEX, m_cbAlarmList);
	DDX_Control(pDX, IDC_GRID_CIM_ECID, m_gridECIDList);
}


BEGIN_MESSAGE_MAP(SubDlg_CIMInterface, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	//ON_MESSAGE(WM_USER_CIM_VIEW_MSG, &SubDlg_CIMInterface::OnUpdateCIMMsg)
	ON_BN_CLICKED(IDC_BT_LOAD_COMPLETE, &SubDlg_CIMInterface::OnBnClickedBtLoadComplete)
	ON_BN_CLICKED(IDC_BT_INSPECTION_START, &SubDlg_CIMInterface::OnBnClickedBtInspectionStart)
	ON_BN_CLICKED(IDC_BT_INSPECTION_END, &SubDlg_CIMInterface::OnBnClickedBtInspectionEnd)
	ON_BN_CLICKED(IDC_BT_UNLOAD_COMPLETE, &SubDlg_CIMInterface::OnBnClickedBtUnloadComplete)
	ON_BN_CLICKED(IDC_BT_EQ_NORMAL, &SubDlg_CIMInterface::OnBnClickedBtEqNormal)
	ON_BN_CLICKED(IDC_BT_EQP_FAULT, &SubDlg_CIMInterface::OnBnClickedBtEqpFault)
	ON_BN_CLICKED(IDC_BT_EQP_PM, &SubDlg_CIMInterface::OnBnClickedBtEqpPm)
	ON_BN_CLICKED(IDC_BT_PRST_IDLE, &SubDlg_CIMInterface::OnBnClickedBtPrstIdle)
	ON_BN_CLICKED(IDC_BT_PRST_SETUP, &SubDlg_CIMInterface::OnBnClickedBtPrstSetup)
	ON_BN_CLICKED(IDC_BT_PRST_EXECUTE, &SubDlg_CIMInterface::OnBnClickedBtPrstExecute)
	ON_BN_CLICKED(IDC_BT_PRST_PAUSE, &SubDlg_CIMInterface::OnBnClickedBtPrstPause)
	ON_BN_CLICKED(IDC_BT_PRST_DISABLE, &SubDlg_CIMInterface::OnBnClickedBtPrstDisable)
	ON_BN_CLICKED(IDC_BT_PRST_RESUME, &SubDlg_CIMInterface::OnBnClickedBtPrstResume)
	ON_BN_CLICKED(IDC_BT_MCMD_REQUEST, &SubDlg_CIMInterface::OnBnClickedBtMcmdRequest)
	ON_BN_CLICKED(IDC_BT_ALARM_REPORT_SET, &SubDlg_CIMInterface::OnBnClickedBtAlarmReportSet)
	ON_BN_CLICKED(IDC_BT_ALARM_REPORT2, &SubDlg_CIMInterface::OnBnClickedBtAlarmReport2)
	ON_CBN_SELCHANGE(IDC_CB_ALARM_INDEX, &SubDlg_CIMInterface::OnCbnSelchangeCbAlarmIndex)
	ON_BN_CLICKED(IDC_BT_ECID_LOAD, &SubDlg_CIMInterface::OnBnClickedBtEcidLoad)
	ON_BN_CLICKED(IDC_BT_ECID_SAVE, &SubDlg_CIMInterface::OnBnClickedBtEcidSave)


	ON_MESSAGE(WM_USER_ECID_CHANGE, &SubDlg_CIMInterface::ECID_Update)
	ON_BN_CLICKED(IDC_BT_MCC_CREATE_TEST_DATA, &SubDlg_CIMInterface::OnBnClickedBtMccCreateTestData)
	ON_BN_CLICKED(IDC_BT_MCC_UPLOAD_REPORT, &SubDlg_CIMInterface::OnBnClickedBtMccUploadReport)
END_MESSAGE_MAP()


// SubDlg_CIMInterface 메시지 처리기입니다.
BOOL SubDlg_CIMInterface::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void SubDlg_CIMInterface::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;

	if(G_pCIMInterface != nullptr)
	{
		GetDlgItem(IDC_ST_CIM_IP)->SetWindowText(G_pCIMInterface->m_chServerIP);
		GetDlgItem(IDC_ST_CIM_PORT)->SetWindowText(G_pCIMInterface->m_chServerPortNum);
	}
	CString strNewAddData;
	
	for(int AStep= 0; AStep<Alarm_List_Cnt; AStep++)
	{
		strNewAddData.Format("%d", G_strAlarmList[AStep].nAlarmID);
		m_cbAlarmList.AddString(strNewAddData);
	}

	int CellHeight = 24;
	//////////////////////////////////////////////////////////////////////////
	m_gridECIDList.SetDefCellWidth(50);
	m_gridECIDList.SetDefCellHeight(CellHeight);
	//m_gridECIDList.AutoFill();

	m_gridECIDList.SetRowCount(1+ECID_List_Cnt);
	m_gridECIDList.SetColumnCount(7);
	m_gridECIDList.SetFixedRowCount(1);		
	m_gridECIDList.SetFixedColumnCount(2);
	m_gridECIDList.SetListMode(FALSE);
	//m_gridECIDList.SetEditable(FALSE);
	m_gridECIDList.SetColumnResize(FALSE);
	m_gridECIDList.SetRowResize(FALSE);
	m_gridECIDList.SetFixedTextColor(RGB(255,200,100));
	m_gridECIDList.SetFixedBkColor(RGB(0,0,0));

	m_gridECIDList.SetFont(&G_GridFont);
	m_gridECIDList.SetItemText(0, 0, "ID");			m_gridECIDList.SetColumnWidth(0, 45);
	m_gridECIDList.SetItemText(0, 1, "Name");		m_gridECIDList.SetColumnWidth(1, 145);
	m_gridECIDList.SetItemText(0, 2, "SLL");		m_gridECIDList.SetColumnWidth(2, 57);
	m_gridECIDList.SetItemText(0, 3, "WLL");		m_gridECIDList.SetColumnWidth(3, 57);
	m_gridECIDList.SetItemText(0, 4, "DEF");		m_gridECIDList.SetColumnWidth(4, 57);
	m_gridECIDList.SetItemText(0, 5, "WUL");		m_gridECIDList.SetColumnWidth(5, 57);
	m_gridECIDList.SetItemText(0, 6, "SUL");		m_gridECIDList.SetColumnWidth(6, 57);

	for(int VStep = 1; VStep<=ECID_List_Cnt; VStep++)
	{
		m_gridECIDList.SetItemFormat(VStep, 0, DT_CENTER);
		//m_gridECIDList.SetItemFormat(VStep, 1, DT_CENTER);
		m_gridECIDList.SetItemFormat(VStep, 2, DT_CENTER);
		m_gridECIDList.SetItemFormat(VStep, 3, DT_CENTER);
		m_gridECIDList.SetItemFormat(VStep, 4, DT_CENTER);
		m_gridECIDList.SetItemFormat(VStep, 5, DT_CENTER);
		m_gridECIDList.SetItemFormat(VStep, 6, DT_CENTER);
	}
	//ECID Data Load
	ECIDList_Load();
	SetTimer(TIMER_CIM_DLG_UPDATE, 1000, 0);
	
}
void SubDlg_CIMInterface::ECIDList_Load()
{
	char strReadData[256];
	CString strECID_Section;

	for(int VStep = 1; VStep<=ECID_List_Cnt; VStep++)
	{
		strECID_Section.Format("%d", VStep);
		m_gridECIDList.SetItemText(VStep, 0, strECID_Section);
		strECID_Section.Format("%s%d", Section_ECID_, VStep);
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(strECID_Section, Key_ECName, "None", &strReadData[0], 256, SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 1, strReadData);
		G_strECIDList[VStep-1].nECID = VStep;
		sprintf_s(G_strECIDList[VStep-1].strECName, "%s", strReadData);

		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(strECID_Section, Key_ECSLL, "0", &strReadData[0], 256, SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 2, strReadData);
		G_strECIDList[VStep-1].nECSLL = atoi(strReadData);

		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(strECID_Section, Key_ECWLL, "0", &strReadData[0], 256, SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 3, strReadData);
		G_strECIDList[VStep-1].nECWLL = atoi(strReadData);

		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(strECID_Section, Key_ECDEF, "0", &strReadData[0], 256, SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 4, strReadData);
		G_strECIDList[VStep-1].nECDEF = atoi(strReadData);

		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(strECID_Section, Key_ECWUL, "0", &strReadData[0], 256, SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 5, strReadData);
		G_strECIDList[VStep-1].nECWUL = atoi(strReadData);

		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(strECID_Section, Key_ECSUL, "0", &strReadData[0], 256, SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 6, strReadData);
		G_strECIDList[VStep-1].nECSUL = atoi(strReadData);
	}
	m_gridECIDList.Refresh();
}
void SubDlg_CIMInterface::ECIDList_Write()
{

	CString strNewData;
	CString strECID_Section;

	for(int VStep = 1; VStep<=ECID_List_Cnt; VStep++)
	{
		strECID_Section.Format("%s%d", Section_ECID_, VStep);

		strNewData.Format("%s", m_gridECIDList.GetItemText(VStep, 1));
		WritePrivateProfileString(strECID_Section, Key_ECName, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		sprintf_s(G_strECIDList[VStep-1].strECName, "%s", strNewData);
		//G_strECIDList[VStep-1].strECName
		strNewData.Format("%s", m_gridECIDList.GetItemText(VStep, 2));
		WritePrivateProfileString(strECID_Section, Key_ECSLL, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		G_strECIDList[VStep-1].nECSLL = atoi(strNewData);

		strNewData.Format("%s", m_gridECIDList.GetItemText(VStep, 3));
		WritePrivateProfileString(strECID_Section, Key_ECWLL, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		G_strECIDList[VStep-1].nECWLL = atoi(strNewData);

		strNewData.Format("%s", m_gridECIDList.GetItemText(VStep, 4));
		WritePrivateProfileString(strECID_Section, Key_ECDEF, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		G_strECIDList[VStep-1].nECDEF = atoi(strNewData);

		strNewData.Format("%s", m_gridECIDList.GetItemText(VStep, 5));
		WritePrivateProfileString(strECID_Section, Key_ECWUL, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		G_strECIDList[VStep-1].nECWUL = atoi(strNewData);

		strNewData.Format("%s", m_gridECIDList.GetItemText(VStep, 6));
		WritePrivateProfileString(strECID_Section, Key_ECSUL, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		G_strECIDList[VStep-1].nECSUL = atoi(strNewData);
	}


}


void SubDlg_CIMInterface::ECIDList_ValueUpDate()
{
	CString strNewData;
	CString strECID_Section;

	for(int VStep = 1; VStep<=ECID_List_Cnt; VStep++)
	{
		strECID_Section.Format("%s%d", Section_ECID_, VStep);

		strNewData.Format("%s", G_strECIDList[VStep-1].strECName);
		WritePrivateProfileString(strECID_Section, Key_ECName, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 1, strNewData);

		//G_strECIDList[VStep-1].strECName
		strNewData.Format("%d", G_strECIDList[VStep-1].nECSLL);
		WritePrivateProfileString(strECID_Section, Key_ECSLL, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 2, strNewData);

		strNewData.Format("%d", G_strECIDList[VStep-1].nECWLL);
		WritePrivateProfileString(strECID_Section, Key_ECWLL, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 3, strNewData);

		strNewData.Format("%d", G_strECIDList[VStep-1].nECDEF);
		WritePrivateProfileString(strECID_Section, Key_ECDEF, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 4, strNewData);

		strNewData.Format("%d", G_strECIDList[VStep-1].nECWUL);
		WritePrivateProfileString(strECID_Section, Key_ECWUL, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 5, strNewData);

		strNewData.Format("%d", G_strECIDList[VStep-1].nECSUL);
		WritePrivateProfileString(strECID_Section, Key_ECSUL, strNewData.GetBuffer(strNewData.GetLength()), SetECIDList_PATH);
		m_gridECIDList.SetItemText(VStep, 6, strNewData);
	}
	m_gridECIDList.Refresh();

}
LRESULT		SubDlg_CIMInterface::ECID_Update(WPARAM wParam, LPARAM lParam)
{
	ECIDList_ValueUpDate();
	return 0;
}
HBRUSH SubDlg_CIMInterface::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL SubDlg_CIMInterface::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

void SubDlg_CIMInterface::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}
void SubDlg_CIMInterface::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(TIMER_CIM_DLG_UPDATE == nIDEvent)
	{
		if(G_pCIMInterface != nullptr)
		{
			if(G_pCIMInterface->m_bConnect == TRUE)
			{
				GetDlgItem(IDC_ST_SERVER_CONNECT)->SetWindowText("Connect OK");
			}
			else
			{
				GetDlgItem(IDC_ST_SERVER_CONNECT)->SetWindowText("Connect NG");
			}
		}

		

		//if(m_pEQST_Mode != nullptr)
		{
			if(m_EQST_Mode_Old != m_EQST_Mode)
			{
				switch(m_EQST_Mode)
				{
				case EQP_NORMAL:
					GetDlgItem(IDC_ST_EQST)->SetWindowText("EQP_NORMAL");
					m_PRST_Process_Old =PRST_NONE;
					break;
				case EQP_FAULT:
					GetDlgItem(IDC_ST_EQST)->SetWindowText("EQP_FAULT");
					G_MotObj.m_fnSetSignalTower(STWOER_MODE5_FAULT);
					// Alarm 이 있을 경우 모든 UI Diable
					m_PRST_Process_Old =PRST_NONE;
					G_VS10TASK_STATE = TASK_STATE_ERROR;
					break;
				case EQP_PM:
					G_MotObj.m_fnSetSignalTower(STWOER_MODE7_PM);
					GetDlgItem(IDC_ST_EQST)->SetWindowText("EQP_PM");
					m_PRST_Process_Old =PRST_NONE;
					G_VS10TASK_STATE = TASK_STATE_PM;
					break;
				default:
					GetDlgItem(IDC_ST_EQST)->SetWindowText("None");
					m_PRST_Process_Old =PRST_NONE;
					break;
				}
				
				G_pCIMInterface->m_fnSendEqstChangeReport(m_EQST_Mode, BY_OP);
				m_EQST_Mode_Old = m_EQST_Mode;
			}
		}

		//if(m_pPRST_Process != nullptr)
		{
			if(m_PRST_Process_Old != m_PRST_Process)
			{
				switch(m_PRST_Process)
				{
				case PRST_IDLE:
					if(m_EQST_Mode == EQP_NORMAL)
						G_VS10TASK_STATE = TASK_STATE_IDLE;
					if(G_MotInfo.m_pMotState->stGripperStatus.In10_Sn_LeftTensionStickDetection == 1
						|| G_MotInfo.m_pMotState->stGripperStatus.In22_Sn_RightTensionStickDetection == 1)
					{					
						G_MotObj.m_fnSetSignalTower(STWOER_MODE3_IDLE2);
					}
					else
					{
						G_MotObj.m_fnSetSignalTower(STWOER_MODE2_IDLE1);
					}
					GetDlgItem(IDC_ST_PRST)->SetWindowText("PRST_IDLE");
					break;
				case PRST_SETUP:
					GetDlgItem(IDC_ST_PRST)->SetWindowText("PRST_SETUP");
					if(m_EQST_Mode == EQP_NORMAL)
					{
						G_MotObj.m_fnSetSignalTower(STWOER_MODE3_IDLE2);
						G_VS10TASK_STATE = TASK_STATE_INIT;
					}
					break;
				case PRST_EXECUTE:
					G_MotObj.m_fnSetSignalTower(STWOER_MODE1_RUN);
					GetDlgItem(IDC_ST_PRST)->SetWindowText("PRST_EXECUTE");
					if(m_EQST_Mode == EQP_NORMAL)
					{
						G_VS10TASK_STATE = TASK_STATE_RUN;
					}
					break;
				case PRST_PAUSE:
					GetDlgItem(IDC_ST_PRST)->SetWindowText("PRST_PAUSE");
					if(m_EQST_Mode == EQP_NORMAL)
					{
						G_MotObj.m_fnSetSignalTower(STWOER_MODE3_IDLE2);
						G_VS10TASK_STATE = TASK_STATE_PM;
					}
					break;
				//case PRST_DISABLE:
				//	GetDlgItem(IDC_ST_PRST)->SetWindowText("PRST_DISABLE");
				//	break;
				//case PRST_RESUME:
				//	GetDlgItem(IDC_ST_PRST)->SetWindowText("PRST_RESUME");
				//	break;
				default:
					GetDlgItem(IDC_ST_PRST)->SetWindowText("None");
				}
				G_pCIMInterface->m_fnSendPrstChangeReport(m_PRST_Process, BY_EQUIP);//BY_OP);
				m_PRST_Process_Old = m_PRST_Process;
			}
			
		}
	}
	CDialogEx::OnTimer(nIDEvent);
}
void SubDlg_CIMInterface::WriteCIMMsg(ListColor nType, char* pWritLog )
{
	if(m_listCIMLogView.GetCount() > LIST_ALARM_COUNT)
		m_listCIMLogView.DeleteString(0);
	CXListBoxAddString(&m_listCIMLogView, nType, pWritLog);
	m_listCIMLogView.SetScrollPos(SB_VERT , m_listCIMLogView.GetCount(), TRUE);
	m_listCIMLogView.SetTopIndex(m_listCIMLogView.GetCount() - 1);


	if(this->IsWindowVisible() == TRUE)
		G_WriteInfo_FromCIM(nType, pWritLog);
}
//LRESULT SubDlg_CIMInterface::OnUpdateCIMMsg(WPARAM wParam, LPARAM lParam)
//{
//	ListColor nType = (ListColor)wParam;
//	char *pNewLog  = (char *)lParam;
//	if(m_listCIMLogView.GetCount() > LIST_ALARM_COUNT)
//		m_listCIMLogView.DeleteString(0);
//	CXListBoxAddString(&m_listCIMLogView, nType, pNewLog);
//	m_listCIMLogView.SetScrollPos(SB_VERT , m_listCIMLogView.GetCount(), TRUE);
//	m_listCIMLogView.SetTopIndex(m_listCIMLogView.GetCount() - 1);	
//
//	G_WriteInfo(nType, pNewLog);
//
//	return 0;
//}

void SubDlg_CIMInterface::OnBnClickedBtLoadComplete()
{
	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);

	GetDlgItem(IDC_ST_MATERIAL_INFO)->SetWindowText("MATERIAL_INFO : None");
	GetDlgItem(IDC_ST_JOBSTART_CMD_ACK)->SetWindowText("JOBSTART_CMD_ACK : None");
	GetDlgItem(IDC_ST_FILESERVER_UPLOAD_REPORT_ACK)->SetWindowText("UPLOAD_REPORT_ACK : None");
	//TEST
	G_pCIMInterface->m_fnSendLoadComplete(CString(strReadData));
}


void SubDlg_CIMInterface::OnBnClickedBtInspectionStart()
{
	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	G_pCIMInterface->m_fnSendInspectionStart(CString(strReadData));
	m_EQST_Mode = EQP_NORMAL;
	m_PRST_Process = PRST_EXECUTE;
	::SendMessage(this->GetParent()->GetSafeHwnd(), WM_USER_WRITE_INSPECT_RET, Write_SetStartTime, 0);
}


void SubDlg_CIMInterface::OnBnClickedBtInspectionEnd()
{


	::SendMessage(this->GetParent()->GetSafeHwnd(), WM_USER_WRITE_INSPECT_RET, Write_SetEndTime, 0);

	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);

	//모든 Result Data를 File로 기록 한다.
	CString NewPath;
	NewPath.Format("%s%s\\", MasterToCIM_Result_PATH_Ret, strReadData);
	G_fnCheckDirAndCreate(NewPath.GetBuffer(NewPath.GetLength()));//폴더 생성 확인.
	//CTime tCurrentTime;
	//tCurrentTime = CTime::GetCurrentTime();
	//CString strCurrentTime;
	//strCurrentTime = tCurrentTime.Format("%Y%m%d%H%M%S");
	//CString strDataFile;
	//strDataFile.Format("%s%s_%s.dat",NewPath, strReadData, strCurrentTime);
	//WriteMeasureAllResult();
	::SendMessage(this->GetParent()->GetSafeHwnd(), WM_USER_WRITE_INSPECT_RET, Write_ResultAll, 0);

	char strReadResult[256];
	memset(strReadResult, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowProcessResult, "OK", &strReadResult[0], 256, VS10Master_PARAM_INI_PATH);

	G_pCIMInterface->m_fnSendInspectionComp(CString(strReadData), CString(strReadResult));
	m_EQST_Mode = EQP_NORMAL;
	m_PRST_Process = PRST_IDLE;
	//////////////////////////////////////////////////////////////////////////
}


void SubDlg_CIMInterface::OnBnClickedBtUnloadComplete()
{
	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	G_pCIMInterface->m_fnSendUnloadComp(CString(strReadData));

	m_EQST_Mode = EQP_NORMAL;
	m_PRST_Process = PRST_IDLE;

	GetDlgItem(IDC_ST_MATERIAL_INFO)->SetWindowText("MATERIAL_INFO : None");
	GetDlgItem(IDC_ST_JOBSTART_CMD_ACK)->SetWindowText("JOBSTART_CMD_ACK : None");
	GetDlgItem(IDC_ST_FILESERVER_UPLOAD_REPORT_ACK)->SetWindowText("UPLOAD_REPORT_ACK : None");
}


void SubDlg_CIMInterface::OnBnClickedBtEqNormal()
{	
	//if(m_pEQST_Mode != nullptr)
	{
		int AlarmListCnt = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH);
		if(m_EQST_Mode == EQP_FAULT && AlarmListCnt > 0)
		{
			AfxMessageBox("장비 Alarm 상태 ,설비 상태 변경 금지");
			return;
		}
		m_EQST_Mode = EQP_NORMAL;
		//G_pCIMInterface->m_fnSendEqstChangeReport(*m_pEQST_Mode, BY_OP, 0);
	}
}


void SubDlg_CIMInterface::OnBnClickedBtEqpFault()
{
	//if(m_pEQST_Mode != nullptr)
	{
		m_EQST_Mode = EQP_FAULT;
		m_PRST_Process = PRST_SETUP;
		//G_pCIMInterface->m_fnSendEqstChangeReport(*m_pEQST_Mode, BY_OP, 0);
	}
}


void SubDlg_CIMInterface::OnBnClickedBtEqpPm()
{
	//if(m_pEQST_Mode != nullptr)
	{
		int AlarmListCnt = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH);
		if(m_EQST_Mode == EQP_FAULT && AlarmListCnt > 0)
		{
			AfxMessageBox("장비 Alarm 상태 ,설비 상태 변경 금지");
			return;
		}

		m_EQST_Mode = EQP_PM;
		m_PRST_Process = PRST_SETUP;
		//G_pCIMInterface->m_fnSendEqstChangeReport(*m_pEQST_Mode, BY_OP, 0);
	}
}


void SubDlg_CIMInterface::OnBnClickedBtPrstIdle()
{
	//if(m_pPRST_Process != nullptr)
	{
		int AlarmListCnt = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH);
		if(m_EQST_Mode == EQP_FAULT && AlarmListCnt > 0)
		{
			AfxMessageBox("장비 Alarm 상태 ,설비 상태 변경 금지");
			return;
		}
		m_PRST_Process = PRST_IDLE;
		//G_pCIMInterface->m_fnSendPrstChangeReport(*m_pPRST_Process, BY_OP);
	}
}


void SubDlg_CIMInterface::OnBnClickedBtPrstSetup()
{
	//if(m_pPRST_Process != nullptr)
	{
		int AlarmListCnt = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH);
		if(m_EQST_Mode == EQP_FAULT && AlarmListCnt> 0)
		{
			AfxMessageBox("장비 Alarm 상태 ,설비 상태 변경 금지");
			return;
		}

		m_PRST_Process = PRST_SETUP;
		//G_pCIMInterface->m_fnSendPrstChangeReport(*m_pPRST_Process, BY_OP);
	}
}


void SubDlg_CIMInterface::OnBnClickedBtPrstExecute()
{
	//if(m_pPRST_Process != nullptr)
	{
		int AlarmListCnt = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH);
		if(m_EQST_Mode == EQP_FAULT && AlarmListCnt > 0)
		{
			AfxMessageBox("장비 Alarm 상태 ,설비 상태 변경 금지");
			return;
		}
		m_PRST_Process = PRST_EXECUTE;
		//G_pCIMInterface->m_fnSendPrstChangeReport(*m_pPRST_Process, BY_OP);
	}
}


void SubDlg_CIMInterface::OnBnClickedBtPrstPause()
{
	//if(m_pPRST_Process != nullptr)
	{
		m_PRST_Process_Resume = m_PRST_Process;
		m_PRST_Process = PRST_PAUSE;
		
		//G_pCIMInterface->m_fnSendPrstChangeReport(*m_pPRST_Process, BY_OP);
	}
}


void SubDlg_CIMInterface::OnBnClickedBtPrstDisable()
{
	G_pCIMInterface->m_fnSendStickExistReport(TRUE);
}


void SubDlg_CIMInterface::OnBnClickedBtPrstResume()
{
	G_pCIMInterface->m_fnSendStickExistReport(FALSE);
}


void SubDlg_CIMInterface::OnBnClickedBtMcmdRequest()
{
	GetDlgItem(IDC_ST_MCMD_STATE)->SetWindowText("None");
	G_pCIMInterface->m_fnSendMCMDRequest();
}

void SubDlg_CIMInterface::SendSetCIMAlarm(int AlarmIndex)
{
	if(AlarmIndex>=0 && AlarmIndex<Alarm_List_Cnt)
	{ 
		CTime NowTime = CTime::GetCurrentTime();
		G_pCIMInterface->m_fnSendAlarmReport(TRUE, G_strAlarmList[AlarmIndex].nAlarmType, G_strAlarmList[AlarmIndex].nAlarmID, G_strAlarmList[AlarmIndex].strAlarmText, NowTime);

		if(G_strAlarmList[AlarmIndex].nAlarmType == ALARM_HEAVY && (m_EQST_Mode != EQP_FAULT && m_PRST_Process != PRST_PAUSE))
		{
			// 상태 변경 보고 Heavy Alarm 시 조건.
			m_EQST_Mode_Resume=m_EQST_Mode;
			m_PRST_Process_Resume = m_PRST_Process;
			m_EQST_Mode = EQP_FAULT;
			m_PRST_Process = PRST_PAUSE;
			m_bAlarmHeavyChange = true;			
		}
		else  // Alarm 경알람일때
		{
			if(m_bAlarmHeavyChange == false) // Havey Alarm 이 아닐때만 
				G_MotObj.m_fnSetSignalTower(STWOER_MODE4_ALARM);
		}
	}
}
void SubDlg_CIMInterface::SendResetAllCIMAlarm()
{
	int AlarmListCnt = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH);
	if(AlarmListCnt>0)
	{
		for(int IdStep= 1; IdStep<=AlarmListCnt; IdStep++)
		{
			CString strKeyValue;
			strKeyValue.Format("%s%d", Key_AlarmIndex_, IdStep);
			int AlarmCodeIndex = GetPrivateProfileInt(Section_Alarm, strKeyValue.GetBuffer(strKeyValue.GetLength()), 0, SetAlarmList_PATH);

			G_strAlarmList[AlarmCodeIndex].bSetAlarm = false;//Clear되었으면 해지 하자.
			CTime NowTime = CTime::GetCurrentTime();
			G_pCIMInterface->m_fnSendAlarmReport(FALSE, G_strAlarmList[AlarmCodeIndex].nAlarmType, G_strAlarmList[AlarmCodeIndex].nAlarmID, G_strAlarmList[AlarmCodeIndex].strAlarmText, NowTime);
		}
		//Count를 0화 한다.
		CString strTotalCnt;
		strTotalCnt.Format("%d", 0);
		WritePrivateProfileString(Section_Alarm, Key_AlarmTotalCnt, strTotalCnt,  SetAlarmList_PATH);
		if(m_bAlarmHeavyChange == true)
		{
			m_EQST_Mode = m_EQST_Mode_Resume;
			m_PRST_Process = m_PRST_Process_Resume;
			m_EQST_Mode_Resume = EQP_FAULT;
			m_PRST_Process_Resume = PRST_RESUME;
			m_bAlarmHeavyChange = false;
		}
	}
}

CString SubDlg_CIMInterface::SendAlarmList()
{
	CString strRetValue;
	strRetValue.Format("");
	int AlarmListCnt = GetPrivateProfileInt(Section_Alarm, Key_AlarmTotalCnt, 0,  SetAlarmList_PATH);
	if(AlarmListCnt>0)
	{
		CString strKeyValue;
		int AlarmCodeIndex = 0;
		for(int IdStep= 1; IdStep<=AlarmListCnt-1; IdStep++)
		{
				
			strKeyValue.Format("%s%d", Key_AlarmIndex_, IdStep);
			AlarmCodeIndex = GetPrivateProfileInt(Section_Alarm, strKeyValue.GetBuffer(strKeyValue.GetLength()), 0, SetAlarmList_PATH);
			strRetValue.AppendFormat("%d,",  G_strAlarmList[AlarmCodeIndex].nAlarmID);
		}
		strKeyValue.Format("%s%d", Key_AlarmIndex_, (AlarmListCnt-1));
		AlarmCodeIndex = GetPrivateProfileInt(Section_Alarm, strKeyValue.GetBuffer(strKeyValue.GetLength()), 0, SetAlarmList_PATH);
		strRetValue.AppendFormat("%d",  G_strAlarmList[AlarmCodeIndex].nAlarmID);
	}
	return strRetValue;
}
void SubDlg_CIMInterface::OnBnClickedBtAlarmReportSet()
{
	int SelectItem = m_cbAlarmList.GetCurSel();
	if(SelectItem>=0 && SelectItem<Alarm_List_Cnt)
	{ 
		G_AlarmAdd(G_strAlarmList[SelectItem].nAlarmID);
	}
}
void SubDlg_CIMInterface::OnBnClickedBtAlarmReport2()
{

}


void SubDlg_CIMInterface::OnCbnSelchangeCbAlarmIndex()
{
	int SelectItem = m_cbAlarmList.GetCurSel();
	if(SelectItem>=0 && SelectItem<Alarm_List_Cnt)
	{
		GetDlgItem(IDC_ST_ALARM_TYPE)->SetWindowText(G_strAlarmList[SelectItem].nAlarmType==ALARM_HEAVY?"HEAVY":"LIGHT");
		GetDlgItem(IDC_ST_ALARM_TEXT)->SetWindowText(G_strAlarmList[SelectItem].strAlarmText);
	}
}



void SubDlg_CIMInterface::OnBnClickedBtEcidLoad()
{
	ECIDList_Load();
}


void SubDlg_CIMInterface::OnBnClickedBtEcidSave()
{
	ECIDList_Write();
	G_pCIMInterface->m_fnSendECID_ChangeReport();
}


void SubDlg_CIMInterface::OnBnClickedBtMccCreateTestData()
{
	// 1. 파일 정보를 Ini에 Write 한다.  한번 정보를 받으면 가변적이지 않는 Data을 적는다.




	//변경사항이 계속 유지되는 것들.
	// 	m_MccCommand.m_fnMCC_WriteInfo(N_MODULE_ID, "TEST1");   // Module ID는 가변적이다.
	// 	m_MccCommand.m_fnMCC_WriteInfo(N_LOGTYPE, "TEST2");     // Log Type은 함수안에...

	G_MccCommand.m_fnMCC_WriteInfo(N_STEPID, "001");//m_CIMInterface.m_fnGetMaterialInfo(STEPID));		 // Step ID는 고정


	char strReadData[256];
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunBoxID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	G_MccCommand.m_fnMCC_WriteInfo(N_BOXID, strReadData);		 // BOX ID도 고정


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_Process_Data, Key_NowRunStickID, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
	G_MccCommand.m_fnMCC_WriteInfo(N_STICKID, strReadData);

	G_MccCommand.m_fnMCC_WriteInfo(N_HOSTPPID, HOSTID_NAME);	 // Host ID도 고정
	G_MccCommand.m_fnMCC_WriteInfo(N_EQPPPID, EQP_NAME_A3MSI01N);	     // Eqp ID도 고정

	// 2. Write End 했음을 알린다.
	G_MccCommand.m_fnMCC_WriteInfoEnd();

	//여기까지가 스틱이 변경될때 입력.
	//





	// 3. Event를Log를 남긴다.	
	//MCC 정의 SEQ Start/End 한상으로 기록.
	G_MccCommand.m_fnMCC_ActionLog(INIT_LIFT_ORIGIN, TRUE, "StickID");
	Sleep(100);
	G_MccCommand.m_fnMCC_ActionLog(INIT_LIFT_ORIGIN, FALSE, "StickID");

	G_MccCommand.m_fnMCC_ActionLog(LOAD_POS_MOVE01, TRUE, "StickID");
	Sleep(100);
	G_MccCommand.m_fnMCC_ActionLog(LOAD_POS_MOVE01, FALSE, "StickID");
	Sleep(100);


	//I/O정보추가.(On일때)
	G_MccCommand.m_fnMCC_SignalLog(STEP1_BOX_DETECT_SENSOR1_1, TRUE, "StickID");
	Sleep(100);	
	//.(Off일때)
	G_MccCommand.m_fnMCC_SignalLog(STEP1_BOX_DETECT_SENSOR1_1, FALSE, "StickID");
	Sleep(100);

	//스틱 인
	G_MccCommand.m_fnMCC_EventLog_CompInOut(TRUE, MODULE_BC01_BT01, "StickID", LAYER_BT01, LAYER_BT01);
	Sleep(100); 	
	//스틱 아웃
	G_MccCommand.m_fnMCC_EventLog_CompInOut(FALSE, MODULE_BC01_BT01, "StickID", LAYER_BT01, LAYER_TM01);
	Sleep(100);


	//설비 상태가 바뀔때
	G_MccCommand.m_fnMCC_EventLog_ChangeProcessState(MODULE_BC01_BT01, "StickID", "IDLE", "EXECUTING");
	Sleep(100);

	//알람발생시에 기록.
	G_MccCommand.m_fnMCC_EventLog_ChangeState(MODULE_BC01_BT01, "StickID", "NORMAL", "FAULT", "1000", "DoorOpen");

	//레시피가 변경될때.
	G_MccCommand.m_fnMCC_EventLog_ChangePPID(MODULE_BC01_BT01, "StickID", "AK_R_01", "GK_M_01");

	// InfoValue
	G_MccCommand.m_fnMCC_InfoInputValue(SET_SERVO_UPDOWN_SPD, 111);

	G_MccCommand.m_fnMCC_InfoInputValue(SERVO_UPDOWN_CUR_SPD, 222);

	G_MccCommand.m_fnMCC_InfoInputValue(SET_SERVO_UPDOWN_POS, 333);

	G_MccCommand.m_fnMCC_InfoInputValue(SERVO_UPDOWN_CUR_POS, 444);

	G_MccCommand.m_fnMCC_InfoInputValue(SERVO_UPDOWN_TOQ, 555);	
}


void SubDlg_CIMInterface::OnBnClickedBtMccUploadReport()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
