
// VS11IndexerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "VS11IndexerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//Observation Application Link State.
typedef enum
{
	ObserveSW_Init =-1,				//<<< 초기 상태
	ObserveSW_None =0,				//<<< Observation APP이 구동되지 않았다.
	ObserveSW_OK ,					//<<< Observation APP이 정상 구동중이다.
	ObserveSW_CtrlPowerOff,			//<<< Observation APP는 구동중이나 Controller가 Off 상태 이다.
	ObserveSW_NoneRemote,			//<<< Observation APP와 Controller는 구동중 이지만 Remote Mode가 아니다.
	////////
	ObserveSW_ManualMode			//<<< Main UI에서 Manual Mode로 변환 한것이다.
}ObsAppState;

//Observation App 동작상태 (ObserveSW_OK APP상태에서만 Check.)
typedef enum
{
	ActionObs_Init =-2,						//<<< 초기 상태
	ActionObs_Manual =-1,					//<<< ObserveSW_ManualMode일때는 상태 Check를 하지 않는다.
	ActionObs_None  =0,						//<<< 동작 명령 수행중이 아닐때.

	//ActionObs_AutoGain,						//<<< Auto Gain 동작 수행시.
	ActionObs_AutoFocus,					//<<< Auto Focus 동작 수행시.
	//ActionObs_AutoUpDownPosSetting,			//<<< 측정 Upper/Lower 위치 선정 수행시.
	ActionObs_AutoUpDownPosSetByImg,		//<<< 측정 Upper/Lower 위치 선정 수행시.
	ActionObs_StartMeasurement,				//<<< 3D Scan 수행시.
	ActionObs_AutoSaveData,					//<<< 측정 Data 저장 수행시.

	//ActionObs_AutoGain_Error,				//<<< Auto Gain 동작 수행시 Error.
	ActionObs_AutoFocus_Error,				//<<< Auto Focus 동작 수행시 Error.
	//ActionObs_AutoUpDownPosSet_Error,		//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
	ActionObs_AutoUpDownPosSetByImg_Error,	//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
	ActionObs_StartMeasurement_Error,		//<<< 3D Scan 수행시 Error.
	ActionObs_AutoSaveData_Error,			//<<< 측정 Data 저장 수행시 Error.

	ActionObs_RemoteMode_Error//<<< 측정 가능 Remote Mode가 아니다.
}ObsActionState;



CVS11IndexerDlg*			g_cpMainDlg;
CInterServerInterface*		g_cpServerInterface;

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();
// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CVS11IndexerDlg 대화 상자
CVS11IndexerDlg::CVS11IndexerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVS11IndexerDlg::IDD, pParent)
	, m_nMasterStatusCount(0)
	, m_nMotionStatusCount(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_iplBackGroundOrg = nullptr;
	m_iplBackGround = nullptr;
	
}
CVS11IndexerDlg::~CVS11IndexerDlg()
{
	if(m_iplBackGround != nullptr)
	{
		cvReleaseImage(&m_iplBackGroundOrg);
		cvReleaseImage(&m_iplBackGround);
	}
	m_iplBackGroundOrg = nullptr;
	m_iplBackGround = nullptr;

	G_VS10TASK_STATE = TASK_STATE_NONE;
	G_VS11TASK_STATE = TASK_STATE_NONE;
	G_VS24TASK_STATE = TASK_STATE_NONE;
	m_elMainSequence = MAIN_SEQUENCE_IDLE;	
}
void CVS11IndexerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG_VIEW, m_ctrListLogView);
	DDX_Control(pDX, IDOK, m_ctrBtnExit);
	DDX_Control(pDX, IDC_ST_TASK_STATE, m_ctrStaticTaskInfo);
	DDX_Control(pDX, IDC_BTN_LOT_START, m_ctrBtnLotStart);

	DDX_Control(pDX, IDC_BT_MEASURE_INIT, m_btMeasure3DInit);
	DDX_Control(pDX, IDC_BT_MEASURE_START, m_btStartMeasure3D);
	DDX_Control(pDX, IDC_BT_3D_LENS_1, m_bt3DLensSelect[0]);
	DDX_Control(pDX, IDC_BT_3D_LENS_2, m_bt3DLensSelect[1]);
	DDX_Control(pDX, IDC_BT_3D_LENS_3, m_bt3DLensSelect[2]);
	DDX_Control(pDX, IDC_BT_3D_LENS_4, m_bt3DLensSelect[3]);
	DDX_Control(pDX, IDC_ST_TASK_STATE_3D, m_ctrStaticTask3DMeasure);
	DDX_Control(pDX, IDC_ST_PROC_STATE_3D, m_ctrStaticProc3DMeasure);
	DDX_Control(pDX, IDC_ST_MEASURE_INDEX, m_stMeasureCnt3D);
	DDX_Control(pDX, IDC_BTN_EP_INIT, m_ctrBtnEpInit);
	DDX_Control(pDX, IDC_BTN_EP_RESET, m_ctrBtnEpReset);
	DDX_Control(pDX, IDC_BTN_EP_PROC_START, m_ctrBtnEpProcStart);
	DDX_Control(pDX, IDC_BTN_EP_SCAN_START, m_ctrBtnEpScanStart);
	DDX_Control(pDX, IDC_BTN_EP_SCAN_END, m_ctrBtnEpScanEnd);
	DDX_Control(pDX, IDC_BTN_EP_PROC_END, m_ctrBtnEpProcEnd);
}

BEGIN_MESSAGE_MAP(CVS11IndexerDlg, CDialogEx)
	ON_MESSAGE(WM_VS64USER_CALLBACK, &CVS11IndexerDlg::Sys_fnMessageCallback)//VS64 Interface
	ON_MESSAGE(WM_USER_ACC_CODE_UPDATE, &CVS11IndexerDlg::Sys_fnBarcodeCallback)//VS64 Interface
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CTLCOLOR()
	ON_WM_NCDESTROY()
	ON_BN_CLICKED(IDOK, &CVS11IndexerDlg::OnBnClickedOk)	
	ON_WM_TIMER()
	
	ON_BN_CLICKED(IDC_BTN_LOT_START, &CVS11IndexerDlg::OnBnClickedBtnLotStart)
	
	ON_BN_CLICKED(IDC_BT_MEASURE_START, &CVS11IndexerDlg::OnBnClickedBtMeasureStart)
	ON_BN_CLICKED(IDC_BT_3D_LENS_1, &CVS11IndexerDlg::OnBnClickedBt3dLens)
	ON_BN_CLICKED(IDC_BT_3D_LENS_2, &CVS11IndexerDlg::OnBnClickedBt3dLens)
	ON_BN_CLICKED(IDC_BT_3D_LENS_3, &CVS11IndexerDlg::OnBnClickedBt3dLens)
	ON_BN_CLICKED(IDC_BT_3D_LENS_4, &CVS11IndexerDlg::OnBnClickedBt3dLens)
	ON_BN_CLICKED(IDC_BT_MEASURE_INIT, &CVS11IndexerDlg::OnBnClickedBtMeasureInit)
	ON_BN_CLICKED(IDC_BTN_IMAGE_SAVE, &CVS11IndexerDlg::OnBnClickedBtnImageSave)
	ON_BN_CLICKED(IDC_BTN_LOT_START, &CVS11IndexerDlg::OnBnClickedBtnLotStart)
END_MESSAGE_MAP()


// CVS11IndexerDlg 메시지 처리기

BOOL CVS11IndexerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.
	
	//
	m_btBackColor = RGB(65,65,65);
		
	//////////////////////////////////////////////////////////////////////////
	//1) Server Interface Connection.
	
	Sys_fnInitVS64Interface();

	CString NewWindowsName;
	NewWindowsName.Format("VS64 - Indexer Task(No.%02d)", m_ProcInitVal.m_nTaskNum);
	SetWindowText(NewWindowsName);
	
	m_fnCreateButton();

	m_fnReadSystemFile();
		// TODO: 여기에 추가 초기화 작업을 추가합니다.
		
	m_fnThreadCreator();
	
	G_VS11TASK_STATE = TASK_STATE_IDLE;
	in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
	//SetTimer(STATUS_DISP_TIMER, 3000, 0);
	

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}


int CVS11IndexerDlg::m_fnThreadCreator()
{
	g_cpMainDlg = this;
	g_cpServerInterface = &m_cpServerInterface;

	m_sThreadParam.cObjectPointer1 = this;
	m_sThreadParam.cObjectPointer2 = &m_cpServerInterface;
	m_sThreadParam.nScanInterval = THREAD_INTERVAL;
		

	return 0;

}





void CVS11IndexerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVS11IndexerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}




// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVS11IndexerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CVS11IndexerDlg::m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...)
{
	char	cLogBuffer[MAX_LOG_BUFF];
	va_list vargs;

	va_start(vargs, pErrLog);

	vsprintf_s(cLogBuffer, pErrLog, (va_list)vargs);

	va_end(vargs);

	m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nTaskNum ,uLogLevel, cLogBuffer);

	//strcpy_s(&cLogBuffer[strlen(cLogBuffer)], sizeof("\r\n"),"\r\n");

	//printf(cLogBuffer);
}

void CVS11IndexerDlg::Sys_fnInitVS64Interface()
{
	if(1 < __argc)
	{
		int nInputTaskNum = atoi(__argv[1]);
		char *pLinkPath = NULL;

		char path_buffer[_MAX_PATH];
		char chFilePath[_MAX_PATH] = {0,};
		char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		//실행 파일 이름을 포함한 Full path 가 얻어진다.
		::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
		//디렉토리만 구해낸다.
		_splitpath_s(path_buffer, drive, dir, fname, ext);
		strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
		strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

		switch(nInputTaskNum)
		{

		case TASK_10_Master		: pLinkPath = MSG_TASK_INI_FullPath_TN10; break;
		case TASK_11_Indexer		: pLinkPath = MSG_TASK_INI_FullPath_TN11; break;
		case TASK_21_Inspector	: pLinkPath = MSG_TASK_INI_FullPath_TN21; break;
		case TASK_24_Motion		: pLinkPath = MSG_TASK_INI_FullPath_TN24; break;
		case TASK_31_Mesure3D	: pLinkPath = MSG_TASK_INI_FullPath_TN31; break;
		case TASK_32_Mesure2D	: pLinkPath = MSG_TASK_INI_FullPath_TN32; break;
		case TASK_33_SerialPort	: pLinkPath = MSG_TASK_INI_FullPath_TN33; break;
		case TASK_60_Log			: pLinkPath = MSG_TASK_INI_FullPath_TN60; break;
		case TASK_90_TaskLoader: pLinkPath = MSG_TASK_INI_FullPath_TN90; break;

		case TASK_99_Sample: pLinkPath = MSG_TASK_INI_FullPath_TN99; break;
		default:
			{
				CString ErrorMessage;
				ErrorMessage.Format("Unkown TaskNum(%d)", nInputTaskNum);
				AfxMessageBox(ErrorMessage);
				SendMessage(WM_CLOSE, 0,  0);	
			}return;
		}

		strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

		m_fnReadDatFile(chFilePath);
		m_ProcInitVal.m_nTaskNum = nInputTaskNum;
		//m_ProcInitVal.m_nLogFileID = nInputTaskNum;

	}
	else
	{
		AfxMessageBox("Input Run Task Number");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	m_cpServerInterface.m_fnSetLogTaskInfo(m_ProcInitVal.m_nLogTaskNo, m_ProcInitVal.m_nLogBlockLovel);	
	if(m_cpServerInterface.m_fnRegisterTask(m_ProcInitVal.m_nTaskNum, m_ProcInitVal.m_sServerIP, m_ProcInitVal.m_nPortNo) != 0)
	{
		AfxMessageBox("Client Regist Fail");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	else
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, "Client Regist OK : %d", m_ProcInitVal.m_nTaskNum);
	}

	m_cpServerInterface.m_fnSetAppWindowHandle(m_hWnd);
	m_cpServerInterface.m_fnSetAppUserMessage(WM_VS64USER_CALLBACK);
}


/*
*	Module Name	:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function			:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS11IndexerDlg::Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	CMDMSG* CmdMsg = (CMDMSG*)lParam;		//SmartPointer 버전으로 개조가 필요하다... Work Item..

	try
	{
		if(CmdMsg != NULL)
		{
			nRet = Sys_fnAnalyzeMsg(CmdMsg);

			if(OKAY != nRet)
			{
				//Client 메시지 처리에대한 에러처리...
			}

			m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
			m_cpServerInterface.m_fnFreeMemory(CmdMsg);
			return OKAY;
		}
		else
		{
			throw CVs64Exception(_T("CVS19MWDlg::m_fnVS64MsgProc, Message did not become well."));
		}
	}
	catch (CVs64Exception& e)
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, e.m_fnGetErrorMsg());

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, exception -> %s"), e.what());
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, CException -> %s"), wszErr);
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	return OKAY;
}

/*
*	Module Name	:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function			:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS11IndexerDlg::Sys_fnBarcodeCallback(WPARAM wParam, LPARAM lParam)
{	
	try
	{

	}
	catch (CVs64Exception& e)
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS,  e.m_fnGetErrorMsg());		
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnBarcodeCallback, exception -> %s"), e.what());
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));		
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnBarcodeCallback, CException -> %s"), wszErr);
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));		
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnBarcodeCallback, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));		
	}
	return OKAY;
}

HBRUSH CVS11IndexerDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();	

	switch (DLG_ID_Number)
	{
	case IDC_ST_TASK_STATE:
	case IDC_ST_TASK_STATE_3D:
	case IDC_ST_PROC_STATE_3D:
		return hbr;		
	default:
		break;
	}

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CVS11IndexerDlg::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x20, 0x20, 0x20);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyle1.SetButtonStyle(&tStyle);
	
	m_tStyle2.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 6.0;
	tStyle.m_dHighLightY = 4.0;
	tStyle.m_dRadius = 4.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle2.SetButtonStyle(&tStyle);

	m_tStyle3.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 6.0;
	tStyle.m_dHighLightY = 4.0;
	tStyle.m_dRadius = 4.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x36, 0x55, 0x9E);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x16, 0x35, 0x7E);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyle3.SetButtonStyle(&tStyle);

	
	LOGFONT tFont;
	m_ctrBtnExit.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"Arial", strlen("Arial"));
	tFont.lfHeight = 60;

	tColorScheme tColor;
	m_ctrBtnExit.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	m_ctrBtnExit.SetTextColor(&tColor);
	m_ctrBtnExit.SetCheckButton(false);	
	m_ctrBtnExit.SetFont(&tFont);
	m_ctrBtnExit.SetRoundButtonStyle(&m_tStyle1);		
	
	m_ctrBtnLotStart.SetTextColor(&tColor);
	m_ctrBtnLotStart.SetCheckButton(false);	
	m_ctrBtnLotStart.SetFont(&tFont);
	m_ctrBtnLotStart.SetRoundButtonStyle(&m_tStyle1);
	
	m_ctrBtnExit.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	
	for(int nStep = 0; nStep < 4; nStep++)
	{
		m_bt3DLensSelect[nStep].SetTextColor(&tColor);
		m_bt3DLensSelect[nStep].SetCheckButton(false);		
		m_bt3DLensSelect[nStep].SetFont(&tFont);
		m_bt3DLensSelect[nStep].SetRoundButtonStyle(&m_tStyle2);
	}
	m_btStartMeasure3D.SetTextColor(&tColor);
	m_btStartMeasure3D.SetCheckButton(false);		
	m_btStartMeasure3D.SetFont(&tFont);
	m_btStartMeasure3D.SetRoundButtonStyle(&m_tStyle2);

	m_btMeasure3DInit.SetTextColor(&tColor);
	m_btMeasure3DInit.SetCheckButton(false);		
	m_btMeasure3DInit.SetFont(&tFont);
	m_btMeasure3DInit.SetRoundButtonStyle(&m_tStyle2);
	
	//////////////////////////////
	m_ctrBtnEpInit.SetTextColor(&tColor);
	m_ctrBtnEpInit.SetCheckButton(false);
	m_ctrBtnEpInit.SetFont(&tFont);
	m_ctrBtnEpInit.SetRoundButtonStyle(&m_tStyle2);

	m_ctrBtnEpReset.SetTextColor(&tColor);
	m_ctrBtnEpReset.SetCheckButton(false);
	m_ctrBtnEpReset.SetFont(&tFont);
	m_ctrBtnEpReset.SetRoundButtonStyle(&m_tStyle2);

	m_ctrBtnEpProcStart.SetTextColor(&tColor);
	m_ctrBtnEpProcStart.SetCheckButton(false);
	m_ctrBtnEpProcStart.SetFont(&tFont);
	m_ctrBtnEpProcStart.SetRoundButtonStyle(&m_tStyle2);

	m_ctrBtnEpScanStart.SetTextColor(&tColor);
	m_ctrBtnEpScanStart.SetCheckButton(false);
	m_ctrBtnEpScanStart.SetFont(&tFont);
	m_ctrBtnEpScanStart.SetRoundButtonStyle(&m_tStyle2);

	m_ctrBtnEpScanEnd.SetTextColor(&tColor);
	m_ctrBtnEpScanEnd.SetCheckButton(false);
	m_ctrBtnEpScanEnd.SetFont(&tFont);
	m_ctrBtnEpScanEnd.SetRoundButtonStyle(&m_tStyle2);

	m_ctrBtnEpProcEnd.SetTextColor(&tColor);
	m_ctrBtnEpProcEnd.SetCheckButton(false);
	m_ctrBtnEpProcEnd.SetFont(&tFont);
	m_ctrBtnEpProcEnd.SetRoundButtonStyle(&m_tStyle2);
	
	m_TaskStateFont.CreateFont(70, 0, 0, 0, FW_BOLD,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Arial Black");

	m_TaskStateSmallFont.CreateFont(30, 0, 0, 0, FW_BOLD,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Arial Black");
	m_TaskStateColor = RGB(128, 128, 128);

	m_ctrStaticTaskInfo.SetFont(&m_TaskStateFont);
	m_ctrStaticTaskInfo.SetTextColor(RGB(0, 0, 0));
	m_ctrStaticTaskInfo.SetBkColor(m_TaskStateColor);

	m_ctrStaticTask3DMeasure.SetFont(&m_TaskStateSmallFont);
	m_ctrStaticTask3DMeasure.SetTextColor(RGB(0, 0, 0));
	m_ctrStaticTask3DMeasure.SetBkColor(m_TaskStateColor);
	
	m_ctrStaticProc3DMeasure.SetFont(&m_TaskStateSmallFont);
	m_ctrStaticProc3DMeasure.SetTextColor(RGB(0, 0, 0));
	m_ctrStaticProc3DMeasure.SetBkColor(m_TaskStateColor);

	m_stMeasureCnt3D.SetFont(&m_TaskStateFont);
	m_stMeasureCnt3D.SetTextColor(RGB(0, 0, 0));
	m_stMeasureCnt3D.SetBkColor(m_TaskStateColor);
		
}

void CVS11IndexerDlg::OnNcDestroy()
{
	__super::OnNcDestroy();	
	
	
	m_TaskStateFont.DeleteObject();
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CVS11IndexerDlg::m_fnAppendList(CString strLog, LOG_INFO nColorType)
{	
	SYSTEMTIME		systime;

	CString		strLogString;

	GetLocalTime(&systime);

	//로그 타스트로 로그 전송하고,,
	m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, strLog);

	strLogString.Format("[%02dh %02dm %02ds]",systime.wHour, systime.wMinute, systime.wSecond);	
	strLogString = strLogString + strLog;	
	//로그 출력.
	m_fnLogDisplay(strLogString, nColorType);

}

void CVS11IndexerDlg::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{
	if(m_ctrListLogView.GetCount() > LIST_ALARM_COUNT)
	{
		m_ctrListLogView.DeleteString(0);
	}
	switch(nColorType)
	{
	case TEXT_NORMAL_BLACK:
		m_ctrListLogView.AddLine((CXListBox::Color)1, 	(CXListBox::Color)0, 	strLog);
		break;
	case TEXT_COLOR_GREEN:
		m_ctrListLogView.AddLine((CXListBox::Color)1, 	(CXListBox::Color)3, 	strLog);
		break;
	case TEXT_WARRING_YELLOW:
		m_ctrListLogView.AddLine((CXListBox::Color)0, 	(CXListBox::Color)12, 	strLog);
		break;
	case TEXT_ERROR_RED:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)10, strLog);
		break;
	case TEXT_NORMAL_WHITE:
		m_ctrListLogView.AddLine((CXListBox::Color)0, (CXListBox::Color)1, strLog);
		break;
	case TEXT_COLOR_BLUE:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)13, strLog);
		break;
	default:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)0, strLog);
		break;
	}

	m_ctrListLogView.SetScrollPos(SB_VERT , m_ctrListLogView.GetCount(), TRUE);
	m_ctrListLogView.SetTopIndex(m_ctrListLogView.GetCount() - 1);	
}

void CVS11IndexerDlg::OnBnClickedOk()
{
	m_sThreadParam.bThreadFlag = FALSE;
		
	Sleep(300);
	OnOK();
}

BOOL CVS11IndexerDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return __super::PreTranslateMessage(pMsg);
}


int CVS11IndexerDlg::m_fnReadSystemFile(void){
	
	return 0;
}

/*
*	Module Name	:	AOI_fnAnalyzeMsg
*	Parameter		:	Massage Parameter Pointer
*	Return			:	없음
*	Function			:	Sever로 부터 저달 되는 Command를 내부 Function과 Link 한다.(처리)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
int CVS11IndexerDlg::Sys_fnAnalyzeMsg(CMDMSG* RcvCmdMsg)
{
	VS24MotData stReturn_Mess;	
	CString	strLogMessage;
	try
	{
		if(RcvCmdMsg->uTask_Dest == m_ProcInitVal.m_nTaskNum) 
		{	
			switch(RcvCmdMsg->uFunID_Dest)
			{
				//////////////////////////////////////////////////////////////////////////
				//Project Default Command.
			case nBiz_Func_System:
			{
				switch (RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_Alive_Question:
				{
					m_cpServerInterface.m_fnSendCommand_Nrs(RcvCmdMsg->uTask_Src, nBiz_Func_System, nBiz_Seq_Alive_Response, G_VS11TASK_STATE);
					break;
				}
				case nBiz_Seq_Alive_Response:
				{
					TASK_State elTempState;
					elTempState = (TASK_State)RcvCmdMsg->uUnitID_Dest;

					switch (RcvCmdMsg->uTask_Src)
					{
					case TASK_10_Master:	
						if (G_VS10TASK_STATE != elTempState)
						{
							m_fnTaskStatusChage(TASK_10_Master, elTempState);
						}
						m_nMasterStatusCount = 0;
						G_VS10TASK_STATE = elTempState;
						//상태 갱신
						break;					
					case TASK_24_Motion:		
						if (G_VS24TASK_STATE != elTempState)
						{
							m_fnTaskStatusChage(TASK_24_Motion, elTempState);
						}
						m_nMotionStatusCount = 0;
						G_VS24TASK_STATE = elTempState;
						//상태 갱신
						break;					
					default:					
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;					
					}//End Switch	
					break;
				}
				default:
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
				break;
			}			
			case nBiz_Func_ACC: //VAC에 대한 리턴 케이스
			{					
				stReturn_Mess.Reset();							
				memcpy((char*)&stReturn_Mess, (char*)RcvCmdMsg->cMsgBuf, sizeof(VS24MotData));

				switch(RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_Read_QR:	
					break;	
				case nBiz_Seq_Light_Align:					
					break;					
				default:					
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
			}break;
			case nBiz_Func_Vac: //VAC에 대한 리턴 케이스
			{					
				stReturn_Mess.Reset();				

				int nMessSize = sizeof(VS24MotData);
				memcpy((char*)&stReturn_Mess, (char*)RcvCmdMsg->cMsgBuf, nMessSize);

				switch(RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_TM01_Vac_ON:						
					break;				
			
				default:
					{
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					}break;
				}//End Switch
			}break;
			case nBiz_3DMeasure_System:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Recv_AutoFocusEnd:	
						break;
					case nBiz_Recv_StartMeasureEnd:					
						m_btStartMeasure3D.SetRoundButtonStyle(&m_tStyle2);
						break;					
					case nBiz_3DSend_SelectLens:
						{
							m_bt3DLensSelect[0].SetRoundButtonStyle(&m_tStyle2);
							m_bt3DLensSelect[1].SetRoundButtonStyle(&m_tStyle2);
							m_bt3DLensSelect[2].SetRoundButtonStyle(&m_tStyle2);
							m_bt3DLensSelect[3].SetRoundButtonStyle(&m_tStyle2);

							switch(RcvCmdMsg->uUnitID_Dest)
							{
							case 1:m_bt3DLensSelect[0].SetRoundButtonStyle(&m_tStyle3);break;
							case 2:m_bt3DLensSelect[1].SetRoundButtonStyle(&m_tStyle3);break;
							case 3:m_bt3DLensSelect[2].SetRoundButtonStyle(&m_tStyle3);break;
							case 4:m_bt3DLensSelect[3].SetRoundButtonStyle(&m_tStyle3);break;
							default:
								break;
							}
						}break;	
								
					default:					
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
			case nBiz_3DScan_System:
			{
				switch (RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_3DScan_UnitInitialize:
					strLogMessage.Format("nBiz_3DScan_UnitInitialize Return.");
					m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
					break;
				case nBiz_3DScan_AlarmReset:
					strLogMessage.Format("nBiz_3DScan_AlarmReset Return.");
					m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
					break;
				case nBiz_3DScan_ProcessStart:
					strLogMessage.Format("nBiz_3DScan_ProcessStart Return.");
					m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
					break;
				case nBiz_3DScan_ScanStart:
					strLogMessage.Format("nBiz_3DScan_ScanStart Return.");
					m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
					break;
				case nBiz_3DScan_ScanEnd:
					strLogMessage.Format("nBiz_3DScan_ScanEnd Return.");
					m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
					break;
				case nBiz_3DScan_ProcessEnd:
					strLogMessage.Format("nBiz_3DScan_ProcessEnd Return.");
					m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
					break;
				default:
					break;
				}
				break; 
			}//nBiz_3DScan_System			
			case nBiz_3DMeasure_Status:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_3DRecv_Status:	
						{
							CString nNowState;
							switch(RcvCmdMsg->uUnitID_Dest)
							{
							case ObserveSW_Init://<<< 초기 상태
								{
									nNowState.Format("ObserveSW_Init");
									m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
								}break;
							case ObserveSW_None://<<< Observation APP이 구동되지 않았다.
								{
									nNowState.Format("ObserveSW_None");
									m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
								}break;
							case ObserveSW_OK://<<< Observation APP이 정상 구동중이다.
								{
									nNowState.Format("ObserveSW_OK");
									m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
								}break;
							case ObserveSW_CtrlPowerOff://<<< Observation APP는 구동중이나 Controller가 Off 상태 이다.
								{
									nNowState.Format("ObserveSW_CtrlPowerOff");
									m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
								}break;
							case ObserveSW_NoneRemote://<<< Observation APP와 Controller는 구동중 이지만 Remote Mode가 아니다.
								{
									nNowState.Format("ObserveSW_NoneRemote");
									m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
								}break;
							case ObserveSW_ManualMode://<<< Main UI에서 Manual Mode로 변환 한것이다.
								{
									nNowState.Format("ObserveSW_ManualMode");
									m_ctrStaticTask3DMeasure.SetWindowText(nNowState);
								}break;
							default:
								break;
							}
						}break;
						
					case nBiz_3DRecv_ActionStatus:					
						{
							CString nNowState;
							switch(RcvCmdMsg->uUnitID_Dest)
							{
							case ActionObs_Init://<<< 초기 상태
								{
									nNowState.Format("ActionObs_Init");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
									
								}break;
							case ActionObs_Manual://<<< ObserveSW_ManualMode일때는 상태 Check를 하지 않는다.
								{
									nNowState.Format("ActionObs_Manual");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_None://<<< 동작 명령 수행중이 아닐때.
								{
									nNowState.Format("ActionObs_None");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_AutoFocus://<<< Auto Focus 동작 수행시.
								{
									nNowState.Format("ActionObs_AutoFocus");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_AutoUpDownPosSetByImg://<<< 측정 Upper/Lower 위치 선정 수행시.
								{
									nNowState.Format("ActionObs_AutoUpDownPosSetByImg");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_StartMeasurement://<<< 3D Scan 수행시.
								{
									nNowState.Format("ActionObs_StartMeasurement");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_AutoSaveData://<<< 측정 Data 저장 수행시.
								{
									nNowState.Format("ActionObs_AutoSaveData");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_AutoFocus_Error://<<< Auto Focus 동작 수행시 Error.
								{
									nNowState.Format("ActionObs_AutoFocus_Error");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_AutoUpDownPosSetByImg_Error://<<< 측정 Upper/Lower 위치 선정 수행시 Error.
								{
									nNowState.Format("ActionObs_AutoUpDownPosSetByImg_Error");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_StartMeasurement_Error://<<< 3D Scan 수행시 Error.
								{
									nNowState.Format("ActionObs_StartMeasurement_Error");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_AutoSaveData_Error://<<< 측정 Data 저장 수행시 Error.
								{
									nNowState.Format("ActionObs_AutoSaveData_Error");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							case ActionObs_RemoteMode_Error://<<< 측정 가능 Remote Mode가 아니다.
								{
									nNowState.Format("ActionObs_RemoteMode_Error");
									m_ctrStaticProc3DMeasure.SetWindowText(nNowState);
								}break;
							}
						}break;					
					default:					
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
			default:
				{
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
				}break;
			}//End Switch
		}
		else
		{
			throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
		}
	}
	catch (...)
	{
		throw; 
	}
	return OKAY;
}


void CVS11IndexerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (STATUS_DISP_TIMER == nIDEvent)
	{
		in_fn_TaskStateChange(this, &m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		
		switch (G_VS11TASK_STATE)
		{
		case TASK_STATE_NONE:
			G_VS11TASK_STATE = TASK_STATE_INIT;
			break;
		case TASK_STATE_INIT:
			G_VS11TASK_STATE = TASK_STATE_IDLE;
			break;
		case TASK_STATE_IDLE:
			G_VS11TASK_STATE = TASK_STATE_RUN;
			break;
		case TASK_STATE_RUN:
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			break;
		case TASK_STATE_ERROR:
			G_VS11TASK_STATE = TASK_STATE_PM;
			break;
		case TASK_STATE_PM:
			G_VS11TASK_STATE = TASK_STATE_NONE;
			break;
		}		
	}
	__super::OnTimer(nIDEvent);
}

void CVS11IndexerDlg::m_fnTaskStatusChage(int nTaskNumber, TASK_State elTempStatus)
{
	CString strLogMessage;
	switch (elTempStatus)
	{
	case TASK_STATE_NONE:
	{	
		strLogMessage.Format("TASK No.%2d : 상태 변경 to NONE.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
		break;
	}
	case TASK_STATE_INIT:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to INIT.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		break;
		
	}
	case TASK_STATE_IDLE:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to IDLE.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_COLOR_GREEN);
		break;
	}
	case TASK_STATE_RUN:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to RUN.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_COLOR_GREEN);
		break;
	}
	case TASK_STATE_ERROR:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to ERROR.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		break;
	}
	case TASK_STATE_PM:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to PM.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		break;
	}
	default:
		break;
	}

}



void CVS11IndexerDlg::OnBnClickedBtMeasureInit()
{
	m_cpServerInterface.m_fnSendCommand_Nrs(
		TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_AlarmReset, nBiz_Unit_Zero);

	m_cpServerInterface.m_fnSendCommand_Nrs(
		TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_ManualModeON, nBiz_Unit_Zero);
	//m_cpServerInterface.m_fnSendCommand_Nrs(
	//TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_UnitInitialize, nBiz_Unit_Zero);
}


void CVS11IndexerDlg::OnBnClickedBtMeasureStart()
{
	CString strMeasureCnt;
	m_stMeasureCnt3D.GetWindowText(strMeasureCnt);
	int nMCnt = atoi(strMeasureCnt)+1;
	strMeasureCnt.Format("%d", nMCnt);
	m_stMeasureCnt3D.SetWindowText(strMeasureCnt);


	struct SEND_STARTMEASURE_DATA {
		int nMaskType;
		int nMeasureAngle;
		int nTargetThick;
		float nSpaceGap;
		BOOL bMeasureAll;
	};

	SEND_STARTMEASURE_DATA SendData;
	SendData.nMaskType = 2;		//1:MASK_TYPE_STRIPE, 2:MASK_TYPE_DOT
	SendData.nMeasureAngle = 0;		//0: 0.0Degree, 1:45.0Degree
	SendData.nTargetThick = 45;						// um 단위.
	SendData.nSpaceGap = 1000.5f;	// um 단위. Space Gap
	SendData.bMeasureAll = FALSE;	// FALSE : 가운데 하나 검사, TRUE : 전체 검사

	CString strSavePath;
	strSavePath.Format("Z:\\Measer3D\\");
	char *pSendData = new char[sizeof(SEND_STARTMEASURE_DATA) + strSavePath.GetLength()+1];
	memcpy(pSendData, &SendData, sizeof(SEND_STARTMEASURE_DATA));
	memcpy(pSendData+sizeof(SEND_STARTMEASURE_DATA), strSavePath.GetBuffer(strSavePath.GetLength()), strSavePath.GetLength());

	m_cpServerInterface.m_fnSendCommand_Nrs(
		TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_StartMeasure, (USHORT)nMCnt,(USHORT)(sizeof(SEND_STARTMEASURE_DATA) + strSavePath.GetLength()) , (UCHAR*)pSendData);

	delete [] pSendData;

	m_btStartMeasure3D.SetRoundButtonStyle(&m_tStyle3);
}


void CVS11IndexerDlg::OnBnClickedBt3dLens()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();

	SHORT nLensIndex=0;
	switch(wID)
	{
	case IDC_BT_3D_LENS_1: nLensIndex =1; break;
	case IDC_BT_3D_LENS_2: nLensIndex =2;  break;
	case IDC_BT_3D_LENS_3: nLensIndex =3; break;
	case IDC_BT_3D_LENS_4: nLensIndex =4;  break;

	}
	if(nLensIndex>0 && nLensIndex<=4)
	{
		m_cpServerInterface.m_fnSendCommand_Nrs(
			TASK_31_Mesure3D, nBiz_3DMeasure_System, nBiz_3DSend_SelectLens, nLensIndex);
	}	
}

void CVS11IndexerDlg::OnBnClickedBtnImageSave()
{
	
}

void CVS11IndexerDlg::m_fnSet_CDTP_Data(int nWidth, int nHeight, int nPosX, int nPosY)
{
	
}

void CVS11IndexerDlg::OnBnClickedBtnLotStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
