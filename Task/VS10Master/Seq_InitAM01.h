#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
class CSeq_InitAM01: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_DATA_CHECK,
		//SEQ_STEP_DOOR_LOCK,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SENSOR_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_XY,
		SEQ_STEP_SCOPE_SAFETY_XY,
		SEQ_STEP_TASK_LINK_CHECK,
		//===================================
		//SEQ_STEP_REVIEW_LENS_INIT,
		SEQ_STEP_REVIEW_LENS_CHANGE_2xLens,//Light가 바뀐다.
		SEQ_STEP_SCOPE_LENS_CHANGE_10xLens,
		//===================================
		SEQ_STEP_LIGHT_RING_BACK_REFLECT_OFF,
		//SEQ_STEP_LOADCELL_LINK_OPEN,
		//SEQ_STEP_QRCODE_LINK_OPEN,
		//SEQ_STEP_AIRE_LINK_OPEN,//Main에서 주기적으로 읽는다.
		//SEQ_STEP_SPACE_SENSOR_LINK_OPEN,
		//===================================
		SEQ_STEP_CHECK_STICK,
		//SEQ_STEP_GRIPPER_OPEN,//GRIPPER Sol은 Gripper가 Open되어 있을때만.
		SEQ_STEP_GRIPPER_SOL_BWD,
		SEQ_STEP_GRIPPER_CLOSE,
		SEQ_STEP_GRIPPER_ALL_STANDBY,
		SEQ_STEP_GRIPPER_Z_STANDBY,
		SEQ_STEP_GRIPPER_JIG_STANDBY,
		SEQ_STEP_EQ_RUN_STATE_SET,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_InitAM01(void);
	~CSeq_InitAM01(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:
	TLCheck *m_pTaskLinkCheck;
	SubDlg_CIMInterface			* m_pSubCIMInterfaceView;
	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;
	GripperPosition		*m_pGripperPos;
	int *m_pNowLensIndex_3DScope;
	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;
	CInterServerInterface  *m_pServerInterface;
};

