// CIMMsgViewDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "CIMMsgViewDlg.h"
#include "afxdialogex.h"


// CCIMMsgViewDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCIMMsgViewDlg, CDialogEx)

CCIMMsgViewDlg::CCIMMsgViewDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCIMMsgViewDlg::IDD, pParent)
{

}

CCIMMsgViewDlg::~CCIMMsgViewDlg()
{
}

void CCIMMsgViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCIMMsgViewDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CCIMMsgViewDlg 메시지 처리기입니다.

BOOL CCIMMsgViewDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCIMMsgViewDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;
	GetDlgItem(IDC_ST_MSG_TYPE)->SetFont(&G_TaskStateSmallFont);


	GetDlgItem(IDC_ST_MSG_1)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_2)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_3)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_4)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_5)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_6)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_7)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_8)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_9)->SetFont(&G_TaskStateSmallFont);
	GetDlgItem(IDC_ST_MSG_10)->SetFont(&G_TaskStateSmallFont);
	
	
	for(int iStep = 0; iStep<10; iStep++)
	{
		m_strCIMMsg[iStep].Format("%d) Message", iStep+1);
	}
	GetDlgItem(IDC_ST_MSG_1)->SetWindowText(m_strCIMMsg[0]);
	GetDlgItem(IDC_ST_MSG_2)->SetWindowText(m_strCIMMsg[1]);
	GetDlgItem(IDC_ST_MSG_3)->SetWindowText(m_strCIMMsg[2]);
	GetDlgItem(IDC_ST_MSG_4)->SetWindowText(m_strCIMMsg[3]);
	GetDlgItem(IDC_ST_MSG_5)->SetWindowText(m_strCIMMsg[4]);
	GetDlgItem(IDC_ST_MSG_6)->SetWindowText(m_strCIMMsg[5]);
	GetDlgItem(IDC_ST_MSG_7)->SetWindowText(m_strCIMMsg[6]);
	GetDlgItem(IDC_ST_MSG_8)->SetWindowText(m_strCIMMsg[7]);
	GetDlgItem(IDC_ST_MSG_9)->SetWindowText(m_strCIMMsg[8]);
	GetDlgItem(IDC_ST_MSG_10)->SetWindowText(m_strCIMMsg[9]);
}
void CCIMMsgViewDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}

HBRUSH CCIMMsgViewDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

BOOL CCIMMsgViewDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CCIMMsgViewDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CDialogEx::OnTimer(nIDEvent);
}
void CCIMMsgViewDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow ==  TRUE)
	{
		GetDlgItem(IDC_ST_MSG_1)->SetWindowText(m_strCIMMsg[0]);
		GetDlgItem(IDC_ST_MSG_2)->SetWindowText(m_strCIMMsg[1]);
		GetDlgItem(IDC_ST_MSG_3)->SetWindowText(m_strCIMMsg[2]);
		GetDlgItem(IDC_ST_MSG_4)->SetWindowText(m_strCIMMsg[3]);
		GetDlgItem(IDC_ST_MSG_5)->SetWindowText(m_strCIMMsg[4]);
		GetDlgItem(IDC_ST_MSG_6)->SetWindowText(m_strCIMMsg[5]);
		GetDlgItem(IDC_ST_MSG_7)->SetWindowText(m_strCIMMsg[6]);
		GetDlgItem(IDC_ST_MSG_8)->SetWindowText(m_strCIMMsg[7]);
		GetDlgItem(IDC_ST_MSG_9)->SetWindowText(m_strCIMMsg[8]);
		GetDlgItem(IDC_ST_MSG_10)->SetWindowText(m_strCIMMsg[9]);
	}
}
void CCIMMsgViewDlg::SetMessageData(int MsgType, char *strpMsg)
{
	if(MsgType == 1)
	{
		GetDlgItem(IDC_ST_MSG_TYPE)->SetWindowText("OP Call");
	}
	else
	{
		GetDlgItem(IDC_ST_MSG_TYPE)->SetWindowText("Terminal Message");
	}
	char strMsgBuf[100+1];
	for(int MStep=0; MStep<10; MStep++)
	{
		memset(strMsgBuf, 0x00, 101);
		memcpy(strMsgBuf, &strpMsg[MStep*100], 100);
		m_strCIMMsg[MStep].Format("%2d)%s", MStep+1, strMsgBuf);
	}
}