// StickReicpeDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS10Master.h"
#include "StickReicpeDlg.h"
#include "afxdialogex.h"


// CStickReicpeDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CStickReicpeDlg, CDialogEx)

CStickReicpeDlg::CStickReicpeDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CStickReicpeDlg::IDD, pParent)
{
	m_pAlignCellStartX = nullptr;
	m_pAlignCellStartY = nullptr;
}

CStickReicpeDlg::~CStickReicpeDlg()
{
}

void CStickReicpeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_RECIPE_FILE, m_listRecipeFile);
	DDX_Control(pDX, IDC_GRID_RECIPE_STICK_CELL, m_gridRecipe_CellInfo);
	DDX_Control(pDX, IDC_GRID_RECIPE_SCAN3D, m_gridRecipe_Scan3D);
	DDX_Control(pDX, IDC_GRID_RECIPE_MICRO_SCAN, m_gridRecipe_MicroScan);
	DDX_Control(pDX, IDC_GRID_RECIPE_MACRO_SCAN, m_gridRecipe_MacroScan);
	DDX_Control(pDX, IDC_GRID_RECIPE_3DSCOPE, m_gridRecipe_3DScope);
	DDX_Control(pDX, IDC_GRID_RECIPE_CDTP, m_gridRecipe_CDTP);

	DDX_Control(pDX, IDC_BT_SAVE, m_btSaveRecipe);
	DDX_Control(pDX, IDC_BT_EXIT, m_btExitRecipe);
	DDX_Control(pDX, IDC_BT_COPY_RECIPE, m_btCopyRecipe);
	DDX_Control(pDX, IDC_BT_SET_RECIPE, m_btSetNowRecipe);
	DDX_Control(pDX, IDC_BT_OPEN_RECIPE_FOLDER, m_btOpenRecipeFolder);
	
}


BEGIN_MESSAGE_MAP(CStickReicpeDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_SHOWWINDOW()
	ON_LBN_SELCHANGE(IDC_LIST_RECIPE_FILE, &CStickReicpeDlg::OnLbnSelchangeListRecipeFile)
	ON_BN_CLICKED(IDC_BT_SAVE, &CStickReicpeDlg::OnBnClickedBtSave)
	ON_BN_CLICKED(IDC_BT_COPY_RECIPE, &CStickReicpeDlg::OnBnClickedBtCopyRecipe)
	ON_BN_CLICKED(IDC_BT_EXIT, &CStickReicpeDlg::OnBnClickedBtExit)
	ON_BN_CLICKED(IDC_BT_SET_RECIPE, &CStickReicpeDlg::OnBnClickedBtSetRecipe)
	ON_BN_CLICKED(IDC_BT_OPEN_RECIPE_FOLDER, &CStickReicpeDlg::OnBnClickedBtOpenRecipeFolder)
	ON_BN_CLICKED(IDC_CH_RESET_CELL_ALIGN_VALUE, &CStickReicpeDlg::OnBnClickedChResetCellAlignValue)
END_MESSAGE_MAP()


// CStickReicpeDlg 메시지 처리기입니다.


BOOL CStickReicpeDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		return TRUE;
	}

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE) //Space키 눌렀을때 방지..
	{
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
void CStickReicpeDlg::InitCtrl_UI()
{
	m_btBackColor = G_Color_WinBack;


	m_listRecipeFile.SetFont(&G_RecipeFileFont);

	GetDlgItem(IDC_ED_COPY_RECIPE_NAME)->SetFont(&G_RecipeFileFont);

	tButtonStyle tStyle;
	// Get default Style
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_dHighLightX = 10.0;
	//tStyle.m_dHighLightY = 8.0;
	//tStyle.m_dRadius = 5.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btSaveRecipe.GetTextColor(&tColor);
		m_btSaveRecipe.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0x00, 0x00);//RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,25,"굴림");
			tFont.lfHeight =25;
		}

		m_btSaveRecipe.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSaveRecipe.SetTextColor(&tColor);
		m_btSaveRecipe.SetCheckButton(false);
		m_btSaveRecipe.SetFont(&tFont);

		m_btExitRecipe.SetRoundButtonStyle(&m_tButtonStyle);
		m_btExitRecipe.SetTextColor(&tColor);
		m_btExitRecipe.SetCheckButton(false);
		m_btExitRecipe.SetFont(&tFont);

		m_btCopyRecipe.SetRoundButtonStyle(&m_tButtonStyle);
		m_btCopyRecipe.SetTextColor(&tColor);
		m_btCopyRecipe.SetCheckButton(false);
		m_btCopyRecipe.SetFont(&tFont);


		m_btSetNowRecipe.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSetNowRecipe.SetTextColor(&tColor);
		m_btSetNowRecipe.SetCheckButton(false);
		m_btSetNowRecipe.SetFont(&tFont);

		m_btOpenRecipeFolder.SetRoundButtonStyle(&m_tButtonStyle);
		m_btOpenRecipeFolder.SetTextColor(&tColor);
		m_btOpenRecipeFolder.SetCheckButton(false);
		m_btOpenRecipeFolder.SetFont(&tFont);
	}


	//CGridCtrl m_gridRecipe_CellInfo;
	//CGridCtrl m_gridRecipe_Scan3D;
	//CGridCtrl m_gridRecipe_MicroScan;
	//CGridCtrl m_gridRecipe_MacroScan;
	//CGridCtrl m_gridRecipe_3DScope;
	//CGridCtrl m_gridRecipe_CDTP;

	int CellHeight = 24;
	//////////////////////////////////////////////////////////////////////////
	m_gridRecipe_CellInfo.SetDefCellWidth(50);
	m_gridRecipe_CellInfo.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();

	m_gridRecipe_CellInfo.SetRowCount(3+9+1);
	m_gridRecipe_CellInfo.SetColumnCount(4);
	m_gridRecipe_CellInfo.SetFixedRowCount(1);		
	m_gridRecipe_CellInfo.SetFixedColumnCount(1);
	m_gridRecipe_CellInfo.SetListMode(FALSE);
	//m_gridRecipe_CellInfo.SetEditable(FALSE);
	m_gridRecipe_CellInfo.SetColumnResize(FALSE);
	m_gridRecipe_CellInfo.SetRowResize(FALSE);
	m_gridRecipe_CellInfo.SetFixedTextColor(RGB(255,200,100));
	m_gridRecipe_CellInfo.SetFixedBkColor(RGB(0,0,0));

	m_gridRecipe_CellInfo.SetFont(&G_GridFont);
	m_gridRecipe_CellInfo.SetItemText(0, 0, "스틱정보");			m_gridRecipe_CellInfo.SetColumnWidth(0, 180);
	m_gridRecipe_CellInfo.SetItemText(0, 1, "Value");			m_gridRecipe_CellInfo.SetColumnWidth(1, 90);
	m_gridRecipe_CellInfo.SetItemText(0, 2, "Unit");			m_gridRecipe_CellInfo.SetColumnWidth(2, 45);
	m_gridRecipe_CellInfo.SetItemText(0, 3, "Comment");		m_gridRecipe_CellInfo.SetColumnWidth(3, 226);

	int InputIndex = 1;
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Scan_CellAlign_Pos_Shift);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Scan_CellAlign_Pos_Drive);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Scan_Cell_Tension_Value);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Stick_Width);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Stick_Edge_Distance_W);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Stick_Edge_Distance_H);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Cell_Width);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Cell_Height);	
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Cell_Horizontal_Count);	
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Cell_Vertical_Count);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Cell_Next_Horizontal_Distance);		
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 0, Key_Cell_Next_Vertical_Distance);	
	InputIndex = 1;
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CellInfo.SetItemFormat(InputIndex++, 1, DT_CENTER);
	InputIndex = 1;
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "g");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "ea");	
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "ea");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");		
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 2, "um");	
	InputIndex = 1;
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Cell Edge Align의 기본 위치(폭)");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Cell Edge Align의 기본 위치(높이)");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Stick 인장 값 (g)");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Stick 폭");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Stick 좌상단과 첫 Cell Edge 거리(폭)");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Stick 좌상단과 첫 Cell Edge 거리(높이)");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Cell 폭");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Cell 높이");	
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Cell 가로 개수");	
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Cell 세로 개수");
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Cell 다음 Cell까지 거리(폭 포함)");		
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 3, "Cell 다음 Cell까지 거리(높이 포함)");	
	//////////////////////////////////////////////////////////////////////////
	m_gridRecipe_Scan3D.SetDefCellWidth(50);
	m_gridRecipe_Scan3D.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();

	m_gridRecipe_Scan3D.SetRowCount(3+1);//(5+1);
	m_gridRecipe_Scan3D.SetColumnCount(4);
	m_gridRecipe_Scan3D.SetFixedRowCount(1);		
	m_gridRecipe_Scan3D.SetFixedColumnCount(1);
	m_gridRecipe_Scan3D.SetListMode(FALSE);
	//m_gridRecipe_Scan3D.SetEditable(FALSE);
	m_gridRecipe_Scan3D.SetColumnResize(FALSE);
	m_gridRecipe_Scan3D.SetRowResize(FALSE);
	m_gridRecipe_Scan3D.SetFixedTextColor(RGB(255,100,100));
	m_gridRecipe_Scan3D.SetFixedBkColor(RGB(0,0,0));

	m_gridRecipe_Scan3D.SetFont(&G_GridFont);
	m_gridRecipe_Scan3D.SetItemText(0, 0, "굴곡측정");			m_gridRecipe_Scan3D.SetColumnWidth(0, 180);
	m_gridRecipe_Scan3D.SetItemText(0, 1, "Value");			m_gridRecipe_Scan3D.SetColumnWidth(1, 90);
	m_gridRecipe_Scan3D.SetItemText(0, 2, "Unit");			m_gridRecipe_Scan3D.SetColumnWidth(2, 45);
	m_gridRecipe_Scan3D.SetItemText(0, 3, "Comment");		m_gridRecipe_Scan3D.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 0, Key_Trigger_Step);
	//m_gridRecipe_Scan3D.SetItemText(InputIndex++, 0, Key_Scan_Count);	
	//m_gridRecipe_Scan3D.SetItemText(InputIndex++, 0, Key_Scan_Distance);	
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 0, Key_Pass_Min_Height);	
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 0, Key_Pass_Max_Height);	
	InputIndex = 1;
	m_gridRecipe_Scan3D.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridRecipe_Scan3D.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridRecipe_Scan3D.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_Scan3D.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_Scan3D.SetItemFormat(InputIndex++, 1, DT_CENTER);

	InputIndex = 1;
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 2, "um");
	//m_gridRecipe_Scan3D.SetItemText(InputIndex++, 2, "val");
	//m_gridRecipe_Scan3D.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 2, "um");	
	InputIndex = 1;
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 3, "Scan 라인 간격");
	//m_gridRecipe_Scan3D.SetItemText(InputIndex++, 3, "Scan 회수");	
	//m_gridRecipe_Scan3D.SetItemText(InputIndex++, 3, "Scan 거리");	
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 3, "정상 판정 최소 높이");	
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 3, "정상 판정 최대 높이");	

	//////////////////////////////////////////////////////////////////////////
	m_gridRecipe_MicroScan.SetDefCellWidth(50);
	m_gridRecipe_MicroScan.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();

	m_gridRecipe_MicroScan.SetRowCount(13+1);
	m_gridRecipe_MicroScan.SetColumnCount(4);
	m_gridRecipe_MicroScan.SetFixedRowCount(1);		
	m_gridRecipe_MicroScan.SetFixedColumnCount(1);
	m_gridRecipe_MicroScan.SetListMode(FALSE);
	//m_gridRecipe_MicroScan.SetEditable(FALSE);
	m_gridRecipe_MicroScan.SetColumnResize(FALSE);
	m_gridRecipe_MicroScan.SetRowResize(FALSE);
	m_gridRecipe_MicroScan.SetFixedTextColor(RGB(100,255,100));
	m_gridRecipe_MicroScan.SetFixedBkColor(RGB(0,0,0));

	m_gridRecipe_MicroScan.SetFont(&G_GridFont);
	m_gridRecipe_MicroScan.SetItemText(0, 0, "이물검사(Micro)");			m_gridRecipe_MicroScan.SetColumnWidth(0, 180);
	m_gridRecipe_MicroScan.SetItemText(0, 1, "Value");			m_gridRecipe_MicroScan.SetColumnWidth(1, 90);
	m_gridRecipe_MicroScan.SetItemText(0, 2, "Unit");			m_gridRecipe_MicroScan.SetColumnWidth(2, 45);
	m_gridRecipe_MicroScan.SetItemText(0, 3, "Comment");		m_gridRecipe_MicroScan.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Scale_Width);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Scale_Height);	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Detect_Min);	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Detect_Max);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Skip_Out_Line);		
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Size_Filter_Minimum_Width);	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Size_Filter_Minimum_Height);	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Size_Filter_Maximum_Width);	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Size_Filter_Maximum_Height);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Defect_Distance);		
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Filter_Mask_Type);	

	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Light_Back);		
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 0, Key_MIC_Light_Ring);	
	InputIndex = 1;
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MicroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);

	InputIndex = 1;
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "um");		
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "um");		
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 2, "val");	
	InputIndex = 1;
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "가로 방향 반복 주기");
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "세로 방향 반복 주기");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "검출 허용 최소 값");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "검출 허용 최대 값");
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "취득 영상 Skip Area 지정");		
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "최소 검출 크기(가로방향 크기)");
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "최소 검출 크기(세로방향 크기)");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "최대 검출 크기(가로방향 크기)");	
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "최대 검출 크기(세로방향 크기)");
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "검출 대상간 합산 거리");		
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "검출 Filter Type");
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "하부 조명 값(0~255)");		
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 3, "검출 조명 값(0~255)");	

	//////////////////////////////////////////////////////////////////////////
	CGridCellCombo *pCell = NULL;
	CStringArray options;
	if (!m_gridRecipe_MicroScan.SetCellType(11,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("100");
	options.Add("300");
	options.Add("500");
	pCell = (CGridCellCombo*) m_gridRecipe_MicroScan.GetCell(11, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	



	//////////////////////////////////////////////////////////////////////////
	m_gridRecipe_MacroScan.SetDefCellWidth(50);
	m_gridRecipe_MacroScan.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();

	m_gridRecipe_MacroScan.SetRowCount(13+1);
	m_gridRecipe_MacroScan.SetColumnCount(4);
	m_gridRecipe_MacroScan.SetFixedRowCount(1);		
	m_gridRecipe_MacroScan.SetFixedColumnCount(1);
	m_gridRecipe_MacroScan.SetListMode(FALSE);
	//m_gridRecipe_MacroScan.SetEditable(FALSE);
	m_gridRecipe_MacroScan.SetColumnResize(FALSE);
	m_gridRecipe_MacroScan.SetRowResize(FALSE);
	m_gridRecipe_MacroScan.SetFixedTextColor(RGB(100,255,200));
	m_gridRecipe_MacroScan.SetFixedBkColor(RGB(0,0,0));

	m_gridRecipe_MacroScan.SetFont(&G_GridFont);
	m_gridRecipe_MacroScan.SetItemText(0, 0, "이물검사(Macro)");			m_gridRecipe_MacroScan.SetColumnWidth(0, 180);
	m_gridRecipe_MacroScan.SetItemText(0, 1, "Value");			m_gridRecipe_MacroScan.SetColumnWidth(1, 90);
	m_gridRecipe_MacroScan.SetItemText(0, 2, "Unit");			m_gridRecipe_MacroScan.SetColumnWidth(2, 45);
	m_gridRecipe_MacroScan.SetItemText(0, 3, "Comment");		m_gridRecipe_MacroScan.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Scale_Width);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Scale_Height);	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Detect_Min);	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Detect_Max);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Skip_Out_Line);		
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Size_Filter_Minimum_Width);	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Size_Filter_Minimum_Height);	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Size_Filter_Maximum_Width);	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Size_Filter_Maximum_Height);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Defect_Distance);		
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Filter_Mask_Type);	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Light_Upper);		
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 0, Key_MAC_Light_Ring);	
	InputIndex = 1;
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_MacroScan.SetItemFormat(InputIndex++, 1, DT_CENTER);
	InputIndex = 1;
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "um");		
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "um");		
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "val");		
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 2, "val");	
	InputIndex = 1;
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "가로 방향 반복 주기");
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "세로 방향 반복 주기");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "검출 허용 최소 값");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "검출 허용 최대 값");
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "취득 영상 Skip Area 지정");		
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "최소 검출 크기(가로방향 크기)");
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "최소 검출 크기(세로방향 크기)");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "최대 검출 크기(가로방향 크기)");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "최대 검출 크기(세로방향 크기)");
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "검출 대상간 합산 거리");		
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "검출 Filter Type");
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "상부 조명 값(0~255)");	
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 3, "검출 조명 값(0~255)");	
	if (!m_gridRecipe_MacroScan.SetCellType(11,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("100");
	options.Add("300");
	options.Add("500");
	pCell = (CGridCellCombo*) m_gridRecipe_MacroScan.GetCell(11, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	
	//////////////////////////////////////////////////////////////////////////
	m_gridRecipe_3DScope.SetDefCellWidth(50);
	m_gridRecipe_3DScope.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();

	m_gridRecipe_3DScope.SetRowCount(8+1);
	m_gridRecipe_3DScope.SetColumnCount(4);
	m_gridRecipe_3DScope.SetFixedRowCount(1);		
	m_gridRecipe_3DScope.SetFixedColumnCount(1);
	m_gridRecipe_3DScope.SetListMode(FALSE);
	//m_gridRecipe_3DScope.SetEditable(FALSE);
	m_gridRecipe_3DScope.SetColumnResize(FALSE);
	m_gridRecipe_3DScope.SetRowResize(FALSE);
	m_gridRecipe_3DScope.SetFixedTextColor(RGB(100,100,255));
	m_gridRecipe_3DScope.SetFixedBkColor(RGB(0,0,0));

	m_gridRecipe_3DScope.SetFont(&G_GridFont);
	m_gridRecipe_3DScope.SetItemText(0, 0, "둔턱측정");			m_gridRecipe_3DScope.SetColumnWidth(0, 180);
	m_gridRecipe_3DScope.SetItemText(0, 1, "Value");			m_gridRecipe_3DScope.SetColumnWidth(1, 90);
	m_gridRecipe_3DScope.SetItemText(0, 2, "Unit");				m_gridRecipe_3DScope.SetColumnWidth(2, 45);
	m_gridRecipe_3DScope.SetItemText(0, 3, "Comment");		m_gridRecipe_3DScope.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Stick_Type);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Stick_Thick);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Measure_Angle);	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Measure_Area);	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Space_Distance);	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Measure_Lens_Index);	

	m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Measure_Point_Type);	
	//m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Measure_Cell_Type);	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 0, Key_Measure_Cell_List);

	InputIndex = 1;
	m_gridRecipe_3DScope.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_3DScope.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridRecipe_3DScope.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridRecipe_3DScope.SetItemFormat(InputIndex++, 1, DT_CENTER);
	//m_gridRecipe_3DScope.SetItemFormat(InputIndex++, 1, DT_CENTER);

	InputIndex = 1;
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 2, "um");	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 2, "um");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 2, "val");

	InputIndex = 1;
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 3, "스틱 종류");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 3, "스틱 두께");	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 3, "측정 각도(직각 or 대각)");	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 3, "----");	
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 3, "Space Sensor 근접 거리");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 3, "Scope 측정 Lens");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 3, "Cell 안의 측정 위치");
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 3, "Stick 안의 Cell 선택 위치");


	if (!m_gridRecipe_3DScope.SetCellType(1,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("Stripe");
	options.Add("Dot");
	pCell = (CGridCellCombo*) m_gridRecipe_3DScope.GetCell(1, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

	if (!m_gridRecipe_3DScope.SetCellType(3,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("R_Angle");//Right_Angle
	options.Add("Diagonal");//Diagonal
	pCell = (CGridCellCombo*) m_gridRecipe_3DScope.GetCell(3, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

	if (!m_gridRecipe_3DScope.SetCellType(4,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("Point");
	options.Add("Area");
	pCell = (CGridCellCombo*) m_gridRecipe_3DScope.GetCell(4, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	



	if (!m_gridRecipe_3DScope.SetCellType(6,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("10X");
	options.Add("20X");
	options.Add("50X");
	options.Add("150X");
	pCell = (CGridCellCombo*) m_gridRecipe_3DScope.GetCell(6, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

	if (!m_gridRecipe_3DScope.SetCellType(7,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("Type_1");//Cell Edge 1Point
	options.Add("Type_2");//Cell Center 1Point
	options.Add("Type_3");//Cell Edge 대각 2Point
	options.Add("Type_4");//Cell Edge All 4Point 
	options.Add("Type_5");//Cell All 5Point 
	options.Add("Type_6");//Cell Center 3 Point
	pCell = (CGridCellCombo*) m_gridRecipe_3DScope.GetCell(7, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

	//if (!m_gridRecipe_3DScope.SetCellType(8,1, RUNTIME_CLASS(CGridCellCombo)))
	//	return ;
	//options.RemoveAll();
	//options.Add("ALL");//Cell 전체
	//options.Add("Odd");//홀수 Cell
	//options.Add("Even");//짝수 Cell
	//pCell = (CGridCellCombo*) m_gridRecipe_3DScope.GetCell(8, 1);
	//pCell->SetOptions(options);
	//pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

	//////////////////////////////////////////////////////////////////////////
	m_gridRecipe_CDTP.SetDefCellWidth(50);
	m_gridRecipe_CDTP.SetDefCellHeight(CellHeight);
	//m_GridNetworkStaus.AutoFill();
	m_gridRecipe_CDTP.SetRowCount(5+4+1);
	m_gridRecipe_CDTP.SetColumnCount(4);
	m_gridRecipe_CDTP.SetFixedRowCount(1);		
	m_gridRecipe_CDTP.SetFixedColumnCount(1);
	m_gridRecipe_CDTP.SetListMode(FALSE);
	//m_gridRecipe_CDTP.SetEditable(FALSE);
	m_gridRecipe_CDTP.SetColumnResize(FALSE);
	m_gridRecipe_CDTP.SetRowResize(FALSE);
	m_gridRecipe_CDTP.SetFixedTextColor(RGB(200,100,255));
	m_gridRecipe_CDTP.SetFixedBkColor(RGB(0,0,0));

	m_gridRecipe_CDTP.SetFont(&G_GridFont);
	m_gridRecipe_CDTP.SetItemText(0, 0, "CD, TP 측정");			m_gridRecipe_CDTP.SetColumnWidth(0, 180);
	m_gridRecipe_CDTP.SetItemText(0, 1, "Value");			m_gridRecipe_CDTP.SetColumnWidth(1, 90);
	m_gridRecipe_CDTP.SetItemText(0, 2, "Unit");			m_gridRecipe_CDTP.SetColumnWidth(2, 45);
	m_gridRecipe_CDTP.SetItemText(0, 3, "Comment");		m_gridRecipe_CDTP.SetColumnWidth(3, 226);

	InputIndex = 1;
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_Measure_Lens);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_Edge_Pos_Count_X);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_Edge_Pos_Count_Y);	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_Light_Upper);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_Light_Back);	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_CD_Light_Threshold);	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_CD_Measure_Direction);	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_TP_Mark_Distance_X);	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 0, Key_TP_Mark_Distance_Y);	

	InputIndex = 1;
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 2, "val");	
	InputIndex = 1;
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	m_gridRecipe_CDTP.SetItemFormat(InputIndex++, 1, DT_CENTER);
	InputIndex = 1;
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "측정 랜즈");
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "측정위치 지정 (X방향_Cell Edge에서 부터)");
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "측정위치 지정 (Y방향_Cell Edge에서 부터)");	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "상부 조명 값(0~255)");	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "하부 조명 값(0~255)");
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "CD 조명 처리 임계값  ");	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "CD 측정 방향 (+:수직, X:대각)");	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "TP 마크 거리 (Shift:X) ");	
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 3, "TP 마크 거리 (Drive:Y) ");	
	if (!m_gridRecipe_CDTP.SetCellType(1,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	//options.Add("02x");
	options.Add("10x");
	options.Add("20x");
	pCell = (CGridCellCombo*) m_gridRecipe_CDTP.GetCell(1, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

	if (!m_gridRecipe_CDTP.SetCellType(7,1, RUNTIME_CLASS(CGridCellCombo)))
		return ;
	options.RemoveAll();
	options.Add("+");
	options.Add("X");
	pCell = (CGridCellCombo*) m_gridRecipe_CDTP.GetCell(7, 1);
	pCell->SetOptions(options);
	pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE	

}

HBRUSH CStickReicpeDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	if(DLG_ID_Number == IDC_LIST_RECIPE_FILE)
	{
		pDC->SetTextColor(RGB(255, 255, 0)); 
		pDC->SetBkColor(RGB(0, 0, 0));
		return (HBRUSH)GetStockObject(BLACK_BRUSH);
	}
	if(	DLG_ID_Number == IDC_LIST_RECIPE_FILE ||
		DLG_ID_Number == IDC_GRID_RECIPE_STICK_CELL ||
		DLG_ID_Number == IDC_GRID_RECIPE_SCAN3D ||
		DLG_ID_Number == IDC_GRID_RECIPE_MICRO_SCAN ||
		DLG_ID_Number == IDC_GRID_RECIPE_MACRO_SCAN ||
		DLG_ID_Number == IDC_GRID_RECIPE_3DSCOPE)
		return hbr;
	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CStickReicpeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitCtrl_UI();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CStickReicpeDlg::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}
void CStickReicpeDlg::LoadRecipeFileList()
{
	m_listRecipeFile.ResetContent();
	CString tpath;
	tpath.Format("%s\\*.ini", VS10Master_RECIPE_INI_PATH);
	//검색 클래스
	CFileFind finder;

	//CFileFind는 파일, 디렉터리가 존재하면 TRUE 를 반환함
	BOOL bWorking = finder.FindFile(tpath); //
	CString fileName;
	CString DirName;
	while (bWorking)
	{
		//다음 파일 / 폴더 가 존재하면다면 TRUE 반환
		bWorking = finder.FindNextFile();
		//파일 일때
		if (finder.IsArchived())
		{
			//파일의 이름
			CString _fileName =  finder.GetFileName();

			// 현재폴더 상위폴더 썸네일파일은 제외
			if( _fileName == _T(".") || 
				_fileName == _T("..")|| 
				_fileName == _T("Thumbs.db") ) continue;

			fileName =  finder.GetFileTitle();
			m_listRecipeFile.AddString(fileName);          
		}
		// 디렉터리 일때
		//if (finder.IsDirectory())
		//{
		// 필요하면 여기서 처리
		//DirName = finder.GetFileName();
		//}
	}
}
void CStickReicpeDlg::LoadRecipe_CellInfo(char *pFilePath)
{
	//////////////////////////////////////////////////////////////////////////
	char strReadData[256];
	int InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Scan_CellAlign_Pos_Shift, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Scan_CellAlign_Pos_Drive, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Scan_Cell_Tension_Value, "0.0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Stick_Width, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Stick_Edge_Distance_W, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Stick_Edge_Distance_H, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Width, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Horizontal_Count, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Vertical_Count, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Next_Horizontal_Distance, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CELL_INFO, Key_Cell_Next_Vertical_Distance, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CellInfo.SetItemText(InputIndex++, 1, strReadData);
	m_gridRecipe_CellInfo.Refresh();
	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCAN_3D, Key_Trigger_Step, "0.0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 1, strReadData);

	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_SCAN_3D, Key_Scan_Count, "0", &strReadData[0], 256, pFilePath);
	//m_gridRecipe_Scan3D.SetItemText(InputIndex++, 1, strReadData);

	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_SCAN_3D, Key_Scan_Distance, "0", &strReadData[0], 256, pFilePath);
	//m_gridRecipe_Scan3D.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCAN_3D, Key_Pass_Min_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCAN_3D, Key_Pass_Max_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_Scan3D.SetItemText(InputIndex++, 1, strReadData);
	m_gridRecipe_Scan3D.Refresh();
	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Scale_Width, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Scale_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Detect_Min, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Detect_Max, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Skip_Out_Line, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Minimum_Width, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Minimum_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Maximum_Width, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Maximum_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Defect_Distance, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Filter_Mask_Type, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Light_Back, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MICRO_SCAN, Key_MIC_Light_Ring, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MicroScan.SetItemText(InputIndex++, 1, strReadData);

	m_gridRecipe_MicroScan.Refresh();

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Scale_Width, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Scale_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Detect_Min, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Detect_Max, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Skip_Out_Line, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Minimum_Width, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Minimum_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Maximum_Width, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Maximum_Height, "0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Defect_Distance, "200", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Filter_Mask_Type, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Light_Upper, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_MACRO_SCAN, Key_MAC_Light_Ring, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_MacroScan.SetItemText(InputIndex++, 1, strReadData);

	m_gridRecipe_MacroScan.Refresh();

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Stick_Type, "Dot", &strReadData[0], 256, pFilePath);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Stick_Thick, "0.0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Angle, "R_Angle", &strReadData[0], 256, pFilePath);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Area, "Point", &strReadData[0], 256, pFilePath);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Space_Distance, "20.0", &strReadData[0], 256, pFilePath);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Lens_Index, "20X", &strReadData[0], 256, pFilePath);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Point_Type, "TYPE_1", &strReadData[0], 256, pFilePath);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);

	//memset(strReadData, 0x00, 256);
	//GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Cell_Type, "ALL", &strReadData[0], 256, pFilePath);
	//m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_SCOPE_3D, Key_Measure_Cell_List, "1, 4, 7", &strReadData[0], 256, pFilePath);
	m_gridRecipe_3DScope.SetItemText(InputIndex++, 1, strReadData);
	m_gridRecipe_3DScope.Refresh();

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Measure_Lens, "10x", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Edge_Pos_Count_X, "5", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Edge_Pos_Count_Y, "5", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Light_Upper, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_Light_Back, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_CD_Light_Threshold, "60", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_CD_Measure_Direction, "+", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);


	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_TP_Mark_Distance_X, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);

	memset(strReadData, 0x00, 256);
	GetPrivateProfileString(Section_CD_TP, Key_TP_Mark_Distance_Y, "100", &strReadData[0], 256, pFilePath);
	m_gridRecipe_CDTP.SetItemText(InputIndex++, 1, strReadData);

	m_gridRecipe_CDTP.Refresh();
}

void CStickReicpeDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE)
	{
		CheckDlgButton(IDC_CH_RESET_CELL_ALIGN_VALUE, BST_CHECKED);
		LoadRecipeFileList();

		char strReadData[256];
		memset(strReadData, 0x00, 256);
		GetPrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, "", &strReadData[0], 256, VS10Master_PARAM_INI_PATH);
		if(strlen(strReadData)!=7)
		{
			CString strNowRecipeName;
			m_listRecipeFile.GetText(0, strNowRecipeName);
			m_strNowEditRecipeFile.Format("%s\\%s.ini", VS10Master_RECIPE_INI_PATH, strNowRecipeName);
			if(m_listRecipeFile.SelectString(0, strNowRecipeName)>=0)
				LoadRecipe_CellInfo(m_strNowEditRecipeFile.GetBuffer(m_strNowEditRecipeFile.GetLength()));
			else
				WritePrivateProfileString(Section_Process_Data, Key_NowRunRecipeName,"", VS10Master_PARAM_INI_PATH);
		}
		else
		{
			m_strNowEditRecipeFile.Format("%s\\%s.ini", VS10Master_RECIPE_INI_PATH, strReadData);
			if(m_listRecipeFile.SelectString(0, strReadData)>=0)
				LoadRecipe_CellInfo(m_strNowEditRecipeFile.GetBuffer(m_strNowEditRecipeFile.GetLength()));
			else
				WritePrivateProfileString(Section_Process_Data, Key_NowRunRecipeName,"", VS10Master_PARAM_INI_PATH);
		}
	}
}


void CStickReicpeDlg::OnLbnSelchangeListRecipeFile()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nIndex =m_listRecipeFile.GetCurSel();
	CString strNowRecipeName;
	m_listRecipeFile.GetText(nIndex, strNowRecipeName);
	m_strNowEditRecipeFile.Format("%s\\%s.ini", VS10Master_RECIPE_INI_PATH, strNowRecipeName);
	LoadRecipe_CellInfo(m_strNowEditRecipeFile.GetBuffer(m_strNowEditRecipeFile.GetLength()));
}
void CStickReicpeDlg::SaveRecipe(CString strNewPath)
{
	CString strNewData;
	//Cell Info Save
	int InputIndex = 1;
	
	//int AlignShiftPos = 0;
	//int AlignDrivePos = 0;
	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	//AlignShiftPos = atoi(strNewData);
	WritePrivateProfileString(Section_CELL_INFO, Key_Scan_CellAlign_Pos_Shift, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);
	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	//AlignDrivePos = atoi(strNewData);
	WritePrivateProfileString(Section_CELL_INFO, Key_Scan_CellAlign_Pos_Drive, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	//이전 스틱 정보를 갱신하면 Cell Align정보를 Reset하자
	//if(AlignShiftPos !=  *m_pAlignCellStartX|| AlignDrivePos!=*m_pAlignCellStartY)
	if( IsDlgButtonChecked(IDC_CH_RESET_CELL_ALIGN_VALUE) ==BST_CHECKED)
	{
		WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosShift, "0", VS10Master_PARAM_INI_PATH);
		WritePrivateProfileString(Section_Process_Data, Key_NowCellAlignPosDrive, "0", VS10Master_PARAM_INI_PATH);
	}


	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Scan_Cell_Tension_Value, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Stick_Width, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Stick_Edge_Distance_W, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Stick_Edge_Distance_H, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Cell_Width, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Cell_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Cell_Horizontal_Count, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Cell_Vertical_Count, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Cell_Next_Horizontal_Distance, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CellInfo.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CELL_INFO, Key_Cell_Next_Vertical_Distance, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);
	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridRecipe_Scan3D.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCAN_3D, Key_Trigger_Step, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	//strNewData.Format("%s", m_gridRecipe_Scan3D.GetItemText(InputIndex++, 1));
	//WritePrivateProfileString(Section_SCAN_3D, Key_Scan_Count, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	//strNewData.Format("%s", m_gridRecipe_Scan3D.GetItemText(InputIndex++, 1));
	//WritePrivateProfileString(Section_SCAN_3D, Key_Scan_Distance, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_Scan3D.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCAN_3D, Key_Pass_Min_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_Scan3D.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCAN_3D, Key_Pass_Max_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCOPE_3D, Key_Stick_Type, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCOPE_3D, Key_Stick_Thick, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCOPE_3D, Key_Measure_Angle, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCOPE_3D, Key_Measure_Area, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);
	
	strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCOPE_3D, Key_Space_Distance, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);
	strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCOPE_3D, Key_Measure_Lens_Index, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCOPE_3D, Key_Measure_Point_Type, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	//strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	//WritePrivateProfileString(Section_SCOPE_3D, Key_Measure_Cell_Type, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_3DScope.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_SCOPE_3D, Key_Measure_Cell_List, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Scale_Width, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Scale_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Detect_Min, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Detect_Max, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Skip_Out_Line, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Minimum_Width, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Minimum_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Maximum_Width, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Size_Filter_Maximum_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Defect_Distance, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Filter_Mask_Type, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Light_Back, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MicroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MICRO_SCAN, Key_MIC_Light_Ring, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Scale_Width, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Scale_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Detect_Min, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Detect_Max, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Skip_Out_Line, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Minimum_Width, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Minimum_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Maximum_Width, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Size_Filter_Maximum_Height, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Defect_Distance, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Filter_Mask_Type, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Light_Upper, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_MacroScan.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_MACRO_SCAN, Key_MAC_Light_Ring, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	//////////////////////////////////////////////////////////////////////////
	InputIndex = 1;
	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_Measure_Lens, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_Edge_Pos_Count_X, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_Edge_Pos_Count_Y, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_Light_Upper, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_Light_Back, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_CD_Light_Threshold, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_CD_Measure_Direction, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);


	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_TP_Mark_Distance_X, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

	strNewData.Format("%s", m_gridRecipe_CDTP.GetItemText(InputIndex++, 1));
	WritePrivateProfileString(Section_CD_TP, Key_TP_Mark_Distance_Y, strNewData.GetBuffer(strNewData.GetLength()), strNewPath);

}

void CStickReicpeDlg::OnBnClickedBtSave()
{
	SaveRecipe(m_strNowEditRecipeFile);
	int nIndex =m_listRecipeFile.GetCurSel();
	CString strSelectRecipeName;
	m_listRecipeFile.GetText(nIndex, strSelectRecipeName);

	G_pCIMInterface->m_fnSendPPIDCmd(strSelectRecipeName, MODIFY_PPID);
	
	//CIM Interface 
	//G_pCIMInterface->m_fnSendSVID_ReqAck()

	CheckDlgButton(IDC_CH_RESET_CELL_ALIGN_VALUE, BST_CHECKED);
}


void CStickReicpeDlg::OnBnClickedBtCopyRecipe()
{
	CString strNewRecipeName;
	GetDlgItem(IDC_ED_COPY_RECIPE_NAME)->GetWindowText(strNewRecipeName);
	//if(strNewRecipeName.GetLength() == 7)
	if(strNewRecipeName.GetLength() > 0)
	{
		//if(strNewRecipeName.GetAt(2) =='_' && strNewRecipeName.GetAt(4) == '_')
		{
			G_WriteInfo(Log_Normal,"New Recipe Add(%s)", strNewRecipeName.GetBuffer(strNewRecipeName.GetLength()));

			int nIndex =m_listRecipeFile.GetCurSel();
			if(nIndex >=0 )
			{
				CString strNewCopyPath;
				strNewCopyPath.Format("%s\\%s.ini", VS10Master_RECIPE_INI_PATH, strNewRecipeName);
				SaveRecipe(strNewCopyPath);
				LoadRecipeFileList();
				CString strNowRecipeName;
				strNowRecipeName.Format("%s", strNewRecipeName);
				m_listRecipeFile.SelectString(0, strNowRecipeName);
				LoadRecipe_CellInfo(strNewCopyPath.GetBuffer(strNewCopyPath.GetLength()));

				GetDlgItem(IDC_ED_COPY_RECIPE_NAME)->SetWindowText("");

				G_pCIMInterface->m_fnSendPPIDCmd(strNewRecipeName, NEW_PPID);
			}
			else
				G_WriteInfo(Log_Check,"Copy Target Recipe Select Plz....!!!");
		}
		//else
		//	G_WriteInfo(Log_Check,"Recipe Naming conventions Error!!!(%s)", strNewRecipeName.GetBuffer(strNewRecipeName.GetLength()));
	}
	else
		G_WriteInfo(Log_Check,"Recipe Name Size Error!!!(%s)", strNewRecipeName.GetBuffer(strNewRecipeName.GetLength()));
}


void CStickReicpeDlg::OnBnClickedBtExit()
{
	this->ShowWindow(SW_HIDE);
}


void CStickReicpeDlg::OnBnClickedBtSetRecipe()
{
	int nIndex =m_listRecipeFile.GetCurSel();
	if(nIndex >=0 )
	{
		int nIndex =m_listRecipeFile.GetCurSel();
		CString strSelectRecipeName;
		m_listRecipeFile.GetText(nIndex, strSelectRecipeName);
		WritePrivateProfileString(Section_Process_Data, Key_NowRunRecipeName, strSelectRecipeName.GetBuffer(strSelectRecipeName.GetLength()), VS10Master_PARAM_INI_PATH);

		::SendMessage(this->GetParent()->m_hWnd, WM_USER_RECIPE_CHANGE, 0, 0);

		this->ShowWindow(SW_HIDE);
	}
	else
	{
		G_WriteInfo(Log_Check,"New Set Target Recipe Select Plz....!!!");
	}
}


void CStickReicpeDlg::OnBnClickedBtOpenRecipeFolder()
{

	SaveRecipe(m_strNowEditRecipeFile);
	int nIndex =m_listRecipeFile.GetCurSel();
	CString strSelectRecipeName;
	m_listRecipeFile.GetText(nIndex, strSelectRecipeName);


	CString strPath = VS10Master_RECIPE_INI_PATH; // 여기에 경로를.. 
	strPath.Format("%s\\%s.ini", VS10Master_RECIPE_INI_PATH, strSelectRecipeName);

	CString strMsg;
	strMsg.Format("%s File을 삭제하시겠습니까?", strSelectRecipeName);

	if(AfxMessageBox(strMsg,MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDYES)
	{		
		G_pCIMInterface->m_fnSendPPIDCmd(strSelectRecipeName, DELETE_PPID);
		DeleteFile(strPath);
	}

	ShowWindow(SW_HIDE);
	


// 	ShellExecute(NULL, _T("open"), _T("explorer"), strPath,  NULL, SW_SHOW);
// 	//ShellExecute(NULL, _T("open"), _T("explorer"), _T("/select,") + strPath, NULL, SW_SHOW);
// 	this->ShowWindow(SW_HIDE);
}

void CStickReicpeDlg::OnBnClickedChResetCellAlignValue()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
