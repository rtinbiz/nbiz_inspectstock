#pragma once

#ifdef USE_CAM_DEVICE
#include "..\..\CommonHeader\MIL64_92\mil.h"
#endif

#pragma comment (lib, "D:/nBiz_InspectStock/CommonDLL64/Mil.lib")

static const unsigned int  REAL_IMAGE_SIZE_X		=  2448;
static const unsigned int  REAL_IMAGE_SIZE_Y		=  2048;

class CLiveViewCtrl
{
public:
	CLiveViewCtrl(void);
	~CLiveViewCtrl(void);

private:
	int m_nDeviceId;
#ifdef USE_CAM_DEVICE
	MIL_ID MilDigitizer;

	MIL_INT		DigSizeX;			   // Digitizer input width
	MIL_INT		DigSizeY;			   // Digitizer input heigh
	MIL_INT		Band;				      // Number of input color bands of the digitizer

	MIL_ID MilImage[2];        /* Image buffer identifier. */
	//MIL_ID MilDisplay;      /* Display identifier.      */
	//MIL_ID MilOverlayImage;	            //Overlay image buffer identifier 
	//MIL_ID MilImageDisplay;        /* Image buffer (display) identifier. */
	MIL_ID MilApplication;  /* Application identifier.  */
	MIL_ID MilSystem;       /* System identifier.       */
#endif

public:
	HWND 	m_UpdateWindHandle;
	bool		m_RunThread;
	CWinThread	*m_pHandleThread;
	IplImage *m_pLiveViewImg;
public:
	bool m_bViewReviewCalc;
	bool m_bViewCenterLine;

	bool m_bViewRulerLine;
	bool m_bAutoReviewInfo;
	bool m_bCDTPReviewInfo;
	IplImage *m_pAutoReviewImg;
	IplImage *m_pAutoReviewSmallImg;

	int m_NowLensIndex;
	float m_fPixelSizeX;
	float m_fPixelSizeY;
	float m_fRulerScale;
	void DrawRuler(IplImage *pTarget);

	//saveImage��
	bool m_bSaveImage;
	bool m_bSaveOverlayImage;
	char m_strSavePath[MAX_PATH];//saveImage��(Path)
	IplImage **m_ppNewImg;//saveImage��(Iplimge)
	
	bool m_bLiveViewOnOff;
	CvPoint m_MousePos;

	CvPoint m_ReviewStagePos;

	bool m_bDisplayInterlock;
	bool m_bSafetyPos[8];
	bool m_bScopeSafetyPos;
public:
	
	bool InitCamCtrl(int nBoardId, int nDeviceId, HWND UpdateWindHandle, char *pDcfFile);
	void CamGrab(char* chImage);
	void ReleaseCamCtrl();
	bool LiveView(bool OnOff){m_bLiveViewOnOff = OnOff; return m_bLiveViewOnOff;}

	void ChangeRuler(bool OnOff, float fPixelSizeX = 10.0f,float fPixelSizeY= 10.0f, int LensIndex = REVIEW_LENS_2X);//float fRulerScale = 5.0f);
	bool DrawCenterLine(bool OnOff){m_bViewCenterLine = OnOff; return m_bViewCenterLine;}

	bool DrawReviewCalc(bool OnOff){m_bViewReviewCalc = OnOff; return m_bViewReviewCalc;}

	bool DrawAutoReviewInfo(bool OnOff){m_bAutoReviewInfo = OnOff; return m_bAutoReviewInfo;}
	void SetAutoRevewInfo(int nTotalcnt, int CntIndex, CString strBoxID, CString strStickID, COORD_DINFO *pNowReviewData, CvPoint MachinePos, IplImage*pSmallImg=nullptr);

	CString m_strCDTPInfo;
	CvRect m_CDPos;
	CvRect m_TPPos;
	bool DrawCDTPReviewInfo(bool OnOff){m_bCDTPReviewInfo = OnOff; return m_bCDTPReviewInfo;}
	void SetCDTPReviewInfo(int nTotalcnt, int CntIndex, CString strStickID, CvRect CDPos, CvRect TPPos);

	void SaveImage(char *pSaveFullPath);
	void SaveImage(IplImage **ppNewImg);

	
};

UINT Thread_ReviewGrabProc(LPVOID pParam);

double getObjectAngle(IplImage *src);
void rotateImage(const IplImage* src, IplImage* dst, double degree);

DWORD ScaleMeasuer(IplImage* srcClone, int LensIndex, double fPixelSizeX, double fPixelSizeY,  int CheckRectSize = 300, bool isLive = false);