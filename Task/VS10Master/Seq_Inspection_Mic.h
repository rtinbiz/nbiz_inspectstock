#pragma once
#include "..\..\CommonHeader\Sequence\SeqBase.h"
#include "Seq_InsAutoReview.h"

class CSeq_Inspect_Mic: public CSeqBase
{
public:
	enum
	{
		SEQ_STEP_ERROR = SQ_STEP_ERROR,
		SEQ_STEP_IDLE = SQ_STEP_IDLE,
		SEQ_STEP_RUN = SQ_STEP_RUN,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_STICK_STATE_CHECK,
		//========================
		SEQ_STEP_SPACE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_Z,
		SEQ_STEP_RING_LIGHT_DOWN,
		SEQ_STEP_REVIEW_DOWN,
		SEQ_STEP_SCOPE_SAFETY_Z,
		SEQ_STEP_SCAN_SAFETY_XY,
		SEQ_STEP_SCOPE_SAFETY_XY,
		//==============================
		SEQ_STEP_SCAN_INSPECT_Z_POS,
		SEQ_STEP_SCAN_INSPECTION_INIT,
		SEQ_STEP_SCAN_SEND_RECIPE,
		SEQ_STEP_SCAN_INSPECT_START_XY,
		SEQ_STEP_SCAN_LIGHT,
		SEQ_STEP_RING_LIGHT_UP,
		SEQ_STEP_SCAN_INSPECT_DATA_CALC,
		//S--------------------------------------
		SEQ_STEP_SCAN_INSPECT_SCAN_START_POS_XY,
		SEQ_STEP_SCAN_INSPECT_SCAN_START,
		SEQ_STEP_SCAN_INSPECT_SCAN_END,
		//-E-------------------------------------
		SEQ_STEP_END_SCAN_LIGHT,
		SEQ_STEP_END_RING_LIGHT_DOWN,
		SEQ_STEP_END_REVIEW_DOWN,
		//S--------------------------------------
		SEQ_STEP_AUTO_REVEW_SEQ,
		//E--------------------------------------
		SEQ_STEP_END_SCAN_SAFETY_Z,
		SEQ_STEP_END_SCAN_SAFETY_XY,
		SEQ_STEP_END_SCOPE_SAFETY_XY,
		//////////////////////////////////////////////////////////////////////////
		SEQ_STEP_CNT
	};
public:
	CSeq_Inspect_Mic(void);
	~CSeq_Inspect_Mic(void);
	bool fnSeqFlowStart();
	int fnSeqFlowProc(int ProcStep);
	int fnSeqErrorStepProc();//Seq Error 발생시 조치 사항.
	//int m_ResultSEQ;
	int m_StartTech;
	int m_SeqTechTime;
	void  fnSeqUpdateTech();
	//////////////////////////////////////////////////////////////////////////
public:
	bool			*m_pbInspectMicOrMac;//true : Mic, false:Mac
	int						*m_pAlignCellStartX;
	int						*m_pAlignCellStartY;
	GLASS_PARAM				*m_pGlassReicpeParam;
	SAOI_IMAGE_INFO			*m_pstMaskImageInfo_Mic;
	SAOI_COMMON_PARA		*m_pstCommonPara_Mic;	
	//SAOI_IMAGE_INFO			*m_pstMaskImageInfo_Mac;
	//SAOI_COMMON_PARA		*m_pstCommonPara_Mac;	
	HW_PARAM						*m_pHWRecipeParam;//Inspector에서 받아 서 저장 하는것.


	bool								*m_pBlockEnd;
	DWORD							*m_pStartBlockTick;
	DWORD							*m_pStartLineTick;
	int									*m_pCamScnaLineCnt;
	int									*m_pCamScnaImgCnt;
	int									*m_pStartScanLineNum;


	Scope3DPosition	*m_pScopePos;
	ReviewPosition		*m_pReviewPos;
	ObjDistance			*m_pCamToObjDis;
	int m_InspectStartShift;
	int m_InspectStartDrive;


	int m_NowScanIndex;
	CPoint *m_pLineScanStartPosList;

	CSeq_InsAutoReview *m_pSeqObj_InsAutoReview;
	int m_RecvCMD_Ret;
	int m_RecvCMD;
	int m_WaitCMD;
	CInterServerInterface  *m_pServerInterface;
};

