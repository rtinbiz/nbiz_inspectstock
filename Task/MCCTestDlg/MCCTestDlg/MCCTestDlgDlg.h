
// MCCTestDlgDlg.h : 헤더 파일
//

#pragma once

#include "..\..\..\CommonHeader\Class\InterServerInterface.h"
#include "..\..\..\CommonHeader\ProcInitial.h"
#include "..\..\..\CommonHeader\MCC_Command.h"



// CMCCTestDlgDlg 대화 상자
class CMCCTestDlgDlg : public CDialogEx, public CProcInitial, public CInterServerInterface
{
// 생성입니다.
public:
	CMCCTestDlgDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MCCTESTDLG_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	CMCC_Command m_MccCommand;


// 구현입니다.
protected:
	HICON m_hIcon;

	int		m_fnAnalyzeMsg(CMDMSG* cmdMsg);										//내부 메시지 처리자.. 실질적인 Client 메시지 처리 기능을 한다.
	afx_msg LRESULT OnUserCallback(WPARAM wParam, LPARAM lParam);			    //Client message cllback func... 내부에서 m_fnAnalyzeMsg를 호출해준다.
	
	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
};
