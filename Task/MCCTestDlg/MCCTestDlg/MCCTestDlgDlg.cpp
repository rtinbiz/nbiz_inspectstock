
// MCCTestDlgDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "MCCTestDlg.h"
#include "MCCTestDlgDlg.h"
#include "afxdialogex.h"
#include "..\..\..\CommonHeader\MCCDataStruct.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_USER_CALLBACK							WM_USER + 100

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
		
END_MESSAGE_MAP()


// CMCCTestDlgDlg 대화 상자




CMCCTestDlgDlg::CMCCTestDlgDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMCCTestDlgDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMCCTestDlgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMCCTestDlgDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_USER_CALLBACK, OnUserCallback)
	ON_BN_CLICKED(IDC_BUTTON1, &CMCCTestDlgDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CMCCTestDlgDlg 메시지 처리기

BOOL CMCCTestDlgDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	CInterServerInterface(this->m_hWnd, WM_USER_CALLBACK);
	m_fnReadDatFile("APP_TEST.DAT"); // vs64 Data File Read

	int nRet = m_fnRegisterTask(m_ProcInitVal.m_nTaskNum, m_ProcInitVal.m_sServerIP, m_ProcInitVal.m_nPortNo);

	m_fnSetLogTaskInfo(m_ProcInitVal.m_nLogTaskNo, m_ProcInitVal.m_nLogBlockLovel, m_ProcInitVal.m_nLogFileID);		

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CMCCTestDlgDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CMCCTestDlgDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMCCTestDlgDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

LRESULT CMCCTestDlgDlg::OnUserCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	CMDMSG* CmdMsg = (CMDMSG*)lParam;
	if(CmdMsg != NULL)
	{
		nRet = m_fnAnalyzeMsg(CmdMsg);

		if(OKAY != nRet)
		{
			//Client 메시지 처리에대한 에러처리...
		}

		//		if(CmdMsg->uMsgType==CMD_TYPE_NORES){
		m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_fnFreeMemory(CmdMsg);	
		//		}		
		return OKAY;
	}

	//Server Shot Down.
	PostQuitMessage(0);
	return NG;
}


int CMCCTestDlgDlg::m_fnAnalyzeMsg(CMDMSG* m_RcvCmdMsg)
{
	int nRet = OKAY;
	CEdit* pWnd = NULL;	

	UCHAR nReciveData[255];
	memset(nReciveData, 0x00, 255);

	switch(m_RcvCmdMsg->uFunID_Dest)
	{
	case 1:
		{
			switch(m_RcvCmdMsg->uSeqID_Dest)
			{
			case 1: // unit Num 0, 1, 2, 3 (IDLE, READY, BUSY, ALARM)
				{	

				}
			}
		}
	
		break;
	default:
		{
			break;
		}
	}

	return nRet;
}


void CMCCTestDlgDlg::OnBnClickedButton1()
{	
	// 1. 파일 정보를 Ini에 Write 한다.  한번 정보를 받으면 가변적이지 않는 Data을 적는다.
// 	m_MccCommand.m_fnMCC_WriteInfo(N_MODULE_ID, "TEST1");   // Module ID는 가변적이다.
// 	m_MccCommand.m_fnMCC_WriteInfo(N_LOGTYPE, "TEST2");     // Log Type은 함수안에...

	m_MccCommand.m_fnMCC_WriteInfo(N_STEPID, "Step225");		 // Step ID는 고정
	//m_fnMCC_WriteInfo(N_STICKID, "StickGood");  Stick 아이디 
	m_MccCommand.m_fnMCC_WriteInfo(N_BOXID, "BoxGood");		 // BOX ID도 고정
	m_MccCommand.m_fnMCC_WriteInfo(N_HOSTPPID, "Host111");	 // Host ID도 고정
	m_MccCommand.m_fnMCC_WriteInfo(N_EQPPPID, "Eqp222");	     // Eqp ID도 고정

	// 2. Write End 했음을 알린다.
	m_MccCommand.m_fnMCC_WriteInfoEnd();

	// 3. Event를Log를 남긴다.	
	m_MccCommand.m_fnMCC_ActionLog(INIT_LIFT_ORIGIN, TRUE, "ABC");
	Sleep(100);
	m_MccCommand.m_fnMCC_ActionLog(INIT_LIFT_ORIGIN, FALSE, "ABC");

	m_MccCommand.m_fnMCC_ActionLog(LOAD_POS_MOVE01, TRUE, "CDEF");
	Sleep(100);
	m_MccCommand.m_fnMCC_ActionLog(LOAD_POS_MOVE01, FALSE, "CDEF");
	Sleep(100);

	m_MccCommand.m_fnMCC_SignalLog(STEP1_BOX_DETECT_SENSOR1_1, TRUE, "STICKNUM_B");
	Sleep(100);	
 	m_MccCommand.m_fnMCC_SignalLog(STEP1_BOX_DETECT_SENSOR1_1, FALSE, "STICK_C");
	Sleep(100);
 
	m_MccCommand.m_fnMCC_EventLog_CompInOut(TRUE, MODULE_BC01_BT01, "STICKNUM_A", LAYER_BT01, LAYER_BT01);
	Sleep(100); 	
	m_MccCommand.m_fnMCC_EventLog_CompInOut(FALSE, "BC01_BT01", "STICKNUM_A", LAYER_BT01, LAYER_TM01);
	Sleep(100);
 
	m_MccCommand.m_fnMCC_EventLog_ChangeProcessState("BC01_BT01", "STICKNUM_B", "IDLE", "EXECUTING");
	Sleep(100);
 	
 	m_MccCommand.m_fnMCC_EventLog_ChangeState("BC01_BT01", "STICKNUM_B", "NORMAL", "FAULT", "1000", "DoorOpen");
 	m_MccCommand.m_fnMCC_EventLog_ChangePPID("BC01_BT01", "STICKNUM_B", "AK_R_01", "GK_M_01");

	// InfoValue
	m_MccCommand.m_fnMCC_InfoInputValue(SET_SERVO_UPDOWN_SPD, 111);

	m_MccCommand.m_fnMCC_InfoInputValue(SERVO_UPDOWN_CUR_SPD, 222);

	m_MccCommand.m_fnMCC_InfoInputValue(SET_SERVO_UPDOWN_POS, 333);

	m_MccCommand.m_fnMCC_InfoInputValue(SERVO_UPDOWN_CUR_POS, 444);

	m_MccCommand.m_fnMCC_InfoInputValue(SERVO_UPDOWN_TOQ, 555);	

}
