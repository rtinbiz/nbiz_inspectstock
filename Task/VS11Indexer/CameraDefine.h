#pragma once
#include "StdAfx.h"

static const unsigned int  REAL_IMAGE_SIZE_X		=  1920;
static const unsigned int  REAL_IMAGE_SIZE_Y		=  1440;

static const double	PI						=	3.14159;

static const UINT_PTR	PROCESS_TIMER		= 100;
static const UINT_PTR	TIME_TIMER			= 101;

static const UINT		LISTDATACOUNT		= 400;

static const int			 CAM1 =		1;
static const int			 CAM2 =		2;


struct STHREAD_CAMERA
{
	void*		cObjectPointer;					///<Thread Object Pointer
	HWND 		mUpdateWindHandle1;
	HWND 		mUpdateWindHandle2;	
	HWND		hParentHandle;
	BOOL		bThreadFlag;	
	BOOL		bImageSave;
	BOOL		bPaperDetect;
	BOOL		bStickDetect;
	BOOL		bStickAlign;
	STHREAD_CAMERA()
	{
		mUpdateWindHandle1	= NULL;
		mUpdateWindHandle2	= NULL;
		hParentHandle = NULL;
		bThreadFlag			= true;
		bImageSave			= FALSE;
		bPaperDetect		= FALSE;
		bStickDetect = FALSE;
		bStickAlign			= FALSE;
	}
};

struct SRGB_VALUE
{
	BYTE	nRValue;	///해당 픽셀의 Red Value
	BYTE	nGValue;	///해당 픽셀의 Green Value
	BYTE	nBValue;	///해당 픽셀의 Blue Value
	SRGB_VALUE()
	{
		memset(this, 0x00, sizeof(SRGB_VALUE));
	}
};


struct SXY_POSITION
{
	USHORT usXPos;		///X 좌표
	USHORT usYPos;		///Y 좌표.
	SXY_POSITION()
	{
		memset(this, 0x00, sizeof(SXY_POSITION));
	}
};

struct SALIGN_RECIPE
{	
	int		nxytuvw;
	int		nHoleThresValue; // Hole threshold(dark)
	int		nMarkThresValue; // Mark threshold(White)
	int		nRoiLeft;
	int		nRoiTop;
	int		nRoiRight;
	int		nRoiBottom;
	double	lfMinMatchRate;
	int		nMinMarkArea;
	// 'Hole' define
	int		nDiameter; // Hole diameter as pixel
	// '+' Mark (Recipe value)
	int		nMarkArea;	// sum of pixels
	int		nMarkWidth;	// Width of mark. unit:pixles
	int		nMarkBarWidth;	// Width of Mark bar width. unit:pixels
	BOOL	bWhite;	// White mark(true), Dark mark(black)

	int		nHoleThresValue2; // Hole threshold(dark)
	int		nMarkThresValue2; // Mark threshold(White)
	int		nRoiLeft2;
	int		nRoiTop2;
	int		nRoiRight2;
	int		nRoiBottom2;
	double	lfMinMatchRate2;
	int		nMinMarkArea2;
	// 'Hole' define
	int		nDiameter2; // Hole diameter as pixel
	// '+' Mark (Recipe value)
	int		nMarkArea2;	// sum of pixels
	int		nMarkWidth2;	// Width of mark. unit:pixles
	int		nMarkBarWidth2;	// Width of Mark bar width. unit:pixels
	BOOL	bWhite2;	// White mark(true), Dark mark(black)
	int		nGoodRange;
	double	dX1; // unit:um. 1번 카메라 recipe setting시 나온 dX
	double	dY1; // 1번 카메라 recipe setting시 나온 dY
	double	dX2; // 2번 카메라 recipe setting시 나온 dX
	double	dY2; // 2번 카메라 recipe setting시 나온 dY

	// Align mark position (Recipe value) 단위 : um
	double	lfPosX1; // 1번 mark의 x 좌표(center 0,0기준)
	double	lfPosY1; // 1번 mark의 y 좌표(center 0,0기준)
	double	lfPosX2; // 2번 mark의 x 좌표(center 0,0기준)
	double	lfPosY2; // 2번 mark의 y 좌표(center 0,0기준)
	
	SALIGN_RECIPE()
	{
		memset(this, 0x00, sizeof(SALIGN_RECIPE));
	}
};

#pragma  pack(1)

//////////////////////////////////////////////////////////////////////////
//Bit Data 정의
typedef union
{
	WORD AUnit_W;
	struct
	{
		WORD m_Ready:1;
		WORD m_Busy:1;
		WORD m_NG:1;	
		WORD m_OK:1;	
		WORD m_Inspect:1;
		WORD m_Alarm:1;

		//WORD m_Heartbeat:1;
		//WORD m_LogReadOK:1;
		WORD m_Reserve:10;

	}AUnit_Bit;

}AUnit_Mem;

typedef union
{
	WORD PLC_W;
	struct
	{
		WORD m_Ready:1;
		WORD m_Start:1;
		WORD m_Moving:1;	
		WORD m_AlarmRest:1;	
		WORD m_Heartbeat:1;
		WORD m_LogWrite:1;

		WORD m_Reserve:12;

	}PLC_Bit;

}PLC_Mem;

//////////////////////////////////////////////////////////////////////////
//String 구조 선언
typedef struct 
{
	//char strGlassID[16];
	//char strBatchID[16];
	//char strLotID[16];
	//char strCSTID[16];
	//char strSlotID[4];
	//char strProcID[20];
	//char strProductID[20];
	//char strStepID[12];
	//char strProductType[4];
	//char strPPID[16];

	char strLotID[8*2];
	char strProcessingCode[4*2];
	char strLotSpecificData[4*2];
	char strHostRecipeNumber[1*2];
	char strGlassType[1*2];
	char strGlsCode[1*2];
	char strGlassID[8*2];
	char strGlsJudge[1*2];
	char strGlassSpecificData[4*2];
	char strGlassAddData[6*2];
	char strPreviousUnitProcessingData[4*2];
	char strGlassProcessingData[3*2];
	char strGlassRoutingData[3*2];

}GlassInfo;

typedef struct 
{
	char strTime[17];//YYYYMMDD:HH:MM:SS
	char strSeparate1;//Space '20h'
	char strLevel;//H/L/E
	char strSeparate2;//Space '20h'
	char strLog[80];//"xxxxx"
}FixPLC_Log;

typedef struct 
{
	char strTime[17];//YYYYMMDD:HH:MM:SS
	char strSeparate1;//Space '20h'
	char strLevel;//H/L/E
	char strSeparate2;//Space '20h'
	char strLogLenght[2];//"35"
	char *pstrLog;
}VarPLC_Log;

//////////////////////////////////////////////////////////////////////////
//관리 구조 선언.
typedef struct 
{
	AUnit_Mem		m_BitSig;
	WORD			m_WatchDog;
	int					m_ResultX1;
	int					m_ResultX2;
	int					m_ResultY1;
	int					m_ResultY2;

	USHORT			m_AlarmCode;
	char				m_RecipeName[8*2];

}AUnit_Data;


typedef struct 
{
	PLC_Mem		m_BitSig;
	WORD			m_WatchDog;
	GlassInfo		m_GlassInfo;
	char				m_RecipeName[8*2];
	FixPLC_Log		m_PLCLog;

}PLC_Data;

//////////////////////////////////////////////////////////////////////////
//전체 Memory 선언.
typedef struct 
{
	AUnit_Data m_AUnit;
	PLC_Data	m_PLC;

}COM_DATA;
#pragma pack()

