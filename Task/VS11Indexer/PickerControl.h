#pragma once

static const BOOL LOCK = TRUE;
static const BOOL FREE = FALSE;

class CPickerControl
{
public:
	CPickerControl();
	~CPickerControl();

	int m_fnSetParameter(
		STM02_RECIPE* stpointer1,
		SBO01_POSITION* stpointer2,
		STT01_POSITION* stpointer3		
		);

	int	m_fnPaperMove_LT01_TM02();
	void m_fnSet_PaperMove_LT01_TM02_Step(EL_PAPER_MOVE_LT01_TM02 step);
	int	m_fnPaperMove_TM02_UT01();
	int	m_fnPaperMove_TM02_BT01();

	int m_fnStickMove_LT01_TM02();
	void m_fnSet_StickMove_LT01_TM02_Step(EL_STICK_MOVE_LT01_TM02 step);

	int m_fnStickMove_TM02_TT01();
	void m_fnSet_StickMove_TM02_TT01_Step(EL_STICK_MOVE_TM02_TT01 step);

	int m_fnStickMove_TT01_AM01();	
	void m_fnSet_StickMove_TT01_AM01_Step(EL_STICK_MOVE_TT01_AM01 step);

	int m_fnStickMove_AM01_UT02();
	void m_fnSet_StickMove_AM01_UT02_Step(EL_STICK_MOVE_AM01_UT02 step);

	int m_fnStickMove_TT01_UT02();

	int m_fnStickMove_UT02_TM02();
	int m_fnStickMove_TM02_UT01();
	int m_fnStickMove_TM02_BT01();
public:	
	EL_PAPER_MOVE_LT01_TM02 m_elPPMove_LT01_TM02;
	EL_PAPER_MOVE_TM02_UT01 m_elPPMove_TM02_UT01;
	EL_PAPER_MOVE_TM02_BT01 m_elPPMove_TM02_BT01;
	EL_STICK_MOVE_LT01_TM02 m_elSTMove_LT01_TM02;
	EL_STICK_MOVE_TM02_TT01 m_elSTMove_TM02_TT01;
	EL_STICK_MOVE_TT01_AM01 m_elSTMove_TT01_AM01;
	EL_STICK_MOVE_AM01_UT02 m_elSTMove_AM01_UT02;
	EL_STICK_MOVE_UT02_TM02 m_elSTMove_UT02_TM02;
	EL_STICK_MOVE_TM02_UT01 m_elSTMove_TM02_UT01;
	EL_STICK_MOVE_TM02_BT01 m_elSTMove_TM02_BT01;
	EL_STICK_MOVE_TT01_UT02 m_elSTMove_TT01_UT02;
	SEQUENCE_ERROR m_elSeq_Error;

	SSTICK_INFO m_stStickInfo;

	const STM02_RECIPE*		m_st_TM02_Cur_Recipe;
	const SBO01_POSITION*	m_st_BO01_Pos;
	const STT01_POSITION*	m_st_TT01_Pos;

	BOOL m_bTurnTablePositionLock;
	BOOL m_bSingleStickRun;
	BOOL m_bLastObject;

	//const STM02_POSITION* m_st_TM02_Pos;	
};

