#include "StdAfx.h"
#include "DemoSequence.h"
#include "VS11Indexer.h"
#include "VS11IndexerDlg.h"



CDemoSequence::CDemoSequence()
{
	m_elPaperMoveStep = PM_MAIN_SEQ_IDLE;
	m_elStickMoveStep = SM_MAIN_SEQ_IDLE;
}


CDemoSequence::~CDemoSequence()
{
}

int CDemoSequence::m_fnSetParameter(SBT01_POSITION* stpointer)
{
	m_st_BT01_Pos = stpointer;	
//  	cpMainDlg = (CVS11IndexerDlg*)cpointer1;
//  	cpServerInterface = (CInterServerInterface*)cpointer2;
	return 0;
}

// int CDemoSequence::m_fnPaperMove_F_LT01_T_UT01()
// {
// 	CString strLogMessage;
// 	switch (m_elPaperMoveStep)
// 	{
// 	case PM_MAIN_SEQ_IDLE:
// 
// 		break;
// 	case PM_TM02_DRIVE_LT01_POS:
// 		G_VS11TASK_STATE = TASK_STATE_RUN;
// 		in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Pos->nDrive_LT01_Pos);
// 		m_elPaperMoveStep = PM_TM02_DRIVE_LT01_CHECK;
// 		break;
// 	case PM_TM02_DRIVE_LT01_CHECK:
// 		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
// 				m_st_TM02_Pos->nDrive_LT01_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elPaperMoveStep = PM_TM02_CYL_DN;
// 			}			
// 		}		
// 		break;
// 	case PM_TM02_CYL_DN:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperCylDn();
// 		//g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperCylDn2();
// 		m_elPaperMoveStep = PM_TM02_CYL_DN_CHECK;
// 		break;
// 	case PM_TM02_CYL_DN_CHECK:
// 		if (G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS == COMPLETE/* &&
// 			G_MOTION_STATUS[TM02_MID_CYL].B_COMMAND_STATUS == COMPLETE*/)
// 		{
// 			if (G_STMACHINE_STATUS.stPickerCylinder.In01_Sn_PickerPaperLeftDown == ON &&
// 				G_STMACHINE_STATUS.stPickerCylinder.In03_Sn_PickerPaperRightDown == ON /*&&
// 				G_STMACHINE_STATUS.stPickerCylinder.In05_Sn_PickerPaperCenterDown == ON*/)
// 			{
// 				m_elPaperMoveStep = PM_TM02_Z_LT01_POS;
// 			}			
// 		}		
// 		break;
// 	case PM_TM02_Z_LT01_POS:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Pos->nPaperZ_LT01_Pos);
// 		m_elPaperMoveStep = PM_TM02_Z_LT01_CHECK;
// 		break;
// 	case PM_TM02_Z_LT01_CHECK:
// 		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
// 				m_st_TM02_Pos->nPaperZ_LT01_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elPaperMoveStep = PM_TM02_PP_GET_PURGE;
// 			}			
// 		}		
// 		break;	
// 
// 	case PM_TM02_PP_GET_PURGE:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperPurgeOn();
// 		m_elPaperMoveStep = PM_TM02_PP_GET_PURGE_CHECK;
// 		break;
// 	case PM_TM02_PP_GET_PURGE_CHECK:
// 		if (G_MOTION_STATUS[TM02_PP_PURGE].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			m_elPaperMoveStep = PM_TM02_PP_VAC_ON;
// 		}
// 		break;
// 
// 	case PM_TM02_PP_VAC_ON:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperVacOn();
// 		m_elPaperMoveStep = PM_TM02_PP_VAC_ON_CHECK;
// 		break;
// 	case PM_TM02_PP_VAC_ON_CHECK:
// 		if (G_MOTION_STATUS[TM02_PP_VAC].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			//if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerVacuum == ON)
// 			{
// 				m_elPaperMoveStep = PM_TM02_Z_READY_POS_S1;
// 			}			
// 		}		
// 		break;
// 	case PM_TM02_Z_READY_POS_S1:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Pos->nStickZ_Ready_Pos);
// 		m_elPaperMoveStep = PM_TM02_Z_READY_CHECK_S1;
// 		break;
// 	case PM_TM02_Z_READY_CHECK_S1:
// 		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
// 				m_st_TM02_Pos->nStickZ_Ready_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elPaperMoveStep = PM_TM02_DRIVE_UT01_POS;
// 			}			
// 		}		
// 		break;
// 	case PM_TM02_DRIVE_UT01_POS:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Pos->nDrive_UT01_Pos);
// 		m_elPaperMoveStep = PM_TM02_DRIVE_UT01_CHECK;
// 		break;
// 	case PM_TM02_DRIVE_UT01_CHECK:
// 		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
// 				m_st_TM02_Pos->nDrive_UT01_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elPaperMoveStep = PM_TM02_Z_UT01_POS;
// 			}			
// 		}
// 		
// 		break;
// 	case PM_TM02_Z_UT01_POS:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Pos->nPaperZ_UT01_Pos);
// 		m_elPaperMoveStep = PM_TM02_Z_UT01_CHECK;
// 		break;
// 	case PM_TM02_Z_UT01_CHECK:
// 		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
// 				m_st_TM02_Pos->nPaperZ_UT01_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elPaperMoveStep = PM_TM02_PP_VAC_OFF;
// 			}			
// 		}		
// 		break;
// 	case PM_TM02_PP_VAC_OFF:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperVacOff();
// 		m_elPaperMoveStep = PM_TM02_PP_PURGE;
// 		break;
// 	case PM_TM02_PP_PURGE:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperPurgeOn();
// 		m_elPaperMoveStep = PM_TM02_PP_PURGE_CHECK;
// 		break;
// 	case PM_TM02_PP_PURGE_CHECK:
// 		if (G_MOTION_STATUS[TM02_PP_PURGE].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			m_elPaperMoveStep = PM_TM02_PP_VAC_OFF_CHECK;
// 		}		
// 		break;
// 	case PM_TM02_PP_VAC_OFF_CHECK:
// 		if (G_MOTION_STATUS[TM02_PP_VAC].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			//if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerVacuum == OFF)
// 			{
// 				m_elPaperMoveStep = PM_TM02_CYL_UP;
// 			}			
// 		}
// 		break;
// 	case PM_TM02_CYL_UP:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperCylUp();
// 		//g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperCylUp2();
// 		m_elPaperMoveStep = PM_TM02_CYL_UP_CHECK;
// 		break;
// 	case PM_TM02_CYL_UP_CHECK:
// 		if (G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS == COMPLETE/* &&
// 			G_MOTION_STATUS[TM02_MID_CYL].B_COMMAND_STATUS == COMPLETE*/)
// 		{
// 			if (G_STMACHINE_STATUS.stPickerCylinder.In00_Sn_PickerPaperLeftUp == ON &&
// 				G_STMACHINE_STATUS.stPickerCylinder.In02_Sn_PickerPaperRightUp == ON /*&&
// 				G_STMACHINE_STATUS.stPickerCylinder.In04_Sn_PickerPaperCenterUp == ON*/)
// 			{
// 				m_elPaperMoveStep = PM_TM02_Z_READY_POS_S2;
// 			}			
// 		}		
// 		break;
// 	case PM_TM02_Z_READY_POS_S2:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Pos->nStickZ_Ready_Pos);
// 		m_elPaperMoveStep = PM_TM02_Z_READY_CHECK_S2;
// 		break;
// 	case PM_TM02_Z_READY_CHECK_S2:
// 		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
// 				m_st_TM02_Pos->nStickZ_Ready_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elPaperMoveStep = PM_SEQ_COMP;
// 			}			
// 		}		
// 		break;
// 	case PM_SEQ_COMP:
// 		strLogMessage.Format("TM02 : 간지 이동 완료(F : LT01  -> T : UT01).");
// 		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
// 		m_elPaperMoveStep = PM_MAIN_SEQ_IDLE;
// 		g_cpMainDlg->m_elDemoSequence = DEMO_SEQUENCE_IDLE;
// 
// 		G_VS11TASK_STATE = TASK_STATE_IDLE;
// 		in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
// 
// 		break;	
// 	default:
// 		break;
// 	}
// 	return 0;
// }

int CDemoSequence::m_fnPaperMove_F_LT01_T_UT01()
{
	CString strLogMessage;
	switch (m_elPaperMoveStep)
	{
	case PM_MAIN_SEQ_IDLE:

		break;
	case PM_TM02_DRIVE_LT01_POS:
		G_VS11TASK_STATE = TASK_STATE_RUN;
		in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		g_cpMainDlg->m_cBoxTable->m_fnBT01UpDnMove(m_st_BT01_Pos->nBadBoxPosition);
		m_elPaperMoveStep = PM_TM02_DRIVE_LT01_CHECK;
		break;
	case PM_TM02_DRIVE_LT01_CHECK:
		if (G_MOTION_STATUS[BT01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
				m_st_BT01_Pos->nBadBoxPosition) <= POSITION_OFFSET)
			{
				m_elPaperMoveStep = PM_TM02_DRIVE_UT01_POS;
			}			
		}		
		break;	
	case PM_TM02_DRIVE_UT01_POS:
		g_cpMainDlg->m_cBoxTable->m_fnBT01UpDnMove(m_st_BT01_Pos->nBox7Position);
		m_elPaperMoveStep = PM_TM02_DRIVE_UT01_CHECK;
		break;
	case PM_TM02_DRIVE_UT01_CHECK:
		if (G_MOTION_STATUS[BT01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
				m_st_BT01_Pos->nBox7Position) <= POSITION_OFFSET)
			{
				m_elPaperMoveStep = PM_TM02_DRIVE_LT01_POS;
			}			
		}
		
		break;
	
	case PM_SEQ_COMP:
		strLogMessage.Format("TM02 : 간지 이동 완료(F : LT01  -> T : UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
		m_elPaperMoveStep = PM_MAIN_SEQ_IDLE;
		g_cpMainDlg->m_elDemoSequence = DEMO_SEQUENCE_IDLE;

		G_VS11TASK_STATE = TASK_STATE_IDLE;
		in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);

		break;	
	default:
		break;
	}
	return 0;
}

 int CDemoSequence::m_fnStickMove_F_LT01_T_UT01()
 {
	 return 0;
 }
// int CDemoSequence::m_fnStickMove_F_LT01_T_UT01()
// {
// 	CString strLogMessage;
// 
// 	switch (m_elStickMoveStep)
// 	{
// 	case SM_MAIN_SEQ_IDLE:
// 
// 		break;
// 	case SM_TM02_DRIVE_LT01_POS:
// 		G_VS11TASK_STATE = TASK_STATE_RUN;
// 		in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
// 
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Pos->nDrive_LT01_Pos);
// 		m_elStickMoveStep = SM_TM02_DRIVE_LT01_CHECK;
// 		break;
// 	case SM_TM02_DRIVE_LT01_CHECK:
// 		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
// 				m_st_TM02_Pos->nDrive_LT01_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elStickMoveStep = SM_TM02_Z_LT01_POS;
// 			}
// 		}		
// 		break;
// 	case SM_TM02_Z_LT01_POS:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Pos->nStickZ_LT01_Pos);
// 		m_elStickMoveStep = SM_TM02_Z_LT01_CHECK;
// 		break;
// 	case SM_TM02_Z_LT01_CHECK:
// 		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
// 				m_st_TM02_Pos->nStickZ_LT01_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elStickMoveStep = SM_TM02_ST_VAC_ON;
// 			}
// 		}
// 		
// 		break;
// 	case SM_TM02_ST_VAC_ON:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnStickVacOn();
// 		m_elStickMoveStep = SM_TM02_ST_VAC_ON_CHECK;
// 		break;
// 	case SM_TM02_ST_VAC_ON_CHECK:
// 		if (G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			//if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerVacuum == ON)
// 			{
// 				m_elStickMoveStep = SM_TM02_Z_READY_POS_S1;
// 			}
// 		}
// 		
// 		break;
// 	case SM_TM02_Z_READY_POS_S1:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Pos->nStickZ_Ready_Pos);
// 		m_elStickMoveStep = SM_TM02_Z_READY_CHECK_S1;
// 		break;
// 	case SM_TM02_Z_READY_CHECK_S1:
// 		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
// 				m_st_TM02_Pos->nStickZ_Ready_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elStickMoveStep = SM_TM02_DRIVE_UT01_POS;
// 			}
// 		}		
// 		break;
// 	case SM_TM02_DRIVE_UT01_POS:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Pos->nDrive_UT01_Pos);
// 		m_elStickMoveStep = SM_TM02_DRIVE_UT01_CHECK;
// 		break;
// 	case SM_TM02_DRIVE_UT01_CHECK:
// 		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
// 				m_st_TM02_Pos->nDrive_UT01_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elStickMoveStep = SM_TM02_Z_UT01_POS;
// 			}
// 		}		
// 		break;
// 	case SM_TM02_Z_UT01_POS:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Pos->nStickZ_UT01_Pos);
// 		m_elStickMoveStep = SM_TM02_Z_UT01_CHECK;
// 		break;
// 	case SM_TM02_Z_UT01_CHECK:
// 		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
// 				m_st_TM02_Pos->nStickZ_UT01_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elStickMoveStep = SM_TM02_ST_VAC_OFF;
// 			}
// 		}
// 		
// 		break;
// 	case SM_TM02_ST_VAC_OFF:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnStickVacOff();
// 		m_elStickMoveStep = SM_TM02_ST_PURGE;
// 		break;	
// 	case SM_TM02_ST_PURGE:
// 		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnStickPurgeOn();
// 		m_elStickMoveStep = SM_TM02_ST_PURGE_CHECK;
// 		break;
// 	case SM_TM02_ST_PURGE_CHECK:
// 
// 		if (G_MOTION_STATUS[TM02_ST_PURGE].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			m_elStickMoveStep = SM_TM02_ST_VAC_OFF_CHECK;
// 		}
// 		
// 		break;
// 	case SM_TM02_ST_VAC_OFF_CHECK:
// 		if (G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			//if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerVacuum == OFF)
// 			{
// 				m_elStickMoveStep = SM_TM02_Z_READY_POS_S2;
// 			}
// 		}
// 		break;
// 	case SM_TM02_Z_READY_POS_S2:
// 		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Pos->nStickZ_Ready_Pos);
// 		m_elStickMoveStep = SM_TM02_Z_READY_CHECK_S2;
// 		break;
// 	case SM_TM02_Z_READY_CHECK_S2:
// 		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
// 		{
// 			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
// 				m_st_TM02_Pos->nStickZ_Ready_Pos) <= POSITION_OFFSET)
// 			{
// 				m_elStickMoveStep = SM_SEQ_COMP;
// 			}
// 		}
// 		
// 		break;
// 	case SM_SEQ_COMP:
// 		strLogMessage.Format("TM02 : 스틱 이동 완료(F : LT01 -> T : UT01).");
// 		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
// 		m_elStickMoveStep = SM_MAIN_SEQ_IDLE;
// 		g_cpMainDlg->m_elDemoSequence = DEMO_SEQUENCE_IDLE;
// 
// 		G_VS11TASK_STATE = TASK_STATE_IDLE;
// 		in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
// 		break;	
// 	default:
// 		break;
// 	}
// 	return 0;
// }
