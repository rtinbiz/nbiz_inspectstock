#pragma once
#include "afxwin.h"


// CDisplayDlg1 대화 상자입니다.

class CDisplayDlg1 : public CDialogEx
{
	DECLARE_DYNAMIC(CDisplayDlg1)

public:
	CDisplayDlg1(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDisplayDlg1();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_RC1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	int m_fnCreateButton(void);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void m_fnDisplaySignal();
	void m_fnDisplayPosition_BT01_UpDn(BT01_SIGNAL_STEP step);
	void m_fnDisplayPosition_TM01_Drive(TM01_SIGNAL_STEP step);
	void m_fnDisplayPosition_LT01_Pusher(LT01_SIGNAL_STEP step);
	void m_fnDisplayPosition_BO01_Drive(BO01_SIGNAL_STEP step);
	void m_fnDisplayPosition_BO01_UpDn(BO01_SIGNAL_STEP step);
	void m_fnDisplayPosition_BO01_Rotate(BO01_SIGNAL_STEP step);
	void m_fnDisplayPosition_UT01_Pusher(UT01_SIGNAL_STEP step);
	void m_fnDisplayAllOn();
	void m_fnDisplayAllOff();

	void m_fnBoxSelectBtn_Release();
public:
	COLORREF	m_btBackColor;
	
	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyleOn;	
	CRoundButtonStyle	m_tStyleOff;
	CRoundButtonStyle	m_tStyleWork;


	CRoundButton2 m_ctrBtn_BT01[BT01_SIGNAL_COUNT];		
	CRoundButton2 m_ctrBtn_TM01[TM01_SIGNAL_COUNT];	
	CRoundButton2 m_ctrBtn_LT01[LT01_SIGNAL_COUNT];
	CRoundButton2 m_ctrBtn_BO01[BO01_SIGNAL_COUNT];	
	CRoundButton2 m_ctrBtn_UT01[UT01_SIGNAL_COUNT];	

	CRoundButton2 m_ctrBtn_WorkBox[BOX_COUNT - 1];


	afx_msg void OnBnClickedBtnWorkBox1();
	afx_msg void OnBnClickedBtnWorkBox2();
	afx_msg void OnBnClickedBtnWorkBox3();
	afx_msg void OnBnClickedBtnWorkBox4();
	afx_msg void OnBnClickedBtnWorkBox5();
	afx_msg void OnBnClickedBtnWorkBox6();
	afx_msg void OnBnClickedBtnWorkBox7();
};
