// MainControl.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "MainControl.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CMainControl 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMainControl, CDialogEx)

CMainControl::CMainControl(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMainControl::IDD, pParent)
{

}

CMainControl::~CMainControl()
{
}

void CMainControl::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON1, m_ctrBtnManualSequence[STEP_BOX_LOADING]);
	DDX_Control(pDX, IDC_BUTTON2, m_ctrBtnManualSequence[STEP_COVER_OPEN]);
	DDX_Control(pDX, IDC_BUTTON3, m_ctrBtnManualSequence[STEP_COVER_CLOSE]);
	DDX_Control(pDX, IDC_BUTTON4, m_ctrBtnManualSequence[STEP_MOVE_TO_LT01]);
	DDX_Control(pDX, IDC_BUTTON5, m_ctrBtnManualSequence[STEP_BOX_UNLOADING]);
	DDX_Control(pDX, IDC_BUTTON6, m_ctrBtnManualSequence[STEP_PP_MOVE_TO_TM02]);
	DDX_Control(pDX, IDC_BUTTON7, m_ctrBtnManualSequence[STEP_PP_MOVE_TO_UT01]);
	DDX_Control(pDX, IDC_BUTTON8, m_ctrBtnManualSequence[STEP_PP_MOVE_TO_BT01]);
	DDX_Control(pDX, IDC_BUTTON9, m_ctrBtnManualSequence[STEP_ST_MOVE_TO_TM02]);
	DDX_Control(pDX, IDC_BUTTON10, m_ctrBtnManualSequence[STEP_ST_MOVE_TO_TT01]);
	DDX_Control(pDX, IDC_BUTTON11, m_ctrBtnManualSequence[STEP_ST_MOVE_TO_AM01]);
	DDX_Control(pDX, IDC_BUTTON12, m_ctrBtnManualSequence[STEP_ST_MOVE_TO_UT02]);
	DDX_Control(pDX, IDC_BUTTON13, m_ctrBtnManualSequence[STEP_ST_MOVE_TO_TM02_2]);
	DDX_Control(pDX, IDC_BUTTON14, m_ctrBtnManualSequence[STEP_ST_MOVE_TO_UT01]);
	DDX_Control(pDX, IDC_BUTTON15, m_ctrBtnManualSequence[STEP_ST_MOVE_TO_BT01]);
	DDX_Control(pDX, IDC_BUTTON16, m_ctrBtnManualSequence[STEP_ST_CHECK_TO_UT02]);
	DDX_Control(pDX, IDC_BTN_CAPTURE, m_ctrBtnImageSave);

	DDX_Control(pDX, IDC_BTN_BOX_INDEX1, m_ctrBtnBoxIndex[BOX1 - 1]);
	DDX_Control(pDX, IDC_BTN_BOX_INDEX2, m_ctrBtnBoxIndex[BOX2 - 1]);
	DDX_Control(pDX, IDC_BTN_BOX_INDEX3, m_ctrBtnBoxIndex[BOX3 - 1]);
	DDX_Control(pDX, IDC_BTN_BOX_INDEX4, m_ctrBtnBoxIndex[BOX4 - 1]);
	DDX_Control(pDX, IDC_BTN_BOX_INDEX5, m_ctrBtnBoxIndex[BOX5 - 1]);
	DDX_Control(pDX, IDC_BTN_BOX_INDEX6, m_ctrBtnBoxIndex[BOX6 - 1]);
	DDX_Control(pDX, IDC_BTN_BOX_INDEX7, m_ctrBtnBoxIndex[BOX7 - 1]);
}


BEGIN_MESSAGE_MAP(CMainControl, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON1, &CMainControl::OnBnClicked_STEP_BOX_LOADING)
	ON_BN_CLICKED(IDC_BUTTON2, &CMainControl::OnBnClicked_STEP_COVER_OPEN)
	ON_BN_CLICKED(IDC_BUTTON3, &CMainControl::OnBnClicked_STEP_COVER_CLOSE)
	ON_BN_CLICKED(IDC_BUTTON4, &CMainControl::OnBnClicked_STEP_MOVE_TO_LT01)
	ON_BN_CLICKED(IDC_BUTTON5, &CMainControl::OnBnClicked_STEP_BOX_UNLOADING)
	ON_BN_CLICKED(IDC_BUTTON6, &CMainControl::OnBnClicked_STEP_PP_MOVE_TO_TM02)
	ON_BN_CLICKED(IDC_BUTTON7, &CMainControl::OnBnClicked_STEP_PP_MOVE_TO_UT01)
	ON_BN_CLICKED(IDC_BUTTON8, &CMainControl::OnBnClicked_STEP_PP_MOVE_TO_BT01)
	ON_BN_CLICKED(IDC_BUTTON9, &CMainControl::OnBnClicked_STEP_ST_MOVE_TO_TM02)
	ON_BN_CLICKED(IDC_BUTTON10, &CMainControl::OnBnClicked_STEP_ST_MOVE_TO_TT01)
	ON_BN_CLICKED(IDC_BUTTON11, &CMainControl::OnBnClicked_STEP_ST_MOVE_TO_AM01)
	ON_BN_CLICKED(IDC_BUTTON12, &CMainControl::OnBnClicked_STEP_ST_MOVE_TO_UT02)
	ON_BN_CLICKED(IDC_BUTTON13, &CMainControl::OnBnClicked_STEP_ST_MOVE_TO_TM02_2)
	ON_BN_CLICKED(IDC_BUTTON14, &CMainControl::OnBnClicked_STEP_ST_MOVE_TO_UT01)
	ON_BN_CLICKED(IDC_BUTTON15, &CMainControl::OnBnClicked_STEP_ST_MOVE_TO_BT01)
	ON_BN_CLICKED(IDC_BUTTON16, &CMainControl::OnBnClicked_STEP_ST_CHECK_TO_UT02)
	ON_BN_CLICKED(IDC_BTN_CAPTURE, &CMainControl::OnBnClickedBtnCapture)
	ON_BN_CLICKED(IDC_BTN_BOX_INDEX7, &CMainControl::OnBnClickedBtnBoxIndex7)
	ON_BN_CLICKED(IDC_BTN_BOX_INDEX6, &CMainControl::OnBnClickedBtnBoxIndex6)
	ON_BN_CLICKED(IDC_BTN_BOX_INDEX5, &CMainControl::OnBnClickedBtnBoxIndex5)
	ON_BN_CLICKED(IDC_BTN_BOX_INDEX4, &CMainControl::OnBnClickedBtnBoxIndex4)
	ON_BN_CLICKED(IDC_BTN_BOX_INDEX3, &CMainControl::OnBnClickedBtnBoxIndex3)
	ON_BN_CLICKED(IDC_BTN_BOX_INDEX2, &CMainControl::OnBnClickedBtnBoxIndex2)
	ON_BN_CLICKED(IDC_BTN_BOX_INDEX1, &CMainControl::OnBnClickedBtnBoxIndex1)
END_MESSAGE_MAP()


// CMainControl 메시지 처리기입니다.


BOOL CMainControl::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CMainControl::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
}


HBRUSH CMainControl::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CMainControl::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);

	m_fnCreateButton();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMainControl::m_fnCreateButton(void)
{
	tButtonStyle tStyle;

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 8.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled = m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled = RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled = RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked = RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked = RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed = RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed = RGB(0x20, 0x80, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);

	m_tStyle2.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 8.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled = m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled = RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled = RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked = RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked = RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed = RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed = RGB(0x20, 0x20, 0x20);
	m_tStyle2.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	m_ctrBtnManualSequence[STEP_BOX_LOADING].GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled = RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked = RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed = RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnManualSequence[STEP_BOX_LOADING].GetFont(&tFont);
	strncpy_s(tFont.lfFaceName, "굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	for (int nStep = 0; nStep < STEP_COUNT; nStep++)
	{
		m_ctrBtnManualSequence[nStep].SetTextColor(&tColor);
		m_ctrBtnManualSequence[nStep].SetCheckButton(false);
		m_ctrBtnManualSequence[nStep].SetFont(&tFont);
		m_ctrBtnManualSequence[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	for (int nStep = 0; nStep < BOX7; nStep++)
	{
		m_ctrBtnBoxIndex[nStep].SetTextColor(&tColor);
		m_ctrBtnBoxIndex[nStep].SetCheckButton(false);
		m_ctrBtnBoxIndex[nStep].SetFont(&tFont);
		m_ctrBtnBoxIndex[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	m_ctrBtnImageSave.SetTextColor(&tColor);
	m_ctrBtnImageSave.SetCheckButton(false);
	m_ctrBtnImageSave.SetFont(&tFont);
	m_ctrBtnImageSave.SetRoundButtonStyle(&m_tStyle2);
	
}

void CMainControl::OnBnClicked_STEP_BOX_LOADING()//STEP_BOX_LOADING
{
	g_cpMainDlg->m_elBoxSequence = BOX_MOVE_F_BT01_T_LT01;
	g_cpMainDlg->m_cBoxControl->m_elBoxLoadStep = BL_LT01_RECIPE_SEND;
}


void CMainControl::OnBnClicked_STEP_COVER_OPEN()//STEP_COVER_OPEN
{
	g_cpMainDlg->m_elBoxSequence = BOX_COVER_OPEN;
	g_cpMainDlg->m_cBoxControl->m_elCoverOpenStep = CO_BO01_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_COVER_CLOSE()//STEP_COVER_CLOSE
{
	g_cpMainDlg->m_elBoxSequence = BOX_COVER_CLOSE;
	g_cpMainDlg->m_cBoxControl->m_elCoverCloseStep = CC_BO01_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_MOVE_TO_LT01()//STEP_MOVE_TO_LT01
{
	g_cpMainDlg->m_elBoxSequence = BOX_MOVE_F_UT01_T_LT01;
	g_cpMainDlg->m_cBoxControl->m_elBoxMoveToLT01Step = BML_BO01_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_BOX_UNLOADING()//STEP_BOX_UNLOADING
{
	g_cpMainDlg->m_elBoxSequence = BOX_MOVE_F_LT01_T_BT01;
	g_cpMainDlg->m_cBoxControl->m_elBoxUnloadingStep = BU_TM01_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_PP_MOVE_TO_TM02()//STEP_PP_MOVE_TO_TM02
{
	g_cpMainDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
	g_cpMainDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_PP_MOVE_TO_UT01()//STEP_PP_MOVE_TO_UT01
{
	g_cpMainDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_UT01;
	g_cpMainDlg->m_cPickerControl->m_elPPMove_TM02_UT01 = PPU_TM02_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_PP_MOVE_TO_BT01()//STEP_PP_MOVE_TO_BT01
{
	g_cpMainDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_BT01;
	g_cpMainDlg->m_cPickerControl->m_elPPMove_TM02_BT01 = PPB_TM02_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_ST_MOVE_TO_TM02()//STEP_ST_MOVE_TO_TM02
{
	g_cpMainDlg->m_elPickerSequence = STICK_MOVE_F_LT01_T_TM02;
	g_cpMainDlg->m_cPickerControl->m_elSTMove_LT01_TM02 = STLT_TM02_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_ST_MOVE_TO_TT01()//STEP_ST_MOVE_TO_TT01
{
	g_cpMainDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_TT01;
	g_cpMainDlg->m_cPickerControl->m_elSTMove_TM02_TT01 = STTT_TM02_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_ST_MOVE_TO_AM01()//STEP_ST_MOVE_TO_AM01
{
	g_cpMainDlg->m_elTurnTableSequence = STICK_MOVE_F_TT01_T_AM01;
	g_cpMainDlg->m_cPickerControl->m_elSTMove_TT01_AM01 = STTA_TT01_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_ST_MOVE_TO_UT02()//STEP_ST_MOVE_TO_UT02
{
	g_cpMainDlg->m_elTurnTableSequence = STICK_MOVE_F_AM01_T_UT02;
	g_cpMainDlg->m_cPickerControl->m_elSTMove_AM01_UT02 = STAU_RCV_STICK_RESULT;
}



void CMainControl::OnBnClicked_STEP_ST_MOVE_TO_TM02_2()//STEP_ST_MOVE_TO_TM02_2
{
	g_cpMainDlg->m_elPickerSequence = STICK_MOVE_F_UT02_T_TM02;
	g_cpMainDlg->m_cPickerControl->m_elSTMove_UT02_TM02 = STUT_TT01_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_ST_MOVE_TO_UT01()//STEP_ST_MOVE_TO_UT01
{
	g_cpMainDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_UT01;
	g_cpMainDlg->m_cPickerControl->m_elSTMove_TM02_UT01 = STTU_TM02_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_ST_MOVE_TO_BT01()//STEP_ST_MOVE_TO_BT01
{
	g_cpMainDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_BT01;
	g_cpMainDlg->m_cPickerControl->m_elSTMove_TM02_BT01 = STTB_TM02_INTERLOCK_CHECK;
}


void CMainControl::OnBnClicked_STEP_ST_CHECK_TO_UT02()
{
	g_cpMainDlg->m_elTurnTableSequence = STICK_MOVE_F_TT01_T_UT02;
	g_cpMainDlg->m_cPickerControl->m_elSTMove_TT01_UT02 = STU_TT01_INTERLOCK_CHECK;
}


void CMainControl::OnBnClickedBtnCapture()
{
	g_cpMainDlg->m_cCameraControl.m_fnImageSave();
	m_fnLogDisplay("Image Save to D:\\nBiz_InspectStock\\CamImage\\", TEXT_NORMAL_BLACK);
}


void CMainControl::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{	
	//CVS11IndexerDlg*  vpMainDlg = (CVS11IndexerDlg*)AfxGetApp()->m_pMainWnd;
	g_cpMainDlg->m_fnAppendList(strLog, nColorType);
}


void CMainControl::OnBnClickedBtnBoxIndex7()
{
	for (int nStep = 0; nStep < BOX7; nStep++)
	{		
		m_ctrBtnBoxIndex[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	g_cpMainDlg->m_cBoxControl->m_fnSet_Current_Box_Index((UINT)BOX7);

	m_ctrBtnBoxIndex[BOX7 - 1].SetRoundButtonStyle(&m_tStyle1);
}


void CMainControl::OnBnClickedBtnBoxIndex6()
{
	for (int nStep = 0; nStep < BOX7; nStep++)
	{
		m_ctrBtnBoxIndex[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	g_cpMainDlg->m_cBoxControl->m_fnSet_Current_Box_Index((UINT)BOX6);

	m_ctrBtnBoxIndex[BOX6 - 1].SetRoundButtonStyle(&m_tStyle1);
}


void CMainControl::OnBnClickedBtnBoxIndex5()
{
	for (int nStep = 0; nStep < BOX7; nStep++)
	{
		m_ctrBtnBoxIndex[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	g_cpMainDlg->m_cBoxControl->m_fnSet_Current_Box_Index((UINT)BOX5);

	m_ctrBtnBoxIndex[BOX5 - 1].SetRoundButtonStyle(&m_tStyle1);
}


void CMainControl::OnBnClickedBtnBoxIndex4()
{
	for (int nStep = 0; nStep < BOX7; nStep++)
	{
		m_ctrBtnBoxIndex[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	g_cpMainDlg->m_cBoxControl->m_fnSet_Current_Box_Index((UINT)BOX4);

	m_ctrBtnBoxIndex[BOX4 - 1].SetRoundButtonStyle(&m_tStyle1);
}


void CMainControl::OnBnClickedBtnBoxIndex3()
{
	for (int nStep = 0; nStep < BOX7; nStep++)
	{
		m_ctrBtnBoxIndex[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	g_cpMainDlg->m_cBoxControl->m_fnSet_Current_Box_Index((UINT)BOX3);

	m_ctrBtnBoxIndex[BOX3 - 1].SetRoundButtonStyle(&m_tStyle1);
}


void CMainControl::OnBnClickedBtnBoxIndex2()
{
	for (int nStep = 0; nStep < BOX7; nStep++)
	{
		m_ctrBtnBoxIndex[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	g_cpMainDlg->m_cBoxControl->m_fnSet_Current_Box_Index((UINT)BOX2);

	m_ctrBtnBoxIndex[BOX2 - 1].SetRoundButtonStyle(&m_tStyle1);
}


void CMainControl::OnBnClickedBtnBoxIndex1()
{
	for (int nStep = 0; nStep < BOX7; nStep++)
	{
		m_ctrBtnBoxIndex[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	g_cpMainDlg->m_cBoxControl->m_fnSet_Current_Box_Index((UINT)BOX1);

	m_ctrBtnBoxIndex[BOX1 - 1].SetRoundButtonStyle(&m_tStyle1);
}


//g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;