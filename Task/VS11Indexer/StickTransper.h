#pragma once
#include "afxwin.h"
#include "afxcmn.h"

// CStickTransper 대화 상자입니다.

class CStickTransper : public CDialogEx
{
	DECLARE_DYNAMIC(CStickTransper)

public:
	CStickTransper(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStickTransper();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_TM02 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	int m_fnCreateControlFont(void);
	int m_fnSetParameter(STM02_POSITION* stPointer);
	int m_fnDisplayParameter(void);
	void m_fnCreateButton(void);
	afx_msg void OnBnClickedBtnSave();
	afx_msg void OnBnClickedBtnCancel();
	
	void m_fnTM02DriveMove(int nPosValue);
	void m_fnTM02UpDnMove(int nPosValue, int nSpeedMode = Speed_Normal);
	void m_fnTM02ThetaMove(int nPosValue);
	void m_fnTM02ShiftMove(int nPosValue);
	void m_fnTM02PositionMove(int nPosValue, int nMotionType, int nTimeOut, int nSpeedMode = Speed_Normal);
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);

	afx_msg void OnBnClickedBtnPosMove();
	afx_msg void OnBnClickedBtnPosDn();
	afx_msg void OnBnClickedBtnPosUp();
	afx_msg void OnBnClickedBtnPosMoveT();
	afx_msg void OnBnClickedBtnPosDnT();
	afx_msg void OnBnClickedBtnPosUpT();
	afx_msg void OnBnClickedBtnPosMoveS();
	afx_msg void OnBnClickedBtnPosDnS();
	afx_msg void OnBnClickedBtnPosUpS();
	afx_msg void OnBnClickedBtnPosMoveZ();
	afx_msg void OnBnClickedBtnPosDnZ();
	afx_msg void OnBnClickedBtnPosUpZ();
	afx_msg void OnBnClickedBtnPosMoveZ2();
	afx_msg void OnBnClickedBtnPosDnZ2();
	afx_msg void OnBnClickedBtnPosUpZ2();
	afx_msg void OnBnClickedBtnPaperCylUp();
	afx_msg void OnBnClickedBtnPaperCylDn();
	afx_msg void OnBnClickedBtnPaperCylUp2();
	afx_msg void OnBnClickedBtnPaperCylDn2();
	afx_msg void OnBnClickedBtnStickVacOn();
	afx_msg void OnBnClickedBtnStickVacOff();
	void m_fnStickVacuum(AIR_SELECTION nAirSelect, int nActType);

	afx_msg void OnBnClickedBtnStickPurgeOn();
	void m_fnStickPurgeOn(AIR_SELECTION nAirSelect, int nPurgeTime);

	afx_msg void OnBnClickedBtnPaperVacOn();
	afx_msg void OnBnClickedBtnPaperVacOff();
	void m_fnPaperVacuum(AIR_SELECTION nAirSelect, int nActType);
	afx_msg void OnBnClickedBtnPaperPurgeOn();
	void m_fnPaperPurgeOn(AIR_SELECTION nAirSelect, int nPurgeTime);

	afx_msg void OnBnClickedBtnReadQr();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	afx_msg void OnBnClickedBtnLight();
	afx_msg void OnBnClickedBtnMotionStop();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	void m_fnCheckStickAirStep();
	void m_fnCheckPaperAirStep();

	int m_fn_TM02_Drive_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_UpDn_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_Theta_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_Shift_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_LR_PickerUp_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_LR_PickerDn_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_CT_PickerUp_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_CT_PickerDn_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_ST_VacOn_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_ST_VacOff_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_ST_Purge_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_PP_VacOn_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_PP_VacOff_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_PP_Purge_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_ReadQR_Return(VS24MotData stReturn_Mess);
	int m_fn_TM02_LightSet_Return(VS24MotData stReturn_Mess);
	void m_fnTM02ActionStop(int nMotionType, int nTimeOut);
	bool m_fnTM02InterlockCheck(int AxisNum, long newPosition);
	void m_fnDisplaySignal();
	void m_fnAlignLightSet(int nLightValue);
public:
	//CInterServerInterface*  g_cpServerInterface;
	COLORREF	m_btBackColor;
	CFont m_EditFont;
	STM02_POSITION* m_st_TM02_Pos;
	STT01_POSITION* m_st_TT01_Pos;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;	
	CRoundButtonStyle	m_tStyleOn;	
	CRoundButtonStyle	m_tStyleOff;	

	CRoundButton2 m_ctrBtnMove;
	CRoundButton2 m_ctrBtnMoveDn;
	CRoundButton2 m_ctrBtnMoveUp;
	CRoundButton2 m_ctrBtnValueSave;
	CRoundButton2 m_ctrBtnSaveCancel;
	CRoundButton2 m_ctrBtnMoveDefault;
	CRoundButton2 m_ctrBtnMoveT;
	CRoundButton2 m_ctrBtnMoveDnT;
	CRoundButton2 m_ctrBtnMoveUpT;
	CRoundButton2 m_ctrBtnMoveS;
	CRoundButton2 m_ctrBtnMoveDnS;
	CRoundButton2 m_ctrBtnMoveUpS;
	CRoundButton2 m_ctrBtnMoveSZ;
	CRoundButton2 m_ctrBtnMoveDnSZ;
	CRoundButton2 m_ctrBtnMoveUpSZ;
	CRoundButton2 m_ctrBtnMovePZ;
	CRoundButton2 m_ctrBtnMoveDnPZ;
	CRoundButton2 m_ctrBtnMoveUpPZ;
	CRoundButton2 m_ctrBtnVacOnS;
	CRoundButton2 m_ctrBtnVacOffS;
	CRoundButton2 m_ctrBtnPurgeS;
	CRoundButton2 m_ctrBtnVacOnP;
	CRoundButton2 m_ctrBtnVacOffP;
	CRoundButton2 m_ctrBtnPurgeP;
	CRoundButton2 m_ctrBtnQRCode;
	CRoundButton2 m_ctrBtnQRCodeSet;
	CRoundButton2 m_ctrBtnCylUpLR;
	CRoundButton2 m_ctrBtnCylDnLR;
	CRoundButton2 m_ctrBtnCylUpMid;
	CRoundButton2 m_ctrBtnCylDnMid;
	CRoundButton2 m_ctrBtnLightSet;
	CRoundButton2 m_ctrBtnTM02ActionStop;

	CRoundButton2 m_ctrBtnManualAlign;
	CRoundButton2 m_ctrBtnZThresholdSet;

	VS24MotData m_stTM02_Mess;

	int m_nCtrDriveIndex;
	int m_nCtrThetaIndex;
	int m_nCtrShiftIndex;
	int m_nCtrStickZIndex;
	int m_nCtrPaperZIndex;

	AIR_SELECTION m_stAirStep;

	char m_chStickQRcode[MAX_QRCODE];
	
	CSliderCtrl m_ctrSliderLight1_1;	
	
	
	afx_msg void OnBnClickedBtnSetQr();
	afx_msg void OnBnClickedBtnManualAlign();
	
	afx_msg void OnBnClickedBtnThresholdSet();

};
