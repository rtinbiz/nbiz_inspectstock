#pragma once
#include "afxwin.h"


// CDisplayDlg2 대화 상자입니다.

class CDisplayDlg2 : public CDialogEx
{
	DECLARE_DYNAMIC(CDisplayDlg2)

public:
	CDisplayDlg2(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDisplayDlg2();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_RC2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	int m_fnCreateButton(void);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void m_fnDisplaySignal();

	void m_fnDisplayPosition_TM02_Drive(TM02_SIGNAL_STEP step);
	void m_fnDisplayPosition_TM02_UpDn(TM02_SIGNAL_STEP step);
	void m_fnDisplayPosition_TM02_Shift(TM02_SIGNAL_STEP step);
	void m_fnDisplayPosition_TM02_Tilt(TM02_SIGNAL_STEP step);
	void m_fnDisplayPosition_TT01_Drive(TT01_SIGNAL_STEP step);
	void m_fnDisplayPosition_TT01_Rotate(TT01_SIGNAL_STEP step);
	void m_fnDisplayPosition_TT01_Tilt(TT01_SIGNAL_STEP step);


	void m_fnDisplayAllOn();
	void m_fnDisplayAllOff();
public:
	COLORREF	m_btBackColor;
	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyleOn;	
	CRoundButtonStyle	m_tStyleOff;
	CRoundButton2 m_ctrBtn_TM02[TM02_SIGNAL_COUNT];
	CRoundButton2 m_ctrBtn_TT01[TT01_SIGNAL_COUNT];
	CRoundButton2 m_ctrBtn_UT02[UT02_SIGNAL_COUNT];
	CRoundButton2 m_ctrBtn_AM01[AM01_SIGNAL_COUNT];
	
};
