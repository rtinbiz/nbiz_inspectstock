#ifndef _GLOBALDEFINE_H_
#define _GLOBALDEFINE_H_

#define IMAGE_BACK_GROUND "D:\\nBiz_InspectStock\\Data\\Resource\\VS11Indexer\\BackGround.PNG"
#define SYSTEM_PARAM_INI_PATH "D:\\nBiz_InspectStock\\Data\\Task_INI\\VS11Indexer_Sys_Param.ini"
#define TM02_RECIPE_INI_PATH "D:\\nBiz_InspectStock\\Data\\TeachingRecipe\\"

#define  WM_USER_ACC_CODE_UPDATE			 (WM_USER + 1040)
#define  WM_USER_ACC_PAPER_DETECT			 WM_USER_ACC_CODE_UPDATE + 1
#define  WM_USER_ACC_STICK_DETECT			 WM_USER_ACC_PAPER_DETECT + 1
#define  WM_USER_ACC_STICK_ALIGN			 WM_USER_ACC_STICK_DETECT + 1

#define  SECTION_BT01_POS_DATA		"BT01_POS_DATA"
#define  KEY_BAD_POS				"BAD_POS"
#define  KEY_POS1					"POS1"
#define  KEY_POS2					"POS2"
#define  KEY_POS3					"POS3"
#define  KEY_POS4					"POS4"
#define  KEY_POS5					"POS5"
#define  KEY_POS6					"POS6"
#define  KEY_POS7					"POS7"
#define  KEY_READY_POS				"READY_POS"

#define  SECTION_TM01_POS_DATA		"TM01_POS_DATA"
#define  KEY_FORK_BT01				"FORK_BT01"
#define  KEY_FORK_LT01				"FORK_LT01"
#define	 KEY_TM01_READY				"TM01_READY"
#define	 KEY_TM01_BCR_READ			"TM01_BCR_READ"
#define  KEY_PURGE_TIME				"PURGE_TIME"

#define  SECTION_LT01_POS_DATA		"LT01_POS_DATA"
#define  SECTION_UT01_POS_DATA		"UT01_POS_DATA"
#define  KEY_GUIDE_SMALL			"GUIDE_SMALL"
#define  KEY_GUIDE_MID				"GUIDE_MID"
#define  KEY_GUIDE_LARGE			"GUIDE_LARGE"
#define  KEY_GUIDE_READY			"GUIDE_READY"
#define  KEY_PURGE_TIME				"PURGE_TIME"

#define  SECTION_BO01_POS_DATA		"BO01_POS_DATA"
#define  KEY_DRIVE_LT01_SMALL		"DRIVE_LT01_SMALL"
#define  KEY_DRIVE_UT01_SMALL		"DRIVE_UT01_SMALL"
#define  KEY_DRIVE_LT01_MID			"DRIVE_LT01_MID"
#define  KEY_DRIVE_UT01_MID			"DRIVE_UT01_MID"
#define  KEY_DRIVE_LT01_LARGE		"DRIVE_LT01_LARGE"
#define  KEY_DRIVE_UT01_LARGE		"DRIVE_UT01_LARGE"
#define  KEY_Z_READY_POS			"Z_READY_POS"
#define  KEY_Z_LT01_UP_COVER		"Z_LT01_UP_COVER"
#define  KEY_Z_LT01_DN_COVER		"Z_LT01_DN_COVER"
#define  KEY_Z_UT01_UP_COVER		"Z_UT01_UP_COVER"
#define  KEY_Z_UT01_DN_COVER		"Z_UT01_DN_COVER"
#define  KEY_R_PAD_UP				"R_PAD_UP"
#define  KEY_R_PAD_DN				"R_PAD_DN"
#define  KEY_PURGE_TIME				"PURGE_TIME"

#define  SECTION_TM02_POS_DATA		"TM02_POS_DATA"
#define  KEY_DRIVE_POS_BT01			"DRIVE_POS_BT01"
#define  KEY_DRIVE_POS_BT01_PP		"DRIVE_POS_BT01_PP"
#define  KEY_DRIVE_POS_LT01			"DRIVE_POS_LT01"
#define  KEY_DRIVE_POS_UT01			"DRIVE_POS_UT01"
#define  KEY_DRIVE_POS_UT01_PP		"DRIVE_POS_UT01_PP"
#define  KEY_DRIVE_POS_TT01			"DRIVE_POS_TT01"
#define  KEY_DRIVE_POS_UT02			"DRIVE_POS_UT02"
#define  KEY_DRIVE_POS_QR			"DRIVE_POS_QR"
#define  KEY_DRIVE_POS_ALIGN		"DRIVE_POS_ALIGN"
#define  KEY_T_POS_BT01				"T_POS_BT01"
#define  KEY_T_POS_LT01				"T_POS_LT01"
#define  KEY_T_POS_UT01				"T_POS_UT01"
#define  KEY_T_POS_TT01				"T_POS_TT01"
#define  KEY_T_POS_UT02				"T_POS_UT02"
#define  KEY_T_POS_QR				"T_POS_QR"
#define  KEY_SHIFT_POS_BT01			"SHIFT_POS_BT01"
#define  KEY_SHIFT_POS_LT01			"SHIFT_POS_LT01"
#define  KEY_SHIFT_POS_UT01			"SHIFT_POS_UT01"
#define  KEY_SHIFT_POS_TT01			"SHIFT_POS_TT01"
#define  KEY_SHIFT_POS_UT02			"SHIFT_POS_UT02"
#define  KEY_SHIFT_POS_QR			"SHIFT_POS_QR"
#define  KEY_STICK_Z_POS_BT01		"STICK_Z_POS_BT01"
#define  KEY_STICK_Z_POS_LT01		"STICK_Z_POS_LT01"
#define  KEY_STICK_Z_POS_UT01		"STICK_Z_POS_UT01"
#define  KEY_STICK_Z_POS_TT01		"STICK_Z_POS_TT01"
#define  KEY_STICK_Z_POS_UT02		"STICK_Z_POS_UT02"
#define  KEY_STICK_Z_POS_QR			"STICK_Z_POS_QR"
#define  KEY_STICK_Z_POS_READY		"STICK_Z_POS_READY"
#define  KEY_STICK_Z_POS_ALIGN		"STICK_Z_POS_ALIGN"
#define  KEY_PAPER_Z_POS_BT01		"PAPER_Z_POS_BT01"
#define  KEY_PAPER_Z_POS_LT01		"PAPER_Z_POS_LT01"
#define  KEY_PAPER_Z_POS_UT01		"PAPER_Z_POS_UT01"
#define  KEY_PURGE_STICK_TIME		"PURGE_STICK_TIME"
#define  KEY_PURGE_PAPER_TIME		"PURGE_PAPER_TIME"

#define  KEY_LIGHT_VALUE1_1			"LIGHT_VALUE1_1"

#define  SECTION_TT01_POS_DATA		"TT01_POS_DATA"
#define  KEY_DRIVE_POS_READY		"DRIVE_POS_READY"
#define  KEY_DRIVE_POS_TURN			"DRIVE_POS_TURN"
#define  KEY_DRIVE_POS_AM01			"DRIVE_POS_AM01"
#define  KEY_ROTATE_SIDE_UP			"ROTATE_SIDE_UP"
#define  KEY_ROTATE_SIDE_DN			"ROTATE_SIDE_DN"
#define  KEY_T_POS_READY			"T_POS_READY"
#define  KEY_UP_PURGE_TIME			"UP_PURGE_TIME"
#define  KEY_DN_PURGE_TIME			"DN_PURGE_TIME"

#define  SECTION_UT02_POS_DATA		"UT02_POS_DATA"
#define  KEY_PURGE_TIME				"PURGE_TIME"

//TM02 Recipe
#define  KEY_TM02_BOX_TYPE			"TM02_BOX_TYPE"
#define  KEY_TM02_STICK_SIZE		"TM02_STICK_SIZE"
#define	 KEY_AM01_LOAD_OFFSET		"AM01_LOAD_OFFSET"
#define  KEY_TM02_LT01_DRIVE_ALIGN	"TM02_LT01_DRIVE_ALIGN"
#define  KEY_TM02_LT01_Z_ALIGN		"TM02_LT01_Z_ALIGN"
#define  KEY_TM02_LT01_DRIVE_ST_GET	"TM02_LT01_DRIVE_ST_GET"
#define  KEY_TM02_LT01_DRIVE_PP_GET	"TM02_LT01_DRIVE_PP_GET"
#define  KEY_TM02_LT01_SHIFT_GET	"TM02_LT01_SHIFT_GET"
#define  KEY_TM02_LT01_TILT_GET		"TM02_LT01_TILT_GET"
#define  KEY_TM02_LT01_Z_PP_GET		"TM02_LT01_Z_PP_GET"
#define  KEY_TM02_LT01_Z_ST_GET		"TM02_LT01_Z_ST_GET"
#define  KEY_TM02_LT01_PP_PURGE_GET	"TM02_LT01_PP_PURGE_GET"
#define  KEY_TM02_LT01_VAC_PP_GET	"TM02_LT01_VAC_PP_GET"
#define  KEY_TM02_LT01_VAC_ST_GET	"TM02_LT01_VAC_ST_GET"

#define  KEY_TM02_Z_READY_POS		"TM02_Z_READY_POS"

#define  KEY_TM02_QR_DRIVE_POS		"TM02_QR_DRIVE_POS"
#define  KEY_TM02_QR_Z_POS			"TM02_QR_Z_POS"

#define  KEY_TM02_TT01_DRIVE_PUT	"TM02_TT01_DRIVE_PUT"
#define  KEY_TM02_TT01_SHIFT_PUT	"TM02_TT01_SHIFT_PUT"
#define  KEY_TM02_TT01_TILT_PUT		"TM02_TT01_TILT_PUT"
#define  KEY_TM02_TT01_Z_ST_PUT		"TM02_TT01_Z_ST_PUT"
#define  KEY_TM02_TT01_ST_PURGE_PUT	"TM02_TT01_ST_PURGE_PUT"

#define  KEY_TM02_UT02_DRIVE_GET	"TM02_UT02_DRIVE_GET"
#define  KEY_TM02_UT02_SHIFT_GET	"TM02_UT02_SHIFT_GET"
#define  KEY_TM02_UT02_TILT_GET		"TM02_UT02_TILT_GET"
#define  KEY_TM02_UT02_Z_ST_GET		"TM02_UT02_Z_ST_GET"
#define  KEY_TM02_UT02_VAC_GET		"TM02_UT02_VAC_GET"

#define  KEY_TM02_UT01_DRIVE_PP_PUT	"TM02_UT01_DRIVE_PP_PUT"
#define  KEY_TM02_UT01_DRIVE_ST_PUT	"TM02_UT01_DRIVE_ST_PUT"
#define  KEY_TM02_UT01_SHIFT_PUT	"TM02_UT01_SHIFT_PUT"
#define  KEY_TM02_UT01_TILT_PUT		"TM02_UT01_TILT_PUT"
#define  KEY_TM02_UT01_Z_PP_PUT		"TM02_UT01_Z_PP_PUT"
#define  KEY_TM02_UT01_Z_ST_PUT		"TM02_UT01_Z_ST_PUT"
#define  KEY_TM02_UT01_PURGE_PUT	"TM02_UT01_PURGE_PUT"

#define  KEY_TM02_BT01_DRIVE_PP_PUT	"TM02_BT01_DRIVE_PP_PUT"
#define  KEY_TM02_BT01_DRIVE_ST_PUT	"TM02_BT01_DRIVE_ST_PUT"
#define  KEY_TM02_BT01_SHIFT_PUT	"TM02_BT01_SHIFT_PUT"
#define  KEY_TM02_BT01_TILT_PUT		"TM02_BT01_TILT_PUT"
#define  KEY_TM02_BT01_Z_PP_PUT		"TM02_BT01_Z_PP_PUT"
#define  KEY_TM02_BT01_Z_ST_PUT		"TM02_BT01_Z_ST_PUT"
#define  KEY_TM02_BT01_PURGE_PUT	"TM02_BT01_PURGE_PUT"

#define  KEY_TT01_VAC_GET			"TT01_VAC_GET"
#define  KEY_UT02_VAC_GET			"UT02_VAC_GET"
//TM02 Recipe




#define Interlock_PARAM_INI_PATH "D:\\nBiz_InspectStock\\Data\\Task_INI\\Interlock_Sys_Param.ini"

//Indexer Interlock 
#define  Section_BT01		"BT01"
#define  Key_Table_UpDn_Min		"Table_UpDn_Min"
#define  Key_Table_UpDn_Max		"Table_UpDn_Max"
#define  Key_UpDn_SafeZone_TM01_Min		"UpDn_SafeZone_TM01_Min"
#define  Key_UpDn_SafeZone_TM01_Max		"UpDn_SafeZone_TM01_Max"

#define  Section_TM01		"TM01"
#define  Key_Fork_Drive_Min		"Fork_Drive_Min"
#define  Key_Fork_Drive_Max		"Fork_Drive_Max"

#define  Section_LT01		"LT01"
#define  Key_Pusher_Min		"Pusher_Min"
#define  Key_Pusher_Max		"Pusher_Max"

#define  Section_BO01		"BO01"
#define  Key_Drive_Min			"Drive_Min"
#define  Key_Drive_Max			"Drive_Max"
#define  Key_UpDn_Min 			"UpDn_Min"
#define  Key_UpDn_Max 			"UpDn_Max"
#define  Key_Rotate_Min 		"Rotate_Min"
#define  Key_Rotate_Max 		"Rotate_Max"
#define  Key_UpDn_SafeZone_One_Min 		"UpDn_SafeZone_One_Min"
#define  Key_UpDn_SafeZone_One_Max 		"UpDn_SafeZone_One_Max"
#define  Key_UpDn_SafeZone_Two_Min 		"UpDn_SafeZone_Two_Min"
#define  Key_UpDn_SafeZone_Two_Max 		"UpDn_SafeZone_Two_Max"
#define  Key_Drive_SafeZone_Min 		"Drive_SafeZone_Min"
#define  Key_Drive_SafeZone_Max 		"Drive_SafeZone_Max"
#define  Key_Rotate_SafeZone_Min 		"Rotate_SafeZone_Min"
#define  Key_Rotate_SafeZone_Max 		"Rotate_SafeZone_Max"

#define  Section_UT01		"UT01"
#define  Key_Pusher_Min		"Pusher_Min"
#define  Key_Pusher_Max		"Pusher_Max"

#define  Section_TM02		"TM02"
#define  Key_Drive_Min		"Drive_Min"
#define  Key_Drive_Max		"Drive_Max"
#define  Key_UpDn_Min 		"UpDn_Min"
#define  Key_UpDn_Max 		"UpDn_Max"
#define  Key_Tilt_Min 		"Tilt_Min"
#define  Key_Tilt_Max 		"Tilt_Max"
#define  Key_Shift_Min 		"Shift_Min"
#define  Key_Shift_Max 		"Shift_Max"
#define  Key_Drive_TT01_Pos_Low 	"Drive_TT01_Pos_Low"
#define  Key_Drive_TT01_Pos_High 	"Drive_TT01_Pos_High"
#define  Key_UpDn_TT01_Max 		"UpDn_TT01_Max"
#define  Key_Drive_UT02_Pos_Low 	"Drive_UT02_Pos_Low"
#define  Key_Drive_UT02_Pos_High 	"Drive_UT02_Pos_High"
#define  Key_Turn_Table_Safe_Pos 	"TurnTable_Safe_Pos"
#define  Key_UpDn_UT02_Max 		"UpDn_UT02_Max"
#define  Key_Drive_UT01_Pos_Low 	"Drive_UT01_Pos_Low"
#define  Key_Drive_UT01_Pos_High 	"Drive_UT01_Pos_High"
#define  Key_UpDn_UT01_Max 		"UpDn_UT01_Max"
#define  Key_Drive_LT01_Pos_Low 	"Drive_LT01_Pos_Low"
#define  Key_Drive_LT01_Pos_High 	"Drive_LT01_Pos_High"
#define  Key_UpDn_LT01_Max 		"UpDn_LT01_Max"
#define  Key_Drive_BT01_Pos_Low 	"Drive_BT01_Pos_Low"
#define  Key_Drive_BT01_Pos_High 	"Drive_BT01_Pos_High"
#define  Key_UpDn_BT01_Max 		"UpDn_BT01_Max"

#define  Section_TT01		"TT01"
#define  Key_Drive_Min			"Drive_Min"
#define  Key_Drive_Max			"Drive_Max"
#define  Key_Rotate_Min 		"Rotate_Min"
#define  Key_Rotate_Max 		"Rotate_Max"
#define  Key_Tilt_Min 			"Tilt_Min"
#define  Key_Tilt_Max 			"Tilt_Max"
//Indexer Interlock 

#endif

