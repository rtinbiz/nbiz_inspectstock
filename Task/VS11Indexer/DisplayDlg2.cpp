// DisplayDlg2.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "DisplayDlg2.h"
#include "afxdialogex.h"


// CDisplayDlg2 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDisplayDlg2, CDialogEx)

CDisplayDlg2::CDisplayDlg2(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDisplayDlg2::IDD, pParent)
{

}

CDisplayDlg2::~CDisplayDlg2()
{
}

void CDisplayDlg2::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_TM02_BOX_ALIGN, m_ctrBtn_TM02[TM02_BOX_ALIGN]);
	DDX_Control(pDX, IDC_BTN_TM02_STICK_LOADING, m_ctrBtn_TM02[TM02_STICK_LOAD]);
	DDX_Control(pDX, IDC_BTN_TM02_STICK_UNLOADING, m_ctrBtn_TM02[TM02_STICK_UNLOAD]);
	DDX_Control(pDX, IDC_BTN_TM02_PAPER_MOVE, m_ctrBtn_TM02[TM02_PAPER_MOVE]);
	DDX_Control(pDX, IDC_BTN_TM02_POS_BT01, m_ctrBtn_TM02[TM02_POS_BT01]);
	DDX_Control(pDX, IDC_BTN_TM02_POS_LT01, m_ctrBtn_TM02[TM02_POS_LT01]);
	DDX_Control(pDX, IDC_BTN_TM02_POS_UT01, m_ctrBtn_TM02[TM02_POS_UT01]);
	DDX_Control(pDX, IDC_BTN_TM02_POS_QR, m_ctrBtn_TM02[TM02_POS_QR]);
	DDX_Control(pDX, IDC_BTN_TM02_POS_TT01, m_ctrBtn_TM02[TM02_POS_TT01]);
	DDX_Control(pDX, IDC_BTN_TM02_POS_UT02, m_ctrBtn_TM02[TM02_POS_UT02]);
	DDX_Control(pDX, IDC_BTN_TM02_ZPOS_UP, m_ctrBtn_TM02[TM02_POS_Z_UP]);
	DDX_Control(pDX, IDC_BTN_TM02_ZPOS_DN, m_ctrBtn_TM02[TM02_POS_Z_DN]);
	DDX_Control(pDX, IDC_BTN_TM02_ZPOS_QR, m_ctrBtn_TM02[TM02_POS_Z_QR]);
	DDX_Control(pDX, IDC_BTN_TM02_ZPOS_ALIGN, m_ctrBtn_TM02[TM02_POS_ALIGN]);
	DDX_Control(pDX, IDC_BTN_TM02_LEFT_IN_STICK_DETECT, m_ctrBtn_TM02[TM02_LEFT_IN_STICK]);
	DDX_Control(pDX, IDC_BTN_TM02_RIGHT_IN_STICK_DETECT, m_ctrBtn_TM02[TM02_RIGHT_IN_STICK]);
	DDX_Control(pDX, IDC_BTN_TM02_LEFT_OUT_STICK_DETECT, m_ctrBtn_TM02[TM02_LEFT_OUT_STICK]);
	DDX_Control(pDX, IDC_BTN_TM02_RIGHT_OUT_STICK_DETECT, m_ctrBtn_TM02[TM02_RIGHT_OUT_STICK]);
	DDX_Control(pDX, IDC_BTN_TM02_LEFT_IN_PAPER_DETECT, m_ctrBtn_TM02[TM02_LEFT_IN_PAPER]);
	DDX_Control(pDX, IDC_BTN_TM02_RIGHT_IN_PAPER_DETECT, m_ctrBtn_TM02[TM02_RIGHT_IN_PAPER]);
	DDX_Control(pDX, IDC_BTN_TM02_LEFT_OUT_PAPER_DETECT, m_ctrBtn_TM02[TM02_LEFT_OUT_PAPER]);
	DDX_Control(pDX, IDC_BTN_TM02_RIGHT_OUT_PAPER_DETECT, m_ctrBtn_TM02[TM02_RIGHT_OUT_PAPER]);
	DDX_Control(pDX, IDC_BTN_TM02_LEFT_CYL_UP, m_ctrBtn_TM02[TM02_LEFT_CYL_UP]);
	DDX_Control(pDX, IDC_BTN_TM02_LEFT_CYL_DN, m_ctrBtn_TM02[TM02_LEFT_CYL_DN]);
	DDX_Control(pDX, IDC_BTN_TM02_RIGHT_CYL_UP, m_ctrBtn_TM02[TM02_RIGHT_CYL_UP]);
	DDX_Control(pDX, IDC_BTN_TM02_RIGHT_CYL_DN, m_ctrBtn_TM02[TM02_RIGHT_CYL_DN]);
	DDX_Control(pDX, IDC_BTN_TM02_CENTER_CYL_UP, m_ctrBtn_TM02[TM02_CENTER_CYL_UP]);
	DDX_Control(pDX, IDC_BTN_TM02_RIGHT_CYL_DN2, m_ctrBtn_TM02[TM02_CENTER_CYL_DN]);
	DDX_Control(pDX, IDC_BTN_TM02_SI_VAC, m_ctrBtn_TM02[TM02_STICK_IN_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_TM02_SO_VAC, m_ctrBtn_TM02[TM02_STICK_OUT_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_TM02_PI_VAC, m_ctrBtn_TM02[TM02_PAPER_IN_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_TM02_PO_VAC, m_ctrBtn_TM02[TM02_PAPER_OUT_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_TM02_THETA, m_ctrBtn_TM02[TM02_THETA_READY_POS]);
	DDX_Control(pDX, IDC_BTN_TM02_SHIFT, m_ctrBtn_TM02[TM02_XAXIS_READY_POS]);
	DDX_Control(pDX, IDC_BTN_TT01_STICK_LOADING, m_ctrBtn_TT01[TT01_STICK_LOAD]);
	DDX_Control(pDX, IDC_BTN_TT01_STICK_UNLOADING, m_ctrBtn_TT01[TT01_STICK_UNLOAD]);
	DDX_Control(pDX, IDC_BTN_TT01_POS_READY, m_ctrBtn_TT01[TT01_POS_READY]);
	DDX_Control(pDX, IDC_BTN_TT01_POS_TURN, m_ctrBtn_TT01[TT01_POS_TURN]);
	DDX_Control(pDX, IDC_BTN_TT01_POS_AM01, m_ctrBtn_TT01[TT01_POS_AM01]);
	DDX_Control(pDX, IDC_BTN_TT01_LEFT_UP_STICK, m_ctrBtn_TT01[TT01_LEFT_UP_STICK]);
	DDX_Control(pDX, IDC_BTN_TT01_RIGHT_UP_STICK, m_ctrBtn_TT01[TT01_RIGHT_UP_STICK]);
	DDX_Control(pDX, IDC_BTN_TT01_LEFT_DN_STICK, m_ctrBtn_TT01[TT01_LEFT_DN_STICK]);
	DDX_Control(pDX, IDC_BTN_TT01_RIGHT_DN_STICK, m_ctrBtn_TT01[TT01_RIGHT_DN_STICK]);
	DDX_Control(pDX, IDC_BTN_TT01_POS_UP, m_ctrBtn_TT01[TT01_POS_UP]);
	DDX_Control(pDX, IDC_BTN_TT01_POS_DN, m_ctrBtn_TT01[TT01_POS_DN]);
	DDX_Control(pDX, IDC_BTN_TT01_THETA, m_ctrBtn_TT01[TT01_THETA_READY]);
	DDX_Control(pDX, IDC_BTN_TT01_UP_VAC, m_ctrBtn_TT01[TT01_UP_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_TT01_DN_VAC, m_ctrBtn_TT01[TT01_DN_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_TT01_CLASH, m_ctrBtn_TT01[TT01_CLASH_SENSOR]);
	DDX_Control(pDX, IDC_BTN_TT01_TURN_SENSOR, m_ctrBtn_TT01[TT01_TURN_POSITION]);
	DDX_Control(pDX, IDC_BTN_UT02_LEFT_STICK, m_ctrBtn_UT02[UT02_LEFT_STICK]);
	DDX_Control(pDX, IDC_BTN_UT02_RIGHT_STICK, m_ctrBtn_UT02[UT02_RIGHT_STICK]);
	DDX_Control(pDX, IDC_BTN_UT02_LCYL_UP, m_ctrBtn_UT02[UT02_LEFT_CYL_UP]);
	DDX_Control(pDX, IDC_BTN_UT02_LCYL_DN, m_ctrBtn_UT02[UT02_LEFT_CYL_DN]);
	DDX_Control(pDX, IDC_BTN_UT02_RCYL_UP, m_ctrBtn_UT02[UT02_RIGHT_CYL_UP]);
	DDX_Control(pDX, IDC_BTN_UT02_RCYL_DN, m_ctrBtn_UT02[UT02_RIGHT_CYL_DN]);
	DDX_Control(pDX, IDC_BTN_UT02_VAC_SENSOR, m_ctrBtn_UT02[UT02_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_AM01_LOADING, m_ctrBtn_AM01[AM01_STICK_LOAD]);
	DDX_Control(pDX, IDC_BTN_AM01_UNLOADING, m_ctrBtn_AM01[AM01_STICK_UNLOAD]);
	DDX_Control(pDX, IDC_BTN_AM01_3D_SCAN, m_ctrBtn_AM01[AM01_WORK_3DSCAN]);
	DDX_Control(pDX, IDC_BTN_AM01_DEFECT, m_ctrBtn_AM01[AM01_WORK_DEFECT]);
	DDX_Control(pDX, IDC_BTN_AM01_CDTP, m_ctrBtn_AM01[AM01_WORK_CDCHECK]);
	DDX_Control(pDX, IDC_BTN_AM01_SCOPE, m_ctrBtn_AM01[AM01_WORK_3DSCOPE]);
}


BEGIN_MESSAGE_MAP(CDisplayDlg2, CDialogEx)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CDisplayDlg2 메시지 처리기입니다.


void CDisplayDlg2::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
}

BOOL CDisplayDlg2::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);

	m_fnCreateButton();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH CDisplayDlg2::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


int CDisplayDlg2::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style Green
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 12.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;	
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);

	// Get default Style Dark Gray
	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 12.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x20, 0x20, 0x20);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyleOff.SetButtonStyle(&tStyle);
	
	for(int nStep = TM02_BOX_ALIGN; nStep < TM02_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TM02[nStep].SetRoundButtonStyle(&m_tStyleOff);		
	}	

	for(int nStep = TT01_STICK_LOAD; nStep < TT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TT01[nStep].SetRoundButtonStyle(&m_tStyleOff);		
	}	

	for(int nStep = UT02_LEFT_STICK; nStep < UT02_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_UT02[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}	

	for(int nStep = AM01_STICK_LOAD; nStep < AM01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_AM01[nStep].SetRoundButtonStyle(&m_tStyleOff);		
	}

	return 0;
}

BOOL CDisplayDlg2::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDisplayDlg2::m_fnDisplaySignal()
{
	//TM02
	if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerFrontStickVacuum == ON)
		m_ctrBtn_TM02[TM02_STICK_IN_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_STICK_IN_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);	

	if (G_STMACHINE_STATUS.stPickerSensor.In10_Sn_PickerBackStickVacuum == ON)
		m_ctrBtn_TM02[TM02_STICK_OUT_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_STICK_OUT_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In09_Sn_PickerFrontPaperVacuum == ON)
		m_ctrBtn_TM02[TM02_PAPER_IN_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_PAPER_IN_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In11_Sn_PickerBackPaperVacuum == ON)
		m_ctrBtn_TM02[TM02_PAPER_OUT_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_PAPER_OUT_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In01_Sn_PickerLeftInputStickDetect == ON)
		m_ctrBtn_TM02[TM02_LEFT_IN_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_LEFT_IN_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In02_Sn_PickerLeftInputPaperDetect == ON)
		m_ctrBtn_TM02[TM02_LEFT_IN_PAPER].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_LEFT_IN_PAPER].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In03_Sn_PickerLeftOutputStickDetect == ON)
		m_ctrBtn_TM02[TM02_LEFT_OUT_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_LEFT_OUT_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In04_Sn_PickerLeftOutputPaperDetect == ON)
		m_ctrBtn_TM02[TM02_LEFT_OUT_PAPER].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_LEFT_OUT_PAPER].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In05_Sn_PickerRightInputStickDetect == ON)
		m_ctrBtn_TM02[TM02_RIGHT_IN_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_RIGHT_IN_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In06_Sn_PickerRightInputPaperDetect == ON)
		m_ctrBtn_TM02[TM02_RIGHT_IN_PAPER].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_RIGHT_IN_PAPER].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In07_Sn_PickerRightOutputStickDetect == ON)
		m_ctrBtn_TM02[TM02_RIGHT_OUT_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_RIGHT_OUT_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerSensor.In08_Sn_PickerRightOutputPaperDetect == ON)
		m_ctrBtn_TM02[TM02_RIGHT_OUT_PAPER].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_RIGHT_OUT_PAPER].SetRoundButtonStyle(&m_tStyleOff);


	if (G_STMACHINE_STATUS.stPickerCylinder.In00_Sn_PickerPaperLeftUp == ON)
		m_ctrBtn_TM02[TM02_LEFT_CYL_UP].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_LEFT_CYL_UP].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerCylinder.In01_Sn_PickerPaperLeftDown == ON)
		m_ctrBtn_TM02[TM02_LEFT_CYL_DN].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_LEFT_CYL_DN].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerCylinder.In02_Sn_PickerPaperRightUp == ON)
		m_ctrBtn_TM02[TM02_RIGHT_CYL_UP].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_RIGHT_CYL_UP].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerCylinder.In03_Sn_PickerPaperRightDown == ON)
		m_ctrBtn_TM02[TM02_RIGHT_CYL_DN].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_RIGHT_CYL_DN].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerCylinder.In04_Sn_PickerPaperCenterUp == ON)
		m_ctrBtn_TM02[TM02_CENTER_CYL_UP].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_CENTER_CYL_UP].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stPickerCylinder.In05_Sn_PickerPaperCenterDown == ON)
		m_ctrBtn_TM02[TM02_CENTER_CYL_DN].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_CENTER_CYL_DN].SetRoundButtonStyle(&m_tStyleOff);

	//TT01  2014/11/11
	if (G_STMACHINE_STATUS.stTurnTableSensor.In00_Sn_TurnTableUpperVacuum == ON)
		m_ctrBtn_TT01[TT01_UP_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_UP_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stTurnTableSensor.In07_Sn_TurnTableDownVacuum == ON)
		m_ctrBtn_TT01[TT01_DN_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_DN_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stTurnTableSensor.In01_Sn_TurnTableUpperLeftStickDetect == ON)
		m_ctrBtn_TT01[TT01_LEFT_UP_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_LEFT_UP_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stTurnTableSensor.In02_Sn_TurnTableUpperRightStickDetect == ON)
		m_ctrBtn_TT01[TT01_RIGHT_UP_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_RIGHT_UP_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stTurnTableSensor.In03_Sn_TurnTableLowerLeftStickDetect == ON)
		m_ctrBtn_TT01[TT01_LEFT_DN_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_LEFT_DN_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stTurnTableSensor.In04_Sn_TurnTableLowerRightStickDetect == ON)
		m_ctrBtn_TT01[TT01_RIGHT_DN_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_RIGHT_DN_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stTurnTableSensor.In05_Sn_TurnTableUpperPosition == ON)
		m_ctrBtn_TT01[TT01_POS_UP].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_POS_UP].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stTurnTableSensor.In06_Sn_TurnTableLowerPosition == ON)
		m_ctrBtn_TT01[TT01_POS_DN].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_POS_DN].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stTurnTableSensor.In10_Sn_ClashPrevent_TurnTableAndCCD == ON)
		m_ctrBtn_TT01[TT01_CLASH_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_CLASH_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stEmissionTable.In10_Sn_TurnTableRotateEnablePos == ON)
 		m_ctrBtn_TT01[TT01_TURN_POSITION].SetRoundButtonStyle(&m_tStyleOn);
 	else
 		m_ctrBtn_TT01[TT01_TURN_POSITION].SetRoundButtonStyle(&m_tStyleOff);

	
	//UT02
	if (G_STMACHINE_STATUS.stEmissionTable.In00_Sn_OutputTableLeftStickDetect == ON)
		m_ctrBtn_UT02[UT02_LEFT_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT02[UT02_LEFT_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stEmissionTable.In01_Sn_OutputTableRightStickDetect == ON)
		m_ctrBtn_UT02[UT02_RIGHT_STICK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT02[UT02_RIGHT_STICK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stEmissionTable.In04_Sn_OutputTableVacuum == ON)
		m_ctrBtn_UT02[UT02_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT02[UT02_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stEmissionTable.In02_Sn_OutputLTableCylinderUp == ON)
		m_ctrBtn_UT02[UT02_LEFT_CYL_UP].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT02[UT02_LEFT_CYL_UP].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stEmissionTable.In03_Sn_OutputLTableCylinderDown == ON)
		m_ctrBtn_UT02[UT02_LEFT_CYL_DN].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT02[UT02_LEFT_CYL_DN].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stEmissionTable.In05_Sn_OutputRTableCylinderUp == ON)
		m_ctrBtn_UT02[UT02_RIGHT_CYL_UP].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT02[UT02_RIGHT_CYL_UP].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stEmissionTable.In06_Sn_OutputRTableCylinderDown == ON)
		m_ctrBtn_UT02[UT02_RIGHT_CYL_DN].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT02[UT02_RIGHT_CYL_DN].SetRoundButtonStyle(&m_tStyleOff);

}

void CDisplayDlg2::m_fnDisplayPosition_TM02_Drive(TM02_SIGNAL_STEP step)
{
	for (int nStep = TM02_POS_BT01; nStep <= TM02_POS_UT02; nStep++)
	{
		m_ctrBtn_TM02[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != TM02_SIGNAL_COUNT)
		m_ctrBtn_TM02[step].SetRoundButtonStyle(&m_tStyleOn);
}

void CDisplayDlg2::m_fnDisplayPosition_TM02_UpDn(TM02_SIGNAL_STEP step)
{
	for (int nStep = TM02_POS_Z_UP; nStep <= TM02_POS_ALIGN; nStep++)
	{
		m_ctrBtn_TM02[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != TM02_SIGNAL_COUNT)
		m_ctrBtn_TM02[step].SetRoundButtonStyle(&m_tStyleOn);
}

void CDisplayDlg2::m_fnDisplayPosition_TM02_Shift(TM02_SIGNAL_STEP step)
{
	if (step == TM02_XAXIS_READY_POS)
		m_ctrBtn_TM02[TM02_XAXIS_READY_POS].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_XAXIS_READY_POS].SetRoundButtonStyle(&m_tStyleOff);
}

void CDisplayDlg2::m_fnDisplayPosition_TM02_Tilt(TM02_SIGNAL_STEP step)
{
	if (step == TM02_THETA_READY_POS)
		m_ctrBtn_TM02[TM02_THETA_READY_POS].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM02[TM02_THETA_READY_POS].SetRoundButtonStyle(&m_tStyleOff);
}

void CDisplayDlg2::m_fnDisplayPosition_TT01_Drive(TT01_SIGNAL_STEP step)
{
	for (int nStep = TT01_POS_READY; nStep <= TT01_POS_AM01; nStep++)
	{
		m_ctrBtn_TT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != TT01_SIGNAL_COUNT)
		m_ctrBtn_TT01[step].SetRoundButtonStyle(&m_tStyleOn);

	if (step == TT01_POS_TURN)
		G_SAFE_ZONE[TT01_SAFE_POS] = SAFE;
	else
		G_SAFE_ZONE[TT01_SAFE_POS] = UNSAFE;	
}

void CDisplayDlg2::m_fnDisplayPosition_TT01_Rotate(TT01_SIGNAL_STEP step)
{
	for (int nStep = TT01_POS_UP; nStep <= TT01_POS_DN; nStep++)
	{
		m_ctrBtn_TT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != TT01_SIGNAL_COUNT)
		m_ctrBtn_TT01[step].SetRoundButtonStyle(&m_tStyleOn);
}

void CDisplayDlg2::m_fnDisplayPosition_TT01_Tilt(TT01_SIGNAL_STEP step)
{
	if (step == TT01_THETA_READY)
		m_ctrBtn_TT01[TT01_THETA_READY].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TT01[TT01_THETA_READY].SetRoundButtonStyle(&m_tStyleOff);
}



void CDisplayDlg2::m_fnDisplayAllOn()
{
	for (int nStep = TM02_BOX_ALIGN; nStep < TM02_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TM02[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}

	for (int nStep = TT01_STICK_LOAD; nStep < TT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TT01[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}

	for (int nStep = UT02_LEFT_STICK; nStep < UT02_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_UT02[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}

	for (int nStep = AM01_STICK_LOAD; nStep < AM01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_AM01[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}
	
}

void CDisplayDlg2::m_fnDisplayAllOff()
{
	for (int nStep = TM02_BOX_ALIGN; nStep < TM02_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TM02[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	for (int nStep = TT01_STICK_LOAD; nStep < TT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	for (int nStep = UT02_LEFT_STICK; nStep < UT02_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_UT02[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	for (int nStep = AM01_STICK_LOAD; nStep < AM01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_AM01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}
}