#pragma once
#include "afxwin.h"


// CRecipeDefine 대화 상자입니다.

class CRecipeDefine : public CDialogEx
{
	DECLARE_DYNAMIC(CRecipeDefine)

public:
	CRecipeDefine(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRecipeDefine();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_RECIPE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()	
public:
	virtual void PostNcDestroy();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	void m_fnCreateButton(void);
	int m_fnCreateControlFont(void);
	int m_fnSetParameter(STM02_RECIPE* stPointer);
	afx_msg void OnLbnSelchangeListRecipeList();
	int m_fnDisplayCurRecipe();
	afx_msg void OnBnClickedBtnNew();
	afx_msg void OnBnClickedBtnReplace();
	afx_msg void OnBnClickedBtnSave();
	int m_fnGetRecipeList();
	void m_fnFileSave();
	void m_fnCheckVacAirStep();
	void m_fnGetCurValue();
public:
	COLORREF	m_btBackColor;
	CFont m_EditFont;
	CFont m_EditFont2;

	CRoundButtonStyle m_tButtonStyle;
	CRoundButtonStyle	m_tStyle1;

	CRoundButton2 m_ctrBtnListAdd;
	CRoundButton2 m_ctrBtnListReplace;
	CRoundButton2 m_ctrBtnListSave;
	CRoundButton2 m_ctrBtnListCalc;
	CRoundButton2 m_ctrBtnCancel;

	CListBox m_ctrListBoxRecipe;
	CComboBox m_ctrCboBoxType;
	CComboBox m_ctrCboStickSize;

	STM02_RECIPE* m_st_TM02_Recipe;	  
	STM02_RECIPE m_st_Cur_Recipe;
	
	
	int m_nCurSelIndex;
	afx_msg void OnEnChangeEditCurId();
	
	
	afx_msg void OnBnClickedBtnCancel();
	afx_msg void OnBnClickedBtnCalc();
	
};
