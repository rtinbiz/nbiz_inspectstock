#pragma once



#ifdef CAMERA_USED
#pragma comment( lib , "..//..//CommonDLL64//Mil.lib")
#endif

#include "StdAfx.h"
#include "CameraDefine.h"
#include "SoliosXCL.h"
class CCameraControl
{
public:
	CCameraControl(void);
	~CCameraControl(void);

	void m_fnThreadCreator();
	void m_fnThreadStop(void);	

	void	m_fnSetThreadParam(HWND	hWnd1, HWND hWnd2, HWND hParent);
	static unsigned int __stdcall	THREAD_CAMERA_FRAME_GRAB(LPVOID PosParam);

	void m_fnMilCreate();
	void m_fnMilOpenDevice(int nDevice);
	void m_fnMilFree(void);
	void m_fnMilGetImage(char* ch1, char* ch2);	
	
	void m_fnImageSave();
	void m_fnPaperDetect();
	void m_fnStickDetect();
	void m_fnStickAlign(int Treshold=120);
	void m_fnOffsetClear();

	BOOL m_gnGetPaperDetectResult();
	BOOL m_gnGetStickDetectResult();
	int	m_fnGetShiftOffset();
	int	m_fnGetTiltOffset();
	int	m_fnGetDriveOffset();
public:
	
	STHREAD_CAMERA	sThreadParam;	
		
#ifdef CAMERA_USED
	CSoliosXCL* m_board1;
	CSoliosXCL* m_board2;
#endif
	IplImage* m_iplCam1Image;
	IplImage* m_iplCam2Image;

	BOOL	m_bPaperDetect;
	BOOL	m_bStickDetect;
	int		m_nTM02ShiftOffset;
	int		m_nTM02TiltOffset;
	int		m_nTM02DriveOffset;
	int		m_nAlignThreshold;
};
