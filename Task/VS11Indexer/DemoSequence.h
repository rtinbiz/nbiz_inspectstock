#pragma once
class CDemoSequence
{
public:
	CDemoSequence();
	~CDemoSequence();

	int m_fnSetParameter(SBT01_POSITION* stpointer);

	int m_fnPaperMove_F_LT01_T_UT01();
	int m_fnStickMove_F_LT01_T_UT01();

public:
	EL_DEMO_PP_LT01_UT01 m_elPaperMoveStep;
	EL_DEMO_ST_LT01_UT01 m_elStickMoveStep;

	const SBT01_POSITION* m_st_BT01_Pos;
	//const STM02_POSITION* m_st_TM02_Pos;
};

