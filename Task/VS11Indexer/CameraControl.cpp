#include "StdAfx.h"
#include "CameraControl.h"

CCameraControl::CCameraControl(void)	
{	
	m_iplCam1Image = cvCreateImage( cvSize(REAL_IMAGE_SIZE_X, REAL_IMAGE_SIZE_Y), IPL_DEPTH_8U, 1);	
	m_iplCam2Image = cvCreateImage( cvSize(REAL_IMAGE_SIZE_X, REAL_IMAGE_SIZE_Y), IPL_DEPTH_8U, 1);	
	
	cvSetZero(m_iplCam1Image);
	cvSetZero(m_iplCam2Image);

	m_bPaperDetect = FALSE;
	m_bStickDetect = FALSE;

	m_nTM02ShiftOffset = 0;
	m_nTM02TiltOffset = 0;
	m_nTM02DriveOffset = 0;
	m_nAlignThreshold=0;
#ifdef CAMERA_USED
	m_board1 = NULL;	
	m_board2 = NULL;
#endif
}

CCameraControl::~CCameraControl(void)
{	
	cvReleaseImage(&m_iplCam1Image);
	cvReleaseImage(&m_iplCam2Image);	
}


//Mil Process Start
void CCameraControl::m_fnMilCreate()
{
#ifdef CAMERA_USED
	if ( m_board1 == NULL )
	{
		m_board1 = new CSoliosXCL;	
	}
	if ( m_board2 == NULL )
	{
		m_board2 = new CSoliosXCL;

	}
#endif
}

void CCameraControl::m_fnMilOpenDevice(int nDevice)
{
	CString strDcf;
#ifdef CAMERA_USED
	if ( nDevice == 0 && m_board1 != NULL)
	{
		strDcf = "D:\\nBiz_InspectStock\\DCFs\\XCL_C280C_8bit_2tap_c.dcf";
		m_board1->OpenDevice(0,0,strDcf); // strDcf = _T("C:\\Data\\CIS G20U20A_0.dcf")
	}
	else if ( nDevice == 1 && m_board2 != NULL)
	{
		strDcf = "D:\\nBiz_InspectStock\\DCFs\\XCL_C280C_8bit_2tap_c1.dcf";
		m_board2->OpenDevice(0,1,strDcf); // strDcf = _T("C:\\Data\\CIS G20U20A_1.dcf")
	}
#endif
}


void CCameraControl::m_fnMilGetImage(char* ch1, char* ch2)
{
#ifdef CAMERA_USED
	//if ( nChannel == 0 )
	{
		m_board1->Grab(ch2);
		///m_board1->GetImage(vpGetImage);
	}
	//else
	{
		m_board2->Grab(ch1);
		//m_board2->GetImage(vpGetImage);
	}
#endif
	
}
//Mil Process End

 unsigned int __stdcall CCameraControl::THREAD_CAMERA_FRAME_GRAB(LPVOID pParam)
 {
	STHREAD_CAMERA *pProThreadData = (STHREAD_CAMERA*)pParam;
 	HWND MassageHandle1				= pProThreadData->mUpdateWindHandle1;
 	HWND MassageHandle2				= pProThreadData->mUpdateWindHandle2;
	
 	CCameraControl* cCameraControl = nullptr;
 	cCameraControl = (CCameraControl*)pProThreadData->cObjectPointer;
 
 	CvFont cvDrawFont; 	
 
 	float	hscale      = 0.5f;
 	float	vscale      = 0.5f;
 	float	italicscale = 0.0f;
 	int		thickness   = 1; 
 	int		retValue	=0;
 
 	cvInitFont (&cvDrawFont, CV_FONT_HERSHEY_SIMPLEX , hscale, vscale, italicscale, thickness, CV_AA);
 
 	//////////////////////////////////////////////////////////////////////////
 	CDC	*pDc1;	CDC	*pDc2;
 	HDC phDC1;	HDC phDC2; 
 	phDC1 = ::GetDC(MassageHandle1); 	pDc1 = CDC::FromHandle(phDC1);
 	phDC2 = ::GetDC(MassageHandle2); 	pDc2 = CDC::FromHandle(phDC2);
	
 	CvvImage ViewImage; 

	IplImage* iplGrab1 = cvCreateImage( cvSize(REAL_IMAGE_SIZE_X, REAL_IMAGE_SIZE_Y), IPL_DEPTH_8U, 1);	
	IplImage* iplGrab2 = cvCreateImage( cvSize(REAL_IMAGE_SIZE_X, REAL_IMAGE_SIZE_Y), IPL_DEPTH_8U, 1);		
	cvSetZero(iplGrab1); cvSetZero(iplGrab2);

 	IplImage* iplCam1 = cvCreateImage( cvSize(REAL_IMAGE_SIZE_X, REAL_IMAGE_SIZE_Y), IPL_DEPTH_8U, 3);	
 	IplImage* iplCam2 = cvCreateImage( cvSize(REAL_IMAGE_SIZE_X, REAL_IMAGE_SIZE_Y), IPL_DEPTH_8U, 3);		
 	cvSetZero(iplCam1);	cvSetZero(iplCam2); 	

	IplImage *pGrayAlign = cvCreateImage(cvSize(REAL_IMAGE_SIZE_X, REAL_IMAGE_SIZE_Y), IPL_DEPTH_8U, 1);
	cvSetZero(pGrayAlign);
		
	double dMin = 0.0, dMax = 0.0;
	double dLimit = 0.85;//0.90;
	CvPoint left_top;
	CvPoint LeftStart, LeftEnd;
	CvPoint RightStart, RightEnd;

	IplImage* pMaskLeft = cvCreateImage(cvSize(40, 40), IPL_DEPTH_8U, 1);
	IplImage* pMaskRight = cvCreateImage(cvSize(40, 40), IPL_DEPTH_8U, 1);
	cvSetZero(pMaskLeft); cvSetZero(pMaskRight);

	LeftStart.x = 20; LeftStart.y = 20; LeftEnd.x = 39; LeftEnd.y = 39;
	cvRectangle(pMaskLeft, LeftStart, LeftEnd, cvScalarAll(0xff), -1);
	RightStart.x = 20; RightStart.y = 20; RightEnd.x = 0; RightEnd.y = 39;
	cvRectangle(pMaskRight, RightStart, RightEnd, cvScalarAll(0xff), -1);

	IplImage* pMatchBufferLeft = cvCreateImage(cvSize(REAL_IMAGE_SIZE_X - pMaskLeft->width + 1,
		REAL_IMAGE_SIZE_Y - pMaskLeft->height + 1),	IPL_DEPTH_32F, 1);
	IplImage* pMatchBufferRight = cvCreateImage(cvSize(REAL_IMAGE_SIZE_X - pMaskRight->width + 1,
		REAL_IMAGE_SIZE_Y - pMaskRight->height + 1), IPL_DEPTH_32F, 1);

	cCameraControl->m_fnMilCreate();
	cCameraControl->m_fnMilOpenDevice(0);
	cCameraControl->m_fnMilOpenDevice(1);
	
 	CRect nRect; CRect nRect2;
 	CWnd* pWnd = CWnd::FromHandle(MassageHandle1);
 	pWnd->GetClientRect(nRect);		
 	
	CWnd* pWnd2 = CWnd::FromHandle(MassageHandle2);
	pWnd2->GetClientRect(nRect2);		
	 
	CvPoint MLineS, MLineE;
	CvPoint MLineS2, MLineE2;

	MLineS.x = 0; MLineS.y = REAL_IMAGE_SIZE_Y/2;
	MLineE.x = REAL_IMAGE_SIZE_X; MLineE.y = REAL_IMAGE_SIZE_Y/2;
	MLineS2.x = REAL_IMAGE_SIZE_X/2; MLineS2.y = 0;
	MLineE2.x = REAL_IMAGE_SIZE_X/2; MLineE2.y = REAL_IMAGE_SIZE_Y;

	CvFont G_cvDrawSmallFont; 
	char strProcMsg[128];

	hscale			= 2.4f;
	vscale			= 2.4f;
	italicscale		= 0.0f;
	thickness		= 3;
	cvInitFont (&G_cvDrawSmallFont, CV_FONT_HERSHEY_COMPLEX , hscale, vscale, italicscale, thickness, CV_AA);
	
	SYSTEMTIME		systime;
	char chFilePath_1[_MAX_PATH] = {0, };
	char chFilePath_2[_MAX_PATH] = {0, };

 	while(pProThreadData->bThreadFlag)
 	{
 		Sleep(40); 		

		cCameraControl->m_fnMilGetImage(iplGrab1->imageData, iplGrab2->imageData);
			
 		cvCvtColor( iplGrab1, iplCam1, CV_BayerBG2BGR );
 		cvCvtColor( iplGrab2, iplCam2, CV_BayerBG2BGR );		
		//cvCopyImage(iplGrab1, iplCam1);
		//cvCopyImage(iplGrab2, iplCam2);
		
		if (pProThreadData->bPaperDetect)
		{
			cvCvtColor(iplCam1, pGrayAlign, CV_BGR2GRAY);
			cvThreshold(pGrayAlign, pGrayAlign, 120, 255, CV_THRESH_BINARY); 

			cvSetZero(pMatchBufferLeft); 
			cvMatchTemplate(pGrayAlign, pMaskLeft, pMatchBufferLeft, CV_TM_CCOEFF_NORMED);
			cvMinMaxLoc(pMatchBufferLeft, &dMin, &dMax, NULL, &left_top);

			if(dMax < dLimit) //스틱이 아니라면...
			{
				cCameraControl->m_bPaperDetect = TRUE; //간지다...
			}		

			//Right Cam
			cvCvtColor(iplCam2, pGrayAlign, CV_BGR2GRAY);
			cvThreshold(pGrayAlign, pGrayAlign, 120, 255, CV_THRESH_BINARY); 

			cvSetZero(pMatchBufferRight);
			cvMatchTemplate(pGrayAlign, pMaskRight, pMatchBufferRight, CV_TM_CCOEFF_NORMED);
			cvMinMaxLoc(pMatchBufferRight, &dMin, &dMax, NULL, &left_top);

			if(dMax < dLimit && cCameraControl->m_bPaperDetect == TRUE)
			{
				cCameraControl->m_bPaperDetect = TRUE;
			}
			else
				cCameraControl->m_bPaperDetect = FALSE;	

			::SendMessage(pProThreadData->hParentHandle, WM_USER_ACC_PAPER_DETECT, NULL, NULL);
			pProThreadData->bPaperDetect = FALSE;
		}

		if (pProThreadData->bStickDetect)
		{
			cvCvtColor(iplCam1, pGrayAlign, CV_BGR2GRAY);
			cvThreshold(pGrayAlign, pGrayAlign, 120, 255, CV_THRESH_BINARY); 

			cvSetZero(pMatchBufferLeft); 
			cvMatchTemplate(pGrayAlign, pMaskLeft, pMatchBufferLeft, CV_TM_CCOEFF_NORMED);
			cvMinMaxLoc(pMatchBufferLeft, &dMin, &dMax, NULL, &left_top);

			if(dMax >= dLimit)
			{
				cCameraControl->m_bStickDetect = TRUE;
			}		

			//Right Cam
			cvCvtColor(iplCam2, pGrayAlign, CV_BGR2GRAY);
			cvThreshold(pGrayAlign, pGrayAlign, 120, 255, CV_THRESH_BINARY); 

			cvSetZero(pMatchBufferRight);
			cvMatchTemplate(pGrayAlign, pMaskRight, pMatchBufferRight, CV_TM_CCOEFF_NORMED);
			cvMinMaxLoc(pMatchBufferRight, &dMin, &dMax, NULL, &left_top);

			if(dMax >= dLimit && cCameraControl->m_bStickDetect == TRUE)
			{
				cCameraControl->m_bStickDetect = TRUE;
			}
			else
				cCameraControl->m_bStickDetect = FALSE;			

			::SendMessage(pProThreadData->hParentHandle, WM_USER_ACC_STICK_DETECT, NULL, NULL);
			pProThreadData->bStickDetect = FALSE;
		}

		if (pProThreadData->bStickAlign)
		{
			//Left Cam
			cvCvtColor(iplCam1, pGrayAlign, CV_BGR2GRAY);
			cvThreshold(pGrayAlign, pGrayAlign, 120, 255, CV_THRESH_BINARY); 

			cvSetZero(pMatchBufferLeft); 
			cvMatchTemplate(pGrayAlign, pMaskLeft, pMatchBufferLeft, CV_TM_CCOEFF_NORMED);
			cvMinMaxLoc(pMatchBufferLeft, &dMin, &dMax, NULL, &left_top);

			if(dMax >= dLimit)
			{
				LeftStart.x = left_top.x; LeftStart.y = left_top.y;
				LeftEnd.x = left_top.x + pMaskLeft->width; LeftEnd.y = left_top.y + pMaskLeft->height;
				cvRectangle(iplCam1, LeftStart, LeftEnd, CV_RGB(0, 255, 0), 5);
			}

			sprintf_s(strProcMsg,128,"[Y : %d px]",LeftStart.y + (pMaskLeft->height / 2));
			cvPutText(iplCam1, strProcMsg,  cvPoint(50 , 80), &G_cvDrawSmallFont, CV_RGB(255,0,0));

			sprintf_s(strProcMsg,128,"[X : %d px]",LeftStart.x + (pMaskLeft->width / 2));
			cvPutText(iplCam1, strProcMsg,  cvPoint(50 , 160), &G_cvDrawSmallFont, CV_RGB(255,0,0));

			//Right Cam
			cvCvtColor(iplCam2, pGrayAlign, CV_BGR2GRAY);
			cvThreshold(pGrayAlign, pGrayAlign, 120, 255, CV_THRESH_BINARY); 

			cvSetZero(pMatchBufferRight);
			cvMatchTemplate(pGrayAlign, pMaskRight, pMatchBufferRight, CV_TM_CCOEFF_NORMED);
			cvMinMaxLoc(pMatchBufferRight, &dMin, &dMax, NULL, &left_top);

			if(dMax >= dLimit)
			{
				RightStart.x = left_top.x; RightStart.y = left_top.y;
				RightEnd.x = left_top.x + pMaskRight->width; RightEnd.y = left_top.y + pMaskRight->height;
				cvRectangle(iplCam2, RightStart, RightEnd, CV_RGB(0, 255, 0), 5);
			}			

			sprintf_s(strProcMsg,128,"[Y : %d px]",RightStart.y + (pMaskRight->height / 2));
			cvPutText(iplCam2, strProcMsg,  cvPoint(50 , 80), &G_cvDrawSmallFont, CV_RGB(255,0,0));

			sprintf_s(strProcMsg,128,"[X : %d px]",1920 - (RightStart.x + (pMaskLeft->width / 2)));
			cvPutText(iplCam2, strProcMsg,  cvPoint(50 , 160), &G_cvDrawSmallFont, CV_RGB(255,0,0));


			cCameraControl->m_nTM02TiltOffset = (LeftStart.y + (pMaskLeft->height / 2)) - (RightStart.y + (pMaskRight->height / 2));
			if(abs(cCameraControl->m_nTM02TiltOffset) <= 2)
				cCameraControl->m_nTM02TiltOffset = 0;

			cCameraControl->m_nTM02ShiftOffset = (LeftStart.x + (pMaskLeft->width / 2)) - (1920 - (RightStart.x + (pMaskRight->width / 2)));
			if(abs(cCameraControl->m_nTM02ShiftOffset) <= 2)
				cCameraControl->m_nTM02ShiftOffset = 0;

			cCameraControl->m_nTM02DriveOffset = 720 - LeftStart.y - (pMaskRight->height / 2);			
			if(abs(cCameraControl->m_nTM02DriveOffset) <= 2)
				cCameraControl->m_nTM02DriveOffset = 0;

			pProThreadData->bStickAlign = FALSE;
			::SendMessage(pProThreadData->hParentHandle, WM_USER_ACC_STICK_ALIGN, NULL, NULL);					
		}

		if (pProThreadData->bImageSave)
		{
			ZeroMemory(chFilePath_1, _MAX_PATH);
			ZeroMemory(chFilePath_2, _MAX_PATH);
			GetLocalTime(&systime);
			sprintf_s(chFilePath_1, "D:\\nBiz_InspectStock\\CamImage\\%02d%02d%02d%02d_1.BMP", systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
			sprintf_s(chFilePath_2, "D:\\nBiz_InspectStock\\CamImage\\%02d%02d%02d%02d_2.BMP", systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);

			cvSaveImage(chFilePath_1, iplCam1);
			cvSaveImage(chFilePath_2, iplCam2);

			pProThreadData->bImageSave = FALSE;
		}

		cvLine(iplCam1, MLineS, MLineE, CV_RGB(255,0,0), 2);
		cvLine(iplCam1, MLineS2, MLineE2, CV_RGB(255,0,0), 2);
		cvLine(iplCam2, MLineS, MLineE, CV_RGB(255,0,0), 2);
		cvLine(iplCam2, MLineS2, MLineE2, CV_RGB(255,0,0), 2);	 				
		
 		/*cvCvtColor(iplCam1, pGrayAlign, CV_BGR2GRAY);
 		cvThreshold(pGrayAlign, pGrayAlign, 80, 255, CV_THRESH_BINARY); 
 		cvCvtColor(pGrayAlign, iplCam1, CV_GRAY2BGR);*/
// 
// 		cvCvtColor(iplCam2, pGrayAlign, CV_BGR2GRAY);
// 		cvThreshold(pGrayAlign, pGrayAlign, 80, 255, CV_THRESH_BINARY); 
// 		cvCvtColor(pGrayAlign, iplCam2, CV_GRAY2BGR);


// 		cvCvtColor(iplCam1, iplCam1, CV_BGR2HSV);
// 		cvCvtColor(iplCam2, iplCam2, CV_BGR2HSV);
		
		ViewImage.CopyOf(iplCam1);
		ViewImage.DrawToHDC(pDc1->m_hDC, nRect);

		ViewImage.CopyOf(iplCam2);
		ViewImage.DrawToHDC(pDc2->m_hDC, nRect);
 	}
 	//////////////////////////////////////////////////////////////////////////
 	
 	ViewImage.Destroy();	
	
	ReleaseDC(MassageHandle1, phDC1);
	ReleaseDC(MassageHandle2, phDC2);	
 
 	cvReleaseImage(&iplCam1);
 	cvReleaseImage(&iplCam2);
 
	cvReleaseImage(&iplGrab1);
	cvReleaseImage(&iplGrab2);

	cvReleaseImage(&pGrayAlign);

	cvReleaseImage(&pMaskLeft);
	cvReleaseImage(&pMaskRight);
	cvReleaseImage(&pMatchBufferLeft);
	cvReleaseImage(&pMatchBufferRight);


	cCameraControl->m_fnMilFree();

 	return 0;
 }

void	CCameraControl::m_fnThreadCreator()
{
	HANDLE					h_ACCHandle;
	h_ACCHandle = (HANDLE)_beginthreadex( NULL, 0, this->THREAD_CAMERA_FRAME_GRAB, (void*)&sThreadParam, 0, NULL);				
	SetThreadPriority(h_ACCHandle, THREAD_PRIORITY_HIGHEST);		
	CloseHandle(h_ACCHandle);
}

void	CCameraControl::m_fnSetThreadParam(HWND hWnd1, HWND hWnd2, HWND hParent)
{
	sThreadParam.cObjectPointer		= this;	
	sThreadParam.mUpdateWindHandle1 = hWnd1;
	sThreadParam.mUpdateWindHandle2 = hWnd2;
	sThreadParam.hParentHandle = hParent;
	sThreadParam.bThreadFlag		= TRUE;
}

void CCameraControl::m_fnThreadStop(void)
{
	sThreadParam.bThreadFlag = false;
	Sleep(10);
}

void CCameraControl::m_fnMilFree(void)
{
#ifdef CAMERA_USED
	m_board1->Stop();
	m_board2->Stop();

	m_board1->Halt();
	m_board2->Halt();

	m_board1->FreeBuffers();
	m_board2->FreeBuffers();

	delete m_board1;
	delete m_board2;

	m_board1 = NULL;
	m_board2 = NULL;
#endif
}

void CCameraControl::m_fnImageSave()
{
	sThreadParam.bImageSave = TRUE;
}

void CCameraControl::m_fnPaperDetect()
{
	m_bPaperDetect = FALSE;
	sThreadParam.bPaperDetect = TRUE;	
}

void CCameraControl::m_fnStickDetect()
{
	m_bStickDetect = FALSE;
	sThreadParam.bStickDetect = TRUE;
}

void CCameraControl::m_fnStickAlign(int Treshold)
{
	m_nTM02ShiftOffset = 0;
	m_nTM02TiltOffset = 0;
	m_nTM02DriveOffset = 0;
	m_nAlignThreshold=Treshold;
	sThreadParam.bStickAlign = TRUE;
}

void CCameraControl::m_fnOffsetClear()
{
	m_nTM02ShiftOffset = 0;
	m_nTM02TiltOffset = 0;
	m_nTM02DriveOffset = 0;
}

BOOL CCameraControl::m_gnGetPaperDetectResult()
{
	//return TRUE;
	return m_bPaperDetect;
}

BOOL CCameraControl::m_gnGetStickDetectResult()
{
	return m_bStickDetect;
}

int	CCameraControl::m_fnGetShiftOffset()
{
	return m_nTM02ShiftOffset;
}

int	CCameraControl::m_fnGetTiltOffset()
{
	return m_nTM02TiltOffset;
}

int	CCameraControl::m_fnGetDriveOffset()
{
	return m_nTM02DriveOffset;
}