#pragma once
#include "afxwin.h"


// CTurnTable 대화 상자입니다.

class CTurnTable : public CDialogEx
{
	DECLARE_DYNAMIC(CTurnTable)

public:
	CTurnTable(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTurnTable();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_TT01 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	
public:
	virtual void PostNcDestroy();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnSave();
	afx_msg void OnBnClickedBtnCancel();
	afx_msg void OnBnClickedBtnPosMove();
	afx_msg void OnBnClickedBtnPosDn();
	afx_msg void OnBnClickedBtnPosUp();
	afx_msg void OnBnClickedBtnPosMove2();
	afx_msg void OnBnClickedBtnPosDn2();
	afx_msg void OnBnClickedBtnPosUp2();
	afx_msg void OnBnClickedBtnPosMove3();
	afx_msg void OnBnClickedBtnPosDn3();
	afx_msg void OnBnClickedBtnPosUp3();
	afx_msg void OnBnClickedBtnUpVacOn();
	afx_msg void OnBnClickedBtnUpVacOff();
	void m_fnStickUpperVac(AIR_SELECTION nAirSelect, int nActType);

	afx_msg void OnBnClickedBtnUpPurgeOn();
	void m_fnStickUpperPurge(AIR_SELECTION nAirSelect, int nPurgeTime);

	afx_msg void OnBnClickedBtnDnVacOn();
	afx_msg void OnBnClickedBtnDnVacOff();
	void m_fnStickLowerVac(AIR_SELECTION nAirSelect, int nActType);

	afx_msg void OnBnClickedBtnDnPurgeOn();
	void m_fnStickLowerPurge(AIR_SELECTION nAirSelect, int nPurgeTime);

	void m_fnCreateButton(void);
	int m_fnCreateControlFont(void);
	int m_fnSetParameter(STT01_POSITION* stPointer);
	int m_fnDisplayParameter(void);
	
	//Send Command
	void m_fnTT01DriveMove(int nPosValue);
	void m_fnTT01RotateMove(int nPosValue);
	void m_fnTT01ThetaMove(int nPosValue);
	void m_fnTT01ThetaAlignMove(int nPosValue);
	void m_fnTT01PositionMove(int nPosValue, int nMotionType, int nTimeOut);
	
	
	void m_fnCheckUpAirStep();
	void m_fnCheckDnAirStep();

	//Receive Result
	int m_fn_TT01_Drive_Return(VS24MotData stReturn_Mess);
	int m_fn_TT01_Theta_Return(VS24MotData stReturn_Mess);
	int m_fn_TM01_Rotate_Return(VS24MotData stReturn_Mess);
	int m_fn_TT01_UP_VacOn_Return(VS24MotData stReturn_Mess);
	int m_fn_TT01_UP_VacOff_Return(VS24MotData stReturn_Mess);
	int m_fn_TT01_UP_Purge_Return(VS24MotData stReturn_Mess);
	int m_fn_TT01_DN_VacOn_Return(VS24MotData stReturn_Mess);
	int m_fn_TT01_DN_VacOff_Return(VS24MotData stReturn_Mess);
	int m_fn_TT01_DN_Purge_Return(VS24MotData stReturn_Mess);

	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	void m_fnTT01ActionStop(int nMotionType, int nTimeOut);
	bool m_fnTT01InterlockCheck(int AxisNum, long newPosition);
	void m_fnDisplaySignal();
public:
	//CInterServerInterface*  g_cpServerInterface;
	COLORREF	m_btBackColor;
	CFont m_EditFont;
	STT01_POSITION* m_st_TT01_Pos;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;	
	CRoundButtonStyle	m_tStyleOn;	
	CRoundButtonStyle	m_tStyleOff;	

	CRoundButton2 m_ctrBtnMove;
	CRoundButton2 m_ctrBtnMoveDn;
	CRoundButton2 m_ctrBtnMoveUp;
	CRoundButton2 m_ctrBtnMoveR;
	CRoundButton2 m_ctrBtnMoveDnR;
	CRoundButton2 m_ctrBtnMoveUpR;
	CRoundButton2 m_ctrBtnMoveT;
	CRoundButton2 m_ctrBtnMoveDnT;
	CRoundButton2 m_ctrBtnMoveUpT;
	CRoundButton2 m_ctrBtnVacOnUp;
	CRoundButton2 m_ctrBtnVacOffUp;
	CRoundButton2 m_ctrBtnPurgeUp;
	CRoundButton2 m_ctrBtnVacOnDn;
	CRoundButton2 m_ctrBtnVacOffDn;
	CRoundButton2 m_ctrBtnPurgeDn;
	CRoundButton2 m_ctrBtnValueSave;
	CRoundButton2 m_ctrBtnSaveCancel;
	CRoundButton2 m_ctrBtnMoveDefault;
	CRoundButton2 m_ctrBtnTT01ActionStop;

	VS24MotData m_stTT01_Mess;	

	AIR_SELECTION m_stAirStep;
	
	int m_nCtrDriveIndex;
	int m_nCtrRotateIndex;	
	afx_msg void OnBnClickedBtnMotionStop();
};
