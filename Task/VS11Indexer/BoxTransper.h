#pragma once
#include "afxwin.h"
#include "BCRCtrl.h"

// CBoxTransper 대화 상자입니다.


class CBoxTransper : public CDialogEx
{
	DECLARE_DYNAMIC(CBoxTransper)

public:
	CBoxTransper(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBoxTransper();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_TM01 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	CBCRCtrl m_cBcrCtrl;
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PostNcDestroy();
	int m_fnCreateControlFont(void);
	int m_fnSetParameter(STM01_POSITION* stPointer);
	int m_fnDisplayParameter(void);	
	void m_fnCreateButton(void);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnSave4();
	afx_msg void OnBnClickedBtnCancel4();	
	afx_msg void OnBnClickedBtnPosMove();
	afx_msg void OnBnClickedBtnPosDn();
	afx_msg void OnBnClickedBtnPosUp();
	afx_msg void OnBnClickedBtnCylUp();
	afx_msg void OnBnClickedBtnCylDn();
	afx_msg void OnBnClickedBtnVacOn();
	afx_msg void OnBnClickedBtnVacOff();
	afx_msg void OnBnClickedBtnRead();
	afx_msg void OnBnClickedBtnMotionStop();
	afx_msg void OnBnClickedBtnPurge();

	void m_fnTM01PositionMove(int nPosValue, int nMotionType, int nTimeOut);
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	void m_fnTM01ForkMove(int nPosValue);
	void m_fnBoxBarcodeDisp(char* chData);

	int m_fn_TM01_Drive_Return(VS24MotData stReturn_Mess);
	int m_fn_TM01_ForkUP_Return(VS24MotData stReturn_Mess);
	int m_fn_TM01_ForkDn_Return(VS24MotData stReturn_Mess);
	int m_fn_TM01_VacOn_Return(VS24MotData stReturn_Mess);
	int m_fn_TM01_VacOff_Return(VS24MotData stReturn_Mess);
	int m_fn_TM01_Purge_Return(VS24MotData stReturn_Mess);
	int m_fn_TM01_BCR_Return(LPARAM lParam);
	void m_fnTM01ActionStop(int nMotionType, int nTimeOut);
	bool m_fnTM01InterlockCheck(int AxisNum, long newPosition);
	void m_fnDisplaySignal();
public:
	//CInterServerInterface*  g_cpServerInterface;
	COLORREF	m_btBackColor;
	CFont m_EditFont;
	STM01_POSITION* m_st_TM01_Pos;

	CRoundButtonStyle	m_tButtonStyle;
	CRoundButtonStyle	m_tStyle1;
	CRoundButtonStyle	m_tStyleOff;
	CRoundButtonStyle	m_tStyleOn;

	CRoundButton2 m_ctrBtnMove;
	CRoundButton2 m_ctrBtnMoveDn;
	CRoundButton2 m_ctrBtnMoveUp;
	CRoundButton2 m_ctrBtnValueSave;
	CRoundButton2 m_ctrBtnSaveCancel;
	CRoundButton2 m_ctrBtnMoveDefault;
	CRoundButton2 m_ctrBtnCylUp;
	CRoundButton2 m_ctrBtnCylDn;
	CRoundButton2 m_ctrBtnVacOn;
	CRoundButton2 m_ctrBtnVacOff;
	CRoundButton2 m_ctrBtnBcr;
	CRoundButton2 m_ctrBtnBcrSet;
	CRoundButton2 m_ctrBtnTM01ActionStop;
	CRoundButton2 m_ctrBtnPurge;
	VS24MotData m_stTM01_Mess;

	char m_chBoxBarcode[MAX_BARCODE];

	int m_nCtrPosIndex;
	bool m_bBCR_Connection;
	
	afx_msg void OnBnClickedBtnSet();
};
