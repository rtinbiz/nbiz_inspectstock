// SoliosXCL.h: interface for the CSoliosXCL class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SoliosXCL_H__B65DF28F_D6B6_40C3_86E5_6DAEFAA0C1EB__INCLUDED_)
#define AFX_SoliosXCL_H__B65DF28F_D6B6_40C3_86E5_6DAEFAA0C1EB__INCLUDED_

#ifdef YMATROX_EXPORTS
#define YMATROX_API __declspec(dllexport)
#else
#define YMATROX_API __declspec(dllimport)
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "mil.h"


#ifndef NOBOARD
#endif
#define MAX_BUFFER 2

#ifdef CAMERA_USED
class CSoliosXCL  
{
public:
	int m_nBuffer;
	long GetGrabIndex();
	int m_nDeviceId;
	void ChangeGrabMode(int nMode);
	int m_nNumTrigger;
	void FreeBuffers();
	int m_nTriggerInput;
	void BufferDisplay(int nIndex);
	BOOL m_bLive;
	void SaveBuffer(int nIndex, LPCTSTR strFile);
	void ProcessGrab(int nFrames);
	int m_nGrabMode;
	void ChangeDCF(int nBoardId, int nDeviceId, LPCTSTR strDcf);
	void ClearOverlay();
	void ShowRuler(double lfRulerInterval, double lfRes);
	void ShowCenterCross();
	BOOL m_bInitOverlay;
	void InitializeOverlay();
	BOOL m_bPocess;
	void Halt();
	void SetDisplayWnd(HWND hWnd);
	void OpenDevice(int nBoardId, int nDeviceId, LPCTSTR strDcf, int nBoardName);
	void OpenDevice(int nBoardId, int nDeviceId, LPCTSTR strDcf);
	void PutImage(LPVOID pImage1,int nIndex);
	void* m_pLib;

	void	SetCallBack( int (*pCall)(BYTE* pBuf, LPVOID pParam, int nCnt), LPVOID pView );
	void	SetCallBackComm( int (*pCallComm)(BYTE* pBuf, LPVOID pParam), LPVOID pView );
	void	SetAcquisionParam( int nExpose_us, int nNumOfLine, int nPeriod_us ) {};
	void	SetTrigger();

	CSoliosXCL();
	virtual ~CSoliosXCL();

	long		SizeX;				   // Buffer Size X
	long		SizeY;				   // Buffer Size Y
	MIL_INT		DigSizeX;			   // Digitizer input width
	MIL_INT		DigSizeY;			   // Digitizer input heigh
	MIL_INT		Band;				      // Number of input color bands of the digitizer
	BOOL 		GrabIsStarted;	      // State of the grab
//	CWnd		*GrabInViewPtr;	   // Pointer to the view that has the grab
	long		NumberOfDigitizer;   // Number of digitizers available on the system
	long		m_nGrabIndex;

#ifndef NOBOARD
#endif
//	static MIL_ID MilApplication;  /* Application identifier.  */
  //  static MIL_ID MilSystem;       /* System identifier.       */
	MIL_ID MilDigitizer;
    
    MIL_ID MilImage[MAX_BUFFER];        /* Image buffer identifier. */
    static MIL_ID MilDisplay;      /* Display identifier.      */
	static MIL_ID MilOverlayImage;	            //Overlay image buffer identifier 
	static MIL_ID MilImageDisplay;        /* Image buffer (display) identifier. */
	static MIL_ID MilApplication;  /* Application identifier.  */
	static MIL_ID MilSystem;       /* System identifier.       */
	static int    m_nCntInstance;


	MIL_ID MilByerCoef;
	BYTE	*m_pBufImage;
	static	long MFTYPE OnHookEnd(long HookType, MIL_ID EventId, void MPTYPE *UserStructPtr);
	static	long MFTYPE ProcessingFunction(long HookType, MIL_ID HookId, void MPTYPE *HookDataPtr);
//#endif
//	static BOOL MilBufferContinue;
	
	void DoubleBuffer(int);
	void Grab(char* chImage);
	void Live();
	void Stop();
	void Clear();
//	void GetImage(LPVOID,int);
	void GetImage(LPVOID);
	void SetHorizontal(int);
	void    LiveProc();
	static void GrabThread(void *UserDataPtr);

protected:
	int (*m_pCallBack)(BYTE* pBuf, LPVOID pParam, int nCnt);
	int (*m_pCallBackComm)(BYTE* pBuf, LPVOID pParam);
	LPVOID m_pCBParam;
	LPVOID m_pCBParamComm;
};
#endif
#endif // !defined(AFX_SoliosXCL_H__B65DF28F_D6B6_40C3_86E5_6DAEFAA0C1EB__INCLUDED_)
