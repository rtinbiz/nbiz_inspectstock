#ifndef _GLOBALPARAMETER_H_
#define _GLOBALPARAMETER_H_

#include "SequenceDefine.h"
#include "GlobalEnumList.h"
#include "GlobalDefine.h"

#define SAFE_ZONE BOOL
static const BOOL   SAFE = TRUE;
static const BOOL   UNSAFE = FALSE;


static const int	LIST_ALARM_COUNT = 200;

static const int	RETRY_COUNT = 1;

static const int	MAX_BARCODE = 64;

static const int	MAX_QRCODE = 64;

 static const int	ON = 1;
 
 static const int	OFF = 0;

static const int	POSITION_OFFSET = 10;
static const int	UT01_1STEP_OFFSET = 27000;
static const int	UT01_COVERCLOSE_DN_OFFSET = 2000;
static const int	LT01_1STEP_OFFSET = 25000;

static const int	MAX_OBJECT_COUNT = 32;//30; 20150404 정의천

static const int	NONE = 0;

static const int	STICK = 1;

static const int	PAPER = 2;

static const int	MAX_RECIPE = 1000;

static const double PICKER_SHIFT_VALUE = 12.77;

static const double PICKER_ROTATE_VALUE = 3.04;

static const double PICKER_DRIVE_VALUE = 25.44;

static const int PICKER_PAD_OFFSET = 29000;


//메인 스레드 인터벌.
static const int	STATUS_DISP_TIMER = 1000;
static const int	THREAD_INTERVAL = 500;
static const int	ALL_STATUS_TIMER = THREAD_INTERVAL * 6;
static const int	TASK_STATUS_TIMER = THREAD_INTERVAL * 10;
static const int	TASK_STATUS_DISCONNECT = THREAD_INTERVAL * 20;

static const int	PICKER_READY_POSITION_OFFSET = 30000;

#endif