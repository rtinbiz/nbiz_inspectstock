#pragma once
#include "afxwin.h"


// CBoxOpener 대화 상자입니다.

class CBoxOpener : public CDialogEx
{
	DECLARE_DYNAMIC(CBoxOpener)

public:
	CBoxOpener(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBoxOpener();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_BO01 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	int m_fnCreateControlFont(void);
	int m_fnSetParameter(SBO01_POSITION* stPointer);
	int m_fnDisplayParameter(void);
	afx_msg void OnBnClickedBtnSave3();
	afx_msg void OnBnClickedBtnCancel3();
	void m_fnCreateButton(void);
	void m_fnBO01DriveMove(int nPosValue);
	void m_fnBO01UpDnMove(int nPosValue,int nSpeedMode = Speed_Normal);
	void m_fnBO01RotateMove(int nPosValue);
	void m_fnBO01PositionMove(int nPosValue, int nMotionType, int nTimeOut);
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	afx_msg void OnBnClickedBtnPosMove();
	afx_msg void OnBnClickedBtnPosDn();
	afx_msg void OnBnClickedBtnPosUp();
	afx_msg void OnBnClickedBtnZPosMove();
	afx_msg void OnBnClickedBtnZPosDn();
	afx_msg void OnBnClickedBtnZPosUp();
	afx_msg void OnBnClickedBtnPadPosMove();
	afx_msg void OnBnClickedBtnPadPosDn();
	afx_msg void OnBnClickedBtnPadPosUp();
	afx_msg void OnBnClickedBtnCylFwd();
	afx_msg void OnBnClickedBtnCylBwd();
	afx_msg void OnBnClickedBtnVacOn();
	afx_msg void OnBnClickedBtnVacOff();
	afx_msg void OnBnClickedBtnPurge();

	int m_fn_BO01_Drive_Return(VS24MotData stReturn_Mess);
	int m_fn_BO01_UpDn_Return(VS24MotData stReturn_Mess);
	int m_fn_BO01_Rotate_Return(VS24MotData stReturn_Mess);
	int m_fn_BO01_PickerFwd_Return(VS24MotData stReturn_Mess);
	int m_fn_BO01_PickerBwd_Return(VS24MotData stReturn_Mess);
	int m_fn_BO01_VacOn_Return(VS24MotData stReturn_Mess);
	int m_fn_BO01_VacOff_Return(VS24MotData stReturn_Mess);
	int m_fn_BO01_Purge_Return(VS24MotData stReturn_Mess);
	void m_fnBO01ActionStop(int nMotionType, int nTimeOut);
	void m_fnDisplaySignal();
public:
	//CInterServerInterface*  g_cpServerInterface;
	COLORREF	m_btBackColor;
	CFont m_EditFont;
	SBO01_POSITION* m_st_BO01_Pos;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;	
	CRoundButtonStyle	m_tStyleOn;	
	CRoundButtonStyle	m_tStyleOff;	
	CRoundButton2 m_ctrBtnMove;
	CRoundButton2 m_ctrBtnMoveDn;
	CRoundButton2 m_ctrBtnMoveUp;
	CRoundButton2 m_ctrBtnValueSave;
	CRoundButton2 m_ctrBtnSaveCancel;
	CRoundButton2 m_ctrBtnMoveDefault;
	CRoundButton2 m_ctrBtnMoveZ;
	CRoundButton2 m_ctrBtnMoveDnZ;
	CRoundButton2 m_ctrBtnMoveUpZ;
	CRoundButton2 m_ctrBtnMoveR;
	CRoundButton2 m_ctrBtnMoveDnR;
	CRoundButton2 m_ctrBtnMoveUpR;
	CRoundButton2 m_ctrBtnCylFwd;
	CRoundButton2 m_ctrBtnCylBwd;
	CRoundButton2 m_ctrBtnVacOn;
	CRoundButton2 m_ctrBtnVacOff;
	CRoundButton2 m_ctrBtnPurge;
	CRoundButton2 m_ctrBtnBO01ActionStop;

	VS24MotData m_stBO01_Mess;

	int m_nCtrDriveIndex;
	int m_nCtrUpDnIndex;
	int m_nCtrRotateIndex;
	afx_msg void OnBnClickedBtnInit3();
	afx_msg void OnBnClickedBtnMotionStop();
};
