// SoliosXCL.cpp: implementation of the CSoliosXCL class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SoliosXCL.h"
#include <process.h>

static int MilBufferCount;
static int MilBufferNumber;

#ifdef CAMERA_USED

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MIL_ID CSoliosXCL::MilApplication  = M_NULL;  // Application identifier.  
MIL_ID CSoliosXCL::MilSystem       = M_NULL;  // System identifier.       
MIL_ID CSoliosXCL::MilImageDisplay = M_NULL;  // Image buffer (display) identifier. 
MIL_ID CSoliosXCL::MilDisplay      = M_NULL;  // Display identifier.      
MIL_ID CSoliosXCL::MilOverlayImage = M_NULL;  // Overlay image buffer identifier 
int    CSoliosXCL::m_nCntInstance = 0;



MIL_INT MFTYPE DisplayErrorExt(MIL_INT HookType, MIL_ID EventId, void MPTYPE *UserDataPtr)
{
/*
	char 	ErrorMessageFunction_A[M_ERROR_MESSAGE_SIZE] = "";
	char 	ErrorMessageFunction_B[M_ERROR_MESSAGE_SIZE] = "";
	char 	ErrorMessageFunction_C[M_ERROR_MESSAGE_SIZE] = "";
	char 	ErrorMessageFunction_D[M_ERROR_MESSAGE_SIZE] = "";
	char 	ErrorMessageFunction_E[M_ERROR_MESSAGE_SIZE] = "";
	int 	ErrorMessageFunction_F;
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT_FCT , ErrorMessageFunction_A);
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT , ErrorMessageFunction_B);
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT_SUB_1 , ErrorMessageFunction_C);
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT_SUB_2 , ErrorMessageFunction_D);
	MappGetHookInfo(EventId, M_MESSAGE+M_CURRENT_SUB_3 , ErrorMessageFunction_E);
	MappGetHookInfo(EventId, M_CURRENT_SUB_NB , &ErrorMessageFunction_F);
	//M_CURRENT_SUB_NB 
*/

//	CSoliosXCL* pDig = (CSoliosXCL *)UserDataPtr;
//	pDig->SetTrigger();
/*
	CString str_a, str_b, str_c="", str_d="", str_e=""; 
	str_a = ErrorMessageFunction_A;
	str_b = ErrorMessageFunction_B;
	if(ErrorMessageFunction_F<=1)
		str_c = ErrorMessageFunction_C;
	else if(ErrorMessageFunction_F<=2)
		str_d = ErrorMessageFunction_D;
	else if(ErrorMessageFunction_F<=3)
		str_e = ErrorMessageFunction_E;
	theLog.AddLog("Mil Error : "+str_a+" = "+str_b+" "+str_c+" "+str_d+" "+str_e);
	//theLog.Flush();
*/

	return M_NULL;
}

CSoliosXCL::CSoliosXCL()
{
	m_pCallBack = NULL;
	m_pCallBackComm = NULL;
	m_pCBParam  = NULL;
	m_pCBParamComm  = NULL;

	MilBufferCount  = 0;
	MilOverlayImage = M_NULL;  // Overlay image buffer identifier. 
	m_bInitOverlay  = FALSE;
	m_nGrabMode     = 0;       // 0 - con, 1 - trigger
	m_nTriggerInput = 5;
	for ( int i = 0; i < MAX_BUFFER; i++)
		MilImage[i] = M_NULL;
}

void CSoliosXCL::OpenDevice(int nBoardId, int nDeviceId, LPCTSTR strDcf)
{
#ifndef NOBOARD
	if ( m_nCntInstance == 0)
	{
		MappAlloc(M_DEFAULT, &MilApplication);
		MappHookFunction(M_ERROR_CURRENT ,DisplayErrorExt,this);
		MsysAlloc(M_SYSTEM_SOLIOS, nBoardId, M_SETUP, &MilSystem);//SOLIOS보드를 사용하는 경우 -jscarlet
		//MsysAlloc(M_SYSTEM_HELIOS, nBoardId, M_SETUP, &MilSystem);//HELIOS보드를 사용하는 경우 
		//MsysControl(MilSystem,M_PROCESSING_SYSTEM ,M_DEFAULT_HOST);
	}
	
	if ( strDcf == (LPCTSTR)"" )
		MdigAlloc(MilSystem,M_DEFAULT,M_CAMERA_SETUP,M_DEFAULT,&MilDigitizer);
	else 
		MdigAlloc(MilSystem,nDeviceId,(LPSTR)(LPCTSTR)strDcf, M_DEFAULT, &MilDigitizer);
	
	m_nDeviceId = nDeviceId;

	MdigControl(MilDigitizer, M_GRAB_MODE, M_ASYNCHRONOUS);


	MdigInquire(MilDigitizer,M_SIZE_X,&DigSizeX);
	MdigInquire(MilDigitizer,M_SIZE_Y,&DigSizeY);  	
	MdigInquire(MilDigitizer,M_SIZE_BAND,&Band);

	for (int i = 0 ; i < MAX_BUFFER ; i++)
	{
 		MbufAllocColor(MilSystem, 1, DigSizeX, DigSizeY,
  			   			M_DEF_IMAGE_TYPE,
  		       			M_IMAGE + M_GRAB + M_PROC,
  			   			&MilImage[i]);
	}

	m_pBufImage = new BYTE[DigSizeX * DigSizeY];
	if (MilImageDisplay == M_NULL)
	{
		MbufAllocColor(MilSystem, 1, DigSizeX, DigSizeY,
  			   			M_DEF_IMAGE_TYPE,
  		       			M_IMAGE + M_GRAB + M_PROC + M_DISP ,
  			   			&MilImageDisplay);
	}

	MbufClear(MilImage[0],0);
	MbufClear(MilImage[1],0);
	MbufClear(MilImageDisplay,0);

	if (MilDisplay == M_NULL)
	{
		MdispAlloc(MilSystem,M_DEFAULT, M_DISPLAY_SETUP, M_WINDOWED, &MilDisplay);
	}
	m_nGrabIndex = 0;
	m_nCntInstance++;
#endif
}

void CSoliosXCL::OpenDevice(int nBoardId, int nDeviceId, LPCTSTR strDcf, int nBoardName)
{
#ifndef NOBOARD
	if ( m_nCntInstance == 0)
	{
		MappAlloc(M_DEFAULT, &MilApplication);
		MappHookFunction(M_ERROR_CURRENT ,DisplayErrorExt,this);
		MsysAlloc(M_SYSTEM_SOLIOS, nBoardId, M_SETUP, &MilSystem);//SOLIOS보드를 사용하는 경우 
		/*
		if(nBoardName==1)//SOLIOS 
			MsysAlloc(M_SYSTEM_SOLIOS, nBoardId, M_SETUP, &MilSystem);//SOLIOS보드를 사용하는 경우 
		else//HELIOS
			MsysAlloc(M_SYSTEM_HELIOS, nBoardId, M_SETUP, &MilSystem);//HELIOS보드를 사용하는 경우 
			*/
	//	MsysControl(MilSystem,M_PROCESSING_SYSTEM ,M_DEFAULT_HOST);
	}
	
	if ( strDcf == (LPCTSTR)"" )
		MdigAlloc(MilSystem,M_DEFAULT,M_CAMERA_SETUP,M_DEFAULT,&MilDigitizer);
	else 
		MdigAlloc(MilSystem,nDeviceId,(LPSTR)(LPCTSTR)strDcf, M_DEFAULT, &MilDigitizer);
	
	m_nDeviceId = nDeviceId;

	MdigControl(MilDigitizer, M_GRAB_MODE, M_ASYNCHRONOUS);


	MdigInquire(MilDigitizer,M_SIZE_X,&DigSizeX);
	MdigInquire(MilDigitizer,M_SIZE_Y,&DigSizeY);  	
	MdigInquire(MilDigitizer,M_SIZE_BAND,&Band);

	for (int i = 0 ; i < MAX_BUFFER ; i++)
	{
 		MbufAllocColor(MilSystem, 1, DigSizeX, DigSizeY,
  			   			M_DEF_IMAGE_TYPE,
  		       			M_IMAGE + M_GRAB + M_PROC,
  			   			&MilImage[i]);
	}

	m_pBufImage = new BYTE[DigSizeX * DigSizeY];
	if (MilImageDisplay == M_NULL)
	{
		MbufAllocColor(MilSystem, 1, DigSizeX, DigSizeY,
  			   			M_DEF_IMAGE_TYPE,
  		       			M_IMAGE + M_GRAB + M_PROC + M_DISP ,
  			   			&MilImageDisplay);
	}

	MbufClear(MilImage[0],0);
	MbufClear(MilImage[1],0);
	MbufClear(MilImageDisplay,0);

	if (MilDisplay == M_NULL)
	{
		MdispAlloc(MilSystem,M_DEFAULT, M_DISPLAY_SETUP, M_WINDOWED, &MilDisplay);
	}
	m_nGrabIndex = 0;
	m_nCntInstance++;
#endif
}

void CSoliosXCL::SetDisplayWnd(HWND hWnd)
{
	MdispFree(MilDisplay);
	MdispAlloc(MilSystem,M_DEFAULT, M_DISPLAY_SETUP, M_WINDOWED, &MilDisplay);

	MdispSelectWindow(MilDisplay, MilImageDisplay, hWnd);
}

CSoliosXCL::~CSoliosXCL()
{
FreeBuffers();
#ifndef NOBOARD
#endif
}


void CSoliosXCL::SetHorizontal(int nHorizontal)
{
#ifndef NOBOARD
	MdigControl(MilDigitizer,M_SOURCE_SIZE_Y,nHorizontal);
#endif
}

void CSoliosXCL::DoubleBuffer(int Num)
{
#ifndef NOBOARD
#endif
}

void CSoliosXCL::Clear()
{

}
void CSoliosXCL::SetTrigger()
{
	BYTE* s=NULL;
	if (m_pCallBackComm)
		m_pCallBackComm(s, m_pCBParamComm);
}

void CSoliosXCL::Stop()
{
 	m_bLive = FALSE;
	MdigControl(MilDigitizer, M_GRAB_ABORT, M_DEFAULT);
	m_nGrabIndex = -1;

	MbufClear(MilImage[0],0);
	MbufClear(MilImage[1],0);
	memset(m_pBufImage,0,DigSizeX * DigSizeY);
}

void CSoliosXCL::Grab(char* chImage)
{
#ifndef NOBOARD
	MdigControl(MilDigitizer, M_GRAB_MODE, M_SYNCHRONOUS);
	MdigGrab(MilDigitizer, MilImage[0]);
//	MbufCopy(MilImage[0], MilImageDisplay);
	MbufGet(MilImage[0], (BYTE*)chImage);
//	if (m_pCallBack)
//		m_pCallBack(m_pBufImage, m_pCBParam, 0);
//	if (m_pCallBackComm)
//		m_pCallBackComm(m_pCBParamComm);
#endif
}
/*
void CSoliosXCL::GetImage(LPVOID pImage1, int nIndex)
{
#ifndef NOBOARD


	BYTE *pImage;
	BYTE* pDes;
	pDes = (BYTE*)pImage1;
	pImage = new BYTE[DigSizeX*3*DigSizeY];
	memset(pImage, 0 ,DigSizeX*3*DigSizeY);

	MbufGetColor(MilImage[nIndex], M_PACKED+M_BGR24, M_ALL_BAND, pImage);

	int i, step, realSize;
	realSize = DigSizeX*3;
	for (i = 0 , step = (DigSizeY - 1) * realSize ; i < DigSizeY ; i++, step -= realSize) {		
		memcpy(pDes+i*realSize,pImage+step,realSize);		
	}

	delete [] pImage;
#endif
}
*/

void CSoliosXCL::GetImage(LPVOID pImage1)
{
#ifndef NOBOARD
	//pImage1 = (void*)m_pBufImage;
	memcpy(pImage1, m_pBufImage, DigSizeX * DigSizeY);
#endif
}
void CSoliosXCL::PutImage(LPVOID pImage1, int Height)
{
#ifndef NOBOARD

	BYTE* pDes;
	pDes = (BYTE*)pImage1;

	MbufPutColor(MilImageDisplay, M_PACKED+M_BGR24, M_ALL_BAND, pDes);
#endif
}

void CSoliosXCL::Halt()
{
#ifndef NOBOARD
	MdigHalt(MilDigitizer);
#endif
}

void CSoliosXCL::InitializeOverlay()
{
}

void CSoliosXCL::ShowCenterCross()
{
}

void CSoliosXCL::ShowRuler(double lfRulerInterval, double lfRes)
{
}

void CSoliosXCL::ClearOverlay()
{
	MdispControl(MilDisplay, M_OVERLAY, M_DISABLE);

	m_bInitOverlay = FALSE;
}

void CSoliosXCL::ChangeDCF(int nBoardId, int nDeviceId, LPCTSTR strDcf)
{
	if(MilDigitizer!=M_NULL)
	{
		MdigFree(MilDigitizer);
		MilDigitizer = M_NULL;
	}

	MdigAlloc(MilSystem,nDeviceId,(LPSTR)(LPCTSTR)strDcf, M_DEFAULT, &MilDigitizer);

	MdigControl(MilDigitizer, M_GRAB_MODE, M_ASYNCHRONOUS);
}

void CSoliosXCL::ProcessGrab(int nFrames)
{
	if ( nFrames <= 0 )
		return;

	if (m_bLive == TRUE)
	{
//		AfxMessageBox("In Processing");
		return;
	}

	m_nNumTrigger = nFrames;
	m_nGrabIndex = 0;

	m_bLive = TRUE;
	_beginthread(&GrabThread, 0, this);
}

void CSoliosXCL::Live()
{
	if (m_bLive == TRUE)
	{
//		AfxMessageBox("In Processing");
		return;
	}
	m_bLive = TRUE;
	_beginthread(&GrabThread, 0, this);
//	HANDLE hGrabThread;
//	hGrabThread = (HANDLE)_beginthread(&GrabThread, 0, this);
//	SetThreadPriority(hGrabThread, THREAD_PRIORITY_TIME_CRITICAL);
	//THREAD_PRIORITY_HIGHEST(2),THREAD_PRIORITY_ABOVE_NORMAL(1)
}

void CSoliosXCL::LiveProc()
{
	static int nCount;
	int nGrabCount = 0;

	MappControl(M_ERROR, M_PRINT_DISABLE);						// Apr.03.2006. 임시적으로 사용. Modified by CoiStyle
	MdigControl(MilDigitizer, M_GRAB_TIMEOUT, 10000 );
	MdigControl(MilDigitizer, M_GRAB_MODE, M_ASYNCHRONOUS);
	MdigGrab(MilDigitizer, MilImage[0]);
//	TRACE("1-Normal\n");
	int nBuffer = 0;
	if(m_nNumTrigger>0)//m_nNumTrigger는 m_nGrabLines를 FRAME_SIZE로 나눈 값-즉 프레임 개수
	{
		//while(m_nNumTrigger-2>nGrabCount)//while(1)//
		while(m_nNumTrigger>nGrabCount)//2007.01.17
		{//만약에 m_nNumTrigger=10이라면 nGrabCount=0~9일때까지 돌아가므로 총 10번 돈다.
			m_nGrabIndex++;
			MdigGrab(MilDigitizer, MilImage[(nBuffer + 1) % MAX_BUFFER]);
			if (m_bLive == FALSE)
			{
				break;
			}
			
	//		MbufCopy(MilImage[nBuffer], MilImageDisplay);
			MbufGet(MilImage[nBuffer], m_pBufImage);
			if (m_pCallBack && nGrabCount >= 0)
				m_pCallBack(m_pBufImage, m_pCBParam, nGrabCount);
			nGrabCount++;
			nBuffer++;
			nBuffer %= MAX_BUFFER;
		}
	}
	else
	{
		while(1)
		{
			m_nGrabIndex++;
			MdigGrab(MilDigitizer, MilImage[(nBuffer + 1) % MAX_BUFFER]);
			if (m_bLive == FALSE)
			{
				break;
			}
			
	//		MbufCopy(MilImage[nBuffer], MilImageDisplay);
			MbufGet(MilImage[nBuffer], m_pBufImage);
			if (m_pCallBack && nGrabCount >= 0)
				m_pCallBack(m_pBufImage, m_pCBParam, nGrabCount);
			nGrabCount++;
			nBuffer++;
			nBuffer %= MAX_BUFFER;
		}
	}
	MdigGrabWait(MilDigitizer, M_GRAB_END);


}

void CSoliosXCL::GrabThread(void *UserDataPtr)
{
	CSoliosXCL* pDig = (CSoliosXCL *)UserDataPtr;
	pDig->LiveProc();
}


long MFTYPE CSoliosXCL::ProcessingFunction(long HookType, MIL_ID HookId, void MPTYPE *HookDataPtr)
{
//	TRACE("Processing grabbed!!!\n");
	return 0;
}

void CSoliosXCL::SaveBuffer(int nIndex, LPCTSTR strFile)
{
	MbufSave((LPSTR)(LPCTSTR)strFile, MilImage[nIndex]);
}

void CSoliosXCL::BufferDisplay(int nIndex)
{
	MbufCopy(MilImage[nIndex],MilImageDisplay);
}

void CSoliosXCL::FreeBuffers()
{
	::Sleep(50);
	m_nCntInstance--;

	if(m_pBufImage != NULL)
		delete[] m_pBufImage;

	m_pBufImage = NULL;

	for ( int i = 0; i < MAX_BUFFER; i++) 
	{
		if ( MilImage[i] != M_NULL ) 
		{
			MbufFree(MilImage[i]);
			MilImage[i] = M_NULL;
		}
	}
	MbufFree(MilByerCoef);
//	TRACE("MilByerCoef\n");
	if(MilDisplay != M_NULL && MilImageDisplay!= M_NULL)
	{
		MdispDeselect(MilDisplay, MilImageDisplay);
//		TRACE("MilDisplay_MilImageDisplay\n");
	}
	if(MilImageDisplay != M_NULL )
	{
		MbufFree(MilImageDisplay);
		MilImageDisplay = M_NULL;
//		TRACE("MilImageDisplay\n");
	}
	if(MilDisplay != M_NULL )
	{
		MdispFree(MilDisplay);
		MilDisplay = M_NULL;
//		TRACE("MilDisplay\n");
	}
	if(MilDigitizer != M_NULL)
	{
		MdigFree(MilDigitizer);
		MilDigitizer = M_NULL;
//		TRACE("MilDigitizer\n");
	}

	if (m_nCntInstance == 0)
	{
		MsysFree(MilSystem);
		MilSystem = M_NULL;
		if(MilApplication != M_NULL)
		{
			MappFree(MilApplication);
			MilApplication = M_NULL;
		}
	}
}	

void CSoliosXCL::ChangeGrabMode(int nMode)
{
/*
	if ( nMode == 0 )
		MdigControl(MilDigitizer, M_GRAB_CONTINUOUS_END_TRIGGER, M_ENABLE);
	else
		MdigControl(MilDigitizer, M_GRAB_CONTINUOUS_END_TRIGGER, M_DISABLE);
*/
}

long CSoliosXCL::GetGrabIndex()
{
	return m_nGrabIndex;
}

void CSoliosXCL::SetCallBack( int (*pCall)(BYTE *pBuf, LPVOID pParam, int nCnt), LPVOID pView )
{
	m_pCallBack = pCall;
	m_pCBParam  = pView;
}

void CSoliosXCL::SetCallBackComm( int (*pCallComm)(BYTE *pBuf, LPVOID pParam), LPVOID pView )
{
	m_pCallBackComm = pCallComm;
	m_pCBParamComm  = pView;
}

#endif