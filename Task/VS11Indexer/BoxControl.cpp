#include "StdAfx.h"
#include "BoxControl.h"
#include "VS11Indexer.h"
#include "VS11IndexerDlg.h"

CBoxControl::CBoxControl()
{
	m_elBoxLoadStep = BL_MAIN_SEQUENCE_IDLE;
	m_elCoverOpenStep = CO_MAIN_SEQUENCE_IDLE;
	m_elCoverCloseStep = CC_MAIN_SEQUENCE_IDLE;
	m_elBoxMoveToLT01Step = BML_MAIN_SEQUENCE_IDLE;
	m_elBoxUnloadingStep = BU_MAIN_SEQUENCE_IDLE;

	m_elSeq_Error = ERR_SEQ_NONE;
	m_elBoxSize = BOX_SIZE_NONE;

	m_unBox_Load_Timeout = 0;
	m_unVac_Off_Timeout = 0;
	m_unCover_Open_Timeout = 0;

	m_unCurrentBoxIndex = 0;
}

CBoxControl::~CBoxControl()
{
}

int CBoxControl::m_fnSetParameter(
	SBT01_POSITION* stpointer1,
	STM01_POSITION* stpointer2,
	SLT01_POSITION* stpointer3,
	SBO01_POSITION* stpointer4,
	SUT01_POSITION* stpointer5)
{
	m_st_BT01_Pos = stpointer1;
	m_st_TM01_Pos = stpointer2;
	m_st_LT01_Pos = stpointer3;
	m_st_BO01_Pos = stpointer4;
	m_st_UT01_Pos = stpointer5;

	
	return 0;
}

int CBoxControl::m_fnBoxMove_F_BT01_T_LT01()
{
	CString strLogMessage;
	int nBoxIndex = 0;
	int nUpDnPos = 0;
	int nLt01DrivePos = 0;

	switch (m_elBoxLoadStep)
	{
	case BL_MAIN_SEQUENCE_IDLE:
		break;
	case BL_LT01_RECIPE_SEND:
		m_elSeq_Error = ERR_SEQ_NONE;	
		//g_cpMainDlg->OnBnClickedBtnRecipeSet();
		G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_BT01_T_LT01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			m_elBoxLoadStep = BL_BT01_TARGET_POS_MOVE;
			strLogMessage.Format("TM01 : 박스 로딩 시나리오 시작.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSeq_Error = ERR_RECIPE_SET;
			m_elBoxLoadStep = BL_SEQUENCE_COMP;
			strLogMessage.Format("TM01 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		break;

	case BL_LT01_RECIPE_RCV_ACK:
		strLogMessage.Format("AM01 : Recipe 정상 수신 확인.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elBoxLoadStep = BL_BT01_TARGET_POS_MOVE;
		break;

	case BL_BT01_TARGET_POS_MOVE:
		//nBoxIndex = m_fnGetNextBoxIndex(BOX_READY);
		nBoxIndex = m_unCurrentBoxIndex;
		if (nBoxIndex == 0)
		{
			strLogMessage.Format("작업을 수행할 박스가 없습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			m_elSeq_Error = ERR_BT01_BOX_DETECT;
			m_elBoxLoadStep = BL_SEQUENCE_COMP;			
		}
		else
		{
			strLogMessage.Format("BT01 : %d단 박스에 대한 작업 시작.", nBoxIndex);
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);

			nUpDnPos = m_fnGetNextBoxPos(nBoxIndex);
			g_cpMainDlg->m_cBoxTable->m_fnBT01UpDnMove(nUpDnPos);
			m_unBox_Load_Timeout = 0;
			m_elBoxLoadStep = BL_BT01_TARGET_POS_CHECK;
			strLogMessage.Format("BT01 : 업다운 축 박스 위치 이동.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}		
		break;

	case BL_BT01_TARGET_POS_CHECK:
		//nBoxIndex = m_fnGetNextBoxIndex(BOX_READY);
		nBoxIndex = m_unCurrentBoxIndex;
		nUpDnPos = m_fnGetNextBoxPos(nBoxIndex);
		if(m_unBox_Load_Timeout < TIME_OUT_30_SECOND * 1000)
		{
			m_unBox_Load_Timeout += THREAD_INTERVAL;

			if (G_MOTION_STATUS[BT01_UPDN].B_COMMAND_STATUS == COMPLETE)
			{
				if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
					nUpDnPos) <= POSITION_OFFSET)
				{
					g_cpMainDlg->m_fnBoxStatusChange((BOX_LIST)nBoxIndex, BOX_RUN);
					m_elBoxLoadStep = BL_TM01_FORK_FWD;
				}
			}
		}
		else
		{
			strLogMessage.Format("BL_BT01_TARGET_POS_MOVE(TIME_OUT) 에러가 발생했습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_TIME_OUT;
			m_elBoxLoadStep = BL_SEQUENCE_COMP;
		}		
		break;

	case BL_TM01_FORK_FWD:
		g_cpMainDlg->m_cBoxTransper->m_fnTM01ForkMove(m_st_TM01_Pos->nBT01_FwdPos);
		m_elBoxLoadStep = BL_TM01_FORK_FWD_CHECK;
		strLogMessage.Format("TM01 : 포크 박스 테이블로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_TM01_FORK_FWD_CHECK:
		if (G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
				m_st_TM01_Pos->nBT01_FwdPos) <= POSITION_OFFSET)
			{				
				m_elBoxLoadStep = BL_TM01_CYLINDER_UP;
			}
		}		
		break;
	
	case BL_TM01_CYLINDER_UP:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnCylUp();
		m_elBoxLoadStep = BL_TM01_CYLINDER_UP_CHECK;
		strLogMessage.Format("TM01 : 포크 실린더 업.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_TM01_CYLINDER_UP_CHECK:
		if (G_MOTION_STATUS[TM01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In23_Sn_ForkFrontUp == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In25_Sn_ForkRearUp == ON)
			{
				m_elBoxLoadStep = BL_TM01_BOX_DETECT_CHECK;
			}
		}		
		break;

	case BL_TM01_BOX_DETECT_CHECK:
		if (G_STMACHINE_STATUS.stModuleStatus.In21_Sn_ForkBoxDetection == ON)
		{
			m_elBoxLoadStep = BL_TM01_VAC_ON;
			//m_elBoxLoadStep = BL_TM01_FORK_BWD;
		}
		else
		{
			strLogMessage.Format("TM01 : 박스가 감지되지 않습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_TM01_BOX_DETECT;
			m_elBoxLoadStep = BL_TM01_CYL_DN; //박스가 감지되지 않으면 바로 포크를 후진한다.
		}
		break;

	case BL_TM01_VAC_ON:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnVacOn();
		m_unBox_Load_Timeout = 0;
		m_elBoxLoadStep = BL_TM01_VAC_ON_CHECK;
		strLogMessage.Format("TM01 : 박스 흡착 Vac. On.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_TM01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[TM01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In22_Sn_ForkBoxVacuum == ON)
			{
				m_elBoxLoadStep = BL_TM01_FORK_BWD;//BL_TM01_BCR_POS_MOVE; 정의천 일단 skip
			}			
		}		
		break;

	case BL_TM01_BCR_POS_MOVE:
		g_cpMainDlg->m_cBoxTransper->m_fnTM01ForkMove(m_st_TM01_Pos->mTM01_BCR_Pos);
		m_elBoxLoadStep = BL_TM01_BCR_POS_CHECK;
		strLogMessage.Format("TM01 : 포크 바코드 리딩 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_TM01_BCR_POS_CHECK:
		if (G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
				m_st_TM01_Pos->mTM01_BCR_Pos) <= POSITION_OFFSET)
			{				
				m_elBoxLoadStep = BL_TM01_BARCODE_READ; 
			}
		}	
		break;

	case BL_TM01_BARCODE_READ:  
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnRead();
		m_elBoxLoadStep = BL_TM01_BARCODE_READ_CHECK;
		strLogMessage.Format("TM01 : 바코드 리딩 동작 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_TM01_BARCODE_READ_CHECK:
		if (G_MOTION_STATUS[TM01_BCR].B_COMMAND_STATUS == COMPLETE)
		{
			m_elBoxLoadStep = BL_TM01_FORK_BWD;
		}		
		break;

	case BL_TM01_FORK_BWD:
		g_cpMainDlg->m_cBoxTransper->m_fnTM01ForkMove(m_st_TM01_Pos->nLT01_BwdPos);
		m_elBoxLoadStep = BL_TM01_FORK_BWD_CHECK;
		strLogMessage.Format("TM01 : 포크 로딩 테이블로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_TM01_FORK_BWD_CHECK:		
		if (G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
				m_st_TM01_Pos->nLT01_BwdPos) <= POSITION_OFFSET)
			{
				switch (m_elSeq_Error)
				{
				case ERR_SEQ_NONE:
					m_elBoxLoadStep = BL_TM01_VAC_OFF;
					break;				
				case ERR_TM01_BOX_DETECT:
				case ERR_TM01_VAC_ON:
					m_elBoxLoadStep = BL_SEQUENCE_COMP;  //TM01_ERR_VAC_ON 에러라면, 시퀀스를 끝낸다.
					break;
				default:
					break;
				}
			}
		}		
		break;	

	case BL_TM01_VAC_OFF:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnVacOff();
		m_elBoxLoadStep = BL_TM01_PURGE;
		strLogMessage.Format("TM01 : 포크 박스 흡착 Vac. Off.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_TM01_PURGE:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnPurge();
		m_elBoxLoadStep = BL_TM01_VAC_OFF_CHECK;
		strLogMessage.Format("TM01 : 포크 퍼지 동작 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		//m_unVac_Off_Timeout = 0;
		break;

	case BL_TM01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TM01_VAC].B_COMMAND_STATUS == COMPLETE &&
			G_MOTION_STATUS[TM01_PURGE].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In22_Sn_ForkBoxVacuum == OFF)
			{
				m_elBoxLoadStep = BL_TM01_CYL_DN;
			}
			else
			{
				strLogMessage.Format("TM01 : VACUUM OFF 에러가 발생했습니다.");
				g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
				m_elSeq_Error = ERR_TM01_VAC_OFF;
				m_elBoxLoadStep = BL_SEQUENCE_COMP;
			}
		}				
		break;

	case BL_TM01_CYL_DN:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnCylDn();
		m_elBoxLoadStep = BL_TM01_CYL_DN_CHECK;
		strLogMessage.Format("TM01 : 포크 실린더 다운.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_TM01_CYL_DN_CHECK:
		if (G_MOTION_STATUS[TM01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In24_Sn_ForkFrontDown == ON && 
				G_STMACHINE_STATUS.stModuleStatus.In26_Sn_ForkRearDown == ON)
			{
				switch (m_elSeq_Error)
				{
				case ERR_SEQ_NONE:
					m_elBoxLoadStep = BL_LT01_RECIPE_CHANGE;
					break;				
				case ERR_TM01_BOX_DETECT:
				case ERR_TM01_VAC_ON:	//VAC 에 에러가 발생했다면, 실린더를 내린 후 포크를 후진한다.
					m_elBoxLoadStep = BL_TM01_FORK_BWD;
					break;
				default:
					break;
				}
			}
		}
		break;

	case BL_LT01_RECIPE_CHANGE:
		if (m_fnLoadingTableBoxSizeCheck())
		{
			m_elBoxLoadStep = BL_TM01_READY_POS;
		}
		else
		{
			strLogMessage.Format("LT01 : 박스 사이즈 감지 센서에 이상이 있습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_LT01_BOX_DETECT;
			m_elBoxLoadStep = BL_SEQUENCE_COMP;
		}		
		break;

	case BL_TM01_READY_POS:
		g_cpMainDlg->m_cBoxTransper->m_fnTM01ForkMove(m_st_TM01_Pos->nTM01_Ready_Pos);
		strLogMessage.Format("TM01 : 포크 대기 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elBoxLoadStep = BL_TM01_READY_POS_CHECK;
		break;

	case BL_TM01_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
				m_st_TM01_Pos->nTM01_Ready_Pos) <= POSITION_OFFSET)
			{				
				m_elBoxLoadStep = BL_LT01_GUIDE_FWD;
			}
		}
		break;

	case BL_LT01_GUIDE_FWD:
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnCylFwd();
		m_elBoxLoadStep = BL_LT01_GUIDE_FWD_CHECK;
		strLogMessage.Format("LT01 : 가이드 실린더 전진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_LT01_GUIDE_FWD_CHECK:
		if (G_MOTION_STATUS[LT01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In11_Sn_LoadLeftAlignForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In13_Sn_LoadRightAlignForward == ON)
			{
				m_elBoxLoadStep = BL_LT01_PUSHER_FWD;
			}
		}		
		break;

	case BL_LT01_PUSHER_FWD:
		nLt01DrivePos = m_fnGet_LT01_PusherPos();

		g_cpMainDlg->m_cLoadingTable->m_fnLT01PusherMove(nLt01DrivePos);
		m_elBoxLoadStep = BL_LT01_PUSHER_FWD_CHECK;

		strLogMessage.Format("LT01 : 가이드 모터 전진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		break;

	case BL_LT01_PUSHER_FWD_CHECK:
		nLt01DrivePos = m_fnGet_LT01_PusherPos();

		if (G_MOTION_STATUS[LT01_PUSHER].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush] -
				nLt01DrivePos) <= POSITION_OFFSET)
			{
				m_elBoxLoadStep = BL_LT01_VAC_ON;
			}
		}
		break;

	case BL_LT01_VAC_ON:
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnVacOn();
		m_elBoxLoadStep = BL_LT01_VAC_ON_CHECK;
		strLogMessage.Format("LT01 : 박스 흡착 Vac. On.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_LT01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[LT01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In10_Sn_LoadBoxVacuum == ON)
			{
				m_elBoxLoadStep = BL_LT01_PUSHER_BWD;
			}
		}		
		break;

	case BL_LT01_PUSHER_BWD:
		g_cpMainDlg->m_cLoadingTable->m_fnLT01PusherMove(m_st_LT01_Pos->nGuide_Ready_Pos);
		m_elBoxLoadStep = BL_LT01_PUSHER_BWD_CHECK;
		strLogMessage.Format("LT01 : 가이드 모터 후진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_LT01_PUSHER_BWD_CHECK:
		if (G_MOTION_STATUS[LT01_PUSHER].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush] -
				m_st_LT01_Pos->nGuide_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elBoxLoadStep = BL_LT01_GUIDE_BWD;
			}
		}		
		break;

	case BL_LT01_GUIDE_BWD:
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnCylBwd();
		m_elBoxLoadStep = BL_LT01_GUIDE_BWD_CHECK;
		strLogMessage.Format("LT01 : 가이드 실린더 후진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_LT01_GUIDE_BWD_CHECK:
		if (G_MOTION_STATUS[LT01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In12_Sn_LoadLeftAlignBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In14_Sn_LoadRightAlignBackward == ON)
			{
				m_elBoxLoadStep = BL_BT01_BAD_POS_MOVE;
			}
		}		
		break;

	case BL_BT01_BAD_POS_MOVE:
		g_cpMainDlg->m_cBoxTable->m_fnBT01UpDnMove(m_st_BT01_Pos->nBadBoxPosition);
		m_elBoxLoadStep = BL_BT01_BAD_POS_CHECK;
		strLogMessage.Format("BT01 : 불량 스틱 배출 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case BL_BT01_BAD_POS_CHECK:
		if (G_MOTION_STATUS[BT01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
				m_st_BT01_Pos->nBadBoxPosition) <= POSITION_OFFSET)
			{				
				m_elBoxLoadStep = BL_SEQUENCE_COMP;
			}
		}		
		break;

	case BL_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("TM01 : 박스 로딩 시나리오 에러 발생.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
			
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;

			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}

		g_cpMainDlg->m_elBoxSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_BT01_T_LT01] = SEQ_COMPLETE;

		strLogMessage.Format("TM01 : 박스 로딩 시나리오 종료.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elBoxLoadStep = BL_MAIN_SEQUENCE_IDLE;
		break;

	default:
		break;
	}

	return 0;
}

void CBoxControl::m_fnBoxMove_F_BT01_T_LT01_Step(EL_BOX_LOADING step)
{
	m_elBoxLoadStep = step;
}

int CBoxControl::m_fnBoxCoverOpen()
{
	CString strLogMessage;
	int		nBO01DrivePos = 0;
	int		nUt01DrivePos = 0;
	switch (m_elCoverOpenStep)
	{
	case CO_MAIN_SEQUENCE_IDLE:
		break;
	case CO_BO01_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;		

		G_EL_SUB_SEQ_STATUS[BOX_COVER_OPEN] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			//TM02 가 TT01 포지션에 있어야 한다.
			m_elCoverOpenStep = CO_BO01_CYL_BWD_S1;
			strLogMessage.Format("BO01 : 커버 오픈 시나리오 시작.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elCoverOpenStep = CO_SEQUENCE_COMP;
			strLogMessage.Format("BO01 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		break;
	case CO_BO01_CYL_BWD_S1:
		strLogMessage.Format("BO01 : 실린더 백워드.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylBwd();
		m_elCoverOpenStep = CO_BO01_CYL_BWD_CHECK_S1;
		break;
	case CO_BO01_CYL_BWD_CHECK_S1:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In01_Sn_BoxLeftCylinderBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In05_Sn_BoxRightCylinderBackward == ON)
			{
				m_elCoverOpenStep = CO_BO01_Z_READY_MOVE_S1;
			}
		}
		break;
	case CO_BO01_Z_READY_MOVE_S1:
		strLogMessage.Format("BO01 : 실린더 후진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_Ready_Pos);
		m_elCoverOpenStep = CO_BO01_Z_READY_CHECK_S1;
		break;
	case CO_BO01_Z_READY_CHECK_S1:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_LT01_BOX_SIZE_CHECK;
			}
		}
		break;
	case CO_BO01_LT01_BOX_SIZE_CHECK:
		if (m_fnLoadingTableBoxSizeCheck())
		{
			m_elCoverOpenStep = CO_BO01_LT01_POS_MOVE_S1;
		}
		else
		{
			strLogMessage.Format("LT01 : 박스 사이즈 감지 센서에 이상이 있습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_LT01_BOX_DETECT;
			m_elCoverOpenStep = CO_SEQUENCE_COMP;
		}
		break;

	case CO_BO01_LT01_POS_MOVE_S1:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_LT01();

		strLogMessage.Format("BO01 : 주행축 로딩 테이블 박스 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01DriveMove(nBO01DrivePos);
		m_elCoverOpenStep = CO_BO01_LT01_POS_CHECK_S1;
		break;

	case CO_BO01_LT01_POS_CHECK_S1:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_LT01();

		if (G_MOTION_STATUS[BO01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
				nBO01DrivePos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_CYL_FWD;
			}
		}
		break;

	case CO_BO01_CYL_FWD:
		strLogMessage.Format("BO01 : 실린더 전진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylFwd();
		m_elCoverOpenStep = CO_BO01_CYL_FWD_CHECK;
		break;

	case CO_BO01_CYL_FWD_CHECK:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In00_Sn_BoxLeftCylinderForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In04_Sn_BoxRightCylinderForward == ON)
			{
				m_elCoverOpenStep = CO_BO01_PAD_TURN_DN;
			}
		}
		break;

	case CO_BO01_PAD_TURN_DN:
		strLogMessage.Format("BO01 : 흡착 패드 6시 방향으로 회전.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01RotateMove(m_st_BO01_Pos->nR_Pad_Dn_Pos);
		m_elCoverOpenStep = CO_BO01_PAD_TURN_DN_CHECK;
		break;

	case CO_BO01_PAD_TURN_DN_CHECK:
		if (G_MOTION_STATUS[BO01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate] -
				m_st_BO01_Pos->nR_Pad_Dn_Pos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_Z_LT01_UP_POS_MOVE_DN;
			}
		}
		break;

	case CO_BO01_Z_LT01_UP_POS_MOVE_DN:
		strLogMessage.Format("BO01 : 업다운 축 상단 커버 흡착 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_LT01_Up_Cover_Pos);
		m_elCoverOpenStep = CO_BO01_Z_LT01_UP_POS_MOVE_DN_CHECK;
		break;

	case CO_BO01_Z_LT01_UP_POS_MOVE_DN_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_LT01_Up_Cover_Pos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_VAC_ON;
			}
		}
		break;

	case CO_BO01_VAC_ON:
		strLogMessage.Format("BO01 : 박스 흡착 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnVacOn();
		m_elCoverOpenStep = CO_BO01_VAC_ON_CHECK;
		break;

	case CO_BO01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[BO01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In03_Sn_BoxLeftVacuum == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In06_Sn_BoxRightVacuum == ON)
			{
				m_elCoverOpenStep = CO_BO01_Z_OPEN_READY_MOVE;
			}
		}
		break;

	case CO_BO01_Z_OPEN_READY_MOVE:
		strLogMessage.Format("BO01 : 업다운 축 오픈 대기 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_LT01_Up_Cover_Pos - 20000);

		m_unCover_Open_Timeout = 0;
		m_elCoverOpenStep = CO_BO01_Z_OPEN_READY_CHECK;
		break;
	case CO_BO01_Z_OPEN_READY_CHECK:

		if(m_unCover_Open_Timeout < TIME_OUT_5_SECOND * 1000)
		{
			m_unCover_Open_Timeout += THREAD_INTERVAL;			
		}
		else
		{
			if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
			{
				if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
					(m_st_BO01_Pos->nZ_LT01_Up_Cover_Pos - 20000)) <= POSITION_OFFSET)
				{
					m_elCoverOpenStep = CO_BO01_Z_READY_MOVE_S2;
				}
			}		
		}
		break;

	case CO_BO01_Z_READY_MOVE_S2:
		strLogMessage.Format("BO01 : 업다운 축 대기 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_Ready_Pos);
		m_elCoverOpenStep = CO_BO01_Z_READY_CHECK_S2;
		break;

	case CO_BO01_Z_READY_CHECK_S2:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_UT01_POS_MOVE;
			}
		}
		break;

	case CO_BO01_UT01_POS_MOVE:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_UT01();
		strLogMessage.Format("BO01 : 주행축 언로드 테이블로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01DriveMove(nBO01DrivePos);
		m_elCoverOpenStep = CO_BO01_UT01_POS_CHECK;
		break;

	case CO_BO01_UT01_POS_CHECK:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_UT01();

		if (G_MOTION_STATUS[BO01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
				nBO01DrivePos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_PAD_TURN_UP;
			}
		}
		break;

	case CO_BO01_PAD_TURN_UP:
		strLogMessage.Format("BO01 : 흡착 패드 12시 방향으로 회전.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01RotateMove(m_st_BO01_Pos->nR_Pad_Up_Pos);
		m_elCoverOpenStep = CO_BO01_PAD_TURN_UP_CHECK;
		break;

	case CO_BO01_PAD_TURN_UP_CHECK:
		if (G_MOTION_STATUS[BO01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate] -
				m_st_BO01_Pos->nR_Pad_Up_Pos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_BOX_DETECT;
			}
		}
		break;

	case CO_BO01_BOX_DETECT:
		if (G_STMACHINE_STATUS.stModuleStatus.In02_Sn_BoxDetection == ON)
		{
			m_elCoverOpenStep = CO_BO01_Z_UT01_1STEP_DN_POS_MOVE_DN;
		}
		else
		{
			strLogMessage.Format("BO01 : 박스를 감지하지 못했습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_BO01_BOX_DETECT;
			m_elCoverOpenStep = CO_SEQUENCE_COMP;
		}
		break;
		// step 추가 정의천
//		CO_BO01_Z_UT01_DN_POS_MOVE_DN
	case CO_BO01_Z_UT01_1STEP_DN_POS_MOVE_DN:
		strLogMessage.Format("BO01 : 업다운 축 1차 하단 커버 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos-UT01_1STEP_OFFSET);
		m_elCoverOpenStep = CO_BO01_Z_UT01_1STEP_DN_POS_MOVE_DN_CHECK;
		break;
	case CO_BO01_Z_UT01_1STEP_DN_POS_MOVE_DN_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				(m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos - UT01_1STEP_OFFSET)) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_VAC_OFF;
			}
		}
		break;///////////////////////////////////////
	case CO_BO01_VAC_OFF:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnVacOff();
		strLogMessage.Format("BO01 : 박스 흡착 해제.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_elCoverOpenStep = CO_BO01_PURGE;
		break;

	case CO_BO01_PURGE:
		strLogMessage.Format("BO01 : 퍼지 동작 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnPurge();
		m_elCoverOpenStep = CO_BO01_VAC_OFF_CHECK;
		break;

	case CO_BO01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[BO01_VAC].B_COMMAND_STATUS == COMPLETE &&
			G_MOTION_STATUS[BO01_PURGE].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In03_Sn_BoxLeftVacuum == OFF && 
				G_STMACHINE_STATUS.stModuleStatus.In06_Sn_BoxRightVacuum == OFF)
			{
				m_elCoverOpenStep = CO_BO01_Z_UT01_DN_POS_MOVE_DN;
			}
		}
		break;

	case CO_BO01_Z_UT01_DN_POS_MOVE_DN:
		strLogMessage.Format("BO01 : 업다운 축 하단 커버 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos);
		m_elCoverOpenStep = CO_BO01_Z_UT01_DN_POS_MOVE_DN_CHECK;
		break;

	case CO_BO01_Z_UT01_DN_POS_MOVE_DN_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_BO01_CYL_BWD_S2;
			}
		}
		break;

	case CO_BO01_CYL_BWD_S2:
		strLogMessage.Format("BO01 : 실린더 후진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylBwd();
		m_elCoverOpenStep = CO_BO01_CYL_BWD_CHECK_S2;
		break;

	case CO_BO01_CYL_BWD_CHECK_S2:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In01_Sn_BoxLeftCylinderBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In05_Sn_BoxRightCylinderBackward == ON)
			{
				m_elCoverOpenStep = CO_UT01_GUIDE_FWD;
			}
		}
		break;

	case CO_UT01_GUIDE_FWD:
		strLogMessage.Format("UT01 : 가이드 실린더 전진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cUnloadingTable->OnBnClickedBtnCylFwd();
		m_elCoverOpenStep = CO_UT01_GUIDE_FWD_CHECK;
		break;

	case CO_UT01_GUIDE_FWD_CHECK:
		if (G_MOTION_STATUS[UT01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In17_Sn_UnloadBoxLeftAlignForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In19_Sn_UnloadBoxRightAlignForward == ON)
			{
				m_elCoverOpenStep = CO_UT01_PUSHER_FWD;
			}
		}
		break;

	case CO_UT01_PUSHER_FWD:
		nUt01DrivePos = m_fnGet_UT01_PusherPos();
		strLogMessage.Format("UT01 : 가이드 축 전진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cUnloadingTable->m_fnUT01PusherMove(nUt01DrivePos);
		m_elCoverOpenStep = CO_UT01_PUSHER_FWD_CHECK;
		break;

	case CO_UT01_PUSHER_FWD_CHECK:
		nUt01DrivePos = m_fnGet_UT01_PusherPos();

		if (G_MOTION_STATUS[UT01_PUSHER].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_UT01_BoxUnLoadPush] -
				nUt01DrivePos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_UT01_VAC_ON;
			}
		}
		break;

	case CO_UT01_VAC_ON:
		strLogMessage.Format("UT01 : 박스 흡착.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cUnloadingTable->OnBnClickedBtnVacOn();
		m_elCoverOpenStep = CO_UT01_VAC_ON_CHECK;
		break;

	case CO_UT01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[UT01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In16_Sn_UnloadBoxVacuum == ON)
			{
				m_elCoverOpenStep = CO_UT01_PUSHER_BWD;
			}
		}
		break;

	case CO_UT01_PUSHER_BWD:
		strLogMessage.Format("UT01 : 가이드 축 대기 위치.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cUnloadingTable->m_fnUT01PusherMove(m_st_UT01_Pos->nGuide_Ready_Pos);
		m_elCoverOpenStep = CO_UT01_PUSHER_BWD_CHECK;
		break;

	case CO_UT01_PUSHER_BWD_CHECK:
		if (G_MOTION_STATUS[UT01_PUSHER].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_UT01_BoxUnLoadPush] -
				m_st_UT01_Pos->nGuide_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elCoverOpenStep = CO_UT01_GUIDE_BWD;
			}
		}
		break;

	case CO_UT01_GUIDE_BWD:
		strLogMessage.Format("UT01 : 가이드 실린더 후진.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cUnloadingTable->OnBnClickedBtnCylBwd();
		m_elCoverOpenStep = CO_UT01_GUIDE_BWD_CHECK;
		break;

	case CO_UT01_GUIDE_BWD_CHECK:
		if (G_MOTION_STATUS[UT01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In18_Sn_UnloadBoxLeftAlignBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In20_Sn_UnloadBoxRightAlignBackward == ON)
			{
				m_elCoverOpenStep = CO_SEQUENCE_COMP;
			}
		}
		break;
	case CO_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("BO01 : 커버 오픈 시나리오 에러 발생.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;

			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("BO01 : 커버 오픈 시나리오 종료.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_elBoxSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[BOX_COVER_OPEN] = SEQ_COMPLETE;

		m_elSeq_Error = ERR_SEQ_NONE;
		m_elCoverOpenStep = CO_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}


int CBoxControl::m_fnBoxCoverClose()
{
	CString strLogMessage;
	int		nBO01DrivePos = 0;

	switch (m_elCoverCloseStep)
	{
	case CC_MAIN_SEQUENCE_IDLE:
		break;
	case CC_BO01_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[BOX_COVER_CLOSE] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			//TM02 가 TT01 포지션에 있어야 한다.
			m_elCoverCloseStep = CC_BO01_CYL_BWD_S1;
			strLogMessage.Format("TM01 : 커버 클로즈 시나리오 시작.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elCoverCloseStep = CC_SEQUENCE_COMP;
			strLogMessage.Format("TM01 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		break;
			

	case CC_BO01_CYL_BWD_S1:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylBwd();
		m_elCoverCloseStep = CC_BO01_CYL_BWD_CHECK_S1;
		break;
	case CC_BO01_CYL_BWD_CHECK_S1:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In01_Sn_BoxLeftCylinderBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In05_Sn_BoxRightCylinderBackward == ON)
			{
				m_elCoverCloseStep = CC_BO01_Z_READY_MOVE_S1;
			}
		}
		break;
	case CC_BO01_Z_READY_MOVE_S1:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_Ready_Pos);
		m_elCoverCloseStep = CC_BO01_Z_READY_CHECK_S1;
		break;
	case CC_BO01_Z_READY_CHECK_S1:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_LT01_POS_MOVE_S1;
			}
		}
		break;
	case CC_BO01_LT01_POS_MOVE_S1:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_LT01();
		g_cpMainDlg->m_cBoxOpener->m_fnBO01DriveMove(nBO01DrivePos);
		m_elCoverCloseStep = CC_BO01_LT01_POS_CHECK_S1;
		break;
	case CC_BO01_LT01_POS_CHECK_S1:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_LT01();

		if (G_MOTION_STATUS[BO01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
				nBO01DrivePos) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_CYL_FWD_S1;
			}
		}
		break;
	case CC_BO01_CYL_FWD_S1:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylFwd();
		m_elCoverCloseStep = CC_BO01_CYL_FWD_CHECK_S1;
		break;
	case CC_BO01_CYL_FWD_CHECK_S1:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In00_Sn_BoxLeftCylinderForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In04_Sn_BoxRightCylinderForward == ON)
			{
				m_elCoverCloseStep = CC_BO01_PAD_TURN_UP;
			}
		}
		break;
	case CC_BO01_PAD_TURN_UP:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01RotateMove(m_st_BO01_Pos->nR_Pad_Up_Pos);
		m_elCoverCloseStep = CC_BO01_PAD_TURN_UP_CHECK;
		break;
	case CC_BO01_PAD_TURN_UP_CHECK:
		if (G_MOTION_STATUS[BO01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate] -
				m_st_BO01_Pos->nR_Pad_Up_Pos) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_CYL_BWD_S2;
			}
		}
		break;
	case CC_BO01_CYL_BWD_S2:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylBwd();
		m_elCoverCloseStep = CC_BO01_CYL_BWD_CHECK_S2;
		break;
	case CC_BO01_CYL_BWD_CHECK_S2:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In01_Sn_BoxLeftCylinderBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In05_Sn_BoxRightCylinderBackward == ON)
			{
				m_elCoverCloseStep = CC_BO01_Z_LT01_DN_POS_MOVE_DN;
			}
		}
		break;
	case CC_BO01_Z_LT01_DN_POS_MOVE_DN:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos);
		m_elCoverCloseStep = CC_BO01_Z_LT01_DN_POS_MOVE_DN_CHECK;
		break;
	case CC_BO01_Z_LT01_DN_POS_MOVE_DN_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_CYL_FWD_S2;
			}
		}
		break;
	case CC_BO01_CYL_FWD_S2:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylFwd();
		m_elCoverCloseStep = CC_BO01_CYL_FWD_CHECK_S2;
		break;
	case CC_BO01_CYL_FWD_CHECK_S2:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In00_Sn_BoxLeftCylinderForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In04_Sn_BoxRightCylinderForward == ON)
			{
				m_elCoverCloseStep = CC_BO01_Z_READY_1STEPMOVE;
			}
		}
		break;
// 정의천 2015.04.08 추가 
	case CC_BO01_Z_READY_1STEPMOVE:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos - LT01_1STEP_OFFSET);
		m_elCoverCloseStep = CC_BO01_Z_READY_1STEPMOVE_CHECK;
		break;
	case CC_BO01_Z_READY_1STEPMOVE_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				(m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos - LT01_1STEP_OFFSET)) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_VAC_ON;
			}
		}
		break;
	case CC_BO01_VAC_ON:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnVacOn();
		m_elCoverCloseStep = CC_BO01_VAC_ON_CHECK;
		break;
	case CC_BO01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[BO01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In03_Sn_BoxLeftVacuum == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In06_Sn_BoxRightVacuum == ON)
			{
				m_elCoverCloseStep = CC_LT01_VAC_OFF;
			}
		}
		break;
	case CC_LT01_VAC_OFF:
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnVacOff();
		m_elCoverCloseStep = CC_LT01_VAC_PURGE;
		break;//정의천 2015.04.08 purge 추가
	case CC_LT01_VAC_PURGE:
		strLogMessage.Format("LT01 : 퍼지 동작 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnPurge();
		m_elCoverCloseStep = CC_LT01_VAC_OFF_CHECK;
		break;

	case CC_LT01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[LT01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In10_Sn_LoadBoxVacuum == OFF)
			{
				m_elCoverCloseStep = CC_BO01_Z_READY_MOVE_S2;
			}
		}
		break;
/////////////////////////
	case CC_BO01_Z_READY_MOVE_S2:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_Ready_Pos);
		m_elCoverCloseStep = CC_BO01_Z_READY_CHECK_S2;
		break;
	case CC_BO01_Z_READY_CHECK_S2:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_PAD_TURN_DN;
			}
		}
		break;
	case CC_BO01_PAD_TURN_DN:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01RotateMove(m_st_BO01_Pos->nR_Pad_Dn_Pos);
		m_elCoverCloseStep = CC_BO01_PAD_TURN_DN_CHECK;
		break;
	case CC_BO01_PAD_TURN_DN_CHECK:
		if (G_MOTION_STATUS[BO01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate] -
				m_st_BO01_Pos->nR_Pad_Dn_Pos) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_BOX_DETECT;
			}
		}
		break;
	case CC_BO01_BOX_DETECT:
		if (G_STMACHINE_STATUS.stModuleStatus.In02_Sn_BoxDetection == ON)
		{
			m_elCoverCloseStep = CC_BO01_UT01_POS_MOVE;
		}
		else
		{
			strLogMessage.Format("BO01 : 박스를 감지하지 못했습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_BO01_BOX_DETECT;
			m_elCoverCloseStep = CC_SEQUENCE_COMP;
		}
		break;
	case CC_BO01_UT01_POS_MOVE:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_UT01();
		g_cpMainDlg->m_cBoxOpener->m_fnBO01DriveMove(nBO01DrivePos);
		m_elCoverCloseStep = CC_BO01_UT01_POS_CHECK;
		break;
	case CC_BO01_UT01_POS_CHECK:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_UT01();

		if (G_MOTION_STATUS[BO01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
				nBO01DrivePos) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_Z_UT01_UP_POS_MOVE_DN;
			}
		}
		break;
	case CC_BO01_Z_UT01_UP_POS_MOVE_DN:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_UT01_Up_Cover_Pos-UT01_COVERCLOSE_DN_OFFSET);
		m_elCoverCloseStep = CC_BO01_Z_UT01_UP_POS_MOVE_DN_CHECK;
		break;
	case CC_BO01_Z_UT01_UP_POS_MOVE_DN_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				(m_st_BO01_Pos->nZ_UT01_Up_Cover_Pos-UT01_COVERCLOSE_DN_OFFSET)) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_BO01_VAC_OFF;
			}
		}
		break;
	case CC_BO01_VAC_OFF:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnVacOff();
		m_elCoverCloseStep = CC_BO01_PURGE;
		break;
	case CC_BO01_PURGE:
		strLogMessage.Format("BO01 : 퍼지 동작 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnPurge();
		m_elCoverCloseStep = CC_BO01_VAC_OFF_CHECK;
		break;
	case CC_BO01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[BO01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In03_Sn_BoxLeftVacuum == OFF &&
				G_STMACHINE_STATUS.stModuleStatus.In06_Sn_BoxRightVacuum == OFF)
			{
				m_elCoverCloseStep = CC_BO01_Z_READY_MOVE_S3;
			}
		}
		break;
	case CC_BO01_Z_READY_MOVE_S3:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_Ready_Pos);
		m_elCoverCloseStep = CC_BO01_Z_READY_CHECK_S3;
		break;
	case CC_BO01_Z_READY_CHECK_S3:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elCoverCloseStep = CC_SEQUENCE_COMP;
			}
		}
		break;
	case CC_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("BO01 : 커버 클로즈 시나리오 에러 발생.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;

			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}

		strLogMessage.Format("BO01 : 커버 클로즈 시나리오 종료.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);


		g_cpMainDlg->m_elBoxSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[BOX_COVER_CLOSE] = SEQ_COMPLETE;

		m_elSeq_Error = ERR_SEQ_NONE;
		m_elCoverCloseStep = CC_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}


int CBoxControl::m_fnBoxMove_F_UT01_T_LT01()
{
	CString strLogMessage;
	int		nBO01DrivePos = 0;
	int		nLt01DrivePos = 0;
	switch (m_elBoxMoveToLT01Step)
	{
	case BML_MAIN_SEQUENCE_IDLE:
		break;
	case BML_BO01_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;		
		G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_UT01_T_LT01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			//TM02 가 TT01 포지션에 있어야 한다.
			m_elBoxMoveToLT01Step = BML_UT01_VAC_OFF;
			strLogMessage.Format("BO01 : 박스 배출 시나리오 시작.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elBoxMoveToLT01Step = BML_SEQUENCE_COMP;
			strLogMessage.Format("BO01 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}

		break;
	case BML_UT01_VAC_OFF:
		g_cpMainDlg->m_cUnloadingTable->OnBnClickedBtnVacOff();
		m_elBoxMoveToLT01Step = BML_UT01_VAC_PURGE;
		break;
//정의천 pURGE 추가
	case BML_UT01_VAC_PURGE:
		g_cpMainDlg->m_cUnloadingTable->OnBnClickedBtnPurge();
		m_elBoxMoveToLT01Step = BML_UT01_VAC_OFF_CHECK;
		break;
	case BML_UT01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[UT01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In16_Sn_UnloadBoxVacuum == OFF)
			{
				m_elBoxMoveToLT01Step = BML_BO01_Z_READY_MOVE_S1;
			}
		}
		break;
	case BML_BO01_Z_READY_MOVE_S1:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_Ready_Pos);
		m_elBoxMoveToLT01Step = BML_BO01_Z_READY_CHECK_S1;
		break;
	case BML_BO01_Z_READY_CHECK_S1:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_CYL_FWD_S1;
			}
		}
		break;
	case BML_BO01_CYL_FWD_S1:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylFwd();
		m_elBoxMoveToLT01Step = BML_BO01_CYL_FWD_CHECK_S1;
		break;
	case BML_BO01_CYL_FWD_CHECK_S1:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In00_Sn_BoxLeftCylinderForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In04_Sn_BoxRightCylinderForward == ON)
			{
				m_elBoxMoveToLT01Step = BML_BO01_PAD_TURN_UP;
			}
		}
		break;
	case BML_BO01_PAD_TURN_UP:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01RotateMove(m_st_BO01_Pos->nR_Pad_Up_Pos);
		m_elBoxMoveToLT01Step = BML_BO01_PAD_TURN_UP_CHECK;
		break;
	case BML_BO01_PAD_TURN_UP_CHECK:
		if (G_MOTION_STATUS[BO01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate] -
				m_st_BO01_Pos->nR_Pad_Up_Pos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_CYL_BWD_S1;
			}
		}
		break;
	case BML_BO01_CYL_BWD_S1:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylBwd();
		m_elBoxMoveToLT01Step = BML_BO01_CYL_BWD_CHECK_S1;
		break;
	case BML_BO01_CYL_BWD_CHECK_S1:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In01_Sn_BoxLeftCylinderBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In05_Sn_BoxRightCylinderBackward == ON)
			{
				m_elBoxMoveToLT01Step = BML_BO01_Z_UT01_DN_POS_MOVE_DN;
			}
		}
		break;
	case BML_BO01_Z_UT01_DN_POS_MOVE_DN:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos);
		m_elBoxMoveToLT01Step = BML_BO01_Z_UT01_DN_POS_MOVE_DN_CHECK;
		break;
	case BML_BO01_Z_UT01_DN_POS_MOVE_DN_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_CYL_FWD_S2;
			}
		}
		break;
	case BML_BO01_CYL_FWD_S2:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylFwd();
		m_elBoxMoveToLT01Step = BML_BO01_CYL_FWD_CHECK_S2;
		break;
	case BML_BO01_CYL_FWD_CHECK_S2:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In00_Sn_BoxLeftCylinderForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In04_Sn_BoxRightCylinderForward == ON)
			{
				m_elBoxMoveToLT01Step = BML_BO01_Z_READY_1STEPMOVE;
			}
		}
		break;
//정의천 2015.04.08 추가,변경
	case BML_BO01_Z_READY_1STEPMOVE:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos - UT01_1STEP_OFFSET);
		m_elBoxMoveToLT01Step = BML_BO01_Z_READY_1STEPMOVE_CHECK;
		break;
	case BML_BO01_Z_READY_1STEPMOVE_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				(m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos - UT01_1STEP_OFFSET)) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_BOX_DETECT;
			}
		}
		break;
	case BML_BO01_BOX_DETECT:
		if (G_STMACHINE_STATUS.stModuleStatus.In02_Sn_BoxDetection == ON)
		{
			m_elBoxMoveToLT01Step = BML_BO01_VAC_ON;
		}
		else
		{
			strLogMessage.Format("BO01 : 박스를 감지하지 못했습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_BO01_BOX_DETECT;
			m_elBoxMoveToLT01Step = BML_SEQUENCE_COMP;
		}
		break;
	case BML_BO01_VAC_ON:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnVacOn();
		m_elBoxMoveToLT01Step = BML_BO01_VAC_ON_CHECK;
		break;
	case BML_BO01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[BO01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In03_Sn_BoxLeftVacuum == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In06_Sn_BoxRightVacuum == ON)
			{
				m_elBoxMoveToLT01Step = BML_BO01_Z_READY_MOVE_S2;
			}
		}
		break;
//////////////////////////////
	case BML_BO01_Z_READY_MOVE_S2:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_Ready_Pos);
		m_elBoxMoveToLT01Step = BML_BO01_Z_READY_CHECK_S2;
		break;
	case BML_BO01_Z_READY_CHECK_S2:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_LT01_POS_MOVE;
			}
		}
		break;

	case BML_BO01_LT01_POS_MOVE:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_LT01();
		g_cpMainDlg->m_cBoxOpener->m_fnBO01DriveMove(nBO01DrivePos);
		m_elBoxMoveToLT01Step = BML_BO01_LT01_POS_CHECK;
		break;
	case BML_BO01_LT01_POS_CHECK:
		nBO01DrivePos = m_fnGet_BO01_Drive_Pos_At_LT01();

		if (G_MOTION_STATUS[BO01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
				nBO01DrivePos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_Z_LT01_DN_POS_MOVE_1STEP_DN;
			}
		}
		break;
// 정의천 2015.04.08 추가 / 변경
	case BML_BO01_Z_LT01_DN_POS_MOVE_1STEP_DN:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos - LT01_1STEP_OFFSET);
		m_elBoxMoveToLT01Step = BML_BO01_Z_LT01_DN_POS_MOVE_1STEP_DN_CHECK;
		break;
	case BML_BO01_Z_LT01_DN_POS_MOVE_1STEP_DN_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				(m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos - LT01_1STEP_OFFSET)) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_VAC_OFF;
			}
		}
		break;
	case BML_BO01_VAC_OFF:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnVacOff();
		m_elBoxMoveToLT01Step = BML_BO01_VAC_PURGE;
		break;
// PURGE 추가
	case BML_BO01_VAC_PURGE:
		strLogMessage.Format("BO01 : 퍼지 동작 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnPurge();
		m_elBoxMoveToLT01Step = BML_BO01_VAC_OFF_CHECK;
	case BML_BO01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[BO01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In03_Sn_BoxLeftVacuum == OFF &&
				G_STMACHINE_STATUS.stModuleStatus.In06_Sn_BoxRightVacuum == OFF)
			{
				m_elBoxMoveToLT01Step = BML_BO01_Z_LT01_DN_POS_MOVE_DN;
			}
		}
		break;
//////////////////////////////////////////////////////////////////////
	case BML_BO01_Z_LT01_DN_POS_MOVE_DN:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos);
		m_elBoxMoveToLT01Step = BML_BO01_Z_LT01_DN_POS_MOVE_DN_CHECK;
		break;
	case BML_BO01_Z_LT01_DN_POS_MOVE_DN_CHECK:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_CYL_BWD_S2;
			}
		}
		break;
	case BML_BO01_CYL_BWD_S2:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylBwd();
		m_elBoxMoveToLT01Step = BML_BO01_CYL_BWD_CHECK_S2;
		break;
	case BML_BO01_CYL_BWD_CHECK_S2:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In01_Sn_BoxLeftCylinderBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In05_Sn_BoxRightCylinderBackward == ON)
			{
				m_elBoxMoveToLT01Step = BML_BO01_Z_READY_MOVE_S3;
			}
		}
		break;
	case BML_BO01_Z_READY_MOVE_S3:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01UpDnMove(m_st_BO01_Pos->nZ_Ready_Pos);
		m_elBoxMoveToLT01Step = BML_BO01_Z_READY_CHECK_S3;
		break;
	case BML_BO01_Z_READY_CHECK_S3:
		if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
				m_st_BO01_Pos->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_CYL_FWD_S3;
			}
		}
		break;
	case BML_BO01_CYL_FWD_S3:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylFwd();
		m_elBoxMoveToLT01Step = BML_BO01_CYL_FWD_CHECK_S3;
		break;
	case BML_BO01_CYL_FWD_CHECK_S3:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In00_Sn_BoxLeftCylinderForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In04_Sn_BoxRightCylinderForward == ON)
			{
				m_elBoxMoveToLT01Step = BML_BO01_PAD_TURN_DN;
			}
		}
		break;
	case BML_BO01_PAD_TURN_DN:
		g_cpMainDlg->m_cBoxOpener->m_fnBO01RotateMove(m_st_BO01_Pos->nR_Pad_Dn_Pos);
		m_elBoxMoveToLT01Step = BML_BO01_PAD_TURN_DN_CHECK;
		break;
	case BML_BO01_PAD_TURN_DN_CHECK:
		if (G_MOTION_STATUS[BO01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate] -
				m_st_BO01_Pos->nR_Pad_Dn_Pos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_BO01_CYL_BWD_S3;
			}
		}
		break;
	case BML_BO01_CYL_BWD_S3:
		g_cpMainDlg->m_cBoxOpener->OnBnClickedBtnCylBwd();
		m_elBoxMoveToLT01Step = BML_BO01_CYL_BWD_CHECK_S3;
		break;
	case BML_BO01_CYL_BWD_CHECK_S3:
		if (G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In01_Sn_BoxLeftCylinderBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In05_Sn_BoxRightCylinderBackward == ON)
			{
				m_elBoxMoveToLT01Step = BML_LT01_GUIDE_FWD;
			}
		}
		break;
	case BML_LT01_GUIDE_FWD:
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnCylFwd();
		m_elBoxMoveToLT01Step = BML_LT01_GUIDE_FWD_CHECK;
		break;

	case BML_LT01_GUIDE_FWD_CHECK:
		if (G_MOTION_STATUS[LT01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In11_Sn_LoadLeftAlignForward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In13_Sn_LoadRightAlignForward == ON)
			{
				m_elBoxMoveToLT01Step = BML_LT01_PUSHER_FWD;
			}
		}
		break;
	case BML_LT01_PUSHER_FWD:
		nLt01DrivePos = m_fnGet_LT01_PusherPos();

		g_cpMainDlg->m_cLoadingTable->m_fnLT01PusherMove(nLt01DrivePos);
		m_elBoxMoveToLT01Step = BML_LT01_PUSHER_FWD_CHECK;
		break;
	case BML_LT01_PUSHER_FWD_CHECK:
		nLt01DrivePos = m_fnGet_LT01_PusherPos();

		if (G_MOTION_STATUS[LT01_PUSHER].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush] -
				nLt01DrivePos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_LT01_VAC_ON;
			}
		}

		break;
	case BML_LT01_VAC_ON:
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnVacOn();
		m_elBoxMoveToLT01Step = BML_LT01_VAC_ON_CHECK;
		break;

	case BML_LT01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[LT01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In10_Sn_LoadBoxVacuum == ON)
			{
				m_elBoxMoveToLT01Step = BML_LT01_PUSHER_BWD;
			}
		}
		break;

	case BML_LT01_PUSHER_BWD:
		g_cpMainDlg->m_cLoadingTable->m_fnLT01PusherMove(m_st_LT01_Pos->nGuide_Ready_Pos);
		m_elBoxMoveToLT01Step = BML_LT01_PUSHER_BWD_CHECK;
		break;

	case BML_LT01_PUSHER_BWD_CHECK:
		if (G_MOTION_STATUS[LT01_PUSHER].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush] -
				m_st_LT01_Pos->nGuide_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elBoxMoveToLT01Step = BML_LT01_GUIDE_BWD;
			}
		}
		break;
	case BML_LT01_GUIDE_BWD:
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnCylBwd();
		m_elBoxMoveToLT01Step = BML_LT01_GUIDE_BWD_CHECK;
		break;

	case BML_LT01_GUIDE_BWD_CHECK:
		if (G_MOTION_STATUS[LT01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In12_Sn_LoadLeftAlignBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In14_Sn_LoadRightAlignBackward == ON)
			{
				m_elBoxMoveToLT01Step = BML_LT01_VAC_OFF;
			}
		}
		break;
	case BML_LT01_VAC_OFF:
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnVacOff();
		m_elBoxMoveToLT01Step = BML_LT01_VAC_PURGE;
		break;
//정의천 2015.04.08 추가
// LT01 PURGE
	case BML_LT01_VAC_PURGE:
		strLogMessage.Format("LT01 : 퍼지 동작 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cLoadingTable->OnBnClickedBtnPurge();
		m_elBoxMoveToLT01Step = BML_LT01_VAC_OFF_CHECK;
////////////////////////////////////////////////////////////////////////
	case BML_LT01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[LT01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In10_Sn_LoadBoxVacuum == OFF)
			{
				m_elBoxMoveToLT01Step = BML_SEQUENCE_COMP;
			}
		}
		break;
	case BML_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;

			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}

		g_cpMainDlg->m_elBoxSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_UT01_T_LT01] = SEQ_COMPLETE;

		m_elSeq_Error = ERR_SEQ_NONE;
		m_elBoxMoveToLT01Step = BML_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}


	return 0;
}

int CBoxControl::m_fnBoxMove_F_LT01_T_BT01()
{
	CString strLogMessage;
	int nBoxIndex = 0;
	int nUpDnPos = 0;

	switch (m_elBoxUnloadingStep)
	{
	case BU_MAIN_SEQUENCE_IDLE:
		break;
	case BU_TM01_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;
		
		G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_LT01_T_BT01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			//TM02 가 TT01 포지션에 있어야 한다.
			m_elBoxUnloadingStep = BU_BT01_TARGET_POS_MOVE;
			strLogMessage.Format("TM01 : 박스 배출 시나리오 시작.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elBoxUnloadingStep = BU_SEQUENCE_COMP;
			strLogMessage.Format("TM01 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}

		break;
	case BU_BT01_TARGET_POS_MOVE:
		//nBoxIndex = m_fnGetNextBoxIndex(BOX_RUN);
		nBoxIndex = m_unCurrentBoxIndex;
		if (nBoxIndex == 0)
		{
			strLogMessage.Format("작업중이 슬롯이 없습니다..");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			m_elSeq_Error = ERR_BT01_BOX_STATUS;
			m_elBoxUnloadingStep = BU_SEQUENCE_COMP;
		}
		else
		{
			nUpDnPos = m_fnGetNextBoxPos(nBoxIndex);
			g_cpMainDlg->m_cBoxTable->m_fnBT01UpDnMove(nUpDnPos);
			m_elBoxUnloadingStep = BU_BT01_TARGET_POS_CHECK;
		}
		break;
	case BU_BT01_TARGET_POS_CHECK:
		//nBoxIndex = m_fnGetNextBoxIndex(BOX_RUN);
		nBoxIndex = m_unCurrentBoxIndex;
		nUpDnPos = m_fnGetNextBoxPos(nBoxIndex);

		if (G_MOTION_STATUS[BT01_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
				nUpDnPos) <= POSITION_OFFSET)
			{				
				m_elBoxUnloadingStep = BU_TM01_FORK_BWD_S1;
			}
		}
		break;
	case BU_TM01_FORK_BWD_S1:
		g_cpMainDlg->m_cBoxTransper->m_fnTM01ForkMove(m_st_TM01_Pos->nTM01_Ready_Pos);
		m_elBoxUnloadingStep = BU_TM01_FORK_BWD_CHECK_S1;
		break;
	case BU_TM01_FORK_BWD_CHECK_S1:
		if (G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
				m_st_TM01_Pos->nTM01_Ready_Pos) <= POSITION_OFFSET)
			{				
				m_elBoxUnloadingStep = BU_TM01_CYLINDER_UP;
			}
		}
		break;
	case BU_TM01_CYLINDER_UP:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnCylUp();
		m_elBoxUnloadingStep = BU_TM01_CYLINDER_UP_CHECK;
		break;
	case BU_TM01_CYLINDER_UP_CHECK:
		if (G_MOTION_STATUS[TM01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In23_Sn_ForkFrontUp == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In25_Sn_ForkRearUp == ON)
			{
				m_elBoxUnloadingStep = BU_TM01_BOX_DETECT_CHECK;
			}
		}
		break;
	case BU_TM01_BOX_DETECT_CHECK:
		if (G_STMACHINE_STATUS.stModuleStatus.In21_Sn_ForkBoxDetection == ON)
		{
			//m_elBoxUnloadingStep = BU_TM01_VAC_ON;
			m_elBoxUnloadingStep = BU_TM01_FORK_FWD;
		}
		else
		{
			strLogMessage.Format("TM01 : 박스가 감지되지 않습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_TM01_BOX_DETECT;
			m_elBoxUnloadingStep = BU_TM01_CYL_DN; //박스가 감지되지 않으면 바로 포크를 후진한다.
		}
		break;
	case BU_TM01_VAC_ON:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnVacOn();
		m_elBoxUnloadingStep = BU_TM01_VAC_ON_CHECK;
		break;
	case BU_TM01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[TM01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In22_Sn_ForkBoxVacuum == ON)
			{
				m_elBoxUnloadingStep = BU_TM01_FORK_FWD;
			}			
		}
		break;
	case BU_TM01_FORK_FWD:
		g_cpMainDlg->m_cBoxTransper->m_fnTM01ForkMove(m_st_TM01_Pos->nBT01_FwdPos);
		m_elBoxUnloadingStep = BU_TM01_FORK_FWD_CHECK;
		break;
	case BU_TM01_FORK_FWD_CHECK:
		if (G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
				m_st_TM01_Pos->nBT01_FwdPos) <= POSITION_OFFSET)
			{
				m_elBoxUnloadingStep = BU_TM01_VAC_OFF;
			}
		}
		break;
	case BU_TM01_VAC_OFF:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnVacOff();
		m_elBoxUnloadingStep = BU_TM01_PURGE;
		break;

	case BU_TM01_PURGE:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnPurge();
		m_elBoxUnloadingStep = BU_TM01_VAC_OFF_CHECK;
		break;
	case BU_TM01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TM01_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In22_Sn_ForkBoxVacuum == OFF)
			{
				m_elBoxUnloadingStep = BU_TM01_CYL_DN;
			}
			else
			{
				strLogMessage.Format("TM01 : VACUUM OFF 에러가 발생했습니다.");
				g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
				m_elSeq_Error = ERR_TM01_VAC_OFF;
				m_elBoxUnloadingStep = BU_TM01_FORK_BWD_S2;
			}
		}
		break;
	case BU_TM01_CYL_DN:
		g_cpMainDlg->m_cBoxTransper->OnBnClickedBtnCylDn();
		m_elBoxUnloadingStep = BU_TM01_CYL_DN_CHECK;
		break;
	case BU_TM01_CYL_DN_CHECK:
		if (G_MOTION_STATUS[TM01_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stModuleStatus.In24_Sn_ForkFrontDown == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In26_Sn_ForkRearDown == ON)
			{
				m_elBoxUnloadingStep = BU_TM01_FORK_BWD_S2;				
			}
		}
		break;
	case BU_TM01_FORK_BWD_S2:
		g_cpMainDlg->m_cBoxTransper->m_fnTM01ForkMove(m_st_TM01_Pos->nTM01_Ready_Pos);
		m_elBoxUnloadingStep = BU_TM01_FORK_BWD_CHECK_S2;
		break;
	case BU_TM01_FORK_BWD_CHECK_S2:
		nBoxIndex = m_unCurrentBoxIndex;

		if (G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
				m_st_TM01_Pos->nTM01_Ready_Pos) <= POSITION_OFFSET)
			{
				if (m_elSeq_Error == ERR_SEQ_NONE)
					g_cpMainDlg->m_fnBoxStatusChange((BOX_LIST)nBoxIndex, BOX_COMP);

				m_elBoxUnloadingStep = BU_SEQUENCE_COMP;				
			}
		}		
		break;
	case BU_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;

			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}

		g_cpMainDlg->m_elBoxSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_LT01_T_BT01] = SEQ_COMPLETE;

		m_elSeq_Error = ERR_SEQ_NONE;
		m_elBoxUnloadingStep = BU_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

bool CBoxControl::m_fnLoadingTableBoxSizeCheck()
{
	if (G_STMACHINE_STATUS.stModuleStatus.In07_Sn_LoadBoxSmallDetection == ON)
		m_elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stModuleStatus.In08_Sn_LoadBoxMediumDetection == ON)
		m_elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stModuleStatus.In09_Sn_LoadBoxLargeDetection == ON)
		m_elBoxSize = BOX_SIZE_LARGE;

	if (G_BOX_STATUS[BAD_BOX].elBoxSize == m_elBoxSize) //둘이 동일 하다면, 정상.
	{
		return true;
	}

	return false;
}

int CBoxControl::m_fnGetNextBoxIndex(BOX_STATUS elBoxStatus)
{
	int nBoxIndex = 0;

	for (int bl = (int)BOX7; bl > (int)BAD_BOX; bl--)
	{
		if (G_BOX_STATUS[bl].elBoxStatus == elBoxStatus)
		{
			nBoxIndex = bl;
			break;
		}
	}	

	return nBoxIndex;
}

int CBoxControl::m_fnGetNextBoxPos(int nBoxIndex)
{
	int nBoxPosition = 0;
	switch (nBoxIndex)
	{
	case 1:
		nBoxPosition = m_st_BT01_Pos->nBox1Position;
		break;
	case 2:
		nBoxPosition = m_st_BT01_Pos->nBox2Position;
		break;
	case 3:
		nBoxPosition = m_st_BT01_Pos->nBox3Position;
		break;
	case 4:
		nBoxPosition = m_st_BT01_Pos->nBox4Position;
		break;
	case 5:
		nBoxPosition = m_st_BT01_Pos->nBox5Position;
		break;
	case 6:
		nBoxPosition = m_st_BT01_Pos->nBox6Position;
		break;
	case 7:
		nBoxPosition = m_st_BT01_Pos->nBox7Position;
		break;
	default:
		break;
	}

	return nBoxPosition;
}

int CBoxControl::m_fnGet_LT01_PusherPos()
{
	switch (m_elBoxSize)
	{
	case BOX_SIZE_NONE:
		break;
	case BOX_SIZE_SMALL:
		return m_st_LT01_Pos->nGuide_Small_Pos;
		break;
	case BOX_SIZE_MEDIUM:
		return m_st_LT01_Pos->nGuide_Mid_Pos;
		break;
	case BOX_SIZE_LARGE:
		return m_st_LT01_Pos->nGuide_Large_Pos;
		break;
	default:
		break;
	}

	return m_st_LT01_Pos->nGuide_Ready_Pos;
}

int CBoxControl::m_fnGet_UT01_PusherPos()
{
	switch (m_elBoxSize)
	{
	case BOX_SIZE_NONE:
		break;
	case BOX_SIZE_SMALL:
		return m_st_UT01_Pos->nGuide_Small_Pos;
		break;
	case BOX_SIZE_MEDIUM:
		return m_st_UT01_Pos->nGuide_Mid_Pos;
		break;
	case BOX_SIZE_LARGE:
		return m_st_UT01_Pos->nGuide_Large_Pos;
		break;
	default:
		break;
	}

	return m_st_UT01_Pos->nGuide_Ready_Pos;
}

int CBoxControl::m_fnGet_BO01_Drive_Pos_At_LT01()
{
	//정의천  2015.04.08
	if (G_STMACHINE_STATUS.stModuleStatus.In07_Sn_LoadBoxSmallDetection == ON)
		m_elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stModuleStatus.In08_Sn_LoadBoxMediumDetection == ON)
		m_elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stModuleStatus.In09_Sn_LoadBoxLargeDetection == ON)
		m_elBoxSize = BOX_SIZE_LARGE;

	switch (m_elBoxSize)
	{
	case BOX_SIZE_NONE:
		break;
	case BOX_SIZE_SMALL:
		return m_st_BO01_Pos->nDrive_LT01_Small_Pos;
		break;
	case BOX_SIZE_MEDIUM:
		return m_st_BO01_Pos->nDrive_LT01_Mid_Pos;
		break;
	case BOX_SIZE_LARGE:
		return m_st_BO01_Pos->nDrive_LT01_Large_Pos;
		break;
	default:
		break;
	}

	return 0;
}

int CBoxControl::m_fnGet_BO01_Drive_Pos_At_UT01()
{
	switch (m_elBoxSize)
	{
	case BOX_SIZE_NONE:
		break;
	case BOX_SIZE_SMALL:
		return m_st_BO01_Pos->nDrive_UT01_Small_Pos;
		break;
	case BOX_SIZE_MEDIUM:
		return m_st_BO01_Pos->nDrive_UT01_Mid_Pos;
		break;
	case BOX_SIZE_LARGE:
		return m_st_BO01_Pos->nDrive_UT01_Large_Pos;
		break;
	default:
		break;
	}

	return 0;
}

void CBoxControl::m_fnSet_Current_Box_Index(UINT unBoxIndex/*7~1*/)
{
	m_unCurrentBoxIndex = unBoxIndex;
}