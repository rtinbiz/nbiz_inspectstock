// BoxOpener.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "BoxOpener.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CBoxOpener 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBoxOpener, CDialogEx)

CBoxOpener::CBoxOpener(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBoxOpener::IDD, pParent)
	, m_nCtrDriveIndex(0)
	, m_nCtrUpDnIndex(0)
	, m_nCtrRotateIndex(0)
{

}

CBoxOpener::~CBoxOpener()
{
}

void CBoxOpener::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_POS_MOVE, m_ctrBtnMove);
	DDX_Control(pDX, IDC_BTN_POS_DN, m_ctrBtnMoveDn);
	DDX_Control(pDX, IDC_BTN_POS_UP, m_ctrBtnMoveUp);
	DDX_Control(pDX, IDC_BTN_SAVE3, m_ctrBtnValueSave);
	DDX_Control(pDX, IDC_BTN_CANCEL3, m_ctrBtnSaveCancel);
	DDX_Control(pDX, IDC_BTN_INIT3, m_ctrBtnMoveDefault);
	DDX_Control(pDX, IDC_BTN_Z_POS_MOVE, m_ctrBtnMoveZ);
	DDX_Control(pDX, IDC_BTN_Z_POS_DN, m_ctrBtnMoveDnZ);
	DDX_Control(pDX, IDC_BTN_Z_POS_UP, m_ctrBtnMoveUpZ);
	DDX_Control(pDX, IDC_BTN_PAD_POS_MOVE, m_ctrBtnMoveR);
	DDX_Control(pDX, IDC_BTN_PAD_POS_DN, m_ctrBtnMoveDnR);
	DDX_Control(pDX, IDC_BTN_PAD_POS_UP, m_ctrBtnMoveUpR);
	DDX_Control(pDX, IDC_BTN_CYL_FWD, m_ctrBtnCylFwd);
	DDX_Control(pDX, IDC_BTN_CYL_BWD, m_ctrBtnCylBwd);
	DDX_Control(pDX, IDC_BTN_VAC_ON, m_ctrBtnVacOn);
	DDX_Control(pDX, IDC_BTN_VAC_OFF, m_ctrBtnVacOff);
	DDX_Control(pDX, IDC_BTN_PURGE, m_ctrBtnPurge);
	DDX_Radio(pDX, IDC_RADIO_LT01_S, m_nCtrDriveIndex);
	DDX_Radio(pDX, IDC_RADIO_LT01_UP, m_nCtrUpDnIndex);
	DDX_Radio(pDX, IDC_RADIO_PAD_UP, m_nCtrRotateIndex);
	DDX_Control(pDX, IDC_BTN_MOTION_STOP, m_ctrBtnBO01ActionStop);
}


BEGIN_MESSAGE_MAP(CBoxOpener, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_SAVE3, &CBoxOpener::OnBnClickedBtnSave3)
	ON_BN_CLICKED(IDC_BTN_CANCEL3, &CBoxOpener::OnBnClickedBtnCancel3)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE, &CBoxOpener::OnBnClickedBtnPosMove)
	ON_BN_CLICKED(IDC_BTN_POS_DN, &CBoxOpener::OnBnClickedBtnPosDn)
	ON_BN_CLICKED(IDC_BTN_POS_UP, &CBoxOpener::OnBnClickedBtnPosUp)
	ON_BN_CLICKED(IDC_BTN_Z_POS_MOVE, &CBoxOpener::OnBnClickedBtnZPosMove)
	ON_BN_CLICKED(IDC_BTN_Z_POS_DN, &CBoxOpener::OnBnClickedBtnZPosDn)
	ON_BN_CLICKED(IDC_BTN_Z_POS_UP, &CBoxOpener::OnBnClickedBtnZPosUp)
	ON_BN_CLICKED(IDC_BTN_PAD_POS_MOVE, &CBoxOpener::OnBnClickedBtnPadPosMove)
	ON_BN_CLICKED(IDC_BTN_PAD_POS_DN, &CBoxOpener::OnBnClickedBtnPadPosDn)
	ON_BN_CLICKED(IDC_BTN_PAD_POS_UP, &CBoxOpener::OnBnClickedBtnPadPosUp)
	ON_BN_CLICKED(IDC_BTN_CYL_FWD, &CBoxOpener::OnBnClickedBtnCylFwd)
	ON_BN_CLICKED(IDC_BTN_CYL_BWD, &CBoxOpener::OnBnClickedBtnCylBwd)
	ON_BN_CLICKED(IDC_BTN_VAC_ON, &CBoxOpener::OnBnClickedBtnVacOn)
	ON_BN_CLICKED(IDC_BTN_VAC_OFF, &CBoxOpener::OnBnClickedBtnVacOff)
	ON_BN_CLICKED(IDC_BTN_PURGE, &CBoxOpener::OnBnClickedBtnPurge)
	ON_BN_CLICKED(IDC_BTN_INIT3, &CBoxOpener::OnBnClickedBtnInit3)
	ON_BN_CLICKED(IDC_BTN_MOTION_STOP, &CBoxOpener::OnBnClickedBtnMotionStop)
END_MESSAGE_MAP()


// CBoxOpener 메시지 처리기입니다.
BOOL CBoxOpener::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CBoxOpener::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_EditFont.DeleteObject();
	CDialogEx::PostNcDestroy();
}


HBRUSH CBoxOpener::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CBoxOpener::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style On
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);

	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyleOff.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	m_ctrBtnMove.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnMove.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	m_ctrBtnCylFwd.SetTextColor(&tColor);
	m_ctrBtnCylFwd.SetCheckButton(false);			
	m_ctrBtnCylFwd.SetFont(&tFont);
	m_ctrBtnCylFwd.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnCylBwd.SetTextColor(&tColor);
	m_ctrBtnCylBwd.SetCheckButton(false);			
	m_ctrBtnCylBwd.SetFont(&tFont);
	m_ctrBtnCylBwd.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOn.SetTextColor(&tColor);
	m_ctrBtnVacOn.SetCheckButton(false);			
	m_ctrBtnVacOn.SetFont(&tFont);
	m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOff.SetTextColor(&tColor);
	m_ctrBtnVacOff.SetCheckButton(false);			
	m_ctrBtnVacOff.SetFont(&tFont);
	m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnPurge.SetTextColor(&tColor);
	m_ctrBtnPurge.SetCheckButton(false);			
	m_ctrBtnPurge.SetFont(&tFont);
	m_ctrBtnPurge.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnMove.SetTextColor(&tColor);
	m_ctrBtnMove.SetCheckButton(false);			
	m_ctrBtnMove.SetFont(&tFont);
	m_ctrBtnMove.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDn.SetTextColor(&tColor);
	m_ctrBtnMoveDn.SetCheckButton(false);			
	m_ctrBtnMoveDn.SetFont(&tFont);
	m_ctrBtnMoveDn.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUp.SetTextColor(&tColor);
	m_ctrBtnMoveUp.SetCheckButton(false);			
	m_ctrBtnMoveUp.SetFont(&tFont);
	m_ctrBtnMoveUp.SetRoundButtonStyle(&m_tStyle1);	


	m_ctrBtnMoveZ.SetTextColor(&tColor);
	m_ctrBtnMoveZ.SetCheckButton(false);			
	m_ctrBtnMoveZ.SetFont(&tFont);
	m_ctrBtnMoveZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDnZ.SetTextColor(&tColor);
	m_ctrBtnMoveDnZ.SetCheckButton(false);			
	m_ctrBtnMoveDnZ.SetFont(&tFont);
	m_ctrBtnMoveDnZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUpZ.SetTextColor(&tColor);
	m_ctrBtnMoveUpZ.SetCheckButton(false);			
	m_ctrBtnMoveUpZ.SetFont(&tFont);
	m_ctrBtnMoveUpZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveR.SetTextColor(&tColor);
	m_ctrBtnMoveR.SetCheckButton(false);			
	m_ctrBtnMoveR.SetFont(&tFont);
	m_ctrBtnMoveR.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDnR.SetTextColor(&tColor);
	m_ctrBtnMoveDnR.SetCheckButton(false);			
	m_ctrBtnMoveDnR.SetFont(&tFont);
	m_ctrBtnMoveDnR.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUpR.SetTextColor(&tColor);
	m_ctrBtnMoveUpR.SetCheckButton(false);			
	m_ctrBtnMoveUpR.SetFont(&tFont);
	m_ctrBtnMoveUpR.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnValueSave.SetTextColor(&tColor);
	m_ctrBtnValueSave.SetCheckButton(false);			
	m_ctrBtnValueSave.SetFont(&tFont);
	m_ctrBtnValueSave.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnSaveCancel.SetTextColor(&tColor);
	m_ctrBtnSaveCancel.SetCheckButton(false);			
	m_ctrBtnSaveCancel.SetFont(&tFont);
	m_ctrBtnSaveCancel.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDefault.SetTextColor(&tColor);
	m_ctrBtnMoveDefault.SetCheckButton(false);			
	m_ctrBtnMoveDefault.SetFont(&tFont);
	m_ctrBtnMoveDefault.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnBO01ActionStop.SetTextColor(&tColor);	m_ctrBtnBO01ActionStop.SetCheckButton(false);
	m_ctrBtnBO01ActionStop.SetFont(&tFont);	m_ctrBtnBO01ActionStop.SetRoundButtonStyle(&m_tStyle1);

}

int CBoxOpener::m_fnCreateControlFont(void)
{	
// 	GetDlgItem(IDC_EDIT_BAD_POS)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS1)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS2)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS3)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS4)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS5)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS6)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS7)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_READY_POS)->SetFont(&m_EditFont);

	//SetDlgItemInt(IDC_EDIT_Z_POS_UP, 100);
	//SetDlgItemInt(IDC_EDIT_Z_POS_DN, 100);
	return 0;
}

BOOL CBoxOpener::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);
	m_EditFont.CreatePointFont(100,"Arial");	

	m_fnCreateControlFont();
	m_fnCreateButton();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CBoxOpener::m_fnSetParameter(SBO01_POSITION* stPointer)
{
	m_st_BO01_Pos = stPointer;	
	return 0;
}

int CBoxOpener::m_fnDisplayParameter(void)
{
	SetDlgItemInt(IDC_EDIT_LT01_SPOS, m_st_BO01_Pos->nDrive_LT01_Small_Pos);
	SetDlgItemInt(IDC_EDIT_LT01_MPOS, m_st_BO01_Pos->nDrive_LT01_Mid_Pos);
	SetDlgItemInt(IDC_EDIT_LT01_LPOS, m_st_BO01_Pos->nDrive_LT01_Large_Pos);
	SetDlgItemInt(IDC_EDIT_UT01_SPOS, m_st_BO01_Pos->nDrive_UT01_Small_Pos);
	SetDlgItemInt(IDC_EDIT_UT01_MPOS, m_st_BO01_Pos->nDrive_UT01_Mid_Pos);
	SetDlgItemInt(IDC_EDIT_UT01_LPOS, m_st_BO01_Pos->nDrive_UT01_Large_Pos);
	SetDlgItemInt(IDC_EDIT_READY_POS, m_st_BO01_Pos->nZ_Ready_Pos);
	SetDlgItemInt(IDC_EDIT_LT01_UP_POS, m_st_BO01_Pos->nZ_LT01_Up_Cover_Pos);
	SetDlgItemInt(IDC_EDIT_LT01_DN_POS, m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos);
	SetDlgItemInt(IDC_EDIT_UT01_UP_POS, m_st_BO01_Pos->nZ_UT01_Up_Cover_Pos);
	SetDlgItemInt(IDC_EDIT_UT01_DN_POS, m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos);
	SetDlgItemInt(IDC_EDIT_PAD12_POS, m_st_BO01_Pos->nR_Pad_Up_Pos);
	SetDlgItemInt(IDC_EDIT_PAD6_POS, m_st_BO01_Pos->nR_Pad_Dn_Pos);
	SetDlgItemInt(IDC_EDIT_PURGE_SEC, m_st_BO01_Pos->nPurge_Time);
	return 0;
}

void CBoxOpener::OnBnClickedBtnCancel3()
{
	m_fnDisplayParameter();
}

void CBoxOpener::OnBnClickedBtnSave3()
{
	CString strWriteData;

	m_st_BO01_Pos->nDrive_LT01_Small_Pos = GetDlgItemInt(IDC_EDIT_LT01_SPOS);
	m_st_BO01_Pos->nDrive_LT01_Mid_Pos = GetDlgItemInt(IDC_EDIT_LT01_MPOS);	
	m_st_BO01_Pos->nDrive_LT01_Large_Pos = GetDlgItemInt(IDC_EDIT_LT01_LPOS);
	m_st_BO01_Pos->nDrive_UT01_Small_Pos = GetDlgItemInt(IDC_EDIT_UT01_SPOS);	
	m_st_BO01_Pos->nDrive_UT01_Mid_Pos = GetDlgItemInt(IDC_EDIT_UT01_MPOS);	
	m_st_BO01_Pos->nDrive_UT01_Large_Pos = GetDlgItemInt(IDC_EDIT_UT01_LPOS);	
	m_st_BO01_Pos->nZ_Ready_Pos = GetDlgItemInt(IDC_EDIT_READY_POS);	
	m_st_BO01_Pos->nZ_LT01_Up_Cover_Pos = GetDlgItemInt(IDC_EDIT_LT01_UP_POS);	
	m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos = GetDlgItemInt(IDC_EDIT_LT01_DN_POS);	
	m_st_BO01_Pos->nZ_UT01_Up_Cover_Pos = GetDlgItemInt(IDC_EDIT_UT01_UP_POS);	
	m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos = GetDlgItemInt(IDC_EDIT_UT01_DN_POS);	
	m_st_BO01_Pos->nR_Pad_Up_Pos = GetDlgItemInt(IDC_EDIT_PAD12_POS);	
	m_st_BO01_Pos->nR_Pad_Dn_Pos = GetDlgItemInt(IDC_EDIT_PAD6_POS);	
	m_st_BO01_Pos->nPurge_Time = GetDlgItemInt(IDC_EDIT_PURGE_SEC);	

	strWriteData.Format("%d", m_st_BO01_Pos->nDrive_LT01_Small_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_LT01_SMALL, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nDrive_UT01_Small_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_UT01_SMALL, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nDrive_LT01_Mid_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_LT01_MID, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nDrive_UT01_Mid_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_UT01_MID, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nDrive_LT01_Large_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_LT01_LARGE, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nDrive_UT01_Large_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_UT01_LARGE, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nZ_Ready_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_READY_POS, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nZ_LT01_Up_Cover_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_LT01_UP_COVER, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nZ_LT01_Dn_Cover_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_LT01_DN_COVER, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nZ_UT01_Up_Cover_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_UT01_UP_COVER, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nZ_UT01_Dn_Cover_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_UT01_DN_COVER, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nR_Pad_Up_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_R_PAD_UP, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nR_Pad_Dn_Pos);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_R_PAD_DN, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BO01_Pos->nPurge_Time);
	WritePrivateProfileString(SECTION_BO01_POS_DATA, KEY_PURGE_TIME, strWriteData, SYSTEM_PARAM_INI_PATH);
}

void CBoxOpener::OnBnClickedBtnPosMove()
{
	UpdateData(TRUE);	
	int nPosValue = 0;
	CString strLogMessage;

	switch(m_nCtrDriveIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_LT01_SPOS);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_UT01_SPOS);	break;	
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_LT01_MPOS);	break;	
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_UT01_MPOS);	break;	
	case 4:
		nPosValue = GetDlgItemInt(IDC_EDIT_LT01_LPOS);	break;	
	case 5:
		nPosValue = GetDlgItemInt(IDC_EDIT_UT01_LPOS);	break;	
	}
	
	if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == BUSY)
	{
		strLogMessage.Format("BO01_DRIVE : BO01_UPDN 축이 이동 중입니다. 정지 후 재 실행 하십시요.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		return;
	}
	//else if (G_MOTION_STATUS[BO01_ROTATE].B_COMMAND_STATUS == BUSY)
	//{
	//	strLogMessage.Format("BO01_DRIVE : BO01_ROTATE 축이 회전 중입니다. 정지 후 재 실행 하십시요.");
	//	m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);	
	//	return;
	//}
	
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
		m_st_BO01_Pos->nZ_Ready_Pos) > POSITION_OFFSET)
	{
		strLogMessage.Format("BO01_DRIVE : BO01_UPDN 대기 위치에 있지 않습니다.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		return;
	}
	
	m_fnBO01DriveMove(nPosValue);
}


void CBoxOpener::OnBnClickedBtnPosDn()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	nPosValue = nPosValue - nOffsetPos;
	
	m_fnBO01DriveMove(nPosValue);
}

void CBoxOpener::OnBnClickedBtnPosUp()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);	
	nPosValue = nPosValue + nOffsetPos;	

	m_fnBO01DriveMove(nPosValue);
}

void CBoxOpener::OnBnClickedBtnZPosMove()
{
	UpdateData(TRUE);	
	int nPosValue = 0;
	CString strLogMessage;

	switch(m_nCtrUpDnIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_LT01_UP_POS);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_LT01_DN_POS);	break;	
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_UT01_UP_POS);break;	
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_UT01_DN_POS);break;	
	case 4:
		nPosValue = GetDlgItemInt(IDC_EDIT_READY_POS);	break;		
	}
	//정의천
	if (G_MOTION_STATUS[BO01_DRIVE].B_COMMAND_STATUS == BUSY)
	{
		strLogMessage.Format("BO01_UPDN :  BO01_DRIVE 축이 이동 중입니다. 정지 후 재 실행 하십시요.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);		
		return;
	}	

	m_fnBO01UpDnMove(nPosValue , Speed_Pickup);
}

void CBoxOpener::OnBnClickedBtnZPosDn()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_Z_POS_DN);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);	
	nPosValue = nPosValue - nOffsetPos;
	m_fnBO01UpDnMove(nPosValue);	
}

void CBoxOpener::OnBnClickedBtnZPosUp()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_Z_POS_UP);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);	
	nPosValue = nPosValue + nOffsetPos;
	m_fnBO01UpDnMove(nPosValue);		
}

void CBoxOpener::OnBnClickedBtnPadPosMove()
{
	UpdateData(TRUE);	
	int nPosValue = 0;
	CString strLogMessage;

	switch(m_nCtrRotateIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_PAD12_POS);break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_PAD6_POS);	break;		
	}

////
	if (G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS == BUSY)
	{
		strLogMessage.Format("BO01_UPDN 축이 이동 중입니다. 정지 후 재 실행 하십시요.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);		
		return;
	}
    
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
		m_st_BO01_Pos->nZ_Ready_Pos) > POSITION_OFFSET)
	{
		strLogMessage.Format("BO01_UPDN :  Z_Posiotn 이 회전 가능 위치에 있지 않습니다.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		return;
	}	

	m_fnBO01RotateMove(nPosValue);
}

void CBoxOpener::OnBnClickedBtnPadPosDn()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_PAD_POS_DN);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE);
	nPosValue = nPosValue - nOffsetPos;

	m_fnBO01RotateMove(nPosValue);
}

void CBoxOpener::OnBnClickedBtnPadPosUp()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_PAD_POS_UP);

	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE);
	nPosValue = nPosValue + nOffsetPos;

	m_fnBO01RotateMove(nPosValue);	
}

void CBoxOpener::OnBnClickedBtnCylFwd()
{
	m_stBO01_Mess.Reset();

	m_stBO01_Mess.nAction = m_stBO01_Mess.Act_None;
	m_stBO01_Mess.nResult = m_stBO01_Mess.Ret_None;	
	m_stBO01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stBO01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BO01_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BO01_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_BO01_PICKER_FWD, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_BO01_PICKER_FWD, nBiz_Unit_Zero
				);

			strLogMessage.Format("BO01_CYL : FWD.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("BO01_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BO01_CYL : FWD 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxOpener::OnBnClickedBtnCylBwd()
{
	m_stBO01_Mess.Reset();

	m_stBO01_Mess.nAction = m_stBO01_Mess.Act_None;
	m_stBO01_Mess.nResult = m_stBO01_Mess.Ret_None;	
	m_stBO01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stBO01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BO01_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BO01_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_BO01_PICKER_BWD, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_BO01_PICKER_BWD, nBiz_Unit_Zero
				);

			strLogMessage.Format("BO01_CYL : BWD.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("BO01_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BO01_CYL : BWD 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxOpener::OnBnClickedBtnVacOn()
{
	m_stBO01_Mess.Reset();

	m_stBO01_Mess.nAction = m_stBO01_Mess.Act_None;
	m_stBO01_Mess.nResult = m_stBO01_Mess.Ret_None;	
	m_stBO01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stBO01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BO01_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BO01_VAC].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_BO01_Vac_ON, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_BO01_Vac_ON, nBiz_Unit_Zero
				);

			strLogMessage.Format("BO01_VAC : ON.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("BO01_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BO01_VAC : ON 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_VAC].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxOpener::OnBnClickedBtnVacOff()
{
	m_stBO01_Mess.Reset();

	m_stBO01_Mess.nAction = m_stBO01_Mess.Act_None;
	m_stBO01_Mess.nResult = m_stBO01_Mess.Ret_None;	
	m_stBO01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stBO01_Mess, nMessSize);


	CString strLogMessage;

	if(G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BO01_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BO01_VAC].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_BO01_Vac_OFF, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_BO01_Vac_OFF, nBiz_Unit_Zero
				);

			strLogMessage.Format("BO01_VAC : OFF.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("BO01_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BO01_VAC : OFF 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_VAC].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxOpener::OnBnClickedBtnPurge()
{
	int nPurgeTime = GetDlgItemInt(IDC_EDIT_PURGE_SEC);

	m_stBO01_Mess.Reset();

	m_stBO01_Mess.nAction = m_stBO01_Mess.Act_None;
	m_stBO01_Mess.nResult = m_stBO01_Mess.Ret_None;	
	m_stBO01_Mess.ActionTime = nPurgeTime;
	m_stBO01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stBO01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[BO01_PURGE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BO01_PURGE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BO01_PURGE].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BO01_PURGE].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_BO01_Purge, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_BO01_Purge, nBiz_Unit_Zero
				);

			strLogMessage.Format("BO01_PURGE : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("BO01_PURGE : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BO01_PURGE : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_PURGE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BO01_PURGE].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxOpener::m_fnBO01DriveMove(int nPosValue)
{
	CString strLogMessage;

	if(G_MOTION_STATUS[BO01_DRIVE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BO01_DRIVE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BO01_DRIVE].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BO01_DRIVE].UN_CMD_COUNT++;

			m_fnBO01PositionMove(nPosValue, nBiz_Seq_BO01_BoxDrive, TIME_OUT_120_SECOND);

			strLogMessage.Format("BO01_DRIVE : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("BO01_DRIVE : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BO01_DRIVE : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_DRIVE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BO01_DRIVE].UN_CMD_COUNT = 0;
	}
}

void CBoxOpener::m_fnBO01UpDnMove(int nPosValue,int nSpeedMode)
{
	CString strLogMessage;

	if(G_MOTION_STATUS[BO01_UPDN].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BO01_UPDN].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BO01_UPDN].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BO01_UPDN].UN_CMD_COUNT++;

			m_fnBO01PositionMove(nPosValue, nBiz_Seq_BO01_BoxUpDn, TIME_OUT_60_SECOND);

			strLogMessage.Format("BO01_UPDN : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("BO01_UPDN : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BO01_UPDN : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_UPDN].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BO01_UPDN].UN_CMD_COUNT = 0;
	}
}

void CBoxOpener::m_fnBO01RotateMove(int nPosValue)
{
	CString strLogMessage;

	if(G_MOTION_STATUS[BO01_ROTATE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BO01_ROTATE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BO01_ROTATE].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BO01_ROTATE].UN_CMD_COUNT++;

			m_fnBO01PositionMove(nPosValue, nBiz_Seq_BO01_BoxRotate, TIME_OUT_60_SECOND);

			strLogMessage.Format("BO01_ROTATE : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("BO01_ROTATE : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BO01_ROTATE : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_ROTATE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BO01_ROTATE].UN_CMD_COUNT = 0;
	}
}

void CBoxOpener::m_fnBO01PositionMove(int nPosValue, int nMotionType, int nTimeOut)
{	
	m_stBO01_Mess.Reset();

	m_stBO01_Mess.nAction = m_stBO01_Mess.Act_Move;
	m_stBO01_Mess.nResult = m_stBO01_Mess.Ret_None;
	m_stBO01_Mess.newPosition1 = nPosValue;
	m_stBO01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stBO01_Mess, nMessSize);
	memcpy((char*)chMsgBuf, (char*)&m_stBO01_Mess, nMessSize);
	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize,	chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete []chMsgBuf;	
}

void CBoxOpener::m_fnBO01ActionStop(int nMotionType, int nTimeOut)
{
	m_stBO01_Mess.Reset();

	m_stBO01_Mess.nAction = m_stBO01_Mess.Act_Stop;
	m_stBO01_Mess.nResult = m_stBO01_Mess.Ret_None;
	m_stBO01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stBO01_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize, chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete[]chMsgBuf;
}

void CBoxOpener::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{	
	//CVS11IndexerDlg*  vpMainDlg = (CVS11IndexerDlg*)AfxGetApp()->m_pMainWnd;
	g_cpMainDlg->m_fnAppendList(strLog, nColorType);
}


int CBoxOpener::m_fn_BO01_Drive_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BO01_DRIVE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BO01_DRIVE : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BO01_DRIVE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BO01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BO01_DRIVE : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BO01_DRIVE : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BO01_DRIVE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnBO01DriveMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("BO01_DRIVE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxOpener::m_fn_BO01_UpDn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BO01_UPDN].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	

	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BO01_UPDN : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BO01_UPDN].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BO01_UPDN].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BO01_UPDN : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_UPDN].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BO01_UPDN : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BO01_UPDN].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnBO01UpDnMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("BO01_UPDN : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_UPDN].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxOpener::m_fn_BO01_Rotate_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BO01_ROTATE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	

	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BO01_ROTATE : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BO01_ROTATE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BO01_ROTATE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BO01_ROTATE : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_ROTATE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BO01_ROTATE : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BO01_ROTATE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnBO01RotateMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("BO01_ROTATE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_ROTATE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxOpener::m_fn_BO01_PickerFwd_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BO01_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BO01_CYL : 피커 전진 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BO01_CYL : 피커 전진 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BO01_CYL : 피커 전진 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnCylFwd();
	}break;
	default:
	{
		strLogMessage.Format("BO01_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxOpener::m_fn_BO01_PickerBwd_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BO01_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BO01_CYL : 피커 후진 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BO01_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BO01_CYL : 피커 후진 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BO01_CYL : 피커 후진 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnCylBwd();
	}break;
	default:
	{
		strLogMessage.Format("BO01_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxOpener::m_fn_BO01_VacOn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BO01_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BO01_VAC : Vac. ON 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BO01_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BO01_VAC : Vac. ON 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BO01_VAC : Vac. ON 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnVacOn();
	}break;
	default:
	{
		strLogMessage.Format("BO01_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxOpener::m_fn_BO01_VacOff_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BO01_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BO01_VAC : Vac. OFF 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BO01_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BO01_VAC : Vac. OFF 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BO01_VAC : Vac. OFF 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnVacOff();
	}break;
	default:
	{
		strLogMessage.Format("BO01_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxOpener::m_fn_BO01_Purge_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BO01_PURGE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BO01_PURGE : Purge 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BO01_PURGE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BO01_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BO01_PURGE : Purge 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BO01_PURGE : Purge 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BO01_PURGE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPurge();
	}break;
	default:
	{
		strLogMessage.Format("BO01_PURGE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BO01_PURGE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}


void CBoxOpener::OnBnClickedBtnInit3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CBoxOpener::OnBnClickedBtnMotionStop()
{
	m_fnBO01ActionStop(nBiz_Seq_BO01_BoxRotate, TIME_OUT_10_SECOND);
	m_fnBO01ActionStop(nBiz_Seq_BO01_BoxUpDn, TIME_OUT_10_SECOND);
	m_fnBO01ActionStop(nBiz_Seq_BO01_BoxDrive, TIME_OUT_10_SECOND);
}

void CBoxOpener::m_fnDisplaySignal()
{
	if (G_STMACHINE_STATUS.stModuleStatus.In00_Sn_BoxLeftCylinderForward == ON &&
		G_STMACHINE_STATUS.stModuleStatus.In04_Sn_BoxRightCylinderForward == ON)
	{
		m_ctrBtnCylFwd.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnCylBwd.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnCylFwd.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnCylBwd.SetRoundButtonStyle(&m_tStyleOn);
	}

	if (G_STMACHINE_STATUS.stModuleStatus.In03_Sn_BoxLeftVacuum == ON &&
		G_STMACHINE_STATUS.stModuleStatus.In06_Sn_BoxRightVacuum == ON)
	{
		m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOn);
	}
}
