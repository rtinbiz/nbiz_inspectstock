#pragma once


// CBoxStatus 대화 상자입니다.

class CBoxStatus : public CDialogEx
{
	DECLARE_DYNAMIC(CBoxStatus)

public:
	CBoxStatus(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBoxStatus();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_BOX_STATUS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	
public:
	virtual void PostNcDestroy();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
