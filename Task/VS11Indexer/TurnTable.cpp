// TurnTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "TurnTable.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CTurnTable 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTurnTable, CDialogEx)

CTurnTable::CTurnTable(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTurnTable::IDD, pParent)
	, m_nCtrDriveIndex(0)
	, m_nCtrRotateIndex(0)
{

}

CTurnTable::~CTurnTable()
{
}

void CTurnTable::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO_READY, m_nCtrDriveIndex);
	DDX_Radio(pDX, IDC_RADIO_SIDE_UP, m_nCtrRotateIndex);
	DDX_Control(pDX, IDC_BTN_POS_MOVE, m_ctrBtnMove);
	DDX_Control(pDX, IDC_BTN_POS_DN, m_ctrBtnMoveDn);
	DDX_Control(pDX, IDC_BTN_POS_UP, m_ctrBtnMoveUp);
	DDX_Control(pDX, IDC_BTN_POS_MOVE2, m_ctrBtnMoveR);
	DDX_Control(pDX, IDC_BTN_POS_DN2, m_ctrBtnMoveDnR);
	DDX_Control(pDX, IDC_BTN_POS_UP2, m_ctrBtnMoveUpR);
	DDX_Control(pDX, IDC_BTN_POS_MOVE3, m_ctrBtnMoveT);
	DDX_Control(pDX, IDC_BTN_POS_DN3, m_ctrBtnMoveDnT);
	DDX_Control(pDX, IDC_BTN_POS_UP3, m_ctrBtnMoveUpT);
	DDX_Control(pDX, IDC_BTN_UP_VAC_ON, m_ctrBtnVacOnUp);
	DDX_Control(pDX, IDC_BTN_UP_VAC_OFF, m_ctrBtnVacOffUp);
	DDX_Control(pDX, IDC_BTN_UP_PURGE_ON, m_ctrBtnPurgeUp);
	DDX_Control(pDX, IDC_BTN_DN_VAC_ON, m_ctrBtnVacOnDn);
	DDX_Control(pDX, IDC_BTN_DN_VAC_OFF, m_ctrBtnVacOffDn);
	DDX_Control(pDX, IDC_BTN_DN_PURGE_ON, m_ctrBtnPurgeDn);
	DDX_Control(pDX, IDC_BTN_SAVE, m_ctrBtnValueSave);
	DDX_Control(pDX, IDC_BTN_CANCEL, m_ctrBtnSaveCancel);
	DDX_Control(pDX, IDC_BTN_INIT, m_ctrBtnMoveDefault);
	DDX_Control(pDX, IDC_BTN_MOTION_STOP, m_ctrBtnTT01ActionStop);
}


BEGIN_MESSAGE_MAP(CTurnTable, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_SAVE, &CTurnTable::OnBnClickedBtnSave)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CTurnTable::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE, &CTurnTable::OnBnClickedBtnPosMove)
	ON_BN_CLICKED(IDC_BTN_POS_DN, &CTurnTable::OnBnClickedBtnPosDn)
	ON_BN_CLICKED(IDC_BTN_POS_UP, &CTurnTable::OnBnClickedBtnPosUp)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE2, &CTurnTable::OnBnClickedBtnPosMove2)
	ON_BN_CLICKED(IDC_BTN_POS_DN2, &CTurnTable::OnBnClickedBtnPosDn2)
	ON_BN_CLICKED(IDC_BTN_POS_UP2, &CTurnTable::OnBnClickedBtnPosUp2)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE3, &CTurnTable::OnBnClickedBtnPosMove3)
	ON_BN_CLICKED(IDC_BTN_POS_DN3, &CTurnTable::OnBnClickedBtnPosDn3)
	ON_BN_CLICKED(IDC_BTN_POS_UP3, &CTurnTable::OnBnClickedBtnPosUp3)
	ON_BN_CLICKED(IDC_BTN_UP_VAC_ON, &CTurnTable::OnBnClickedBtnUpVacOn)
	ON_BN_CLICKED(IDC_BTN_UP_VAC_OFF, &CTurnTable::OnBnClickedBtnUpVacOff)
	ON_BN_CLICKED(IDC_BTN_UP_PURGE_ON, &CTurnTable::OnBnClickedBtnUpPurgeOn)
	ON_BN_CLICKED(IDC_BTN_DN_VAC_ON, &CTurnTable::OnBnClickedBtnDnVacOn)
	ON_BN_CLICKED(IDC_BTN_DN_VAC_OFF, &CTurnTable::OnBnClickedBtnDnVacOff)
	ON_BN_CLICKED(IDC_BTN_DN_PURGE_ON, &CTurnTable::OnBnClickedBtnDnPurgeOn)
	ON_BN_CLICKED(IDC_BTN_MOTION_STOP, &CTurnTable::OnBnClickedBtnMotionStop)
END_MESSAGE_MAP()


// CTurnTable 메시지 처리기입니다.

BOOL CTurnTable::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CTurnTable::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_EditFont.DeleteObject();
	CDialogEx::PostNcDestroy();
}

HBRUSH CTurnTable::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CTurnTable::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style On
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);

	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyleOff.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	m_ctrBtnMove.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnMove.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	m_ctrBtnMove.SetTextColor(&tColor); m_ctrBtnMove.SetCheckButton(false);			
	m_ctrBtnMove.SetFont(&tFont);	m_ctrBtnMove.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDn.SetTextColor(&tColor); m_ctrBtnMoveDn.SetCheckButton(false);			
	m_ctrBtnMoveDn.SetFont(&tFont);	m_ctrBtnMoveDn.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUp.SetTextColor(&tColor); m_ctrBtnMoveUp.SetCheckButton(false);			
	m_ctrBtnMoveUp.SetFont(&tFont);	m_ctrBtnMoveUp.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveR.SetTextColor(&tColor); m_ctrBtnMoveR.SetCheckButton(false);			
	m_ctrBtnMoveR.SetFont(&tFont);	m_ctrBtnMoveR.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDnR.SetTextColor(&tColor); m_ctrBtnMoveDnR.SetCheckButton(false);			
	m_ctrBtnMoveDnR.SetFont(&tFont);	m_ctrBtnMoveDnR.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUpR.SetTextColor(&tColor); m_ctrBtnMoveUpR.SetCheckButton(false);			
	m_ctrBtnMoveUpR.SetFont(&tFont);	m_ctrBtnMoveUpR.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveT.SetTextColor(&tColor); m_ctrBtnMoveT.SetCheckButton(false);			
	m_ctrBtnMoveT.SetFont(&tFont);	m_ctrBtnMoveT.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDnT.SetTextColor(&tColor); m_ctrBtnMoveDnT.SetCheckButton(false);			
	m_ctrBtnMoveDnT.SetFont(&tFont);	m_ctrBtnMoveDnT.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUpT.SetTextColor(&tColor); m_ctrBtnMoveUpT.SetCheckButton(false);			
	m_ctrBtnMoveUpT.SetFont(&tFont);	m_ctrBtnMoveUpT.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnVacOnUp.SetTextColor(&tColor); m_ctrBtnVacOnUp.SetCheckButton(false);			
	m_ctrBtnVacOnUp.SetFont(&tFont);	m_ctrBtnVacOnUp.SetRoundButtonStyle(&m_tStyleOff);	

	m_ctrBtnVacOffUp.SetTextColor(&tColor); m_ctrBtnVacOffUp.SetCheckButton(false);			
	m_ctrBtnVacOffUp.SetFont(&tFont);	m_ctrBtnVacOffUp.SetRoundButtonStyle(&m_tStyleOff);	

	m_ctrBtnPurgeUp.SetTextColor(&tColor); m_ctrBtnPurgeUp.SetCheckButton(false);			
	m_ctrBtnPurgeUp.SetFont(&tFont);	m_ctrBtnPurgeUp.SetRoundButtonStyle(&m_tStyleOff);	

	m_ctrBtnVacOnDn.SetTextColor(&tColor); m_ctrBtnVacOnDn.SetCheckButton(false);			
	m_ctrBtnVacOnDn.SetFont(&tFont);	m_ctrBtnVacOnDn.SetRoundButtonStyle(&m_tStyleOff);	

	m_ctrBtnVacOffDn.SetTextColor(&tColor); m_ctrBtnVacOffDn.SetCheckButton(false);			
	m_ctrBtnVacOffDn.SetFont(&tFont);	m_ctrBtnVacOffDn.SetRoundButtonStyle(&m_tStyleOff);	

	m_ctrBtnPurgeDn.SetTextColor(&tColor); m_ctrBtnPurgeDn.SetCheckButton(false);			
	m_ctrBtnPurgeDn.SetFont(&tFont);	m_ctrBtnPurgeDn.SetRoundButtonStyle(&m_tStyleOff);	

	m_ctrBtnValueSave.SetTextColor(&tColor); m_ctrBtnValueSave.SetCheckButton(false);			
	m_ctrBtnValueSave.SetFont(&tFont);	m_ctrBtnValueSave.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnSaveCancel.SetTextColor(&tColor); m_ctrBtnSaveCancel.SetCheckButton(false);			
	m_ctrBtnSaveCancel.SetFont(&tFont);	m_ctrBtnSaveCancel.SetRoundButtonStyle(&m_tStyle1);		

	m_ctrBtnMoveDefault.SetTextColor(&tColor); m_ctrBtnMoveDefault.SetCheckButton(false);			
	m_ctrBtnMoveDefault.SetFont(&tFont);	m_ctrBtnMoveDefault.SetRoundButtonStyle(&m_tStyle1);		

	m_ctrBtnTT01ActionStop.SetTextColor(&tColor);	m_ctrBtnTT01ActionStop.SetCheckButton(false);
	m_ctrBtnTT01ActionStop.SetFont(&tFont);	m_ctrBtnTT01ActionStop.SetRoundButtonStyle(&m_tStyle1);

}

int CTurnTable::m_fnCreateControlFont(void)
{	
	// 	GetDlgItem(IDC_EDIT_BAD_POS)->SetFont(&m_EditFont);
	// 	GetDlgItem(IDC_EDIT_POS1)->SetFont(&m_EditFont);
	// 	GetDlgItem(IDC_EDIT_POS2)->SetFont(&m_EditFont);
	// 	GetDlgItem(IDC_EDIT_POS3)->SetFont(&m_EditFont);
	// 	GetDlgItem(IDC_EDIT_POS4)->SetFont(&m_EditFont);
	// 	GetDlgItem(IDC_EDIT_POS5)->SetFont(&m_EditFont);
	// 	GetDlgItem(IDC_EDIT_POS6)->SetFont(&m_EditFont);
	// 	GetDlgItem(IDC_EDIT_POS7)->SetFont(&m_EditFont);
	// 	GetDlgItem(IDC_EDIT_READY_POS)->SetFont(&m_EditFont);

	//SetDlgItemInt(IDC_EDIT_Z_POS_UP, 100);
	//SetDlgItemInt(IDC_EDIT_Z_POS_DN, 100);
	return 0;
}

BOOL CTurnTable::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);
	m_EditFont.CreatePointFont(100,"Arial");	

	m_fnCreateControlFont();
	m_fnCreateButton();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CTurnTable::m_fnSetParameter(STT01_POSITION* stPointer)
{
	m_st_TT01_Pos = stPointer;	
	return 0;
}

int CTurnTable::m_fnDisplayParameter(void)
{
	SetDlgItemInt(IDC_EDIT_READY_POS, m_st_TT01_Pos->nDrive_Ready_Pos);
	SetDlgItemInt(IDC_EDIT_TURN_POS, m_st_TT01_Pos->nDrive_Turn_Pos);
	SetDlgItemInt(IDC_EDIT_AM01_POS, m_st_TT01_Pos->nDrive_AM01_Pos);
	SetDlgItemInt(IDC_EDIT_SIDE_UP, m_st_TT01_Pos->nRotate_SideUp_Pos);
	SetDlgItemInt(IDC_EDIT_SIDE_DN, m_st_TT01_Pos->nRotate_SideDn_Pos);	
	SetDlgItemInt(IDC_EDIT_T_READY, m_st_TT01_Pos->nT_Ready_Pos);	 
	SetDlgItemInt(IDC_EDIT_UP_PURGE_SEC, m_st_TT01_Pos->nUpPurge_Time);
	SetDlgItemInt(IDC_EDIT_DN_PURGE_SEC, m_st_TT01_Pos->nDnPurge_Time);
	return 0;
}

void CTurnTable::OnBnClickedBtnSave()
{
	CString strWriteData;

	m_st_TT01_Pos->nDrive_Ready_Pos = GetDlgItemInt(IDC_EDIT_READY_POS);
	m_st_TT01_Pos->nDrive_Turn_Pos = GetDlgItemInt(IDC_EDIT_TURN_POS);	
	m_st_TT01_Pos->nDrive_AM01_Pos = GetDlgItemInt(IDC_EDIT_AM01_POS);
	m_st_TT01_Pos->nRotate_SideUp_Pos = GetDlgItemInt(IDC_EDIT_SIDE_UP);	
	m_st_TT01_Pos->nRotate_SideDn_Pos = GetDlgItemInt(IDC_EDIT_SIDE_DN);
	m_st_TT01_Pos->nT_Ready_Pos = GetDlgItemInt(IDC_EDIT_T_READY);	
	m_st_TT01_Pos->nUpPurge_Time = GetDlgItemInt(IDC_EDIT_UP_PURGE_SEC);
	m_st_TT01_Pos->nDnPurge_Time = GetDlgItemInt(IDC_EDIT_DN_PURGE_SEC);	

	strWriteData.Format("%d", m_st_TT01_Pos->nDrive_Ready_Pos);
	WritePrivateProfileString(SECTION_TT01_POS_DATA, KEY_DRIVE_POS_READY, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TT01_Pos->nDrive_Turn_Pos);
	WritePrivateProfileString(SECTION_TT01_POS_DATA, KEY_DRIVE_POS_TURN, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TT01_Pos->nDrive_AM01_Pos);
	WritePrivateProfileString(SECTION_TT01_POS_DATA, KEY_DRIVE_POS_AM01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TT01_Pos->nRotate_SideUp_Pos);
	WritePrivateProfileString(SECTION_TT01_POS_DATA, KEY_ROTATE_SIDE_UP, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TT01_Pos->nRotate_SideDn_Pos);
	WritePrivateProfileString(SECTION_TT01_POS_DATA, KEY_ROTATE_SIDE_DN, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TT01_Pos->nT_Ready_Pos);
	WritePrivateProfileString(SECTION_TT01_POS_DATA, KEY_T_POS_READY, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TT01_Pos->nUpPurge_Time);
	WritePrivateProfileString(SECTION_TT01_POS_DATA, KEY_UP_PURGE_TIME, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TT01_Pos->nDnPurge_Time);
	WritePrivateProfileString(SECTION_TT01_POS_DATA, KEY_DN_PURGE_TIME, strWriteData, SYSTEM_PARAM_INI_PATH);
	
}

void CTurnTable::OnBnClickedBtnCancel()
{
	m_fnDisplayParameter();
}

void CTurnTable::OnBnClickedBtnPosMove()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nCtrDriveIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_READY_POS);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_TURN_POS);	break;	
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_AM01_POS);	break;		
	}
	m_fnTT01DriveMove(nPosValue);
		
}

void CTurnTable::OnBnClickedBtnPosDn()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	nPosValue = nPosValue - nOffsetPos;

	m_fnTT01DriveMove(nPosValue);
}

void CTurnTable::OnBnClickedBtnPosUp()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	nPosValue = nPosValue + nOffsetPos;	

	m_fnTT01DriveMove(nPosValue);
}

void CTurnTable::OnBnClickedBtnPosMove2()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nCtrRotateIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_SIDE_UP);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_SIDE_DN);	break;		
	}
	m_fnTT01RotateMove(nPosValue);
}

void CTurnTable::OnBnClickedBtnPosDn2()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN2);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE);
	nPosValue = nPosValue - nOffsetPos;	

	m_fnTT01RotateMove(nPosValue);
}

void CTurnTable::OnBnClickedBtnPosUp2()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP2);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE);
	nPosValue = nPosValue + nOffsetPos;	
		
	m_fnTT01RotateMove(nPosValue);
}

void CTurnTable::OnBnClickedBtnPosMove3()
{	
	int nPosValue = 0;

	nPosValue = GetDlgItemInt(IDC_EDIT_T_READY);
	
	m_fnTT01ThetaMove(nPosValue);
}

void CTurnTable::OnBnClickedBtnPosDn3()
{	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN3);
	
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_TILT);
	nPosValue = nPosValue - nOffsetPos;	

	m_fnTT01ThetaMove(nPosValue);
}

void CTurnTable::OnBnClickedBtnPosUp3()
{
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP3);

	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_TILT);
	nPosValue = nPosValue + nOffsetPos;	

	m_fnTT01ThetaMove(nPosValue);
}

void CTurnTable::OnBnClickedBtnUpVacOn()
{
	m_fnCheckUpAirStep();

	CString strLogMessage;
	strLogMessage.Format("TT01_UP_VAC : ON.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickUpperVac(m_stAirStep, nBiz_Seq_TT01_UP_Vac_ON);	
}

void CTurnTable::OnBnClickedBtnUpVacOff()
{
	m_fnCheckUpAirStep();

	CString strLogMessage;
	strLogMessage.Format("TT01_UP_VAC : OFF.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickUpperVac(m_stAirStep, nBiz_Seq_TT01_UP_Vac_OFF);	
}

void CTurnTable::m_fnStickUpperVac(AIR_SELECTION nAirSelect, int nActType)
{
	m_stTT01_Mess.Reset();
	m_stTT01_Mess.nAction = m_stTT01_Mess.Act_None;
	m_stTT01_Mess.nResult = m_stTT01_Mess.Ret_None;
	m_stTT01_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stTT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTT01_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TT01_UP_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TT01_UP_VAC].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nActType, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nActType, nBiz_Unit_Zero
				);					
		}
		else
		{
			strLogMessage.Format("TT01_UP_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TT01_UP_VAC : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_UP_VAC].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CTurnTable::OnBnClickedBtnUpPurgeOn()
{
	int nPurgeTime = GetDlgItemInt(IDC_EDIT_UP_PURGE_SEC);

	m_fnCheckUpAirStep();

	m_fnStickUpperPurge(m_stAirStep, nPurgeTime);
}

void CTurnTable::m_fnStickUpperPurge(AIR_SELECTION nAirSelect, int nPurgeTime)
{
	m_stTT01_Mess.Reset();

	m_stTT01_Mess.nAction = m_stTT01_Mess.Act_None;
	m_stTT01_Mess.nResult = m_stTT01_Mess.Ret_None;
	m_stTT01_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stTT01_Mess.ActionTime = nPurgeTime;
	m_stTT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTT01_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TT01_UP_PURGE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TT01_UP_PURGE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TT01_UP_PURGE].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TT01_UP_PURGE].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_TT01_UP_Purge, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_TT01_UP_Purge, nBiz_Unit_Zero
				);

			strLogMessage.Format("TT01_UP_PURGE : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TT01_UP_PURGE : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TT01_UP_PURGE : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_UP_PURGE].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TT01_UP_PURGE].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CTurnTable::OnBnClickedBtnDnVacOn()
{
	m_fnCheckDnAirStep();
	CString strLogMessage;
	strLogMessage.Format("TT01_DN_VAC : ON.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickLowerVac(m_stAirStep, nBiz_Seq_TT01_DN_Vac_ON);	
}

void CTurnTable::OnBnClickedBtnDnVacOff()
{
	m_fnCheckDnAirStep();
	CString strLogMessage;
	strLogMessage.Format("TT01_DN_VAC : OFF.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickLowerVac(m_stAirStep, nBiz_Seq_TT01_DN_Vac_OFF);
	
}

void CTurnTable::m_fnStickLowerVac(AIR_SELECTION nAirSelect, int nActType)
{
	m_stTT01_Mess.Reset();
	m_stTT01_Mess.nAction = m_stTT01_Mess.Act_None;
	m_stTT01_Mess.nResult = m_stTT01_Mess.Ret_None;
	m_stTT01_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stTT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTT01_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TT01_DN_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TT01_DN_VAC].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nActType, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nActType, nBiz_Unit_Zero
				);			
		}
		else
		{
			strLogMessage.Format("TT01_DN_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TT01_DN_VAC : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DN_VAC].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}


void CTurnTable::OnBnClickedBtnDnPurgeOn()
{
	int nPurgeTime = GetDlgItemInt(IDC_EDIT_DN_PURGE_SEC);

	m_fnCheckDnAirStep();

	CString strLogMessage;
	strLogMessage.Format("TT01_DN_PURGE : 동작 실행.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickLowerPurge(m_stAirStep, nPurgeTime);
}

void CTurnTable::m_fnStickLowerPurge(AIR_SELECTION nAirSelect, int nPurgeTime)
{
	m_stTT01_Mess.Reset();

	m_stTT01_Mess.nAction = m_stTT01_Mess.Act_None;
	m_stTT01_Mess.nResult = m_stTT01_Mess.Ret_None;
	m_stTT01_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stTT01_Mess.ActionTime = nPurgeTime;
	m_stTT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTT01_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TT01_DN_PURGE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TT01_DN_PURGE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TT01_DN_PURGE].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TT01_DN_PURGE].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_TT01_DN_Purge, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_TT01_DN_Purge, nBiz_Unit_Zero
				);			
		}
		else
		{
			strLogMessage.Format("TT01_DN_PURGE : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TT01_DN_PURGE : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DN_PURGE].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TT01_DN_PURGE].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CTurnTable::m_fnCheckUpAirStep()
{
	m_stAirStep.nAirSelection = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC1))
		m_stAirStep.nAirStep.m_btStep1 = 1;
	else
		m_stAirStep.nAirStep.m_btStep1 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC2))
		m_stAirStep.nAirStep.m_btStep2 = 1;
	else
		m_stAirStep.nAirStep.m_btStep2 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC3))
		m_stAirStep.nAirStep.m_btStep3 = 1;
	else
		m_stAirStep.nAirStep.m_btStep3 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC4))
		m_stAirStep.nAirStep.m_btStep4 = 1;
	else
		m_stAirStep.nAirStep.m_btStep4 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC5))
		m_stAirStep.nAirStep.m_btStep5 = 1;
	else
		m_stAirStep.nAirStep.m_btStep5 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC6))
		m_stAirStep.nAirStep.m_btStep6 = 1;
	else
		m_stAirStep.nAirStep.m_btStep6 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC7))
		m_stAirStep.nAirStep.m_btStep7 = 1;
	else
		m_stAirStep.nAirStep.m_btStep7 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC8))
		m_stAirStep.nAirStep.m_btStep8 = 1;
	else
		m_stAirStep.nAirStep.m_btStep8 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC9))
		m_stAirStep.nAirStep.m_btStep9 = 1;
	else
		m_stAirStep.nAirStep.m_btStep9 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_UP_VAC10))
		m_stAirStep.nAirStep.m_btStep10 = 1;
	else
		m_stAirStep.nAirStep.m_btStep10 = 0;
}

void CTurnTable::m_fnCheckDnAirStep()
{
	m_stAirStep.nAirSelection = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC1))
		m_stAirStep.nAirStep.m_btStep1 = 1;
	else
		m_stAirStep.nAirStep.m_btStep1 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC2))
		m_stAirStep.nAirStep.m_btStep2 = 1;
	else
		m_stAirStep.nAirStep.m_btStep2 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC3))
		m_stAirStep.nAirStep.m_btStep3 = 1;
	else
		m_stAirStep.nAirStep.m_btStep3 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC4))
		m_stAirStep.nAirStep.m_btStep4 = 1;
	else
		m_stAirStep.nAirStep.m_btStep4 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC5))
		m_stAirStep.nAirStep.m_btStep5 = 1;
	else
		m_stAirStep.nAirStep.m_btStep5 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC6))
		m_stAirStep.nAirStep.m_btStep6 = 1;
	else
		m_stAirStep.nAirStep.m_btStep6 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC7))
		m_stAirStep.nAirStep.m_btStep7 = 1;
	else
		m_stAirStep.nAirStep.m_btStep7 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC8))
		m_stAirStep.nAirStep.m_btStep8 = 1;
	else
		m_stAirStep.nAirStep.m_btStep8 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC9))
		m_stAirStep.nAirStep.m_btStep9 = 1;
	else
		m_stAirStep.nAirStep.m_btStep9 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_DN_VAC10))
		m_stAirStep.nAirStep.m_btStep10 = 1;
	else
		m_stAirStep.nAirStep.m_btStep10 = 0;
}

void CTurnTable::m_fnTT01DriveMove(int nPosValue)
{
	CString strLogMessage;

	if(m_fnTT01InterlockCheck(AXIS_TT01_TableDrive, nPosValue))
		return;

	if(G_MOTION_STATUS[TT01_DRIVE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TT01_DRIVE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TT01_DRIVE].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TT01_DRIVE].UN_CMD_COUNT++;

			m_fnTT01PositionMove(nPosValue, nBiz_Seq_TT01_TableDrive, TIME_OUT_120_SECOND);

			strLogMessage.Format("TT01_DRIVE : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TT01_DRIVE : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TT01_DRIVE : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DRIVE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TT01_DRIVE].UN_CMD_COUNT = 0;
	}	
}

void CTurnTable::m_fnTT01RotateMove(int nPosValue)
{
	CString strLogMessage;

	if(m_fnTT01InterlockCheck(AXIS_TT01_TableTurn, nPosValue))
		return;

	if(G_MOTION_STATUS[TT01_ROTATE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TT01_ROTATE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TT01_ROTATE].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TT01_ROTATE].UN_CMD_COUNT++;

			m_fnTT01PositionMove(nPosValue, nBiz_Seq_TT01_TableTurn, TIME_OUT_120_SECOND);

			strLogMessage.Format("TT01_ROTATE : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TT01_ROTATE : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TT01_ROTATE : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_ROTATE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TT01_ROTATE].UN_CMD_COUNT = 0;
	}
}

void CTurnTable::m_fnTT01ThetaMove(int nPosValue)
{
	CString strLogMessage;

	if(m_fnTT01InterlockCheck(AXIS_TT01_ThetaAlign, nPosValue))
		return;

	if(G_MOTION_STATUS[TT01_THETA].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TT01_THETA].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TT01_THETA].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TT01_THETA].UN_CMD_COUNT++;

			m_fnTT01PositionMove(nPosValue, nBiz_Seq_TT01_ThetaAlign, TIME_OUT_20_SECOND);

			strLogMessage.Format("TT01_THETA : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TT01_THETA : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TT01_THETA : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_THETA].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TT01_THETA].UN_CMD_COUNT = 0;
	}
}

void CTurnTable::m_fnTT01ThetaAlignMove(int nPosValue)
{
	CString strLogMessage;

	if(m_fnTT01InterlockCheck(AXIS_TT01_ThetaAlign, nPosValue))
		return;

	m_fnTT01PositionMove(nPosValue, nBiz_Seq_TT01_ThetaAlign, TIME_OUT_20_SECOND);

	strLogMessage.Format("TT01_THETA : [%d]위치로 이동 명령.", nPosValue);
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
}

bool CTurnTable::m_fnTT01InterlockCheck(int AxisNum, long newPosition)
{
	CString strLogMessage;

	//구동 가능한 상태 이다.(fals:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
	CHECK_AXIS_TT01(AxisNum);

	switch(AxisNum)
	{
		//TT01
	case AXIS_TT01_TableDrive:
		{
			int nDriveMin = GetPrivateProfileInt(Section_TT01, Key_Drive_Min, 0, Interlock_PARAM_INI_PATH);
			int nDriveMax = GetPrivateProfileInt(Section_TT01, Key_Drive_Max, 0, Interlock_PARAM_INI_PATH);
				
			int nTT01_DriveMin = GetPrivateProfileInt(Section_TM02, Key_Drive_TT01_Pos_Low, 0, Interlock_PARAM_INI_PATH);
			int nTT01_DriveMax = GetPrivateProfileInt(Section_TM02, Key_Drive_TT01_Pos_High, 0, Interlock_PARAM_INI_PATH);
			int nTT01_UnDnMax = GetPrivateProfileInt(Section_TM02, Key_UpDn_TT01_Max, 0, Interlock_PARAM_INI_PATH);

			if (!(G_STMACHINE_STATUS.stEmissionTable.In03_Sn_OutputLTableCylinderDown == ON &&
				G_STMACHINE_STATUS.stEmissionTable.In06_Sn_OutputRTableCylinderDown == ON))
			{
				m_fnLogDisplay("AXIS_TT01_TableDrive : 배출 테이블 실린더가 Up 상태입니다.", TEXT_WARRING_YELLOW);
				return true;
			}		

			if(!(nDriveMin == 0 &&nDriveMax == 0))		
			{
				if(newPosition < nDriveMin || newPosition > nDriveMax)
				{
					strLogMessage.Format("AXIS_TT01_TableDrive : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
						nDriveMin, newPosition, nDriveMax );
					m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
					return true;
				}
			}

			if(!(nTT01_DriveMin == 0 && nTT01_DriveMax == 0))
			{
				//턴테이블 상단에 피커가 위치할 때는...
				if(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] >= nTT01_DriveMin &&
					G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] <= nTT01_DriveMax)
				{
					if(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] >= (nTT01_UnDnMax - 25000))
					{
						strLogMessage.Format("AXIS_TT01_TableDrive : 피커 Z축이 안전한 위치에 있지 않습니다.");
						m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
						return true;
					}
				}
			}
			break;
		}
	case AXIS_TT01_ThetaAlign:
		{
			int nTiltMin = GetPrivateProfileInt(Section_TT01, Key_Tilt_Min, 0, Interlock_PARAM_INI_PATH);
			int nTiltMax = GetPrivateProfileInt(Section_TT01, Key_Tilt_Max, 0, Interlock_PARAM_INI_PATH);

			if (!(G_STMACHINE_STATUS.stEmissionTable.In03_Sn_OutputLTableCylinderDown == ON &&
				G_STMACHINE_STATUS.stEmissionTable.In06_Sn_OutputRTableCylinderDown == ON))
			{
				m_fnLogDisplay("AXIS_TT01_ThetaAlign : 배출 테이블 실린더가 Up 상태입니다.", TEXT_WARRING_YELLOW);
				return true;
			}		

			if(!(nTiltMin == 0 && nTiltMax == 0))		
			{
				if(newPosition < nTiltMin || newPosition > nTiltMax)
				{
					strLogMessage.Format("AXIS_TT01_ThetaAlign : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
						nTiltMin, newPosition, nTiltMax );
					m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
					return true;
				}
			}
			break;
		}
	case AXIS_TT01_TableTurn:
		{
			if(G_SAFE_ZONE[TT01_SAFE_POS] == UNSAFE)
			{					
				m_fnLogDisplay("AXIS_TT01_TableTurn : Tun table 이 안전한 위치에 있지 않습니다.", TEXT_WARRING_YELLOW);
				return true;
			}

			if(abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] - m_st_TT01_Pos->nDrive_Turn_Pos) >= POSITION_OFFSET)
			{					
				m_fnLogDisplay("AXIS_TT01_TableTurn : Tun table 이 안전한 위치에 있지 않습니다.", TEXT_WARRING_YELLOW);
				return true;
			}

			int nRotateMin = GetPrivateProfileInt(Section_TT01, Key_Rotate_Min, 0, Interlock_PARAM_INI_PATH);
			int nRotateMax = GetPrivateProfileInt(Section_TT01, Key_Rotate_Max, 0, Interlock_PARAM_INI_PATH);
			
			if(!(nRotateMin == 0 && nRotateMax == 0))		
			{
				if(newPosition < nRotateMin || newPosition > nRotateMax)
				{
					strLogMessage.Format("AXIS_TT01_TableTurn : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
						nRotateMin, newPosition, nRotateMax );
					m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
					return true;
				}
			}
			break;
		}
	default: 
		return true;
	}
	return false;
}

void CTurnTable::m_fnTT01PositionMove(int nPosValue, int nMotionType, int nTimeOut)
{	
	m_stTT01_Mess.Reset();

	m_stTT01_Mess.nAction = m_stTT01_Mess.Act_Move;
	m_stTT01_Mess.nResult = m_stTT01_Mess.Ret_None;
	m_stTT01_Mess.newPosition1 = nPosValue;
	m_stTT01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTT01_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize,	chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete []chMsgBuf;	
}

void CTurnTable::m_fnTT01ActionStop(int nMotionType, int nTimeOut)
{
	m_stTT01_Mess.Reset();

	m_stTT01_Mess.nAction = m_stTT01_Mess.Act_Stop;
	m_stTT01_Mess.nResult = m_stTT01_Mess.Ret_None;
	m_stTT01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTT01_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize, chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete[]chMsgBuf;
}

void CTurnTable::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{	
	CVS11IndexerDlg*  vpMainDlg = (CVS11IndexerDlg*)AfxGetApp()->m_pMainWnd;
	vpMainDlg->m_fnAppendList(strLog, nColorType);
}

int CTurnTable::m_fn_TT01_Drive_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_DRIVE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TT01_DRIVE : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TT01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TT01_DRIVE : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TT01_DRIVE : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_DRIVE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnTT01DriveMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("TT01_DRIVE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CTurnTable::m_fn_TT01_Theta_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_THETA].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TT01_THETA : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TT01_THETA].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TT01_THETA].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TT01_THETA : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_THETA].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TT01_THETA : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_THETA].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnTT01ThetaMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("TT01_THETA : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_THETA].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CTurnTable::m_fn_TM01_Rotate_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_ROTATE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TT01_ROTATE : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TT01_ROTATE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TT01_ROTATE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TT01_ROTATE : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_ROTATE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TT01_ROTATE : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_ROTATE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnTT01RotateMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("TT01_ROTATE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_ROTATE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CTurnTable::m_fn_TT01_UP_VacOn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_UP_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
		case stReturn_Mess.Ret_Ok:
		{
			strLogMessage = "TT01_UP_VAC : Vac. ON 수행 완료.";
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);	
			G_MOTION_STATUS[TT01_UP_VAC].B_COMMAND_STATUS = COMPLETE;
			G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT = 0;
		}break;
		case stReturn_Mess.Ret_Error:
		{
			strLogMessage = "TT01_UP_VAC : Vac. ON 에러 발생.";
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT = 0;
		}break;
		case stReturn_Mess.Ret_TimeOut:
		{
			strLogMessage.Format("TT01_UP_VAC : Vac. ON 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT);
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
			//OnBnClickedBtnUpVacOn();
		}break;
		default:
		{
			strLogMessage.Format("TT01_UP_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT = 0;
		}break;
	}
	return 0;
}

int CTurnTable::m_fn_TT01_UP_VacOff_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_UP_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
		{
			strLogMessage = "TT01_UP_VAC : Vac. OFF 수행 완료.";
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
			G_MOTION_STATUS[TT01_UP_VAC].B_COMMAND_STATUS = COMPLETE;
			G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_Error:
		{
			strLogMessage = "TT01_UP_VAC : Vac. OFF 에러 발생.";
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_TimeOut:
		{
			strLogMessage.Format("TT01_UP_VAC : Vac. OFF 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT);
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
			//OnBnClickedBtnUpVacOff();
		}break;
	default:
		{
			strLogMessage.Format("TT01_UP_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TT01_UP_VAC].UN_CMD_COUNT = 0;
		}break;
	}
	return 0;
}

int CTurnTable::m_fn_TT01_UP_Purge_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_UP_PURGE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
		{
			strLogMessage = "TT01_UP_PURGE : Purge 수행 완료.";
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
			G_MOTION_STATUS[TT01_UP_PURGE].B_COMMAND_STATUS = COMPLETE;
			G_MOTION_STATUS[TT01_UP_PURGE].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_Error:
		{
			strLogMessage = "TT01_UP_PURGE : Purge 에러 발생.";
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TT01_UP_PURGE].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_TimeOut:
		{
			strLogMessage.Format("TT01_UP_PURGE : Purge 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_UP_PURGE].UN_CMD_COUNT);
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
			//OnBnClickedBtnUpPurgeOn();
		}break;
	default:
		{
			strLogMessage.Format("TT01_UP_PURGE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TT01_UP_PURGE].UN_CMD_COUNT = 0;
		}break;
	}
	return 0;
}

int CTurnTable::m_fn_TT01_DN_VacOn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_DN_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		strLogMessage = "TT01_DN_VAC : Vac. ON 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TT01_DN_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_Error:	
		strLogMessage = "TT01_DN_VAC : Vac. ON 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_TimeOut:	
		strLogMessage.Format("TT01_DN_VAC : Vac. ON 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnDnVacOn();
		break;
	default:
		strLogMessage.Format("TT01_DN_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT = 0;
		break;
	}
	return 0;

}

int CTurnTable::m_fn_TT01_DN_VacOff_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_DN_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TT01_DN_VAC : Vac. OFF 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TT01_DN_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TT01_DN_VAC : Vac. OFF 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TT01_DN_VAC : Vac. OFF 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnDnVacOff();
	}break;
	default:
	{
		strLogMessage.Format("TT01_DN_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DN_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CTurnTable::m_fn_TT01_DN_Purge_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TT01_DN_PURGE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult){
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TT01_DN_PURGE : Purge 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TT01_DN_PURGE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TT01_DN_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TT01_DN_PURGE : Purge 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DN_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TT01_DN_PURGE : Purge 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TT01_DN_PURGE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnDnPurgeOn();
	}break;
	default:
	{
		strLogMessage.Format("TT01_DN_PURGE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TT01_DN_PURGE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

void CTurnTable::OnBnClickedBtnMotionStop()
{
	m_fnTT01ActionStop(nBiz_Seq_TT01_TableDrive, TIME_OUT_10_SECOND);
	m_fnTT01ActionStop(nBiz_Seq_TT01_ThetaAlign, TIME_OUT_10_SECOND);
	m_fnTT01ActionStop(nBiz_Seq_TT01_TableTurn, TIME_OUT_10_SECOND);
}

void CTurnTable::m_fnDisplaySignal()
{
	if (G_STMACHINE_STATUS.stTurnTableSensor.In00_Sn_TurnTableUpperVacuum == ON)
	{
		m_ctrBtnVacOnUp.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnVacOffUp.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnVacOnUp.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnVacOffUp.SetRoundButtonStyle(&m_tStyleOn);
	}

	if (G_STMACHINE_STATUS.stTurnTableSensor.In07_Sn_TurnTableDownVacuum == ON)
	{
		m_ctrBtnVacOnDn.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnVacOffDn.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnVacOnDn.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnVacOffDn.SetRoundButtonStyle(&m_tStyleOn);
	}	
}
