// BoxStatus.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "BoxStatus.h"
#include "afxdialogex.h"


// CBoxStatus 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBoxStatus, CDialogEx)

CBoxStatus::CBoxStatus(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBoxStatus::IDD, pParent)
{

}

CBoxStatus::~CBoxStatus()
{
}

void CBoxStatus::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBoxStatus, CDialogEx)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CBoxStatus 메시지 처리기입니다.


void CBoxStatus::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
}


BOOL CBoxStatus::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CBoxStatus::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreTranslateMessage(pMsg);
}


HBRUSH CBoxStatus::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
