#pragma once
#include "afxwin.h"


// CMainControl 대화 상자입니다.

class CMainControl : public CDialogEx
{
	DECLARE_DYNAMIC(CMainControl)

public:
	CMainControl(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMainControl();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_CTR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	void m_fnCreateButton(void);
public:
	COLORREF	m_btBackColor;

	CRoundButtonStyle m_tButtonStyle;
	CRoundButtonStyle	m_tStyle1;
	CRoundButtonStyle	m_tStyle2;

	CRoundButton2 m_ctrBtnManualSequence[STEP_COUNT];
	CRoundButton2 m_ctrBtnImageSave;

	CRoundButton2 m_ctrBtnBoxIndex[BOX7];

	afx_msg void OnBnClicked_STEP_BOX_LOADING();
	afx_msg void OnBnClicked_STEP_COVER_OPEN();
	afx_msg void OnBnClicked_STEP_COVER_CLOSE();
	afx_msg void OnBnClicked_STEP_MOVE_TO_LT01();
	afx_msg void OnBnClicked_STEP_BOX_UNLOADING();
	afx_msg void OnBnClicked_STEP_PP_MOVE_TO_TM02();
	afx_msg void OnBnClicked_STEP_PP_MOVE_TO_UT01();
	afx_msg void OnBnClicked_STEP_PP_MOVE_TO_BT01();
	afx_msg void OnBnClicked_STEP_ST_MOVE_TO_TM02();
	afx_msg void OnBnClicked_STEP_ST_MOVE_TO_TT01();
	afx_msg void OnBnClicked_STEP_ST_MOVE_TO_AM01();
	afx_msg void OnBnClicked_STEP_ST_MOVE_TO_UT02();
	afx_msg void OnBnClicked_STEP_ST_MOVE_TO_TM02_2();
	afx_msg void OnBnClicked_STEP_ST_MOVE_TO_UT01();
	afx_msg void OnBnClicked_STEP_ST_MOVE_TO_BT01();
	afx_msg void OnBnClicked_STEP_ST_CHECK_TO_UT02();
	afx_msg void OnBnClickedBtnCapture();
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	
	afx_msg void OnBnClickedBtnBoxIndex7();
	afx_msg void OnBnClickedBtnBoxIndex6();
	afx_msg void OnBnClickedBtnBoxIndex5();
	afx_msg void OnBnClickedBtnBoxIndex4();
	afx_msg void OnBnClickedBtnBoxIndex3();
	afx_msg void OnBnClickedBtnBoxIndex2();
	afx_msg void OnBnClickedBtnBoxIndex1();
};
