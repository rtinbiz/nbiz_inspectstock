#pragma once
#include "afxwin.h"


// CEmissionTable 대화 상자입니다.

class CEmissionTable : public CDialogEx
{
	DECLARE_DYNAMIC(CEmissionTable)

public:
	CEmissionTable(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CEmissionTable();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_UT02 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnSave();
	afx_msg void OnBnClickedBtnCancel();
	afx_msg void OnBnClickedBtnCylUp();
	afx_msg void OnBnClickedBtnCylDn();
	afx_msg void OnBnClickedBtnStickVacOn();
	afx_msg void OnBnClickedBtnStickVacOff();
	void m_fnStickVacuum(AIR_SELECTION nAirSelect, int nActType);

	afx_msg void OnBnClickedBtnStickPurgeOn();
	void m_fnStickPurge(AIR_SELECTION nAirSelect, int nPurgeTime);

	int m_fnCreateControlFont(void);
	int m_fnSetParameter(SUT02_POSITION* stPointer);
	int m_fnDisplayParameter(void);
	void m_fnCreateButton(void);
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	void m_fnCheckStickAirStep();
	
	//Receive Result
	int m_fn_UT02_TableUp_Return(VS24MotData stReturn_Mess);
	int m_fn_UT02_TableDn_Return(VS24MotData stReturn_Mess);
	int m_fn_UT02_VacOn_Return(VS24MotData stReturn_Mess);
	int m_fn_UT02_VacOff_Return(VS24MotData stReturn_Mess);
	int m_fn_UT02_Purge_Return(VS24MotData stReturn_Mess);
	void m_fnDisplaySignal();
public:
	//CInterServerInterface*  g_cpServerInterface;
	COLORREF	m_btBackColor;
	CFont m_EditFont;	
	SUT02_POSITION* m_st_UT02_Pos;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;	
	CRoundButtonStyle	m_tStyleOn;	
	CRoundButtonStyle	m_tStyleOff;

	VS24MotData m_stUT02_Mess;
	

	AIR_SELECTION m_stAirStep;
	
	CRoundButton2 m_ctrBtnCylUp;
	CRoundButton2 m_ctrBtnCylDn;
	CRoundButton2 m_ctrBtnVacOn;
	CRoundButton2 m_ctrBtnVacOff;
	CRoundButton2 m_ctrBtnPurge;
	CRoundButton2 m_ctrBtnValueSave;
	CRoundButton2 m_ctrBtnSaveCancel;
	CRoundButton2 m_ctrBtnMoveDefault;
	CRoundButton2 m_ctrBtnUT02ActionStop;
};
