// BoxTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "BoxTable.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CBoxTable 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBoxTable, CDialogEx)

CBoxTable::CBoxTable(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBoxTable::IDD, pParent)
	, m_nTableIndex(0)
	, m_nTargetBoxPos(0)
{

}

CBoxTable::~CBoxTable()
{
}

void CBoxTable::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_POS_MOVE, m_ctrBtnMove);
	DDX_Control(pDX, IDC_BTN_POS_DN, m_ctrBtnMoveDn);
	DDX_Control(pDX, IDC_BTN_POS_UP, m_ctrBtnMoveUp);
	DDX_Control(pDX, IDC_BTN_SAVE, m_ctrBtnValueSave);
	DDX_Control(pDX, IDC_BTN_CANCEL, m_ctrBtnSaveCancel);
	DDX_Control(pDX, IDC_BTN_INIT, m_ctrBtnMoveDefault);
	DDX_Radio(pDX, IDC_RADIO_POS8, m_nTableIndex);
	DDX_Control(pDX, IDC_BTN_MOTION_STOP, m_ctrBtnBT01ActionStop);
}


BEGIN_MESSAGE_MAP(CBoxTable, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CBoxTable::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CBoxTable::OnBnClickedBtnSave)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE, &CBoxTable::OnBnClickedBtnPosMove)
	ON_BN_CLICKED(IDC_BTN_POS_DN, &CBoxTable::OnBnClickedBtnPosDn)
	ON_BN_CLICKED(IDC_BTN_POS_UP, &CBoxTable::OnBnClickedBtnPosUp)
	ON_BN_CLICKED(IDC_BTN_INIT, &CBoxTable::OnBnClickedBtnInit)
	ON_BN_CLICKED(IDC_BTN_MOTION_STOP, &CBoxTable::OnBnClickedBtnMotionStop)
END_MESSAGE_MAP()


// CBoxTable 메시지 처리기입니다.
BOOL CBoxTable::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CBoxTable::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_EditFont.DeleteObject();

	CDialogEx::PostNcDestroy();
}


HBRUSH CBoxTable::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();	

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);			
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CBoxTable::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);
	
	tColorScheme tColor;
	m_ctrBtnMove.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnMove.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	m_ctrBtnMove.SetTextColor(&tColor);
	m_ctrBtnMove.SetCheckButton(false);			
	m_ctrBtnMove.SetFont(&tFont);
	m_ctrBtnMove.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnMoveDn.SetTextColor(&tColor);
	m_ctrBtnMoveDn.SetCheckButton(false);			
	m_ctrBtnMoveDn.SetFont(&tFont);
	m_ctrBtnMoveDn.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnMoveUp.SetTextColor(&tColor);
	m_ctrBtnMoveUp.SetCheckButton(false);			
	m_ctrBtnMoveUp.SetFont(&tFont);
	m_ctrBtnMoveUp.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnValueSave.SetTextColor(&tColor);
	m_ctrBtnValueSave.SetCheckButton(false);			
	m_ctrBtnValueSave.SetFont(&tFont);
	m_ctrBtnValueSave.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnSaveCancel.SetTextColor(&tColor);
	m_ctrBtnSaveCancel.SetCheckButton(false);			
	m_ctrBtnSaveCancel.SetFont(&tFont);
	m_ctrBtnSaveCancel.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnMoveDefault.SetTextColor(&tColor);
	m_ctrBtnMoveDefault.SetCheckButton(false);			
	m_ctrBtnMoveDefault.SetFont(&tFont);
	m_ctrBtnMoveDefault.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnBT01ActionStop.SetTextColor(&tColor);	m_ctrBtnBT01ActionStop.SetCheckButton(false);
	m_ctrBtnBT01ActionStop.SetFont(&tFont);	m_ctrBtnBT01ActionStop.SetRoundButtonStyle(&m_tStyle1);

}

int CBoxTable::m_fnCreateControlFont(void)
{	
// 	GetDlgItem(IDC_EDIT_BAD_POS)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS1)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS2)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS3)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS4)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS5)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS6)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS7)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_READY_POS)->SetFont(&m_EditFont);

	//SetDlgItemInt(IDC_EDIT_Z_POS_UP, 100);
	//SetDlgItemInt(IDC_EDIT_Z_POS_DN, 100);
	return 0;
}

BOOL CBoxTable::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);
	m_EditFont.CreatePointFont(100,"Arial");	

	m_fnCreateControlFont();

	m_fnCreateButton();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CBoxTable::m_fnSetParameter(SBT01_POSITION* stPointer)
{
	m_st_BT01_Pos = stPointer;	
	return 0;
}

int CBoxTable::m_fnDisplayParameter(void)
{
	SetDlgItemInt(IDC_EDIT_BAD_POS, m_st_BT01_Pos->nBadBoxPosition);
	SetDlgItemInt(IDC_EDIT_POS1, m_st_BT01_Pos->nBox1Position);
	SetDlgItemInt(IDC_EDIT_POS2, m_st_BT01_Pos->nBox2Position);
	SetDlgItemInt(IDC_EDIT_POS3, m_st_BT01_Pos->nBox3Position);
	SetDlgItemInt(IDC_EDIT_POS4, m_st_BT01_Pos->nBox4Position);
	SetDlgItemInt(IDC_EDIT_POS5, m_st_BT01_Pos->nBox5Position);
	SetDlgItemInt(IDC_EDIT_POS6, m_st_BT01_Pos->nBox6Position);
	SetDlgItemInt(IDC_EDIT_POS7, m_st_BT01_Pos->nBox7Position);
	SetDlgItemInt(IDC_EDIT_READY_POS, m_st_BT01_Pos->nReadyPosition);
	return 0;
}

void CBoxTable::OnBnClickedBtnCancel()
{
	m_fnDisplayParameter();
}

void CBoxTable::OnBnClickedBtnSave()
{
	CString strWriteData;

	m_st_BT01_Pos->nBadBoxPosition = GetDlgItemInt(IDC_EDIT_BAD_POS);
	m_st_BT01_Pos->nBox1Position = GetDlgItemInt(IDC_EDIT_POS1);
	m_st_BT01_Pos->nBox2Position = GetDlgItemInt(IDC_EDIT_POS2);
	m_st_BT01_Pos->nBox3Position = GetDlgItemInt(IDC_EDIT_POS3);
	m_st_BT01_Pos->nBox4Position = GetDlgItemInt(IDC_EDIT_POS4);
	m_st_BT01_Pos->nBox5Position = GetDlgItemInt(IDC_EDIT_POS5);
	m_st_BT01_Pos->nBox6Position = GetDlgItemInt(IDC_EDIT_POS6);
	m_st_BT01_Pos->nBox7Position = GetDlgItemInt(IDC_EDIT_POS7);
	m_st_BT01_Pos->nReadyPosition = GetDlgItemInt(IDC_EDIT_READY_POS);

	strWriteData.Format("%d", m_st_BT01_Pos->nBadBoxPosition);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_BAD_POS, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BT01_Pos->nBox1Position);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS1, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BT01_Pos->nBox2Position);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS2, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BT01_Pos->nBox3Position);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS3, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BT01_Pos->nBox4Position);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS4, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BT01_Pos->nBox5Position);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS5, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BT01_Pos->nBox6Position);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS6, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BT01_Pos->nBox7Position);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS7, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_BT01_Pos->nReadyPosition);
	WritePrivateProfileString(SECTION_BT01_POS_DATA, KEY_READY_POS, strWriteData, SYSTEM_PARAM_INI_PATH);	
}

void CBoxTable::OnBnClickedBtnPosMove()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nTableIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_BAD_POS);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS1);		break;
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS2);		break;
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS3);		break;
	case 4:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS4);		break;
	case 5:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS5);		break;
	case 6:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS6);		break;
	case 7:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS7);		break;
	case 8:
		nPosValue = GetDlgItemInt(IDC_EDIT_READY_POS);		break;
	}

	m_nTargetBoxPos = m_nTableIndex;
	m_fnBT01UpDnMove(nPosValue);
	
}

void CBoxTable::OnBnClickedBtnPosDn()
{
	UpdateData(TRUE);	
	int nPosValue = 0;
	m_nTargetBoxPos = m_nTableIndex;

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN);

	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);	
	nPosValue = nPosValue - nOffsetPos;	

	m_fnBT01UpDnMove(nPosValue);
}

void CBoxTable::OnBnClickedBtnPosUp()
{
	UpdateData(TRUE);	
	int nPosValue = 0;
	m_nTargetBoxPos = m_nTableIndex;

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);	
	nPosValue = nPosValue + nOffsetPos;	

	m_fnBT01UpDnMove(nPosValue);
}

void CBoxTable::m_fnBT01UpDnMove(int nPosValue)
{
	CString strLogMessage;

	if(m_fnBT01InterlockCheck(AXIS_BT01_CassetteUpDown, nPosValue))
		return;

	if(G_MOTION_STATUS[BT01_UPDN].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[BT01_UPDN].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[BT01_UPDN].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[BT01_UPDN].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[BT01_UPDN].UN_CMD_COUNT++;

			m_fnBT01PositionMove(nPosValue, nBiz_Seq_BT01_CassetteUpDown, TIME_OUT_30_SECOND);

			strLogMessage.Format("BT01_UPDN : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("BT01_UPDN : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("BT01_UPDN : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BT01_UPDN].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[BT01_UPDN].UN_CMD_COUNT = 0;
	}	
}


bool CBoxTable::m_fnBT01InterlockCheck(int AxisNum, long newPosition)
{
	CString strLogMessage;

	CHECK_AXIS_BT01(AxisNum);	
	switch(AxisNum)
	{
		//BT01
	case AXIS_BT01_CassetteUpDown:
		{
			int nTableUpDnMin = GetPrivateProfileInt(Section_BT01, Key_Table_UpDn_Min, 0, Interlock_PARAM_INI_PATH);
			int nTableUpDnMax = GetPrivateProfileInt(Section_BT01, Key_Table_UpDn_Max, 0, Interlock_PARAM_INI_PATH);
			int nSafeZoneMin = GetPrivateProfileInt(Section_BT01, Key_UpDn_SafeZone_TM01_Min, 0, Interlock_PARAM_INI_PATH);
			int nSafeZoneMax = GetPrivateProfileInt(Section_BT01, Key_UpDn_SafeZone_TM01_Max, 0, Interlock_PARAM_INI_PATH);

			if(!(nSafeZoneMin == 0 && nSafeZoneMax == 0))			
			{
				if(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] < nSafeZoneMin || 
					G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward]  > nSafeZoneMax)
				{
					m_fnLogDisplay("AXIS_BT01_CassetteUpDown : AXIS_TM01_ForwardBackward Position Interlock", TEXT_WARRING_YELLOW);
					return true;
				}
			}

			if(!(nTableUpDnMin == 0 && nTableUpDnMax == 0))			
			{
				if(newPosition < nTableUpDnMin || newPosition > nTableUpDnMax)
				{
					strLogMessage.Format("AXIS_BT01_CassetteUpDown : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
						nTableUpDnMin, newPosition, nTableUpDnMax );
					m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
					return true;
				}
			}
			break;
		}
	default: 
		return true;
	}
	return false;
}

void CBoxTable::m_fnBT01PositionMove(int nPosValue, int nMotionType, int nTimeOut)
{	
	m_stBT01_Mess.Reset();

	m_stBT01_Mess.nAction = m_stBT01_Mess.Act_Move;
	m_stBT01_Mess.nResult = m_stBT01_Mess.Ret_None;
	m_stBT01_Mess.newPosition1 = nPosValue;
	m_stBT01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stBT01_Mess, nMessSize);

	int nReturnCode = g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize,	chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete []chMsgBuf;	
}

void CBoxTable::m_fnBT01ActionStop(int nMotionType, int nTimeOut)
{
	m_stBT01_Mess.Reset();

	m_stBT01_Mess.nAction = m_stBT01_Mess.Act_Stop;
	m_stBT01_Mess.nResult = m_stBT01_Mess.Ret_None;
	m_stBT01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stBT01_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize, chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete[]chMsgBuf;
}

void CBoxTable::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{	
	//CVS11IndexerDlg*  vpMainDlg = (CVS11IndexerDlg*)AfxGetApp()->m_pMainWnd;
	g_cpMainDlg->m_fnAppendList(strLog, nColorType);
}

int CBoxTable::m_fn_BT01_UpDn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[BT01_UPDN].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	

	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "BT01_UPDN : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[BT01_UPDN].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[BT01_UPDN].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "BT01_UPDN : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BT01_UPDN].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("BT01_UPDN : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[BT01_UPDN].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnBT01UpDnMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("BT01_UPDN : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[BT01_UPDN].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

void CBoxTable::OnBnClickedBtnInit()
{
	int nPosValue = 0;

	nPosValue = GetDlgItemInt(IDC_EDIT_READY_POS);

	CString strLogMessage;	

	strLogMessage.Format("BT01_UPDN : 대기 위치 이동");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnBT01UpDnMove(nPosValue);
}


void CBoxTable::OnBnClickedBtnMotionStop()
{
	m_fnBT01ActionStop(nBiz_Seq_BT01_CassetteUpDown, TIME_OUT_10_SECOND);
}
