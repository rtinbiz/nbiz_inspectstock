#pragma once

class CBoxControl
{
public:
	CBoxControl();
	~CBoxControl();
	
	int m_fnSetParameter(
		SBT01_POSITION* stpointer1, 
		STM01_POSITION* stpointer2,
		SLT01_POSITION* stpointer3,
		SBO01_POSITION* stpointer4,
		SUT01_POSITION* stpointer5);

	int m_fnBoxMove_F_BT01_T_LT01();
	void m_fnBoxMove_F_BT01_T_LT01_Step(EL_BOX_LOADING step);

	int m_fnBoxCoverOpen();

	int m_fnBoxCoverClose();

	int m_fnBoxMove_F_UT01_T_LT01();

	int m_fnBoxMove_F_LT01_T_BT01();	
	
	bool m_fnLoadingTableBoxSizeCheck();

	int m_fnGetNextBoxIndex(BOX_STATUS elBoxStatus);

	int m_fnGetNextBoxPos(int nBoxIndex);

	int m_fnGet_LT01_PusherPos();

	int m_fnGet_UT01_PusherPos();

	int m_fnGet_BO01_Drive_Pos_At_LT01();

	int m_fnGet_BO01_Drive_Pos_At_UT01();

	void m_fnSet_Current_Box_Index(UINT unBoxIndex);
private:
	UINT m_unCurrentBoxIndex;

public:
	EL_BOX_LOADING m_elBoxLoadStep;
	EL_COVER_OPEN  m_elCoverOpenStep;
	EL_COVER_CLOSE m_elCoverCloseStep;
	EL_BOX_MOVE_TO_LT01 m_elBoxMoveToLT01Step;
	EL_BOX_UNLOADING m_elBoxUnloadingStep;
	
	SEQUENCE_ERROR m_elSeq_Error;
	BOX_SIZE		m_elBoxSize;

	const SBT01_POSITION*  m_st_BT01_Pos;
	const STM01_POSITION* m_st_TM01_Pos;
	const SLT01_POSITION* m_st_LT01_Pos;
	const SBO01_POSITION* m_st_BO01_Pos;
	const SUT01_POSITION* m_st_UT01_Pos;

	UINT m_unBox_Load_Timeout;
	UINT m_unVac_Off_Timeout;
	UINT m_unCover_Open_Timeout;

	
};

