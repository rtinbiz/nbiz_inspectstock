// StickTransper.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "StickTransper.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CStickTransper 대화 상자입니다.

IMPLEMENT_DYNAMIC(CStickTransper, CDialogEx)

CStickTransper::CStickTransper(CWnd* pParent /*=NULL*/)
	: CDialogEx(CStickTransper::IDD, pParent)
	, m_nCtrDriveIndex(0)
	, m_nCtrThetaIndex(0)
	, m_nCtrShiftIndex(0)
	, m_nCtrStickZIndex(0)
	, m_nCtrPaperZIndex(0)
{

}

CStickTransper::~CStickTransper()
{
}

void CStickTransper::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_POS_MOVE, m_ctrBtnMove);
	DDX_Control(pDX, IDC_BTN_POS_DN, m_ctrBtnMoveDn);
	DDX_Control(pDX, IDC_BTN_POS_UP, m_ctrBtnMoveUp);
	DDX_Control(pDX, IDC_BTN_SAVE, m_ctrBtnValueSave);
	DDX_Control(pDX, IDC_BTN_CANCEL, m_ctrBtnSaveCancel);
	DDX_Control(pDX, IDC_BTN_INIT, m_ctrBtnMoveDefault);
	DDX_Control(pDX, IDC_BTN_POS_MOVE_T, m_ctrBtnMoveT);
	DDX_Control(pDX, IDC_BTN_POS_DN_T, m_ctrBtnMoveDnT);
	DDX_Control(pDX, IDC_BTN_POS_UP_T, m_ctrBtnMoveUpT);
	DDX_Control(pDX, IDC_BTN_POS_MOVE_S, m_ctrBtnMoveS);
	DDX_Control(pDX, IDC_BTN_POS_DN_S, m_ctrBtnMoveDnS);
	DDX_Control(pDX, IDC_BTN_POS_UP_S, m_ctrBtnMoveUpS);
	DDX_Control(pDX, IDC_BTN_POS_MOVE_Z, m_ctrBtnMoveSZ);
	DDX_Control(pDX, IDC_BTN_POS_DN_Z, m_ctrBtnMoveDnSZ);
	DDX_Control(pDX, IDC_BTN_POS_UP_Z, m_ctrBtnMoveUpSZ);
	DDX_Control(pDX, IDC_BTN_POS_MOVE_Z2, m_ctrBtnMovePZ);
	DDX_Control(pDX, IDC_BTN_POS_DN_Z2, m_ctrBtnMoveDnPZ);
	DDX_Control(pDX, IDC_BTN_POS_UP_Z2, m_ctrBtnMoveUpPZ);
	DDX_Control(pDX, IDC_BTN_STICK_VAC_ON, m_ctrBtnVacOnS);
	DDX_Control(pDX, IDC_BTN_STICK_VAC_OFF, m_ctrBtnVacOffS);
	DDX_Control(pDX, IDC_BTN_STICK_PURGE_ON, m_ctrBtnPurgeS);
	DDX_Control(pDX, IDC_BTN_PAPER_VAC_ON, m_ctrBtnVacOnP);
	DDX_Control(pDX, IDC_BTN_PAPER_VAC_OFF, m_ctrBtnVacOffP);
	DDX_Control(pDX, IDC_BTN_PAPER_PURGE_ON, m_ctrBtnPurgeP);
	DDX_Control(pDX, IDC_BTN_READ_QR, m_ctrBtnQRCode);
	DDX_Control(pDX, IDC_BTN_SET_QR, m_ctrBtnQRCodeSet);
	DDX_Control(pDX, IDC_BTN_PAPER_CYL_UP, m_ctrBtnCylUpLR);
	DDX_Control(pDX, IDC_BTN_PAPER_CYL_DN, m_ctrBtnCylDnLR);
	DDX_Control(pDX, IDC_BTN_PAPER_CYL_UP2, m_ctrBtnCylUpMid);
	DDX_Control(pDX, IDC_BTN_PAPER_CYL_DN2, m_ctrBtnCylDnMid);
	DDX_Radio(pDX, IDC_RADIO_TM02_BT01, m_nCtrDriveIndex);
	DDX_Radio(pDX, IDC_RADIO_TM02_BT01_T, m_nCtrThetaIndex);
	DDX_Radio(pDX, IDC_RADIO_TM02_BT01_S, m_nCtrShiftIndex);
	DDX_Radio(pDX, IDC_RADIO_TM02_READY_Z, m_nCtrStickZIndex);
	DDX_Radio(pDX, IDC_RADIO_TM02_BT01_Z2, m_nCtrPaperZIndex);
	DDX_Control(pDX, IDC_SLIDER_LIGHT1_1, m_ctrSliderLight1_1);
	DDX_Control(pDX, IDC_BTN_LIGHT, m_ctrBtnLightSet);
	DDX_Control(pDX, IDC_BTN_MOTION_STOP, m_ctrBtnTM02ActionStop);
	DDX_Control(pDX, IDC_BTN_MANUAL_ALIGN, m_ctrBtnManualAlign);
	DDX_Control(pDX, IDC_BTN_THRESHOLD_SET, m_ctrBtnZThresholdSet);
}


BEGIN_MESSAGE_MAP(CStickTransper, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_SAVE, &CStickTransper::OnBnClickedBtnSave)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CStickTransper::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE, &CStickTransper::OnBnClickedBtnPosMove)
	ON_BN_CLICKED(IDC_BTN_POS_DN, &CStickTransper::OnBnClickedBtnPosDn)
	ON_BN_CLICKED(IDC_BTN_POS_UP, &CStickTransper::OnBnClickedBtnPosUp)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE_T, &CStickTransper::OnBnClickedBtnPosMoveT)
	ON_BN_CLICKED(IDC_BTN_POS_DN_T, &CStickTransper::OnBnClickedBtnPosDnT)
	ON_BN_CLICKED(IDC_BTN_POS_UP_T, &CStickTransper::OnBnClickedBtnPosUpT)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE_S, &CStickTransper::OnBnClickedBtnPosMoveS)
	ON_BN_CLICKED(IDC_BTN_POS_DN_S, &CStickTransper::OnBnClickedBtnPosDnS)
	ON_BN_CLICKED(IDC_BTN_POS_UP_S, &CStickTransper::OnBnClickedBtnPosUpS)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE_Z, &CStickTransper::OnBnClickedBtnPosMoveZ)
	ON_BN_CLICKED(IDC_BTN_POS_DN_Z, &CStickTransper::OnBnClickedBtnPosDnZ)
	ON_BN_CLICKED(IDC_BTN_POS_UP_Z, &CStickTransper::OnBnClickedBtnPosUpZ)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE_Z2, &CStickTransper::OnBnClickedBtnPosMoveZ2)
	ON_BN_CLICKED(IDC_BTN_POS_DN_Z2, &CStickTransper::OnBnClickedBtnPosDnZ2)
	ON_BN_CLICKED(IDC_BTN_POS_UP_Z2, &CStickTransper::OnBnClickedBtnPosUpZ2)
	ON_BN_CLICKED(IDC_BTN_PAPER_CYL_UP, &CStickTransper::OnBnClickedBtnPaperCylUp)
	ON_BN_CLICKED(IDC_BTN_PAPER_CYL_DN, &CStickTransper::OnBnClickedBtnPaperCylDn)
	ON_BN_CLICKED(IDC_BTN_PAPER_CYL_UP2, &CStickTransper::OnBnClickedBtnPaperCylUp2)
	ON_BN_CLICKED(IDC_BTN_PAPER_CYL_DN2, &CStickTransper::OnBnClickedBtnPaperCylDn2)
	ON_BN_CLICKED(IDC_BTN_STICK_VAC_ON, &CStickTransper::OnBnClickedBtnStickVacOn)
	ON_BN_CLICKED(IDC_BTN_STICK_VAC_OFF, &CStickTransper::OnBnClickedBtnStickVacOff)
	ON_BN_CLICKED(IDC_BTN_STICK_PURGE_ON, &CStickTransper::OnBnClickedBtnStickPurgeOn)
	ON_BN_CLICKED(IDC_BTN_PAPER_VAC_ON, &CStickTransper::OnBnClickedBtnPaperVacOn)
	ON_BN_CLICKED(IDC_BTN_PAPER_VAC_OFF, &CStickTransper::OnBnClickedBtnPaperVacOff)
	ON_BN_CLICKED(IDC_BTN_PAPER_PURGE_ON, &CStickTransper::OnBnClickedBtnPaperPurgeOn)
	ON_BN_CLICKED(IDC_BTN_READ_QR, &CStickTransper::OnBnClickedBtnReadQr)
	ON_WM_VSCROLL()
	ON_BN_CLICKED(IDC_BTN_LIGHT, &CStickTransper::OnBnClickedBtnLight)
	ON_BN_CLICKED(IDC_BTN_MOTION_STOP, &CStickTransper::OnBnClickedBtnMotionStop)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_BTN_SET_QR, &CStickTransper::OnBnClickedBtnSetQr)
	ON_BN_CLICKED(IDC_BTN_MANUAL_ALIGN, &CStickTransper::OnBnClickedBtnManualAlign)
	ON_BN_CLICKED(IDC_BTN_THRESHOLD_SET, &CStickTransper::OnBnClickedBtnThresholdSet)
END_MESSAGE_MAP()


// CStickTransper 메시지 처리기입니다.

BOOL CStickTransper::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CStickTransper::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_EditFont.DeleteObject();
	CDialogEx::PostNcDestroy();
}

HBRUSH CStickTransper::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CStickTransper::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style On
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);

	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyleOff.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	m_ctrBtnMove.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnMove.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	m_ctrBtnVacOnS.SetTextColor(&tColor);
	m_ctrBtnVacOnS.SetCheckButton(false);			
	m_ctrBtnVacOnS.SetFont(&tFont);
	m_ctrBtnVacOnS.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOffS.SetTextColor(&tColor);
	m_ctrBtnVacOffS.SetCheckButton(false);			
	m_ctrBtnVacOffS.SetFont(&tFont);
	m_ctrBtnVacOffS.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnPurgeS.SetTextColor(&tColor);
	m_ctrBtnPurgeS.SetCheckButton(false);			
	m_ctrBtnPurgeS.SetFont(&tFont);
	m_ctrBtnPurgeS.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOnP.SetTextColor(&tColor);
	m_ctrBtnVacOnP.SetCheckButton(false);			
	m_ctrBtnVacOnP.SetFont(&tFont);
	m_ctrBtnVacOnP.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOffP.SetTextColor(&tColor);
	m_ctrBtnVacOffP.SetCheckButton(false);			
	m_ctrBtnVacOffP.SetFont(&tFont);
	m_ctrBtnVacOffP.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnPurgeP.SetTextColor(&tColor);
	m_ctrBtnPurgeP.SetCheckButton(false);			
	m_ctrBtnPurgeP.SetFont(&tFont);
	m_ctrBtnPurgeP.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnQRCode.SetTextColor(&tColor);
	m_ctrBtnQRCode.SetCheckButton(false);			
	m_ctrBtnQRCode.SetFont(&tFont);
	m_ctrBtnQRCode.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnQRCodeSet.SetTextColor(&tColor);
	m_ctrBtnQRCodeSet.SetCheckButton(false);			
	m_ctrBtnQRCodeSet.SetFont(&tFont);
	m_ctrBtnQRCodeSet.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnCylUpLR.SetTextColor(&tColor);
	m_ctrBtnCylUpLR.SetCheckButton(false);			
	m_ctrBtnCylUpLR.SetFont(&tFont);
	m_ctrBtnCylUpLR.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnCylDnLR.SetTextColor(&tColor);
	m_ctrBtnCylDnLR.SetCheckButton(false);			
	m_ctrBtnCylDnLR.SetFont(&tFont);
	m_ctrBtnCylDnLR.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnCylUpMid.SetTextColor(&tColor);
	m_ctrBtnCylUpMid.SetCheckButton(false);			
	m_ctrBtnCylUpMid.SetFont(&tFont);
	m_ctrBtnCylUpMid.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnCylDnMid.SetTextColor(&tColor);
	m_ctrBtnCylDnMid.SetCheckButton(false);			
	m_ctrBtnCylDnMid.SetFont(&tFont);
	m_ctrBtnCylDnMid.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnMove.SetTextColor(&tColor);
	m_ctrBtnMove.SetCheckButton(false);			
	m_ctrBtnMove.SetFont(&tFont);
	m_ctrBtnMove.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDn.SetTextColor(&tColor);
	m_ctrBtnMoveDn.SetCheckButton(false);			
	m_ctrBtnMoveDn.SetFont(&tFont);
	m_ctrBtnMoveDn.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUp.SetTextColor(&tColor);
	m_ctrBtnMoveUp.SetCheckButton(false);			
	m_ctrBtnMoveUp.SetFont(&tFont);
	m_ctrBtnMoveUp.SetRoundButtonStyle(&m_tStyle1);	
	
	m_ctrBtnMoveT.SetTextColor(&tColor);
	m_ctrBtnMoveT.SetCheckButton(false);			
	m_ctrBtnMoveT.SetFont(&tFont);
	m_ctrBtnMoveT.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDnT.SetTextColor(&tColor);
	m_ctrBtnMoveDnT.SetCheckButton(false);			
	m_ctrBtnMoveDnT.SetFont(&tFont);
	m_ctrBtnMoveDnT.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUpT.SetTextColor(&tColor);
	m_ctrBtnMoveUpT.SetCheckButton(false);			
	m_ctrBtnMoveUpT.SetFont(&tFont);
	m_ctrBtnMoveUpT.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveS.SetTextColor(&tColor);
	m_ctrBtnMoveS.SetCheckButton(false);			
	m_ctrBtnMoveS.SetFont(&tFont);
	m_ctrBtnMoveS.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDnS.SetTextColor(&tColor);
	m_ctrBtnMoveDnS.SetCheckButton(false);			
	m_ctrBtnMoveDnS.SetFont(&tFont);
	m_ctrBtnMoveDnS.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUpS.SetTextColor(&tColor);
	m_ctrBtnMoveUpS.SetCheckButton(false);			
	m_ctrBtnMoveUpS.SetFont(&tFont);
	m_ctrBtnMoveUpS.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnMoveSZ.SetTextColor(&tColor);
	m_ctrBtnMoveSZ.SetCheckButton(false);			
	m_ctrBtnMoveSZ.SetFont(&tFont);
	m_ctrBtnMoveSZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDnSZ.SetTextColor(&tColor);
	m_ctrBtnMoveDnSZ.SetCheckButton(false);			
	m_ctrBtnMoveDnSZ.SetFont(&tFont);
	m_ctrBtnMoveDnSZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUpSZ.SetTextColor(&tColor);
	m_ctrBtnMoveUpSZ.SetCheckButton(false);			
	m_ctrBtnMoveUpSZ.SetFont(&tFont);
	m_ctrBtnMoveUpSZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMovePZ.SetTextColor(&tColor);
	m_ctrBtnMovePZ.SetCheckButton(false);			
	m_ctrBtnMovePZ.SetFont(&tFont);
	m_ctrBtnMovePZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDnPZ.SetTextColor(&tColor);
	m_ctrBtnMoveDnPZ.SetCheckButton(false);			
	m_ctrBtnMoveDnPZ.SetFont(&tFont);
	m_ctrBtnMoveDnPZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUpPZ.SetTextColor(&tColor);
	m_ctrBtnMoveUpPZ.SetCheckButton(false);			
	m_ctrBtnMoveUpPZ.SetFont(&tFont);
	m_ctrBtnMoveUpPZ.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnValueSave.SetTextColor(&tColor);
	m_ctrBtnValueSave.SetCheckButton(false);			
	m_ctrBtnValueSave.SetFont(&tFont);
	m_ctrBtnValueSave.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnSaveCancel.SetTextColor(&tColor);
	m_ctrBtnSaveCancel.SetCheckButton(false);			
	m_ctrBtnSaveCancel.SetFont(&tFont);
	m_ctrBtnSaveCancel.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDefault.SetTextColor(&tColor);
	m_ctrBtnMoveDefault.SetCheckButton(false);			
	m_ctrBtnMoveDefault.SetFont(&tFont);
	m_ctrBtnMoveDefault.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnLightSet.SetTextColor(&tColor);
	m_ctrBtnLightSet.SetCheckButton(false);
	m_ctrBtnLightSet.SetFont(&tFont);
	m_ctrBtnLightSet.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnTM02ActionStop.SetTextColor(&tColor);
	m_ctrBtnTM02ActionStop.SetCheckButton(false);
	m_ctrBtnTM02ActionStop.SetFont(&tFont);
	m_ctrBtnTM02ActionStop.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnManualAlign.SetTextColor(&tColor);
	m_ctrBtnManualAlign.SetCheckButton(false);			
	m_ctrBtnManualAlign.SetFont(&tFont);
	m_ctrBtnManualAlign.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnZThresholdSet.SetTextColor(&tColor);
	m_ctrBtnZThresholdSet.SetCheckButton(false);			
	m_ctrBtnZThresholdSet.SetFont(&tFont);
	m_ctrBtnZThresholdSet.SetRoundButtonStyle(&m_tStyle1);	
	
}

int CStickTransper::m_fnCreateControlFont(void)
{	
// 	GetDlgItem(IDC_EDIT_BAD_POS)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS1)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS2)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS3)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS4)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS5)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS6)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS7)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_READY_POS)->SetFont(&m_EditFont);

	//SetDlgItemInt(IDC_EDIT_Z_POS_UP, 100);
	//SetDlgItemInt(IDC_EDIT_Z_POS_DN, 100);
	return 0;
}

BOOL CStickTransper::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);
	m_EditFont.CreatePointFont(100,"Arial");	

	m_fnCreateControlFont();
	m_fnCreateButton();

	m_ctrSliderLight1_1.SetRange(0, 255);
	m_ctrSliderLight1_1.SetPos(255);
	SetDlgItemInt(IDC_STATIC_LIGHT1_1, 0);

	SetDlgItemInt(IDC_EDIT_THRESHOLD_VAL,120);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CStickTransper::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSliderCtrl* pSlider = (CSliderCtrl*)pScrollBar;

	if (pSlider == &m_ctrSliderLight1_1)
		SetDlgItemInt(IDC_STATIC_LIGHT1_1, 255 - pSlider->GetPos());	

	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CStickTransper::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSliderCtrl* pSlider = (CSliderCtrl*)pScrollBar;

	if (pSlider == &m_ctrSliderLight1_1)
		SetDlgItemInt(IDC_STATIC_LIGHT1_1, pSlider->GetPos());

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


int CStickTransper::m_fnSetParameter(STM02_POSITION* stPointer)
{
	m_st_TM02_Pos = stPointer;	
	return 0;
}

int CStickTransper::m_fnDisplayParameter(void)
{
	SetDlgItemInt(IDC_EDIT_TM02_BT01_POS, m_st_TM02_Pos->nDrive_BT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_BT01_PP_POS, m_st_TM02_Pos->nDrive_BT01_PP_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_LT01_POS, m_st_TM02_Pos->nDrive_LT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT01_POS, m_st_TM02_Pos->nDrive_UT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT01_PP_POS, m_st_TM02_Pos->nDrive_UT01_PP_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_TT01_POS, m_st_TM02_Pos->nDrive_TT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT02_POS, m_st_TM02_Pos->nDrive_UT02_Pos);	
	SetDlgItemInt(IDC_EDIT_TM02_QR_POS, m_st_TM02_Pos->nDrive_QR_Pos);	
	SetDlgItemInt(IDC_EDIT_TM02_ALIGN_POS, m_st_TM02_Pos->nDrive_Align_Pos);	

	SetDlgItemInt(IDC_EDIT_TM02_BT01_POS_T, m_st_TM02_Pos->nT_BT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_LT01_POS_T, m_st_TM02_Pos->nT_LT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT01_POS_T, m_st_TM02_Pos->nT_UT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_TT01_POS_T, m_st_TM02_Pos->nT_TT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT02_POS_T, m_st_TM02_Pos->nT_UT02_Pos);	
	SetDlgItemInt(IDC_EDIT_TM02_QR_POS_T, m_st_TM02_Pos->nT_QR_Pos);

	SetDlgItemInt(IDC_EDIT_TM02_BT01_POS_S, m_st_TM02_Pos->nShift_BT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_LT01_POS_S, m_st_TM02_Pos->nShift_LT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT01_POS_S, m_st_TM02_Pos->nShift_UT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_TT01_POS_S, m_st_TM02_Pos->nShift_TT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT02_POS_S, m_st_TM02_Pos->nShift_UT02_Pos);	
	SetDlgItemInt(IDC_EDIT_TM02_QR_POS_S, m_st_TM02_Pos->nShift_QR_Pos);

	SetDlgItemInt(IDC_EDIT_TM02_BT01_POS_Z, m_st_TM02_Pos->nStickZ_BT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_LT01_POS_Z, m_st_TM02_Pos->nStickZ_LT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT01_POS_Z, m_st_TM02_Pos->nStickZ_UT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_TT01_POS_Z, m_st_TM02_Pos->nStickZ_TT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT02_POS_Z, m_st_TM02_Pos->nStickZ_UT02_Pos);	
	SetDlgItemInt(IDC_EDIT_TM02_QR_POS_Z, m_st_TM02_Pos->nStickZ_QR_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_READY_POS_Z, m_st_TM02_Pos->nStickZ_Ready_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_ALIGN_POS_Z, m_st_TM02_Pos->nStickZ_Align_Pos);

	SetDlgItemInt(IDC_EDIT_TM02_BT01_POS_Z2, m_st_TM02_Pos->nPaperZ_BT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_LT01_POS_Z2, m_st_TM02_Pos->nPaperZ_LT01_Pos);
	SetDlgItemInt(IDC_EDIT_TM02_UT01_POS_Z2, m_st_TM02_Pos->nPaperZ_UT01_Pos);		

	SetDlgItemInt(IDC_EDIT_STICK_PURGE_SEC, m_st_TM02_Pos->nStickPurge_Time);
	SetDlgItemInt(IDC_EDIT_PAPER_PURGE_SEC, m_st_TM02_Pos->nPaperPurge_Time);

	SetDlgItemInt(IDC_STATIC_LIGHT1_1, m_st_TM02_Pos->nLightValue1_1);	

	m_ctrSliderLight1_1.SetPos(m_st_TM02_Pos->nLightValue1_1);

	return 0;
}

void CStickTransper::OnBnClickedBtnCancel()
{
	m_fnDisplayParameter();
}

void CStickTransper::OnBnClickedBtnSave()
{
	CString strWriteData;

	m_st_TM02_Pos->nDrive_BT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS);
	m_st_TM02_Pos->nDrive_BT01_PP_Pos = GetDlgItemInt(IDC_EDIT_TM02_BT01_PP_POS);
	m_st_TM02_Pos->nDrive_LT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS);	
	m_st_TM02_Pos->nDrive_UT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS);
	m_st_TM02_Pos->nDrive_UT01_PP_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT01_PP_POS);
	m_st_TM02_Pos->nDrive_TT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_TT01_POS);	
	m_st_TM02_Pos->nDrive_UT02_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT02_POS);
	m_st_TM02_Pos->nDrive_QR_Pos = GetDlgItemInt(IDC_EDIT_TM02_QR_POS);	
	m_st_TM02_Pos->nDrive_Align_Pos = GetDlgItemInt(IDC_EDIT_TM02_ALIGN_POS);

	m_st_TM02_Pos->nT_BT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS_T);
	m_st_TM02_Pos->nT_LT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS_T);	
	m_st_TM02_Pos->nT_UT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS_T);
	m_st_TM02_Pos->nT_TT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_TT01_POS_T);	
	m_st_TM02_Pos->nT_UT02_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT02_POS_T);
	m_st_TM02_Pos->nT_QR_Pos = GetDlgItemInt(IDC_EDIT_TM02_QR_POS_T);	

	m_st_TM02_Pos->nShift_BT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS_S);
	m_st_TM02_Pos->nShift_LT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS_S);	
	m_st_TM02_Pos->nShift_UT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS_S);
	m_st_TM02_Pos->nShift_TT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_TT01_POS_S);	
	m_st_TM02_Pos->nShift_UT02_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT02_POS_S);
	m_st_TM02_Pos->nShift_QR_Pos = GetDlgItemInt(IDC_EDIT_TM02_QR_POS_S);	

	m_st_TM02_Pos->nStickZ_BT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS_Z);
	m_st_TM02_Pos->nStickZ_LT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS_Z);	
	m_st_TM02_Pos->nStickZ_UT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS_Z);
	m_st_TM02_Pos->nStickZ_TT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_TT01_POS_Z);	
	m_st_TM02_Pos->nStickZ_UT02_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT02_POS_Z);
	m_st_TM02_Pos->nStickZ_QR_Pos = GetDlgItemInt(IDC_EDIT_TM02_QR_POS_Z);	
	m_st_TM02_Pos->nStickZ_Ready_Pos = GetDlgItemInt(IDC_EDIT_TM02_READY_POS_Z);
	m_st_TM02_Pos->nStickZ_Align_Pos = GetDlgItemInt(IDC_EDIT_TM02_ALIGN_POS_Z);

	m_st_TM02_Pos->nPaperZ_BT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS_Z2);
	m_st_TM02_Pos->nPaperZ_LT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS_Z2);	
	m_st_TM02_Pos->nPaperZ_UT01_Pos = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS_Z2);	

	m_st_TM02_Pos->nStickPurge_Time = GetDlgItemInt(IDC_EDIT_STICK_PURGE_SEC);
	m_st_TM02_Pos->nPaperPurge_Time = GetDlgItemInt(IDC_EDIT_PAPER_PURGE_SEC);	

	m_st_TM02_Pos->nLightValue1_1 = GetDlgItemInt(IDC_STATIC_LIGHT1_1);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_BT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_BT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_BT01_PP_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_BT01_PP, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_LT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_LT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_UT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_UT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_UT01_PP_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_UT01_PP, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_TT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_TT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_UT02_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_UT02, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_QR_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_QR, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nDrive_Align_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_ALIGN, strWriteData, SYSTEM_PARAM_INI_PATH);


	strWriteData.Format("%d", m_st_TM02_Pos->nT_BT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_BT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nT_LT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_LT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nT_UT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_UT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nT_TT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_TT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nT_UT02_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_UT02, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nT_QR_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_QR, strWriteData, SYSTEM_PARAM_INI_PATH);


	strWriteData.Format("%d", m_st_TM02_Pos->nShift_BT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_BT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nShift_LT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_LT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nShift_UT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_UT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nShift_TT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_TT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nShift_UT02_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_UT02, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nShift_QR_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_QR, strWriteData, SYSTEM_PARAM_INI_PATH);


	strWriteData.Format("%d", m_st_TM02_Pos->nStickZ_BT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_BT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nStickZ_LT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_LT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nStickZ_UT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_UT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nStickZ_TT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_TT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nStickZ_UT02_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_UT02, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nStickZ_QR_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_QR, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nStickZ_Ready_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_READY, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nStickZ_Align_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_ALIGN, strWriteData, SYSTEM_PARAM_INI_PATH);


	strWriteData.Format("%d", m_st_TM02_Pos->nPaperZ_BT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_PAPER_Z_POS_BT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nPaperZ_LT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_PAPER_Z_POS_LT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nPaperZ_UT01_Pos);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_PAPER_Z_POS_UT01, strWriteData, SYSTEM_PARAM_INI_PATH);

		
	strWriteData.Format("%d", m_st_TM02_Pos->nStickPurge_Time);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_PURGE_STICK_TIME, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nPaperPurge_Time);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_PURGE_PAPER_TIME, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM02_Pos->nLightValue1_1);
	WritePrivateProfileString(SECTION_TM02_POS_DATA, KEY_LIGHT_VALUE1_1, strWriteData, SYSTEM_PARAM_INI_PATH);	
}

void CStickTransper::OnBnClickedBtnPosMove()
{
	UpdateData(TRUE);
	int nPosValue = 0;

	switch(m_nCtrDriveIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_BT01_PP_POS);	break;
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS);	break;
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS);	break;
	case 4:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT01_PP_POS);	break;
	case 5:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_TT01_POS);	break;
	case 6:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT02_POS);	break;
	case 7:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_QR_POS);	break;
	case 8:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_ALIGN_POS);	break;
	}
	m_fnTM02DriveMove(nPosValue);
}

void CStickTransper::OnBnClickedBtnPosDn()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);	
	nPosValue = nPosValue - nOffsetPos;

	m_fnTM02DriveMove(nPosValue);

}

void CStickTransper::OnBnClickedBtnPosUp()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);	
	nPosValue = nPosValue + nOffsetPos;

	m_fnTM02DriveMove(nPosValue);
	
}

void CStickTransper::OnBnClickedBtnPosMoveT()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nCtrThetaIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS_T);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS_T);	break;	
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS_T);	break;	
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_TT01_POS_T);	break;	
	case 4:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT02_POS_T);	break;	
	case 5:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_QR_POS_T);	break;	
	}
	m_fnTM02ThetaMove(nPosValue);
}

void CStickTransper::OnBnClickedBtnPosDnT()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN_T);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_TILT);
	nPosValue = nPosValue - nOffsetPos;

	m_fnTM02ThetaMove(nPosValue);	
}

void CStickTransper::OnBnClickedBtnPosUpT()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP_T);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_TILT);
	nPosValue = nPosValue + nOffsetPos;

	m_fnTM02ThetaMove(nPosValue);	
}

void CStickTransper::OnBnClickedBtnPosMoveS()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nCtrShiftIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS_S);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS_S);	break;	
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS_S);	break;	
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_TT01_POS_S);	break;	
	case 4:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT02_POS_S);	break;	
	case 5:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_QR_POS_S);	break;	
	}
	m_fnTM02ShiftMove(nPosValue);
}

void CStickTransper::OnBnClickedBtnPosDnS()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN_S);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_SHIFT);	
	nPosValue = nPosValue - nOffsetPos;	

	m_fnTM02ShiftMove(nPosValue);
}

void CStickTransper::OnBnClickedBtnPosUpS()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP_S);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_SHIFT);	
	nPosValue = nPosValue + nOffsetPos;	

	m_fnTM02ShiftMove(nPosValue);
	
}

void CStickTransper::OnBnClickedBtnPosMoveZ()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nCtrStickZIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_READY_POS_Z);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS_Z);	break;
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS_Z);	break;
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS_Z);	break;
	case 4:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_TT01_POS_Z);	break;
	case 5:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT02_POS_Z);	break;
	case 6:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_QR_POS_Z);	break;
	case 7:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_ALIGN_POS_Z);	break;
	}
//stick 잡으러 갈때 간지 실린더 다운되어 있으면 경고,,,,/// 20150319, 정의천
	/*if (G_STMACHINE_STATUS.stPickerCylinder.In01_Sn_PickerPaperLeftDown == ON &&
	G_STMACHINE_STATUS.stPickerCylinder.In03_Sn_PickerPaperRightDown == ON)
	{
	AfxMessageBox("간지 실린더 다운 되어 있습니다.");
	return;
	}*/
	/////////////////////////////////////////////////////////////////////////////	
	m_fnTM02UpDnMove(nPosValue,Speed_Pickup);
}

void CStickTransper::OnBnClickedBtnPosDnZ()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN_Z);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);
	nPosValue = nPosValue - nOffsetPos;

	m_fnTM02UpDnMove(nPosValue);
}

void CStickTransper::OnBnClickedBtnPosUpZ()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP_Z);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);
	nPosValue = nPosValue + nOffsetPos;

	m_fnTM02UpDnMove(nPosValue);	
}

void CStickTransper::OnBnClickedBtnPosMoveZ2()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nCtrPaperZIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_BT01_POS_Z2);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_LT01_POS_Z2);	break;	
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM02_UT01_POS_Z2);	break;
	}
	m_fnTM02UpDnMove(nPosValue,Speed_Pickup);
}

void CStickTransper::OnBnClickedBtnPosDnZ2()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN_Z2);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);
	nPosValue = nPosValue - nOffsetPos;

	m_fnTM02UpDnMove(nPosValue);
}

void CStickTransper::OnBnClickedBtnPosUpZ2()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP_Z2);
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);
	nPosValue = nPosValue + nOffsetPos;

	m_fnTM02UpDnMove(nPosValue);	
}

void CStickTransper::OnBnClickedBtnPaperCylUp()
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;	
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_LR_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_LR_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_TM02_LR_PAPER_PICKER_UP, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_TM02_LR_PAPER_PICKER_UP, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM02_LR_CYL : UP.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM02_LR_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_LR_CYL : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LR_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CStickTransper::OnBnClickedBtnPaperCylDn()
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;	
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);
	
	CString strLogMessage;

	if(G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_LR_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_LR_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_TM02_LR_PAPER_PICKER_DN, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_TM02_LR_PAPER_PICKER_DN, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM02_LR_CYL : DOWN.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TM02_LR_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_LR_CYL : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LR_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CStickTransper::OnBnClickedBtnPaperCylUp2()
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;	
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_MID_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_MID_CYL].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_MID_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_TM02_CNT_PAPER_PICKER_UP, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_TM02_CNT_PAPER_PICKER_UP, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM02_MID_CYL : UP.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM02_MID_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_MID_CYL : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_MID_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CStickTransper::OnBnClickedBtnPaperCylDn2()
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;	
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_MID_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_MID_CYL].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_MID_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_TM02_CNT_PAPER_PICKER_DN, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_TM02_CNT_PAPER_PICKER_DN, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM02_MID_CYL : DOWN.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM02_MID_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_MID_CYL : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_MID_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CStickTransper::OnBnClickedBtnStickVacOn()
{
	m_fnCheckStickAirStep();

	CString strLogMessage;
	strLogMessage.Format("TM02_ST_VAC : ON.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickVacuum(m_stAirStep, nBiz_Seq_TM02_ST_Vac_ON);	
}

void CStickTransper::OnBnClickedBtnStickVacOff()
{
	m_fnCheckStickAirStep();

	CString strLogMessage;
	strLogMessage.Format("TM02_ST_VAC : OFF.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickVacuum(m_stAirStep, nBiz_Seq_TM02_ST_Vac_OFF);	
}

void CStickTransper::m_fnStickVacuum(AIR_SELECTION nAirSelect, int nActType)
{
	m_stTM02_Mess.Reset();
	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;
	m_stTM02_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TM02_ST_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_ST_VAC].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nActType, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nActType, nBiz_Unit_Zero
				);			

			
		}
		else
		{
			strLogMessage.Format("TM02_ST_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_ST_VAC : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_ST_VAC].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}



void CStickTransper::OnBnClickedBtnStickPurgeOn()
{
	int nPurgeTime = GetDlgItemInt(IDC_EDIT_STICK_PURGE_SEC);

	m_fnCheckStickAirStep();

	m_fnStickPurgeOn(m_stAirStep, nPurgeTime);
}

void CStickTransper::m_fnStickPurgeOn(AIR_SELECTION nAirSelect, int nPurgeTime)
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;
	m_stTM02_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stTM02_Mess.ActionTime = nPurgeTime;
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TM02_ST_PURGE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TM02_ST_PURGE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_ST_PURGE].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_ST_PURGE].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TM02_ST_PURGE].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_TM02_ST_Purge, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_TM02_ST_Purge, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM02_ST_PURGE : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM02_ST_PURGE : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_ST_PURGE : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_ST_PURGE].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TM02_ST_PURGE].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CStickTransper::OnBnClickedBtnPaperVacOn()
{
	m_fnCheckPaperAirStep();

	CString strLogMessage;
	strLogMessage.Format("TM02_PP_VAC : ON.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnPaperVacuum(m_stAirStep, nBiz_Seq_TM02_PP_Vac_ON);
}

void CStickTransper::OnBnClickedBtnPaperVacOff()
{
	m_fnCheckPaperAirStep();

	CString strLogMessage;
	strLogMessage.Format("TM02_PP_VAC : OFF.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnPaperVacuum(m_stAirStep, nBiz_Seq_TM02_PP_Vac_OFF);	
}


void CStickTransper::m_fnPaperVacuum(AIR_SELECTION nAirSelect, int nActType)
{
	m_stTM02_Mess.Reset();
	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;
	m_stTM02_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TM02_PP_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_PP_VAC].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_PP_VAC].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nActType, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nActType, nBiz_Unit_Zero
				);

			
		}
		else
		{
			strLogMessage.Format("TM02_PP_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_PP_VAC : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_PP_VAC].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CStickTransper::OnBnClickedBtnPaperPurgeOn()
{
	int nPurgeTime = GetDlgItemInt(IDC_EDIT_PAPER_PURGE_SEC);

	m_fnCheckPaperAirStep();

	m_fnPaperPurgeOn(m_stAirStep, nPurgeTime);	
}

void CStickTransper::m_fnPaperPurgeOn(AIR_SELECTION nAirSelect, int nPurgeTime)
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;
	m_stTM02_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stTM02_Mess.ActionTime = nPurgeTime;
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TM02_PP_PURGE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TM02_PP_PURGE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_PP_PURGE].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_PP_PURGE].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TM02_PP_PURGE].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_TM02_PP_Purge, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_TM02_PP_Purge, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM02_PP_PURGE : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TM02_PP_PURGE : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_PP_PURGE : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_PP_PURGE].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TM02_PP_PURGE].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CStickTransper::OnBnClickedBtnReadQr()
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;		
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[TM02_QR].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_QR].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_QR].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_QR].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_QR].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_ACC, nBiz_Seq_Read_QR, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_ACC, nBiz_Seq_Read_QR, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM02_QR : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM02_QR : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_QR : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_QR].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_QR].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CStickTransper::OnBnClickedBtnLight()
{
	m_fnAlignLightSet( GetDlgItemInt(IDC_STATIC_LIGHT1_1));	
}

void CStickTransper::m_fnAlignLightSet(int nLightValue)
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_None;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;
	m_stTM02_Mess.newPosition1 = nLightValue;	
	m_stTM02_Mess.SelectPoint = 0x01;
	m_stTM02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[TM02_LIGHT].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[TM02_LIGHT].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_LIGHT].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_LIGHT].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[TM02_LIGHT].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_ACC, nBiz_Seq_Light_Align, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_ACC, nBiz_Seq_Light_Align, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM02_LIGHT : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TM02_LIGHT : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_LIGHT : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LIGHT].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[TM02_LIGHT].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CStickTransper::m_fnCheckStickAirStep()
{
	m_stAirStep.nAirSelection = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC1))
		m_stAirStep.nAirStep.m_btStep1 = 1;
	else
		m_stAirStep.nAirStep.m_btStep1 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC2))
		m_stAirStep.nAirStep.m_btStep2 = 1;
	else
		m_stAirStep.nAirStep.m_btStep2 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC3))
		m_stAirStep.nAirStep.m_btStep3 = 1;
	else
		m_stAirStep.nAirStep.m_btStep3 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC4))
		m_stAirStep.nAirStep.m_btStep4 = 1;
	else
		m_stAirStep.nAirStep.m_btStep4 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC5))
		m_stAirStep.nAirStep.m_btStep5 = 1;
	else
		m_stAirStep.nAirStep.m_btStep5 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC6))
		m_stAirStep.nAirStep.m_btStep6 = 1;
	else
		m_stAirStep.nAirStep.m_btStep6 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC7))
		m_stAirStep.nAirStep.m_btStep7 = 1;
	else
		m_stAirStep.nAirStep.m_btStep7 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC8))
		m_stAirStep.nAirStep.m_btStep8 = 1;
	else
		m_stAirStep.nAirStep.m_btStep8 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC9))
		m_stAirStep.nAirStep.m_btStep9 = 1;
	else
		m_stAirStep.nAirStep.m_btStep9 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC10))
		m_stAirStep.nAirStep.m_btStep10 = 1;
	else
		m_stAirStep.nAirStep.m_btStep10 = 0;
}

void CStickTransper::m_fnCheckPaperAirStep()
{
	m_stAirStep.nAirSelection = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC1))
		m_stAirStep.nAirStep.m_btStep1 = 1;
	else
		m_stAirStep.nAirStep.m_btStep1 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC2))
		m_stAirStep.nAirStep.m_btStep2 = 1;
	else
		m_stAirStep.nAirStep.m_btStep2 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC3))
		m_stAirStep.nAirStep.m_btStep3 = 1;
	else
		m_stAirStep.nAirStep.m_btStep3 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC4))
		m_stAirStep.nAirStep.m_btStep4 = 1;
	else
		m_stAirStep.nAirStep.m_btStep4 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC5))
		m_stAirStep.nAirStep.m_btStep5 = 1;
	else
		m_stAirStep.nAirStep.m_btStep5 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC6))
		m_stAirStep.nAirStep.m_btStep6 = 1;
	else
		m_stAirStep.nAirStep.m_btStep6 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC7))
		m_stAirStep.nAirStep.m_btStep7 = 1;
	else
		m_stAirStep.nAirStep.m_btStep7 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC8))
		m_stAirStep.nAirStep.m_btStep8 = 1;
	else
		m_stAirStep.nAirStep.m_btStep8 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC9))
		m_stAirStep.nAirStep.m_btStep9 = 1;
	else
		m_stAirStep.nAirStep.m_btStep9 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_PAPER_VAC10))
		m_stAirStep.nAirStep.m_btStep10 = 1;
	else
		m_stAirStep.nAirStep.m_btStep10 = 0;
}

void CStickTransper::m_fnTM02DriveMove(int nPosValue)
{
	CString strLogMessage;

	if(m_fnTM02InterlockCheck(AXIS_TM02_PickerDrive, nPosValue))
		return;

	if(G_MOTION_STATUS[TM02_DRIVE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_DRIVE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_DRIVE].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_DRIVE].UN_CMD_COUNT++;

			m_fnTM02PositionMove(nPosValue, nBiz_Seq_TM02_PickerDrive, TIME_OUT_120_SECOND);

			strLogMessage.Format("TM02_DRIVE : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM02_DRIVE : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_DRIVE : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_DRIVE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_DRIVE].UN_CMD_COUNT = 0;
	}	
}

void CStickTransper::m_fnTM02UpDnMove(int nPosValue, int nSpeedMode)
{
	CString strLogMessage;

	if(m_fnTM02InterlockCheck(AXIS_TM02_PickerUpDown, nPosValue))
		return;

	if(G_MOTION_STATUS[TM02_UPDN].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_UPDN].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_UPDN].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_UPDN].UN_CMD_COUNT++;

			m_fnTM02PositionMove(nPosValue, nBiz_Seq_TM02_PickerUpDown, TIME_OUT_120_SECOND, nSpeedMode);

			strLogMessage.Format("TM02_UPDN : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TM02_UPDN : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_UPDN : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_UPDN].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_UPDN].UN_CMD_COUNT = 0;
	}
}

void CStickTransper::m_fnTM02ThetaMove(int nPosValue)
{
	CString strLogMessage;

	if(G_MOTION_STATUS[TM02_THETA].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_THETA].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_THETA].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_THETA].UN_CMD_COUNT++;

			m_fnTM02PositionMove(nPosValue, nBiz_Seq_TM02_PickerRotate, TIME_OUT_20_SECOND);

			strLogMessage.Format("TM02_THETA : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM02_THETA : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_THETA : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_THETA].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_THETA].UN_CMD_COUNT = 0;
	}
}

void CStickTransper::m_fnTM02ShiftMove(int nPosValue)
{
	CString strLogMessage;

	if(G_MOTION_STATUS[TM02_SHIFT].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM02_SHIFT].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM02_SHIFT].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM02_SHIFT].UN_CMD_COUNT++;

			m_fnTM02PositionMove(nPosValue, nBiz_Seq_TM02_PickerShift, TIME_OUT_20_SECOND);

			strLogMessage.Format("TM02_SHIFT : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM02_SHIFT : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM02_SHIFT : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_SHIFT].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM02_SHIFT].UN_CMD_COUNT = 0;
	}
}

void CStickTransper::m_fnTM02PositionMove(int nPosValue, int nMotionType, int nTimeOut, int nSpeedMode)
{	
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_Move;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;
	m_stTM02_Mess.newPosition1 = nPosValue;
	m_stTM02_Mess.TimeOut = nTimeOut;
	m_stTM02_Mess.SpeedMode = nSpeedMode;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize,	chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete []chMsgBuf;	
}

bool CStickTransper::m_fnTM02InterlockCheck(int AxisNum, long newPosition)
{
	CString strLogMessage;

	CHECK_AXIS_TM02(AxisNum);

	switch(AxisNum)
	{
		//TM02
	case AXIS_TM02_PickerDrive://좌우 Gantry구동
		{
			int nPicker_DriveMin = GetPrivateProfileInt(Section_TM02, Key_Drive_Min, 0, Interlock_PARAM_INI_PATH);
			int nPicker_DriveMax = GetPrivateProfileInt(Section_TM02, Key_Drive_Max, 0, Interlock_PARAM_INI_PATH);				

			if(!(nPicker_DriveMin == 0 && nPicker_DriveMax == 0))		
			{
				if(newPosition < nPicker_DriveMin || newPosition > nPicker_DriveMax)
				{
					strLogMessage.Format("AXIS_TM02_PickerDrive : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
						nPicker_DriveMin, newPosition, nPicker_DriveMax );
					m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
					return true;
				}
			}

			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] - newPosition) > 100000)
			{				
				if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
					m_st_TM02_Pos->nStickZ_Ready_Pos) > POSITION_OFFSET)
				{
					strLogMessage.Format("AXIS_TM02_PickerDrive : 100mm 초과 이동시 Z축은 대기위치(%d)여야 합니다.", m_st_TM02_Pos->nStickZ_Ready_Pos);
					m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
					return true;
				}
			}			
			break;
		}
	case AXIS_TM02_PickerUpDown:
		{
			int nZ_Low_Limit = GetPrivateProfileInt(Section_TM02, Key_UpDn_Min, 0, Interlock_PARAM_INI_PATH);

			int nTT01_DriveMin = GetPrivateProfileInt(Section_TM02, Key_Drive_TT01_Pos_Low, 0, Interlock_PARAM_INI_PATH);
			int nTT01_DriveMax = GetPrivateProfileInt(Section_TM02, Key_Drive_TT01_Pos_High, 0, Interlock_PARAM_INI_PATH);
			int nTT01_UnDnMax = GetPrivateProfileInt(Section_TM02, Key_UpDn_TT01_Max, 0, Interlock_PARAM_INI_PATH);
			int nTT01SafePosMin = GetPrivateProfileInt(Section_TM02, Key_Turn_Table_Safe_Pos, 0, Interlock_PARAM_INI_PATH);
			int nUT02_UnDnMax = GetPrivateProfileInt(Section_TM02, Key_UpDn_UT02_Max, 0, Interlock_PARAM_INI_PATH);
			
			int nLT01_DriveMin = GetPrivateProfileInt(Section_TM02, Key_Drive_LT01_Pos_Low, 0, Interlock_PARAM_INI_PATH);
			int nLT01_DriveMax = GetPrivateProfileInt(Section_TM02, Key_Drive_LT01_Pos_High, 0, Interlock_PARAM_INI_PATH);
			int nLT01_UpDnMax = GetPrivateProfileInt(Section_TM02, Key_UpDn_LT01_Max, 0, Interlock_PARAM_INI_PATH);

			int nBT01_DriveMin = GetPrivateProfileInt(Section_TM02, Key_Drive_BT01_Pos_Low, 0, Interlock_PARAM_INI_PATH);
			int nBT01_DriveMax = GetPrivateProfileInt(Section_TM02, Key_Drive_BT01_Pos_High, 0, Interlock_PARAM_INI_PATH);
			int nBT01_UpDnMax = GetPrivateProfileInt(Section_TM02, Key_UpDn_BT01_Max, 0, Interlock_PARAM_INI_PATH);


			if (!(G_STMACHINE_STATUS.stModuleStatus.In12_Sn_LoadLeftAlignBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In14_Sn_LoadRightAlignBackward == ON))
			{
				m_fnLogDisplay("AXIS_TM02_PickerUpDown : 로딩 테이블 가이드 실린더를 후진 시켜야 합니다.", TEXT_WARRING_YELLOW);
				return true;
			}
			if (!(G_STMACHINE_STATUS.stModuleStatus.In18_Sn_UnloadBoxLeftAlignBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In20_Sn_UnloadBoxRightAlignBackward == ON))
			{
				m_fnLogDisplay("AXIS_TM02_PickerUpDown : 언로딩 테이블 가이드 실린더를 후진 시켜야 합니다.", TEXT_WARRING_YELLOW);
				return true;
			}

			//주행축이 LT01, UT01 구간 일때.
			if(!(nLT01_DriveMin == 0 && nLT01_DriveMax == 0))
			{
				if(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] >= nLT01_DriveMin && 
					G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] <= nLT01_DriveMax)
				{
					if(newPosition > nLT01_UpDnMax)
					{
						strLogMessage.Format("AXIS_TM02_PickerUpDown : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
							nZ_Low_Limit, newPosition, nLT01_UpDnMax );
						m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
						return true;
					}
				}
			}

			//주행축이 BT01 구간 일때.
			if(!(nBT01_DriveMin == 0 && nBT01_DriveMax == 0))
			{
				if(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] >= nBT01_DriveMin && 
					G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] <= nBT01_DriveMax)
				{
					if(newPosition > nBT01_DriveMax)
					{
						strLogMessage.Format("AXIS_TM02_PickerUpDown : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
							nZ_Low_Limit, newPosition, nBT01_DriveMax );
						m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
						return true;
					}
				}
			}

			//주행축이 TT01, UT02 구간 일때.
			if(!(nTT01_DriveMin == 0 && nTT01_DriveMax == 0))
			{
				if(/*G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] >= nTT01_DriveMin && */
					G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] <= nTT01_DriveMax)
				{
					//턴테이블이 회전위치 보다 멀리있다면..
					if(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] >= nTT01SafePosMin)
					{
						//업다운 축은 배출 테이블 까지 갈수 있다.
						if(newPosition > nUT02_UnDnMax)
						{
							strLogMessage.Format("AXIS_TM02_PickerUpDown : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
								nZ_Low_Limit, newPosition, nUT02_UnDnMax );
							m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
							return true;
						}
					}
					else //턴테이블이 정위치에 가까이 있다면..
					{
						//업다운 축은 턴 테이블 까지 갈수 있다.
						if(newPosition > nTT01_UnDnMax)
						{
							strLogMessage.Format("AXIS_TM02_PickerUpDown : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
								nZ_Low_Limit, newPosition, nTT01_UnDnMax );
							m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
							return true;
						}
					}
				}
			}
			break;
		}
	case AXIS_TM02_PickerShift:
		{
			break;
		}
	case AXIS_TM02_PickerRotate:
		{
			break;
		}
	default: 
		return true;
	}
	return false;
}

void CStickTransper::m_fnTM02ActionStop(int nMotionType, int nTimeOut)
{
	m_stTM02_Mess.Reset();

	m_stTM02_Mess.nAction = m_stTM02_Mess.Act_Stop;
	m_stTM02_Mess.nResult = m_stTM02_Mess.Ret_None;	
	m_stTM02_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTM02_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize, chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete[]chMsgBuf;
}

void CStickTransper::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{	
	CVS11IndexerDlg*  vpMainDlg = (CVS11IndexerDlg*)AfxGetApp()->m_pMainWnd;
	vpMainDlg->m_fnAppendList(strLog, nColorType);
}

int CStickTransper::m_fn_TM02_ReadQR_Return(VS24MotData stReturn_Mess)
{	
	CString strLogMessage;
	
	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		//strLogMessage = "TM02_QR : 포지션 이동 수행 완료.";
		strLogMessage.Format("TM02_QR : %s", m_chStickQRcode);
		SetDlgItemText(IDC_EDIT_QRCODE, m_chStickQRcode);
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_QR].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_QR].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_Error:
		strLogMessage = "TM02_QR : QR 리더 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_QR].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_TimeOut:
		strLogMessage.Format("TM02_QR : 리딩 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_QR].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		OnBnClickedBtnReadQr();
		break;
	case stReturn_Mess.Ret_NowRun:		
		break;
	default:	
		strLogMessage.Format("TM02_QR : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_QR].UN_CMD_COUNT = 0;
		break;
	}

	G_MOTION_STATUS[TM02_QR].B_MOTION_STATUS = IDLE;

	return 0;
}

int CStickTransper::m_fn_TM02_Drive_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_DRIVE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_DRIVE : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_DRIVE].UN_CMD_COUNT = 0;
		G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS = COMPLETE;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_DRIVE : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_DRIVE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_DRIVE : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_DRIVE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnTM02DriveMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("TM02_DRIVE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_DRIVE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_UpDn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_UPDN].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	

	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_UPDN : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_UPDN].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_UPDN : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_UPDN].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_UPDN : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_UPDN].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnTM02UpDnMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("TM02_UPDN : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_UPDN].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_Theta_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_THETA].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	

	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_THETA : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_THETA].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_THETA : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_THETA].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_THETA : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_THETA].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnTM02ThetaMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("TM02_THETA : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_THETA].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_Shift_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_SHIFT].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,	

	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_SHIFT : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_SHIFT].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_SHIFT : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_SHIFT].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_SHIFT : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_SHIFT].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnTM02ShiftMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("TM02_SHIFT : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_SHIFT].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_LR_PickerUp_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_LR_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_LR_CYL : 좌우 간지 실린더 업 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_LR_CYL : 좌우 간지 실린더 업 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_LR_CYL : 좌우 간지 실린더 업 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPaperCylUp();
	}break;
	default:
	{
		strLogMessage.Format("TM02_LR_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_LR_PickerDn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_LR_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_LR_CYL : 좌우 간지 실린더 다운 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_LR_CYL : 좌우 간지 실린더 다운 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_LR_CYL : 좌우 간지 실린더 다운 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPaperCylDn();
	}break;
	default:
	{
		strLogMessage.Format("TM02_LR_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LR_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_CT_PickerUp_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_MID_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_MID_CYL : 중앙 간지 실린더 업 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

		G_MOTION_STATUS[TM02_MID_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_MID_CYL : 중앙 간지 실린더 업 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_MID_CYL : 중앙 간지 실린더 업 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPaperCylUp2();
	}break;
	default:
	{
		strLogMessage.Format("TM02_MID_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_CT_PickerDn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_MID_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_MID_CYL : 좌우 간지 실린더 다운 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_MID_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_MID_CYL : 좌우 간지 실린더 다운 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_MID_CYL : 좌우 간지 실린더 다운 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPaperCylDn2();
	}break;
	default:
	{
		strLogMessage.Format("TM02_MID_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_MID_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_ST_VacOn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_ST_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_ST_VAC : Vac. ON 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_ST_VAC : Vac. ON 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_ST_VAC : Vac. ON 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnStickVacOn();
	}break;
	default:
	{
		strLogMessage.Format("TM02_ST_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_ST_VacOff_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_ST_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_ST_VAC : Vac. OFF 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_ST_VAC : Vac. OFF 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_ST_VAC : Vac. OFF 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnStickVacOff();
	}break;
	default:
	{
		strLogMessage.Format("TM02_ST_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_ST_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_ST_Purge_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_ST_PURGE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_ST_PURGE : Purge 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_ST_PURGE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_ST_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_ST_PURGE : Purge 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_ST_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_ST_PURGE : Purge 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_ST_PURGE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnStickPurgeOn();
	}break;
	default:
	{
		strLogMessage.Format("TM02_ST_PURGE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_ST_PURGE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_PP_VacOn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_PP_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_PP_VAC : Vac. ON 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_PP_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_PP_VAC : Vac. ON 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_PP_VAC : Vac. ON 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPaperVacOn();
	}break;
	default:
	{
		strLogMessage.Format("TM02_PP_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_PP_VacOff_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_PP_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_PP_VAC : Vac. OFF 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_PP_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_PP_VAC : Vac. OFF 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_PP_VAC : Vac. OFF 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPaperVacOff();
	}break;
	default:
	{
		strLogMessage.Format("TM02_PP_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_PP_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_PP_Purge_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_PP_PURGE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM02_PP_PURGE : Purge 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

		G_MOTION_STATUS[TM02_PP_PURGE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_PP_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM02_PP_PURGE : Purge 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_PP_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM02_PP_PURGE : Purge 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_PP_PURGE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPaperPurgeOn();
	}break;
	default:
	{
		strLogMessage.Format("TM02_PP_PURGE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_PP_PURGE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CStickTransper::m_fn_TM02_LightSet_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM02_LIGHT].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		strLogMessage = "TM02_LIGHT : 조명 값 변경 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM02_LIGHT].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM02_LIGHT].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_Error:	
		strLogMessage = "TM02_LIGHT : 조명 값 변경 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LIGHT].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_TimeOut:	
		strLogMessage.Format("TM02_LIGHT : 조명 값 변경 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM02_LIGHT].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnLight();
		break;
	case stReturn_Mess.Ret_NowRun:			
		break;
	default:	
		strLogMessage.Format("TM02_LIGHT : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM02_LIGHT].UN_CMD_COUNT = 0;
		break;
	}
	return 0;
}

void CStickTransper::OnBnClickedBtnMotionStop()
{
	m_fnTM02ActionStop(nBiz_Seq_TM02_PickerDrive, TIME_OUT_10_SECOND);
	m_fnTM02ActionStop(nBiz_Seq_TM02_PickerUpDown, TIME_OUT_10_SECOND);
	m_fnTM02ActionStop(nBiz_Seq_TM02_PickerRotate, TIME_OUT_10_SECOND);
	m_fnTM02ActionStop(nBiz_Seq_TM02_PickerShift, TIME_OUT_10_SECOND);
}

void CStickTransper::m_fnDisplaySignal()
{
	if (G_STMACHINE_STATUS.stPickerCylinder.In00_Sn_PickerPaperLeftUp == ON &&
		G_STMACHINE_STATUS.stPickerCylinder.In02_Sn_PickerPaperRightUp == ON)
	{
		m_ctrBtnCylUpLR.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnCylDnLR.SetRoundButtonStyle(&m_tStyleOff);
	}
	
	if (G_STMACHINE_STATUS.stPickerCylinder.In01_Sn_PickerPaperLeftDown == ON &&
		G_STMACHINE_STATUS.stPickerCylinder.In03_Sn_PickerPaperRightDown == ON)
	{
		m_ctrBtnCylUpLR.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnCylDnLR.SetRoundButtonStyle(&m_tStyleOn);
	}
	
	if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerFrontStickVacuum == ON ||
		G_STMACHINE_STATUS.stPickerSensor.In10_Sn_PickerBackStickVacuum == ON)
	{
		m_ctrBtnVacOnS.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnVacOffS.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnVacOnS.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnVacOffS.SetRoundButtonStyle(&m_tStyleOn);
	}


	if (G_STMACHINE_STATUS.stPickerSensor.In09_Sn_PickerFrontPaperVacuum == ON ||
		G_STMACHINE_STATUS.stPickerSensor.In11_Sn_PickerBackPaperVacuum == ON)
	{
		m_ctrBtnVacOnP.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnVacOffP.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnVacOnP.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnVacOffP.SetRoundButtonStyle(&m_tStyleOn);
	}

}

void CStickTransper::OnBnClickedBtnSetQr()
{
	CString strQRCode;
	GetDlgItemText(IDC_EDIT_QRCODE, strQRCode);
	memset(m_chStickQRcode, 0x00, MAX_QRCODE);

	memcpy(m_chStickQRcode, (LPSTR)(LPCTSTR)strQRCode, strQRCode.GetLength());	
}

void CStickTransper::OnBnClickedBtnManualAlign()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//g_cpMainDlg->m_cCameraControl.m_fnStickAlign();
	if(AfxMessageBox("자동 Align을 수행하시겠습니까?",MB_YESNO)==IDYES)
	{
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;
		g_cpMainDlg->m_elPickerSequence = STICK_MOVE_F_LT01_T_TM02;
		g_cpMainDlg->m_cPickerControl->m_fnSet_StickMove_LT01_TM02_Step( STLT_TM02_STICK_ALIGN);		
		return;
	}

	if(AfxMessageBox("수동 Align 위치 조정이 완료 되었습니까?",MB_YESNO)==IDNO) 
		return;

	G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;
	g_cpMainDlg->m_elPickerSequence = STICK_MOVE_F_LT01_T_TM02;
	g_cpMainDlg->m_cPickerControl->m_fnSet_StickMove_LT01_TM02_Step( STLT_TM02_STICK_ALIGN_RETURN);
}


void CStickTransper::OnBnClickedBtnThresholdSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	g_cpMainDlg->m_cCameraControl.m_nAlignThreshold=GetDlgItemInt(IDC_EDIT_THRESHOLD_VAL);
}
