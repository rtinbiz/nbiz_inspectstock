// RecipeDefine.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "RecipeDefine.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CRecipeDefine 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRecipeDefine, CDialogEx)

CRecipeDefine::CRecipeDefine(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRecipeDefine::IDD, pParent)
	, m_nCurSelIndex(0)
{

}

CRecipeDefine::~CRecipeDefine()
{
}

void CRecipeDefine::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_NEW, m_ctrBtnListAdd);
	DDX_Control(pDX, IDC_BTN_REPLACE, m_ctrBtnListReplace);
	DDX_Control(pDX, IDC_BTN_SAVE, m_ctrBtnListSave);
	DDX_Control(pDX, IDC_BTN_CALC, m_ctrBtnListCalc);
	DDX_Control(pDX, IDC_LIST_RECIPE_LIST, m_ctrListBoxRecipe);
	DDX_Control(pDX, IDC_CBO_BOX_SIZE, m_ctrCboBoxType);
	DDX_Control(pDX, IDC_BTN_CANCEL, m_ctrBtnCancel);
	DDX_Control(pDX, IDC_CBO_STICK_SIZE, m_ctrCboStickSize);
}


BEGIN_MESSAGE_MAP(CRecipeDefine, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_LBN_SELCHANGE(IDC_LIST_RECIPE_LIST, &CRecipeDefine::OnLbnSelchangeListRecipeList)
	ON_BN_CLICKED(IDC_BTN_NEW, &CRecipeDefine::OnBnClickedBtnNew)
	ON_BN_CLICKED(IDC_BTN_REPLACE, &CRecipeDefine::OnBnClickedBtnReplace)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CRecipeDefine::OnBnClickedBtnSave)
	ON_EN_CHANGE(IDC_EDIT_CUR_ID, &CRecipeDefine::OnEnChangeEditCurId)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CRecipeDefine::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_CALC, &CRecipeDefine::OnBnClickedBtnCalc)
END_MESSAGE_MAP()

// CRecipeDefine 메시지 처리기입니다.

void CRecipeDefine::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_EditFont.DeleteObject();
	m_EditFont2.DeleteObject();
	CDialogEx::PostNcDestroy();
}


BOOL CRecipeDefine::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65, 65, 65);
	m_EditFont.CreatePointFont(200, "Arial");
	m_EditFont2.CreatePointFont(120, "Arial");

	SetDlgItemText(IDC_CBO_BOX_SIZE, "Box Type Select");

	m_ctrCboStickSize.AddString("28000");
	m_ctrCboStickSize.AddString("69000");
	m_ctrCboStickSize.AddString("100000");
	m_ctrCboStickSize.AddString("128000");
	m_ctrCboStickSize.AddString("169000");
	m_ctrCboStickSize.AddString("200000");
	m_ctrCboStickSize.AddString("240000");
	m_ctrCboStickSize.AddString("312000");
	m_ctrCboStickSize.AddString("368000");
	m_ctrCboStickSize.AddString("409000");
	m_ctrCboStickSize.AddString("440000");	

	m_fnCreateControlFont();

	m_fnCreateButton();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
   

BOOL CRecipeDefine::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}


HBRUSH CRecipeDefine::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number = pWnd->GetDlgCtrlID();
	
	switch (nCtlColor)
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT:
		pDC->SetTextColor(RGB(255, 255, 0));
		pDC->SetBkColor(m_btBackColor);
		return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		break;
	case CTLCOLOR_LISTBOX://List Color
		pDC->SetTextColor(RGB(255, 100, 100));
		pDC->SetBkColor(m_btBackColor);
		return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		break;
	}

	pDC->SetTextColor(RGB(255, 255, 255));
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CRecipeDefine::m_fnCreateButton(void)
{
	tButtonStyle tStyle;

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled = m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled = RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled = RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked = RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked = RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed = RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed = RGB(0x20, 0x20, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	m_ctrBtnListAdd.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled = RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked = RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed = RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnListAdd.GetFont(&tFont);
	strncpy_s(tFont.lfFaceName, "굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	m_ctrBtnListAdd.SetTextColor(&tColor); m_ctrBtnListAdd.SetCheckButton(false);
	m_ctrBtnListAdd.SetFont(&tFont); m_ctrBtnListAdd.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnListReplace.SetTextColor(&tColor); m_ctrBtnListReplace.SetCheckButton(false);
	m_ctrBtnListReplace.SetFont(&tFont); m_ctrBtnListReplace.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnListSave.SetTextColor(&tColor); m_ctrBtnListSave.SetCheckButton(false);
	m_ctrBtnListSave.SetFont(&tFont); m_ctrBtnListSave.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnListCalc.SetTextColor(&tColor); m_ctrBtnListCalc.SetCheckButton(false);
	m_ctrBtnListCalc.SetFont(&tFont); m_ctrBtnListCalc.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnCancel.SetTextColor(&tColor); m_ctrBtnCancel.SetCheckButton(false);
	m_ctrBtnCancel.SetFont(&tFont); m_ctrBtnCancel.SetRoundButtonStyle(&m_tStyle1);

	
}


int CRecipeDefine::m_fnCreateControlFont(void)
{
	GetDlgItem(IDC_EDIT_CUR_ID)->SetFont(&m_EditFont);
	GetDlgItem(IDC_CBO_BOX_SIZE)->SetFont(&m_EditFont2);
	GetDlgItem(IDC_CBO_STICK_SIZE)->SetFont(&m_EditFont2);
	GetDlgItem(IDC_LIST_RECIPE_LIST)->SetFont(&m_EditFont2);

	return 0;
}

int CRecipeDefine::m_fnSetParameter(STM02_RECIPE* stPointer)
{
	m_st_TM02_Recipe = stPointer;

	return 0;
}

void CRecipeDefine::OnLbnSelchangeListRecipeList()
{	
	CString strRecipeName;
	CString strFullPath;

	m_nCurSelIndex = m_ctrListBoxRecipe.GetCurSel();
	m_ctrListBoxRecipe.GetText(m_nCurSelIndex, strRecipeName);

	SetDlgItemText(IDC_EDIT_CUR_ID, strRecipeName);

	strFullPath = _T(TM02_RECIPE_INI_PATH) + strRecipeName + ".ini";

	m_st_Cur_Recipe.nBoxType = GetPrivateProfileInt(strRecipeName, KEY_TM02_BOX_TYPE, 0, strFullPath);
	m_st_Cur_Recipe.nStickSize = GetPrivateProfileInt(strRecipeName, KEY_TM02_STICK_SIZE, 0, strFullPath);
	m_st_Cur_Recipe.nAM01LoadOffset = GetPrivateProfileInt(strRecipeName, KEY_AM01_LOAD_OFFSET, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_Drive_Align = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_DRIVE_ALIGN, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_Z_Align = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_Z_ALIGN, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_Drive_PP_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_DRIVE_PP_GET, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_Drive_ST_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_DRIVE_ST_GET, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_Shift_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_SHIFT_GET, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_Tilt_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_TILT_GET, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_Z_PP_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_Z_PP_GET, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_Z_ST_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_Z_ST_GET, 0, strFullPath);
	m_st_Cur_Recipe.nLT01_PP_Purge_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_PP_PURGE_GET, 0, strFullPath);
	m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_VAC_PP_GET, 0, strFullPath);
	m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_VAC_ST_GET, 0, strFullPath);

	m_st_Cur_Recipe.nZ_Ready_Pos = GetPrivateProfileInt(strRecipeName, KEY_TM02_Z_READY_POS, 0, strFullPath);
	
	m_st_Cur_Recipe.nQR_Drive_Pos = GetPrivateProfileInt(strRecipeName, KEY_TM02_QR_DRIVE_POS, 0, strFullPath);
	m_st_Cur_Recipe.nQR_Z_Pos = GetPrivateProfileInt(strRecipeName, KEY_TM02_QR_Z_POS, 0, strFullPath);

	m_st_Cur_Recipe.nTT01_Dirve_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_DRIVE_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nTT01_Shift_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_SHIFT_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nTT01_Tilt_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_TILT_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nTT01_Z_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_Z_ST_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nTT01_ST_Purge_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_ST_PURGE_PUT, 0, strFullPath);

	m_st_Cur_Recipe.nUT02_Drive_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_DRIVE_GET, 0, strFullPath);
	m_st_Cur_Recipe.nUT02_Shift_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_SHIFT_GET, 0, strFullPath);
	m_st_Cur_Recipe.nUT02_Tilt_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_TILT_GET, 0, strFullPath);
	m_st_Cur_Recipe.nUT02_Z_ST_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_Z_ST_GET, 0, strFullPath);
	m_st_Cur_Recipe.stUT02_Vac_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_VAC_GET, 0, strFullPath);

	m_st_Cur_Recipe.nUT01_Drive_PP_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_DRIVE_PP_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nUT01_Drive_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_DRIVE_ST_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nUT01_Shift_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_SHIFT_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nUT01_Tilt_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_TILT_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nUT01_Z_PP_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_Z_PP_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nUT01_Z_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_Z_ST_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nUT01_Purge_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_PURGE_PUT, 0, strFullPath);

	m_st_Cur_Recipe.nBT01_Drive_PP_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_DRIVE_PP_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nBT01_Drive_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_DRIVE_ST_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nBT01_Shift_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_SHIFT_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nBT01_Tilt_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_TILT_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nBT01_Z_PP_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_Z_PP_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nBT01_Z_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_Z_ST_PUT, 0, strFullPath);
	m_st_Cur_Recipe.nBT01_Purge_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_PURGE_PUT, 0, strFullPath);

	m_st_Cur_Recipe.stTT01_Vac_Table_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_TT01_VAC_GET, 0, strFullPath);
	m_st_Cur_Recipe.stUT02_Vac_Table_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_UT02_VAC_GET, 0, strFullPath);
	
	m_fnDisplayCurRecipe();
}


int CRecipeDefine::m_fnDisplayCurRecipe()
{
	switch (m_st_Cur_Recipe.nBoxType)
	{
	case 1:
		//SetDlgItemText(IDC_CBO_BOX_SIZE, "SMALL");
		m_ctrCboBoxType.SetCurSel(0);
		break;
	case 2:
		SetDlgItemText(IDC_CBO_BOX_SIZE, "MEDIUM");	
		m_ctrCboBoxType.SetCurSel(1);
		break;
	case 3:
		SetDlgItemText(IDC_CBO_BOX_SIZE, "LARGE");		
		m_ctrCboBoxType.SetCurSel(2);
		break;
	default:
		break;
	}

	SetDlgItemInt(IDC_CBO_STICK_SIZE, m_st_Cur_Recipe.nStickSize);
	SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, m_st_Cur_Recipe.nAM01LoadOffset);
	SetDlgItemInt(IDC_LT01_EDIT1, m_st_Cur_Recipe.nLT01_Drive_Align);
	SetDlgItemInt(IDC_LT01_EDIT2, m_st_Cur_Recipe.nLT01_Z_Align);
	SetDlgItemInt(IDC_LT01_EDIT3, m_st_Cur_Recipe.nLT01_Drive_PP_Get);
	SetDlgItemInt(IDC_LT01_EDIT4, m_st_Cur_Recipe.nLT01_Drive_ST_Get);
	SetDlgItemInt(IDC_LT01_EDIT5, m_st_Cur_Recipe.nLT01_Shift_Get);
	SetDlgItemInt(IDC_LT01_EDIT6, m_st_Cur_Recipe.nLT01_Tilt_Get);
	SetDlgItemInt(IDC_LT01_EDIT7, m_st_Cur_Recipe.nLT01_Z_PP_Get);
	SetDlgItemInt(IDC_LT01_EDIT8, m_st_Cur_Recipe.nLT01_Z_ST_Get);
	SetDlgItemInt(IDC_LT01_EDIT9, m_st_Cur_Recipe.nLT01_PP_Purge_Get);


	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep1 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(TRUE);	
	else	
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
	
	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep2 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep3 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep4 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep5 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep6 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep7 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep8 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep9 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep10 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);


	/////////////////
	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep1 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep2 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep3 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep4 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep5 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep6 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep7 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep8 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep9 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep10 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

	SetDlgItemInt(IDC_Z_READY, m_st_Cur_Recipe.nZ_Ready_Pos);

	SetDlgItemInt(IDC_QR_EDIT1, m_st_Cur_Recipe.nQR_Drive_Pos);
	SetDlgItemInt(IDC_QR_EDIT2, m_st_Cur_Recipe.nQR_Z_Pos);

	SetDlgItemInt(IDC_TT01_EDIT1, m_st_Cur_Recipe.nTT01_Dirve_Put);
	SetDlgItemInt(IDC_TT01_EDIT2, m_st_Cur_Recipe.nTT01_Shift_Put);
	SetDlgItemInt(IDC_TT01_EDIT3, m_st_Cur_Recipe.nTT01_Tilt_Put);
	SetDlgItemInt(IDC_TT01_EDIT4, m_st_Cur_Recipe.nTT01_Z_ST_Put);
	SetDlgItemInt(IDC_TT01_EDIT5, m_st_Cur_Recipe.nTT01_ST_Purge_Put);

	SetDlgItemInt(IDC_UT02_EDIT1, m_st_Cur_Recipe.nUT02_Drive_Get);
	SetDlgItemInt(IDC_UT02_EDIT2, m_st_Cur_Recipe.nUT02_Shift_Get);
	SetDlgItemInt(IDC_UT02_EDIT3, m_st_Cur_Recipe.nUT02_Tilt_Get);
	SetDlgItemInt(IDC_UT02_EDIT4, m_st_Cur_Recipe.nUT02_Z_ST_Get);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep1 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep2 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep3 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep4 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep5 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep6 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep7 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep8 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep9 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(FALSE);

	if (m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep10 == ON)
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(TRUE);
	else
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);

	SetDlgItemInt(IDC_UT01_EDIT1, m_st_Cur_Recipe.nUT01_Drive_PP_Put);
	SetDlgItemInt(IDC_UT01_EDIT7, m_st_Cur_Recipe.nUT01_Drive_ST_Put);
	SetDlgItemInt(IDC_UT01_EDIT2, m_st_Cur_Recipe.nUT01_Shift_Put);
	SetDlgItemInt(IDC_UT01_EDIT3, m_st_Cur_Recipe.nUT01_Tilt_Put);
	SetDlgItemInt(IDC_UT01_EDIT4, m_st_Cur_Recipe.nUT01_Z_PP_Put);
	SetDlgItemInt(IDC_UT01_EDIT5, m_st_Cur_Recipe.nUT01_Z_ST_Put);
	SetDlgItemInt(IDC_UT01_EDIT6, m_st_Cur_Recipe.nUT01_Purge_Put);

	SetDlgItemInt(IDC_BT01_EDIT1, m_st_Cur_Recipe.nBT01_Drive_PP_Put);
	SetDlgItemInt(IDC_BT01_EDIT7, m_st_Cur_Recipe.nBT01_Drive_ST_Put);
	SetDlgItemInt(IDC_BT01_EDIT2, m_st_Cur_Recipe.nBT01_Shift_Put);
	SetDlgItemInt(IDC_BT01_EDIT3, m_st_Cur_Recipe.nBT01_Tilt_Put);
	SetDlgItemInt(IDC_BT01_EDIT4, m_st_Cur_Recipe.nBT01_Z_PP_Put);
	SetDlgItemInt(IDC_BT01_EDIT5, m_st_Cur_Recipe.nBT01_Z_ST_Put);
	SetDlgItemInt(IDC_BT01_EDIT6, m_st_Cur_Recipe.nBT01_Purge_Put);
	

	return 0;
}

void CRecipeDefine::OnBnClickedBtnNew()
{
	CString strRecipeName;	
	int nCurSelIndex = m_ctrListBoxRecipe.GetCurSel();
	if (nCurSelIndex == -1)
	{
		AfxMessageBox("복제할 항목을 선택해야 합니다.", MB_ICONSTOP);
		return;
	}

	m_ctrListBoxRecipe.GetText(m_nCurSelIndex, strRecipeName);
	strRecipeName = strRecipeName + "_COPY";
	m_ctrListBoxRecipe.AddString(strRecipeName);
	m_ctrListBoxRecipe.EnableWindow(FALSE);	

	m_nCurSelIndex = m_ctrListBoxRecipe.GetCount() - 1;

	m_ctrListBoxRecipe.SetCurSel(m_nCurSelIndex);
	
	SetDlgItemText(IDC_EDIT_CUR_ID, strRecipeName);

	SendMessageW(::GetDlgItem(this->GetSafeHwnd(), IDC_EDIT_CUR_ID), EM_SETREADONLY, FALSE, 0);
}

void CRecipeDefine::OnBnClickedBtnReplace()
{
	int nCurSelIndex = m_ctrListBoxRecipe.GetCurSel();
	if (nCurSelIndex == -1)
	{
		AfxMessageBox("수정할 항목을 선택해야 합니다.", MB_ICONSTOP);
		return;
	}
	SendMessageW(::GetDlgItem(this->GetSafeHwnd(), IDC_EDIT_CUR_ID), EM_SETREADONLY, FALSE	, 0);
	m_ctrListBoxRecipe.EnableWindow(FALSE);
}

void CRecipeDefine::OnBnClickedBtnCancel()
{
	SendMessageW(::GetDlgItem(this->GetSafeHwnd(), IDC_EDIT_CUR_ID), EM_SETREADONLY, TRUE, 0);
	m_ctrListBoxRecipe.EnableWindow(TRUE);
	m_ctrListBoxRecipe.ResetContent();
	m_fnGetRecipeList();
	SetDlgItemText(IDC_EDIT_CUR_ID, "");
}

void CRecipeDefine::OnBnClickedBtnSave()
{		
	m_st_Cur_Recipe.nBoxType = m_ctrCboBoxType.GetCurSel() + 1;

	m_fnCheckVacAirStep();

	m_fnGetCurValue();

	m_fnFileSave();

	SendMessageW(::GetDlgItem(this->GetSafeHwnd(), IDC_EDIT_CUR_ID), EM_SETREADONLY, TRUE, 0);
	m_ctrListBoxRecipe.EnableWindow(TRUE);
	m_ctrListBoxRecipe.ResetContent();
	m_fnGetRecipeList();
	SetDlgItemText(IDC_EDIT_CUR_ID, "");
}

void CRecipeDefine::m_fnFileSave()
{
	int		nStep = 0;
	char	chFilePath[_MAX_PATH] = { 0, };	
	char	chTypeName[nTypeNameLength] = { 0, };
	
	CString strNewRecipeName;	CString strDelRecipeName;
	CString strNewFullPath;			CString strDelFullPath;

	m_ctrListBoxRecipe.GetText(m_nCurSelIndex, strDelRecipeName);
	strDelFullPath = _T(TM02_RECIPE_INI_PATH) + strDelRecipeName + ".ini";
	DeleteFile(strDelFullPath);

	GetDlgItemText(IDC_EDIT_CUR_ID, strNewRecipeName);
	strNewFullPath = _T(TM02_RECIPE_INI_PATH) + strNewRecipeName + ".ini";
	
	FILE* stream;
	CString	cstGetString;
	char*	chGetString;

	if (fopen_s(&stream, strNewFullPath, ("w")) == 0)
	{	
		ZeroMemory(chTypeName, nTypeNameLength);
		chTypeName[0] = '[';

		chGetString = (LPSTR)(LPCSTR)strNewRecipeName;
		strncpy_s(&chTypeName[1], nTypeNameLength - strlen(chTypeName), chGetString, strlen(chGetString));
		strncpy_s(&chTypeName[strlen(chTypeName)], nTypeNameLength - strlen(chTypeName), "]", 1);
		fprintf(stream, ("%s\n"), chTypeName);

		fprintf(stream, ("TM02_BOX_TYPE=%d\n"), m_st_Cur_Recipe.nBoxType);
		fprintf(stream, ("TM02_STICK_SIZE=%d\n"), m_st_Cur_Recipe.nStickSize);
		fprintf(stream, ("AM01_LOAD_OFFSET=%d\n"), m_st_Cur_Recipe.nAM01LoadOffset);
		fprintf(stream, ("\n"));
		fprintf(stream, ("TM02_LT01_DRIVE_ALIGN=%d\n"), m_st_Cur_Recipe.nLT01_Drive_Align);
		fprintf(stream, ("TM02_LT01_Z_ALIGN=%d\n"), m_st_Cur_Recipe.nLT01_Z_Align);
		fprintf(stream, ("TM02_LT01_DRIVE_PP_GET=%d\n"), m_st_Cur_Recipe.nLT01_Drive_PP_Get);
		fprintf(stream, ("TM02_LT01_DRIVE_ST_GET=%d\n"), m_st_Cur_Recipe.nLT01_Drive_ST_Get);
		fprintf(stream, ("TM02_LT01_SHIFT_GET=%d\n"), m_st_Cur_Recipe.nLT01_Shift_Get);
		fprintf(stream, ("TM02_LT01_TILT_GET=%d\n"), m_st_Cur_Recipe.nLT01_Tilt_Get);
		fprintf(stream, ("TM02_LT01_Z_PP_GET=%d\n"), m_st_Cur_Recipe.nLT01_Z_PP_Get);
		fprintf(stream, ("TM02_LT01_Z_ST_GET=%d\n"), m_st_Cur_Recipe.nLT01_Z_ST_Get);
		fprintf(stream, ("TM02_LT01_PP_PURGE_GET=%d\n"), m_st_Cur_Recipe.nLT01_PP_Purge_Get);
		fprintf(stream, ("TM02_LT01_VAC_PP_GET=%d\n"), m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirSelection);		
		fprintf(stream, ("TM02_LT01_VAC_ST_GET=%d\n"), m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirSelection);
		fprintf(stream, ("\n"));
		fprintf(stream, ("TM02_Z_READY_POS=%d\n"), m_st_Cur_Recipe.nZ_Ready_Pos);

		fprintf(stream, ("TM02_QR_DRIVE_POS=%d\n"), m_st_Cur_Recipe.nQR_Drive_Pos);
		fprintf(stream, ("TM02_QR_Z_POS=%d\n"), m_st_Cur_Recipe.nQR_Z_Pos);
		fprintf(stream, ("\n"));
		fprintf(stream, ("TM02_TT01_DRIVE_PUT=%d\n"), m_st_Cur_Recipe.nTT01_Dirve_Put);
		fprintf(stream, ("TM02_TT01_SHIFT_PUT=%d\n"), m_st_Cur_Recipe.nTT01_Shift_Put);
		fprintf(stream, ("TM02_TT01_TILT_PUT=%d\n"), m_st_Cur_Recipe.nTT01_Tilt_Put);
		fprintf(stream, ("TM02_TT01_Z_ST_PUT=%d\n"), m_st_Cur_Recipe.nTT01_Z_ST_Put);
		fprintf(stream, ("TM02_TT01_ST_PURGE_PUT=%d\n"), m_st_Cur_Recipe.nTT01_ST_Purge_Put);
		fprintf(stream, ("\n"));
		fprintf(stream, ("TM02_UT02_DRIVE_GET=%d\n"), m_st_Cur_Recipe.nUT02_Drive_Get);
		fprintf(stream, ("TM02_UT02_SHIFT_GET=%d\n"), m_st_Cur_Recipe.nUT02_Shift_Get);
		fprintf(stream, ("TM02_UT02_TILT_GET=%d\n"), m_st_Cur_Recipe.nUT02_Tilt_Get);
		fprintf(stream, ("TM02_UT02_Z_ST_GET=%d\n"), m_st_Cur_Recipe.nUT02_Z_ST_Get);
		fprintf(stream, ("TM02_UT02_VAC_GET=%d\n"), m_st_Cur_Recipe.stUT02_Vac_Get.nAirSelection);		
		fprintf(stream, ("\n"));
		fprintf(stream, ("TM02_UT01_DRIVE_PP_PUT=%d\n"), m_st_Cur_Recipe.nUT01_Drive_PP_Put);
		fprintf(stream, ("TM02_UT01_DRIVE_ST_PUT=%d\n"), m_st_Cur_Recipe.nUT01_Drive_ST_Put);
		fprintf(stream, ("TM02_UT01_SHIFT_PUT=%d\n"), m_st_Cur_Recipe.nUT01_Shift_Put);
		fprintf(stream, ("TM02_UT01_TILT_PUT=%d\n"), m_st_Cur_Recipe.nUT01_Tilt_Put);
		fprintf(stream, ("TM02_UT01_Z_PP_PUT=%d\n"), m_st_Cur_Recipe.nUT01_Z_PP_Put);
		fprintf(stream, ("TM02_UT01_Z_ST_PUT=%d\n"), m_st_Cur_Recipe.nUT01_Z_ST_Put);
		fprintf(stream, ("TM02_UT01_PURGE_PUT=%d\n"), m_st_Cur_Recipe.nUT01_Purge_Put);
		fprintf(stream, ("\n"));
		fprintf(stream, ("TM02_BT01_DRIVE_PP_PUT=%d\n"), m_st_Cur_Recipe.nBT01_Drive_PP_Put);
		fprintf(stream, ("TM02_BT01_DRIVE_ST_PUT=%d\n"), m_st_Cur_Recipe.nBT01_Drive_ST_Put);
		fprintf(stream, ("TM02_BT01_SHIFT_PUT=%d\n"), m_st_Cur_Recipe.nBT01_Shift_Put);
		fprintf(stream, ("TM02_BT01_TILT_PUT=%d\n"), m_st_Cur_Recipe.nBT01_Tilt_Put);
		fprintf(stream, ("TM02_BT01_Z_PP_PUT=%d\n"), m_st_Cur_Recipe.nBT01_Z_PP_Put);
		fprintf(stream, ("TM02_BT01_Z_ST_PUT=%d\n"), m_st_Cur_Recipe.nBT01_Z_ST_Put);
		fprintf(stream, ("TM02_BT01_PURGE_PUT=%d\n"), m_st_Cur_Recipe.nBT01_Purge_Put);
		fprintf(stream, ("\n"));
		fprintf(stream, ("TT01_VAC_GET=%d\n"), m_st_Cur_Recipe.stTT01_Vac_Table_Get.nAirSelection);
		fprintf(stream, ("UT02_VAC_GET=%d\n"), m_st_Cur_Recipe.stUT02_Vac_Table_Get.nAirSelection);		
		
		fclose(stream);	
	}
}

void CRecipeDefine::m_fnGetCurValue()
{
	m_st_Cur_Recipe.nStickSize = GetDlgItemInt(IDC_CBO_STICK_SIZE);
	m_st_Cur_Recipe.nAM01LoadOffset = GetDlgItemInt(IDC_EDIT_LOAD_OFFSET);
	m_st_Cur_Recipe.nLT01_Drive_Align = GetDlgItemInt(IDC_LT01_EDIT1);
	m_st_Cur_Recipe.nLT01_Z_Align = GetDlgItemInt(IDC_LT01_EDIT2);
	m_st_Cur_Recipe.nLT01_Drive_PP_Get = GetDlgItemInt(IDC_LT01_EDIT3);
	m_st_Cur_Recipe.nLT01_Drive_ST_Get = GetDlgItemInt(IDC_LT01_EDIT4);
	m_st_Cur_Recipe.nLT01_Shift_Get = GetDlgItemInt(IDC_LT01_EDIT5);
	m_st_Cur_Recipe.nLT01_Tilt_Get = GetDlgItemInt(IDC_LT01_EDIT6);
	m_st_Cur_Recipe.nLT01_Z_PP_Get = GetDlgItemInt(IDC_LT01_EDIT7);
	m_st_Cur_Recipe.nLT01_Z_ST_Get = GetDlgItemInt(IDC_LT01_EDIT8);
	m_st_Cur_Recipe.nLT01_PP_Purge_Get = GetDlgItemInt(IDC_LT01_EDIT9);	
	
	m_st_Cur_Recipe.nZ_Ready_Pos = GetDlgItemInt(IDC_Z_READY);

	m_st_Cur_Recipe.nQR_Drive_Pos = GetDlgItemInt(IDC_QR_EDIT1);
	m_st_Cur_Recipe.nQR_Z_Pos = GetDlgItemInt(IDC_QR_EDIT2);

	m_st_Cur_Recipe.nTT01_Dirve_Put = GetDlgItemInt(IDC_TT01_EDIT1);
	m_st_Cur_Recipe.nTT01_Shift_Put = GetDlgItemInt(IDC_TT01_EDIT2);
	m_st_Cur_Recipe.nTT01_Tilt_Put = GetDlgItemInt(IDC_TT01_EDIT3);
	m_st_Cur_Recipe.nTT01_Z_ST_Put = GetDlgItemInt(IDC_TT01_EDIT4);
	m_st_Cur_Recipe.nTT01_ST_Purge_Put = GetDlgItemInt(IDC_TT01_EDIT5);

	m_st_Cur_Recipe.nUT02_Drive_Get = GetDlgItemInt(IDC_UT02_EDIT1);
	m_st_Cur_Recipe.nUT02_Shift_Get = GetDlgItemInt(IDC_UT02_EDIT2);
	m_st_Cur_Recipe.nUT02_Tilt_Get = GetDlgItemInt(IDC_UT02_EDIT3);
	m_st_Cur_Recipe.nUT02_Z_ST_Get = GetDlgItemInt(IDC_UT02_EDIT4);

	m_st_Cur_Recipe.nUT01_Drive_PP_Put = GetDlgItemInt(IDC_UT01_EDIT1);
	m_st_Cur_Recipe.nUT01_Drive_ST_Put = GetDlgItemInt(IDC_UT01_EDIT7);
	m_st_Cur_Recipe.nUT01_Shift_Put = GetDlgItemInt(IDC_UT01_EDIT2);
	m_st_Cur_Recipe.nUT01_Tilt_Put = GetDlgItemInt(IDC_UT01_EDIT3);
	m_st_Cur_Recipe.nUT01_Z_PP_Put = GetDlgItemInt(IDC_UT01_EDIT4);
	m_st_Cur_Recipe.nUT01_Z_ST_Put = GetDlgItemInt(IDC_UT01_EDIT5);
	m_st_Cur_Recipe.nUT01_Purge_Put = GetDlgItemInt(IDC_UT01_EDIT6);

	m_st_Cur_Recipe.nBT01_Drive_PP_Put = GetDlgItemInt(IDC_BT01_EDIT1);
	m_st_Cur_Recipe.nBT01_Drive_ST_Put = GetDlgItemInt(IDC_BT01_EDIT7);
	m_st_Cur_Recipe.nBT01_Shift_Put = GetDlgItemInt(IDC_BT01_EDIT2);
	m_st_Cur_Recipe.nBT01_Tilt_Put = GetDlgItemInt(IDC_BT01_EDIT3);
	m_st_Cur_Recipe.nBT01_Z_PP_Put = GetDlgItemInt(IDC_BT01_EDIT4);
	m_st_Cur_Recipe.nBT01_Z_ST_Put = GetDlgItemInt(IDC_BT01_EDIT5);
	m_st_Cur_Recipe.nBT01_Purge_Put = GetDlgItemInt(IDC_BT01_EDIT6);
}

void CRecipeDefine::m_fnCheckVacAirStep()
{
	m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirSelection = 0;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC1))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep1 = 1;	

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC2))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep2 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC3))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep3 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC4))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep4 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC5))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep5 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC6))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep6 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC7))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep7 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC8))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep8 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC9))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep9 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_PP_VAC10))
		m_st_Cur_Recipe.stLT01_Vac_PP_Get.nAirStep.m_btStep10 = 1;
	
	///////////////
	m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirSelection = 0;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC1))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep1 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC2))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep2 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC3))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep3 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC4))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep4 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC5))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep5 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC6))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep6 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC7))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep7 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC8))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep8 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC9))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep9 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_LT01_VAC10))
		m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirStep.m_btStep10 = 1;



	m_st_Cur_Recipe.stUT02_Vac_Get.nAirSelection = 0;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC1))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep1 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC2))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep2 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC3))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep3 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC4))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep4 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC5))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep5 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC6))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep6 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC7))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep7 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC8))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep8 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC9))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep9 = 1;

	if (IsDlgButtonChecked(IDC_CHECK_UT02_VAC10))
		m_st_Cur_Recipe.stUT02_Vac_Get.nAirStep.m_btStep10 = 1;


	m_st_Cur_Recipe.stTT01_Vac_Table_Get.nAirSelection = 0;
	m_st_Cur_Recipe.stTT01_Vac_Table_Get.nAirSelection = m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirSelection;
	

	m_st_Cur_Recipe.stUT02_Vac_Table_Get.nAirSelection = 0;
	m_st_Cur_Recipe.stUT02_Vac_Table_Get.nAirSelection = m_st_Cur_Recipe.stLT01_Vac_ST_Get.nAirSelection;		
}


int CRecipeDefine::m_fnGetRecipeList()
{
	char chFilePath[_MAX_PATH] = { 0, };

	strncpy_s(chFilePath, _MAX_PATH, TM02_RECIPE_INI_PATH, strlen(TM02_RECIPE_INI_PATH));
	strncpy_s(&chFilePath[strlen(TM02_RECIPE_INI_PATH)], _MAX_PATH - strlen(TM02_RECIPE_INI_PATH), "*.ini", strlen("*.ini"));

	char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];

	CFileFind  finder;
	BOOL bWorking = finder.FindFile(chFilePath);
	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		_splitpath_s(finder.GetFileName(), drive, dir, fname, ext);
		if (!finder.IsDots() && !finder.IsDirectory())
		{
			m_ctrListBoxRecipe.AddString(fname);
		}
			
	}
	return 0;
}

void CRecipeDefine::OnEnChangeEditCurId()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
// 	CString strCurID;
 //	GetDlgItemText(IDC_EDIT_CUR_ID, strCurID);	
 	//m_ctrListBoxRecipe.SetDlgItemTextA(m_nCurSelIndex, strCurID);
 
// 	m_ctrListBoxRecipe.Invalidate(FALSE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CRecipeDefine::OnBnClickedBtnCalc()
{
	int nStickSize = GetDlgItemInt(IDC_CBO_STICK_SIZE);
	switch (nStickSize)
	{
	case 28000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 36000);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);
		break;
	case 69000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 15500);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);
		break;
	case 100000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 0);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(TRUE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(TRUE);
		break;
	case 128000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 36000);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);
		break;
	case 169000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 15500);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);
		break;
	case 200000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 0);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(TRUE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(TRUE);
		break;
	case 240000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 100000);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);
		break;
	case 312000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 64000);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);
		break;
	case 368000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 36000);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);
		break;
	case 409000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 15500);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(FALSE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(FALSE);
		break;
	case 440000:
		SetDlgItemInt(IDC_EDIT_LOAD_OFFSET, 0);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC3))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_VAC10))->SetCheck(TRUE);

		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC3))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_LT01_PP_VAC10))->SetCheck(TRUE);

		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC1))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC2))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC3))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC4))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC5))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC6))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC7))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC8))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC9))->SetCheck(TRUE);
		((CButton*)GetDlgItem(IDC_CHECK_UT02_VAC10))->SetCheck(TRUE);
		break;	
	default:
		break;
	}
}
