#pragma once

#define BCR_NET_IP						"192.168.1.15"
#define BCR_NET_PORT					23

//#define BCR_GET_TRIGGER_MODE		"||>GET TRIGGER"
#define BCR_TRIGGER_ON				"||>TRIGGER ON"
#define BCR_TRIGGER_OFF				"||>TRIGGER OFF"
//#define BCR_GET_LIGHT_AIMER			"||>GET LIGHT.AIMER"
#define BCR_LIGHT_AIMER_ON			"||>SET LIGHT.AIMER 1"
#define BCR_LIGHT_AIMER_OFF			"||>SET LIGHT.AIMER 0"


class CBCRCtrl
{
public:
	CBCRCtrl(void);
	~CBCRCtrl(void);
public:
	SOCKET m_BCRSockfd;
	bool		m_RunThread;
	CWinThread	*m_pHandleThread;
public:	
	CString m_IPAddress;
	CString m_PortName;

	HWND m_ParentUpdate;

	//Read Time Out을 측정 하기 위해서.
	bool bTimerRun;
	HANDLE hTimer;
	HANDLE hTimerQueue;
	int TimerRun();
	int TimerStop();
public:
	void SetConnectInfo(CString IPAddress, CString PortName);
	bool BCRConnect(DWORD nIPADD, int PortNum);
	bool BCRDisconnect();
	void RecvCallBack(LPVOID pBuf, DWORD recvLen);

	//////////////////////////////////////////////////////////////////////////
	bool LinkOpen();
	bool SetTriggerOnOff(bool OnOff);//Data Read를 할때 On을 한다.
	bool SetLightAimerOnOff(bool OnOff);//Code위치를 지정 할때 켠다.

	void m_fnSetParentHandle(HWND ParentHandle);
};
UINT Thread_BCRCMDProc(LPVOID pParam);
