// EmissionTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "EmissionTable.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CEmissionTable 대화 상자입니다.

IMPLEMENT_DYNAMIC(CEmissionTable, CDialogEx)

CEmissionTable::CEmissionTable(CWnd* pParent /*=NULL*/)
	: CDialogEx(CEmissionTable::IDD, pParent)
{

}

CEmissionTable::~CEmissionTable()
{
}

void CEmissionTable::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_CYL_UP, m_ctrBtnCylUp);
	DDX_Control(pDX, IDC_BTN_CYL_DN, m_ctrBtnCylDn);
	DDX_Control(pDX, IDC_BTN_STICK_VAC_ON, m_ctrBtnVacOn);
	DDX_Control(pDX, IDC_BTN_STICK_VAC_OFF, m_ctrBtnVacOff);
	DDX_Control(pDX, IDC_BTN_STICK_PURGE_ON, m_ctrBtnPurge);
	DDX_Control(pDX, IDC_BTN_SAVE, m_ctrBtnValueSave);
	DDX_Control(pDX, IDC_BTN_CANCEL, m_ctrBtnSaveCancel);
	DDX_Control(pDX, IDC_BTN_INIT, m_ctrBtnMoveDefault);
	DDX_Control(pDX, IDC_BTN_MOTION_STOP, m_ctrBtnUT02ActionStop);
}


BEGIN_MESSAGE_MAP(CEmissionTable, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_SAVE, &CEmissionTable::OnBnClickedBtnSave)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CEmissionTable::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_CYL_UP, &CEmissionTable::OnBnClickedBtnCylUp)
	ON_BN_CLICKED(IDC_BTN_CYL_DN, &CEmissionTable::OnBnClickedBtnCylDn)
	ON_BN_CLICKED(IDC_BTN_STICK_VAC_ON, &CEmissionTable::OnBnClickedBtnStickVacOn)
	ON_BN_CLICKED(IDC_BTN_STICK_VAC_OFF, &CEmissionTable::OnBnClickedBtnStickVacOff)
	ON_BN_CLICKED(IDC_BTN_STICK_PURGE_ON, &CEmissionTable::OnBnClickedBtnStickPurgeOn)
END_MESSAGE_MAP()


// CEmissionTable 메시지 처리기입니다.

BOOL CEmissionTable::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CEmissionTable::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_EditFont.DeleteObject();
	CDialogEx::PostNcDestroy();
}


HBRUSH CEmissionTable::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CEmissionTable::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style On
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);

	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyleOff.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	m_ctrBtnValueSave.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnValueSave.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;
	
 	m_ctrBtnCylUp.SetTextColor(&tColor); m_ctrBtnCylUp.SetCheckButton(false);			
 	m_ctrBtnCylUp.SetFont(&tFont); m_ctrBtnCylUp.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnCylDn.SetTextColor(&tColor); m_ctrBtnCylDn.SetCheckButton(false);			
	m_ctrBtnCylDn.SetFont(&tFont); m_ctrBtnCylDn.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOn.SetTextColor(&tColor); m_ctrBtnVacOn.SetCheckButton(false);			
	m_ctrBtnVacOn.SetFont(&tFont); m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOff.SetTextColor(&tColor); m_ctrBtnVacOff.SetCheckButton(false);			
	m_ctrBtnVacOff.SetFont(&tFont); m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnPurge.SetTextColor(&tColor); m_ctrBtnPurge.SetCheckButton(false);			
	m_ctrBtnPurge.SetFont(&tFont); m_ctrBtnPurge.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnValueSave.SetTextColor(&tColor); m_ctrBtnValueSave.SetCheckButton(false);			
	m_ctrBtnValueSave.SetFont(&tFont); m_ctrBtnValueSave.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnSaveCancel.SetTextColor(&tColor); m_ctrBtnSaveCancel.SetCheckButton(false);			
	m_ctrBtnSaveCancel.SetFont(&tFont); m_ctrBtnSaveCancel.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnMoveDefault.SetTextColor(&tColor); m_ctrBtnMoveDefault.SetCheckButton(false);			
	m_ctrBtnMoveDefault.SetFont(&tFont); m_ctrBtnMoveDefault.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnUT02ActionStop.SetTextColor(&tColor);	m_ctrBtnUT02ActionStop.SetCheckButton(false);
	m_ctrBtnUT02ActionStop.SetFont(&tFont);	m_ctrBtnUT02ActionStop.SetRoundButtonStyle(&m_tStyle1);

}

int CEmissionTable::m_fnCreateControlFont(void)
{		
	return 0;
}

BOOL CEmissionTable::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);
	m_EditFont.CreatePointFont(100,"Arial");	

	m_fnCreateControlFont();
	m_fnCreateButton();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CEmissionTable::m_fnSetParameter(SUT02_POSITION* stPointer)
{		
	m_st_UT02_Pos = stPointer;

	return 0;
}

int CEmissionTable::m_fnDisplayParameter(void)
{
	SetDlgItemInt(IDC_EDIT_STICK_PURGE_SEC, m_st_UT02_Pos->nPurge_Time);
	
	return 0;
}

void CEmissionTable::OnBnClickedBtnSave()
{
	CString strWriteData;

	m_st_UT02_Pos->nPurge_Time = GetDlgItemInt(IDC_EDIT_STICK_PURGE_SEC);	
	
	strWriteData.Format("%d", m_st_UT02_Pos->nPurge_Time);
	WritePrivateProfileString(SECTION_UT02_POS_DATA, KEY_PURGE_TIME, strWriteData, SYSTEM_PARAM_INI_PATH);	
}

void CEmissionTable::OnBnClickedBtnCancel()
{
	m_fnDisplayParameter();
}

void CEmissionTable::OnBnClickedBtnCylUp()
{
	m_stUT02_Mess.Reset();

	m_stUT02_Mess.nAction = m_stUT02_Mess.Act_None;
	m_stUT02_Mess.nResult = m_stUT02_Mess.Ret_None;	
	m_stUT02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stUT02_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[UT02_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[UT02_CYL].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_UT02_TABLE_UP, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_UT02_TABLE_UP, nBiz_Unit_Zero
				);

			strLogMessage.Format("UT02_CYL : UP.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("UT02_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("UT02_CYL : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CEmissionTable::OnBnClickedBtnCylDn()
{
	m_stUT02_Mess.Reset();

	m_stUT02_Mess.nAction = m_stUT02_Mess.Act_None;
	m_stUT02_Mess.nResult = m_stUT02_Mess.Ret_None;	
	m_stUT02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stUT02_Mess, nMessSize);


	CString strLogMessage;

	if(G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[UT02_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[UT02_CYL].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_UT02_TABLE_DN, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_UT02_TABLE_DN, nBiz_Unit_Zero
				);

			strLogMessage.Format("UT02_CYL : DOWN.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("UT02_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("UT02_CYL : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CEmissionTable::OnBnClickedBtnStickVacOn()
{
	m_fnCheckStickAirStep();

	CString strLogMessage;
	strLogMessage.Format("UT02_VAC : ON.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickVacuum(m_stAirStep, nBiz_Seq_UT02_Vac_ON);	
}

void CEmissionTable::OnBnClickedBtnStickVacOff()
{
	m_fnCheckStickAirStep();

	CString strLogMessage;
	strLogMessage.Format("UT02_VAC : OFF.");
	m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

	m_fnStickVacuum(m_stAirStep, nBiz_Seq_UT02_Vac_OFF);	
}

void CEmissionTable::m_fnStickVacuum(AIR_SELECTION nAirSelect, int nActType)
{
	m_stUT02_Mess.Reset();
	m_stUT02_Mess.nAction = m_stUT02_Mess.Act_None;
	m_stUT02_Mess.nResult = m_stUT02_Mess.Ret_None;
	m_stUT02_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stUT02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stUT02_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[UT02_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[UT02_VAC].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nActType, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nActType, nBiz_Unit_Zero
				);			
		}
		else
		{
			strLogMessage.Format("UT02_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("UT02_VAC : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_VAC].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CEmissionTable::OnBnClickedBtnStickPurgeOn()
{
	int nPurgeTime = GetDlgItemInt(IDC_EDIT_STICK_PURGE_SEC);

	m_fnCheckStickAirStep();

	m_fnStickPurge(m_stAirStep, nPurgeTime);	
}

void CEmissionTable::m_fnStickPurge(AIR_SELECTION nAirSelect, int nPurgeTime)
{
	m_stUT02_Mess.Reset();

	m_stUT02_Mess.nAction = m_stUT02_Mess.Act_None;
	m_stUT02_Mess.nResult = m_stUT02_Mess.Ret_None;
	m_stUT02_Mess.SelectPoint = nAirSelect.nAirSelection;
	m_stUT02_Mess.ActionTime = nPurgeTime;
	m_stUT02_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stUT02_Mess, nMessSize);

	CString strLogMessage;

	if (G_MOTION_STATUS[UT02_PURGE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if (G_MOTION_STATUS[UT02_PURGE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[UT02_PURGE].B_MOTION_STATUS = RUNNING;
			G_MOTION_STATUS[UT02_PURGE].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_UT02_Purge, nBiz_Unit_Zero,
				nMessSize, chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_UT02_Purge, nBiz_Unit_Zero
				);

			strLogMessage.Format("UT02_PURGE : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		}
		else
		{
			strLogMessage.Format("UT02_PURGE : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("UT02_PURGE : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_PURGE].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[UT02_PURGE].UN_CMD_COUNT = 0;
	}

	delete[]chMsgBuf;
}

void CEmissionTable::m_fnCheckStickAirStep()
{
	m_stAirStep.nAirSelection = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC1))
		m_stAirStep.nAirStep.m_btStep1 = 1;
	else
		m_stAirStep.nAirStep.m_btStep1 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC2))
		m_stAirStep.nAirStep.m_btStep2 = 1;
	else
		m_stAirStep.nAirStep.m_btStep2 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC3))
		m_stAirStep.nAirStep.m_btStep3 = 1;
	else
		m_stAirStep.nAirStep.m_btStep3 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC4))
		m_stAirStep.nAirStep.m_btStep4 = 1;
	else
		m_stAirStep.nAirStep.m_btStep4 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC5))
		m_stAirStep.nAirStep.m_btStep5 = 1;
	else
		m_stAirStep.nAirStep.m_btStep5 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC6))
		m_stAirStep.nAirStep.m_btStep6 = 1;
	else
		m_stAirStep.nAirStep.m_btStep6 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC7))
		m_stAirStep.nAirStep.m_btStep7 = 1;
	else
		m_stAirStep.nAirStep.m_btStep7 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC8))
		m_stAirStep.nAirStep.m_btStep8 = 1;
	else
		m_stAirStep.nAirStep.m_btStep8 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC9))
		m_stAirStep.nAirStep.m_btStep9 = 1;
	else
		m_stAirStep.nAirStep.m_btStep9 = 0;

	if(IsDlgButtonChecked(IDC_CHECK_STICK_VAC10))
		m_stAirStep.nAirStep.m_btStep10 = 1;
	else
		m_stAirStep.nAirStep.m_btStep10 = 0;
}

void CEmissionTable::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{	
	CVS11IndexerDlg*  vpMainDlg = (CVS11IndexerDlg*)AfxGetApp()->m_pMainWnd;
	vpMainDlg->m_fnAppendList(strLog, nColorType);
}

int CEmissionTable::m_fn_UT02_TableUp_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[UT02_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		strLogMessage = "UT02_CYL : 테이블 업 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[UT02_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_Error:	
		strLogMessage = "UT02_CYL : 테이블 업 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_TimeOut:	
		strLogMessage.Format("UT02_CYL :테이블 업 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnCylUp();
		break;
	default:	
		strLogMessage.Format("UT02_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT = 0;
		break;
	}
	return 0;
}

int CEmissionTable::m_fn_UT02_TableDn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[UT02_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		strLogMessage = "UT02_CYL : 테이블 다운 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[UT02_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_Error:	
		strLogMessage = "UT02_CYL : 테이블 다운 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_TimeOut:	
		strLogMessage.Format("UT02_CYL : 테이블 다운 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnCylDn();
		break;
	default:	
		strLogMessage.Format("UT02_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_CYL].UN_CMD_COUNT = 0;
		break;
	}
	return 0;
}

int CEmissionTable::m_fn_UT02_VacOn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[UT02_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		strLogMessage = "UT02_VAC : Vac. ON 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[UT02_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_Error:	
		strLogMessage = "UT02_VAC : Vac. ON 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_TimeOut:	
		strLogMessage.Format("UT02_VAC : Vac. ON 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnStickVacOn();
		break;
	default:	
		strLogMessage.Format("UT02_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT = 0;
		break;
	}
	return 0;
}

int CEmissionTable::m_fn_UT02_VacOff_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[UT02_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		strLogMessage = "UT02_VAC : Vac. OFF 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[UT02_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_Error:	
		strLogMessage = "UT02_VAC : Vac. OFF 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_TimeOut:	
		strLogMessage.Format("UT02_VAC : Vac. OFF 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnStickVacOff();
		break;
	default:	
		strLogMessage.Format("UT02_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_VAC].UN_CMD_COUNT = 0;
		break;
	}
	return 0;
}

int CEmissionTable::m_fn_UT02_Purge_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[UT02_PURGE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		strLogMessage = "UT02_PURGE : Purge 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[UT02_PURGE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[UT02_PURGE].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_Error:	
		strLogMessage = "UT02_PURGE : Purge 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_PURGE].UN_CMD_COUNT = 0;
		break;
	case stReturn_Mess.Ret_TimeOut:	
		strLogMessage.Format("UT02_PURGE : Purge 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[UT02_PURGE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnStickPurgeOn();
		break;
	default:	
		strLogMessage.Format("UT02_PURGE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[UT02_PURGE].UN_CMD_COUNT = 0;
		break;
	}
	return 0;
}

void CEmissionTable::m_fnDisplaySignal()
{
	if (G_STMACHINE_STATUS.stEmissionTable.In02_Sn_OutputLTableCylinderUp == ON &&
		G_STMACHINE_STATUS.stEmissionTable.In05_Sn_OutputRTableCylinderUp == ON)
	{
		m_ctrBtnCylUp.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnCylDn.SetRoundButtonStyle(&m_tStyleOff);
	}

	if (G_STMACHINE_STATUS.stEmissionTable.In03_Sn_OutputLTableCylinderDown == ON &&
		G_STMACHINE_STATUS.stEmissionTable.In06_Sn_OutputRTableCylinderDown == ON)
	{
		m_ctrBtnCylUp.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnCylDn.SetRoundButtonStyle(&m_tStyleOn);
	}	

	if (G_STMACHINE_STATUS.stEmissionTable.In04_Sn_OutputTableVacuum == ON)
	{
		m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOn);
	}


	

}