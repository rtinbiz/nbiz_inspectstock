// LoadingTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "LoadingTable.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CLoadingTable 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLoadingTable, CDialogEx)

CLoadingTable::CLoadingTable(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLoadingTable::IDD, pParent)
	, m_nCtrPosIndex(0)
{

}

CLoadingTable::~CLoadingTable()
{
}

void CLoadingTable::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_CYL_FWD, m_ctrBtnCylFwd);
	DDX_Control(pDX, IDC_BTN_CYL_BWD, m_ctrBtnCylBwd);
	DDX_Control(pDX, IDC_BTN_VAC_ON, m_ctrBtnVacOn);
	DDX_Control(pDX, IDC_BTN_VAC_OFF, m_ctrBtnVacOff);
	DDX_Control(pDX, IDC_BTN_PURGE, m_ctrBtnPurge);
	DDX_Control(pDX, IDC_BTN_POS_MOVE, m_ctrBtnMove);
	DDX_Control(pDX, IDC_BTN_POS_DN, m_ctrBtnMoveDn);
	DDX_Control(pDX, IDC_BTN_POS_UP, m_ctrBtnMoveUp);
	DDX_Control(pDX, IDC_BTN_SAVE, m_ctrBtnValueSave);
	DDX_Control(pDX, IDC_BTN_CANCEL, m_ctrBtnSaveCancel);
	DDX_Control(pDX, IDC_BTN_INIT, m_ctrBtnMoveDefault);
	DDX_Radio(pDX, IDC_RADIO_SMALL, m_nCtrPosIndex);
	DDX_Control(pDX, IDC_BTN_MOTION_STOP, m_ctrBtnLT01ActionStop);
}


BEGIN_MESSAGE_MAP(CLoadingTable, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_SAVE, &CLoadingTable::OnBnClickedBtnSave)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CLoadingTable::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_CYL_FWD, &CLoadingTable::OnBnClickedBtnCylFwd)
	ON_BN_CLICKED(IDC_BTN_CYL_BWD, &CLoadingTable::OnBnClickedBtnCylBwd)
	ON_BN_CLICKED(IDC_BTN_VAC_ON, &CLoadingTable::OnBnClickedBtnVacOn)
	ON_BN_CLICKED(IDC_BTN_VAC_OFF, &CLoadingTable::OnBnClickedBtnVacOff)
	ON_BN_CLICKED(IDC_BTN_PURGE, &CLoadingTable::OnBnClickedBtnPurge)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE, &CLoadingTable::OnBnClickedBtnPosMove)
	ON_BN_CLICKED(IDC_BTN_POS_DN, &CLoadingTable::OnBnClickedBtnPosDn)
	ON_BN_CLICKED(IDC_BTN_POS_UP, &CLoadingTable::OnBnClickedBtnPosUp)
	ON_BN_CLICKED(IDC_BTN_MOTION_STOP, &CLoadingTable::OnBnClickedBtnMotionStop)
END_MESSAGE_MAP()

// CLoadingTable 메시지 처리기입니다.
BOOL CLoadingTable::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CLoadingTable::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_EditFont.DeleteObject();
	CDialogEx::PostNcDestroy();
}

HBRUSH CLoadingTable::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CLoadingTable::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style On
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);

	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyleOff.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	m_ctrBtnMove.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnMove.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	m_ctrBtnCylFwd.SetTextColor(&tColor);
	m_ctrBtnCylFwd.SetCheckButton(false);			
	m_ctrBtnCylFwd.SetFont(&tFont);
	m_ctrBtnCylFwd.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnCylBwd.SetTextColor(&tColor);
	m_ctrBtnCylBwd.SetCheckButton(false);			
	m_ctrBtnCylBwd.SetFont(&tFont);
	m_ctrBtnCylBwd.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOn.SetTextColor(&tColor);
	m_ctrBtnVacOn.SetCheckButton(false);			
	m_ctrBtnVacOn.SetFont(&tFont);
	m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOff.SetTextColor(&tColor);
	m_ctrBtnVacOff.SetCheckButton(false);			
	m_ctrBtnVacOff.SetFont(&tFont);
	m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnPurge.SetTextColor(&tColor);
	m_ctrBtnPurge.SetCheckButton(false);			
	m_ctrBtnPurge.SetFont(&tFont);
	m_ctrBtnPurge.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnMove.SetTextColor(&tColor);
	m_ctrBtnMove.SetCheckButton(false);			
	m_ctrBtnMove.SetFont(&tFont);
	m_ctrBtnMove.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDn.SetTextColor(&tColor);
	m_ctrBtnMoveDn.SetCheckButton(false);			
	m_ctrBtnMoveDn.SetFont(&tFont);
	m_ctrBtnMoveDn.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUp.SetTextColor(&tColor);
	m_ctrBtnMoveUp.SetCheckButton(false);			
	m_ctrBtnMoveUp.SetFont(&tFont);
	m_ctrBtnMoveUp.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnValueSave.SetTextColor(&tColor);
	m_ctrBtnValueSave.SetCheckButton(false);			
	m_ctrBtnValueSave.SetFont(&tFont);
	m_ctrBtnValueSave.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnSaveCancel.SetTextColor(&tColor);
	m_ctrBtnSaveCancel.SetCheckButton(false);			
	m_ctrBtnSaveCancel.SetFont(&tFont);
	m_ctrBtnSaveCancel.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDefault.SetTextColor(&tColor);
	m_ctrBtnMoveDefault.SetCheckButton(false);			
	m_ctrBtnMoveDefault.SetFont(&tFont);
	m_ctrBtnMoveDefault.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnLT01ActionStop.SetTextColor(&tColor);	m_ctrBtnLT01ActionStop.SetCheckButton(false);
	m_ctrBtnLT01ActionStop.SetFont(&tFont);	m_ctrBtnLT01ActionStop.SetRoundButtonStyle(&m_tStyle1);

}

int CLoadingTable::m_fnCreateControlFont(void)
{	
// 	GetDlgItem(IDC_EDIT_BAD_POS)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS1)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS2)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS3)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS4)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS5)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS6)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS7)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_READY_POS)->SetFont(&m_EditFont);

	//SetDlgItemInt(IDC_EDIT_Z_POS_UP, 100);
	//SetDlgItemInt(IDC_EDIT_Z_POS_DN, 100);
	return 0;
}

BOOL CLoadingTable::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);
	m_EditFont.CreatePointFont(100,"Arial");	

	m_fnCreateControlFont();

	m_fnCreateButton();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CLoadingTable::m_fnSetParameter(SLT01_POSITION* stPointer)
{
	m_st_LT01_Pos = stPointer;	
	return 0;
}

int CLoadingTable::m_fnDisplayParameter(void)
{
	SetDlgItemInt(IDC_EDIT_POS_SMALL, m_st_LT01_Pos->nGuide_Small_Pos);
	SetDlgItemInt(IDC_EDIT_POS_MEDIUM, m_st_LT01_Pos->nGuide_Mid_Pos);
	SetDlgItemInt(IDC_EDIT_POS_LARGE, m_st_LT01_Pos->nGuide_Large_Pos);
	SetDlgItemInt(IDC_EDIT_POS_READY, m_st_LT01_Pos->nGuide_Ready_Pos);
	SetDlgItemInt(IDC_EDIT_PURGE_SEC, m_st_LT01_Pos->nPurge_Time);	
	return 0;
}

void CLoadingTable::OnBnClickedBtnCancel()
{
	m_fnDisplayParameter();
}

void CLoadingTable::OnBnClickedBtnSave()
{
	CString strWriteData;

	m_st_LT01_Pos->nGuide_Small_Pos = GetDlgItemInt(IDC_EDIT_POS_SMALL);
	m_st_LT01_Pos->nGuide_Mid_Pos = GetDlgItemInt(IDC_EDIT_POS_MEDIUM);	
	m_st_LT01_Pos->nGuide_Large_Pos = GetDlgItemInt(IDC_EDIT_POS_LARGE);
	m_st_LT01_Pos->nGuide_Ready_Pos = GetDlgItemInt(IDC_EDIT_POS_READY);
	m_st_LT01_Pos->nPurge_Time = GetDlgItemInt(IDC_EDIT_PURGE_SEC);	

	strWriteData.Format("%d", m_st_LT01_Pos->nGuide_Small_Pos);
	WritePrivateProfileString(SECTION_LT01_POS_DATA, KEY_GUIDE_SMALL, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_LT01_Pos->nGuide_Mid_Pos);
	WritePrivateProfileString(SECTION_LT01_POS_DATA, KEY_GUIDE_MID, strWriteData, SYSTEM_PARAM_INI_PATH);
	
	strWriteData.Format("%d", m_st_LT01_Pos->nGuide_Large_Pos);
	WritePrivateProfileString(SECTION_LT01_POS_DATA, KEY_GUIDE_LARGE, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_LT01_Pos->nGuide_Ready_Pos);
	WritePrivateProfileString(SECTION_LT01_POS_DATA, KEY_GUIDE_READY, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_LT01_Pos->nPurge_Time);
	WritePrivateProfileString(SECTION_LT01_POS_DATA, KEY_PURGE_TIME, strWriteData, SYSTEM_PARAM_INI_PATH);
}

void CLoadingTable::OnBnClickedBtnCylFwd()
{
	m_stLT01_Mess.Reset();

	m_stLT01_Mess.nAction = m_stLT01_Mess.Act_None;
	m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;	
	m_stLT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stLT01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[LT01_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[LT01_CYL].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[LT01_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_LT01_GUIDE_FWD, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_LT01_GUIDE_FWD, nBiz_Unit_Zero
				);

			strLogMessage.Format("LT01_CYL : FWD.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("LT01_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("LT01_CYL : FWD 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}


void CLoadingTable::OnBnClickedBtnCylBwd()
{
	m_stLT01_Mess.Reset();

	m_stLT01_Mess.nAction = m_stLT01_Mess.Act_None;
	m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;	
	m_stLT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stLT01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[LT01_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[LT01_CYL].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[LT01_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_LT01_GUIDE_BWD, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_LT01_GUIDE_BWD, nBiz_Unit_Zero
				);

			strLogMessage.Format("LT01_CYL : BWD.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("LT01_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("LT01_CYL : BWD 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CLoadingTable::OnBnClickedBtnVacOn()
{
	m_stLT01_Mess.Reset();

	m_stLT01_Mess.nAction = m_stLT01_Mess.Act_None;
	m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;	
	m_stLT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stLT01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[LT01_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[LT01_VAC].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[LT01_VAC].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_LT01_Vac_ON, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_LT01_Vac_ON, nBiz_Unit_Zero
				);

			strLogMessage.Format("LT01_VAC : ON.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("LT01_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("LT01_VAC : ON 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_VAC].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CLoadingTable::OnBnClickedBtnVacOff()
{
	m_stLT01_Mess.Reset();

	m_stLT01_Mess.nAction = m_stLT01_Mess.Act_None;
	m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;	
	m_stLT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stLT01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[LT01_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[LT01_VAC].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_LT01_Vac_OFF, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_LT01_Vac_OFF, nBiz_Unit_Zero
				);

			strLogMessage.Format("LT01_VAC : OFF.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("LT01_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("LT01_VAC : OFF 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_VAC].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CLoadingTable::OnBnClickedBtnPurge()
{
	int nPurgeTime = GetDlgItemInt(IDC_EDIT_PURGE_SEC);

	m_stLT01_Mess.Reset();

	m_stLT01_Mess.nAction = m_stLT01_Mess.Act_None;
	m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;	
	m_stLT01_Mess.ActionTime = nPurgeTime;
	m_stLT01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stLT01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[LT01_PURGE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[LT01_PURGE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[LT01_PURGE].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[LT01_PURGE].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_LT01_Purge, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_LT01_Purge, nBiz_Unit_Zero
				);

			strLogMessage.Format("LT01_PURGE : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("LT01_PURGE : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("LT01_PURGE : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_PURGE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[LT01_PURGE].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CLoadingTable::OnBnClickedBtnPosMove()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nCtrPosIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS_SMALL);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS_MEDIUM);	break;	
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS_LARGE);	break;	
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_POS_READY);	break;
	}
	
	m_fnLT01PusherMove(nPosValue);
}

void CLoadingTable::OnBnClickedBtnPosDn()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN);

	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	nPosValue = nPosValue - nOffsetPos;
	m_fnLT01PusherMove(nPosValue);

	return;	
}

void CLoadingTable::OnBnClickedBtnPosUp()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP);

	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	nPosValue = nPosValue + nOffsetPos;
	m_fnLT01PusherMove(nPosValue);

	return;
}

void CLoadingTable::m_fnLT01PusherMove(int nPosValue)
{
	CString strLogMessage;

	if(m_fnLT01InterlockCheck(AXIS_LT01_BoxLoadPush, nPosValue))
		return;

	if(G_MOTION_STATUS[LT01_PUSHER].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[LT01_PUSHER].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[LT01_PUSHER].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[LT01_PUSHER].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[LT01_PUSHER].UN_CMD_COUNT++;

			m_fnLT01PositionMove(nPosValue, nBiz_Seq_LT01_BoxLoadPush, TIME_OUT_120_SECOND);

			strLogMessage.Format("LT01_PUSHER : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("LT01_PUSHER : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("LT01_PUSHER : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_PUSHER].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[LT01_PUSHER].UN_CMD_COUNT = 0;
	}
}

bool CLoadingTable::m_fnLT01InterlockCheck(int AxisNum, long newPosition)
{
	//구동 가능한 상태 이다.(fals:구동 할수 있다, true: 구동 할수없음.)
	//Interlock이 걸린것을 의미 함.
	CString strLogMessage;

	CHECK_AXIS_LT01(AxisNum);	
	switch(AxisNum)
	{
		//LT01
	case AXIS_LT01_BoxLoadPush:
		{
			int nPusherMin = GetPrivateProfileInt(Section_LT01, Key_Pusher_Min, 0, Interlock_PARAM_INI_PATH);
			int nPusherMax = GetPrivateProfileInt(Section_LT01, Key_Pusher_Max, 0, Interlock_PARAM_INI_PATH);

			if(newPosition < nPusherMin || newPosition > nPusherMax)
			{
				strLogMessage.Format("AXIS_LT01_BoxLoadPush : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
					nPusherMin, newPosition, nPusherMax );
				m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
				return true;
			}
		}break;
	default: 
		return true;
	}
	return false;
}

void CLoadingTable::m_fnLT01PositionMove(int nPosValue, int nMotionType, int nTimeOut)
{	
	m_stLT01_Mess.Reset();

	m_stLT01_Mess.nAction = m_stLT01_Mess.Act_Move;
	m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;
	m_stLT01_Mess.newPosition1 = nPosValue;
	m_stLT01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stLT01_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize,	chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete []chMsgBuf;	
}

void CLoadingTable::m_fnLT01ActionStop(int nMotionType, int nTimeOut)
{
	m_stLT01_Mess.Reset();

	m_stLT01_Mess.nAction = m_stLT01_Mess.Act_Stop;
	m_stLT01_Mess.nResult = m_stLT01_Mess.Ret_None;
	m_stLT01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stLT01_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize, chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete[]chMsgBuf;
}

void CLoadingTable::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{	
	CVS11IndexerDlg*  vpMainDlg = (CVS11IndexerDlg*)AfxGetApp()->m_pMainWnd;
	vpMainDlg->m_fnAppendList(strLog, nColorType);
}

int CLoadingTable::m_fn_LT01_Pusher_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[LT01_PUSHER].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "LT01_PUSHER : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[LT01_PUSHER].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[LT01_PUSHER].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "LT01_PUSHER : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_PUSHER].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("LT01_PUSHER : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[LT01_PUSHER].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnLT01PusherMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("LT01_PUSHER : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_PUSHER].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CLoadingTable::m_fn_LT01_GuideFwd_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[LT01_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "LT01_CYL : 가이드 전진 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

		G_MOTION_STATUS[LT01_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "LT01_CYL : 가이드 전진 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("LT01_CYL : 가이드 전진 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnCylFwd();
	}break;
	default:
	{
		strLogMessage.Format("LT01_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CLoadingTable::m_fn_LT01_GuideBwd_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[LT01_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "LT01_CYL : 가이드 후진 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

		G_MOTION_STATUS[LT01_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "LT01_CYL : 가이드 후진 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("LT01_CYL : 가이드 후진 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnCylBwd();
	}break;
	default:
	{
		strLogMessage.Format("LT01_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CLoadingTable::m_fn_LT01_VacOn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[LT01_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "LT01_VAC : Vac. ON 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

		G_MOTION_STATUS[LT01_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "LT01_VAC : Vac. ON 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("LT01_VAC : Vac. ON 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnVacOn();
	}break;
	default:
	{
		strLogMessage.Format("LT01_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CLoadingTable::m_fn_LT01_VacOff_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[LT01_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "LT01_VAC : Vac. OFF 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[LT01_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "LT01_VAC : Vac. OFF 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("LT01_VAC : Vac. OFF 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnVacOff();
	}break;
	default:
	{
		strLogMessage.Format("LT01_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CLoadingTable::m_fn_LT01_Purge_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[LT01_PURGE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "LT01_PURGE : Purge 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[LT01_PURGE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[LT01_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "LT01_PURGE : Purge 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_PURGE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("LT01_PURGE : Purge 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[LT01_PURGE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnPurge();
	}break;
	default:
	{
		strLogMessage.Format("LT01_PURGE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[LT01_PURGE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

void CLoadingTable::OnBnClickedBtnMotionStop()
{
	m_fnLT01ActionStop(nBiz_Seq_LT01_BoxLoadPush, TIME_OUT_10_SECOND);
}

void CLoadingTable::m_fnDisplaySignal()
{
	if (G_STMACHINE_STATUS.stModuleStatus.In10_Sn_LoadBoxVacuum == ON)
	{
		m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOn);
	}

	if (G_STMACHINE_STATUS.stModuleStatus.In11_Sn_LoadLeftAlignForward == ON &&
		G_STMACHINE_STATUS.stModuleStatus.In13_Sn_LoadRightAlignForward == ON)
		m_ctrBtnCylFwd.SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtnCylFwd.SetRoundButtonStyle(&m_tStyleOff);


	if (G_STMACHINE_STATUS.stModuleStatus.In12_Sn_LoadLeftAlignBackward == ON &&
		G_STMACHINE_STATUS.stModuleStatus.In14_Sn_LoadRightAlignBackward == ON)
		m_ctrBtnCylBwd.SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtnCylBwd.SetRoundButtonStyle(&m_tStyleOff);

}
