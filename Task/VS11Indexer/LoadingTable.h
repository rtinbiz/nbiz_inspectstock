#pragma once
#include "afxwin.h"


// CLoadingTable 대화 상자입니다.

class CLoadingTable : public CDialogEx
{
	DECLARE_DYNAMIC(CLoadingTable)

public:
	CLoadingTable(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLoadingTable();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_LT01 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	int m_fnCreateControlFont(void);
	afx_msg void OnBnClickedBtnSave();
	afx_msg void OnBnClickedBtnCancel();
	int m_fnSetParameter(SLT01_POSITION* stPointer);
	int m_fnDisplayParameter(void);
	void m_fnCreateButton(void);
	afx_msg void OnBnClickedBtnCylFwd();
	afx_msg void OnBnClickedBtnCylBwd();
	afx_msg void OnBnClickedBtnVacOn();
	afx_msg void OnBnClickedBtnVacOff();
	afx_msg void OnBnClickedBtnPurge();
	afx_msg void OnBnClickedBtnPosMove();
	afx_msg void OnBnClickedBtnPosDn();
	afx_msg void OnBnClickedBtnPosUp();
	void m_fnLT01PositionMove(int nPosValue, int nMotionType, int nTimeOut);
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	void m_fnLT01PusherMove(int nPosValue);

	int m_fn_LT01_Pusher_Return(VS24MotData stReturn_Mess);
	int m_fn_LT01_GuideFwd_Return(VS24MotData stReturn_Mess);
	int m_fn_LT01_GuideBwd_Return(VS24MotData stReturn_Mess);
	int m_fn_LT01_VacOn_Return(VS24MotData stReturn_Mess);
	int m_fn_LT01_VacOff_Return(VS24MotData stReturn_Mess);
	int m_fn_LT01_Purge_Return(VS24MotData stReturn_Mess);
	void m_fnLT01ActionStop(int nMotionType, int nTimeOut);
	bool m_fnLT01InterlockCheck(int AxisNum, long newPosition);
	void m_fnDisplaySignal();
public:
	//CInterServerInterface*  g_cpServerInterface;

	COLORREF	m_btBackColor;
	CFont m_EditFont;
	SLT01_POSITION* m_st_LT01_Pos;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;	
	CRoundButtonStyle	m_tStyleOn;	
	CRoundButtonStyle	m_tStyleOff;	

	CRoundButton2 m_ctrBtnCylFwd;
	CRoundButton2 m_ctrBtnCylBwd;
	CRoundButton2 m_ctrBtnVacOn;
	CRoundButton2 m_ctrBtnVacOff;
	CRoundButton2 m_ctrBtnPurge;
	CRoundButton2 m_ctrBtnMove;
	CRoundButton2 m_ctrBtnMoveDn;
	CRoundButton2 m_ctrBtnMoveUp;
	CRoundButton2 m_ctrBtnValueSave;
	CRoundButton2 m_ctrBtnSaveCancel;
	CRoundButton2 m_ctrBtnMoveDefault;
	CRoundButton2 m_ctrBtnLT01ActionStop;

	int m_nCtrPosIndex;

	VS24MotData m_stLT01_Mess;
	afx_msg void OnBnClickedBtnMotionStop();
};
