#pragma once
#include "afxwin.h"


// CBoxTable 대화 상자입니다.

class CBoxTable : public CDialogEx
{
	DECLARE_DYNAMIC(CBoxTable)

public:
	CBoxTable(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBoxTable();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_BT01 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	int m_fnCreateControlFont(void);
	int m_fnSetParameter(SBT01_POSITION* stPointer);
	int m_fnDisplayParameter(void);
	afx_msg void OnBnClickedBtnCancel();
	afx_msg void OnBnClickedBtnSave();
	void m_fnCreateButton(void);
	afx_msg void OnBnClickedBtnPosMove();
	afx_msg void OnBnClickedBtnPosDn();
	afx_msg void OnBnClickedBtnPosUp();
	void m_fnBT01PositionMove(int nPosValue, int nMotionType, int nTimeOut);
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	void m_fnBT01UpDnMove(int nPosValue);
	int m_fn_BT01_UpDn_Return(VS24MotData stReturn_Mess);
	void m_fnBT01ActionStop(int nMotionType, int nTimeOut);
	afx_msg void OnBnClickedBtnInit();
	afx_msg void OnBnClickedBtnMotionStop();
	bool m_fnBT01InterlockCheck(int AxisNum, long newPosition);

public:
	//CInterServerInterface*  g_cpServerInterface;

	COLORREF	m_btBackColor;
	CFont m_EditFont;
	
	SBT01_POSITION* m_st_BT01_Pos;
	
	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;	

	CRoundButton2 m_ctrBtnMove;
	CRoundButton2 m_ctrBtnMoveDn;
	CRoundButton2 m_ctrBtnMoveUp;
	CRoundButton2 m_ctrBtnValueSave;
	CRoundButton2 m_ctrBtnSaveCancel;
	CRoundButton2 m_ctrBtnMoveDefault;
	CRoundButton2 m_ctrBtnBT01ActionStop;

	VS24MotData m_stBT01_Mess;	
	
	int m_nTableIndex;
	
	int m_nTargetBoxPos;
	
};
