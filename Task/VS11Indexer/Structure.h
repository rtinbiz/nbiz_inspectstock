#ifndef _STRUCTURE_H_
#define _STRUCTURE_H_

#include "GlobalParam.h"

/**
@struct	SBOX_TABLE_POSITION
@brief	박스 테이블 포지션 정보
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/14  16:32
*/
typedef struct tag_BT01_Position
{
	int		nBadBoxPosition;
	int		nBox1Position;
	int		nBox2Position;
	int		nBox3Position;
	int		nBox4Position;
	int		nBox5Position;
	int		nBox6Position;
	int		nBox7Position;
	int		nReadyPosition;
	//int		nTargetPosition;
	
	tag_BT01_Position()
	{
		memset(this, 0x00, sizeof(tag_BT01_Position));
	}
}SBT01_POSITION;


/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:12
*/
typedef struct tag_TM01_Position
{
	int		nBT01_FwdPos;
	int		nLT01_BwdPos;
	int		nTM01_Ready_Pos;
	int		mTM01_BCR_Pos;
	int		nPurge_Time;
	//int		nTargetPosition;
	tag_TM01_Position()
	{
		memset(this, 0x00, sizeof(tag_TM01_Position));
	}
}STM01_POSITION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:49
*/
typedef struct tag_LT01_Position
{
	int		nGuide_Small_Pos;
	int		nGuide_Mid_Pos;	
	int		nGuide_Large_Pos;
	int		nGuide_Ready_Pos;
	int		nPurge_Time;
	//int		nTargetPosition;
	tag_LT01_Position()
	{
		memset(this, 0x00, sizeof(tag_LT01_Position));
	}
}SLT01_POSITION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:50
*/
typedef struct tag_BO01_Position
{
	int		nDrive_LT01_Small_Pos;
	int		nDrive_LT01_Mid_Pos;	
	int		nDrive_LT01_Large_Pos;
	int		nDrive_UT01_Small_Pos;
	int		nDrive_UT01_Mid_Pos;	
	int		nDrive_UT01_Large_Pos;
	int		nZ_Ready_Pos;
	int		nZ_LT01_Up_Cover_Pos;
	int		nZ_LT01_Dn_Cover_Pos;
	int		nZ_UT01_Up_Cover_Pos;
	int		nZ_UT01_Dn_Cover_Pos;
	int		nR_Pad_Up_Pos;
	int		nR_Pad_Dn_Pos;
	int		nPurge_Time;

// 	int		nDriveTarget;
// 	int		nZTarget;
// 	int		nRTarget;

	tag_BO01_Position()
	{
		memset(this, 0x00, sizeof(tag_BO01_Position));
	}
}SBO01_POSITION;


/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:49
*/
typedef struct tag_UT01_Position
{
	int		nGuide_Small_Pos;
	int		nGuide_Mid_Pos;	
	int		nGuide_Large_Pos;
	int		nGuide_Ready_Pos;
	int		nPurge_Time;
	//int		nTargetPosition;
	tag_UT01_Position()
	{
		memset(this, 0x00, sizeof(tag_UT01_Position));
	}
}SUT01_POSITION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/20  16:54
*/
typedef struct tag_TM02_Position
{
	int		nDrive_BT01_Pos;
	int		nDrive_BT01_PP_Pos;
	int		nDrive_LT01_Pos;
	int		nDrive_UT01_Pos;
	int		nDrive_UT01_PP_Pos;
	int		nDrive_TT01_Pos;
	int		nDrive_UT02_Pos;
	int		nDrive_QR_Pos;
	int		nDrive_Align_Pos;

	int		nT_BT01_Pos;
	int		nT_LT01_Pos;
	int		nT_UT01_Pos;
	int		nT_TT01_Pos;
	int		nT_UT02_Pos;
	int		nT_QR_Pos;

	int		nShift_BT01_Pos;
	int		nShift_LT01_Pos;
	int		nShift_UT01_Pos;
	int		nShift_TT01_Pos;
	int		nShift_UT02_Pos;
	int		nShift_QR_Pos;

	int		nStickZ_Ready_Pos;
	int		nStickZ_BT01_Pos;
	int		nStickZ_LT01_Pos;
	int		nStickZ_UT01_Pos;
	int		nStickZ_TT01_Pos;
	int		nStickZ_UT02_Pos;
	int		nStickZ_QR_Pos;
	int		nStickZ_Align_Pos;

	int		nPaperZ_BT01_Pos;
	int		nPaperZ_LT01_Pos;
	int		nPaperZ_UT01_Pos;

	int		nStickPurge_Time;
	int		nPaperPurge_Time;

	int		nLightValue1_1;
// 	int		nDriveTarget;
// 	int		nTTarget;
// 	int		nZTarget;
// 	int		nShiftTarget;

	tag_TM02_Position()
	{
		memset(this, 0x00, sizeof(tag_TM02_Position));
	}
}STM02_POSITION;


/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/27  15:26
*/
typedef struct tag_TT01_Position
{
	int		nDrive_Ready_Pos;
	int		nDrive_Turn_Pos;
	int		nDrive_AM01_Pos;	
	
	int		nRotate_SideUp_Pos;
	int		nRotate_SideDn_Pos;
	
	int		nT_Ready_Pos;

	int		nUpPurge_Time;
	int		nDnPurge_Time;

// 	int		nDriveTarget;
// 	int		nTTarget;

	tag_TT01_Position()
	{
		memset(this, 0x00, sizeof(tag_TT01_Position));
	}
}STT01_POSITION;

/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/27  15:30
*/
typedef struct tag_UT02_Position
{
	int		nPurge_Time;

	tag_UT02_Position()
	{
		memset(this, 0x00, sizeof(tag_UT02_Position));
	}
}SUT02_POSITION;

typedef struct tag_Stick_Info
{
	char chBoxBarcode[MAX_BARCODE];
	char chStickQRcode[MAX_QRCODE];
	int	nLoadOffset;
	tag_Stick_Info()
	{		
		memset(this, 0x00, sizeof(tag_Stick_Info));
	}
}SSTICK_INFO;

typedef union AIR_SELECTION_TAG
{
	UINT nAirSelection;
	struct
	{
		UINT m_btStep1 : 1;
		UINT m_btStep2 : 1;
		UINT m_btStep3 : 1;
		UINT m_btStep4 : 1;
		UINT m_btStep5 : 1;
		UINT m_btStep6 : 1;
		UINT m_btStep7 : 1;
		UINT m_btStep8 : 1;
		UINT m_btStep9 : 1;
		UINT m_btStep10 : 1;
		UINT m_btStep11 : 1;
		UINT m_btStep12 : 1;
		UINT m_btStep13 : 1;
		UINT m_btStep14 : 1;
		UINT m_btStep15 : 1;
		UINT m_btStep16 : 1;
		UINT m_btStep32 : 16;
	}nAirStep;

	AIR_SELECTION_TAG()
	{
		memset(this, 0x00, sizeof(AIR_SELECTION_TAG));
	}

}AIR_SELECTION;

static const int nTypeNameLength = 40;
typedef struct tag_TM02_Recipe
{
	
	char m_chTypeName[nTypeNameLength];
	int		nBoxType; //1:Small, 2:Medium, 3:Large
	int		nStickSize; //mm
	int		nAM01LoadOffset;

	int		nLT01_Drive_Align;
	int		nLT01_Z_Align;
	int		nLT01_Drive_PP_Get;
	int		nLT01_Drive_ST_Get;
	int		nLT01_Shift_Get;
	int		nLT01_Tilt_Get;
	int		nLT01_Z_PP_Get;
	int		nLT01_Z_ST_Get;
	int		nLT01_PP_Purge_Get;
	AIR_SELECTION		stLT01_Vac_PP_Get;
	AIR_SELECTION		stLT01_Vac_ST_Get;

	int		nZ_Ready_Pos;
	int		nQR_Drive_Pos;
	int		nQR_Z_Pos;

	int		nTT01_Dirve_Put;
	int		nTT01_Shift_Put;
	int		nTT01_Tilt_Put;
	int		nTT01_Z_ST_Put;
	int		nTT01_ST_Purge_Put;
	
	int		nUT02_Drive_Get;
	int		nUT02_Shift_Get;
	int		nUT02_Tilt_Get;
	int		nUT02_Z_ST_Get;
	AIR_SELECTION		stUT02_Vac_Get;


	int		nUT01_Drive_PP_Put;
	int		nUT01_Drive_ST_Put;
	int		nUT01_Shift_Put;
	int		nUT01_Tilt_Put;
	int		nUT01_Z_PP_Put;
	int		nUT01_Z_ST_Put;
	int		nUT01_Purge_Put;

	int		nBT01_Drive_PP_Put;
	int		nBT01_Drive_ST_Put;
	int		nBT01_Shift_Put;
	int		nBT01_Tilt_Put;
	int		nBT01_Z_PP_Put;
	int		nBT01_Z_ST_Put;
	int		nBT01_Purge_Put;

	AIR_SELECTION		stTT01_Vac_Table_Get;
	AIR_SELECTION		stUT02_Vac_Table_Get;

	tag_TM02_Recipe()
	{
		memset(this, 0x00, sizeof(tag_TM02_Recipe));
	}
}STM02_RECIPE;


/**
@struct	
@brief	
@remark	
-	
@author	고정진(JUNGJIN.GO)
@date	2014/10/27  23:25
*/
struct STHREAD_PARAM
{
	void*			cObjectPointer1;
	void*			cObjectPointer2;
	void*			cObjectPointer3;
	void*			cObjectPointer4;
	int				nScanInterval;
	BOOL			bThreadFlag;
	STHREAD_PARAM()
	{
		cObjectPointer1 = nullptr;
		cObjectPointer2 = nullptr;
		cObjectPointer3 = nullptr;
		cObjectPointer4 = nullptr;
		nScanInterval = 500;
		bThreadFlag = TRUE;
	}
};

#endif