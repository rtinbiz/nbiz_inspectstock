
// VS11IndexerDlg.h : 헤더 파일
//

#pragma once
#include "DisplayDlg1.h"
#include "DisplayDlg2.h"
#include "BoxTable.h"
#include "BoxTransper.h"
#include "BoxOpener.h"
#include "LoadingTable.h"
#include "UnloadingTable.h"
#include "StickTransper.h"
#include "MainControl.h"
#include "TurnTable.h"
#include "EmissionTable.h"
#include "RecipeDefine.h"
#include "BoxStatus.h"


#include "afxwin.h"

#include "DemoSequence.h"
#include "BoxControl.h"
#include "PickerControl.h"
#include "CameraControl.h"

// CVS11IndexerDlg 대화 상자
class CVS11IndexerDlg : public CDialogEx, public CProcInitial//VS64 Interface
{
// 생성입니다.
public:
	CVS11IndexerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~CVS11IndexerDlg();

	CDisplayDlg1*	m_cDisplayWindow1;
	CDisplayDlg2*	m_cDisplayWindow2;
	CBoxTable*		m_cBoxTable;
	CBoxTransper* m_cBoxTransper;
	CBoxOpener*	m_cBoxOpener;
	CLoadingTable* m_cLoadingTable;
	CUnloadingTable* m_cUnloadingTable;
	CStickTransper* m_cStickTransper;
	CTurnTable*		m_cTurnTable;
	CEmissionTable*	m_cEmissionTable;
	CMainControl*		m_cMainControl;
	CRecipeDefine*	m_cRecipeDefine;
	CDemoSequence* m_cDemoSeq;
	CBoxControl*		m_cBoxControl;
	CPickerControl*	m_cPickerControl;
	CBoxStatus*		m_pBoxStatus;


	CInterServerInterface  m_cpServerInterface;
	CCameraControl  m_cCameraControl;


// 대화 상자 데이터입니다.
	enum { IDD = IDD_VS11INDEXER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnNcDestroy();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	
	void m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...);
	void Sys_fnInitVS64Interface();
	int				Sys_fnAnalyzeMsg(CMDMSG* cmdMsg);//Message Process Function.
	LRESULT		Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam);
	LRESULT		Sys_fnBarcodeCallback(WPARAM wParam, LPARAM lParam);
	LRESULT		Sys_fnPaperDetectCallback(WPARAM wParam, LPARAM lParam);
	LRESULT		Sys_fnStickDetectCallback(WPARAM wParam, LPARAM lParam);
	LRESULT		Sys_fnStickAlignCallback(WPARAM wParam, LPARAM lParam);

	static unsigned int __stdcall THREAD_MAIN_PROCESS(LPVOID pParam);

	static unsigned int __stdcall THREAD_MAIN_SEQUENCE(LPVOID pParam);

	static unsigned int __stdcall THREAD_BOX_SEQUENCE(LPVOID pParam);

	static unsigned int __stdcall THREAD_PICKER_SEQUENCE(LPVOID pParam);

	static unsigned int __stdcall THREAD_TURN_TABLE_SEQUENCE(LPVOID pParam);


	static unsigned int __stdcall THREAD_DEMO_SEQUENCE(LPVOID pParam);
			
	void m_fnDrawBoxTable(void);
	void m_fnDrawBoxTable(BOX_LIST elBoxList, CvPoint Start, CvPoint End);
	void m_fnDrawLoadingTable(void);
	void m_fnDrawUnLoadingTable(void);
	void m_fnDrawBadBoxTable(void);
	void m_fnDrawTurnTable(void);
	void m_fnDrawEmissionTable(void);
	void m_fnDrawInspectTable(void);

	void m_fnLoadingTableObjectInit(void);
	void m_fnUnLoadingTableObjectInit(void);
	void m_fnBadTableObjectInit(void);
	void m_fnImageMerge(void);	
	void m_fnDrawImageView();
	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	void m_fnAppendList(CString strLog, LOG_INFO nColorType);
	afx_msg void OnBnClickedOk();
	void m_fnCreateButton(void);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	int m_fnReadSystemFile(void); 	

	afx_msg void OnBnClickedBtnCallPanel();
	void m_fnAllStatusRead();
	void m_fnAllStatusRelease(VS24MotData stReturn_Mess);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void m_fnTaskStatusChage(int nTaskNumber, TASK_State elTempStatus);
	afx_msg void OnBnClickedBtnLotStart();
	int m_fnProcessClear();
	int m_fnThreadCreator();
	int m_fnSetWindowPosition();
	int m_fnSetParameter();
	void m_fnBoxTableRelease();
	void m_fnBoxStatusChange(BOX_LIST elBoxList, BOX_STATUS elBoxStatus);
	
	void m_fnBT01PositionRelease();
	void m_fnTM01PositionRelease();	
	void m_fnLT01PositionRelease();	
	void m_fnBO01PositionRelease();	
	void m_fnUT01PositionRelease();
	void m_fnTM02PositionRelease();
	void m_fnTT01PositionRelease();
	int m_fnGetRecipeList();
	int m_fnCreateControlFont(void);
	afx_msg void OnBnClickedBtnRecipeSet();
	afx_msg void OnBnClickedBtnProcReset();	
	afx_msg void OnBnClickedBtnProcDoor();
	void m_fnFrontDoorLock();
	void m_fnFrontDoorUnLock();
	int m_fnFrontDoorLock_Return(VS24MotData stReturn_Mess);
	int m_fnFrontDoorUnLock_Return(VS24MotData stReturn_Mess);

	void m_fnSideDoorLock();
	void m_fnSideDoorUnLock();
	int m_fnSideDoorLock_Return(VS24MotData stReturn_Mess);
	int m_fnSideDoorUnLock_Return(VS24MotData stReturn_Mess);

	int m_fnGetCurrentObjectIndex();

public:
	IplImage*	m_iplBackGround;
	IplImage*	m_iplBackGroundOrg;

	COLORREF	m_btBackColor;
	HDC			m_HDCView;
	CRect			m_RectiewArea;
	
	CXListBox m_ctrListLogView;
	
	CRoundButton2 m_ctrBtnExit;	
	CRoundButton2 m_ctrBtnLotStart;
	CRoundButton2 m_ctrBtnRecipeSet;
	CRoundButton2 m_ctrBtnProcReset;
	CRoundButton2 m_ctrBtnFrontDoor;
	CRoundButton2 m_ctrBtnLotRetry;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;
	CRoundButtonStyle	m_tStyle2;
	CRoundButtonStyle	m_tStyle3;
	
	//UI 버튼 인덱스
	CRoundButton2 m_ctrBtnCallPanel[CTR_COUNT];	

	//각 모듈 포지션 테이터.
	SBT01_POSITION  m_st_BT01_Position;
	STM01_POSITION	m_st_TM01_Position;
	SLT01_POSITION	m_st_LT01_Position;
	SBO01_POSITION	m_st_BO01_Position;
	SUT01_POSITION	m_st_UT01_Position;
	STM02_POSITION	m_st_TM02_Position;
	STT01_POSITION	m_st_TT01_Position;
	SUT02_POSITION	m_st_UT02_Position;
	STM02_RECIPE	m_st_TM02_Recipe;
	
	STHREAD_PARAM	m_sThreadParam;
	
	//Task Status 갱신 UI Control
	CColorStaticST m_ctrStaticTaskInfo;

	CFont		m_TaskStateFont;
	COLORREF	m_TaskStateColor;

	CComboBox m_ctrCboRecipeList;
		
	//각 테이블 스틱 간지 상태.
// 	SBOX_STICK_STATUS G_STLT01_OBJECT[MAX_OBJECT_COUNT];
// 	SBOX_STICK_STATUS G_STUT01_OBJECT[MAX_OBJECT_COUNT];
// 	SBOX_STICK_STATUS G_STBT01_OBJECT[MAX_OBJECT_COUNT];
	
	int m_nMasterStatusCount;	//마스터 상태 갱신에 대한 시간 체크
	int m_nMotionStatusCount;	//모션타스크 상태 갱신에 대한 시간 체크
	int	m_nAllStatusCount;		//전체 상태 갱신에 대한 시간 체크

	//메인 시퀀스 스텝.
	EL_MAIN_SEQ	m_elMainSequence;

	EL_SUB_SEQ m_elBoxSequence;
	EL_SUB_SEQ m_elPickerSequence;
	EL_SUB_SEQ m_elTurnTableSequence;
	EL_DEMO_SEQ m_elDemoSequence;

	

	HWND 		mUpdateWindHandle1;
	HWND 		mUpdateWindHandle2;

	CFont m_EditFont;
	CFont m_EditFont2;

	afx_msg void OnBnClickedBtnDemoPaper();
	afx_msg void OnBnClickedBtnDemoStick();	
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();	
		
	int M_N_WORK_STICK_RESULT;
	VS24MotData m_stMD01_Mess;
	
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	double m_fnGetLengthData(IplImage *gray_image, CvPoint nTemp1, CvPoint nTemp2, int nType);
	double m_fnGetLengthDataLongX(IplImage *gray_image, CvPoint nTemp1, CvPoint nTemp2, int nType);
	double m_fnGetLengthDataLongY(IplImage *gray_image, CvPoint nTemp1, CvPoint nTemp2, int nType);
	afx_msg void OnBnClickedBtnLotRetry();

};

extern CVS11IndexerDlg*				g_cpMainDlg;
extern CInterServerInterface*		g_cpServerInterface;