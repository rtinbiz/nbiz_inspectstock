
// VS11IndexerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "VS11IndexerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CVS11IndexerDlg*			g_cpMainDlg;
CInterServerInterface*		g_cpServerInterface;

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();
// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CVS11IndexerDlg 대화 상자
CVS11IndexerDlg::CVS11IndexerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVS11IndexerDlg::IDD, pParent)
	, m_nMasterStatusCount(0)
	, m_nMotionStatusCount(0)
	, m_nAllStatusCount(0)
	, M_N_WORK_STICK_RESULT(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_iplBackGroundOrg = nullptr;
	m_iplBackGround = nullptr;
	
}
CVS11IndexerDlg::~CVS11IndexerDlg()
{
	if(m_iplBackGround != nullptr)
	{
		cvReleaseImage(&m_iplBackGroundOrg);
		cvReleaseImage(&m_iplBackGround);
	}
	m_iplBackGroundOrg = nullptr;
	m_iplBackGround = nullptr;

	G_VS10TASK_STATE = TASK_STATE_NONE;
	G_VS11TASK_STATE = TASK_STATE_NONE;
	G_VS24TASK_STATE = TASK_STATE_NONE;


	
	m_elMainSequence = MAIN_SEQUENCE_IDLE;
	m_elBoxSequence = SUB_SEQUENCE_IDLE;
	m_elPickerSequence = SUB_SEQUENCE_IDLE;
	m_elTurnTableSequence = SUB_SEQUENCE_IDLE;


	m_elDemoSequence = DEMO_SEQUENCE_IDLE;
}
void CVS11IndexerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG_VIEW, m_ctrListLogView);
	DDX_Control(pDX, IDOK, m_ctrBtnExit);
	DDX_Control(pDX, IDC_BTN_CALL_CTR, m_ctrBtnCallPanel[CTR_MAIN]);
	DDX_Control(pDX, IDC_BTN_CALL_BT01, m_ctrBtnCallPanel[CTR_BT01]);
	DDX_Control(pDX, IDC_BTN_CALL_TM01, m_ctrBtnCallPanel[CTR_TM01]);
	DDX_Control(pDX, IDC_BTN_CALL_LT01, m_ctrBtnCallPanel[CTR_LT01]);
	DDX_Control(pDX, IDC_BTN_CALL_BO01, m_ctrBtnCallPanel[CTR_BO01]);
	DDX_Control(pDX, IDC_BTN_CALL_UT01, m_ctrBtnCallPanel[CTR_UT01]);
	DDX_Control(pDX, IDC_BTN_CALL_TM02, m_ctrBtnCallPanel[CTR_TM02]);
	DDX_Control(pDX, IDC_BTN_CALL_TT01, m_ctrBtnCallPanel[CTR_TT01]);
	DDX_Control(pDX, IDC_BTN_CALL_UT02, m_ctrBtnCallPanel[CTR_UT02]);
	DDX_Control(pDX, IDC_BTN_CALL_RECIPE, m_ctrBtnCallPanel[CTR_RECIPE]);
	DDX_Control(pDX, IDC_BTN_CALL_CONTROL, m_ctrBtnCallPanel[CTR_CONTROL]);
	DDX_Control(pDX, IDC_ST_TASK_STATE, m_ctrStaticTaskInfo);
	DDX_Control(pDX, IDC_BTN_LOT_START, m_ctrBtnLotStart);
	DDX_Control(pDX, IDC_COMBO_RECIPE, m_ctrCboRecipeList);
	DDX_Control(pDX, IDC_BTN_RECIPE_SET, m_ctrBtnRecipeSet);
	DDX_Control(pDX, IDC_BTN_PROC_RESET, m_ctrBtnProcReset);
	DDX_Control(pDX, IDC_BTN_PROC_DOOR, m_ctrBtnFrontDoor);
	DDX_Control(pDX, IDC_BTN_LOT_RETRY, m_ctrBtnLotRetry);
}

BEGIN_MESSAGE_MAP(CVS11IndexerDlg, CDialogEx)
	ON_MESSAGE(WM_VS64USER_CALLBACK, &CVS11IndexerDlg::Sys_fnMessageCallback)//VS64 Interface
	ON_MESSAGE(WM_USER_ACC_CODE_UPDATE, &CVS11IndexerDlg::Sys_fnBarcodeCallback)//BCR
	ON_MESSAGE(WM_USER_ACC_PAPER_DETECT, &CVS11IndexerDlg::Sys_fnPaperDetectCallback)//CAMERA
	ON_MESSAGE(WM_USER_ACC_STICK_DETECT, &CVS11IndexerDlg::Sys_fnStickDetectCallback)//CAMERA
	ON_MESSAGE(WM_USER_ACC_STICK_ALIGN, &CVS11IndexerDlg::Sys_fnStickAlignCallback)//CAMERA
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CTLCOLOR()
	ON_WM_NCDESTROY()
	ON_BN_CLICKED(IDOK, &CVS11IndexerDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_CALL_CTR, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_BT01, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_TM01, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_LT01, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_BO01, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_UT01, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_TM02, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_TT01, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_UT02, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_RECIPE, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_BN_CLICKED(IDC_BTN_CALL_CONTROL, &CVS11IndexerDlg::OnBnClickedBtnCallPanel)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_DEMO_PAPER, &CVS11IndexerDlg::OnBnClickedBtnDemoPaper)
	ON_BN_CLICKED(IDC_BTN_DEMO_STICK, &CVS11IndexerDlg::OnBnClickedBtnDemoStick)
	ON_BN_CLICKED(IDC_BTN_LOT_START, &CVS11IndexerDlg::OnBnClickedBtnLotStart)
	ON_BN_CLICKED(IDC_BUTTON1, &CVS11IndexerDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CVS11IndexerDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BTN_RECIPE_SET, &CVS11IndexerDlg::OnBnClickedBtnRecipeSet)
	ON_BN_CLICKED(IDC_BTN_PROC_RESET, &CVS11IndexerDlg::OnBnClickedBtnProcReset)
	ON_BN_CLICKED(IDC_BTN_PROC_DOOR, &CVS11IndexerDlg::OnBnClickedBtnProcDoor)
	ON_WM_LBUTTONDBLCLK()
	ON_BN_CLICKED(IDC_BTN_LOT_RETRY, &CVS11IndexerDlg::OnBnClickedBtnLotRetry)
END_MESSAGE_MAP()


// CVS11IndexerDlg 메시지 처리기

BOOL CVS11IndexerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.
		
	m_btBackColor = RGB(65,65,65);

	m_cDisplayWindow1	= new CDisplayDlg1(this);
	m_cDisplayWindow2	= new CDisplayDlg2(this);
	m_cBoxTable			= new CBoxTable(this);
	m_cBoxTransper		= new CBoxTransper(this);
	m_cBoxOpener		= new CBoxOpener(this);
	m_cLoadingTable		= new CLoadingTable(this);
	m_cUnloadingTable	= new CUnloadingTable(this);
	m_cStickTransper	= new CStickTransper(this);
	m_cTurnTable		= new CTurnTable(this);
	m_cEmissionTable	= new CEmissionTable(this);
	m_cMainControl		= new CMainControl(this);
	m_cRecipeDefine		= new CRecipeDefine(this);
	m_cDemoSeq			= new CDemoSequence();
	m_cBoxControl		= new CBoxControl();
	m_cPickerControl	= new CPickerControl();
	m_pBoxStatus		= new CBoxStatus();
	//////////////////////////////////////////////////////////////////////////
	//1) Server Interface Connection.
	
	Sys_fnInitVS64Interface();

	CString NewWindowsName;
	NewWindowsName.Format("VS64 - Indexer Task(No.%02d)", m_ProcInitVal.m_nTaskNum);
	SetWindowText(NewWindowsName);

	m_fnSetWindowPosition();

	m_fnProcessClear();

	m_fnCreateButton();

	m_fnReadSystemFile();

	m_fnGetRecipeList();

	m_cRecipeDefine->m_fnGetRecipeList();

	m_fnSetParameter();
	
	m_EditFont.CreatePointFont(200, "Arial");
	m_EditFont2.CreatePointFont(160, "Arial");

	SetDlgItemText(IDC_COMBO_RECIPE, "Recipe Select");

	m_fnCreateControlFont();

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	GetDlgItem(IDC_ST_IMG_VIEW)->GetClientRect(&m_RectiewArea);
	m_RectiewArea.left	= m_RectiewArea.left + 50;
	m_RectiewArea.right = m_RectiewArea.right - 50;

	m_HDCView			=  GetDlgItem(IDC_ST_IMG_VIEW)->GetDC()->m_hAttribDC;		
	m_iplBackGround		= cvLoadImage(IMAGE_BACK_GROUND);
	m_iplBackGroundOrg	= cvCloneImage(m_iplBackGround);
	
	m_fnThreadCreator();

	G_VS11TASK_STATE = TASK_STATE_IDLE;
	in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
	//SetTimer(STATUS_DISP_TIMER, 3000, 0);
	

	G_AUTO_MODE_FLAG = FALSE;

// 	m_ctrStaticTaskInfo.SetFont(&m_TaskStateFont);
// 	m_ctrStaticTaskInfo.SetTextColor(RGB(0, 0, 0));
// 	m_ctrStaticTaskInfo.SetBkColor(m_TaskStateColor);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

int CVS11IndexerDlg::m_fnProcessClear()
{
	G_VS10TASK_STATE = TASK_STATE_NONE;
	G_VS11TASK_STATE = TASK_STATE_IDLE;
	in_fn_TaskStateChange(this, &m_ctrStaticTaskInfo, G_VS11TASK_STATE);
	G_VS24TASK_STATE = TASK_STATE_NONE;

	G_AUTO_MODE_FLAG = FALSE;

	m_elMainSequence = MAIN_SEQUENCE_IDLE;
	m_elBoxSequence = SUB_SEQUENCE_IDLE;
	m_elPickerSequence = SUB_SEQUENCE_IDLE;
	m_elTurnTableSequence = SUB_SEQUENCE_IDLE;


	m_elDemoSequence = DEMO_SEQUENCE_IDLE;	

	m_cBoxControl->m_elBoxLoadStep = BL_MAIN_SEQUENCE_IDLE;
	m_cBoxControl->m_elCoverOpenStep = CO_MAIN_SEQUENCE_IDLE;
	m_cBoxControl->m_elCoverCloseStep = CC_MAIN_SEQUENCE_IDLE;
	m_cBoxControl->m_elBoxMoveToLT01Step = BML_MAIN_SEQUENCE_IDLE;
	m_cBoxControl->m_elBoxUnloadingStep = BU_MAIN_SEQUENCE_IDLE;

	m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elPPMove_TM02_UT01 = PPU_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elPPMove_TM02_BT01 = PPB_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elSTMove_LT01_TM02 = STLT_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elSTMove_TM02_TT01 = STTT_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elSTMove_AM01_UT02 = STAU_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elSTMove_UT02_TM02 = STUT_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elSTMove_TM02_UT01 = STTU_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elSTMove_TM02_BT01 = STTB_MAIN_SEQUENCE_IDLE;
	m_cPickerControl->m_elSTMove_TT01_UT02 = STU_MAIN_SEQUENCE_IDLE;

	for (int nStep = 0; nStep < MOTION_COUNT; nStep++)
	{
		G_MOTION_STATUS[nStep].B_MOTION_STATUS = IDLE;
		G_MOTION_STATUS[nStep].B_COMMAND_STATUS = READY;
		G_MOTION_STATUS[nStep].UN_CMD_COUNT = 0;
		G_MOTION_STATUS[nStep].UN_TIME_OUT_VAL = 0;
	}

	for (int bl = (int)BAD_BOX; bl < (int)BOX_COUNT; bl++)
	{
		G_BOX_STATUS[bl].elBoxSize = BOX_SIZE_NONE;
		G_BOX_STATUS[bl].elBoxStatus = BOX_EMPTY;
	}	
	
	for (int bl = (int)BT01_SAFE_POS; bl < (int)LOCK_COUNT; bl++)
	{
		G_SAFE_ZONE[bl] = UNSAFE;
	}	
		

	for (int bl = (int)SUB_SEQUENCE_IDLE; bl < (int)SUB_SEQUENCE_COMP; bl++)
	{
		G_EL_SUB_SEQ_STATUS[bl] = SEQ_IDLE;
	}
		

	M_N_WORK_STICK_RESULT = NG;

	G_RECIPE_SET = FALSE;

	m_cPickerControl->m_bTurnTablePositionLock = FREE;
	m_cPickerControl->m_bSingleStickRun = FALSE;
	m_cPickerControl->m_bLastObject = FALSE;

	ZeroMemory(m_st_TM02_Recipe.m_chTypeName, sizeof(STM02_RECIPE));

	return 0;
}

int CVS11IndexerDlg::m_fnCreateControlFont(void)
{	
	GetDlgItem(IDC_COMBO_RECIPE)->SetFont(&m_EditFont2);
	return 0;
}

int CVS11IndexerDlg::m_fnThreadCreator()
{
	g_cpMainDlg = this;
	g_cpServerInterface = &m_cpServerInterface;

	m_sThreadParam.cObjectPointer1 = this;
	m_sThreadParam.cObjectPointer2 = &m_cpServerInterface;
	m_sThreadParam.nScanInterval = THREAD_INTERVAL;

	HANDLE					h_ACCHandle;
	h_ACCHandle = (HANDLE)_beginthreadex(NULL, 0, this->THREAD_MAIN_PROCESS, (void*)&m_sThreadParam, 0, NULL);
	SetThreadPriority(h_ACCHandle, THREAD_PRIORITY_HIGHEST);
	CloseHandle(h_ACCHandle);

	h_ACCHandle = (HANDLE)_beginthreadex(NULL, 0, this->THREAD_MAIN_SEQUENCE, (void*)&m_sThreadParam, 0, NULL);
	SetThreadPriority(h_ACCHandle, THREAD_PRIORITY_HIGHEST);
	CloseHandle(h_ACCHandle);
	
	h_ACCHandle = (HANDLE)_beginthreadex(NULL, 0, this->THREAD_BOX_SEQUENCE, (void*)&m_sThreadParam, 0, NULL);
	SetThreadPriority(h_ACCHandle, THREAD_PRIORITY_HIGHEST);
	CloseHandle(h_ACCHandle);

	h_ACCHandle = (HANDLE)_beginthreadex(NULL, 0, this->THREAD_PICKER_SEQUENCE, (void*)&m_sThreadParam, 0, NULL);
	SetThreadPriority(h_ACCHandle, THREAD_PRIORITY_HIGHEST);
	CloseHandle(h_ACCHandle);

	h_ACCHandle = (HANDLE)_beginthreadex(NULL, 0, this->THREAD_TURN_TABLE_SEQUENCE, (void*)&m_sThreadParam, 0, NULL);
	SetThreadPriority(h_ACCHandle, THREAD_PRIORITY_HIGHEST);
	CloseHandle(h_ACCHandle);


// 	h_ACCHandle = (HANDLE)_beginthreadex(NULL, 0, this->THREAD_DEMO_SEQUENCE, (void*)&m_sThreadParam, 0, NULL);
// 	SetThreadPriority(h_ACCHandle, THREAD_PRIORITY_HIGHEST);
// 	CloseHandle(h_ACCHandle);
	
	mUpdateWindHandle1 = GetDlgItem(IDC_STATIC_CAM1)->m_hWnd;
	mUpdateWindHandle2 = GetDlgItem(IDC_STATIC_CAM2)->m_hWnd;

	m_cCameraControl.m_fnSetThreadParam(mUpdateWindHandle1, mUpdateWindHandle2, this->m_hWnd);
	m_cCameraControl.m_fnThreadCreator();

	return 0;
}

int CVS11IndexerDlg::m_fnSetWindowPosition()
{
	this->MoveWindow(0, -1, 1918, 1080, TRUE);

	m_cDisplayWindow1->Create(IDD_DIALOG_RC1);
	m_cDisplayWindow1->MoveWindow(0, 0, 1022, 410, TRUE);
	m_cDisplayWindow1->ShowWindow(SW_SHOW);

	m_cDisplayWindow2->Create(IDD_DIALOG_RC2);
	m_cDisplayWindow2->MoveWindow(1023, 0, 1090, 410, TRUE);
	m_cDisplayWindow2->ShowWindow(SW_SHOW);

	m_cBoxTable->Create(IDD_DIALOG_BT01);
	m_cBoxTable->MoveWindow(700, 438, 940, 607, TRUE);
	m_cBoxTable->ShowWindow(SW_HIDE);

	m_cBoxTransper->Create(IDD_DIALOG_TM01);
	m_cBoxTransper->MoveWindow(700, 438, 940, 607, TRUE);
	m_cBoxTransper->ShowWindow(SW_HIDE);

	m_cBoxOpener->Create(IDD_DIALOG_BO01);
	m_cBoxOpener->MoveWindow(700, 438, 940, 607, TRUE);
	m_cBoxOpener->ShowWindow(SW_HIDE);

	m_cLoadingTable->Create(IDD_DIALOG_LT01);
	m_cLoadingTable->MoveWindow(700, 438, 940, 607, TRUE);
	m_cLoadingTable->ShowWindow(SW_HIDE);

	m_cUnloadingTable->Create(IDD_DIALOG_UT01);
	m_cUnloadingTable->MoveWindow(700, 438, 940, 607, TRUE);
	m_cUnloadingTable->ShowWindow(SW_HIDE);

	m_cStickTransper->Create(IDD_DIALOG_TM02);
	m_cStickTransper->MoveWindow(700, 438, 940, 607, TRUE);
	m_cStickTransper->ShowWindow(SW_HIDE);

	m_cTurnTable->Create(IDD_DIALOG_TT01);
	m_cTurnTable->MoveWindow(700, 438, 940, 607, TRUE);
	m_cTurnTable->ShowWindow(SW_HIDE);

	m_cEmissionTable->Create(IDD_DIALOG_UT02);
	m_cEmissionTable->MoveWindow(700, 438, 940, 607, TRUE);
	m_cEmissionTable->ShowWindow(SW_HIDE);

	m_cRecipeDefine->Create(IDD_DIALOG_RECIPE);
	m_cRecipeDefine->MoveWindow(700, 438, 940, 607, TRUE);
	m_cRecipeDefine->ShowWindow(SW_HIDE);

	m_cMainControl->Create(IDD_DIALOG_CTR);
	//m_cMainControl->MoveWindow(1536, 438, 368, 530, TRUE);
	m_cMainControl->MoveWindow(700, 438, 940, 607, TRUE);
	m_cMainControl->ShowWindow(SW_HIDE);

	m_pBoxStatus->Create(IDD_DIALOG_BOX_STATUS);
	m_pBoxStatus->MoveWindow(10, 438, 1900, 607, TRUE);
	m_pBoxStatus->ShowWindow(SW_HIDE);

	return 0;
}

int CVS11IndexerDlg::m_fnSetParameter()
{
	m_cBoxTable->m_fnSetParameter(&m_st_BT01_Position);
	m_cBoxTable->m_fnDisplayParameter();

	m_cBoxTransper->m_fnSetParameter(&m_st_TM01_Position);
	m_cBoxTransper->m_fnDisplayParameter();

	m_cLoadingTable->m_fnSetParameter(&m_st_LT01_Position);
	m_cLoadingTable->m_fnDisplayParameter();

	m_cBoxOpener->m_fnSetParameter(&m_st_BO01_Position);
	m_cBoxOpener->m_fnDisplayParameter();

	m_cUnloadingTable->m_fnSetParameter(&m_st_UT01_Position);
	m_cUnloadingTable->m_fnDisplayParameter();

	m_cStickTransper->m_fnSetParameter(&m_st_TM02_Position);
	m_cStickTransper->m_fnDisplayParameter();

	m_cTurnTable->m_fnSetParameter(&m_st_TT01_Position);
	m_cTurnTable->m_fnDisplayParameter();

	m_cEmissionTable->m_fnSetParameter(&m_st_UT02_Position);
	m_cEmissionTable->m_fnDisplayParameter();	

	m_cDemoSeq->m_fnSetParameter(&m_st_BT01_Position);

	m_cBoxControl->m_fnSetParameter(&m_st_BT01_Position, &m_st_TM01_Position, &m_st_LT01_Position, &m_st_BO01_Position, &m_st_UT01_Position);
	
	m_cPickerControl->m_fnSetParameter(&m_st_TM02_Recipe, &m_st_BO01_Position, &m_st_TT01_Position);

	m_cRecipeDefine->m_fnSetParameter(&m_st_TM02_Recipe);
	
	return 0;
}

void CVS11IndexerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVS11IndexerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		m_fnDrawImageView();
		CDialogEx::OnPaint();
	}
}

void CVS11IndexerDlg::m_fnDrawImageView()
{
	m_fnImageMerge();
	m_fnDrawBoxTable();
	m_fnDrawLoadingTable();
	m_fnDrawUnLoadingTable();
	m_fnDrawBadBoxTable();
	m_fnDrawTurnTable();
	m_fnDrawEmissionTable();
	m_fnDrawInspectTable();

	if(m_iplBackGround != nullptr)
	{
		
		CvvImage	ViewImage;
		ViewImage.Create(m_iplBackGround->width, m_iplBackGround->height, ((IPL_DEPTH_8U & 255)*3) );
		ViewImage.CopyOf(m_iplBackGround);
		ViewImage.DrawToHDC(m_HDCView, m_RectiewArea);
		ViewImage.Destroy();
	}
}


void CVS11IndexerDlg::m_fnDrawBoxTable(BOX_LIST elBoxList, CvPoint Start, CvPoint End)
{
	switch (G_BOX_STATUS[elBoxList].elBoxStatus)
	{
	case BOX_EMPTY:		
		break;
	case BOX_NORMAL:
		cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);
		break;
	case BOX_READY:
		cvRectangle(m_iplBackGround, Start, End, CV_RGB(0, 255, 0), -1);
		break;
	case BOX_RUN:
		cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 0, 0), -1);
		break;
	case BOX_COMP:	
		cvRectangle(m_iplBackGround, Start, End, CV_RGB(0, 0, 255), -1);
		break;
	case BOX_CANCEL:
		cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 0), -1);
		break;
	default:
		break;
	}
}

void CVS11IndexerDlg::m_fnDrawBoxTable(void)
{
	//BOX_LIST elBoxList;
	CvPoint Start, End;
	Start.x = 18;	Start.y = 302;
	End.x = Start.x + 158;	End.y = Start.y + 26;
	//1단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In18_Sn_BoxTable7_100mm == ON)	
		m_fnDrawBoxTable(BOX7, Start, End);		
		//cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);
	
		
	Start.x = 18;	Start.y = 337;
	End.x = 176;	End.y = Start.y + 26;
	//2단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In15_Sn_BoxTable6_100mm == ON)
		m_fnDrawBoxTable(BOX6, Start, End);
		//cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);

	Start.x = 18;	Start.y = 372;
	End.x = 176;	End.y = Start.y + 26;
	//3단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In12_Sn_BoxTable5_100mm == ON)
		m_fnDrawBoxTable(BOX5, Start, End);
		//cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);

	Start.x = 18;	Start.y = 407;
	End.x = 176;	End.y = Start.y + 26;
	//4단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In09_Sn_BoxTable4_100mm == ON)
		m_fnDrawBoxTable(BOX4, Start, End);
		//cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);

	Start.x = 18;	Start.y = 442;
	End.x = 176;	End.y = Start.y + 26;
	//5단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In06_Sn_BoxTable3_100mm == ON)
		m_fnDrawBoxTable(BOX3, Start, End);
		//cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);

	Start.x = 18;	Start.y = 477;
	End.x = 176;	End.y = Start.y + 26;
	//6단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In03_Sn_BoxTable2_100mm == ON)
		m_fnDrawBoxTable(BOX2, Start, End);
		//cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);

	Start.x = 18;	Start.y = 512;
	End.x = 176;	End.y = Start.y + 26;
	//7단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In00_Sn_BoxTable1_100mm == ON)
		m_fnDrawBoxTable(BOX1, Start, End);
		//cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);

	//Start.x = 18;	Start.y = 267;
	//End.x = 176;	End.y = Start.y + 26;
	//불량단
	Start.x = 717;	Start.y = 368;
	End.x = Start.x + 144;	End.y = Start.y + 26;
	if (G_STMACHINE_STATUS.stBoxTableSensor.In21_Sn_BoxTableF_100mm == ON)	
		m_fnDrawBoxTable(BAD_BOX, Start, End);
		//cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);
	 		
}

void CVS11IndexerDlg::m_fnDrawLoadingTable(void)
{
//	m_fnLoadingTableObjectInit();

	CvPoint Start, End;
	Start.x = 268;	Start.y = 368;
	End.x = Start.x + 144;	End.y = Start.y + 26;

	if (G_STMACHINE_STATUS.stModuleStatus.In07_Sn_LoadBoxSmallDetection == ON)
		cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);	
	
	Start.x = 268;	Start.y = 362;
	End.x = Start.x + 144;	

// 	if (G_STMACHINE_STATUS.stModuleStatus.In07_Sn_LoadBoxSmallDetection == ON)
// 	{
// 		for (int nStep = 0; nStep < MAX_OBJECT_COUNT; nStep++)
// 		{
// 			if (G_STLT01_OBJECT[nStep].unObjectStatus == TRUE)
// 			{
// 				if (G_STLT01_OBJECT[nStep].unObjectType == PAPER)
// 				{
// 					End.y = Start.y;
// 					cvLine(m_iplBackGround, Start, End, CV_RGB(0, 0, 255), 2); //Paper
// 					Start.y -= 10;
// 				}
// 				else
// 				{
// 					End.y = Start.y;
// 					cvLine(m_iplBackGround, Start, End, CV_RGB(200, 200, 200), 8);  //Stick
// 					Start.y -= 10;
// 				}
// 			}
// 		}
// 	}
	
}

void CVS11IndexerDlg::m_fnDrawUnLoadingTable(void)
{
	m_fnUnLoadingTableObjectInit();

	CvPoint Start, End;
	Start.x = 483;	Start.y = 368;
	End.x = Start.x + 144;	End.y = Start.y + 26;

	if (G_STMACHINE_STATUS.stModuleStatus.In15_Sn_UnloadBoxDetection == ON)
		cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);
	
	Start.x = 483;	Start.y = 362;
	End.x = Start.x + 144;

// 	if (G_STMACHINE_STATUS.stModuleStatus.In15_Sn_UnloadBoxDetection == ON)
// 	{
// 		for (int nStep = 0; nStep < MAX_OBJECT_COUNT; nStep++)
// 		{
// 			if (G_STUT01_OBJECT[nStep].unObjectStatus == TRUE)
// 			{
// 				if (G_STUT01_OBJECT[nStep].unObjectType == PAPER)
// 				{
// 					End.y = Start.y;
// 					cvLine(m_iplBackGround, Start, End, CV_RGB(0, 0, 255), 2); //Paper
// 					Start.y -= 10;
// 				}
// 				else
// 				{
// 					End.y = Start.y;
// 					cvLine(m_iplBackGround, Start, End, CV_RGB(200, 200, 200), 8);  //Stick
// 					Start.y -= 10;
// 				}
// 			}
// 		}
// 	}
	
}

void CVS11IndexerDlg::m_fnDrawBadBoxTable(void)
{
	m_fnBadTableObjectInit();

	CvPoint Start, End;	
	Start.x = 717;	Start.y = 368;
	End.x = Start.x + 144;	End.y = Start.y + 26;
	//불량단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In21_Sn_BoxTableF_100mm == ON)
		cvRectangle(m_iplBackGround, Start, End, CV_RGB(255, 255, 255), -1);	

	Start.x = 717;	Start.y = 362;
	End.x = Start.x + 144;

// 	if (G_STMACHINE_STATUS.stBoxTableSensor.In21_Sn_BoxTableF_100mm == ON)
// 	{
// 		for (int nStep = 0; nStep < MAX_OBJECT_COUNT; nStep++)
// 		{
// 			if (G_STBT01_OBJECT[nStep].unObjectStatus == TRUE)
// 			{
// 				if (G_STBT01_OBJECT[nStep].unObjectType == PAPER)
// 				{
// 					End.y = Start.y;
// 					cvLine(m_iplBackGround, Start, End, CV_RGB(0, 0, 255), 2); //Paper
// 					Start.y -= 10;
// 				}
// 				else
// 				{
// 					End.y = Start.y;
// 					cvLine(m_iplBackGround, Start, End, CV_RGB(200, 200, 200), 8);  //Stick
// 					Start.y -= 10;
// 				}
// 			}
// 		}
// 	}
	
}

void CVS11IndexerDlg::m_fnLoadingTableObjectInit(void)
{	
	for (int nStep = 0; nStep < MAX_OBJECT_COUNT; nStep++)
		G_STLT01_OBJECT[nStep].unObjectStatus = TRUE;

	G_STLT01_OBJECT[0].unObjectType = PAPER;	G_STLT01_OBJECT[1].unObjectType = PAPER;		

	G_STLT01_OBJECT[2].unObjectType = STICK;	G_STLT01_OBJECT[3].unObjectType = PAPER;	

	G_STLT01_OBJECT[4].unObjectType = PAPER;	G_STLT01_OBJECT[5].unObjectType = STICK;	

	G_STLT01_OBJECT[6].unObjectType = PAPER;	G_STLT01_OBJECT[7].unObjectType = PAPER;		

	G_STLT01_OBJECT[8].unObjectType = STICK;	G_STLT01_OBJECT[9].unObjectType = PAPER;	

	G_STLT01_OBJECT[10].unObjectType = PAPER;	G_STLT01_OBJECT[11].unObjectType = STICK;	

	G_STLT01_OBJECT[12].unObjectType = PAPER;	G_STLT01_OBJECT[13].unObjectType = PAPER;	

	G_STLT01_OBJECT[14].unObjectType = STICK;	G_STLT01_OBJECT[15].unObjectType = PAPER;	

	G_STLT01_OBJECT[16].unObjectType = PAPER;	G_STLT01_OBJECT[17].unObjectType = STICK;	

	G_STLT01_OBJECT[18].unObjectType = PAPER;	G_STLT01_OBJECT[19].unObjectType = PAPER;	

	G_STLT01_OBJECT[20].unObjectType = STICK;	G_STLT01_OBJECT[21].unObjectType = PAPER;	

	G_STLT01_OBJECT[22].unObjectType = PAPER;	G_STLT01_OBJECT[23].unObjectType = STICK;	

	G_STLT01_OBJECT[24].unObjectType = PAPER;	G_STLT01_OBJECT[25].unObjectType = PAPER;	

	G_STLT01_OBJECT[26].unObjectType = STICK;	G_STLT01_OBJECT[27].unObjectType = PAPER;	

	G_STLT01_OBJECT[28].unObjectType = PAPER;	G_STLT01_OBJECT[29].unObjectType = STICK;	

	G_STLT01_OBJECT[30].unObjectType = PAPER;	G_STLT01_OBJECT[31].unObjectType = PAPER;


	//G_STLT01_OBJECT[0].unObjectType = PAPER;	G_STLT01_OBJECT[1].unObjectType = STICK;		

	//G_STLT01_OBJECT[2].unObjectType = PAPER;	G_STLT01_OBJECT[3].unObjectType = PAPER;	

	//G_STLT01_OBJECT[4].unObjectType = STICK;	G_STLT01_OBJECT[5].unObjectType = PAPER;	

	//G_STLT01_OBJECT[6].unObjectType = PAPER;	G_STLT01_OBJECT[7].unObjectType = STICK;		

	//G_STLT01_OBJECT[8].unObjectType = PAPER;	G_STLT01_OBJECT[9].unObjectType = PAPER;	

	//G_STLT01_OBJECT[10].unObjectType = STICK;	G_STLT01_OBJECT[11].unObjectType = PAPER;	

	//G_STLT01_OBJECT[12].unObjectType = PAPER;	G_STLT01_OBJECT[13].unObjectType = STICK;	

	//G_STLT01_OBJECT[14].unObjectType = PAPER;	G_STLT01_OBJECT[15].unObjectType = PAPER;	

	//G_STLT01_OBJECT[16].unObjectType = STICK;	G_STLT01_OBJECT[17].unObjectType = PAPER;	

	//G_STLT01_OBJECT[18].unObjectType = PAPER;	G_STLT01_OBJECT[19].unObjectType = STICK;	

	//G_STLT01_OBJECT[20].unObjectType = PAPER;	G_STLT01_OBJECT[21].unObjectType = PAPER;	

	//G_STLT01_OBJECT[22].unObjectType = STICK;	G_STLT01_OBJECT[23].unObjectType = PAPER;	

	//G_STLT01_OBJECT[24].unObjectType = PAPER;	G_STLT01_OBJECT[25].unObjectType = STICK;	

	//G_STLT01_OBJECT[26].unObjectType = PAPER;	G_STLT01_OBJECT[27].unObjectType = PAPER;	

	//G_STLT01_OBJECT[28].unObjectType = STICK;	G_STLT01_OBJECT[29].unObjectType = PAPER;	
}

void CVS11IndexerDlg::m_fnUnLoadingTableObjectInit(void)
{
	for (int nStep = 0; nStep < MAX_OBJECT_COUNT; nStep++)
		G_STUT01_OBJECT[0].unObjectStatus = FALSE;

	G_STUT01_OBJECT[0].unObjectType = PAPER;	G_STUT01_OBJECT[1].unObjectType = PAPER;	

	G_STUT01_OBJECT[2].unObjectType = PAPER;	G_STUT01_OBJECT[3].unObjectType = STICK;	

	G_STUT01_OBJECT[4].unObjectType = PAPER;	G_STUT01_OBJECT[5].unObjectType = PAPER;	

	G_STUT01_OBJECT[6].unObjectType = STICK;	G_STUT01_OBJECT[7].unObjectType = PAPER;		

	G_STUT01_OBJECT[8].unObjectType = PAPER;	G_STUT01_OBJECT[9].unObjectType = STICK;	

	G_STUT01_OBJECT[10].unObjectType = PAPER;	G_STUT01_OBJECT[11].unObjectType = PAPER;	

	G_STUT01_OBJECT[12].unObjectType = STICK;	G_STUT01_OBJECT[13].unObjectType = PAPER;	

	G_STUT01_OBJECT[14].unObjectType = PAPER;	G_STUT01_OBJECT[15].unObjectType = STICK;	

	G_STUT01_OBJECT[16].unObjectType = PAPER;	G_STUT01_OBJECT[17].unObjectType = PAPER;	

	G_STUT01_OBJECT[18].unObjectType = STICK;	G_STUT01_OBJECT[19].unObjectType = PAPER;	

	G_STUT01_OBJECT[20].unObjectType = PAPER;	G_STUT01_OBJECT[21].unObjectType = STICK;	

	G_STUT01_OBJECT[22].unObjectType = PAPER;	G_STUT01_OBJECT[23].unObjectType = PAPER;	

	G_STUT01_OBJECT[24].unObjectType = STICK;	G_STUT01_OBJECT[25].unObjectType = PAPER;	

	G_STUT01_OBJECT[26].unObjectType = PAPER;	G_STUT01_OBJECT[27].unObjectType = STICK;	

	G_STUT01_OBJECT[28].unObjectType = PAPER;	G_STUT01_OBJECT[29].unObjectType = PAPER;	

	G_STUT01_OBJECT[30].unObjectType = STICK;	G_STUT01_OBJECT[31].unObjectType = PAPER;	


	//G_STUT01_OBJECT[0].unObjectType = PAPER;	G_STUT01_OBJECT[1].unObjectType = PAPER;	

	//G_STUT01_OBJECT[2].unObjectType = STICK;	G_STUT01_OBJECT[3].unObjectType = PAPER;	

	//G_STUT01_OBJECT[4].unObjectType = PAPER;	G_STUT01_OBJECT[5].unObjectType = STICK;	

	//G_STUT01_OBJECT[6].unObjectType = PAPER;	G_STUT01_OBJECT[7].unObjectType = PAPER;		

	//G_STUT01_OBJECT[8].unObjectType = STICK;	G_STUT01_OBJECT[9].unObjectType = PAPER;	

	//G_STUT01_OBJECT[10].unObjectType = PAPER;	G_STUT01_OBJECT[11].unObjectType = STICK;	

	//G_STUT01_OBJECT[12].unObjectType = PAPER;	G_STUT01_OBJECT[13].unObjectType = PAPER;	

	//G_STUT01_OBJECT[14].unObjectType = STICK;	G_STUT01_OBJECT[15].unObjectType = PAPER;	

	//G_STUT01_OBJECT[16].unObjectType = PAPER;	G_STUT01_OBJECT[17].unObjectType = STICK;	

	//G_STUT01_OBJECT[18].unObjectType = PAPER;	G_STUT01_OBJECT[19].unObjectType = PAPER;	

	//G_STUT01_OBJECT[20].unObjectType = STICK;	G_STUT01_OBJECT[21].unObjectType = PAPER;	

	//G_STUT01_OBJECT[22].unObjectType = PAPER;	G_STUT01_OBJECT[23].unObjectType = STICK;	

	//G_STUT01_OBJECT[24].unObjectType = PAPER;	G_STUT01_OBJECT[25].unObjectType = STICK;	

	//G_STUT01_OBJECT[26].unObjectType = PAPER;	G_STUT01_OBJECT[27].unObjectType = STICK;	

	//G_STUT01_OBJECT[28].unObjectType = PAPER;	G_STUT01_OBJECT[29].unObjectType = NONE;	
}

void CVS11IndexerDlg::m_fnBadTableObjectInit(void)
{
	for (int nStep = 0; nStep < MAX_OBJECT_COUNT; nStep++)
		G_STBT01_OBJECT[0].unObjectStatus = FALSE;

	G_STBT01_OBJECT[0].unObjectType = PAPER;	G_STBT01_OBJECT[1].unObjectType = STICK;	

	G_STBT01_OBJECT[2].unObjectType = PAPER;	G_STBT01_OBJECT[3].unObjectType = PAPER;	

	G_STBT01_OBJECT[4].unObjectType = STICK;	G_STBT01_OBJECT[5].unObjectType = PAPER;	

	G_STBT01_OBJECT[6].unObjectType = PAPER;	G_STBT01_OBJECT[7].unObjectType = STICK;		

	G_STBT01_OBJECT[8].unObjectType = PAPER;	G_STBT01_OBJECT[9].unObjectType = PAPER;	

	G_STBT01_OBJECT[10].unObjectType = STICK;	G_STBT01_OBJECT[11].unObjectType = PAPER;	

	G_STBT01_OBJECT[12].unObjectType = PAPER;	G_STBT01_OBJECT[13].unObjectType = STICK;	

	G_STBT01_OBJECT[14].unObjectType = PAPER;	G_STBT01_OBJECT[15].unObjectType = PAPER;	

	G_STBT01_OBJECT[16].unObjectType = STICK;	G_STBT01_OBJECT[17].unObjectType = PAPER;	

	G_STBT01_OBJECT[18].unObjectType = PAPER;	G_STBT01_OBJECT[19].unObjectType = STICK;	

	G_STBT01_OBJECT[20].unObjectType = PAPER;	G_STBT01_OBJECT[21].unObjectType = PAPER;	

	G_STBT01_OBJECT[22].unObjectType = STICK;	G_STBT01_OBJECT[23].unObjectType = PAPER;	

	G_STBT01_OBJECT[24].unObjectType = PAPER;	G_STBT01_OBJECT[25].unObjectType = STICK;	

	G_STBT01_OBJECT[26].unObjectType = PAPER;	G_STBT01_OBJECT[27].unObjectType = PAPER;	

	G_STBT01_OBJECT[28].unObjectType = STICK;	G_STBT01_OBJECT[29].unObjectType = PAPER;	

	G_STBT01_OBJECT[30].unObjectType = PAPER;	G_STBT01_OBJECT[31].unObjectType = PAPER;	


	//G_STBT01_OBJECT[0].unObjectType = PAPER;	G_STBT01_OBJECT[1].unObjectType = STICK;	

	//G_STBT01_OBJECT[2].unObjectType = PAPER;	G_STBT01_OBJECT[3].unObjectType = PAPER;	

	//G_STBT01_OBJECT[4].unObjectType = STICK;	G_STBT01_OBJECT[5].unObjectType = PAPER;	

	//G_STBT01_OBJECT[6].unObjectType = PAPER;	G_STBT01_OBJECT[7].unObjectType = STICK;		

	//G_STBT01_OBJECT[8].unObjectType = PAPER;	G_STBT01_OBJECT[9].unObjectType = PAPER;	

	//G_STBT01_OBJECT[10].unObjectType = STICK;	G_STBT01_OBJECT[11].unObjectType = PAPER;	

	//G_STBT01_OBJECT[12].unObjectType = PAPER;	G_STBT01_OBJECT[13].unObjectType = STICK;	

	//G_STBT01_OBJECT[14].unObjectType = PAPER;	G_STBT01_OBJECT[15].unObjectType = PAPER;	

	//G_STBT01_OBJECT[16].unObjectType = STICK;	G_STBT01_OBJECT[17].unObjectType = PAPER;	

	//G_STBT01_OBJECT[18].unObjectType = PAPER;	G_STBT01_OBJECT[19].unObjectType = STICK;	

	//G_STBT01_OBJECT[20].unObjectType = PAPER;	G_STBT01_OBJECT[21].unObjectType = PAPER;	

	//G_STBT01_OBJECT[22].unObjectType = STICK;	G_STBT01_OBJECT[23].unObjectType = PAPER;	

	//G_STBT01_OBJECT[24].unObjectType = PAPER;	G_STBT01_OBJECT[25].unObjectType = STICK;	

	//G_STBT01_OBJECT[26].unObjectType = PAPER;	G_STBT01_OBJECT[27].unObjectType = PAPER;	

	//G_STBT01_OBJECT[28].unObjectType = STICK;	G_STBT01_OBJECT[29].unObjectType = PAPER;	
}

void CVS11IndexerDlg::m_fnDrawTurnTable(void)
{
	CvPoint Start, End;
	Start.x = 374;	Start.y = 506;
	End.x = Start.x + 144; End.y = Start.y;

	if (G_STMACHINE_STATUS.stTurnTableSensor.In01_Sn_TurnTableUpperLeftStickDetect == ON)
		cvLine(m_iplBackGround, Start, End, CV_RGB(200, 200, 200), 8);  //Stick

	if (G_STMACHINE_STATUS.stTurnTableSensor.In03_Sn_TurnTableLowerLeftStickDetect == ON)
		cvLine(m_iplBackGround, Start, End, CV_RGB(200, 200, 200), 8);  //Stick

}

void CVS11IndexerDlg::m_fnDrawEmissionTable(void)
{
	CvPoint Start, End;
	Start.x = 596;	Start.y = 506;
	End.x = Start.x + 144; End.y = Start.y;

	if (G_STMACHINE_STATUS.stEmissionTable.In00_Sn_OutputTableLeftStickDetect == ON)
		cvLine(m_iplBackGround, Start, End, CV_RGB(200, 200, 200), 8);  //Stick	
}

void CVS11IndexerDlg::m_fnDrawInspectTable(void)
{
	CvPoint Start, End;
	Start.x = 376;	Start.y = 610;
	End.x = Start.x + 144; End.y = Start.y;

	if (G_STMACHINE_STATUS.stGripperStatus.In10_Sn_LeftTensionStickDetection == ON)
		cvLine(m_iplBackGround, Start, End, CV_RGB(200, 200, 200), 8);  //Stick
}

void CVS11IndexerDlg::m_fnImageMerge(void)
{
	//백그라운드 카피
	cvCopyImage(m_iplBackGroundOrg ,m_iplBackGround);
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVS11IndexerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CVS11IndexerDlg::m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...)
{
	char	cLogBuffer[MAX_LOG_BUFF];
	va_list vargs;

	va_start(vargs, pErrLog);

	vsprintf_s(cLogBuffer, pErrLog, (va_list)vargs);

	va_end(vargs);

	m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nTaskNum ,uLogLevel, cLogBuffer);

	//strcpy_s(&cLogBuffer[strlen(cLogBuffer)], sizeof("\r\n"),"\r\n");

	//printf(cLogBuffer);
}

void CVS11IndexerDlg::Sys_fnInitVS64Interface()
{
	if(1 < __argc)
	{
		int nInputTaskNum = atoi(__argv[1]);
		char *pLinkPath = NULL;

		char path_buffer[_MAX_PATH];
		char chFilePath[_MAX_PATH] = {0,};
		char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		//실행 파일 이름을 포함한 Full path 가 얻어진다.
		::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
		//디렉토리만 구해낸다.
		_splitpath_s(path_buffer, drive, dir, fname, ext);
		strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
		strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

		switch(nInputTaskNum)
		{

		case TASK_10_Master		: pLinkPath = MSG_TASK_INI_FullPath_TN10; break;
		case TASK_11_Indexer		: pLinkPath = MSG_TASK_INI_FullPath_TN11; break;
		case TASK_21_Inspector	: pLinkPath = MSG_TASK_INI_FullPath_TN21; break;
		case TASK_24_Motion		: pLinkPath = MSG_TASK_INI_FullPath_TN24; break;
		case TASK_31_Mesure3D	: pLinkPath = MSG_TASK_INI_FullPath_TN31; break;
		case TASK_32_Mesure2D	: pLinkPath = MSG_TASK_INI_FullPath_TN32; break;
		case TASK_33_SerialPort	: pLinkPath = MSG_TASK_INI_FullPath_TN33; break;
		case TASK_60_Log			: pLinkPath = MSG_TASK_INI_FullPath_TN60; break;
		case TASK_90_TaskLoader: pLinkPath = MSG_TASK_INI_FullPath_TN90; break;

		case TASK_99_Sample: pLinkPath = MSG_TASK_INI_FullPath_TN99; break;
		default:
			{
				CString ErrorMessage;
				ErrorMessage.Format("Unkown TaskNum(%d)", nInputTaskNum);
				AfxMessageBox(ErrorMessage);
				SendMessage(WM_CLOSE, 0,  0);	
			}return;
		}

		strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

		m_fnReadDatFile(chFilePath);
		m_ProcInitVal.m_nTaskNum = nInputTaskNum;
		//m_ProcInitVal.m_nLogFileID = nInputTaskNum;
	}
	else
	{
		AfxMessageBox("Input Run Task Number");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	m_cpServerInterface.m_fnSetLogTaskInfo(m_ProcInitVal.m_nLogTaskNo, m_ProcInitVal.m_nLogBlockLovel);	
	if(m_cpServerInterface.m_fnRegisterTask(m_ProcInitVal.m_nTaskNum, m_ProcInitVal.m_sServerIP, m_ProcInitVal.m_nPortNo) != 0)
	{
		AfxMessageBox("Client Regist Fail");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	else
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, "Client Regist OK : %d", m_ProcInitVal.m_nTaskNum);
	}

	m_cpServerInterface.m_fnSetAppWindowHandle(m_hWnd);
	m_cpServerInterface.m_fnSetAppUserMessage(WM_VS64USER_CALLBACK);
}


/*
*	Module Name	:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function			:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS11IndexerDlg::Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	CMDMSG* CmdMsg = (CMDMSG*)lParam;		//SmartPointer 버전으로 개조가 필요하다... Work Item..

	try
	{
		if(CmdMsg != NULL)
		{
			nRet = Sys_fnAnalyzeMsg(CmdMsg);

			if(OKAY != nRet)
			{
				//Client 메시지 처리에대한 에러처리...
			}

			m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
			m_cpServerInterface.m_fnFreeMemory(CmdMsg);
			return OKAY;
		}
		else
		{
			throw CVs64Exception(_T("CVS19MWDlg::m_fnVS64MsgProc, Message did not become well."));
		}
	}
	catch (CVs64Exception& e)
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, e.m_fnGetErrorMsg());

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, exception -> %s"), e.what());
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, CException -> %s"), wszErr);
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	return OKAY;
}

/*
*	Module Name	:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function			:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS11IndexerDlg::Sys_fnBarcodeCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	
	try
	{
		nRet = m_cBoxTransper->m_fn_TM01_BCR_Return(lParam);
		return OKAY;

// 		if(lParam != NULL)
// 		{
// 			nRet = m_fn_TM01_BCR_Return(lParam);
// 
// 			if(OKAY != nRet)
// 			{
// 				//Client 메시지 처리에대한 에러처리...
// 			}
// 
// 			return OKAY;
// 		}
// 		else
// 		{
// 			throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnBarcodeCallback, Message did not become well."));
// 		}
	}
	catch (CVs64Exception& e)
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS,  e.m_fnGetErrorMsg());		
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnBarcodeCallback, exception -> %s"), e.what());
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));		
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnBarcodeCallback, CException -> %s"), wszErr);
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));		
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnBarcodeCallback, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));		
	}
	return OKAY;
}

LRESULT CVS11IndexerDlg::Sys_fnPaperDetectCallback(WPARAM wParam, LPARAM lParam)
{
//	int	nRet;
	try
	{
		m_cPickerControl->m_fnSet_PaperMove_LT01_TM02_Step(PPG_TM02_PAPER_CHECK_RETURN);		
		return OKAY;		
	}
	catch (CVs64Exception& e)
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, e.m_fnGetErrorMsg());
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnPaperDetectCallback, exception -> %s"), e.what());
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255);
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnPaperDetectCallback, CException -> %s"), wszErr);
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnPaperDetectCallback, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	return OKAY;
}

LRESULT CVS11IndexerDlg::Sys_fnStickDetectCallback(WPARAM wParam, LPARAM lParam)
{
//	int	nRet;
	try
	{
		m_cPickerControl->m_fnSet_StickMove_LT01_TM02_Step(STLT_TM02_STICK_CHECK_RETURN);
		return OKAY;
	}
	catch (CVs64Exception& e)
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, e.m_fnGetErrorMsg());
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnPaperDetectCallback, exception -> %s"), e.what());
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255);
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnPaperDetectCallback, CException -> %s"), wszErr);
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnPaperDetectCallback, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	return OKAY;
}

LRESULT CVS11IndexerDlg::Sys_fnStickAlignCallback(WPARAM wParam, LPARAM lParam)
{
	//int	nRet;
	try
	{
		if (m_cCameraControl.m_fnGetShiftOffset() == 0 &&
			m_cCameraControl.m_fnGetTiltOffset() == 0 &&
			m_cCameraControl.m_fnGetDriveOffset() == 0)
		{
			m_cPickerControl->m_fnSet_StickMove_LT01_TM02_Step(STLT_TM02_STICK_ALIGN_RETURN);
		}
		else
		{
			m_cPickerControl->m_fnSet_StickMove_LT01_TM02_Step(STLT_TM02_SHIFT_ALIGN_POS);
		}
		
		return OKAY;
	}
	catch (CVs64Exception& e)
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, e.m_fnGetErrorMsg());
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnStickAlignCallback, exception -> %s"), e.what());
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255);
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnStickAlignCallback, CException -> %s"), wszErr);
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS11IndexerDlg::Sys_fnStickAlignCallback, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));
	}
	return OKAY;
}

HBRUSH CVS11IndexerDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();	

	switch (DLG_ID_Number)
	{
	case IDC_ST_TASK_STATE:
		return hbr;		
	default:
		break;
	}

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CVS11IndexerDlg::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x20, 0x20, 0x20);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyle1.SetButtonStyle(&tStyle);
	
	m_tStyle2.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 6.0;
	tStyle.m_dHighLightY = 4.0;
	tStyle.m_dRadius = 4.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle2.SetButtonStyle(&tStyle);

	m_tStyle3.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 6.0;
	tStyle.m_dHighLightY = 4.0;
	tStyle.m_dRadius = 4.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x36, 0x55, 0x9E);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x16, 0x35, 0x7E);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyle3.SetButtonStyle(&tStyle);

	
	LOGFONT tFont;
	m_ctrBtnExit.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"Arial", strlen("Arial"));
	tFont.lfHeight = 60;

	tColorScheme tColor;
	m_ctrBtnExit.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	m_ctrBtnExit.SetTextColor(&tColor);
	m_ctrBtnExit.SetCheckButton(false);	
	m_ctrBtnExit.SetFont(&tFont);
	m_ctrBtnExit.SetRoundButtonStyle(&m_tStyle1);		
	
	m_ctrBtnLotStart.SetTextColor(&tColor);
	m_ctrBtnLotStart.SetCheckButton(false);	
	m_ctrBtnLotStart.SetFont(&tFont);
	m_ctrBtnLotStart.SetRoundButtonStyle(&m_tStyle1);
	
	m_ctrBtnRecipeSet.SetTextColor(&tColor);
	m_ctrBtnRecipeSet.SetCheckButton(false);
	m_ctrBtnRecipeSet.SetFont(&tFont);
	m_ctrBtnRecipeSet.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnProcReset.SetTextColor(&tColor);
	m_ctrBtnProcReset.SetCheckButton(false);
	m_ctrBtnProcReset.SetFont(&tFont);
	m_ctrBtnProcReset.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnFrontDoor.SetTextColor(&tColor);
	m_ctrBtnFrontDoor.SetCheckButton(false);
	m_ctrBtnFrontDoor.SetFont(&tFont);
	m_ctrBtnFrontDoor.SetRoundButtonStyle(&m_tStyle2);
	
	tFont.lfHeight = 18;
	m_ctrBtnLotRetry.SetTextColor(&tColor);
	m_ctrBtnLotRetry.SetCheckButton(false);
	m_ctrBtnLotRetry.SetFont(&tFont);
	m_ctrBtnLotRetry.SetRoundButtonStyle(&m_tStyle2);	

	m_ctrBtnExit.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	for(int nStep = 0; nStep < CTR_COUNT; nStep++)
	{
		m_ctrBtnCallPanel[nStep].SetTextColor(&tColor);
		m_ctrBtnCallPanel[nStep].SetCheckButton(false);		

		m_ctrBtnCallPanel[nStep].SetFont(&tFont);
		m_ctrBtnCallPanel[nStep].SetRoundButtonStyle(&m_tStyle2);
	}

	m_ctrBtnCallPanel[CTR_MAIN].SetFont(&tFont);
	m_ctrBtnCallPanel[CTR_MAIN].SetRoundButtonStyle(&m_tStyle3);

	m_TaskStateFont.CreateFont(70, 0, 0, 0, FW_BOLD,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Arial Black");
	
	m_TaskStateColor = RGB(128, 128, 128);

	m_ctrStaticTaskInfo.SetFont(&m_TaskStateFont);
	m_ctrStaticTaskInfo.SetTextColor(RGB(0, 0, 0));
	m_ctrStaticTaskInfo.SetBkColor(m_TaskStateColor);
}

void CVS11IndexerDlg::OnNcDestroy()
{
	__super::OnNcDestroy();
	
	m_cDisplayWindow1->PostNcDestroy();
	m_cDisplayWindow2->PostNcDestroy();
	m_cBoxTable->PostNcDestroy();
	m_cBoxTransper->PostNcDestroy();
	m_cBoxOpener->PostNcDestroy();
	m_cLoadingTable->PostNcDestroy();
	m_cUnloadingTable->PostNcDestroy();
	m_cStickTransper->PostNcDestroy();
	m_cTurnTable->PostNcDestroy();
	m_cEmissionTable->PostNcDestroy();
	m_cRecipeDefine->PostNcDestroy();
	m_cMainControl->PostNcDestroy();
	m_pBoxStatus->PostNcDestroy();

	m_cCameraControl.m_fnThreadStop();

	delete (CDisplayDlg1*)m_cDisplayWindow1;
	delete (CDisplayDlg2*)m_cDisplayWindow2;	
	delete (CBoxTable*)m_cBoxTable;	
	delete (CBoxTransper*)m_cBoxTransper;	
	delete (CBoxOpener*)m_cBoxOpener;	
	delete (CLoadingTable*)m_cLoadingTable;	
	delete (CUnloadingTable*)m_cUnloadingTable;	
	delete (CStickTransper*)m_cStickTransper;	
	delete (CTurnTable*)m_cTurnTable;
	delete (CEmissionTable*)m_cEmissionTable;
	delete (CRecipeDefine*)m_cRecipeDefine;

	delete (CMainControl*)m_cMainControl;	
	delete (CDemoSequence*)m_cDemoSeq;
	delete (CBoxControl*)m_cBoxControl;
	delete (CPickerControl*)m_cPickerControl;
	delete (CBoxStatus*)m_pBoxStatus;

	m_cDisplayWindow1 = nullptr;
	m_cDisplayWindow2 = nullptr;
	m_cBoxTable = nullptr;
	m_cBoxTransper = nullptr;
	m_cBoxOpener = nullptr;
	m_cLoadingTable = nullptr;
	m_cUnloadingTable = nullptr;
	m_cStickTransper = nullptr;
	m_cTurnTable = nullptr;
	m_cEmissionTable = nullptr;
	m_cMainControl = nullptr;
	m_cRecipeDefine = nullptr;
	m_cDemoSeq = nullptr;
	m_cBoxControl = nullptr;
	m_cPickerControl = nullptr;
	m_pBoxStatus = nullptr;

	m_TaskStateFont.DeleteObject();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CVS11IndexerDlg::m_fnAppendList(CString strLog, LOG_INFO nColorType)
{	
	SYSTEMTIME		systime;

	CString		strLogString;

	GetLocalTime(&systime);

	//로그 타스트로 로그 전송하고,,
	m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, strLog);

	strLogString.Format("[%02dh %02dm %02ds]",systime.wHour, systime.wMinute, systime.wSecond);	
	strLogString = strLogString + strLog;	
	//로그 출력.
	m_fnLogDisplay(strLogString, nColorType);
}

void CVS11IndexerDlg::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{
	if(m_ctrListLogView.GetCount() > LIST_ALARM_COUNT)
	{
		m_ctrListLogView.DeleteString(0);
	}
	switch(nColorType)
	{
	case TEXT_NORMAL_BLACK:
		m_ctrListLogView.AddLine((CXListBox::Color)1, 	(CXListBox::Color)0, 	strLog);
		break;
	case TEXT_COLOR_GREEN:
		m_ctrListLogView.AddLine((CXListBox::Color)1, 	(CXListBox::Color)3, 	strLog);
		break;
	case TEXT_WARRING_YELLOW:
		m_ctrListLogView.AddLine((CXListBox::Color)0, 	(CXListBox::Color)12, 	strLog);
		break;
	case TEXT_ERROR_RED:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)10, strLog);
		break;
	case TEXT_NORMAL_WHITE:
		m_ctrListLogView.AddLine((CXListBox::Color)0, (CXListBox::Color)1, strLog);
		break;
	case TEXT_COLOR_BLUE:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)13, strLog);
		break;
	default:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)0, strLog);
		break;
	}

	m_ctrListLogView.SetScrollPos(SB_VERT , m_ctrListLogView.GetCount(), TRUE);
	m_ctrListLogView.SetTopIndex(m_ctrListLogView.GetCount() - 1);	
}

void CVS11IndexerDlg::OnBnClickedOk()
{
	m_sThreadParam.bThreadFlag = FALSE;
	Sleep(300);
	OnOK();
}

BOOL CVS11IndexerDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return __super::PreTranslateMessage(pMsg);
}

void CVS11IndexerDlg::OnBnClickedBtnCallPanel()
{
	CWnd *pwnd = GetFocus(); 
	int wID = pwnd->GetDlgCtrlID();
	
	for(int nStep = 0; nStep < CTR_COUNT; nStep++)
	{		
		m_ctrBtnCallPanel[nStep].SetRoundButtonStyle(&m_tStyle2);
		//m_ctrBtnCallPanel[nStep].Invalidate(FALSE);
		m_cBoxTable->ShowWindow(SW_HIDE);
		m_cBoxTransper->ShowWindow(SW_HIDE);
		m_cBoxOpener->ShowWindow(SW_HIDE);
		m_cLoadingTable->ShowWindow(SW_HIDE);
		m_cUnloadingTable->ShowWindow(SW_HIDE);
		m_cStickTransper->ShowWindow(SW_HIDE);
		m_cTurnTable->ShowWindow(SW_HIDE);
		m_cEmissionTable->ShowWindow(SW_HIDE);
		m_cMainControl->ShowWindow(SW_HIDE);
		m_cRecipeDefine->ShowWindow(SW_HIDE);
	}	

	switch(wID)
	{
	case IDC_BTN_CALL_CTR:
		m_ctrBtnCallPanel[CTR_MAIN].SetRoundButtonStyle(&m_tStyle3);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_SHOW);
		//m_ctrBtnCallPanel->ShowWindow(SW_SHOW);
		break;
	case IDC_BTN_CALL_BT01:
		m_ctrBtnCallPanel[CTR_BT01].SetRoundButtonStyle(&m_tStyle3);
		m_cBoxTable->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;
	case IDC_BTN_CALL_TM01:	
		m_ctrBtnCallPanel[CTR_TM01].SetRoundButtonStyle(&m_tStyle3);
		m_cBoxTransper->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;
	case IDC_BTN_CALL_LT01:	
		m_ctrBtnCallPanel[CTR_LT01].SetRoundButtonStyle(&m_tStyle3);
		m_cLoadingTable->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;
	case IDC_BTN_CALL_BO01:	
		m_ctrBtnCallPanel[CTR_BO01].SetRoundButtonStyle(&m_tStyle3);
		m_cBoxOpener->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;
	case IDC_BTN_CALL_UT01:	
		m_ctrBtnCallPanel[CTR_UT01].SetRoundButtonStyle(&m_tStyle3);
		m_cUnloadingTable->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;
	case IDC_BTN_CALL_TM02:	
		m_ctrBtnCallPanel[CTR_TM02].SetRoundButtonStyle(&m_tStyle3);
		m_cStickTransper->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;	
	case IDC_BTN_CALL_TT01:	
		m_ctrBtnCallPanel[CTR_TT01].SetRoundButtonStyle(&m_tStyle3);
		m_cTurnTable->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;	
	case IDC_BTN_CALL_UT02:	
		m_ctrBtnCallPanel[CTR_UT02].SetRoundButtonStyle(&m_tStyle3);
		m_cEmissionTable->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;	
	case IDC_BTN_CALL_RECIPE:
		m_ctrBtnCallPanel[CTR_RECIPE].SetRoundButtonStyle(&m_tStyle3);
		m_cRecipeDefine->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;
	case IDC_BTN_CALL_CONTROL:
		m_ctrBtnCallPanel[CTR_CONTROL].SetRoundButtonStyle(&m_tStyle3);
		m_cMainControl->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ST_IMG_VIEW)->ShowWindow(SW_HIDE);
		break;
	}
	//Invalidate(FALSE);
}

int CVS11IndexerDlg::m_fnReadSystemFile(void)
{
	char chReadData[MAX_PATH];
	memset(chReadData, 0x00, MAX_PATH);
	//BT01 Start
	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_BAD_POS, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nBadBoxPosition = _ttoi(chReadData);
	
	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS1, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nBox1Position = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS2, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nBox2Position = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS3, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nBox3Position = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS4, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nBox4Position = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS5, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nBox5Position = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS6, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nBox6Position = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_POS7, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nBox7Position = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BT01_POS_DATA, KEY_READY_POS, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BT01_Position.nReadyPosition = _ttoi(chReadData);
	//BT01 End

	//TM01 Start
	GetPrivateProfileString(SECTION_TM01_POS_DATA, KEY_FORK_BT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM01_Position.nBT01_FwdPos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM01_POS_DATA, KEY_FORK_LT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM01_Position.nLT01_BwdPos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM01_POS_DATA, KEY_TM01_READY, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM01_Position.nTM01_Ready_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM01_POS_DATA, KEY_TM01_BCR_READ, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM01_Position.mTM01_BCR_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM01_POS_DATA, KEY_PURGE_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM01_Position.nPurge_Time = _ttoi(chReadData);
	//TM01 End

	//LT01 Start
	GetPrivateProfileString(SECTION_LT01_POS_DATA, KEY_GUIDE_SMALL, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_LT01_Position.nGuide_Small_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_LT01_POS_DATA, KEY_GUIDE_MID, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_LT01_Position.nGuide_Mid_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_LT01_POS_DATA, KEY_GUIDE_LARGE, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_LT01_Position.nGuide_Large_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_LT01_POS_DATA, KEY_GUIDE_READY, "", chReadData, MAX_PATH, SYSTEM_PARAM_INI_PATH);
	m_st_LT01_Position.nGuide_Ready_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_LT01_POS_DATA, KEY_PURGE_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_LT01_Position.nPurge_Time = _ttoi(chReadData);
	//LT01 End

	//BO01 Start
	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_LT01_SMALL, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nDrive_LT01_Small_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_UT01_SMALL, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nDrive_UT01_Small_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_LT01_MID, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nDrive_LT01_Mid_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_UT01_MID, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nDrive_UT01_Mid_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_LT01_LARGE, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nDrive_LT01_Large_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_DRIVE_UT01_LARGE, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nDrive_UT01_Large_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_READY_POS, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nZ_Ready_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_LT01_UP_COVER, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nZ_LT01_Up_Cover_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_LT01_DN_COVER, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nZ_LT01_Dn_Cover_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_UT01_UP_COVER, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nZ_UT01_Up_Cover_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_Z_UT01_DN_COVER, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nZ_UT01_Dn_Cover_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_R_PAD_UP, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nR_Pad_Up_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_R_PAD_DN, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nR_Pad_Dn_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_BO01_POS_DATA, KEY_PURGE_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_BO01_Position.nPurge_Time = _ttoi(chReadData);
	//BO01 End

	//UT01 Start
	GetPrivateProfileString(SECTION_UT01_POS_DATA, KEY_GUIDE_SMALL, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_UT01_Position.nGuide_Small_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_UT01_POS_DATA, KEY_GUIDE_MID, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_UT01_Position.nGuide_Mid_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_UT01_POS_DATA, KEY_GUIDE_LARGE, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_UT01_Position.nGuide_Large_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_UT01_POS_DATA, KEY_GUIDE_READY, "", chReadData, MAX_PATH, SYSTEM_PARAM_INI_PATH);
	m_st_UT01_Position.nGuide_Ready_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_UT01_POS_DATA, KEY_PURGE_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_UT01_Position.nPurge_Time = _ttoi(chReadData);
	//UT01 End

	//TM02 Start
	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_BT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nDrive_BT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_BT01_PP, "", chReadData, MAX_PATH, SYSTEM_PARAM_INI_PATH);
	m_st_TM02_Position.nDrive_BT01_PP_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_LT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nDrive_LT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_UT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nDrive_UT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_UT01_PP, "", chReadData, MAX_PATH, SYSTEM_PARAM_INI_PATH);
	m_st_TM02_Position.nDrive_UT01_PP_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_TT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nDrive_TT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_UT02, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nDrive_UT02_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_QR, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nDrive_QR_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_DRIVE_POS_ALIGN, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nDrive_Align_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_BT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nT_BT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_LT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nT_LT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_UT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nT_UT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_TT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nT_TT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_UT02, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nT_UT02_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_T_POS_QR, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nT_QR_Pos = _ttoi(chReadData);


	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_BT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nShift_BT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_LT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nShift_LT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_UT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nShift_UT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_TT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nShift_TT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_UT02, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nShift_UT02_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_SHIFT_POS_QR, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nShift_QR_Pos = _ttoi(chReadData);


	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_BT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nStickZ_BT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_LT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nStickZ_LT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_UT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nStickZ_UT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_TT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nStickZ_TT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_UT02, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nStickZ_UT02_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_QR, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nStickZ_QR_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_ALIGN, "", chReadData, MAX_PATH, SYSTEM_PARAM_INI_PATH);
	m_st_TM02_Position.nStickZ_Align_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_STICK_Z_POS_READY, "", chReadData, MAX_PATH, SYSTEM_PARAM_INI_PATH);
	m_st_TM02_Position.nStickZ_Ready_Pos = _ttoi(chReadData);


	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_PAPER_Z_POS_BT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nPaperZ_BT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_PAPER_Z_POS_LT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nPaperZ_LT01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_PAPER_Z_POS_UT01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nPaperZ_UT01_Pos = _ttoi(chReadData);

		
	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_PURGE_STICK_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nStickPurge_Time = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_PURGE_PAPER_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TM02_Position.nPaperPurge_Time = _ttoi(chReadData);


	GetPrivateProfileString(SECTION_TM02_POS_DATA, KEY_LIGHT_VALUE1_1, "", chReadData, MAX_PATH, SYSTEM_PARAM_INI_PATH);
	m_st_TM02_Position.nLightValue1_1 = _ttoi(chReadData);
	//TM02 End
	
	//TT01 Start
	GetPrivateProfileString(SECTION_TT01_POS_DATA, KEY_DRIVE_POS_READY, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TT01_Position.nDrive_Ready_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TT01_POS_DATA, KEY_DRIVE_POS_TURN, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TT01_Position.nDrive_Turn_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TT01_POS_DATA, KEY_DRIVE_POS_AM01, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TT01_Position.nDrive_AM01_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TT01_POS_DATA, KEY_ROTATE_SIDE_UP, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TT01_Position.nRotate_SideUp_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TT01_POS_DATA, KEY_ROTATE_SIDE_DN, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TT01_Position.nRotate_SideDn_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TT01_POS_DATA, KEY_T_POS_READY, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TT01_Position.nT_Ready_Pos = _ttoi(chReadData);

	GetPrivateProfileString(SECTION_TT01_POS_DATA, KEY_UP_PURGE_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TT01_Position.nUpPurge_Time = _ttoi(chReadData);
	GetPrivateProfileString(SECTION_TT01_POS_DATA, KEY_DN_PURGE_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_TT01_Position.nDnPurge_Time = _ttoi(chReadData);
	//TT01 End

	//TT01 Start
	GetPrivateProfileString(SECTION_UT02_POS_DATA, KEY_PURGE_TIME, "", chReadData, MAX_PATH,  SYSTEM_PARAM_INI_PATH);	
	m_st_UT02_Position.nPurge_Time = _ttoi(chReadData);
	//TT01 End

	return 0;
}



/*
*	Module Name	:	AOI_fnAnalyzeMsg
*	Parameter		:	Massage Parameter Pointer
*	Return			:	없음
*	Function			:	Sever로 부터 저달 되는 Command를 내부 Function과 Link 한다.(처리)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
int CVS11IndexerDlg::Sys_fnAnalyzeMsg(CMDMSG* RcvCmdMsg)
{
	VS24MotData stReturn_Mess;	
	CString strLogMessage;
	try
	{
		if(RcvCmdMsg->uTask_Dest == m_ProcInitVal.m_nTaskNum) 
		{	
			switch(RcvCmdMsg->uFunID_Dest)
			{
				//////////////////////////////////////////////////////////////////////////
				//Project Default Command.
			case nBiz_Func_System:
			{
				switch (RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_Alive_Question:
				{
					m_cpServerInterface.m_fnSendCommand_Nrs(RcvCmdMsg->uTask_Src, nBiz_Func_System, nBiz_Seq_Alive_Response, G_VS11TASK_STATE);
					break;
				}
				case nBiz_Seq_Alive_Response:
				{
					TASK_State elTempState;
					elTempState = (TASK_State)RcvCmdMsg->uUnitID_Dest;

					switch (RcvCmdMsg->uTask_Src)
					{
					case TASK_10_Master:	
						if (G_VS10TASK_STATE != elTempState)
						{
							m_fnTaskStatusChage(TASK_10_Master, elTempState);
						}
						m_nMasterStatusCount = 0;
						G_VS10TASK_STATE = elTempState;
						//상태 갱신
						break;					
					case TASK_24_Motion:		
						if (G_VS24TASK_STATE != elTempState)
						{
							m_fnTaskStatusChage(TASK_24_Motion, elTempState);
						}
						m_nMotionStatusCount = 0;
						G_VS24TASK_STATE = elTempState;
						//상태 갱신
						break;					
					default:					
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;					
					}//End Switch	
					break;
				}
				default:
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
				break;
			}
			case nBiz_Func_MotionAct: //모션에 대한 리턴 케이스
			{					
				stReturn_Mess.Reset();				

				int nMessSize = sizeof(VS24MotData);
				memcpy((char*)&stReturn_Mess, (char*)RcvCmdMsg->cMsgBuf, nMessSize);
				switch(RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_TT01_TableDrive: 						
					m_cTurnTable->m_fn_TT01_Drive_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TT01_ThetaAlign: 	
					if (G_MOTION_STATUS[TT01_THETA].B_MOTION_STATUS == IDLE)
					{
						m_cpServerInterface.m_fnSendCommand_Nrs(
							TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Stick_Align_Value_Ack, nBiz_Unit_Zero);
					}
					else
					{
						m_cTurnTable->m_fn_TT01_Theta_Return(stReturn_Mess);
					}					
					break;
				case nBiz_Seq_TT01_TableTurn: 						
					m_cTurnTable->m_fn_TM01_Rotate_Return(stReturn_Mess);
					break;
				case nBiz_Seq_BT01_CassetteUpDown: //카세트 업다운에 대한 리턴 케이스						
					m_cBoxTable->m_fn_BT01_UpDn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM01_ForwardBackward: //카세트 업다운에 대한 리턴 케이스						
					m_cBoxTransper->m_fn_TM01_Drive_Return(stReturn_Mess);
					break;			
				case nBiz_Seq_LT01_BoxLoadPush: //						
					m_cLoadingTable->m_fn_LT01_Pusher_Return(stReturn_Mess);
					break;	
				case nBiz_Seq_BO01_BoxDrive: //						
					m_cBoxOpener->m_fn_BO01_Drive_Return(stReturn_Mess);
					break;	
				case nBiz_Seq_BO01_BoxUpDn: //						
					m_cBoxOpener->m_fn_BO01_UpDn_Return(stReturn_Mess);
					break;	
				case nBiz_Seq_BO01_BoxRotate: //						
					m_cBoxOpener->m_fn_BO01_Rotate_Return(stReturn_Mess);
					break;	
				case nBiz_Seq_UT01_BoxUnLoadPush: //						
					m_cUnloadingTable->m_fn_UT01_Pusher_Return(stReturn_Mess);
					break;	
				case nBiz_Seq_TM02_PickerDrive: //						
					m_cStickTransper->m_fn_TM02_Drive_Return(stReturn_Mess);
					break;	
				case nBiz_Seq_TM02_PickerUpDown: //						
					m_cStickTransper->m_fn_TM02_UpDn_Return(stReturn_Mess);
					break;	
				case nBiz_Seq_TM02_PickerShift: //						
					m_cStickTransper->m_fn_TM02_Shift_Return(stReturn_Mess);
					break;	
				case nBiz_Seq_TM02_PickerRotate: //						
					m_cStickTransper->m_fn_TM02_Theta_Return(stReturn_Mess);
					break;	
				default:						
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
			}break;
			case nBiz_Func_CylinderAct: //실린더에 대한 리턴 케이스
			{					
				stReturn_Mess.Reset();				

				int nMessSize = sizeof(VS24MotData);
				memcpy((char*)&stReturn_Mess, (char*)RcvCmdMsg->cMsgBuf, nMessSize);

				switch(RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_TM01_FORK_UP: 						
					m_cBoxTransper->m_fn_TM01_ForkUP_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM01_FORK_DN: 						
					m_cBoxTransper->m_fn_TM01_ForkDn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_LT01_GUIDE_FWD: 						
					m_cLoadingTable->m_fn_LT01_GuideFwd_Return(stReturn_Mess);
					break;
				case nBiz_Seq_LT01_GUIDE_BWD: 						
					m_cLoadingTable->m_fn_LT01_GuideBwd_Return(stReturn_Mess);
					break;
				case nBiz_Seq_BO01_PICKER_FWD: 						
					m_cBoxOpener->m_fn_BO01_PickerFwd_Return(stReturn_Mess);
					break;
				case nBiz_Seq_BO01_PICKER_BWD: 						
					m_cBoxOpener->m_fn_BO01_PickerBwd_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT01_GUIDE_FWD: 						
					m_cUnloadingTable->m_fn_UT01_GuideFwd_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT01_GUIDE_BWD: 						
					m_cUnloadingTable->m_fn_UT01_GuideBwd_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_LR_PAPER_PICKER_UP: 						
					m_cStickTransper->m_fn_TM02_LR_PickerUp_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_LR_PAPER_PICKER_DN: 						
					m_cStickTransper->m_fn_TM02_LR_PickerDn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_CNT_PAPER_PICKER_UP: 						
					m_cStickTransper->m_fn_TM02_CT_PickerUp_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_CNT_PAPER_PICKER_DN: 						
					m_cStickTransper->m_fn_TM02_CT_PickerDn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT02_TABLE_UP: 						
					m_cEmissionTable->m_fn_UT02_TableUp_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT02_TABLE_DN: 						
					m_cEmissionTable->m_fn_UT02_TableDn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_Door_Release_Front:
					m_fnFrontDoorUnLock_Return(stReturn_Mess);
					break;
				case nBiz_Seq_Door_Lock_Front:
					m_fnFrontDoorLock_Return(stReturn_Mess);
					break;
				case nBiz_Seq_Door_Release_Side:
					m_fnSideDoorUnLock_Return(stReturn_Mess);
					break;
				case nBiz_Seq_Door_Lock_Side:
					m_fnSideDoorLock_Return(stReturn_Mess);
					break;
				default:						
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
			}break;
			case nBiz_Func_ACC: //VAC에 대한 리턴 케이스
			{					
				stReturn_Mess.Reset();							
				memcpy((char*)&stReturn_Mess, (char*)RcvCmdMsg->cMsgBuf, sizeof(VS24MotData));

				switch(RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_Read_QR:	
					if (stReturn_Mess.nResult == stReturn_Mess.Ret_Ok)  //정상처리 일 때만 버퍼 카피.
					{
						memcpy(m_cStickTransper->m_chStickQRcode, (char*)RcvCmdMsg->cMsgBuf + sizeof(VS24MotData), MAX_QRCODE);
						//m_cPickerControl->m_fnSet_StickMove_TM02_TT01_Step(STTT_TM02_READ_QR_CODE_RETURN);
					}						
					
					m_cStickTransper->m_fn_TM02_ReadQR_Return(stReturn_Mess);	
					
					break;	
				case nBiz_Seq_Light_Align:					
					m_cStickTransper->m_fn_TM02_LightSet_Return(stReturn_Mess);
					break;					
				default:					
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
			}break;
			case nBiz_Func_Vac: //VAC에 대한 리턴 케이스
			{					
				stReturn_Mess.Reset();				

				int nMessSize = sizeof(VS24MotData);
				memcpy((char*)&stReturn_Mess, (char*)RcvCmdMsg->cMsgBuf, nMessSize);

				switch(RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_TM01_Vac_ON:						
					m_cBoxTransper->m_fn_TM01_VacOn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM01_Vac_OFF:						
					m_cBoxTransper->m_fn_TM01_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM01_Purge:						
					m_cBoxTransper->m_fn_TM01_Purge_Return(stReturn_Mess);
					break;
				case nBiz_Seq_LT01_Vac_ON:						
					m_cLoadingTable->m_fn_LT01_VacOn_Return(stReturn_Mess);
					break;						
				case nBiz_Seq_LT01_Vac_OFF:						
					m_cLoadingTable->m_fn_LT01_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_LT01_Purge:						
					m_cLoadingTable->m_fn_LT01_Purge_Return(stReturn_Mess);
					break;
				case nBiz_Seq_BO01_Vac_ON:						
					m_cBoxOpener->m_fn_BO01_VacOn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_BO01_Vac_OFF:						
					m_cBoxOpener->m_fn_BO01_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_BO01_Purge:						
					m_cBoxOpener->m_fn_BO01_Purge_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT01_Vac_ON:						
					m_cUnloadingTable->m_fn_UT01_VacOn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT01_Vac_OFF:
					m_cUnloadingTable->m_fn_UT01_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT01_Purge:
					m_cUnloadingTable->m_fn_UT01_Purge_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_ST_Vac_ON:						
					m_cStickTransper->m_fn_TM02_ST_VacOn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_ST_Vac_OFF:						
					m_cStickTransper->m_fn_TM02_ST_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_ST_Purge:						
					m_cStickTransper->m_fn_TM02_ST_Purge_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_PP_Vac_ON:						
					m_cStickTransper->m_fn_TM02_PP_VacOn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_PP_Vac_OFF:						
					m_cStickTransper->m_fn_TM02_PP_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TM02_PP_Purge:						
					m_cStickTransper->m_fn_TM02_PP_Purge_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TT01_UP_Vac_ON:						
					m_cTurnTable->m_fn_TT01_UP_VacOn_Return(stReturn_Mess);		
					break;
				case nBiz_Seq_TT01_UP_Vac_OFF:						
					m_cTurnTable->m_fn_TT01_UP_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TT01_UP_Purge:						
					m_cTurnTable->m_fn_TT01_UP_Purge_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TT01_DN_Vac_ON:						
					m_cTurnTable->m_fn_TT01_DN_VacOn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TT01_DN_Vac_OFF:						
					m_cTurnTable->m_fn_TT01_DN_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_TT01_DN_Purge:						
					m_cTurnTable->m_fn_TT01_DN_Purge_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT02_Vac_ON:						
					m_cEmissionTable->m_fn_UT02_VacOn_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT02_Vac_OFF:						
					m_cEmissionTable->m_fn_UT02_VacOff_Return(stReturn_Mess);
					break;
				case nBiz_Seq_UT02_Purge:						
					m_cEmissionTable->m_fn_UT02_Purge_Return(stReturn_Mess);
					break;
				default:
					{
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					}break;
				}//End Switch
			}break;
			case nBiz_Func_Status: //VAC에 대한 리턴 케이스
			{
				stReturn_Mess.Reset();				
				memcpy((char*)&stReturn_Mess, (char*)RcvCmdMsg->cMsgBuf, sizeof(VS24MotData));

				switch (RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_ALL:
					memcpy((char*)&G_STMACHINE_STATUS, (char*)RcvCmdMsg->cMsgBuf + sizeof(VS24MotData), sizeof(STMACHINE_STATUS));
					m_fnAllStatusRelease(stReturn_Mess);	  //전체 IO 상태 갱신 부터 한다.
					m_fnBoxTableRelease();  //박스 테이블의 상태를 갱신한다.
					m_fnDrawImageView();  //전체 상태 갱신 시점에 한다. 박스 시퀀스 이벤트와 동기화 해야 한다.
					break;				
				default:				
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
			}break;
			case nBiz_Func_MasterToIndexer: //마스터에 대한 리턴 케이스
			{
// 				stReturn_Mess.Reset();
// 				memcpy((char*)&stReturn_Mess, (char*)RcvCmdMsg->cMsgBuf, sizeof(VS24MotData));
				int nAlignValue = 0;

				switch (RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_Recipe_Change_Ack:
					if (RcvCmdMsg->uUnitID_Dest == OKAY)
					{
						strLogMessage.Format("Recipe 정상 송신 확인.");
						m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
						m_cBoxControl->m_fnBoxMove_F_BT01_T_LT01_Step(BL_LT01_RECIPE_RCV_ACK);
					}						
					else
					{
						strLogMessage.Format("Recipe 송신 실패. Master PC 확인 후 재시도 하시오.");
						m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
					}
					break;
				case nBiz_Seq_Stick_Information_Ack:
					if (RcvCmdMsg->uUnitID_Dest == OKAY)
					{						
						m_cPickerControl->m_fnSet_StickMove_TT01_AM01_Step(STTA_RCV_STICK_INFO_ACK);
					}
					else
					{
						strLogMessage.Format("Stick Information 송신 실패. Master PC 확인 후 재시도 하시오.");
						m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
					}
					break;
				case nBiz_Seq_Stick_Load_Ready_OK:
					if (RcvCmdMsg->uUnitID_Dest == OKAY)
					{
						m_cPickerControl->m_fnSet_StickMove_TT01_AM01_Step(STTA_RCV_STICK_LOAD_READY_ACK);
					}
					else
					{
						strLogMessage.Format("AM01 대기 상태 확인 실패. Master PC 확인 후 재시도 하시오.");
						m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
					}
					break;
				case nBiz_Seq_Stick_Align_Value:
					memcpy((char*)&nAlignValue, (char*)RcvCmdMsg->cMsgBuf, sizeof(int));
					m_cTurnTable->m_fnTT01ThetaAlignMove(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_ThetaAlign] + nAlignValue);
					strLogMessage.Format("TT01 축 보정.");
					m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
					break;
				case nBiz_Seq_Stick_Align_End:
					if (RcvCmdMsg->uUnitID_Dest == OKAY)
					{						
						m_cPickerControl->m_fnSet_StickMove_TT01_AM01_Step(STTA_RCV_AM01_ALIGN_END);
					}
					else
					{
						strLogMessage.Format("AM01 Align 동작 실패. Master PC 확인 후 재시도 하시오.");
						m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
					}
					break;
				case nBiz_Seq_Stick_Load_Standby:
					
					m_cPickerControl->m_fnSet_StickMove_TT01_AM01_Step(STTA_RCV_AM01_LOAD_STANDBY);
					break;
				case nBiz_Seq_Stick_Load_Comp:					
					m_cPickerControl->m_fnSet_StickMove_TT01_AM01_Step(STTA_RCV_AM01_STICK_LOAD_COMP);
					break;

				case nBiz_Seq_Load_Vac_Off:					
					m_cPickerControl->m_fnSet_StickMove_TT01_AM01_Step(STTA_RCV_STICK_VAC_OFF);
					break;

				case nBiz_Seq_Load_Vac_On:					
					m_cPickerControl->m_fnSet_StickMove_TT01_AM01_Step(STTA_RCV_STICK_VAC_ON);
					break;

				case nBiz_Seq_Stick_Result:
					if (RcvCmdMsg->uUnitID_Dest == OKAY)
					{
						M_N_WORK_STICK_RESULT = OKAY;	
						strLogMessage.Format("AM01 : 정상 스틱 확인.");
						m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);											
					}
					else
					{
						M_N_WORK_STICK_RESULT = NG;
						strLogMessage.Format("AM01 : 불량 스틱 확인.");
						m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);						
					}
					m_cPickerControl->m_fnSet_StickMove_AM01_UT02_Step(STAU_RCV_STICK_RESULT);
					m_elTurnTableSequence = STICK_MOVE_F_AM01_T_UT02;
					break;
				case nBiz_Seq_Stick_Unload_Ready:									
					m_cPickerControl->m_fnSet_StickMove_AM01_UT02_Step(STAU_RCV_UNLOAD_READY);
					break;
				case nBiz_Seq_Stick_Unload_Standby:
								
					m_cPickerControl->m_fnSet_StickMove_AM01_UT02_Step(STAU_RCV_AM01_UNLOAD_STANDBY);
					break;
				case nBiz_Seq_Stick_Unload_Comp:
								
					m_cPickerControl->m_fnSet_StickMove_AM01_UT02_Step(STAU_RCV_AM01_UNLOAD_COMP);
					break;
				case nBiz_Seq_TT01_Pos_Req:
					if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
						m_st_TT01_Position.nDrive_Ready_Pos) <= POSITION_OFFSET)
					{
						g_cpServerInterface->m_fnSendCommand_Nrs(
							TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_UT02, nBiz_Unit_Zero);
					}
					else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
						m_st_TT01_Position.nDrive_Turn_Pos) <= POSITION_OFFSET)
					{
						g_cpServerInterface->m_fnSendCommand_Nrs(
							TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_Turn, nBiz_Unit_Zero);
					}
					else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
						m_st_TT01_Position.nDrive_AM01_Pos) <= POSITION_OFFSET)
					{
						g_cpServerInterface->m_fnSendCommand_Nrs(
							TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_AM01, nBiz_Unit_Zero);
					}

					break;
				default:
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
			}break;
			default:
				{
					throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
				}break;
			}//End Switch
		}
		else
		{
			throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
		}
	}
	catch (...)
	{
		throw; 
	}
	return OKAY;
}


void CVS11IndexerDlg::m_fnAllStatusRead()
{
	CString strLogMessage;
	
	if (G_MOTION_STATUS[ALL_STATUS].B_MOTION_STATUS == IDLE)
	{
		m_cpServerInterface.m_fnSendCommand_Nrs(
			TASK_24_Motion, nBiz_Func_Status, nBiz_Seq_ALL, nBiz_Unit_Zero);

		G_MOTION_STATUS[ALL_STATUS].B_MOTION_STATUS = RUNNING;	

		m_nAllStatusCount = 0;
	}
	else
	{
		m_nAllStatusCount += THREAD_INTERVAL;
		if(m_nAllStatusCount >= ALL_STATUS_TIMER)
		{			
			G_MOTION_STATUS[ALL_STATUS].B_MOTION_STATUS = IDLE;
		}
	}
}

void CVS11IndexerDlg::m_fnAllStatusRelease(VS24MotData stReturn_Mess)
{		
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:	
		//strLogMessage = "ALL_STATUS : 인덱서 상태 갱신.";
		//m_fnLogDisplay(strLogMessage, TEXT_COLOR_GREEN);	
		break;
	case stReturn_Mess.Ret_Error:		
		strLogMessage = "ALL_STATUS : 인덱서 상태 갱신 에러.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);		
		return;
	case stReturn_Mess.Ret_TimeOut:	
		strLogMessage.Format("ALL_STATUS : 타임아웃 발생.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);		
		return;
	case stReturn_Mess.Ret_NowRun:
		Sleep(100);				
		return;
	default:	
		strLogMessage.Format("ALL_STATUS : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);		
		return;
	}
	
	if(G_STMACHINE_STATUS.stNormalStatus.In04_Sn_IDXFrontDoorLock == OFF)
	{
		m_ctrBtnFrontDoor.SetRoundButtonStyle(&m_tStyle1);
		SetDlgItemText(IDC_BTN_PROC_DOOR, "LOCK");
	}		
	else
	{
		m_ctrBtnFrontDoor.SetRoundButtonStyle(&m_tStyle2);
		SetDlgItemText(IDC_BTN_PROC_DOOR, "UNLOCK");
	}


	m_cDisplayWindow1->m_fnDisplaySignal();
	m_cDisplayWindow2->m_fnDisplaySignal();
	m_cBoxTransper->m_fnDisplaySignal();
	m_cLoadingTable->m_fnDisplaySignal();
	m_cBoxOpener->m_fnDisplaySignal();
	m_cUnloadingTable->m_fnDisplaySignal();
	m_cStickTransper->m_fnDisplaySignal();
	m_cTurnTable->m_fnDisplaySignal();
	m_cEmissionTable->m_fnDisplaySignal();
	m_fnBT01PositionRelease();
	m_fnTM01PositionRelease();
	m_fnLT01PositionRelease();
	m_fnBO01PositionRelease();
	m_fnUT01PositionRelease();
	m_fnTM02PositionRelease();
	m_fnTT01PositionRelease();

	G_MOTION_STATUS[ALL_STATUS].B_MOTION_STATUS = IDLE;
	return;
}

void CVS11IndexerDlg::m_fnBT01PositionRelease()
{
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nBadBoxPosition) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_BAD);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nBox1Position) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_1);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nBox2Position) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_2);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nBox3Position) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_3);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nBox4Position) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_4);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nBox5Position) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_5);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nBox6Position) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_6);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nBox7Position) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_7);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown] -
		m_st_BT01_Position.nReadyPosition) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_TABLE_POS_WORK);
	}
	else
		m_cDisplayWindow1->m_fnDisplayPosition_BT01_UpDn(BT01_SIGNAL_COUNT);

	int nOldPos = m_cBoxTable->GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);
	int nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_BT01_CassetteUpDown];

	if (nOldPos != nNewPos)
	{
		m_cBoxTable->SetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cBoxTable->SetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS, nNewPos);
	}
}

void CVS11IndexerDlg::m_fnTM01PositionRelease()
{
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
		m_st_TM01_Position.nBT01_FwdPos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_TM01_Drive(TM01_FORK_FWD);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
		m_st_TM01_Position.nLT01_BwdPos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_TM01_Drive(TM01_FORK_BWD);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward] -
		m_st_TM01_Position.nTM01_Ready_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_TM01_Drive(TM01_FORK_READY);
	}
	else
		m_cDisplayWindow1->m_fnDisplayPosition_TM01_Drive(TM01_SIGNAL_COUNT);


	int nOldPos = m_cBoxTransper->GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	int nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_TM01_ForwardBackward];

	if (nOldPos != nNewPos)
	{
		m_cBoxTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}

	if(nNewPos == 0)
	{
		m_cBoxTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}
}

void CVS11IndexerDlg::m_fnLT01PositionRelease()
{
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush] -
		m_st_LT01_Position.nGuide_Ready_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_LT01_Pusher(LT01_PUSHER_READY);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush] -
		m_st_LT01_Position.nGuide_Small_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_LT01_Pusher(LT01_PUSHER_SMALL);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush] -
		m_st_LT01_Position.nGuide_Mid_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_LT01_Pusher(LT01_PUSHER_MID);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush] -
		m_st_LT01_Position.nGuide_Large_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_LT01_Pusher(LT01_PUSHER_LARGE);
	}
	else
		m_cDisplayWindow1->m_fnDisplayPosition_LT01_Pusher(LT01_SIGNAL_COUNT);


	int nOldPos = m_cLoadingTable->GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	int nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_LT01_BoxLoadPush];

	if (nOldPos != nNewPos)
	{
		m_cLoadingTable->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}

	if(nNewPos == 0)
	{
		m_cLoadingTable->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}
}

void CVS11IndexerDlg::m_fnBO01PositionRelease()
{
	//BO01 Drive
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
		m_st_BO01_Position.nDrive_LT01_Small_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Drive(BO01_POS_LT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
		m_st_BO01_Position.nDrive_LT01_Mid_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Drive(BO01_POS_LT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
		m_st_BO01_Position.nDrive_LT01_Large_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Drive(BO01_POS_LT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
		m_st_BO01_Position.nDrive_UT01_Small_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Drive(BO01_POS_UT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
		m_st_BO01_Position.nDrive_UT01_Mid_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Drive(BO01_POS_UT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive] -
		m_st_BO01_Position.nDrive_UT01_Large_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Drive(BO01_POS_UT01);
	}
	else
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Drive(BO01_SIGNAL_COUNT);

	int nOldPos = m_cBoxOpener->GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	int nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxDrive];

	if (nOldPos != nNewPos)
	{
		m_cBoxOpener->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cBoxOpener->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}

	//BO01 UpDn
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
		m_st_BO01_Position.nZ_LT01_Dn_Cover_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_UpDn(BO01_POS_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
		m_st_BO01_Position.nZ_LT01_Up_Cover_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_UpDn(BO01_POS_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
		m_st_BO01_Position.nZ_UT01_Dn_Cover_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_UpDn(BO01_POS_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
		m_st_BO01_Position.nZ_UT01_Up_Cover_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_UpDn(BO01_POS_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn] -
		m_st_BO01_Position.nZ_Ready_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_UpDn(BO01_POS_UP);
	}
	else
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_UpDn(BO01_SIGNAL_COUNT);

	nOldPos = m_cBoxOpener->GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);
	nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxUpDn];

	if (nOldPos != nNewPos)
	{
		m_cBoxOpener->SetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cBoxOpener->SetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS, nNewPos);
	}

	//BO01 Rotate
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate] -
		m_st_BO01_Position.nR_Pad_Up_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Rotate(BO01_PAD_UP);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate] -
		m_st_BO01_Position.nR_Pad_Dn_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Rotate(BO01_PAD_DN);
	}
	else
		m_cDisplayWindow1->m_fnDisplayPosition_BO01_Rotate(BO01_SIGNAL_COUNT);

	nOldPos = m_cBoxOpener->GetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE);
	nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_BO01_BoxRotate];

	if (nOldPos != nNewPos)
	{
		m_cBoxOpener->SetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cBoxOpener->SetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE, nNewPos);
	}
}

void CVS11IndexerDlg::m_fnUT01PositionRelease()
{
	//UT01
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_UT01_BoxUnLoadPush] -
		m_st_UT01_Position.nGuide_Ready_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_UT01_Pusher(UT01_PUSHER_READY);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_UT01_BoxUnLoadPush] -
		m_st_UT01_Position.nGuide_Small_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_UT01_Pusher(UT01_PUSHER_SMALL);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_UT01_BoxUnLoadPush] -
		m_st_UT01_Position.nGuide_Mid_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_UT01_Pusher(UT01_PUSHER_MID);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_UT01_BoxUnLoadPush] -
		m_st_UT01_Position.nGuide_Large_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow1->m_fnDisplayPosition_UT01_Pusher(UT01_PUSHER_LARGE);
	}
	else
		m_cDisplayWindow1->m_fnDisplayPosition_UT01_Pusher(UT01_SIGNAL_COUNT);

	int nOldPos = m_cUnloadingTable->GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	int nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_UT01_BoxUnLoadPush];

	if (nOldPos != nNewPos)
	{
		m_cUnloadingTable->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cUnloadingTable->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}
}

void CVS11IndexerDlg::m_fnTM02PositionRelease()
{
	//TM02 Drive
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
		m_st_TM02_Position.nDrive_BT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_POS_BT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
		m_st_TM02_Position.nDrive_BT01_PP_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_POS_BT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
		m_st_TM02_Position.nDrive_LT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_POS_LT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
		m_st_TM02_Position.nDrive_UT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_POS_UT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
		m_st_TM02_Position.nDrive_UT01_PP_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_POS_UT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
		m_st_TM02_Position.nDrive_QR_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_POS_QR);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
		m_st_TM02_Position.nDrive_TT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_POS_TT01);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
		m_st_TM02_Position.nDrive_UT02_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_POS_UT02);
	}
	else
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Drive(TM02_SIGNAL_COUNT);

	int nOldPos = m_cStickTransper->GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	int nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive];

	if (nOldPos != nNewPos)
	{
		m_cStickTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cStickTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}

	//TM02 UpDn
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nStickZ_Ready_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_UP);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nStickZ_QR_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_QR);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nStickZ_Align_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_ALIGN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nStickZ_BT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nStickZ_LT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nStickZ_UT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nStickZ_TT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nStickZ_UT02_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nPaperZ_BT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nPaperZ_LT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_DN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
		m_st_TM02_Position.nPaperZ_UT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_POS_Z_DN);
	}
	else
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_UpDn(TM02_SIGNAL_COUNT);

	nOldPos = m_cStickTransper->GetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS);
	nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown];

	if (nOldPos != nNewPos)
	{
		m_cStickTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cStickTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_ZPOS, nNewPos);
	}
	//TM02 Shift
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
		m_st_TM02_Position.nShift_LT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Shift(TM02_XAXIS_READY_POS);
	}
	else
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Shift(TM02_SIGNAL_COUNT);

	nOldPos = m_cStickTransper->GetDlgItemInt(IDC_EDIT_CUR_POS_SHIFT);
	nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift];

	if (nOldPos != nNewPos)
	{
		m_cStickTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_SHIFT, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cStickTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_SHIFT, nNewPos);
	}

	//TM02 Rotate
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
		m_st_TM02_Position.nT_LT01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Tilt(TM02_THETA_READY_POS);
	}
	else
		m_cDisplayWindow2->m_fnDisplayPosition_TM02_Tilt(TM02_SIGNAL_COUNT);

	nOldPos = m_cStickTransper->GetDlgItemInt(IDC_EDIT_CUR_POS_TILT);
	nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate];

	if (nOldPos != nNewPos)
	{
		m_cStickTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_TILT, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cStickTransper->SetDlgItemInt(IDC_EDIT_CUR_POS_TILT, nNewPos);
	}
}

void CVS11IndexerDlg::m_fnTT01PositionRelease()
{
	//TT01 Drive
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
		m_st_TT01_Position.nDrive_Ready_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Drive(TT01_POS_READY);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
		m_st_TT01_Position.nDrive_Turn_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Drive(TT01_POS_TURN);
	}
	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
		m_st_TT01_Position.nDrive_AM01_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Drive(TT01_POS_AM01);
	}
	else
		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Drive(TT01_SIGNAL_COUNT);

	int nOldPos = m_cTurnTable->GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);
	int nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive];

	if (nOldPos != nNewPos)
	{
		m_cTurnTable->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cTurnTable->SetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE, nNewPos);
	}
	//TT01 Rotate
	// 	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableTurn] -
	// 		m_st_TT01_Position.nRotate_SideUp_Pos) <= POSITION_OFFSET)
	// 	{
	// 		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Rotate(TT01_POS_UP);
	// 	}
	// 	else if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableTurn] -
	// 		m_st_TT01_Position.nRotate_SideDn_Pos) <= POSITION_OFFSET)
	// 	{
	// 		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Rotate(TT01_POS_DN);
	// 	}	
	// 	else
	// 		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Rotate(TT01_SIGNAL_COUNT);
	nOldPos = m_cTurnTable->GetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE);
	nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableTurn];
	if (nOldPos != nNewPos)
	{
		m_cTurnTable->SetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cTurnTable->SetDlgItemInt(IDC_EDIT_CUR_POS_ROTATE, nNewPos);
	}


	//TT01 Tilt
	if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_ThetaAlign] -
		m_st_TT01_Position.nT_Ready_Pos) <= POSITION_OFFSET)
	{
		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Tilt(TT01_THETA_READY);
	}
	else
		m_cDisplayWindow2->m_fnDisplayPosition_TT01_Tilt(TT01_SIGNAL_COUNT);

	nOldPos = m_cTurnTable->GetDlgItemInt(IDC_EDIT_CUR_POS_TILT);
	nNewPos = G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_ThetaAlign];

	if (nOldPos != nNewPos)
	{
		m_cTurnTable->SetDlgItemInt(IDC_EDIT_CUR_POS_TILT, nNewPos);
	}
	if(nNewPos == 0)
	{
		m_cTurnTable->SetDlgItemInt(IDC_EDIT_CUR_POS_TILT, nNewPos);
	}

}

void CVS11IndexerDlg::m_fnBoxTableRelease()
{
	for (int bl = (int)BAD_BOX; bl < (int)BOX_COUNT; bl++)
	{
		G_BOX_STATUS[bl].elBoxSize = BOX_SIZE_NONE;
	}

	//G_BOX_STATUS
	//1단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In00_Sn_BoxTable1_100mm == ON)
		G_BOX_STATUS[BOX1].elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In01_Sn_BoxTable1_200mm == ON)
		G_BOX_STATUS[BOX1].elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In02_Sn_BoxTable1_440mm == ON)
		G_BOX_STATUS[BOX1].elBoxSize = BOX_SIZE_LARGE;

	//2단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In03_Sn_BoxTable2_100mm == ON)
		G_BOX_STATUS[BOX2].elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In04_Sn_BoxTable2_200mm == ON)
		G_BOX_STATUS[BOX2].elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In05_Sn_BoxTable2_440mm == ON)
		G_BOX_STATUS[BOX2].elBoxSize = BOX_SIZE_LARGE;

	//3단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In06_Sn_BoxTable3_100mm == ON)
		G_BOX_STATUS[BOX3].elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In07_Sn_BoxTable3_200mm == ON)
		G_BOX_STATUS[BOX3].elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In08_Sn_BoxTable3_440mm == ON)
		G_BOX_STATUS[BOX3].elBoxSize = BOX_SIZE_LARGE;


	//4단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In09_Sn_BoxTable4_100mm == ON)
		G_BOX_STATUS[BOX4].elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In10_Sn_BoxTable4_200mm == ON)
		G_BOX_STATUS[BOX4].elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In11_Sn_BoxTable4_440mm == ON)
		G_BOX_STATUS[BOX4].elBoxSize = BOX_SIZE_LARGE;


	//5단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In12_Sn_BoxTable5_100mm == ON)
		G_BOX_STATUS[BOX5].elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In13_Sn_BoxTable5_200mm == ON)
		G_BOX_STATUS[BOX5].elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In14_Sn_BoxTable5_440mm == ON)
		G_BOX_STATUS[BOX5].elBoxSize = BOX_SIZE_LARGE;


	//6단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In15_Sn_BoxTable6_100mm == ON)
		G_BOX_STATUS[BOX6].elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In16_Sn_BoxTable6_200mm == ON)
		G_BOX_STATUS[BOX6].elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In17_Sn_BoxTable6_440mm == ON)
		G_BOX_STATUS[BOX6].elBoxSize = BOX_SIZE_LARGE;


	//7단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In18_Sn_BoxTable7_100mm == ON)
		G_BOX_STATUS[BOX7].elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In19_Sn_BoxTable7_200mm == ON)
		G_BOX_STATUS[BOX7].elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In20_Sn_BoxTable7_440mm == ON)
		G_BOX_STATUS[BOX7].elBoxSize = BOX_SIZE_LARGE;


	//불량단

	if (G_STMACHINE_STATUS.stBoxTableSensor.In21_Sn_BoxTableF_100mm == ON)
		G_BOX_STATUS[BAD_BOX].elBoxSize = BOX_SIZE_SMALL;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In22_Sn_BoxTableF_200mm == ON)
		G_BOX_STATUS[BAD_BOX].elBoxSize = BOX_SIZE_MEDIUM;

	if (G_STMACHINE_STATUS.stBoxTableSensor.In23_Sn_BoxTableF_440mm == ON)
		G_BOX_STATUS[BAD_BOX].elBoxSize = BOX_SIZE_LARGE;


	for (int bl = (int)BAD_BOX; bl < (int)BOX_COUNT; bl++)
	{
		if (G_BOX_STATUS[bl].elBoxSize != BOX_SIZE_NONE)
		{
			if(G_BOX_STATUS[bl].elBoxStatus == BOX_EMPTY)  //정의천
			      m_fnBoxStatusChange((BOX_LIST)bl, BOX_NORMAL);
			//G_BOX_STATUS[bl].elBoxStatus = BOX_NORMAL;
		}
		else
		{
			m_fnBoxStatusChange((BOX_LIST)bl, BOX_EMPTY);
			//G_BOX_STATUS[bl].elBoxStatus = BOX_EMPTY;
		}
	}

}

void CVS11IndexerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (STATUS_DISP_TIMER == nIDEvent)
	{
		in_fn_TaskStateChange(this, &m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		
		switch (G_VS11TASK_STATE)
		{
		case TASK_STATE_NONE:
			G_VS11TASK_STATE = TASK_STATE_INIT;
			break;
		case TASK_STATE_INIT:
			G_VS11TASK_STATE = TASK_STATE_IDLE;
			break;
		case TASK_STATE_IDLE:
			G_VS11TASK_STATE = TASK_STATE_RUN;
			break;
		case TASK_STATE_RUN:
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			break;
		case TASK_STATE_ERROR:
			G_VS11TASK_STATE = TASK_STATE_PM;
			break;
		case TASK_STATE_PM:
			G_VS11TASK_STATE = TASK_STATE_NONE;
			break;
		}		
	}
	__super::OnTimer(nIDEvent);
}

void CVS11IndexerDlg::m_fnTaskStatusChage(int nTaskNumber, TASK_State elTempStatus)
{
	CString strLogMessage;
	switch (elTempStatus)
	{
	case TASK_STATE_NONE:
	{	
		strLogMessage.Format("TASK No.%2d : 상태 변경 to NONE.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
		break;
	}
	case TASK_STATE_INIT:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to INIT.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		break;
		
	}
	case TASK_STATE_IDLE:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to IDLE.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_COLOR_GREEN);
		break;
	}
	case TASK_STATE_RUN:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to RUN.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_COLOR_GREEN);
		break;
	}
	case TASK_STATE_ERROR:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to ERROR.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		break;
	}
	case TASK_STATE_PM:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to PM.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		break;
	}
	default:
		break;
	}

}

//데모용
void CVS11IndexerDlg::OnBnClickedBtnDemoPaper()
{
	m_elDemoSequence = DEMO_PAPER_MOVE_F_LT01_T_UT01;
	m_cDemoSeq->m_elPaperMoveStep = PM_TM02_DRIVE_LT01_POS;
}
//데모용
void CVS11IndexerDlg::OnBnClickedBtnDemoStick()
{
	m_elDemoSequence = DEMO_STICK_MOVE_F_LT01_T_UT01;
	m_cDemoSeq->m_elStickMoveStep = SM_TM02_DRIVE_LT01_POS;
}

void CVS11IndexerDlg::m_fnBoxStatusChange(BOX_LIST elBoxList, BOX_STATUS elNewStatus)
{
	BOX_STATUS elOldStatus;
	elOldStatus = G_BOX_STATUS[elBoxList].elBoxStatus;

	switch (elNewStatus)
	{
	case BOX_EMPTY:
		switch (elOldStatus)
		{
		case BOX_EMPTY:			
		case BOX_NORMAL:			
		case BOX_COMP:			
		case BOX_CANCEL:
			G_BOX_STATUS[elBoxList].elBoxStatus = elNewStatus;
			break;
		default:			
			break;
		}
		break;
	case BOX_NORMAL:
		switch (elOldStatus)
		{
		case BOX_EMPTY:
		case BOX_NORMAL:
		case BOX_READY:
			G_BOX_STATUS[elBoxList].elBoxStatus = elNewStatus;
			break;
		default:
			break;
		}
		break;
	case BOX_READY:
		switch (elOldStatus)
		{		
		case BOX_NORMAL:
		case BOX_COMP:
		case BOX_CANCEL:
			G_BOX_STATUS[elBoxList].elBoxStatus = elNewStatus;
			break;
		default:
			break;
		}
		break;
	case BOX_RUN:
		switch (elOldStatus)
		{
		case BOX_READY:		
			G_BOX_STATUS[elBoxList].elBoxStatus = elNewStatus;
			break;
		default:
			break;
		}
		break;
	case BOX_COMP:
		switch (elOldStatus)
		{
		case BOX_RUN:
			G_BOX_STATUS[elBoxList].elBoxStatus = elNewStatus;
			break;
		default:
			break;
		}
		break;
	case BOX_CANCEL:
		switch (elOldStatus)
		{		
		case BOX_READY:
		case BOX_NORMAL:
		case BOX_COMP:
		case BOX_CANCEL:
			G_BOX_STATUS[elBoxList].elBoxStatus = elNewStatus;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}

	CString	strLogMessage;
	if (elOldStatus != elNewStatus && G_BOX_STATUS[elBoxList].elBoxStatus == elNewStatus)
	{
		strLogMessage.Format("No.%d Box Status Change. F[%s], T[%s]", elBoxList, g_lpsBoxStatus[elOldStatus], g_lpsBoxStatus[elNewStatus]);
		m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
	}
}

unsigned int __stdcall CVS11IndexerDlg::THREAD_MAIN_PROCESS(LPVOID pParam)
{
	STHREAD_PARAM *pProThreadData = (STHREAD_PARAM*)pParam;

	CVS11IndexerDlg* cVS11IndexerDlg = nullptr;
	CInterServerInterface*  cServerInterface = nullptr;

	cVS11IndexerDlg = (CVS11IndexerDlg*)pProThreadData->cObjectPointer1;
	cServerInterface = (CInterServerInterface*)pProThreadData->cObjectPointer2;

	int nCount = 0;
	CString strLogMessage;

	//int m_nMasterStatusCount;  //마스터 상태 갱신에 대한 시간 체크
	//int m_nMotionStatusCount; //모션타스크 상태 갱신에 대한 시간 체크


	while (pProThreadData->bThreadFlag)
	{
		Sleep(pProThreadData->nScanInterval);

		if (G_VS24TASK_STATE == TASK_STATE_IDLE ||
			G_VS24TASK_STATE == TASK_STATE_RUN)
		{
			cVS11IndexerDlg->m_fnAllStatusRead();
		}

		if (cVS11IndexerDlg->m_nMasterStatusCount == TASK_STATUS_TIMER)
		{
			cServerInterface->m_fnSendCommand_Nrs(
				TASK_10_Master, nBiz_Func_System, nBiz_Seq_Alive_Question, nBiz_Unit_Zero);
		}

		if (cVS11IndexerDlg->m_nMotionStatusCount == TASK_STATUS_TIMER)
		{
			cServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_System, nBiz_Seq_Alive_Question, nBiz_Unit_Zero);
		}

		//Disconnect 처리
		if (cVS11IndexerDlg->m_nMasterStatusCount == TASK_STATUS_DISCONNECT)
		{
			cVS11IndexerDlg->m_nMasterStatusCount = 0;

			if (G_VS10TASK_STATE != TASK_STATE_ERROR)
			{
				G_VS10TASK_STATE = TASK_STATE_ERROR;
				strLogMessage.Format("TASK No.10 : Master 프로그램이 연결되어있지 않음. 상태 변경 to ERROR.");
				cVS11IndexerDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			}
		}
		//Disconnect 처리
		if (cVS11IndexerDlg->m_nMotionStatusCount == TASK_STATUS_DISCONNECT)
		{
			cVS11IndexerDlg->m_nMotionStatusCount = 0;

			if (G_VS24TASK_STATE != TASK_STATE_ERROR)
			{
				G_VS24TASK_STATE = TASK_STATE_ERROR;
				strLogMessage.Format("TASK No.24 : Motion 프로그램이 연결되어있지 않음. 상태 변경 to ERROR.");
				cVS11IndexerDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			}
		}

		cVS11IndexerDlg->m_nMasterStatusCount += THREAD_INTERVAL;
		cVS11IndexerDlg->m_nMotionStatusCount += THREAD_INTERVAL;

		cVS11IndexerDlg->m_cDisplayWindow1->m_fnBoxSelectBtn_Release();

		// 		if (nCount == 0)
		// 		{
		// 			cVS11IndexerDlg->m_cDisplayWindow1->m_fnDisplayAllOn();
		// 			cVS11IndexerDlg->m_cDisplayWindow2->m_fnDisplayAllOn();
		// 			nCount = 1;
		// 		}
		// 		else
		// 		{
		// 			cVS11IndexerDlg->m_cDisplayWindow1->m_fnDisplayAllOff();
		// 			cVS11IndexerDlg->m_cDisplayWindow2->m_fnDisplayAllOff();
		// 			nCount = 0;
		// 		}			
	}
	_endthreadex(0);
	return 0;
}

unsigned int __stdcall CVS11IndexerDlg::THREAD_MAIN_SEQUENCE(LPVOID pParam)
{
	STHREAD_PARAM *pProThreadData = (STHREAD_PARAM*)pParam;

	CVS11IndexerDlg* cVS11IndexerDlg = nullptr;
	CInterServerInterface*  cServerInterface = nullptr;

	cVS11IndexerDlg = (CVS11IndexerDlg*)pProThreadData->cObjectPointer1;
	cServerInterface = (CInterServerInterface*)pProThreadData->cObjectPointer2;

	int nCount = 0;
	int nNextBoxIndex = 0;
	int nNextObjectIndex = 0;
	int nFirstPapperCount=0;

	CString strLogMessage;

	while (pProThreadData->bThreadFlag)
	{
		Sleep(pProThreadData->nScanInterval);

		if (G_AUTO_MODE_FLAG == TRUE)
		{
			switch (cVS11IndexerDlg->m_elMainSequence)
			{
			case MAIN_SEQUENCE_IDLE:
				break;
			case BOX_LOAD_SEQUENCE:
				if (G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_BT01_T_LT01] == SEQ_IDLE)
				{
					nNextBoxIndex = cVS11IndexerDlg->m_cBoxControl->m_fnGetNextBoxIndex(BOX_READY);

					if (nNextBoxIndex == 0)
					{
						strLogMessage.Format("MAIN_SEQUENCE : 검사를 진행 할 박스가 없습니다.");
						cVS11IndexerDlg->m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
						cVS11IndexerDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;
						break;
					}

					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_BT01_T_LT01] = SEQ_RUNNING;

					cVS11IndexerDlg->m_cBoxControl->m_fnSet_Current_Box_Index(nNextBoxIndex);
					cVS11IndexerDlg->m_elBoxSequence = BOX_MOVE_F_BT01_T_LT01;
					cVS11IndexerDlg->m_cBoxControl->m_elBoxLoadStep = BL_LT01_RECIPE_SEND;
					break;
				}
				
				if (G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_BT01_T_LT01] == SEQ_COMPLETE && 
					G_EL_SUB_SEQ_STATUS[BOX_COVER_OPEN] == SEQ_IDLE)
				{
					G_EL_SUB_SEQ_STATUS[BOX_COVER_OPEN] = SEQ_RUNNING;
					
					cVS11IndexerDlg->m_elBoxSequence = BOX_COVER_OPEN;
					cVS11IndexerDlg->m_cBoxControl->m_elCoverOpenStep = CO_BO01_INTERLOCK_CHECK;
					break;
				}

				if (G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_BT01_T_LT01] == SEQ_COMPLETE &&
					G_EL_SUB_SEQ_STATUS[BOX_COVER_OPEN] == SEQ_COMPLETE)
				{
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_BT01_T_LT01] = SEQ_IDLE;
					G_EL_SUB_SEQ_STATUS[BOX_COVER_OPEN] = SEQ_IDLE;

					cVS11IndexerDlg->m_fnLoadingTableObjectInit();
					cVS11IndexerDlg->m_elMainSequence = STICK_PAPER_SEQUENCE;
					break;
				}

				break;
			case STICK_PAPER_SEQUENCE:
				nNextObjectIndex = cVS11IndexerDlg->m_fnGetCurrentObjectIndex();

				switch (nNextObjectIndex)
				{
				case 0: //0번 간지에 대한 시나리오 동작.
					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;
						
						cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_UT01;
						cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_UT01 = PPU_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_IDLE;

						G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
						break;
					}

					break;
				case 1: //1번 간지에 대한 시나리오 동작.
					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_BT01;
						cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_BT01 = PPB_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_IDLE;
		
						G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
						break;
					}

					break;				
				case 2: //1번 스틱에 대한 시나리오 동작.
					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_LT01_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_LT01_TM02 = STLT_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_TT01;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_TT01 = STTT_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_COMPLETE)
					{
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_IDLE;

						cVS11IndexerDlg->m_elTurnTableSequence = STICK_MOVE_F_TT01_T_AM01;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TT01_AM01 = STTA_TT01_INTERLOCK_CHECK;

						G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
						break;
					}
					break;
				case 3:					
				case 4:
					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_UT01;
						cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_UT01 = PPU_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_IDLE;
						G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
						break;
					}
					break;
				case 5:
				case 8:
				case 11:
				case 14:
				case 17:
				case 20:
				case 23:
				case 26:
					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_LT01_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_LT01_TM02 = STLT_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_TT01;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_TT01 = STTT_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_IDLE)
					{										
						cVS11IndexerDlg->m_elTurnTableSequence = STICK_MOVE_F_TT01_T_AM01;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TT01_AM01 = STTA_TT01_INTERLOCK_CHECK;

						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_RUNNING;
						cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_UT02_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_UT02_TM02 = STUT_TT01_INTERLOCK_CHECK;
						break;
					}

					if (cVS11IndexerDlg->M_N_WORK_STICK_RESULT == NG)
					{
						if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_IDLE)
						{	
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;
							cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_BT01;
							cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_BT01 = STTB_TM02_INTERLOCK_CHECK;							
							break;
						}

						if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE)
						{
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] = SEQ_IDLE;
							G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
							break;
						}
					}
					else
					{
						if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
						{
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;
							cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_UT01;
							cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_UT01 = STTU_TM02_INTERLOCK_CHECK;
							break;
						}

						if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE)
						{
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] = SEQ_IDLE;
							G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
							break;
						}
					}

					break;
				case 6:
				case 7:
				case 9:
				case 10:
				case 12:
				case 13:
				case 15:
				case 16:
				case 18:
				case 19:
				case 21:
				case 22:
				case 24:
				case 25:
				case 27:
					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
						break;
					}


					if (cVS11IndexerDlg->M_N_WORK_STICK_RESULT == NG)
					{
						if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_IDLE)
						{
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;

							cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_BT01;
							cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_BT01 = PPB_TM02_INTERLOCK_CHECK;
							break;
						}

						if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE)
						{
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_IDLE;
							G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;

							if(nNextObjectIndex==27) //정의천 추가
							{
								cVS11IndexerDlg->m_elMainSequence=MAIN_SEQUENCE_WAIT;
							}

							break;
						}
					}
					else
					{
						if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
						{
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;

							cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_UT01;
							cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_UT01 = PPU_TM02_INTERLOCK_CHECK;
							break;
						}

						if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE)
						{
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_IDLE;
							G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
							if(nNextObjectIndex==27) //간지 처리 후 AM01 검사 완료 후 언로딩 될 때 까지 기다린다.//정의천 추가
							{
								// 간지 한장만 UT01에 덮은 다음 마지막 전장의 스틱이 UT02로 나올 때까지 WAIT 걸어야 함.
								// -> MAIN SEQUENCE를 STICK_PAPER_SEQUENCE-> MAIN_SEQUENCE_WAIT 로 변경 함(특별 기능 없음)
								// WAITE 해제는 AM01->UT01 SEQUENCE 완료 후 WAITE 상태 이면 
								cVS11IndexerDlg->m_elMainSequence=MAIN_SEQUENCE_WAIT;
							}
							break;
						}
					}
					break;						
				case 28:/// 로딩 및 검사 종료 후까지 기다려야함
					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_AM01_T_UT02] == SEQ_COMPLETE)
					{
					//	if (G_STMACHINE_STATUS.stEmissionTable.In04_Sn_OutputTableVacuum == ON)
					//	{
							if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_IDLE)
							{
								G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_RUNNING;
								cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_UT02_T_TM02;
								cVS11IndexerDlg->m_cPickerControl->m_elSTMove_UT02_TM02 = STUT_TT01_INTERLOCK_CHECK_STEP27;
								break;
							}
							
							if (cVS11IndexerDlg->M_N_WORK_STICK_RESULT == NG)
							{
								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&									
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;
									cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_BT01;
									cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_BT01 = STTB_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
								{									
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

									cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
									cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;

									cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_BT01;
									cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_BT01 = PPB_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE)
								{
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_IDLE;
									G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
									break;
								}
							}
							else
							{
								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;
									cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_UT01;
									cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_UT01 = STTU_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
								{									
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

									cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
									cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;

									cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_UT01;
									cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_UT01 = PPU_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE)
								{
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_IDLE;
									G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
									break;
								}
							}
						//}
					}
					break;
				case 29: // 마지막 스틱 로딩
					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_LT01_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_LT01_TM02 = STLT_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_TT01;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_TT01 = STTT_TM02_INTERLOCK_CHECK;
						break;
					}

					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] == SEQ_COMPLETE)
					{
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
						G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_IDLE;

						cVS11IndexerDlg->m_elTurnTableSequence = STICK_MOVE_F_TT01_T_AM01;
						cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TT01_AM01 = STTA_TT01_INTERLOCK_CHECK;
						G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
						
						cVS11IndexerDlg->m_elMainSequence=MAIN_SEQUENCE_WAIT; // 마지막 스틱 검사 완료 후 언로딩 까지 기다린다.//정의천 추가
						
						break;
					}
					break;
				case 30: /// 로딩 및 검사 종료 후까지 기다려야함.  
					if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_AM01_T_UT02] == SEQ_COMPLETE)
					{
						//if (G_STMACHINE_STATUS.stTurnTableSensor.In05_Sn_TurnTableUpperPosition == ON)//????????????
						//{
							if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_IDLE)
							{
								G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_RUNNING;
								cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_UT02_T_TM02;
								cVS11IndexerDlg->m_cPickerControl->m_elSTMove_UT02_TM02 = STUT_TT01_INTERLOCK_CHECK_STEP27;
								break;
							}

							if (cVS11IndexerDlg->M_N_WORK_STICK_RESULT == NG)
							{
								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;
									cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_BT01;
									cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_BT01 = STTB_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

									cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
									cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;

									cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_BT01;
									cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_BT01 = PPB_TM02_INTERLOCK_CHECK_LAST;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE)
								{
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_IDLE;

									//cVS11IndexerDlg->m_elMainSequence = BOX_UNLOAD_SEQUENCE;
									G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
									break;
								}
							}
							else
							{
								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;
									cVS11IndexerDlg->m_elPickerSequence = STICK_MOVE_F_TM02_T_UT01;
									cVS11IndexerDlg->m_cPickerControl->m_elSTMove_TM02_UT01 = STTU_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

									cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
									cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
								{
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;

									cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_UT01;
									cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_UT01 = PPU_TM02_INTERLOCK_CHECK;//PPU_TM02_INTERLOCK_CHECK_LAST;
									break;
								}

								if (G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE)
								{
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
									G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_IDLE;
									G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;

									//cVS11IndexerDlg->m_elMainSequence = BOX_UNLOAD_SEQUENCE;
									break;
								}
							}
						//}
					}
					break;
///////////////////////////////////////////////////////////////////////////////////////////////
				case 31:
					if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_IDLE)
					{
						G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

						cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_LT01_T_TM02;
						cVS11IndexerDlg->m_cPickerControl->m_elPPMove_LT01_TM02 = PPG_TM02_INTERLOCK_CHECK;
						break;
					}


					if (cVS11IndexerDlg->M_N_WORK_STICK_RESULT == NG)
					{
						if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_IDLE)
						{
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;

							cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_BT01;
							cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_BT01 = PPB_TM02_INTERLOCK_CHECK;
							break;
						}

						if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] == SEQ_COMPLETE)
						{
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_IDLE;
							G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
							cVS11IndexerDlg->m_elMainSequence = BOX_UNLOAD_SEQUENCE;
							break;
						}
					}
					else
					{
						if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_IDLE)
						{
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;

							cVS11IndexerDlg->m_elPickerSequence = PAPER_MOVE_F_TM02_T_UT01;
							cVS11IndexerDlg->m_cPickerControl->m_elPPMove_TM02_UT01 = PPU_TM02_INTERLOCK_CHECK_LAST;//PPU_TM02_INTERLOCK_CHECK;
							break;
						}

						if (G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] == SEQ_COMPLETE &&
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] == SEQ_COMPLETE)
						{
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_IDLE;
							G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_IDLE;
							G_STLT01_OBJECT[nNextObjectIndex].unObjectStatus = FALSE;
							cVS11IndexerDlg->m_elMainSequence = BOX_UNLOAD_SEQUENCE;
							break;
						}
					}
					break;
////////////////////////////////////////////////////////////////////////////////////////////////
				default:
					break;
				}
				break;
			case BOX_UNLOAD_SEQUENCE:
				if (G_EL_SUB_SEQ_STATUS[BOX_COVER_CLOSE] == SEQ_IDLE)
				{
					G_EL_SUB_SEQ_STATUS[BOX_COVER_CLOSE] = SEQ_RUNNING;
										
					cVS11IndexerDlg->m_elBoxSequence = BOX_COVER_CLOSE;
					cVS11IndexerDlg->m_cBoxControl->m_elCoverCloseStep = CC_BO01_INTERLOCK_CHECK;
					break;
				}

				if (G_EL_SUB_SEQ_STATUS[BOX_COVER_CLOSE] == SEQ_COMPLETE &&
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_UT01_T_LT01] == SEQ_IDLE)
				{
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_UT01_T_LT01] = SEQ_RUNNING;

					cVS11IndexerDlg->m_elBoxSequence = BOX_MOVE_F_UT01_T_LT01;
					cVS11IndexerDlg->m_cBoxControl->m_elBoxMoveToLT01Step = BML_BO01_INTERLOCK_CHECK;
					break;
				}

				if (G_EL_SUB_SEQ_STATUS[BOX_COVER_CLOSE] == SEQ_COMPLETE &&
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_UT01_T_LT01] == SEQ_COMPLETE &&
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_LT01_T_BT01] == SEQ_IDLE)
				{
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_LT01_T_BT01] = SEQ_RUNNING;

					g_cpMainDlg->m_elBoxSequence = BOX_MOVE_F_LT01_T_BT01;
					g_cpMainDlg->m_cBoxControl->m_elBoxUnloadingStep = BU_TM01_INTERLOCK_CHECK;
					break;
				}

				if (G_EL_SUB_SEQ_STATUS[BOX_COVER_CLOSE] == SEQ_COMPLETE &&
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_UT01_T_LT01] == SEQ_COMPLETE &&
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_LT01_T_BT01] == SEQ_COMPLETE)
				{
					G_EL_SUB_SEQ_STATUS[BOX_COVER_CLOSE] = SEQ_IDLE;
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_UT01_T_LT01] = SEQ_IDLE;
					G_EL_SUB_SEQ_STATUS[BOX_MOVE_F_LT01_T_BT01] = SEQ_IDLE;

					cVS11IndexerDlg->m_elMainSequence = BOX_LOAD_SEQUENCE;
					break;
				}

				break;			
			case MAIN_SEQUENCE_WAIT:
				{
					nNextObjectIndex = cVS11IndexerDlg->m_fnGetCurrentObjectIndex();

					switch (nNextObjectIndex)
					{
					case 28:
						//if(G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TT01_T_AM01] ==SEQ_COMPLETE)
						//G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_AM01_T_UT02] = SEQ_COMPLETE;
						//cVS11IndexerDlg->m_elMainSequence=STICK_PAPER_SEQUENCE;
						break;
					case 30:
						break;
					default:
						break;

					}
				}
				break;
			case MAIN_SEQUENCE_COMP:
				cVS11IndexerDlg->m_elBoxSequence = SUB_SEQUENCE_IDLE;
				break;
			default:
				strLogMessage.Format("THREAD_MAIN_SEQUENCE : Sequence Index(%d) 오류입니다.", (int)cVS11IndexerDlg->m_elMainSequence);
				cVS11IndexerDlg->m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
				break;
			}
		}
	}
	_endthreadex(0);
	return 0;
}

int CVS11IndexerDlg::m_fnGetCurrentObjectIndex()
{
	int nReturnCode = MAX_OBJECT_COUNT;

	for (int nStep = 0; nStep < MAX_OBJECT_COUNT; nStep++)
	{
		if (G_STLT01_OBJECT[nStep].unObjectStatus == TRUE)
		{
			nReturnCode = nStep;
			break;
		}			
	}

	return nReturnCode;
}

unsigned int __stdcall CVS11IndexerDlg::THREAD_BOX_SEQUENCE(LPVOID pParam)
{
	STHREAD_PARAM *pProThreadData = (STHREAD_PARAM*)pParam;

	CVS11IndexerDlg* cVS11IndexerDlg = nullptr;
	CInterServerInterface*  cServerInterface = nullptr;

	cVS11IndexerDlg = (CVS11IndexerDlg*)pProThreadData->cObjectPointer1;
	cServerInterface = (CInterServerInterface*)pProThreadData->cObjectPointer2;

	int nCount = 0;
	CString strLogMessage;

	while (pProThreadData->bThreadFlag)
	{
		Sleep(pProThreadData->nScanInterval);

		switch (cVS11IndexerDlg->m_elBoxSequence)
		{
		case SUB_SEQUENCE_IDLE:
			break;
		case BOX_MOVE_F_BT01_T_LT01:
			cVS11IndexerDlg->m_cBoxControl->m_fnBoxMove_F_BT01_T_LT01();
			break;
		case BOX_COVER_OPEN:
			cVS11IndexerDlg->m_cBoxControl->m_fnBoxCoverOpen();
			break;
		case BOX_COVER_CLOSE:
			cVS11IndexerDlg->m_cBoxControl->m_fnBoxCoverClose();
			break;
		case BOX_MOVE_F_UT01_T_LT01:
			cVS11IndexerDlg->m_cBoxControl->m_fnBoxMove_F_UT01_T_LT01();
			break;
		case BOX_MOVE_F_LT01_T_BT01:
			cVS11IndexerDlg->m_cBoxControl->m_fnBoxMove_F_LT01_T_BT01();
			break;
		case SUB_SEQUENCE_COMP:
			cVS11IndexerDlg->m_elBoxSequence = SUB_SEQUENCE_IDLE;
			break;
		default:
			strLogMessage.Format("THREAD_BOX_SEQUENCE : Sequence Index(%d) 오류입니다.", (int)cVS11IndexerDlg->m_elBoxSequence);
			cVS11IndexerDlg->m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			break;
		}
	}
	_endthreadex(0);
	return 0;
}

unsigned int __stdcall CVS11IndexerDlg::THREAD_PICKER_SEQUENCE(LPVOID pParam)
{
	STHREAD_PARAM *pProThreadData = (STHREAD_PARAM*)pParam;

	CVS11IndexerDlg* cVS11IndexerDlg = nullptr;
	CInterServerInterface*  cServerInterface = nullptr;

	cVS11IndexerDlg = (CVS11IndexerDlg*)pProThreadData->cObjectPointer1;
	cServerInterface = (CInterServerInterface*)pProThreadData->cObjectPointer2;

	int nCount = 0;
	CString strLogMessage;

	while (pProThreadData->bThreadFlag)
	{
		Sleep(pProThreadData->nScanInterval);

		switch (cVS11IndexerDlg->m_elPickerSequence)
		{
		case SUB_SEQUENCE_IDLE:
			break;
		case PAPER_MOVE_F_LT01_T_TM02:
			cVS11IndexerDlg->m_cPickerControl->m_fnPaperMove_LT01_TM02();
			break;
		case PAPER_MOVE_F_TM02_T_UT01:
			cVS11IndexerDlg->m_cPickerControl->m_fnPaperMove_TM02_UT01();
			break;
		case PAPER_MOVE_F_TM02_T_BT01:
			cVS11IndexerDlg->m_cPickerControl->m_fnPaperMove_TM02_BT01();
			break;
		case STICK_MOVE_F_LT01_T_TM02:
			cVS11IndexerDlg->m_cPickerControl->m_fnStickMove_LT01_TM02();
			break;
		case STICK_MOVE_F_TM02_T_TT01:
			cVS11IndexerDlg->m_cPickerControl->m_fnStickMove_TM02_TT01();
			break;
		case STICK_MOVE_F_UT02_T_TM02:
			cVS11IndexerDlg->m_cPickerControl->m_fnStickMove_UT02_TM02();
			break;
		case STICK_MOVE_F_TM02_T_UT01:
			cVS11IndexerDlg->m_cPickerControl->m_fnStickMove_TM02_UT01();
			break;
		case STICK_MOVE_F_TM02_T_BT01:
			cVS11IndexerDlg->m_cPickerControl->m_fnStickMove_TM02_BT01();
			break;
		case SUB_SEQUENCE_COMP:
			cVS11IndexerDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
			break;
		default:
			strLogMessage.Format("THREAD_PICKER_SEQUENCE : Sequence Index(%d) 오류입니다.", (int)cVS11IndexerDlg->m_elPickerSequence);
			cVS11IndexerDlg->m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			break;
		}
	}
	_endthreadex(0);
	return 0;
}

unsigned int __stdcall CVS11IndexerDlg::THREAD_TURN_TABLE_SEQUENCE(LPVOID pParam)
{
	STHREAD_PARAM *pProThreadData = (STHREAD_PARAM*)pParam;

	CVS11IndexerDlg* cVS11IndexerDlg = nullptr;
	CInterServerInterface*  cServerInterface = nullptr;

	cVS11IndexerDlg = (CVS11IndexerDlg*)pProThreadData->cObjectPointer1;
	cServerInterface = (CInterServerInterface*)pProThreadData->cObjectPointer2;

	int nCount = 0;
	CString strLogMessage;

	while (pProThreadData->bThreadFlag)
	{
		Sleep(pProThreadData->nScanInterval);

		switch (cVS11IndexerDlg->m_elTurnTableSequence)
		{
		case SUB_SEQUENCE_IDLE:
			break;
		case STICK_MOVE_F_TT01_T_AM01://유일 하게 독립적인 동작이 가능한 시퀀스. 별도의 스텝 관리가 되어야 한다.
			cVS11IndexerDlg->m_cPickerControl->m_fnStickMove_TT01_AM01();
			break;
		case STICK_MOVE_F_AM01_T_UT02:
			cVS11IndexerDlg->m_cPickerControl->m_fnStickMove_AM01_UT02();
			break;
		case STICK_MOVE_F_TT01_T_UT02:
			cVS11IndexerDlg->m_cPickerControl->m_fnStickMove_TT01_UT02();
			break;
		case SUB_SEQUENCE_COMP:
			cVS11IndexerDlg->m_elTurnTableSequence = SUB_SEQUENCE_IDLE;
			break;
		default:
			strLogMessage.Format("THREAD_TURN_TABLE_SEQUENCE : Sequence Index(%d) 오류입니다.", (int)cVS11IndexerDlg->m_elTurnTableSequence);
			cVS11IndexerDlg->m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			break;
		}
	}
	_endthreadex(0);
	return 0;
}

unsigned int __stdcall CVS11IndexerDlg::THREAD_DEMO_SEQUENCE(LPVOID pParam)
{
	STHREAD_PARAM *pProThreadData = (STHREAD_PARAM*)pParam;

	CVS11IndexerDlg* cVS11IndexerDlg = nullptr;
	CInterServerInterface*  cServerInterface = nullptr;

	cVS11IndexerDlg = (CVS11IndexerDlg*)pProThreadData->cObjectPointer1;
	cServerInterface = (CInterServerInterface*)pProThreadData->cObjectPointer2;

	int nCount = 0;

	while (pProThreadData->bThreadFlag)
	{
		Sleep(pProThreadData->nScanInterval);

		switch (cVS11IndexerDlg->m_elDemoSequence)
		{
		case DEMO_SEQUENCE_IDLE:
			break;
		case DEMO_PAPER_MOVE_F_LT01_T_UT01:
			cVS11IndexerDlg->m_cDemoSeq->m_fnPaperMove_F_LT01_T_UT01();
			break;
		case DEMO_STICK_MOVE_F_LT01_T_UT01:
			cVS11IndexerDlg->m_cDemoSeq->m_fnStickMove_F_LT01_T_UT01();
			break;
		default:
			break;
		}
	}
	_endthreadex(0);
	return 0;
}

void CVS11IndexerDlg::OnBnClickedBtnLotStart()
{

	if (G_VS10TASK_STATE == TASK_STATE_IDLE && ( G_VS24TASK_STATE ==TASK_STATE_IDLE || G_VS24TASK_STATE == TASK_STATE_RUN))
	{;}
	else
	{
		AfxMessageBox(" Master Task(IDLE) or Motion Task( IDLE or RUN)의 상태를 \n 확인 하신 후 재시작 하십시요!");
		return;
	}
		
// 	CvPoint LeftStart, LeftEnd;
// 	CvPoint RightStart, RightEnd;
// 
// 	IplImage* pMaskLeft = cvCreateImage(cvSize(40, 40), IPL_DEPTH_8U, 1);
// 	IplImage* pMaskRight = cvCreateImage(cvSize(40, 40), IPL_DEPTH_8U, 1);
// 	cvSetZero(pMaskLeft); cvSetZero(pMaskRight);
// 
// 	LeftStart.x = 20; LeftStart.y = 20; LeftEnd.x = 39; LeftEnd.y = 39;
// 	cvRectangle(pMaskLeft, LeftStart, LeftEnd, cvScalarAll(0xFF), -1);
// 	RightStart.x = 20; RightStart.y = 20; RightEnd.x = 0; RightEnd.y = 39;
// 	cvRectangle(pMaskRight, RightStart, RightEnd, cvScalarAll(0xFF), -1);
// 
// 	cvSaveImage("C:\\left.bmp", pMaskLeft);
// 	cvSaveImage("C:\\Right.bmp", pMaskRight);

	CString	strLogMessage;
	bool bBoxStatusError = false;
	bool bWorkBoxDetect = false;

	if(G_RECIPE_SET)
	{
		if (G_VS11TASK_STATE != TASK_STATE_IDLE)
		{
			strLogMessage.Format("설비가 IDLE 상태가 아닙니다.");
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			return;
		}

		if (G_BOX_STATUS[BAD_BOX].elBoxSize == BOX_SIZE_NONE)
		{
			strLogMessage.Format("불량 배출 박스가 없습니다. 박스를 적재해야 합니다.");
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			bBoxStatusError = true;
			m_cBoxTable->m_fnBT01UpDnMove(m_st_BT01_Position.nReadyPosition);
			return;
		}
		else
		{
			m_fnBoxStatusChange(BAD_BOX, BOX_READY);
		}

		if (!bBoxStatusError)
		{
			for (int bl = (int)BOX1; bl < (int)BOX_COUNT; bl++)
			{
				if (G_BOX_STATUS[bl].elBoxSize != BOX_SIZE_NONE)
				{
					bWorkBoxDetect = true;

					if (G_BOX_STATUS[bl].elBoxSize == G_BOX_STATUS[BAD_BOX].elBoxSize)
					{					
						m_fnBoxStatusChange((BOX_LIST)bl, BOX_READY);
						continue;
					}
					else
					{
						strLogMessage.Format("박스 사이즈가 다릅니다. 모든 박스 사이즈가 동일해야 합니다.");
						m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
						bBoxStatusError = true;
						break;
					}
				}
			}
		}

		if (bBoxStatusError)
		{
			for (int bl = (int)BOX1; bl < (int)BOX_COUNT; bl++)
			{
				if (G_BOX_STATUS[bl].elBoxSize != BOX_SIZE_NONE)
				{					
					m_fnBoxStatusChange((BOX_LIST)bl, BOX_CANCEL);				
				}
			}

			m_cBoxTable->m_fnBT01UpDnMove(m_st_BT01_Position.nReadyPosition);

			strLogMessage.Format("박스 테이블 상태 에러입니다. 작업자 위치로 이동합니다.");
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			return;
		}

		if (!bWorkBoxDetect)
		{
			m_cBoxTable->m_fnBT01UpDnMove(m_st_BT01_Position.nReadyPosition);

			strLogMessage.Format("검사를 진행할 박스가 없습니다. 작업자 위치로 이동합니다.");
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			return;
		}

		m_elMainSequence = BOX_LOAD_SEQUENCE;  //첫 박스 로드 시퀀스 시작.
		G_VS11TASK_STATE = TASK_STATE_RUN;     //Task 상태를 런 모드로 셑.
		G_AUTO_MODE_FLAG = TRUE;				//Auto Mode 시작.

		in_fn_TaskStateChange(this, &m_ctrStaticTaskInfo, TASK_STATE_RUN);
		strLogMessage.Format("LOT START.");
		m_fnAppendList(strLogMessage, TEXT_COLOR_GREEN);
	}
	else
	{
		strLogMessage.Format("BC01 : 레시피가 설정되지 않았습니다.");
		m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
	}	
}

//데모용
void CVS11IndexerDlg::OnBnClickedButton1()
{
	if (m_elDemoSequence == DEMO_SEQUENCE_IDLE)
	{
		m_elDemoSequence = DEMO_PAPER_MOVE_F_LT01_T_UT01;
		SetDlgItemText(IDC_BUTTON1, "Pause");
	}
	else
	{
		m_elDemoSequence = DEMO_SEQUENCE_IDLE;
		SetDlgItemText(IDC_BUTTON1, "Continue");
	}
}

//데모용
void CVS11IndexerDlg::OnBnClickedButton2()
{
	if (m_elDemoSequence == DEMO_SEQUENCE_IDLE)
	{
		m_elDemoSequence = DEMO_STICK_MOVE_F_LT01_T_UT01;
		SetDlgItemText(IDC_BUTTON2, "Pause");
	}
	else
	{
		m_elDemoSequence = DEMO_SEQUENCE_IDLE;
		SetDlgItemText(IDC_BUTTON2, "Continue");
	}
}

int CVS11IndexerDlg::m_fnGetRecipeList()
{	
	char chFilePath[_MAX_PATH] = { 0, };

	strncpy_s(chFilePath, _MAX_PATH, TM02_RECIPE_INI_PATH, strlen(TM02_RECIPE_INI_PATH));
	strncpy_s(&chFilePath[strlen(TM02_RECIPE_INI_PATH)], _MAX_PATH - strlen(TM02_RECIPE_INI_PATH), "*.ini", strlen("*.ini"));

	char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];

	CFileFind  finder;
	BOOL bWorking = finder.FindFile(chFilePath);
	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		_splitpath_s(finder.GetFileName(), drive, dir, fname, ext);
		if (!finder.IsDots() && !finder.IsDirectory())
		{
			m_ctrCboRecipeList.AddString(fname);
		}
	}	
	return 0;
}


void CVS11IndexerDlg::OnBnClickedBtnRecipeSet()
{
	CString strRecipeName;
	CString strFullPath;
	
	GetDlgItemText(IDC_COMBO_RECIPE, strRecipeName);	

	strFullPath = _T(TM02_RECIPE_INI_PATH) + strRecipeName + ".ini";
	
	ZeroMemory(m_st_TM02_Recipe.m_chTypeName, nTypeNameLength);

	strncpy_s(m_st_TM02_Recipe.m_chTypeName, (LPSTR)(LPCSTR)strRecipeName, strRecipeName.GetLength());
	
	m_st_TM02_Recipe.nBoxType = GetPrivateProfileInt(strRecipeName, KEY_TM02_BOX_TYPE, 0, strFullPath);
	m_st_TM02_Recipe.nStickSize = GetPrivateProfileInt(strRecipeName, KEY_TM02_STICK_SIZE, 0, strFullPath);
	m_st_TM02_Recipe.nAM01LoadOffset = GetPrivateProfileInt(strRecipeName, KEY_AM01_LOAD_OFFSET, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_Drive_Align = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_DRIVE_ALIGN, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_Z_Align = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_Z_ALIGN, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_Drive_PP_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_DRIVE_PP_GET, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_Drive_ST_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_DRIVE_ST_GET, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_Shift_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_SHIFT_GET, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_Tilt_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_TILT_GET, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_Z_PP_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_Z_PP_GET, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_Z_ST_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_Z_ST_GET, 0, strFullPath);
	m_st_TM02_Recipe.nLT01_PP_Purge_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_PP_PURGE_GET, 0, strFullPath);
	m_st_TM02_Recipe.stLT01_Vac_PP_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_VAC_PP_GET, 0, strFullPath);
	m_st_TM02_Recipe.stLT01_Vac_ST_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_TM02_LT01_VAC_ST_GET, 0, strFullPath);
	
	m_st_TM02_Recipe.nZ_Ready_Pos = GetPrivateProfileInt(strRecipeName, KEY_TM02_Z_READY_POS, 0, strFullPath);

	m_st_TM02_Recipe.nQR_Drive_Pos = GetPrivateProfileInt(strRecipeName, KEY_TM02_QR_DRIVE_POS, 0, strFullPath);
	m_st_TM02_Recipe.nQR_Z_Pos = GetPrivateProfileInt(strRecipeName, KEY_TM02_QR_Z_POS, 0, strFullPath);
	
	m_st_TM02_Recipe.nTT01_Dirve_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_DRIVE_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nTT01_Shift_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_SHIFT_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nTT01_Tilt_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_TILT_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nTT01_Z_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_Z_ST_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nTT01_ST_Purge_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_TT01_ST_PURGE_PUT, 0, strFullPath);
	
	m_st_TM02_Recipe.nUT02_Drive_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_DRIVE_GET, 0, strFullPath);
	m_st_TM02_Recipe.nUT02_Shift_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_SHIFT_GET, 0, strFullPath);
	m_st_TM02_Recipe.nUT02_Tilt_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_TILT_GET, 0, strFullPath);
	m_st_TM02_Recipe.nUT02_Z_ST_Get = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_Z_ST_GET, 0, strFullPath);
	m_st_TM02_Recipe.stUT02_Vac_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT02_VAC_GET, 0, strFullPath);

	m_st_TM02_Recipe.nUT01_Drive_PP_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_DRIVE_PP_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nUT01_Drive_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_DRIVE_ST_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nUT01_Shift_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_SHIFT_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nUT01_Tilt_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_TILT_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nUT01_Z_PP_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_Z_PP_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nUT01_Z_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_Z_ST_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nUT01_Purge_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_UT01_PURGE_PUT, 0, strFullPath);

	m_st_TM02_Recipe.nBT01_Drive_PP_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_DRIVE_PP_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nBT01_Drive_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_DRIVE_ST_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nBT01_Shift_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_SHIFT_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nBT01_Tilt_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_TILT_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nBT01_Z_PP_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_Z_PP_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nBT01_Z_ST_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_Z_ST_PUT, 0, strFullPath);
	m_st_TM02_Recipe.nBT01_Purge_Put = GetPrivateProfileInt(strRecipeName, KEY_TM02_BT01_PURGE_PUT, 0, strFullPath);
	
	m_st_TM02_Recipe.stTT01_Vac_Table_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_TT01_VAC_GET, 0, strFullPath);
	m_st_TM02_Recipe.stUT02_Vac_Table_Get.nAirSelection = GetPrivateProfileInt(strRecipeName, KEY_UT02_VAC_GET, 0, strFullPath);


	int nMessSize = nTypeNameLength;
	UCHAR* chMsgBuf = new UCHAR[nTypeNameLength + 1];
	memset((void*)chMsgBuf, 0x00, nTypeNameLength + 1);

	memcpy((char*)chMsgBuf, m_st_TM02_Recipe.m_chTypeName, nMessSize);

	m_cpServerInterface.m_fnSendCommand_Nrs(
		TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Recipe_Change, nBiz_Unit_Zero, nMessSize, chMsgBuf);

	if(m_st_TM02_Recipe.nBoxType != 0)
		G_RECIPE_SET = TRUE;
	else
		G_RECIPE_SET = FALSE;


	delete []chMsgBuf;
}


void CVS11IndexerDlg::OnBnClickedBtnProcReset()
{
	m_fnProcessClear();
}

void CVS11IndexerDlg::OnBnClickedBtnProcDoor()
{
	if(G_STMACHINE_STATUS.stNormalStatus.In04_Sn_IDXFrontDoorLock == OFF)
	{
		m_fnFrontDoorUnLock();
		m_fnSideDoorUnLock();
	}		
	else
	{
		m_fnFrontDoorLock();
		m_fnSideDoorLock();

		//m_ctrBtnFrontDoor.SetRoundButtonStyle(&m_tStyle2);
		//SetDlgItemText(IDC_BTN_PROC_DOOR, "UNLOCK");
	}
}

void CVS11IndexerDlg::m_fnFrontDoorLock()
{
	m_stMD01_Mess.Reset();

	m_stMD01_Mess.nAction = m_stMD01_Mess.Act_None;
	m_stMD01_Mess.nResult = m_stMD01_Mess.Ret_None;	
	m_stMD01_Mess.TimeOut = TIME_OUT_5_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stMD01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[FRONT_DOOR_LOCK].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[FRONT_DOOR_LOCK].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[FRONT_DOOR_LOCK].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[FRONT_DOOR_LOCK].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[FRONT_DOOR_LOCK].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Front, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Front, nBiz_Unit_Zero
				);

			strLogMessage.Format("FRONT_DOOR_LOCK : LOCK CMD.");
			m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
			
		}
		else
		{
			strLogMessage.Format("FRONT_DOOR_LOCK : CYL. BUSY 상태.");
			m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("FRONT_DOOR_LOCK : LOCK 재시도 실패.");
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[FRONT_DOOR_LOCK].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[FRONT_DOOR_LOCK].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CVS11IndexerDlg::m_fnFrontDoorUnLock()
{
	m_stMD01_Mess.Reset();

	m_stMD01_Mess.nAction = m_stMD01_Mess.Act_None;
	m_stMD01_Mess.nResult = m_stMD01_Mess.Ret_None;	
	m_stMD01_Mess.TimeOut = TIME_OUT_5_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stMD01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[FRONT_DOOR_UNLOCK].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[FRONT_DOOR_UNLOCK].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[FRONT_DOOR_UNLOCK].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[FRONT_DOOR_UNLOCK].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[FRONT_DOOR_UNLOCK].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Front, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Front, nBiz_Unit_Zero
				);

			strLogMessage.Format("FRONT_DOOR_UNLOCK : UN-LOCK CMD.");
			m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("FRONT_DOOR_UNLOCK : CYL. BUSY 상태.");
			m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("FRONT_DOOR_UNLOCK : UN-LOCK 재시도 실패.");
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[FRONT_DOOR_UNLOCK].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[FRONT_DOOR_UNLOCK].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}


int CVS11IndexerDlg::m_fnFrontDoorLock_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[FRONT_DOOR_LOCK].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
		{
			strLogMessage = "FRONT_DOOR_LOCK : 수행 완료.";
			m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);

			G_MOTION_STATUS[FRONT_DOOR_LOCK].B_COMMAND_STATUS = COMPLETE;
			G_MOTION_STATUS[FRONT_DOOR_LOCK].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_Error:
		{
			strLogMessage = "FRONT_DOOR_LOCK : 에러 발생.";
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[FRONT_DOOR_LOCK].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_TimeOut:
		{
			strLogMessage.Format("FRONT_DOOR_LOCK : 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[FRONT_DOOR_LOCK].UN_CMD_COUNT);
			m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
			//m_fnFrontDoorLock();
		}break;
	default:
		{
			strLogMessage.Format("FRONT_DOOR_LOCK : 리턴 값 에러[%d].", stReturn_Mess.nResult);
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[FRONT_DOOR_LOCK].UN_CMD_COUNT = 0;
		}break;
	}
	return 0;
}

int CVS11IndexerDlg::m_fnFrontDoorUnLock_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[FRONT_DOOR_UNLOCK].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
		{
			strLogMessage = "FRONT_DOOR_UNLOCK : 수행 완료.";
			m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);

			G_MOTION_STATUS[FRONT_DOOR_UNLOCK].B_COMMAND_STATUS = COMPLETE;
			G_MOTION_STATUS[FRONT_DOOR_UNLOCK].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_Error:
		{
			strLogMessage = "FRONT_DOOR_UNLOCK : 에러 발생.";
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[FRONT_DOOR_UNLOCK].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_TimeOut:
		{
			strLogMessage.Format("FRONT_DOOR_UNLOCK : 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[FRONT_DOOR_UNLOCK].UN_CMD_COUNT);
			m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
			//m_fnFrontDoorUnLock();
		}break;
	default:
		{
			strLogMessage.Format("FRONT_DOOR_UNLOCK : 리턴 값 에러[%d].", stReturn_Mess.nResult);
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[FRONT_DOOR_UNLOCK].UN_CMD_COUNT = 0;
		}break;
	}
	return 0;
}

void CVS11IndexerDlg::m_fnSideDoorLock()
{
	m_stMD01_Mess.Reset();

	m_stMD01_Mess.nAction = m_stMD01_Mess.Act_None;
	m_stMD01_Mess.nResult = m_stMD01_Mess.Ret_None;	
	m_stMD01_Mess.TimeOut = TIME_OUT_5_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stMD01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[SIDE_DOOR_LOCK].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[SIDE_DOOR_LOCK].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[SIDE_DOOR_LOCK].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[SIDE_DOOR_LOCK].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[SIDE_DOOR_LOCK].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Side, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_Door_Lock_Side, nBiz_Unit_Zero
				);

			strLogMessage.Format("SIDE_DOOR_LOCK : LOCK CMD.");
			m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);

		}
		else
		{
			strLogMessage.Format("SIDE_DOOR_LOCK : CYL. BUSY 상태.");
			m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("SIDE_DOOR_LOCK : LOCK 재시도 실패.");
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[SIDE_DOOR_LOCK].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[SIDE_DOOR_LOCK].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CVS11IndexerDlg::m_fnSideDoorUnLock()
{
	m_stMD01_Mess.Reset();

	m_stMD01_Mess.nAction = m_stMD01_Mess.Act_None;
	m_stMD01_Mess.nResult = m_stMD01_Mess.Ret_None;	
	m_stMD01_Mess.TimeOut = TIME_OUT_5_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stMD01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[SIDE_DOOR_UNLOCK].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[SIDE_DOOR_UNLOCK].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[SIDE_DOOR_UNLOCK].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[SIDE_DOOR_UNLOCK].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[SIDE_DOOR_UNLOCK].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Side, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_Door_Release_Side, nBiz_Unit_Zero
				);

			strLogMessage.Format("SIDE_DOOR_UNLOCK : UN-LOCK CMD.");
			m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("SIDE_DOOR_UNLOCK : CYL. BUSY 상태.");
			m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("SIDE_DOOR_UNLOCK : UN-LOCK 재시도 실패.");
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[SIDE_DOOR_UNLOCK].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[SIDE_DOOR_UNLOCK].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}


int CVS11IndexerDlg::m_fnSideDoorLock_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[SIDE_DOOR_LOCK].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
		{
			strLogMessage = "SIDE_DOOR_LOCK : 수행 완료.";
			m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);

			G_MOTION_STATUS[SIDE_DOOR_LOCK].B_COMMAND_STATUS = COMPLETE;
			G_MOTION_STATUS[SIDE_DOOR_LOCK].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_Error:
		{
			strLogMessage = "SIDE_DOOR_LOCK : 에러 발생.";
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[SIDE_DOOR_LOCK].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_TimeOut:
		{
			strLogMessage.Format("SIDE_DOOR_LOCK : 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[SIDE_DOOR_LOCK].UN_CMD_COUNT);
			m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
			//m_fnFrontDoorLock();
		}break;
	default:
		{
			strLogMessage.Format("SIDE_DOOR_LOCK : 리턴 값 에러[%d].", stReturn_Mess.nResult);
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[SIDE_DOOR_LOCK].UN_CMD_COUNT = 0;
		}break;
	}
	return 0;
}

int CVS11IndexerDlg::m_fnSideDoorUnLock_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[SIDE_DOOR_UNLOCK].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
		{
			strLogMessage = "SIDE_DOOR_UNLOCK : 수행 완료.";
			m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);

			G_MOTION_STATUS[SIDE_DOOR_UNLOCK].B_COMMAND_STATUS = COMPLETE;
			G_MOTION_STATUS[SIDE_DOOR_UNLOCK].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_Error:
		{
			strLogMessage = "SIDE_DOOR_UNLOCK : 에러 발생.";
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[SIDE_DOOR_UNLOCK].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_TimeOut:
		{
			strLogMessage.Format("SIDE_DOOR_UNLOCK : 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[SIDE_DOOR_UNLOCK].UN_CMD_COUNT);
			m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
			//m_fnFrontDoorUnLock();
		}break;
	default:
		{
			strLogMessage.Format("SIDE_DOOR_UNLOCK : 리턴 값 에러[%d].", stReturn_Mess.nResult);
			m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[SIDE_DOOR_UNLOCK].UN_CMD_COUNT = 0;
		}break;
	}
	return 0;
}

void CVS11IndexerDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	this->MoveWindow(0, -1, 1918, 1080, TRUE);
	__super::OnLButtonDblClk(nFlags, point);
}

#define ALL_LENGTH 1
#define BLACK_LENGTH 2
#define WHITE_LENGTH 3


double CVS11IndexerDlg::m_fnGetLengthData(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType)
{
	if((abs(nStart.x - nEnd.x) - abs(nStart.y - nEnd.y)) >= 0)
	{
		return m_fnGetLengthDataLongX(gray_image, nStart, nEnd, nType);
	}
	else
	{
		return m_fnGetLengthDataLongY(gray_image, nStart, nEnd, nType);
	}
}

double CVS11IndexerDlg::m_fnGetLengthDataLongX(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType)
{
	CvPoint nPoint1; CvPoint nPoint2; CvPoint nTemp;

	if(nStart.x > nEnd.x)
	{
		nTemp = nStart;
		nStart = nEnd;
		nEnd = nTemp;
	}

	if((nEnd.x - nStart.x) == 0)
		return 0;

	double dTemp1 = (double)(nEnd.y - nStart.y) / (double)(nEnd.x - nStart.x);

	int nStepY = 0;
	double dValue = 0.0;

	for (int nStepX = nStart.x; nStepX < nEnd.x; nStepX++)
	{
		nStepY = (int)(nStart.y + (nStepX - nStart.x) * dTemp1);
		dValue = cvGetReal2D(gray_image, nStepY, nStepX);
		nPoint1.x = nStepX; nPoint1.y = nStepY;

		if ((int)dValue == 0)
		{
			//cvCircle(rgb_image, nPoint1, 10, CV_RGB(0, 255, 0), 2);
			break;
		}
	}

	for (int nStepX = nEnd.x; nStepX > nStart.x; nStepX--)
	{
		nStepY = (int)(nStart.y + (nStepX - nStart.x) * dTemp1);
		dValue = cvGetReal2D(gray_image, nStepY, nStepX);
		nPoint2.x = nStepX; nPoint2.y = nStepY;

		if ((int)dValue == 0)
		{
			//cvCircle(rgb_image, nPoint2, 10, CV_RGB(0, 255, 0), 2);
			break;
		}
	}

	double dXLength = (double)(nEnd.x - nStart.x);
	double dYLength = (double)(nEnd.y - nStart.y);
	double dLengthAll = sqrt(dXLength * dXLength + dYLength * dYLength);

	if (nType == ALL_LENGTH)
		return dLengthAll;

	dXLength = (double)(nPoint2.x - nPoint1.x);
	dYLength = (double)(nPoint2.y - nPoint1.y);
	double dLengthBlack = sqrt(dXLength * dXLength + dYLength * dYLength);

	if (nType == BLACK_LENGTH)
		return dLengthBlack;

	double dLengthWhite = dLengthAll - dLengthBlack;

	if (nType == WHITE_LENGTH)
		return dLengthWhite;

	return dLengthBlack;
}

double CVS11IndexerDlg::m_fnGetLengthDataLongY(IplImage *gray_image, CvPoint nStart, CvPoint nEnd, int nType)
{
	CvPoint nPoint1; CvPoint nPoint2; CvPoint nTemp;

	if(nStart.y > nEnd.y)
	{
		nTemp = nStart;
		nStart = nEnd;
		nEnd = nTemp;
	}

	if((nEnd.y - nStart.y) == 0)
		return 0;

	double dTemp1 = (double)(nEnd.x - nStart.x) / (double)(nEnd.y - nStart.y);

	int nStepX = 0;
	double dValue = 0.0;

	for (int nStepY = nStart.y; nStepY < nEnd.y; nStepY++)
	{
		nStepX = (int)(nStart.x + (nStepY - nStart.y) * dTemp1);
		dValue = cvGetReal2D(gray_image, nStepY, nStepX);
		nPoint1.y = nStepY; nPoint1.x = nStepX;

		if ((int)dValue == 0)
		{
			//cvCircle(rgb_image, nPoint1, 10, CV_RGB(0, 255, 0), 2);
			break;
		}
	}

	for (int nStepY = nEnd.y; nStepY > nStart.y; nStepY--)
	{
		nStepX = (int)(nStart.x + (nStepY - nStart.y) * dTemp1);
		dValue = cvGetReal2D(gray_image, nStepY, nStepX);
		nPoint2.y = nStepY; nPoint2.x = nStepX;

		if ((int)dValue == 0)
		{
			//cvCircle(rgb_image, nPoint2, 10, CV_RGB(0, 255, 0), 2);
			break;
		}
	}

	double dXLength = (double)(nEnd.x - nStart.x);
	double dYLength = (double)(nEnd.y - nStart.y);
	double dLengthAll = sqrt(dXLength * dXLength + dYLength * dYLength);

	if (nType == ALL_LENGTH)
		return dLengthAll;

	dXLength = (double)(nPoint2.x - nPoint1.x);
	dYLength = (double)(nPoint2.y - nPoint1.y);
	double dLengthBlack = sqrt(dXLength * dXLength + dYLength * dYLength);

	if (nType == BLACK_LENGTH)
		return dLengthBlack;

	double dLengthWhite = dLengthAll - dLengthBlack;

	if (nType == WHITE_LENGTH)
		return dLengthWhite;

	return dLengthBlack;
}



void CVS11IndexerDlg::OnBnClickedBtnLotRetry()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nCount = 0;
	int nNextBoxIndex = 0;
	int nNextObjectIndex = 0;
	int nFirstPapperCount=0;

//	for(int i=0, i<32, i++)
//	{ 
//		G_STLT01_OBJECT[i].unObjectStatus=TRUE;
//	}

	switch(m_elMainSequence)
	{
	case MAIN_SEQUENCE_IDLE:
		break;
	case BOX_LOAD_SEQUENCE:
		break;
	case STICK_PAPER_SEQUENCE:
		nNextObjectIndex = m_fnGetCurrentObjectIndex();

		switch (nNextObjectIndex)
		{
		case 0: 
			break;
		case 1: 
			break;
		case 2: 
			break;
		case 3:
		case 4:
			break;
		case 5:
		case 8:
		case 11:
		case 14:
		case 17:
		case 20:
		case 23:
		case 26:
			break;
		case 6:
		case 7:
		case 9:
		case 10:
		case 12:
		case 13:
		case 15:
		case 16:
		case 18:
		case 19:
		case 21:
		case 22:
		case 24:
		case 25:
		case 27:
			break;
		case 28:
			break;
		case 29:
			break;
		case 30:
			break;
		case 31:
			break;
		default:
			break;
		}
		break;
	case BOX_UNLOAD_SEQUENCE:
		break;
	case MAIN_SEQUENCE_WAIT:
		break;
	case MAIN_SEQUENCE_COMP:
		break;
	default:
		break;
	}
}
