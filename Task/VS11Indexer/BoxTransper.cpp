// BoxTransper.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "BoxTransper.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"

// CBoxTransper 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBoxTransper, CDialogEx)

CBoxTransper::CBoxTransper(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBoxTransper::IDD, pParent)
	, m_nCtrPosIndex(0)
	, m_bBCR_Connection(false)
{

}

CBoxTransper::~CBoxTransper()
{
}

void CBoxTransper::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_POS_MOVE, m_ctrBtnMove);
	DDX_Control(pDX, IDC_BTN_POS_DN, m_ctrBtnMoveDn);
	DDX_Control(pDX, IDC_BTN_POS_UP, m_ctrBtnMoveUp);
	DDX_Control(pDX, IDC_BTN_SAVE4, m_ctrBtnValueSave);
	DDX_Control(pDX, IDC_BTN_CANCEL4, m_ctrBtnSaveCancel);
	DDX_Control(pDX, IDC_BTN_INIT4, m_ctrBtnMoveDefault);
	DDX_Control(pDX, IDC_BTN_CYL_UP, m_ctrBtnCylUp);
	DDX_Control(pDX, IDC_BTN_CYL_DN, m_ctrBtnCylDn);
	DDX_Control(pDX, IDC_BTN_VAC_ON, m_ctrBtnVacOn);
	DDX_Control(pDX, IDC_BTN_VAC_OFF, m_ctrBtnVacOff);
	DDX_Control(pDX, IDC_BTN_READ, m_ctrBtnBcr);
	DDX_Control(pDX, IDC_BTN_SET, m_ctrBtnBcrSet);
	DDX_Radio(pDX, IDC_RADIO_BT01, m_nCtrPosIndex);
	DDX_Control(pDX, IDC_BTN_MOTION_STOP, m_ctrBtnTM01ActionStop);
	DDX_Control(pDX, IDC_BTN_PURGE, m_ctrBtnPurge);
}


BEGIN_MESSAGE_MAP(CBoxTransper, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_SAVE4, &CBoxTransper::OnBnClickedBtnSave4)
	ON_BN_CLICKED(IDC_BTN_CANCEL4, &CBoxTransper::OnBnClickedBtnCancel4)
	ON_BN_CLICKED(IDC_BTN_POS_MOVE, &CBoxTransper::OnBnClickedBtnPosMove)
	ON_BN_CLICKED(IDC_BTN_POS_DN, &CBoxTransper::OnBnClickedBtnPosDn)
	ON_BN_CLICKED(IDC_BTN_POS_UP, &CBoxTransper::OnBnClickedBtnPosUp)
	ON_BN_CLICKED(IDC_BTN_CYL_UP, &CBoxTransper::OnBnClickedBtnCylUp)
	ON_BN_CLICKED(IDC_BTN_CYL_DN, &CBoxTransper::OnBnClickedBtnCylDn)
	ON_BN_CLICKED(IDC_BTN_VAC_ON, &CBoxTransper::OnBnClickedBtnVacOn)
	ON_BN_CLICKED(IDC_BTN_VAC_OFF, &CBoxTransper::OnBnClickedBtnVacOff)
	ON_BN_CLICKED(IDC_BTN_READ, &CBoxTransper::OnBnClickedBtnRead)
	ON_BN_CLICKED(IDC_BTN_MOTION_STOP, &CBoxTransper::OnBnClickedBtnMotionStop)
	ON_BN_CLICKED(IDC_BTN_PURGE, &CBoxTransper::OnBnClickedBtnPurge)
	ON_BN_CLICKED(IDC_BTN_SET, &CBoxTransper::OnBnClickedBtnSet)
END_MESSAGE_MAP()


// CBoxTransper 메시지 처리기입니다.
BOOL CBoxTransper::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CBoxTransper::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_EditFont.DeleteObject();
	CDialogEx::PostNcDestroy();
}

HBRUSH CBoxTransper::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(m_btBackColor);
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CBoxTransper::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style On
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);
	
	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle1.SetButtonStyle(&tStyle);


	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyleOff.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	m_ctrBtnMove.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	LOGFONT tFont;
	m_ctrBtnMove.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;

	m_ctrBtnCylUp.SetTextColor(&tColor);
	m_ctrBtnCylUp.SetCheckButton(false);			
	m_ctrBtnCylUp.SetFont(&tFont);
	m_ctrBtnCylUp.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnCylDn.SetTextColor(&tColor);
	m_ctrBtnCylDn.SetCheckButton(false);			
	m_ctrBtnCylDn.SetFont(&tFont);
	m_ctrBtnCylDn.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOn.SetTextColor(&tColor);
	m_ctrBtnVacOn.SetCheckButton(false);			
	m_ctrBtnVacOn.SetFont(&tFont);
	m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnVacOff.SetTextColor(&tColor);
	m_ctrBtnVacOff.SetCheckButton(false);			
	m_ctrBtnVacOff.SetFont(&tFont);
	m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOff);

	m_ctrBtnBcr.SetTextColor(&tColor);
	m_ctrBtnBcr.SetCheckButton(false);			
	m_ctrBtnBcr.SetFont(&tFont);
	m_ctrBtnBcr.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnBcrSet.SetTextColor(&tColor);
	m_ctrBtnBcrSet.SetCheckButton(false);			
	m_ctrBtnBcrSet.SetFont(&tFont);
	m_ctrBtnBcrSet.SetRoundButtonStyle(&m_tStyle1);

	m_ctrBtnMove.SetTextColor(&tColor);
	m_ctrBtnMove.SetCheckButton(false);			
	m_ctrBtnMove.SetFont(&tFont);
	m_ctrBtnMove.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDn.SetTextColor(&tColor);
	m_ctrBtnMoveDn.SetCheckButton(false);			
	m_ctrBtnMoveDn.SetFont(&tFont);
	m_ctrBtnMoveDn.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveUp.SetTextColor(&tColor);
	m_ctrBtnMoveUp.SetCheckButton(false);			
	m_ctrBtnMoveUp.SetFont(&tFont);
	m_ctrBtnMoveUp.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnValueSave.SetTextColor(&tColor);
	m_ctrBtnValueSave.SetCheckButton(false);			
	m_ctrBtnValueSave.SetFont(&tFont);
	m_ctrBtnValueSave.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnSaveCancel.SetTextColor(&tColor);
	m_ctrBtnSaveCancel.SetCheckButton(false);			
	m_ctrBtnSaveCancel.SetFont(&tFont);
	m_ctrBtnSaveCancel.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnMoveDefault.SetTextColor(&tColor);
	m_ctrBtnMoveDefault.SetCheckButton(false);			
	m_ctrBtnMoveDefault.SetFont(&tFont);
	m_ctrBtnMoveDefault.SetRoundButtonStyle(&m_tStyle1);	

	m_ctrBtnPurge.SetTextColor(&tColor);
	m_ctrBtnPurge.SetCheckButton(false);			
	m_ctrBtnPurge.SetFont(&tFont);
	m_ctrBtnPurge.SetRoundButtonStyle(&m_tStyleOff);
	
	m_ctrBtnTM01ActionStop.SetTextColor(&tColor);	m_ctrBtnTM01ActionStop.SetCheckButton(false);
	m_ctrBtnTM01ActionStop.SetFont(&tFont);	m_ctrBtnTM01ActionStop.SetRoundButtonStyle(&m_tStyle1);

}

int CBoxTransper::m_fnCreateControlFont(void)
{	
// 	GetDlgItem(IDC_EDIT_BAD_POS)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS1)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS2)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS3)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS4)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS5)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS6)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_POS7)->SetFont(&m_EditFont);
// 	GetDlgItem(IDC_EDIT_READY_POS)->SetFont(&m_EditFont);

	//SetDlgItemInt(IDC_EDIT_Z_POS_UP, 100);
	//SetDlgItemInt(IDC_EDIT_Z_POS_DN, 100);
	return 0;
}

BOOL CBoxTransper::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);
	m_EditFont.CreatePointFont(100,"Arial");	

	m_fnCreateControlFont();
	m_fnCreateButton();

	m_bBCR_Connection = m_cBcrCtrl.LinkOpen();
	m_cBcrCtrl.m_fnSetParentHandle(this->GetParent()->m_hWnd);  //오브젝트 생성 전이라안됨..

	CString strLogMessage;

	if(m_bBCR_Connection)
	{
		strLogMessage.Format("TM01_BCR : 연결 성공.");
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
	}
	else
	{
		strLogMessage.Format("TM01_BCR :연결 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CBoxTransper::m_fnSetParameter(STM01_POSITION* stPointer)
{
	m_st_TM01_Pos = stPointer;	
	
	m_cBcrCtrl.m_fnSetParentHandle(this->GetParent()->m_hWnd);
	return 0;
}

int CBoxTransper::m_fnDisplayParameter(void)
{
	SetDlgItemInt(IDC_EDIT_BT01_POS, m_st_TM01_Pos->nBT01_FwdPos);
	SetDlgItemInt(IDC_EDIT_LT01_POS, m_st_TM01_Pos->nLT01_BwdPos);
	SetDlgItemInt(IDC_EDIT_TM01_READY, m_st_TM01_Pos->nTM01_Ready_Pos);
	SetDlgItemInt(IDC_EDIT_TM01_BCR_READ, m_st_TM01_Pos->mTM01_BCR_Pos);
	SetDlgItemInt(IDC_EDIT_PURGE_SEC, m_st_TM01_Pos->nPurge_Time);
	
	return 0;
}

void CBoxTransper::OnBnClickedBtnCancel4()
{
	m_fnDisplayParameter();
}

void CBoxTransper::OnBnClickedBtnSave4()
{
	CString strWriteData;

	m_st_TM01_Pos->nBT01_FwdPos = GetDlgItemInt(IDC_EDIT_BT01_POS);
	m_st_TM01_Pos->nLT01_BwdPos = GetDlgItemInt(IDC_EDIT_LT01_POS);	
	m_st_TM01_Pos->nTM01_Ready_Pos = GetDlgItemInt(IDC_EDIT_TM01_READY);	
	m_st_TM01_Pos->mTM01_BCR_Pos = GetDlgItemInt(IDC_EDIT_TM01_BCR_READ);	
	m_st_TM01_Pos->nPurge_Time = GetDlgItemInt(IDC_EDIT_PURGE_SEC);	

	strWriteData.Format("%d", m_st_TM01_Pos->nBT01_FwdPos);
	WritePrivateProfileString(SECTION_TM01_POS_DATA, KEY_FORK_BT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM01_Pos->nLT01_BwdPos);
	WritePrivateProfileString(SECTION_TM01_POS_DATA, KEY_FORK_LT01, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM01_Pos->nTM01_Ready_Pos);
	WritePrivateProfileString(SECTION_TM01_POS_DATA, KEY_TM01_READY, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM01_Pos->mTM01_BCR_Pos);
	WritePrivateProfileString(SECTION_TM01_POS_DATA, KEY_TM01_BCR_READ, strWriteData, SYSTEM_PARAM_INI_PATH);

	strWriteData.Format("%d", m_st_TM01_Pos->nPurge_Time);
	WritePrivateProfileString(SECTION_TM01_POS_DATA, KEY_PURGE_TIME, strWriteData, SYSTEM_PARAM_INI_PATH);
	
}

void CBoxTransper::OnBnClickedBtnPosMove()
{
	UpdateData(TRUE);	
	int nPosValue = 0;

	switch(m_nCtrPosIndex)
	{
	case 0:
		nPosValue = GetDlgItemInt(IDC_EDIT_BT01_POS);	break;
	case 1:
		nPosValue = GetDlgItemInt(IDC_EDIT_LT01_POS);	break;	
	case 2:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM01_READY);	break;	
	case 3:
		nPosValue = GetDlgItemInt(IDC_EDIT_TM01_BCR_READ);	break;	
	}
	
	m_fnTM01ForkMove(nPosValue);
}

void CBoxTransper::OnBnClickedBtnPosDn()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_DN);
	
	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);	
	nPosValue = nPosValue - nOffsetPos;
	m_fnTM01ForkMove(nPosValue);	
}

void CBoxTransper::OnBnClickedBtnPosUp()
{
	UpdateData(TRUE);	
	int nPosValue = 0;	

	int nOffsetPos = GetDlgItemInt(IDC_EDIT_POS_UP);

	nPosValue = GetDlgItemInt(IDC_EDIT_CUR_POS_DRIVE);

	nPosValue = nPosValue + nOffsetPos;
	m_fnTM01ForkMove(nPosValue);
	
}

void CBoxTransper::OnBnClickedBtnCylUp()
{
	m_stTM01_Mess.Reset();
	CString strLogMessage;

	if (G_STMACHINE_STATUS.stModuleStatus.In10_Sn_LoadBoxVacuum == ON)
	{
		strLogMessage.Format("TM01_CYL_UP : 로딩 테이블 Vac. 를 Off 해야 합니다.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		return;
	}

	if (G_STMACHINE_STATUS.stModuleStatus.In15_Sn_UnloadBoxDetection == ON)
	{
		strLogMessage.Format("TM01_CYL_UP : 언로딩 테이블에 박스가 감지 되었습니다.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		return;
	}

	m_stTM01_Mess.nAction = m_stTM01_Mess.Act_None;
	m_stTM01_Mess.nResult = m_stTM01_Mess.Ret_None;	
	m_stTM01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM01_Mess, nMessSize);
	
	if(G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM01_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM01_CYL].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM01_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_TM01_FORK_UP, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_TM01_FORK_UP, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM01_CYL : UP.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TM01_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM01_CYL : UP 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxTransper::OnBnClickedBtnCylDn()
{
	m_stTM01_Mess.Reset();
	CString strLogMessage;

	if (G_STMACHINE_STATUS.stModuleStatus.In22_Sn_ForkBoxVacuum == ON)
	{
		strLogMessage.Format("TM01_CYL_DN : Fork Vac. 를 Off 해야 합니다.");
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		return;
	}

	m_stTM01_Mess.nAction = m_stTM01_Mess.Act_None;
	m_stTM01_Mess.nResult = m_stTM01_Mess.Ret_None;	
	m_stTM01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM01_Mess, nMessSize);
	
	if(G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM01_CYL].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM01_CYL].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM01_CYL].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_CylinderAct, nBiz_Seq_TM01_FORK_DN, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_CylinderAct, nBiz_Seq_TM01_FORK_DN, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM01_CYL : DOWN.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TM01_CYL : CYL. BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM01_CYL : DOWN 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_CYL].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxTransper::OnBnClickedBtnVacOn()
{
	m_stTM01_Mess.Reset();

	m_stTM01_Mess.nAction = m_stTM01_Mess.Act_None;
	m_stTM01_Mess.nResult = m_stTM01_Mess.Ret_None;	
	m_stTM01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM01_Mess, nMessSize);
	CString strLogMessage;

	if(G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM01_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM01_VAC].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM01_VAC].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_TM01_Vac_ON, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_TM01_Vac_ON, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM01_VAC : ON.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM01_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM01_VAC : ON 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_VAC].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxTransper::OnBnClickedBtnVacOff()
{
	m_stTM01_Mess.Reset();

	m_stTM01_Mess.nAction = m_stTM01_Mess.Act_None;
	m_stTM01_Mess.nResult = m_stTM01_Mess.Ret_None;	
	m_stTM01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM01_VAC].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM01_VAC].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM01_VAC].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_TM01_Vac_OFF, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_TM01_Vac_OFF, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM01_VAC : OFF.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

			
		}
		else
		{
			strLogMessage.Format("TM01_VAC : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM01_VAC : OFF 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_VAC].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxTransper::OnBnClickedBtnPurge()
{
	int nPurgeTime = GetDlgItemInt(IDC_EDIT_PURGE_SEC);

	m_stTM01_Mess.Reset();

	m_stTM01_Mess.nAction = m_stTM01_Mess.Act_None;
	m_stTM01_Mess.nResult = m_stTM01_Mess.Ret_None;	
	m_stTM01_Mess.ActionTime = nPurgeTime;
	m_stTM01_Mess.TimeOut = TIME_OUT_10_SECOND;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM01_Mess, nMessSize);

	CString strLogMessage;

	if(G_MOTION_STATUS[TM01_PURGE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM01_PURGE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM01_PURGE].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM01_PURGE].UN_CMD_COUNT++;

			g_cpServerInterface->m_fnSendCommand_Nrs(
				TASK_24_Motion, nBiz_Func_Vac, nBiz_Seq_TM01_Purge, nBiz_Unit_Zero,
				nMessSize,	chMsgBuf,
				TASK_11_Indexer, nBiz_Func_Vac, nBiz_Seq_TM01_Purge, nBiz_Unit_Zero
				);

			strLogMessage.Format("TM01_PURGE : 동작 실행.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);


		}
		else
		{
			strLogMessage.Format("TM01_PURGE : IO BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM01_PURGE : 동작 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_PURGE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM01_PURGE].UN_CMD_COUNT = 0;
	}

	delete []chMsgBuf;	
}

void CBoxTransper::OnBnClickedBtnRead()
{
	CString strLogMessage;
	G_MOTION_STATUS[TM01_BCR].B_COMMAND_STATUS = READY;

	m_cBcrCtrl.m_fnSetParentHandle(g_cpMainDlg->m_hWnd);

	if(m_bBCR_Connection != true)
	{
		m_bBCR_Connection = m_cBcrCtrl.LinkOpen();			

		if(m_bBCR_Connection)
		{
			strLogMessage.Format("TM01_BCR : 연결 성공.");
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		}
		else
		{
			strLogMessage.Format("TM01_BCR :연결 실패.");
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		}
	}
	
	if(m_bBCR_Connection)
	{
		if (G_MOTION_STATUS[TM01_BCR].UN_CMD_COUNT < RETRY_COUNT)
		{
			if (G_MOTION_STATUS[TM01_BCR].B_MOTION_STATUS == IDLE)
			{
				G_MOTION_STATUS[TM01_BCR].B_COMMAND_STATUS = BUSY;
				G_MOTION_STATUS[TM01_BCR].B_MOTION_STATUS = RUNNING;
				G_MOTION_STATUS[TM01_BCR].UN_CMD_COUNT++;

				m_bBCR_Connection = m_cBcrCtrl.SetTriggerOnOff(true);

				strLogMessage.Format("TM01_BCR : 박스 바코드 읽기 동작 수행.");
				m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);				
			}
			else
			{
				strLogMessage.Format("TM01_BCR : 해당 동작 BUSY 상태.");
				m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
			}
		}
		else
		{
			strLogMessage.Format("TM01_BCR : 재시도 실패.");
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TM01_BCR].B_MOTION_STATUS = IDLE;
			G_MOTION_STATUS[TM01_BCR].UN_CMD_COUNT = 0;
		}
	}
}

void CBoxTransper::m_fnBoxBarcodeDisp(char* chData)
{
	memset(m_chBoxBarcode, 0x00, MAX_BARCODE);
	strncpy_s(m_chBoxBarcode, MAX_BARCODE, chData, MAX_BARCODE);

	SetDlgItemText(IDC_EDIT_BARCODE, m_chBoxBarcode);
}

void CBoxTransper::m_fnTM01ForkMove(int nPosValue)
{
	CString strLogMessage;

	if(m_fnTM01InterlockCheck(AXIS_TM01_ForwardBackward, nPosValue))
		return;

	if(G_MOTION_STATUS[TM01_DRIVE].UN_CMD_COUNT < RETRY_COUNT)
	{
		if(G_MOTION_STATUS[TM01_DRIVE].B_MOTION_STATUS == IDLE)
		{
			G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS = BUSY;
			G_MOTION_STATUS[TM01_DRIVE].B_MOTION_STATUS = RUNNING;	
			G_MOTION_STATUS[TM01_DRIVE].UN_CMD_COUNT++;

			m_fnTM01PositionMove(nPosValue, nBiz_Seq_TM01_ForwardBackward, TIME_OUT_120_SECOND);

			strLogMessage.Format("TM01_DRIVE : [%d]위치로 이동 명령.", nPosValue);
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);			
		}
		else
		{
			strLogMessage.Format("TM01_DRIVE : 해당 축 BUSY 상태.");
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		}
	}
	else
	{
		strLogMessage.Format("TM01_DRIVE : 재시도 실패.");
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_DRIVE].B_MOTION_STATUS = IDLE;	
		G_MOTION_STATUS[TM01_DRIVE].UN_CMD_COUNT = 0;
	}
}

bool CBoxTransper::m_fnTM01InterlockCheck(int AxisNum, long newPosition)
{
	CString strLogMessage;

	CHECK_AXIS_TM01(AxisNum);
	
	switch(AxisNum)
	{
		//TM01
	case AXIS_TM01_ForwardBackward:
		{
			int nForkDriveMin = GetPrivateProfileInt(Section_TM01, Key_Fork_Drive_Min, 0, Interlock_PARAM_INI_PATH);
			int nForkDriveMax = GetPrivateProfileInt(Section_TM01, Key_Fork_Drive_Max, 0, Interlock_PARAM_INI_PATH);

			if (!(G_STMACHINE_STATUS.stModuleStatus.In12_Sn_LoadLeftAlignBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In14_Sn_LoadRightAlignBackward == ON))
			{
				m_fnLogDisplay("AXIS_TM01_ForwardBackward : 로딩 테이블 가이드 실린더를 후진 시켜야 합니다.", TEXT_WARRING_YELLOW);
				return true;
			}
			if (!(G_STMACHINE_STATUS.stModuleStatus.In18_Sn_UnloadBoxLeftAlignBackward == ON &&
				G_STMACHINE_STATUS.stModuleStatus.In20_Sn_UnloadBoxRightAlignBackward == ON))
			{
				m_fnLogDisplay("AXIS_TM01_ForwardBackward : 언로딩 테이블 가이드 실린더를 후진 시켜야 합니다.", TEXT_WARRING_YELLOW);
				return true;
			}

			if(G_SAFE_ZONE[BT01_SAFE_POS] == UNSAFE)
			{					
				m_fnLogDisplay("AXIS_TM01_ForwardBackward : Box table 이 안전한 위치에 있지 않습니다.", TEXT_WARRING_YELLOW);
				return true;
			}

			if(!(nForkDriveMin == 0 && nForkDriveMax == 0))		
			{
				if(newPosition < nForkDriveMin || newPosition > nForkDriveMax)
				{
					strLogMessage.Format("AXIS_TM01_ForwardBackward : [Min(%d)<New(%d)<Max(%d)] 범위 내에 있지 않습니다.",
						nForkDriveMin, newPosition, nForkDriveMax );
					m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
					return true;
				}
			}
			break;
		}
	default: 
		return true;
	}
	return false;
}

void CBoxTransper::m_fnTM01PositionMove(int nPosValue, int nMotionType, int nTimeOut)
{	
	m_stTM01_Mess.Reset();

	m_stTM01_Mess.nAction = m_stTM01_Mess.Act_Move;
	m_stTM01_Mess.nResult = m_stTM01_Mess.Ret_None;
	m_stTM01_Mess.newPosition1 = nPosValue;
	m_stTM01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize+1];
	memset((void*)chMsgBuf, 0x00, nMessSize+1);	

	memcpy((char*)chMsgBuf, (char*)&m_stTM01_Mess, nMessSize);


	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize,	chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete []chMsgBuf;	
}

void CBoxTransper::m_fnTM01ActionStop(int nMotionType, int nTimeOut)
{
	m_stTM01_Mess.Reset();

	m_stTM01_Mess.nAction = m_stTM01_Mess.Act_Stop;
	m_stTM01_Mess.nResult = m_stTM01_Mess.Ret_None;
	m_stTM01_Mess.TimeOut = nTimeOut;

	int nMessSize = sizeof(VS24MotData);
	UCHAR* chMsgBuf = new UCHAR[nMessSize + 1];
	memset((void*)chMsgBuf, 0x00, nMessSize + 1);

	memcpy((char*)chMsgBuf, (char*)&m_stTM01_Mess, nMessSize);

	g_cpServerInterface->m_fnSendCommand_Nrs(
		TASK_24_Motion, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero,
		nMessSize, chMsgBuf,
		TASK_11_Indexer, nBiz_Func_MotionAct, nMotionType, nBiz_Unit_Zero
		);

	delete[]chMsgBuf;
}

void CBoxTransper::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{	
	CVS11IndexerDlg*  vpMainDlg = (CVS11IndexerDlg*)AfxGetApp()->m_pMainWnd;
	vpMainDlg->m_fnAppendList(strLog, nColorType);
}

int CBoxTransper::m_fn_TM01_Drive_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM01_DRIVE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM01_DRIVE : 포지션 이동 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM01_DRIVE].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM01_DRIVE : 포지션 이동중 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM01_DRIVE : 이동 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM01_DRIVE].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//m_fnTM01ForkMove(stReturn_Mess.newPosition1);
	}break;
	default:
	{
		strLogMessage.Format("TM01_DRIVE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_DRIVE].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxTransper::m_fn_TM01_ForkUP_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM01_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM01_CYL : 포크 업 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

		G_MOTION_STATUS[TM01_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM01_CYL : 포크 업 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM01_CYL :포크 업 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnCylUp();
	}break;
	default:
	{
		strLogMessage.Format("TM01_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxTransper::m_fn_TM01_ForkDn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM01_CYL].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM01_CYL : 포크 다운 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM01_CYL].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM01_CYL : 포크 다운 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM01_CYL : 포크 다운 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnCylDn();
	}break;
	default:
	{
		strLogMessage.Format("TM01_CYL : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_CYL].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxTransper::m_fn_TM01_VacOn_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM01_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM01_VAC : Vac. ON 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM01_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM01_VAC : Vac. ON 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM01_VAC : Vac. ON 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnVacOn();
	}break;
	default:
	{
		strLogMessage.Format("TM01_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxTransper::m_fn_TM01_VacOff_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM01_VAC].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
	{
		strLogMessage = "TM01_VAC : Vac. OFF 수행 완료.";
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);

		G_MOTION_STATUS[TM01_VAC].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_Error:
	{
		strLogMessage = "TM01_VAC : Vac. OFF 에러 발생.";
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT = 0;
	}break;
	case stReturn_Mess.Ret_TimeOut:
	{
		strLogMessage.Format("TM01_VAC : Vac. OFF 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnVacOff();
	}break;
	default:
	{
		strLogMessage.Format("TM01_VAC : 리턴 값 에러[%d].", stReturn_Mess.nResult);
		m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
		G_MOTION_STATUS[TM01_VAC].UN_CMD_COUNT = 0;
	}break;
	}
	return 0;
}

int CBoxTransper::m_fn_TM01_Purge_Return(VS24MotData stReturn_Mess)
{
	G_MOTION_STATUS[TM01_PURGE].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	switch (stReturn_Mess.nResult)
	{
	case stReturn_Mess.Ret_Ok:
		{
			strLogMessage = "TM01_PURGE : Purge 수행 완료.";
			m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
			G_MOTION_STATUS[TM01_PURGE].B_COMMAND_STATUS = COMPLETE;
			G_MOTION_STATUS[TM01_PURGE].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_Error:
		{
			strLogMessage = "TM01_PURGE : Purge 에러 발생.";
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TM01_PURGE].UN_CMD_COUNT = 0;
		}break;
	case stReturn_Mess.Ret_TimeOut:
		{
			strLogMessage.Format("TM01_PURGE : Purge 명령에 대한 %d회 타임아웃 발생.", G_MOTION_STATUS[TM01_PURGE].UN_CMD_COUNT);
			m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
			//OnBnClickedBtnPurge();
		}break;
	default:
		{
			strLogMessage.Format("TM01_PURGE : 리턴 값 에러[%d].", stReturn_Mess.nResult);
			m_fnLogDisplay(strLogMessage, TEXT_ERROR_RED);
			G_MOTION_STATUS[TM01_PURGE].UN_CMD_COUNT = 0;
		}break;
	}
	return 0;
}

int CBoxTransper::m_fn_TM01_BCR_Return(LPARAM lParam)
{
	G_MOTION_STATUS[TM01_BCR].B_MOTION_STATUS = IDLE;
	//Ret_None =0,	Ret_Ok = 1,	Ret_Error,	Ret_TimeOut,
	CString strLogMessage;

	char chBoxBarcode[MAX_BARCODE + 6];
	memset(chBoxBarcode, 0x00, MAX_BARCODE + 6);

	if (lParam == NULL)
	{
		strLogMessage.Format("TM01_BCR : 박스 바코드 읽기 실패 %d회.", G_MOTION_STATUS[TM01_BCR].UN_CMD_COUNT);
		m_fnLogDisplay(strLogMessage, TEXT_WARRING_YELLOW);
		//OnBnClickedBtnRead();
	}
	else
	{
		memcpy(chBoxBarcode, (char*)lParam, MAX_BARCODE);
		m_fnBoxBarcodeDisp(chBoxBarcode);

		//strLogMessage = "TM01_BCR : 박스 바코드 읽기 완료.";
		strLogMessage.Format("TM01_BCR : %s.", chBoxBarcode);
		m_fnLogDisplay(strLogMessage, TEXT_NORMAL_BLACK);
		G_MOTION_STATUS[TM01_BCR].B_COMMAND_STATUS = COMPLETE;
		G_MOTION_STATUS[TM01_BCR].UN_CMD_COUNT = 0;
	}

	return 0;
}

void CBoxTransper::OnBnClickedBtnMotionStop()
{
	m_fnTM01ActionStop(nBiz_Seq_TM01_ForwardBackward, TIME_OUT_10_SECOND);
}

void CBoxTransper::m_fnDisplaySignal()
{
	if (G_STMACHINE_STATUS.stModuleStatus.In22_Sn_ForkBoxVacuum == ON)
	{
		m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOn);
		m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOff);
	}
	else
	{
		m_ctrBtnVacOn.SetRoundButtonStyle(&m_tStyleOff);
		m_ctrBtnVacOff.SetRoundButtonStyle(&m_tStyleOn);
	}

	if (G_STMACHINE_STATUS.stModuleStatus.In23_Sn_ForkFrontUp == ON &&
		G_STMACHINE_STATUS.stModuleStatus.In25_Sn_ForkRearUp == ON)
		m_ctrBtnCylUp.SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtnCylUp.SetRoundButtonStyle(&m_tStyleOff);
	
	if (G_STMACHINE_STATUS.stModuleStatus.In24_Sn_ForkFrontDown == ON &&
		G_STMACHINE_STATUS.stModuleStatus.In26_Sn_ForkRearDown == ON)
		m_ctrBtnCylDn.SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtnCylDn.SetRoundButtonStyle(&m_tStyleOff);	
}

void CBoxTransper::OnBnClickedBtnSet()
{
	memset(m_chBoxBarcode, 0x00, MAX_BARCODE);
	CString strBarcode;
	GetDlgItemText(IDC_EDIT_BARCODE, strBarcode);

	memcpy(m_chBoxBarcode, (LPSTR)(LPCTSTR)strBarcode, strBarcode.GetLength());	
}
