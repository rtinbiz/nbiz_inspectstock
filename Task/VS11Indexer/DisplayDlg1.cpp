// DisplayDlg1.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11Indexer.h"
#include "DisplayDlg1.h"
#include "afxdialogex.h"
#include "VS11IndexerDlg.h"


// CDisplayDlg1 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDisplayDlg1, CDialogEx)

CDisplayDlg1::CDisplayDlg1(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDisplayDlg1::IDD, pParent)
{

}

CDisplayDlg1::~CDisplayDlg1()
{
}

void CDisplayDlg1::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT8, m_ctrBtn_BT01[BT01_BOX_DETECT8_1]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT8_1, m_ctrBtn_BT01[BT01_BOX_DETECT8_2]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT8_2, m_ctrBtn_BT01[BT01_BOX_DETECT8_3]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT1, m_ctrBtn_BT01[BT01_BOX_DETECT1_1]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT1_1, m_ctrBtn_BT01[BT01_BOX_DETECT1_2]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT1_2, m_ctrBtn_BT01[BT01_BOX_DETECT1_3]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT2, m_ctrBtn_BT01[BT01_BOX_DETECT2_1]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT2_1, m_ctrBtn_BT01[BT01_BOX_DETECT2_2]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT2_2, m_ctrBtn_BT01[BT01_BOX_DETECT2_3]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT3, m_ctrBtn_BT01[BT01_BOX_DETECT3_1]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT3_1, m_ctrBtn_BT01[BT01_BOX_DETECT3_2]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT3_2, m_ctrBtn_BT01[BT01_BOX_DETECT3_3]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT4, m_ctrBtn_BT01[BT01_BOX_DETECT4_1]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT4_1, m_ctrBtn_BT01[BT01_BOX_DETECT4_2]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT4_2, m_ctrBtn_BT01[BT01_BOX_DETECT4_3]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT5, m_ctrBtn_BT01[BT01_BOX_DETECT5_1]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT5_1, m_ctrBtn_BT01[BT01_BOX_DETECT5_2]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT5_2, m_ctrBtn_BT01[BT01_BOX_DETECT5_3]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT6, m_ctrBtn_BT01[BT01_BOX_DETECT6_1]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT6_1, m_ctrBtn_BT01[BT01_BOX_DETECT6_2]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT6_2, m_ctrBtn_BT01[BT01_BOX_DETECT6_3]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT7, m_ctrBtn_BT01[BT01_BOX_DETECT7_1]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT7_1, m_ctrBtn_BT01[BT01_BOX_DETECT7_2]);
	DDX_Control(pDX, IDC_BTN_BOX_DETECT7_2, m_ctrBtn_BT01[BT01_BOX_DETECT7_3]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS1, m_ctrBtn_BT01[BT01_TABLE_POS_1]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS2, m_ctrBtn_BT01[BT01_TABLE_POS_2]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS3, m_ctrBtn_BT01[BT01_TABLE_POS_3]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS4, m_ctrBtn_BT01[BT01_TABLE_POS_4]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS5, m_ctrBtn_BT01[BT01_TABLE_POS_5]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS6, m_ctrBtn_BT01[BT01_TABLE_POS_6]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS7, m_ctrBtn_BT01[BT01_TABLE_POS_7]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS8, m_ctrBtn_BT01[BT01_TABLE_POS_BAD]);
	DDX_Control(pDX, IDC_BTN_TABLE_POS9, m_ctrBtn_BT01[BT01_TABLE_POS_WORK]);
	
	DDX_Control(pDX, IDC_BTN_TM01_BOX_INPUT, m_ctrBtn_TM01[TM01_BOX_INPUT]);
	DDX_Control(pDX, IDC_BTN_TM01_BOX_OUTPUT, m_ctrBtn_TM01[TM01_BOX_OUTPUT]);
	DDX_Control(pDX, IDC_BTN_TM01_FORK_FWD, m_ctrBtn_TM01[TM01_FORK_FWD]);
	DDX_Control(pDX, IDC_BTN_TM01_FORK_BWD, m_ctrBtn_TM01[TM01_FORK_BWD]);
	DDX_Control(pDX, IDC_BTN_TM01_FORK_READY, m_ctrBtn_TM01[TM01_FORK_READY]);
	DDX_Control(pDX, IDC_BTN_TM01_FRONT_FORK_UP, m_ctrBtn_TM01[TM01_FRONT_CYL_UP]);
	DDX_Control(pDX, IDC_BTN_TM01_FRONT_FORK_DN, m_ctrBtn_TM01[TM01_FRONT_CYL_DN]);
	DDX_Control(pDX, IDC_BTN_TM01_REAR_FORK_UP, m_ctrBtn_TM01[TM01_REAR_CYL_UP]);
	DDX_Control(pDX, IDC_BTN_TM01_REAR_FORK_DN, m_ctrBtn_TM01[TM01_REAR_CYL_DN]);
	DDX_Control(pDX, IDC_BTN_TM01_BOX_DETECT, m_ctrBtn_TM01[TM01_BOX_DETECT]);
	DDX_Control(pDX, IDC_BTN_TM01_VAC_SENSOR, m_ctrBtn_TM01[TM01_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_TM01_ORIGIN, m_ctrBtn_TM01[TM01_ORIGIN_SENSOR]);

	DDX_Control(pDX, IDC_BTN_LT01_DETECT1, m_ctrBtn_LT01[LT01_BOX_DETECT1]);
	DDX_Control(pDX, IDC_BTN_LT01_DETECT2, m_ctrBtn_LT01[LT01_BOX_DETECT2]);
	DDX_Control(pDX, IDC_BTN_LT01_DETECT3, m_ctrBtn_LT01[LT01_BOX_DETECT3]);
	DDX_Control(pDX, IDC_BTN_LT01_LEFT_GUIDE_FWD, m_ctrBtn_LT01[LT01_LGUIDE_FWD]);
	DDX_Control(pDX, IDC_BTN_LT01_LEFT_GUIDE_BWD, m_ctrBtn_LT01[LT01_LGUIDE_BWD]);
	DDX_Control(pDX, IDC_BTN_LT01_RIGHT_GUIDE_FWD, m_ctrBtn_LT01[LT01_RGUIDE_FWD]);
	DDX_Control(pDX, IDC_BTN_LT01_RIGHT_GUIDE_BWD, m_ctrBtn_LT01[LT01_RGUIDE_BWD]);
	DDX_Control(pDX, IDC_BTN_LT01_GUIDE_MOTOR_READY, m_ctrBtn_LT01[LT01_PUSHER_READY]);
	DDX_Control(pDX, IDC_BTN_LT01_GUIDE_MOTOR_SMALL, m_ctrBtn_LT01[LT01_PUSHER_SMALL]);
	DDX_Control(pDX, IDC_BTN_LT01_GUIDE_MOTOR_MID, m_ctrBtn_LT01[LT01_PUSHER_MID]);
	DDX_Control(pDX, IDC_BTN_LT01_GUIDE_MOTOR_LARGE, m_ctrBtn_LT01[LT01_PUSHER_LARGE]);
	DDX_Control(pDX, IDC_BTN_LT01_PAPER_LOCK, m_ctrBtn_LT01[LT01_PAPER_LOCK]);
	DDX_Control(pDX, IDC_BTN_LT01_VAC_SENSOR, m_ctrBtn_LT01[LT01_VAC_SENSOR]);	

	DDX_Control(pDX, IDC_BTN_BO01_BOX_OPEN, m_ctrBtn_BO01[BO01_BOX_OPEN]);
	DDX_Control(pDX, IDC_BTN_BO01_BOX_CLOSE, m_ctrBtn_BO01[BO01_BOX_CLOSE]);
	DDX_Control(pDX, IDC_BTN_BO01_BOX_UNLOADING, m_ctrBtn_BO01[BO01_BOX_UNLOAD]);
	DDX_Control(pDX, IDC_BTN_BO01_POS_LT01, m_ctrBtn_BO01[BO01_POS_LT01]);
	DDX_Control(pDX, IDC_BTN_BO01_POS_UT01, m_ctrBtn_BO01[BO01_POS_UT01]);
	DDX_Control(pDX, IDC_BTN_BO01_POS_UP, m_ctrBtn_BO01[BO01_POS_UP]);
	DDX_Control(pDX, IDC_BTN_BO01_POS_DN, m_ctrBtn_BO01[BO01_POS_DN]);
	DDX_Control(pDX, IDC_BTN_BO01_VACPAD_UP, m_ctrBtn_BO01[BO01_PAD_UP]);
	DDX_Control(pDX, IDC_BTN_BO01_VACPAD_DN, m_ctrBtn_BO01[BO01_PAD_DN]);
	DDX_Control(pDX, IDC_BTN_BO01_LEFT_CYL_FWD, m_ctrBtn_BO01[BO01_LCYL_FWD]);
	DDX_Control(pDX, IDC_BTN_BO01_LEFT_CYL_BWD, m_ctrBtn_BO01[BO01_LCYL_BWD]);
	DDX_Control(pDX, IDC_BTN_BO01_RIGHT_CYL_FWD, m_ctrBtn_BO01[BO01_RCYL_FWD]);
	DDX_Control(pDX, IDC_BTN_BO01_RIGHT_CYL_BWD, m_ctrBtn_BO01[BO01_RCYL_BWD]);
	DDX_Control(pDX, IDC_BTN_BO01_LEFT_BOX_DETECT, m_ctrBtn_BO01[BO01_L_BOX_DETECT]);
	DDX_Control(pDX, IDC_BTN_BO01_RIGHT_BOX_DETECT, m_ctrBtn_BO01[BO01_R_BOX_DETECT]);
	DDX_Control(pDX, IDC_BTN_BO01_LEFT_VAC, m_ctrBtn_BO01[BO01_L_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_BO01_RIGHT_VAC, m_ctrBtn_BO01[BO01_R_VAC_SENSOR]);
	DDX_Control(pDX, IDC_BTN_UT01_DETECT, m_ctrBtn_UT01[UT01_BOX_DETECT]);
	DDX_Control(pDX, IDC_BTN_UT01_LEFT_GUIDE_FWD, m_ctrBtn_UT01[UT01_LGUIDE_FWD]);
	DDX_Control(pDX, IDC_BTN_UT01_LEFT_GUIDE_BWD, m_ctrBtn_UT01[UT01_LGUIDE_BWD]);
	DDX_Control(pDX, IDC_BTN_UT01_RIGHT_GUIDE_FWD, m_ctrBtn_UT01[UT01_RGUIDE_FWD]);
	DDX_Control(pDX, IDC_BTN_UT01_RIGHT_GUIDE_BWD, m_ctrBtn_UT01[UT01_RGUIDE_BWD]);
	DDX_Control(pDX, IDC_BTN_UT01_GUIDE_MOTOR_READY, m_ctrBtn_UT01[UT01_PUSHER_READY]);
	DDX_Control(pDX, IDC_BTN_UT01_GUIDE_MOTOR_SMALL, m_ctrBtn_UT01[UT01_PUSHER_SMALL]);
	DDX_Control(pDX, IDC_BTN_UT01_GUIDE_MOTOR_MID, m_ctrBtn_UT01[UT01_PUSHER_MID]);
	DDX_Control(pDX, IDC_BTN_UT01_GUIDE_MOTOR_LARGE, m_ctrBtn_UT01[UT01_PUSHER_LARGE]);
	DDX_Control(pDX, IDC_BTN_UT01_VAC_SENSOR, m_ctrBtn_UT01[UT01_VAC_SENSOR]);

	DDX_Control(pDX, IDC_BTN_WORK_BOX1, m_ctrBtn_WorkBox[BOX1 - 1]);
	DDX_Control(pDX, IDC_BTN_WORK_BOX2, m_ctrBtn_WorkBox[BOX2 - 1]);
	DDX_Control(pDX, IDC_BTN_WORK_BOX3, m_ctrBtn_WorkBox[BOX3 - 1]);
	DDX_Control(pDX, IDC_BTN_WORK_BOX4, m_ctrBtn_WorkBox[BOX4 - 1]);
	DDX_Control(pDX, IDC_BTN_WORK_BOX5, m_ctrBtn_WorkBox[BOX5 - 1]);
	DDX_Control(pDX, IDC_BTN_WORK_BOX6, m_ctrBtn_WorkBox[BOX6 - 1]);
	DDX_Control(pDX, IDC_BTN_WORK_BOX7, m_ctrBtn_WorkBox[BOX7 - 1]);
}


BEGIN_MESSAGE_MAP(CDisplayDlg1, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_WORK_BOX1, &CDisplayDlg1::OnBnClickedBtnWorkBox1)
	ON_BN_CLICKED(IDC_BTN_WORK_BOX2, &CDisplayDlg1::OnBnClickedBtnWorkBox2)
	ON_BN_CLICKED(IDC_BTN_WORK_BOX3, &CDisplayDlg1::OnBnClickedBtnWorkBox3)
	ON_BN_CLICKED(IDC_BTN_WORK_BOX4, &CDisplayDlg1::OnBnClickedBtnWorkBox4)
	ON_BN_CLICKED(IDC_BTN_WORK_BOX5, &CDisplayDlg1::OnBnClickedBtnWorkBox5)
	ON_BN_CLICKED(IDC_BTN_WORK_BOX6, &CDisplayDlg1::OnBnClickedBtnWorkBox6)
	ON_BN_CLICKED(IDC_BTN_WORK_BOX7, &CDisplayDlg1::OnBnClickedBtnWorkBox7)
END_MESSAGE_MAP()


// CDisplayDlg1 메시지 처리기입니다.


void CDisplayDlg1::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
}

BOOL CDisplayDlg1::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btBackColor = RGB(65,65,65);

	m_fnCreateButton();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


HBRUSH CDisplayDlg1::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


int CDisplayDlg1::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style Green
	m_tStyleOn.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;
	tStyle.m_tColorFace.m_tEnabled		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x60, 0xFF, 0x60);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x80, 0x20);
	m_tStyleOn.SetButtonStyle(&tStyle);

	// Get default Style Dark Gray
	m_tStyleOff.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 12.0;
	// Change Color Schema of Button
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x20, 0x20, 0x20);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyleOff.SetButtonStyle(&tStyle);


	m_tStyleWork.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 12.0;
	//tStyle.m_dRadiusHighLight = 10.0;

	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled = m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	// Change Color Schema of Button
	tStyle.m_tColorFace.m_tEnabled = RGB(0xFF, 0x00, 0x40);
	tStyle.m_tColorBorder.m_tEnabled = RGB(0x20, 0x20, 0x40);
	tStyle.m_tColorFace.m_tClicked = RGB(0xBE, 0xBE, 0x80);
	tStyle.m_tColorBorder.m_tClicked = RGB(0xDB, 0x92, 0x40);
	tStyle.m_tColorFace.m_tPressed = RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tPressed = RGB(0x80, 0x80, 0x20);
	m_tStyleWork.SetButtonStyle(&tStyle);
		
// 	tColorScheme tColor;
// 	m_ctrBtn_BT01_BD8[0].GetTextColor(&tColor);
// 
// 	// Change Color
// 	tColor.m_tEnabled	= RGB(0xAF, 0x00, 0x00);
// 	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
// 	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
// 
// 	m_ctrBtn_BT01_BD8[0].SetTextColor(&tColor);
// 	m_ctrBtn_BT01_BD8[0].SetCheckButton(false);
// 	m_ctrBtn_BT01_BD8[1].SetTextColor(&tColor);
// 	m_ctrBtn_BT01_BD8[1].SetCheckButton(false);
// 	m_ctrBtn_BT01_BD8[2].SetTextColor(&tColor);
// 	m_ctrBtn_BT01_BD8[2].SetCheckButton(false);


	for (int nStep = BOX1 - 1; nStep < BOX7; nStep++)
	{
		m_ctrBtn_WorkBox[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}


	for(int nStep = BT01_BOX_DETECT1_1; nStep < BT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_BT01[nStep].SetRoundButtonStyle(&m_tStyleOff);		
	}	

	for(int nStep = TM01_BOX_INPUT; nStep < TM01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TM01[nStep].SetRoundButtonStyle(&m_tStyleOff);		
	}	

	for(int nStep = LT01_BOX_DETECT1; nStep < LT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_LT01[nStep].SetRoundButtonStyle(&m_tStyleOff);		
	}	

	for(int nStep = BO01_BOX_OPEN; nStep < BO01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_BO01[nStep].SetRoundButtonStyle(&m_tStyleOff);		
	}	

	for(int nStep = UT01_BOX_DETECT; nStep < UT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_UT01[nStep].SetRoundButtonStyle(&m_tStyleOff);		
	}	

	return 0;
}

BOOL CDisplayDlg1::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDisplayDlg1::m_fnDisplaySignal()
{
	//1단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In00_Sn_BoxTable1_100mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT1_1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT1_1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In01_Sn_BoxTable1_200mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT1_2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT1_2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In02_Sn_BoxTable1_440mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT1_3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT1_3].SetRoundButtonStyle(&m_tStyleOff);

	//2단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In03_Sn_BoxTable2_100mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT2_1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT2_1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In04_Sn_BoxTable2_200mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT2_2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT2_2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In05_Sn_BoxTable2_440mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT2_3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT2_3].SetRoundButtonStyle(&m_tStyleOff);

	//3단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In06_Sn_BoxTable3_100mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT3_1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT3_1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In07_Sn_BoxTable3_200mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT3_2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT3_2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In08_Sn_BoxTable3_440mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT3_3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT3_3].SetRoundButtonStyle(&m_tStyleOff);

	//4단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In09_Sn_BoxTable4_100mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT4_1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT4_1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In10_Sn_BoxTable4_200mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT4_2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT4_2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In11_Sn_BoxTable4_440mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT4_3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT4_3].SetRoundButtonStyle(&m_tStyleOff);

	//5단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In12_Sn_BoxTable5_100mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT5_1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT5_1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In13_Sn_BoxTable5_200mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT5_2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT5_2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In14_Sn_BoxTable5_440mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT5_3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT5_3].SetRoundButtonStyle(&m_tStyleOff);

	//6단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In15_Sn_BoxTable6_100mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT6_1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT6_1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In16_Sn_BoxTable6_200mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT6_2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT6_2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In17_Sn_BoxTable6_440mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT6_3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT6_3].SetRoundButtonStyle(&m_tStyleOff);

	//7단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In18_Sn_BoxTable7_100mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT7_1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT7_1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In19_Sn_BoxTable7_200mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT7_2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT7_2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In20_Sn_BoxTable7_440mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT7_3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT7_3].SetRoundButtonStyle(&m_tStyleOff);

	//불량단
	if (G_STMACHINE_STATUS.stBoxTableSensor.In21_Sn_BoxTableF_100mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT8_1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT8_1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In22_Sn_BoxTableF_200mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT8_2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT8_2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stBoxTableSensor.In23_Sn_BoxTableF_440mm == ON)
		m_ctrBtn_BT01[BT01_BOX_DETECT8_3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BT01[BT01_BOX_DETECT8_3].SetRoundButtonStyle(&m_tStyleOff);


	//TM01
	if (G_STMACHINE_STATUS.stModuleStatus.In21_Sn_ForkBoxDetection == ON)
		m_ctrBtn_TM01[TM01_BOX_DETECT].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM01[TM01_BOX_DETECT].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In22_Sn_ForkBoxVacuum == ON)
		m_ctrBtn_TM01[TM01_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM01[TM01_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In23_Sn_ForkFrontUp == ON)
		m_ctrBtn_TM01[TM01_FRONT_CYL_UP].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM01[TM01_FRONT_CYL_UP].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In24_Sn_ForkFrontDown == ON)
		m_ctrBtn_TM01[TM01_FRONT_CYL_DN].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM01[TM01_FRONT_CYL_DN].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In25_Sn_ForkRearUp == ON)
		m_ctrBtn_TM01[TM01_REAR_CYL_UP].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM01[TM01_REAR_CYL_UP].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In26_Sn_ForkRearDown == ON)
		m_ctrBtn_TM01[TM01_REAR_CYL_DN].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM01[TM01_REAR_CYL_DN].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stEmissionTable.In09_Sn_ForkStandbyPos == ON)
		m_ctrBtn_TM01[TM01_ORIGIN_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_TM01[TM01_ORIGIN_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	//LT01
	if (G_STMACHINE_STATUS.stModuleStatus.In07_Sn_LoadBoxSmallDetection == ON)
		m_ctrBtn_LT01[LT01_BOX_DETECT1].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_BOX_DETECT1].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In08_Sn_LoadBoxMediumDetection == ON)
		m_ctrBtn_LT01[LT01_BOX_DETECT2].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_BOX_DETECT2].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In09_Sn_LoadBoxLargeDetection == ON)
		m_ctrBtn_LT01[LT01_BOX_DETECT3].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_BOX_DETECT3].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In10_Sn_LoadBoxVacuum == ON)
		m_ctrBtn_LT01[LT01_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	

	//Paper Interlock
	if (G_STMACHINE_STATUS.stNormalStatus.In30_TwoPaperAbsorbInterlock1 == OFF ||
		G_STMACHINE_STATUS.stNormalStatus.In31_TwoPaperAbsorbInterlock2 == OFF)
		m_ctrBtn_LT01[LT01_PAPER_LOCK].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_PAPER_LOCK].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In11_Sn_LoadLeftAlignForward == ON)
		m_ctrBtn_LT01[LT01_LGUIDE_FWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_LGUIDE_FWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In12_Sn_LoadLeftAlignBackward == ON)
		m_ctrBtn_LT01[LT01_LGUIDE_BWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_LGUIDE_BWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In13_Sn_LoadRightAlignForward == ON)
		m_ctrBtn_LT01[LT01_RGUIDE_FWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_RGUIDE_FWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In14_Sn_LoadRightAlignBackward == ON)
		m_ctrBtn_LT01[LT01_RGUIDE_BWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_LT01[LT01_RGUIDE_BWD].SetRoundButtonStyle(&m_tStyleOff);


	//UT01
	if (G_STMACHINE_STATUS.stModuleStatus.In15_Sn_UnloadBoxDetection == ON)
		m_ctrBtn_UT01[UT01_BOX_DETECT].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT01[UT01_BOX_DETECT].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In16_Sn_UnloadBoxVacuum == ON)
		m_ctrBtn_UT01[UT01_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT01[UT01_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In17_Sn_UnloadBoxLeftAlignForward == ON)
		m_ctrBtn_UT01[UT01_LGUIDE_FWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT01[UT01_LGUIDE_FWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In18_Sn_UnloadBoxLeftAlignBackward == ON)
		m_ctrBtn_UT01[UT01_LGUIDE_BWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT01[UT01_LGUIDE_BWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In19_Sn_UnloadBoxRightAlignForward == ON)
		m_ctrBtn_UT01[UT01_RGUIDE_FWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT01[UT01_RGUIDE_FWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In20_Sn_UnloadBoxRightAlignBackward == ON)
		m_ctrBtn_UT01[UT01_RGUIDE_BWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_UT01[UT01_RGUIDE_BWD].SetRoundButtonStyle(&m_tStyleOff);

	//BO01
	if (G_STMACHINE_STATUS.stModuleStatus.In00_Sn_BoxLeftCylinderForward == ON)
		m_ctrBtn_BO01[BO01_LCYL_FWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BO01[BO01_LCYL_FWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In01_Sn_BoxLeftCylinderBackward == ON)
		m_ctrBtn_BO01[BO01_LCYL_BWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BO01[BO01_LCYL_BWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In02_Sn_BoxDetection == ON)
		m_ctrBtn_BO01[BO01_L_BOX_DETECT].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BO01[BO01_L_BOX_DETECT].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In03_Sn_BoxLeftVacuum == ON)
		m_ctrBtn_BO01[BO01_L_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BO01[BO01_L_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In04_Sn_BoxRightCylinderForward == ON)
		m_ctrBtn_BO01[BO01_RCYL_FWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BO01[BO01_RCYL_FWD].SetRoundButtonStyle(&m_tStyleOff);

	if (G_STMACHINE_STATUS.stModuleStatus.In05_Sn_BoxRightCylinderBackward == ON)
		m_ctrBtn_BO01[BO01_RCYL_BWD].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BO01[BO01_RCYL_BWD].SetRoundButtonStyle(&m_tStyleOff);
	
	if (G_STMACHINE_STATUS.stModuleStatus.In06_Sn_BoxRightVacuum == ON)
		m_ctrBtn_BO01[BO01_R_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOn);
	else
		m_ctrBtn_BO01[BO01_R_VAC_SENSOR].SetRoundButtonStyle(&m_tStyleOff);
}

void CDisplayDlg1::m_fnDisplayPosition_BT01_UpDn(BT01_SIGNAL_STEP step)
{
	for (int nStep = BT01_TABLE_POS_1; nStep <= BT01_TABLE_POS_WORK; nStep++)
	{
		m_ctrBtn_BT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != BT01_SIGNAL_COUNT)
		m_ctrBtn_BT01[step].SetRoundButtonStyle(&m_tStyleOn);

	if (step >= BT01_TABLE_POS_1 && step <= BT01_TABLE_POS_7)
		G_SAFE_ZONE[BT01_SAFE_POS] = SAFE;
	else
		G_SAFE_ZONE[BT01_SAFE_POS] = UNSAFE;
	
}

void CDisplayDlg1::m_fnDisplayPosition_TM01_Drive(TM01_SIGNAL_STEP step)
{
	for (int nStep = TM01_FORK_FWD; nStep <= TM01_FORK_READY; nStep++)
	{
		m_ctrBtn_TM01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != TM01_SIGNAL_COUNT)
		m_ctrBtn_TM01[step].SetRoundButtonStyle(&m_tStyleOn);
}

void CDisplayDlg1::m_fnDisplayPosition_LT01_Pusher(LT01_SIGNAL_STEP step)
{
	for (int nStep = LT01_PUSHER_READY; nStep <= LT01_PUSHER_LARGE; nStep++)
	{
		m_ctrBtn_LT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != LT01_SIGNAL_COUNT)
		m_ctrBtn_LT01[step].SetRoundButtonStyle(&m_tStyleOn);
}

void CDisplayDlg1::m_fnDisplayPosition_BO01_Drive(BO01_SIGNAL_STEP step)
{
	
	for (int nStep = BO01_POS_LT01; nStep <= BO01_POS_UT01; nStep++)
	{
		m_ctrBtn_BO01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != BO01_SIGNAL_COUNT)
		m_ctrBtn_BO01[step].SetRoundButtonStyle(&m_tStyleOn);		
}

void CDisplayDlg1::m_fnDisplayPosition_BO01_UpDn(BO01_SIGNAL_STEP step)
{		
	for (int nStep = BO01_POS_UP; nStep <= BO01_POS_DN; nStep++)
	{
		m_ctrBtn_BO01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != BO01_SIGNAL_COUNT)
		m_ctrBtn_BO01[step].SetRoundButtonStyle(&m_tStyleOn);
}

void CDisplayDlg1::m_fnDisplayPosition_BO01_Rotate(BO01_SIGNAL_STEP step)
{	
	for (int nStep = BO01_PAD_UP; nStep <= BO01_PAD_DN; nStep++)
	{
		m_ctrBtn_BO01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != BO01_SIGNAL_COUNT)
		m_ctrBtn_BO01[step].SetRoundButtonStyle(&m_tStyleOn);
}

void CDisplayDlg1::m_fnDisplayPosition_UT01_Pusher(UT01_SIGNAL_STEP step)
{
	for (int nStep = UT01_PUSHER_READY; nStep <= UT01_PUSHER_LARGE; nStep++)
	{
		m_ctrBtn_UT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	if (step != UT01_SIGNAL_COUNT)
		m_ctrBtn_UT01[step].SetRoundButtonStyle(&m_tStyleOn);
}

void CDisplayDlg1::m_fnDisplayAllOn()
{
	for (int nStep = BT01_BOX_DETECT1_1; nStep < BT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_BT01[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}

	for (int nStep = TM01_BOX_INPUT; nStep < TM01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TM01[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}

	for (int nStep = LT01_BOX_DETECT1; nStep < LT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_LT01[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}

	for (int nStep = BO01_BOX_OPEN; nStep < BO01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_BO01[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}

	for (int nStep = UT01_BOX_DETECT; nStep < UT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_UT01[nStep].SetRoundButtonStyle(&m_tStyleOn);
	}
}

void CDisplayDlg1::m_fnDisplayAllOff()
{
	for (int nStep = BT01_BOX_DETECT1_1; nStep < BT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_BT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	for (int nStep = TM01_BOX_INPUT; nStep < TM01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_TM01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	for (int nStep = LT01_BOX_DETECT1; nStep < LT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_LT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	for (int nStep = BO01_BOX_OPEN; nStep < BO01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_BO01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}

	for (int nStep = UT01_BOX_DETECT; nStep < UT01_SIGNAL_COUNT; nStep++)
	{
		m_ctrBtn_UT01[nStep].SetRoundButtonStyle(&m_tStyleOff);
	}
}


void CDisplayDlg1::m_fnBoxSelectBtn_Release()
{
	for (int nStep = (int)BOX1; nStep < BOX_COUNT; nStep++)
	{
		if (G_BOX_STATUS[nStep].elBoxStatus == BOX_READY || G_BOX_STATUS[nStep].elBoxStatus == BOX_RUN)
		{			
			m_ctrBtn_WorkBox[nStep - 1].SetRoundButtonStyle(&m_tStyleWork);
		}
		else
		{			
			m_ctrBtn_WorkBox[nStep - 1].SetRoundButtonStyle(&m_tStyleOff);
		}
	}
}


void CDisplayDlg1::OnBnClickedBtnWorkBox1()
{
	if (G_BOX_STATUS[BOX1].elBoxStatus == BOX_NORMAL || 
		G_BOX_STATUS[BOX1].elBoxStatus == BOX_COMP || 
		G_BOX_STATUS[BOX1].elBoxStatus == BOX_CANCEL)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX1, BOX_READY);
		//m_ctrBtn_WorkBox[BOX1 - 1].SetRoundButtonStyle(&m_tStyleWork);
	}
	else if (G_BOX_STATUS[BOX1].elBoxStatus == BOX_READY)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX1, BOX_NORMAL);
		//m_ctrBtn_WorkBox[BOX1 - 1].SetRoundButtonStyle(&m_tStyleOff);
	}
}


void CDisplayDlg1::OnBnClickedBtnWorkBox2()
{
	if (G_BOX_STATUS[BOX2].elBoxStatus == BOX_NORMAL ||
		G_BOX_STATUS[BOX2].elBoxStatus == BOX_COMP ||
		G_BOX_STATUS[BOX2].elBoxStatus == BOX_CANCEL)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX2, BOX_READY);
		//m_ctrBtn_WorkBox[BOX2 - 1].SetRoundButtonStyle(&m_tStyleWork);
	}
	else if (G_BOX_STATUS[BOX2].elBoxStatus == BOX_READY)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX2, BOX_NORMAL);
		//m_ctrBtn_WorkBox[BOX2 - 1].SetRoundButtonStyle(&m_tStyleOff);
	}
}


void CDisplayDlg1::OnBnClickedBtnWorkBox3()
{
	if (G_BOX_STATUS[BOX3].elBoxStatus == BOX_NORMAL ||
		G_BOX_STATUS[BOX3].elBoxStatus == BOX_COMP ||
		G_BOX_STATUS[BOX3].elBoxStatus == BOX_CANCEL)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX3, BOX_READY);
		//m_ctrBtn_WorkBox[BOX3 - 1].SetRoundButtonStyle(&m_tStyleWork);
	}
	else if (G_BOX_STATUS[BOX3].elBoxStatus == BOX_READY)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX3, BOX_NORMAL);
		//m_ctrBtn_WorkBox[BOX3 - 1].SetRoundButtonStyle(&m_tStyleOff);
	}
}


void CDisplayDlg1::OnBnClickedBtnWorkBox4()
{
	if (G_BOX_STATUS[BOX4].elBoxStatus == BOX_NORMAL ||
		G_BOX_STATUS[BOX4].elBoxStatus == BOX_COMP ||
		G_BOX_STATUS[BOX4].elBoxStatus == BOX_CANCEL)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX4, BOX_READY);
		//m_ctrBtn_WorkBox[BOX4 - 1].SetRoundButtonStyle(&m_tStyleWork);
	}
	else if (G_BOX_STATUS[BOX4].elBoxStatus == BOX_READY)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX4, BOX_NORMAL);
		//m_ctrBtn_WorkBox[BOX4 - 1].SetRoundButtonStyle(&m_tStyleOff);
	}
}


void CDisplayDlg1::OnBnClickedBtnWorkBox5()
{
	if (G_BOX_STATUS[BOX5].elBoxStatus == BOX_NORMAL ||
		G_BOX_STATUS[BOX5].elBoxStatus == BOX_COMP ||
		G_BOX_STATUS[BOX5].elBoxStatus == BOX_CANCEL)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX5, BOX_READY);
		//m_ctrBtn_WorkBox[BOX5 - 1].SetRoundButtonStyle(&m_tStyleWork);
	}
	else if (G_BOX_STATUS[BOX5].elBoxStatus == BOX_READY)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX5, BOX_NORMAL);
		//m_ctrBtn_WorkBox[BOX5 - 1].SetRoundButtonStyle(&m_tStyleOff);
	}
}


void CDisplayDlg1::OnBnClickedBtnWorkBox6()
{
	if (G_BOX_STATUS[BOX6].elBoxStatus == BOX_NORMAL ||
		G_BOX_STATUS[BOX6].elBoxStatus == BOX_COMP ||
		G_BOX_STATUS[BOX6].elBoxStatus == BOX_CANCEL)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX6, BOX_READY);
		//m_ctrBtn_WorkBox[BOX6 - 1].SetRoundButtonStyle(&m_tStyleWork);
	}
	else if (G_BOX_STATUS[BOX6].elBoxStatus == BOX_READY)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX6, BOX_NORMAL);
		//m_ctrBtn_WorkBox[BOX6 - 1].SetRoundButtonStyle(&m_tStyleOff);
	}
}


void CDisplayDlg1::OnBnClickedBtnWorkBox7()
{
	if (G_BOX_STATUS[BOX7].elBoxStatus == BOX_NORMAL ||
		G_BOX_STATUS[BOX7].elBoxStatus == BOX_COMP ||
		G_BOX_STATUS[BOX7].elBoxStatus == BOX_CANCEL)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX7, BOX_READY);
		//m_ctrBtn_WorkBox[BOX7 - 1].SetRoundButtonStyle(&m_tStyleWork);
	}
	else if (G_BOX_STATUS[BOX7].elBoxStatus == BOX_READY)
	{
		g_cpMainDlg->m_fnBoxStatusChange(BOX7, BOX_NORMAL);
		//m_ctrBtn_WorkBox[BOX7 - 1].SetRoundButtonStyle(&m_tStyleOff);
	}
}
