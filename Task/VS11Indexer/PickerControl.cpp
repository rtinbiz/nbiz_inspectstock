#include "StdAfx.h"
#include "PickerControl.h"
#include "VS11Indexer.h"
#include "VS11IndexerDlg.h"

CPickerControl::CPickerControl()
{
	m_elPPMove_LT01_TM02 = PPG_MAIN_SEQUENCE_IDLE;
	m_elPPMove_TM02_UT01 = PPU_MAIN_SEQUENCE_IDLE;
	m_elPPMove_TM02_BT01 = PPB_MAIN_SEQUENCE_IDLE;
	m_elSTMove_LT01_TM02 = STLT_MAIN_SEQUENCE_IDLE;
	m_elSTMove_TM02_TT01 = STTT_MAIN_SEQUENCE_IDLE;

	m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
	m_elSTMove_AM01_UT02 = STAU_MAIN_SEQUENCE_IDLE;
	m_elSTMove_UT02_TM02 = STUT_MAIN_SEQUENCE_IDLE;
	m_elSTMove_TM02_UT01 = STTU_MAIN_SEQUENCE_IDLE;
	m_elSTMove_TM02_BT01 = STTB_MAIN_SEQUENCE_IDLE;

	m_elSeq_Error = ERR_SEQ_NONE;
	m_bTurnTablePositionLock = FREE;
	m_bSingleStickRun = FALSE;
	m_bLastObject = FALSE;
}

CPickerControl::~CPickerControl()
{
}

int CPickerControl::m_fnSetParameter(
	STM02_RECIPE* stpointer1,
	SBO01_POSITION* stpointer2,
	STT01_POSITION* stpointer3)
{
	m_st_TM02_Cur_Recipe = stpointer1;
	m_st_BO01_Pos = stpointer2;
	m_st_TT01_Pos = stpointer3;	
	return 0;
}

int	CPickerControl::m_fnPaperMove_LT01_TM02()
{
	CString strLogMessage;
	int	nTM02DrivePos = 0;
	BOOL bResult = FALSE;
	
	switch (m_elPPMove_LT01_TM02)
	{
	case PPG_MAIN_SEQUENCE_IDLE:
		break;
	case PPG_TM02_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			m_elPPMove_LT01_TM02 = PPG_TM02_DRIVE_GET_POS; //PPG_TM02_DRIVE_ALIGN_POS; 정의천
			strLogMessage.Format("TM02 : 간지 흡착 시나리오 시작(LT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elPPMove_LT01_TM02 = PPG_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다(LT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}		
		break;
	case PPG_TM02_DRIVE_ALIGN_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nLT01_Drive_Align);
		strLogMessage.Format("TM02 : 주행축 간지 감지 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_DRIVE_ALIGN_POS_CHECK;
		break;
	case PPG_TM02_DRIVE_ALIGN_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nLT01_Drive_Align) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_ALIGN_LIGHT_ON;
			}
		}
		break;

	case PPG_TM02_ALIGN_LIGHT_ON:
		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnLight();
		m_elPPMove_LT01_TM02 = PPG_TM02_ALIGN_LIGHT_ON_CHECK;	
		break;
	case PPG_TM02_ALIGN_LIGHT_ON_CHECK:
		if (G_MOTION_STATUS[TM02_LIGHT].B_COMMAND_STATUS == COMPLETE)
		{			
			m_elPPMove_LT01_TM02 = PPG_TM02_Z_ALIGN_POS_S1;			
		}
		break;			

	case PPG_TM02_Z_ALIGN_POS_S1:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nLT01_Z_Align);
		strLogMessage.Format("TM02 : 업다운축 간지 감지 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_Z_ALIGN_POS_CHECK_S1;
		break;
	case PPG_TM02_Z_ALIGN_POS_CHECK_S1:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nLT01_Z_Align) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_PAPER_CHECK;
			}
		}
		break;
	case PPG_TM02_PAPER_CHECK:
		g_cpMainDlg->m_cCameraControl.m_fnPaperDetect();
		strLogMessage.Format("TM02 : 업다운축 간지 감지 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_MAIN_SEQUENCE_IDLE;
		break;
	case PPG_TM02_PAPER_CHECK_RETURN:
		bResult = g_cpMainDlg->m_cCameraControl.m_gnGetPaperDetectResult();
		g_cpMainDlg->m_cStickTransper->m_fnAlignLightSet(0);

		//Pending
		//향후 간지 인식 기능을 추가 하여야 한다.
		//현재 위 함수는 TRUE 만 리턴한다.
		
		if (bResult)
		{
			m_elPPMove_LT01_TM02 = PPG_TM02_Z_READY_POS_1;
		}
		else
		{
			strLogMessage.Format("TM02 : 간지를 감지하지 못했습니다(LT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			m_elSeq_Error = ERR_LT01_PAPER_DETECT;
			m_elPPMove_LT01_TM02 = PPG_SEQUENCE_COMP;
		}
		break;
	case PPG_TM02_Z_READY_POS_1:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos);
		strLogMessage.Format("TM02 : 업다운축 대기 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_Z_READY_POS_CHECK_1;
		break;
	case PPG_TM02_Z_READY_POS_CHECK_1:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_DRIVE_GET_POS;
			}
		}		
		break;
	case PPG_TM02_DRIVE_GET_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nLT01_Drive_PP_Get);
		strLogMessage.Format("TM02 : 주행축 간지 흡착 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_DRIVE_GET_POS_CHECK;
		break;
	case PPG_TM02_DRIVE_GET_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nLT01_Drive_PP_Get) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_SHIFT_GET_POS;
			}
		}
		break;
	case PPG_TM02_SHIFT_GET_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(m_st_TM02_Cur_Recipe->nLT01_Shift_Get);
		strLogMessage.Format("TM02 : 쉬프트축 간지 흡착 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_SHIFT_GET_POS_CHECK;
		break;
	case PPG_TM02_SHIFT_GET_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
				m_st_TM02_Cur_Recipe->nLT01_Shift_Get) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_TILT_GET_POS;
			}
		}
		break;
	case PPG_TM02_TILT_GET_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(m_st_TM02_Cur_Recipe->nLT01_Tilt_Get);
		strLogMessage.Format("TM02 : 틸트축 간지 흡착 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_TILT_GET_POS_CHECK;
		break;
	case PPG_TM02_TILT_GET_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
				m_st_TM02_Cur_Recipe->nLT01_Tilt_Get) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_PP_CYL_DN;
			}
		}
		break;
	case PPG_TM02_PP_CYL_DN:
		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperCylDn();
		strLogMessage.Format("TM02 : 간지 흡착 실린더 다운(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_PP_CYL_DN_CHECK;
		break;
	case PPG_TM02_PP_CYL_DN_CHECK:
		if (G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerCylinder.In01_Sn_PickerPaperLeftDown == ON &&
				G_STMACHINE_STATUS.stPickerCylinder.In03_Sn_PickerPaperRightDown == ON)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_Z_PP_GET_READY_POS;
			}
		}
		break;

		//2015.03.26 GJJ S
	case PPG_TM02_Z_PP_GET_READY_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nLT01_Z_PP_Get - PICKER_READY_POSITION_OFFSET);
		strLogMessage.Format("TM02 : 업다운축 간지 흡착 1차 대기 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_Z_PP_GET_READY_POS_CHECK;

		break;
	case PPG_TM02_Z_PP_GET_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				(m_st_TM02_Cur_Recipe->nLT01_Z_PP_Get - PICKER_READY_POSITION_OFFSET)) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_Z_PP_GET_POS;
			}
		}
		break;
		//2015.03.26 GJJ E

	case PPG_TM02_Z_PP_GET_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nLT01_Z_PP_Get, Speed_Pickup);
		strLogMessage.Format("TM02 : 업다운축 간지 흡착 포지션 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_Z_PP_GET_POS_CHECK;
		break;
	case PPG_TM02_Z_PP_GET_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nLT01_Z_PP_Get) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_PP_PURGE_GET;
			}
		}
		break;

	case PPG_TM02_PP_PURGE_GET:
		g_cpMainDlg->m_cStickTransper->m_fnPaperPurgeOn(m_st_TM02_Cur_Recipe->stLT01_Vac_PP_Get, m_st_TM02_Cur_Recipe->nLT01_PP_Purge_Get);
		strLogMessage.Format("TM02 : 간지 흡착전 퍼지 실행(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_PP_VAC_ON;
		break;
	case PPG_TM02_PP_VAC_ON:
		g_cpMainDlg->m_cStickTransper->m_fnPaperVacuum(m_st_TM02_Cur_Recipe->stLT01_Vac_PP_Get, nBiz_Seq_TM02_PP_Vac_ON);
		strLogMessage.Format("TM02 : 간지 흡착 Vac On(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_PP_VAC_ON_CHECK;
		break;
	case PPG_TM02_PP_VAC_ON_CHECK:
		if (G_MOTION_STATUS[TM02_PP_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerSensor.In11_Sn_PickerBackPaperVacuum == ON)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_Z_ALIGN_POS_S2;
			}
		}
		break;
	case PPG_TM02_Z_ALIGN_POS_S2:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nLT01_Z_Align, Speed_Pickup);
		strLogMessage.Format("TM02 : 업다운축 1차 대기 위치로 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_Z_ALIGN_POS_CHECK_S2;
		break;
	case PPG_TM02_Z_ALIGN_POS_CHECK_S2:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nLT01_Z_Align) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_TM02_PP_INTERLOCK_CHECK;
			}
		}
		break;
	case PPG_TM02_PP_INTERLOCK_CHECK:
		if (G_STMACHINE_STATUS.stNormalStatus.In30_TwoPaperAbsorbInterlock1 == ON ||
			G_STMACHINE_STATUS.stNormalStatus.In31_TwoPaperAbsorbInterlock2 == ON)
		{
			m_elPPMove_LT01_TM02 = PPG_SEQUENCE_COMP;
			m_elSeq_Error = ERR_TM02_TWO_PAPER;

			strLogMessage.Format("TM02 : 간지 인터락 센서가 감지되었습니다(LT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);			
		}
		else
		{
			m_elPPMove_LT01_TM02 = PPG_TM02_Z_READY_POS_2;
		}
		break;
	case PPG_TM02_Z_READY_POS_2:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos);
		strLogMessage.Format("TM02 : 업다운축 대기 위치로 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_LT01_TM02 = PPG_TM02_Z_READY_POS_CHECK_2;
		break;
	case PPG_TM02_Z_READY_POS_CHECK_2:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elPPMove_LT01_TM02 = PPG_SEQUENCE_COMP;
			}
		}
		break;

	case PPG_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("TM02 : 간지 흡착 시나리오 에러 발생(LT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;
			
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("TM02 : 간지 흡착 시나리오 종료(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_LT01_T_TM02] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elPPMove_LT01_TM02 = PPG_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

void CPickerControl::m_fnSet_PaperMove_LT01_TM02_Step(EL_PAPER_MOVE_LT01_TM02 step)
{
	m_elPPMove_LT01_TM02 = step;
}

int	CPickerControl::m_fnPaperMove_TM02_UT01()
{
	CString strLogMessage;
	int	nTM02DrivePos = 0;
	BOOL bResult = FALSE;

	switch (m_elPPMove_TM02_UT01)
	{
	case PPU_MAIN_SEQUENCE_IDLE:
		break;
	case PPU_TM02_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			m_elPPMove_TM02_UT01 = PPU_TM02_DRIVE_PUT_POS;
			strLogMessage.Format("TM02 : 간지 배출 시나리오 시작(UT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elPPMove_TM02_UT01 = PPU_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}	
		
		break;
	case PPU_TM02_INTERLOCK_CHECK_LAST:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;
		m_bLastObject = TRUE;

		if (G_RECIPE_SET)
		{
			m_elPPMove_TM02_UT01 = PPU_TM02_DRIVE_PUT_POS;
			strLogMessage.Format("TM02 : 간지 배출 시나리오 시작(UT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elPPMove_TM02_UT01 = PPU_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}

		break;
	case PPU_TM02_DRIVE_PUT_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nUT01_Drive_PP_Put);
		strLogMessage.Format("TM02 : 주행축 간지 배출 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_DRIVE_PUT_POS_CHECK;
		break;
	case PPU_TM02_DRIVE_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nUT01_Drive_PP_Put) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_UT01 = PPU_TM02_SHIFT_PUT_POS;
			}
		}
		break;
	case PPU_TM02_SHIFT_PUT_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(m_st_TM02_Cur_Recipe->nUT01_Shift_Put);
		strLogMessage.Format("TM02 : 쉬프트축 간지 배출 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_SHIFT_PUT_POS_CHECK;
		break;
	case PPU_TM02_SHIFT_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
				m_st_TM02_Cur_Recipe->nUT01_Shift_Put) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_UT01 = PPU_TM02_TILT_PUT_POS;
			}
		}
		break;
	case PPU_TM02_TILT_PUT_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(m_st_TM02_Cur_Recipe->nUT01_Tilt_Put);
		strLogMessage.Format("TM02 : 틸트축 간지 배출 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_TILT_PUT_POS_CHECK;
		break;
	case PPU_TM02_TILT_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
				m_st_TM02_Cur_Recipe->nUT01_Tilt_Put) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_UT01 = PPU_TM02_Z_PUT_READY_POS;
			}
		}
		break;

	//2015.03.26 GJJ S
	case PPU_TM02_Z_PUT_READY_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nUT01_Z_PP_Put - PICKER_READY_POSITION_OFFSET);
		strLogMessage.Format("TM02 : 업다운축 간지 배출 1차 대기 포지션 이동(UT01)..");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_Z_PUT_READY_POS_CHECK;
		break;
	case PPU_TM02_Z_PUT_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				(m_st_TM02_Cur_Recipe->nUT01_Z_PP_Put - PICKER_READY_POSITION_OFFSET)) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_UT01 = PPU_TM02_Z_PUT_POS;
			}
		}
		break;
	//2015.03.26 GJJ E

	case PPU_TM02_Z_PUT_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nUT01_Z_PP_Put, Speed_Pickup);
		strLogMessage.Format("TM02 : 업다운축 간지 배출 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_Z_PUT_POS_CHECK;
		break;
	case PPU_TM02_Z_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nUT01_Z_PP_Put) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_UT01 = PPU_TM02_PP_VAC_OFF;
			}
		}
		break;
	case PPU_TM02_PP_VAC_OFF:
		g_cpMainDlg->m_cStickTransper->m_fnPaperVacuum(m_st_TM02_Cur_Recipe->stLT01_Vac_PP_Get, nBiz_Seq_TM02_PP_Vac_OFF);
		strLogMessage.Format("TM02 : 간지 배출 Vac Off(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_PP_PURGE_PUT;
		break;
	case PPU_TM02_PP_PURGE_PUT:
		g_cpMainDlg->m_cStickTransper->m_fnPaperPurgeOn(m_st_TM02_Cur_Recipe->stLT01_Vac_PP_Get, m_st_TM02_Cur_Recipe->nUT01_Purge_Put);
		strLogMessage.Format("TM02 : 간지 배출 Purge On(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_PP_VAC_OFF_CHECK;
		break;
	case PPU_TM02_PP_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TM02_PP_VAC].B_COMMAND_STATUS == COMPLETE &&
			G_MOTION_STATUS[TM02_PP_PURGE].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerSensor.In11_Sn_PickerBackPaperVacuum == OFF)
			{
				m_elPPMove_TM02_UT01 = PPU_TM02_PP_CYL_UP;
			}
		}
		break;
	case PPU_TM02_PP_CYL_UP:		
		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperCylUp();
		strLogMessage.Format("TM02 : 간지 실린더 업(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_PP_CYL_UP_CHECK;
		break;
	case PPU_TM02_PP_CYL_UP_CHECK:
		if (G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerCylinder.In00_Sn_PickerPaperLeftUp == ON &&
				G_STMACHINE_STATUS.stPickerCylinder.In02_Sn_PickerPaperRightUp == ON)
			{
				m_elPPMove_TM02_UT01 = PPU_TM02_Z_READY_POS;
			}
		}
		break;
	case PPU_TM02_Z_READY_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos);
		strLogMessage.Format("TM02 : 업다운축 대기 위치 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_UT01 = PPU_TM02_Z_READY_POS_CHECK;
		break;
	case PPU_TM02_Z_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				if (m_bLastObject == TRUE)
					m_elPPMove_TM02_UT01 = PPU_TM02_DRIVE_TT01_POS;
				else
					m_elPPMove_TM02_UT01 = PPU_SEQUENCE_COMP;
			}
		}
		break;

	case PPU_TM02_DRIVE_TT01_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nTT01_Dirve_Put);
		m_elPPMove_TM02_UT01 = PPU_TM02_DRIVE_TT01_POS_CHECK;
		strLogMessage.Format("TM02 : 주행축 턴테이블 위치 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case PPU_TM02_DRIVE_TT01_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nTT01_Dirve_Put) <= POSITION_OFFSET)
			{
				m_bLastObject = FALSE;
				m_elPPMove_TM02_UT01 = PPU_SEQUENCE_COMP;
			}
		}
		break;


	case PPU_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{			
			strLogMessage.Format("TM02 : 간지 배출 시나리오 에러 발생(UT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;

			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("TM02 : 간지 배출 시나리오 종료(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_bLastObject = FALSE;
		g_cpMainDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_UT01] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elPPMove_TM02_UT01 = PPU_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}


int	CPickerControl::m_fnPaperMove_TM02_BT01()
{
	CString strLogMessage;
	int	nTM02DrivePos = 0;
	BOOL bResult = FALSE;

	switch (m_elPPMove_TM02_BT01)
	{
	case PPB_MAIN_SEQUENCE_IDLE:
		break;
	case PPB_TM02_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;	
		G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			m_elPPMove_TM02_BT01 = PPB_TM02_DRIVE_PUT_POS;
			strLogMessage.Format("TM02 : 간지 배출 시나리오 시작(BT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elPPMove_TM02_BT01 = PPB_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}	

		break;
	case PPB_TM02_INTERLOCK_CHECK_LAST:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;
		m_bLastObject = TRUE;

		if (G_RECIPE_SET)
		{
			m_elPPMove_TM02_BT01 = PPB_TM02_DRIVE_PUT_POS;
			strLogMessage.Format("TM02 : 간지 배출 시나리오 시작(BT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elPPMove_TM02_BT01 = PPB_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}

		break;
	case PPB_TM02_DRIVE_PUT_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nBT01_Drive_PP_Put);
		strLogMessage.Format("TM02 : 주행축 간지 배출 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_DRIVE_PUT_POS_CHECK;
		break;
	case PPB_TM02_DRIVE_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nBT01_Drive_PP_Put) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_BT01 = PPB_TM02_SHIFT_PUT_POS;
			}
		}
		break;
	case PPB_TM02_SHIFT_PUT_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(m_st_TM02_Cur_Recipe->nBT01_Shift_Put);
		strLogMessage.Format("TM02 : 쉬프트축 간지 배출 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_SHIFT_PUT_POS_CHECK;
		break;
	case PPB_TM02_SHIFT_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
				m_st_TM02_Cur_Recipe->nBT01_Shift_Put) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_BT01 = PPB_TM02_TILT_PUT_POS;
			}
		}
		break;
	case PPB_TM02_TILT_PUT_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(m_st_TM02_Cur_Recipe->nBT01_Tilt_Put);
		strLogMessage.Format("TM02 : 틸트축 간지 배출 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_TILT_PUT_POS_CHECK;
		break;
	case PPB_TM02_TILT_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
				m_st_TM02_Cur_Recipe->nBT01_Tilt_Put) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_BT01 = PPB_TM02_Z_PUT_READY_POS;
			}
		}
		break;

	//2015.03.26 GJJ S
	case PPB_TM02_Z_PUT_READY_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nBT01_Z_PP_Put - PICKER_READY_POSITION_OFFSET);
		strLogMessage.Format("TM02 : 업다운축 간지 배출 1차 대기 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_Z_PUT_READY_POS_CHECK;
		break;
	case PPB_TM02_Z_PUT_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				(m_st_TM02_Cur_Recipe->nBT01_Z_PP_Put - PICKER_READY_POSITION_OFFSET)) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_BT01 = PPB_TM02_Z_PUT_POS;
			}
		}
		break;
	//2015.03.26 GJJ E

	case PPB_TM02_Z_PUT_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nBT01_Z_PP_Put, Speed_Pickup);
		strLogMessage.Format("TM02 : 업다운축 간지 배출 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_Z_PUT_POS_CHECK;
		break;
	case PPB_TM02_Z_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nBT01_Z_PP_Put) <= POSITION_OFFSET)
			{
				m_elPPMove_TM02_BT01 = PPB_TM02_PP_VAC_OFF;
			}
		}
		break;
	case PPB_TM02_PP_VAC_OFF:
		g_cpMainDlg->m_cStickTransper->m_fnPaperVacuum(m_st_TM02_Cur_Recipe->stLT01_Vac_PP_Get, nBiz_Seq_TM02_PP_Vac_OFF);
		strLogMessage.Format("TM02 : 간지 배출 Vac Off(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_PP_PURGE_PUT;
		break;
	case PPB_TM02_PP_PURGE_PUT:
		g_cpMainDlg->m_cStickTransper->m_fnPaperPurgeOn(m_st_TM02_Cur_Recipe->stLT01_Vac_PP_Get, m_st_TM02_Cur_Recipe->nUT01_Purge_Put);
		strLogMessage.Format("TM02 : 간지 배출 Purge On(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_PP_VAC_OFF_CHECK;
		break;
	case PPB_TM02_PP_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TM02_PP_VAC].B_COMMAND_STATUS == COMPLETE &&
			G_MOTION_STATUS[TM02_PP_PURGE].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerSensor.In11_Sn_PickerBackPaperVacuum == OFF)
			{
				m_elPPMove_TM02_BT01 = PPB_TM02_PP_CYL_UP;
			}
		}
		break;
	case PPB_TM02_PP_CYL_UP:		
		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnPaperCylUp();
		strLogMessage.Format("TM02 : 간지 실린더 업(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_PP_CYL_UP_CHECK;
		break;
	case PPB_TM02_PP_CYL_UP_CHECK:
		if (G_MOTION_STATUS[TM02_LR_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerCylinder.In00_Sn_PickerPaperLeftUp == ON &&
				G_STMACHINE_STATUS.stPickerCylinder.In02_Sn_PickerPaperRightUp == ON)
			{
				m_elPPMove_TM02_BT01 = PPB_TM02_Z_READY_POS;
			}
		}
		break;
	case PPB_TM02_Z_READY_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos);
		strLogMessage.Format("TM02 : 업다운축 대기 위치 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elPPMove_TM02_BT01 = PPB_TM02_Z_READY_POS_CHECK;
		break;
	case PPB_TM02_Z_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				if (m_bLastObject == TRUE)
					m_elPPMove_TM02_BT01 = PPB_TM02_DRIVE_TT01_POS;
				else
					m_elPPMove_TM02_BT01 = PPB_SEQUENCE_COMP;
			}
		}
		break;
	case PPB_TM02_DRIVE_TT01_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nTT01_Dirve_Put);
		m_elPPMove_TM02_BT01 = PPB_TM02_DRIVE_TT01_POS_CHECK;
		strLogMessage.Format("TM02 : 주행축 턴테이블 위치 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case PPB_TM02_DRIVE_TT01_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nTT01_Dirve_Put) <= POSITION_OFFSET)
			{
				m_bLastObject = FALSE;
				m_elPPMove_TM02_BT01 = PPB_SEQUENCE_COMP;
			}
		}
		break;
	case PPB_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{			
			strLogMessage.Format("TM02 : 간지 배출 시나리오 에러 발생(BT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);			

			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;

			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("TM02 : 간지 배출 시나리오 종료(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_bLastObject = FALSE;
		G_EL_SUB_SEQ_STATUS[PAPER_MOVE_F_TM02_T_BT01] = SEQ_COMPLETE;
		g_cpMainDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elPPMove_TM02_BT01 = PPB_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

int CPickerControl::m_fnStickMove_LT01_TM02()
{
	CString strLogMessage;
	BOOL bResult = FALSE;
	int	 nShiftOffset = 0;
	int  nTiltOffset = 0;
	int  nDriveOffset = 0;

	switch (m_elSTMove_LT01_TM02)
	{
	case STLT_MAIN_SEQUENCE_IDLE:
		break;
	case STLT_TM02_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_RUNNING;
		if(G_RECIPE_SET)
		{
			m_elSTMove_LT01_TM02 = STLT_TM02_DRIVE_ALIGN_POS;
			strLogMessage.Format("TM02 : 스틱 흡착 시나리오 시작(LT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_LT01_TM02 = STLT_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		///////  간지 실린더 다운되어 있으면 ...//////////////////////////-20150319 정의천
		if (G_STMACHINE_STATUS.stPickerCylinder.In01_Sn_PickerPaperLeftDown == ON &&
			G_STMACHINE_STATUS.stPickerCylinder.In03_Sn_PickerPaperRightDown == ON)
		{
			m_elSTMove_LT01_TM02 = STLT_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 간지 실린더 다운 되어 있습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		//////////////////////////////////////////////////////////////////////////

		break;
	case STLT_TM02_DRIVE_ALIGN_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nLT01_Drive_Align);
		m_elSTMove_LT01_TM02 = STLT_TM02_DRIVE_ALIGN_POS_CHECK;
		strLogMessage.Format("TM02 : 주행축 스틱 얼라인 위치 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_DRIVE_ALIGN_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nLT01_Drive_Align) <= POSITION_OFFSET)
			{
				m_elSTMove_LT01_TM02 = STLT_TM02_ALIGN_LIGHT_ON;
			}
		}
		break;
	case STLT_TM02_ALIGN_LIGHT_ON:
		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnLight();
		m_elSTMove_LT01_TM02 = STLT_TM02_ALIGN_LIGHT_ON_CHECK;		
		break;
	case STLT_TM02_ALIGN_LIGHT_ON_CHECK:
		if (G_MOTION_STATUS[TM02_LIGHT].B_COMMAND_STATUS == COMPLETE)
		{			
			m_elSTMove_LT01_TM02 = STLT_TM02_Z_ALIGN_POS_S1;			
		}
		break;
	case STLT_TM02_Z_ALIGN_POS_S1:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nLT01_Z_Align);
		m_elSTMove_LT01_TM02 = STLT_TM02_Z_ALIGN_POS_CHECK_S1;
		strLogMessage.Format("TM02 : 업다운축 스틱 얼라인 위치 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_Z_ALIGN_POS_CHECK_S1:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nLT01_Z_Align) <= POSITION_OFFSET)
			{
				m_elSTMove_LT01_TM02 = STLT_TM02_STICK_CHECK;
			}
		}
		break;	
	case STLT_TM02_STICK_CHECK:
		g_cpMainDlg->m_cCameraControl.m_fnStickDetect();
		m_elSTMove_LT01_TM02 = STLT_MAIN_SEQUENCE_IDLE;
		strLogMessage.Format("TM02 : 스틱 유무 확인(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_STICK_CHECK_RETURN:
		bResult = g_cpMainDlg->m_cCameraControl.m_gnGetStickDetectResult();
		//bResult = TRUE;

		if (bResult)
		{
			m_elSTMove_LT01_TM02 = STLT_TM02_SHIFT_GET_POS;
		}
		else
		{
			g_cpMainDlg->m_cStickTransper->m_fnAlignLightSet(0);

			strLogMessage.Format("TM02 : 스틱을 감지하지 못했습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			m_elSeq_Error = ERR_LT01_STICK_DETECT;
			m_elSTMove_LT01_TM02 = STLT_SEQUENCE_COMP;
		}
		break;
	case STLT_TM02_SHIFT_GET_POS:		
		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(m_st_TM02_Cur_Recipe->nLT01_Shift_Get);
		m_elSTMove_LT01_TM02 = STLT_TM02_SHIFT_GET_POS_CHECK;		
		strLogMessage.Format("TM02 : 쉬프트 축 흡착 위치 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_SHIFT_GET_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
				(m_st_TM02_Cur_Recipe->nLT01_Shift_Get)) <= POSITION_OFFSET)
			{
				m_elSTMove_LT01_TM02 = STLT_TM02_TILT_GET_POS;
			}
		}
		break;
	case STLT_TM02_TILT_GET_POS:		
		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(m_st_TM02_Cur_Recipe->nLT01_Tilt_Get);
		m_elSTMove_LT01_TM02 = STLT_TM02_TILT_GET_POS_CHECK;
		strLogMessage.Format("TM02 : 회전 축 흡착 위치 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_TILT_GET_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
				(m_st_TM02_Cur_Recipe->nLT01_Tilt_Get)) <= POSITION_OFFSET)
			{
				m_elSTMove_LT01_TM02 = STLT_TM02_STICK_ALIGN;
			}
		}
		break;
	case STLT_TM02_STICK_ALIGN:
		g_cpMainDlg->m_cCameraControl.m_fnStickAlign();
		m_elSTMove_LT01_TM02 = STLT_MAIN_SEQUENCE_IDLE;
		strLogMessage.Format("TM02 : 얼라인 동작 수행(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case STLT_TM02_SHIFT_ALIGN_POS:
		nShiftOffset = g_cpMainDlg->m_cCameraControl.m_fnGetShiftOffset();
		nShiftOffset = int(double(nShiftOffset * PICKER_SHIFT_VALUE));		
		
		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] + nShiftOffset);
		m_elSTMove_LT01_TM02 = STLT_TM02_SHIFT_ALIGN_POS_CHECK;

		strLogMessage.Format("TM02 : 쉬프트 축 얼라인 보정치 적용(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_SHIFT_ALIGN_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{			
			m_elSTMove_LT01_TM02 = STLT_TM02_TILT_ALIGN_POS;		
		}
		break;
	case STLT_TM02_TILT_ALIGN_POS:
		nTiltOffset = g_cpMainDlg->m_cCameraControl.m_fnGetTiltOffset();
		nTiltOffset = int(double(nTiltOffset * PICKER_ROTATE_VALUE));	
		
		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] - nTiltOffset);
		m_elSTMove_LT01_TM02 = STLT_TM02_TILT_ALIGN_POS_CHECK;	
		strLogMessage.Format("TM02 : 회전 축 얼라인 보정치 적용(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case STLT_TM02_TILT_ALIGN_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{			
			m_elSTMove_LT01_TM02 = STLT_TM02_DRIVE_ALIGN_POS2;			
		}
		break;			

	case STLT_TM02_DRIVE_ALIGN_POS2:
		nDriveOffset = g_cpMainDlg->m_cCameraControl.m_fnGetDriveOffset();
		nDriveOffset = int(double(nDriveOffset * PICKER_DRIVE_VALUE));	
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] - nDriveOffset);
		m_elSTMove_LT01_TM02 = STLT_TM02_DRIVE_ALIGN_POS_CHECK2;
		strLogMessage.Format("TM02 : 주행 축 얼라인 보정치 적용(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;

	case STLT_TM02_DRIVE_ALIGN_POS_CHECK2:		
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{			
			m_elSTMove_LT01_TM02 = STLT_TM02_STICK_ALIGN;			
		}
		break;

	case STLT_TM02_STICK_ALIGN_RETURN:
		g_cpMainDlg->m_cCameraControl.m_fnOffsetClear();
		g_cpMainDlg->m_cStickTransper->m_fnAlignLightSet(0);

		m_elSTMove_LT01_TM02 = STLT_TM02_DRIVE_GET_POS;
		//m_elSTMove_LT01_TM02 = STLT_SEQUENCE_COMP;
		strLogMessage.Format("TM02 : 얼라인 동작 완료(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		
		break;	

	case STLT_TM02_DRIVE_GET_POS:
		//nDriveOffset = g_cpMainDlg->m_cCameraControl.m_fnGetDriveOffset();
		//g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nLT01_Drive_ST_Get + nDriveOffset);
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive]  - PICKER_PAD_OFFSET);
		m_elSTMove_LT01_TM02 = STLT_TM02_DRIVE_GET_POS_CHECK;
		strLogMessage.Format("TM02 : 주행축 흡착 위치 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_DRIVE_GET_POS_CHECK:
		nDriveOffset = g_cpMainDlg->m_cCameraControl.m_fnGetDriveOffset();
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			//if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
			//	m_st_TM02_Cur_Recipe->nLT01_Drive_ST_Get + nDriveOffset) <= POSITION_OFFSET)
			{
				m_elSTMove_LT01_TM02 = STLT_TM02_Z_ST_GET_POS;
			}
		}
		break;	
	case STLT_TM02_Z_ST_GET_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nLT01_Z_ST_Get, Speed_Pickup);
		m_elSTMove_LT01_TM02 = STLT_TM02_Z_ST_GET_POS_CHECK;
		strLogMessage.Format("TM02 : 업다운축 흡착 위치 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_Z_ST_GET_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nLT01_Z_ST_Get) <= POSITION_OFFSET)
			{
				m_elSTMove_LT01_TM02 = STLT_TM02_ST_VAC_ON;
			}
		}
		break;
	case STLT_TM02_ST_VAC_ON:
		g_cpMainDlg->m_cStickTransper->m_fnStickVacuum(m_st_TM02_Cur_Recipe->stLT01_Vac_ST_Get, nBiz_Seq_TM02_ST_Vac_ON);
		m_elSTMove_LT01_TM02 = STLT_TM02_ST_VAC_ON_CHECK;
		strLogMessage.Format("TM02 : Vac. On(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_ST_VAC_ON_CHECK:
		if (G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerFrontStickVacuum == ON)
			{
				m_elSTMove_LT01_TM02 = STLT_TM02_Z_READY_POS;
			}
		}
		break;
	case STLT_TM02_Z_READY_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos, Speed_Pickup);
		m_elSTMove_LT01_TM02 = STLT_TM02_Z_READY_POS_CHECK;
		strLogMessage.Format("TM02 : 업다운 축 대기 위치 이동(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STLT_TM02_Z_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_LT01_TM02 = STLT_TM02_ST_VAC_ON_CHECK2;
			}
		}
		break;
	case STLT_TM02_ST_VAC_ON_CHECK2:
		if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerFrontStickVacuum == OFF)
		{
			strLogMessage.Format("TM02 : VAC 에러가 발생했습니다(LT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			m_elSeq_Error = ERR_TM01_VAC_ON;
		}
		m_elSTMove_LT01_TM02 = STLT_SEQUENCE_COMP;
		break;
	case STLT_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
			strLogMessage.Format("TM02 : 스틱 흡착 시나리오 에러 발생(LT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		}
		strLogMessage.Format("TM02 : 스틱 흡착 시나리오 종료(LT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_LT01_T_TM02] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elSTMove_LT01_TM02 = STLT_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

void CPickerControl::m_fnSet_StickMove_LT01_TM02_Step(EL_STICK_MOVE_LT01_TM02 step)
{
	m_elSTMove_LT01_TM02 = step;
}


int CPickerControl::m_fnStickMove_TM02_TT01()
{
	CString strLogMessage;
	BOOL bResult = FALSE;

	switch (m_elSTMove_TM02_TT01)
	{
	case STTT_MAIN_SEQUENCE_IDLE:
		break;
	case STTT_TM02_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			//m_elSTMove_TM02_TT01 = STTT_TM02_DRIVE_QR_POS;
			m_elSTMove_TM02_TT01 = STTT_TM02_Z_READY_POS_1;
			strLogMessage.Format("TM02 : 스틱 이송 시나리오 시작(TT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_TM02_TT01 = STTT_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}	

		break;
	case STTT_TM02_DRIVE_QR_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nQR_Drive_Pos);
		m_elSTMove_TM02_TT01 = STTT_TM02_DRIVE_QR_POS_CHECK;
		strLogMessage.Format("TM02 : 주행축 QR 포지션 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TM02_DRIVE_QR_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nQR_Drive_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_Z_QR_POS;
			}
		}
		break;
	case STTT_TM02_Z_QR_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nQR_Z_Pos, Speed_Pickup);
		m_elSTMove_TM02_TT01 = STTT_TM02_Z_QR_POS_CHECK;
		strLogMessage.Format("TM02 : 업다운 축 QR 위치 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TM02_Z_QR_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nQR_Z_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_READ_QR_CODE;
			}
		}
		break;
	case STTT_TM02_READ_QR_CODE:
		g_cpMainDlg->m_cStickTransper->OnBnClickedBtnReadQr();
		m_elSTMove_TM02_TT01 = STTT_TM02_READ_QR_CODE_RETURN;
		strLogMessage.Format("TM02 : QR 코드 읽기(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TM02_READ_QR_CODE_RETURN:
		if (G_MOTION_STATUS[TM02_QR].B_COMMAND_STATUS = COMPLETE)
		{
			m_elSTMove_TM02_TT01 = STTT_TM02_Z_READY_POS_1;
		}		
		break;
	case STTT_TM02_Z_READY_POS_1:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos, Speed_Pickup);
		m_elSTMove_TM02_TT01 = STTT_TM02_Z_READY_POS_CHECK_1;
		strLogMessage.Format("TM02 : 업다운 축 대기 위치 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TM02_Z_READY_POS_CHECK_1:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_DRIVE_TT01_POS;
				Sleep(2000);
			}
		}
		break;
	case STTT_TM02_DRIVE_TT01_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nTT01_Dirve_Put);
		m_elSTMove_TM02_TT01 = STTT_TM02_DRIVE_TT01_POS_CHECK;
		strLogMessage.Format("TM02 : 주행축 턴테이블 위치 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TM02_DRIVE_TT01_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nTT01_Dirve_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_SHIFT_TT01_POS;
			}
		}
		break;
	case STTT_TM02_SHIFT_TT01_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(m_st_TM02_Cur_Recipe->nTT01_Shift_Put);
		m_elSTMove_TM02_TT01 = STTT_TM02_SHIFT_TT01_POS_CHECK;
		strLogMessage.Format("TM02 : 쉬프트 축 턴테이블 위치 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TM02_SHIFT_TT01_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
				m_st_TM02_Cur_Recipe->nTT01_Shift_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_TILT_TT01_POS;
			}
		}
		break;
	case STTT_TM02_TILT_TT01_POS:
		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(m_st_TM02_Cur_Recipe->nTT01_Tilt_Put);
		m_elSTMove_TM02_TT01 = STTT_TM02_TILT_TT01_POS_CHECK;
		strLogMessage.Format("TM02 : 회전 축 턴테이블 위치 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TM02_TILT_TT01_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
				m_st_TM02_Cur_Recipe->nTT01_Tilt_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_Z_TT01_POS;
			}
		}
		break;
	case STTT_TM02_Z_TT01_POS:
		//턴테이블 위치, 상면 체크.

		if (G_STMACHINE_STATUS.stTurnTableSensor.In01_Sn_TurnTableUpperLeftStickDetect == ON ||
			G_STMACHINE_STATUS.stTurnTableSensor.In02_Sn_TurnTableUpperRightStickDetect == ON)
		{
			//현재 턴테이블 상면에 스특이 감지 된다면 대기.
			break;
		}
			
		if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
			m_st_TT01_Pos->nDrive_Ready_Pos) <= POSITION_OFFSET)  //UT02 에 위치 하는가?
		{
			if (g_cpMainDlg->m_elTurnTableSequence == SUB_SEQUENCE_IDLE)
			{
				if (G_STMACHINE_STATUS.stTurnTableSensor.In05_Sn_TurnTableUpperPosition == ON) //상면이 위에 있는가?
				{
					strLogMessage.Format("TT01 : 상면 정위치 체크 확인. OK");
					g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
					g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nTT01_Z_ST_Put, Speed_Pickup);
					m_elSTMove_TM02_TT01 = STTT_TM02_Z_TT01_POS_CHECK;
					strLogMessage.Format("TM02 : 업다운 축 스틱 전달 위치 이동(TT01).");
					g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
				}
			}			
		}
		
		break;
	case STTT_TM02_Z_TT01_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nTT01_Z_ST_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_VAC_OFF;
			}
		}
		break;
	case STTT_TM02_VAC_OFF:
		g_cpMainDlg->m_cStickTransper->m_fnStickVacuum(m_st_TM02_Cur_Recipe->stLT01_Vac_ST_Get, nBiz_Seq_TM02_ST_Vac_OFF);
		m_elSTMove_TM02_TT01 = STTT_TT01_VAC_ON;
		strLogMessage.Format("TM02 : 스틱 흡착 Vac Off(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TT01_VAC_ON:
		g_cpMainDlg->m_cTurnTable->m_fnStickUpperVac(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, nBiz_Seq_TT01_UP_Vac_ON);
		m_elSTMove_TM02_TT01 = STTT_TT01_VAC_ON_CHECK;
		strLogMessage.Format("TT01 : 스틱 흡착 Vac On(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TT01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[TT01_UP_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stTurnTableSensor.In00_Sn_TurnTableUpperVacuum == ON)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_VAC_OFF_CHECK;
			}
		}
		break;	
// 	case STTT_TM02_PURGE:
// 		g_cpMainDlg->m_cStickTransper->m_fnStickPurgeOn(m_st_TM02_Cur_Recipe->stLT01_Vac_ST_Get, m_st_TM02_Cur_Recipe->nTT01_ST_Purge_Put);
// 		m_elSTMove_TM02_TT01 = STTT_TM02_VAC_OFF_CHECK;
// 		break;
	case STTT_TM02_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerSensor.In00_Sn_PickerFrontStickVacuum == OFF)
			{
				m_elSTMove_TM02_TT01 = STTT_TM02_Z_READY_POS_2;
			}
		}
		break;
	case STTT_TM02_Z_READY_POS_2:
		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos);
		m_elSTMove_TM02_TT01 = STTT_TM02_Z_READY_POS_CHECK_2;
		strLogMessage.Format("TM02 : 업다운 축 대기 위치 이동(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		break;
	case STTT_TM02_Z_READY_POS_CHECK_2:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_TT01 = STTT_SEQUENCE_COMP;
			}
		}
		break;
	case STTT_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("TM02 : 스틱 이송 시나리오 에러 발생(TT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;

			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("TM02 : 스틱 이송 시나리오 종료(TT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_TT01] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elSTMove_TM02_TT01 = STTT_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

void CPickerControl::m_fnSet_StickMove_TM02_TT01_Step(EL_STICK_MOVE_TM02_TT01 step)
{
	m_elSTMove_TM02_TT01 = step;
}

int CPickerControl::m_fnStickMove_TT01_AM01()
{
	CString strLogMessage;
	BOOL bResult = FALSE;
	UCHAR* chMsgBuf;
	int nMessSize = 0;

	switch (m_elSTMove_TT01_AM01)
	{
	case STTA_MAIN_SEQUENCE_IDLE:
		break;
	case STTA_TT01_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TT01_T_AM01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			m_elSTMove_TT01_AM01 = STTA_SND_STICK_INFO;
			strLogMessage.Format("TT01 : 스틱 이송 시나리오 시작(AM01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_TT01_AM01 = STTA_SEQUENCE_COMP;
			strLogMessage.Format("TT01 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		break;
	case STTA_SND_STICK_INFO:		
		strLogMessage.Format("TT01 : 스틱 인포메이션 전달.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		//strncpy_s(m_stStickInfo.chBoxBarcode, MAX_BARCODE, g_cpMainDlg->m_cBoxTransper->m_chBoxBarcode, MAX_BARCODE);
		//strncpy_s(m_stStickInfo.chStickQRcode, MAX_QRCODE, g_cpMainDlg->m_cStickTransper->m_chStickQRcode, MAX_QRCODE);
		strncpy_s(m_stStickInfo.chBoxBarcode, MAX_BARCODE, "A3MSI01N_CIM_TEST_BOX", strlen("A3MSI01N_CIM_TEST_BOX"));
		strncpy_s(m_stStickInfo.chStickQRcode, MAX_QRCODE, "DCW1L18GRN150129_999", strlen("DCW1L18GRN150129_999"));
		
		m_stStickInfo.nLoadOffset = m_st_TM02_Cur_Recipe->nAM01LoadOffset;
		nMessSize = sizeof(SSTICK_INFO);
		chMsgBuf = new UCHAR[nMessSize + 1];
		memset((void*)chMsgBuf, 0x00, nMessSize + 1);
		memcpy((char*)chMsgBuf, &m_stStickInfo, nMessSize);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Stick_Information, nBiz_Unit_Zero, nMessSize, chMsgBuf);
		delete[]chMsgBuf;

		m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
		break;
	case STTA_RCV_STICK_INFO_ACK:
		strLogMessage.Format("TT01 : AM01 스틱 인포메이션 정상 수신 확인.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_elSTMove_TT01_AM01 = STTA_SND_STICK_LOAD_READY;
		break;
	case STTA_SND_STICK_LOAD_READY:
		strLogMessage.Format("TT01 : AM01 대기 상태 확인 요청.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Stick_Load_Ready, nBiz_Unit_Zero);
		m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
		break;
	case STTA_RCV_STICK_LOAD_READY_ACK:
		strLogMessage.Format("TT01 : AM01 대기 상태 확인.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		m_elSTMove_TT01_AM01 = STTA_TT01_DRIVE_AM01_POS;
		break;
	case STTA_TT01_DRIVE_AM01_POS:
		strLogMessage.Format("TT01 : AM01 스틱 로딩 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_AM01_Pos);
		m_elSTMove_TT01_AM01 = STTA_TT01_DRIVE_AM01_POS_CHECK;
		break;
	case STTA_TT01_DRIVE_AM01_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_AM01_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TT01_AM01 = STTA_SND_TT01_DRIVE_IN_POSITION;
			}
		}
		break;
	case STTA_SND_TT01_DRIVE_IN_POSITION:
		strLogMessage.Format("TT01 : AM01 정위치 전달.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_AM01, nBiz_Unit_Zero);
		m_elSTMove_TT01_AM01 = STTA_SND_AM01_ALIGN_START;
		break;

	case STTA_SND_AM01_ALIGN_START:
		strLogMessage.Format("TT01 : AM01 스틱 얼라인 동작 가능 상태 전달.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Theta_Align_Start, nBiz_Unit_Zero);
		m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
		break;

	case STTA_RCV_AM01_ALIGN_END:
		strLogMessage.Format("TT01 : AM01 Align 동작 수행 완료.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
		break;	

	case STTA_RCV_STICK_VAC_ON:
		strLogMessage.Format("TT01 : 상면 스틱 흡착 수행 수신.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnStickUpperVac(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, nBiz_Seq_TT01_UP_Vac_ON);
		m_elSTMove_TT01_AM01 = STTA_STICK_VAC_ON_CHECK;
		break;

	case STTA_STICK_VAC_ON_CHECK:
		if (G_MOTION_STATUS[TT01_UP_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stTurnTableSensor.In00_Sn_TurnTableUpperVacuum == ON)
			{
				m_elSTMove_TT01_AM01 = STTA_SND_STICK_VAC_ON;
			}
		}
		break;
	case STTA_SND_STICK_VAC_ON:
		strLogMessage.Format("TT01 : 상면 스틱 흡착 수행 완료 전달(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Load_Vac_On_Ack, nBiz_Unit_Zero);
		m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
		break;
	case STTA_RCV_STICK_VAC_OFF:
		strLogMessage.Format("TT01 : 상면 스틱 흡착 해제 수행 수신.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnStickUpperVac(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, nBiz_Seq_TT01_UP_Vac_OFF);
		m_elSTMove_TT01_AM01 = STTA_STICK_PURGE;
		break;
	case STTA_STICK_PURGE:
		strLogMessage.Format("TT01 : 상면 퍼지 동작.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnStickUpperPurge(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, m_st_TM02_Cur_Recipe->nTT01_ST_Purge_Put);
		m_elSTMove_TT01_AM01 = STTA_STICK_VAC_OFF_CHECK;		
		break;
	case STTA_STICK_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TT01_UP_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stTurnTableSensor.In00_Sn_TurnTableUpperVacuum == OFF)
			{
				m_elSTMove_TT01_AM01 = STTA_SND_STICK_VAC_OFF;
			}
		}		
		break;
	case STTA_SND_STICK_VAC_OFF:
		strLogMessage.Format("TT01 : 상면 스틱 흡착 해제 수행 완료 전달(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Load_Vac_Off_Ack, nBiz_Unit_Zero);
		m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
		break;
	case STTA_RCV_AM01_LOAD_STANDBY:
		strLogMessage.Format("TT01 : AM01 그리퍼 클로즈(Load Standby).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_elSTMove_TT01_AM01 = STTA_TT01_VAC_OFF;
		break;
	case STTA_TT01_VAC_OFF:
		strLogMessage.Format("TT01 : 상면 스틱 흡착 해제.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnStickUpperVac(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, nBiz_Seq_TT01_UP_Vac_OFF);
		m_elSTMove_TT01_AM01 = STTA_TT01_PURGE;
		break;
	case STTA_TT01_PURGE:
		strLogMessage.Format("TT01 : 상면 퍼지 동작.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnStickUpperPurge(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, m_st_TM02_Cur_Recipe->nTT01_ST_Purge_Put);
		m_elSTMove_TT01_AM01 = STTA_TT01_VAC_OFF_CHECK;		
		break;
	case STTA_TT01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TT01_UP_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stTurnTableSensor.In00_Sn_TurnTableUpperVacuum == OFF)
			{
				m_elSTMove_TT01_AM01 = STTA_SND_TT01_VAC_OFF;
			}
		}
		break;
	case STTA_SND_TT01_VAC_OFF:
		strLogMessage.Format("TT01 : 상면 스틱 흡착 해제 수행 완료 전달(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_VAC_OFF, nBiz_Unit_Zero);
		m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
		break;
	case STTA_RCV_AM01_STICK_LOAD_COMP:
		strLogMessage.Format("TT01 : AM01 Stick Load Complete.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_elSTMove_TT01_AM01 = STTA_TT01_DRIVE_TURN_POS;
		// 현재 test 중 스틱 반전이 불필요하여....원복 필요
		//m_elSTMove_TT01_AM01 = STTA_TT01_DRIVE_UT02_POS; //20150315 정의천
		break;
	case STTA_TT01_DRIVE_TURN_POS:
		if (m_bTurnTablePositionLock == FREE)
		{
			strLogMessage.Format("TT01 : 주행 축 턴 포지션 이동.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

			g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_Turn_Pos);
			m_elSTMove_TT01_AM01 = STTA_TT01_DRIVE_TURN_POS_CHECK;
		}		
		break;
	case STTA_TT01_DRIVE_TURN_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_Turn_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TT01_AM01 = STTA_SND_TT01_DRIVE_TURN_POS;
			}
		}
		break;
	case STTA_SND_TT01_DRIVE_TURN_POS:
		strLogMessage.Format("TT01 : 주행 축 턴 포지션 도착 전달(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_Turn, nBiz_Unit_Zero);
		m_elSTMove_TT01_AM01 = STTA_TT01_ROTATE_TURN_DN;
		break;
	case STTA_TT01_ROTATE_TURN_DN:
		strLogMessage.Format("TT01 : 회전축 하면 탑 위치로 회전.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01RotateMove(m_st_TT01_Pos->nRotate_SideDn_Pos);
		m_elSTMove_TT01_AM01 = STTA_TT01_ROTATE_TURN_DN_CHECK;
		break;
	case STTA_TT01_ROTATE_TURN_DN_CHECK:
		if (G_MOTION_STATUS[TT01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableTurn] -
				m_st_TT01_Pos->nRotate_SideDn_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TT01_AM01 = STTA_TT01_DRIVE_UT02_POS;
			}
		}
		break;
	case STTA_TT01_DRIVE_UT02_POS:
		strLogMessage.Format("TT01 : 주행 축 대기 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_Ready_Pos);
		m_elSTMove_TT01_AM01 = STTA_TT01_DRIVE_UT02_POS_CHECK;
		break;
	case STTA_TT01_DRIVE_UT02_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TT01_AM01 = STTA_TT01_TILT_READY_POS;
			}
		}
		break;

	case STTA_TT01_TILT_READY_POS:
		strLogMessage.Format("TT01 : TILT 축 대기 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01ThetaMove(m_st_TT01_Pos->nT_Ready_Pos);
		m_elSTMove_TT01_AM01 = STTA_TT01_TILT_READY_POS_CHECK;
		break;
	case STTA_TT01_TILT_READY_POS_CHECK:
		if (G_MOTION_STATUS[TT01_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_ThetaAlign] -
				m_st_TT01_Pos->nT_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TT01_AM01 = STTA_SND_TT01_DRIVE_UT02_POS;
			}
		}
		break;

	case STTA_SND_TT01_DRIVE_UT02_POS:
		strLogMessage.Format("TT01 : 주행 축 대기 위치 도착 전달(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_UT02, nBiz_Unit_Zero);
		m_elSTMove_TT01_AM01 = STTA_SEQUENCE_COMP;
		break;
	case STTA_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("TT01 : 스틱 이송 시나리오 에러 발생(AM01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;
			
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("TT01 : 스틱 이송 시나리오 종료(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_elTurnTableSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TT01_T_AM01] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elSTMove_TT01_AM01 = STTA_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

void CPickerControl::m_fnSet_StickMove_TT01_AM01_Step(EL_STICK_MOVE_TT01_AM01 step)
{
	m_elSTMove_TT01_AM01 = step;
}

int CPickerControl::m_fnStickMove_AM01_UT02()
{
	CString strLogMessage;

	switch (m_elSTMove_AM01_UT02)
	{
	case STAU_MAIN_SEQUENCE_IDLE:
		break;	
	case STAU_RCV_STICK_RESULT:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_AM01_T_UT02] = SEQ_RUNNING;

		strLogMessage.Format("AM01 : 스틱 이송 시나리오 시작 수신(UT02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		if(G_RECIPE_SET)
		{
			m_elSTMove_AM01_UT02 = STAU_SND_STICK_RESULT_ACK;
			strLogMessage.Format("AM01 : 레시피 설정 확인(UT02).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_AM01_UT02 = STAU_SEQUENCE_COMP;
			strLogMessage.Format("TT01 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		//턴테이블 면에대한 처리 추가 할것.
// 
		if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableTurn] -
			m_st_TT01_Pos->nRotate_SideDn_Pos) <= POSITION_OFFSET)
		{
			m_elSTMove_AM01_UT02 = STAU_SND_STICK_RESULT_ACK;
			strLogMessage.Format("TT01 : 턴테이블 하면 위치 확인.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_AM01_UT02 = STAU_SEQUENCE_COMP;
			strLogMessage.Format("TT01 : 턴테이블이 하면_TOP 상태가 아닙니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}

		break;
	case STAU_SND_STICK_RESULT_ACK:		
		strLogMessage.Format("TT01 : 스틱 이송 시나리오 시작 수신 응답.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Stick_Result_ACK, nBiz_Unit_Zero);
		m_elSTMove_AM01_UT02 = STAU_MAIN_SEQUENCE_IDLE;		
		break;
	case STAU_RCV_UNLOAD_READY:
		strLogMessage.Format("AM01 : Stick Unload Ready 수신.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_elSTMove_AM01_UT02 = STAU_TT01_INTERLOCK_CHECK;
		break;
	case STAU_TT01_INTERLOCK_CHECK:
		m_elSTMove_AM01_UT02 = STAU_TT01_DRIVE_AM01_POS;
		break;	
	case STAU_TT01_DRIVE_AM01_POS:
		strLogMessage.Format("TT01 : 주행축 AM01 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_AM01_Pos);
		m_elSTMove_AM01_UT02 = STAU_TT01_DRIVE_AM01_POS_CHECK;
		break;
	case STAU_TT01_DRIVE_AM01_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_AM01_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_AM01_UT02 = STAU_SND_TT01_DRIVE_IN_POSITION;
			}
		}
		break;
	case STAU_SND_TT01_DRIVE_IN_POSITION:
		strLogMessage.Format("TT01 : AM01 정위치 도작 알림.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_AM01, nBiz_Unit_Zero);
		m_elSTMove_AM01_UT02 = STAU_SND_UNLOAD_READY_OK;
		break;
	case STAU_SND_UNLOAD_READY_OK:
		strLogMessage.Format("TT01 : Stick Unload Ready 응답.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_Stick_Unload_Ready_OK, nBiz_Unit_Zero);
		m_elSTMove_AM01_UT02 = STAU_MAIN_SEQUENCE_IDLE;
		break;
	case STAU_RCV_AM01_UNLOAD_STANDBY:
		strLogMessage.Format("AM01 : Stick Unload Standby 수신.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);	

		m_elSTMove_AM01_UT02 = STAU_TT01_VAC_ON;
		break;
	case STAU_TT01_VAC_ON:
		strLogMessage.Format("TT01 : 하면 스틱 흡착 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cTurnTable->m_fnStickLowerVac(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, nBiz_Seq_TT01_DN_Vac_ON);
		m_elSTMove_AM01_UT02 = STAU_TT01_VAC_ON_CHECK;
		break;
	case STAU_TT01_VAC_ON_CHECK:
		if (G_MOTION_STATUS[TT01_DN_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stTurnTableSensor.In07_Sn_TurnTableDownVacuum == ON)
			{
				m_elSTMove_AM01_UT02 = STAU_SND_TT01_VAC_ON;
			}
		}
		break;
	case STAU_SND_TT01_VAC_ON:
		strLogMessage.Format("TT01 : 하면 스틱 흡착 완료 알림(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_VAC_ON, nBiz_Unit_Zero);
		m_elSTMove_AM01_UT02 = STAU_MAIN_SEQUENCE_IDLE;
		break;
	case STAU_RCV_AM01_UNLOAD_COMP:
		strLogMessage.Format("AM01 : Stick Unload Complete 수신.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_elSTMove_AM01_UT02 = STAU_TT01_DRIVE_TURN_POS;
		break;
	case STAU_TT01_DRIVE_TURN_POS:
		strLogMessage.Format("TT01 : 주행축 턴 포지션 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_Turn_Pos);
		m_elSTMove_AM01_UT02 = STAU_TT01_DRIVE_TURN_POS_CHECK;
		break;
	case STAU_TT01_DRIVE_TURN_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_Turn_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_AM01_UT02 = STAU_SND_TT01_DRIVE_TURN_POS;
			}
		}
		break;
	case STAU_SND_TT01_DRIVE_TURN_POS:
		strLogMessage.Format("TT01 : 주행축 턴 포지션 도착 알림(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_Turn, nBiz_Unit_Zero);
		m_elSTMove_AM01_UT02 = STAU_TT01_ROTATE_TURN_UP;
		break;
	case STAU_TT01_ROTATE_TURN_UP:
		strLogMessage.Format("TT01 : 회전 축 상면 탑 위치로 회전.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01RotateMove(m_st_TT01_Pos->nRotate_SideUp_Pos);
		m_elSTMove_AM01_UT02 = STAU_TT01_ROTATE_TURN_UP_CHECK;
		break;
	case STAU_TT01_ROTATE_TURN_UP_CHECK:
		if (G_MOTION_STATUS[TT01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableTurn] -
				m_st_TT01_Pos->nRotate_SideUp_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_AM01_UT02 = STAU_TT01_DRIVE_UT02_POS;
			}
		}
		break;
	case STAU_TT01_DRIVE_UT02_POS:
		strLogMessage.Format("TT01 : 주행 축 대기 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_Ready_Pos);
		m_elSTMove_AM01_UT02 = STAU_TT01_DRIVE_UT02_POS_CHECK;
		break;
	case STAU_TT01_DRIVE_UT02_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_AM01_UT02 = STAU_SND_TT01_DRIVE_UT02_POS;
			}
		}
		break;
	case STAU_SND_TT01_DRIVE_UT02_POS:
		strLogMessage.Format("TT01 : 주행 축 대기 위치 도착 알림(AM01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpServerInterface->m_fnSendCommand_Nrs(
			TASK_10_Master, nBiz_Func_IndexerToMaster, nBiz_Seq_TT01_Pos_UT02, nBiz_Unit_Zero);
		m_elSTMove_AM01_UT02 = STAU_UT02_CYL_UP;
		break;
	case STAU_UT02_CYL_UP:
		strLogMessage.Format("UT02 : 배출 테이블 실린더 업.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cEmissionTable->OnBnClickedBtnCylUp();
		m_elSTMove_AM01_UT02 = STAU_UT02_CYL_UP_CHECK;
		break;
	case STAU_UT02_CYL_UP_CHECK:
		if (G_MOTION_STATUS[UT02_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stEmissionTable.In02_Sn_OutputLTableCylinderUp == ON &&
				G_STMACHINE_STATUS.stEmissionTable.In05_Sn_OutputRTableCylinderUp == ON)
			{
				m_elSTMove_AM01_UT02 = STAU_TT01_VAC_OFF;
			}
		}
		break;
	case STAU_TT01_VAC_OFF:
		strLogMessage.Format("TT01 : 하면 스틱 흡착 해제.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnStickLowerVac(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, nBiz_Seq_TT01_DN_Vac_OFF);
		m_elSTMove_AM01_UT02 = STAU_UT02_VAC_ON;
		break;
	//case STAU_TT01_PURGE:
	//	g_cpMainDlg->m_cTurnTable->m_fnStickLowerPurge(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, m_st_TM02_Cur_Recipe->nTT01_ST_Purge_Put);
	//	m_elSTMove_AM01_UT02 = STAU_TT01_VAC_OFF_CHECK;
	//	break;	
	case STAU_UT02_VAC_ON:
		strLogMessage.Format("UT02 : 스틱 흡착 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cEmissionTable->m_fnStickVacuum(m_st_TM02_Cur_Recipe->stUT02_Vac_Table_Get, nBiz_Seq_UT02_Vac_ON);
		m_elSTMove_AM01_UT02 = STAU_UT02_VAC_ON_CHECK;
		break;

	case STAU_UT02_VAC_ON_CHECK:
		if (G_MOTION_STATUS[UT02_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stEmissionTable.In04_Sn_OutputTableVacuum == ON)
			{
				m_elSTMove_AM01_UT02 = STAU_TT01_VAC_OFF_CHECK;
			}
		}
		break;

	case STAU_TT01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TT01_DN_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stTurnTableSensor.In07_Sn_TurnTableDownVacuum == OFF)
			{
				m_elSTMove_AM01_UT02 = STAU_UT02_CYL_DN;
			}
		}
		break;

	case STAU_UT02_CYL_DN:
		strLogMessage.Format("UT02 : 배출 테이블 실린더 다운.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cEmissionTable->OnBnClickedBtnCylDn();
		m_elSTMove_AM01_UT02 = STAU_UT02_CYL_DN_CHECK;
		break;

	case STAU_UT02_CYL_DN_CHECK:
		if (G_MOTION_STATUS[UT02_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stEmissionTable.In03_Sn_OutputLTableCylinderDown == ON &&
				G_STMACHINE_STATUS.stEmissionTable.In06_Sn_OutputRTableCylinderDown == ON)
			{
				m_elSTMove_AM01_UT02 = STAU_SEQUENCE_COMP;
			}
		}
		break;

	case STAU_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("AM01 : 스틱 이송 시나리오 에러 발생(UT02).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("AM01 : 스틱 이송 시나리오 종료(UT02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_elTurnTableSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_AM01_T_UT02] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elSTMove_AM01_UT02 = STAU_MAIN_SEQUENCE_IDLE;
		if(g_cpMainDlg->m_elMainSequence==MAIN_SEQUENCE_WAIT)
		{
			g_cpMainDlg->m_elMainSequence=STICK_PAPER_SEQUENCE;
		}
		break;

	default:
		break;
	}
	return 0;
}

void CPickerControl::m_fnSet_StickMove_AM01_UT02_Step(EL_STICK_MOVE_AM01_UT02 step)
{
	m_elSTMove_AM01_UT02 = step;
}

int CPickerControl::m_fnStickMove_UT02_TM02()
{
	CString strLogMessage;
	BOOL bResult = FALSE;

	switch (m_elSTMove_UT02_TM02)
	{
	case STUT_MAIN_SEQUENCE_IDLE:
		break;
	case STUT_TT01_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_RUNNING;
		m_bSingleStickRun = FALSE;

		if(G_RECIPE_SET)
		{
			m_elSTMove_UT02_TM02 = STUT_TM02_DRIVE_UT02_POS;
			strLogMessage.Format("UT02 : 스틱 이송 시나리오 시작(TM02).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_UT02_TM02 = STUT_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		break;


	case STUT_TT01_INTERLOCK_CHECK_STEP27:
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_RUNNING;
		m_bSingleStickRun = TRUE;

		m_elSTMove_UT02_TM02 = STUT_TT01_DRIVE_AM01_POS;
		strLogMessage.Format("UT02 : 스틱 이송 시나리오 시작(TM02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
			
		break;
	case STUT_TT01_DRIVE_AM01_POS:
		strLogMessage.Format("TT01 : AM01 스틱 로딩 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_AM01_Pos);
		m_elSTMove_UT02_TM02 = STUT_TT01_DRIVE_AM01_POS_CHECK;

		break;
	case STUT_TT01_DRIVE_AM01_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_AM01_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_UT02_TM02 = STUT_TM02_DRIVE_UT02_POS;
			}
		}
		break;


	case STUT_TM02_DRIVE_UT02_POS:
		strLogMessage.Format("TM02 : 주행축 UT02 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nUT02_Drive_Get);
		m_elSTMove_UT02_TM02 = STUT_TM02_DRIVE_UT02_POS_CHECK;
		break;
	case STUT_TM02_DRIVE_UT02_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nUT02_Drive_Get) <= POSITION_OFFSET)
			{
				m_elSTMove_UT02_TM02 = STUT_TM02_SHIFT_UT02_POS;
			}
		}
		break;
	case STUT_TM02_SHIFT_UT02_POS:
		strLogMessage.Format("TM02 : 쉬프트 축 UT02 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(m_st_TM02_Cur_Recipe->nUT02_Shift_Get);
		m_elSTMove_UT02_TM02 = STUT_TM02_SHIFT_UT02_POS_CHECK;
		break;
	case STUT_TM02_SHIFT_UT02_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
				m_st_TM02_Cur_Recipe->nUT02_Shift_Get) <= POSITION_OFFSET)
			{
				m_elSTMove_UT02_TM02 = STUT_TM02_TILT_UT02_POS;
			}
		}
		break;
	case STUT_TM02_TILT_UT02_POS:
		strLogMessage.Format("TM02 : 회전축 UT02 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(m_st_TM02_Cur_Recipe->nUT02_Tilt_Get);
		m_elSTMove_UT02_TM02 = STUT_TM02_TILT_UT02_POS_CHECK;
		break;
	case STUT_TM02_TILT_UT02_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
				m_st_TM02_Cur_Recipe->nUT02_Tilt_Get) <= POSITION_OFFSET)
			{
				m_elSTMove_UT02_TM02 = STUT_TM02_Z_UT02_READY_POS;
			}
		}
		break;
		//2015.03.26 GJJ S
	case STUT_TM02_Z_UT02_READY_POS:
		if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
			m_st_TT01_Pos->nDrive_AM01_Pos) <= POSITION_OFFSET)  //AM01 에 위치 하는가?
		{
			m_bTurnTablePositionLock = LOCK;

			strLogMessage.Format("TM02 : TT01 주행축 위치 확인. OK");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);

			strLogMessage.Format("TM02 : 업다운축 스틱 흡착 1차 대기 위치 이동.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

			g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nUT02_Z_ST_Get - PICKER_READY_POSITION_OFFSET);
			m_elSTMove_UT02_TM02 = STUT_TM02_Z_UT02_READY_POS_CHECK;
		}
		break;
	case STUT_TM02_Z_UT02_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				(m_st_TM02_Cur_Recipe->nUT02_Z_ST_Get - PICKER_READY_POSITION_OFFSET)) <= POSITION_OFFSET)
			{
				m_elSTMove_UT02_TM02 = STUT_TM02_Z_UT02_POS;
			}
		}

		break;
		//2015.03.26 GJJ E
	case STUT_TM02_Z_UT02_POS:
		if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
			m_st_TT01_Pos->nDrive_AM01_Pos) <= POSITION_OFFSET)  //AM01 에 위치 하는가?
		{			
			strLogMessage.Format("TM02 : TT01 주행축 위치 확인. OK");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);

			strLogMessage.Format("TM02 : 업다운축 스틱 흡착 위치 이동.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

			g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nUT02_Z_ST_Get, Speed_Pickup);
			m_elSTMove_UT02_TM02 = STUT_TM02_Z_UT02_POS_CHECK;
		}
		break;
	case STUT_TM02_Z_UT02_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nUT02_Z_ST_Get) <= POSITION_OFFSET)
			{
				m_elSTMove_UT02_TM02 = STUT_UT02_VAC_OFF;
			}
		}
		break;

	case STUT_UT02_VAC_OFF:
		strLogMessage.Format("UT02 : 스틱 흡착 해제.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cEmissionTable->m_fnStickVacuum(m_st_TM02_Cur_Recipe->stUT02_Vac_Table_Get, nBiz_Seq_UT02_Vac_OFF);
		m_elSTMove_UT02_TM02 = STUT_TM02_VAC_ON;
		break;
// 	case STUT_UT02_PURGE:
// 		g_cpMainDlg->m_cEmissionTable->m_fnStickPurge(m_st_TM02_Cur_Recipe->stUT02_Vac_Table_Get, m_st_TM02_Cur_Recipe->nTT01_ST_Purge_Put);
// 		m_elSTMove_UT02_TM02 = STUT_UT02_VAC_OFF_CHECK;
// 		break;
	case STUT_TM02_VAC_ON:
		strLogMessage.Format("TM02 : 스틱 흡착 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnStickVacuum(m_st_TM02_Cur_Recipe->stUT02_Vac_Get, nBiz_Seq_TM02_ST_Vac_ON);
		m_elSTMove_UT02_TM02 = STUT_TM02_VAC_ON_CHECK;
		break;
	case STUT_TM02_VAC_ON_CHECK:
		if (G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerSensor.In10_Sn_PickerBackStickVacuum == ON)
			{
				m_elSTMove_UT02_TM02 = STUT_UT02_VAC_OFF_CHECK;
			}
		}
		break;	
	case STUT_UT02_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[UT02_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stEmissionTable.In04_Sn_OutputTableVacuum == OFF)
			{
				m_elSTMove_UT02_TM02 = STUT_TM02_Z_READY_POS;
			}
		}
		break;
	case STUT_TM02_Z_READY_POS:
		strLogMessage.Format("TM02 : 업다운 축 대기 위치 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos, Speed_Pickup);
		m_elSTMove_UT02_TM02 = STUT_TM02_Z_READY_POS_CHECK;
		break;
	case STUT_TM02_Z_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_bTurnTablePositionLock = FREE;

				if (m_bSingleStickRun == TRUE)
					m_elSTMove_UT02_TM02 = STUT_TT01_DRIVE_UT02_POS;
				else
					m_elSTMove_UT02_TM02 = STUT_SEQUENCE_COMP;
			}
		}
		break;
		
	case STUT_TT01_DRIVE_UT02_POS:
		strLogMessage.Format("TT01 : 주행 축 대기 위치로 이동.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_Ready_Pos);
		m_elSTMove_UT02_TM02 = STUT_TT01_DRIVE_UT02_POS_CHECK;
		break;
	case STUT_TT01_DRIVE_UT02_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_UT02_TM02 = STUT_SEQUENCE_COMP;
			}
		}
		break;

	case STUT_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("UT02 : 스틱 이송 시나리오 에러 발생(TM02).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;
			
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("UT02 : 스틱 이송 시나리오 종료(TM02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_bTurnTablePositionLock = FREE;
		m_bSingleStickRun = FALSE;

		g_cpMainDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_UT02_T_TM02] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elSTMove_UT02_TM02 = STUT_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

int CPickerControl::m_fnStickMove_TM02_UT01()
{
	CString strLogMessage;
	switch (m_elSTMove_TM02_UT01)
	{
	case STTU_MAIN_SEQUENCE_IDLE:
		break;
	case STTU_TM02_INTERLOCK_CHECK:		
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			m_elSTMove_TM02_UT01 = STTU_TM02_DRIVE_PUT_POS;
			strLogMessage.Format("TM02 : 스틱 이송 시나리오 시작(UT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_TM02_UT01 = STTU_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}

		break;
	case STTU_TM02_DRIVE_PUT_POS:
		strLogMessage.Format("TM02 : 주행축 UT01 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nUT01_Drive_ST_Put);
		m_elSTMove_TM02_UT01 = STTU_TM02_DRIVE_PUT_POS_CHECK;
		break;
	case STTU_TM02_DRIVE_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nUT01_Drive_ST_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_UT01 = STTU_TM02_SHIFT_PUT_POS;
			}
		}
		break;
	case STTU_TM02_SHIFT_PUT_POS:
		strLogMessage.Format("TM02 : 쉬프트 축 UT01 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(m_st_TM02_Cur_Recipe->nUT01_Shift_Put);
		m_elSTMove_TM02_UT01 = STTU_TM02_SHIFT_PUT_POS_CHECK;
		break;
	case STTU_TM02_SHIFT_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
				m_st_TM02_Cur_Recipe->nUT01_Shift_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_UT01 = STTU_TM02_TILT_PUT_POS;
			}
		}
		break;
	case STTU_TM02_TILT_PUT_POS:
		strLogMessage.Format("TM02 : 회전 축 UT01 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(m_st_TM02_Cur_Recipe->nUT01_Tilt_Put);
		m_elSTMove_TM02_UT01 = STTU_TM02_TILT_PUT_POS_CHECK;
		break;
	case STTU_TM02_TILT_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
				m_st_TM02_Cur_Recipe->nUT01_Tilt_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_UT01 = STTU_TM02_Z_PUT_POS;
			}
		}
		break;
	case STTU_TM02_Z_PUT_POS:
		strLogMessage.Format("TM02 : 업다운 축 UT01 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nUT01_Z_ST_Put, Speed_Pickup);
		m_elSTMove_TM02_UT01 = STTU_TM02_Z_PUT_POS_CHECK;
		break;
	case STTU_TM02_Z_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nUT01_Z_ST_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_UT01 = STTU_TM02_ST_VAC_OFF;
			}
		}
		break;
	case STTU_TM02_ST_VAC_OFF:
		strLogMessage.Format("TM02 : 스틱 흡착 해제(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnStickVacuum(m_st_TM02_Cur_Recipe->stUT02_Vac_Get, nBiz_Seq_TM02_ST_Vac_OFF);
		m_elSTMove_TM02_UT01 = STTU_TM02_ST_PURGE_PUT;
		break;
	case STTU_TM02_ST_PURGE_PUT:
		strLogMessage.Format("TM02 : 퍼지 동작 수행(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnStickPurgeOn(m_st_TM02_Cur_Recipe->stUT02_Vac_Get, m_st_TM02_Cur_Recipe->nUT01_Purge_Put);
		m_elSTMove_TM02_UT01 = STTU_TM02_ST_VAC_OFF_CHECK;
		break;
	case STTU_TM02_ST_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerSensor.In10_Sn_PickerBackStickVacuum == OFF)
			{
				m_elSTMove_TM02_UT01 = STTU_TM02_Z_READY_POS;
			}
		}
		break;
	case STTU_TM02_Z_READY_POS:
		strLogMessage.Format("TM02 : 업다운 축 대기 위치로 이동(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos);
		m_elSTMove_TM02_UT01 = STTU_TM02_Z_READY_POS_CHECK;
		break;
	case STTU_TM02_Z_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_UT01 = STTU_SEQUENCE_COMP;
			}
		}
		break;
	case STTU_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("TM02 : 스틱 이송 시나리오 에러 발생(UT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;
			
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("TM02 : 스틱 이송 시나리오 종료(UT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_UT01] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elSTMove_TM02_UT01 = STTU_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

int CPickerControl::m_fnStickMove_TM02_BT01()
{
	CString	strLogMessage;

	switch (m_elSTMove_TM02_BT01)
	{
	case STTB_MAIN_SEQUENCE_IDLE:
		break;
	case STTB_TM02_INTERLOCK_CHECK:		
		m_elSeq_Error = ERR_SEQ_NONE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] = SEQ_RUNNING;

		if(G_RECIPE_SET)
		{
			m_elSTMove_TM02_BT01 = STTB_TM02_DRIVE_PUT_POS;
			strLogMessage.Format("TM02 : 스틱 이송 시나리오 시작(BT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_TM02_BT01 = STTB_SEQUENCE_COMP;
			strLogMessage.Format("TM02 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}
		break;
	case STTB_TM02_DRIVE_PUT_POS:
		strLogMessage.Format("TM02 : 주행축 BT01 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02DriveMove(m_st_TM02_Cur_Recipe->nBT01_Drive_ST_Put);
		m_elSTMove_TM02_BT01 = STTB_TM02_DRIVE_PUT_POS_CHECK;
		break;
	case STTB_TM02_DRIVE_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerDrive] -
				m_st_TM02_Cur_Recipe->nBT01_Drive_ST_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_BT01 = STTB_TM02_SHIFT_PUT_POS;
			}
		}
		break;
	case STTB_TM02_SHIFT_PUT_POS:
		strLogMessage.Format("TM02 : 쉬프트 축 BT01 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cStickTransper->m_fnTM02ShiftMove(m_st_TM02_Cur_Recipe->nBT01_Shift_Put);
		m_elSTMove_TM02_BT01 = STTB_TM02_SHIFT_PUT_POS_CHECK;
		break;
	case STTB_TM02_SHIFT_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_SHIFT].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerShift] -
				m_st_TM02_Cur_Recipe->nBT01_Shift_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_BT01 = STTB_TM02_TILT_PUT_POS;
			}
		}
		break;
	case STTB_TM02_TILT_PUT_POS:
		strLogMessage.Format("TM02 : 회전축 BT01 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02ThetaMove(m_st_TM02_Cur_Recipe->nBT01_Tilt_Put);
		m_elSTMove_TM02_BT01 = STTB_TM02_TILT_PUT_POS_CHECK;
		break;
	case STTB_TM02_TILT_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_THETA].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerRotate] -
				m_st_TM02_Cur_Recipe->nBT01_Tilt_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_BT01 = STTB_TM02_Z_PUT_POS;
			}
		}
		break;
	case STTB_TM02_Z_PUT_POS:
		strLogMessage.Format("TM02 : 업다운축 BT01 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nBT01_Z_ST_Put, Speed_Pickup);
		m_elSTMove_TM02_BT01 = STTB_TM02_Z_PUT_POS_CHECK;
		break;
	case STTB_TM02_Z_PUT_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nBT01_Z_ST_Put) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_BT01 = STTB_TM02_ST_VAC_OFF;
			}
		}
		break;
	case STTB_TM02_ST_VAC_OFF:
		strLogMessage.Format("TM02 : 스티 흡착 해제(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnStickVacuum(m_st_TM02_Cur_Recipe->stUT02_Vac_Get, nBiz_Seq_TM02_ST_Vac_OFF);
		m_elSTMove_TM02_BT01 = STTB_TM02_ST_PURGE_PUT;
		break;
	case STTB_TM02_ST_PURGE_PUT:
		strLogMessage.Format("TM02 : 퍼지 동작 수행(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnStickPurgeOn(m_st_TM02_Cur_Recipe->stUT02_Vac_Get, m_st_TM02_Cur_Recipe->nBT01_Purge_Put);
		m_elSTMove_TM02_BT01 = STTB_TM02_ST_VAC_OFF_CHECK;
		break;
	case STTB_TM02_ST_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TM02_ST_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stPickerSensor.In10_Sn_PickerBackStickVacuum == OFF)
			{
				m_elSTMove_TM02_BT01 = STTB_TM02_Z_READY_POS;
			}
		}
		break;
	case STTB_TM02_Z_READY_POS:
		strLogMessage.Format("TM02 : 업다운 축 대기 위치로 이동(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cStickTransper->m_fnTM02UpDnMove(m_st_TM02_Cur_Recipe->nZ_Ready_Pos);
		m_elSTMove_TM02_BT01 = STTB_TM02_Z_READY_POS_CHECK;
		break;
	case STTB_TM02_Z_READY_POS_CHECK:
		if (G_MOTION_STATUS[TM02_UPDN].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TM02_PickerUpDown] -
				m_st_TM02_Cur_Recipe->nZ_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TM02_BT01 = STTB_SEQUENCE_COMP;
			}
		}
		break;
	case STTB_SEQUENCE_COMP:
		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("TM02 : 스틱 이송 시나리오 에러 발생(BT01).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
			g_cpMainDlg->m_elMainSequence = MAIN_SEQUENCE_IDLE;
			
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("TM02 : 스틱 이송 시나리오 종료(BT01).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_elPickerSequence = SUB_SEQUENCE_IDLE;
		G_EL_SUB_SEQ_STATUS[STICK_MOVE_F_TM02_T_BT01] = SEQ_COMPLETE;
		m_elSeq_Error = ERR_SEQ_NONE;
		m_elSTMove_TM02_BT01 = STTB_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}
	return 0;
}

int CPickerControl::m_fnStickMove_TT01_UT02()
{
	CString strLogMessage;

	switch (m_elSTMove_TT01_UT02)
	{
	case STU_MAIN_SEQUENCE_IDLE:
		break;
	case STU_TT01_INTERLOCK_CHECK:
		m_elSeq_Error = ERR_SEQ_NONE;

		if(G_RECIPE_SET)
		{
			m_elSTMove_TT01_UT02 = STU_TT01_DRIVE_TURN_POS;
			strLogMessage.Format("TT01 : 스틱 자동 반송 시나리오 시작(UT02).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		}
		else
		{
			m_elSTMove_TT01_UT02 = STU_SEQUENCE_COMP;
			strLogMessage.Format("TT01 : 레시피가 설정되지 않았습니다.");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		}


		
		break;
	case STU_TT01_DRIVE_TURN_POS:
		strLogMessage.Format("TT01 : 주행축 턴 포지션 이동(UT02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_Turn_Pos);
		m_elSTMove_TT01_UT02 = STU_TT01_DRIVE_TURN_POS_CHECK;
		break;
	case STU_TT01_DRIVE_TURN_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_Turn_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TT01_UT02 = STU_TT01_ROTATE_TURN_DN;
			}
		}
		break;
	case STU_TT01_ROTATE_TURN_DN:
		strLogMessage.Format("TT01 : 회전축 하면 탑 위치로 회전(UT02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01RotateMove(m_st_TT01_Pos->nRotate_SideDn_Pos);
		m_elSTMove_TT01_UT02 = STU_TT01_ROTATE_TURN_DN_CHECK;
		break;
	case STU_TT01_ROTATE_TURN_DN_CHECK:
		if (G_MOTION_STATUS[TT01_ROTATE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableTurn] -
				m_st_TT01_Pos->nRotate_SideDn_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TT01_UT02 = STU_TT01_DRIVE_UT02_POS;
			}
		}
		break;
	case STU_TT01_DRIVE_UT02_POS:
		strLogMessage.Format("TT01 : 주행축 대기 위치 이동(UT02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnTT01DriveMove(m_st_TT01_Pos->nDrive_Ready_Pos);
		m_elSTMove_TT01_UT02 = STU_TT01_DRIVE_UT02_POS_CHECK;
		break;
	case STU_TT01_DRIVE_UT02_POS_CHECK:
		if (G_MOTION_STATUS[TT01_DRIVE].B_COMMAND_STATUS == COMPLETE)
		{
			if (abs(G_STMACHINE_STATUS.nMotionPos[AXIS_TT01_TableDrive] -
				m_st_TT01_Pos->nDrive_Ready_Pos) <= POSITION_OFFSET)
			{
				m_elSTMove_TT01_UT02 = STU_UT02_CYL_UP;
			}
		}
		break;
	case STU_UT02_CYL_UP:
		strLogMessage.Format("UT02 : 배출 테이블 실린더 업.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cEmissionTable->OnBnClickedBtnCylUp();
		m_elSTMove_TT01_UT02 = STU_UT02_CYL_UP_CHECK;
		break;
	case STU_UT02_CYL_UP_CHECK:
		if (G_MOTION_STATUS[UT02_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stEmissionTable.In02_Sn_OutputLTableCylinderUp == ON &&
				G_STMACHINE_STATUS.stEmissionTable.In05_Sn_OutputRTableCylinderUp == ON)
			{
				m_elSTMove_TT01_UT02 = STU_TT01_VAC_OFF;
			}
		}
		break;
	case STU_TT01_VAC_OFF:
		strLogMessage.Format("TT01 : 상면 스틱 흡착 해제(UT02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cTurnTable->m_fnStickUpperVac(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, nBiz_Seq_TT01_UP_Vac_OFF);
		m_elSTMove_TT01_UT02 = STU_UT02_VAC_ON;
		break;
	case STU_UT02_VAC_ON:
		strLogMessage.Format("UT02 : 스틱 흡착 수행.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		g_cpMainDlg->m_cEmissionTable->m_fnStickVacuum(m_st_TM02_Cur_Recipe->stUT02_Vac_Table_Get, nBiz_Seq_UT02_Vac_ON);
		m_elSTMove_TT01_UT02 = STU_UT02_VAC_ON_CHECK;
		break;
	case STU_UT02_VAC_ON_CHECK:
		if (G_MOTION_STATUS[UT02_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stEmissionTable.In04_Sn_OutputTableVacuum == ON)
			{
				m_elSTMove_TT01_UT02 = STU_TT01_VAC_OFF_CHECK;
			}
		}
		break;	
// 	case STU_TT01_PURGE:
// 		g_cpMainDlg->m_cTurnTable->m_fnStickUpperPurge(m_st_TM02_Cur_Recipe->stTT01_Vac_Table_Get, m_st_TM02_Cur_Recipe->nTT01_ST_Purge_Put);
// 		m_elSTMove_TT01_UT02 = STU_TT01_VAC_OFF_CHECK;
// 		break;
	case STU_TT01_VAC_OFF_CHECK:
		if (G_MOTION_STATUS[TT01_UP_VAC].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stTurnTableSensor.In00_Sn_TurnTableUpperVacuum == OFF)
			{
				m_elSTMove_TT01_UT02 = STU_UT02_CYL_DN;
			}
		}
		break;
	case STU_UT02_CYL_DN:
		strLogMessage.Format("UT02 : 배출 테이블 실린더 다운.");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);
		g_cpMainDlg->m_cEmissionTable->OnBnClickedBtnCylDn();
		m_elSTMove_TT01_UT02 = STU_UT02_CYL_DN_CHECK;
		break;
	case STU_UT02_CYL_DN_CHECK:
		if (G_MOTION_STATUS[UT02_CYL].B_COMMAND_STATUS == COMPLETE)
		{
			if (G_STMACHINE_STATUS.stEmissionTable.In03_Sn_OutputLTableCylinderDown == ON &&
				G_STMACHINE_STATUS.stEmissionTable.In06_Sn_OutputRTableCylinderDown == ON)
			{
				m_elSTMove_TT01_UT02 = STU_SEQUENCE_COMP;
			}
		}
		break;
	case STU_SEQUENCE_COMP:

		if (m_elSeq_Error != ERR_SEQ_NONE)
		{
			strLogMessage.Format("TT01 : 스틱 자동 반송 시나리오 에러 발생(UT02).");
			g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_ERROR_RED);

			g_cpMainDlg->m_elTurnTableSequence = SUB_SEQUENCE_IDLE;
			G_VS11TASK_STATE = TASK_STATE_ERROR;
			in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS11TASK_STATE);
		}
		strLogMessage.Format("TT01 : 스틱 자동 반송 시나리오 종료(UT02).");
		g_cpMainDlg->m_fnAppendList(strLogMessage, TEXT_NORMAL_WHITE);

		m_elSeq_Error = ERR_SEQ_NONE;
		m_elSTMove_TT01_UT02 = STU_MAIN_SEQUENCE_IDLE;
		break;
	default:
		break;
	}

	return 0;
}
