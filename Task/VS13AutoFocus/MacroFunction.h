#ifndef _MACROFUNCTION_H_
#define _MACROFUNCTION_H_

#include "..\\..\\CommonHeader/UI/StaticSt/ColorStaticST.h"

inline TASK_State in_fn_TaskStateChange(CWnd* pParent, CColorStaticST *pStateCtrl, TASK_State newTaskState)
{
	if (pStateCtrl == nullptr)
		return newTaskState;

	switch (newTaskState)
	{
	case TASK_STATE_NONE:
	{
		pStateCtrl->SetWindowText("NONE");
		// Set text blinking colors
		pStateCtrl->SetBlinkTextColors(RGB(255, 255, 255), RGB(0, 0, 0));
		// Set background blinking colors
		pStateCtrl->SetBlinkBkColors(RGB(128, 128, 128), RGB(255, 255, 255));
		//pStateCtrl->SetBkColor(G_Color_Off);
	}break;

	case TASK_STATE_INIT:
	{
		pStateCtrl->SetWindowText("INIT");
		// Set text blinking colors
		pStateCtrl->SetBlinkTextColors(RGB(0, 0, 255), RGB(0, 0, 0));
		// Set background blinking colors
		pStateCtrl->SetBlinkBkColors(RGB(255, 255, 0), RGB(0, 255, 0));
		//pStateCtrl->SetBkColor(G_Color_Off);
	}break;
	case TASK_STATE_IDLE:
	{
		pStateCtrl->SetWindowText("IDLE");
		// Set text blinking colors
		pStateCtrl->SetBlinkTextColors(RGB(0, 0, 255), RGB(0, 0, 0));
		// Set background blinking colors
		pStateCtrl->SetBlinkBkColors(RGB(0, 255, 0), RGB(0, 255, 0));
	}break;
	case TASK_STATE_RUN:
	{
		pStateCtrl->SetWindowText("RUN");
		// Set text blinking colors
		pStateCtrl->SetBlinkTextColors(RGB(255, 0, 0), RGB(0, 0, 255));
		// Set background blinking colors
		pStateCtrl->SetBlinkBkColors(RGB(0, 255, 0), RGB(0, 255, 0));
	}break;
	case TASK_STATE_ERROR:
	{
		pStateCtrl->SetWindowText("ERROR");
		// Set text blinking colors
		pStateCtrl->SetBlinkTextColors(RGB(255, 0, 0), RGB(0, 0, 255));
		// Set background blinking colors
		pStateCtrl->SetBlinkBkColors(RGB(0, 0, 255), RGB(255, 0, 0));
	}break;
	case TASK_STATE_PM:
	{
		pStateCtrl->SetWindowText("PM");
		// Set text blinking colors
		pStateCtrl->SetBlinkTextColors(RGB(0, 0, 255), RGB(0, 0, 0));
		// Set background blinking colors
		pStateCtrl->SetBlinkBkColors(RGB(255, 0, 0), RGB(255, 0, 0));
		//pStateCtrl->SetBkColor(G_Color_Off);
	}break;
	default:
		break;
	}
	// Start background blinking
	pStateCtrl->StartBkBlink(TRUE, CColorStaticST::ST_FLS_FAST);
	// Start text blinking
	pStateCtrl->StartTextBlink(TRUE, CColorStaticST::ST_FLS_FAST);
	// We want be notified when the control blinks
	pStateCtrl->EnableNotify(pParent, WM_USER + 10);
		
	return newTaskState;
}

#endif