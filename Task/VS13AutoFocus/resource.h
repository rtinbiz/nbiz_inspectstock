//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VS13AutoFocus.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_VS13AF_DIALOG               102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_LIST_LOG_VIEW               1030
#define IDC_ST_TASK_STATE               1106
#define IDC_BTN_LOT_START               1220
#define IDC_BTN_LENS1                   1233
#define IDC_BTN_LENS2                   1234
#define IDC_BTN_LENS3                   1235
#define IDC_BTN_LENS4                   1236
#define IDC_BTN_AUTO_FOCUS              1237
#define IDC_BTN_HOME                    1238
#define IDC_BTN_AUTO_FOCUS2             1239
#define IDC_BTN_AF_OFF                  1239
#define IDC_BTN_HOME2                   1240
#define IDC_BTN_READ_NOWPOS             1240

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1239
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
