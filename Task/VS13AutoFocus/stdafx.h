
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원


#include <afxsock.h>            // MFC 소켓 확장



#define SVID_LIST_PATH "E:\\MasterShared\\[04_Master]\\SVID_List.ini"
#define  Section_SVID		"SVID"
//#define  Key_STICK_TENSION_GRIP	"STICK_TENSION_GRIP"
//#define  Key_STICK_TENSION_FINE "STICK_TENSION_FINE"
//#define  Key_STICK_INSPEC_CAM_GARY_MAX "STICK_INSPEC_CAM_GARY_MAX"
//#define  Key_STICK_INSPEC_CAM_GARY_MIN "STICK_INSPEC_CAM_GARY_MIN "
//#define  Key_STICK_INSPEC_CAM_VOLTAGE  "STICK_INSPEC_CAM_VOLTAGE "
//#define  Key_STICK_INSPEC_CAM_TEMPERATURE "STICK_INSPEC_CAM_TEMPERATURE"
//#define  Key_STICK_INSPEC_DARKFIELD_VALUE "STICK_INSPEC_DARKFIELD_VALUE"
//#define  Key_STICK_INSPEC_BACKLIGHT_VALUE "STICK_INSPEC_BACKLIGHT_VALUE"
//#define  Key_STICK_INSPEC_MACRO_LIGHT_VALUE "STICK_INSPEC_MACRO_LIGHT_VALUE"
#define  Key_STICK_REVIEW_IMG_CONTRAST "STICK_REVIEW_IMG_CONTRAST"
#define  Key_STICK_REVIEWAF_TIME "STICK_REVIEWAF_TIME"
//#define  Key_MOT_AXIS_MEASURE_X1_OVERLOAD "MOT_AXIS_MEASURE_X1_OVERLOAD"
//#define  Key_MOT_AXIS_MEASURE_X2_OVERLOAD "OT_AXIS_MEASURE_X2_OVERLOAD"
//#define  Key_MOT_AXIS_MEASURE_Y_OVERLOAD "MOT_AXIS_MEASURE_Y_OVERLOAD"
//#define  Key_MOT_AXIS_INSPECT_X_OVERLOAD "MOT_AXIS_INSPECT_X_OVERLOAD"
//#define  Key_MOT_AXIS_INSPECT_Y_OVERLOAD "MOT_AXIS_INSPECT_Y_OVERLOAD"
//#define  Key_TEMPERATURE_STATE_PC_BOX "TEMPERATURE_STATE_PC_BOX"
//#define  Key_TEMPERATURE_STATE_ELECTRICAL_BOX "TEMPERATURE_STATE_ELECTRICAL_BOX"
//#define  Key_PNEUMATIC_AIR "PNEUMATIC_AIR"
//#define  Key_PNEUMATIC_N2 "PNEUMATIC_N2"
//Image Process Lib Loading
#include "..\..\CommonHeader\ImgProcLink.h"
//VS64 Interface Lib Loading
#include "..\..\CommonHeader\VS_ServerLink.h"


#include "Buttonclass/roundbuttonstyle.h"
#include "Buttonclass/roundbutton2.h"

#include "ListBox/XListBox.h"


#include "GlobalParam.h"
#include "Structure.h"
#include "StatusStructure.h"
#include "MacroFunction.h"

//전역 변수로 설정했다.
typedef struct AFModuleData_Tag
{
	int		 nMiv;
	BOOL     bIn_focus;		
	BOOL     bLaser_enabled;		
	int		 nPosition;  
	int      nFocusOldPos;
	int      nFocusNowPos;

	unsigned short  nNowPwm;

	float nPos;
	int nErrorCode;
	AFModuleData_Tag()
	{
		memset(this, 0x00, sizeof(AFModuleData_Tag));
	}
	void resetValue()
	{
		memset(this, 0x00, sizeof(AFModuleData_Tag));
	}

}AFModuleData;	

extern TASK_State G_VS13TASK_STATE;

#define LENS_CHANGE_TIMEOUT_SEC	100
#define TIMER_AF_2X								11901
#define TIMER_AF_10X								11902
#define TIMER_AF_20X								11903

#define TIMER_AF_CHECK							11904

#define AF_TIMEOUT_SEC							50
#define TIMER_AF_RUN								11905
//#define SIMUL_RUN

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


