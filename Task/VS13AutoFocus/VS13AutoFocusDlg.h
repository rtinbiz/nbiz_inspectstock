
// VS13AutoFocusDlg.h : 헤더 파일
//

#pragma once

#include "afxwin.h"
#include "AFModuleControl.h"



// CVS13AutoFocusDlg 대화 상자
class CVS13AutoFocusDlg : public CDialogEx, public CProcInitial//VS64 Interface
{
// 생성입니다.
public:
	CVS13AutoFocusDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~CVS13AutoFocusDlg();
	
	CInterServerInterface  m_cpServerInterface;


	CAFModuleControl m_AFModule;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VS13AF_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnNcDestroy();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	
	void m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...);
	void Sys_fnInitVS64Interface();
	int				Sys_fnAnalyzeMsg(CMDMSG* cmdMsg);//Message Process Function.	
	LRESULT		Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam);

	void m_fnLogDisplay(CString strLog, LOG_INFO nColorType);
	void m_fnAppendList(CString strLog, LOG_INFO nColorType);
	afx_msg void OnBnClickedOk();
	void m_fnCreateButton(void);
	virtual BOOL PreTranslateMessage(MSG* pMsg);	
	
	
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void m_fnTaskStatusChage(int nTaskNumber, TASK_State elTempStatus);
	afx_msg void OnBnClickedBtnLotStart();
	
	int m_fnSetWindowPosition();
	
	AFModuleData m_AfModuleData;	


public:	

	COLORREF	m_btBackColor;
	HDC			m_HDCView;
	CRect			m_RectiewArea;
	
	CXListBox m_ctrListLogView;
	
	CRoundButton2 m_ctrBtnExit;	
	CRoundButton2 m_ctrBtnLotStart;

	CRoundButtonStyle m_tButtonStyle;	
	CRoundButtonStyle	m_tStyle1;
	CRoundButtonStyle	m_tStyle2;
	CRoundButtonStyle	m_tStyle3;
		
	bool m_bAFOnce;
	//Task Status 갱신 UI Control
	CColorStaticST m_ctrStaticTaskInfo;
	
	CFont			m_TaskStateFont;
	CFont			m_TaskStateSmallFont;
	COLORREF	m_TaskStateColor;
	
	int m_nZoomTimerCount; 
	afx_msg void OnBnClickedBtnLens1();
	afx_msg void OnBnClickedBtnLens2();
	afx_msg void OnBnClickedBtnLens3();
	afx_msg void OnBnClickedBtnLens4();

	int m_nAFTimerCount; 
	afx_msg void OnBnClickedBtnAutoFocus();
	afx_msg void OnBnClickedBtnHome();
	afx_msg void OnBnClickedBtnAfOff();
	CRoundButton2 m_btAF_On;
	CRoundButton2 m_btAF_Off;
	CRoundButton2 m_btAF_Home;
	CRoundButton2 m_btLens_Index1;
	CRoundButton2 m_btLens_Index2;
	CRoundButton2 m_btLens_Index3;
	CRoundButton2 m_btLens_Index4;
	CRoundButton2 m_btReadNowPos;
	afx_msg void OnBnClickedBtnReadNowpos();

	DWORD m_AFTime;
};

extern CVS13AutoFocusDlg*				g_cpMainDlg;
extern CInterServerInterface*		g_cpServerInterface;