
// VS13AutoFocusDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VS13AutoFocus.h"
#include "VS13AutoFocusDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
//
////Observation Application Link State.
//typedef enum
//{
//	ObserveSW_Init =-1,				//<<< 초기 상태
//	ObserveSW_None =0,				//<<< Observation APP이 구동되지 않았다.
//	ObserveSW_OK ,					//<<< Observation APP이 정상 구동중이다.
//	ObserveSW_CtrlPowerOff,			//<<< Observation APP는 구동중이나 Controller가 Off 상태 이다.
//	ObserveSW_NoneRemote,			//<<< Observation APP와 Controller는 구동중 이지만 Remote Mode가 아니다.
//	////////
//	ObserveSW_ManualMode			//<<< Main UI에서 Manual Mode로 변환 한것이다.
//}ObsAppState;
//
////Observation App 동작상태 (ObserveSW_OK APP상태에서만 Check.)
//typedef enum
//{
//	ActionObs_Init =-2,						//<<< 초기 상태
//	ActionObs_Manual =-1,					//<<< ObserveSW_ManualMode일때는 상태 Check를 하지 않는다.
//	ActionObs_None  =0,						//<<< 동작 명령 수행중이 아닐때.
//
//	//ActionObs_AutoGain,						//<<< Auto Gain 동작 수행시.
//	ActionObs_AutoFocus,					//<<< Auto Focus 동작 수행시.
//	//ActionObs_AutoUpDownPosSetting,			//<<< 측정 Upper/Lower 위치 선정 수행시.
//	ActionObs_AutoUpDownPosSetByImg,		//<<< 측정 Upper/Lower 위치 선정 수행시.
//	ActionObs_StartMeasurement,				//<<< 3D Scan 수행시.
//	ActionObs_AutoSaveData,					//<<< 측정 Data 저장 수행시.
//
//	//ActionObs_AutoGain_Error,				//<<< Auto Gain 동작 수행시 Error.
//	ActionObs_AutoFocus_Error,				//<<< Auto Focus 동작 수행시 Error.
//	//ActionObs_AutoUpDownPosSet_Error,		//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
//	ActionObs_AutoUpDownPosSetByImg_Error,	//<<< 측정 Upper/Lower 위치 선정 수행시 Error.
//	ActionObs_StartMeasurement_Error,		//<<< 3D Scan 수행시 Error.
//	ActionObs_AutoSaveData_Error,			//<<< 측정 Data 저장 수행시 Error.
//
//	ActionObs_RemoteMode_Error//<<< 측정 가능 Remote Mode가 아니다.
//}ObsActionState;



CVS13AutoFocusDlg*			g_cpMainDlg;
CInterServerInterface*		g_cpServerInterface;

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();
// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CVS13AutoFocusDlg 대화 상자
CVS13AutoFocusDlg::CVS13AutoFocusDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVS13AutoFocusDlg::IDD, pParent)	
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);	
	m_nZoomTimerCount = 0;
	m_nAFTimerCount =0;
	m_bAFOnce = false;

	m_AFTime= 0;
}
CVS13AutoFocusDlg::~CVS13AutoFocusDlg()
{
	G_VS13TASK_STATE = TASK_STATE_NONE;	
}
void CVS13AutoFocusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG_VIEW, m_ctrListLogView);
	DDX_Control(pDX, IDOK, m_ctrBtnExit);	
	DDX_Control(pDX, IDC_ST_TASK_STATE, m_ctrStaticTaskInfo);
	DDX_Control(pDX, IDC_BTN_LOT_START, m_ctrBtnLotStart);	
	DDX_Control(pDX, IDC_BTN_AUTO_FOCUS, m_btAF_On);
	DDX_Control(pDX, IDC_BTN_AF_OFF, m_btAF_Off);
	DDX_Control(pDX, IDC_BTN_HOME, m_btAF_Home);
	DDX_Control(pDX, IDC_BTN_LENS1, m_btLens_Index1);
	DDX_Control(pDX, IDC_BTN_LENS2, m_btLens_Index2);
	DDX_Control(pDX, IDC_BTN_LENS3, m_btLens_Index3);
	DDX_Control(pDX, IDC_BTN_LENS4, m_btLens_Index4);
	DDX_Control(pDX, IDC_BTN_READ_NOWPOS, m_btReadNowPos);
}

BEGIN_MESSAGE_MAP(CVS13AutoFocusDlg, CDialogEx)
	ON_MESSAGE(WM_VS64USER_CALLBACK, &CVS13AutoFocusDlg::Sys_fnMessageCallback)//VS64 Interface	
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CTLCOLOR()
	ON_WM_NCDESTROY()
	ON_BN_CLICKED(IDOK, &CVS13AutoFocusDlg::OnBnClickedOk)	
	ON_WM_TIMER()	
	ON_BN_CLICKED(IDC_BTN_LOT_START, &CVS13AutoFocusDlg::OnBnClickedBtnLotStart)
	ON_BN_CLICKED(IDC_BTN_LENS1, &CVS13AutoFocusDlg::OnBnClickedBtnLens1)
	ON_BN_CLICKED(IDC_BTN_LENS2, &CVS13AutoFocusDlg::OnBnClickedBtnLens2)
	ON_BN_CLICKED(IDC_BTN_LENS3, &CVS13AutoFocusDlg::OnBnClickedBtnLens3)
	ON_BN_CLICKED(IDC_BTN_LENS4, &CVS13AutoFocusDlg::OnBnClickedBtnLens4)
	ON_BN_CLICKED(IDC_BTN_AUTO_FOCUS, &CVS13AutoFocusDlg::OnBnClickedBtnAutoFocus)
	ON_BN_CLICKED(IDC_BTN_HOME, &CVS13AutoFocusDlg::OnBnClickedBtnHome)
	ON_BN_CLICKED(IDC_BTN_AF_OFF, &CVS13AutoFocusDlg::OnBnClickedBtnAfOff)
	ON_BN_CLICKED(IDC_BTN_READ_NOWPOS, &CVS13AutoFocusDlg::OnBnClickedBtnReadNowpos)
END_MESSAGE_MAP()


// CVS13AutoFocusDlg 메시지 처리기

BOOL CVS13AutoFocusDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.
	
	//
	m_btBackColor = RGB(65,65,65);
	

	//////////////////////////////////////////////////////////////////////////
	//1) Server Interface Connection.
	
	Sys_fnInitVS64Interface();

	CString NewWindowsName;
	NewWindowsName.Format("VS64 - Indexer Task(No.%02d)", m_ProcInitVal.m_nTaskNum);
	SetWindowText(NewWindowsName);

	m_fnSetWindowPosition();	

	m_fnCreateButton();

	// TODO: 여기에 추가 초기화 작업을 추가합니다.	

	
	BOOL bRes = TRUE;
	CString strComport; // 홈 시퀀스에서 이니셜 한다.// 

	strComport.Format("COM%d", 3);
	if (m_AFModule.m_fnAFModuleInit(strComport.GetBuffer()) == FALSE)
	{		
		bRes = FALSE;
	}
	strComport.ReleaseBuffer();

	if(bRes == TRUE)
	{
		int nError = 0;
		Sleep(100);
		m_AFModule.m_fnLlcOnOff(TRUE);
		m_AFModule.m_fnHomeLlc(TRUE, &nError);
		m_AFModule.m_fnSetLLCSpeed(300, 2500, 2500);

		if(nError==0)
			OnBnClickedBtnLens1();


		SetTimer(TIMER_AF_CHECK, 800, NULL);
	}

	g_cpMainDlg = this;

	G_VS13TASK_STATE = TASK_STATE_IDLE;
	in_fn_TaskStateChange(g_cpMainDlg->GetParent(), &g_cpMainDlg->m_ctrStaticTaskInfo, G_VS13TASK_STATE);
	//SetTimer(STATUS_DISP_TIMER, 3000, 0);
	
// 	m_ctrStaticTaskInfo.SetFont(&m_TaskStateFont);
// 	m_ctrStaticTaskInfo.SetTextColor(RGB(0, 0, 0));
// 	m_ctrStaticTaskInfo.SetBkColor(m_TaskStateColor);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}





int CVS13AutoFocusDlg::m_fnSetWindowPosition()
{
	//this->MoveWindow(0, -1, 1715, 1080, TRUE);	

	return 0;
}



void CVS13AutoFocusDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVS13AutoFocusDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}



// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVS13AutoFocusDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CVS13AutoFocusDlg::m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...)
{
	char	cLogBuffer[MAX_LOG_BUFF];
	va_list vargs;

	va_start(vargs, pErrLog);

	vsprintf_s(cLogBuffer, pErrLog, (va_list)vargs);

	va_end(vargs);

	m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nTaskNum ,uLogLevel, cLogBuffer);

	//strcpy_s(&cLogBuffer[strlen(cLogBuffer)], sizeof("\r\n"),"\r\n");

	//printf(cLogBuffer);
}

void CVS13AutoFocusDlg::Sys_fnInitVS64Interface()
{
	if(1 < __argc)
	{
		int nInputTaskNum = atoi(__argv[1]);
		char *pLinkPath = NULL;

		char path_buffer[_MAX_PATH];
		char chFilePath[_MAX_PATH] = {0,};
		char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		//실행 파일 이름을 포함한 Full path 가 얻어진다.
		::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
		//디렉토리만 구해낸다.
		_splitpath_s(path_buffer, drive, dir, fname, ext);
		strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
		strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

		switch(nInputTaskNum)
		{

		case TASK_10_Master		: pLinkPath = MSG_TASK_INI_FullPath_TN10; break;
		case TASK_11_Indexer		: pLinkPath = MSG_TASK_INI_FullPath_TN11; break;
		case TASK_13_AutoFocus	: pLinkPath = MSG_TASK_INI_FullPath_TN13; break;
		case TASK_21_Inspector	: pLinkPath = MSG_TASK_INI_FullPath_TN21; break;
		case TASK_24_Motion		: pLinkPath = MSG_TASK_INI_FullPath_TN24; break;
		case TASK_31_Mesure3D	: pLinkPath = MSG_TASK_INI_FullPath_TN31; break;
		case TASK_32_Mesure2D	: pLinkPath = MSG_TASK_INI_FullPath_TN32; break;
		case TASK_33_SerialPort	: pLinkPath = MSG_TASK_INI_FullPath_TN33; break;
		case TASK_60_Log			: pLinkPath = MSG_TASK_INI_FullPath_TN60; break;
		case TASK_90_TaskLoader: pLinkPath = MSG_TASK_INI_FullPath_TN90; break;

		case TASK_99_Sample: pLinkPath = MSG_TASK_INI_FullPath_TN99; break;
		default:
			{
				CString ErrorMessage;
				ErrorMessage.Format("Unkown TaskNum(%d)", nInputTaskNum);
				AfxMessageBox(ErrorMessage);
				SendMessage(WM_CLOSE, 0,  0);	
			}return;
		}

		strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

		m_fnReadDatFile(chFilePath);
		m_ProcInitVal.m_nTaskNum = nInputTaskNum;
		//m_ProcInitVal.m_nLogFileID = nInputTaskNum;

	}
	else
	{
		AfxMessageBox("Input Run Task Number");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	m_cpServerInterface.m_fnSetLogTaskInfo(m_ProcInitVal.m_nLogTaskNo, m_ProcInitVal.m_nLogBlockLovel);	
	if(m_cpServerInterface.m_fnRegisterTask(m_ProcInitVal.m_nTaskNum, m_ProcInitVal.m_sServerIP, m_ProcInitVal.m_nPortNo) != 0)
	{
		AfxMessageBox("Client Regist Fail");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	else
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, "Client Regist OK : %d", m_ProcInitVal.m_nTaskNum);
	}

	m_cpServerInterface.m_fnSetAppWindowHandle(m_hWnd);
	m_cpServerInterface.m_fnSetAppUserMessage(WM_VS64USER_CALLBACK);
}


/*
*	Module Name	:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function			:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS13AutoFocusDlg::Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	CMDMSG* CmdMsg = (CMDMSG*)lParam;		//SmartPointer 버전으로 개조가 필요하다... Work Item..

	try
	{
		if(CmdMsg != NULL)
		{
			nRet = Sys_fnAnalyzeMsg(CmdMsg);

			if(OKAY != nRet)
			{
				//Client 메시지 처리에대한 에러처리...
			}

			m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
			m_cpServerInterface.m_fnFreeMemory(CmdMsg);
			return OKAY;
		}
		else
		{
			throw CVs64Exception(_T("CVS19MWDlg::m_fnVS64MsgProc, Message did not become well."));
		}
	}
	catch (CVs64Exception& e)
	{
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, e.m_fnGetErrorMsg());

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, exception -> %s"), e.what());
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, CException -> %s"), wszErr);
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_cpServerInterface.m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_cpServerInterface.m_fnFreeMemory(CmdMsg);
	}
	return OKAY;
}



HBRUSH CVS13AutoFocusDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();	

	switch (DLG_ID_Number)
	{
	case IDC_ST_TASK_STATE:	
		return hbr;		
	default:
		break;
	}

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
		break;
	case CTLCOLOR_STATIC:
		{
		}break;
	case CTLCOLOR_BTN:
		break;
	case CTLCOLOR_EDIT :
		{
			pDC->SetTextColor(RGB(255, 255, 0)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	case CTLCOLOR_LISTBOX://List Color
		{
			pDC->SetTextColor(RGB(255, 100, 100)); 
			pDC->SetBkColor(RGB(0, 0, 0));
			return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
		}break;
	}

	pDC->SetTextColor(RGB(255, 255, 255)); 
	pDC->SetBkColor(m_btBackColor);
	return (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CVS13AutoFocusDlg::m_fnCreateButton(void)
{
	tButtonStyle tStyle;	

	// Get default Style Dark
	m_tStyle1.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 10.0;
	tStyle.m_dHighLightY = 8.0;
	tStyle.m_dRadius = 6.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x20, 0x20, 0x20);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyle1.SetButtonStyle(&tStyle);
	
	m_tStyle2.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 6.0;
	tStyle.m_dHighLightY = 4.0;
	tStyle.m_dRadius = 4.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x60, 0x60, 0x60);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorFace.m_tClicked		= RGB(0xBE, 0xBE, 0x40);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0xAA, 0xAA, 0x20);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x40, 0x40, 0x40);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x20, 0x20, 0x20);
	m_tStyle2.SetButtonStyle(&tStyle);

	m_tStyle3.GetButtonStyle(&tStyle);
	tStyle.m_dHighLightX = 6.0;
	tStyle.m_dHighLightY = 4.0;
	tStyle.m_dRadius = 4.0;
	//tStyle.m_dRadiusHighLight = 10.0;	
	tStyle.m_tColorBack.m_tDisabled = m_btBackColor;
	tStyle.m_tColorBack.m_tEnabled	= m_btBackColor;
	tStyle.m_tColorBack.m_tClicked = m_btBackColor;
	tStyle.m_tColorBack.m_tPressed = m_btBackColor;
	tStyle.m_tColorBack.m_tHot = m_btBackColor;

	tStyle.m_tColorFace.m_tEnabled		= RGB(0x36, 0x55, 0x9E);
	tStyle.m_tColorBorder.m_tEnabled	= RGB(0x16, 0x35, 0x7E);
	tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
	tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);
	tStyle.m_tColorFace.m_tPressed		= RGB(0x80, 0x80, 0x80);
	tStyle.m_tColorBorder.m_tPressed	= RGB(0x40, 0x40, 0xFF);
	m_tStyle3.SetButtonStyle(&tStyle);

	
	LOGFONT tFont;
	m_ctrBtnExit.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"Arial", strlen("Arial"));
	tFont.lfHeight = 18;

	tColorScheme tColor;
	m_ctrBtnExit.GetTextColor(&tColor);
	// Change Color
	tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
	tColor.m_tClicked		= RGB(0x00, 0xAF, 0x00);
	tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);
	// Set Text-Color

	m_ctrBtnExit.SetTextColor(&tColor);
	m_ctrBtnExit.SetCheckButton(false);	
	m_ctrBtnExit.SetFont(&tFont);
	m_ctrBtnExit.SetRoundButtonStyle(&m_tStyle1);		
	
	m_ctrBtnLotStart.SetTextColor(&tColor);
	m_ctrBtnLotStart.SetCheckButton(false);	
	m_ctrBtnLotStart.SetFont(&tFont);
	m_ctrBtnLotStart.SetRoundButtonStyle(&m_tStyle1);
	

	m_btAF_On.SetTextColor(&tColor);
	m_btAF_On.SetCheckButton(true, true);	
	m_btAF_On.SetFont(&tFont);
	m_btAF_On.SetRoundButtonStyle(&m_tStyle2);

	m_btAF_Off.SetTextColor(&tColor);
	m_btAF_Off.SetCheckButton(true, true);	
	m_btAF_Off.SetFont(&tFont);
	m_btAF_Off.SetRoundButtonStyle(&m_tStyle2);

	m_btAF_Home.SetTextColor(&tColor);
	m_btAF_Home.SetCheckButton(true, true);	
	m_btAF_Home.SetFont(&tFont);
	m_btAF_Home.SetRoundButtonStyle(&m_tStyle2);

	m_btLens_Index1.SetTextColor(&tColor);
	m_btLens_Index1.SetCheckButton(true, true);	
	m_btLens_Index1.SetFont(&tFont);
	m_btLens_Index1.SetRoundButtonStyle(&m_tStyle3);

	m_btLens_Index2.SetTextColor(&tColor);
	m_btLens_Index2.SetCheckButton(true, true);	
	m_btLens_Index2.SetFont(&tFont);
	m_btLens_Index2.SetRoundButtonStyle(&m_tStyle3);

	m_btLens_Index3.SetTextColor(&tColor);
	m_btLens_Index3.SetCheckButton(true, true);	
	m_btLens_Index3.SetFont(&tFont);
	m_btLens_Index3.SetRoundButtonStyle(&m_tStyle3);


	m_btLens_Index4.SetTextColor(&tColor);
	m_btLens_Index4.SetCheckButton(true, true);	
	m_btLens_Index4.SetFont(&tFont);
	m_btLens_Index4.SetRoundButtonStyle(&m_tStyle3);

	m_btReadNowPos.SetTextColor(&tColor);
	m_btReadNowPos.SetCheckButton(true, true);	
	m_btReadNowPos.SetFont(&tFont);
	m_btReadNowPos.SetRoundButtonStyle(&m_tStyle3);

	

	m_ctrBtnExit.GetFont(&tFont);	
	strncpy_s(tFont.lfFaceName,"굴림", strlen("굴림"));
	tFont.lfHeight = 14;



	m_TaskStateFont.CreateFont(70, 0, 0, 0, FW_BOLD,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Arial Black");

	m_TaskStateSmallFont.CreateFont(30, 0, 0, 0, FW_BOLD,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Arial Black");
	m_TaskStateColor = RGB(128, 128, 128);

	//m_ctrStaticTaskInfo.SetFont(&m_TaskStateFont);
	m_ctrStaticTaskInfo.SetTextColor(RGB(0, 0, 0));
	m_ctrStaticTaskInfo.SetBkColor(m_TaskStateColor);


	
}

void CVS13AutoFocusDlg::OnNcDestroy()
{
	__super::OnNcDestroy();	
	
	
	
	m_TaskStateFont.DeleteObject();
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CVS13AutoFocusDlg::m_fnAppendList(CString strLog, LOG_INFO nColorType)
{	
	SYSTEMTIME		systime;

	CString		strLogString;

	GetLocalTime(&systime);

	//로그 타스트로 로그 전송하고,,
	m_cpServerInterface.m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, strLog);

	strLogString.Format("[%02dh %02dm %02ds]",systime.wHour, systime.wMinute, systime.wSecond);	
	strLogString = strLogString + strLog;	
	//로그 출력.
	m_fnLogDisplay(strLogString, nColorType);

}

void CVS13AutoFocusDlg::m_fnLogDisplay(CString strLog, LOG_INFO nColorType)
{
	if(m_ctrListLogView.GetCount() > LIST_ALARM_COUNT)
	{
		m_ctrListLogView.DeleteString(0);
	}
	switch(nColorType)
	{
	case TEXT_NORMAL_BLACK:
		m_ctrListLogView.AddLine((CXListBox::Color)1, 	(CXListBox::Color)0, 	strLog);
		break;
	case TEXT_COLOR_GREEN:
		m_ctrListLogView.AddLine((CXListBox::Color)1, 	(CXListBox::Color)3, 	strLog);
		break;
	case TEXT_WARRING_YELLOW:
		m_ctrListLogView.AddLine((CXListBox::Color)0, 	(CXListBox::Color)12, 	strLog);
		break;
	case TEXT_ERROR_RED:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)10, strLog);
		break;
	case TEXT_NORMAL_WHITE:
		m_ctrListLogView.AddLine((CXListBox::Color)0, (CXListBox::Color)1, strLog);
		break;
	case TEXT_COLOR_BLUE:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)13, strLog);
		break;
	default:
		m_ctrListLogView.AddLine((CXListBox::Color)1, (CXListBox::Color)0, strLog);
		break;
	}

	m_ctrListLogView.SetScrollPos(SB_VERT , m_ctrListLogView.GetCount(), TRUE);
	m_ctrListLogView.SetTopIndex(m_ctrListLogView.GetCount() - 1);	
}

void CVS13AutoFocusDlg::OnBnClickedOk()
{	
	m_AFModule.m_fnAFClose();

	OnOK();
}

BOOL CVS13AutoFocusDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return __super::PreTranslateMessage(pMsg);
}



/*
*	Module Name	:	AOI_fnAnalyzeMsg
*	Parameter		:	Massage Parameter Pointer
*	Return			:	없음
*	Function			:	Sever로 부터 저달 되는 Command를 내부 Function과 Link 한다.(처리)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
int CVS13AutoFocusDlg::Sys_fnAnalyzeMsg(CMDMSG* RcvCmdMsg)
{
	VS24MotData stReturn_Mess;	

	try
	{
		if(RcvCmdMsg->uTask_Dest == m_ProcInitVal.m_nTaskNum) 
		{	
			switch(RcvCmdMsg->uFunID_Dest)
			{
				//////////////////////////////////////////////////////////////////////////
				//Project Default Command.
			case nBiz_Func_System:
			{
				switch (RcvCmdMsg->uSeqID_Dest)
				{
				case nBiz_Seq_Alive_Question:
				{
#ifdef SIMUL_RUN
					m_AfModuleData.bIn_focus =rand()%2==0?1:0;
					m_AfModuleData.nMiv =rand()%2==0?1:0;
#endif
					//m_cpServerInterface.m_fnSendCommand_Nrs(RcvCmdMsg->uTask_Src, nBiz_Func_System, nBiz_Seq_Alive_Response, G_VS13TASK_STATE);
					m_cpServerInterface.m_fnSendCommand_Nrs(RcvCmdMsg->uTask_Src, nBiz_Func_System, nBiz_Seq_Alive_Response, (USHORT)G_VS13TASK_STATE,
						(USHORT)sizeof(AFModuleData), (UCHAR*)&m_AfModuleData);

					break;
				}
				case nBiz_Seq_Alive_Response:
				{
					TASK_State elTempState;
					elTempState = (TASK_State)RcvCmdMsg->uUnitID_Dest;

					switch (RcvCmdMsg->uTask_Src)
					{
					case TASK_10_Master:							
						//상태 갱신
						break;					
					case TASK_24_Motion:		
						
						//상태 갱신
						break;					
					default:					
						throw CVs64Exception(_T("CVS13AutoFocusDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;					
					}//End Switch	
					break;
				}
				default:
					throw CVs64Exception(_T("CVS13AutoFocusDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
				break;
			}
			case nBiz_Func_ReviewLens: //모션에 대한 리턴 케이스
			{					
				switch(RcvCmdMsg->uSeqID_Dest)
				{				
				case nBiz_Seq_Lens1: //	
					OnBnClickedBtnLens1();
					break;		
				case nBiz_Seq_Lens2: //		
					OnBnClickedBtnLens2();
					break;
				case nBiz_Seq_Lens3: //		
					OnBnClickedBtnLens3();
					break;
				case nBiz_Seq_Lens4: //	
					OnBnClickedBtnLens4();
					break;
				default:						
					throw CVs64Exception(_T("CVS13AutoFocusDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
					break;
				}//End Switch
			}break;	
			case nBiz_Func_AF: //모션에 대한 리턴 케이스
				{					
					switch(RcvCmdMsg->uSeqID_Dest)
					{		
					case nBiz_Seq_Init:
						{
							//int nError = 0;
							//m_AFModule.m_fnLlcOnOff(TRUE);					
							////m_AFModule.m_fnHomeLlc(FALSE, &nError);	
							//m_AFModule.m_fnHomeLlc(TRUE, &nError);//Home완료 까지 기다리자.	
							//m_AFModule.m_fnSetLLCSpeed(300, 2500, 2500);
							//m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_Init, nError);
						}break;
					case nBiz_Seq_AF:
						{
							if(RcvCmdMsg->uUnitID_Dest == 1)//AFOn
								OnBnClickedBtnAutoFocus();
							else if(RcvCmdMsg->uUnitID_Dest == 3)//AFOn Once
							{
								

								if(m_bAFOnce == false)
								{
									m_AFTime = ::GetTickCount();
									m_bAFOnce = true;
									OnBnClickedBtnAutoFocus();
								}
								else
									m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AF, 0);
							}
							else
								OnBnClickedBtnAfOff();//AFOff

						}break;
					default:						
						throw CVs64Exception(_T("CVS13AutoFocusDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;					
			case nBiz_Func_Light: //모션에 대한 리턴 케이스
				{					
					switch(RcvCmdMsg->uSeqID_Dest)
					{				
					case nBiz_Seq_SetReviewLight:
						{
							u_short SetLightValue = (int)RcvCmdMsg->uUnitID_Dest;
							if(SetLightValue>100)
								SetLightValue = 100;
							m_AFModule.m_fnWriteLedPwm(0, SetLightValue);
							Sleep(100);
							m_AFModule.m_fnReadLedPwm(0, &SetLightValue);
							m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_Light, nBiz_Seq_SetReviewLight, SetLightValue);

							CString strNewValue;
							strNewValue.Format("%d", SetLightValue);
							WritePrivateProfileString(Section_SVID, Key_STICK_REVIEW_IMG_CONTRAST, strNewValue, SVID_LIST_PATH);
						}break;
					default:						
						throw CVs64Exception(_T("CVS13AutoFocusDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;		
			default:
				{
					throw CVs64Exception(_T("CVS13AutoFocusDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
				}break;
			}//End Switch
		}
		else
		{
			throw CVs64Exception(_T("CVS13AutoFocusDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
		}
	}
	catch (...)
	{
		throw; 
	}
	return OKAY;
}




void CVS13AutoFocusDlg::m_fnTaskStatusChage(int nTaskNumber, TASK_State elTempStatus)
{
	CString strLogMessage;
	switch (elTempStatus)
	{
	case TASK_STATE_NONE:
	{	
		strLogMessage.Format("TASK No.%2d : 상태 변경 to NONE.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_NORMAL_BLACK);
		break;
	}
	case TASK_STATE_INIT:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to INIT.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);
		break;
		
	}
	case TASK_STATE_IDLE:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to IDLE.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_COLOR_GREEN);
		break;
	}
	case TASK_STATE_RUN:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to RUN.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_COLOR_GREEN);
		break;
	}
	case TASK_STATE_ERROR:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to ERROR.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		break;
	}
	case TASK_STATE_PM:
	{
		strLogMessage.Format("TASK No.%2d : 상태 변경 to PM.", nTaskNumber);
		m_fnAppendList(strLogMessage, TEXT_ERROR_RED);
		break;
	}
	default:
		break;
	}

}


void CVS13AutoFocusDlg::OnBnClickedBtnLotStart()
{

}

void CVS13AutoFocusDlg::OnBnClickedBtnLens1()
{
	//OnBnClickedBtnAfOff();
	m_AFModule.m_fnAFZStop();	
	m_AFModule.m_fnLaserOff();
	m_btAF_On.SetCheck(false);
	m_btAF_Off.SetCheck(true);

	int nReturn = 0;
	m_AFModule.m_fnSetMoveLlc(0, FALSE, &nReturn);
	m_nZoomTimerCount = 0;
	KillTimer(TIMER_AF_2X);
	KillTimer(TIMER_AF_10X);
	KillTimer(TIMER_AF_20X);
	SetTimer(TIMER_AF_2X, 100, NULL);	

	m_btLens_Index1.SetCheck(false);
	m_btLens_Index2.SetCheck(false);
	m_btLens_Index3.SetCheck(false);
	m_btLens_Index4.SetCheck(false);

}


void CVS13AutoFocusDlg::OnBnClickedBtnLens2()
{
	//OnBnClickedBtnAfOff();
	m_AFModule.m_fnAFZStop();	
	m_AFModule.m_fnLaserOff();
	m_btAF_On.SetCheck(false);
	m_btAF_Off.SetCheck(true);

	int nReturn = 0;
	m_AFModule.m_fnSetMoveLlc(1, FALSE, &nReturn);
	m_nZoomTimerCount = 0;
	KillTimer(TIMER_AF_2X);
	KillTimer(TIMER_AF_10X);
	KillTimer(TIMER_AF_20X);
	SetTimer(TIMER_AF_10X, 100, NULL);	
	m_btLens_Index1.SetCheck(false);
	m_btLens_Index2.SetCheck(false);
	m_btLens_Index3.SetCheck(false);
	m_btLens_Index4.SetCheck(false);
}


void CVS13AutoFocusDlg::OnBnClickedBtnLens3()
{
	//OnBnClickedBtnAfOff();
	m_AFModule.m_fnAFZStop();	
	m_AFModule.m_fnLaserOff();
	m_btAF_On.SetCheck(false);
	m_btAF_Off.SetCheck(true);

	int nReturn = 0;
	m_AFModule.m_fnSetMoveLlc(2, FALSE, &nReturn);
	m_nZoomTimerCount = 0;
	KillTimer(TIMER_AF_2X);
	KillTimer(TIMER_AF_10X);
	KillTimer(TIMER_AF_20X);
	SetTimer(TIMER_AF_20X, 100, NULL);	
	m_btLens_Index1.SetCheck(false);
	m_btLens_Index2.SetCheck(false);
	m_btLens_Index3.SetCheck(false);
	m_btLens_Index4.SetCheck(false);
}
void CVS13AutoFocusDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	int nRecipeIndex = 0;
	if(nIDEvent == TIMER_AF_2X)
	{
		m_nZoomTimerCount++;

		if(m_nZoomTimerCount > LENS_CHANGE_TIMEOUT_SEC) // TimeOut
		{
			KillTimer(TIMER_AF_2X);
			m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens1, 0);//Time Out Error Return .(Unit Zero)
		}
		else if(m_nZoomTimerCount > 10)
		{	
#ifdef SIMUL_RUN
			KillTimer(TIMER_AF_2X);
			m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens1, nBiz_Seq_Lens1);
			m_btLens_Index1.SetCheck(true);
#else
			m_AFModule.m_fnChangeRecipe(0);	
			m_AFModule.m_fnReadRecipe(&nRecipeIndex);

			if(nRecipeIndex == 0)				
			{
				//OnBnClickedBtnAutoFocus();			
				KillTimer(TIMER_AF_2X);
				m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens1, nBiz_Seq_Lens1);
				m_btLens_Index1.SetCheck(true);
			}
#endif
			
		}
	}
	if(nIDEvent == TIMER_AF_10X)
	{
		m_nZoomTimerCount++;

		if(m_nZoomTimerCount > LENS_CHANGE_TIMEOUT_SEC) // TimeOut
		{
			KillTimer(TIMER_AF_10X);
			m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens2, 0);//Time Out Error Return .(Unit Zero)
		}
		else if(m_nZoomTimerCount > 10)
		{	
#ifdef SIMUL_RUN
			KillTimer(TIMER_AF_10X);
			m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens2, nBiz_Seq_Lens2);
			m_btLens_Index2.SetCheck(true);
#else
			m_AFModule.m_fnChangeRecipe(1);	
			m_AFModule.m_fnReadRecipe(&nRecipeIndex);

			if(nRecipeIndex == 1)				
			{
				//OnBnClickedBtnAutoFocus();			
				KillTimer(TIMER_AF_10X);
				m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens2, nBiz_Seq_Lens2);
				m_btLens_Index2.SetCheck(true);
			}
#endif
		}
	}
	if(nIDEvent == TIMER_AF_20X)
	{
		m_nZoomTimerCount++;

		if(m_nZoomTimerCount > LENS_CHANGE_TIMEOUT_SEC) // TimeOut
		{
			KillTimer(TIMER_AF_20X);
			m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens3, 0);//Time Out Error Return .(Unit Zero)
		}
		else if(m_nZoomTimerCount > 10)
		{	
#ifdef SIMUL_RUN
			KillTimer(TIMER_AF_20X);
			m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens3, nBiz_Seq_Lens3);
			m_btLens_Index3.SetCheck(true);
#else
			m_AFModule.m_fnChangeRecipe(2);	
			m_AFModule.m_fnReadRecipe(&nRecipeIndex);

			if(nRecipeIndex == 2)				
			{
				//OnBnClickedBtnAutoFocus();			
				KillTimer(TIMER_AF_20X);
				m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_ReviewLens, nBiz_Seq_Lens3, nBiz_Seq_Lens3);
				m_btLens_Index3.SetCheck(true);
			}
#endif
		}
	}
	if(nIDEvent == TIMER_AF_RUN)
	{
		m_nAFTimerCount++;
		int AF_Result = 0;
		if(m_nAFTimerCount > AF_TIMEOUT_SEC) // TimeOut
		{
			KillTimer(TIMER_AF_RUN);
			
			OnBnClickedBtnAfOff();	
			m_bAFOnce = false;
			m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AF, 0, 
				(USHORT)sizeof(int), (UCHAR*)&AF_Result);


		}
		else if(m_nAFTimerCount > 3)
		{	
#ifdef SIMUL_RUN
			KillTimer(TIMER_AF_RUN);
			AF_Result= 1;
			m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AF, 0, 
				(USHORT)sizeof(int), (UCHAR*)&AF_Result);
			m_btAF_On.SetCheck(false);
			m_bAFOnce = false;
#else
			AFModuleData AfModuleData;	
			AfModuleData.resetValue();
			m_AFModule.m_fnReadState(&AfModuleData);

			if(AfModuleData.bIn_focus==TRUE && AfModuleData.nMiv >0)				
			{
				AF_Result= 1;
				KillTimer(TIMER_AF_RUN);
				OnBnClickedBtnAfOff();	
				
				m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AF, 0, 
					(USHORT)sizeof(int), (UCHAR*)&AF_Result);
				m_btAF_On.SetCheck(false);
				m_bAFOnce = false;

				OnBnClickedBtnReadNowpos();

				CString strNewValue;
				strNewValue.Format("%d", ::GetTickCount()-m_AFTime);
				WritePrivateProfileString(Section_SVID, Key_STICK_REVIEWAF_TIME, strNewValue, SVID_LIST_PATH);
				m_AFTime = 0;

			}
#endif
		}
	}
	if(nIDEvent == TIMER_AF_CHECK)
	{
		m_AFModule.m_fnReadState(&m_AfModuleData);
	}
	__super::OnTimer(nIDEvent);
}

void CVS13AutoFocusDlg::OnBnClickedBtnLens4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CVS13AutoFocusDlg::OnBnClickedBtnAutoFocus()
{
	if(m_bAFOnce == true)
	{
		SetTimer(TIMER_AF_RUN, 300, NULL);	
		m_AFModule.m_fnLaserOn();
		m_AFModule.m_fnAFZAFOn();
		m_nAFTimerCount = 0;
	}
	else
	{
		m_AFModule.m_fnLaserOn();
		m_AFModule.m_fnAFZAFOn();
		m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AF, 1);
		//m_AFModule.m_fnAFZAFAOIOn();

		m_btAF_On.SetCheck(true);
		m_btAF_Off.SetCheck(false);
	}

	//m_nAFTimerCount = 0;
	//SetTimer(TIMER_AF_RUN, 100, NULL);	
	//Sleep(100);
	//AFModuleData AfModuleData;	
	//AfModuleData.resetValue();
	//int TimeOut = 120;//5sec
	//DWORD TimeCeck= GetTickCount();
	//while(AfModuleData.bIn_focus<=0 || AfModuleData.nMiv<=0)
	//{
	//	m_AFModule.m_fnReadState(&AfModuleData);
	//	Sleep(10);
	//	TimeOut--;
	//	if(TimeOut<0)
	//	{
	//		TimeCeck= GetTickCount()-TimeCeck;
	//		m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AF, 0);
	//		return;
	//	}
	//}
	// TimeCeck= GetTickCount()-TimeCeck;
	//m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AF, 1);
	
}

void CVS13AutoFocusDlg::OnBnClickedBtnAfOff()
{
	m_AFModule.m_fnAFZStop();	
	m_AFModule.m_fnLaserOff();

	if(m_bAFOnce != true)
		m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AF, 0);
	m_btAF_On.SetCheck(false);
	m_btAF_Off.SetCheck(true);
}


void CVS13AutoFocusDlg::OnBnClickedBtnHome()
{
	int nError = 0;
	//m_AFModule.m_fnHomeLlc(FALSE, &nError);


	m_AFModule.m_fnLlcOnOff(TRUE);
	m_AFModule.m_fnHomeLlc(FALSE, &nError);
	m_AFModule.m_fnSetLLCSpeed(300, 2500, 2500);
}




void CVS13AutoFocusDlg::OnBnClickedBtnReadNowpos()
{
	CString strLogMessage;
	int NowAFPos = 0;
	m_AFModule.m_fnAFZPosRead(&NowAFPos);
	strLogMessage.Format("AF:%d", NowAFPos);
	m_fnAppendList(strLogMessage, TEXT_WARRING_YELLOW);

	m_cpServerInterface.m_fnSendCommand_Nrs(TASK_10_Master, nBiz_Func_AF, nBiz_Seq_AFNowPos, 0, 
		(USHORT)sizeof(int), (UCHAR*)&NowAFPos);
}
