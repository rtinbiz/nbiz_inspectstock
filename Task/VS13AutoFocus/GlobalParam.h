#ifndef _GLOBALPARAMETER_H_
#define _GLOBALPARAMETER_H_

#include "SequenceDefine.h"
#include "GlobalEnumList.h"
#include "GlobalDefine.h"

static const int	LIST_ALARM_COUNT = 200;

static const int	RETRY_COUNT = 1;

static const int	MAX_BARCODE = 24;

static const int	MAX_QRCODE = 24;

static const int	ON = 1;

static const int	OFF = 0;

static const int	POSITION_OFFSET = 10;

static const int	MAX_OBJECT_COUNT = 30;

static const int	NONE = 0;

static const int	STICK = 1;

static const int	PAPER = 2;

#endif