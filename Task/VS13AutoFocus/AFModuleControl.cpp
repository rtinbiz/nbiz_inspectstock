#include "StdAfx.h"
#include "AFModuleControl.h"



CAFModuleControl::CAFModuleControl(void)
{
	InitializeCriticalSection(&m_csAF);
}

CAFModuleControl::~CAFModuleControl(void)
{	
	DeleteCriticalSection(&m_csAF);	
}

void CAFModuleControl::m_fnAFClose()
{
	//CSequenceManager::m_fnStartScenario(FALSE);
}

BOOL CAFModuleControl::m_fnAFModuleInit(char *chPortName)
{	
	char logMsg[512] = {'\0'};
	char dispMsg[256] = {'\0'};
	int ret = 0;
	int iRet = -1;
	HANDLE h[2] = {INVALID_HANDLE_VALUE, INVALID_HANDLE_VALUE}; 

	// obtain arguments	
	int baudrate_start = 9600;//9600;		// upon powercycle sensor always starts with 9600
	int baudrate_run = 57600;
	int point_count = -1;

	// open port, return if failed
	ret = atf_OpenConnection (chPortName, baudrate_start);
	if (ret)
	{
		// example of saving message to the log file - terminate message with new line character '\n'
		sprintf_s(logMsg, "Error: Failed to open Com port. [ErrorNum=%d; ComPort=%s; BaudRate=%d]\n", ret, chPortName, baudrate_start);
		//G_AddLog(3, logMsg);	
		iRet = ret;

		goto exitmain;
	}
	else
	{
		// example of saving message to the log file - terminate message with new line character '\n'
		sprintf_s(logMsg, "Info: Com port opened successfully. [ComPort=%s; BaudRate=%d]\n", chPortName, baudrate_start);
		//G_AddLog(3, logMsg);
	}

	// verify that communication can be established
	ret = atf_PingAck();
	if (ret)
	{
		// example of saving message to the log file - terminate message with new line character '\n'
		sprintf_s(logMsg, "Error: Failed to ping sensor. [ErrorNum=%d]\n", ret);
		// use log level 1 to save all messages
		//G_AddLog(3, logMsg);
		//G_AddLog(3, "Check serial port connection - switch sensor power OFF/ON.\n");
		iRet = ret;
		goto exitmain;
	}
	else
	{
		// example of saving message to the log file - terminate message with new line character '\n'
		sprintf_s(logMsg, "Info: Sensor pinged successfully.\n");
		// use log level 1 to save all messages
		//G_AddLog(3, logMsg);		
	}

	// Change the baudrate for faster communication if desired
	if (baudrate_run != baudrate_start)
	{
		// OK, sensor is there, now we need to change baudrate to the desired one
		ret = atf_WriteBaud (baudrate_run);
		if (ret)
		{
			// example of saving message to the log file - terminate message with new line character '\n'
			sprintf_s(logMsg, "Error: Failed to change sensor Com port baudrate. [ErrorNum=%d]\n", ret);
			// use log level 1 to save all messages
			atf_saveToLog(logMsg, 1);
			// or using other logging function the above two lines of code can be replaced by the folowing:
			//			atf_saveToLog2(1, "%s%d%s", "Error: Failed to change sensor Com port baudrate. [ErrorNum=", ret, "]\n");
			//G_AddLog(3, logMsg);
			iRet = ret;
			goto exitmain;
		}

		Sleep(300);	// get time to change

		// change Com port baudrate
		ret = atf_ChangeCommBaudrate(baudrate_run);
		if (ret)
		{
			// example of saving message to the log file - terminate message with new line character '\n'
			sprintf_s(logMsg, "Error: Failed to change Com port baudrate. [ErrorNum=%d; Baudrate=%d]\n", ret, baudrate_run);
			// use log level 1 to save all messages
			//G_AddLog(3, logMsg);
			iRet = ret;
			goto exitmain;
		}
		else
		{
			// example of saving message to the log file - terminate message with new line character '\n'
			sprintf_s(logMsg, "Info: Com port baudrate changed successfully. [Baudrate=%d]\n", baudrate_run);
			// use log level 1 to save all messages			
			// or using other logging function the above two lines of code can be replaced by the folowing:
			//			atf_saveToLog2(1, "%s%d%s", "Info: Com port baudrate changed successfully. [Baudrate=", baudrate_run, "]\n");
			//G_AddLog(3, logMsg);
		}
		
		// wait a little
		Sleep (200);

		// re-verify that communication can be established
		ret = atf_PingAck();
		if (ret)
		{
			// example of saving message to the log file - terminate message with new line character '\n'
			sprintf_s(logMsg, "Error: Failed to ping sensor after Com port baudrate change. [ErrorNum=%d]\n", ret);
			// use log level 1 to save all messages
			atf_saveToLog(logMsg, 1);
			// or using other logging function the above two lines of code can be replaced by the folowing:
			//			atf_saveToLog2(1, "%s%d%s", "Error: Failed to ping sensor after Com port baudrate change. [ErrorNum=", ret, "]\n");
			//G_AddLog(3, logMsg);
			//G_AddLog(3, "Check serial port connection - switch sensor power OFF/ON.\n");
			iRet = ret;
			goto exitmain;;
		}
		else
		{
			// example of saving message to the log file - terminate message with new line character '\n'
			sprintf_s(logMsg, "Info: Sensor pinged successfully.\n");
			// use log level 1 to save all messages
			atf_saveToLog(logMsg, 1);
		}
	}

	// read current sensor configuration. At that point you may want 
	// to insert a code to change some of parameters. Reading of the parameters
	// is optional and doesn't affect sensor. (unless is done too often and then
	// it can cuase measureing delays)
	//
 	short status = 0;
// 
	if (atf_ReadStatus (&status))
	{
		//G_AddLog(3, "ERROR: failed to read status\n");
		goto exitmain;
	}

	RsData rs;

	if (atf_ReadPositionPacked (&rs))
	{
		//G_AddLog(3, "ERROR: failed to read position\n");
		goto exitmain;
	}

	AtfCodesEnm af_code = AfStatusOK;

	if (atf_ReadErrorCode (&af_code))
	{
		//G_AddLog(3, "ERROR: failed to read operation code\n");
		goto exitmain;
	}
// 
// 	u_short laser = 0;
// 
// 	if (atf_ReadLaserPower (&laser, 10))
// 	{
// 		G_AddLog(3, "ERROR: failed to read laser power\n");
// 		goto exitmain;
// 	}
// 
	int objective = 0;

	if (atf_ReadObjNum (&objective))
	{
		//G_AddLog(3, "ERROR: failed to read current objective\n");
		goto exitmain;
	}
// 
	int hwStatus = 0;

	if (atf_ReadHwStat (&hwStatus))
	{
		//G_AddLog(3, "EERROR: failed to read hardware status\n");
		goto exitmain;
	}

	u_short lightPwm = 0;

	if (atf_ReadLedPwm (0,&lightPwm))
	{
		//G_AddLog(3, "ERROR: failed to read pwm\n");
		goto exitmain;
	}

	// print out what has been learned
// 	sprintf_s(dispMsg, "%s0x%x%s%d%s%s%s%s%s%d%s%d%s%d%s0x%x%s%d%s%s%s", 
// 		"\nATF stat=", status,
// 		"; pos=" , rs.position,
// 		(rs.miv ? "; Miv" : "; "),
// 		(rs.in_focus ? "; InFoc" : "; "),
// 		(rs.laser_enabled ? ";; " : ", Laser Disabled; "),
// 		//(af_code <= AfNotAvailable ? g_AtfStatusName[af_code] : "BadCode"),
// 		"; code=", (int)af_code,
// 		"; laser=", laser,
// 		"; objective=", objective,
// 		"; fpga diag=", hwStatus,
// 		"; light pwm=", lightPwm,
// 		";\nATF info: ", atf_PartInfo(), "\n");
// 
// 	G_AddLog(3, dispMsg);

	// in a loop read measurements and report position unless key has been pressed
	for (;point_count!=0;point_count--) 
	{
		atf_ReadPositionPacked (&rs);
		sprintf_s(dispMsg, "%s%d%s%s%s%s",
			"Pos=", rs.position,
			(rs.miv ? "; Miv" : ";"),
			(rs.in_focus ? ";InFoc" : ";"),
			(rs.laser_enabled ? ";;" : "; Laser Disabled;"),
			"\n");
//		G_AddLog(3, dispMsg);

		//if (point_count == 0 || _kbhit()) break;
		break;

		Sleep (500);
	}
// 	G_AddLog(3, "Resetting sensor. Wait...\n"); // 임시 리셋 해제
 	atf_Reset();

	// Laser ON
	atf_EnableLaser();

	//atf_WriteLedCurrent(1, 300);

	iRet = 0;
	Sleep(200);

	//CSequenceManager::m_fnStartScenario(TRUE, FALSE);
	return TRUE;
exitmain:
	{
		atf_CloseConnection();	
		//G_AddLog(3, "AF Module Init Fail");
		return FALSE;
	}	
}

void CAFModuleControl::m_fnCheck()
{	
// 	if(G_SystemModeData.unSystemMode == INSPECT_EXIT)
// 		return;
// 
// 	Sleep(300);
// 
// 	CMainWnd *pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;	
// 
// 	
// 	float nPos = 0;
// 	int nErrorCode = 0;
// 	char dispMsg[256] = {'\0'};
// 
// 	// 포지션 Read
// 	EnterCriticalSection(&m_csAF);	
// 	RsData rs;
// 	atf_ReadPositionPacked (&rs);
// 	
// 	pMainWnd->m_DataHandling.m_AfModuleData.nPosition = rs.position;
// 	pMainWnd->m_DataHandling.m_AfModuleData.nMiv = rs.miv;
// 	pMainWnd->m_DataHandling.m_AfModuleData.bIn_focus = rs.in_focus;
// 	pMainWnd->m_DataHandling.m_AfModuleData.bLaser_enabled = rs.laser_enabled;
// 
// 	m_fnReadLedPwm(1, &pMainWnd->m_DataHandling.m_AfModuleData.nNowPwm);
// 
// 	atf_getLlcPosition(&nPos, &nErrorCode);
// 	
// 	LeaveCriticalSection(&m_csAF);
// 
// 	//atf_getLlcMotorCurrent(&nPos, &nErrorCode);
// // 
// 	int nInpos = 3;
//  	if(nInpos > abs(0-nPos))
//  	{
// #ifdef USE_GLASS_INSPECTION_10
// 		G_SystemModeData.unLensIndex = LENS_INDEX_1X;
// #else
// 		G_SystemModeData.unLensIndex = LENS_INDEX_2X;
// #endif
// 		
//  	}
//  	else if(nInpos > abs(38-nPos))
//  	{
//  		G_SystemModeData.unLensIndex = LENS_INDEX_10X;
//  	}
//  	else if(nInpos > abs(76-nPos))
//  	{
 	//	G_SystemModeData.unLensIndex = LENS_INDEX_20X;
 //	}
	
// 	int iRet = 0, nPos =0;
// 	iRet = atf_ReadAbsZPos(&nPos);
// 	G_AddLog(3, "%d : Pos", nPos);
// 
// 	sprintf_s(dispMsg, "%s%d%s%s%s%s",
// 		"Pos=", rs.position,
// 		(rs.miv ? "; Miv" : ";"),
// 		(rs.in_focus ? ";InFoc" : ";"),
// 		(rs.laser_enabled ? ";;" : "; Laser Disabled;"),
// 		"\n");
// 	G_AddLog(3, dispMsg);

	// Limit Check
	
}

void CAFModuleControl::m_fnAFZAbsMove(int nPos)
{
	EnterCriticalSection(&m_csAF);
	atf_MoveZ(nPos);

	LeaveCriticalSection(&m_csAF);
}

void CAFModuleControl::m_fnAFZMoveStop()
{
	EnterCriticalSection(&m_csAF);
	atf_StopZMotor();
	LeaveCriticalSection(&m_csAF);
}



void CAFModuleControl::m_fnAFZStop()
{
	EnterCriticalSection(&m_csAF);

	atf_AfStop();
	LeaveCriticalSection(&m_csAF);
}

void CAFModuleControl::m_fnAFZAFOn()
{
	EnterCriticalSection(&m_csAF);
	atf_AFTrack();
	LeaveCriticalSection(&m_csAF);
}

void CAFModuleControl::m_fnLaserOn()
{
	EnterCriticalSection(&m_csAF);
	atf_EnableLaser();
	LeaveCriticalSection(&m_csAF);
}

void CAFModuleControl::m_fnLaserOff()
{
	EnterCriticalSection(&m_csAF);
	atf_DisableLaser();
	LeaveCriticalSection(&m_csAF);
} 

void CAFModuleControl::m_fnAFZAFAOIOn()
{
	EnterCriticalSection(&m_csAF);
	atf_AFAoiTrack();
	//atf_AFAoiOnlyTrack();
	LeaveCriticalSection(&m_csAF);
}

void CAFModuleControl::m_fnAFZMakeZeroPos()
{
	EnterCriticalSection(&m_csAF);
	atf_Make0();
	LeaveCriticalSection(&m_csAF);
}

void CAFModuleControl::m_fnAFZPosClear()
{
	EnterCriticalSection(&m_csAF);
	atf_WriteAbsZPos(0);
	LeaveCriticalSection(&m_csAF);
}

void CAFModuleControl::m_fnAFZPosRead(int *pPos)
{
	EnterCriticalSection(&m_csAF);
	atf_ReadAbsZPos(pPos);
	LeaveCriticalSection(&m_csAF);
}

void CAFModuleControl::m_fnAFZAoiOnceAF()
{
	EnterCriticalSection(&m_csAF);	
	atf_AfDuvJump();
	LeaveCriticalSection(&m_csAF);
}

int CAFModuleControl::m_fnReadLedCurrent (int which, u_short* pCurrent)
{
	EnterCriticalSection(&m_csAF);
	atf_ReadLedCurrent (which, pCurrent);
	LeaveCriticalSection(&m_csAF);

	return 0;
}
int CAFModuleControl::m_fnWriteLedCurrent (int which, u_short uCurrent)
{
	EnterCriticalSection(&m_csAF);
	atf_WriteLedCurrent (which, uCurrent);
	LeaveCriticalSection(&m_csAF);

	return 0;
}
int CAFModuleControl::m_fnReadLedPwm (int which, u_short* pPWM)
{
	EnterCriticalSection(&m_csAF);
	atf_ReadLedPwm (which, pPWM);
	LeaveCriticalSection(&m_csAF);

	return 0;
}
int CAFModuleControl::m_fnWriteLedPwm (int which, u_short uPWM)
{
	EnterCriticalSection(&m_csAF);
	atf_WriteLedPwm (which, uPWM);
	LeaveCriticalSection(&m_csAF);

	return 0;
}

int CAFModuleControl::m_fnReadHWSwitch(int *nStatus)
{
	EnterCriticalSection(&m_csAF);
	atf_ReadHwStat (nStatus);
	LeaveCriticalSection(&m_csAF);

	return 0;
}

int CAFModuleControl::m_fnChangeRecipe(int nNum)
{
	EnterCriticalSection(&m_csAF);
	atf_WriteObjNum(nNum);
	LeaveCriticalSection(&m_csAF);

	return 0;
}

int CAFModuleControl::m_fnReadRecipe(int *pNum)
{
	EnterCriticalSection(&m_csAF);
	atf_ReadObjNum(pNum);
	LeaveCriticalSection(&m_csAF);

	return 0;
}


BOOL  CAFModuleControl::m_fnHomeLlc(bool bWaitForEnd, int *piErrCode)
{
	EnterCriticalSection(&m_csAF);
	atf_homeLlc(bWaitForEnd, piErrCode);
	LeaveCriticalSection(&m_csAF);
	return TRUE;
}

void GenerateFileName(char *pathname, const char *filename, char *filename_full) 
{
	sprintf_s(filename_full, MAX_PATH, "%s\\%s", pathname, filename);
}

BOOL  CAFModuleControl::m_fnLlcOnOff(BOOL bRes)
{
	EnterCriticalSection(&m_csAF);
	// initialze LLC
	BOOL bRetLoc = 	FALSE;
	int iErrCode = 0;
	bRetLoc = atf_initializeLlc(NULL, &iErrCode);
	// enable LLC motor
	
	if(bRes)
		bRetLoc = atf_enableLlcMotor(&iErrCode);
	else 
		bRetLoc = atf_disableLlcMotor(&iErrCode);

	LeaveCriticalSection(&m_csAF);

	return TRUE;
}

BOOL  CAFModuleControl::m_fnSetLLCSpeed(int nSpeed, int nAcc, int nDec)
{
	EnterCriticalSection(&m_csAF);
	// initialze LLC
	BOOL bRetLoc = 	FALSE;
	int iErrCode = 0;
	bRetLoc = atf_setLlcSpeed(nSpeed, &iErrCode);
	atf_setLlcAcceleration(nAcc, &iErrCode);
	atf_setLlcDeceleration(nDec, &iErrCode);

	LeaveCriticalSection(&m_csAF);

	return TRUE;
}

BOOL  CAFModuleControl::m_fnIsLlcInHomePos(int *piErrCode)
{
	EnterCriticalSection(&m_csAF);
	atf_isLlcInHomePos(piErrCode);
	LeaveCriticalSection(&m_csAF);
	return TRUE;
}

void	 CAFModuleControl::m_fnSetObjectiveNum(int iObjNum)
{
	EnterCriticalSection(&m_csAF);
	atf_setObjectiveNum(iObjNum);
	LeaveCriticalSection(&m_csAF);
}


void	 CAFModuleControl::m_fnSetMoveLlc(int iObjectiveNumber, bool bWaitForEnd, int *piErrCode)
{
	EnterCriticalSection(&m_csAF);
	atf_moveLlcToObjective(iObjectiveNumber, bWaitForEnd, piErrCode);
	LeaveCriticalSection(&m_csAF);
}

int 	CAFModuleControl::m_fnGetObjectiveNum()
{
	EnterCriticalSection(&m_csAF);
	int nObjectNum = 0;
	nObjectNum = atf_getObjectiveNum();
	return nObjectNum;
	LeaveCriticalSection(&m_csAF);
}
BOOL  CAFModuleControl::m_fnReadState(AFModuleData *pReadBuf)
{
	RsData rs;
	EnterCriticalSection(&m_csAF);
	
	atf_ReadPositionPacked (&rs);
	pReadBuf->nPosition = rs.position;
	pReadBuf->nMiv = rs.miv;
	pReadBuf->bIn_focus = rs.in_focus;
	pReadBuf->bLaser_enabled = rs.laser_enabled;
	atf_ReadLedPwm (0, &pReadBuf->nNowPwm);
	atf_getLlcPosition(&pReadBuf->nPos, &pReadBuf->nErrorCode);

	LeaveCriticalSection(&m_csAF);
	return TRUE;
}

// LED control ------------------------------------------------------------------------------
// there are up to 3 led (0,1,2)
int atf_ReadLedCurrent (int which, u_short* pCurrent);
int atf_WriteLedCurrent (int which, u_short uCurrent);
int atf_ReadLedPwm (int which, u_short* pPWM);
int atf_WriteLedPwm (int which, u_short uPWM);


/* LLC 쓰지 않을 듯
// 1  Home
int iRet = 0;
char llcCmd[128] = {'\0'};
// LLC home
sprintf(llcCmd, "XQ##;");
iRet = atf_CommToLlc (llcCmd, llcCmd);


//2  위치 이동
iPos = 380000;
sprintf(llcCmd, "PA=%d;", iPos);
iRet = atf_CommToLlc (llcCmd, llcCmd);

sprintf(llcCmd, "BG;");
iRet = atf_CommToLlc (llcCmd, llcCmd);

//3 stop
// LLC stop
sprintf(llcCmd, "ST;");
iRet = atf_CommToLlc (llcCmd, llcCmd);
*/

// 1. 		iRet = atf_WriteAbsZPos(iPosW);

// 2.     iRet = atf_ReadAbsZPos(&iPosR);

// 3.    atf_ReadLastMoveZ

// executes motion of move microsteps
//4.      int atf_MoveZ (int move);


// stops AF tracking
// int atf_AfStop ();
// starts AF tracking
// int atf_AFTrack ();
// start AF tracking first, once at focus continue on AOI tracking
// int atf_AFAoiTrack ();
// execute focus and disables, if DUV jump is defined will be taken
// int atf_AfDuvJump ();
// start AOI tracking
// int atf_AFAoiOnlyTrack ();
// 
// status and results ------------------------------------------------------------------------
// reads sensor position, typically in range +-512
// int atf_ReadPosition (float &fpos);
