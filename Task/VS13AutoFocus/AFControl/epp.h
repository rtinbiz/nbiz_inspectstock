/******************************************************************************
 *	
 *	(C) Copyright WDI 2010
 *	
 ******************************************************************************
 *
 *	FILE:		epp.h
 *
 *	PROJECT:	AFT Sensors
 *
 *	SUBPROJECT:	.
 *
 *	Description: Embedded part configuration protocol.
 *               Protocol design for accessing configuration of the part.
 *	
 ******************************************************************************
 *	
 *	Change Activity
 *	Defect  Date       Developer        Description
 *	Number  DD/MM/YYYY Name
 *	======= ========== ================ =======================================
 *          21/09/2009 Andrew L.        Initial version
 *          02/11/2010 Chris O.           
 *          15/11/2010 Chris O.         Added synchronization objects for
 *                                      multithreaded applications
 *                                     
 *****************************************************************************/

#pragma once

// data type definitions
typedef enum 
{
	DtNoDataEnm					= 0x00,
	DtByteEnm					= 0x10,
	DtWordEnm					= 0x20,
	DtDwordEnm					= 0x30,
	DtDataType					= 0x30,
	DtArrayEnm					= 0x40,
	DtOffsetEnm					= 0x80,
	DtSignedEnm					= 0x80,
	DtLastDataEnm				= 0x80,
} EppDataType;
#define DtTypeSize(dt) (((dt)&DtDataType)==DtDwordEnm ? 4 : (((dt)&DtDataType)>>4))
#define DtGetDataType(dt) ((dt)&DtDataType)

typedef enum  
{
	ErrOK						= 0,
	ErrNoAccess					= 1,
	ErrWrongType				= 2,
	ErrOutOfBound				= 3,
	ErrInvalid					= 4,
	ErrUnavailable				= 5,
	ErrNotSupported				= 6,
	ErrSntaxError				= 7,
	ErrNoresources				= 8,
	ErrInternal					= 9,
	ErrOperFailed				= 10,
	ErrTimeout					= 11,
	ErrChksum					= 12,
	ErrUnknown
} EppErrorCodes;

#define ErrNamesTab const char * g_eppErrorNames[] = \
{	\
		"0-ErrOK",						\
		"1-ErrNoAccess",				\
		"2-ErrWrongType",				\
		"3-ErrOutOfBound",				\
		"4-ErrInvalid",					\
		"5-ErrUnknown",					\
		"6-ErrNotSupported",			\
		"7-ErrSntaxError",				\
		"8-ErrNoresources",				\
		"9-ErrInternal",				\
		"10-ErrOperFailed",				\
		"11-ErrTimeout",				\
		"12-ErrChksum",					\
		"ErrUnknown"					\
};

#ifdef __cplusplus
#include "tserial.h"
#include "logger.h"

class CEpp : public Tserial
{
	// communication buffer
	char commBuff[5000];

public:
	// query code for data types
	enum 
	{
		QcParamInfo					= 1,
		QcParamName					= 2,
		QcParamComment				= 3,
		QcParamError				= 4,        // most recent error code visible on this parameter
		QcReadTime					= 5,        // time in seconds parameter was read most recently
		QcWriteTime					= 6,        // time in seconds parameter was written most recently
		QcValueCurrent				= 7,
		QcValueMax					= 8,
		QcValueMin					= 9,
		QcValueSetDate				= 10,
		QcEnumValueExclusive		= 11,
		QcEnumValueCombined			= 12,
		QcEnumName					= 13,
		QcEnumAllParamInfo			= 15,
	};
//	#define TagGetTag(tg) ((tg)&0xf)


	// session commands
	enum 
	{
		SessionCmdRead	            = 'R',
		SessionCmdWrite             = 'W',
		SessionCmdReadResponse      = 'r',
	};
	// transport commands
	enum 
	{
		TransportCodingBinaryLittleEndian	= 'l',
		TransportCodingBinaryBigEndian	    = 'b',
		TransportCodingAsciiStx		        = '<',
		TransportCodingAsciiEtx		        = '>',
		TransportAck			            = 'A',
		TransportNak			            = 'N'
	};

	// com port timeouts for operations taking long period of time
	enum
	{
		MicroStepTimeout    = 1000,
		MakeZeroTimeout     = 3000,
		SaveToSensorTimeout = 8000,
		MagnifChangeTimeout = 1000,
		MotorParamTimeout	= 1000
	};

#define EPPENTERFUN(funname) saveToLog2(CLogger::LogLow, "%s%s", funname, " - entered\n");
#define EPPEXITFUN(funname) saveToLog2(CLogger::LogLow, "%s%s", funname, " - exited\n");
#define EPPLOGMSG(pLogMsg, LogLev) saveToLog(pLogMsg, LogLev);
#define EPPLOGMSG2(LogLev, format, ...) saveToLog2(LogLev, format, __VA_ARGS__);

	// serial port, communication
	int Open (const char *port_arg, int rate_arg); 
	// reuse port that is already opened
	void Attach (HANDLE h); 
	// close port 
	void Close (); 
	// leave handle opened, assuming someone else needs it
	void Detach (); 

    typedef void (*ErrorFunctionPtr)(void* locale, bool bReading, EppErrorCodes err,int bid, int pid, EppDataType tag, int num, int offset);
    private: 
        ErrorFunctionPtr m_pFun;
        void* m_pFunLocale;
    public:
	void PrintfError (void* locale, bool bReading, EppErrorCodes err, int bid, int pid, EppDataType tag, int num, int offset);

    void RegisterErrorFunction (ErrorFunctionPtr pFun, void* pFunLocale) 
	{
        m_pFun = pFun;
        m_pFunLocale = pFunLocale;
    }

	// constructor & destructor
	CEpp () : Tserial(), m_pLogger(NULL)
	{
        m_pFun = (ErrorFunctionPtr)0;
        m_pFunLocale = 0;
		InitializeCriticalSection(&m_cs);
	}
	CEpp (CLogger *pLogger) : Tserial()
	{
		m_pLogger = pLogger;       
		m_pFun = (ErrorFunctionPtr)0;
        m_pFunLocale = 0;
		InitializeCriticalSection(&m_cs);
	}

	~CEpp() 
	{ 
		disconnect();
		DeleteCriticalSection(&m_cs);
	}

	// ------- generic interface --------------------
	const char* errName (EppErrorCodes err);

	// read the content of the element. returns ErrOK if all is fine, or other 
	// error otherwise
	EppErrorCodes _ReadArray (void *to, int bid, int pid, EppDataType tag, int num = 1, int offset = 0);
	EppErrorCodes ReadArray (void *to, int bid, int pid, EppDataType tag, int num = 1, int offset = 0) 
	{
		EppErrorCodes err = _ReadArray (to, bid, pid, tag, num, offset);
        if (serial_handle!=INVALID_HANDLE_VALUE && err!=ErrOK && err!=ErrUnavailable) 
		{
            PrintfError(m_pFunLocale, true, err, bid, pid, tag, num, offset);
        }
		if (err==ErrOperFailed) 
		{
			int bide = 0, pide = 0, err = 0;
			if (ReadMostRecentRetcode (&bide, &pide, (EppErrorCodes*)(&err))==ErrOK) 
			{
				EPPLOGMSG2(CLogger::LogLow, "%s%c%s%d%s%d%s",
						   "ATF Last Read Error: id=(", isalpha(bide) ? bide : '?', ", ", pide, ") Err=", err, ".\n");
			}
		}
		return err;
	}
	// sets the value of the single element
	EppErrorCodes _WriteArray (void *from, int bid, int pid, EppDataType tag, int num = 1, int offset = 0);
	EppErrorCodes WriteArray (void *from, int bid, int pid, EppDataType tag, int num = 1, int offset = 0) 
	{
		if (DtGetDataType(tag)==DtNoDataEnm) 
		{
			num=0;
			offset = 0;
		}
		EppErrorCodes err = _WriteArray (from, bid, pid, tag, num, offset);
        if (serial_handle!=INVALID_HANDLE_VALUE && err!=ErrOK && err!=ErrUnavailable) 
		{
            PrintfError(m_pFunLocale, false, err, bid, pid, tag, num, offset);
        }
		if (err==ErrOperFailed) 
		{
			int bide = 0, pide = 0, err = 0;
			if (ReadMostRecentRetcode (&bide, &pide, (EppErrorCodes*)(&err))==ErrOK) 
			{
				EPPLOGMSG2(CLogger::LogLow, "%s%c%s%d%s%d%s",
						   "ATF Last Read Error: id=(", isalpha(bide) ? bide : '?', ", ", pide, ") Err=", err, ".\n");
			}
			/* * * * This is code sample for testing only
			if (ReadIdError (bid, pid,(EppErrorCodes*)(&err))==ErrOK) 
			{
				EPPLOGMSG2(CLogger::LogLow, "%s%c%s%d%s%d%s",
						   "ATF Last Write Error 2: id=(", isalpha(bide) ? bid : '?', ", ", pid, ") Err=", err, ".\n");
			}
			EppDataType tag;
			int num;
			int ver;
			if (ReadIdCommInfo (bid, pid, &tag, &num, &ver)==ErrOK) 
			{
				EPPLOGMSG2(CLogger::LogLow, "%s%c%s%d%s%d%s%d%s%d%s",
						   "ATF Last Write Error Tag: id=(", isalpha(bide) ? bid : '?', ", ", pid, "), tag=", tag, ", num=", num, ", ver=", ver, ".\n");
			}
			*/
		}
		return err;
	}

	typedef enum 
	{
		IsEppVerUnknow	= 0,
		IsEppVerCheck	= 1,
		IsEppVerBasic	= 2,
		IsEppVerSupport = 3
	} EppVer_Mode;
	static EppVer_Mode s_m_ev;		// epp protocol version 

	bool EppVerCheck();

	// reads information describing the type of data used for particular communication register
	inline EppErrorCodes ReadIdCommInfo (int bid, int pid, EppDataType *ptag, int *pnum, int *pversion)
	{
		short commTable[4];

		if (!EppVerCheck()) return ErrNotSupported;
		EppErrorCodes err = _ReadArray (commTable, bid, pid, (EppDataType)(QcParamInfo|DtWordEnm), 3, 0);

		if (err==ErrOK) 
		{
			*ptag = (EppDataType)(commTable[0]);
			*pnum = commTable[1];
			*pversion = commTable[2];
		}

		return err;
	}

	// reads most recent status returned with particular command
	inline EppErrorCodes ReadIdError (int bid, int pid, EppErrorCodes* perr)
	{
		short commTable[4];

		if (!EppVerCheck()) return ErrNotSupported;
		EppErrorCodes err = _ReadArray (commTable, bid, pid, (EppDataType)(QcParamError|DtWordEnm), 1);

		if (err==ErrOK) 
		{
			*perr = (EppErrorCodes)(commTable[0]);
		}

		return err;
	}

	typedef struct	          // element describing most recent communication error
	{
		short iError;         // error that has been reported addressing iMostRecentField
		short iField;         // field that had been addressed most recently
		short iLen;           // number of elements accessed in most recent command
		short iOffset;        // offset in most recent command
		short iType;          // type of the acecss and data field used in most recent command
	} CommMostRecent;

	// reads most recent return code assigned by sensor
	inline EppErrorCodes ReadMostRecentRetcode (int *pbid, int *ppid, EppErrorCodes* perr)
	{
		CommMostRecent commErr;

		if (!EppVerCheck()) return ErrNotSupported;
		EppErrorCodes err = _ReadArray (&commErr, 's', 20, DtWordEnm, 2);

		if (err==ErrOK) 
		{
			*pbid = (commErr.iField >> 8) & 0xff;
			*ppid = commErr.iField & 0xff;
			*perr = (EppErrorCodes)(commErr.iError);
		}

		return err;
	}

	// restore communication
	int RecoverSensor();

	// check for Ack character received from the other side
	int PingAck();

	// resets sensor
	int Reset();

	EppErrorCodes _ChangeCommTimeouts(DWORD dwTimeout);
	EppErrorCodes _ResetCommTimeouts();
	EppErrorCodes _ChangeCommBaudrate(DWORD dwBaudRate);
	EppErrorCodes _GetCommBaudrate(DWORD *dwBaudrate);
	inline bool isSerialConnection() {return serial_handle != INVALID_HANDLE_VALUE ? true : false; }
	EppErrorCodes sendCmnd(char *pToSend, char *pToRecv = NULL, int iTimeout = 0, int iExpByteNum = 0);

	// logging
	CLogger *m_pLogger;
	void saveToLog(const char *pLogMsg, CLogger::LogLevels iLogLevel);
	void saveToLog2(CLogger::LogLevels iLogLevel, char *pFormat, ...);
	inline void setLogger(CLogger *pLogger) { m_pLogger = pLogger; }

private:
	CRITICAL_SECTION m_cs;
};

#endif // __cplusplus
