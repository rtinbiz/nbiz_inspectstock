#pragma once

typedef unsigned char   u_char;
typedef unsigned short  u_short;
typedef unsigned int    u_int;
typedef unsigned long   u_long;


#include "AFControl/atf_lib.h"
#include "Windows.h"
#include <iostream>
#include <tchar.h>
#include <conio.h>

class CAFModuleControl
{
public:
	CAFModuleControl(void);
	~CAFModuleControl(void);

public:
	BOOL m_fnAFModuleInit(char* chPortName);
	void m_fnAFZAbsMove(int nPos);
	void m_fnAFZMoveStop();
	void m_fnAFZStop();
	void m_fnAFZAFOn();
	void m_fnAFZAFAOIOn();
	void m_fnAFZMakeZeroPos();
	void m_fnAFZPosClear();
	void m_fnAFZPosRead(int *pPos);
	void m_fnAFClose();
	void m_fnCheck();
	void m_fnAFZAoiOnceAF();
	void m_fnLaserOn();
	void m_fnLaserOff();

	int m_fnReadLedCurrent (int which, u_short* pCurrent);
	int m_fnWriteLedCurrent (int which, u_short uCurrent);
	int m_fnReadLedPwm (int which, u_short* pPWM);
	int m_fnWriteLedPwm (int which, u_short uPWM);
	int m_fnChangeRecipe(int nNum);
	int m_fnReadRecipe(int *pNum);
	int m_fnReadHWSwitch(int *nStatus);

	// LLC
	
	BOOL  m_fnSetLLCSpeed(int nSpeed, int nAcc, int nDec);
	BOOL  m_fnHomeLlc(bool bWaitForEnd, int *piErrCode);
	BOOL  m_fnLlcOnOff(BOOL bRes);
	BOOL  m_fnIsLlcInHomePos(int *piErrCode);
	void  m_fnSetObjectiveNum(int iObjNum);
	void	m_fnSetMoveLlc(int iObjectiveNumber, bool bWaitForEnd, int *piErrCode);
	int	  m_fnGetObjectiveNum();	

	BOOL	m_fnReadState(AFModuleData *pReadBuf);
	CRITICAL_SECTION m_csAF;

private:
};
