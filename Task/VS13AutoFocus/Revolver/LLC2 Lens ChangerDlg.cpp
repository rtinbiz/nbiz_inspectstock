//*----------------------------------------------------------------------------
//*                WDI - Wise Device Inc, Feb 2011 
//*----------------------------------------------------------------------------
//* File Name    : LLC2 Lens ChangerDlg.cpp
//* Description  : Designed for controlling LLC2A device
//*                Implementation file
//*----------------------------------------------------------------------------
//* Version      : 700052A01
//* Date         : Feb 15, 2011
//*----------------------------------------------------------------------------*/

#include "stdafx.h"
#include "LLC2 Lens ChangerDlg.h"

#include <math.h>

#pragma comment(lib,"Version.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLLC2LensChangerDlg dialog

CLLC2LensChangerDlg::CLLC2LensChangerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLLC2LensChangerDlg::IDD, pParent)
  , m_uiSpeed(70)
  , m_uiDistance(14000)
  , m_strFW_Version(_T("0"))
  , m_strCPLD_Version(_T("0"))
  , m_strSW_Version(_T("0"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLLC2LensChangerDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_PORT_LENS, m_cmbSettingPort);
  DDX_Text(pDX, IDC_EDIT_SPEED, m_uiSpeed);
  DDX_Control(pDX, IDC_IMG_RIGHT, m_ctlImg_Right);
  DDX_Control(pDX, IDC_IMG_LEFT, m_ctlImg_Left);
  DDX_Control(pDX, IDC_EDIT_SPEED, m_ctlSpeed);
  DDX_Text(pDX, IDC_FW_VER, m_strFW_Version);
  DDX_Text(pDX, IDC_CPLD_VER, m_strCPLD_Version);
  DDX_Text(pDX, IDC_SW_VER, m_strSW_Version);
}

BEGIN_MESSAGE_MAP(CLLC2LensChangerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
  ON_CBN_SELCHANGE(IDC_PORT_LENS, &CLLC2LensChangerDlg::OnCbnSelchangePort)
  ON_BN_CLICKED(IDC_BUTTON_HOME, &CLLC2LensChangerDlg::OnBnClickedButtonHome)
  ON_BN_CLICKED(IDC_BUTTON_RIGHT, &CLLC2LensChangerDlg::OnBnClickedButtonRight)
  ON_BN_CLICKED(IDC_BUTTON_LEFT, &CLLC2LensChangerDlg::OnBnClickedButtonLeft)
  ON_BN_CLICKED(IDCANCEL, &CLLC2LensChangerDlg::OnBnClickedCancel)
  ON_WM_TIMER()
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SPEED, &CLLC2LensChangerDlg::OnDeltaposSpinSpeed)
  ON_BN_CLICKED(IDC_BUTTON_FACTORY_DEFAULT, &CLLC2LensChangerDlg::OnBnClickedButtonFactoryDefault)
  ON_EN_CHANGE(IDC_EDIT_SPEED, &CLLC2LensChangerDlg::OnEnChangeEditSpeed)
  ON_EN_KILLFOCUS(IDC_EDIT_SPEED, &CLLC2LensChangerDlg::OnEnKillfocusEditSpeed)
  ON_BN_CLICKED(IDC_BUTTON_CALL_LED_CALIBRATE, &CLLC2LensChangerDlg::OnBnClickedButtonCallLedCalibrate)
  ON_WM_NCDESTROY()
END_MESSAGE_MAP()


// CLLC2LensChangerDlg message handlers

BOOL CLLC2LensChangerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

  // Get File Version Info
  GetVersionInfo();

  // Initialize Port
  SetPortRegistry(uiPorts);
  m_cmbSettingPort.SetCurSel(0);
  SortPort(m_cmbSettingPort, uiPorts);

  // Initialize Baud-Rate
  m_nBaudRate = CBR_9600;

  m_bIsRight = TRUE;
  m_bIsOnProcess = FALSE;
  m_bIsOnMotorMove = FALSE;

  m_uiMotorStatus = 0;

  m_nClock = DEFAULT_CLOCK;                  // 'K'
  m_nNofSteps_Flight = DEFAULT_STEPS_FLIGHT; // 'W'
  m_nNofSteps = DEFAULT_NOFSTEPS;            // 'N'

  m_uiTmpSpeed = m_uiSpeed;

  // Timer for checking Motor status
  SetTimer(MOTOR_STATUS_TIMER, TIMER1_TIME, NULL); // Check Motor Status regularily.

//   {
// 	G_AddLog(3, "포트연결 실패 %d번", m_nComportIndex[3]+1);
// 	bRes = FALSE;
//   }	
  m_nSettingPort = 2;

  OnCbnSelchangePort();
//   if(serComm.Open(m_nSettingPort, m_nBaudRate) == FALSE)
//   {
// 	 G_AddLog(3, "포트연결 실패 %d번", pMainWnd->m_SerailInterface.m_nComportIndex[3]+1);
// 	 return 0;
//   }



  ShowWindow(SW_HIDE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLLC2LensChangerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLLC2LensChangerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// Read Port Info from Registry
BOOL CLLC2LensChangerDlg::SetPortRegistry(CUIntArray& uiPorts)
{
  uiPorts.RemoveAll();

  BOOL bSuccess = FALSE;

  HKEY hSERIALCOMM;
  if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("HARDWARE\\DEVICEMAP\\SERIALCOMM"), 0, KEY_QUERY_VALUE, &hSERIALCOMM) == ERROR_SUCCESS)
  {
    DWORD dwMaxValueNameLen;
    DWORD dwMaxValueLen;
    DWORD dwQueryInfo = RegQueryInfoKey(hSERIALCOMM, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &dwMaxValueNameLen, &dwMaxValueLen, NULL, NULL);
    if (dwQueryInfo == ERROR_SUCCESS)
    {
      DWORD dwMaxValueNameSizeInChars = dwMaxValueNameLen + 1; //Include space for the NULL terminator
      DWORD dwMaxValueNameSizeInBytes = dwMaxValueNameSizeInChars * sizeof(TCHAR);
      DWORD dwMaxValueDataSizeInChars = dwMaxValueLen/sizeof(TCHAR) + 1; //Include space for the NULL terminator
      DWORD dwMaxValueDataSizeInBytes = dwMaxValueDataSizeInChars * sizeof(TCHAR);

      ATL::CHeapPtr<TCHAR> szValueName;
      ATL::CHeapPtr<BYTE> byValue;
      if (szValueName.Allocate(dwMaxValueNameSizeInChars) && byValue.Allocate(dwMaxValueDataSizeInBytes))
      {
        bSuccess = TRUE;

        DWORD dwIndex = 0;
        DWORD dwType;
        DWORD dwValueNameSize = dwMaxValueNameSizeInChars;
        DWORD dwDataSize = dwMaxValueDataSizeInBytes;
        memset(szValueName.m_pData, 0, dwMaxValueNameSizeInBytes);
        memset(byValue.m_pData, 0, dwMaxValueDataSizeInBytes);
        LONG nEnum = RegEnumValue(hSERIALCOMM, dwIndex, szValueName, &dwValueNameSize, NULL, &dwType, byValue, &dwDataSize);
        while (nEnum == ERROR_SUCCESS)
        {
          //If the value is the correct type, add the port number to the array
          if (dwType == REG_SZ)
          {
            TCHAR* szPort = reinterpret_cast<TCHAR*>(byValue.m_pData);
            uiPorts.Add(_ttoi(&szPort[3]));
          }

          dwValueNameSize = dwMaxValueNameSizeInChars;
          dwDataSize = dwMaxValueDataSizeInBytes;
          memset(szValueName.m_pData, 0, dwMaxValueNameSizeInBytes);
          memset(byValue.m_pData, 0, dwMaxValueDataSizeInBytes);
          ++dwIndex;
          nEnum = RegEnumValue(hSERIALCOMM, dwIndex, szValueName, &dwValueNameSize, NULL, &dwType, byValue, &dwDataSize);
        }
      }
      else
        SetLastError(ERROR_OUTOFMEMORY);
    }
		
    //Close the registry key   
    RegCloseKey(hSERIALCOMM);

    if (dwQueryInfo != ERROR_SUCCESS)
      SetLastError(dwQueryInfo);
  }

  return bSuccess;
}

BOOL CLLC2LensChangerDlg::SortPort(CComboBox& comboPort, CUIntArray& uiPorts)
{
// Get array size
  UINT PortMax = (UINT)uiPorts.GetSize();
  UINT prev, next, tmp;

  for(UINT loop = 0; loop < PortMax-1; loop++)
  {
    prev = uiPorts.GetAt(loop);

// Sort port number
    for(UINT sloop = loop + 1; sloop < PortMax; sloop++)
    {
      next = uiPorts.GetAt(sloop);

      if(prev > next)
      {
        tmp = prev;
        prev = next;
        next = tmp;

        uiPorts.SetAt(loop, prev);
        uiPorts.SetAt(sloop, next);
      }
    }
  }

  CString cPort;
  tmp = 0;

// Add ports to the ComboBox
  for(UINT loop = 0; loop < PortMax; loop++)
  {
    prev = uiPorts.GetAt(loop);

    if(prev != tmp)
    {
      cPort.Format(_T("COM%d"), prev);
      comboPort.AddString(cPort);
    }

    tmp = prev;
  }

  return 0;
}

// When communication port is disconneted, it will initialize display
void CLLC2LensChangerDlg::PortDisconnected(void)
{
  UpdateData();
  m_uiSpeed = 0;
  m_uiDistance = 0;
  UpdateData(FALSE);

  m_ctlImg_Right.ShowWindow(SW_HIDE);
  m_ctlImg_Left.ShowWindow(SW_HIDE);

  if(serComm.IsOpened())
  {
    serComm.Close();
  }
  m_cmbSettingPort.SetCurSel(0);
}

// Select Port
void CLLC2LensChangerDlg::OnCbnSelchangePort()
{
  // TODO: Add your control notification handler code here
  BOOL SerComm_Res;

  CString cStr;
//  int nPos;

//   int nCurSel = m_cmbSettingPort.GetCurSel();
// 
//   if(nCurSel == 0)
//   {
//     PortDisconnected();
//     AfxMessageBox(_T("Communication Port is disconnected."));
//     return;
//   }
// 
//   nPos = m_cmbSettingPort.GetLBTextLen(nCurSel);
//   m_cmbSettingPort.GetLBText(nCurSel, cStr.GetBuffer(nPos));
//   cStr.ReleaseBuffer();

//  m_nSettingPort = _ttoi(cStr.Mid(3,3));

  if(serComm.IsOpened())
  {
    serComm.Close();
  }

  SerComm_Res = serComm.Open(m_nSettingPort, m_nBaudRate);

  if(!SerComm_Res)
  {
    PortDisconnected();
    AfxMessageBox(_T("Communication Port is disconnected."));
    return;
  }

  m_bIsOnProcess = TRUE;

  serComm.Flush();

  transDataBuffer[0] = TRANS_CMD_ACK;

  serComm.SendData(transDataBuffer, TRANS_CMD_ACK_SIZE); // 1 Bytes Data
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_ACK_SIZE);

  if(!readSize)
  {
    PortDisconnected();
    AfxMessageBox(_T("Cannot Communicate with LLC2 Board."));

    m_bIsOnProcess = FALSE;

    return;
  }
  else if(recvDataBuffer[0] != RECV_CMD_RES_ACK)
  {
    transDataBuffer[0] = END_DELIMENATOR;
    serComm.SendData(transDataBuffer, TRANS_CMD_ACK_SIZE); // 1 Bytes Data
    readSize = serComm.ReadData(recvDataBuffer, RECV_DATA_MAX);

    transDataBuffer[0] = TRANS_CMD_ACK;
    serComm.SendData(transDataBuffer, TRANS_CMD_ACK_SIZE); // 1 Bytes Data
    readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_ACK_SIZE);
    if(recvDataBuffer[0] != RECV_CMD_RES_ACK)
    {
      AfxMessageBox(_T("Response is not correct."));

      serComm.Close();
      m_cmbSettingPort.SetCurSel(0);

      m_bIsOnProcess = FALSE;
      return;
    }
  }

  serComm.Flush(PURGE_RXCLEAR);
// Read Firmware Version
  transDataBuffer[0] = (char)TRANS_CMD_GET_FW_VER;
  serComm.SendData(transDataBuffer, TRANS_CMD_GET_FW_VER_SIZE); // 1 Bytes Data
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_GET_FW_VER_SIZE);
  recvDataBuffer[readSize] = NULL;

  CString cStr_FW = CString(recvDataBuffer);

  serComm.Flush(PURGE_RXCLEAR);
// Read CPLD Version
  transDataBuffer[0] = (char)TRANS_CMD_GET_CPLD_VER;
  serComm.SendData(transDataBuffer, TRANS_CMD_GET_CPLD_VER_SIZE); // 1 Bytes Data
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_GET_CPLD_VER_SIZE);

  char cpldVersion[CPLD_NUMBER_MAX_SIZE];
  cpldVersion[0] = CPLD_FIRST_NUMBER;
  UINT uiPos = GetCPLD_Version(recvDataBuffer, &cpldVersion[1]);
  cpldVersion[uiPos+1] = NULL;

  serComm.Flush(PURGE_RXCLEAR);
// Read K; Clock
  transDataBuffer[0] = TRANS_CMD_CLOCK;
  transDataBuffer[1] = END_DELIMENATOR; 
  serComm.SendData(transDataBuffer, TRANS_CMD_RD_CLOCK_SIZE); // 2 Bytes Data
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_RD_CLOCK_SIZE);
  m_nClock = CMD_Tokenizer(recvDataBuffer, readSize);

  m_nNofSteps_Flight = DEFAULT_STEPS_FLIGHT;

  serComm.Flush(PURGE_RXCLEAR);
// Read N; Number of Steps
  transDataBuffer[0] = TRANS_CMD_NSTEPS;
  transDataBuffer[1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_RD_NSTEPS_SIZE); // 2 Bytes Data
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_RD_NSTEPS_SIZE);
  m_nNofSteps = CMD_Tokenizer(recvDataBuffer, readSize);

  if(m_nNofSteps != DEFAULT_NOFSTEPS)
  {
    m_nNofSteps = DEFAULT_NOFSTEPS;

    transDataBuffer[0] = TRANS_CMD_NSTEPS;
    DATA_Tokenizer(transDataBuffer, m_nNofSteps, TRANS_KN_DATA_SIZE);
    transDataBuffer[TRANS_KN_DATA_SIZE+1] = END_DELIMENATOR;
    serComm.SendData(transDataBuffer, RECV_CMD_RD_NSTEPS_SIZE); // 'Nhhhh;'
    readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_RD_NSTEPS_SIZE);
  }

  UpdateData();

  m_strFW_Version = cStr_FW;
  m_strCPLD_Version = CString(cpldVersion);

  // Calculate AC/DC
  if(m_nClock)
  {
    m_dblAC_DC = (double)2*ROTARY_LENGTH/MOTOR_STEP * pow((double)HARD_CLOCK/m_nClock, 2);
    m_dblSpeed = sqrt(2 * ROTARY_LENGTH * m_nNofSteps_Flight * m_dblAC_DC / MOTOR_STEP);

    m_uiTmpSpeed = m_uiSpeed = (UINT)m_dblSpeed;

    if(m_uiSpeed > SPEED_MAX_VALUE)
    {
      m_uiTmpSpeed = m_uiSpeed = SPEED_MAX_VALUE;
    }
  }
  else
  {
    m_dblSpeed = 0;
    m_uiTmpSpeed = m_uiSpeed = 0;
  }

  // Set distance
  m_uiDistance = m_nNofSteps;

  m_bIsOnProcess = FALSE;

  UpdateData(FALSE);
}

UINT CLLC2LensChangerDlg::GetCPLD_Version(char recvDataBuffer[], char strCPLD[])
{
  char ch;
  UINT uiPos = 0;

  for(int loop = 0; loop < CPLD_BYTES; loop++)
  {
    for(int sloop = 0; sloop < 2; sloop++) // 2 is for nibble
    {
      ch = (recvDataBuffer[loop] >> (SHIFT_ONE_NIBBLE * (1 - sloop))) & 0xf;
      if((ch >= 0) && (ch <= 9))
      {
        strCPLD[uiPos++] = ch + '0';
      }
      else
      {
        strCPLD[uiPos++] = ch + 'A' - 10;
      }
    }
  }

  return uiPos;
}

UINT CLLC2LensChangerDlg::CMD_Tokenizer(char *recvDataBuffer, UINT size)
{
  UINT recvData = 0;

  for(UINT loop = RECV_DATA_START_POS; loop < size; loop ++)
  {
    if((recvDataBuffer[loop] >= '0') && (recvDataBuffer[loop] <= '9'))
    {
      recvData <<= SHIFT_ONE_NIBBLE;
      recvData += (recvDataBuffer[loop] - '0');
    }
    else if((recvDataBuffer[loop] >= 'A') && (recvDataBuffer[loop] <= 'F'))
    {
      recvData <<= SHIFT_ONE_NIBBLE;
      recvData += (recvDataBuffer[loop] - 'A' + 10);
    }
    else if((recvDataBuffer[loop] >= 'a') && (recvDataBuffer[loop] <= 'f'))
    {
      recvData <<= SHIFT_ONE_NIBBLE;
      recvData += (recvDataBuffer[loop] - 'a' + 10);
    }
    else
    {
      if((loop != size - 1) || (recvDataBuffer[loop] != END_DELIMENATOR))
      {
        return UINT_MAX;
      }

      break;
    }
  }

  return recvData;
}

void CLLC2LensChangerDlg::DATA_Tokenizer(char *transDataBuffer, UINT data, UINT size)
{
  for(UINT loop = TRANS_DATA_START_POS; loop < size + 1; loop++)
  {
    transDataBuffer[loop] = (char)((data >> (size-loop)*SHIFT_ONE_NIBBLE) & 0xf);

    if((transDataBuffer[loop] >= 0) && (transDataBuffer[loop] <= 9))
    {
      transDataBuffer[loop] += '0';
    }
    else
    {
      transDataBuffer[loop] += ('A' - 10);
    }
  }
}

// Read File Version Info
void CLLC2LensChangerDlg::GetVersionInfo (void)
{
  TCHAR buff[MAX_PATH];

  if(::GetModuleFileName(0, buff, sizeof(buff)))
  {
    DWORD dwHnd;
    DWORD nSize;

    nSize = ::GetFileVersionInfoSize((LPTSTR)buff, &dwHnd);
    if (!nSize)
    {
      return;	// no version information available
    }

    LPBYTE lpVersionInfo = new BYTE[nSize];

    if(!::GetFileVersionInfo((LPTSTR)buff, 0L, nSize, lpVersionInfo))
    {
      delete [] lpVersionInfo;
      lpVersionInfo = NULL;
      return;
    }

    UINT uLen;

    if (!::VerQueryValue(lpVersionInfo, _T ("\\"), (LPVOID *)&m_fixedFileInfo, &uLen))
    {
      delete [] lpVersionInfo;
      lpVersionInfo = NULL;
      return;
    }


    DWORD dwFileVersionMS = m_fixedFileInfo->dwFileVersionMS;
    DWORD dwFileVersionLS = m_fixedFileInfo->dwFileVersionLS;

    delete [] lpVersionInfo;

    CString cStr;
    cStr.Format(_T("700052%c%d%d"), HIWORD(dwFileVersionMS) + 'A' - 1,
                                    LOWORD(dwFileVersionMS), HIWORD(dwFileVersionLS));

    UpdateData();
    m_strSW_Version = cStr;
    UpdateData(FALSE);
  }
} 

void CLLC2LensChangerDlg::ChangeDistance(UINT uiDistance)
{
// Write N; Number of Steps
  transDataBuffer[0] = TRANS_CMD_NSTEPS;
  DATA_Tokenizer(transDataBuffer, uiDistance, TRANS_KN_DATA_SIZE);
  transDataBuffer[TRANS_KN_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_NSTEPS_SIZE); // 'Nhhhh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_NSTEPS_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_NSTEPS_ACK_POS] != RECV_CMD_ACK)
  {
    CString cStr;
    cStr.Format(_T("Communication error with LLC2 Board. N = %d"), m_nNofSteps);
    AfxMessageBox(cStr);
    return;
  }
}

// Find Home position
void CLLC2LensChangerDlg::OnBnClickedButtonHome()
{
  // TODO: Add your control notification handler code here

  if(serComm.IsOpened() == FALSE)
  {
    AfxMessageBox(_T("Communication Port is not opened."));
    return;
  }

  if(!m_uiSpeed || !m_uiDistance)
  {
    return;
  }

// If Motor is busy, try next time...
  if(m_bIsOnProcess || m_bIsOnMotorMove || (m_uiMotorStatus & STATUS_BUSY))
  {
    return;
  }

  m_bIsOnMotorMove = TRUE;

  m_bIsOnProcess = TRUE;

  serComm.Flush(PURGE_RXCLEAR);

  switch(m_bIsRight)
  {
    case LEFT_DIRECTION:
      if(!(m_uiMotorStatus & STATUS_LEFT_TRIPPED))
      {
        ChangeDistance(HOME_NOFSTEPS);
        MoveScheme(FALSE);
        ChangeDistance(DEFAULT_NOFSTEPS);
      }
      break;

    case RIGHT_DIRECTION:
      if(!(m_uiMotorStatus & STATUS_RIGHT_TRIPPED))
      {
        ChangeDistance(HOME_NOFSTEPS);
        MoveScheme(TRUE);
        ChangeDistance(DEFAULT_NOFSTEPS);
      }
      break;
  }

  m_bIsOnProcess = FALSE;
}

void CLLC2LensChangerDlg::OnBnClickedButtonRight()
{
  // TODO: Add your control notification handler code here
  if(serComm.IsOpened() == FALSE)
  {
    AfxMessageBox(_T("Communication Port is not opened."));
    return;
  }

  if(!m_uiSpeed || !m_uiDistance)
  {
    return;
  }

// If Motor is busy, try next time...
  if(m_bIsOnProcess || m_bIsOnMotorMove || (m_uiMotorStatus & STATUS_BUSY))
  {
    return;
  }

  m_bIsOnMotorMove = TRUE;

  m_bIsOnProcess = TRUE;
  MoveRight();
  m_bIsOnProcess = FALSE;

  m_bIsRight = TRUE;
}

void CLLC2LensChangerDlg::MoveRight(void)
{
  serComm.Flush(PURGE_RXCLEAR);

  if(!(m_uiMotorStatus & STATUS_RIGHT_TRIPPED))
  {
    serComm.SendData(TRANS_CMD_MOVE_RIGHT, TRANS_CMD_MOVE_SIZE);
    readSize = serComm.ReadData(recvDataBuffer, RECV_DATA_MAX);

    if((recvDataBuffer[TRANS_DATA_START_POS] != TRANS_CMD_MOVE_RIGHT[TRANS_DATA_START_POS]) ||
       (recvDataBuffer[RECV_CMD_MOVE_POS] != RECV_CMD_ACK))
    {
      m_bIsOnMotorMove = FALSE;
      AfxMessageBox(_T("Move Right Error!!!"));
      return;
    }
  }

  m_bIsOnMotorMove = TRUE;
}

void CLLC2LensChangerDlg::OnBnClickedButtonLeft()
{
  // TODO: Add your control notification handler code here
  if(serComm.IsOpened() == FALSE)
  {
    AfxMessageBox(_T("Communication Port is not opened."));
    return;
  }

  if(!m_uiSpeed || !m_uiDistance)
  {
    return;
  }

// If Motor is busy, try next time...
  if(m_bIsOnProcess || m_bIsOnMotorMove || (m_uiMotorStatus & STATUS_BUSY))
  {
    return;
  }

  m_bIsOnProcess = TRUE;
  MoveLeft();
  m_bIsOnProcess = FALSE;

  m_bIsRight = FALSE;
}

void CLLC2LensChangerDlg::MoveLeft(void)
{
  serComm.Flush(PURGE_RXCLEAR);

  if(!(m_uiMotorStatus & STATUS_LEFT_TRIPPED))
  {
    serComm.SendData(TRANS_CMD_MOVE_LEFT, TRANS_CMD_MOVE_SIZE);
    readSize = serComm.ReadData(recvDataBuffer, RECV_DATA_MAX);

    if((recvDataBuffer[TRANS_DATA_START_POS] != TRANS_CMD_MOVE_LEFT[TRANS_DATA_START_POS]) ||
       (recvDataBuffer[RECV_CMD_MOVE_POS] != RECV_CMD_ACK))
    {
      m_bIsOnMotorMove = FALSE;
      AfxMessageBox(_T("Move Left Error!!!"));
      return;
    }
  }

  m_bIsOnMotorMove = TRUE;
}

void CLLC2LensChangerDlg::OnBnClickedCancel()
{
  // TODO: Add your control notification handler code here

  int Res;

  Res = AfxMessageBox(_T("Do you want to close this program?"), MB_YESNO);

  if(Res == IDNO)
  {
    return;
  }

  if(serComm.IsOpened())
  {
    serComm.Close();
  }

  OnCancel();
}

void CLLC2LensChangerDlg::OnOK()
{
  // TODO: Add your control notification handler code here
}

// Change Speed value
void CLLC2LensChangerDlg::SaveSpeed(void)
{
  m_nNofSteps_Flight = DEFAULT_STEPS_FLIGHT;

  m_dblAC_DC = (double)(m_uiSpeed * m_uiSpeed) * MOTOR_STEP / (2 * ROTARY_LENGTH * m_nNofSteps_Flight);

  if(m_dblAC_DC > 0)
  {
    m_nClock = (UINT)(HARD_CLOCK * sqrt(2 * ROTARY_LENGTH / (m_dblAC_DC * MOTOR_STEP)));
    if(m_nClock > CLOCK_END_VALUE)
    {
      m_nClock = CLOCK_END_VALUE;
    }
  }
  else
  {
    m_nClock = CLOCK_END_VALUE;
  }

//  m_uiTmpSpeed = m_uiSpeed;

// Write K; Clock
  transDataBuffer[0] = TRANS_CMD_CLOCK;
  DATA_Tokenizer(transDataBuffer, m_nClock, TRANS_KN_DATA_SIZE);
  transDataBuffer[TRANS_KN_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_CLOCK_SIZE); // 'Khhhh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_CLOCK_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_CLOCK_ACK_POS] != RECV_CMD_ACK)
  {
    CString cStr;
    cStr.Format(_T("Communication error with LLC2 Board. K = %d"), m_nClock);
    AfxMessageBox(cStr);
    return;
  }
}

void CLLC2LensChangerDlg::OnTimer(UINT nIDEvent)
{
  static BOOL IsMotorOff = FALSE;
  static UINT DisconnectCount = 0;

  if(!serComm.IsOpened())
  {
    return;
  }

  if(nIDEvent == MOTOR_STATUS_TIMER)
  {
    if(m_bIsOnProcess)
    {
      return;
    }

    serComm.Flush(PURGE_RXCLEAR);
    serComm.SendData(TRANS_CMD_STATUS, TRANS_CMD_STATUS_SIZE);
    readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_STATUS_SIZE);
    m_uiMotorStatus = CMD_Tokenizer(recvDataBuffer, readSize);

// If motor is busy, return
    if(!readSize)
    {
      if(!IsMotorOff)
      {
        if(DisconnectCount > DISCONNECT_COUNT_MAX)
        {
          IsMotorOff = TRUE;
          PortDisconnected();
          AfxMessageBox(_T("Communication Port is disconnected."));
        }
        DisconnectCount++;
      }
      return;
    }

    if(m_uiMotorStatus & STATUS_BUSY)
    {
      return;
    }

    IsMotorOff = FALSE;
    DisconnectCount = 0;

    switch(m_uiMotorStatus & CHECK_TRIPPED_DIRECTION)
    {
      case 0:
        m_ctlImg_Right.ShowWindow(SW_HIDE);
        m_ctlImg_Left.ShowWindow(SW_HIDE);

        if(m_bIsOnMotorMove)
        {
          BOOL IsRight = m_bIsRight;
          KillTimer(MOTOR_STATUS_TIMER);
          MoveScheme(IsRight);
          m_bIsOnMotorMove = FALSE;
          SetTimer(MOTOR_STATUS_TIMER, TIMER1_TIME, NULL);
        }

        break;

      case STATUS_LEFT_TRIPPED:
        m_ctlImg_Right.ShowWindow(SW_HIDE);
        m_ctlImg_Left.ShowWindow(SW_SHOW);
		G_SystemModeData.unLensIndex = LENS_INDEX_2X;
        break;

      case STATUS_RIGHT_TRIPPED:
        m_ctlImg_Right.ShowWindow(SW_SHOW);
        m_ctlImg_Left.ShowWindow(SW_HIDE);
		G_SystemModeData.unLensIndex = LENS_INDEX_20X;
        break;

      case CHECK_TRIPPED_DIRECTION:
        m_ctlImg_Right.ShowWindow(SW_HIDE);
        m_ctlImg_Left.ShowWindow(SW_HIDE);
		G_SystemModeData.unLensIndex = LENS_INDEX_ING;
        break;
    }

    m_bIsOnMotorMove = FALSE;
  }
}

void CLLC2LensChangerDlg::MoveScheme(BOOL IsRight)
{
  serComm.Flush(PURGE_RXCLEAR);
// Write K; Clock
  transDataBuffer[0] = TRANS_CMD_CLOCK;
  DATA_Tokenizer(transDataBuffer, MOVE_SCHEME_CLOCK, TRANS_KN_DATA_SIZE);
  transDataBuffer[TRANS_KN_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_CLOCK_SIZE); // 'Khhhh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_CLOCK_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_CLOCK_ACK_POS] != RECV_CMD_ACK)
  {
    m_bIsOnMotorMove = FALSE;

    CString cStr;
    cStr.Format(_T("Communication error with LLC2 Board. K = %d"), m_nClock);
    AfxMessageBox(cStr);
    return;
  }

#ifdef MOVE_SCHEME_W
// Write W; Number of Steps Flight
  m_nNofSteps_Flight = MOVE_SCHEME_STEPS_FLIGHT;
  transDataBuffer[0] = TRANS_CMD_NSTEPS_FLIGHT;
  DATA_Tokenizer(transDataBuffer, m_nNofSteps_Flight, TRANS_WP_DATA_SIZE);
  transDataBuffer[TRANS_WP_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_NSTEPS_FLIGHT_SIZE); // 'Whh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_NSTEPS_FLIGHT_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_NSTEPS_FLIGHT_ACK_POS] != RECV_CMD_ACK)
  {
    m_bIsOnMotorMove = FALSE;

    AfxMessageBox(_T("Communication error with LLC2 Board."));
    return;
  }
#endif

  if(IsRight)
  {
    serComm.SendData(TRANS_CMD_MOVE_RIGHT, TRANS_CMD_MOVE_SIZE);
  }
  else
  {
    serComm.SendData(TRANS_CMD_MOVE_LEFT, TRANS_CMD_MOVE_SIZE);
  }

  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_MOVE_SIZE);

// Write K; Clock
  transDataBuffer[0] = TRANS_CMD_CLOCK;
  DATA_Tokenizer(transDataBuffer, m_nClock, TRANS_KN_DATA_SIZE);
  transDataBuffer[TRANS_KN_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_CLOCK_SIZE); // 'Khhhh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_CLOCK_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_CLOCK_ACK_POS] != RECV_CMD_ACK)
  {
    m_bIsOnMotorMove = FALSE;

    CString cStr;
    cStr.Format(_T("Communication error with LLC2 Board. K = %d"), m_nClock);
    AfxMessageBox(cStr);
    return;
  }

#ifdef MOVE_SCHEME_W
// Write W; Number of Steps Flight
  m_nNofSteps_Flight = DEFAULT_STEPS_FLIGHT;
  transDataBuffer[0] = TRANS_CMD_NSTEPS_FLIGHT;
  DATA_Tokenizer(transDataBuffer, m_nNofSteps_Flight, TRANS_WP_DATA_SIZE);
  transDataBuffer[TRANS_WP_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_NSTEPS_FLIGHT_SIZE); // 'Whh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_NSTEPS_FLIGHT_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_NSTEPS_FLIGHT_ACK_POS] != RECV_CMD_ACK)
  {
    m_bIsOnMotorMove = FALSE;

    AfxMessageBox(_T("Communication error with LLC2 Board."));
    return;
  }
#endif
}

void CLLC2LensChangerDlg::OnDeltaposSpinSpeed(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  *pResult = 0;

  UINT tmpSpeed = m_uiSpeed;
  CString cStr;

  UpdateData();

  if(pNMUpDown->iDelta < 0)
  {
    if(tmpSpeed >= SPEED_MAX_VALUE)
    {
      cStr.Format(_T("Speed is between %d and %d.") , SPEED_MIN_VALUE, SPEED_MAX_VALUE);
      AfxMessageBox(cStr);
      return;
    }

    m_uiSpeed++;
  }
  else
  {
    if(tmpSpeed == SPEED_MIN_VALUE)
    {
      cStr.Format(_T("Speed is between %d and %d.") , SPEED_MIN_VALUE, SPEED_MAX_VALUE);
      AfxMessageBox(cStr);
      return;
    }

    m_uiSpeed--;
  }

  UpdateData(FALSE);
}

// This method will check Serial port connection.
BOOL CLLC2LensChangerDlg::ChkConnection(void)
{
  if(serComm.IsOpened() == FALSE)
  {
    AfxMessageBox(_T("Communication Port is not opened."));
    return FALSE;
  }

  serComm.Flush();

  transDataBuffer[0] = TRANS_CMD_ACK; // Send 'A'

  serComm.SendData(transDataBuffer, TRANS_CMD_ACK_SIZE); // 1 Bytes Data
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_ACK_SIZE);

  if(!readSize)
  {
    AfxMessageBox(_T("Cannot Communicate with LLC2 Board."));

    serComm.Close();
    m_cmbSettingPort.SetCurSel(0);

    return FALSE;
  }
  else if(recvDataBuffer[0] != RECV_CMD_RES_ACK)
  {
    transDataBuffer[0] = END_DELIMENATOR;
    serComm.SendData(transDataBuffer, TRANS_CMD_ACK_SIZE); // 1 Bytes Data
    readSize = serComm.ReadData(recvDataBuffer, RECV_DATA_MAX);

    transDataBuffer[0] = TRANS_CMD_ACK;
    serComm.SendData(transDataBuffer, TRANS_CMD_ACK_SIZE); // 1 Bytes Data
    readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_ACK_SIZE);
    if(recvDataBuffer[0] != RECV_CMD_RES_ACK)
    {
      AfxMessageBox(_T("Response is not correct."));

      serComm.Close();
      m_cmbSettingPort.SetCurSel(0);

      return FALSE;;
    }
  }

  return TRUE;
}

// This method will set default values Speed = 70 mm/s and Distance = 14000
void CLLC2LensChangerDlg::OnBnClickedButtonFactoryDefault()
{
  // TODO: Add your control notification handler code here

  BOOL IsConnected = ChkConnection();

  if(IsConnected == FALSE)
  {
    return;
  }

  UpdateData();
  m_bIsOnProcess = TRUE;

  FactoryDefault();

  m_dblAC_DC = (double)2*ROTARY_LENGTH/MOTOR_STEP * pow((double)HARD_CLOCK/m_nClock, 2);
  m_dblSpeed = sqrt(2 * ROTARY_LENGTH * m_nNofSteps_Flight * m_dblAC_DC / MOTOR_STEP);

  m_uiSpeed = (UINT)m_dblSpeed;

  m_uiDistance = m_nNofSteps;

  m_bIsOnProcess = FALSE;
  UpdateData(FALSE);
}

void CLLC2LensChangerDlg::FactoryDefault(void)
{
  serComm.Flush(PURGE_RXCLEAR);

// Write K; Clock
  m_nClock = DEFAULT_CLOCK;
  transDataBuffer[0] = TRANS_CMD_CLOCK;
  DATA_Tokenizer(transDataBuffer, m_nClock, TRANS_KN_DATA_SIZE);
  transDataBuffer[TRANS_KN_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_CLOCK_SIZE); // 'Khhhh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_CLOCK_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_CLOCK_ACK_POS] != RECV_CMD_ACK)
  {
    AfxMessageBox(_T("Communication error with LLC2 Board."));
    return;
  }

// Write W; Number of Steps Flight
  m_nNofSteps_Flight = DEFAULT_STEPS_FLIGHT;
  transDataBuffer[0] = TRANS_CMD_NSTEPS_FLIGHT;
  DATA_Tokenizer(transDataBuffer, m_nNofSteps_Flight, TRANS_WP_DATA_SIZE);
  transDataBuffer[TRANS_WP_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_NSTEPS_FLIGHT_SIZE); // 'Whh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_NSTEPS_FLIGHT_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_NSTEPS_FLIGHT_ACK_POS] != RECV_CMD_ACK)
  {
    AfxMessageBox(_T("Communication error with LLC2 Board."));
    return;
  }

// Write N; Number of Steps
  m_nNofSteps = DEFAULT_NOFSTEPS;
  transDataBuffer[0] = TRANS_CMD_NSTEPS;
  DATA_Tokenizer(transDataBuffer, m_nNofSteps, TRANS_KN_DATA_SIZE);
  transDataBuffer[TRANS_KN_DATA_SIZE+1] = END_DELIMENATOR;
  serComm.SendData(transDataBuffer, TRANS_CMD_WR_NSTEPS_SIZE); // 'Nhhhh;'
  readSize = serComm.ReadData(recvDataBuffer, RECV_CMD_WR_NSTEPS_SIZE);
  if(recvDataBuffer[RECV_CMD_WR_NSTEPS_ACK_POS] != RECV_CMD_ACK)
  {
    AfxMessageBox(_T("Communication error with LLC2 Board."));
    return;
  }
}

void CLLC2LensChangerDlg::OnEnChangeEditSpeed()
{
  // TODO:  If this is a RICHEDIT control, the control will not
  // send this notification unless you override the CDialog::OnInitDialog()
  // function and call CRichEditCtrl().SetEventMask()
  // with the ENM_CHANGE flag ORed into the mask.

  // TODO:  Add your control notification handler code here
  const int MAX_BUF_SIZE = 10;
  TCHAR buff[MAX_BUF_SIZE];

  m_ctlSpeed.GetWindowText(buff, MAX_BUF_SIZE);

  m_uiSpeed = (UINT)_ttoi(buff);

  if(m_uiSpeed > SPEED_MAX_VALUE)
  {
    CString cStr;

    cStr.Format(_T("Enter an integer between %d and %d.") , SPEED_MIN_VALUE, SPEED_MAX_VALUE);
    AfxMessageBox(cStr);

    m_ctlSpeed.SetFocus();

    UpdateData();
    m_uiSpeed = m_uiTmpSpeed;
    UpdateData(FALSE);
  }
  else
  {
    m_uiTmpSpeed = m_uiSpeed;
  }
}

void CLLC2LensChangerDlg::OnEnKillfocusEditSpeed()
{
  // TODO: Add your control notification handler code here
  if(ChkConnection() == FALSE)
  {
    return;
  }

  if(m_uiSpeed > SPEED_MAX_VALUE)
  {
    return;
  }

  m_bIsOnProcess = TRUE;
  SaveSpeed();
  m_bIsOnProcess = FALSE;
}

// Call LED Calibrate method
void CLLC2LensChangerDlg::OnBnClickedButtonCallLedCalibrate()
{
  // TODO: Add your control notification handler code here

//   if(serComm.IsOpened() == FALSE)
//   {
//     AfxMessageBox(_T("Communication Port is not opened."));
//     return;
//   }
// 
//   KillTimer(MOTOR_STATUS_TIMER);      // Check Motor Status regularily.
// 
//   CLLC2A_LED_ControlDlg LedDlg(serComm);
//   LedDlg.DoModal();
// 
//   SetTimer(MOTOR_STATUS_TIMER, TIMER1_TIME, NULL);      // Check Motor Status regularily.
}

void CLLC2LensChangerDlg::OnNcDestroy()
{
	CDialog::OnNcDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}
