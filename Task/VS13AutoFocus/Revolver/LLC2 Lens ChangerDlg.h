//*----------------------------------------------------------------------------
//*                WDI - Wise Device Inc, Feb 2011 
//*----------------------------------------------------------------------------
//* File Name    : LLC2 Lens ChangerDlg.h
//* Description  : Designed for controlling LLC2A device
//*                Header file
//*----------------------------------------------------------------------------
//* Version      : 700052A01
//* Date         : Feb 15, 2011
//*----------------------------------------------------------------------------*/

#pragma once
#include "afxwin.h"
#include "Serial.h"
#include "LLC2_Config.h"
//#include "LLC2A_LED_ControlDlg.h"
#define IDD_LLC2LENSCHANGER_DIALOG 102
#include "..\resource.h"

// #define IDC_PORT_LENS                   1000
// #define IDC_EDIT_SPEED                  1005
// #define IDC_IMG_RIGHT                   1013
// #define IDC_IMG_LEFT                    1014
// #define IDC_EDIT_SPEED                  1005
// #define IDC_FW_VER                      1019
// #define IDC_CPLD_VER                    1020
// #define IDC_SW_VER                      1021
// #define IDR_MAINFRAME                   128


// CLLC2LensChangerDlg dialog
class CLLC2LensChangerDlg : public CDialog
{
// Construction
public:
	CLLC2LensChangerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_LLC2LENSCHANGER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
  virtual void OnOK(); // override CDialog::OnOK

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

public:
  CSerial serComm;

  static const UINT RECV_DATA_MAX = 128;
  static const UINT TRANS_DATA_MAX = 128;
  char transDataBuffer[TRANS_DATA_MAX], recvDataBuffer[RECV_DATA_MAX];
  int readSize;

// Define Port Variables
  CUIntArray uiPorts;
  int m_nSettingPort;
  CComboBox m_cmbSettingPort;

// Define Baud-Rate Values
  DWORD m_nBaudRate;

// Define EditBox Variables and Controls
  UINT m_uiSpeed;
  UINT m_uiDistance;
  CEdit m_ctlSpeed;

  UINT m_uiTmpSpeed;

  double m_dblSpeed;
  double m_dblAC_DC;

// Define Other Variables
  UINT m_nClock;           // 'K'
  UINT m_nNofSteps_Flight; // 'W'
  UINT m_nNofSteps;        // 'N'

  BOOL m_bIsRight;
  BOOL m_bIsOnProcess;
  BOOL m_bIsOnMotorMove;
  UINT m_uiMotorStatus;

// Define Indicator Variables
  CStatic m_ctlImg_Right;
  CStatic m_ctlImg_Left;

	CString m_strFixedFileVersion;

// Define the variables for Software, Firmware and CPLD Version
  CString m_strFW_Version;
  CString m_strCPLD_Version;
  CString m_strSW_Version;

// Define the values for File Version Info
  VS_FIXEDFILEINFO	*m_fixedFileInfo;

// Define the function for reading Serial Ports
  BOOL SetPortRegistry(CUIntArray& uiPorts);
  BOOL SortPort(CComboBox& comboPort, CUIntArray& uiPorts);
  BOOL ChkConnection(void);
  void PortDisconnected(void);

// Define the function for File Version Infomation
  void GetVersionInfo (void);

// Define Library Functions
  void MoveRight(void);
  void MoveLeft(void);
  void SaveSpeed(void);
  void FactoryDefault(void);
  void MoveScheme(BOOL IsRight);
  UINT CMD_Tokenizer(char *recvDataBuffer, UINT size);
  void DATA_Tokenizer(char *transDataBuffer, UINT data, UINT size);
  UINT GetCPLD_Version(char recvDataBuffer[], char strCPLD[]);
  void ChangeDistance(UINT uiDistance);

// Define Dialog Control Functions
  afx_msg void OnCbnSelchangePort();
  afx_msg void OnBnClickedButtonHome();
  afx_msg void OnBnClickedButtonRight();
  afx_msg void OnBnClickedButtonLeft();
  afx_msg void OnBnClickedCancel();
  afx_msg void OnTimer(UINT nIDEvent);
  afx_msg void OnDeltaposSpinSpeed(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedButtonFactoryDefault();
  afx_msg void OnEnChangeEditSpeed();
  afx_msg void OnEnKillfocusEditSpeed();
  afx_msg void OnBnClickedButtonCallLedCalibrate();
  afx_msg void OnNcDestroy();
};
