//*----------------------------------------------------------------------------
//*                WDI - Wise Device Inc, Feb 2011 
//*----------------------------------------------------------------------------
//* File Name    : Serial.h
//* Description  : Serial Communication
//*----------------------------------------------------------------------------
//* Version      : 700052A01
//* Date         : Feb 15, 2011
//*----------------------------------------------------------------------------*/

#ifndef __SERIAL_H
#define __SERIAL_H

#define FC_DTRDSR       0x01
#define FC_RTSCTS       0x02
#define FC_XONXOFF      0x04
#define ASCII_BEL       0x07
#define ASCII_BS        0x08
#define ASCII_LF        0x0A
#define ASCII_CR        0x0D
#define ASCII_XON       0x11
#define ASCII_XOFF      0x13

class CSerial
{
public:
  CSerial();
  ~CSerial();

  BOOL Open(int nPort = 1, int nBaud = 9600);
  BOOL Close(void);

  int ReadData(void *, int);
  int SendData(const char *, int);
  int ReadDataWaiting(void);
  HRESULT Flush(DWORD dwFlag = PURGE_TXCLEAR | PURGE_RXCLEAR);

  BOOL IsOpened(void) { return (m_bOpened); }

protected:
  BOOL WriteCommByte(unsigned char);

  HANDLE m_hIDComDev;
  BOOL m_bOpened;
};

#endif // End of __SERIAL_H
