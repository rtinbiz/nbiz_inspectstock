//*----------------------------------------------------------------------------
//*                WDI - Wise Device Inc, Feb 2011 
//*----------------------------------------------------------------------------
//* File Name    : Serial.cpp
//* Description  : Serial Communication
//*----------------------------------------------------------------------------
//* Version      : 700052A01
//* Date         : Feb 15, 2011
//*----------------------------------------------------------------------------*/

#include "stdafx.h"
#include "Serial.h"

CSerial::CSerial()
{
  m_hIDComDev = NULL;
  m_bOpened = FALSE;
}

CSerial::~CSerial()
{

}

BOOL CSerial::Open( int nPort, int nBaud )
{
  if( m_bOpened ) return (TRUE);

  DWORD	dwError;
  CString cStr;
  DCB dcb;

  //Call CreateFile to open up the comms port
  CString sPort;
  sPort.Format(_T("\\\\.\\COM%d"), nPort);
  m_hIDComDev = CreateFile(sPort, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

  if( m_hIDComDev == NULL ) return (FALSE);

  m_bOpened = SetupComm(m_hIDComDev, 128, 128); // set buffer sizes
	if (!m_bOpened)
	{
		dwError = GetLastError();
//    cStr.Format(_T("SetupComm failed: Port=%s Error=%d"), sPort, dwError);
//		AfxMessageBox(cStr);

    CloseHandle(m_hIDComDev);
    return (FALSE);
	}

	m_bOpened = GetCommState(m_hIDComDev, &dcb);
	if(!m_bOpened)
	{
		dwError = GetLastError();
//    cStr.Format(_T("GetCommState failed: Port=%s Error=%d"), sPort, dwError);
//		AfxMessageBox(cStr);

    CloseHandle(m_hIDComDev);
    return (FALSE);
	}

  dcb.DCBlength = sizeof(DCB);
  dcb.BaudRate = nBaud;
  dcb.ByteSize = 8;
  dcb.Parity = NOPARITY;
  dcb.StopBits = ONESTOPBIT; // One stop bit
  dcb.fAbortOnError = TRUE;

  m_bOpened = SetCommState( m_hIDComDev, &dcb);
	if(!m_bOpened)
	{
		dwError = GetLastError();
//    cStr.Format(_T("SetCommState failed: Port=%s Error = %d"), sPort, dwError);
//		AfxMessageBox(cStr);

    CloseHandle(m_hIDComDev);
    return (FALSE);
	}

  COMMTIMEOUTS CommTimeOuts;

  m_bOpened = GetCommTimeouts (m_hIDComDev, &CommTimeOuts);
	if(!m_bOpened)
	{
		dwError = GetLastError();
//    cStr.Format(_T("GetCommTimeouts failed: Port=%s Error = %d"), sPort, dwError);
//		AfxMessageBox(cStr);

    CloseHandle(m_hIDComDev);
    return (FALSE);
	}
  
  CommTimeOuts.ReadIntervalTimeout = 50;
  CommTimeOuts.ReadTotalTimeoutMultiplier = 50;
  CommTimeOuts.ReadTotalTimeoutConstant = 10;
  CommTimeOuts.WriteTotalTimeoutMultiplier = 50;
  CommTimeOuts.WriteTotalTimeoutConstant = 10;

  m_bOpened = SetCommTimeouts( m_hIDComDev, &CommTimeOuts );
	if(!m_bOpened)
	{
		dwError = GetLastError();
//    cStr.Format(_T("SetCommTimeouts failed: Port=%s Error = %d"), sPort, dwError);
//		AfxMessageBox(cStr);

    CloseHandle(m_hIDComDev);
    return (FALSE);
	}

  m_bOpened = TRUE;

  return (m_bOpened);
}

BOOL CSerial::Close(void)
{
  if( !m_bOpened || m_hIDComDev == NULL ) return (TRUE);

  CloseHandle(m_hIDComDev);
  m_bOpened = FALSE;
  m_hIDComDev = NULL;

  return (TRUE);
}

int CSerial::SendData(const char *buffer, int size)
{
  if(!m_bOpened || (m_hIDComDev == NULL)) return (0);

  BOOL bWriteStat;
  DWORD dwBytesWritten = 0;

  bWriteStat = WriteFile(m_hIDComDev, buffer, size, &dwBytesWritten, NULL);
  if(!bWriteStat && (GetLastError() == ERROR_IO_PENDING))
  {
    dwBytesWritten = 0;
  }

  return ((int) dwBytesWritten);
}

int CSerial::ReadDataWaiting(void)
{
  if(!m_bOpened || (m_hIDComDev == NULL)) return (0);

  DWORD dwErrorFlags;
  COMSTAT ComStat;

  ClearCommError(m_hIDComDev, &dwErrorFlags, &ComStat);

  return ((int) ComStat.cbInQue);
}

int CSerial::ReadData(void *buffer, int limit)
{
  if(!m_bOpened || (m_hIDComDev == NULL)) return (0);

  BOOL bReadStatus = FALSE;
  DWORD dwBytesRead = limit;//, dwErrorFlags;
 // COMSTAT ComStat;

  bReadStatus = ReadFile(m_hIDComDev, buffer, dwBytesRead, &dwBytesRead, NULL);
  if(!bReadStatus)
  {
    if(GetLastError() == ERROR_IO_PENDING)
    {
      return ((int) dwBytesRead);
    }
    return (0);
  }

  return ((int) dwBytesRead);
}

HRESULT CSerial::Flush(DWORD dwFlag)
{
  if(PurgeComm(m_hIDComDev, dwFlag))
  {
    return S_OK;
  }
  else
  {
    return E_FAIL;
  }
}