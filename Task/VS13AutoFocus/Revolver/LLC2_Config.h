//*----------------------------------------------------------------------------
//*                WDI - Wise Device Inc, Feb 2011 
//*----------------------------------------------------------------------------
//* File Name    : LLC2.h
//* Description  : LLCA device configuration
//*----------------------------------------------------------------------------
//* Version      : 700052A01
//* Date         : Feb 15, 2011
//*----------------------------------------------------------------------------*/

#ifndef __LLC2_H_
#define __LLC2_H_

// Define Hardware
#define HARD_CLOCK      (3686400) // Clock / 2
#define MOTOR_STEP      (8 * 200) // Microstep = 8, Full Step = 200 fs * ms
#define ROTARY_LENGTH   (4)       // L

// Define Sleep Time
#define TEN_mSEC        (10)
#define THIRTY_mSEC     (TEN_mSEC * 3)
#define FIFTY_mSEC      (TEN_mSEC * 5)
#define SEVENTY_mSEC    (TEN_mSEC * 7)
#define HUNDRED_mSEC    (TEN_mSEC * 10)
#define THREE_HUND_mSEC (HUNDRED_mSEC * 3)
#define FIVE_HUND_mSEC  (HUNDRED_mSEC * 5)
#define SIX_HUND_mSEC   (HUNDRED_mSEC * 6)
#define SEVEN_HUND_mSEC (HUNDRED_mSEC * 7)
#define EIGHT_HUND_mSEC (HUNDRED_mSEC * 8)
#define NINE_HUND_mSEC  (HUNDRED_mSEC * 9)
#define ONE_SEC         (HUNDRED_mSEC * 10)
#define FIVE_SEC        (HUNDRED_mSEC * 50)
#define TEN_SEC         (HUNDRED_mSEC * 100)

// Define sleep time after send data to LLC2
#define SLEEP_TIME      (SEVENTY_mSEC)

// Define Timer call time
#define TIMER1_TIME     (FIFTY_mSEC)

#define SHIFT_ONE_NIBBLE    (4)
#define SHIFT_ONE_BYTE      (8)

// Define Communication Data Size
#define TRANS_CMD_ACK_SIZE                (1)
#define TRANS_CMD_ACK_RESET               (1)
#define TRANS_CMD_GET_FW_VER_SIZE         (1)
#define TRANS_CMD_GET_CPLD_VER_SIZE       (1)
#define TRANS_CMD_RD_CLOCK_SIZE           (2)
#define TRANS_CMD_RD_NSTEPS_FLIGHT_SIZE   (2)
#define TRANS_CMD_RD_NSTEPS_SIZE          (2)
#define TRANS_CMD_WR_CLOCK_SIZE           (6)
#define TRANS_CMD_WR_NSTEPS_FLIGHT_SIZE   (4)
#define TRANS_CMD_WR_NSTEPS_SIZE          (6)

#define RECV_CMD_ACK_SIZE                 (1) // 'X'
#define RECV_CMD_GET_FW_VER_SIZE          (9) // 
#define RECV_CMD_GET_CPLD_VER_SIZE        (4) // 
#define RECV_CMD_RD_CLOCK_SIZE            (7) // K;hhhh;
#define RECV_CMD_RD_NSTEPS_SIZE           (7) // N;hhhh;
#define RECV_CMD_WR_NSTEPS_SIZE           (8) // Nhhhh;A; or Nhhhh;N;
#define RECV_CMD_WR_CLOCK_SIZE            (8) // Khhhh;A; or Khhhh;N;
#define RECV_CMD_WR_NSTEPS_FLIGHT_SIZE    (6) // Whh;A; or Whh;N;
#define RECV_CMD_MOVE_SIZE                (5) // MF(R);A; or MF(R);N;
#define RECV_CMD_STATUS_SIZE              (5) // S;hh;

#define RECV_CMD_WR_CLOCK_ACK_POS         (6) // 'K'
#define RECV_CMD_WR_NSTEPS_FLIGHT_ACK_POS (4) // 'W'
#define RECV_CMD_WR_NSTEPS_ACK_POS        (6) // 'N'

#define TRANS_KN_DATA_SIZE                (4) // 'Khhhh;' 'Nhhhh;'
#define TRANS_WP_DATA_SIZE                (2) // 'Whh' 'Phh'

#define RECV_CMD_MOVE_POS                 (3)
#define RECV_DATA_START_POS               (2)

#define TRANS_DATA_START_POS              (1)

#define CPLD_FIRST_NUMBER                 ('7')
#define CPLD_NUMBER_MAX_SIZE              (20)
#define CPLD_BYTES                        (4)

// Define Transmit Data
#define TRANS_CMD_ACK           ('A')
#define TRANS_CMD_RESET         ('Z')

#define TRANS_CMD_CLOCK         ('K') // 1153 < K < 65536
#define TRANS_CMD_NSTEPS_FLIGHT ('W') // 255
#define TRANS_CMD_NSTEPS        ('N') // 0 <= N < 65536

#define CLOCK_START_VALUE       (1154)   // 'K'
#define CLOCK_END_VALUE         (65535)
#define NSTEPS_START_VALUE      (0)      // 'N'
#define NSTEPS_END_VALUE        (65535)

#define TRANS_CMD_STATUS        ("S;")
#define TRANS_CMD_STATUS_SIZE   (2)

#define TRANS_CMD_MOVE_RIGHT    ("MF;")
#define TRANS_CMD_MOVE_LEFT     ("MR;")
#define TRANS_CMD_MOVE_SIZE     (3)

#define TRANS_CMD_GET_FW_VER    (0xFA)
#define TRANS_CMD_GET_CPLD_VER  (0xFC)

#define END_DELIMENATOR         (';')

// Define Receive data
#define RECV_CMD_RES_ACK        ('X')
#define RECV_CMD_ACK            ('A')
#define RECV_CMD_NACK           ('N')

// Define Status Register
#define STATUS_LEFT_TRIPPED     (0x01)
#define STATUS_RIGHT_TRIPPED    (0x02)
#define STATUS_DIR_REVERSE      (0x04)
#define STATUS_BUSY             (0x80)

#define CHECK_TRIPPED_DIRECTION (STATUS_RIGHT_TRIPPED | STATUS_LEFT_TRIPPED)

#define DEFAULT_CLOCK           (4204) // 70 mm/s -> W:255 -> 4204 W:127 -> 2960
#define DEFAULT_STEPS_FLIGHT    (255)  // 'W'
#define DEFAULT_NOFSTEPS        (14000)
#define HOME_NOFSTEPS           (20000)

#define MOVE_SCHEME_W           (1)

#ifdef MOVE_SCHEME_W
#define MOVE_SCHEME_CLOCK       (1848)  // W:2 -> 1861 W:1 -> 1536 W:127 -> 17000 W:255 -> 24527
#else
#define MOVE_SCHEME_CLOCK       (17000) // W:1 -> 1536 W:127 -> 17000 W:255 -> 24527
#endif

#define MOVE_SCHEME_STEPS_FLIGHT (2)  // 'W'

#define DATA_SIZE               (4)

#define RIGHT_DIRECTION         (TRUE)
#define LEFT_DIRECTION          (FALSE)
#define MOTOR_STATUS_TIMER      (1)
#define MOTOR_STATUS_MOVE_TIMER (2)

#define SPEED_MAX_VALUE         (80)
#define SPEED_MIN_VALUE         (0)

#define DISCONNECT_COUNT_MAX    (5)

// LED Control - Illumination Commands
#define T_CMD_STATUS            (0x80)
#define T_CMD_TURN_ON           (0x81)
#define T_CMD_TURN_OFF          (0x82)
#define T_CMD_READ_CUR          (0x84)
#define T_CMD_READ_PWM          (0x88)
#define T_CMD_CALIBRATE         (0xC4)
#define T_CMD_SET_PWM           (0xC8)

// Define LED Status
#define HW_ERROR                (0x80 | 0x40 | 0x20)
#define LED_ON                  (0x10)
#define DEFECTIVE_LED           (0x08)
#define LED_CIR_OPEN            (0x04)
#define LED_OK                  (0x02)
#define LED_CIR_SHORT           (0x01)

#define LOW_BYTE                (0x0F)

// Define LED communication data size
#define T_CMD_STATUS_SIZE       (1)
#define T_CMD_TRUN_ON_SIZE      (1)
#define T_CMD_TRUN_OFF_SIZE     (1)
#define T_CMD_READ_CUR_SIZE     (1)
#define T_CMD_READ_PWM_SIZE     (1)
#define T_CMD_CALIBRATE_SIZE    (2)
#define T_CMD_SET_PWM_SIZE      (2)

#define R_CMD_READ_CUR_SIZE     (1)
#define R_CMD_READ_PWM_SIZE     (1)
#define R_CMD_STATUS_SIZE       (1)
#define R_CMD_SET_PWM_SIZE      (1)
#define R_CMD_CALIBRATE_SIZE    (1)

#define LED_CUR_MIN_VALUE       (50)
#define LED_CUR_MAX_VALUE       (800)

#define LED_PWM_MIN_VALUE       (0)
#define LED_PWM_MAX_VALUE       (100)

#define LED_CUR_BASE_UNIT       (10)

#endif
