
// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// Basler_CamLive.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


//////////////////////////////////////////////////////////////////////////
//2012.07.30 stshyoung Start
#include <TLHELP32.H>   //  CreateToolhelp32Snapshot 함수를 사용하기 위함 
BOOL KillProcess( char *ClassName, char *pstrProcessName)
{
	if(pstrProcessName == NULL)
		return TRUE;
	HWND hOsk=::FindWindow(ClassName, pstrProcessName);
	if(hOsk == NULL)
		return TRUE;

	HANDLE hProcess = NULL;
	DWORD dwProcessId = 0;
	DWORD dwExitCode =0;
	// 프로세스 아디를 가져온다.
	GetWindowThreadProcessId(hOsk,&dwProcessId);

	// 종료 조건으로 제어하기 위한 핸들을 가져 온다.
	if((hProcess = OpenProcess(PROCESS_TERMINATE,0,dwProcessId)) == 0) return FALSE;

	// 종료 조건 코드값을 가져온다.
	GetExitCodeProcess(hProcess,&dwExitCode);

	if(hProcess != NULL) 
	{
		TerminateProcess(hProcess,dwExitCode);
		CloseHandle (hProcess);
	}
	return TRUE;
}

BOOL KillProcess2(CString szProcessName)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 sEntry32;
	sEntry32.dwSize = sizeof(PROCESSENTRY32); // 이거 빠지면 검색 잘 안됨

	if((hSnapshot == INVALID_HANDLE_VALUE) || (hSnapshot == NULL)) return FALSE;

	BOOL bRemain = Process32First(hSnapshot, &sEntry32);
	while(bRemain)
	{
		CString szExeName;
		szExeName.Format(_T("%s"), sEntry32.szExeFile);

		if(szExeName.CompareNoCase(szProcessName) == 0) 
		{
			HANDLE process_handle = OpenProcess(PROCESS_TERMINATE, FALSE, sEntry32.th32ProcessID);
			if (INVALID_HANDLE_VALUE != process_handle)
			{
				TerminateProcess(process_handle, 0);
				CloseHandle(process_handle);
			}

			//CloseToolhelp32Snapshot(hSnapshot);
			CloseHandle(hSnapshot);
			return TRUE;
		}

		bRemain = Process32Next(hSnapshot, &sEntry32);
	}

	//CloseToolhelp32Snapshot(hSnapshot);
	CloseHandle(hSnapshot);

	return FALSE;
} 

BOOL FindProcess(CString szProcessName)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 sEntry32;
	sEntry32.dwSize = sizeof(PROCESSENTRY32); // 이거 빠지면 검색 잘 안됨

	if((hSnapshot == INVALID_HANDLE_VALUE) || (hSnapshot == NULL)) return FALSE;

	BOOL bRemain = Process32First(hSnapshot, &sEntry32);
	while(bRemain)
	{
		CString szExeName;
		szExeName.Format(_T("%s"), sEntry32.szExeFile);

		if(szExeName.CompareNoCase(szProcessName) == 0) 
		{
			//CloseToolhelp32Snapshot(hSnapshot);
			CloseHandle(hSnapshot);
			return TRUE;
		}

		bRemain = Process32Next(hSnapshot, &sEntry32);
	}

	//CloseToolhelp32Snapshot(hSnapshot);
	CloseHandle(hSnapshot);

	return FALSE;
} 

int FindProcessCnt(CString szProcessName)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 sEntry32;
	sEntry32.dwSize = sizeof(PROCESSENTRY32); // 이거 빠지면 검색 잘 안됨

	if((hSnapshot == INVALID_HANDLE_VALUE) || (hSnapshot == NULL)) return FALSE;

	BOOL bRemain = Process32First(hSnapshot, &sEntry32);

	int RetCnt = 0;
	while(bRemain)
	{
		CString szExeName;
		szExeName.Format(_T("%s"), sEntry32.szExeFile);

		if(szExeName.CompareNoCase(szProcessName) == 0) 
		{
			//CloseToolhelp32Snapshot(hSnapshot);
			RetCnt++;
		}

		bRemain = Process32Next(hSnapshot, &sEntry32);
	}

	//CloseToolhelp32Snapshot(hSnapshot);
	CloseHandle(hSnapshot);

	return RetCnt;
} 