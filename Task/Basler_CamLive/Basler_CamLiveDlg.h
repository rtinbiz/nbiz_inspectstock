
// Basler_CamLiveDlg.h : 헤더 파일
//

#pragma once
enum
{
	LIVE_MODE_NONE = 0,
	LIVE_MODE_VIEW = 1,
	LIVE_MODE_AF,
	LIVE_MODE_GRAB,
};
#pragma pack(1)
typedef struct SMP_LIVE_TAG
{
	char LiveImage[LIVE_IMG_SIZE];
	int LiveGrabCamsRun;
	int UpDateMode;
	int LiveCheck;
}SMP_LIVE;
#pragma pack()

typedef struct LiveThreadData_TAG
{
	bool RunThread;
	SMP_LIVE *pSMP;
	Camera_t *pCamera;
	CWinThread *pLiveThread;
	LiveThreadData_TAG()
	{
		RunThread = FALSE;
		pSMP = NULL;
		pCamera = NULL;
		pLiveThread = NULL;
	}
}LData;

// CBasler_CamLiveDlg 대화 상자
class CBasler_CamLiveDlg : public CDialogEx
{
// 생성입니다.
public:
	CBasler_CamLiveDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~CBasler_CamLiveDlg();
// 대화 상자 데이터입니다.
	enum { IDD = IDD_BASLER_CAMLIVE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);


public:
	HANDLE hLIMapping;
	SMP_LIVE * lpMapping;
	ITransportLayer *pTl;
	DeviceInfoList_t devices;

	Camera_t *pCamera;
	LData LiveData;
	void ConnectLiveCam();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedOk();

	// Automagically call PylonInitialize and PylonTerminate to ensure the pylon runtime system.
	// is initialized during the lifetime of this object
	Pylon::PylonAutoInitTerm autoInitTerm;
};
#ifndef SIMUL_LIVE_IMG
UINT thfn_LiveThread(LPVOID pParam);
#else
UINT thfn_ImgThread(LPVOID pParam);
#endif