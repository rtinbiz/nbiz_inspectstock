
// Basler_CamLiveDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "Basler_CamLive.h"
#include "Basler_CamLiveDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifndef SIMUL_LIVE_IMG
UINT thfn_LiveThread(LPVOID pParam)
{
	LData *pData = (LData *) pParam;
	SMP_LIVE *pSMP = pData->pSMP;
	Camera_t *pCam = pData->pCamera;
	try
	{
		// Open the camera
		pCam->Open();

		// Get the first stream grabber object of the selected camera
		Camera_t::StreamGrabber_t StreamGrabber(pCam->GetStreamGrabber(0));

		// Open the stream grabber
		StreamGrabber.Open();

		// Set the image format and AOI
		pCam->PixelFormat.SetValue(PixelFormat_Mono8);

		pCam->OffsetX.SetValue(0);
		pCam->OffsetY.SetValue(0);
		pCam->Width.SetValue(pCam->Width.GetMax());
		pCam->Height.SetValue(pCam->Height.GetMax());
		//pCam->OffsetX.SetValue(13);
		//pCam->OffsetY.SetValue(18);
		//pCam->Width.SetValue(1600);
		//pCam->Height.SetValue(1200);

		//pCam1 = cvCreateImage(cvSize((int)pCam->Width.GetMax(), (int)pCam->Height.GetMax()),IPL_DEPTH_8U , 1);
		//pCam2 = cvCreateImage(cvSize((int)Camera2.Width.GetMax(), (int)Camera2.Height.GetMax()),IPL_DEPTH_8U , 1);
		//Disable acquisition start trigger if available
		{
			GenApi::IEnumEntry* acquisitionStart = pCam->TriggerSelector.GetEntry( TriggerSelector_AcquisitionStart);
			if ( acquisitionStart && GenApi::IsAvailable( acquisitionStart))
			{
				pCam->TriggerSelector.SetValue( TriggerSelector_AcquisitionStart);
				pCam->TriggerMode.SetValue( TriggerMode_Off);
			}
		}

		//Disable frame start trigger if available
		{
			GenApi::IEnumEntry* frameStart = pCam->TriggerSelector.GetEntry( TriggerSelector_FrameStart);
			if ( frameStart && GenApi::IsAvailable( frameStart))
			{
				pCam->TriggerSelector.SetValue( TriggerSelector_FrameStart);
				pCam->TriggerMode.SetValue( TriggerMode_Off);
			}
		}

		//Set acquisition mode
		pCam->AcquisitionMode.SetValue(AcquisitionMode_SingleFrame);
		//Set exposure settings
		pCam->ExposureMode.SetValue(ExposureMode_Timed);
		pCam->ExposureTimeRaw.SetValue(30000);
		// Create an image buffer
		const size_t ImageSize = (size_t)(pCam->PayloadSize.GetValue());
 
		// We won't use image buffers greater than ImageSize
		StreamGrabber.MaxBufferSize.SetValue(ImageSize);
		// We won't queue more than one image buffer at a time
		StreamGrabber.MaxNumBuffer.SetValue(1);

		// Allocate all resources for grabbing. Critical parameters like image
		// size now must not be changed until FinishGrab() is called.
		StreamGrabber.PrepareGrab();
	

		const StreamBufferHandle hBuffer =  StreamGrabber.RegisterBuffer(pData->pSMP->LiveImage, ImageSize);
		
		// Put the buffer into the grab queue for grabbing
		StreamGrabber.QueueBuffer(hBuffer, NULL);

		// Grab 10 times
		const uint32_t numGrabs = 100;
		//IplImage *destimage = cvCreateImage(cvSize((int)pCam->Width.GetMax(),(int)pCam->Height.GetMax()),IPL_DEPTH_8U, 1);

		DWORD GrabTime1 = 0;
		pSMP->LiveCheck = 4;
		while(pData->RunThread == true && pSMP->LiveGrabCamsRun == 1)
		{
			//cout << "Cam1==========================================================" << endl;
			// Let the camera acquire one single image ( Acquisiton mode equals
			// SingleFrame! )
			if(pData->pSMP->LiveCheck == 4 && pSMP->UpDateMode > LIVE_MODE_NONE)
			{
				GrabTime1 = ::GetTickCount();
				pCam->AcquisitionStart.Execute();

				// Wait for the grabbed image with a timeout of 3 seconds
				if (StreamGrabber.GetWaitObject().Wait(3000))
				{
					// Get the grab result from the grabber's result queue
					GrabResult Result;
					StreamGrabber.RetrieveResult(Result);

					if (Result.Succeeded())
					{
						pSMP->LiveCheck = 1;

						//Result.GetImage()
						memcpy(pSMP->LiveImage, Result.Buffer(), ImageSize);
						//memcpy(destimage->imageData, pSMP->LiveImage, ImageSize);
						//cvSaveImage("d:\\th_gimg.bmp",destimage);


						GrabTime1 = ::GetTickCount() - GrabTime1;

						//if (n < numGrabs - 1)
							StreamGrabber.QueueBuffer(Result.Handle(), NULL);
						pSMP->LiveCheck = 2;

						if(pSMP->UpDateMode == LIVE_MODE_GRAB)
							pSMP->UpDateMode = LIVE_MODE_NONE;
					}
					else
					{
						break;
					}
				}
			}
			else
				Sleep(35);
		}
		// Clean up

		// You must deregister the buffers before freeing the memory
		StreamGrabber.DeregisterBuffer(hBuffer);

		// Free all resources used for grabbing
		StreamGrabber.FinishGrab();

		// Close stream grabber
		StreamGrabber.Close();

		// Close camera
		pCam->Close();
	}
    catch (GenICam::GenericException &e)
    {
		CString strCatchMsg;
        // Error handling
        strCatchMsg.Format( "%s", e.GetDescription());
		pData->pLiveThread = NULL;
		return 0;
    }
	pData->pLiveThread = NULL;

	return 0;
}
#else
UINT thfn_ImgThread(LPVOID pParam)
{
	LData *pData = (LData *) pParam;
	SMP_LIVE *pSMP = pData->pSMP;

	IplImage *pLoadTestImage1 = nullptr;
	pLoadTestImage1 = cvLoadImage("D:\\nBiz_InspectStock\\Data\\Resource\\VS10Master\\TAlignTest.bmp", CV_LOAD_IMAGE_GRAYSCALE);

	DWORD GrabTime1 = 0;
	pSMP->LiveCheck = 4;

	int TestCnt = 0;
	while(pData->RunThread == true && pSMP->LiveGrabCamsRun == 1)
	{
		if(pSMP->LiveCheck == 4 && pSMP->UpDateMode> LIVE_MODE_NONE)
		{
			GrabTime1 = ::GetTickCount();
			pSMP->LiveCheck= 1;

			memcpy(pSMP->LiveImage, pLoadTestImage1->imageData, LIVE_IMG_SIZE);

			GrabTime1 = ::GetTickCount() - GrabTime1;

			pSMP->LiveCheck = 2;
	
			if(pSMP->UpDateMode == LIVE_MODE_GRAB)
				pSMP->UpDateMode= LIVE_MODE_NONE;
		}
		//Sleep(33);//30fps
		Sleep(60);//15fps
	}
	cvReleaseImage(&pLoadTestImage1);
	pData->pLiveThread = NULL;

	return 0;
}
#endif
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CBasler_CamLiveDlg 대화 상자




CBasler_CamLiveDlg::CBasler_CamLiveDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBasler_CamLiveDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	pCamera = NULL;
	hLIMapping = NULL;
}
CBasler_CamLiveDlg::~CBasler_CamLiveDlg()
{
	LiveData.RunThread = FALSE;
	while(LiveData.pLiveThread != NULL)
	{
		Sleep(10);
	}
	LiveData.RunThread = FALSE;

	if(pCamera != NULL)
		delete pCamera;


	CloseHandle(hLIMapping);
}
void CBasler_CamLiveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CBasler_CamLiveDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK, &CBasler_CamLiveDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CBasler_CamLiveDlg 메시지 처리기

BOOL CBasler_CamLiveDlg::OnInitDialog()
{
	//HWND hWnd = NULL;
	//hWnd = ::FindWindowA(NULL, "Basler_CamLive");
	//if(hWnd != NULL)
	//{
	//	::SendMessageA(this->m_hWnd, WM_CLOSE, 0 , 0); 
	//	return FALSE;
	//}

	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.
//#ifndef SIMUL_LIVE_IMG
//	while(TRUE)
//	{
//		if(FindProcessCnt("IpConfigurator.exe")>=2)
//		{
//			KillProcess2("IpConfigurator.exe");
//			KillProcess2("IpConfigurator.exe");
//			break;
//		}
//		Sleep(10);
//	}
//#endif


	int MapImageSize = (LIVE_IMG_SIZE);
	int MapSize = sizeof(SMP_LIVE);
		
	hLIMapping = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, MapSize, CAM_LIVE_IMG_MAP_NAME); 
	lpMapping = (SMP_LIVE*)MapViewOfFile(hLIMapping, FILE_MAP_ALL_ACCESS, 	0, 	0, 	0);

	lpMapping->LiveGrabCamsRun = 1;
	
	// Create the camera object of the first available camera.
	// The camera object is used to set and get all available
	// camera features.
	ConnectLiveCam();
	::SetTimer(this->m_hWnd, UI_LIVE_CONNECT, 3000, 0);//InitDlg

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

//"netsh interface set interface Camera1 ENABLE"
//"netsh interface set interface Camera1 DISABLED"
//"netsh interface set interface Camera2 ENABLE"
//"netsh interface set interface Camera2 DISABLED"
void CBasler_CamLiveDlg::ConnectLiveCam()
{
	if(LiveData.pLiveThread !=nullptr)
	{
		if(this->IsWindowVisible() == TRUE)
			this->ShowWindow(SW_HIDE);
		return;
	}
	if(this->IsWindowVisible() == FALSE)
		this->ShowWindow(SW_SHOW);

	if(lpMapping->LiveGrabCamsRun == 0)
		lpMapping->LiveGrabCamsRun = 1;

#ifndef SIMUL_LIVE_IMG

	// Automagically call PylonInitialize and PylonTerminate to ensure the pylon runtime system.
	// is initialized during the lifetime of this object
	//Pylon::PylonAutoInitTerm autoInitTerm;

	//////////////////////////////////////////////////////
    // Get the transport layer factory
    CTlFactory& TlFactory = CTlFactory::GetInstance();
    // Create the transport layer object needed to enumerate or
    // create a camera object of type Camera_t::DeviceClass()
	pTl = TlFactory.CreateTl(Camera_t::DeviceClass());

    // Exit the application if the specific transport layer is not available
    if (! pTl)
        return;
	// Get all attached cameras and exit the application if no camera is found
	if (0 == pTl->EnumerateDevices(devices))
		return;
#endif
#ifndef SIMUL_LIVE_IMG
	int CamCount =devices.size();
#else
#endif
	LiveData.RunThread = false;
	while(LiveData.pLiveThread != nullptr)
		Sleep(10);
	if(pCamera != nullptr)
		delete pCamera;
	pCamera = nullptr;

	CString strSerialNumber;
	if(LiveData.pLiveThread ==nullptr)
	{
		//////////////////////////////////////////////////////
#ifndef SIMUL_LIVE_IMG
		if(pCamera != NULL)
			delete pCamera;
		try
		{
			pCamera = new Camera_t(pTl->CreateDevice(devices[0]));
		}
		catch (CException* e)
		{
			e = nullptr;
			return;
		}

#else
#endif
		LiveData.pCamera = pCamera;
		LiveData.pSMP = lpMapping;
		LiveData.RunThread = TRUE;
#ifndef SIMUL_LIVE_IMG
		LiveData.pLiveThread = ::AfxBeginThread(thfn_LiveThread, &LiveData, THREAD_PRIORITY_HIGHEST);
#else
		LiveData.pLiveThread = ::AfxBeginThread(thfn_ImgThread, &LiveData, THREAD_PRIORITY_HIGHEST);
#endif
	}
	
}
void CBasler_CamLiveDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CBasler_CamLiveDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CBasler_CamLiveDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



BOOL CBasler_CamLiveDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CBasler_CamLiveDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == UI_LIVE_CONNECT)
	{
		ConnectLiveCam();
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CBasler_CamLiveDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnOK();
}
