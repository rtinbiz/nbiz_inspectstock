/*
* Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
*
* Please refer to the NVIDIA end user license agreement (EULA) associated
* with this source code for terms and conditions that govern your use of
* this software. Any use, reproduction, disclosure, or distribution of
* this software and related documentation outside the terms of the EULA
* is strictly prohibited.
*
*/

#ifndef SHR_QATEST_H
#define SHR_QATEST_H

// *********************************************************************
// Generic utilities for NVIDIA GPU Computing SDK 
// *********************************************************************

// reminders for output window and build log
#ifdef _WIN32
    #pragma message ("Note: including windows.h")
    #pragma message ("Note: including math.h")
    #pragma message ("Note: including assert.h")
#endif

// OS dependent includes
#ifdef _WIN32
    // Headers needed for Windows
    #include <windows.h>
#else
    // Headers needed for Linux
    #include <sys/stat.h>
    #include <sys/types.h>
    #include <sys/time.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <stdarg.h>
#endif

#ifndef STRCASECMP
#ifdef _WIN32
#define STRCASECMP _stricmp
#else
#define STRCASECMP strcasecmp
#endif
#endif

#ifndef STRNCASECMP
#ifdef _WIN32
#define STRNCASECMP _strnicmp
#else
#define STRNCASECMP strncasecmp
#endif
#endif


// Standardized QA Start/Finish for CUDA SDK tests
#define shrQAStart(a, b)      __shrQAStart(a, b)
#define shrQAFinish(a, b, c)  __shrQAFinish(a, b, c)
#define shrQAFinish2(a, b, c) __shrQAFinish2(a, b, c)

inline void __shrQAStart(int argc, char **argv)
{
    bool bQATest = false;
    for (int i=1; i < argc; i++) {
        int string_start = 0;
        while (argv[i][string_start] == '-')
           string_start++;
        char *string_argv = &argv[i][string_start];

		if (!STRCASECMP(string_argv, "qatest")) {
           bQATest = true;
        }
    }
    if (bQATest) {
        printf("&&&& RUNNING %s", argv[0]);
        for (int i=1; i < argc; i++) printf(" %s", argv[i]);
        printf("\n");
    } else {
        printf("[%s] starting...\n", argv[0]);
    }
}

enum eQAstatus {
    QA_FAILED = 0,
    QA_PASSED = 1,
    QA_WAIVED = 2
};


inline void __shrQAFinish(int argc, const char **argv, int iStatus)
{
    bool bQATest = false, bNoPrompt = false;
    const char *sStatus[] = { "FAILED", "PASSED", "WAIVED", NULL };
	
    for (int i=1; i < argc; i++) {
        int string_start = 0;
            while (argv[i][string_start] == '-')
                string_start++;
            const char *string_argv = &argv[i][string_start];

	    if (!STRCASECMP(string_argv, "qatest")) {
           bQATest = true;
        }	
        if (!STRCASECMP(string_argv, "noprompt")) {
           bNoPrompt = true;
        }	
    }

    if (bQATest) {
        printf("&&&& %s %s", sStatus[iStatus], argv[0]);
        for (int i=1; i < argc; i++) printf(" %s", argv[i]);
        printf("\n");
    } else {
        printf("[%s] test results...\n%s\n", argv[0], sStatus[iStatus]);
    }
    if (!bNoPrompt) {
        printf("\nPress ENTER to exit...\n");
        fflush( stdout);
        fflush( stderr);
        getchar();
    }
}

inline void __shrQAFinish2(bool bQAtest, const char *appName, int iStatus)
{
    const char *sStatus[] = { "FAILED", "PASSED", "WAIVED", NULL };

    if (bQAtest) {
        printf("&&&& %s %s\n", sStatus[iStatus], appName);
    } else {
        printf("[%s] test results...\n%s\n", appName, sStatus[iStatus]);
    }
}

inline void shrQAFinishExit(int argc, const char **argv, int iStatus)
{
    __shrQAFinish(argc, argv, iStatus);

    exit(iStatus ? EXIT_SUCCESS : EXIT_FAILURE); 
}

inline void shrQAFinishExit2(bool bQAtest, const char *appName, int iStatus)
{
    __shrQAFinish2(bQAtest, appName, iStatus);

    exit(iStatus ? EXIT_SUCCESS : EXIT_FAILURE);
}

#endif
