
#ifndef		ANAIMAOI_INTERFACE_H
#define		ANAIMAOI_INTERFACE_H

#define  AOI_DLL_PROCESS_CNT_MAX	4
#define  AOI_DLL_PROCESS_CNT		4

#define  AOI_DLLPROCESS_STATE_NONE	0
#define  AOI_DLLPROCESS_STATE_RUN	1
#define  AOI_DLLPROCESS_STATE_IDEL	2
/** 
@brief		Glass 최소 반복 패턴 단위 이미지 정보.

@remark
-			Mask 이미지의 정보를 갖는다.
-			Auto Repair 에 사용된다.
@author		고정진
@date		2007.10.01
*/ 
struct SMASK_IMAGEINFO
{	
	int		nScaleX;
	int		nScaleY;						///마스크 이미지상 픽셀 의 크기	
	int		nErrorRange;					///구간 별 허용치.	
	int		nOutLineThick;					///제거할 테두리 두께.
	int		nBlockCount;
	int		nThreadCount;
	int		nMinimumSizeX;					///최소 검출 단위
	int		nMinimumSizeY;					///최소 검출 단위
	char	chCudaResultPath[MAX_PATH];		///쿠다 결과 파일 생성 위치
	SMASK_IMAGEINFO()
	{
		memset(this, 0x00, sizeof(SMASK_IMAGEINFO));
	}
};


/** 
@brief		공용 파라미터.

@remark
-				
@author		고정진
@date		2007.10.01
*/ 
struct SCOMMON_PARA
{
	int		nCD_SlitSize;
	BOOL	bCD_SizeCheck;
	int		nCD_PassLine;			///퍼센티지 1-100 사이의 값을 가진다.	 Default 33%
	int		nCD_PassHighLimit;		///<하이리밑 이상이면 패스한다.	nCD_PassLine 보다 크고 300 이하	Default 200%
	int		nCD_IgnoreOut;			///CD Defect 검출시 상,하 테두리 제거  Default 25
	int		nCD_PosLimit;			///최소 검출 위치값 Default 100
	int		nCD_2Channel_Boundary;  ///경제면의 기준으로 사용할 값 Default 120    50 - 200
	int		nDefectDistance;		///동일 결함으로 간주할 거리.			Default 100      10 보다 크고 300 이하
	int		nPixelPerLengthX;		///기본값5 고정, 3.5 마이크로
	int		nPixelPerLengthY;		///기본값5 고정, 3.5	
	BOOL	bImageLogUsed;			///<이미지 로그 생성 유무					true or false
	SCOMMON_PARA()
	{
		memset(this, 0x00, sizeof(SCOMMON_PARA));
	}
};


static	const enum	ERROR_CODE{	ERR_SOURCE_FILE = 1001,
								ERR_ALIGN_FILE,
								ERR_MASK_FILE,
								ERR_THREAD_COUNT,
								ERR_THREAD_NOT_CREATE};


static const int    DEFECT_SEARCH_MODE = 0;

static const int    DOGBON_SEARCH_MODE = 1;

static const int	DEFAULT_THREAD_COUNT	= 2;

//static const int	OKAY					= 1;

extern "C" __declspec(dllimport) int  _AOI_Static_Memory_Create();
extern "C" __declspec(dllimport) int  _AOI_Static_Memory_Delete();

extern "C" __declspec(dllimport) int  _AOI_Defect_Inspection_Start( int nThreadMode = DEFECT_SEARCH_MODE,
																    int nThread = DEFAULT_THREAD_COUNT, 
																    UINT *pProcStateArea = NULL);

extern "C" __declspec(dllimport) int  _AOI_SearchParameterSet( SMASK_IMAGEINFO	sMaskImageInfo,
															   SCOMMON_PARA		sCommonPara);

extern "C" __declspec(dllimport)  void  _AOI_PushItem( InspectDataQ* cInspectDataQ );

//Single Only
extern "C" __declspec(dllimport) int*  _AOI_DefectSearch( SMASK_IMAGEINFO	sMaskImageInfo,
														  SCOMMON_PARA		sCommonPara,
														  BYTE*				byImageSrc);
extern "C" __declspec(dllimport) int*  _AOI_DefectSearchCUDA( SMASK_IMAGEINFO	sMaskImageInfo,
														 SCOMMON_PARA		sCommonPara,
														 BYTE*				byImageSrc);


#endif