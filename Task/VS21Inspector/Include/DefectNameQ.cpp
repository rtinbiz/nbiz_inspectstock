// DefectNmaeQ.cpp: implementation of the CDefectNmaeQ class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DefectNameQ.h"

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

InspectDataQ::InspectDataQ(IMAGE_GRAB_INFO * pInsertImageInfo, fnCreateDefBuf CreatBufFn)
{
	InitializeCriticalSection(&m_CrtSection);
	
	EnterCriticalSection(&m_CrtSection);
	m_CountDefectNode			= 0 ;	//입력된 Defect Node Count.
	m_DefecPopIndex				= 0 ;
	m_pGrabImage				= NULL;	//Grab된 Image.	
	m_EndDefectCount			= END_DEF_NOT_INPUT;

	m_gfnCreateBuff				= CreatBufFn;
	m_pDefDataBuf				= NULL;
	if(pInsertImageInfo != NULL)
	{
		m_ScanLine				= pInsertImageInfo->ScanLine;
		m_ScanImageIndex		= pInsertImageInfo->ScanImageIndex;
		//m_pGrabImage			= new BYTE [pInsertImageInfo->W*pInsertImageInfo->H];
		m_pGrabImage			= cvCreateImage( cvSize(pInsertImageInfo->W, pInsertImageInfo->H), IPL_DEPTH_8U, 1);
		//if(m_pGrabImage != NULL)//Speed Up.(조건문 제거)
		{
			//memcpy(m_pGrabImage, pInsertImageInfo->pInsertImage, (sizeof(BYTE)*(pInsertImageInfo->W*pInsertImageInfo->H)));
			memcpy(m_pGrabImage->imageData, pInsertImageInfo->pInsertImage, (sizeof(BYTE)*(pInsertImageInfo->W*pInsertImageInfo->H)));
		}
	}
	LeaveCriticalSection(&m_CrtSection);
}
InspectDataQ::~InspectDataQ()
{
	//if(m_pGrabImage != NULL)
	//	delete [] m_pGrabImage;
	//m_pGrabImage = NULL;
	if(m_pDefDataBuf != NULL)
		delete [] m_pDefDataBuf;
	m_pDefDataBuf = NULL;

	DeleteCriticalSection(&m_CrtSection);
}

void InspectDataQ::SetStaticMemData(IMAGE_GRAB_INFO * pInsertImageInfo, fnCreateDefBuf CreatBufFn)
{
	InitializeCriticalSection(&m_CrtSection);

	EnterCriticalSection(&m_CrtSection);
	m_CountDefectNode			= 0 ;	//입력된 Defect Node Count.
	m_DefecPopIndex				= 0 ;
	m_EndDefectCount			= END_DEF_NOT_INPUT;

	m_gfnCreateBuff				= CreatBufFn;
	m_pDefDataBuf				= NULL;
	if(pInsertImageInfo != NULL)
	{
		m_ScanLine				= pInsertImageInfo->ScanLine;
		m_ScanImageIndex		= pInsertImageInfo->ScanImageIndex;
		memcpy(m_pGrabImage->imageData, pInsertImageInfo->pInsertImage, (pInsertImageInfo->W*pInsertImageInfo->H));
	}
	LeaveCriticalSection(&m_CrtSection);
}
BYTE * InspectDataQ::QGetGrabImage()
{
	//return m_pGrabImage;
	return (BYTE*)m_pGrabImage->imageData;
}
void InspectDataQ::QDeleteGrabImage()
{
	if(m_pGrabImage != NULL)
		//delete [] m_pGrabImage;
		cvReleaseImage(&m_pGrabImage);
	m_pGrabImage = NULL;
}

BOOL InspectDataQ::QSetDefectBuffer(int DefectNodeCount)
{
	EnterCriticalSection(&m_CrtSection);
	if(m_gfnCreateBuff != NULL && DefectNodeCount>0)
	{
		m_pDefDataBuf = m_gfnCreateBuff(DefectNodeCount); 
	}
	else
	{
		m_pDefDataBuf = NULL;
		LeaveCriticalSection(&m_CrtSection);
		return FALSE;
	}
	LeaveCriticalSection(&m_CrtSection);
	return TRUE;
}
BOOL InspectDataQ::QSetDefectAllBuffer(int DefectNodeCount,DEFECT_DATA *pNewData)
{
	EnterCriticalSection(&m_CrtSection);
	if(m_gfnCreateBuff != NULL && DefectNodeCount>0)
	{
		m_pDefDataBuf = m_gfnCreateBuff(DefectNodeCount); 
		memcpy(m_pDefDataBuf, pNewData, sizeof(DEFECT_DATA)*DefectNodeCount);
		m_EndDefectCount = m_CountDefectNode = DefectNodeCount;
	}
	else
	{
		m_pDefDataBuf = NULL;
		m_EndDefectCount = m_CountDefectNode = DefectNodeCount;
		LeaveCriticalSection(&m_CrtSection);
		return FALSE;
	}
	LeaveCriticalSection(&m_CrtSection);
	return TRUE;
}

BOOL InspectDataQ::QPutDefectNode(DEFECT_DATA *pNewData)//검출된 Defect Data를 저장 한다.(내부에서 Copy한다)
{
	EnterCriticalSection(&m_CrtSection);
	if(m_CountDefectNode< DEFECT_BUFFER_MAX)
	{
		memcpy(&m_pDefDataBuf[m_CountDefectNode], pNewData,sizeof(DEFECT_DATA));
		m_CountDefectNode++;
	}
	
	LeaveCriticalSection(&m_CrtSection);
	return TRUE;
}

//BOOL InspectDataQ::QPutDefectAllNode(int nDefectCnt, DEFECT_DATA *pNewData)//검출된 Defect Data를 저장 한다.(내부에서 Copy한다)
//{
//	EnterCriticalSection(&m_CrtSection);
//	if(nDefectCnt<= DEFECT_BUFFER_MAX)
//	{
//		memcpy(&m_pDefDataBuf, pNewData, sizeof(DEFECT_DATA)*nDefectCnt);
//		m_EndDefectCount = m_CountDefectNode = nDefectCnt ;
//	}
//
//	LeaveCriticalSection(&m_CrtSection);
//	return TRUE;
//}

void InspectDataQ::QPutEndDefectNode(int DefectNodeCount)//검출 종료를 알리는 Node를 추가 한다.
{
	EnterCriticalSection(&m_CrtSection);
	m_EndDefectCount = DefectNodeCount;
	LeaveCriticalSection(&m_CrtSection);
}
BOOL InspectDataQ::QCeckDefectEndNode()//UINT DefectNodeCount)
{	
	EnterCriticalSection(&m_CrtSection); //2010.04.07 GJJ C
	if(m_EndDefectCount == END_DEF_NOT_INPUT)
	{
		LeaveCriticalSection(&m_CrtSection);
		return FALSE;
	}
	m_DefecPopIndex = 0;
	LeaveCriticalSection(&m_CrtSection);
	return TRUE;

	
}
BOOL InspectDataQ::QGetDefectNode(DEFECT_DATA** pDefectNodeBuffer)
{
	EnterCriticalSection(&m_CrtSection);
	*pDefectNodeBuffer = m_pDefDataBuf;
	if(m_EndDefectCount == 0 || m_pDefDataBuf == NULL)
	{
		LeaveCriticalSection(&m_CrtSection);
		return FALSE;
	}

	LeaveCriticalSection(&m_CrtSection);
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
AOI_ImageCtrlQ::AOI_ImageCtrlQ()
:m_HeadNode(NULL), m_TailNode(NULL) 
{
	m_HeadNode.m_pFrontNode		= NULL;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= NULL;

	m_ImageGrabBufCnt			= 0;

	pStaticMemList				= NULL;

	InitializeCriticalSection(&m_CrtSection);
}
AOI_ImageCtrlQ::~AOI_ImageCtrlQ()
{
	QCleanInspectNode();
	DeleteCriticalSection(&m_CrtSection);

	if(pStaticMemList != NULL)
	{
		for(int i= 0; i<m_StaiceMemCnt; i++)
		{
			cvReleaseImage(&pStaticMemList[i].m_pGrabImage);
		}
		delete [] pStaticMemList;
	}
}

//Image Memory를 미리 Loading한다.
BOOL AOI_ImageCtrlQ::QCreateStaticMemBlock(int ImageCount, int ImageW, int ImageH)
{
	EnterCriticalSection(&m_CrtSection);
	pStaticMemList  = new InspectDataQ[ImageCount];
	for(int i= 0; i<ImageCount; i++)
	{
		pStaticMemList[i].m_pGrabImage	= cvCreateImage( cvSize(ImageW, ImageH), IPL_DEPTH_8U, 1);
		memset(pStaticMemList[i].m_pGrabImage->imageData, 0x00, (ImageW * ImageH));
	}

	m_StaiceMemCnt	= ImageCount;
	m_InputNode		= 0;
	m_OutputNode	= 0;

	LeaveCriticalSection(&m_CrtSection);
	return TRUE;
}
InspectDataQ* AOI_ImageCtrlQ::QPutStaticMemNode(IMAGE_GRAB_INFO * pInsertImageInfo, fnCreateDefBuf CreatBufFn)	//새로운 Inspect Node를 생성하고 Pointer를 반환 한다.
{
	EnterCriticalSection(&m_CrtSection);

	//if(pInsertImageInfo == NULL)
	//{
	//	LeaveCriticalSection(&m_CrtSection);
	//	return NULL;
	//}
	if(m_InputNode == m_StaiceMemCnt)
		m_InputNode = 0;

	pStaticMemList[m_InputNode].SetStaticMemData(pInsertImageInfo, CreatBufFn);
	
	m_ImageGrabBufCnt++;

	LeaveCriticalSection(&m_CrtSection);
	return &pStaticMemList[m_InputNode++];
}
InspectDataQ* AOI_ImageCtrlQ::QGetStaticInspectNode(void)						//첫번째 Inspect Node를 반환 한다. (Thread에서 Defect Node End를 체크 처리 한다.)
{
	EnterCriticalSection(&m_CrtSection);
	if(m_OutputNode == m_StaiceMemCnt)
		m_OutputNode = 0;
	LeaveCriticalSection(&m_CrtSection);
	return &pStaticMemList[m_OutputNode++];;
}
InspectDataQ* AOI_ImageCtrlQ::QPutInspectNode(IMAGE_GRAB_INFO * pInsertImageInfo, fnCreateDefBuf CreatBufFn)	//새로운 Inspect Node를 생성하고 Pointer를 반환 한다.
{
	EnterCriticalSection(&m_CrtSection);

	//if(pInsertImageInfo == NULL)
	//{
	//	LeaveCriticalSection(&m_CrtSection);
	//	return NULL;
	//}
	InspectDataQ *pNewNode = new InspectDataQ(pInsertImageInfo, CreatBufFn);

	//if( pNewNode == NULL)
	//{
	//	LeaveCriticalSection(&m_CrtSection);
	//	return NULL;
	//}

	//if(pNewNode->QGetGrabImage() == NULL)
	//{
	//	delete pNewNode;
	//	LeaveCriticalSection(&m_CrtSection);
	//	return NULL;
	//}

	//Data Link
	pNewNode->m_pFrontNode					= &m_HeadNode;
	pNewNode->m_pNextNode					= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode					= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;
	m_ImageGrabBufCnt++;

	LeaveCriticalSection(&m_CrtSection);
	return pNewNode;
}


InspectDataQ* AOI_ImageCtrlQ::QGetInspectNode(void)						//첫번째 Inspect Node를 반환 한다. (Thread에서 Defect Node End를 체크 처리 한다.)
{
	EnterCriticalSection(&m_CrtSection);

	InspectDataQ *pReturnNode = NULL;
	pReturnNode = m_TailNode.m_pFrontNode;
	//전송 Load가 있는지 확인.
	if(pReturnNode ==  &m_HeadNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return NULL;
	}
	LeaveCriticalSection(&m_CrtSection);
	return pReturnNode;
}
BOOL AOI_ImageCtrlQ::QPopInspectNode(void)
{
	EnterCriticalSection(&m_CrtSection);

	InspectDataQ *pReturnNode;
	pReturnNode = m_TailNode.m_pFrontNode;
	//전송 Load가 있는지 확인.
	if(pReturnNode ==  &m_HeadNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return FALSE;
	}
	//Link 제거.
	m_TailNode.m_pFrontNode = pReturnNode->m_pFrontNode;
	(pReturnNode->m_pFrontNode)->m_pNextNode = &m_TailNode;
	delete pReturnNode;//Defect Data만 제거된다.(Link Object)
	m_ImageGrabBufCnt--;
	LeaveCriticalSection(&m_CrtSection);
	return TRUE;
}
//AOI_ImageCtrlQ에서는 제거 된다. (Pointer제거.)
void	AOI_ImageCtrlQ::QCleanInspectNode(void)
{
	EnterCriticalSection(&m_CrtSection);
	InspectDataQ * pDeleteNode = NULL;

	if(m_HeadNode.m_pNextNode != &m_TailNode)
	{
		pDeleteNode =  m_HeadNode.m_pNextNode;

		InspectDataQ * pNextNodeBuf = NULL;
		while (pDeleteNode != &m_TailNode)
		{
			pNextNodeBuf = pDeleteNode->m_pNextNode;
			delete pDeleteNode;

			pDeleteNode = pNextNodeBuf;
			pNextNodeBuf = NULL;
		}
		m_HeadNode.m_pFrontNode		= NULL;
		m_HeadNode.m_pNextNode		= &m_TailNode;

		m_TailNode.m_pFrontNode		= &m_HeadNode;
		m_TailNode.m_pNextNode		= NULL;

		m_ImageGrabBufCnt = 0;
	}

	LeaveCriticalSection(&m_CrtSection);
	return;
	
}

