// DefectNmaeQ.h: interface for the CDefectNmaeQ class.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include "../Include/opencv/cv.h"
#include "../Include/opencv/cxcore.h"
#include "../Include/opencv/highgui.h"
#pragma pack(1)
//////////////////////////////////////////////////////////////////////////
// Defect이 발견되면 해당정보를 저장한다.

//////////////////////////////////////////////////////////////////////////
//#define  DEFECT_BUFFER_MAX	100
//#define  NOR_DEFECT			11
//#define  CD_DEFECT			12
//#define  DEFECT_IMAGE_SIZE_W	200//width
//#define  DEFECT_IMAGE_SIZE_H	200//Height
//#define  DEFECT_IMAGE_SIZE	(DEFECT_IMAGE_SIZE_W*DEFECT_IMAGE_SIZE_H)//width * Height

static const UINT	DEFECT_BUFFER_MAX	= 100;
static const UINT	NOR_DEFECT			= 11;
static const UINT	CD_DEFECT			= 12;
static const UINT	DEFECT_IMAGE_SIZE_W	= 200;//width
static const UINT	DEFECT_IMAGE_SIZE_H	= 200;//Height
static const UINT	DEFECT_IMAGE_SIZE	= (DEFECT_IMAGE_SIZE_W*DEFECT_IMAGE_SIZE_H);//width * Height

static const int	END_DEF_NOT_INPUT	= (-1);

/** 
@brief		Grab Image 저장을 위한 구조체

@remark
-			 Grabber에서 들어온 Data를 처리 하기위해 추가 정보를 기록한다.
-			
@author		최지형
@date		2010.03.00
*/ 
typedef struct IMAGE_GRAB_INFO_TAG
{
	UINT	ScanLine;
	UINT	ScanImageIndex;
	BYTE *  pInsertImage;
	BYTE *  pResultImage;//추가
	int		W;
	int		H;
	IMAGE_GRAB_INFO_TAG()
	{
		memset(this,0x00, sizeof(IMAGE_GRAB_INFO_TAG));
	}

}IMAGE_GRAB_INFO;

/** 
@brief		검출 Defect정보를 저장 한다.

@remark
-			 Defect 정보를 처리 하기 위한 기본 Data구조 이다.
-			
@author		최지형
@date		2010.03.00
*/ 
typedef struct DEFECT_DATA_TAG
{
	int		DefectType;
	int		StartX;
	int		StartY;
	int		EndX;
	int		EndY;

	DEFECT_DATA_TAG()
	{
		memset(this,0x00, sizeof(DEFECT_DATA_TAG));
	}

}DEFECT_DATA;

//////////////////////////////////////////////////////////////////////////
typedef DEFECT_DATA * (*fnCreateDefBuf)(int CreatMemSize);//Input DLL

//////////////////////////////////////////////////////////////////////////
//Inspection DLL로 제공되는 Q Object로. Grab된 이미지를 전달 하며, 검출된 Defect를
//Linked List로 저장 관리 한다.
//////////////////////////////////////////////////////////////////////////
class InspectDataQ  
{
public:
	InspectDataQ(IMAGE_GRAB_INFO * pInsertImageInfo = NULL, fnCreateDefBuf CreatBufFn = NULL);		//생성시 Inspection 대상 Image를 저장한다.
	~InspectDataQ();

private:
	UINT			m_CountDefectNode;					//입력된 Defect Node Count.
	///BYTE *			m_pGrabImage;						//Grab된 Image. 

	DEFECT_DATA		*m_pDefDataBuf; //Local Memory Create(DLL과 Memory 공유 문제)
	fnCreateDefBuf	m_gfnCreateBuff;//Local Memory Create(DLL과 Memory 공유 문제)
public:
	IplImage		*m_pGrabImage;						//Grab된 Image.	
public:
	InspectDataQ	* m_pFrontNode;
	InspectDataQ	* m_pNextNode;

public:
	UINT			m_ScanLine;
	UINT			m_ScanImageIndex;
	
	int				m_EndDefectCount;

private: 
	CRITICAL_SECTION m_CrtSection;
	//BOOL			 m_bEndNodeFlag;//2010.04.07 GJJ
	UINT			 m_DefecPopIndex;					//입력된 Defect Node Count.
public:
	//////////////////////////////////////////////////////////////////////////
	//Inspect DLL에서 사용할 Function.
	BYTE *			QGetGrabImage();					//저장된 Grab Image를 가져온다.
	void			QDeleteGrabImage();					//해당 Image의 Memory영역을 Delete한다.

	//IMG저장용이미지와 검출된 Defect Data를 저장 한다.(Pointer가 아닌 Value로 전달. 내부에서 Copy한다)
	BOOL			QSetDefectBuffer(int DefectNodeCount);
	BOOL			QPutDefectNode(DEFECT_DATA *pNewData);
	
	BOOL			QSetDefectAllBuffer(int DefectNodeCount, DEFECT_DATA *pNewData);
	//BOOL			QPutDefectAllNode(int nDefectCnt, DEFECT_DATA *pNewData);
	void			QPutEndDefectNode(int DefectNodeCount = -1);				//검출 종료를 알리는 Node를 추가 한다.

	//////////////////////////////////////////////////////////////////////////
	//Inspect Task에서 사용할 Function.
	BOOL			QCeckDefectEndNode(void);
	BOOL			QGetDefectNode(DEFECT_DATA **pDefectNodeBuffer);


	void			SetStaticMemData(IMAGE_GRAB_INFO * pInsertImageInfo, fnCreateDefBuf CreatBufFn);
};

class AOI_ImageCtrlQ //Pointer 관리만 한다.
{
public:
	AOI_ImageCtrlQ();
	~AOI_ImageCtrlQ();

public:
	UINT			m_CountDefectNode;						//입력된 Inspect Node Count.
	InspectDataQ	m_HeadNode;
	InspectDataQ	m_TailNode;

	UINT			m_ImageGrabBufCnt;


	InspectDataQ	*pStaticMemList;
	int				m_StaiceMemCnt;
	int			m_InputNode;
	int			m_OutputNode;
private: 
	CRITICAL_SECTION m_CrtSection;

public:
	BOOL			QCreateStaticMemBlock(int ImageCount, int ImageW, int ImageH);
	InspectDataQ*	QPutStaticMemNode(IMAGE_GRAB_INFO * pInsertImageInfo, fnCreateDefBuf CreatBufFn);
	InspectDataQ*	QGetStaticInspectNode(void);

	InspectDataQ*	QPutInspectNode(IMAGE_GRAB_INFO * pInsertImageInfo, fnCreateDefBuf CreatBufFn = NULL);	//새로운 Inspect Node를 생성하고 Pointer를 반환 한다.
	InspectDataQ*	QGetInspectNode(void);				//첫번째 Inspect Node를 반환 한다. (Thread에서 Defect Node End를 체크 처리 한다.)
														//AOI_ImageCtrlQ에서는 제거 된다. (Pointer제거.)
	BOOL			QPopInspectNode(void);				//처리가 종료된 Node제거.(QGetInspectNode() 실행후 Data처리가 끝나면 호출하여 Node를 제거한다.)
	void			QCleanInspectNode(void);
};

 #pragma pack()