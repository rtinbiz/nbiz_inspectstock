/*
*	File Name		:	nBizAnaimAOI.h
*	Function		:	Main Class
*	Create			:	2008.07.08
*	Author			:	고정진
*	Version			:	1.0
*/

#pragma once

#ifndef		AUTOSEARCH_H
#define		AUTOSEARCH_H

#include "ImageFile.h"
#include "ImageFilter.h"


extern "C" void SearchDefect(SIMAGE_INFO* sSourceImage, unsigned char* chResult);



//HWND					hWnd;

class ProcessReady
{
public:
	ImageFilter		cImageFilter;
	ImageFile		cImageInfo;

	SMASK_IMAGEINFO			sMaskImageInfo;
	SCOMMON_PARA		 	sCommonPara;	

	SIMAGE_INFO sSourceImage;
	IplImage *pResultImage;

	unsigned char* chResult;
	//bool g_bFirstCudaBuffer;


public:

	ProcessReady()
	{
		//g_bFirstCudaBuffer = false;
		/*cImageFilter = new ImageFilter;
		cImageInfo = new ImageFile;
		cMaskImage = new ImageFile;
		cAlignImage = new ImageFile;*/
		//sMaskImageInfo = new SMASK_IMAGEINFO;
		//sCommonPara = new SCOMMON_PARA;

		sSourceImage.p_chImageData = new unsigned char[IMAGE_WIDTH * IMAGE_HEIGHT];
		chResult = new unsigned char[IMAGE_WIDTH * IMAGE_HEIGHT];
		//g_bFirstCudaBuffer = true;
		pResultImage = cvCreateImage(cvSize(IMAGE_WIDTH, IMAGE_HEIGHT), IPL_DEPTH_8U, CHANNEL_GRAY);

	}

	~ProcessReady()
	{
		/*delete cImageFilter;
		delete cImageInfo;
		delete cMaskImage;
		delete cAlignImage;*/
		//delete sMaskImageInfo;
		//delete sCommonPara;
		//if(g_bFirstCudaBuffer)
		{
			delete[] sSourceImage.p_chImageData;
			delete[] chResult;
			cvReleaseImage(&pResultImage);
		}
		cImageInfo.m_fnFreeMemory();
	}
	/*
	*	Module Name		:	m_fnParameterSet
	*	Parameter		:	SMASK_IMAGEINFO, SCOMMON_PARA
	*	Return			:	None
	*	Function		:	파라미터 전달.	
	*	Create			:	2008.07.08
	*	Author			:	고정진
	*	Version			:	1.0
	*/
	void	m_fnParameterSet(SMASK_IMAGEINFO	sMaskImage,		
							 SCOMMON_PARA		sCommon)
	{
		//ZeroMemory(sMaskImageInfo.chPPID,PPID_LENGTH);
		sMaskImageInfo = sMaskImage;
		sCommonPara = sCommon;		
		//memcpy(sMaskImageInfo, &sMaskImage, sizeof(SMASK_IMAGEINFO));
		//memcpy(sCommonPara, &sCommon,sizeof(SCOMMON_PARA));

		cImageFilter.m_fnMemberDataSet(&cImageInfo);

		cImageFilter.m_fnMemberDataSet(sMaskImageInfo,sCommonPara);
		cImageInfo.m_fnMemberDataSet(sMaskImageInfo,sCommonPara);

		//tLthread=CreateThread(NULL,0,ThreadDataPop,NULL,0,&tThreadID);
	}

	/*
	*	Module Name		:	m_fnStepLogFlagSet
	*	Parameter		:	로그 기록 여부 Flag.
	*	Return			:	없음
	*	Function		:	
	*	Create			:	2008.07.08
	*	Author			:	고정진
	*	Version			:	1.0
	*/
#ifdef _DEBUG
	void	m_fnStepLogFlagSet(bool bSetFlag)
	{
		cImageInfo.m_gLogFlag = bSetFlag;
	}
#endif

	/*
	*	Module Name		:	m_fnDefectSearch
	*	Parameter		:	File Path.
	*	Return			:	결과 데이터의 시작 포인터.
	*	Function		:	결함 검출 메인 함수. 얼라인 안함
	*	Create			:	2008.07.08
	*	Author			:	고정진
	*	Version			:	1.0
	*/
	int*	m_fnDefectSearch(char* chFileFullPath)
	{
		//bool bResult=FALSE;
		int* nDefectPos;		
		//char strLog[200] = {0,};

		//char chFileName[10] = {0,};
		//strncpy_s(chFileName,&chFileFullPath[strlen(chFileFullPath) - 8],8);
		//
		//bResult = m_fnImageFileRead(chFileFullPath, INSPECT_IMAGE);

		//if(bResult != true)
		//	return 0;

		//bResult		=	cImageFilter.m_fnSearchDefect();
		//nDefectPos	=	cImageInfo.m_fnResultDefectPos();
		//

		//cImageFilter.m_fnReturnDefectData(nDefectPos);
		//
	
		return	nDefectPos;
	}


	/*
	*	Module Name		:	m_fnDefectSearchCUDA
	*	Parameter		:	File Path.
	*	Return			:	결과 데이터의 시작 포인터.
	*	Function		:	결함 검출 메인 함수. 얼라인 한다.
	*	Create			:	2008.07.08
	*	Author			:	고정진
	*	Version			:	1.0
	*/
	int*	m_fnDefectSearchCuda(char* chFileFullPath)
	{
		//bool bResult=FALSE;
		int* nDefectPos;		
		//int	 nScaleX = 0;
		//int  nScaleY = 0;

		//bResult = m_fnImageFileRead(chFileFullPath, INSPECT_IMAGE);
		//nScaleX = cImageInfo.m_sDib_Info.nImageScaleX;
		//nScaleY = cImageInfo.m_sDib_Info.nImageScaleY;

		//if(!g_bFirstCudaBuffer)
		//{
		//	sSourceImage.p_chImageData = new unsigned char[nScaleX * nScaleY];
		//	chResult = new unsigned char[nScaleX * nScaleY];
		//	g_bFirstCudaBuffer = true;
		//}
		//
		////Cuda Buffer Set		2008.8.04 GJJ
		//memcpy(sSourceImage.p_chImageData,cImageInfo.m_sDib_Info.pRaster,nScaleX * nScaleY);
		//sSourceImage.nWidth = nScaleX;
		//sSourceImage.nHeight = nScaleY;		
		//
		//SearchDefect( sSourceImage, chResult);		

		//memcpy(cImageInfo.m_sDib_InfoBack.pRaster,chResult,nScaleX * nScaleY);
		//
		//
		//nDefectPos	=	cImageInfo.m_fnResultDefectPosCUDA();
		//
		//cImageFilter.m_fnReturnDefectData(nDefectPos);
		
		return	nDefectPos;
	}

	/*
	*	Module Name		:	m_fnDefectSearchCudaT
	*	Parameter		:	File Path.
	*	Return			:	결과 데이터의 시작 포인터.
	*	Function		:	결함 검출 메인 함수. 얼라인 한다.
	*	Create			:	2008.07.08
	*	Author			:	고정진
	*	Version			:	1.0
	*/
	void	m_fnDefectSearchCudaT(SOBJECT_STR sObject_Str)
	{
		bool bResult=FALSE;		
		int	 nScaleX = IMAGE_WIDTH;
		int  nScaleY = IMAGE_HEIGHT;

		BYTE* byImageData;
		byImageData = sObject_Str.cInspectDataQ->QGetGrabImage();

		//Cuda Buffer Set		2008.8.04 GJJ
		memcpy(sSourceImage.p_chImageData,byImageData,nScaleX * nScaleY);
		sSourceImage.nWidth = nScaleX;
		sSourceImage.nHeight = nScaleY;		

		SearchDefect( &sSourceImage, chResult);		
		
		memcpy(pResultImage->imageData, chResult, nScaleX * nScaleY);

		int nResult = cvSaveImage(sObject_Str.chImagePath, pResultImage);

		return;
				
	}

	/*
	*	Module Name		:	m_fnDefectSearchT
	*	Parameter		:	File Path.
	*	Return			:	결과 데이터의 시작 포인터.
	*	Function		:	결함 검출 메인 함수. 얼라인 한다.
	*	Create			:	2008.07.08
	*	Author			:	고정진
	*	Version			:	1.0
	*/
	void	m_fnDefectSearchT(SOBJECT_STR sObject_Str)
	{
		bool bResult=FALSE;
		int	 nScaleX = IMAGE_WIDTH;
		int  nScaleY = IMAGE_HEIGHT;
		int	 nCDDefectCount = 0;

		int* nDefectPos;		
		char strLog[200] = {0,};			

		bResult = m_fnImageFileRead(sObject_Str.chImagePath);

//#ifdef _DEBUG		
//		cImageInfo.m_fnAddDebugLog(strLog);		
//#endif
		if(bResult != true)
			return ;

//#ifdef _DEBUG
//		cImageInfo.m_fnAddDebugLog(strLog);
//#endif
		
		




		//nDefectPos		=	cImageInfo.m_fnResultDefectPos();		
		//cImageInfo.m_fnReturnDefectData();
		//BYTE* byImageData;
		//byImageData = sObject_Str.cInspectDataQ->QGetGrabImage();		
		//memcpy(sSourceImage.p_chImageData,byImageData,nScaleX * nScaleY);
		//nCDDefectCount	=	cImageFilter.m_fnSearchCDDefect(&sSourceImage);

		DEFECT_DATA st_DefectData;
		int nDefectCount = cImageInfo.m_fnGetSearchResultCount();

		nDefectCount = 100;

		//for(int nStep = 0; nStep < nDefectCount; nStep++ )
		//{
		//	//cImageInfo.m_fnGetDefectNode( nStep, &st_DefectData);
		//	
		//	
		//	st_DefectData.DefectType = 123;
		//	st_DefectData.StartX = nStep + 1;
		//	st_DefectData.StartY = nStep + 1;
		//	st_DefectData.Width = nStep + 1;
		//	st_DefectData.Hight = nStep + 1;			
		//	
		//	sObject_Str.cInspectDataQ->QPutDefectNode(&st_DefectData);
		//}
		
		//Sleep(100);
		sObject_Str.cInspectDataQ->QPutEndDefectNode();
		//Sleep(100);

/*#ifdef _DEBUG			
		cImageInfo.m_fnAddDebugLog(strLog);
#endif	*/	
		
	}

	/*
	*	Module Name		:	m_fnImageFileRead
	*	Parameter		:	File Path.
	*	Return			:	성공 여부.
	*	Function		:	이미지 파일을 읽는다.
	*	Create			:	2008.07.08
	*	Author			:	고정진
	*	Version			:	1.0
	*/
	bool	m_fnImageFileRead(char* chFileFullPath)
	{

		char strLog[200] = {0,};

		if( GetFileAttributes(_T(chFileFullPath))== -1)
		{

#ifdef _DEBUG
			sprintf_s(strLog,"Can't Find : %s", chFileFullPath);			
			cImageInfo.m_fnAddDebugLog(strLog);
#endif
			return false;			
		}		

		if(strstr(chFileFullPath,"bmp") || strstr(chFileFullPath,"BMP") )
		{			
			cImageInfo.m_fnBMP_ImageLoad(chFileFullPath);			
		}

		return true;
	}
};
#endif