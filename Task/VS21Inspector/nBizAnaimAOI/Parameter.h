/** 
@addtogroup	PARAMETER
@{
*/ 

/**
@file	Parameter.h
@brief	파라미터들의 집합
@remark		
-		
@author	고정진
@date	2007/10/01
*/
#ifndef		PARAMETER_H
#define		PARAMETER_H
#include "ErrorDefine.h"

static const int	OKAY			= 1;				///<정상 처리
static const int	NG				= 0;				///<정상 처리

static const bool READY		= true;
static const bool COMP			= false;

static const int	NORMAL_DEFECT = 1;
static const int	STICK_UPPER_DEFECT = 10;			// 스틱 상면
static const int	GLASS_UPPER_DEFECT = 20;			// 글라스 상면
static const int	GLASS_LOWER_DEFECT = 21;			// 글라스 하면
static const int	ETCHING_OVER_DEFECT = 30;			// 과에칭
static const int	ETCHING_UNDER_DEFECT = 31;		// 미에칭
static const int	ALL_DEFECT = 10000;

static const int START_POSX = 0;
static const int START_POSY = 1;
static const int END_POSX = 2;
static const int END_POSY = 3;
static const int	MIN_THREAD_COUNT = 2;

static const int	MAX_THREAD_COUNT = 8;

static const int	MAX_POST_PROC_THREAD = MAX_THREAD_COUNT - 1;		///<최대 검출 스레드 생성 갯수

static const int	FILE_NAME_LENGTH	=	20;			///<_ACC Result Image 의 파일 이름의 최대 길이

static	const int		MAX_DEFECT			=	1000;		///<검출 가능한 최대 Defect

static const int	IMAGE_MAX_WIDTH		= 20480 + 4;				///<이미지 최대 폭
static const int	IMAGE_MAX_HEIGHT		= 10000 + 4;				///<이미지 쵀대 높이

static const int	CHANNEL_RGB		= 3;				///<3채널 이미지
static const int	CHANNEL_GRAY	= 1;				///<1채널 이미지
static const int	ITEM_NOT_EXIST	= -1;				///<노드 없음

static const int    ACT_SEARCH_MODE = 0;
static const int    PAD_SEARCH_MODE = 1;
static const int	PAD_BUFF_COUNT = 3;


static const unsigned int	FIST_READY		= 0x00000001;
static const unsigned int	FIST_COMP			= 0x00000002;
static const unsigned int	FIST_BUSY			= 0x00000004;
static const unsigned int	SECOND_READY	= 0x00000008;
static const unsigned int	SECOND_COMP	= 0x00000010;
static const unsigned int	SECOND_BUSY		= 0x00000020;
static const unsigned int	THIRD_READY		= 0x00000040;
static const unsigned int	THIRD_COMP		= 0x00000080;
static const unsigned int	THIRD_BUSY		= 0x00000100;
static const unsigned int	ALL_READY			= FIST_READY | SECOND_READY | THIRD_READY;
static const unsigned int	ALL_COMPLETE	= FIST_COMP | SECOND_COMP | THIRD_COMP;
static const unsigned int	ALL_BUSY			= FIST_BUSY | SECOND_BUSY | THIRD_BUSY;
static const unsigned int	READY1BUSY2		= FIST_READY | SECOND_BUSY | THIRD_BUSY;

static const unsigned int	PAD_STEP_TOP	= 0;
static const unsigned int	PAD_STEP_DOWN = 1;
static const unsigned int	PAD_STEP_LEFT	= 2;
static const unsigned int	PAD_STEP_RIGHT = 3;
static const unsigned int	MAX_PAD_AREA	= 4;

static const unsigned int	THREAD_AREA	= 0x00FFFFFF;	///<Thread 상의 정의
static const unsigned int	THREAD_RUN	= 0x01000000;
static const unsigned int	THREAD_IDLE	= 0x02000000;
static const unsigned int	BLOCK_AREA   = 0x00FF0000;
static const unsigned int	INDEX_AREA	= 0x0000FFFF;

#endif
/** 
@}
*/