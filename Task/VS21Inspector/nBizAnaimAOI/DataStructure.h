/** 
@addtogroup	DATAGROUP
@{
*/ 

/**
@file		DataStructure.h
@brief		프로그램 전반에서 사용되는 데이터를 정의
@remark		
@author		고정진
@date		2007.10.01
*/

#pragma once

#ifndef		DATASTRUCTURE_H
#define		DATASTRUCTURE_H

#include	"Parameter.h"
#include	"_ACCStructure.h"


//_ACC Function Define S
extern "C" void _ACC_Act_SearchDefect(unsigned char* chSourceImage, 
												unsigned char* chResultImage, 
												unsigned char* chXLineResult, 
												unsigned char* chYLineResult
												);
extern "C" void _ACC_Act_SetParameter(SAOI_IMAGE_INFO sMaskInfo, 
												SAOI_COMMON_PARA sCommon 
												);

extern "C" int _ACC_Act_SetScale( SAOI_PAD_AREA stPadArea );

extern "C" void _ACC_Pad_SearchDefect(unsigned char* chSourceImage1,		///<이미지 정보 구조체 이다.														
														unsigned char* chResultImage,			///<결과 데이터를 넘겨준다. 이진화된 이미지 형태이다.
														unsigned char* chXLineResult, 
														unsigned char* chYLineResult
														);

extern "C"	void _ACC_Pad_SetArray(unsigned char* chSourceImage2,		///<이미지 정보 구조체 이다.
													unsigned char* chSourceImage3		///<이미지 정보 구조체 이다. 
													);

extern "C" void _ACC_Pad_SetParameter(SAOI_PAD_IMAGE_INFO sMaskInfo, 
														SAOI_PAD_COMMON_PARA sCommon 
														);

//_ACC Function Define E

/**
@struct	SRGB_VALUE
@brief	RGB 요소의 값을 갖는 3Byte 구조체.
@remark	
-		본 구조체는 RGB 요소의 값 입력 및 전달에 쓰인다.
@author	고정진
@date	2010/4/29  11:15
*/
struct SRGB_VALUE
{
	BYTE	nRValue;	///해당 픽셀의 Red Value
	BYTE	nGValue;	///해당 픽셀의 Green Value
	BYTE	nBValue;	///해당 픽셀의 Blue Value
	SRGB_VALUE()
	{
		memset(this, 0x00, sizeof(SRGB_VALUE));
	}
};


/**
@struct	SXY_POSITION
@brief	특정 좌표를 전달하가 위한 구조체이다.
@remark	
-		
@author	고정진
@date	2010/4/29  11:15
*/
struct SXY_POSITION
{
	USHORT usXPos;		///X 좌표
	USHORT usYPos;		///Y 좌표.
	SXY_POSITION()
	{
		memset(this, 0x00, sizeof(SXY_POSITION));
	}
};


/**
@struct	SOBJECT_STR
@brief	처리 데이터 Class Object 를 전달
@remark	
-		전처리(_ACC Process)에 쓰일 데이터를 전달
@author	고정진
@date	2010/4/29  11:15
*/
struct SOBJECT_STR
{
	void*	cObjectPointer;						///<InspectDataQ*	
	SOBJECT_STR()
	{
		memset(this, 0x00, sizeof(SOBJECT_STR));
		cObjectPointer = nullptr;
	}
};


/**
@struct	SOBJECT_STR_PAD
@brief	처리 데이터 Class Object 를 전달
@remark	
-		전처리(_ACC Process)에 쓰일 데이터를 전달
@author	고정진
@date	2012/1/26  11:15
*/
struct SOBJECT_STR_PAD
{
	void*						cObjectPointer[PAD_BUFF_COUNT];		///<InspectDataQ*
	int							nObjectStatus[PAD_BUFF_COUNT];
	unsigned char*		chXLineResult[PAD_BUFF_COUNT];			///<해당열 Defect 검출 Flag	
	unsigned char*		chYLineResult[PAD_BUFF_COUNT];			///<해당열 Defect 검출 Flag	
	unsigned char*		chResultImage[PAD_BUFF_COUNT];		///<해당열 Defect 검출 Flag	
	SOBJECT_STR_PAD()
	{
		memset(this, 0x00, sizeof(SOBJECT_STR_PAD));
		for( int nStep = 0; nStep < PAD_BUFF_COUNT; nStep++)
		{
			cObjectPointer[nStep] = nullptr;
			chXLineResult[nStep] = nullptr;
			chYLineResult[nStep] = nullptr;
			chResultImage[nStep] = nullptr;
		}
		nObjectStatus[0] = FIST_COMP;
		nObjectStatus[1] = SECOND_COMP;
		nObjectStatus[2] = THIRD_COMP;
	}
};

/**
@struct	SRESOBJECT_STR
@brief	처리 데이터 Class Object 를 전달
@remark	
-		후처리에 쓰일 데이터를 전달
@author	고정진
@date	2010/4/29  11:15
*/
struct SRESOBJECT_STR
{
	void*					cObjectPointer;								///<InspectDataQ*	
	unsigned char		chXLineResult[IMAGE_MAX_WIDTH];	///<해당 열 검출 데이터
	unsigned char		chYLineResult[IMAGE_MAX_HEIGHT];	///<해당 행 검출 데이터
	SRESOBJECT_STR()
	{
		memset(this, 0x00, sizeof(SRESOBJECT_STR));
		cObjectPointer = nullptr;
	}
};


/**
@struct	SOBJECT_INSPECTOR_STR
@brief	내부 스레드 오브젝트 포인터를 저장하는 구조체
@remark	
-			
@author	고정진
@date	2010/4/29  11:15
*/
struct SOBJECT_INSPECTOR_STR
{
	void*	cObjectPointer;					///<Thread Object Pointer
	SOBJECT_INSPECTOR_STR()
	{
		memset(this, 0x00, sizeof(SOBJECT_INSPECTOR_STR));
		cObjectPointer = nullptr;
	}
};


/**
@struct		THREAD_PARAM
@brief		스레드 파라미터
@remark	
-				내부 스레드에 정보 전달은 목적으로 하는 구조체
@author		고정진
@date		2009/5/26  12:40
*/
struct THREAD_PARAM
{	
	int			nThreadNumber;		///<해당 스레드의 넘버
	UINT*	pProcStateArea;		///<스레드 정보 갱신 영역 포인터
	void*		vpIncpectorClass;	///<MainInspector Class 의 Object Pointer
	void*		vpThreadCreator;	    ///<스레드 생성자의 Object Pointer		
	THREAD_PARAM()
	{
		memset(this, 0x00, sizeof(THREAD_PARAM));
		vpIncpectorClass = nullptr;
		pProcStateArea = nullptr;	
		vpThreadCreator = nullptr;		
	}
};


/**
@struct		THREAD_PARAM
@brief		스레드 파라미터
@remark	
-				내부 스레드에 정보 전달은 목적으로 하는 구조체
@author		고정진
@date		2009/5/26  12:40
*/
struct SACC_PROCESS_CALL
{		
	void	(*fnAccCall)(unsigned char* chSourceImage, 
										unsigned char* chResultImage, 
										unsigned char* chXLineResult, 
										unsigned char* chYLineResult);
	SACC_PROCESS_CALL()
	{	
		fnAccCall = nullptr;		
	}
};

/**
@struct		DEFECT_INFO
@brief		DEFECT Information
@remark	
-				결함 정보
@author		고정진
@date		2012/9/24  12:40
*/
typedef struct DEFECT_INFO_TAG
{	
	unsigned int		nDValueMin;
	unsigned int		nDValueMax;
	unsigned int		nDValueAvg;
	
	DEFECT_INFO_TAG()
	{
		nDValueMin = 255;
		nDValueMax = 0;
		nDValueAvg = 0;
		//memset(this,0x00, sizeof(DEFECT_INFO_TAG));
	}

}DEFECT_INFO;

#endif

/** 
@}
*/ 