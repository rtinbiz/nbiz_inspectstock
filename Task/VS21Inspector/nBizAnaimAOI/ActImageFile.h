/** 
@addtogroup	ACT_FILE_INFOMATION
@{
*/ 

/**
@file		ImageFile.h
@brief	파일의 Information.
@remark	
-			이미지의 Loading, 픽셀 데이터 읽기, 픽셀 값 변경작업.
@author	고정진
@date	2007.10.01
*/
#pragma once
#ifndef		ACT_IMAGEFILE_H
#define		ACT_IMAGEFILE_H
#include "DataStructure.h"

/**
@class	ImageFile
@brief	Processing 할 이미지의 정보를 갖는다.
@remark		
-			_ACC Result Image 에 대한 관리와, Defect Data 를 관리한다.
@author	고정진
@date	2012/01/11  11:28
*/
class ActImageFile
{
public:
	ActImageFile(void);											///<생성자
	~ActImageFile(void);											///<소멸자		
	
#ifdef _DEBUG
	int				m_gLogFlag;
#endif
	void	m_fnMemberDataSet(	SAOI_IMAGE_INFO sMaskInfo,
								SAOI_COMMON_PARA sCommon);			///마스크 이미지 정보와, 공통 파라미터를 받는다.	
		
	void 	m_fnResultDefectPos(unsigned char* byResultImage, unsigned char* chXLineResult, 
										 unsigned char* chYLineResult = nullptr);	///<결함의 위치와 크기를 검출한다.	

	void m_fnResultDefectType(IplImage *pSourceImage, IplImage *pResultImage);
	int m_fnCheckDefectType(IplImage *pSourceImage, IplImage *pResultImage, CvRect rcDefect);
	int m_fnGetReferencePosDefectType(IplImage *pResultImage, CvRect rcDefect, CvRect &rcRef, int &nRefDistanceX, int &nRefDistanceY);

	void	m_fnSortingDefectPos();

	void	m_fnAddDebugLog(char* strLog);						///<디버그용 로그함수 이다.

	void	m_fnAddDebugLog2(char* strLog);						///<디버그용 로그함수 이다.
		
	bool	m_fnReadPosition(int nXPos, int nYPos, SRGB_VALUE* s_RGBValue);	///<해당 위치의 픽셀 정보를 읽는다.	
	
	void	m_fnReturnDefectData();								///<좌표를 Length 좌표로 변환해 준다. 현재 사용 안함

	bool	m_fnGetDefectNode( int nIndex, DEFECT_DATA* st_DefectData);	///<해당 Index 의 Defect 좌표 정보를 전달한다.

	void	m_fnNorDefectCreate();	///<Size Filter 를 수행한다.

	int		m_fnGetSearchResultCount( );	///<검출된 Defect 의 개수를 리턴한다.

	int*	m_fnGetSearchResultData( );		///<검출된 Defect 리스트를 전달한다.

	void	m_fnDataBufferCreate();

	void	m_fnDataBufferDelete();

private:		
	unsigned char*				m_chImageData;
	SAOI_IMAGE_INFO			m_stMaskImageInfo;						///<Mask 이미지의 정보.
	SAOI_COMMON_PARA		m_stCommonPara;						///<공통 파라미터 정보.		

	DEFECT_DATA					m_stPreDefectData[MAX_DEFECT];
	DEFECT_DATA					m_stPosDefectData[MAX_DEFECT];
	DEFECT_INFO				m_stDefectInfo[MAX_DEFECT];
	//int					m_nDefectPos[MAX_DEFECT][4];		///<전처리 후 결함 데이터 리스트
	//int					m_nRealDefectPos[MAX_DEFECT][5];	///<후처리 후 결함 데이터 리스트
	int					m_nNorDefectCount;					///<CD 포함 Defect List 의 개수
	int					m_nImageWidth;
	int					m_nImageHeight;
	int					m_nImageOffset;

	
};

#endif
/** 
@}
*/ 