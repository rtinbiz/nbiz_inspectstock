/** 
@addtogroup	INLINE_FUNCTION
@{
*/ 

/**
@file		GLOBALFUNCTION.h
@brief	Inline 함수에 대하여 정의한다.

@remark			
@author	고정진
@date	2007/10/01
*/
#ifndef		GLOBALFUNCTION_H
#define		GLOBALFUNCTION_H


/**	
@brief	memcpy 에 대한 인라인 함수
@param 	dest, src, Length
@return	없음.
@remark	
-			
@author	고정진
@date	2007/4/28  14:54
*/
inline	void	m_fnCopyData( void* dest, void* src, int nLength )
{		
	memcpy( dest, src, nLength );

	// 	if (nLength <= 0) return;
	// 	_asm
	// 	{
	// 		mov   edi, dest
	// 		mov   esi, src
	// 		mov   ecx, nLength
	// 		cld
	// 		rep   movsb
	// 	}
};

/**	
@brief	memset 에 대한 인라인 함수
@param 	src, Length, Serchar
@return	없음.
@remark	
-			
@author	고정진
@date	2007/4/28  14:54
*/
inline void	m_SetZeroData(void* pStartAdd,int nLength, char chSetChar)
{
	memset( pStartAdd, chSetChar, nLength );
	// 	if (nLength <= 0) return;
	// 	_asm
	// 	{
	// 		mov   al, chSetChar
	// 		mov   ecx, nLength
	// 		mov   edi, pStartAdd			
	// 		rep   stosb
	// 	}
};

/**	
@brief	memset 에 대한 인라인 함수
@param 	src, Length, Serchar
@return	없음.
@remark	
-			
@author	고정진
@date	2007/4/28  14:54
*/
inline void	m_SetByte(unsigned char* chStartAdd, unsigned int nPosition,unsigned char chSetChar)
{
	chStartAdd[nPosition] = chSetChar;	
};


/**	
@brief	DebugLog 출력
@param 	src, Length, Serchar
@return	없음.
@remark	
-			
@author	고정진
@date	2007/4/28  14:54
*/
inline void	m_OutputDebugLog(char* chLogString)
{
	OutputDebugString(chLogString);	
};



#endif
/** 
@}
*/