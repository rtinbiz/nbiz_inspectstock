/** 
@addtogroup	IMAGE_FILTER
@{
*/ 

/**
@file		ActImageFilter.h
@brief	검출 필터의 관리 클래스
@remark	
-			테스트를 위한 C Code 의 필터를 포함하고 있다.
-			일부 ImageFile 클래스와 중복이 있으나, 관리 되는 Data Source 가 달라 혼돈을 피하기 위함이다.
@author	고정진
@date	2010/4/29  13:56
*/

#pragma once
#ifndef	 IMAGEFILTER_H
#define	 IMAGEFILTER_H

#include "DataStructure.h"
#include "ActImageFile.h"
#include "Math.h"

/**
@class	ActImageFilter
@brief	결함 검출에 필요한 필터 및 부속 데이터.
@remark		
-			결함 검출에 필요한 필터 및 부속 데이터.
@author	고정진
@date	2012/01/11  11:28
*/
class ActImageFilter
{
public:
	ActImageFilter(void);											
	~ActImageFilter(void);	
	
	void	m_fnMemberDataSet(	SAOI_IMAGE_INFO sMaskImageInfo,
											SAOI_COMMON_PARA sCommonPara);	
	
	void	m_fnImageBuffSet(	unsigned char* chSourceImage,	///<이미지 정보 구조체 이다.
										unsigned char* chResultImage	///<결과 데이터를 넘겨준다. 이진화된 이미지 형태이다.													
									);

	void	m_fnSearchDefect(	unsigned char* chXLineResult,	
										unsigned char* chYLineResult	
									);
private:
	bool	m_fnEdgeIgnore(	int nXPos, int nYPos	);

	bool	m_fnEdgeCompare(	int nXPos, int nYPos,
										unsigned char	chComparePixel1,
										unsigned char	chComparePixel2,
										unsigned char	chPixelData
										);

	bool	m_fnEraser_PixelX(	int nXPos, int nYPos	);

	bool	m_fnEraser_PixelY(	int nXPos, int nYPos	);

	bool	m_fnCompare_AxisX(	 int nXPos, int nYPos );

	bool	m_fnCompare_AxisY( int nXPos, int nYPos );

	bool	m_fnRGB_Compare(	unsigned char chPixel_1,unsigned char 
										chPixel_2,int n_DefectRange
										);
	
	bool	m_fnReadPosition( int nXPos, int nYPos, unsigned char* chReadData );

	void	m_fnDataBufferCreate();

	void	m_fnDataBufferDelete();
	
private:
	SAOI_IMAGE_INFO		m_stMaskImageInfo;		///<마스크 이미지의 정보 구조체.
	SAOI_COMMON_PARA	m_stCommonPara;		///<공통 파라미터 구조체 변수.
	
	unsigned char*	m_chSourceImage;
	unsigned char*	m_chResultImage;
	unsigned char*	m_chSizeFilterBuff;			///<Image Data 저장소
	int						m_nHThreshold[256];		///<Threshold
	int						m_nImageWidth;				///<Image Width
	int						m_nImageHeight;				///<Image Height
	int						m_nImageOffset;				///<Image Width Offset
};
#endif
/** 
@}
*/ 