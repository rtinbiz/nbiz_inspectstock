/**
@file		MainInspector.cpp
@brief	메인 프로세스
@remark	
-			
@author	고정진
@date	2010/4/28  18:11
*/
#include "StdAfx.h"
#include "ActMainInspector.h"


/**	
@brief	생성자
@param 	없음
@return	없음
@remark
-			내부 버퍼 초기화
@author	고정진
@date	2010/4/28  18:11
*/
CActMainInspector::CActMainInspector(void)
{
	m_chSourceImage = nullptr;
//	sSourceImage.p_chImageData = nullptr;
	m_chResultImage = nullptr;
	m_chXLineResult = nullptr;
	m_chYLineResult = nullptr;
	m_cvResultImage = nullptr;
	m_nImageWidth = 0;
	m_nImageHeight = 0;

	fnAccProcessCall[0].fnAccCall = _ACC_Act_SearchDefect;
}

/**	
@brief	소멸자
@param 	없음
@return	없음
@remark
-			내부 버퍼 수거.
@author	고정진
@date	2010/4/28  18:11
*/
CActMainInspector::~CActMainInspector(void)
{
	if ( m_chResultImage != nullptr)
	{
		delete[] m_chSourceImage;
		delete[] m_chResultImage;
		delete[] m_chXLineResult ;
		delete[] m_chYLineResult;
		cvReleaseImage(&m_cvResultImage);
		m_chSourceImage = nullptr;
		m_chResultImage = nullptr;
		m_chXLineResult = nullptr;
		m_chYLineResult = nullptr;		
		m_cvResultImage = nullptr;				
	}
	
}

/**	
@brief	외부 파라미터 셑
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	없음
@remark
-		
@author	고정진
@date	2010/4/28  18:11
*/
void	CActMainInspector::m_fnParameterSet(	SAOI_IMAGE_INFO	 sMaskImage,		
																SAOI_COMMON_PARA sCommon)
{	
	m_stMaskImageInfo	= sMaskImage;
	m_stCommonPara		= sCommon;
	//메모리 생성
	m_fnDataBufferCreate();
	//메모리 생성 종료
	m_cImageFilter.m_fnMemberDataSet(m_stMaskImageInfo,m_stCommonPara);
	m_cImageInfo.m_fnMemberDataSet(m_stMaskImageInfo,m_stCommonPara);	
}


/**	
@brief	버퍼생성
@param 	없음
@return	없음
@remark
-			처리에 쓰일 버퍼를 생성한다.
-			버퍼의 크기가 바뀔때만 재생성한다.
@author	고정진
@date	2012/12/15  18:11
*/
void	CActMainInspector::m_fnDataBufferCreate()
{		
	//메모리 생성
	if ( m_stCommonPara.nImageWidth != this->m_nImageWidth || m_stCommonPara.nImageHeight != this->m_nImageHeight )
	{
		m_fnDataBufferDelete();
		this->m_nImageWidth = m_stCommonPara.nImageWidth;
		this->m_nImageHeight = m_stCommonPara.nImageHeight;
		this->m_nImageSize = this->m_nImageWidth * this->m_nImageHeight;
		m_chSourceImage= new unsigned char[this->m_nImageWidth * this->m_nImageHeight];
		m_chResultImage = new unsigned char[this->m_nImageWidth * this->m_nImageHeight];
		m_chXLineResult = new unsigned char[this->m_nImageWidth];		
		m_chYLineResult = new unsigned char[this->m_nImageHeight];		

		memset(m_chSourceImage, 0x00, this->m_nImageSize);
		memset(m_chResultImage, 0x00, this->m_nImageSize);
		memset(m_chXLineResult, 0x00, this->m_nImageWidth);
		memset(m_chYLineResult, 0x00, this->m_nImageHeight);

		m_cvResultImage = cvCreateImage(cvSize(this->m_nImageWidth, this->m_nImageHeight), IPL_DEPTH_8U, CHANNEL_GRAY);
	}	
}


/**	
@brief	버퍼삭제
@param 	없음
@return	없음
@remark
-			현재 버퍼가 생성되어 있다면 파괴한다.
@author	고정진
@date	2012/12/15  18:11
*/
void	CActMainInspector::m_fnDataBufferDelete()
{		
	if ( m_chResultImage != nullptr)
	{
		delete[] m_chSourceImage;
		delete[] m_chResultImage;
		delete[] m_chXLineResult;
		delete[] m_chYLineResult;
		cvReleaseImage(&m_cvResultImage);
		m_chSourceImage = nullptr;
		m_chResultImage = nullptr;
		m_chXLineResult = nullptr;	
		m_chYLineResult = nullptr;		
		m_cvResultImage = nullptr;		
	}

}

/**	
@brief	로그 기록 모드 셑
@param 	flag
@return	없음
@remark
-			디버그 모드에서만 수행한다.
@author	고정진
@date	2010/4/28  18:11
*/
#ifdef _DEBUG
void	CActMainInspector::m_fnStepLogFlagSet(bool bSetFlag)
{
	m_cImageInfo.m_gLogFlag = bSetFlag;
}
#endif


/**	
@brief	싱글 Defect 검출 함수
@param 	Image Source Data
@return	Defect Data Buffer Pointer
@remark
-			하나의 이미지에 데이터에 대하여 검출을 수행한다.
-			CPU 에서 처리하며, 테스트 목적의 코드이다.
@author	고정진
@date	2010/4/28  18:11
*/
int*	CActMainInspector::m_fnDefectSearch(unsigned char* byImageData)
{	
	bool bResult = false;		
	
	memcpy(m_chSourceImage,byImageData,this->m_nImageWidth * this->m_nImageHeight);
	
	ZeroMemory(m_chResultImage, this->m_nImageWidth * this->m_nImageHeight);
	ZeroMemory(m_chXLineResult, this->m_nImageWidth );
	ZeroMemory(m_chYLineResult, this->m_nImageHeight);

	m_cImageFilter.m_fnImageBuffSet( m_chSourceImage, m_chResultImage );

	m_cImageFilter.m_fnSearchDefect( m_chXLineResult, m_chYLineResult);

	memcpy(m_cvResultImage->imageData, m_chResultImage, this->m_nImageWidth * this->m_nImageHeight);

	//int nResult	= cvSaveImage("C:\\C_Result.BMP", m_cvResultImage);
	
	int* npDefectPos = nullptr;		

	m_cImageInfo.m_fnResultDefectPos( m_chResultImage, m_chXLineResult, m_chYLineResult );

	//m_cImageInfo.m_fnSortingDefectPos();

	m_cImageInfo.m_fnNorDefectCreate();	
	
	npDefectPos = m_cImageInfo.m_fnGetSearchResultData();
	
	return npDefectPos;
}

/**	
@brief	싱글 Defect 검출 함수
@param 	Image Source Data
@return	Defect Data Buffer Pointer
@remark
-			하나의 이미지에 데이터에 대하여 검출을 수행한다.
-			GPU 에서 처리하며, 테스트 목적의 코드이다.
@author	고정진
@date	2010/4/28  18:11
*/
int*	CActMainInspector::m_fnDefectSearch_ACC(unsigned char* byImageData)
{	
	bool bResult = false;	
	
	ZeroMemory(m_chResultImage, this->m_nImageWidth * this->m_nImageHeight);
	ZeroMemory(m_chXLineResult, this->m_nImageWidth );
	ZeroMemory(m_chYLineResult, this->m_nImageHeight);

	fnAccProcessCall[0].fnAccCall(  byImageData, m_chResultImage, m_chXLineResult, m_chYLineResult);
	memcpy(m_cvResultImage->imageData, m_chResultImage, this->m_nImageWidth * this->m_nImageHeight);

	int nResult	= cvSaveImage("C:\\G_Result.BMP", m_cvResultImage);		

	m_cImageInfo.m_fnResultDefectPos( m_chResultImage, m_chXLineResult, m_chYLineResult );

	//m_cImageInfo.m_fnSortingDefectPos();

	m_cImageInfo.m_fnNorDefectCreate();	

	int* npDefectPos = nullptr;

	npDefectPos = m_cImageInfo.m_fnGetSearchResultData();

	return npDefectPos;
}

/**	
@brief	_ACC Class 에 접근하는 함수이다.
@param 	SOBJECT_STR
@return	없음
@remark
-			Main _ACC Thread 에서 호출 되어지는 함수이다.
-			처리 중간 필터를 버퍼 관리자에 적재한다.
@author	고정진
@date	2010/4/28  18:11
*/
void	CActMainInspector::m_fnDefectSearch_ACCT(SOBJECT_STR sObject_Str)
{
	bool bResult = false;		
	VSBridge	cInspectDataQ;
	BYTE* byImageData = nullptr;
	cInspectDataQ = (VSBridge)sObject_Str.cObjectPointer;
	byImageData = cInspectDataQ->m_fnQGetGrabImage();
	
	fnAccProcessCall[0].fnAccCall( (unsigned char*)byImageData, m_chResultImage, m_chXLineResult, m_chYLineResult);
	
// 	memcpy(m_cvResultImage->imageData, m_chResultImage, this->m_nImageWidth * this->m_nImageHeight);
// 
// 	int nResult	= cvSaveImage("Z:\\G_Result.BMP", m_cvResultImage);	

}

/**	
@brief	후처리 함수
@param 	SRESOBJECT_STR
@return	없음
@remark
-			Main Postprocess Thread 에서 호출되어지는 함수이다.
-			후처리 함수들을 관리한다.
@author	고정진
@date	2010/4/28  18:11
*/
void	CActMainInspector::m_fnDefectResult_ACCT(SRESOBJECT_STR sResObject_Str)
{
	bool bResult = false;

	VSBridge 	cInspectDataQ;
	cInspectDataQ = (VSBridge)sResObject_Str.cObjectPointer;
	
	m_cImageInfo.m_fnResultDefectPos((unsigned char*)cInspectDataQ->m_fnQGetBufferData(), sResObject_Str.chXLineResult, nullptr );
	
	//m_cImageInfo.m_fnSortingDefectPos();

	m_cImageInfo.m_fnNorDefectCreate();
	
	int nDefectCount = 0;

	DEFECT_DATA* spDefectData = nullptr;

	nDefectCount = m_cImageInfo.m_fnGetSearchResultCount();	

	spDefectData = 	(DEFECT_DATA*)m_cImageInfo.m_fnGetSearchResultData();

	cInspectDataQ->m_fnQSetDefectAllBuffer(nDefectCount, spDefectData);

	//////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG			
	//cImageInfo.m_fnAddDebugLog(strLog);
#endif
	
}


/**	
@brief	후처리 함수
@param 	SRESOBJECT_STR
@return	없음
@remark
-			Main Postprocess Thread 에서 호출되어지는 함수이다.
-			후처리 함수들을 관리한다.
@author	고정진
@date	2010/4/28  18:11
*/
void	CActMainInspector::m_fnDefectResult_ACCT( unsigned char* chResultImage, unsigned char* chXLineResult, unsigned char* chYLineResult, void* vpObject )
{
	bool bResult = false;
	VSBridge 	cInspectDataQ;
	cInspectDataQ = (VSBridge)vpObject;		

	m_cImageInfo.m_fnResultDefectPos(chResultImage, chXLineResult, chYLineResult );

	//m_cImageInfo.m_fnSortingDefectPos();

	// Added by Charles
	memcpy(m_cvResultImage->imageData, chResultImage, this->m_nImageWidth * this->m_nImageHeight);
	m_cImageInfo.m_fnResultDefectType(cInspectDataQ->m_pGrabImage, m_cvResultImage);

	m_cImageInfo.m_fnNorDefectCreate();

	int nDefectCount = 0;

	DEFECT_DATA* spDefectData = nullptr;

	nDefectCount = m_cImageInfo.m_fnGetSearchResultCount();	

	spDefectData = 	(DEFECT_DATA*)m_cImageInfo.m_fnGetSearchResultData();

	cInspectDataQ->m_fnQSetDefectAllBuffer(nDefectCount, spDefectData);

	//////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG			
	//cImageInfo.m_fnAddDebugLog(strLog);
#endif

}

/**	
@brief	이미지 파일을 읽는다.
@param 	파일 경로
@return	 true : 정상, false : 실패
@remark
-			현재 버전에서 사용하지 않는다.
@author	고정진
@date	2010/4/28  18:11
*/
bool	CActMainInspector::m_fnImageFileRead(char* chFileFullPath)
{

	char strLog[200] = {0,};

	if( GetFileAttributes(_T(chFileFullPath))== -1)
	{

#ifdef _DEBUG
		sprintf_s(strLog,"Can't Find : %s", chFileFullPath);			
		m_cImageInfo.m_fnAddDebugLog(strLog);
#endif
		return false;			
	}		
	
	//if(strstr(chFileFullPath,"bmp") || strstr(chFileFullPath,"BMP") )
	{			
		//m_cImageInfo.m_fnImageLoad(chFileFullPath);			
	}

	return true;
}


/**	
@brief	Xline Search Result 전달.
@param 	오브젝트 Index
@return	Result 포인터
@remark
-			Xline Search Result 전달.
@author	고정진
@date	2012/1/27  18:11
*/
unsigned char*  CActMainInspector::m_fnGetXlineResult()
{
	return m_chXLineResult;
}

/**	
@brief	Xline Search Result 전달.
@param 	오브젝트 Index
@return	Result 포인터
@remark
-			Xline Search Result 전달.
@author	고정진
@date	2012/1/27  18:11
*/
unsigned char*  CActMainInspector::m_fnGetYlineResult()
{
	return m_chYLineResult;
}

/**	
@brief	Xline Search Result 전달.
@param 	오브젝트 Index
@return	Result 포인터
@remark
-			Xline Search Result 전달.
@author	고정진
@date	2012/1/27  18:11
*/
unsigned char*  CActMainInspector::m_fnGetResultImage()
{
	return m_chResultImage;
}

/**	
@brief	기준 이미지 폭 전달
@param 	없음
@return	이미지 폭
@remark
-			기준 이미지 폭 전달
@author	고정진
@date	2012/1/27  18:11
*/
int  CActMainInspector::m_fnGetImageWidth()
{
	return m_nImageWidth;
}

/**	
@brief	기준 이미지 폭 전달
@param 	없음
@return	이미지 폭
@remark
-			기준 이미지 폭 전달
@author	고정진
@date	2012/1/27  18:11
*/
int  CActMainInspector::m_fnGetImageHeight()
{
	return m_nImageHeight;
}

/**	
@brief	Main _ACC Thread
@param 	(THREAD_PARAM*)LPVOID;
@return	Normal Code
@remark
-			전처리 메인 스레드 이다.
@author	고정진
@date	2010/4/28  18:11
*/
unsigned int __stdcall	CActMainInspector::THREAD_PRE_PROC_ACT_ACC(LPVOID pParam)
{
	THREAD_PARAM*			pThreadParam		= (THREAD_PARAM*)pParam;
	CActMainInspector*		cInspector			= nullptr;
	CActThreadCreator*		cThreadCreator	= nullptr;
	InspectDataQ*			cInspectDataQ		= nullptr;
	UINT				unThreadStat	= 0;
	unsigned	int		unBlockNum		= 0;
	unsigned	int		unImageIndex	= 0;
	int					THREAD_NUM	= 0;	
	
	cInspector			= (CActMainInspector*)pThreadParam->vpIncpectorClass;
	cThreadCreator	= (CActThreadCreator*)pThreadParam->vpThreadCreator;		

	SOBJECT_STR			sObject_Str;
	SRESOBJECT_STR		sResObject_Str;
	//int nCountSum	= 0;
	int nReturn		= 0;	

	THREAD_NUM = pThreadParam->nThreadNumber - 1;

	while(cThreadCreator->m_fnGetThreadFlag())	
	{		
		nReturn = cThreadCreator->m_fnPopTemplateQNode(&sObject_Str);		
		unThreadStat = unThreadStat & THREAD_AREA;

		if(nReturn == ITEM_NOT_EXIST)
		{			
			Sleep(1);
			unThreadStat = unThreadStat | THREAD_IDLE;
			memcpy(&pThreadParam->pProcStateArea[THREAD_NUM], &unThreadStat, sizeof(unsigned int));
			continue;
		}			

		unThreadStat	= 0;

		cInspectDataQ	= (VSBridge)sObject_Str.cObjectPointer;
		unBlockNum		= cInspectDataQ->m_ScanLine;
		unBlockNum		= unBlockNum << 16;
		unImageIndex	= cInspectDataQ->m_ScanImageIndex;	

		unThreadStat	= unThreadStat | THREAD_RUN | unBlockNum | unImageIndex;

		memcpy(&pThreadParam->pProcStateArea[THREAD_NUM], &unThreadStat, sizeof(unsigned int));			

		cInspector->m_fnDefectSearch_ACCT( sObject_Str );

		//sResObject_Str.cObjectPointer = sObject_Str.cObjectPointer;
		//memcpy(sResObject_Str.chXLineResult, cInspector->m_fnGetXlineResult(), cInspector->m_fnGetImageWidth());
		//sResObject_Str.vpBufferImage = cInspector->m_chBufferImage;  동적 메모리  할당

		//cThreadCreator->m_fnPushTemplateResQNode(sResObject_Str);

		cThreadCreator->m_fnSetResultBuffer(
			cInspector->m_fnGetResultImage(), 
			cInspector->m_fnGetXlineResult(), 
			cInspector->m_fnGetYlineResult(), 
			sObject_Str.cObjectPointer);
	}

	_endthreadex(0);

	delete cInspector;
	return 0;
}

/**	
@brief	메인 후처리 스레드
@param 	(THREAD_PARAM*)pParam;
@return	Normal Code
@remark
-		
@author	고정진
@date	2010/4/28  18:11
*/
unsigned int __stdcall CActMainInspector::THREAD_POST_PROC_ACT_ACC(LPVOID pParam)
{
	THREAD_PARAM*		pThreadParam		= (THREAD_PARAM*)pParam;
	CActMainInspector*		cInspector			= nullptr;
	CActThreadCreator*		cThreadCreator	= nullptr;	
	InspectDataQ*		cInspectDataQ		= nullptr;
	UINT				unThreadStat	= 0;
	unsigned	int		unBlockNum		= 0;
	unsigned	int		unImageIndex	= 0;
	int					THREAD_NUM	= 0;
	int					OBJECT_INDEX = 0;
	cInspector			= (CActMainInspector*)pThreadParam->vpIncpectorClass;
	cThreadCreator	= (CActThreadCreator*)pThreadParam->vpThreadCreator;	
	SRESOBJECT_STR sResObject_Str;
	bool bBufferFlag = COMP;
	int nReturn = 0;
	
	//int nCountSum = 0;


	THREAD_NUM = pThreadParam->nThreadNumber - 1;
	OBJECT_INDEX = THREAD_NUM - 1;

	while(cThreadCreator->m_fnGetThreadFlag())
	{		

		//nReturn = cThreadCreator->m_fnPopTemplateResQNode(&sResObject_Str);

		bBufferFlag = cThreadCreator->m_fnGetBufferFlag(OBJECT_INDEX);

		unThreadStat = unThreadStat & THREAD_AREA;

		//if(nReturn == ITEM_NOT_EXIST)
		if(bBufferFlag == COMP)
		{
			Sleep(1);
			unThreadStat = unThreadStat | THREAD_IDLE;
			memcpy(&pThreadParam->pProcStateArea[THREAD_NUM], &unThreadStat, sizeof(unsigned int));
			continue;
		}

		unThreadStat	= 0;		

		//cInspectDataQ	= (InspectDataQ*)sResObject_Str.cObjectPointer;
		cInspectDataQ	= (VSBridge)cThreadCreator->m_fnGetObjectPointer(OBJECT_INDEX);
		unBlockNum		= cInspectDataQ->m_ScanLine;
		unBlockNum		= unBlockNum << 16;
		unImageIndex	= cInspectDataQ->m_ScanImageIndex;		

		unThreadStat	= unThreadStat | THREAD_RUN | unBlockNum | unImageIndex;	

		memcpy(&pThreadParam->pProcStateArea[THREAD_NUM], &unThreadStat, sizeof(unsigned int));

		//cInspector->m_fnDefectResult_ACCT(sResObject_Str);		

		cInspector->m_fnDefectResult_ACCT(
			cThreadCreator->m_fnGetResultImage(OBJECT_INDEX), 															
			cThreadCreator->m_fnGetXLineResult(OBJECT_INDEX), 															
			cThreadCreator->m_fnGetYLineResult(OBJECT_INDEX), 															
			cThreadCreator->m_fnGetObjectPointer(OBJECT_INDEX));	

		cThreadCreator->m_fnSetBufferFlag(COMP, OBJECT_INDEX);		
	}

	_endthreadex(0);
	delete cInspector;
	return 0;
}