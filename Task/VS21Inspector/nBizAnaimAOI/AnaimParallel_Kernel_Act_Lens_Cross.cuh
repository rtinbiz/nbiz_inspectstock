/**
@file	Anaim_ACC_Kernel.cu
@brief	커널단에서 수행되는 코드이다.
@remark	
@author	고정진
@date	2010/4/19  12:03
*/

#ifndef ANAIM_ACC_KERNEL_CORSS_H_
#define ANAIM_ACC_KERNEL_CORSS_H_

#include "AnaimParallel_Kernel_Act_Lens.cuh"

__device__ inline unsigned char _ACC_Compare_AxisX_Cross(																	
	int nXPos, int nYPos, int nScaleX, int nZoneFlag
	);

__device__ inline unsigned char _ACC_Compare_AxisY_Cross(
	int nXPos, int nYPos, int nScaleY, int nZoneFlag
	);

/**	
@brief	메인 쿠다 프로세스 함수이다.
@param 	검출 결과 이미지 버퍼, 검출 결과 스텝 버퍼 X,Y
-		이미지 폭, 이미지 높이, 패턴 길이 X,Y, 최소 검출 사이즈 X,Y
-		검출 허용치, 외곽의 무시할 영역 두께
@return	없음.(넘겨 받은 포인터 버퍼로 넘겨준다.)
@remark	
-		전체 프로세스를 관리한다.
@author	고정진
@date	2010/4/28  14:54
*/
__global__ void	_ACC_SearchDefectOnBlock_Cross(	
	unsigned char *p_chSearchResult,
	unsigned char *p_chSizeFilterBuff,
	unsigned char *p_chSearchLineX	,
	unsigned char *p_chSearchLineY			
	)
{
	unsigned char	chCompareValue = '\0';
	bool	bSearchResult	= PIXEL_DIFF;
	bool	bFindDead		= NORMAL;
	int		nAxis_X			= 0;
	int		nAxis_Y			= 0;	
	int		nTargetStep	= 0;
	int		nScaleX			= 0;
	int		nScaleY			= 0;
	int		nZoneFlag		= 0;

	nAxis_X = (blockIdx.x * blockDim.x) + threadIdx.x;	


	//기본 검출 프로세스
	if(nAxis_X >= stMaskInfo.nOutLineLeft  && nAxis_X < (stMaskInfo.nImageWidth - stMaskInfo.nOutLineRight))
	{
		nScaleX = (int)(tex1Dfetch( texScaleBuffX, nAxis_X ));
		nScaleY = (int)(tex1Dfetch( texScaleBuffY, nAxis_X ));
		nZoneFlag = (int)(tex1Dfetch( texZoneFlag, nAxis_X ));

		for( nAxis_Y = stMaskInfo.nOutLineUp; nAxis_Y < stMaskInfo.nImageHeight - stMaskInfo.nOutLineDown; nAxis_Y++ )
		{
			nTargetStep = nAxis_X + (stMaskInfo.nImageWidth * nAxis_Y) + (stMaskInfo.nImageOffset * nAxis_Y);

			chCompareValue =	_ACC_Compare_AxisY_Cross( nAxis_X, nAxis_Y, nScaleY, nZoneFlag );

			if( nScaleX != SCALE_ZERO && chCompareValue != LIVE_PIXEL ) //반복 단위가 0 이 아니라면...
				chCompareValue =	_ACC_Compare_AxisX_Cross(	 nAxis_X, nAxis_Y, nScaleX, nZoneFlag ); //X축 비교를 수행한다.				

			p_chSearchResult[nTargetStep] = chCompareValue;
			p_chSizeFilterBuff[nTargetStep] = chCompareValue;	
		}
	}
	__syncthreads();
	//cutilSafeCall( cudaThreadSynchronize() );
	//필터 처리 프로세스
	for( nAxis_Y = stMaskInfo.nOutLineUp; nAxis_Y < stMaskInfo.nImageHeight - stMaskInfo.nOutLineDown; nAxis_Y++ )
	{
		///<X 축 방향의 인접 필터
		bSearchResult = _ACC_Eraser_PixelX(	p_chSearchResult, p_chSizeFilterBuff, nAxis_X, nAxis_Y );
		///<Y출 방향의 인접 필터
		bSearchResult = _ACC_Eraser_PixelY(	p_chSearchResult, p_chSizeFilterBuff, 	nAxis_X, nAxis_Y );

		//Edge 처리 프로세스
		///<해당 픽셀이 Defect 로 판정 되었다면
		if( bSearchResult == DEFECT )
		{
			///<에지 처리 프로세스를 수행하라.
			//bSearchResult = _ACC_EdgeIgnore(	p_chSearchResult, nAxis_X, nAxis_Y );
			///<해당 픽셀이 Defect 로 판정 되었다면			
			///<코너 처리 프로세스를 수행하라.
			bSearchResult = _ACC_CornerDetect_NW_SE(	p_chSearchResult, nAxis_X, nAxis_Y );			
			///<해당 픽셀이 Defect 로 판정 되었다면	

			if( bSearchResult == DEFECT )
			{	
				///<코너 처리 프로세스를 수행하라.
				bSearchResult = _ACC_CornerDetect_NE_SW(	p_chSearchResult, nAxis_X, nAxis_Y );			
				///<해당 픽셀이 Defect 로 판정 되었다면	
				///<해당 열에서 Defect 가 발견되었음을 알린다.				
				if( bSearchResult == DEFECT )
				{
					bFindDead = DEFECT;
					p_chSearchLineY[nAxis_Y] = DEAD_PIXEL;
				}				
			}
		}
	}

	///<해당 열에서 Defect 가 발견되었다면, 해당 버퍼 인덱스에 세팅한다.
	if( bFindDead )
		p_chSearchLineX[nAxis_X] = DEAD_PIXEL;
	//else
	//p_chSearchLineX[nAxis_X] = LIVE_PIXEL;
	__syncthreads();

}
/**	
@brief	최초 검출 함수 이다.
@param 	검출 결과 이미지 버퍼, 이미지 폭, 이미지 높이, 패턴 길이 X,Y, 
-		검출 허용치, 외곽의 무시할 영역 두께, 연산을 수행할 좌표
@return	비교 결과.(PIXEL_EQUAL : 정상, PIXEL_DIFF : 이상)
@remark
-		X축 방향 3회,  Y축 방향 3회 최대 6회 수행한다.
@author	고정진
@date	2010/4/28  16:30
*/
__device__ inline unsigned char _ACC_Compare_AxisX_Cross(																
	int nXPos, int nYPos, int nScaleX, int nZoneFlag
	)
{	
	unsigned char	chPixelData = '\0';
	unsigned char	chComparePixel = '\0';	
	unsigned char	chCompareValue = '\0';
	int				nComparePosX  = 0;
	int				nComparePosY  = 0;

	chPixelData = tex2D( texSourceImage, nXPos, nYPos );		

	if( chPixelData == DEAD_PIXEL )
		return LIVE_PIXEL;

	int		nCompareStep	= 1;
	int		nTempPosX		= 0;
	//해당 좌표가 중앙에서 좌측이라면 비교군은 우측에서 선택한다.
	if( nXPos <= stMaskInfo.nImageWidth / 2 )
	{		
		if( nXPos - nScaleX - stMaskInfo.nOutLineLeft > 0 )		
			nCompareStep = nCompareStep * (-1);		

		for( int nPoint = nCompareStep; nPoint < (nCompareStep + TOTAL_INSPECT_COUNT); nPoint++ )
		{
			if( nPoint == 0 )	continue;

			nTempPosX = nScaleX * nPoint;			

			if( nTempPosX + nXPos + stMaskInfo.nOutLineRight >= stMaskInfo.nImageWidth )	break;			

			nComparePosY = nYPos;
			for( int i = -1 ; i <= 1 ; i++ )
			{					
				nComparePosX = nXPos + nTempPosX + i;						
				chComparePixel = tex2D( texSourceImage, nComparePosX, nComparePosY );
				//White Skip
				if( chComparePixel == DEAD_PIXEL )
					continue;

				chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel, nZoneFlag );

				if( chCompareValue == LIVE_PIXEL )	
					return LIVE_PIXEL;					
			}

			nComparePosX = nXPos + nTempPosX;
			for( int j = -1; j <= 1 ; j++ )
			{				
				nComparePosY = nYPos + j;					
				chComparePixel = tex2D( texSourceImage, nComparePosX, nComparePosY );
				//White Skip
				if( chComparePixel == DEAD_PIXEL )
					continue;

				chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel, nZoneFlag );

				if( chCompareValue == LIVE_PIXEL )	
					return LIVE_PIXEL;							
			}
		}
	}
	//해당 좌표가 중앙에서 우측이라면 비교군은 좌측에서 선택한다.
	else// if(nXPos>=IMAGE_X_SCALE/2)
	{		
		if( nXPos + nScaleX + stMaskInfo.nOutLineRight >= stMaskInfo.nImageWidth )		
			nCompareStep = nCompareStep * (-1);		

		for( int nPoint = nCompareStep; nPoint > (nCompareStep - TOTAL_INSPECT_COUNT); nPoint-- )
		{
			if( nPoint == 0 )	continue;

			nTempPosX = nScaleX * nPoint;

			if( nTempPosX + nXPos - stMaskInfo.nOutLineLeft <= 0 )		break;				

			nComparePosY = nYPos;	
			for( int i=-1; i <= 1; i++ )
			{					
				nComparePosX = nXPos + nTempPosX + i;					
				chComparePixel = tex2D( texSourceImage, nComparePosX, nComparePosY );
				//White Skip
				if( chComparePixel == DEAD_PIXEL )
					continue;

				chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel, nZoneFlag );

				if( chCompareValue == LIVE_PIXEL )	
					return LIVE_PIXEL;				
			}

			nComparePosX = nXPos + nTempPosX;
			for( int j=-1; j <= 1;j++ )
			{				
				nComparePosY = nYPos + j;					
				chComparePixel = tex2D( texSourceImage, nComparePosX, nComparePosY );
				//White Skip
				if( chComparePixel == DEAD_PIXEL )
					continue;

				chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel, nZoneFlag );

				if( chCompareValue == LIVE_PIXEL )	
					return LIVE_PIXEL;		
			}
		}	
	}
	
	return chCompareValue;
}

/**	
@brief	최초 검출 함수 이다.
@param 	검출 결과 이미지 버퍼, 이미지 폭, 이미지 높이, 패턴 길이 X,Y, 
-		검출 허용치, 외곽의 무시할 영역 두께, 연산을 수행할 좌표
@return	비교 결과.(PIXEL_EQUAL : 정상, PIXEL_DIFF : 이상)
@remark
-		X축 방향 3회,  Y축 방향 3회 최대 6회 수행한다.
@author	고정진
@date	2010/4/28  16:30
*/
__device__ inline unsigned char _ACC_Compare_AxisY_Cross(
	int nXPos, int nYPos, int nScaleY, int nZoneFlag
	)
{
	unsigned char	chPixelData	= '\0';
	unsigned char	chComparePixel = '\0';
	unsigned char	chCompareValue = '\0';
	int				nComparePosX  = 0;
	int				nComparePosY  = 0;	

	chPixelData = tex2D( texSourceImage, nXPos, nYPos );

	if( chPixelData == DEAD_PIXEL )
		return LIVE_PIXEL;

	int nTempPosY		= 0;
	int nCompareStep	= 1;

	//해당 좌표가 중앙에서 상측이라면 비교군은 하측에서 선택한다.
	if( nYPos <= stMaskInfo.nImageHeight / 2 )
	{
		if( nYPos - nScaleY - stMaskInfo.nOutLineUp > 0 )
			nCompareStep = nCompareStep * (-1);

		//Y 축 비교는 Y 축 범위 안에서 3회 실시.
		for( int nPoint = nCompareStep; nPoint < (nCompareStep + TOTAL_INSPECT_COUNT); nPoint++ )
		{
			if( nPoint == 0 )	continue;

			nTempPosY = nScaleY * nPoint;

			if( nTempPosY + nYPos + stMaskInfo.nOutLineDown >= stMaskInfo.nImageHeight )	return DEAD_PIXEL;

			nComparePosY = nYPos + nTempPosY;			
			for( int i = -1; i <= 1; i++ )
			{				
				nComparePosX = nXPos + i;						
				chComparePixel = tex2D( texSourceImage, nComparePosX, nComparePosY );
				//White Skip
				if( chComparePixel == DEAD_PIXEL )
					continue;

				chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel, nZoneFlag );

				if( chCompareValue == LIVE_PIXEL )	
					return LIVE_PIXEL;					
			}
			
			nComparePosX = nXPos;
			for( int j = -1; j <= 1; j++ )
			{				
				nComparePosY = nYPos + nTempPosY + j;
				chComparePixel = tex2D( texSourceImage, nComparePosX, nComparePosY );
				//White Skip
				if( chComparePixel == DEAD_PIXEL )
					continue;

				chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel, nZoneFlag );

				if( chCompareValue == LIVE_PIXEL )	
					return LIVE_PIXEL;						
			}
		}
	}
	//해당 좌표가 중앙에서 하측이라면 비교군은 상측에서 선택한다.
	else// if(nYPos>=IMAGE_Y_SCALE/2)
	{		
		if( nYPos + nScaleY + stMaskInfo.nOutLineDown >= stMaskInfo.nImageHeight)
			nCompareStep = nCompareStep * (-1);
		//Y 축 비교는 Y 축 범위 안에서 3회 실시.
		for( int nPoint = nCompareStep; nPoint > (nCompareStep - TOTAL_INSPECT_COUNT); nPoint--)
		{
			if( nPoint == 0)	continue;

			nTempPosY = nScaleY * nPoint;

			if( nTempPosY + nYPos - stMaskInfo.nOutLineUp <= 0 )	return DEAD_PIXEL;

			nComparePosY = nYPos + nTempPosY;				
			for( int i = -1; i <= 1; i++ )
			{					
				nComparePosX = nXPos + i;
				chComparePixel = tex2D( texSourceImage, nComparePosX, nComparePosY );
				//White Skip
				if( chComparePixel == DEAD_PIXEL )
					continue;

				chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel, nZoneFlag );

				if( chCompareValue == LIVE_PIXEL )	
					return LIVE_PIXEL;								
			}
				
			nComparePosX = nXPos;
			for( int j = -1; j <= 1; j++ )
			{				
				nComparePosY = nYPos + nTempPosY + j;	
				chComparePixel = tex2D( texSourceImage, nComparePosX, nComparePosY );
				//White Skip
				if( chComparePixel == DEAD_PIXEL )
					continue;

				chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel, nZoneFlag );

				if( chCompareValue == LIVE_PIXEL )	
					return LIVE_PIXEL;									
			}
		}
	}

	return chCompareValue;
}



#endif	