/*
*	File Name			:	LinkedList.h
*	Function				:	Linked List 의 template Class
*								저장하는 구조체 선언.
*	Create				:	2008.02.22
*	Author				:	고정진
*	Version				:	1.0
*/
#pragma once

#ifndef		LINKEDLIST_H
#define		LINKEDLIST_H


/*
*	Module name		:	NODE_DATA
*	Function				:	정의 되지 않은 데이터 노드와 다음 노드를 가리키는 어드레스 정의						
*	Create				:	2008.02.26
*	Author				:	고정진
*	Version				:	1.0
*/
template <typename T>
struct NODE_DATA
{
	T	st_DataList;
	//int	nPreviousNode;
	INT_PTR nNextNode;
	NODE_DATA()
	{
		memset(this, 0x00, sizeof(NODE_DATA));
	}
};

/*
*	Module name		:	LinkedList
*	Function				:	정의 되지 않은 데이터에 대한, Linked List 정의.
*	Create				:	2008.02.26
*	Author				:	고정진
*	Version				:	1.0
*/
template <typename T>
class TemplateQueue
{
public:
	TemplateQueue();	
	~TemplateQueue(void);
	
	void			m_PushNode(T m_ReadData);
	int				m_PopNode(T* PopData, int nSize);	
	int				m_NodeCount();	
protected:
	int*			M_GNODE_COUNT;			//전체 노드의 갯수.
	INT_PTR*			M_GREAD_ADDRESS;		    //다음 노드를 가리키는 어드레스	
	NODE_DATA<T>*	m_ST_FirstNode;		//시작 노드를 저장.
	NODE_DATA<T>*	m_ST_LastNode;		//항상 마지막 노드를 저장.	
	NODE_DATA<T>*	m_ST_NewNode;		//새로의 노드의 생성.
	NODE_DATA<T>*	m_ST_CurrentNode;	//현재 노드	

private:
	static const int ITEM_NOT_EXIST	= -1;
};

/*
*	Module Name	:	LinkedList
*	Parameter		:	노드 카운트 초기화.
*	Return			:	None
*	Function			:	생성자, 노드 카운트 초기화.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
TemplateQueue<T>::TemplateQueue()
{	
	M_GNODE_COUNT	= new int;
	M_GREAD_ADDRESS = new INT_PTR;
	*M_GREAD_ADDRESS= 0;
	*M_GNODE_COUNT	= 0;

	m_ST_FirstNode	= 0;
	m_ST_LastNode	= 0;
	m_ST_NewNode	= 0;
	m_ST_CurrentNode= 0;
}


/*
*	Module Name	:	LinkedList
*	Parameter		:	없음.
*	Return			:	없음
*	Function			:	소멸자. 노드 자원의 수거.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
TemplateQueue<T>::~TemplateQueue(void)
{
	*M_GREAD_ADDRESS = (INT_PTR)m_ST_FirstNode;	//처음 노드의 포인터를 받아온다.

	INT_PTR ns = 0;
	int nc = *M_GNODE_COUNT;							//전체 노드의 개수를 받는다.
	INT_PTR ng = *this->M_GREAD_ADDRESS;				//현재 노드의 포인터 주소를 받는다.
	
	
	while( ns < nc)
	{
		if( ng != NULL )
		{
			INT_PTR *pIntx;
			INT_PTR Intx =10;
			pIntx = &Intx;
			INT_PTR IntxZ =*pIntx;
			INT_PTR *pIntxC =pIntx;

			pIntx = this->M_GREAD_ADDRESS;
			
			 m_ST_CurrentNode = (NODE_DATA<T>*)(*this->M_GREAD_ADDRESS);	
			//*((int*)&m_ST_CurrentNode) = *this->M_GREAD_ADDRESS;
			*M_GREAD_ADDRESS = m_ST_CurrentNode->nNextNode;
			delete m_ST_CurrentNode;
		}

		ns++;

		if( ns == nc)
			break;
	}

	delete	M_GNODE_COUNT;
	delete	M_GREAD_ADDRESS;	
}

/*
*	Module Name	:	m_SetNextNode
*	Parameter		:	정의 되지 않은 데이터
*	Return			:	None
*	Function			:	노드의 추가
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
void TemplateQueue<T>::m_PushNode(T m_ReadData)
{
	//_CrtSetBreakAlloc(205);
	m_ST_NewNode = new NODE_DATA<T>;
	int nc = *M_GNODE_COUNT;
	
	if( nc != NULL)
	{
		m_ST_LastNode->nNextNode = (INT_PTR)m_ST_NewNode;
		//m_ST_NewNode->nPreviousNode = (int)m_ST_LastNode;
		m_ST_NewNode->nNextNode = 0;	
	}
	else
	{
		m_ST_FirstNode = m_ST_NewNode;
	}
	
	m_ST_LastNode = m_ST_NewNode;

	void*	dest	= (void*)&m_ST_NewNode->st_DataList;
	void*	src		= (void*)&m_ReadData;
	int		nLength	= sizeof(T);

	memcpy( dest, src, nLength );

	(*M_GNODE_COUNT)++;
	
}

/*
*	Module Name	:	m_ReadNextNode
*	Parameter		:	NONE
*	Return			:	정의 되지 않은 노드 데이터
*	Function			:	다음 노드의 데이터.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
int TemplateQueue<T>::m_PopNode(T* PopData, int nSize)
{
	//*(int*)&m_ST_CurrentNode = *this->M_GREAD_ADDRESS;
	//m_ST_CurrentNode = (NODE_DATA<T>*)(*this->M_GREAD_ADDRESS);	
	//*M_GREAD_ADDRESS = m_ST_CurrentNode->nNextNode;

	if(*M_GNODE_COUNT <= 0)
	{
		memset(PopData,0x00,nSize);
		*M_GNODE_COUNT = 0;
		return ITEM_NOT_EXIST;
	}
	m_ST_CurrentNode = m_ST_FirstNode;	
	memcpy(PopData,m_ST_FirstNode,nSize);
	
	m_ST_FirstNode	 = (NODE_DATA<T>*)(m_ST_FirstNode->nNextNode);
	
	delete m_ST_CurrentNode;
	(*M_GNODE_COUNT)--;
	return *M_GNODE_COUNT;
}


/*
*	Module Name	:	m_NodeCount
*	Parameter		:	없음
*	Return			:	노드의 갯수
*	Function			:	노드의 갯수 반환.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
int TemplateQueue<T>::m_NodeCount()
{
	return *M_GNODE_COUNT;	
}

#endif