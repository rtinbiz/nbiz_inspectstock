/**
@file		PadMainInspector.cpp
@brief	메인 프로세스
@remark	
-			
@author	고정진
@date	2012/1/19  18:11
*/
#include "StdAfx.h"
#include "PadMainInspector.h"


/**	
@brief	생성자
@param 	없음
@return	없음
@remark
-			내부 버퍼 초기화
@author	고정진
@date	2010/4/28  18:11
*/
CPadMainInspector::CPadMainInspector(void)
{
// 	m_chSourceImage = nullptr;
// 	m_chResultImage = nullptr;	
	for( int nStep = 0; nStep < PAD_BUFF_COUNT; nStep++)
	{
		m_stPadObj.cObjectPointer[nStep] = nullptr;
		m_stPadObj.chXLineResult[nStep] = nullptr;
		m_stPadObj.chYLineResult[nStep] = nullptr;
		m_stPadObj.chResultImage[nStep] = nullptr;
	}
	byStandard1 = nullptr;
	byStandard2 = nullptr;
	m_cvResultImage = nullptr;
	m_nImageWidth = 0;
	m_nImageHeight = 0;
	fnAccProcessCall[0].fnAccCall = _ACC_Pad_SearchDefect;
}

/**	
@brief	소멸자
@param 	없음
@return	없음
@remark
-			내부 버퍼 수거.
@author	고정진
@date	2010/4/28  18:11
*/
CPadMainInspector::~CPadMainInspector(void)
{
	if ( m_stPadObj.chResultImage[0] != nullptr)
	{
// 		delete[] m_chSourceImage;
// 		delete[] m_chResultImage;

		for( int nStep = 0; nStep < PAD_BUFF_COUNT; nStep++)
		{
			delete[] m_stPadObj.chXLineResult[nStep];
			delete[] m_stPadObj.chYLineResult[nStep];
			delete[] m_stPadObj.chResultImage[nStep];	

			m_stPadObj.chXLineResult[nStep] = nullptr;
			m_stPadObj.chYLineResult[nStep] = nullptr;
			m_stPadObj.chResultImage[nStep] = nullptr;
		}	

		cvReleaseImage(&m_cvResultImage);
// 		m_chSourceImage = nullptr;
// 		m_chResultImage = nullptr;
		m_cvResultImage = nullptr;				
	}

}

/**	
@brief	외부 파라미터 셑
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	없음
@remark
-		
@author	고정진
@date	2010/4/28  18:11
*/
void	CPadMainInspector::m_fnParameterSet(	SAOI_PAD_IMAGE_INFO	 sMaskImage,		
																SAOI_PAD_COMMON_PARA sCommon
																)
{	
	m_stMaskImageInfo	= sMaskImage;
	m_stCommonPara		= sCommon;
	//메모리 생성
	m_fnDataBufferCreate();
 	
 	m_cImageInfo.m_fnMemberDataSet(m_stMaskImageInfo,m_stCommonPara);	
}


/**	
@brief	버퍼생성
@param 	없음
@return	없음
@remark
-			처리에 쓰일 버퍼를 생성한다.
-			버퍼의 크기가 바뀔때만 재생성한다.
@author	고정진
@date	2012/12/15  18:11
*/
void	CPadMainInspector::m_fnDataBufferCreate()
{		
	//메모리 생성
	if ( m_stCommonPara.nImageWidth != this->m_nImageWidth || m_stCommonPara.nImageHeight != this->m_nImageHeight )
	{
		m_fnDataBufferDelete();
		this->m_nImageWidth = m_stCommonPara.nImageWidth;
		this->m_nImageHeight = m_stCommonPara.nImageHeight;
		this->m_nImageSize = this->m_nImageWidth * this->m_nImageHeight;

		for( int nStep = 0; nStep < PAD_BUFF_COUNT; nStep++)
		{
			m_stPadObj.chXLineResult[nStep]  = new unsigned char[this->m_nImageWidth];
			m_stPadObj.chYLineResult[nStep]  = new unsigned char[this->m_nImageHeight];
			m_stPadObj.chResultImage[nStep]  = new unsigned char[this->m_nImageSize];
			memset(m_stPadObj.chXLineResult[nStep], 0x00, this->m_nImageWidth);
			memset(m_stPadObj.chYLineResult[nStep], 0x00, this->m_nImageHeight);
			memset(m_stPadObj.chResultImage[nStep], 0x00, this->m_nImageSize);
		}	

		m_cvResultImage = cvCreateImage(cvSize(this->m_nImageWidth, this->m_nImageHeight), IPL_DEPTH_8U, CHANNEL_GRAY);
	}	
}


/**	
@brief	버퍼삭제
@param 	없음
@return	없음
@remark
-			현재 버퍼가 생성되어 있다면 파괴한다.
@author	고정진
@date	2012/12/15  18:11
*/
void	CPadMainInspector::m_fnDataBufferDelete()
{		
	if ( m_stPadObj.chResultImage[0] != nullptr)
	{		
		for( int nStep = 0; nStep < PAD_BUFF_COUNT; nStep++)
		{
			delete[] m_stPadObj.chXLineResult[nStep];
			delete[] m_stPadObj.chYLineResult[nStep];
			delete[] m_stPadObj.chResultImage[nStep];	

			m_stPadObj.chXLineResult[nStep] = nullptr;
			m_stPadObj.chYLineResult[nStep] = nullptr;
			m_stPadObj.chResultImage[nStep] = nullptr;
		}	

		cvReleaseImage(&m_cvResultImage);	
		m_cvResultImage = nullptr;		
	}

}

/**	
@brief	로그 기록 모드 셑
@param 	flag
@return	없음
@remark
-			디버그 모드에서만 수행한다.
@author	고정진
@date	2010/4/28  18:11
*/
#ifdef _DEBUG
void	CPadMainInspector::m_fnStepLogFlagSet(bool bSetFlag)
{
	m_cImageInfo.m_gLogFlag = bSetFlag;
}
#endif


/**	
@brief	싱글 Defect 검출 함수
@param 	Image Source Data
@return	Defect Data Buffer Pointer
@remark
-			하나의 이미지에 데이터에 대하여 검출을 수행한다.
-			CPU 에서 처리하며, 테스트 목적의 코드이다.
@author	고정진
@date	2010/4/28  18:11
*/
int*	CPadMainInspector::m_fnDefectSearch(unsigned char* byImageData,
															unsigned char* byImageMask1,
															unsigned char* byImageMask2)
{	
	bool bResult = false;	

	int* npDefectPos = nullptr;	

	return npDefectPos;
}

/**	
@brief	싱글 Defect 검출 함수
@param 	Image Source Data
@return	Defect Data Buffer Pointer
@remark
-			하나의 이미지에 데이터에 대하여 검출을 수행한다.
-			GPU 에서 처리하며, 테스트 목적의 코드이다.
@author	고정진
@date	2010/4/28  18:11
*/
int*	CPadMainInspector::m_fnDefectSearch_ACC(unsigned char* byImageData,
																	unsigned char* byImageMask1,
																	unsigned char* byImageMask2)
{	
	bool bResult = false;	
	
	_ACC_Pad_SetArray(byImageMask1, byImageMask2);

	fnAccProcessCall[0].fnAccCall( byImageData, m_stPadObj.chResultImage[0], m_stPadObj.chXLineResult[0], m_stPadObj.chYLineResult[0] );

	memcpy(m_cvResultImage->imageData, m_stPadObj.chResultImage[0], this->m_nImageWidth * this->m_nImageHeight);
	int nResult	= cvSaveImage("C:\\G_Result.BMP", m_cvResultImage);		

	m_cImageInfo.m_fnResultDefectPos( m_stPadObj.chResultImage[0], m_stPadObj.chXLineResult[0], m_stPadObj.chYLineResult[0] );
	//m_cImageInfo.m_fnSortingDefectPos();
	m_cImageInfo.m_fnNorDefectCreate();	

	int* npDefectPos = nullptr;	

	npDefectPos = m_cImageInfo.m_fnGetSearchResultData();
	return npDefectPos;
}

/**	
@brief	_ACC Class 에 접근하는 함수이다.
@param 	SOBJECT_STR
@return	없음
@remark
-			Main _ACC Thread 에서 호출 되어지는 함수이다.
-			처리 중간 필터를 버퍼 관리자에 적재한다.
@author	고정진
@date	2010/4/28  18:11
*/
void	CPadMainInspector::m_fnDefectSearch_ACCT(SOBJECT_STR sObject_Str)
{

}


/**	
@brief	후처리 함수
@param 	SRESOBJECT_STR
@return	없음
@remark
-			Main Postprocess Thread 에서 호출되어지는 함수이다.
-			후처리 함수들을 관리한다.
@author	고정진
@date	2010/4/28  18:11
*/
void	CPadMainInspector::m_fnDefectResult_ACCT(SRESOBJECT_STR sResObject_Str)
{
	bool bResult = false;

	VSBridge		cInspectDataQ;
	cInspectDataQ = (VSBridge)sResObject_Str.cObjectPointer;

	m_cImageInfo.m_fnResultDefectPos((unsigned char*)cInspectDataQ->m_fnQGetBufferData(), sResObject_Str.chXLineResult, nullptr );
	//m_cImageInfo.m_fnSortingDefectPos();
	m_cImageInfo.m_fnNorDefectCreate();

	int nDefectCount = 0;

	DEFECT_DATA* spDefectData = nullptr;

	nDefectCount = m_cImageInfo.m_fnGetSearchResultCount();	

	spDefectData = 	(DEFECT_DATA*)m_cImageInfo.m_fnGetSearchResultData();

	cInspectDataQ->m_fnQSetDefectAllBuffer(nDefectCount, spDefectData);

	//////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG			
	//cImageInfo.m_fnAddDebugLog(strLog);
#endif

}

/**	
@brief	후처리 함수
@param 	SRESOBJECT_STR
@return	없음
@remark
-			Main Postprocess Thread 에서 호출되어지는 함수이다.
-			후처리 함수들을 관리한다.
@author	고정진
@date	2010/4/28  18:11
*/
void	CPadMainInspector::m_fnDefectResult_ACCT( unsigned char* chResultImage, unsigned char* chXLineResult, unsigned char* chYLineResult, void* vpObject )
{
	bool bResult = false;
	VSBridge		cInspectDataQ;
	cInspectDataQ = (VSBridge)vpObject;

	m_cImageInfo.m_fnResultDefectPos(chResultImage, chXLineResult, chYLineResult );
	//m_cImageInfo.m_fnSortingDefectPos();
	m_cImageInfo.m_fnNorDefectCreate();

	int nDefectCount = 0;

	DEFECT_DATA* spDefectData = nullptr;

	nDefectCount = m_cImageInfo.m_fnGetSearchResultCount();	

	spDefectData = 	(DEFECT_DATA*)m_cImageInfo.m_fnGetSearchResultData();

	cInspectDataQ->m_fnQSetDefectAllBuffer(nDefectCount, spDefectData);

	//////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG			
	//cImageInfo.m_fnAddDebugLog(strLog);
#endif

}


/**	
@brief	패드 버퍼 초기화
@param 	없음
@return	 없음
@remark
-			패드 버퍼 초기화
@author	고정진
@date	2012/1/27  18:11
*/
void	CPadMainInspector::m_fnSetAllComplete()
{
	m_stPadObj.cObjectPointer[0] = nullptr;
	m_stPadObj.cObjectPointer[1] = nullptr;
	m_stPadObj.cObjectPointer[2] = nullptr;
	m_stPadObj.nObjectStatus[0] = FIST_COMP;
	m_stPadObj.nObjectStatus[1] = SECOND_COMP;
	m_stPadObj.nObjectStatus[2] = THIRD_COMP;
}

/**	
@brief	패드 버퍼 초기화
@param 	없음
@return	 없음
@remark
-			패드 버퍼 초기화
@author	고정진
@date	2012/1/27  18:11
*/
void	CPadMainInspector::m_fnSetComplete(int nIndex)
{
	m_stPadObj.cObjectPointer[nIndex] = nullptr;	
	m_stPadObj.nObjectStatus[nIndex] = FIST_COMP;	
}

/**	
@brief	패드 버퍼 초기화
@param 	없음
@return	 없음
@remark
-			패드 버퍼 초기화
@author	고정진
@date	2012/1/27  18:11
*/
void	CPadMainInspector::m_fnSetStartBusy()
{
	m_stPadObj.cObjectPointer[0] = nullptr;	
	m_stPadObj.nObjectStatus[0] = FIST_COMP;		
	m_stPadObj.nObjectStatus[1] = SECOND_BUSY;
	m_stPadObj.nObjectStatus[2] = THIRD_BUSY;
}

/**	
@brief	검출할 오브젝트 전달
@param 	SOBJECT_STR
@return	 없음
@remark
-			검출할 오브젝트 전달
@author	고정진
@date	2012/1/27  18:11
*/
int  CPadMainInspector::m_fnSetObject(SOBJECT_STR sObject_Str)
{
	int nIndex = m_fnGetNullptrIndex();

	if ( nIndex == ERR_PAD_IMAGE_BUFFER )
		return ERR_PAD_IMAGE_BUFFER;

	int nObjStatus = 0;

	m_stPadObj.cObjectPointer[nIndex] = sObject_Str.cObjectPointer;
	
	nObjStatus = (int)pow((double)SECOND_READY, nIndex);
	
	m_stPadObj.nObjectStatus[nIndex] = nObjStatus;
	
	return OKAY;
}

/**	
@brief	전처리 완료된 오브젝트 포인터 전달
@param 	오브젝트 Index
@return	오브젝트 포인터
@remark
-			전처리 완료된 오브젝트 포인터 전달
@author	고정진
@date	2012/1/27  18:11
*/
void*  CPadMainInspector::m_fnGetObject(int nObjectIndex)
{
	return m_stPadObj.cObjectPointer[nObjectIndex];
}

/**	
@brief	Xline Search Result 전달.
@param 	오브젝트 Index
@return	Result 포인터
@remark
-			Xline Search Result 전달.
@author	고정진
@date	2012/1/27  18:11
*/
unsigned char*  CPadMainInspector::m_fnGetXlineResult(int nIndex)
{
	return m_stPadObj.chXLineResult[nIndex];
}

/**	
@brief	Xline Search Result 전달.
@param 	오브젝트 Index
@return	Result 포인터
@remark
-			Xline Search Result 전달.
@author	고정진
@date	2012/1/27  18:11
*/
unsigned char*  CPadMainInspector::m_fnGetYlineResult(int nIndex)
{
	return m_stPadObj.chYLineResult[nIndex];
}

/**	
@brief	Xline Search Result 전달.
@param 	오브젝트 Index
@return	Result 포인터
@remark
-			Xline Search Result 전달.
@author	고정진
@date	2012/1/27  18:11
*/
unsigned char*  CPadMainInspector::m_fnGetResultImage(int nIndex)
{
	return m_stPadObj.chResultImage[nIndex];
}

/**	
@brief	기준 이미지 폭 전달
@param 	없음
@return	이미지 폭
@remark
-			기준 이미지 폭 전달
@author	고정진
@date	2012/1/27  18:11
*/
int  CPadMainInspector::m_fnGetImageWidth()
{
	return m_nImageWidth;
}


/**	
@brief	기준 이미지 폭 전달
@param 	없음
@return	이미지 폭
@remark
-			기준 이미지 폭 전달
@author	고정진
@date	2012/1/27  18:11
*/
int  CPadMainInspector::m_fnGetImageHeight()
{
	return m_nImageHeight;
}


/**	
@brief	비어있는 버퍼 탐색
@param 	없음
@return	 버퍼 index
@remark
-			비어있는 버퍼 탐색
@author	고정진
@date	2012/1/27  18:11
*/
int		CPadMainInspector::m_fnGetNullptrIndex()
{
	int nIndex = 0;
	for( ; nIndex <= 2; nIndex++ )
	{
		if( m_stPadObj.cObjectPointer[nIndex] == nullptr )
			return nIndex;
	}
	return ERR_PAD_IMAGE_BUFFER;
}

/**	
@brief	패드 오브젝트 플래그 리턴
@param 	없음
@return	 SOBJECT_STR_PAD.nObjectStatus
@remark
-			패드 오브젝트 플래그 리턴
@author	고정진
@date	2012/1/27  18:11
*/
int		CPadMainInspector::m_fnGetPadObjFlag()
{
	int nObjFlag = 0;

	nObjFlag = m_stPadObj.nObjectStatus[0] | m_stPadObj.nObjectStatus[1] | m_stPadObj.nObjectStatus[2];

	return nObjFlag;
}

/**	
@brief	0번 index 이미지 inspect
@param 	없음
@return	 없음
@remark
-			0번 index 이미지 inspect
@author	고정진
@date	2012/1/27  18:11
*/
void	CPadMainInspector::m_fnOnePointInspect()
{
	bool bResult = false;
	VSBridge		cInspectDataQ;
	BYTE* byImageData0 = nullptr;

	cInspectDataQ = (VSBridge)m_stPadObj.cObjectPointer[0];
	byImageData0 = cInspectDataQ->m_fnQGetPADImage();

	_ACC_Pad_SetArray((unsigned char*)byStandard1, (unsigned char*)byStandard2);

	fnAccProcessCall[0].fnAccCall( (unsigned char*)byImageData0, 
		m_stPadObj.chResultImage[0], m_stPadObj.chXLineResult[0], m_stPadObj.chYLineResult[0]);
	
	byStandard1 = byStandard2;
	byStandard2 = byImageData0;

}

/**	
@brief	전체 버퍼 inspect
@param 	없음
@return	 없음
@remark
-			전체 버퍼 inspect
-			초기 한번만 실행
@author	고정진
@date	2012/1/27  18:11
*/
void	CPadMainInspector::m_fnAllPointInspect()
{
	void* vpObjPointer = nullptr;

	bool bResult = false;		
	VSBridge	cInspectDataQ;
	BYTE* byImageData0 = nullptr;
	BYTE* byImageData1 = nullptr;
	BYTE* byImageData2 = nullptr;	

	cInspectDataQ = (VSBridge)m_stPadObj.cObjectPointer[0];
	byImageData0 = cInspectDataQ->m_fnQGetPADImage();

	cInspectDataQ = (VSBridge)m_stPadObj.cObjectPointer[1];
	byImageData1 = cInspectDataQ->m_fnQGetPADImage();

	cInspectDataQ = (VSBridge)m_stPadObj.cObjectPointer[2];
	byImageData2 = cInspectDataQ->m_fnQGetPADImage();		

	_ACC_Pad_SetArray((unsigned char*)byImageData0, (unsigned char*)byImageData2);
	fnAccProcessCall[0].fnAccCall( (unsigned char*)byImageData1, m_stPadObj.chResultImage[1], m_stPadObj.chXLineResult[1], m_stPadObj.chYLineResult[1] );	

	_ACC_Pad_SetArray((unsigned char*)byImageData0, (unsigned char*)byImageData1);
	fnAccProcessCall[0].fnAccCall( (unsigned char*)byImageData2,	m_stPadObj.chResultImage[2], m_stPadObj.chXLineResult[2], m_stPadObj.chYLineResult[2] );	

	_ACC_Pad_SetArray((unsigned char*)byImageData1, (unsigned char*)byImageData2);
	fnAccProcessCall[0].fnAccCall( (unsigned char*)byImageData0, 	m_stPadObj.chResultImage[0], m_stPadObj.chXLineResult[0], m_stPadObj.chYLineResult[0] );	

	byStandard1 = byImageData1;
	byStandard2 = byImageData2;

}


/**	
@brief	Main _ACC Thread
@param 	(THREAD_PARAM*)LPVOID;
@return	Normal Code
@remark
-			전처리 메인 스레드 이다.
@author	고정진
@date	2010/4/28  18:11
*/
unsigned int __stdcall	CPadMainInspector::THREAD_PRE_PROC_PAD_ACC(LPVOID pParam)
{
	THREAD_PARAM*			pThreadParam		= (THREAD_PARAM*)pParam;
	CPadMainInspector*		cInspector			= nullptr;
	CPadThreadCreator*		cThreadCreator	= nullptr;
	
	UINT				unThreadStat	= 0;
	unsigned	int		unBlockNum		= 0;
	unsigned	int		unImageIndex	= 0;
	int					THREAD_NUM	= 0;
	int					nPadObjFlag	= 0;

	cInspector		= (CPadMainInspector*)pThreadParam->vpIncpectorClass;
	cThreadCreator	= (CPadThreadCreator*)pThreadParam->vpThreadCreator;	

	SOBJECT_STR			sObject_Str;
	SRESOBJECT_STR		sResObject_Str;
	int nReturn		= 0;	

	THREAD_NUM = pThreadParam->nThreadNumber - 1;

	while(cThreadCreator->m_fnGetThreadFlag())	
	{
		if( cThreadCreator->m_fnGetProcessFlag() )
		{
			if( cThreadCreator->m_fnGetProcessCount()  <= 0 )
				cThreadCreator->m_fnSetProcessFlag(false);

			nReturn = cThreadCreator->m_fnPopTemplateQNode(&sObject_Str);		
			unThreadStat = unThreadStat & THREAD_AREA;

			if(nReturn == ITEM_NOT_EXIST)
			{			
				Sleep(1);				
				cInspector->m_fnSetAllComplete();
				continue;
			}

			unThreadStat	= 0;

			nReturn = cInspector->m_fnSetObject(sObject_Str);
			if ( nReturn == ERR_PAD_IMAGE_BUFFER )
				break;
						
			if( cInspector->m_fnGetPadObjFlag() == READY1BUSY2 )
			{
				cInspector->m_fnOnePointInspect();

				cThreadCreator->m_fnSetResultBuffer(
										cInspector->m_fnGetResultImage(0), 
										cInspector->m_fnGetXlineResult(0), 
										cInspector->m_fnGetYlineResult(0), 
										cInspector->m_fnGetObject(0));

				cInspector->m_fnSetComplete(0);
				//continue;
			}

			if( cInspector->m_fnGetPadObjFlag() == ALL_READY )
			{
				cInspector->m_fnAllPointInspect();

				cThreadCreator->m_fnSetResultBuffer(
										cInspector->m_fnGetResultImage(0), 
										cInspector->m_fnGetXlineResult(0), 
										cInspector->m_fnGetYlineResult(0), 
										cInspector->m_fnGetObject(0));

				cThreadCreator->m_fnSetResultBuffer(
										cInspector->m_fnGetResultImage(1), 
										cInspector->m_fnGetXlineResult(1), 
										cInspector->m_fnGetYlineResult(1), 
										cInspector->m_fnGetObject(1));

				cThreadCreator->m_fnSetResultBuffer(
										cInspector->m_fnGetResultImage(2), 
										cInspector->m_fnGetXlineResult(2), 
										cInspector->m_fnGetYlineResult(2), 
										cInspector->m_fnGetObject(2));
					

				cInspector->m_fnSetStartBusy();

				//continue;
			}			
		}
		else
		{
			Sleep(1);
		}
	}

	_endthreadex(0);

	delete cInspector;
	return 0;
}

/**	
@brief	메인 후처리 스레드
@param 	(THREAD_PARAM*)pParam;
@return	Normal Code
@remark
-		
@author	고정진
@date	2010/4/28  18:11
*/
unsigned int __stdcall CPadMainInspector::THREAD_POST_PROC_PAD_ACC(LPVOID pParam)
{
	THREAD_PARAM*			pThreadParam		= (THREAD_PARAM*)pParam;
	CPadMainInspector*		cInspector			= nullptr;
	CPadThreadCreator*		cThreadCreator	= nullptr;	
	VSBridge						cInspectDataQ		= nullptr;

	UINT				unThreadStat	= 0;
	unsigned	int		unBlockNum		= 0;
	unsigned	int		unImageIndex	= 0;
	int					THREAD_NUM	= 0;
	int					OBJECT_INDEX = 0;
	cInspector			= (CPadMainInspector*)pThreadParam->vpIncpectorClass;
	cThreadCreator	= (CPadThreadCreator*)pThreadParam->vpThreadCreator;	
	SRESOBJECT_STR sResObject_Str;

	int nReturn = 0;	
	bool bBufferFlag = COMP;
	//int nCountSum = 0;
	THREAD_NUM = pThreadParam->nThreadNumber - 1;
	OBJECT_INDEX = THREAD_NUM - 1;
	while(cThreadCreator->m_fnGetThreadFlag())
	{		

		//nReturn = cThreadCreator->m_fnPopTemplateResQNode(&sResObject_Str);
		bBufferFlag = cThreadCreator->m_fnGetBufferFlag(OBJECT_INDEX);
		//unThreadStat = unThreadStat & THREAD_AREA;

		if(bBufferFlag == COMP)
		{
			Sleep(1);			
			continue;
		}		

		unThreadStat	= 0;	

		cInspectDataQ	= (VSBridge)sResObject_Str.cObjectPointer;

		cInspector->m_fnDefectResult_ACCT(cThreadCreator->m_fnGetResultImage(OBJECT_INDEX), 
															cThreadCreator->m_fnGetXLineResult(OBJECT_INDEX), 
															cThreadCreator->m_fnGetYLineResult(OBJECT_INDEX), 
															cThreadCreator->m_fnGetObjectPointer(OBJECT_INDEX));	

		cThreadCreator->m_fnSetBufferFlag(COMP, OBJECT_INDEX);
	}

	_endthreadex(0);
	delete cInspector;
	return 0;
}