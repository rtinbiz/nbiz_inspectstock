/**
@file	AnaimCuda.cu
@brief	Cuda 메인 파일
@remark	
@author	고정진
@date	2010/4/19  10:53
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <cuda_runtime.h>
#include <helper_cuda.h>

#include "AnaimParallel_Kernel_Pad.cuh"
#include "AnaimParallel_Kernel_Pad_Direct.cuh"
#include "AnaimParallel_Kernel_Pad_Aromatic.cuh"
#include "AnaimParallel_Kernel_Pad_Cross.cuh"
#include "AnaimParallel_Kernel_Pad_Star.cuh"
#include "AnaimParallel_Kernel_Pad_5Star.cuh"
#include "AnaimParallel_Kernel_Pad_7Star.cuh"

static  int	nPadCurrentWidth		= 0;				///<이미지 폭
static  int	nPadCurrentHeight	= 0;				///<이미지 높이
static int	nPadImageSize			= 0;				///<이미지 사이즈

SAOI_PAD_IMAGE_INFO			stPadMaskInformation;		///<이미지 정보 구조체 이다.
SAOI_PAD_COMMON_PARA		stPadCommonPara;
SACC_IMAGE_INFO				stAccPadImageInfo;

unsigned char				chPadHThresholdTemp[256];
static cudaArray*			arrPadSourceImage1 = NULL;	///<메인 이미지 어레이 구조를 담는다.
static cudaArray*			arrPadSourceImage2 = NULL;	///<메인 이미지 어레이 구조를 담는다.
static cudaArray*			arrPadSourceImage3 = NULL;	///<메인 이미지 어레이 구조를 담는다.
static unsigned char*	chPadSearchResult = NULL;	///<결과 데이터를 보관할 그래픽 메모리 버퍼를 생성한다.
static unsigned char*	chPadSearchXLine  = NULL;	///<결과 데이터를 보관할 그래픽 메모리 버퍼를 생성한다.
static unsigned char*	chPadSearchYLine  = NULL;	///<결과 데이터를 보관할 그래픽 메모리 버퍼를 생성한다.
static unsigned char*	chPadSizeFilterBuff = NULL;	///<Image Data 저장소


//Function Declare Start
void _ACC_Pad_CreateTextureData(SAOI_PAD_COMMON_PARA sCommon);
void _ACC_Pad_DeleteTextureData();
//Function Declare End


/**	
@brief	파라미터 설정.
@param	SMASK_AOI_IMAGEINFO
@return	없음
@remark	
-		_ACC Process 에서 사용하는 기본 파라미터로
		자세한 정보는 _ACCStructure.h 파일을 참고한다.
@author	고정진
@date	2010/4/19  11:39
*/
extern "C"
void _ACC_Pad_SetParameter(SAOI_PAD_IMAGE_INFO stMaskInfo, SAOI_PAD_COMMON_PARA stCommon )
{
	stPadMaskInformation		= stMaskInfo;
	stPadCommonPara				= stCommon;

	stAccPadImageInfo.nScaleX1 = stMaskInfo.nScaleXReserve1;
	stAccPadImageInfo.nScaleY1 = stMaskInfo.nScaleYReserve1;
	stAccPadImageInfo.nOutLineLeft = stMaskInfo.nOutLineLeft;
	stAccPadImageInfo.nOutLineRight = stMaskInfo.nOutLineRight;
	stAccPadImageInfo.nOutLineUp = stMaskInfo.nOutLineUp;
	stAccPadImageInfo.nOutLineDown = stMaskInfo.nOutLineDown;
	stAccPadImageInfo.nMinimumSizeX = stMaskInfo.nMinimumSizeX;
	stAccPadImageInfo.nMinimumSizeY = stMaskInfo.nMinimumSizeY;
	stAccPadImageInfo.nMaximumSizeX = stMaskInfo.nMaximumSizeX;
	stAccPadImageInfo.nMaximumSizeY = stMaskInfo.nMaximumSizeY;
	stAccPadImageInfo.nDefectDistance = stCommon.nDefectDistance;
	stAccPadImageInfo.nImageWidth = stCommon.nImageWidth;
	stAccPadImageInfo.nImageHeight = stCommon.nImageHeight;
	stAccPadImageInfo.nImageOffset = ((stCommon.nImageWidth * 3) % 4);

	_ACC_Pad_CreateTextureData(stPadCommonPara);
	//if (nThreshold == 0) Cleanup();
}


/**	
@brief	넘겨 받은 데이터로 GPU 메모리 상에 텍스쳐 생성
@param	SIMAGE_INFO
@return	없음
@remark	
@author	고정진
@date	2010/4/19  11:27
*/
void _ACC_Pad_CreateTextureData(SAOI_PAD_COMMON_PARA sCommon)
{
	
	//레인지값 설정. S
	int nMinThreshold = stPadMaskInformation.nMinRangeMain;
	int nMaxThreshold = stPadMaskInformation.nMaxRangeMain;
	if( nMinThreshold > nMaxThreshold )
		nMaxThreshold = nMinThreshold;

	for( int nStep = 0 ; nStep <= 255; nStep++ )
	{
		chPadHThresholdTemp[nStep] = (unsigned char) (((float)nMaxThreshold - (float)nMinThreshold) / 256.0 * nStep + nMinThreshold );
	}

	
	checkCudaErrors( cudaMemcpyToSymbol(chPadThreshold, chPadHThresholdTemp, sizeof(char) * 256 ) );	
	checkCudaErrors( cudaMemcpyToSymbol((const void*)&stPadMaskInfo.nScaleX1, (const void*)&stAccPadImageInfo.nScaleX1, sizeof(SACC_IMAGE_INFO) ) );

	if( sCommon.nImageWidth != nPadCurrentWidth || sCommon.nImageHeight != nPadCurrentHeight )
	{
		_ACC_Pad_DeleteTextureData();		
		
		nPadCurrentWidth = sCommon.nImageWidth;
		nPadCurrentHeight = sCommon.nImageHeight;		
		nPadImageSize		= nPadCurrentWidth * nPadCurrentHeight;

		cudaChannelFormatDesc desc	= cudaCreateChannelDesc<unsigned char>();	
		checkCudaErrors( cudaMallocArray(&arrPadSourceImage1, &desc, nPadCurrentWidth, nPadCurrentHeight) );
		checkCudaErrors( cudaBindTextureToArray(texPadSourceImage1, arrPadSourceImage1) );	////텍스처와 어레이를 묶는다.

		checkCudaErrors( cudaMallocArray(&arrPadSourceImage2, &desc, nPadCurrentWidth, nPadCurrentHeight) );
		checkCudaErrors( cudaBindTextureToArray(texPadSourceImage2, arrPadSourceImage2) );	////텍스처와 어레이를 묶는다.

		checkCudaErrors( cudaMallocArray(&arrPadSourceImage3, &desc, nPadCurrentWidth, nPadCurrentHeight) );
		checkCudaErrors( cudaBindTextureToArray(texPadSourceImage3, arrPadSourceImage3) );	////텍스처와 어레이를 묶는다.
		
		if(chPadSearchResult == NULL)
			checkCudaErrors( cudaMalloc((void**) &chPadSearchResult, nPadCurrentWidth * nPadCurrentHeight) );
		if(chPadSizeFilterBuff == NULL)
			checkCudaErrors( cudaMalloc((void**) &chPadSizeFilterBuff, nPadCurrentWidth * nPadCurrentHeight) );
		if(chPadSearchXLine == NULL)
			checkCudaErrors( cudaMalloc((void**) &chPadSearchXLine, nPadCurrentWidth ));
		if(chPadSearchYLine == NULL)
			checkCudaErrors( cudaMalloc((void**) &chPadSearchYLine, nPadCurrentHeight ));

		checkCudaErrors( cudaMemset( chPadSearchResult, 0, nPadCurrentWidth * nPadCurrentHeight ) );
		checkCudaErrors( cudaMemset( chPadSizeFilterBuff, 0, nPadCurrentWidth * nPadCurrentHeight ) );
		checkCudaErrors( cudaMemset( chPadSearchXLine, 0, nPadCurrentWidth));
		checkCudaErrors( cudaMemset( chPadSearchYLine, 0, nPadCurrentHeight));
	}
}

/**	
@brief	GPU 메모리 자원 수거
@param	없음
@return	없음
@remark	
-		생성한 텍스처를 제거한다.
@author	고정진
@date	2010/4/19  11:42
*/
void _ACC_Pad_DeleteTextureData()
{
	////묶었던 텍스처와 어레이를 분리한다.
	checkCudaErrors( cudaUnbindTexture(texPadSourceImage1) );
	checkCudaErrors( cudaUnbindTexture(texPadSourceImage2) );
	checkCudaErrors( cudaUnbindTexture(texPadSourceImage3) );

	//Textuer Memory Delete
	if(arrPadSourceImage1 != NULL)
		checkCudaErrors( cudaFreeArray(arrPadSourceImage1) );
	arrPadSourceImage1 = NULL;

	if(arrPadSourceImage2 != NULL)
		checkCudaErrors( cudaFreeArray(arrPadSourceImage2) );
	arrPadSourceImage2 = NULL;

	if(arrPadSourceImage3 != NULL)
		checkCudaErrors( cudaFreeArray(arrPadSourceImage3) );
	arrPadSourceImage3 = NULL;

	if(chPadSearchResult != NULL)
		checkCudaErrors(cudaFree(chPadSearchResult));
	chPadSearchResult = NULL;

	if(chPadSizeFilterBuff != NULL)
		checkCudaErrors(cudaFree(chPadSizeFilterBuff));
	chPadSizeFilterBuff = NULL;

	if(chPadSearchXLine != NULL)
		checkCudaErrors(cudaFree(chPadSearchXLine));	
	chPadSearchXLine = NULL;

	if(chPadSearchYLine != NULL)
		checkCudaErrors(cudaFree(chPadSearchYLine));	
	chPadSearchYLine = NULL;
}

extern "C"
	void _ACC_Pad_SetArray(unsigned char* chSourceImage2,		///<이미지 정보 구조체 이다.
										unsigned char* chSourceImage3		///<이미지 정보 구조체 이다. 
										)
{
	/*memset( chSourceImage2, 0x00, nPadCurrentWidth * nPadCurrentHeight);
	memset( chSourceImage3, 0x00, nPadCurrentWidth * nPadCurrentHeight);*/

	checkCudaErrors( cudaMemcpyToArray(arrPadSourceImage2, 0, 0, chSourceImage2, 
															nPadImageSize, cudaMemcpyHostToDevice) );

	checkCudaErrors( cudaMemcpyToArray(arrPadSourceImage3, 0, 0, chSourceImage3, 
															nPadImageSize, cudaMemcpyHostToDevice) );
}




/**	
@brief	Search 메인 함수
@param	SIMAGE_INFO, chResultImage, chXLineResult, chYLineResult
@return	없음
@remark	
-		원본 이미지를 입력 받고, 넘겨 받은 char* 에 결과 데이터를 넘겨 준다.
@author	고정진
@date	2010/4/19  11:46
*/
extern "C" 
void _ACC_Pad_SearchDefect(unsigned char* chSourceImage1,		///<이미지 정보 구조체 이다.										
										 unsigned char* chResultImage,			///<결과 데이터를 넘겨준다. 이진화된 이미지 형태이다.
										 unsigned char* chXLineResult,			///<X라인의 탐색 결과를 넘겨준다. Merge 시 사용한다.
										 unsigned char* chYLineResult= NULL	///<Y라인츼 탐색 결과를 넘겨준다. 현재 사용하지 않는다.
										 )
{	
	////결과 데이터 버퍼를 초기화한다.
	checkCudaErrors( cudaMemset( chPadSearchResult, 0, nPadImageSize) );
	checkCudaErrors( cudaMemset( chPadSizeFilterBuff, 0, nPadImageSize ) );
	checkCudaErrors( cudaMemset( chPadSearchXLine, 0, nPadCurrentWidth));
	checkCudaErrors( cudaMemset( chPadSearchYLine, 0, nPadCurrentHeight));
	
	checkCudaErrors( cudaMemcpyToArray(arrPadSourceImage1, 0, 0, chSourceImage1, 
			nPadImageSize, cudaMemcpyHostToDevice) );
	
	switch( stPadCommonPara.nMaskType )
	{
	case MASK_TYPE_AROMATIC:
	case MASK_TYPE_AROMATIC_JUMP:
		_ACC_Pad_SearchDefectOnBlock_Aromatic<<<stPadMaskInformation.nBlockCount, stPadMaskInformation.nThreadCount>>>
			(  chPadSearchResult, chPadSizeFilterBuff, chPadSearchXLine, chPadSearchYLine );
		break;
	case MASK_TYPE_CROSS:
		_ACC_Pad_SearchDefectOnBlock_Cross<<<stPadMaskInformation.nBlockCount, stPadMaskInformation.nThreadCount>>>
			(  chPadSearchResult, chPadSizeFilterBuff, chPadSearchXLine, chPadSearchYLine );
		break;
	case MASK_TYPE_STAR:
		_ACC_Pad_SearchDefectOnBlock_Star<<<stPadMaskInformation.nBlockCount, stPadMaskInformation.nThreadCount>>>
			(  chPadSearchResult, chPadSizeFilterBuff, chPadSearchXLine, chPadSearchYLine );
		break;
	case MASK_TYPE_5STAR:
		_ACC_Pad_SearchDefectOnBlock_5Star<<<stPadMaskInformation.nBlockCount, stPadMaskInformation.nThreadCount>>>
			(  chPadSearchResult, chPadSizeFilterBuff, chPadSearchXLine, chPadSearchYLine );
		break;
	case MASK_TYPE_7STAR:
		_ACC_Pad_SearchDefectOnBlock_7Star<<<stPadMaskInformation.nBlockCount, stPadMaskInformation.nThreadCount>>>
			(  chPadSearchResult, chPadSizeFilterBuff, chPadSearchXLine, chPadSearchYLine );
		break;
	case MASK_TYPE_DIRECT:
		_ACC_Pad_SearchDefectOnBlock_Direct<<<stPadMaskInformation.nBlockCount, stPadMaskInformation.nThreadCount>>>
			(  chPadSearchResult, chPadSizeFilterBuff, chPadSearchXLine, chPadSearchYLine );
		break;
	default:
		_ACC_Pad_SearchDefectOnBlock_Cross<<<stPadMaskInformation.nBlockCount, stPadMaskInformation.nThreadCount>>>
			(  chPadSearchResult, chPadSizeFilterBuff, chPadSearchXLine, chPadSearchYLine );
		break;
	}
	cudaThreadSynchronize();
	
	//결과 데이터를 메인 메모리로 넘겨준다.
	checkCudaErrors( cudaMemcpy(	chResultImage, chPadSearchResult, 
													nPadImageSize, cudaMemcpyDeviceToHost));

	checkCudaErrors( cudaMemcpy(	chXLineResult, chPadSearchXLine, 
													nPadCurrentWidth, cudaMemcpyDeviceToHost));

	checkCudaErrors( cudaMemcpy(	chYLineResult, chPadSearchYLine, 
													nPadCurrentHeight, cudaMemcpyDeviceToHost));	
		
}