/**
@file		PadThreadCreator.cpp
@brief	스레드 관리자
@remark		
-			전체 처리 스레드의 관리
@author	고정진
@date	2012/1/19
*/
#include "StdAfx.h"
#include "PadThreadCreator.h"

/**	
@brief	생성자
@param 	없음
@return	없음
@remark	
-			파라미터 초기화
@author	고정진
@date	2007/4/28  17:03
*/
CPadThreadCreator::CPadThreadCreator(void)
{
 	m_nErrorCode = OKAY;	
 	m_bThreadFlag = false;
	m_bProcessFlag = false;
	m_nProcessPosition = 0;						///<처리할 영역
	m_nProcessCount = 0;						///<처리할 이미지 수
 	InitializeCriticalSection(&cri_SrcSection);
 	InitializeCriticalSection(&cri_ResSection);
 
 	cMainInspectorQ = new LinkedMatrix<SOBJECT_INSPECTOR_STR>(1);
}

/**	
@brief	소멸자
@param 	없음
@return	없음
@remark
-		자원 수거
@author	고정진
@date	2010/4/29  10:21
*/
CPadThreadCreator::~CPadThreadCreator(void)
{
 	m_fnSetThreadFlag(false);
 	Sleep(100);
 	DeleteCriticalSection(&cri_SrcSection);
 	DeleteCriticalSection(&cri_ResSection);
 	delete cMainInspectorQ;
	delete[] m_bBufferFlag;
}

/**	
@brief	파라미터 셑
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	OKAY : 정상, Other : Error
@remark	
-			외부에서 입력 받은 파라미터를 _ACC 와 현재 실행중인 스레드에 전달하는 역할이다.
@author	고정진
@date	2012/1/18  10:36
*/
int		CPadThreadCreator::m_fnParameterSet(	SAOI_PAD_IMAGE_INFO		sMaskImage,		
																SAOI_PAD_COMMON_PARA sCommon,
																int nPosition)
{	
	int		nResultCode = OKAY;
	int		nTableLimit = 0;
	SOBJECT_INSPECTOR_STR	sObject_Str;

	nTableLimit = cMainInspectorQ->m_NodeCount(0);

	if( nTableLimit <= 0 )
	{
		m_nErrorCode		= ERR_THREAD_NOT_CREATE;
		return m_nErrorCode;
	}
// 	if( sCommon.nImageWidth >= IMAGE_MAX_WIDTH /*|| sCommon.nImageHeight >= IMAGE_MAX_HEIGHT*/ )
// 	{
// 		m_nErrorCode		= ERR_IMAGE_SIZE_OVER;
// 		return m_nErrorCode;
// 	}

  	m_stMaskImageInfo[nPosition] = sMaskImage;
 	m_stCommonPara[nPosition] = sCommon;	
	
	return nResultCode;
}

/**	
@brief	버퍼생성
@param 	없음
@return	없음
@remark
-			처리에 쓰일 버퍼를 생성한다.
-			버퍼의 크기가 바뀔때만 재생성한다.
@author	고정진
@date	2012/12/15  18:11
*/
void	CPadThreadCreator::m_fnDataBufferCreate(int nPosition)
{		
	//메모리 생성
	if ( m_stCommonPara[nPosition].nImageWidth != this->m_nImageWidth || m_stCommonPara[nPosition].nImageHeight != this->m_nImageHeight )
	{
		m_fnDataBufferDelete();

		this->m_nImageWidth = m_stCommonPara[nPosition].nImageWidth;
		this->m_nImageHeight = m_stCommonPara[nPosition].nImageHeight;
		m_nImageSize = this->m_nImageWidth * this->m_nImageHeight;

		m_chResultImage = new unsigned char*[m_nTotalThreadCount];
		m_chXLineResult = new unsigned char*[m_nTotalThreadCount];
		m_chYLineResult = new unsigned char*[m_nTotalThreadCount];

		m_vpObjectPointer = new __int64[m_nTotalThreadCount];
		for(int nStep = 0; nStep < m_nTotalThreadCount ; nStep++)
		{
			m_chResultImage[nStep] = new unsigned char[this->m_nImageWidth * this->m_nImageHeight];
			memset(m_chResultImage[nStep], 0x00,  this->m_nImageWidth * this->m_nImageHeight);
			m_chXLineResult[nStep] = new unsigned char[this->m_nImageWidth];	
			memset(m_chXLineResult[nStep], 0x00,  this->m_nImageWidth );

			m_chYLineResult[nStep] = new unsigned char[this->m_nImageHeight];	
			memset(m_chYLineResult[nStep], 0x00,  this->m_nImageHeight );

			m_bBufferFlag[nStep] = COMP;
		}
	}	
}


/**	
@brief	버퍼삭제
@param 	없음
@return	없음
@remark
-			현재 버퍼가 생성되어 있다면 파괴한다.
@author	고정진
@date	2012/12/15  18:11
*/
void	CPadThreadCreator::m_fnDataBufferDelete()
{		
	if ( m_chResultImage != nullptr)
	{
		for(int nStep = 0; nStep < m_nTotalThreadCount; nStep++)
		{
			delete[] m_chResultImage[nStep];	
			delete[] m_chXLineResult[nStep];	
			delete[] m_chYLineResult[nStep];	
		}
		//delete[] m_bBufferFlag;
		delete[] m_vpObjectPointer;
		delete[] m_chResultImage;			
		delete[] m_chXLineResult;		
		delete[] m_chYLineResult;		
	}
}


/**	
@brief	테스트 함수
@param 	SAOI_IMAGE_INFO, SAOI_COMMON_PARA, 이미지 데이터
@return	Defect Position
@remark
-		하나의 이미지에 대하여 처리를 수행한다.
-		CPU 에서 작동한다.
@author	고정진
@date	2010/4/29  10:36
*/
int*	CPadThreadCreator::m_fnDefectSearch( SAOI_PAD_IMAGE_INFO	sMaskImage,		
																SAOI_PAD_COMMON_PARA sCommon,
																unsigned char* uchImageSrc,
																unsigned char* uchImageMask1,
																unsigned char* uchImageMask2)
{	
	int* nDefectPos = nullptr;

	CPadMainInspector cMainInspector;

	cMainInspector.m_fnParameterSet(sMaskImage, sCommon);

	nDefectPos = cMainInspector.m_fnDefectSearch(uchImageSrc, uchImageMask1, uchImageMask2);

	return nDefectPos;
}

/**	
@brief	테스트 함수
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA, 이미지 데이터
@return	Defect Data
@remark
-			하나의 이미지에 대하여 처리를 수행한다.
-			GPU 에서 작동한다.
@author	고정진
@date	2010/4/29  10:36
*/
int*	CPadThreadCreator::m_fnDefectSearch_ACC(	SAOI_PAD_IMAGE_INFO sMaskImage,		
																		SAOI_PAD_COMMON_PARA sCommon,
																		unsigned char* uchImageSrc,
																		unsigned char* uchImageMask1,
																		unsigned char* uchImageMask2)
{	
	int* nDefectPos = nullptr;

	CPadMainInspector		cMainInspector; 

	_ACC_Pad_SetParameter( sMaskImage, sCommon);

	cMainInspector.m_fnParameterSet( sMaskImage, sCommon );

	nDefectPos = cMainInspector.m_fnDefectSearch_ACC(uchImageSrc, uchImageMask1, uchImageMask2);

	return nDefectPos;
}

/**
@brief	Result Buffer Flag Return
@param 	index
@return	Buffer Flag
@remark
-			Result Buffer Flag Return
@author	고정진
@date	2012/12/15  18:11
*/
bool	CPadThreadCreator::m_fnGetBufferFlag(int nIndex)
{		
	return m_bBufferFlag[nIndex];
}

/**
@brief	Result Buffer Flag Set
@param 	상태, index
@return	없음
@remark
-			Result Buffer Flag Set
@author	고정진
@date	2012/12/15  18:11
*/
void	CPadThreadCreator::m_fnSetBufferFlag(bool bFlag, int nIndex)
{		
	m_bBufferFlag[nIndex] = bFlag;
}

/**
@brief	VSB Object Pointer Return
@param 	index
@return	VSB Object Pointer
@remark
-			VSB Object Pointer Return
@author	고정진
@date	2012/12/15  18:11
*/
void*	CPadThreadCreator::m_fnGetObjectPointer(int nIndex)
{		
	return (void*)m_vpObjectPointer[nIndex];
}

/**
@brief	Result Buffer Image Return
@param 	index
@return	Result Buffer Image
@remark
-			Result Buffer Image Return
@author	고정진
@date	2012/12/15  18:11
*/
unsigned char*	CPadThreadCreator::m_fnGetResultImage(int nIndex)
{		
	return m_chResultImage[nIndex];
}

/**
@brief	XLine Result Return
@param 	index
@return	XLine Result
@remark
-			XLine Result Return
@author	고정진
@date	2012/12/15  18:11
*/
unsigned char*	CPadThreadCreator::m_fnGetXLineResult(int nIndex)
{		
	return m_chXLineResult[nIndex];
}

/**
@brief	XLine Result Return
@param 	index
@return	XLine Result
@remark
-			XLine Result Return
@author	고정진
@date	2012/12/15  18:11
*/
unsigned char*	CPadThreadCreator::m_fnGetYLineResult(int nIndex)
{		
	return m_chYLineResult[nIndex];
}
/**
@brief	PreProcess Result Set
@param 	chResultImage, chXLine, voidPointer
@return	TRUE
@remark
-			PreProcess Result Set
@author	고정진
@date	2012/12/15  18:11
*/

bool	CPadThreadCreator::m_fnSetResultBuffer(unsigned char* chResultImage, unsigned char* chXLine, unsigned char* chYLine, void* voidPointer )
{
	int nCompIndex = ERR_IMAGE_BUFFER;

	while(nCompIndex == ERR_IMAGE_BUFFER)
	{
		nCompIndex = m_fnGetCompIndex();
	}

	memcpy(m_chResultImage[nCompIndex], chResultImage, m_nImageSize);
	memcpy(m_chXLineResult[nCompIndex], chXLine, this->m_nImageWidth);
	memcpy(m_chYLineResult[nCompIndex], chYLine, this->m_nImageHeight);

	m_vpObjectPointer[nCompIndex] = (__int64)voidPointer;
	m_bBufferFlag[nCompIndex] = READY;

	return true;
}

/**
@brief	Complete Thread Index Return
@param 없음
@return	Thread Index
@remark
-			Complete Thread Index Return
@author	고정진
@date	2012/12/15  18:11
*/
int 	CPadThreadCreator::m_fnGetCompIndex( )
{		
	int nIndex = 0;
	for( ; nIndex < m_nTotalThreadCount; nIndex++ )
	{
		if( m_bBufferFlag[nIndex] == COMP )
			return nIndex;
	}
	return ERR_IMAGE_BUFFER;
}



/**	
@brief	분석 데이터 입력
@param 	InspectDataQ(Class Object Pointer)
@return	ErrorCode
@remark
-			해당 데이터를 내부 큐에 적재한다.
@author	고정진
@date	2010/4/29  10:14
*/
int CPadThreadCreator::m_fnPushItem( VSBridge cInspectDataQ )
{	
	int nResultCode = OKAY;

	if( m_nErrorCode > OKAY )
	{
		nResultCode		= m_nErrorCode;		
		return nResultCode;
	}

	SOBJECT_STR sObject_Str;

	if(cInspectDataQ == NULL)
		return ERR_NULL_DATA_INPUT;

	sObject_Str.cObjectPointer = (void*)cInspectDataQ;		

	//2012.01.16 현지 사용하지 않는다.
	//memcpy(sObject_Str.chImagePath, m_stMaskImage.ch_ACCResultPath, _MAX_PATH);
	//char	chFileName[FILE_NAME_LENGTH]={0,};
	//sprintf_s(chFileName,"%02d_%04d_C.BMP",cInspectDataQ->m_ScanLine,cInspectDataQ->m_ScanImageIndex);
	//memcpy(&sObject_Str.chImagePath[strlen(m_stMaskImage.ch_ACCResultPath)], chFileName, FILE_NAME_LENGTH);
	//2012.01.16 현지 사용하지 않는다.

	m_fnPushTemplateQNode(sObject_Str);

	return OKAY;
}

/**	
@brief	처리 스레드 생성
@param 	생성할 스레드 개수, 스레드 상태를 갱신할 영역 포인터
@return	없음
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
void	CPadThreadCreator::m_fnThreadCreator(  int nSearchMode, int nThreadCount, UINT* pProcStateArea )
{	
	m_fnSetThreadFlag(true);
	m_nTotalThreadCount = nThreadCount - 1;

	m_bBufferFlag = new bool[m_nTotalThreadCount];

	for(int nStep = 0; nStep < m_nTotalThreadCount ; nStep++)
	{		
		m_bBufferFlag[nStep] = COMP;
	}

	for(int nThreadStep = 1; nThreadStep <= nThreadCount; nThreadStep++)
	{
		m_fnThreadExecute(  nSearchMode, nThreadStep, pProcStateArea);
		Sleep(30);
	}
}

/**	
@brief	내부 큐 접근자
@param 	SOBJECT_STR
@return	없음
@remark
-		
@author	고정진
@date	2010/4/29  10:36
*/
void	CPadThreadCreator::m_fnPushTemplateQNode(SOBJECT_STR sObject_Str)
{
	EnterCriticalSection(&cri_SrcSection);
	cTemplateSrcQ.m_PushNode(sObject_Str);
	LeaveCriticalSection(&cri_SrcSection);	
}

/**	
@brief	내부 큐 접근자
@param 	SOBJECT_STR*
@return	ITEM_NOT_EXIST : 노드 없음, Other : 남은 노드 개수
@remark
-		
@author	고정진
@date	2010/4/29  10:36
*/
int		CPadThreadCreator::m_fnPopTemplateQNode(SOBJECT_STR* sObject_Str)
{
	int nReturn = 0;
	EnterCriticalSection(&cri_SrcSection);
	nReturn = cTemplateSrcQ.m_PopNode(sObject_Str, sizeof(SOBJECT_STR));
	LeaveCriticalSection(&cri_SrcSection);	
	return nReturn;
}

/**	
@brief	내부 큐 접근자
@param 	SRESOBJECT_STR
@return	없음
@remark
-		
@author	고정진
@date	2010/4/29  10:36
*/
void	CPadThreadCreator::m_fnPushTemplateResQNode(SRESOBJECT_STR sResObject_Str)
{
	EnterCriticalSection(&cri_ResSection);
	cTemplateResQ.m_PushNode(sResObject_Str);
	LeaveCriticalSection(&cri_ResSection);
}

/**	
@brief	내부 큐 접근자
@param 	SRESOBJECT_STR*
@return	ITEM_NOT_EXIST : 노드 없음, Other : 남은 노드 개수
@remark
-		
@author	고정진
@date	2010/4/29  10:36
*/
int		CPadThreadCreator::m_fnPopTemplateResQNode(SRESOBJECT_STR* sResObject_Str)
{
	int nReturn = 0;
	EnterCriticalSection(&cri_ResSection);
	nReturn = cTemplateResQ.m_PopNode(sResObject_Str, sizeof(SRESOBJECT_STR));
	LeaveCriticalSection(&cri_ResSection);	
	return nReturn;	
}

/**	
@brief	스레드 수행 플래그 접근자
@param 	없음
@return	Flag
@remark
-		
@author	고정진
@date	2010/4/29  10:36
*/
bool	CPadThreadCreator::m_fnGetThreadFlag()
{
	return	m_bThreadFlag;
}

/**	
@brief	스레드 수행 플래그 접근자
@param 	Flag
@return	ERR_THREAD_NOT_CREATE : 생성된 스레드 없음, OKAY : 정상
@remark
-			Flag 가 false 일 경우 큐에 가지고 있던 Thread Object Pointer 를 제거한다.
@author	고정진
@date	2010/4/29  10:36
*/
int	CPadThreadCreator::m_fnSetThreadFlag(bool bThreadFlag)
{

	m_bThreadFlag = bThreadFlag;

	Sleep(100);

	if(bThreadFlag == false)
	{
		int nTableLimit = cMainInspectorQ->m_NodeCount(0);

		if( nTableLimit <= 0 )
			return ERR_THREAD_NOT_CREATE;

		cMainInspectorQ->m_MoveFirst(0);

		for(int nItemNumber = 0; nItemNumber < nTableLimit; nItemNumber++)				
		{			
			cMainInspectorQ->m_PopFirstNode(0);				
		}

		//cActMainInspectorQ->m_NodeCount(0); 노드를 모두 제거 해야 한다...객체가 모두 소멸됬으므로...
		//스레드가 돌고 있는 상황이므로..여기서 객체를 제거 해서는 안된다. 스레드 에서 제거 하도록 놔두자.
	}
	return OKAY;
}

/**	
@brief	처리 스레드 실행
@param 	패드부 포지션, 이미지 개수, 전체 개수
@return	없음
@remark
-			스레드를 동작 시킨다.
@author	고정진
@date	2010/4/29  10:36
*/
int		CPadThreadCreator::m_fnProcessStart(int nPosition, int nPosCount, int nTotalCount )
{	
	int		nResultCode = OKAY;
	int		nTableLimit = 0;
	SOBJECT_INSPECTOR_STR	sObject_Str;
	
	nTableLimit = cMainInspectorQ->m_NodeCount(0);

	if( nTableLimit <= 0 )
	{
		m_nErrorCode		= ERR_THREAD_NOT_CREATE;
		return m_nErrorCode;
	}

	_ACC_Pad_SetParameter( m_stMaskImageInfo[nPosition], m_stCommonPara[nPosition]);

	cMainInspectorQ->m_MoveFirst(0);	

	for(int nItemNumber = 0; nItemNumber < nTableLimit; nItemNumber++)				
	{
		CPadMainInspector*	cMainInspector;
		sObject_Str		= cMainInspectorQ->m_ReadNextNode(0);	
		cMainInspector  = (CPadMainInspector*)sObject_Str.cObjectPointer;
		cMainInspector->m_fnParameterSet(m_stMaskImageInfo[nPosition], m_stCommonPara[nPosition]);
	}

	m_nProcessPosition = nPosition;
	
	m_nProcessCount = nPosCount;
	
	m_fnDataBufferCreate(m_nProcessPosition);

	m_fnSetProcessFlag(true);

	return nResultCode;
}

/**	
@brief	프로세스 플래그 리턴
@param 	없음
@return	프로세스 플래그
@remark
-			
@author	고정진
@date	2012/1/20  10:36
*/
bool	CPadThreadCreator::m_fnGetProcessFlag()
{
	return m_bProcessFlag;
}

/**	
@brief	프로세스 플래그 셑
@param 	플래그
@return	없음
@remark
-			
@author	고정진
@date	2012/1/20  10:36
*/
void		CPadThreadCreator::m_fnSetProcessFlag( bool bFlag )
{
	m_bProcessFlag = bFlag;
}

/**	
@brief	프로세스 카운터 리턴
@param 	없음
@return	프로세스 카운터
@remark
-			
@author	고정진
@date	2012/1/20  10:36
*/
int		CPadThreadCreator::m_fnGetProcessCount()
{
	return m_nProcessCount--;
}

/**	
@brief	처리 스레드 생성자
@param 	생성할 스레드 개수, 스레드 상태 정보를 갱신할 영역 포인터
@return	없음
@remark
-			스레드를 생성하고 해당 스레드를 포함하는 Class Object 를 내부 큐에 저장
@author	고정진
@date	2010/4/29  10:36
*/
void	CPadThreadCreator::m_fnThreadExecute(  int nSearchMode, int nThreadNum, UINT* pProcStateArea)
{
	SOBJECT_INSPECTOR_STR sObject_Str;	

	CPadMainInspector* cMainInspector = new CPadMainInspector;	

	sObject_Str.cObjectPointer = (void*)cMainInspector;	

	cMainInspectorQ->m_SetNextNode(sObject_Str, 0);	

	stThreadParam.nThreadNumber	= nThreadNum;
	stThreadParam.pProcStateArea	= pProcStateArea;
	stThreadParam.vpIncpectorClass	= (void*)cMainInspector;
	stThreadParam.vpThreadCreator	= this;

	if( nThreadNum == 1 )
	{
		h_ACCHandle = (HANDLE)_beginthreadex( NULL, 0, cMainInspector->THREAD_PRE_PROC_PAD_ACC, (void*)&stThreadParam, 0, NULL);			

		SetThreadPriority(h_ACCHandle, THREAD_PRIORITY_HIGHEST);

		CloseHandle(h_ACCHandle);
	}
	else
	{
		h_PostProcHandle[nThreadNum - 2] = (HANDLE)_beginthreadex( NULL, 0, cMainInspector->THREAD_POST_PROC_PAD_ACC, (void*)&stThreadParam, 0, NULL);	

		SetThreadPriority(h_PostProcHandle[nThreadNum - 2], THREAD_PRIORITY_HIGHEST);		

		CloseHandle(h_PostProcHandle[nThreadNum - 2]);
	}
}