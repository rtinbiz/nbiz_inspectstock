/**
@file	Anaim_ACC_Kernel.cu
@brief	커널단에서 수행되는 코드이다.
@remark	
@author	고정진
@date	2010/4/19  12:03
*/

#ifndef ANAIM_PAD_KERNEL_CROSS_H_
#define ANAIM_PAD_KERNEL_CROSS_H_

#include "AnaimParallel_Kernel_Pad.cuh"

__device__ inline unsigned char _ACC_Compare_Axis_Cross( 															
	int nXPos, int nYPos 
	);


/**	
@brief	메인 쿠다 프로세스 함수이다.
@param 	검출 결과 이미지 버퍼, 검출 결과 스텝 버퍼 X,Y
-		이미지 폭, 이미지 높이, 패턴 길이 X,Y, 최소 검출 사이즈 X,Y
-		검출 허용치, 외곽의 무시할 영역 두께
@return	없음.(넘겨 받은 포인터 버퍼로 넘겨준다.)
@remark	
-		전체 프로세스를 관리한다.
@author	고정진
@date	2010/4/28  14:54
*/
__global__ void	_ACC_Pad_SearchDefectOnBlock_Cross(	
	unsigned char *p_chSearchResult,
	unsigned char *p_chSizeFilterBuff,
	unsigned char *p_chSearchLineX	,
	unsigned char *p_chSearchLineY
	)
{
	bool	bSearchResult	= PIXEL_DIFF;
	unsigned char	chCompareValue = '\0';
	bool	bFindDead		= NORMAL;
	int		nAxis_X			= 0;
	int		nAxis_Y			= 0;	
	int		nTargetStep	= 0;	

	nAxis_X = (blockIdx.x * blockDim.x) + threadIdx.x;	

	//기본 검출 프로세스
	if(nAxis_X >= stPadMaskInfo.nOutLineLeft  && nAxis_X < (stPadMaskInfo.nImageWidth - stPadMaskInfo.nOutLineRight))
	{
		for( nAxis_Y = stPadMaskInfo.nOutLineUp; nAxis_Y < stPadMaskInfo.nImageHeight - stPadMaskInfo.nOutLineDown; nAxis_Y++ )
		{
			nTargetStep = nAxis_X + (stPadMaskInfo.nImageWidth * nAxis_Y) + (stPadMaskInfo.nImageOffset * nAxis_Y);

			chCompareValue =	_ACC_Compare_Axis_Cross( nAxis_X, nAxis_Y );	

			p_chSearchResult[nTargetStep] = chCompareValue;
			p_chSizeFilterBuff[nTargetStep] = chCompareValue;	
		}
	}

	__syncthreads();
	//cutilSafeCall( cudaThreadSynchronize() );
	//필터 처리 프로세스
	for( nAxis_Y = stPadMaskInfo.nOutLineUp; nAxis_Y < stPadMaskInfo.nImageHeight - stPadMaskInfo.nOutLineDown; nAxis_Y++ )
	{
		///<X 축 방향의 인접 필터
		bSearchResult = _ACC_Eraser_PixelX(	p_chSearchResult, p_chSizeFilterBuff, nAxis_X, nAxis_Y );
		///<Y출 방향의 인접 필터
		bSearchResult = _ACC_Eraser_PixelY(	p_chSearchResult, p_chSizeFilterBuff, 	nAxis_X, nAxis_Y );

		//Edge 처리 프로세스
		///<해당 픽셀이 Defect 로 판정 되었다면
		if( bSearchResult == DEFECT )
		{
			///<에지 처리 프로세스를 수행하라.
			//bSearchResult = _ACC_EdgeIgnore(	p_chSearchResult, nAxis_X, nAxis_Y );
			///<해당 픽셀이 Defect 로 판정 되었다면
			if( bSearchResult == DEFECT )
			{	///<해당 열에서 Defect 가 발견되었음을 알린다.				
				bFindDead = DEFECT;	
				p_chSearchLineY[nAxis_Y] = DEAD_PIXEL;
			}
		}
	}

	///<해당 열에서 Defect 가 발견되었다면, 해당 버퍼 인덱스에 세팅한다.
	if( bFindDead )
		p_chSearchLineX[nAxis_X] = DEAD_PIXEL;
	__syncthreads();
}

/**	
@brief	최초 검출 함수 이다.
@param 	검출 결과 이미지 버퍼, 이미지 폭, 이미지 높이, 패턴 길이 X,Y, 
-		검출 허용치, 외곽의 무시할 영역 두께, 연산을 수행할 좌표
@return	비교 결과.(PIXEL_EQUAL : 정상, PIXEL_DIFF : 이상)
@remark
-		X축 방향 3회,  Y축 방향 3회 최대 6회 수행한다.
@author	고정진
@date	2010/4/28  16:30
*/
__device__ inline unsigned char _ACC_Compare_Axis_Cross(		
	int nXPos, int nYPos
	)
{	
	unsigned char	chPixelData = '\0';
	unsigned char	chComparePixel = '\0';	
	unsigned char	chCompareValue =LIVE_PIXEL;
	int				nComparePosX  = 0;
	int				nComparePosY  = 0;

	chPixelData = tex2D( texPadSourceImage1, nXPos, nYPos );	
	//if( chPixelData == DEAD_PIXEL )
	//	return LIVE_PIXEL;
	//해당 좌표가 중앙에서 좌측이라면 비교군은 우측에서 선택한다.

	for( int i = -1 ; i <= 1 ; i++ )
	{		
		nComparePosX = nXPos + i;
		nComparePosY = nYPos;

		if( nComparePosX >= stPadMaskInfo.nImageWidth || nComparePosX < 0)
			continue;	

		chComparePixel = tex2D( texPadSourceImage2, nComparePosX, nComparePosY );
		if( chComparePixel == LIVE_PIXEL )
			return LIVE_PIXEL;
		else
		{
			chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel );
			if( chCompareValue == LIVE_PIXEL )	
				return LIVE_PIXEL;
		}			

		chComparePixel = tex2D( texPadSourceImage3, nComparePosX, nComparePosY );
		if( chComparePixel == LIVE_PIXEL )
			return LIVE_PIXEL;
		else
		{
			chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel );
			if( chCompareValue == LIVE_PIXEL )	
				return LIVE_PIXEL;
		}			
	}

	for( int j = -1; j <= 1 ; j++ )
	{
		nComparePosX = nXPos;
		nComparePosY = nYPos + j;
		
		if( nComparePosY >= stPadMaskInfo.nImageHeight || nComparePosY < 0)
			continue;

		chComparePixel = tex2D( texPadSourceImage2, nComparePosX, nComparePosY );
		if( chComparePixel == LIVE_PIXEL )
			return LIVE_PIXEL;
		else
		{
			chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel );
			if( chCompareValue == LIVE_PIXEL )	
				return LIVE_PIXEL;
		}			

		chComparePixel = tex2D( texPadSourceImage3, nComparePosX, nComparePosY );
		if( chComparePixel == LIVE_PIXEL )
			return LIVE_PIXEL;
		else
		{
			chCompareValue = _ACC_RGB_Compare( chPixelData, chComparePixel );
			if( chCompareValue == LIVE_PIXEL )	
				return LIVE_PIXEL;
		}			
	}

	return chCompareValue;
}


#endif		