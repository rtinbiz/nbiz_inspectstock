// nBizAnaimAOI.cpp : DLL 응용 프로그램에 대한 진입점을 정의합니다.
//
/**
@file	nBizAnaimAOI.cpp
@brief	외부 인터페이스 함수 정의
@remark	
@author	고정진
@date	2010/4/29  10:20
*/

#include "stdafx.h"
#include "ActThreadCreator.h"
#include "PadThreadCreator.h"
#include "ActDisscolor.h"
#include "Parameter.h"

#ifdef _MANAGED
#pragma managed(push, off)
#endif

CActDisscolor			cActDisscolor;
CActThreadCreator	cActThreadCreator;								///<스레드 관리자 멤버 선언
CPadThreadCreator  cPadThreadCreator;								///<스레드 관리자 멤버 선언
int							g_nRealDefectPos[MAX_DEFECT][sizeof(DEFECT_DATA)];		///<후처리 후 결함 데이터 리스트, 테스트용

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//여기서 부터 액티브 처리에 대한 API////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
@brief	처리 스레드 생성
@param 	생성할 스레드 개수, 스레드 상태를 갱신할 영역 포인터
@return	OKAY : 정상 처리, ERR_THREAD_COUNT : 스레드 입력 개수 에러
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int  _AOI_Act_Defect_Inspection_Start( 
	int nThreadCount, 																								
	UINT *pProcStateArea 
	)
{
	if( nThreadCount < MIN_THREAD_COUNT || nThreadCount > MAX_THREAD_COUNT )
		return ERR_THREAD_COUNT;

	//Act 처리에 대한 스레드 생성
	cActThreadCreator.m_fnThreadCreator(  ACT_SEARCH_MODE, nThreadCount, pProcStateArea );		

	return OKAY;
}

/**	
@brief	스레드 중단.
@param 	없음
@return	OKAY : 정상 처리
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int  _AOI_Act_Defect_Inspection_Stop()
{
	cActThreadCreator.m_fnSetThreadFlag(false);

	Sleep(100);

	//nTableLimit = cMainInspectorQueue->m_NodeCount(0); 을 제거해야 한다. 객체가 이미 소멸 됬으므로..

	return OKAY;
}

/**	
@brief	파라미터 설정
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	OKAY : 정상 처리, ERR_THREAD_NOT_CREATE : 스레드 생성되지 않았음(스레드 부터 생성할것)
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int  _AOI_Act_SearchParameterSet(
	SAOI_IMAGE_INFO		stMaskImageInfo,																						
	SAOI_COMMON_PARA	stCommonPara
	)
{	
	int nResult = 0;
	nResult = cActThreadCreator.m_fnParameterSet( stMaskImageInfo, stCommonPara );	
	return nResult;
}


/**	
@brief	파라미터 설정
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	OKAY : 정상 처리, ERR_THREAD_NOT_CREATE : 스레드 생성되지 않았음(스레드 부터 생성할것)
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int  _AOI_Act_SearchScaleSet( 
	SAOI_PAD_AREA stPadArea 
	)
{		
	int nResult = 0;

	nResult = cActThreadCreator.m_fnScaleSet( stPadArea );	

	return nResult;
}

/**	
@brief	분석 데이터 입력
@param 	InspectDataQ(Class Object Pointer)
@return	없음
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport)  int  _AOI_Act_PushItem( 
	InspectDataQ* cInspectDataQ 
	)
{
	if( cInspectDataQ != nullptr)
		return cActThreadCreator.m_fnPushItem( cInspectDataQ );

	return ERR_NULL_DATA_INPUT;
}

/**	
@brief	파라미터 설정
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	OKAY : 정상 처리, ERR_THREAD_NOT_CREATE : 스레드 생성되지 않았음(스레드 부터 생성할것)
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C++" __declspec(dllexport) int  _AOI_Disscolor_ParameterSet(	
	SDISSCOLOR_INFO stDissImageInfo, 
	char* chAlignImagePath	
	)
{		
	int nResult = 0;	
	nResult = cActDisscolor.m_fnDisscolor_ParamSet(stDissImageInfo, chAlignImagePath);
	return nResult;
}

/**	
@brief	파라미터 설정
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	OKAY : 정상 처리, ERR_THREAD_NOT_CREATE : 스레드 생성되지 않았음(스레드 부터 생성할것)
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C++" __declspec(dllexport) int  _AOI_Disscolor_Inspect(	
	unsigned char* uchTargetImage, int nDefWidth, int nDefHeight
	)
{		
	int nResult = 0;	
	nResult = cActDisscolor.m_fnDisscolor_Search( uchTargetImage, nDefWidth, nDefHeight );
	return nResult;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//여기까지 액티브 처리에 대한 API///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//여기서 부터 패드부 처리에 대한 API////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
@brief	처리 스레드 생성
@param 	생성할 스레드 개수, 스레드 상태를 갱신할 영역 포인터
@return	OKAY : 정상 처리, ERR_THREAD_COUNT : 스레드 입력 개수 에러
@remark
-		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int  _AOI_Pad_Defect_Inspection_Start(	
	int nThreadCount, 
	UINT *pProcStateArea
	)
{
	if( nThreadCount < MIN_THREAD_COUNT || nThreadCount > MAX_THREAD_COUNT )
		return ERR_THREAD_COUNT;

	cPadThreadCreator.m_fnThreadCreator(  PAD_SEARCH_MODE,nThreadCount, pProcStateArea );	

	return OKAY;
}

/**	
@brief	패드부 파라미터 설정
@param 	SAOI_PAD_IMAGE_INFO, SAOI_PAD_COMMON_PARA, nPosition(상하좌우)
@return	OKAY : 정상 처리, ERR_THREAD_NOT_CREATE : 스레드 생성되지 않았음(스레드 부터 생성할것)
@remark
-		
@author	고정진
@date	2012/1/18  10:14
*/
extern "C" __declspec(dllexport) int  _AOI_Pad_SearchParameterSet(	
	SAOI_PAD_IMAGE_INFO	stMaskImageInfo,
	SAOI_PAD_COMMON_PARA	stCommonPara, 
	int nPosition 
	)
{		
	int nResult = 0;		

	nResult = cPadThreadCreator.m_fnParameterSet( stMaskImageInfo, stCommonPara, nPosition );	

	return nResult;
}


/**	
@brief	패드부 검출 시작
@param 	nPosition(상하좌우), nPositionCount(포지션별 반복개수), nTotalCount(전체 이미지 개수)
@return	OKAY : 정상 처리
@remark
-		
@author	고정진
@date	2012/1/18  10:14
*/
extern "C" __declspec(dllexport) int  _AOI_Pad_Process_Start(
	int nPosition, int nPositionCount 
	)
{		
	int nResult = 0;

	nResult = cPadThreadCreator.m_fnProcessStart( nPosition, nPositionCount);	

	return nResult;
}

/**	
@brief	스레드 중단.
@param 	없음
@return	OKAY : 정상 처리
@remark		
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int  _AOI_Pad_Defect_Inspection_Stop()
{
	cPadThreadCreator.m_fnSetThreadFlag(false);

	Sleep(100);

	//nTableLimit = cMainInspectorQueue->m_NodeCount(0); 을 제거해야 한다. 객체가 이미 소멸 됬으므로..

	return OKAY;
}

/**	
@brief	분석 데이터 입력
@param 	InspectDataQ(Class Object Pointer)
@return	없음
@remark
-		
@author	고정진
@date	2012/1/18  10:14
*/
extern "C" __declspec(dllexport)  int  _AOI_Pad_PushItem( 
	InspectPadDataQ* cInspectDataQ 
	)
{
	if( cInspectDataQ != nullptr)
		return cPadThreadCreator.m_fnPushItem( cInspectDataQ );

	return ERR_NULL_DATA_INPUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//여기 까지 패드부 처리에 대한 API//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//여기서부터 싱클 테스트용 API//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/**	
@brief	테스트 함수
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA, 이미지 소스 데이터
@return	Defect 정보
@remark
-		하나의 이미지에 대하여 수행한다.
-		CPU 를 이용하여 처리한다.
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int*  _AOI_DefectSearch(  
	SAOI_IMAGE_INFO		stMaskImageInfo,
	SAOI_COMMON_PARA	stCommonPara,
	unsigned char*			uchImageSrc
	)
{		
	int* nDefectData = nullptr;

	nDefectData = cActThreadCreator.m_fnDefectSearch( stMaskImageInfo, stCommonPara, uchImageSrc );	

	memcpy(&g_nRealDefectPos, nDefectData, sizeof( DEFECT_DATA ) * MAX_DEFECT);	

	return (int*)g_nRealDefectPos;
}

/**	
@brief	테스트 함수
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA, 이미지 소스 데이터
@return	Defect 정보
@remark
-		하나의 이미지에 대하여 수행한다.
-		GPU 를 이용하여 처리한다.
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int*  _AOI_DefectSearch_ACC(	
	SAOI_IMAGE_INFO		stMaskImageInfo,
	SAOI_COMMON_PARA	stCommonPara,
	SAOI_PAD_AREA			stPadArea,
	unsigned char*			uchImageSrc
	)
{		
	int* nDefectData = nullptr;

	nDefectData = cActThreadCreator.m_fnDefectSearch_ACC( stMaskImageInfo, stCommonPara, stPadArea, uchImageSrc );	
	memcpy(&g_nRealDefectPos, nDefectData, sizeof( DEFECT_DATA )  * MAX_DEFECT);	

	return (int*)g_nRealDefectPos;
}

/**	
@brief	테스트 함수
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA, 이미지 소스 데이터
@return	Defect 정보
@remark
-		하나의 이미지에 대하여 수행한다.
-		CPU 를 이용하여 처리한다.
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int*  _AOI_Pad_DefectSearch(  
	SAOI_PAD_IMAGE_INFO		stMaskImageInfo,
	SAOI_PAD_COMMON_PARA	stCommonPara,
	unsigned char*			uchImageSrc,
	unsigned char*			uchImageMask1,
	unsigned char*			uchImageMask2
	)
{		
	int* nDefectData = nullptr;

	nDefectData = cPadThreadCreator.m_fnDefectSearch( stMaskImageInfo, stCommonPara, uchImageSrc, uchImageMask1, uchImageMask2 );	

	memcpy(&g_nRealDefectPos, nDefectData, sizeof( DEFECT_DATA ) * MAX_DEFECT);	

	return (int*)g_nRealDefectPos;
}

/**	
@brief	테스트 함수
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA, 이미지 소스 데이터
@return	Defect 정보
@remark
-		하나의 이미지에 대하여 수행한다.
-		GPU 를 이용하여 처리한다.
@author	고정진
@date	2010/4/29  10:14
*/
extern "C" __declspec(dllexport) int*  _AOI_Pad_DefectSearch_ACC(	
	SAOI_PAD_IMAGE_INFO		stMaskImageInfo,
	SAOI_PAD_COMMON_PARA	stCommonPara,
	unsigned char*			uchImageSrc,
	unsigned char*			uchImageMask1,
	unsigned char*			uchImageMask2
	)
{		
	int* nDefectData = nullptr;

	nDefectData = cPadThreadCreator.m_fnDefectSearch_ACC( stMaskImageInfo, stCommonPara, uchImageSrc, uchImageMask1, uchImageMask2 );	
	memcpy(&g_nRealDefectPos, nDefectData, sizeof( DEFECT_DATA ) * MAX_DEFECT);	

	return (int*)g_nRealDefectPos;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//여기까지 싱글 테스트용 API////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _MANAGED
#pragma managed(pop)
#endif
