/** 
@addtogroup	ERROR_DEFINE
@{
*/ 

/**
@file	ErrorDefine.h
@brief	에러 코드 정의
@remark		
-		
@author	고정진
@date	2007/10/01
*/
#ifndef		ERRORDEFINE_H
#define		ERRORDEFINE_H

static	const enum	ERROR_CODE{	
	ERR_SOURCE_FILE = 1001,	///<에러코드 정의
	ERR_ALIGN_FILE,
	ERR_MASK_FILE,
	ERR_ALIGN_FAIL,
	ERR_THREAD_COUNT,
	ERR_THREAD_NOT_CREATE,
	ERR_NULL_DATA_INPUT,
	ERR_DEFECT_BUFF_FULL = 2001,
	ERR_IMAGE_SIZE_OVER,
	ERR_PAD_IMAGE_BUFFER,
	ERR_IMAGE_BUFFER};

#endif
/** 
@}
*/