/** 
@addtogroup	PAD_MAIN_INSPECTOR
@{
*/ 

/**
@file		PadMainInspector.h
@brief	메인 프로세스
@remark	
-			
@author	고정진
@date	2010/4/15  12:39
*/
#pragma once
#ifndef		PAD_MAININSPECTOR_H
#define		PAD_MAININSPECTOR_H
#include "PadImageFile.h"
#include "DataStructure.h"
#include "PadThreadCreator.h"

/**
@class	CPadMainInspector
@brief	메인 프로세스
@remark		
-			검출 프로세스 관리자
@author	고정진
@date	2012/01/11  11:28
*/
#pragma once
class CPadMainInspector
{
	typedef InspectPadDataQ* VSBridge;

public:
	CPadMainInspector(void);
	~CPadMainInspector(void);
	void	m_fnParameterSet(	SAOI_PAD_IMAGE_INFO	sMaskImage,		
										SAOI_PAD_COMMON_PARA	sCommon
										);

	int*	m_fnDefectSearch(  unsigned char* byImageData,
										unsigned char* byImageMask1,
										unsigned char* byImageMask2);	

	int*	m_fnDefectSearch_ACC(  unsigned char* byImageData,
												unsigned char* byImageMask1,
												unsigned char* byImageMask2);	

	static unsigned int __stdcall	THREAD_PRE_PROC_PAD_ACC(LPVOID PosParam);

	static unsigned int __stdcall	THREAD_POST_PROC_PAD_ACC(LPVOID PosParam);	

private:
#ifdef _DEBUG
	void	m_fnStepLogFlagSet( bool bSetFlag );	
#endif		
	void	m_fnSetAllComplete();	

	void	m_fnSetComplete(int nIndex);
	
	void	m_fnSetStartBusy();

	int		m_fnSetObject(SOBJECT_STR sObject_Str);

	void*  m_fnGetObject(int nObjectIndex);

	unsigned char*  m_fnGetXlineResult(int nIndex);

	unsigned char*  m_fnGetYlineResult(int nIndex);	

	unsigned char*  m_fnGetResultImage(int nIndex);

	int		m_fnGetImageWidth();

	int		m_fnGetImageHeight();

	int		m_fnGetNullptrIndex();

	int		m_fnGetPadObjFlag();

	void	m_fnOnePointInspect();

	void	m_fnAllPointInspect();

	void	m_fnDefectSearch_ACCT( SOBJECT_STR sObject_Str );

	void	m_fnDefectResult_ACCT( SRESOBJECT_STR sResObject_Str );	

	void	m_fnDefectResult_ACCT( unsigned char* chResultImage, unsigned char* chXLineResult, 
												unsigned char* chYLineResult, void* vpObject );

	bool	m_fnImageFileRead( char* chFileFullPath );

	void	m_fnDataBufferDelete();

	void	m_fnDataBufferCreate();

private:
	
	PadImageFile					m_cImageInfo;				///<ImageFile Class 형 멤버
	SOBJECT_STR_PAD			m_stPadObj;
	SAOI_PAD_IMAGE_INFO		m_stMaskImageInfo;					///<마스크 이미지 정보
	SAOI_PAD_COMMON_PARA	m_stCommonPara;					///<Common Para
	SACC_PROCESS_CALL	fnAccProcessCall[4];
	IplImage*				m_cvResultImage;		///<_ACC result Image 를 저장하기 위해 사용한다.
	
// 	unsigned char*		m_chSourceImage;
// 	unsigned char*		m_chResultImage;		///<Image Data 저장소
	int		m_nImageWidth;
	int		m_nImageHeight;
	int		m_nImageSize;

	BYTE* byStandard1;
	BYTE* byStandard2;


};
#endif
/** 
@}
*/ 