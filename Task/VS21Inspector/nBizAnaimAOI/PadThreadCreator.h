/** 
@addtogroup	THREAD_CREATOR
@{
*/ 

/**
@file		ThreadCreator.h
@brief	스레드 관리자
@remark	
-			
@author	고정진
@date	2010/4/15  12:39
*/
#pragma once
#ifndef	 PAD_THREADCREATOR_H
#define	 PAD_THREADCREATOR_H
#include "PadMainInspector.h"
#include "LinkedList.h"
#include "LinkedMatrix.h"
#include "DataStructure.h"

//#include "actthreadcreator.h"

/**
@class	CPadThreadCreator
@brief	스레드 관리자.
@remark		
-			CActThreadCreator 를 상속
@author	고정진
@date	2012/01/11  11:28
*/
class CPadThreadCreator
{
	typedef InspectPadDataQ* VSBridge;

public:
	CPadThreadCreator(void);
	~CPadThreadCreator(void);

	int		m_fnParameterSet(	SAOI_PAD_IMAGE_INFO	sMaskImage,		
										SAOI_PAD_COMMON_PARA sCommon,
										int nPosition);

	int		m_fnProcessStart(int nPosition, int nPosCount, int nTotalCount = 0 );

	bool	m_fnGetProcessFlag();

	void	m_fnSetProcessFlag(bool bFlag);

	int		m_fnGetProcessCount();

	int		m_fnPushItem(VSBridge cInspectDataQ );	

	void	m_fnThreadCreator(  int nSearchMode, int nThreadCount, UINT* pProcStateArea);	

	void	m_fnPushTemplateQNode(SOBJECT_STR sObject_Str);	

	int		m_fnPopTemplateQNode(SOBJECT_STR* sObject_Str);	

	void	m_fnPushTemplateResQNode(SRESOBJECT_STR sResObject_Str );	

	int		m_fnPopTemplateResQNode(SRESOBJECT_STR* sResObject_Str);	

	void	m_fnPushMainInspectorQNode(SOBJECT_INSPECTOR_STR sObject_Str);	

	int		m_fnPopMainInspectorQNode(SOBJECT_INSPECTOR_STR* sObject_Str);

	bool	m_fnGetThreadFlag();

	int		m_fnSetThreadFlag(bool bThreadFlag);

	void	m_fnSetBufferFlag(bool bFlag, int nIndex);

	bool	m_fnGetBufferFlag(int nIndex);

	void*	m_fnGetObjectPointer(int nIndex);

	unsigned char*	m_fnGetResultImage(int nIndex);

	unsigned char*	m_fnGetXLineResult(int nIndex);

	unsigned char*	m_fnGetYLineResult(int nIndex);

	bool	m_fnSetResultBuffer( unsigned char* chResultImage, unsigned char* chXLine, 
										unsigned char* chYLine, void* voidPointer);

	int*	m_fnDefectSearch(  SAOI_PAD_IMAGE_INFO	sMaskImage,		
										SAOI_PAD_COMMON_PARA sCommon,
										unsigned char* uchImageSrc,
										unsigned char* uchImageMask1,
										unsigned char* uchImageMask2);

	int*	m_fnDefectSearch_ACC(	SAOI_PAD_IMAGE_INFO sMaskImage,		
												SAOI_PAD_COMMON_PARA sCommon,
												unsigned char* uchImageSrc,
												unsigned char* uchImageMask1,
												unsigned char* uchImageMask2);

private:
	void	m_fnThreadExecute(  int nSearchMode, int nThreadNum, UINT* pProcStateArea );

	void	m_fnDataBufferDelete();

	void	m_fnDataBufferCreate(int nPosition);

	int 	m_fnGetCompIndex();
private:
	//공용 멤버 선언 S
	int							m_nErrorCode;								///<전체 프로세스에 대한 에러코드
	bool						m_bThreadFlag;							///<Thread 상태 플래그
	int							m_nProcessPosition;						///<처리할 영역
	int							m_nProcessCount;						///<처리할 이미지 수
	bool						m_bProcessFlag;
	
	int							m_nImageWidth;
	int							m_nImageHeight;
	int							m_nImageSize;
	int							m_nTotalThreadCount;

	bool*						m_bBufferFlag;
	__int64*					m_vpObjectPointer;
	unsigned char**		m_chResultImage;						///<Image Data 저장소
	unsigned char**		m_chXLineResult;						///<Image Data 저장소
	unsigned char**		m_chYLineResult;						///<Image Data 저장소
	
	THREAD_PARAM		stThreadParam;	
	//공용 멤버 선언 E

	//ACT 멤버 선언 S
	HANDLE					h_ACCHandle;								///<Thread Handle
	HANDLE					h_PostProcHandle[MAX_POST_PROC_THREAD];								///<Thread Handle

	CRITICAL_SECTION	cri_SrcSection;							///<전처리 버퍼에 대한 크리티컬 섹션			
	CRITICAL_SECTION	cri_ResSection;							///<후처리 버퍼에 대한 크리티컬 섹션

	TemplateQueue<SOBJECT_STR>	cTemplateSrcQ;					///<_ACC 필터 처리 전 데이터 이다.
	TemplateQueue<SRESOBJECT_STR>	cTemplateResQ;				///<_ACC 필터 처리 후 데이터 이다.
	LinkedMatrix<SOBJECT_INSPECTOR_STR>* cMainInspectorQ;	///<메인 인스펙터의 오브젝트 포인터 이다. 런타임 중에 파라미터 값을 변경하기 위해 사용할 것이다.
	
	SAOI_PAD_IMAGE_INFO		m_stMaskImageInfo[MAX_PAD_AREA];				///<마스크 이미지 정보
	SAOI_PAD_COMMON_PARA	m_stCommonPara[MAX_PAD_AREA];					///<Common Para	

};

#endif
/** 
@}
*/ 


