/**
@file	AnaimCuda.cu
@brief	Cuda 메인 파일
@remark	
@author	고정진
@date	2010/4/19  10:53
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <cuda_runtime.h>
#include <helper_cuda.h>

//Lens Zone Start

#include "AnaimParallel_Kernel_Act_Lens.cuh"
#include "AnaimParallel_Kernel_Act_Lens_Direct.cuh"
#include "AnaimParallel_Kernel_Act_Lens_Direct_HF.cuh"
#include "AnaimParallel_Kernel_Act_Lens_Direct_Sign.cuh"
#include "AnaimParallel_Kernel_Act_Lens_Aromatic.cuh"
#include "AnaimParallel_Kernel_Act_Lens_Aromatic_Jump.cuh"
#include "AnaimParallel_Kernel_Act_Lens_Cross.cuh"
#include "AnaimParallel_Kernel_Act_Lens_Star.cuh"
//Lens Zone End

/*#include "Parameter.h"*/


static  int	nCurrentWidth		= 0;				///<이미지 폭
static  int	nCurrentHeight		= 0;				///<이미지 높이
static int	nActImageSize		= 0;				///<이미지 사이즈

SAOI_IMAGE_INFO			stMaskInformation;		///<이미지 정보 구조체 이다.
SAOI_COMMON_PARA		stCommonPara;
SACC_IMAGE_INFO			stAccImageInfo;

unsigned char				chThresholdTemp[MAX_ACTIVE_ZONE][MAX_GRAY_LEVEL];
static cudaArray*			arrSourceImage = NULL;		///<메인 이미지 어레이 구조를 담는다.
static unsigned char*	chSearchResult = NULL;			///<결과 데이터를 보관할 그래픽 메모리 버퍼를 생성한다.
static unsigned char*	chSearchXLine  = NULL;			///<결과 데이터를 보관할 그래픽 메모리 버퍼를 생성한다.
static unsigned char*	chSearchYLine  = NULL;			///<결과 데이터를 보관할 그래픽 메모리 버퍼를 생성한다.
static unsigned char*	chSizeFilterBuff = NULL;			///<Image Data 저장소
static unsigned int*		nScaleBuffX		 = NULL;			///<Image Data 저장소
static unsigned int*		nScaleBuffXtmp	 = NULL;
static unsigned int*		nScaleBuffY			= NULL;			///<Image Data 저장소
static unsigned int*		nScaleBuffYtmp	= NULL;
static unsigned int*		nZoneFlag			= NULL;		
static unsigned int*		nZoneFlagTemp	= NULL;

//Function Declare Start
void _ACC_CreateTextureData();
void _ACC_DeleteTextureData();
void _ACC_CreateThresholdLinear();
//Function Declare End


/**	
@brief	파라미터 설정.
@param	SMASK_AOI_IMAGEINFO
@return	없음
@remark	
-		_ACC Process 에서 사용하는 기본 파라미터로
		자세한 정보는 _ACCStructure.h 파일을 참고한다.
@author	고정진
@date	2010/4/19  11:39
*/
extern "C"
void _ACC_Act_SetParameter(SAOI_IMAGE_INFO stMaskInfo, SAOI_COMMON_PARA stCommon )
{
	stMaskInformation		= stMaskInfo;
	stCommonPara				= stCommon;
	
	stAccImageInfo.nScaleX1 = stMaskInfo.nScaleX1;
	stAccImageInfo.nScaleY1 = stMaskInfo.nScaleY1;
	stAccImageInfo.nScaleX2 = stMaskInfo.nScaleX2;
	stAccImageInfo.nScaleY2 = stMaskInfo.nScaleY2;
	stAccImageInfo.nScaleX3 = stMaskInfo.nScaleX3;
	stAccImageInfo.nScaleY3 = stMaskInfo.nScaleY3;
	stAccImageInfo.nOutLineLeft = stMaskInfo.nOutLineLeft;
	stAccImageInfo.nOutLineRight = stMaskInfo.nOutLineRight;
	stAccImageInfo.nOutLineUp = stMaskInfo.nOutLineUp;
	stAccImageInfo.nOutLineDown = stMaskInfo.nOutLineDown;
	stAccImageInfo.nMinimumSizeX = stMaskInfo.nMinimumSizeX;
	stAccImageInfo.nMinimumSizeY = stMaskInfo.nMinimumSizeY;
	stAccImageInfo.nMaximumSizeX = stMaskInfo.nMaximumSizeX;
	stAccImageInfo.nMaximumSizeY = stMaskInfo.nMaximumSizeY;
	stAccImageInfo.nDefectDistance = stCommon.nDefectDistance;
	stAccImageInfo.nImageWidth = stCommon.nImageWidth;
	stAccImageInfo.nImageHeight = stCommon.nImageHeight;
	stAccImageInfo.nImageOffset = ((stCommon.nImageWidth * 3) % 4);
	
	_ACC_CreateThresholdLinear();

	_ACC_CreateTextureData();
}


/**	
@brief	
@param	
@return	없음
@remark	
@author	고정진
@date	2010/4/19  11:27
*/
void _ACC_CreateThresholdLinear()
{	
	//ZONE_CENTER_ACT
	int nMinThreshold = stMaskInformation.nMinRangeMain;
	int nMaxThreshold = stMaskInformation.nMaxRangeMain;		

	if( nMinThreshold > nMaxThreshold )
		nMaxThreshold = nMinThreshold;

	for( int nStep = 0 ; nStep <= 255; nStep++ )
	{
		chThresholdTemp[ZONE_CENTER_ACT][nStep] = (unsigned char) (((float)nMaxThreshold - (float)nMinThreshold) / 256.0 * nStep + nMinThreshold );
	}

	//ZONE_LEFT_PAD
	nMinThreshold = stMaskInformation.nMinRangeLPad;
	nMaxThreshold = stMaskInformation.nMaxRangeLPad;	

	if( nMinThreshold > nMaxThreshold )
		nMaxThreshold = nMinThreshold;

	for( int nStep = 0 ; nStep <= 255; nStep++ )
	{
		chThresholdTemp[ZONE_LEFT_PAD][nStep] = (unsigned char) (((float)nMaxThreshold - (float)nMinThreshold) / 256.0 * nStep + nMinThreshold );
	}

	//ZONE_RIGHT_PAD
	nMinThreshold = stMaskInformation.nMinRangeRPad;
	nMaxThreshold = stMaskInformation.nMaxRangeRPad;	

	if( nMinThreshold > nMaxThreshold )
		nMaxThreshold = nMinThreshold;

	for( int nStep = 0 ; nStep <= 255; nStep++ )
	{
		chThresholdTemp[ZONE_RIGHT_PAD][nStep] = (unsigned char) (((float)nMaxThreshold - (float)nMinThreshold) / 256.0 * nStep + nMinThreshold );
	}
}



/**	
@brief	넘겨 받은 데이터로 GPU 메모리 상에 텍스쳐 생성
@param	SIMAGE_INFO
@return	없음
@remark	
@author	고정진
@date	2010/4/19  11:27
*/
void _ACC_CreateTextureData()
{	
	checkCudaErrors( cudaMemcpyToSymbol(chThreshold, chThresholdTemp, sizeof(unsigned char) * MAX_GRAY_LEVEL * MAX_ACTIVE_ZONE) );	
	//checkCudaErrors( cudaMemcpyToSymbol(chThreshold, chThresholdTemp, sizeof(char) * 256 ) );	
// 	checkCudaErrors( cudaMemcpyToSymbol(chThresholdLPad, chThresholdLTemp, sizeof(char) * 256 ) );
// 	checkCudaErrors( cudaMemcpyToSymbol(chThresholdRPad, chThresholdRTemp, sizeof(char) * 256 ) );
	checkCudaErrors( cudaMemcpyToSymbol((const void*)&stMaskInfo.nScaleX1, (const void*)&stAccImageInfo.nScaleX1, sizeof(SACC_IMAGE_INFO) ) );

	if( stCommonPara.nImageWidth != nCurrentWidth || stCommonPara.nImageHeight != nCurrentHeight )
	{
		_ACC_DeleteTextureData();

		nCurrentWidth = stCommonPara.nImageWidth;
		nCurrentHeight = stCommonPara.nImageHeight;	
		nActImageSize = nCurrentWidth * nCurrentHeight;

		cudaChannelFormatDesc desc	= cudaCreateChannelDesc<unsigned char>();	
		checkCudaErrors( cudaMallocArray(&arrSourceImage, &desc, nCurrentWidth, nCurrentHeight) );
		checkCudaErrors( cudaBindTextureToArray(texSourceImage, arrSourceImage, desc) );	////텍스처와 어레이를 묶는다.			
		
		if(nScaleBuffX == NULL)
			checkCudaErrors( cudaMalloc((void**) &nScaleBuffX, nCurrentWidth * sizeof(uint)));
		if(nScaleBuffY == NULL)
			checkCudaErrors( cudaMalloc((void**) &nScaleBuffY, nCurrentWidth * sizeof(uint)));
		if(nZoneFlag == NULL)
			checkCudaErrors( cudaMalloc((void**) &nZoneFlag, nCurrentWidth * sizeof(uint)));

		cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindUnsigned);
		checkCudaErrors( cudaBindTexture(0,texScaleBuffX, nScaleBuffX, channelDesc) );	////텍스처와 어레이를 묶는다.		
 		checkCudaErrors( cudaBindTexture(0,texScaleBuffY, nScaleBuffY, channelDesc) );	////텍스처와 어레이를 묶는다.
		checkCudaErrors( cudaBindTexture(0,texZoneFlag, nZoneFlag, channelDesc) );	////텍스처와 어레이를 묶는다.

		if(chSearchResult == NULL)
			checkCudaErrors( cudaMalloc((void**) &chSearchResult, nCurrentWidth * nCurrentHeight) );
		if(chSizeFilterBuff == NULL)
			checkCudaErrors( cudaMalloc((void**) &chSizeFilterBuff, nCurrentWidth * nCurrentHeight) );
		if(chSearchXLine == NULL)
			checkCudaErrors( cudaMalloc((void**) &chSearchXLine, nCurrentWidth ));
		if(chSearchYLine == NULL)
			checkCudaErrors( cudaMalloc((void**) &chSearchYLine, nCurrentHeight ));

		checkCudaErrors( cudaMemset( chSearchResult, 0, nCurrentWidth * nCurrentHeight ) );
		checkCudaErrors( cudaMemset( chSizeFilterBuff, 0, nCurrentWidth * nCurrentHeight ) );
		checkCudaErrors( cudaMemset( chSearchXLine, 0, nCurrentWidth));
		checkCudaErrors( cudaMemset( chSearchYLine, 0, nCurrentHeight));
	}
}

/**	
@brief	stPadArea 설정.
@param	stPadArea
@return	nCurrentWidth
@remark	
-		_ACC_Act_SetParameter 이후에 반드시 호출 되어야 한다.		
@author	고정진
@date	2010/4/19  11:39
*/
extern "C"
int _ACC_Act_SetScale(SAOI_PAD_AREA stPadArea )
{	
	//int nPadZone[16] = {0,};
	//memcpy( nPadZone, stPadArea, sizeof(int) * 16 );
	
	//X Scale Set Start
	nScaleBuffXtmp = new unsigned int[nCurrentWidth];

	int nZoneIndex = 0;

	for( int nStep = 0; nStep < nCurrentWidth; nStep++)
	{
		if( nStep > stCommonPara.nZoneStep[nZoneIndex] && nZoneIndex < 4 )
			nZoneIndex++;

		nScaleBuffXtmp[nStep] = stAccImageInfo.nScaleX1 + stCommonPara.nZoneOffset[nZoneIndex];
	}
	//X Scale Set End	

	//Y Scale Set Start
	nScaleBuffYtmp = new unsigned int[nCurrentWidth];
	nZoneFlagTemp = new unsigned int[nCurrentWidth];

	for( int nStep = 0; nStep < nCurrentWidth; nStep++)
	{
		nScaleBuffYtmp[nStep] = stAccImageInfo.nScaleY1;
		nZoneFlagTemp[nStep] = ZONE_CENTER_ACT;
	}

	if( stPadArea.stLeft.nStartX != -1 && stPadArea.stLeft.nEndX != -1)
	{
		for( int nStep = stPadArea.stLeft.nStartX; nStep <= stPadArea.stLeft.nEndX; nStep++)
		{
			nScaleBuffYtmp[nStep] = stAccImageInfo.nScaleY2;
			nScaleBuffXtmp[nStep] = stAccImageInfo.nScaleX2;
			nZoneFlagTemp[nStep] = ZONE_LEFT_PAD;
		}
	}

	if( stPadArea.stRight.nStartX != -1 && stPadArea.stRight.nEndX != -1)
	{
		for( int nStep = stPadArea.stRight.nStartX; nStep <= stPadArea.stRight.nEndX; nStep++)
		{
			nScaleBuffYtmp[nStep] = stAccImageInfo.nScaleY3;
			nScaleBuffXtmp[nStep] = stAccImageInfo.nScaleX3;
			nZoneFlagTemp[nStep] = ZONE_RIGHT_PAD;
		}
	}
	//Y Scale Set End	

	checkCudaErrors( cudaMemcpy(	nScaleBuffX, nScaleBuffXtmp, 
		nCurrentWidth* sizeof(uint), cudaMemcpyHostToDevice));
	delete []nScaleBuffXtmp;

	checkCudaErrors( cudaMemcpy(	nScaleBuffY, nScaleBuffYtmp, 
		nCurrentWidth* sizeof(uint), cudaMemcpyHostToDevice));
	delete []nScaleBuffYtmp;

	checkCudaErrors( cudaMemcpy(	nZoneFlag, nZoneFlagTemp, 
		nCurrentWidth* sizeof(uint), cudaMemcpyHostToDevice));
	delete []nZoneFlagTemp;

	return nCurrentWidth;
}

/**	
@brief	GPU 메모리 자원 수거
@param	없음
@return	없음
@remark	
-		생성한 텍스처를 제거한다.
@author	고정진
@date	2010/4/19  11:42
*/
void _ACC_DeleteTextureData()
{
	////묶었던 텍스처와 어레이를 분리한다.
	checkCudaErrors( cudaUnbindTexture(texSourceImage) );	
	checkCudaErrors( cudaUnbindTexture(texScaleBuffX) );	
	checkCudaErrors( cudaUnbindTexture(texScaleBuffY) );	
	checkCudaErrors( cudaUnbindTexture(texZoneFlag) );	

	//Textuer Memory Delete
	if(arrSourceImage != NULL)
		checkCudaErrors( cudaFreeArray(arrSourceImage) );
	arrSourceImage = NULL;

	if(chSearchResult != NULL)
		checkCudaErrors(cudaFree(chSearchResult));
	chSearchResult = NULL;

	if(chSizeFilterBuff != NULL)
		checkCudaErrors(cudaFree(chSizeFilterBuff));
	chSizeFilterBuff = NULL;

	if(chSearchXLine != NULL)
		checkCudaErrors(cudaFree(chSearchXLine));	
	chSearchXLine = NULL;

	if(chSearchYLine != NULL)
		checkCudaErrors(cudaFree(chSearchYLine));	
	chSearchYLine = NULL;

	if(nScaleBuffX != NULL)
		checkCudaErrors(cudaFree(nScaleBuffX));	
	nScaleBuffX = NULL;

 	if(nScaleBuffY != NULL)
 	 	checkCudaErrors(cudaFree(nScaleBuffY));	
 	nScaleBuffY = NULL;

	if(nZoneFlag != NULL)
		checkCudaErrors(cudaFree(nZoneFlag));	
	nZoneFlag = NULL;

}


/**	
@brief	Search 메인 함수
@param	SIMAGE_INFO, chResultImage, chXLineResult, chYLineResult
@return	없음
@remark	
-		원본 이미지를 입력 받고, 넘겨 받은 char* 에 결과 데이터를 넘겨 준다.
@author	고정진
@date	2010/4/19  11:46
*/
extern "C" 
void _ACC_Act_SearchDefect(
unsigned char* chSourceImage,		///<이미지 정보 구조체 이다.
unsigned char* chResultImage,	///<결과 데이터를 넘겨준다. 이진화된 이미지 형태이다.
unsigned char* chXLineResult,	///<X라인의 탐색 결과를 넘겨준다. Merge 시 사용한다.
unsigned char* chYLineResult= NULL	///<Y라인츼 탐색 결과를 넘겨준다. 현재 사용하지 않는다.
)
{
	////결과 데이터 버퍼를 초기화한다.
	checkCudaErrors( cudaMemset( chSearchResult, 0, nActImageSize ) );
	checkCudaErrors( cudaMemset( chSizeFilterBuff, 0, nActImageSize ) );
	checkCudaErrors( cudaMemset( chSearchXLine, 0, nCurrentWidth));
	checkCudaErrors( cudaMemset( chSearchYLine, 0, nCurrentHeight));
	
	checkCudaErrors( cudaMemcpyToArray(arrSourceImage, 0, 0, chSourceImage, 
			nActImageSize, cudaMemcpyHostToDevice) );

	switch( stCommonPara.nMaskType )
	{
	case MASK_TYPE_DIRECT:
		_ACC_SearchDefectOnBlock_Direct<<<stMaskInformation.nBlockCount, stMaskInformation.nThreadCount>>>
			(  chSearchResult, chSizeFilterBuff, chSearchXLine, chSearchYLine);		
		break;
	case MASK_TYPE_DIRECT_HF:
		_ACC_SearchDefectOnBlock_Direct_HF<<<stMaskInformation.nBlockCount, stMaskInformation.nThreadCount>>>
			(  chSearchResult, chSizeFilterBuff, chSearchXLine, chSearchYLine);	
		break;
	case MASK_TYPE_DIRECT_SIGN:
		_ACC_SearchDefectOnBlock_Direct_Sign<<<stMaskInformation.nBlockCount, stMaskInformation.nThreadCount>>>
			(  chSearchResult, chSizeFilterBuff, chSearchXLine, chSearchYLine);	
		break;
	case MASK_TYPE_AROMATIC_JUMP:
		_ACC_SearchDefectOnBlock_Aromatic_Jump<<<stMaskInformation.nBlockCount, stMaskInformation.nThreadCount>>>
			(  chSearchResult, chSizeFilterBuff, chSearchXLine, chSearchYLine);	
		break;
	case MASK_TYPE_AROMATIC:
		_ACC_SearchDefectOnBlock_Aromatic<<<stMaskInformation.nBlockCount, stMaskInformation.nThreadCount>>>
			(  chSearchResult, chSizeFilterBuff, chSearchXLine, chSearchYLine);
		break;
	case MASK_TYPE_CROSS:
		_ACC_SearchDefectOnBlock_Cross<<<stMaskInformation.nBlockCount, stMaskInformation.nThreadCount>>>
			(  chSearchResult, chSizeFilterBuff, chSearchXLine, chSearchYLine);	
		break;
	case MASK_TYPE_STAR:
		_ACC_SearchDefectOnBlock_Star<<<stMaskInformation.nBlockCount, stMaskInformation.nThreadCount>>>
			(  chSearchResult, chSizeFilterBuff, chSearchXLine, chSearchYLine);		
		break;
	default:
		_ACC_SearchDefectOnBlock_Direct<<<stMaskInformation.nBlockCount, stMaskInformation.nThreadCount>>>
			(  chSearchResult, chSizeFilterBuff, chSearchXLine, chSearchYLine);		
		break;
	}

	 cudaThreadSynchronize();

	//결과 데이터를 메인 메모리로 넘겨준다.
	checkCudaErrors( cudaMemcpy(	chResultImage, chSearchResult, 
								nActImageSize, cudaMemcpyDeviceToHost));

	checkCudaErrors( cudaMemcpy(	chXLineResult, chSearchXLine, 
								nCurrentWidth, cudaMemcpyDeviceToHost));

	checkCudaErrors( cudaMemcpy(	chYLineResult, chSearchYLine, 
								nCurrentHeight, cudaMemcpyDeviceToHost));
}