/** 
@addtogroup	ACT_MAIN_INSPECTOR
@{
*/ 

/**
@file		ActMainInspector.h
@brief	메인 프로세스
@remark	
-			
@author	고정진
@date	2010/4/15  12:39
*/
#pragma once
#ifndef		ACT_MAININSPECTOR_H
#define		ACT_MAININSPECTOR_H
#include "ActImageFile.h"
#include "ActImageFilter.h"
#include "DataStructure.h"
#include "ActThreadCreator.h"


/**
@class	CActMainInspector
@brief	메인 프로세스
@remark		
-			검출 프로세스 관리자
@author	고정진
@date	2012/01/11  11:28
*/
class CActMainInspector
{
	typedef InspectDataQ* VSBridge;

public:
	CActMainInspector(void);
	~CActMainInspector(void);			

	void	m_fnParameterSet(	SAOI_IMAGE_INFO	sMaskImage,		
										SAOI_COMMON_PARA	sCommon );

	int*	m_fnDefectSearch( unsigned char* byImageData );

	int*	m_fnDefectSearch_ACC( unsigned char* byImageData );		

	static unsigned int __stdcall	THREAD_PRE_PROC_ACT_ACC(LPVOID PosParam);

	static unsigned int __stdcall	THREAD_POST_PROC_ACT_ACC(LPVOID PosParam);

private:
#ifdef _DEBUG
	void	m_fnStepLogFlagSet( bool bSetFlag );	
#endif		

	unsigned char*  m_fnGetXlineResult();

	unsigned char*  m_fnGetYlineResult();

	unsigned char*  m_fnGetResultImage();

	int		m_fnGetImageWidth();

	int		m_fnGetImageHeight();

	void	m_fnDefectSearch_ACCT( SOBJECT_STR sObject_Str );

	void	m_fnDefectResult_ACCT( SRESOBJECT_STR sResObject_Str );	

	void	m_fnDefectResult_ACCT(unsigned char* chResultImage, 
											unsigned char* chXLineResult, 
											unsigned char* chYLineResult, 
											void* vpObject);

	bool	m_fnImageFileRead( char* chFileFullPath );
	
	void	m_fnDataBufferDelete();

	void	m_fnDataBufferCreate();
	
private:
	ActImageFilter			m_cImageFilter;			///<ImageFilter Class 형 멤버
	ActImageFile			m_cImageInfo;				///<ImageFile Class 형 멤버

	SAOI_IMAGE_INFO		m_stMaskImageInfo;	///<SMASK_AOI_IMAGEINFO
	SAOI_COMMON_PARA	m_stCommonPara;	///<SCOMMON_AOI_PARA
	SACC_PROCESS_CALL	fnAccProcessCall[4];
	IplImage*					m_cvResultImage;	///<_ACC result Image 를 저장하기 위해 사용한다.
	
	unsigned char*		m_chSourceImage;
	unsigned char*		m_chResultImage;		///<Image Data 저장소
	unsigned char*		m_chXLineResult;			///<해당열 Defect 검출 Flag	
	unsigned char*		m_chYLineResult;			///<해당행 Defect 검출 Flag

	int		m_nImageWidth;
	int		m_nImageHeight;
	int		m_nImageSize;
	
};

#endif
/** 
@}
*/ 


