/**
@file	Anaim_ACC_Kernel.cu
@brief	커널단에서 수행되는 코드이다.
@remark	
@author	고정진
@date	2010/4/19  12:03
*/

#ifndef ANAIM_PAD_KERNEL_H_
#define ANAIM_PAD_KERNEL_H_

#include "_ACCStructure.h"

// Texture reference for reading image
texture<unsigned char, 2> texPadSourceImage1;	///<커널에서 사용하는 텍스처 이다. 어레이와 동기화 된다.
texture<unsigned char, 2> texPadSourceImage2;	///<커널에서 사용하는 텍스처 이다. 어레이와 동기화 된다.
texture<unsigned char, 2> texPadSourceImage3;	///<커널에서 사용하는 텍스처 이다. 어레이와 동기화 된다.

__constant__ unsigned char chPadThreshold[256];
__constant__ SACC_IMAGE_INFO stPadMaskInfo;

__device__ inline unsigned char	_ACC_RGB_Compare( 
	unsigned char chPixel_1,
	unsigned char chPixel_2
	);

__device__ inline bool	_ACC_Eraser_PixelX(	
	unsigned char *p_chSearchResult, 										
	unsigned char *p_chSizeFilterBuff,										
	int nXPos, int nYPos										
	);

__device__ inline bool	_ACC_Eraser_PixelY(	
	unsigned char *p_chSearchResult, 											
	unsigned char *p_chSizeFilterBuff,										
	int nXPos, int nYPos
	);

__device__ inline bool	_ACC_EdgeIgnore(	
	unsigned char *p_chSearchResult,										
	int nXPos, int nYPos									
	);

__device__ inline bool	_ACC_EdgeCompare( 
	unsigned char *p_chSearchResult, 										  				    
	int nXPos, int nYPos,
	unsigned char	chComparePixel1,
	unsigned char	chComparePixel2,
	unsigned char	chPixelData 
	);


/**	
@brief	Edge 탐색 함수이다.
@param 	검출 결과 이미지 버퍼, 이미지 폭, 이미지 높이, 패턴 사이즈 X,Y, 
-		연산 좌표 X,Y, 외곽 제거 두께
@return 해당 픽셀의 결함 여부(DEFECT : 결함, NORMAL : 정상)
@remark	
-		결함으로 판정된 좌표를 에지 연산을 통하여 최종 결함 유무를 판단한다.
@author	고정진
@date	2010/4/28  15:07
*/
__device__ inline bool	_ACC_EdgeIgnore(	
	unsigned char *p_chSearchResult, 																
	int nXPos, int nYPos
	)
{
	unsigned char	chPixelData		= '\0';
	unsigned char	chComparePixel1	= '\0';
	unsigned char	chComparePixel2	= '\0';
	
	bool			bResult			= DEFECT;	//초기화 시 DEFECT 로 한다. 필수!!
	bool			bLRSignal		= false;
	bool			bTDSignal		= false;
	
	//방향성 검사. 좌우 방향으로 검사가 가능한지 판단.
	if( (nXPos - stPadMaskInfo.nScaleX1) >= stPadMaskInfo.nOutLineLeft && (nXPos + stPadMaskInfo.nScaleX1) < stPadMaskInfo.nImageWidth - stPadMaskInfo.nOutLineRight)
		bLRSignal = true;
	
	//방향성 검사. 상하 방향으로 검사가 가능한지 판단.
	if( (nYPos - stPadMaskInfo.nScaleY1) >= stPadMaskInfo.nOutLineUp && (nYPos + stPadMaskInfo.nScaleY1) < stPadMaskInfo.nImageHeight - stPadMaskInfo.nOutLineDown)
		bTDSignal = true;

	chPixelData = tex2D( texPadSourceImage1, nXPos, nYPos );

	if( bLRSignal )
	{
		chComparePixel1 = tex2D( texPadSourceImage1, nXPos - stPadMaskInfo.nScaleX1, nYPos );
		chComparePixel2 = tex2D( texPadSourceImage1, nXPos + stPadMaskInfo.nScaleX1, nYPos );

		bResult = _ACC_EdgeCompare(	p_chSearchResult, 
									nXPos, nYPos, chComparePixel1, chComparePixel2, chPixelData );
	}

	if( bTDSignal && bResult == DEFECT )
	{
		chComparePixel1 = tex2D( texPadSourceImage1, nXPos, nYPos - stPadMaskInfo.nScaleY1 );
		chComparePixel2 = tex2D( texPadSourceImage1, nXPos, nYPos + stPadMaskInfo.nScaleY1 );

		bResult = _ACC_EdgeCompare(	p_chSearchResult, 
			nXPos, nYPos, chComparePixel1, chComparePixel2, chPixelData );
	}

	return bResult;

}

/**	
@brief	Edge 부인지 판단한다.
@param 	검출 결과 이미지 버퍼, 이미지 폭, 이미지 높이, 연산 좌표 X,Y,
-		비교 데이터 1,2, 원점 데이터
@return	해당 픽셀의 결함 여부(DEFECT : 결함, NORMAL : 정상)
@remark
-		좌우 픽셀 또는 상하 픽셀의 차를 이용하여 Edge 부인지 판단한다.
-		Edge 면의 밝기가 약간 중간 정도의 밝기를 가지는 원리를 이용하였다.
@author	고정진
@date	2010/4/28  15:49
*/
__device__ inline bool	_ACC_EdgeCompare( 
	unsigned char *p_chSearchResult, 										  					    
	int nXPos, int nYPos,
	unsigned char	chComparePixel1,
	unsigned char	chComparePixel2,
	unsigned char	chPixelData 
	)
{
	unsigned int	nTargetStep		= 0;
	//int				nOffset			= 0;
	int				nSubValue		= 0;

	nSubValue = abs((int)chComparePixel2 - (int)chComparePixel1);

	if( nSubValue > (int)chPixelData )
	{		
		nTargetStep = (stPadMaskInfo.nImageWidth * nYPos) + nXPos + (stPadMaskInfo.nImageOffset * nYPos);
		p_chSearchResult[nTargetStep]	= 0x00;

		return NORMAL;
	}

	return DEFECT;
}

/**	
@brief	X 축 방향 최소 검출 사이즈 측정
@param 	검출 결과 이미지 버퍼, 이미지 폭, 이미지 높이, 패턴 사이즈 X,Y, 
-		연산 좌표 X,Y, 최소 검출 사이즈
@return	해당 픽셀의 결함 여부(DEFECT : 결함, NORMAL : 정상)
@remark
-		해당 검출 좌표의 폭이 최소 검출 치를 넘지 않는다면 NORMAL 처리
@author	고정진
@date	2010/4/28  15:56
*/
__device__ inline bool	_ACC_Eraser_PixelX(	
	unsigned char *p_chSearchResult, 
	unsigned char *p_chSizeFilterBuff,																				
	int nXPos, int nYPos											
	)
{
	unsigned char	chPixelData = '\0';
	unsigned char	chComparePixel = '\0';
	unsigned int	nTargetStep = 0;	
	unsigned int	nTargetStepTemp = 0;

	if(nXPos < 0 || nYPos < 0 || nXPos >= stPadMaskInfo.nImageWidth || nYPos >= stPadMaskInfo.nImageHeight)
		return false;	

	nTargetStep = (stPadMaskInfo.nImageWidth * nYPos) + nXPos + (stPadMaskInfo.nImageOffset * nYPos);

	chPixelData = p_chSizeFilterBuff[nTargetStep];

	int nTotalWhitePixel = 0;

	if(	chPixelData != LIVE_PIXEL )  ///<결함 픽셀이라면,
	{

		//좌픽셀 검사.
		for( int i = 1; i < stPadMaskInfo.nMinimumSizeX; i++ )
		{
			if( ( nXPos - i ) <= 0)
				break;

			nTargetStepTemp = (nXPos - i) + (stPadMaskInfo.nImageWidth * nYPos) + (stPadMaskInfo.nImageOffset * nYPos);

			chComparePixel = p_chSizeFilterBuff[nTargetStepTemp];

			if(	chComparePixel != LIVE_PIXEL )
			{
				nTotalWhitePixel++;
			}
			else
				break;
		}

		if( nTotalWhitePixel >= (stPadMaskInfo.nMinimumSizeX - 1) )
			return DEFECT;

		//우 픽셀 검사
		for( int i = 1; i < stPadMaskInfo.nMinimumSizeX; i++ )
		{			
			if( (nXPos + i) >= stPadMaskInfo.nImageWidth)
				break;
			nTargetStepTemp = ( nXPos + i ) + (stPadMaskInfo.nImageWidth * nYPos) + (stPadMaskInfo.nImageOffset * nYPos);

			chComparePixel = p_chSizeFilterBuff[nTargetStepTemp];

			if(	chComparePixel != LIVE_PIXEL )
			{
				nTotalWhitePixel++;
			}
			else
				break;					
		}

		if( nTotalWhitePixel >= (stPadMaskInfo.nMinimumSizeX - 1) )
			return DEFECT;


		//이곳 까지 왔다면 좌우 기준치 이하이므로
		//픽셀을 복원한다. 0xFF : 불량, 0x00 : 정상
		//nTargetStep = nXPos + (stMaskInfo.nImageWidth * nYPos) + (stMaskInfo.nImageOffset * nYPos);
		p_chSearchResult[nTargetStep]	= LIVE_PIXEL;
		return NORMAL;
	}	
	else ///<결함 픽셀이 아니라면,
		return NORMAL;
}

/**	
@brief	Y 축 방향 최소 검출 사이즈 측정
@param 	검출 결과 이미지 버퍼, 이미지 폭, 이미지 높이, 패턴 사이즈 X,Y, 
-		연산 좌표 X,Y, 최소 검출 사이즈
@return	해당 픽셀의 결함 여부(DEFECT : 결함, NORMAL : 정상)
@remark
-		해당 검출 좌표의 높이가 최소 검출 치를 넘지 않는다면 NORMAL 처리
@author	고정진
@date	2010/4/28  15:56
*/
__device__ inline bool	_ACC_Eraser_PixelY(	
	unsigned char *p_chSearchResult, 
	unsigned char *p_chSizeFilterBuff,																			
	int nXPos, int nYPos											
	)
{
	unsigned char chPixelDataSF		= '\0';
	unsigned char chPixelDataRD		= '\0';
	unsigned char chComparePixel	= '\0';
	unsigned int	nTargetStep = 0;
	unsigned int	nTargetStepTemp = 0;

	nTargetStep = nXPos + (stPadMaskInfo.nImageWidth * nYPos) + (stPadMaskInfo.nImageOffset * nYPos);

	chPixelDataSF = p_chSizeFilterBuff[nTargetStep];
	chPixelDataRD = p_chSearchResult[nTargetStep];

	int nTotalWhitePixel = 0;

	if(	chPixelDataSF == LIVE_PIXEL )
		return NORMAL;

	if(	/*chPixelDataSF != LIVE_PIXEL &&  */chPixelDataRD == LIVE_PIXEL )
	{
		//상 픽셀 검사
		for(int i = 1; i < stPadMaskInfo.nMinimumSizeY; i++)
		{		
			if((nYPos - i) <= 0)
				break;

			nTargetStepTemp = nXPos+ (stPadMaskInfo.nImageWidth * ( nYPos - i )) + (stPadMaskInfo.nImageOffset * nYPos);

			chComparePixel = p_chSizeFilterBuff[nTargetStepTemp];

			if(	chComparePixel != LIVE_PIXEL )
			{
				nTotalWhitePixel++;		
			}
			else
				break;
		}

		if( nTotalWhitePixel >= (stPadMaskInfo.nMinimumSizeY - 1) )
		{
			p_chSearchResult[nTargetStep]	= chPixelDataSF;
			return DEFECT;
		}

		//하 픽셀 검사
		for(int i = 1; i < stPadMaskInfo.nMinimumSizeY; i++ )
		{		
			if((nYPos + i) >= stPadMaskInfo.nImageHeight)
				break;

			nTargetStepTemp = nXPos+ (stPadMaskInfo.nImageWidth * ( nYPos + i)) + (stPadMaskInfo.nImageOffset * nYPos);

			chComparePixel = p_chSizeFilterBuff[nTargetStepTemp];

			if(	chComparePixel != LIVE_PIXEL )
			{
				nTotalWhitePixel++;
			}
			else
				break;							
		}

		if( nTotalWhitePixel >= (stPadMaskInfo.nMinimumSizeY - 1) )
		{
			p_chSearchResult[nTargetStep]	= chPixelDataSF;
			return DEFECT;
		}

		return NORMAL;
	}	
	else
		return DEFECT;

}

/**	
@brief	비교 연산 함수
@param 	비교값 1,2, 오차 허용치
@return	PIXEL_DIFF : 허용치 밖, PIXEL_EQUAL : 허용치 안
@remark
-		
@author	고정진
@date	2010/4/28  16:44
*/
__device__ inline unsigned char	_ACC_RGB_Compare(
	unsigned char chPixel_Src,
	unsigned char chPixel_Des 
	)
{
	int nSrcIndex = (int)chPixel_Src;
	unsigned char nErrorRange = chPadThreshold[nSrcIndex];
	unsigned char chCompare = abs( chPixel_Src - chPixel_Des);

	if( chCompare  > nErrorRange )	///<설정값보다 크다면,
	{
		if( chPixel_Src == 0x00)
			return 0x01;
		else
			return chPixel_Src;
	}
	else										///<아니면
		return LIVE_PIXEL;				///<Zero 를 리턴해라.
}

#endif		