
#pragma once

#ifndef		_ACCSTRUCTURE_H
#define		_ACCSTRUCTURE_H

typedef unsigned int uint;
typedef unsigned char uchar;

static const bool PIXEL_EQUAL	= true;
static const bool PIXEL_DIFF		= false;

static const bool HIGH				= true;
static const bool LOW				= false;

static const bool DEFECT			= true;
static const bool NORMAL			= false;

static const unsigned char		DEAD_PIXEL		= 0xFF;
static const unsigned char		LIVE_PIXEL			= 0x00;

static const int	SCALE_ZERO						= 0;

static const int	TOTAL_INSPECT_COUNT		= 3;			///<최대 비교 횟수

static const  int	MASK_TYPE_DIRECT					= 100;
static const  int	MASK_TYPE_DIRECT_HF			= 101;
static const  int	MASK_TYPE_DIRECT_SIGN			= 102;
static const  int	MASK_TYPE_AROMATIC_JUMP		= 200;
static const  int	MASK_TYPE_AROMATIC				= 300;
static const  int	MASK_TYPE_CROSS					= 400;
static const  int	MASK_TYPE_STAR					= 500;
static const  int	MASK_TYPE_5STAR					= 501;
static const  int	MASK_TYPE_7STAR					= 502;

static const int	MAX_GRAY_LEVEL						= 256;
static const int	MAX_ACTIVE_ZONE					= 3;

static const unsigned int	ZONE_LEFT_PAD						= 0;
static const unsigned int	ZONE_CENTER_ACT					= 1;
static const unsigned int	ZONE_RIGHT_PAD						= 2;

/**
@struct	SIMAGE_INFO
@brief	영상 데이터를 저장.
@remark	
@author	고정진
@date	2010/4/28  16:47
*/
struct SIMAGE_INFO
{
	unsigned char* p_chImageData;				///<이미지 데이터	
	
	SIMAGE_INFO()
	{
		p_chImageData = nullptr;
		memset(this, 0x00, sizeof(SIMAGE_INFO));
	}
};


/**
@struct	SMASK_AOI_IMAGEINFO
@brief	마스크 데이터
@remark	
@author	고정진
@date	2010/4/28  16:47
*/
struct SACC_IMAGE_INFO
{	
	int		nScaleX1;						///<마스크 패턴의 단위
	int		nScaleY1;						///<마스크 패턴의 단위
	int		nScaleX2;						///<마스크 패턴의 단위
	int		nScaleY2;						///<마스크 패턴의 단위
	int		nScaleX3;						///<마스크 패턴의 단위
	int		nScaleY3;						///<마스크 패턴의 단위
	int		nOutLineLeft;					///<제거할 테두리 두께. Default : 1
	int		nOutLineRight;					///<제거할 테두리 두께. Default : 1
	int		nOutLineUp;					///<제거할 테두리 두께. Default : 1
	int		nOutLineDown;				///<제거할 테두리 두께. Default : 1
	int		nMinimumSizeX;				///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
	int		nMinimumSizeY;				///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.	
	int		nMaximumSizeX;				///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
	int		nMaximumSizeY;				///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.	
	int		nDefectDistance;				///<동일 결함으로 간주할 거리.			Default 100 Pixel	
	int		nImageWidth;					///<이미지의 X축 길이
	int		nImageHeight;					///<이미지의 Y축 길이
	int		nImageOffset;
};


/**
@struct	SMASK_AOI_IMAGEINFO
@brief	마스크 데이터
@remark	
@author	고정진
@date	2010/4/28  16:47
*/
struct SAOI_IMAGE_INFO
{	
	int		nScaleX1;						///<마스크 패턴의 단위(Active)
	int		nScaleY1;						///<마스크 패턴의 단위(Active)
	int		nScaleX2;						///<마스크 패턴의 단위(Left Pad)
	int		nScaleY2;						///<마스크 패턴의 단위(Left Pad)
	int		nScaleX3;						///<마스크 패턴의 단위(Right Pad)
	int		nScaleY3;						///<마스크 패턴의 단위(Right Pad)
	int		nMinRangeMain;				///<검출 허용치 Default : 60(Active)
	int		nMaxRangeMain;				///<검출 허용치 Default : 60(Active)
	int		nMinRangeLPad;				///<검출 허용치 Default : 60(Left Pad)
	int		nMaxRangeLPad;				///<검출 허용치 Default : 60(Left Pad)
	int		nMinRangeRPad;				///<검출 허용치 Default : 60(Right Pad)
	int		nMaxRangeRPad;				///<검출 허용치 Default : 60(Right Pad)
	int		nOutLineLeft;					///<제거할 테두리 두께. Default : 1
	int		nOutLineRight;					///<제거할 테두리 두께. Default : 1
	int		nOutLineUp;					///<제거할 테두리 두께. Default : 1
	int		nOutLineDown;				///<제거할 테두리 두께. Default : 1
	int		nBlockCount;					///<_ACC Block, Default : 128
	int		nThreadCount;					///<_ACC Thread, Default : 64
	int		nMinimumSizeX;				///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
	int		nMinimumSizeY;				///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.
	int		nMaximumSizeX;				///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
	int		nMaximumSizeY;				///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.
	SAOI_IMAGE_INFO()
	{
		memset(this, 0x00, sizeof(SAOI_IMAGE_INFO));
	}
};


/**
@struct	SDISSCOLOR_INFO
@brief	마스크 데이터
@remark	
@author	고정진
@date	2010/4/28  16:47
*/
struct SDISSCOLOR_INFO
{	
	int		nScaleX1;						///<마스크 패턴의 단위
	int		nScaleY1;						///<마스크 패턴의 단위	
	int		nMinRange;						///<검출 허용치 Default : 60
	int		nMaxRange;					///<검출 허용치 Default : 60
	int		nGateStart;
	int		nGateHeight;
	int		nImageWidth;
	int		nImageHeight;

	SDISSCOLOR_INFO()
	{
		memset(this, 0x00, sizeof(SDISSCOLOR_INFO));
	}
};

/**
@struct	SCOMMON_AOI_PARA
@brief	공용 파라미터 구조체
@remark	
-		
@author	고정진
@date	2012/4/6  11:15
*/
struct SAOI_COMMON_PARA
{	
	int		nDefectDistance;		///<동일 결함으로 간주할 거리.			Default 100 Pixel	
	int		nImageWidth;			///<이미지의 X축 길이
	int		nImageHeight;			///<이미지의 Y축 길이
	int		nMaskType;			///<마스크의 사이즈	1 : 1, 2 : 5, 3 : 9	
	int		nZoneStep[5];			///렌즈 왜곡에 따른 존 설정
	int		nZoneOffset[5];		///렌즈 왜곡에 따른 존 Offset
	int		bImageLogUsed;		///<이미지 로그 생성 유무					true or false
	SAOI_COMMON_PARA()
	{
		memset(this, 0x00, sizeof(SAOI_COMMON_PARA));
	}
};

/**
@struct	SMASK_AOI_IMAGEINFO
@brief	마스크 데이터
@remark	
@author	고정진
@date	2010/4/28  16:47
*/
struct SAOI_PAD_IMAGE_INFO
{	
	int		nScaleXReserve1;					///<마스크 패턴의 단위
	int		nScaleYReserve1;					///<마스크 패턴의 단위
	int		nScaleXReserve2;				///<마스크 패턴의 단위
	int		nScaleYReserve2;				///<마스크 패턴의 단위
	int		nScaleXReserve3;				///<마스크 패턴의 단위
	int		nScaleYReserve3;				///<마스크 패턴의 단위
	int		nMinRangeMain;				///<검출 허용치 Default : 60(Active)
	int		nMaxRangeMain;				///<검출 허용치 Default : 60(Active)
	int		nMinRangeReserve1;				///<검출 허용치 Default : 60(Left Pad)
	int		nMaxRangeReserve1;				///<검출 허용치 Default : 60(Left Pad)
	int		nMinRangeReserve2;				///<검출 허용치 Default : 60(Right Pad)
	int		nMaxRangeReserve2;				///<검출 허용치 Default : 60(Right Pad)
	int		nOutLineLeft;					///<제거할 테두리 두께. Default : 1
	int		nOutLineRight;					///<제거할 테두리 두께. Default : 1
	int		nOutLineUp;					///<제거할 테두리 두께. Default : 1
	int		nOutLineDown;				///<제거할 테두리 두께. Default : 1
	int		nBlockCount;					///<_ACC Block, Default : 128
	int		nThreadCount;					///<_ACC Thread, Default : 64
	int		nMinimumSizeX;				///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
	int		nMinimumSizeY;				///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.	
	int		nMaximumSizeX;				///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
	int		nMaximumSizeY;				///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.	
	SAOI_PAD_IMAGE_INFO()
	{
		memset(this, 0x00, sizeof(SAOI_PAD_IMAGE_INFO));
	}
};

/**
@struct	SCOMMON_AOI_PARA
@brief	공용 파라미터 구조체
@remark	
-		
@author	고정진
@date	2010/4/29  11:15
*/
struct SAOI_PAD_COMMON_PARA
{	
	int		nDefectDistance;		///<동일 결함으로 간주할 거리.				Default 100 Pixel	
	int		nImageWidth;			///<이미지의 X축 길이
	int		nImageHeight;			///<이미지의 Y축 길이
	int		nMaskType;			///<마스크의 사이즈							1 : 1, 2 : 5, 3 : 9
	int		nZoneStep[5];
	int		nZoneOffset[5];
	int		bImageLogUsed;		///<이미지 로그 생성 유무					true or false

	SAOI_PAD_COMMON_PARA()
	{
		memset(this, 0x00, sizeof(SAOI_PAD_COMMON_PARA));
	}
};


/**
@struct	SAOI_PAD_POS
@brief	공용 파라미터 구조체
@remark	
-		
@author	고정진
@date	2012/5/2  11:15
*/
struct SAOI_PAD_POS
{	
	int		nStartX;		
	int		nStartY;			
	int		nEndX;			
	int		nEndY;

	SAOI_PAD_POS()
	{
		nStartX	=-1;		
		nStartY	=-1;			
		nEndX	=-1;		
		nEndY	=-1;
	}
};


/**
@struct	SAOI_PAD_AREA
@brief	공용 파라미터 구조체
@remark	
-		
@author	고정진
@date	2012/5/2  11:15
*/
struct SAOI_PAD_AREA
{	
	SAOI_PAD_POS stTop;
	SAOI_PAD_POS stDown;
	SAOI_PAD_POS stLeft;
	SAOI_PAD_POS stRight;
};

#endif