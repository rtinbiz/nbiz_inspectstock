/** 
@addtogroup	PAD_FILE_INFOMATION
@{
*/ 

/**
@file		ImageFile.h
@brief	파일의 Information.
@remark	
-			이미지의 Loading, 픽셀 데이터 읽기, 픽셀 값 변경작업.
@author	고정진
@date	2007.10.01
*/
#pragma once
#ifndef		PAD_IMAGEFILE_H
#define		PAD_IMAGEFILE_H
#include "DataStructure.h"

/**
@class	ImageFile
@brief	Processing 할 이미지의 정보를 갖는다.
@remark		
-			_ACC Result Image 에 대한 관리와, Defect Data 를 관리한다.
@author	고정진
@date	2012/01/11  11:28
*/
class PadImageFile
{
public:
	PadImageFile(void);
	~PadImageFile(void);

#ifdef _DEBUG
	int				m_gLogFlag;
#endif
	void	m_fnMemberDataSet(	SAOI_PAD_IMAGE_INFO sMaskInfo,
											SAOI_PAD_COMMON_PARA sCommon);			///마스크 이미지 정보와, 공통 파라미터를 받는다.	

	void 	m_fnResultDefectPos(unsigned char* byResultImage, 
										unsigned char* chXLineResult, 
										unsigned char* chYLineResult = nullptr);	///<결함의 위치와 크기를 검출한다.		

	void 	m_fnSortingDefectPos();

	bool	m_fnReadPosition(int nXPos, int nYPos, SRGB_VALUE* s_RGBValue);	///<해당 위치의 픽셀 정보를 읽는다.	

	void	m_fnReturnDefectData();								///<좌표를 Length 좌표로 변환해 준다. 현재 사용 안함

	bool	m_fnGetDefectNode( int nIndex, DEFECT_DATA* st_DefectData);	///<해당 Index 의 Defect 좌표 정보를 전달한다.

	void	m_fnNorDefectCreate();			///<Size Filter 를 수행한다.

	int		m_fnGetSearchResultCount( );	///<검출된 Defect 의 개수를 리턴한다.

	int*	m_fnGetSearchResultData( );		///<검출된 Defect 리스트를 전달한다.

	void	m_fnDataBufferCreate();

	void	m_fnDataBufferDelete();

private:		
	unsigned char*				m_chImageData;
	SAOI_PAD_IMAGE_INFO			m_stMaskImageInfo;		///<Mask 이미지의 정보.
	SAOI_PAD_COMMON_PARA		m_stCommonPara;		///<공통 파라미터 정보.	
	
	DEFECT_DATA					m_stPreDefectData[MAX_DEFECT];
	DEFECT_DATA					m_stPosDefectData[MAX_DEFECT];
	DEFECT_INFO				m_stDefectInfo[MAX_DEFECT];
	//int					m_nDefectPos[MAX_DEFECT][4];		///<전처리 후 결함 데이터 리스트
	//int					m_nRealDefectPos[MAX_DEFECT][5];	///<후처리 후 결함 데이터 리스트
	int					m_nNorDefectCount;							///<Defect List 의 개수
	int					m_nImageWidth;
	int					m_nImageHeight;
	int					m_nImageOffset;
};
#endif
/** 
@}
*/ 
