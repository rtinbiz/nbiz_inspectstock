/**
@file		ActImageFile.cpp
@brief	이미지 Information 에 대한 구현
@remark	
-			
@author	고정진
@date	2007.09.10
*/

#include "StdAfx.h"
#include "ActImageFile.h"


/**	
@brief	생성자
@param 	없음
@return	없음
@remark
-			파라미터 초기화
@author	고정진
@date	2010/4/28  17:03
*/
ActImageFile::ActImageFile(void)
{
	m_nNorDefectCount = 0;	
	m_nImageWidth = 0;
	m_nImageHeight = 0;
	m_chImageData = nullptr;
}

/**	
@brief	소멸자
@param 	없음
@return	없음
@remark
-		
@author	고정진
@date	2010/4/28  17:03
*/
ActImageFile::~ActImageFile(void)
{
	if( m_chImageData != nullptr)
	{
		//delete[] m_chImageData;
		m_chImageData = nullptr;
	}
}

/**	
@brief	외부 파라미터 수신.
@param 	SAOI_IMAGE_INFO, SAOI_COMMON_PARA
@return	없음
@remark
-		
@author	고정진
@date	2010/4/28  17:04
*/
void	ActImageFile::m_fnMemberDataSet(SAOI_IMAGE_INFO stMaskInfo,
													SAOI_COMMON_PARA stCommon)
{	
	m_stMaskImageInfo	= stMaskInfo;
	m_stCommonPara		= stCommon;	
	
	m_fnDataBufferCreate();

}

/**	
@brief	버퍼생성
@param 	없음
@return	없음
@remark
-		
@author	고정진
@date	2012/12/15  18:11
*/
void	ActImageFile::m_fnDataBufferCreate()
{		
	//메모리 생성
	if ( m_stCommonPara.nImageWidth != this->m_nImageWidth || m_stCommonPara.nImageHeight != this->m_nImageHeight )
	{
		m_fnDataBufferDelete();
		this->m_nImageWidth = m_stCommonPara.nImageWidth;
		this->m_nImageHeight = m_stCommonPara.nImageHeight;
		m_nImageOffset = ((this->m_nImageWidth * 3) % 4);
		//m_chImageData = new unsigned char[this->m_nImageWidth * this->m_nImageHeight];
	}	
}


/**	
@brief	버퍼제거
@param 	없음
@return	없음
@remark
-		
@author	고정진
@date	2012/12/15  18:11
*/
void	ActImageFile::m_fnDataBufferDelete()
{		
	if( m_chImageData != nullptr)
	{
		//delete[] m_chImageData;
		m_chImageData = nullptr;
	}
}


/**	
@brief	Defect 의 위치와 크기 검출.
@param 	1차 필터 처리 이미지, 열 검출 데이터, 행 검출 데이터
@return	없음
@remark
-		
@author	고정진
@date	2010/4/28  17:04
*/
void	ActImageFile::m_fnResultDefectPos(unsigned char* byResultImage, unsigned char* chXLineResult, unsigned char* chYLineResult )
{
	SRGB_VALUE	s_TargetRGBValue;
	bool		bSearchResult	= false;	
	int			nTargetStep	= 0;
	int			nTempBack		= 0;		

	m_chImageData = byResultImage;

	
	ZeroMemory(m_stPreDefectData,sizeof(DEFECT_DATA) * MAX_DEFECT);
	ZeroMemory(m_stDefectInfo,sizeof(DEFECT_INFO) * MAX_DEFECT);

	int nXSumFlag = 0;	
	for(int nXLineCnt = 1; nXLineCnt < this->m_nImageWidth - 1; nXLineCnt++)
	{
		if(chXLineResult[nXLineCnt] == DEAD_PIXEL)
			nXSumFlag++;
	}
	int nYSumFlag = 0;
	for(int nYLineCnt = 0; nYLineCnt < this->m_nImageHeight - 1; nYLineCnt++)
	{	
		if(chYLineResult[nYLineCnt] == DEAD_PIXEL)
			nYSumFlag++;
	}

// 	if( nXSumFlag >= (this->m_nImageWidth / 3) && nYSumFlag >= (this->m_nImageHeight / 3) )
// 	{
// 		m_stPreDefectData[0].StartX = 1;	//입력된 좌표를 셑 한다.
// 		m_stPreDefectData[0].StartY = 1;
// 		m_stPreDefectData[0].EndX = this->m_nImageWidth - 1;
// 		m_stPreDefectData[0].EndY = this->m_nImageHeight - 1;
// 		m_stPreDefectData[0].nDefectCount = 1;
// 		m_stPreDefectData[0].DefectType = ALL_DEFECT;
// 		m_stPreDefectData[0].nDValueAvg = 0;
// 		m_stPreDefectData[0].nDValueMax = 0;
// 		m_stPreDefectData[0].nDValueMin = 0;
// 		return;
// 	}
	
	for(int nXLineCnt = 1; nXLineCnt < this->m_nImageWidth - 1; nXLineCnt++)
	{
		//<해당 열에 결함이 검출되지 않았다면 Skip
		if(chXLineResult[nXLineCnt] == LIVE_PIXEL)
			continue;
		
		for(int nYLineCnt = 0; nYLineCnt < this->m_nImageHeight - 1; nYLineCnt++)
		{			
 			if(chYLineResult[nYLineCnt] == LIVE_PIXEL)
 				continue;

			m_fnReadPosition(nXLineCnt,nYLineCnt,&s_TargetRGBValue);		

			if(	s_TargetRGBValue.nBValue != LIVE_PIXEL) //검출된 에러인가?
			{
				bSearchResult	= TRUE;							//검출 플래그 셑.

				for(int nIndex = 0; nIndex < MAX_DEFECT; nIndex++)
				{
					if(m_stPreDefectData[nIndex].nDefectCount == 0)			//현재 Index 의 시작 좌표가 0 인가?
					{
						m_stPreDefectData[nIndex].nDefectCount++;
						m_stPreDefectData[nIndex].StartX = nXLineCnt;	//입력된 좌표를 셑 한다.
						m_stPreDefectData[nIndex].StartY = nYLineCnt;
						m_stPreDefectData[nIndex].EndX = nXLineCnt;
						m_stPreDefectData[nIndex].EndY = nYLineCnt;

						m_stDefectInfo[nIndex].nDValueMin = (unsigned int)s_TargetRGBValue.nBValue;
						m_stDefectInfo[nIndex].nDValueMax = (unsigned int)s_TargetRGBValue.nBValue;
						m_stDefectInfo[nIndex].nDValueAvg += (unsigned int)s_TargetRGBValue.nBValue;
						break;
					}
					else										//현재 Index 의 시작 좌표가 0 가 아닌가?
					{
						//if((abs(nYLineCnt - m_nDefectPos[nIndex][1]) > m_stCommonPara.nDefectDistance) &&	//입력된 Y 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
						//	(abs(nYLineCnt - m_nDefectPos[nIndex][3]) > m_stCommonPara.nDefectDistance))
						//	continue;

						if((abs(nXLineCnt - m_stPreDefectData[nIndex].StartX) > m_stCommonPara.nDefectDistance) &&	//입력된 Y 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
							(abs(nXLineCnt -m_stPreDefectData[nIndex].EndX) > m_stCommonPara.nDefectDistance))
							continue;


						if(nYLineCnt > m_stPreDefectData[nIndex].StartY && nYLineCnt < m_stPreDefectData[nIndex].EndY )
						{	
							m_stPreDefectData[nIndex].nDefectCount++;

							if(nYLineCnt < m_stPreDefectData[nIndex].StartY)		//입력된 X 좌표가 현재 Index 의 시작 좌표보다 작은가?
							{								
								m_stPreDefectData[nIndex].StartY = nYLineCnt;	//시작 좌표를 입력된 좌표로 설정한다.
							}
							else if(nYLineCnt > m_stPreDefectData[nIndex].EndY)//입력된 X 좌표가 현재 Index 의 끝 좌표보다 큰가?
							{
								m_stPreDefectData[nIndex].EndY = nYLineCnt;	//끝 좌표를 입력된 좌표로 설정한다.
							}

							if( (unsigned int)s_TargetRGBValue.nBValue > m_stDefectInfo[nIndex].nDValueMax )
								m_stDefectInfo[nIndex].nDValueMax = (unsigned int)s_TargetRGBValue.nBValue;

							if( (unsigned int)s_TargetRGBValue.nBValue < m_stDefectInfo[nIndex].nDValueMin )
								m_stDefectInfo[nIndex].nDValueMin = (unsigned int)s_TargetRGBValue.nBValue;

							m_stDefectInfo[nIndex].nDValueAvg += (unsigned int)s_TargetRGBValue.nBValue;
							
							m_stPreDefectData[nIndex].EndX = nXLineCnt;
							break;
						}
						
						else if((abs(nYLineCnt - m_stPreDefectData[nIndex].StartY) > m_stCommonPara.nDefectDistance) &&    //입력된 X 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
						   (abs(nYLineCnt - m_stPreDefectData[nIndex].EndY) > m_stCommonPara.nDefectDistance))	//입력된 X 좌표의 거리가 끝 좌표와 설정된 오차 범위 밖 인가?
						  				//입력된 Y 좌표의 거리가 끝 좌표와 설정된 오차 범위 밖 인가?
						{
							continue;	//다음 포인트를 읽는다.
						}

						else			//오차 범위 내 이라면?
						{	
							m_stPreDefectData[nIndex].nDefectCount++;

							if(nYLineCnt < m_stPreDefectData[nIndex].StartY)		//입력된 X 좌표가 현재 Index 의 시작 좌표보다 작은가?
							{								
								m_stPreDefectData[nIndex].StartY = nYLineCnt;	//시작 좌표를 입력된 좌표로 설정한다.
							}
							else if(nYLineCnt > m_stPreDefectData[nIndex].EndY)//입력된 X 좌표가 현재 Index 의 끝 좌표보다 큰가?
							{
								m_stPreDefectData[nIndex].EndY = nYLineCnt;	//끝 좌표를 입력된 좌표로 설정한다.
							}
							m_stPreDefectData[nIndex].EndX = nXLineCnt;

							if( (unsigned int)s_TargetRGBValue.nBValue > m_stDefectInfo[nIndex].nDValueMax )
								m_stDefectInfo[nIndex].nDValueMax = (unsigned int)s_TargetRGBValue.nBValue;

							if( (unsigned int)s_TargetRGBValue.nBValue < m_stDefectInfo[nIndex].nDValueMin )
								m_stDefectInfo[nIndex].nDValueMin = (unsigned int)s_TargetRGBValue.nBValue;

							m_stDefectInfo[nIndex].nDValueAvg += (unsigned int)s_TargetRGBValue.nBValue;

							break;
						}
					}
				}
			}
			else//결함이 아닌 픽셀인가?
			{

			}
		}
	}	
}


/** 
@brief	Defect 의 위치와 크기 검출.
@param 	없음
@return	Defect List 의 시작 포인터.
@remark	
-			중복 영역에 대한 Merge를 수행한다.
-			전체 Defect 의 개수를 구한다.
@author	고정진
@date	2011/11/29
*/
void 	ActImageFile::m_fnSortingDefectPos()
{
	SRGB_VALUE	s_TargetRGBValue;
	bool		bSearchResult	= FALSE;	
	int			nTargetStep		= 0;
	int			nTempBack		= 0;	
	DEFECT_DATA			nDefectPreTemp[MAX_DEFECT];				///결함 데이터 리스트
	//SDEFECT_JUDGE_LIST_IDSS	m_stDefectIdssListTemp;

	memcpy( nDefectPreTemp, m_stPreDefectData,sizeof(DEFECT_DATA) * MAX_DEFECT );
	ZeroMemory(m_stPreDefectData, sizeof( DEFECT_DATA ) * MAX_DEFECT );	

	if(nDefectPreTemp[0].DefectType == ALL_DEFECT)
	{
		memcpy( &m_stPosDefectData[0], &nDefectPreTemp[0], sizeof(DEFECT_DATA));		
		return;
	}

	for( int nStep = 0; nStep < MAX_DEFECT; nStep++ )
	{
		if ( nDefectPreTemp[nStep].nDefectCount == 0 )
			break;

		bSearchResult	= TRUE;

		for( int nStep2 = 0; nStep2 < MAX_DEFECT; nStep2++ )
		{
			if( nDefectPreTemp[nStep].StartX >= m_stPreDefectData[nStep2].StartX && 
				nDefectPreTemp[nStep].StartY >= m_stPreDefectData[nStep2].StartY &&
				nDefectPreTemp[nStep].EndX <= m_stPreDefectData[nStep2].EndX && 
				nDefectPreTemp[nStep].EndY <= m_stPreDefectData[nStep2].EndY )
			{
				m_stPreDefectData[nStep2].nDefectCount += nDefectPreTemp[nStep].nDefectCount;

				if( m_stDefectInfo[nStep2].nDValueMin > m_stDefectInfo[nStep].nDValueMin )
					m_stDefectInfo[nStep2].nDValueMin = m_stDefectInfo[nStep].nDValueMin;

				if( m_stDefectInfo[nStep2].nDValueMax < m_stDefectInfo[nStep].nDValueMax )
					m_stDefectInfo[nStep2].nDValueMax = m_stDefectInfo[nStep].nDValueMax;

				m_stDefectInfo[nStep2].nDValueAvg += m_stDefectInfo[nStep].nDValueAvg;

				break;
			}
			if( m_stPreDefectData[nStep2].nDefectCount == 0 )				//현재 Index 의 시작 좌표가 0 인가?
			{
				memcpy( &m_stPreDefectData[nStep2], &nDefectPreTemp[nStep], sizeof(DEFECT_DATA));
			
				break;
			}
		}
	}		
}

void	ActImageFile::m_fnResultDefectType(IplImage *pSourceImage, IplImage *pResultImage)
{
	if(m_stPreDefectData[0].DefectType == ALL_DEFECT)
	{
		// Do nothing when defect type is set ALL_DEFECT
		return;
	}

//#define TEST_LOCAL_FILE
#ifdef TEST_LOCAL_FILE
	char szFolder[256] = "Z:\\DefectData";
	char szPath[256] = "";
	sprintf_s(szPath, "%s\\Src.BMP", szFolder);
	cvSaveImage(szPath, pSourceImage);

	sprintf_s(szPath, "%s\\Rst.BMP", szFolder);
	cvSaveImage(szPath, pResultImage);
#endif

	for(int nStep = 0; nStep < MAX_DEFECT; nStep++ )
	{
		if(m_stPreDefectData[nStep].nDefectCount == 0)
			break;	

#ifdef TEST_LOCAL_FILE
		char szText[256];
		sprintf_s(szText, "\n[INS]======== Check Defect (%d) count(%d) size(%d x %d = %d) pos(%d, %d, %d, %d)", nStep, m_stPreDefectData[nStep].nDefectCount, 
			(m_stPreDefectData[nStep].EndY - m_stPreDefectData[nStep].StartY + 1), (m_stPreDefectData[nStep].EndX - m_stPreDefectData[nStep].StartX + 1),
			(m_stPreDefectData[nStep].EndX - m_stPreDefectData[nStep].StartX + 1) * (m_stPreDefectData[nStep].EndY - m_stPreDefectData[nStep].StartY + 1),
			m_stPreDefectData[nStep].StartX, m_stPreDefectData[nStep].StartY,m_stPreDefectData[nStep].EndX, m_stPreDefectData[nStep].EndY);
		OutputDebugStringA(szText);
#endif

		if ( abs(m_stPreDefectData[nStep].EndX - m_stPreDefectData[nStep].StartX) > m_stMaskImageInfo.nMaximumSizeX)
			continue;
		if ( abs(m_stPreDefectData[nStep].EndY - m_stPreDefectData[nStep].StartY) > m_stMaskImageInfo.nMaximumSizeY)
			continue;

		CvRect rcDefect;
		rcDefect.x = m_stPreDefectData[nStep].StartX;
		rcDefect.y = m_stPreDefectData[nStep].StartY;
		rcDefect.width = m_stPreDefectData[nStep].EndX - rcDefect.x + 1;
		rcDefect.height = m_stPreDefectData[nStep].EndY - rcDefect.y + 1;
		m_stPreDefectData[nStep].DefectType = m_fnCheckDefectType(pSourceImage, pResultImage, rcDefect);

#ifdef TEST_LOCAL_FILE
		sprintf_s(szText, "\n[INS]======== -> Defect Type : %d\n", m_stPreDefectData[nStep].DefectType);
		OutputDebugStringA(szText);
#endif
	}
}

#define REF_DIR_NONE 0
#define REF_DIR_UP 1
#define REF_DIR_DOWN 2
#define REF_DIR_LEFT 3
#define REF_DIR_RIGHT 4

int ActImageFile::m_fnCheckDefectType(IplImage *pSourceImage, IplImage *pResultImage, CvRect rcDefect)
{
	// 정상적인 값을 가진 참조 영역의 위치를 구한다.
	CvRect rcRef;
	int nRefDistanceX = 0;
	int nRefDistanceY = 0;
	int nRefDir = m_fnGetReferencePosDefectType(pResultImage, rcDefect, rcRef, nRefDistanceX, nRefDistanceY);
	if(nRefDir == REF_DIR_NONE) {
		return NORMAL_DEFECT;
	}

	cvSetImageROI(pSourceImage, rcRef);
	IplImage *pTemp = cvCloneImage(pSourceImage);
	if(pTemp == 0x00) {
		cvResetImageROI(pSourceImage);
		return NORMAL_DEFECT;
	}
	// Hole 과 Surface 의 경계를 나눌 Threshold 값을 찾는다. Ostu thresholding 값을 사용.
	double nValueThreshold = cvThreshold(pSourceImage, pTemp, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	cvReleaseImage(&pTemp);
	cvResetImageROI(pSourceImage);

	// Hole 과 Surface 의 밝기 평균을 구한다. 
	// Slope 부분은 Threshold 에 따라 Hole 또는 Surface 에 포함된다.
	double nValue;
	double nTotalHoleValue = 0;
	int nTotalHoleCount = 0;
	double nTotalSurfaceValue = 0;
	int nTotalSurfaceCount = 0;
	int nRefStartX = rcRef.x;
	int nRefEndX = nRefStartX + rcRef.width - 1;
	int nRefStartY = rcRef.y;
	int nRefEndY = nRefStartY + rcRef.height - 1;
	for(int nY = nRefStartY; nY <= nRefEndY; nY++) {
		for(int nX = nRefStartX; nX <= nRefEndX; nX++) {
			nValue = (double)CV_IMAGE_ELEM(pSourceImage, UINT8, nY, nX);
			if(nValue > nValueThreshold) {
				// Hole Area
				nTotalHoleValue += nValue;
				nTotalHoleCount++;
			}
			else {
				// Surface Area
				nTotalSurfaceValue += nValue;
				nTotalSurfaceCount++;
			}
		}
	}

	if(nTotalHoleCount == 0 || nTotalSurfaceCount == 0) {
		return NORMAL_DEFECT;
	}

	double nMeanHoleValue = nTotalHoleValue / nTotalHoleCount;
	double nMeanSurfaceValue = nTotalSurfaceValue/ nTotalSurfaceCount;

	// Hole 과 Surface 의 밝기 표준 편차를 구한다.
	// Slope 부분은 Threshold 에 따라 Hole 또는 Surface 에 포함된다.
	nTotalHoleValue = 0;
	nTotalSurfaceValue = 0;
	for(int nY = nRefStartY; nY <= nRefEndY; nY++) {
		for(int nX = nRefStartX; nX <= nRefEndX; nX++) {
			nValue = CV_IMAGE_ELEM(pSourceImage, UINT8, nY, nX);
			if(nValue > nValueThreshold) {
				// Hole Area
				nTotalHoleValue += pow(nValue - nMeanHoleValue, 2);
			}
			else {
				// Surface Area
				nTotalSurfaceValue += pow(nValue - nMeanSurfaceValue, 2);
			}
		}
	}

	double nStdHoleValue;
	if(nTotalHoleCount > 1) {
		nStdHoleValue = sqrt(nTotalHoleValue / (nTotalHoleCount - 1));
	}
	else {
		nStdHoleValue = sqrt(nTotalHoleValue / nTotalHoleCount);
	}
	double nStdSurfaceValue;
	if(nTotalSurfaceCount > 1) {
		nStdSurfaceValue = sqrt(nTotalSurfaceValue / (nTotalSurfaceCount - 1));
	}
	else {
		nStdSurfaceValue = sqrt(nTotalSurfaceValue / nTotalSurfaceCount);
	}

	int nStartX = rcDefect.x;
	int nEndX = nStartX + rcDefect.width - 1;
	int nStartY = rcDefect.y;
	int nEndY = nStartY + rcDefect.height - 1;
	int nRefX;
	int nRefY;

	int nTotalCount = 0;
	int nTotalHoleValueCount = 0;
	int nTotalSurfaceValueCount = 0;
	nTotalHoleCount = 0;
	nTotalSurfaceCount = 0;
	int nTotalSlopeCount = 0;
	double nStdHoleThreshold = 1.0;
	double nStdSurfaceThreshold = 1.0;
	double nStdHoleValueThreshold = 0.3;
	double nStdSurfaceValueThreshold = 0.3;
	for(int nY = nStartY; nY <= nEndY; nY++) {
		for(int nX = nStartX; nX <= nEndX; nX++) {
			nValue = CV_IMAGE_ELEM(pResultImage, UINT8, nY, nX);
			if(nValue == LIVE_PIXEL) {
				continue;
			}

			nRefX = nX + nRefDistanceX;
			nRefY = nY + nRefDistanceY;

			// 위치 체크 : 참조 위치에서 체크. Hole, Surface, Slope 위치 확인
			nValue = CV_IMAGE_ELEM(pSourceImage, UINT8, nRefY, nRefX);
			if(nValue > nMeanHoleValue - nStdHoleValue * nStdHoleThreshold) {
				nTotalHoleCount++;
			}
			else if(nValue < nMeanSurfaceValue + nStdSurfaceValue * nStdSurfaceThreshold) {
				nTotalSurfaceCount++;
			}
			else {
				nTotalSlopeCount++;
			}

			// 밝기 체크 : 현재 위치에서 체크
			nValue = CV_IMAGE_ELEM(pSourceImage, UINT8, nY, nX);
			if(abs(nValue - nMeanHoleValue) < nStdHoleValue * nStdHoleValueThreshold) {
				nTotalHoleValueCount++;
			}
			else if(abs(nValue - nMeanSurfaceValue) < nStdSurfaceValue * nStdSurfaceValueThreshold) {
				nTotalSurfaceValueCount++;
			}

			nTotalCount++;
		}
	}

	BOOL bHolePos = FALSE;
	BOOL bSurfacePos = FALSE;
	BOOL bSlopePos = FALSE;
	double nHolePosRate = (double)nTotalHoleCount / nTotalCount;
	double nSurfacePosRate = (double)nTotalSurfaceCount / nTotalCount;
	double nSlopePosRate = (double)nTotalSlopeCount / nTotalCount;
	double nHolePosRateThreshold = 0.9;
	double nSurfacePosRateThreshold = 0.9;
	double nSlopePosRateThreshold = 0;		// 0% 이상 : 하나라도 있을 경우
	if(nHolePosRate + nSlopePosRate >= nHolePosRateThreshold) {		// Slope 영역도 포함
		bHolePos = TRUE;
	}
	if(nSurfacePosRate + nSlopePosRate >= nSurfacePosRateThreshold) {		// Slope 영역도 포함
		bSurfacePos = TRUE;
	}
	if(nSlopePosRate > nSlopePosRateThreshold) {
		bSlopePos = TRUE;
	}

	BOOL bHoleValue = FALSE;
	BOOL bSurfaceValue = FALSE;
	double nHoleValueRate = (double)nTotalHoleValueCount / nTotalCount;
	double nSurfaceValueRate = (double)nTotalSurfaceValueCount / nTotalCount;
	double nHoleValueRateThreshold = 0.9;
	double nSurfaceValueRateThreshold = 0.9;
	if(nHoleValueRate >= nHoleValueRateThreshold) {
		bHoleValue = TRUE;
	}
	if(nSurfaceValueRate >= nSurfaceValueRateThreshold) {
		bSurfaceValue = TRUE;
	}

#ifdef TEST_LOCAL_FILE
	char szText[256];
	sprintf_s(szText, "\n[INS] Ref Dir : %d, Distance(%d, %d), Area (%d, %d, %d, %d)", nRefDir, nRefDistanceX, nRefDistanceY,
		rcRef.x, rcRef.y, rcRef.x + rcRef.width, rcRef.y + rcRef.width);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Mean Hole : %.2f, Surface : %.2f", nMeanHoleValue, nMeanSurfaceValue);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Std Hole : %.2f, Surface : %.2f", nStdHoleValue, nStdSurfaceValue);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] <POS> Total Count : %d", nTotalCount);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Std Hole Pos Threshold : %.2f, Surface : %.2f", nStdHoleThreshold, nStdSurfaceThreshold);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Hole Count Boundary : > %.2f, Surface : < %.2f", nMeanHoleValue - nStdHoleValue * nStdHoleThreshold,
		nMeanSurfaceValue + nStdSurfaceValue * nStdSurfaceThreshold);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Hole Pos Rate Threshold : %.2f, Surface : %.2f, Slope : %.2f", nHolePosRateThreshold, nSurfacePosRateThreshold, nSlopePosRateThreshold);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Hole Count : %d, Surface : %d, Slope : %d", nTotalHoleCount, nTotalSurfaceCount, nTotalSlopeCount);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] > Hole Pos Rate : %.2f, Surface : %.2f, Slope : %.2f", nHolePosRate, nSurfacePosRate, nSlopePosRate);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] > Is Hole Pos  : %d, Surface : %d, Slope : %d", bHolePos, bSurfacePos, bSlopePos);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] <VALUE> Total Count : %d", nTotalCount);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Std Hole Value Threshold : %.2f, Surface : %.2f", nStdHoleValueThreshold, nStdSurfaceValueThreshold);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Hole Value Boundary : %.2f ~ %.2f, Surface %.2f ~ %.2f", 
		nMeanHoleValue - nStdHoleValue * nStdHoleValueThreshold, nMeanHoleValue + nStdHoleValue * nStdHoleValueThreshold,
		nMeanSurfaceValue - nStdSurfaceValue * nStdSurfaceValueThreshold, nMeanSurfaceValue + nStdSurfaceValue * nStdSurfaceValueThreshold);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Value Rate Threshold : %.2f, Surface : %.2f", nHoleValueRateThreshold, nSurfaceValueRateThreshold);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] Hole Value Count : %d, Surface : %d", nTotalHoleValueCount, nTotalSurfaceValueCount);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] > Hole Value Rate : %.2f, Surface : %.2f", nHoleValueRate, nSurfaceValueRate);
	OutputDebugString(szText);
	sprintf_s(szText, "\n[INS] > Is Hole Value  : %d, Surface : %d", bHoleValue, bSurfaceValue);
	OutputDebugString(szText);
#endif

	// Etching Over : 밝기가 Hole 인데 위치가 Hole 이 아닌경우
	if(bHoleValue && !bHolePos) {
		return ETCHING_OVER_DEFECT;
	}
	
	// Etching Under : 밝기가 Surface 인데 위치가 Surface 가 아닌경우
	if(bSurfaceValue && !bSurfacePos) {
		return ETCHING_UNDER_DEFECT;
	}

	// Stick Upper : 위치가 Hole 일 경우
	if(bHolePos) {
		return STICK_UPPER_DEFECT;
	}

	//  Glass : Hole과 Surface 모두 아니거나, Slope 에 걸쳐있는 경우
	if((!bHolePos && !bSurfacePos) || bSlopePos) {
		return GLASS_UPPER_DEFECT;
	}

	// 위의 경우가 아니 경우 : 순전히 Surface 영역에만 있는 경우.

	return NORMAL_DEFECT;
}

int ActImageFile::m_fnGetReferencePosDefectType(IplImage *pResultImage, CvRect rcDefect, CvRect &rcRef, int &nRefDistanceX, int &nRefDistanceY)
{
#define CHECK_NON_DEFECT_AREA() \
	rcRef.x = nStartX; \
	rcRef.y = nStartY; \
	rcRef.width = nEndX - nStartX + 1; \
	rcRef.height = nEndY - nStartY + 1; \
	cvSetImageROI(pResultImage, rcRef); \
	if(cvCountNonZero(pResultImage) > 0) { \
		cvResetImageROI(pResultImage); \
		continue; \
	} \
	cvResetImageROI(pResultImage); \
	nRefDistanceX = nX - nCenterX; \
	nRefDistanceY = nY - nCenterY; 

	int nSizeX = m_nImageWidth;
	int nSizeY = m_nImageHeight;
	int nGapSizeX = m_stMaskImageInfo.nScaleX1;
	int nGapSizeY = m_stMaskImageInfo.nScaleY1;
	if(nGapSizeX < 100) nGapSizeX = 100;		// minimum 값 설정
	if(nGapSizeY < 100) nGapSizeY = 100;			

	int nHalfGapSizeX = nGapSizeX/2;
	int nHalfGapSizeY = nGapSizeY/2;
	int nCenterX = rcDefect.x + rcDefect.width / 2;
	int nCenterY = rcDefect.y + rcDefect.height / 2;

	int nStartX = nCenterX - nHalfGapSizeX;
	int nEndX = nCenterX + nHalfGapSizeX;
	int nStartY;
	int nEndY;

	// 참고 이미지 위치를 찾는다.
	int nX = nCenterX;
	int nY = nCenterY;
	if(nStartX >= 0 && nEndX <= nSizeX - 1) {
		nY = nCenterY;
		while(nCenterY >= nSizeY / 2 && nY >= 0) {
			nY -= nGapSizeY;     // 위쪽 검색
			nStartY = nY - nHalfGapSizeY;
			nEndY = nY + nHalfGapSizeY;
			if(nStartY < 0) {
				break;
			}

			CHECK_NON_DEFECT_AREA();
			return REF_DIR_UP;
		}

		nY = nCenterY;
		while(nCenterY < nSizeY && nY <= nSizeY - 1) {
			nY += nGapSizeY;      // 아래쪽 검색           
			nStartY = nY - nHalfGapSizeY;
			nEndY = nY + nHalfGapSizeY;
			if(nEndY > nSizeY - 1) {
				break;
			}

			CHECK_NON_DEFECT_AREA();
			return REF_DIR_DOWN;
		}
	}

	nStartY = nCenterY - nHalfGapSizeY;
	nEndY = nCenterY + nHalfGapSizeY;
	nX = nCenterX;
	nY = nCenterY;

	if(nStartY >= 0 && nEndY <= nSizeY - 1) {
		nX = nCenterX;
		while(nCenterX >= nSizeX / 2 && nX >= 0) {
			nX -= nGapSizeX;     // 왼쪽 검색
			nStartX = nX - nHalfGapSizeX;
			nEndX = nX + nHalfGapSizeX;
			if(nStartX < 0) {
				break;
			}

			CHECK_NON_DEFECT_AREA();
			return REF_DIR_LEFT;
		}

		nX = nCenterX;
		while(nCenterX < nSizeX / 2 && nX <= nSizeX - 1) {  
			nX += nGapSizeX;     // 오른쪽 검색
			nStartX = nX - nHalfGapSizeX;
			nEndX = nX + nHalfGapSizeX;
			if(nEndX > nSizeX - 1) {
				break;
			}
			CHECK_NON_DEFECT_AREA();
			return REF_DIR_RIGHT;
		}
	}

	return REF_DIR_NONE;
}


/**	
@brief	Return Data 생성..
@param 	없음
@return	없음
@remark
-			좌표를 Length 좌표로 변환해 준다.
-			현재 사용하지는 않는다.
@author	고정진
@date	2010/4/28  17:04
*/
void	ActImageFile::m_fnReturnDefectData()
{		

	for(int nIndex = 0; nIndex < m_nNorDefectCount ; nIndex++)
	{		
		m_stPreDefectData[nIndex].EndX = m_stPreDefectData[nIndex].EndX - m_stPreDefectData[nIndex].StartX;
		m_stPreDefectData[nIndex].EndY = m_stPreDefectData[nIndex].EndY - m_stPreDefectData[nIndex].StartY;	
	}
}


/**	
@brief	해당 Index 의 Defect 데이터를 전달한다.
@param 	인덱스, 데이터를 전달할 버퍼
@return	OKAY : 정상수행, false : Defect Type Error
@remark
-			현재 사용하지 않는다.
@author	고정진
@date	2010/4/28  17:04
*/
bool	ActImageFile::m_fnGetDefectNode( int nIndex, DEFECT_DATA* st_DefectData)
{	
	if( m_stPosDefectData[nIndex].DefectType > 0 )
	{
		memcpy(st_DefectData, &m_stPosDefectData[nIndex], sizeof(DEFECT_DATA));
// 		st_DefectData->DefectType	= m_nRealDefectPos[nIndex][0];
// 		st_DefectData->StartX			= m_nRealDefectPos[nIndex][1];
// 		st_DefectData->StartY			= m_nRealDefectPos[nIndex][2];
// 		st_DefectData->EndX			= m_nRealDefectPos[nIndex][3];
// 		st_DefectData->EndY			= m_nRealDefectPos[nIndex][4];
		
		return OKAY;
	}
	else
		return false;		
}


/**	
@brief	일반 Defect 버퍼 생성
@param 	없음
@return	없음
@remark
-			일반 Defect 데이터 버퍼를 생성한다.
@author	고정진
@date	2010/4/28  17:04
*/
void	ActImageFile::m_fnNorDefectCreate()
{
	ZeroMemory(&m_stPosDefectData,sizeof(DEFECT_DATA)  * MAX_DEFECT);	

	m_nNorDefectCount = 0;

	if(m_stPreDefectData[0].DefectType == ALL_DEFECT)
	{
		memcpy( &m_stPosDefectData[0], &m_stPreDefectData[0], sizeof(DEFECT_DATA));
		m_nNorDefectCount = 1;
		return;
	}

	for(int nStep = 0; nStep < MAX_DEFECT; nStep++ )
	{
		if(m_stPreDefectData[nStep].nDefectCount == 0)
			break;	
				
		if ( abs(m_stPreDefectData[nStep].EndX - m_stPreDefectData[nStep].StartX) > m_stMaskImageInfo.nMaximumSizeX)
			continue;
		if ( abs(m_stPreDefectData[nStep].EndY - m_stPreDefectData[nStep].StartY) > m_stMaskImageInfo.nMaximumSizeY)
			continue;

		memcpy( &m_stPosDefectData[m_nNorDefectCount], &m_stPreDefectData[nStep], sizeof(DEFECT_DATA));

		// Removed by Charles. Defect Type 을 체크해서 값을 넣는 것으로 수정하였음.
		//m_stPosDefectData[m_nNorDefectCount].DefectType = NORMAL_DEFECT;
		m_stPosDefectData[m_nNorDefectCount].nDValueMin = (int)m_stDefectInfo[nStep].nDValueMin;
		m_stPosDefectData[m_nNorDefectCount].nDValueMax = (int)m_stDefectInfo[nStep].nDValueMax;
		m_stPosDefectData[m_nNorDefectCount].nDValueAvg = (int)(m_stDefectInfo[nStep].nDValueAvg / m_stPreDefectData[nStep].nDefectCount);

		m_nNorDefectCount++;		
	}
}

/**	
@brief	전체 Defect 의 개수 리턴
@param 	없음
@return	m_nNorDefectCount
@remark
-			
@author	고정진
@date	2010/4/28  17:04
*/
int	ActImageFile::m_fnGetSearchResultCount( )
{
	return m_nNorDefectCount;
}


/**	
@brief	Defect Result Buffer 를 리턴한다.
@param 	없음
@return	m_nRealDefectPos or nullptr
@remark
-			
@author	고정진
@date	2010/4/28  17:04
*/
int*	ActImageFile::m_fnGetSearchResultData( )
{
	//if( m_nNorDefectCount > 0 )
	{
		return (int*)m_stPosDefectData;
	}
// 	else
// 		return nullptr;	

	
}

/**	
@brief	이미지 데이터 읽기
@param 	해당 좌표, 넘겨 받을 포인터
@return	true : 정상 수행, false : 입력 좌표 에러
@remark
-			해당 파일, 해당 위치의 RGB 값을 읽어, 넘겨 받은 포인터에 입력.
-			 Bit 버전 에서는 SRGB_VALUE 중 nBValue 만 사용함.
@author	고정진
@date	2010/4/28  17:04
*/
bool	ActImageFile::m_fnReadPosition( int nXPos, int nYPos, SRGB_VALUE* s_RGBValue)
{
	int		nStepPosition = 0;	
	char	strLog[200] = {0,};

	if(nXPos < 0 || nYPos < 0 )
	{
#ifdef _DEBUG
	sprintf_s(strLog,"Limit Under : X = %d, Y = %d.", nXPos,nYPos);		
	//if(m_gLogFlag)					
	this->m_fnAddDebugLog(strLog);
#endif
		return FALSE;
	}
	if(nXPos >= this->m_nImageWidth || nYPos >= this->m_nImageHeight )
	{
#ifdef _DEBUG
	sprintf_s(strLog,"Limit Over : X = %d, Y = %d.", nXPos,nYPos);	
	//if (m_gLogFlag)				
	this->m_fnAddDebugLog(strLog);	
#endif
		return FALSE;
	}
	
	nStepPosition=(nYPos * this->m_nImageWidth) + nXPos + (m_nImageOffset * nYPos);	
	
	memcpy(&s_RGBValue->nBValue,&m_chImageData[nStepPosition], 1);		
	return true;
}

/**	
@brief	로그기록
@param 	로그 데이터
@return	없음
@remark
-			디버깅 용도의 로그 Writer
@author	고정진
@date	2010/4/28  17:04
*/
void ActImageFile::m_fnAddDebugLog(char* strLog)
{
	SYSTEMTIME		systime,fileTime;
	char			cpFileName[128]={0,};
	FILE			*fp;
	WORD			iMonth1;
	WORD			iMonth2;
	HANDLE			hFile;
	errno_t			eErrorCode;	
	FILETIME		tCr,tAc,tWr;
	FILETIME		lCr;
		
	GetLocalTime(&systime);
	sprintf_s(cpFileName,"Z:\\ImageLib_ACC_%02d.log",systime.wDay);

	hFile = CreateFile(cpFileName,GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	GetFileTime(hFile,&tCr,&tAc,&tWr);
	FileTimeToLocalFileTime(&tWr,&lCr);
	FileTimeToSystemTime(&lCr,&fileTime);
	CloseHandle(hFile);
	
	iMonth1 = fileTime.wMonth;
	iMonth2 = systime.wMonth;

	if(iMonth1 == iMonth2)
	{
		eErrorCode = fopen_s(&fp,cpFileName,"a+");		
	}
	else
	{
		eErrorCode = fopen_s(&fp,cpFileName,"w");		
	}

	if(fp != (FILE*)NULL)
	{
		fprintf(fp,"[%02d:%02d:%02d.%03d] %s\n",
			systime.wHour,
			systime.wMinute,
			systime.wSecond,
			systime.wMilliseconds,
			strLog);
		fclose(fp);
	}
}

/**	
@brief	로그기록
@param 	로그 데이터
@return	없음
@remark
-			디버깅 용도의 로그 Writer
@author	고정진
@date	2010/4/28  17:04
*/
void ActImageFile::m_fnAddDebugLog2(char* strLog)
{
	SYSTEMTIME		systime,fileTime;
	char			cpFileName[128]={0,};
	FILE			*fp;
	WORD			iMonth1;
	WORD			iMonth2;
	HANDLE			hFile;
	errno_t			eErrorCode;	
	FILETIME		tCr,tAc,tWr;
	FILETIME		lCr;
		
	GetLocalTime(&systime);
	sprintf_s(cpFileName,"C:\\nbiz\\log\\ImageLibPost_%02d.log",systime.wDay);

	hFile = CreateFile(cpFileName,GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	GetFileTime(hFile,&tCr,&tAc,&tWr);
	FileTimeToLocalFileTime(&tWr,&lCr);
	FileTimeToSystemTime(&lCr,&fileTime);
	CloseHandle(hFile);
	
	iMonth1 = fileTime.wMonth;
	iMonth2 = systime.wMonth;

	if(iMonth1 == iMonth2)
	{
		eErrorCode = fopen_s(&fp,cpFileName,"a+");		
	}
	else
	{
		eErrorCode = fopen_s(&fp,cpFileName,"w");		
	}

	if(fp != (FILE*)NULL)
	{
		fprintf(fp,"[%02d:%02d:%02d.%03d] %s\n",
			systime.wHour,
			systime.wMinute,
			systime.wSecond,
			systime.wMilliseconds,
			strLog);
		fclose(fp);
	}
}