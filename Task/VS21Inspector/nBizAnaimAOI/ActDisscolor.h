/** 
@addtogroup	DISSCOLOR
@{
*/ 

/**
@file		ActDisscolor.h
@brief	Disscolor 재검출 클래스
@remark	
-			
@author	고정진
@date	2012/5/14  13:56
*/

#pragma once
#ifndef	 DISSCOLOR_H
#define	 DISSCOLOR_H

#include "DataStructure.h"

class CActDisscolor
{
public:
	CActDisscolor(void);
	~CActDisscolor(void);

	int		m_fnDisscolor_ParamSet( SDISSCOLOR_INFO stDissImageInfo, char* chAlignImagePath );

	int		m_fnDisscolor_Search( unsigned char* uchTargerImage, int nDefWidth, int nDefHeight );

	int		m_fnDisscolor_Search( IplImage* iplTargetImage, int nDefWidth, int nDefHeight );

	bool	m_fnCompare_AxisX(int nXPos, int nYPos);

	bool	m_fnCompare_AxisY(int nXPos, int nYPos);

	bool	m_fnAlign_Execute(  int nDefWidth, int nDefHeight );

	void	m_fnDataBufferCreate();

	void	m_fnDataBufferDelete();

	bool	m_fnReadPosition( int nXPos, int nYPos, unsigned char* chReadData);

	bool	m_fnRGB_Compare(unsigned char chPixel_1,unsigned char chPixel_2,int n_DefectRange);

public:
private:
	SDISSCOLOR_INFO	m_stDissImageInfo;

	IplImage*	m_iplTargetImage;
	IplImage*	m_iplAlignImage;
	bool*			m_bWidthLineFlag;
	bool*			m_bHeightLineFlag;
	int				m_nImageWidth;
	int				m_nImageHeight;
	int				m_nImageOffset;

	unsigned char			m_chThreshold[256];

};

#endif
/** 
@}
*/ 