/**
@file		PadImageFile.cpp
@brief	이미지 Information 에 대한 구현
@remark	
-			
@author	고정진
@date	2007.09.10
*/

#include "StdAfx.h"
#include "PadImageFile.h"


/**	
@brief	생성자
@param 	없음
@return	없음
@remark
-			파라미터 초기화
@author	고정진
@date	2010/4/28  17:03
*/
PadImageFile::PadImageFile(void)
{
	m_nNorDefectCount = 0;	
	m_nImageWidth = 0;
	m_nImageHeight = 0;
	m_chImageData = nullptr;
}

/**	
@brief	소멸자
@param 	없음
@return	없음
@remark
-		
@author	고정진
@date	2010/4/28  17:03
*/
PadImageFile::~PadImageFile(void)
{
	if( m_chImageData != nullptr)
	{
		m_chImageData = nullptr;
	}
}

/**	
@brief	외부 파라미터 수신.
@param 	SAOI_IMAGE_INFO, SAOI_COMMON_PARA
@return	없음
@remark
-		
@author	고정진
@date	2010/4/28  17:04
*/
void	PadImageFile::m_fnMemberDataSet(SAOI_PAD_IMAGE_INFO stMaskInfo,
														SAOI_PAD_COMMON_PARA stCommon)
{	
	m_stMaskImageInfo	= stMaskInfo;
	m_stCommonPara		= stCommon;	

	m_fnDataBufferCreate();

}

/**	
@brief	버퍼생성
@param 	없음
@return	없음
@remark
-		
@author	고정진
@date	2012/12/15  18:11
*/
void	PadImageFile::m_fnDataBufferCreate()
{		
	//메모리 생성
	if ( m_stCommonPara.nImageWidth != this->m_nImageWidth || m_stCommonPara.nImageHeight != this->m_nImageHeight )
	{
		m_fnDataBufferDelete();
		this->m_nImageWidth = m_stCommonPara.nImageWidth;
		this->m_nImageHeight = m_stCommonPara.nImageHeight;
		m_nImageOffset = ((this->m_nImageWidth * 3) % 4);
		//m_chImageData = new unsigned char[this->m_nImageWidth * this->m_nImageHeight];
	}	
}


/**	
@brief	버퍼제거
@param 	없음
@return	없음
@remark
-		
@author	고정진
@date	2012/12/15  18:11
*/
void	PadImageFile::m_fnDataBufferDelete()
{		
	if( m_chImageData != nullptr)
	{
		//delete[] m_chImageData;
		m_chImageData = nullptr;
	}
}


/**	
@brief	Defect 의 위치와 크기 검출.
@param 	1차 필터 처리 이미지, 열 검출 데이터, 행 검출 데이터
@return	없음
@remark
-		
@author	고정진
@date	2010/4/28  17:04
*/
void	PadImageFile::m_fnResultDefectPos(unsigned char* byResultImage, unsigned char* chXLineResult, unsigned char* chYLineResult )
{
	SRGB_VALUE	s_TargetRGBValue;
	bool		bSearchResult	= false;	
	int			nTargetStep	= 0;
	int			nTempBack		= 0;		

	m_chImageData = byResultImage;
	
	ZeroMemory(m_stPreDefectData,sizeof(DEFECT_DATA) * MAX_DEFECT);
	ZeroMemory(m_stDefectInfo,sizeof(DEFECT_INFO) * MAX_DEFECT);

	int nXSumFlag = 0;	
	for(int nXLineCnt = 1; nXLineCnt < this->m_nImageWidth - 1; nXLineCnt++)
	{
		if(chXLineResult[nXLineCnt] == DEAD_PIXEL)
			nXSumFlag++;
	}
	int nYSumFlag = 0;
	for(int nYLineCnt = 0; nYLineCnt < this->m_nImageHeight - 1; nYLineCnt++)
	{	
		if(chYLineResult[nYLineCnt] == DEAD_PIXEL)
			nYSumFlag++;
	}

	if( nXSumFlag >= (this->m_nImageWidth / 3) && nYSumFlag >= (this->m_nImageHeight / 3) )
	{
		m_stPreDefectData[0].StartX = 1;	//입력된 좌표를 셑 한다.
		m_stPreDefectData[0].StartY = 1;
		m_stPreDefectData[0].EndX = this->m_nImageWidth - 1;
		m_stPreDefectData[0].EndY = this->m_nImageHeight - 1;
		m_stPreDefectData[0].nDefectCount = 1;
		m_stPreDefectData[0].DefectType = ALL_DEFECT;
		m_stPreDefectData[0].nDValueAvg = 0;
		m_stPreDefectData[0].nDValueMax = 0;
		m_stPreDefectData[0].nDValueMin = 0;
		return;
	}

	for(int nXLineCnt = 1; nXLineCnt < this->m_nImageWidth - 1; nXLineCnt++)
	{
		//<해당 열에 결함이 검출되지 않았다면 Skip
		if(chXLineResult[nXLineCnt] == LIVE_PIXEL)
			continue;

		for(int nYLineCnt = 0; nYLineCnt < this->m_nImageHeight - 1; nYLineCnt++)
		{			
			if(chYLineResult[nYLineCnt] == LIVE_PIXEL)
				continue;

			m_fnReadPosition(nXLineCnt,nYLineCnt,&s_TargetRGBValue);		

			if(	s_TargetRGBValue.nBValue != LIVE_PIXEL) //검출된 에러인가?
			{
				bSearchResult	= TRUE;							//검출 플래그 셑.

				for(int nIndex = 0; nIndex < MAX_DEFECT; nIndex++)
				{
					if(m_stPreDefectData[nIndex].nDefectCount == 0)			//현재 Index 의 시작 좌표가 0 인가?
					{
						m_stPreDefectData[nIndex].nDefectCount++;
						m_stPreDefectData[nIndex].StartX = nXLineCnt;	//입력된 좌표를 셑 한다.
						m_stPreDefectData[nIndex].StartY = nYLineCnt;
						m_stPreDefectData[nIndex].EndX = nXLineCnt;
						m_stPreDefectData[nIndex].EndY = nYLineCnt;		

						m_stDefectInfo[nIndex].nDValueMin = (unsigned int)s_TargetRGBValue.nBValue;
						m_stDefectInfo[nIndex].nDValueMax = (unsigned int)s_TargetRGBValue.nBValue;
						m_stDefectInfo[nIndex].nDValueAvg += (unsigned int)s_TargetRGBValue.nBValue;
						break;
					}
					else										//현재 Index 의 시작 좌표가 0 가 아닌가?
					{
						//if((abs(nYLineCnt - m_nDefectPos[nIndex][1]) > m_stCommonPara.nDefectDistance) &&	//입력된 Y 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
						//	(abs(nYLineCnt - m_nDefectPos[nIndex][3]) > m_stCommonPara.nDefectDistance))
						//	continue;

						if((abs(nXLineCnt - m_stPreDefectData[nIndex].StartX) > m_stCommonPara.nDefectDistance) &&	//입력된 Y 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
							(abs(nXLineCnt -m_stPreDefectData[nIndex].EndX) > m_stCommonPara.nDefectDistance))
							continue;


						if(nYLineCnt > m_stPreDefectData[nIndex].StartY && nYLineCnt < m_stPreDefectData[nIndex].EndY )
						{	
							m_stPreDefectData[nIndex].nDefectCount++;

							if(nYLineCnt < m_stPreDefectData[nIndex].StartY)		//입력된 X 좌표가 현재 Index 의 시작 좌표보다 작은가?
							{								
								m_stPreDefectData[nIndex].StartY = nYLineCnt;	//시작 좌표를 입력된 좌표로 설정한다.
							}
							else if(nYLineCnt > m_stPreDefectData[nIndex].EndY)//입력된 X 좌표가 현재 Index 의 끝 좌표보다 큰가?
							{
								m_stPreDefectData[nIndex].EndY = nYLineCnt;	//끝 좌표를 입력된 좌표로 설정한다.
							}

							m_stPreDefectData[nIndex].EndX = nXLineCnt;

							if( (unsigned int)s_TargetRGBValue.nBValue > m_stDefectInfo[nIndex].nDValueMax )
								m_stDefectInfo[nIndex].nDValueMax = (unsigned int)s_TargetRGBValue.nBValue;

							if( (unsigned int)s_TargetRGBValue.nBValue < m_stDefectInfo[nIndex].nDValueMin )
								m_stDefectInfo[nIndex].nDValueMin = (unsigned int)s_TargetRGBValue.nBValue;

							m_stDefectInfo[nIndex].nDValueAvg += (unsigned int)s_TargetRGBValue.nBValue;

							break;
						}
						
						else if((abs(nYLineCnt - m_stPreDefectData[nIndex].StartY) > m_stCommonPara.nDefectDistance) &&    //입력된 X 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
						   (abs(nYLineCnt - m_stPreDefectData[nIndex].EndY) > m_stCommonPara.nDefectDistance))	//입력된 X 좌표의 거리가 끝 좌표와 설정된 오차 범위 밖 인가?
						  				//입력된 Y 좌표의 거리가 끝 좌표와 설정된 오차 범위 밖 인가?
						{
							continue;	//다음 포인트를 읽는다.
						}

						else			//오차 범위 내 이라면?
						{	
							m_stPreDefectData[nIndex].nDefectCount++;

							if(nYLineCnt < m_stPreDefectData[nIndex].StartY)		//입력된 X 좌표가 현재 Index 의 시작 좌표보다 작은가?
							{								
								m_stPreDefectData[nIndex].StartY = nYLineCnt;	//시작 좌표를 입력된 좌표로 설정한다.
							}
							else if(nYLineCnt > m_stPreDefectData[nIndex].EndY)//입력된 X 좌표가 현재 Index 의 끝 좌표보다 큰가?
							{
								m_stPreDefectData[nIndex].EndY = nYLineCnt;	//끝 좌표를 입력된 좌표로 설정한다.
							}
							m_stPreDefectData[nIndex].EndX = nXLineCnt;

							if( (unsigned int)s_TargetRGBValue.nBValue > m_stDefectInfo[nIndex].nDValueMax )
								m_stDefectInfo[nIndex].nDValueMax = (unsigned int)s_TargetRGBValue.nBValue;

							if( (unsigned int)s_TargetRGBValue.nBValue < m_stDefectInfo[nIndex].nDValueMin )
								m_stDefectInfo[nIndex].nDValueMin = (unsigned int)s_TargetRGBValue.nBValue;

							m_stDefectInfo[nIndex].nDValueAvg += (unsigned int)s_TargetRGBValue.nBValue;

							break;
						}
					}
				}
			}
			else//결함이 아닌 픽셀인가?
			{

			}
		}
	}	
}

/** 
@brief	Defect 의 위치와 크기 검출.
@param 	없음
@return	Defect List 의 시작 포인터.
@remark	
-			중복 영역에 대한 Merge를 수행한다.
-			전체 Defect 의 개수를 구한다.
@author	고정진
@date	2011/11/29
*/
void 	PadImageFile::m_fnSortingDefectPos()
{
	SRGB_VALUE	s_TargetRGBValue;
	bool		bSearchResult	= FALSE;	
	int			nTargetStep		= 0;
	int			nTempBack		= 0;	
	DEFECT_DATA			nDefectPreTemp[MAX_DEFECT];				///결함 데이터 리스트
	//SDEFECT_JUDGE_LIST_IDSS	m_stDefectIdssListTemp;

	memcpy( nDefectPreTemp, m_stPreDefectData,sizeof(DEFECT_DATA) * MAX_DEFECT );
	ZeroMemory(m_stPreDefectData, sizeof( DEFECT_DATA ) * MAX_DEFECT );
	
	if(nDefectPreTemp[0].DefectType == ALL_DEFECT)
	{
		memcpy( &m_stPosDefectData[0], &nDefectPreTemp[0], sizeof(DEFECT_DATA));		
		return;
	}

	for( int nStep = 0; nStep < MAX_DEFECT; nStep++ )
	{
		if ( nDefectPreTemp[nStep].DefectType == 0 )
			break;

		bSearchResult	= TRUE;

		for( int nStep2 = 0; nStep2 < MAX_DEFECT; nStep2++ )
		{
			if( nDefectPreTemp[nStep].StartX >= m_stPreDefectData[nStep2].StartX && 
				nDefectPreTemp[nStep].StartY >= m_stPreDefectData[nStep2].StartY &&
				nDefectPreTemp[nStep].EndX <= m_stPreDefectData[nStep2].EndX && 
				nDefectPreTemp[nStep].EndY <= m_stPreDefectData[nStep2].EndY )
			{
				m_stPreDefectData[nStep2].nDefectCount += nDefectPreTemp[nStep].DefectType;

				if( m_stDefectInfo[nStep2].nDValueMin > m_stDefectInfo[nStep].nDValueMin )
					m_stDefectInfo[nStep2].nDValueMin = m_stDefectInfo[nStep].nDValueMin;

				if( m_stDefectInfo[nStep2].nDValueMax < m_stDefectInfo[nStep].nDValueMax )
					m_stDefectInfo[nStep2].nDValueMax = m_stDefectInfo[nStep].nDValueMax;

				m_stDefectInfo[nStep2].nDValueAvg += m_stDefectInfo[nStep].nDValueAvg;

				break;
			}
			if( m_stPreDefectData[nStep2].nDefectCount == 0 )				//현재 Index 의 시작 좌표가 0 인가?
			{
				memcpy( &m_stPreDefectData[nStep2], &nDefectPreTemp[nStep], sizeof(DEFECT_DATA));			
				break;
			}
		}
	}		
}

/**	
@brief	Return Data 생성..
@param 	없음
@return	없음
@remark
-			좌표를 Length 좌표로 변환해 준다.
-			현재 사용하지는 않는다.
@author	고정진
@date	2010/4/28  17:04
*/
void	PadImageFile::m_fnReturnDefectData()
{		

	for(int nIndex = 0; nIndex < m_nNorDefectCount ; nIndex++)
	{		
		m_stPreDefectData[nIndex].EndX = m_stPreDefectData[nIndex].EndX - m_stPreDefectData[nIndex].StartX;
		m_stPreDefectData[nIndex].EndY = m_stPreDefectData[nIndex].EndY - m_stPreDefectData[nIndex].StartY;	
	}
}


/**	
@brief	해당 Index 의 Defect 데이터를 전달한다.
@param 	인덱스, 데이터를 전달할 버퍼
@return	OKAY : 정상수행, false : Defect Type Error
@remark
-			현재 사용하지 않는다.
@author	고정진
@date	2010/4/28  17:04
*/
bool	PadImageFile::m_fnGetDefectNode( int nIndex, DEFECT_DATA* st_DefectData)
{	
	if( m_stPosDefectData[nIndex].DefectType > 0 )
	{
		memcpy(st_DefectData, &m_stPosDefectData[nIndex], sizeof(DEFECT_DATA));
// 		st_DefectData->DefectType	= m_nRealDefectPos[nIndex][0];
// 		st_DefectData->StartX			= m_nRealDefectPos[nIndex][1];
// 		st_DefectData->StartY			= m_nRealDefectPos[nIndex][2];
// 		st_DefectData->EndX			= m_nRealDefectPos[nIndex][3];
// 		st_DefectData->EndY			= m_nRealDefectPos[nIndex][4];
		
		return OKAY;
	}
	else
		return false;		
}

/**	
@brief	일반 Defect 버퍼 생성
@param 	없음
@return	없음
@remark
-			일반 Defect 데이터 버퍼를 생성한다.
@author	고정진
@date	2010/4/28  17:04
*/
void	PadImageFile::m_fnNorDefectCreate()
{	
	ZeroMemory(&m_stPosDefectData,sizeof(DEFECT_DATA)  * MAX_DEFECT);	

	m_nNorDefectCount = 0;

	if(m_stPreDefectData[0].DefectType == ALL_DEFECT)
	{
		memcpy( &m_stPosDefectData[0], &m_stPreDefectData[0], sizeof(DEFECT_DATA));
		m_nNorDefectCount = 1;
		return;
	}

	for(int nStep = 0; nStep < MAX_DEFECT; nStep++ )
	{
		if(m_stPreDefectData[nStep].nDefectCount == 0)
			break;	

		if ( abs(m_stPreDefectData[nStep].EndX - m_stPreDefectData[nStep].StartX) > m_stMaskImageInfo.nMaximumSizeX)
			continue;
		if ( abs(m_stPreDefectData[nStep].EndY - m_stPreDefectData[nStep].StartY) > m_stMaskImageInfo.nMaximumSizeY)
			continue;

		memcpy( &m_stPosDefectData[m_nNorDefectCount], &m_stPreDefectData[nStep], sizeof(DEFECT_DATA));

		m_stPosDefectData[m_nNorDefectCount].DefectType = NORMAL_DEFECT;
		m_stPosDefectData[m_nNorDefectCount].nDValueMin = (int)m_stDefectInfo[nStep].nDValueMin;
		m_stPosDefectData[m_nNorDefectCount].nDValueMax = (int)m_stDefectInfo[nStep].nDValueMax;
		m_stPosDefectData[m_nNorDefectCount].nDValueAvg = (int)(m_stDefectInfo[nStep].nDValueAvg / m_stPreDefectData[nStep].nDefectCount);

		m_nNorDefectCount++;		
	}	
}

/**	
@brief	전체 Defect 의 개수 리턴
@param 	없음
@return	m_nNorDefectCount
@remark
-			
@author	고정진
@date	2010/4/28  17:04
*/
int	PadImageFile::m_fnGetSearchResultCount( )
{
	return m_nNorDefectCount;
}


/**	
@brief	Defect Result Buffer 를 리턴한다.
@param 	없음
@return	m_nRealDefectPos or nullptr
@remark
-			
@author	고정진
@date	2010/4/28  17:04
*/
int*	PadImageFile::m_fnGetSearchResultData( )
{
	//if( m_nNorDefectCount > 0 )
	{
		return (int*)m_stPosDefectData;
	}
// 	else
// 		return nullptr;	


}

/**	
@brief	이미지 데이터 읽기
@param 	해당 좌표, 넘겨 받을 포인터
@return	true : 정상 수행, false : 입력 좌표 에러
@remark
-			해당 파일, 해당 위치의 RGB 값을 읽어, 넘겨 받은 포인터에 입력.
-			 Bit 버전 에서는 SRGB_VALUE 중 nBValue 만 사용함.
@author	고정진
@date	2010/4/28  17:04
*/
bool	PadImageFile::m_fnReadPosition( int nXPos, int nYPos, SRGB_VALUE* s_RGBValue)
{
	int		nStepPosition = 0;	
	char	strLog[200] = {0,};

	if(nXPos < 0 || nYPos < 0 )
	{
		return FALSE;
	}
	if(nXPos >= this->m_nImageWidth || nYPos >= this->m_nImageHeight )
	{
		return FALSE;
	}

	nStepPosition=(nYPos * this->m_nImageWidth) + nXPos + (m_nImageOffset * nYPos);	

	memcpy(&s_RGBValue->nBValue,&m_chImageData[nStepPosition], 1);		
	return true;
}

