/*
*	File Name			:	LinkedList.h
*	Function			:	Linked List 의 template Class
*								저장하는 구조체 선언.
*	Create				:	2008.02.22
*	Author				:	고정진
*	Version				:	1.0
*/
#pragma once

#ifndef		LINKEDMATRIX_H
#define		LINKEDMATRIX_H

/*
*	Module name			:	NODE_DATA
*	Function			:	정의 되지 않은 데이터 노드와 다음 노드를 가리키는 어드레스 정의						
*	Create				:	2008.02.26
*	Author				:	고정진
*	Version				:	1.0
*/
template <typename T>
struct NODE_DATAM
{
	T	st_DataList;
	INT_PTR	nPreviousNode;
	INT_PTR nNextNode;
	NODE_DATAM()
	{
		memset(this, 0x00, sizeof(NODE_DATAM));
	}
};

/*
*	Module name			:	LinkedList
*	Function			:	정의 되지 않은 데이터에 대한, Linked List 정의.
*	Create				:	2008.02.26
*	Author				:	고정진
*	Version				:	1.0
*/
template <typename T>
class LinkedMatrix
{
public:
	LinkedMatrix();
	LinkedMatrix(int nMatCount);
	~LinkedMatrix(void);

	void	m_SetNextNode(T m_ReadData, int nMatNumber);
	T		m_ReadNextNode(int nMatNumber);
	T		m_ReadPreviousNode(int nMatNumber);
	void	m_MoveFirst(int nMatNumber);
	void	m_MoveLast(int nMatNumber);
	int		m_NodeCount(int nMatNumber);
	void	m_PopFirstNode(int nMatNumber);
protected:
	INT_PTR				M_GMAT_COUNT;
	INT_PTR*			M_GSTART_NODE;
	INT_PTR*			M_GLAST_NODE;
	INT_PTR*			M_GCURRENT_NODE;
	int*					M_GNODE_COUNT;			//전체 노드의 갯수.
	INT_PTR*			M_GREAD_ADDRESS;		//다음 노드를 가리키는 어드레스	
	NODE_DATAM<T>*	m_ST_StartNode;			//시작 노드를 저장.
	NODE_DATAM<T>*	m_ST_LastNode;			//항상 마지막 노드를 저장.
	NODE_DATAM<T>*	m_ST_CurrentNode;		//현재 노드를 저장.
	NODE_DATAM<T>*	m_ST_NewNode;			//새로의 노드의 생성.
};

/*
*	Module Name		:	LinkedList
*	Parameter		:	노드 카운트 초기화.
*	Return			:	None
*	Function		:	생성자, 노드 카운트 초기화.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
LinkedMatrix<T>::LinkedMatrix()
{

}

/*
*	Module Name		:	LinkedList
*	Parameter		:	노드 카운트 초기화.
*	Return			:	None
*	Function		:	생성자, 노드 카운트 초기화.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
LinkedMatrix<T>::LinkedMatrix(int nMatCount)
{	
	M_GMAT_COUNT	= nMatCount;
	M_GNODE_COUNT	= new int[nMatCount];
	M_GREAD_ADDRESS = new INT_PTR[nMatCount];
	M_GSTART_NODE	= new INT_PTR[nMatCount];
	M_GLAST_NODE	= new INT_PTR[nMatCount];
	M_GCURRENT_NODE	= new INT_PTR[nMatCount];
	memset(M_GNODE_COUNT,0,sizeof(INT_PTR) * nMatCount);
	memset(M_GREAD_ADDRESS,0,sizeof(INT_PTR) * nMatCount);
	memset(M_GSTART_NODE,0,sizeof(INT_PTR) * nMatCount);
	memset(M_GLAST_NODE,0,sizeof(INT_PTR) * nMatCount);
	memset(M_GCURRENT_NODE,0,sizeof(INT_PTR) * nMatCount);
}

/*
*	Module Name		:	LinkedList
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	소멸자. 노드 자원의 수거.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
LinkedMatrix<T>::~LinkedMatrix(void)
{
	for(int nStep = 0; nStep < M_GMAT_COUNT; nStep++)
	{
		M_GREAD_ADDRESS[nStep] = M_GSTART_NODE[nStep];
		INT_PTR ns = 0;
		INT_PTR nc = M_GNODE_COUNT[nStep];
		INT_PTR ng = this->M_GREAD_ADDRESS[nStep];		
		
		while( ns < nc)
		{
			if( ng != NULL )
			{
				*(INT_PTR*)&m_ST_CurrentNode = this->M_GREAD_ADDRESS[nStep];
				//m_ST_CurrentNode = (NODE_DATA<T>*)(*this->M_GREAD_ADDRESS[nStep]);	
				M_GREAD_ADDRESS[nStep] = m_ST_CurrentNode->nNextNode;
				delete m_ST_CurrentNode;
			}

			ns++;
			if( ns == nc)
				break;
		}	
	}
	
	delete []M_GNODE_COUNT;
	delete []M_GREAD_ADDRESS;
	delete []M_GSTART_NODE;
	delete []M_GLAST_NODE;
	delete []M_GCURRENT_NODE;
}

/*
*	Module Name		:	m_SetNextNode
*	Parameter		:	정의 되지 않은 데이터
*	Return			:	None
*	Function		:	노드의 추가
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
void LinkedMatrix<T>::m_SetNextNode(T m_ReadData,int nMatNumber)
{
	//_CrtSetBreakAlloc(131);
	m_ST_NewNode = new NODE_DATAM<T>;
	int nc = M_GNODE_COUNT[nMatNumber];
	
	if( nc != NULL)
	{
		*(INT_PTR*)&m_ST_LastNode = this->M_GLAST_NODE[nMatNumber];
		m_ST_LastNode->nNextNode    = (INT_PTR)m_ST_NewNode;
		m_ST_NewNode->nPreviousNode	= (INT_PTR)m_ST_LastNode;
		m_ST_NewNode->nNextNode		= 0;
	}
	else
	{
		m_ST_StartNode  = m_ST_NewNode;
		M_GSTART_NODE[nMatNumber]   = (INT_PTR)m_ST_StartNode;
		m_ST_NewNode->nPreviousNode = 0;
	}
	
	m_ST_LastNode = m_ST_NewNode;
	M_GLAST_NODE[nMatNumber] = (INT_PTR)m_ST_LastNode;
	void*	dest	= (void*)&m_ST_NewNode->st_DataList;
	void*	src		= (void*)&m_ReadData;
	int		nLength	= sizeof(T);
	
	memcpy( dest, src, nLength );
	
	(M_GNODE_COUNT[nMatNumber])++;
}

/*
*	Module Name		:	m_DelCurNode
*	Parameter		:	정의 되지 않은 데이터
*	Return			:	None
*	Function		:	노드의 추가
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
void LinkedMatrix<T>::m_PopFirstNode(int nMatNumber)
{
	m_MoveFirst(nMatNumber);

	*(INT_PTR*)&m_ST_CurrentNode = this->M_GREAD_ADDRESS[nMatNumber];

	M_GREAD_ADDRESS[nMatNumber] = m_ST_CurrentNode->nNextNode;
	M_GSTART_NODE[nMatNumber] = m_ST_CurrentNode->nNextNode;

	delete m_ST_CurrentNode;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
	(M_GNODE_COUNT[nMatNumber])--;

}

/*
*	Module Name		:	m_ReadNextNode
*	Parameter		:	NONE
*	Return			:	정의 되지 않은 노드 데이터
*	Function		:	다음 노드의 데이터.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
T LinkedMatrix<T>::m_ReadNextNode(int nMatNumber)
{
	*(INT_PTR*)&m_ST_CurrentNode = this->M_GREAD_ADDRESS[nMatNumber];
	this->M_GREAD_ADDRESS[nMatNumber] = m_ST_CurrentNode->nNextNode;
	return m_ST_CurrentNode->st_DataList;
}

/*
*	Module Name		:	m_ReadNextNode
*	Parameter		:	NONE
*	Return			:	정의 되지 않은 노드 데이터
*	Function		:	다음 노드의 데이터.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
T LinkedMatrix<T>::m_ReadPreviousNode(int nMatNumber)
{
	*(INT_PTR*)&m_ST_CurrentNode = this->M_GREAD_ADDRESS[nMatNumber];
	this->M_GREAD_ADDRESS[nMatNumber] = m_ST_CurrentNode->nPreviousNode;
	return m_ST_CurrentNode->st_DataList;
}

/*
*	Module Name		:	m_MoveFirst
*	Parameter		:	없음
*	Return			:	None
*	Function		:	처음 노드로 이동.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
void LinkedMatrix<T>::m_MoveFirst(int nMatNumber)
{
	M_GREAD_ADDRESS[nMatNumber] = M_GSTART_NODE[nMatNumber];
}

/*
*	Module Name		:	m_MoveLast
*	Parameter		:	없음
*	Return			:	None
*	Function		:	마지막 노드로 이동.
*	Create			:	2008.02.26
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
void LinkedMatrix<T>::m_MoveLast(int nMatNumber)
{
	M_GREAD_ADDRESS[nMatNumber] = M_GLAST_NODE[nMatNumber];
}

/*
*	Module Name		:	m_NodeCount
*	Parameter		:	없음
*	Return			:	노드의 갯수
*	Function		:	노드의 갯수 반환.
*	Create			:	2008.02.25
*	Author			:	고정진
*	Version			:	1.0
*/
template <typename T>
int LinkedMatrix<T>::m_NodeCount(int nMatNumber)
{
	return M_GNODE_COUNT[nMatNumber];
}

#endif