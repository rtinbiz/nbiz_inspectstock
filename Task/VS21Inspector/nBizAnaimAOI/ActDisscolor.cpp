/**
@file		ActDisscolor.cpp
@brief	Disscolor 재처리에 대한 구현
@remark	
-			
@author	고정진
@date	2012.05.14
*/

#include "StdAfx.h"
#include "ActDisscolor.h"


CActDisscolor::CActDisscolor(void)
{
	m_iplTargetImage = nullptr;
	m_iplAlignImage = nullptr;
	m_bWidthLineFlag = nullptr;
	m_bHeightLineFlag = nullptr;

	m_nImageWidth = 0;
	m_nImageHeight = 0;
	m_nImageOffset = 0;
}


CActDisscolor::~CActDisscolor(void)
{
	if( m_iplAlignImage != nullptr)
	{
		delete[] m_bWidthLineFlag;
		delete[] m_bHeightLineFlag;
		m_bWidthLineFlag = nullptr;
		m_bHeightLineFlag = nullptr;	

		cvReleaseImage(&m_iplAlignImage);		
		m_iplAlignImage = nullptr;
	}

	if( m_iplTargetImage != nullptr)
	{
		cvReleaseImage(&m_iplTargetImage);
		m_iplTargetImage = nullptr;
	}
}

/**	
@brief	파라미터 수신
@param 	SAOI_IMAGE_INFO, SAOI_COMMON_PARA
@return	없음
@remark
-			파라미터를 수신하고 필요 버퍼를 생성한다.
-			Threshold 를 계산한다.
@author	고정진
@date	2012/5/14  13:56
*/
int		CActDisscolor::m_fnDisscolor_ParamSet( SDISSCOLOR_INFO stDissImageInfo, char* chAlignImagePath )
{		
	m_stDissImageInfo = stDissImageInfo;

	if( GetFileAttributes(_T(chAlignImagePath))	== -1)
	{		
		return ERR_ALIGN_FILE;			
	}

	m_fnDataBufferCreate();

	int nMinThreshold = stDissImageInfo.nMinRange;
	int nMaxThreshold = stDissImageInfo.nMaxRange;	
	if( nMinThreshold > nMaxThreshold )
		nMaxThreshold = nMinThreshold;

	for( int nStep = 0 ; nStep <= 255; nStep++ )
	{
		m_chThreshold[nStep] = (unsigned char) (((float)nMaxThreshold - (float)nMinThreshold) / 256.0 * nStep + nMinThreshold );
	}

	if( m_iplAlignImage != nullptr)
	{
		cvReleaseImage(&m_iplAlignImage);
		m_iplAlignImage = nullptr;
	}

	m_iplAlignImage = cvLoadImage( chAlignImagePath, CV_LOAD_IMAGE_GRAYSCALE );

	return OKAY;
}


/**	
@brief	버퍼생성
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	없음
@remark
-			버퍼의 사이즈가 변경될 경우에만 버퍼를 재생성한다.
@author	고정진
@date	2012/5/14  13:56
*/
void	CActDisscolor::m_fnDataBufferCreate()
{		
	if ( m_stDissImageInfo.nImageWidth != this->m_nImageWidth || m_stDissImageInfo.nImageHeight != this->m_nImageHeight )
	{
		m_fnDataBufferDelete();
		this->m_nImageWidth = m_stDissImageInfo.nImageWidth;
		this->m_nImageHeight = m_stDissImageInfo.nImageHeight;

		m_bWidthLineFlag = new bool[m_nImageWidth];
		m_bHeightLineFlag = new bool[m_nImageHeight];

		m_nImageOffset = ((this->m_nImageWidth * 3) % 4);		

		m_iplTargetImage = cvCreateImage(cvSize(m_nImageWidth, m_nImageHeight), IPL_DEPTH_8U, 1);
		cvZero(m_iplTargetImage);
	}
}


/**	
@brief	버퍼삭제
@param 	없음
@return	없음
@remark
-			버퍼의 사이즈가 변경될 경우에만 버퍼를 삭제한다.
@author	고정진
@date	2012/5/14  13:56
*/
void	CActDisscolor::m_fnDataBufferDelete()
{	
	if( m_bWidthLineFlag != nullptr )
	{
		delete[] m_bWidthLineFlag;
		m_bWidthLineFlag = nullptr;
	}

	if( m_bHeightLineFlag != nullptr )
	{
		delete[] m_bHeightLineFlag;
		m_bHeightLineFlag = nullptr;
	}	

	if( m_iplAlignImage != nullptr)
	{
		cvReleaseImage(&m_iplAlignImage);
		m_iplAlignImage = nullptr;
	}

	if( m_iplTargetImage != nullptr)
	{
		cvReleaseImage(&m_iplTargetImage);
		m_iplTargetImage = nullptr;
	}	
}

/**	
@brief	검사 수행
@param 	원본이미지, 결함 크기
@return	검출 여부
@remark
-			
@author	고정진
@date	2012/5/14  13:56
*/
// int		CActDisscolor::m_fnDisscolor_Search( char* chTargerImage, int nDefWidth, int nDefHeight )
// {
// 	if( GetFileAttributes(_T(chTargerImage))== -1)
// 	{		
// 		return ERR_SOURCE_FILE;			
// 	}	
// 
// 	m_iplTargetImage = cvLoadImage( chTargerImage, 0 );
// 
// 	return OKAY;
// }

/**	
@brief	검사 수행
@param 	원본이미지, 결함 크기
@return	검출 여부
@remark
-			
@author	고정진
@date	2012/5/14  13:56
*/
int		CActDisscolor::m_fnDisscolor_Search( IplImage* iplTargetImage, int nDefWidth, int nDefHeight )
{
	bool bisGateLine = false;
	int nResultCode = OKAY;
	
	cvZero(m_iplTargetImage);
	cvCopyImage(iplTargetImage, m_iplTargetImage);	

	bisGateLine = m_fnAlign_Execute(nDefWidth, nDefHeight);

	int nStartX = (m_stDissImageInfo.nImageWidth / 2) - (nDefWidth / 2);
	int nEndX = nStartX + nDefWidth;

	int nStartY = (m_stDissImageInfo.nImageHeight / 2) - (nDefHeight / 2);
	int nEndY = nStartY + nDefHeight;

	bool bSearchResult = false;

	if( bisGateLine )
	{
		for(int nAxis_X = nStartX; nAxis_X <= nEndX; nAxis_X++)
		{
			for( int nAxis_Y = nStartY; nAxis_Y < nEndY; nAxis_Y++ )
			{		
				bSearchResult =	m_fnCompare_AxisY(	 nAxis_X, nAxis_Y);

				if( bSearchResult == PIXEL_DIFF &&  m_stDissImageInfo.nScaleX1 != 0 )
					bSearchResult =	m_fnCompare_AxisX(	 nAxis_X, nAxis_Y);

				if( bSearchResult == PIXEL_DIFF )
					return NG;
			}
		}

		nResultCode = OKAY;
	}
	else
		return ERR_ALIGN_FAIL;

	return nResultCode;
}


/**	
@brief	검사 수행
@param 	원본이미지, 결함 크기
@return	검출 여부
@remark
-			
@author	고정진
@date	2012/5/14  13:56
*/
int		CActDisscolor::m_fnDisscolor_Search( unsigned char* uchTargetImage, int nDefWidth, int nDefHeight )
{
	bool bisGateLine = false;
	int nResultCode = OKAY;

	//m_iplTargetImage = iplTargetImage;

	memcpy( m_iplTargetImage->imageData, uchTargetImage, m_iplTargetImage->imageSize );

	bisGateLine = m_fnAlign_Execute(nDefWidth, nDefHeight);

	int nStartX = (m_stDissImageInfo.nImageWidth / 2) - (nDefWidth / 2);
	int nEndX = nStartX + nDefWidth;

	int nStartY = (m_stDissImageInfo.nImageHeight / 2) - (nDefHeight / 2);
	int nEndY = nStartY + nDefHeight;

	bool bSearchResult = false;

	if( bisGateLine )
	{
		for(int nAxis_X = nStartX; nAxis_X <= nEndX; nAxis_X++)
		{
			for( int nAxis_Y = nStartY; nAxis_Y < nEndY; nAxis_Y++ )
			{		
				bSearchResult =	m_fnCompare_AxisY(	 nAxis_X, nAxis_Y);

				if( bSearchResult == PIXEL_DIFF &&  m_stDissImageInfo.nScaleX1 != 0 )
					bSearchResult =	m_fnCompare_AxisX(	 nAxis_X, nAxis_Y);

				if( bSearchResult == PIXEL_DIFF )
					return NG;
			}
		}

		nResultCode = OKAY;

	}
	else
		return ERR_ALIGN_FAIL;

	return nResultCode;
}


/**	
@brief	해당 좌표의 소스 데이터를 읽어 리턴한다.
@param 	해당 좌표, 넘겨줄 포인터
@return	true : 정상 처리, false : 입력 좌표 이상
@remark
-		
@author	고정진
@date	2010/4/28  17:56
*/
bool	CActDisscolor::m_fnReadPosition( int nXPos, int nYPos, unsigned char* chReadData)
{
	int		nStepPosition = 0;	

	if(nXPos < 0 || nYPos < 0 )	
		return FALSE;

	if(nXPos >= this->m_nImageWidth || nYPos >= this->m_nImageHeight )	
		return FALSE;	

	nStepPosition=((nYPos) * this->m_nImageWidth) + nXPos + (m_nImageOffset * nYPos);

	memcpy(chReadData, &m_iplTargetImage->imageData[nStepPosition], 1);

	return true;
}


/**	
@brief	비교 연산 함수
@param 	비교값 1,2, 오차 허용치
@return	PIXEL_DIFF : 허용치 밖, PIXEL_EQUAL : 허용치 안
@remark
-		
@author	고정진
@date	2010/4/28  16:44
*/
bool	CActDisscolor::m_fnRGB_Compare(unsigned char chPixel_1,unsigned char chPixel_2,int n_DefectRange)
{
	int nErrorRange = (int)m_chThreshold[int(chPixel_1)];

	if( abs( (int)chPixel_1 - (int)chPixel_2 ) > nErrorRange )	
		return PIXEL_DIFF;	
	else					
		return PIXEL_EQUAL;	
}

/**	
@brief	최초 검출 함수 이다.
@param 	연산을 수행할 좌표
@return	비교 결과.(PIXEL_EQUAL : 정상, PIXEL_DIFF : 이상)
@remark
-			X축 방향 3회,  Y축 방향 3회 최대 6회 수행한다.
@author	고정진
@date	2010/4/28  16:30
*/
bool CActDisscolor::m_fnCompare_AxisX(	int nXPos, int nYPos )
{	
	unsigned char chPixelData = '\0';
	unsigned char chComparePixel = '\0';	
	unsigned int		nTargetStep   = 0;
	int		nComparePosX  = 0;
	int		nComparePosY  = 0;	
	int		nCompareStep	= 1;
	int		nTempPosX		= 0;

	m_fnReadPosition(nXPos, nYPos, &chPixelData);		

	//좌측면 연산.	
	if( nXPos <= this->m_nImageWidth / 2 )
	{
		if( nXPos - m_stDissImageInfo.nScaleX1 - 1 > 0 )
		{
			nCompareStep = nCompareStep * (-1);
		}

		for( int nPoint = nCompareStep; nPoint < nCompareStep + TOTAL_INSPECT_COUNT; nPoint++ )
		{
			if( nPoint==0 )
				continue;

			nTempPosX = m_stDissImageInfo.nScaleX1 * nPoint;			

			if( nTempPosX + nXPos + 1 >= this->m_nImageWidth )
				break;

			nComparePosX = nXPos + nTempPosX;
			nComparePosY = nYPos;

			m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
			if( m_fnRGB_Compare( chPixelData, chComparePixel, 0 ))
			{
				return PIXEL_EQUAL;
			}			
		}			
	}
	//우측면 연산.
	else// if(nXPos>=IMAGE_X_SCALE/2)
	{
		nCompareStep = 1;

		if( nXPos + m_stDissImageInfo.nScaleX1 + 1 >= this->m_nImageWidth )
		{
			nCompareStep = nCompareStep * (-1);
		}

		for( int nPoint = nCompareStep; nPoint > nCompareStep - TOTAL_INSPECT_COUNT; nPoint-- )
		{
			if( nPoint == 0 )
				continue;

			nTempPosX = m_stDissImageInfo.nScaleX1 * nPoint;

			if( nTempPosX + nXPos - 1 <= 0 )
			{
				break;	
			}

			nComparePosX = nXPos + nTempPosX;
			nComparePosY = nYPos;

			m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
			if( m_fnRGB_Compare( chPixelData, chComparePixel, 0 ) )
			{
				return PIXEL_EQUAL;
			}				
		}		
	}

	return PIXEL_DIFF;
}
/**	
@brief	최초 검출 함수 이다.
@param 	연산을 수행할 좌표
@return	비교 결과.(PIXEL_EQUAL : 정상, PIXEL_DIFF : 이상)
@remark
-			X축 방향 3회,  Y축 방향 3회 최대 6회 수행한다.
@author	고정진
@date	2010/4/28  16:30
*/
bool CActDisscolor::m_fnCompare_AxisY(int nXPos, int nYPos)
{
	unsigned char chPixelData	= '\0';
	unsigned char chComparePixel = '\0';

	int		nComparePosX  = 0;
	int		nComparePosY  = 0;

	m_fnReadPosition(nXPos, nYPos, &chPixelData);	

	//상측면 연산.		
	//각 Scale Step	당 3Point 씩 1Scale 3Point 를 차분 연산한다.	
	int nTempPosY = 0;
	int nCompareStep=1;

	if( nYPos <= this->m_nImageHeight / 2 )
	{
		if( nYPos - m_stDissImageInfo.nScaleY1 - 1 > 0 )
			nCompareStep = nCompareStep * (-1);

		//Y 축 비교는 Y 축 범위 안에서 3회 실시.
		for( int nPoint = nCompareStep; nPoint < (nCompareStep + TOTAL_INSPECT_COUNT); nPoint++ )
		{
			if( nPoint == 0 )
				continue;

			nTempPosY = m_stDissImageInfo.nScaleY1 * nPoint;

			if( nTempPosY + nYPos + 1 >= this->m_nImageHeight )
				return false;		

			nComparePosX = nXPos;
			nComparePosY = nYPos + nTempPosY;

			m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
			if( m_fnRGB_Compare( chPixelData, chComparePixel, 0 ) )
			{
				return PIXEL_EQUAL;
			}				
		}
	}
	//하측면 연산.
	else// if(nYPos>=IMAGE_Y_SCALE/2)
	{
		nCompareStep = 1;
		if( nYPos + m_stDissImageInfo.nScaleY1 + 1 >= this->m_nImageHeight)
			nCompareStep = nCompareStep * (-1);
		//Y 축 비교는 Y 축 범위 안에서 3회 실시.
		for( int nPoint = nCompareStep; nPoint > (nCompareStep - TOTAL_INSPECT_COUNT); nPoint--)
		{
			if( nPoint == 0)
				continue;

			nTempPosY = m_stDissImageInfo.nScaleY1 * nPoint;

			if( nTempPosY + nYPos - 1 <= 0 )
				return false;

			nComparePosX = nXPos;
			nComparePosY = nYPos + nTempPosY;

			if( nComparePosY >= this->m_nImageHeight )
				nComparePosY = 0;

			m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
			if( m_fnRGB_Compare( chPixelData, chComparePixel, 0 ) )
			{
				return PIXEL_EQUAL;	
			}
		}
	}

	return PIXEL_DIFF;
}

/**	
@brief	Align 수행
@param 	없음
@return	없음
@remark
-			
@author	고정진
@date	2012/5/14  13:56
*/
bool	CActDisscolor::m_fnAlign_Execute(  int nDefWidth, int nDefHeight )
{	
	bool bResult = false;

	double min=0, max=0;
	CvPoint left_top;
	left_top.x = 0;
	left_top.y = 0;
	IplImage* MatchBuffer = cvCreateImage( cvSize( m_iplTargetImage->width - m_iplAlignImage->width+1, 
		m_iplTargetImage->height - m_iplAlignImage->height+1 ), IPL_DEPTH_32F, 1 );	// 상관계수를 구할 이미지(C)

	cvSetZero(MatchBuffer);
	cvMatchTemplate(m_iplTargetImage, m_iplAlignImage, MatchBuffer, CV_TM_CCOEFF_NORMED);	// 상관계수를 구하여 C 에 그린다.
	cvMinMaxLoc(MatchBuffer, &min, &max, NULL, &left_top);	// 상관계수가 최대값을 값는 위치 찾기
	cvReleaseImage(&MatchBuffer);

	// 	this->sAlignCenter.usXPos = left_top.x+(m_iplAlignImage->width/2);
	// 	this->sAlignCenter.usYPos = left_top.y+(m_iplAlignImage->height/2);

	// 	int nStart = (m_stDissImageInfo.nImageWidth / 2) - (nDefWidth / 2);
	// 	int nEnd = nStart + nDefWidth;
	// 
	// 	for( int nStep = 0; nStep < m_stDissImageInfo.nImageWidth - 1; nStep ++ )
	// 	{
	// 		if ( nStep >= nStart && nStep <= nEnd )
	// 			m_bWidthLineFlag[nStep] = true;
	// 
	// 		m_bWidthLineFlag[nStep] = false;
	// 	}

	if( max >= 0.9 )
	{
		int nAlignY = left_top.y;

		while( nAlignY >= m_stDissImageInfo.nScaleY1 )
		{
			nAlignY-= m_stDissImageInfo.nScaleY1;
		}

		nAlignY += m_stDissImageInfo.nGateStart;

		int nStart = (m_stDissImageInfo.nImageHeight / 2) - (nDefHeight / 2);
		int nEnd = nStart + nDefHeight;

		for( int nStep = nAlignY; nStep < m_stDissImageInfo.nImageHeight - 1; nStep +=  m_stDissImageInfo.nScaleY1 )
		{
			if ( nStep < nStart && (nStep + m_stDissImageInfo.nGateHeight) > nEnd )
			{
				bResult = true;
				break;
			}
		}
	}
	else
	{
		bResult = false;
	}	

	return bResult;
}