/**
@file		ActImageFilter.cpp
@brief	이미지 Filter 에 대한 구현
@remark	
-			
@author	고정진
@date	2007.09.10
*/

#include "StdAfx.h"
#include "ActImageFilter.h"

/**	
@brief	생성자
@param 	없음
@return	없음
@remark
-			
@author	고정진
@date	2010/4/28  17:37
*/
ActImageFilter::ActImageFilter(void)
{	
	m_nImageWidth = 0;
	m_nImageHeight = 0;
	m_chSizeFilterBuff = nullptr;
}

/**	
@brief	소멸자
@param 	없음
@return	없음
@remark
-			자원 수거
@author	고정진
@date	2010/4/28  17:37
*/
ActImageFilter::~ActImageFilter(void)
{
	if( m_chSizeFilterBuff != nullptr)
	{
		delete[] m_chSizeFilterBuff;
		m_chSizeFilterBuff = nullptr;
	}
}


/**	
@brief	파라미터 수신
@param 	SAOI_IMAGE_INFO, SAOI_COMMON_PARA
@return	없음
@remark
-			파라미터를 수신하고 필요 버퍼를 생성한다.
-			Threshold 를 계산한다.
@author	고정진
@date	2010/4/28  17:39
*/
void	ActImageFilter::m_fnMemberDataSet(SAOI_IMAGE_INFO sMaskInfo,SAOI_COMMON_PARA sCommon)
{	
	m_stMaskImageInfo	= sMaskInfo;
	m_stCommonPara		= sCommon;
	
	int nMinThreshold = m_stMaskImageInfo.nMinRangeMain;
	int nMaxThreshold = m_stMaskImageInfo.nMaxRangeMain;
	//int nThreshRange = abs( nMaxThreshold - nMinThreshold );
	//int nThreshStep = 256 / nThreshRange;	

	if( nMinThreshold > nMaxThreshold )
		nMaxThreshold = nMinThreshold;

	for( int nStep = 0 ; nStep <= 255; nStep++ )
	{
		m_nHThreshold[nStep] = (int) (((float)nMaxThreshold - (float)nMinThreshold) / 256.0 * nStep + nMinThreshold );
	}

	m_fnDataBufferCreate();
}


/**	
@brief	버퍼생성
@param 	SMASK_AOI_IMAGEINFO, SCOMMON_AOI_PARA
@return	없음
@remark
-			버퍼의 사이즈가 변경될 경우에만 버퍼를 재생성한다.
@author	고정진
@date	2012/12/15  18:11
*/
void	ActImageFilter::m_fnDataBufferCreate()
{		
	//메모리 생성
	if ( m_stCommonPara.nImageWidth != this->m_nImageWidth || m_stCommonPara.nImageHeight != this->m_nImageHeight )
	{
		m_fnDataBufferDelete();
		this->m_nImageWidth = m_stCommonPara.nImageWidth;
		this->m_nImageHeight = m_stCommonPara.nImageHeight;
		m_nImageOffset = ((this->m_nImageWidth * 3) % 4);
		m_chSizeFilterBuff = new unsigned char[this->m_nImageWidth * this->m_nImageHeight];
	}	
}


/**	
@brief	버퍼삭제
@param 	없음
@return	없음
@remark
-			버퍼의 사이즈가 변경될 경우에만 버퍼를 삭제한다.
@author	고정진
@date	2012/12/15  18:11
*/
void	ActImageFilter::m_fnDataBufferDelete()
{		
	if( m_chSizeFilterBuff != nullptr)
	{
		delete[] m_chSizeFilterBuff;
		m_chSizeFilterBuff = nullptr;
	}
}


//////////////////////////////////////////////////////////////////////////
//여기서 부터 _ACC 코드와 동일한 처리를 위한 C 코드이다.//////////////
//테스트를 위해 작성한 것이다.///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
/**	
@brief	메인 검출 프로세스 함수이다.
@param 	SIMAGE_INFO, 검출 결과 이미지 버퍼
@return	없음.
@remark	
-			버퍼 셑
@author	고정진
@date	2010/4/28  14:54
*/
void	ActImageFilter::m_fnImageBuffSet(	unsigned char* chSourceImage,	///<원본 이미지 버퍼포인터
													unsigned char* chResultImage	///<검출 결과를 저장할 포인터
												)
{	
	this->m_chSourceImage = chSourceImage;
	this->m_chResultImage = chResultImage;	
}

/**	
@brief	메인 검출 프로세스 함수이다.
@param 	chXLineResult, chYLineResult(현재 사용 안함)
@return	없음.
@remark	
-			전체 검출 프로세스를 관리한다.
@author	고정진
@date	2010/4/28  14:54
*/
void	ActImageFilter::m_fnSearchDefect(	unsigned char* chXLineResult,	///<X라인의 탐색 결과를 넘겨준다. Merge 시 사용한다.
													unsigned char* chYLineResult= nullptr	///<Y라인츼 탐색 결과를 넘겨준다. 현재 사용하지 않는다.
	)
{
	bool	bSearchResult	= false;
	bool	bFindDead		= false;
	int		nAxis_X			= 0;
	int		nAxis_Y			= 0;	
		
	for(nAxis_X = m_stMaskImageInfo.nOutLineLeft; nAxis_X < this->m_nImageWidth - m_stMaskImageInfo.nOutLineRight; nAxis_X++)
	{
		for( nAxis_Y = m_stMaskImageInfo.nOutLineUp; nAxis_Y < this->m_nImageHeight - m_stMaskImageInfo.nOutLineDown; nAxis_Y++ )
		{		
			bSearchResult =	m_fnCompare_AxisX(	 nAxis_X, nAxis_Y);
		}
	}

	//memcpy(m_chSizeFilterBuff, chResultImage, this->m_nImageWidth * this->m_nImageHeight);
	for(nAxis_X = m_stMaskImageInfo.nOutLineLeft; nAxis_X < this->m_nImageWidth - m_stMaskImageInfo.nOutLineRight; nAxis_X++)
	{
		bFindDead		= false;

		for( nAxis_Y = m_stMaskImageInfo.nOutLineUp; nAxis_Y < this->m_nImageHeight - m_stMaskImageInfo.nOutLineDown; nAxis_Y++ )
		{
			bSearchResult = m_fnEraser_PixelX(  nAxis_X, nAxis_Y );
			bSearchResult = m_fnEraser_PixelY(  nAxis_X, nAxis_Y );

			if( bSearchResult == DEFECT )
			{
				bSearchResult = m_fnEdgeIgnore(	 nAxis_X, nAxis_Y );

				if( bSearchResult == DEFECT )
				{					
					bFindDead = DEFECT;			
					chYLineResult[nAxis_Y] = DEAD_PIXEL;	
				}
			}
		}

		if( bFindDead )
			chXLineResult[nAxis_X] = DEAD_PIXEL;	
	}	
}

/**	
@brief	Edge 탐색 함수이다.
@param 연산 좌표 X,Y
@return 해당 픽셀의 결함 여부(DEFECT : 결함, NORMAL : 정상)
@remark	
-			결함으로 판정된 좌표를 에지 연산을 통하여 최종 결함 유무를 판단한다.
@author	고정진
@date	2010/4/28  15:07
*/
bool	ActImageFilter::m_fnEdgeIgnore(	 int nXPos, int nYPos )
{
	unsigned char	chPixelData			= '\0';
	unsigned char	chComparePixel1	= '\0';
	unsigned char	chComparePixel2	= '\0';
	
	bool			bResult			= DEFECT;	
	bool			bLRSignal		= false;
	bool			bTDSignal		= false;
	
	//방향성 검사. 좌우 상항 방향으로 검사가 가능한지 판단.
	if( (nXPos - m_stMaskImageInfo.nScaleX1) >= m_stMaskImageInfo.nOutLineLeft && 
		(nXPos + m_stMaskImageInfo.nScaleX1) < this->m_nImageWidth - m_stMaskImageInfo.nOutLineRight)
		bLRSignal = true;
	
	if( (nYPos - m_stMaskImageInfo.nScaleY1) >= m_stMaskImageInfo.nOutLineUp && 
		(nYPos + m_stMaskImageInfo.nScaleY1) < this->m_nImageHeight - m_stMaskImageInfo.nOutLineDown)
		bTDSignal = true;

	m_fnReadPosition(nXPos, nYPos, &chPixelData);


	if( bLRSignal )
	{
		m_fnReadPosition(nXPos - m_stMaskImageInfo.nScaleX1, nYPos, &chComparePixel1);
		m_fnReadPosition(nXPos + m_stMaskImageInfo.nScaleX1, nYPos, &chComparePixel2);

		bResult = m_fnEdgeCompare(	 nXPos, nYPos, 
									chComparePixel1, chComparePixel2, chPixelData );			
	}

	if( bTDSignal && bResult == DEFECT )
	{
		m_fnReadPosition(nXPos, nYPos - m_stMaskImageInfo.nScaleY1, &chComparePixel1);
		m_fnReadPosition(nXPos, nYPos + m_stMaskImageInfo.nScaleY1, &chComparePixel2);

		bResult = m_fnEdgeCompare(	 nXPos, nYPos, 
									chComparePixel1, chComparePixel2, chPixelData );		
	}

	return bResult;

}
/**	
@brief	Edge 부인지 판단한다.
@param 	연산 좌표 X,Y, 비교 데이터 1,2, 원점 데이터
@return	해당 픽셀의 결함 여부(DEFECT : 결함, NORMAL : 정상)
@remark
-			좌우 픽셀 또는 상하 픽셀의 차를 이용하여 Edge 부인지 판단한다.
-			Edge 면의 밝기가 약간 중간 정도의 밝기를 가지는 원리를 이용하였다.
@author	고정진
@date	2010/4/28  15:49
*/
bool	ActImageFilter::m_fnEdgeCompare( 				   							    
									    int nXPos, int nYPos,
										unsigned char	chComparePixel1,
										unsigned char	chComparePixel2,
										unsigned char	chPixelData )
{
	unsigned int	nTargetStep		= 0;
	//int				nOffset			= 0;
	int				nSubValue		= 0;

	nSubValue = abs((int)chComparePixel2 - (int)chComparePixel1);

	if( nSubValue > (int)chPixelData )
	{		
		nTargetStep = (this->m_nImageWidth * nYPos) + nXPos + (m_nImageOffset * nYPos);				
		m_SetByte(m_chResultImage, nTargetStep, 0x00);		
		return NORMAL;
	}

	return DEFECT;
}

/**	
@brief	X 축 방향 최소 검출 사이즈 측정
@param 	연산 좌표 X,Y, 
@return	해당 픽셀의 결함 여부(DEFECT : 결함, NORMAL : 정상)
@remark
-			해당 검출 좌표의 폭이 최소 검출 치를 넘지 않는다면 NORMAL 처리
@author	고정진
@date	2010/4/28  15:56
*/
bool	ActImageFilter::m_fnEraser_PixelX(	int nXPos, int nYPos	)
{
	unsigned char	chPixelData = '\0';
	unsigned char	chComparePixel = '\0';
	unsigned int	nTargetStep = 0;	

	if(nXPos < 0 || nYPos < 0)
		return false;

	if(nXPos >= this->m_nImageWidth || nYPos >= this->m_nImageHeight)
		return false;	

	nTargetStep = (this->m_nImageWidth * nYPos) + nXPos + (m_nImageOffset * nYPos);

	chPixelData = m_chSizeFilterBuff[nTargetStep];
	
	int nTotalWhitePixel = 0;

	if(	chPixelData == 0xFF )
	{
		//좌픽셀 검사.
		for( int i = 1; i < m_stMaskImageInfo.nMinimumSizeX; i++ )
		{
			if( ( nXPos - i ) <= 0)
				break;

			nTargetStep = (nXPos - i) + (this->m_nImageWidth * nYPos) + (m_nImageOffset * nYPos);

			chComparePixel = m_chSizeFilterBuff[nTargetStep];

			if(	chComparePixel == 0xFF )
			{
				nTotalWhitePixel++;
			}
			else
				break;
		}
		
		if( nTotalWhitePixel >= (m_stMaskImageInfo.nMinimumSizeX - 1) )
			return DEFECT;

		//우 픽셀 검사
		for( int i = 1; i < m_stMaskImageInfo.nMinimumSizeX; i++ )
		{			
			if( (nXPos + i) >= this->m_nImageWidth)
				break;
			nTargetStep = ( nXPos + i ) + (this->m_nImageWidth * nYPos) + (m_nImageOffset * nYPos);

			chComparePixel = m_chSizeFilterBuff[nTargetStep];

			if(	chComparePixel == 0xFF )
			{
				nTotalWhitePixel++;
			}
			else
				break;
		}

		if( nTotalWhitePixel >= (m_stMaskImageInfo.nMinimumSizeX - 1) )
			return DEFECT;


		//	//사선 검사 좌상
		//if( (nXPos - 1) > 0 && (nYPos - 1) > 0 )
		//{
		//	nTargetStep = (nXPos - 1) + (this->m_nImageWidth * (nYPos - 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}
		////사선 검사 우상
		//if( (nXPos + 1) < this->m_nImageWidth && (nYPos - 1) > 0 )
		//{
		//	nTargetStep = (nXPos + 1) + (this->m_nImageWidth * (nYPos - 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}

		////사선 검사 좌하
		//if( (nXPos - 1) > 0 && (nYPos + 1) < this->m_nImageHeight )
		//{
		//	nTargetStep = (nXPos - 1) + (this->m_nImageWidth * (nYPos + 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}

		////사선 검사 우하
		//if( (nXPos + 1) < this->m_nImageWidth && (nYPos + 1) < this->m_nImageHeight )
		//{
		//	nTargetStep = (nXPos + 1) + (this->m_nImageWidth * (nYPos + 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}		

		////상 검사
		//if( (nYPos - 1) > 0 )
		//{
		//	nTargetStep = (nXPos) + (this->m_nImageWidth * (nYPos - 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}		

		////하 검사
		//if( (nYPos + 1) < this->m_nImageHeight )
		//{
		//	nTargetStep = (nXPos) + (this->m_nImageWidth * (nYPos + 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}		

		//if( nTotalWhitePixel >= (m_stMaskImageInfo.nMinimumSizeX - 1) )
		//	return DEFECT;


		//이곳 까지 왔다면 기준치 이하이므로
		//픽셀을 복원한다. 0xFF : 불량, 0x00 : 정상
		nTargetStep = nXPos + (this->m_nImageWidth * nYPos) + (m_nImageOffset * nYPos);
		m_SetByte(m_chResultImage, nTargetStep, 0x00);
		return NORMAL;
	}	
	else
		return NORMAL;
}

/**	
@brief	Y 축 방향 최소 검출 사이즈 측정
@param 	연산 좌표 X,Y
@return	해당 픽셀의 결함 여부(DEFECT : 결함, NORMAL : 정상)
@remark
-			해당 검출 좌표의 높이가 최소 검출 치를 넘지 않는다면 NORMAL 처리
@author	고정진
@date	2010/4/28  15:56
*/
bool	ActImageFilter::m_fnEraser_PixelY(	int nXPos, int nYPos	)
{
	unsigned char chPixelData		= '\0';
	unsigned char chComparePixel	= '\0';
	unsigned int	nTargetStep = 0;	

	nTargetStep = nXPos + (this->m_nImageWidth * nYPos) + (m_nImageOffset * nYPos);

	chPixelData = m_chSizeFilterBuff[nTargetStep];

	int nTotalWhitePixel = 0;

	if(	chPixelData == 0xFF )
	{
		//상 픽셀 검사
		for(int i = 1; i < m_stMaskImageInfo.nMinimumSizeY; i++)
		{		
			if((nYPos - i) <= 0)
				break;
		
			nTargetStep = nXPos+ (this->m_nImageWidth * ( nYPos - i )) + (m_nImageOffset * nYPos);

			chComparePixel = m_chSizeFilterBuff[nTargetStep];

			if(	chComparePixel == 0xFF )
			{
				nTotalWhitePixel++;		
			}
			else
				break;
		}

		if( nTotalWhitePixel >= ( m_stMaskImageInfo.nMinimumSizeY - 1 ) )
			return DEFECT;

		//하 픽셀 검사
		for(int i = 1; i < m_stMaskImageInfo.nMinimumSizeY; i++ )
		{		
			if((nYPos + i) >= this->m_nImageHeight)
				break;

			nTargetStep = nXPos+ (this->m_nImageWidth * ( nYPos + i)) + (m_nImageOffset * nYPos);

			chComparePixel = m_chSizeFilterBuff[nTargetStep];

			if(	chComparePixel == 0xFF )
			{
				nTotalWhitePixel++;
			}
			else
				break;							
		}

		if( nTotalWhitePixel >= (m_stMaskImageInfo.nMinimumSizeY - 1) )
			return DEFECT;


		////사선 검사 좌상
		//if( (nXPos - 1) > 0 && (nYPos - 1) > 0 )
		//{
		//	nTargetStep = (nXPos - 1) + (this->m_nImageWidth * (nYPos - 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}
		////사선 검사 우상
		//if( (nXPos + 1) < this->m_nImageWidth && (nYPos - 1) > 0 )
		//{
		//	nTargetStep = (nXPos + 1) + (this->m_nImageWidth * (nYPos - 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}

		////사선 검사 좌하
		//if( (nXPos - 1) > 0 && (nYPos + 1) < this->m_nImageHeight )
		//{
		//	nTargetStep = (nXPos - 1) + (this->m_nImageWidth * (nYPos + 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}

		////사선 검사 우하
		//if( (nXPos + 1) < this->m_nImageWidth && (nYPos + 1) < this->m_nImageHeight )
		//{
		//	nTargetStep = (nXPos + 1) + (this->m_nImageWidth * (nYPos + 1)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}		

		////좌 검사
		//if( (nXPos - 1) > 0  )
		//{
		//	nTargetStep = (nXPos - 1) + (this->m_nImageWidth * (nYPos)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}

		////우 검사
		//if( (nXPos + 1) < this->m_nImageWidth )
		//{
		//	nTargetStep = (nXPos + 1) + (this->m_nImageWidth * (nYPos)) + nOffset;
		//	chComparePixel = chSizeFilterBuff[nTargetStep];

		//	if(	chComparePixel == 0xFF )
		//	{
		//		nTotalWhitePixel++;
		//	}
		//}		

		//if( nTotalWhitePixel >= (m_stMaskImageInfo.nMinimumSizeY - 1) )
		//	return DEFECT;

				
		nTargetStep = nXPos + (this->m_nImageWidth * nYPos) + (m_nImageOffset * nYPos);
		m_SetByte(m_chResultImage, nTargetStep, 0x00);
		return NORMAL;
	}	
	else
		return NORMAL;

}

/**	
@brief	최초 검출 함수 이다.
@param 	연산을 수행할 좌표
@return	비교 결과.(PIXEL_EQUAL : 정상, PIXEL_DIFF : 이상)
@remark
-			X축 방향 3회,  Y축 방향 3회 최대 6회 수행한다.
@author	고정진
@date	2010/4/28  16:30
*/
bool ActImageFilter::m_fnCompare_AxisX(	int nXPos, int nYPos )
{	
	unsigned char chPixelData = '\0';
	unsigned char chComparePixel = '\0';	
	unsigned int		nTargetStep   = 0;
	int		nComparePosX  = 0;
	int		nComparePosY  = 0;	
	int		nCompareStep	= 1;
	int		nTempPosX		= 0;

	m_fnReadPosition(nXPos, nYPos, &chPixelData);		
	
	//좌측면 연산.	
	if( nXPos <= this->m_nImageWidth / 2 )
	{
		if( nXPos - m_stMaskImageInfo.nScaleX1 - m_stMaskImageInfo.nOutLineLeft > 0 )
		{
			nCompareStep = nCompareStep * (-1);
		}

		for( int nPoint = nCompareStep; nPoint < nCompareStep + TOTAL_INSPECT_COUNT; nPoint++ )
		{
			if( nPoint==0 )
				continue;

			nTempPosX = m_stMaskImageInfo.nScaleX1 * nPoint;			

			if( nTempPosX + nXPos + m_stMaskImageInfo.nOutLineRight >= this->m_nImageWidth )
				break;
			
			//for( int i = -1; i <= 1; i++ )
			{	
				//for( int j = -1; j <= 1; j++ )
				{
					//nComparePosX = nXPos + nTempPosX +i;
					//nComparePosY = nYPos + j;				
					nComparePosX = nXPos + nTempPosX;
					nComparePosY = nYPos;

					m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
					if( m_fnRGB_Compare( chPixelData, chComparePixel, 0 ))
					{
						return PIXEL_EQUAL;
					}
				}
			}
		}

		if( m_fnCompare_AxisY( nXPos, nYPos ))
		{
			return PIXEL_EQUAL;
		}		
	}
	//우측면 연산.
	else// if(nXPos>=IMAGE_X_SCALE/2)
	{
		nCompareStep = 1;

		if( nXPos + m_stMaskImageInfo.nScaleX1 + m_stMaskImageInfo.nOutLineRight >= this->m_nImageWidth )
		{
			nCompareStep = nCompareStep * (-1);
		}

		for( int nPoint = nCompareStep; nPoint > nCompareStep - TOTAL_INSPECT_COUNT; nPoint-- )
		{
			if( nPoint == 0 )
				continue;

			nTempPosX = m_stMaskImageInfo.nScaleX1 * nPoint;

			if( nTempPosX + nXPos - m_stMaskImageInfo.nOutLineLeft <= 0 )
			{
				break;	
			}
				
			//for( int i = -1; i <= 1; i++ )
			{	
				//for( int j = -1; j <= 1; j++ )
				{
					//nComparePosX = nXPos + nTempPosX + i;
					//nComparePosY = nYPos + j;

					nComparePosX = nXPos + nTempPosX;
					nComparePosY = nYPos;

					m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
					if( m_fnRGB_Compare( chPixelData, chComparePixel, 0 ) )
					{
						return PIXEL_EQUAL;
					}
				}
			}
		}

		if( m_fnCompare_AxisY(	nXPos, nYPos ))
		{
			return PIXEL_EQUAL;
		}		
	}

	//결과 좌표를 White Pixel 로 변경한다.	
	nTargetStep = nXPos + (this->m_nImageWidth * nYPos) + (m_nImageOffset * nYPos);
	m_SetByte(m_chResultImage, nTargetStep, 0xFF);
	m_SetByte(m_chSizeFilterBuff, nTargetStep, 0xFF);	
	return PIXEL_DIFF;
}
/**	
@brief	최초 검출 함수 이다.
@param 	연산을 수행할 좌표
@return	비교 결과.(PIXEL_EQUAL : 정상, PIXEL_DIFF : 이상)
@remark
-			X축 방향 3회,  Y축 방향 3회 최대 6회 수행한다.
@author	고정진
@date	2010/4/28  16:30
*/
bool ActImageFilter::m_fnCompare_AxisY(int nXPos, int nYPos)
{
	unsigned char chPixelData	= '\0';
	unsigned char chComparePixel = '\0';
	
	int		nComparePosX  = 0;
	int		nComparePosY  = 0;
	
	m_fnReadPosition(nXPos, nYPos, &chPixelData);	
	
	//상측면 연산.		
	//각 Scale Step	당 3Point 씩 1Scale 3Point 를 차분 연산한다.	
	int nTempPosY = 0;
	int nCompareStep=1;

	if( nYPos <= this->m_nImageHeight / 2 )
	{
		if( nYPos - m_stMaskImageInfo.nScaleY1 - m_stMaskImageInfo.nOutLineUp > 0 )
			nCompareStep = nCompareStep * (-1);

		//Y 축 비교는 Y 축 범위 안에서 3회 실시.
		for( int nPoint = nCompareStep; nPoint < (nCompareStep + TOTAL_INSPECT_COUNT); nPoint++ )
		{
			if( nPoint == 0 )
				continue;

			nTempPosY = m_stMaskImageInfo.nScaleY1 * nPoint;

			if( nTempPosY + nYPos + m_stMaskImageInfo.nOutLineDown >= this->m_nImageHeight )
				return false;
			
			//for( int i = -1; i <= 1; i++ )
			{	
				//for( int j = -1; j <= 1; j++ )
				{
					//nComparePosX = nXPos + i;
					//nComparePosY = nYPos + nTempPosY + j;

					nComparePosX = nXPos;
					nComparePosY = nYPos + nTempPosY;

					m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
					if( m_fnRGB_Compare( chPixelData, chComparePixel, 0 ) )
					{
						return PIXEL_EQUAL;
					}	
				}
			}
		}
	}
	//하측면 연산.
	else// if(nYPos>=IMAGE_Y_SCALE/2)
	{
		nCompareStep = 1;
		if( nYPos + m_stMaskImageInfo.nScaleY1 + m_stMaskImageInfo.nOutLineDown >= this->m_nImageHeight)
			nCompareStep = nCompareStep * (-1);
		//Y 축 비교는 Y 축 범위 안에서 3회 실시.
		for( int nPoint = nCompareStep; nPoint > (nCompareStep - TOTAL_INSPECT_COUNT); nPoint--)
		{
			if( nPoint == 0)
				continue;

			nTempPosY = m_stMaskImageInfo.nScaleY1 * nPoint;

			if( nTempPosY + nYPos - m_stMaskImageInfo.nOutLineUp <= 0 )
				return false;
		
			//for( int i = -1; i <= 1; i++ )
			{	
				//for( int j = -1; j <= 1; j++ )
				{
					//nComparePosX = nXPos + i;
					//nComparePosY = nYPos + nTempPosY + j;

					nComparePosX = nXPos;
					nComparePosY = nYPos + nTempPosY;

					if( nComparePosY >= this->m_nImageHeight )
						nComparePosY = 0;

					m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
					if( m_fnRGB_Compare( chPixelData, chComparePixel, 0 ) )
					{
						return PIXEL_EQUAL;	
					}
				}
			}
		}
	}
	return PIXEL_DIFF;
}

/**	
@brief	비교 연산 함수
@param 	비교값 1,2, 오차 허용치
@return	PIXEL_DIFF : 허용치 밖, PIXEL_EQUAL : 허용치 안
@remark
-		
@author	고정진
@date	2010/4/28  16:44
*/
bool	ActImageFilter::m_fnRGB_Compare(unsigned char chPixel_1,unsigned char chPixel_2,int n_DefectRange)
{
	
	int nErrorRange = m_nHThreshold[int(chPixel_1)];

	if( abs( (int)chPixel_1 - (int)chPixel_2 ) > nErrorRange )
	{
		return PIXEL_DIFF;				
	}
	else
	{				
		return PIXEL_EQUAL;
	}
}

/**	
@brief	해당 좌표의 소스 데이터를 읽어 리턴한다.
@param 	해당 좌표, 넘겨줄 포인터
@return	true : 정상 처리, false : 입력 좌표 이상
@remark
-		
@author	고정진
@date	2010/4/28  17:56
*/
bool	ActImageFilter::m_fnReadPosition( int nXPos, int nYPos, unsigned char* chReadData)
{
	int		nStepPosition = 0;	

	if(nXPos < 0 || nYPos < 0 )
	{
		return FALSE;
	}
	if(nXPos >= this->m_nImageWidth || nYPos >= this->m_nImageHeight )
	{
		return FALSE;
	}
	
	nStepPosition=((nYPos) * this->m_nImageWidth) + nXPos + (m_nImageOffset * nYPos);
	
	memcpy(chReadData, &m_chSourceImage[nStepPosition], 1);
	
	return true;
}
//////////////////////////////////////////////////////////////////////////
//이상 여기까지 _ACC 처리 과정과 동일한 처리를 하는 C 코드 이다.
//테스트를 위해 작성한 것이다.
//////////////////////////////////////////////////////////////////////////


