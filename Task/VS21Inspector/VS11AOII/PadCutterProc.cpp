#include "StdAfx.h"
#include "PadCutterProc.h"
//extern PROC_PAD_ALIGN			G_ProcPadAlign;
UINT fn_OrgImgCutterProcess(LPVOID pParam)
{
	PROC_PADCUT	*			pProcParam		= (PROC_PADCUT *) pParam;

	AOI_ImageCtrlQ *			pIDTCtrlQ		= pProcParam->m_pIDTCtrlQ;
	PAD_ImageCtrlQ *		pPADCtrlQ		= pProcParam->m_pPADCtrlQ;
	PROC_STATE	*			pUpdateState	= pIDTCtrlQ->m_pUpdateState;
	CoordinatesProc *		pCoordProcObj = pProcParam->m_pCoordProcObj;
	InspectDataQ*			pTargetNode	= nullptr;

	int *pPadCutType			= &pCoordProcObj->m_HWParam.PadInspectAreaType;

	CString DebugString;
	int CountPutData = 0;
	//////////////////////////////////////////////////////////////////////////
	SCANIMGINFO	*			pScanAreaInfo		= nullptr;
	//int								PadImageCount = 0;
	//////////////////////////////////////////////////////////////////////////
	DWORD tickTickN = 0;
	//////////////////////////////////////////////////////////////////////////
//#if defined(TEST_SAVE_CUT_AREA_DRAW) || defined(TEST_SAVE_ORG_IMG)
	CvPoint StartPT, EndPT;
	//char TestSaveImage[256];
	//IplImage *pSavePadTestImg = cvCreateImage( cvSize(pCoordProcObj->m_HWParam.m_ProcImageW, pCoordProcObj->m_HWParam.m_ProcImageH), IPL_DEPTH_8U, 3);
	//cvZero(pSavePadTestImg);
//#endif
	while(pProcParam->m_bProcessRun == true)
	{
		pTargetNode = pIDTCtrlQ->m_fnQGetInspectPTR_PadCutter();
		//if(pUpdateState->m_InspectionStop  == INSPECTION_STOP)
		//{
		//	if(pTargetNode != nullptr)
		//		pTargetNode->m_fnQSetProcessEnd(QDATA_PROC_END_SORT);
		//}
		//else 
		if(pTargetNode != nullptr)
		{
			
			tickTickN = ::GetTickCount() ;
			if(pTargetNode->m_ScanLine%2== 0)
				cvFlip(pTargetNode->m_pGrabOrgImage,0);//짝수 방향일경우 Image가 Y축이 바뀐 상태임으로 전환.


			//if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
			//	cvConvertImage(pTargetNode->m_pGrabOrgImage, pSavePadTestImg);


#ifdef		TEST_SAVE_ORG_IMG
			{
				char TestSaveImage[256];
				cvCopyImage(pTargetNode->m_pGrabOrgImage, pTargetNode->m_pGrabImage);
				SCANIMGINFO *pCoodArea = nullptr;
				pCoodArea = pCoordProcObj->m_fnGetCalculateAreaInImg(pTargetNode->m_ScanLine, pTargetNode->m_ScanImageIndex);
				CvPoint TextPos= cvPoint(20,30);
				CString ImgText;
				ImgText.Format("M X:%8d, Y:%8d W:%8d, H:%8d",	pCoodArea->m_ScanImgPos.left, 
					pCoodArea->m_ScanImgPos.top,
					pCoodArea->m_ScanImgPos.Width(),
					pCoodArea->m_ScanImgPos.Height());
				PutTextInImageDefect(pTargetNode->m_pGrabImage, &TextPos, ImgText.GetBuffer(ImgText.GetLength()));
				sprintf_s(TestSaveImage, "%sImg_%d_%d.jpg", TEST_SAVE_ORG_PATH, pTargetNode->m_ScanLine,  pTargetNode->m_ScanImageIndex);
				cvSaveImage(TestSaveImage, pTargetNode->m_pGrabImage);
			}
#endif
			//////////////////////////////////////////////////////////////////////////
			//1) Image Cutting Area Get.
			//cvZero(pTargetNode->m_pGrabImage);
			//memset(pTargetNode->m_pGrabImage->imageData, 0xFF, pTargetNode->m_pGrabImage->imageSize);
			pScanAreaInfo = pCoordProcObj->m_fnGetCalculateAreaInImg(pTargetNode->m_ScanLine, pTargetNode->m_ScanImageIndex);
			if(pScanAreaInfo != nullptr)
			{
				//Cutting 정보 void*로 저장.
				pTargetNode->m_pCoordAraeInImg = (void*)pScanAreaInfo;
				////////////////////////////////////////////////////////////////////////////
				////1) Active Cutting(원판 수정.)
				//for(int CutStep =0; CutStep<pCoodArea->m_findSubRecCnt[0]; CutStep++)
				//{
				//	G_CuttingActiveArea(pTargetNode, &pCoodArea->m_InImgPositionRect[0][CutStep]);
				//	if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
				//	{
				//		StartPT.x	= pCoodArea->m_InImgPositionRect[0][CutStep].left;
				//		StartPT.y	= pCoodArea->m_InImgPositionRect[0][CutStep].top;
				//		EndPT.x		= pCoodArea->m_InImgPositionRect[0][CutStep].right;
				//		EndPT.y	    = pCoodArea->m_InImgPositionRect[0][CutStep].bottom;
				//		//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(255,0,0),5);
				//		cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
				//	}
				//}
				//////////////////////////////////////////////////////////////////////////
				//2) Pad Cutting(Pad부 Queue에 삽입)
				//PadImageCount  = 0;
				if(*pPadCutType	 == INSPACT_PAD_UPDOWN || *pPadCutType == INSPACT_PAD_BOTH)
				{
					for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[1]; CutStep++)
					{//상

						//if((pScanAreaInfo->m_IspectAreaSubtractRect[1][CutStep].Width()/pCoordProcObj->m_GlassParam.m_ScanImgResolution_W)   < (G_ProcPadAlign.m_AlignWidth + G_ProcPadAlign.m_AlignOverWidth*2) ||
						//	(pScanAreaInfo->m_IspectAreaSubtractRect[1][CutStep].Height())   <= pCoordProcObj->m_GlassParam.ActiveDeadZoneSize*3)
						//	continue;

						pPADCtrlQ[0].m_fnFindPAD_And_ImageCopy(pCoordProcObj->m_GlassParam.m_ScanImgResolution_W,
																					pCoordProcObj->m_GlassParam.m_ScanImgResolution_H,
																					pTargetNode->m_pGrabOrgImage, 
																					&pScanAreaInfo->m_InImgPositionRect[1][CutStep], 
																					&pScanAreaInfo->m_IspectAreaSubtractRect[1][CutStep],
																					&pScanAreaInfo->m_ORGTargetRect[1][CutStep]);
						//G_CuttingPADArea(pCoordProcObj, &pPADCtrlQ[0], PadImageCount,  pTargetNode,  &pCoodArea->m_InImgPositionRect[1][CutStep], &pCoodArea->m_IspectAreaSubtractRect[1][CutStep]);
						if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
						{
							StartPT.x	= pScanAreaInfo->m_InImgPositionRect[1][CutStep].left;
							StartPT.y	= pScanAreaInfo->m_InImgPositionRect[1][CutStep].top;
							EndPT.x		= pScanAreaInfo->m_InImgPositionRect[1][CutStep].right;
							EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[1][CutStep].bottom;
							//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(0,255,0),5);
							cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
						}
					}

					for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[2]; CutStep++)
					{//하

						//if((pScanAreaInfo->m_IspectAreaSubtractRect[1][CutStep].Width()/pCoordProcObj->m_GlassParam.m_ScanImgResolution_W)   < (G_ProcPadAlign.m_AlignWidth + G_ProcPadAlign.m_AlignOverWidth*2) ||
						//	(pScanAreaInfo->m_IspectAreaSubtractRect[1][CutStep].Height())  <= pCoordProcObj->m_GlassParam.ActiveDeadZoneSize*3)
						//	continue;

						pPADCtrlQ[1].m_fnFindPAD_And_ImageCopy(pCoordProcObj->m_GlassParam.m_ScanImgResolution_W,
																					pCoordProcObj->m_GlassParam.m_ScanImgResolution_H,
																					pTargetNode->m_pGrabOrgImage, 
																					&pScanAreaInfo->m_InImgPositionRect[2][CutStep], 
																					&pScanAreaInfo->m_IspectAreaSubtractRect[2][CutStep],
																					&pScanAreaInfo->m_ORGTargetRect[2][CutStep]);

						//G_CuttingPADArea(pCoordProcObj, &pPADCtrlQ[1], PadImageCount, pTargetNode, &pCoodArea->m_InImgPositionRect[2][CutStep], &pCoodArea->m_IspectAreaSubtractRect[2][CutStep]);
						if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
						{
							StartPT.x	= pScanAreaInfo->m_InImgPositionRect[2][CutStep].left;
							StartPT.y	= pScanAreaInfo->m_InImgPositionRect[2][CutStep].top;
							EndPT.x		= pScanAreaInfo->m_InImgPositionRect[2][CutStep].right;
							EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[2][CutStep].bottom;
							//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(0,0,255),5);
							cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
						}
					}
				}
				else
				{//이부분은 Active 와 같은 연산을 수행 한다.
					for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[1]; CutStep++)
					{//상
						G_CuttingActiveArea(pTargetNode, &pScanAreaInfo->m_InImgPositionRect[1][CutStep], &pPADCtrlQ[0]);

						if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
						{
							StartPT.x	= pScanAreaInfo->m_InImgPositionRect[1][CutStep].left;
							StartPT.y	= pScanAreaInfo->m_InImgPositionRect[1][CutStep].top;
							EndPT.x		= pScanAreaInfo->m_InImgPositionRect[1][CutStep].right;
							EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[1][CutStep].bottom;
							//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(0,255,0),5);
							cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
						}
					}

					for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[2]; CutStep++)
					{//하
						G_CuttingActiveArea(pTargetNode, &pScanAreaInfo->m_InImgPositionRect[2][CutStep], &pPADCtrlQ[1]);

						if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
						{
							StartPT.x	= pScanAreaInfo->m_InImgPositionRect[2][CutStep].left;
							StartPT.y	= pScanAreaInfo->m_InImgPositionRect[2][CutStep].top;
							EndPT.x		= pScanAreaInfo->m_InImgPositionRect[2][CutStep].right;
							EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[2][CutStep].bottom;
							//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(0,0,255),5);
							cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
						}
					}
				}
				if(*pPadCutType	 == INSPACT_PAD_LEFTRIGHT || *pPadCutType == INSPACT_PAD_BOTH)
				{
					//PadImageCount = pPADCtrlQ[2].QGetNodeCnt();
					for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[3]; CutStep++)
					{//좌
						pPADCtrlQ[2].m_fnFindPAD_And_ImageCopy(pCoordProcObj->m_GlassParam.m_ScanImgResolution_W,
																					pCoordProcObj->m_GlassParam.m_ScanImgResolution_H,
																					pTargetNode->m_pGrabOrgImage, 
																					&pScanAreaInfo->m_InImgPositionRect[3][CutStep], 
																					&pScanAreaInfo->m_IspectAreaSubtractRect[3][CutStep],
																					&pScanAreaInfo->m_ORGTargetRect[3][CutStep]);
						//G_CuttingPADArea(pCoordProcObj, &pPADCtrlQ[2], PadImageCount, pTargetNode, &pCoodArea->m_InImgPositionRect[3][CutStep], &pCoodArea->m_IspectAreaSubtractRect[3][CutStep]);
						if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
						{
							StartPT.x	= pScanAreaInfo->m_InImgPositionRect[3][CutStep].left;
							StartPT.y	= pScanAreaInfo->m_InImgPositionRect[3][CutStep].top;
							EndPT.x		= pScanAreaInfo->m_InImgPositionRect[3][CutStep].right;
							EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[3][CutStep].bottom;
							//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(255,255,0),5);
							cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
						}
					}
					//PadImageCount = pPADCtrlQ[3].QGetNodeCnt();
					for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[4]; CutStep++)
					{//우
						pPADCtrlQ[3].m_fnFindPAD_And_ImageCopy(pCoordProcObj->m_GlassParam.m_ScanImgResolution_W,
																					pCoordProcObj->m_GlassParam.m_ScanImgResolution_H,
																					pTargetNode->m_pGrabOrgImage, 
																					&pScanAreaInfo->m_InImgPositionRect[4][CutStep], 
																					&pScanAreaInfo->m_IspectAreaSubtractRect[4][CutStep],
																					&pScanAreaInfo->m_ORGTargetRect[4][CutStep]);
						//G_CuttingPADArea(pCoordProcObj, &pPADCtrlQ[3], PadImageCount, pTargetNode, &pCoodArea->m_InImgPositionRect[4][CutStep], &pCoodArea->m_IspectAreaSubtractRect[4][CutStep]);
						if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
						{
							StartPT.x	= pScanAreaInfo->m_InImgPositionRect[4][CutStep].left;
							StartPT.y	= pScanAreaInfo->m_InImgPositionRect[4][CutStep].top;
							EndPT.x		= pScanAreaInfo->m_InImgPositionRect[4][CutStep].right;
							EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[4][CutStep].bottom;
							//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(255,0,255),5);
							cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
						}
					}
				}
				else
				{//이부분은 Active 와 같은 연산을 수행 한다.

					for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[3]; CutStep++)
					{//좌
						G_CuttingActiveArea(pTargetNode, &pScanAreaInfo->m_InImgPositionRect[3][CutStep], &pPADCtrlQ[2]);
						if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
						{
							StartPT.x	= pScanAreaInfo->m_InImgPositionRect[3][CutStep].left;
							StartPT.y	= pScanAreaInfo->m_InImgPositionRect[3][CutStep].top;
							EndPT.x		= pScanAreaInfo->m_InImgPositionRect[3][CutStep].right;
							EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[3][CutStep].bottom;
							//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(255,255,0),5);
							cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
						}
					}
					for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[4]; CutStep++)
					{//우
						G_CuttingActiveArea(pTargetNode, &pScanAreaInfo->m_InImgPositionRect[4][CutStep], &pPADCtrlQ[3]);
						if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
						{
							StartPT.x	= pScanAreaInfo->m_InImgPositionRect[4][CutStep].left;
							StartPT.y	= pScanAreaInfo->m_InImgPositionRect[4][CutStep].top;
							EndPT.x		= pScanAreaInfo->m_InImgPositionRect[4][CutStep].right;
							EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[4][CutStep].bottom;
							//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(255,0,255),5);
							cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
						}
					}
				}

				//////////////////////////////////////////////////////////////////////////
				//1) Active Cutting(원판 수정.)
				for(int CutStep =0; CutStep<pScanAreaInfo->m_findSubRecCnt[0]; CutStep++)
				{
					G_CuttingActiveArea(pTargetNode, &pScanAreaInfo->m_InImgPositionRect[0][CutStep]);
					if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
					{
						StartPT.x	= pScanAreaInfo->m_InImgPositionRect[0][CutStep].left;
						StartPT.y	= pScanAreaInfo->m_InImgPositionRect[0][CutStep].top;
						EndPT.x		= pScanAreaInfo->m_InImgPositionRect[0][CutStep].right;
						EndPT.y	    = pScanAreaInfo->m_InImgPositionRect[0][CutStep].bottom;
						//cvDrawRect(pSavePadTestImg,StartPT, EndPT,CV_RGB(255,0,0),5);
						cvDrawRect(pTargetNode->m_pGrabOrgImage,StartPT, EndPT,CV_RGB(255,255,255),5);
					}
				}
			}
			else
			{
				//Coordinate Error
			}
			//Result Process가 해당 Node에 대한 작업을 완료 하였다.
			pTargetNode->m_fnQSetProcessEnd(QDATA_PROC_END_CUTTER);
#ifndef DEBUG_WITHOUT_INSPECT_DLL
			pIDTCtrlQ->fn_QPutInspector(pTargetNode);
#else
			DEFECT_DATA TestDefect[10];
			for(int i=0;i<10; i++)
			{
				TestDefect[i].DefectType =10;
				TestDefect[i].StartX =150*(i+1);
				TestDefect[i].StartY =150*(i+1);

				TestDefect[i].EndX =TestDefect[i].StartX+100;
				TestDefect[i].EndY =TestDefect[i].StartY+100;
			}
			pTargetNode->m_fnQSetDefectAllBuffer(10,TestDefect);
#endif
			CountPutData++;
			if(pUpdateState != nullptr)
			{//첫장은 오류, 1번에서 2번까지의 시간을 잰다.
				pUpdateState->m_CutScanLineNum[pProcParam->m_ProcIndex]			= pTargetNode->m_ScanLine;			
				pUpdateState->m_CutScanLineIndex[pProcParam->m_ProcIndex]		= pTargetNode->m_ScanImageIndex;		
				pUpdateState->m_nCutterProcTick[pProcParam->m_ProcIndex]		= ::GetTickCount() -tickTickN;
			}
			// DEFECT_DATA NewData[10];
			// for(int i=0; i<10; i++)
			// {
			//	 NewData[i].DefectType = 11;
			//	 NewData[i].StartX = 100 + i*100;
			//	 NewData[i].StartY = 100 + i*100;
			//	 NewData[i].EndX = NewData[i].StartX + 100;
			//	 NewData[i].EndY = NewData[i].StartY + 100;
			// }

			//pTargetNode->m_fnQSetDefectAllBuffer(10, &NewData[0]);
			//if(pUpdateState->bTEST_SAVE_CUT_AREA_DRAW == true)
			//{
			//	CvPoint TextPos= cvPoint(20,30);
			//	CString ImgText;
			//	ImgText.Format("M X:%8d, Y:%8d W:%8d, H:%8d",	pCoodArea->m_ScanImgPos.left, 
			//		pCoodArea->m_ScanImgPos.top,
			//		pCoodArea->m_ScanImgPos.Width(),
			//		pCoodArea->m_ScanImgPos.Height());
			//	PutTextInImageDefect(pSavePadTestImg, &TextPos, ImgText.GetBuffer(ImgText.GetLength()));

			//	sprintf_s(TestSaveImage, "%sImg_%d_%d.jpg", TEST_SAVE_CUT_PATH, pTargetNode->m_ScanLine,  pTargetNode->m_ScanImageIndex);
			//	cvSaveImage(TestSaveImage, pSavePadTestImg);
			//	cvZero(pSavePadTestImg);
			//}
		}
		else
			Sleep(1);
	}
	//#if defined(TEST_SAVE_CUT_AREA_DRAW) || defined(TEST_SAVE_ORG_IMG)
	//cvReleaseImage(&pSavePadTestImg);
//#endif
	pProcParam->m_pResultThread = nullptr;
	return 0;
}
extern CoordinatesProc			G_CoordProcObj;
bool G_CuttingActiveArea(InspectDataQ* pTargetNode, CRect* pActiveArea, PAD_ImageCtrlQ * pPadObj)
{
	CvRect Calc_CutImgArea;
	
	Calc_CutImgArea.x			= pActiveArea->left;
	Calc_CutImgArea.y			= pActiveArea->top;
	Calc_CutImgArea.width		= pActiveArea->Width();
	Calc_CutImgArea.height	    = pActiveArea->Height();
	if(Calc_CutImgArea.width <= 0 || Calc_CutImgArea.height <= 0)
		return false;
	//////////////////////////////////////////////////////////////////////////
	//Y축 반복 없을시 날려버려라.(Active)
	if((G_CoordProcObj.m_InspectActiveParam.nScaleY1*3 > Calc_CutImgArea.height))
		return true;

	if(pPadObj != nullptr && pPadObj->m_PadDeadZone != 0)
	{
		if(G_CoordProcObj.m_HWParam.PadInspectAreaType == INSPACT_PAD_UPDOWN)
		{
			//Pad부 Y축 반복 없을시 날려버려라.(좌측 Active In Pad)
			if((G_CoordProcObj.m_InspectPADMaskImageInfo[2].nScaleY1*3 > Calc_CutImgArea.height) 
				&& ((G_CoordProcObj.m_GlassParam.PadStartDistanceX-G_CoordProcObj.m_GlassParam.PadStartDeadZoneDisX)>0))
				return true;

			//Pad부 Y축 반복 없을시 날려버려라.(우측 Active In Pad)
			if((G_CoordProcObj.m_InspectPADMaskImageInfo[3].nScaleY1*3 > Calc_CutImgArea.height) 
				&& (G_CoordProcObj.m_GlassParam.PadEndDistanceX-G_CoordProcObj.m_GlassParam.PadEndDeadZoneDisX)>0)
				return true;
		}

		//CRect PadDeadZone;
		int SizePadDeadZone =  pPadObj->m_PadDeadZonePixel;

		//Pad부의 Dead Zone을 칠한다.
		switch (pPadObj->m_PadIndex)
		{
		case 0://Up
			{		
				if(SizePadDeadZone>pActiveArea->Height())
					SizePadDeadZone = pActiveArea->Height();

				Calc_CutImgArea.x			= pActiveArea->left;
				Calc_CutImgArea.y			= pActiveArea->top;
				Calc_CutImgArea.width		= pActiveArea->Width();
				Calc_CutImgArea.height	    = pActiveArea->Height() - SizePadDeadZone;
			}break;
		case 1://DN
			{
				if(SizePadDeadZone>pActiveArea->Height())
					SizePadDeadZone = pActiveArea->Height();

				Calc_CutImgArea.x			= pActiveArea->left;
				Calc_CutImgArea.y			= pActiveArea->top + SizePadDeadZone;
				Calc_CutImgArea.width		= pActiveArea->Width();
				Calc_CutImgArea.height	    = pActiveArea->Height() - SizePadDeadZone;
			}break;
		case 2://LF
			{
				if(SizePadDeadZone>pActiveArea->Width())
					SizePadDeadZone = pActiveArea->Width();

				Calc_CutImgArea.x			= pActiveArea->left;
				Calc_CutImgArea.y			= pActiveArea->top;
				Calc_CutImgArea.width		= pActiveArea->Width()- SizePadDeadZone;
				Calc_CutImgArea.height	    = pActiveArea->Height();
			}break;

		case 3://RT
			{
				if(SizePadDeadZone>pActiveArea->Width())
					SizePadDeadZone = pActiveArea->Width();

				Calc_CutImgArea.x			= pActiveArea->left+ SizePadDeadZone;
				Calc_CutImgArea.y			= pActiveArea->top;
				Calc_CutImgArea.width		= pActiveArea->Width()- SizePadDeadZone;
				Calc_CutImgArea.height	    = pActiveArea->Height();
			}break;
		}
		if(Calc_CutImgArea.width <= 0 || Calc_CutImgArea.height <= 0)
			return false;
	}

#ifdef USE_C_ROI_CUT

	char *pSorcePos = pTargetNode->m_pGrabOrgImage->imageData;
	char *pTargetPos = pTargetNode->m_pGrabImage->imageData;

	int JumpOffset = ((Calc_CutImgArea.y * pTargetNode->m_pGrabOrgImage->width ) + Calc_CutImgArea.x);
	int CpyMemSize = Calc_CutImgArea.width;
	pSorcePos = pSorcePos + JumpOffset;
	pTargetPos = pTargetPos + JumpOffset;

	JumpOffset = pTargetNode->m_pGrabOrgImage->width;
	for(int CpH=0; CpH<Calc_CutImgArea.height; CpH++)
	{
		memcpy(pTargetPos, pSorcePos, CpyMemSize);
		pTargetPos+=JumpOffset;
		pSorcePos+=JumpOffset;
	}
#else
	cvSetImageROI(pTargetNode->m_pGrabOrgImage, Calc_CutImgArea);
	cvSetImageROI(pTargetNode->m_pGrabImage, Calc_CutImgArea);

	cvCopyImage(pTargetNode->m_pGrabOrgImage, pTargetNode->m_pGrabImage);

	cvResetImageROI(pTargetNode->m_pGrabOrgImage);
	cvResetImageROI(pTargetNode->m_pGrabImage);
	
#endif
	
	return true;
}
//bool G_CuttingPADArea(CoordinatesProc * pCoordProcObj, 
//								PAD_ImageCtrlQ *pPadObj, int PadImgCnt,
//								InspectDataQ* pTargetNode, 
//								CRect* pPadInImgArea, 
//								CRect* pPadArea)
//{
//	
//	//원판에서 자를위치 
//	CvRect Org_CutImgArea, Target_CutImgArea;
//	Org_CutImgArea.x			= pPadInImgArea->left;
//	Org_CutImgArea.y			= pPadInImgArea->top;
//	Org_CutImgArea.width		= pPadInImgArea->Width();
//	Org_CutImgArea.height	    = pPadInImgArea->Height();
//
//	if(Org_CutImgArea.width <= 0 && Org_CutImgArea.height <= 0)
//		return false;
//
//	//Pad부 Image를 Copy할때 높이가 완성 되면 Next Item으로 Pad Queue를 진행 시킨다.
//	InspectPadDataQ* pWorkPadNode = pPadObj->m_fnGetWorkNode();
//	CRect CheckBuf, WorkRect;
//	
//
//	for(int FindStep = 0; FindStep<PadImgCnt; FindStep++)
//	{
//		memcpy(&WorkRect, &pWorkPadNode->m_PadInScanPos, sizeof(CRect));
//		//WorkRect.left		= pWorkPadNode->m_PadInScanPos.left;
//		//WorkRect.top		= pWorkPadNode->m_PadInScanPos.top;
//		//WorkRect.right		= pWorkPadNode->m_PadInScanPos.right;
//		//WorkRect.bottom	= pWorkPadNode->m_PadInScanPos.bottom;
//
//		if(CheckBuf.IntersectRect(WorkRect, pPadArea) == TRUE)
//		{
//			//////////////////////////////////////////////////////////////////////////
//			//좌표계 변환.
//			CheckBuf.left		= CheckBuf.left - pWorkPadNode->m_PadInScanPos.left;
//			CheckBuf.top		= CheckBuf.top - pWorkPadNode->m_PadInScanPos.top;
//			CheckBuf.right		= CheckBuf.right - pWorkPadNode->m_PadInScanPos.left;
//			CheckBuf.bottom	= CheckBuf.bottom - pWorkPadNode->m_PadInScanPos.top;
//
//			CheckBuf.left = (int)((float)CheckBuf.left/pCoordProcObj->m_GlassParam.m_ScanImgResolution_W);
//			CheckBuf.top = (int)((float)CheckBuf.top/pCoordProcObj->m_GlassParam.m_ScanImgResolution_H);
//
//			CheckBuf.right = (int)((float)CheckBuf.right/pCoordProcObj->m_GlassParam.m_ScanImgResolution_W);
//			CheckBuf.bottom = (int)((float)CheckBuf.bottom/pCoordProcObj->m_GlassParam.m_ScanImgResolution_H);
//
//			Target_CutImgArea.x			= CheckBuf.left;
//			Target_CutImgArea.y			= CheckBuf.top;
//			Target_CutImgArea.width		= CheckBuf.Width();
//			Target_CutImgArea.height	    = CheckBuf.Height();
//			//if(Target_CutImgArea.width <= 0 && Target_CutImgArea.height <= 0)
//			//	return false;
//
//			if(Org_CutImgArea.width != Target_CutImgArea.width || Org_CutImgArea.height != Target_CutImgArea.height)
//				return false;
//
//			pWorkPadNode->QDataCpy(pTargetNode->m_pGrabOrgImage, &Org_CutImgArea, &Target_CutImgArea);
//			pWorkPadNode->m_fnQSetProcessEnd(QDATA_PROC_END_PADINPUT);
//			//////////////////////////////////////////////////////////////////////////
//			return true;
//		}
//		else
//		{
//			pPadObj->m_fnMoveNextWorkNode();
//			pWorkPadNode = pPadObj->m_fnGetWorkNode();
//		}
//
//	}
//	return false;
//}
