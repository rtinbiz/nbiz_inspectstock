#include "StdAfx.h"
#include "LocalImgGrab.h"

extern AOIBigImg G_GrabBigger;
bool G_FileGrabThreadRun =  false;
int	   G_FileLoadCount		= 0;
char G_FileLoadPath[MAX_PATH];
CWinThread	*			G_pLoadFileThread = nullptr;


extern ImageSaveQ				G_AlignImgQ;
//////////////////////////////////////////////////////////////////////////
CvFont		G_ImageInfoFont;
void PutTextInImageCreateDefect(IplImage *pMainMap, CvPoint *pNamePos,char*text)
{
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_ImageInfoFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= pNamePos->x-5;
	StartTxt.y	= pNamePos->y+baseline;
	EndTxt.x	= pNamePos->x+text_size.width+5;
	EndTxt.y	= pNamePos->y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, CV_RGB(255, 255, 255));
	extern CvFont		G_ImageInfoFont;
	cvPutText(pMainMap, text, *pNamePos, &G_ImageInfoFont, CV_RGB(0,0,0));

	cvRectangle(pMainMap, StartTxt, EndTxt, CV_RGB(255, 0, 0));
}

bool fn_GetTrargetImagePath(void)
{

	CFileDialog dlg(TRUE);

	dlg.m_ofn.Flags |= OFN_ALLOWMULTISELECT;
	dlg.m_ofn.lpstrFilter = ("BMP(*.bmp)\0*.bmp\0All(*.*)\0*.*\0");
	dlg.m_ofn.lpstrFile = G_FileLoadPath;
	dlg.m_ofn.nMaxFile = MAX_PATH;

	if ( dlg.DoModal() == IDCANCEL )
	{
		return false;
	}
	return true;
}


LocalImgGrab::LocalImgGrab(void)
{
	m_pIDTParam		= nullptr;
	m_pUpdateState	= nullptr;
	m_pGrabCutter	= nullptr;
	cvInitFont (&G_ImageInfoFont, CV_FONT_HERSHEY_TRIPLEX , 0.6f, 0.7f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
}


LocalImgGrab::~LocalImgGrab(void)
{
	if(G_pLoadFileThread != nullptr)
	{
		G_FileGrabThreadRun = false;
	}
	if(m_pGrabCutter != nullptr)
		delete m_pGrabCutter;
	m_pGrabCutter = nullptr;
}
bool LocalImgGrab::initImgGrab(AOI_ImageCtrlQ * pIDTInterface)
{
	m_pIDTParam = pIDTInterface;
	if(m_pGrabCutter != nullptr)
	{
		//AfxMessageBox("del");
		m_pGrabCutter->QClean();
		delete m_pGrabCutter;
		m_pGrabCutter = nullptr;
	}
	m_pGrabCutter = new AOIGrabCutterQ(pIDTInterface, GRAB_CUTTER_BUF);
	return true;
}
bool LocalImgGrab::exitImgGrab(void)
{
	return true;
}
bool LocalImgGrab::GrabStart(int ScanLineNum, int LineScnaWidth, int LoadImgCount, int ImageWidth)
{

	//////////////////////////////////////////////////////////////////////////
	m_pGrabCutter->QStartGrab(LoadImgCount, LineScnaWidth);

	if(ImageWidth == 8192)
		//sprintf_s(G_FileLoadPath, "D:\\nBiz_InspectStock\\Data\\8k_4000.bmp");
		sprintf_s(G_FileLoadPath, "D:\\nBiz_InspectStock\\Data\\TestImage_8k.bmp");
	else if(ImageWidth == 12000)
		sprintf_s(G_FileLoadPath, "D:\\nBiz_InspectStock\\Data\\TestImage_12K.bmp");
	else if(ImageWidth == 16000)
		sprintf_s(G_FileLoadPath, "D:\\nBiz_InspectStock\\Data\\TestImage_16K.bmp");
	
	//if(fn_GetTrargetImagePath() == true)
	{
		m_pIDTParam->m_GrabScanLineNum		= ScanLineNum;
		m_pIDTParam->m_GrabScanImageCnt		= 0;
		G_FileGrabThreadRun = true;
		G_FileLoadCount = LoadImgCount;
		
		if(m_pIDTParam->m_pUpdateState != nullptr)
		{
			m_pIDTParam->m_pUpdateState->m_GrabImageCnt		= 0;
		}
		G_GrabBigger.m_NowScanLine = ScanLineNum;
		G_GrabBigger.m_NowImgIndex = 0;
		//if(G_pLoadFileThread == nullptr)
			G_pLoadFileThread = ::AfxBeginThread(fn_GrabFromFile, m_pGrabCutter, THREAD_PRIORITY_HIGHEST);
		//else
		//	AfxMessageBox("Thread Run Now");

		

	}
	//else
	//{
	//	G_FileGrabThreadRun = false;
	//	G_FileLoadCount = 0;
	//}
	
	return true;
}

bool LocalImgGrab::GrabEnd(void)
{
	G_FileGrabThreadRun = false;
	Sleep(100);
	return true;
}

void Wait(DWORD dwMillisecond)
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();
	while(GetTickCount() - dwStart < dwMillisecond)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}
#include <math.h>
//////////////////////////////////////////////////////////////////////////
UINT fn_GrabFromFile(LPVOID pParam)
{

	AOIGrabCutterQ	*pGrabCutter		= (AOIGrabCutterQ *) pParam;
	AOI_ImageCtrlQ*	pIDTCtrlPtr			= pGrabCutter->m_pIDTCtrlPtr;
	PROC_STATE	*	pUpdateState		= pIDTCtrlPtr->m_pUpdateState;
	InspectDataQ	*	pGrabIMG		= nullptr; 


	{
		
		if(pUpdateState->m_bAlignScan == true)
		{
			for(int ScanCnt =0; ScanCnt<G_FileLoadCount; ScanCnt++)
			{
				IplImage *pNewGrabImg = cvCreateImage(cvSize(pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageW,pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageH), IPL_DEPTH_8U, 1);
				CRect ScanImgPos;
				{
					//CString strDInfo;
					//CvPoint DefPos;
					////cvZero(pNewGrabImg);
					//memset(pNewGrabImg->imageData, 0xFF, pNewGrabImg->imageSize);
					//int CntDef = 0;

					//if(pIDTCtrlPtr->m_GrabScanLineNum%2 == 0)//짝수
					//	CntDef = rand()%50;
					//else
					//	CntDef = rand()%10;

					//for(int CStep = 0; CStep< CntDef; CStep++)
					//{
					//	strDInfo.Format("%02d",  CStep+1);

					//	DefPos.x =  rand()%pNewGrabImg->width ;
					//	DefPos.y =  rand()%pNewGrabImg->height;

					//	if(DefPos.x>(pNewGrabImg->width-300))
					//		DefPos.x = (pNewGrabImg->width-300);

					//	if(DefPos.y>(pNewGrabImg->height-300))
					//		DefPos.y = (pNewGrabImg->height-300);
					//	PutTextInImageCreateDefect( pNewGrabImg, &DefPos, strDInfo.GetBuffer(strDInfo.GetLength()));
					//}
				}
				

				G_AlignImgQ.QPutNode(1,1,1, pNewGrabImg,&ScanImgPos);
				cvReleaseImage(&pNewGrabImg);
			}
		}
		else
		{
			//double tickFreq = (double) cvGetTickFrequency();
			DWORD tickTickN = ::GetTickCount();

			CString ImgText;
			CvPoint TextPos = cvPoint(20,20);
			//////////////////////////////////////////////////////////////////////////
			int LoadStep = 0;
			if(pUpdateState != nullptr)
				pUpdateState->m_nOneLineStartTick = ::GetTickCount();

			srand( (unsigned)time( NULL ) );
			//IplImage *pNewGrabImg = cvCreateImage(cvSize(pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageW,pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageH), IPL_DEPTH_8U, 1);


			//IplImage *pLoadOrgImg = cvLoadImage("D:\\nBiz_InspectStock\\Data\\TestImage_12k.jpg", CV_LOAD_IMAGE_GRAYSCALE);
			//IplImage *pNewGrabImg = cvLoadImage(G_FileLoadPath, CV_LOAD_IMAGE_GRAYSCALE);
			//IplImage *pNewGrabImg = cvCreateImage(cvSize(pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageW,pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageH), IPL_DEPTH_8U, 1);
			IplImage *pNewGrabImg = cvLoadImage("D:\\nBiz_InspectStock\\Data\\TestImage_12k.bmp", CV_LOAD_IMAGE_GRAYSCALE);
			//cvResize(pLoadOrgImg, pNewGrabImg);
			while(G_FileGrabThreadRun == true && LoadStep < G_FileLoadCount)
			{

				//Loading이 너무 빠르다  ㅡ.,ㅡ버퍼를 보고 Loading하자.
				if(pIDTCtrlPtr->m_IdleNodeCount>5)
				{
					//cvZero(pNewGrabImg);
					//memset(pNewGrabImg->imageData, 0x2F,  pNewGrabImg->imageSize);
					//CString strDInfo;
					//CvPoint DefPos;
					//int CntDef = 0;

					//if(pIDTCtrlPtr->m_GrabScanLineNum%2 == 0)//짝수
					//	CntDef = rand()%30;
					//else
					//	CntDef = rand()%30;

					//for(int CStep = 0; CStep< CntDef; CStep++)
					//{
					//	strDInfo.Format("%02d_%02d_%02d",pIDTCtrlPtr->m_GrabScanLineNum, pGrabCutter->m_GrabCountImg+1,  CStep+1);

					//	DefPos.x =  rand()%pNewGrabImg->width ;
					//	DefPos.y =  rand()%pNewGrabImg->height;

					//	if(DefPos.x>(pNewGrabImg->width-300))
					//		DefPos.x = (pNewGrabImg->width-300);

					//	if(DefPos.y>(pNewGrabImg->height-300))
					//		DefPos.y = (pNewGrabImg->height-300);
					//	PutTextInImageCreateDefect( pNewGrabImg, &DefPos, strDInfo.GetBuffer(strDInfo.GetLength()));
					//}
					memcpy(pGrabCutter->QGetImgPtr(), pNewGrabImg->imageData, pNewGrabImg->imageSize);
					pGrabCutter->QProcImgData();

					G_GrabBigger.SetBigImage(pNewGrabImg->imageData);//,  LoadStep+1);

					LoadStep++;
				}
				//cvWaitKey(500);
				Sleep(20);
				//Sleep(70);

				//pGrabIMG	= pIDTCtrlPtr->m_fnQGetInspectPTR_Grabber();
				//if(pGrabIMG != nullptr)
				//{
				//	//cvCopyImage(pLoadImage, pGrabIMG->m_pGrabOrgImage);


				//	//Create New Number Defect
				//	pIDTCtrlPtr->m_GrabScanImageCnt +=1;
				//	if(pIDTCtrlPtr->m_OnlyGrabImageSave == false)
				//	{
				//		pGrabIMG->m_nOneTick				= ::GetTickCount();
				//		pGrabIMG->m_ScanLine				= pIDTCtrlPtr->m_GrabScanLineNum;
				//		pGrabIMG->m_ScanImageIndex	= pIDTCtrlPtr->m_GrabScanImageCnt;
				//		//ImgText.Format("Scan Line Num : %d, Image Index %d", pGrabIMG->m_ScanLine, pGrabIMG->m_ScanImageIndex);
				//		//PutTextInImage(pGrabIMG->m_pGrabImage, &TextPos, &G_ImageInfoFont,  ImgText.GetBuffer(ImgText.GetLength()));
				//		{
				//			CString strDInfo;
				//			CvPoint DefPos;
				//			cvZero(pGrabIMG->m_pGrabOrgImage);
				//			int CntDef = 0;

				//			if(pIDTCtrlPtr->m_GrabScanLineNum%2 == 0)//짝수
				//				CntDef = rand()%80;
				//			else
				//				CntDef = rand()%80;

				//			for(int CStep = 0; CStep< CntDef; CStep++)
				//			{
				//				strDInfo.Format("%02d_%02d_%02d",pGrabIMG->m_ScanLine, pGrabIMG->m_ScanImageIndex,  CStep+1);

				//				DefPos.x =  rand()%pGrabIMG->m_pGrabOrgImage->width ;
				//				DefPos.y =  rand()%pGrabIMG->m_pGrabOrgImage->height;

				//				if(DefPos.x>(pGrabIMG->m_pGrabOrgImage->width-300))
				//					DefPos.x = (pGrabIMG->m_pGrabOrgImage->width-300);

				//				if(DefPos.y>(pGrabIMG->m_pGrabOrgImage->height-300))
				//					DefPos.y = (pGrabIMG->m_pGrabOrgImage->height-300);
				//				PutTextInImageCreateDefect( pGrabIMG->m_pGrabOrgImage, &DefPos, strDInfo.GetBuffer(strDInfo.GetLength()));
				//			}
				//		}
				//		if(pIDTCtrlPtr->m_GrabScanLineNum%2 == 0)
				//		{
				//			cvFlip(pGrabIMG->m_pGrabOrgImage,0);//짝수 방향일경우 Image가 Y축이 바뀐 상태임으로 전환.
				//		}
				//		pGrabIMG->m_fnQSetProcessEnd(QDATA_PROC_END_GRAB);	//현 Image는 Grabber에서 Grab을 종료 했다.
				//		//pIDTCtrlPtr->fn_QPutInspector(pGrabIMG);						//<<<Inspect Process에 등록.
				//		//pIDTCtrlPtr->m_pCuttingProcQ->QPutNode(pGrabIMG);		//<<<Pad Cutter Process에 등록.
				//	}


				//	LoadStep++;
				//	if(pUpdateState != nullptr)
				//	{
				//		pUpdateState->m_GrabImageCnt++;
				//		pUpdateState->m_bMilGrabFunMemError = false;
				//	}
				//}
				//else
				//{
				//	//심각한오류 Grab된 Data를 쓸곳이 없다.ㅡ,.ㅡ;;;
				//	if(pUpdateState != nullptr)
				//		pUpdateState->m_bMilGrabFunMemError = true;
				//}

				//if(pUpdateState != nullptr)
				//{//첫장은 오류, 1번에서 2번까지의 시간을 잰다.
				//	pUpdateState->m_nFileGrabFunProTick = ::GetTickCount()-tickTickN;
				//	tickTickN =  ::GetTickCount();
				//}
				//Sleep(0);
				
			}
			pGrabCutter->SaveLineScanImg();
			//cvReleaseImage(&pLoadOrgImg);
			cvReleaseImage(&pNewGrabImg);
		}
	}
	G_FileGrabThreadRun = false;
	//cvReleaseImage(&pLoadImage);
	G_pLoadFileThread = nullptr;
	return 0;
}
int		G_TestLoadImgCnt = 100;
//UINT fn_GrabFromFilePerLoading(LPVOID pParam)
//{
//	AOI_ImageCtrlQ*	pIDTCtrlPtr			= (AOI_ImageCtrlQ *) pParam;
//	PROC_STATE	*	pUpdateState		= pIDTCtrlPtr->m_pUpdateState;
//	InspectDataQ	*	pGrabIMG			= nullptr; 
//
//	
//	CString RotateImg;
//	RotateImg.Format("%s", G_FileLoadPath);
//	RotateImg.Replace(".bmp", "R.bmp");
//	IplImage *pLoadImage  = cvLoadImage(G_FileLoadPath, CV_LOAD_IMAGE_GRAYSCALE);
//	IplImage *pLoadImage2  = cvLoadImage(RotateImg.GetBuffer(RotateImg.GetLength()), CV_LOAD_IMAGE_GRAYSCALE);
//
//
//	ProcMsgQ InspectorInputQ;
//
//	if(pLoadImage->width != pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageW  || pLoadImage->height != pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageH)
//	{
//		AfxMessageBox("Load Image Size Error");
//	}
//	else
//	{
//		DWORD tickTickN =::GetTickCount(); 
//
//		CString ImgText;
//		CvPoint TextPos = cvPoint(20,20);
//		//////////////////////////////////////////////////////////////////////////
//		int LoadStep = 0;
//		
//
//		while(G_FileGrabThreadRun == true && LoadStep < G_TestLoadImgCnt)
//		{
//			pGrabIMG	= pIDTCtrlPtr->m_fnQGetInspectPTR_Grabber();
//			if(pGrabIMG != nullptr)
//			{
//				if(pLoadImage2 != nullptr && LoadStep%2 == 0)
//				{
//					cvCopyImage(pLoadImage2, pGrabIMG->m_pGrabOrgImage);
//					cvFlip(pGrabIMG->m_pGrabOrgImage,0);//짝수 방향일경우 Image가 Y축이 바뀐 상태임으로 전환.
//					//cvZero(pGrabIMG->m_pGrabImage);
//				}
//				else
//				{
//					cvCopyImage(pLoadImage, pGrabIMG->m_pGrabOrgImage);
//					//cvZero(pGrabIMG->m_pGrabImage);
//				}
//
//				//ImgText.Format("Scan Line Num : %d, Image Index %d", pGrabIMG->m_ScanLine, pGrabIMG->m_ScanImageIndex);
//				//PutTextInImage(pGrabIMG->m_pGrabImage, &TextPos, ImgText.GetBuffer(ImgText.GetLength()));
//
//				pIDTCtrlPtr->m_GrabScanImageCnt +=1;
//				if(pIDTCtrlPtr->m_OnlyGrabImageSave == false)
//				{
//					pGrabIMG->m_nOneTick				= ::GetTickCount();
//					pGrabIMG->m_ScanLine				= pIDTCtrlPtr->m_GrabScanLineNum;
//					pGrabIMG->m_ScanImageIndex	= pIDTCtrlPtr->m_GrabScanImageCnt;
//					pGrabIMG->m_fnQSetProcessEnd(QDATA_PROC_END_GRAB);	//현 Image는 Grabber에서 Grab을 종료 했다.
//				}
//
//
//				LoadStep++;
//				if(pUpdateState != nullptr)
//				{
//					pUpdateState->m_GrabImageCnt++;
//					pUpdateState->m_bMilGrabFunMemError = false;
//				}
//			}
//			else
//			{
//				//심각한오류 Grab된 Data를 쓸곳이 없다.ㅡ,.ㅡ;;;
//				if(pUpdateState != nullptr)
//					pUpdateState->m_bMilGrabFunMemError = true;
//			}
//
//			if(pUpdateState != nullptr)
//			{//첫장은 오류, 1번에서 2번까지의 시간을 잰다.
//				pUpdateState->m_nFileGrabFunProTick =  ::GetTickCount()-tickTickN;
//				tickTickN = ::GetTickCount();
//			}
//
//		}
//		AfxMessageBox("Inspector Start OK");
//		//순수 Anaim동작을 Check 해보기위해 IDT Count에 맞는 수치 만큼 Test한다.
//		if(pUpdateState != nullptr)
//			pUpdateState->m_nOneLineStartTick = ::GetTickCount();
//		for(int InspectStep= 0; InspectStep<G_TestLoadImgCnt; InspectStep++)
//		{
//			pIDTCtrlPtr->fn_QPutInspector(InspectorInputQ.QGetNode());						//<<<Inspect Process에 등록.
//		}
//	}
//	G_FileGrabThreadRun = false;
//	cvReleaseImage(&pLoadImage);
//	G_pLoadFileThread = nullptr;
//	return 0;
//}