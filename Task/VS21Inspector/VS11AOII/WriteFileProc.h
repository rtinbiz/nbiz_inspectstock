#pragma once
#include "..\..\..\CommonHeader\Class\InterServerInterface.h"
#include "..\..\..\CommonHeader\Inspection\CalculateCoordinateDefine.h"
#include "ProcMsgQ.h"
//#include "ProcState.h"
#include "DefectFileFormat.h"
/** 
/** 
@brief		Write Result Process에사 사용되는 구조체

@remark
-			Result Process  이후 Data 처리를 위한 구조체 이다.
-			
@author		최지형
@date		2010.00.00
*/ 
typedef struct WRITE_FILE_PROCESS_TRG
{
	int							m_ProcessNum;//1번은 Defect Write, 2번은 Image Wirte.
	bool						m_bProcessRun;
	int							m_ReviewImgW;
	int							m_ReviewImgH;

	CWinThread	*				m_pResultThread;
	CDefectFileFormat*			m_pWriteFileObj;
	DefectDataQ	*				m_pResultDefectQ;
	CoordinatesProc *			m_pCoordObj;

	CInterServerInterface *		m_pServerInterface;
	PROC_STATE	*				m_pUpdateState;
	ImageSaveQ	*				m_pHistogramImgQ;
	ImageSaveQ	*				m_pInspectSaveImgQ;

	WRITE_FILE_PROCESS_TRG()
	{
		m_ProcessNum			= 0;

		m_bProcessRun			= false;
		m_pResultThread			= nullptr;

		m_ReviewImgW			= 0 ;
		m_ReviewImgH			= 0;
		m_pResultThread			= nullptr;
		m_pWriteFileObj			= nullptr;
		m_pResultDefectQ		= nullptr;
		m_pServerInterface		= nullptr;
		m_pCoordObj				= nullptr;

		m_pUpdateState			= nullptr;
		m_pHistogramImgQ		= nullptr;
		m_pInspectSaveImgQ		= nullptr;
	}

} PROC_RETWRITE, *LPPROC_RETWRITE;

//Result Process Def.
UINT fn_WriteFileProcess(LPVOID pParam);

inline void PutTextInImageDefect(IplImage *pMainMap, CvPoint *pNamePos,char*text);