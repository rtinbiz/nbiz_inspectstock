#pragma once
#include "..\..\..\CommonHeader\Inspection\InspectDataQ.h"
#include "..\..\..\CommonHeader\Inspection\InspectPadDataQ.h"
#include "..\..\..\CommonHeader\Inspection\AnaimAOI_Interface.h"
#include "..\..\..\CommonHeader\Inspection\CalculateCoordinateDefine.h"
#include "..\..\..\CommonHeader\Inspection\CoordinatesProc.h"
//#include "ProcState.h"
#include "ProcMsgQ.h"
//////////////////////////////////////////////////////////////////////////


typedef void (*QInputInspector)(InspectDataQ *pInspectQBuffer);//Input DLL
class AOI_ImageCtrlQ
{
public:
	AOI_ImageCtrlQ(void);
	~AOI_ImageCtrlQ(void);
private: 
	CRITICAL_SECTION m_CrtSection;
public:
	//HW_PARAM		*	m_pHWParam;
	int						m_NodeCount;
	int						m_IdleNodeCount;
private:
	
	InspectDataQ *m_pGrabberNode;			//Grabber Processor 에서 점유한 Node.
	InspectDataQ *m_pPadCutProNode;			//PAD Cutter Process 점유 Node
	InspectDataQ *m_pResultProNode;			//Result Process 점유 Node.
	InspectDataQ *m_pWriteRetProNode;			//Result Process 점유 Node.

	InspectDataQ *m_pNodeBuff;					//Queue를 순환 시킬때 기준점으로 상용.
	
public:
	//Cycle Queue를 주어진 인수만큼 생성한다. (중복 호출시 큰수에 맞게 생성된다.)
	void m_fnQCreateCycleQ(int CountNodes);
	//Cycle Queue의 모든 Data를 지운다.
	void m_fnQDeleteAll();

	//Graber에서 Image Buffer를 호출 한다.
	int					m_GrabScanLineNum;			//>>>현재 Scan중인 Line Number.
	int					m_GrabScanImageCnt;			//>>>현재 Grab중인 Image Index.

	bool				m_OnlyGrabImageSave;
	CString			m_OrgImgSavePath;
	//Line Scan이 시작될때 설정.
	void m_fnQGrabScanLineStart(int ScanLineNum);

	InspectDataQ * m_fnQGetInspectPTR_Grabber();
	InspectDataQ * m_fnQGetInspectPTR_PadCutter();
	InspectDataQ * m_fnQGetInspectPTR_ResultProc();
	InspectDataQ * m_fnQGetInspectPTR_SortRetProc();

	//////////////////////////////////////////////////////////////////////////
	//1) Inspector에 Queue전달(Data)
	QInputInspector	fn_QPutInspector;

	CoordinatesProc*	m_pCoordObj;
	//////////////////////////////////////////////////////////////////////////
	PROC_STATE	*	m_pUpdateState;
	//////////////////////////////////////////////////////////////////////////
	//Queue를 순환.
	InspectDataQ * m_fnQGetNextNode();
	void m_fnQAll_DataReset();

	CString				m_strDefectFile;

};

