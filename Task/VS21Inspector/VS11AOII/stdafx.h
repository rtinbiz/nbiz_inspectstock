
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.

//Driver깔지 않고 적용하면 중복 에러 발생.
#pragma warning(disable : 4005)

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원

#include "DebugDefine.h"
#include "AOI_ALARM_Code.h"

//#if 0		//OpenCV 2.1 =>2.4변경
//#pragma comment (lib, "../LIB/CVLib64/cv210_64.lib")
//#pragma comment (lib, "../LIB/CVLib64/cxcore210_64.lib")
//#pragma comment (lib, "../LIB/CVLib64/highgui210_64.lib")
//
//#include "../Include/opencv/cv.h"
//#include "../Include/opencv/cxcore.h"
//#include "../Include/opencv/highgui.h"
//#else
//	#pragma comment (lib, "../CV242/CV242_LIB/Opencv_core242.lib")
//	#pragma comment (lib, "../CV242/CV242_LIB/opencv_imgproc242.lib")
//	#pragma comment (lib, "../CV242/CV242_LIB/Opencv_highgui242.lib")
//
//	#include "../CV242/opencv2/opencv.hpp"
//#endif

//Image Process Lib Loading
#include "..\..\..\CommonHeader\ImgProcLink.h"
//VS64 Interface Lib Loading
#include "..\..\..\CommonHeader\VS_ServerLink.h"

//64bit Dll Load
#ifdef NDEBUG
#pragma comment (lib, "../../../CommonDLL64/nBizAnaimAOI.lib")
#else
#pragma comment (lib, "../../../CommonDLL64/nBizAnaimAOI.lib")
//#pragma comment (lib, "../../../CommonDLL64/nBizAnaimAOI_D.lib")
#endif



#include "ColorStaticST.h"
#include "LinkGrid.h"


#define  UI_UPDATE_PROC_STATE		10101
#define  UI_UPDATE_Interval					100

#define  UI_FDC_DATA						10101
#define  UI_FDC_DATA_Interval			500

#define  UI_UPDATE_STATE_AOI_TASK	10111
#define  UI_AOI_TASK_Interval				2500
//////////////////////////////////////////////////////////////////////////
#define  PARAM_DRAW_WINDOW			"Show_Map"

#define DALAS_CAM_CCF_PATH "D:\\nBiz_InspectStock\\DCFs\\"

#define BUFFER_DISK_NAME	"R:\\"


#pragma comment(lib,"pdh.lib")

//Graber DLL Link(LIB)
#ifdef MATROX_LINE_SCAN_GRABBER
	#pragma comment (lib, "mil.lib")
#endif

#ifdef DALSA_LINE_SCAN_GRABBER
	#pragma comment (lib, "C:\\Program Files\\Teledyne DALSA\\Sapera\\Lib\\Win64\\SapClassBasic.lib")
	#pragma comment (lib, "Mpr.lib")
#endif

//Result Process Count
//static const UINT RET_PAD_ALINE_SPACE_HEIGHT	= 20;
//static const UINT RET_PAD_ALINE_WIDTH		    = 100;
//static const UINT RET_PAD_ALINE_LIMITWIDTH	    = 4;
#define	TEST_PAD_ALIGN_IMG_SAVE_PATH "d:\\NBIZ_AOI_Monitor\\[x]PadAlignImg\\"
#define  LINE_SCAN_IMG_PATH  "d:\\NBIZ_AOI_Monitor\\AOI_Line_Img\\"

#define	TEST_SAVE_CUT_PATH	 "R:\\NBIZ_AOI_Monitor\\CutterTestImg\\"

#ifdef TEST_SAVE_ORG_IMG
#define	TEST_SAVE_ORG_PATH	 "D:\\NBIZ_AOI_Monitor\\ORGTestImg\\"
#endif
#ifdef TEST_SAVE_PAD_AREA_SAVE
#define  PAD_ORG_IMG_SAVE_PATH  "D:\\NBIZ_AOI_Monitor\\PadORGTestImg\\"
#endif
//Grab Buffer (용)
static const UINT   GRAB_CUTTER_BUF		= 5;

//Cutter Process Count
static const UINT PADCUT_PROC_CNT		= 4;
//Result Process Count
static const UINT RET_PROC_CNT			= 4;

//File Write Process Count(홀수 Index는 Defect Writer, 짝수는 Org Save Thread)
static const UINT WRE_PROC_CNT			= 3;//최소 2개 이상이어야 한다.


//ListView Indexing Max Value
#define  LISTDATACOUNT				400

//Save Org Image Max Buffer Size
#define  SAVE_ORG_IMG_MAX			50

#define  WM_USER_STATE_UPDATE				 (WM_USER + 1030)
//////////////////////////////////////////////////////////////////////////
//Test View별 Data

#define REVIEW_TASK_NUM			75
#define ALIGN_TASK_NUM			76




#define	SAVE_ORG_IMG_FOLDER		"[01]Org_Img"
#define	SAVE_SMALL_IMG_FOLDER		"[02]Small_Img"
#define	SAVE_HISTO_IMG_FOLDER		"[03]Histogram_Img"
#define	SAVE_PADORG_IMG_FOLDER	"[04]PadOrg_Img"
#include "ProcState.h"
extern PROC_PAD_ALIGN			G_ProcPadAlign;
void PutTextInImageDefect(IplImage *pMainMap, CvPoint *pNamePos,char*text);

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


