#include "StdAfx.h"
#include "AOI_ImageCtrlQ.h"


/** 
@brief		AOI_ImageCtrlQ 생성자

@remark
-			 AOI_ImageCtrlQ.
-			
@author		최지형
@date		2012.00.00
*/ 
AOI_ImageCtrlQ::AOI_ImageCtrlQ(void)
{

	InitializeCriticalSection(&m_CrtSection);
	m_NodeCount					= 0;
	m_IdleNodeCount				= 0;
	m_pGrabberNode				= nullptr;	//Grabber Processor 에서 점유한 Node.
	m_pPadCutProNode			= nullptr;
	m_pResultProNode			= nullptr;	//Result Process가 점유한 Node.
	m_pWriteRetProNode			= nullptr;	//Write Result Process가 점유한 Node.

	//Inspect Queue에 InspectDataQ Node를 넣어주는 Function Link.
	fn_QPutInspector				= _AOI_Act_PushItem;

	//Cutter에게 Pad부 Cutting Grab Image 정보를 넘긴다.
//	m_pCuttingProcQ				= nullptr;
	
	m_pNodeBuff					= nullptr;
	//Grab Start 할때 Line Block Number가 주어진다.
	m_GrabScanLineNum			= -1;
	m_GrabScanImageCnt		= 0;
	//Grab된 Image를 저장만 할것인지.
	m_OnlyGrabImageSave		= false;
	m_OrgImgSavePath.Format("");

	m_pUpdateState				= nullptr;

}

/** 
@brief		AOI_ImageCtrlQ 소멸자

@remark
-			 ~AOI_ImageCtrlQ.
-			
@author		최지형
@date		2012.00.00
*/ 
AOI_ImageCtrlQ::~AOI_ImageCtrlQ(void)
{

	EnterCriticalSection(&m_CrtSection);
	InspectDataQ * pBuffer = nullptr;
	InspectDataQ * pDeleteNode = nullptr;
	if(m_pGrabberNode != nullptr)
	{
		pBuffer = m_pGrabberNode->m_pNextNode;

		while(pBuffer != m_pGrabberNode)
		{
			pDeleteNode =  pBuffer;
			pBuffer = pBuffer->m_pNextNode;
			delete pDeleteNode;
			pDeleteNode = nullptr;
			m_NodeCount--;
		}
		delete m_pGrabberNode;
		m_NodeCount--;

		m_pGrabberNode		= nullptr;	//Grabber Processor 에서 점유한 Node.
		m_pNodeBuff			= nullptr;

		m_NodeCount			= 0;
	}
	LeaveCriticalSection(&m_CrtSection);
	DeleteCriticalSection(&m_CrtSection);

}
/** 
@brief		Cycle Queue Create

@remark
-			 Cycle Queue를 지정 Size만큼 생성한다.
-
@author		최지형
@date		2012.00.00
*/ 
void AOI_ImageCtrlQ::m_fnQCreateCycleQ(int CountNodes)
{
	EnterCriticalSection(&m_CrtSection);
	//////////////////////////////////////////////////////////////////////////
	//외부에서 상태 Check를 위해 Link를 건다.
	if(m_pUpdateState != nullptr)
	{
		m_pUpdateState->m_pNodeCount				= &m_NodeCount;
		m_pUpdateState->m_pGrabScanLineNum		= &m_GrabScanLineNum;			
		m_pUpdateState->m_pGrabScanImageCnt	= &m_GrabScanImageCnt;	
		m_pUpdateState->m_pIdleNodeCnt				= &m_IdleNodeCount;		
		m_pUpdateState->m_pOnlyGrabImageSave	= &m_OnlyGrabImageSave;
	}
	//////////////////////////////////////////////////////////////////////////
	if(m_pGrabberNode == nullptr)
	{//최초 생성시 들어온다.
		m_pGrabberNode = new InspectDataQ(m_pCoordObj->m_HWParam.m_ProcImageW, 
															m_pCoordObj->m_HWParam.m_ProcImageH+(m_pCoordObj->m_AXIS_Y_OVER_STEP*2), 
															m_pCoordObj->m_HWParam.m_DefectMaxCntInImg, 
															m_pCoordObj->m_HWParam.m_DefectReviewSizeW, 
															m_pCoordObj->m_HWParam.m_DefectReviewSizeH);
		m_pGrabberNode->m_pNextNode		= m_pGrabberNode;
		m_pGrabberNode->m_pFrontNode	= m_pGrabberNode;
		m_pNodeBuff = m_pGrabberNode;
		
		m_NodeCount++;
	}
	if(CountNodes>m_NodeCount)
	{//새로운 Size가 현재것보다 크면 추가 한다.(m_pGrabberNode Node 앞에 삽입된다.)
		//CountNodes = CountNodes - m_NodeCount;
		InspectDataQ * pNewNode = nullptr;
		InspectDataQ * pBuffer = nullptr;
		for(int CreatStep = m_NodeCount; CreatStep<CountNodes; CreatStep++)
		{
			pNewNode =  new InspectDataQ(	m_pCoordObj->m_HWParam.m_ProcImageW, 
															m_pCoordObj->m_HWParam.m_ProcImageH+(m_pCoordObj->m_AXIS_Y_OVER_STEP*2), 
															m_pCoordObj->m_HWParam.m_DefectMaxCntInImg, 
															m_pCoordObj->m_HWParam.m_DefectReviewSizeW, 
															m_pCoordObj->m_HWParam.m_DefectReviewSizeH);
			pBuffer = m_pGrabberNode->m_pNextNode;
			pNewNode->m_pFrontNode			= m_pGrabberNode;
			pNewNode->m_pNextNode				= pBuffer;
			m_pGrabberNode->m_pNextNode		= pNewNode;
			pBuffer->m_pFrontNode					= pNewNode;

			m_NodeCount++;
		}
	}
	m_IdleNodeCount = m_NodeCount;
	//Result Node는 Grabber Node부터 다시 시작 시킨다.
	//이후 Grabber Process가 도망가면 Result Process에의해 최대한 따라간다.
	m_pWriteRetProNode = m_pResultProNode = m_pPadCutProNode = m_pGrabberNode;

	LeaveCriticalSection(&m_CrtSection);
}
/** 
@brief		Cycle Queue Data Delete

@remark
-			 Cycle Queue Link를 모두 제거 하며 Data도 지운다.
-
@author		최지형
@date		2012.00.00
*/ 
void AOI_ImageCtrlQ::m_fnQDeleteAll()
{
	EnterCriticalSection(&m_CrtSection);
	InspectDataQ * pBuffer = nullptr;
	InspectDataQ * pDeleteNode = nullptr;
	CString*			  pDeleteStr = nullptr;
	if(m_pGrabberNode != nullptr)
	{
		pBuffer = m_pGrabberNode->m_pNextNode;
		
		while(pBuffer != m_pGrabberNode)
		{
			pDeleteNode =  pBuffer;
			pBuffer = pBuffer->m_pNextNode;
			//if(pDeleteNode->m_StrDefect != nullptr)
			//{
			//	pDeleteStr = (CString*)pDeleteNode->m_StrDefect;
			//	delete pDeleteStr;
			//}
			//pDeleteNode->m_StrDefect = nullptr;
			delete pDeleteNode;
			pDeleteNode = nullptr;
			m_NodeCount--;
		}
		delete m_pGrabberNode;
		m_NodeCount--;
		
		m_pGrabberNode		= nullptr;	//Grabber Processor 에서 점유한 Node.
		m_pNodeBuff			= nullptr;

		m_NodeCount			= 0;
	}
	LeaveCriticalSection(&m_CrtSection);
}
/** 
@brief		Start Scan Line

@remark
-			 Line Scan이 시작될때 초기화 값들을 설정 한다.
-
@author		최지형
@date		2012.00.00
*/ 
void AOI_ImageCtrlQ::m_fnQGrabScanLineStart(int ScanLineNum)
{	
	m_GrabScanLineNum		= ScanLineNum;	
	m_GrabScanImageCnt	= 0;
}
InspectDataQ * AOI_ImageCtrlQ::m_fnQGetInspectPTR_Grabber()
{
	EnterCriticalSection(&m_CrtSection);
	InspectDataQ * retPtr = m_pGrabberNode;
	if(retPtr->m_NodeProcState == QDATA_PROC_END_NONE/* && m_pGrabberNode->m_pNextNode != m_pWriteRetProNode*/)//Sort Process까지 끝나고 IDLE상태의 Node만을 취한다.
	{
		m_pGrabberNode = m_pGrabberNode->m_pNextNode;
		m_IdleNodeCount--;
	}
	else
		retPtr = nullptr;
	m_pNodeBuff = m_pGrabberNode;
	LeaveCriticalSection(&m_CrtSection);
	return retPtr;
}
InspectDataQ * AOI_ImageCtrlQ::m_fnQGetInspectPTR_PadCutter()
{
	//두개이상의 Process에서 접근 함으로.
	EnterCriticalSection(&m_CrtSection);
	InspectDataQ * retPtr = nullptr;
	if(m_pPadCutProNode->m_NodeProcState == QDATA_PROC_END_GRAB)// > 0 /*&& m_pPadCutProNode != m_pGrabberNode*/)//Inspect 가 완료된 Node만을 취한다.
	{
		retPtr = m_pPadCutProNode;
		m_pPadCutProNode = m_pPadCutProNode->m_pNextNode;
	}
	LeaveCriticalSection(&m_CrtSection);
	return retPtr;
}
InspectDataQ * AOI_ImageCtrlQ::m_fnQGetInspectPTR_ResultProc()
{
	//두개이상의 Process에서 접근 함으로.
	EnterCriticalSection(&m_CrtSection);
	InspectDataQ * retPtr = nullptr;
	if(m_pResultProNode->m_NodeProcState == QDATA_PROC_END_INSPECT)// > 0 /*&& m_pResultProNode != m_pGrabberNode*/)//Inspect 가 완료된 Node만을 취한다.
	{
		retPtr = m_pResultProNode;
		m_pResultProNode = m_pResultProNode->m_pNextNode;
	}
	LeaveCriticalSection(&m_CrtSection);
	return retPtr;
}
InspectDataQ * AOI_ImageCtrlQ::m_fnQGetInspectPTR_SortRetProc()
{
	//두개이상의 Process에서 접근 함으로.
	EnterCriticalSection(&m_CrtSection);
	InspectDataQ * retPtr = nullptr;
	if(m_pWriteRetProNode->m_NodeProcState & QDATA_PROC_END_RESULT)// > 0)//Inspect 가 완료된 Node만을 취한다.
	{
		retPtr = m_pWriteRetProNode;
		m_pWriteRetProNode = m_pWriteRetProNode->m_pNextNode;
		m_IdleNodeCount++;
	}
	
	LeaveCriticalSection(&m_CrtSection);
	return retPtr;
}

InspectDataQ * AOI_ImageCtrlQ::m_fnQGetNextNode()
{
	InspectDataQ * retPtr = m_pNodeBuff;
		m_pNodeBuff = m_pNodeBuff->m_pNextNode;
	return retPtr;
}
void AOI_ImageCtrlQ::m_fnQAll_DataReset()
{
	//두개이상의 Process에서 접근 함으로.
	EnterCriticalSection(&m_CrtSection);
	InspectDataQ * retPtr = m_pNodeBuff->m_pNextNode;
	while (m_pNodeBuff != retPtr)
	{
		retPtr->m_NodeProcState = QDATA_PROC_END_NONE;

		retPtr = retPtr->m_pNextNode;
	}
	

	m_pNodeBuff = m_pWriteRetProNode = m_pResultProNode = m_pPadCutProNode = m_pGrabberNode;

	m_IdleNodeCount = m_NodeCount;
	LeaveCriticalSection(&m_CrtSection);
}