#pragma once

#include "AOIGrabCutter.h"

#ifdef MATROX_LINE_SCAN_GRABBER
#define BUF_NUM_COUNT 5
#endif
#ifdef DALSA_LINE_SCAN_GRABBER
#define BUF_NUM_COUNT 5
#endif
#ifdef MATROX_LINE_SCAN_GRABBER
// 추가 포함 Dir C/C++ : $(mil_path)\..\include
// 추가 포함 Lib 링크 : $(mil_path)\..\lib
#include "Mil_FnGrab.h"


class CGraberCtrl
{
public:
	CGraberCtrl(void);
	~CGraberCtrl(void);
	bool initGrabber(AOI_ImageCtrlQ * pIDTInterface);
	bool exitGrabber(void);
	bool GrabStart(int ScanLineNum, int LineScnaWidth);
	bool GrabEnd(void);

//////////////////////////////////////////////////////////////////////////
private:
	MBUFHOOKFCTPTR	m_pUserFunction;
	AOI_ImageCtrlQ	*	m_pIDTParam;
	BYTE *					m_pImageBuffer;

	AOIGrabCutterQ	*m_pGrabCutter;
private:
	MIL_ID App;
	MIL_ID Sys;
	MIL_ID Dig;
	MIL_ID Img[BUF_NUM_COUNT];
	MIL_ID MilBufTrans;

public:
	MIL_INT imgW;
	MIL_INT imgH;
public:
	PROC_STATE	*	m_pUpdateState;
};
#endif

//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
#ifdef  DALSA_LINE_SCAN_GRABBER
// 추가 포함 Dir C/C++ : $(mil_path)\..\include
// 추가 포함 Lib 링크 : $(mil_path)\..\lib
#include "Dalsa_FnGrab.h"
class CGraberCtrl
{
public:
	CGraberCtrl(void);
	~CGraberCtrl(void);
	bool initGrabber(char *pCamInfoFilePath, AOI_ImageCtrlQ * pIDTInterface);
	bool exitGrabber(void);
	bool GrabStart(int ScanLineNum, int LineScnaWidth);
	bool GrabEnd(void);

	//////////////////////////////////////////////////////////////////////////
public:
	//MBUFHOOKFCTPTR	m_pUserFunction;
	AOI_ImageCtrlQ	*	m_pIDTParam;
//	BYTE *				m_pImageBuffer;
	AOIGrabCutterQ	*	m_pGrabCutter;

//private:
	SapAcquisition	*	m_pAcq;
	SapAcqDevice	*	m_pAcqDevice;
	SapBuffer		*	m_pBuffers;
	SapTransfer		*	m_pXfer;
	//SapView			*		m_pView;

	//char     m_pacqServerName[CORSERVER_MAX_STRLEN], configFilename[MAX_PATH];
	UINT32					m_ServerNumber;
	UINT32					m_acqDeviceNumber;
	char*						m_pacqServerName;
	char*						m_configFilename;
public:
	PROC_STATE	*	m_pUpdateState;
//private:
//	IplImage	*	m_pImg
};
#endif

#if !defined(DALSA_LINE_SCAN_GRABBER) && !defined(MATROX_LINE_SCAN_GRABBER)
#include "AOI_ImageCtrlQ.h"
class CGraberCtrl
{
public:
	CGraberCtrl(void);
	~CGraberCtrl(void);
	bool initGrabber(AOI_ImageCtrlQ * pIDTInterface);
	bool exitGrabber(void);
	bool GrabStart(int ScanLineNum, int LineScnaWidth);
	bool GrabEnd(void);

	//////////////////////////////////////////////////////////////////////////
private:
	AOI_ImageCtrlQ	*	m_pIDTParam;
	BYTE *					m_pImageBuffer;
	AOIGrabCutterQ	*m_pGrabCutter;

public:
	PROC_STATE	*	m_pUpdateState;
};
#endif