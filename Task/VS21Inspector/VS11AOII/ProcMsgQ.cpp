#include "StdAfx.h"
#include "ProcMsgQ.h"


//////////////////////////////////////////////////////////////////////
// Defect Write 시간Format Create를 줄이기 위해서.(Overflow 일수도....)
//////////////////////////////////////////////////////////////////////

/*
*	Module Name		:	QCMDCtrlList
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

ProcMsgQ::ProcMsgQ()
{
	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_CntNode					= 0;

	InitializeCriticalSection(&m_CrtSection);
}
ProcMsgQ::~ProcMsgQ()
{
	EnterCriticalSection(&m_CrtSection);
	//QClean();
	{
		DataNodeObj * pDeleteNode = NULL;

		if(m_HeadNode.m_pNextNode != &m_TailNode)
		{
			pDeleteNode =  m_HeadNode.m_pNextNode;

			DataNodeObj * pNextNodeBuf = NULL;
			while (pDeleteNode != &m_TailNode)
			{

				pNextNodeBuf = pDeleteNode->m_pNextNode;

				delete pDeleteNode;

				pDeleteNode = pNextNodeBuf;
				pNextNodeBuf = NULL;
			}
			m_HeadNode.m_pFrontNode		= NULL;
			m_HeadNode.m_pNextNode		= &m_TailNode;

			m_TailNode.m_pFrontNode		= &m_HeadNode;
			m_TailNode.m_pNextNode		= NULL;
		}
	}
	LeaveCriticalSection(&m_CrtSection);

	DeleteCriticalSection(&m_CrtSection);
}


void ProcMsgQ::QPutNode(InspectDataQ	*pNewObj)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj *pNewNode = new DataNodeObj;

	pNewNode->pInpectObj = pNewObj;//Data저장.
	
	pNewNode->m_pFrontNode					= &m_HeadNode;
	pNewNode->m_pNextNode					= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode					= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
}

InspectDataQ	* ProcMsgQ::QGetNode()
{
	EnterCriticalSection(&m_CrtSection);
	InspectDataQ	*retData = nullptr;
	
	DataNodeObj *pReturnNode;
	pReturnNode = m_TailNode.m_pFrontNode;

	if(pReturnNode ==  &m_HeadNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return retData;
	}

	m_TailNode.m_pFrontNode = pReturnNode->m_pFrontNode;
	(pReturnNode->m_pFrontNode)->m_pNextNode = &m_TailNode;

	retData = pReturnNode->pInpectObj;

	delete pReturnNode;
	m_CntNode --;

	LeaveCriticalSection(&m_CrtSection);
	return retData;
}

void ProcMsgQ::QClean(void)
{
	EnterCriticalSection(&m_CrtSection);
	DataNodeObj * pDeleteNode = nullptr;

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pDeleteNode =  m_HeadNode.m_pNextNode;

	DataNodeObj * pNextNodeBuf = nullptr;
	while (pDeleteNode != &m_TailNode)
	{
		pNextNodeBuf = pDeleteNode->m_pNextNode;
		delete pDeleteNode;

		pDeleteNode = pNextNodeBuf;
		pNextNodeBuf = nullptr;
	}

	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_CntNode = 0;
	LeaveCriticalSection(&m_CrtSection);
}



/*
*	Module Name		:	QCMDCtrlList
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

DefectDataQ::DefectDataQ()
{
	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_CntNode					= 0;

	InitializeCriticalSection(&m_CrtSection);
}
DefectDataQ::~DefectDataQ()
{
	EnterCriticalSection(&m_CrtSection);
	//QClean();
	{
		DefectNodeObj * pDeleteNode = NULL;

		if(m_HeadNode.m_pNextNode != &m_TailNode)
		{
			pDeleteNode =  m_HeadNode.m_pNextNode;

			DefectNodeObj * pNextNodeBuf = NULL;
			while (pDeleteNode != &m_TailNode)
			{

				pNextNodeBuf = pDeleteNode->m_pNextNode;

				delete pDeleteNode;

				pDeleteNode = pNextNodeBuf;
				pNextNodeBuf = NULL;
			}
			m_HeadNode.m_pFrontNode		= NULL;
			m_HeadNode.m_pNextNode		= &m_TailNode;

			m_TailNode.m_pFrontNode		= &m_HeadNode;
			m_TailNode.m_pNextNode		= NULL;
		}
	}
	LeaveCriticalSection(&m_CrtSection);

	DeleteCriticalSection(&m_CrtSection);
}


void DefectDataQ::QPutNodeAct( int ScnaLine, int ScanCnt, int DefectIndex, DEFECT_DATA*pDefImgPos, COORD_DINFO *pDefCoordPos, BYTE* pDefectImg, int ImgSize)
{
	EnterCriticalSection(&m_CrtSection);
	DefectNodeObj *pNewNode = new DefectNodeObj;

	//Data Set
	pNewNode->m_DefectType		= 1;
	pNewNode->m_ScanLine			= ScnaLine;
	pNewNode->m_ScanCount		= ScanCnt;
	pNewNode->m_DefectIndex		= DefectIndex;
	pNewNode->m_DefImgPos		= *pDefImgPos;
	pNewNode->m_DefCoordPos		= *pDefCoordPos;
	pNewNode->m_ReviewImgSize	= ImgSize;
	pNewNode->m_pDefectImg		= new BYTE[ImgSize];

	memcpy(pNewNode->m_pDefectImg, pDefectImg, ImgSize);

	//Link Set
	pNewNode->m_pFrontNode					= &m_HeadNode;
	pNewNode->m_pNextNode					= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode					= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
}
void DefectDataQ::QPutNodePad(int ScnaLine, int PadPos, int PadIndex, int FlagMent,  int DefectIndex, DEFECT_DATA*pDefImgPos, COORD_DINFO *pDefCoordPos, BYTE* pDefectImg, int ImgSize)
{
	EnterCriticalSection(&m_CrtSection);
	DefectNodeObj *pNewNode = new DefectNodeObj;

	//Data Set
	pNewNode->m_DefectType		= 2;
	pNewNode->m_ScanLine			= ScnaLine;
	pNewNode->m_ScanCount		= PadPos;

	pNewNode->m_IndexPad			= PadIndex;
	pNewNode->m_FlagMentPAD		= FlagMent;
	
	pNewNode->m_DefectIndex		= DefectIndex;

	pNewNode->m_DefImgPos		= *pDefImgPos;
	pNewNode->m_DefCoordPos		= *pDefCoordPos;
	pNewNode->m_ReviewImgSize	= ImgSize;
	pNewNode->m_pDefectImg		=  new BYTE[ImgSize];
	memcpy(pNewNode->m_pDefectImg, pDefectImg, ImgSize);

	//Link Set
	pNewNode->m_pFrontNode					= &m_HeadNode;
	pNewNode->m_pNextNode					= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode					= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
}
bool DefectDataQ::QGetNode(int *pType,int *pScnaLine, int *pScanCnt, int *pPadIndex, int *pFlagMent, int *pDefectInde, DEFECT_DATA*pDefImgPos, COORD_DINFO *pDefCoordPos, BYTE* pDefectImg, int *pImgSize)
{
	EnterCriticalSection(&m_CrtSection);

	DefectNodeObj *pReturnNode;
	pReturnNode = m_TailNode.m_pFrontNode;
	if(pReturnNode ==  &m_HeadNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return false;
	}

	m_TailNode.m_pFrontNode = pReturnNode->m_pFrontNode;
	(pReturnNode->m_pFrontNode)->m_pNextNode = &m_TailNode;

	//Data전달
	*pType				= pReturnNode->m_DefectType;
	*pScnaLine			= pReturnNode->m_ScanLine;
	*pScanCnt			= pReturnNode->m_ScanCount;
	*pDefectInde		= pReturnNode->m_DefectIndex;

	*pPadIndex			= pReturnNode->m_IndexPad;
	*pFlagMent			= pReturnNode->m_FlagMentPAD;

	*pDefImgPos			= pReturnNode->m_DefImgPos;
	*pDefCoordPos		= pReturnNode->m_DefCoordPos;
	*pImgSize			= pReturnNode->m_ReviewImgSize;

	if(pReturnNode->m_pDefectImg != nullptr)
	{
		memcpy( pDefectImg, pReturnNode->m_pDefectImg, pReturnNode->m_ReviewImgSize);
		delete [] pReturnNode->m_pDefectImg;
		pReturnNode->m_pDefectImg = nullptr;
	}
	//대상 지우기.
	delete pReturnNode;
	m_CntNode --;

	LeaveCriticalSection(&m_CrtSection);
	return true;
}

void DefectDataQ::QClean(void)
{
	EnterCriticalSection(&m_CrtSection);
	DefectNodeObj * pDeleteNode = nullptr;

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pDeleteNode =  m_HeadNode.m_pNextNode;

	DefectNodeObj * pNextNodeBuf = nullptr;
	while (pDeleteNode != &m_TailNode)
	{
		pNextNodeBuf = pDeleteNode->m_pNextNode;

		if(pDeleteNode->m_pDefectImg != nullptr)
		{
			delete [] pDeleteNode->m_pDefectImg;
			pDeleteNode->m_pDefectImg = nullptr;
		}
		delete pDeleteNode;

		pDeleteNode = pNextNodeBuf;
		pNextNodeBuf = nullptr;
	}

	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_CntNode = 0;
	LeaveCriticalSection(&m_CrtSection);
}

//////////////////////////////////////////////////////////////////////////
///

/*
*	Module Name		:	QCMDCtrlList
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

ImageSaveQ::ImageSaveQ(int BufferLimitCnt)
{
	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_CntNode					= 0;
	m_BufferLimitCnt			= BufferLimitCnt;
	InitializeCriticalSection(&m_CrtSection);
}
ImageSaveQ::~ImageSaveQ()
{
	EnterCriticalSection(&m_CrtSection);
	//QClean();
	{
		IMGNodeObj * pDeleteNode = NULL;

		if(m_HeadNode.m_pNextNode != &m_TailNode)
		{
			pDeleteNode =  m_HeadNode.m_pNextNode;

			IMGNodeObj * pNextNodeBuf = NULL;
			while (pDeleteNode != &m_TailNode)
			{

				pNextNodeBuf = pDeleteNode->m_pNextNode;

				delete pDeleteNode;

				pDeleteNode = pNextNodeBuf;
				pNextNodeBuf = NULL;
			}
			m_HeadNode.m_pFrontNode		= NULL;
			m_HeadNode.m_pNextNode		= &m_TailNode;

			m_TailNode.m_pFrontNode		= &m_HeadNode;
			m_TailNode.m_pNextNode		= NULL;
		}
	}
	LeaveCriticalSection(&m_CrtSection);

	DeleteCriticalSection(&m_CrtSection);
}


void ImageSaveQ::QPutNode(int Type, int ScanLine, int ScanCount, IplImage *pTargetImage, CRect *pScanImgPos)
{
	EnterCriticalSection(&m_CrtSection);

	if(m_CntNode>m_BufferLimitCnt)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}



	IMGNodeObj *pNewNode = new IMGNodeObj;

	//Data Set
	pNewNode->ImgType		= Type;
	pNewNode->ScanLine		= ScanLine;
	pNewNode->ScanCount		= ScanCount;

	if(pScanImgPos!= nullptr)
		memcpy(&pNewNode->ScanImagePos, pScanImgPos, sizeof(CRect) );
	
	pNewNode->pSaveImg = cvCloneImage( pTargetImage);
//	pNewNode->pSaveImg		= cvCreateImage( cvSize(pTargetImage->width, pTargetImage->height), IPL_DEPTH_8U, 1);
//	cvCopyImage(pTargetImage, pNewNode->pSaveImg);

	if(pNewNode->pSaveImg == nullptr)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}

	//Link Set
	pNewNode->m_pFrontNode						= &m_HeadNode;
	pNewNode->m_pNextNode							= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode						= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_CntNode++;
	LeaveCriticalSection(&m_CrtSection);
}

IMGNodeObj * ImageSaveQ::QGetNode()
{
	//EnterCriticalSection(&m_CrtSection);

	//IMGNodeObj *pReturnNode = nullptr;
	//pReturnNode = m_TailNode.m_pFrontNode;
	//if(pReturnNode ==  &m_HeadNode)
	//{
	//	LeaveCriticalSection(&m_CrtSection);
	//	return nullptr;
	//}
	//LeaveCriticalSection(&m_CrtSection);
	//return pReturnNode;


	EnterCriticalSection(&m_CrtSection);

	IMGNodeObj *pReturnNode = nullptr;
	pReturnNode = m_TailNode.m_pFrontNode;
	if(pReturnNode ==  &m_HeadNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return nullptr;
	}

	m_TailNode.m_pFrontNode = pReturnNode->m_pFrontNode;
	(pReturnNode->m_pFrontNode)->m_pNextNode = &m_TailNode;

	m_CntNode --;

	LeaveCriticalSection(&m_CrtSection);
	return pReturnNode;

}

//bool ImageSaveQ::QPopNode()
//{
//	EnterCriticalSection(&m_CrtSection);
//
//	IMGNodeObj *pReturnNode;
//	pReturnNode = m_TailNode.m_pFrontNode;
//	if(pReturnNode ==  &m_HeadNode)
//	{
//		LeaveCriticalSection(&m_CrtSection);
//		return false;
//	}
//
//	m_TailNode.m_pFrontNode = pReturnNode->m_pFrontNode;
//	(pReturnNode->m_pFrontNode)->m_pNextNode = &m_TailNode;
//
//	if(pReturnNode->pSaveImg != nullptr)
//		cvReleaseImage(&pReturnNode->pSaveImg);
//	pReturnNode->pSaveImg = nullptr;
//
//	//대상 지우기.
//	delete pReturnNode;
//	m_CntNode --;
//
//	LeaveCriticalSection(&m_CrtSection);
//	return true;
//}

void ImageSaveQ::QClean(void)
{
	EnterCriticalSection(&m_CrtSection);
	IMGNodeObj * pDeleteNode = nullptr;

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pDeleteNode =  m_HeadNode.m_pNextNode;

	IMGNodeObj * pNextNodeBuf = nullptr;
	while (pDeleteNode != &m_TailNode)
	{
		pNextNodeBuf = pDeleteNode->m_pNextNode;


		if(pDeleteNode->pSaveImg != nullptr)
			cvReleaseImage(&pDeleteNode->pSaveImg);
		pDeleteNode->pSaveImg = nullptr;

		delete pDeleteNode;

		pDeleteNode = pNextNodeBuf;
		pNextNodeBuf = nullptr;
	}

	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_CntNode = 0;
	LeaveCriticalSection(&m_CrtSection);
}

