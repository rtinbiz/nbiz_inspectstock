#pragma once
#include "StdAfx.h"
#include "Dalsa_FnGrab.h"

#ifdef  DALSA_LINE_SCAN_GRABBER

void PutDalsaTestDefect(IplImage *pMainMap, CvPoint *pNamePos,char*text)
{
	extern CvFont		G_ImageInfoFont;
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_ImageInfoFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= pNamePos->x-5;
	StartTxt.y	= pNamePos->y+baseline;
	EndTxt.x	= pNamePos->x+text_size.width+5;
	EndTxt.y	= pNamePos->y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, CV_RGB(255, 255, 255));
	
	cvPutText(pMainMap, text, *pNamePos, &G_ImageInfoFont, CV_RGB(0,0,0));

	cvRectangle(pMainMap, StartTxt, EndTxt, CV_RGB(255, 0, 0));
}

#include "GrabberCtrl.h"
//Local 변수이나 Speed Up을 위해 전역으로.
AOI_ImageCtrlQ*	G_pIDTCtrlPtr					= nullptr;
InspectDataQ *	G_pGrabIMG					= nullptr;
//MIL_ID				G_ModifiedBufferId			= 0;//Grab MIL ID
PROC_STATE	*	G_pUpdateState				= nullptr;
CoordinatesProc*	G_MilpCoordObj				= nullptr;

DWORD				G_tickTickN = 0;
char					G_ImageSavePath[MAX_PATH]	= {"S:\\AOI_Out\\12_1234.bmp"};

int						G_OnlyGrabIndex				= 0;
void				  *G_pBufferAdd					= nullptr;

extern ImageSaveQ				G_AlignImgQ;
/*
*	Module Name		:	fn_GrabProcess
*	Parameter		:	Grab Parameter
*	Return			:	State.
*	Function		:	Local Grab Test를 위한 Thread Function.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
extern AOIBigImg G_GrabBigger;
void fn_GrabProcess(SapXferCallbackInfo *pInfo) 
{
	if (pInfo->IsTrash())
		return;
	
	int				returnValue = 0;
	CGraberCtrl *pMainObj = (CGraberCtrl *) pInfo->GetContext();
	G_pIDTCtrlPtr		= pMainObj->m_pIDTParam;
	G_MilpCoordObj	= G_pIDTCtrlPtr->m_pCoordObj;
	G_pUpdateState	= G_pIDTCtrlPtr->m_pUpdateState;

	static const int ImageSize = G_MilpCoordObj->m_HWParam.m_ProcImageW * G_MilpCoordObj->m_HWParam.m_ProcImageH;
	if(G_pUpdateState->m_bAlignScan == true)
	{
		IplImage *pNewGrabImg = cvCreateImage(cvSize(G_MilpCoordObj->m_HWParam.m_ProcImageW, G_MilpCoordObj->m_HWParam.m_ProcImageH), IPL_DEPTH_8U, 1);
		
		pMainObj->m_pBuffers->GetAddress(pMainObj->m_pBuffers->GetIndex(), &G_pBufferAdd);
		memcpy(pNewGrabImg->imageData, G_pBufferAdd, ImageSize);
	
		CRect ScanImgPos;
		G_AlignImgQ.QPutNode(1,1,1, pNewGrabImg,&ScanImgPos);
		cvReleaseImage(&pNewGrabImg);
	}
	else if(G_pIDTCtrlPtr->m_OnlyGrabImageSave == true)
	{
		sprintf_s(	G_ImageSavePath, MAX_PATH-1,"%s%04d.bmp",	
			G_pIDTCtrlPtr->m_OrgImgSavePath.GetBuffer(G_pIDTCtrlPtr->m_OrgImgSavePath.GetLength()),
			G_OnlyGrabIndex++);
		//////////////////////////////////////////////////////////////////////////
		//Image Export
		IplImage *pNewGrabImg = cvCreateImage(cvSize(G_MilpCoordObj->m_HWParam.m_ProcImageW, G_MilpCoordObj->m_HWParam.m_ProcImageH), IPL_DEPTH_8U, 1);

		pMainObj->m_pBuffers->GetAddress(&G_pBufferAdd);
		memcpy(pNewGrabImg->imageData, G_pBufferAdd, ImageSize);

		cvSaveImage(G_ImageSavePath, pNewGrabImg);
		cvReleaseImage(&pNewGrabImg);
	}
	else if(G_MilpCoordObj->m_OneLineScnaImgCnt > G_pIDTCtrlPtr->m_GrabScanImageCnt)
	{//조건 검사불필요 해짐.(Result에서 Grab상태 Check로 이용.)
		// Retrieve the data address of the current component
		PUINT8 pData = nullptr;
		int numPix = pMainObj->m_pBuffers->GetWidth() * pMainObj->m_pBuffers->GetHeight();

		pMainObj->m_pBuffers->GetAddress(pMainObj->m_pBuffers->GetIndex(), (void**)&pData);

		if(G_GrabBigger.m_bBigDataGrab == true)
		{
			G_GrabBigger.SetBigImage((char*)pData);
			memset(pMainObj->m_pGrabCutter->QGetImgPtr(), 0x00, numPix);
		}
		else
			memcpy(pMainObj->m_pGrabCutter->QGetImgPtr(), pData, numPix);

		pMainObj->m_pGrabCutter->QProcImgData();
		pMainObj->m_pBuffers->ReleaseAddress(pMainObj->m_pBuffers->GetIndex(), pData);
	}
	if(G_MilpCoordObj->m_OneLineScnaImgCnt == G_pIDTCtrlPtr->m_GrabScanImageCnt)
	{
		if(pMainObj->m_pXfer->IsGrabbing() == TRUE)
		{
			pMainObj->m_pXfer->Abort();
		}
		pMainObj->m_pGrabCutter->SaveLineScanImg();
	}
}

#endif
