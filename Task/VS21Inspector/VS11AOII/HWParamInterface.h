#pragma once

#define  HW_PARAM_PATH_TN21 "..\\Data\\SysHWParam\\HW_Param_Task21.ini"
//#define  HW_PARAM_PATH_TN12 "..\\Data\\SysHWParam\\HW_Param_Task12.ini"
//#define  HW_PARAM_PATH_TN13 "..\\Data\\SysHWParam\\HW_Param_Task13.ini"
//#define  HW_PARAM_PATH_TN14 "..\\Data\\SysHWParam\\HW_Param_Task14.ini";



#define  Section_HWINFO		"HWINFO"		
	#define  Key_VSB_Size		"VSB_Size"
	#define  Key_GrabImageW	"GrabImageW"
	#define  Key_GrabImageH	"GrabImageH"

	#define  Key_DefectMaxCntInImg	"DefectMaxCntInImg"
	#define  Key_DefectReviewSizeW	"DefectReviewSizeW"
	#define  Key_DefectReviewSizeH		"DefectReviewSizeH"

	#define  Key_INSPECTION_CAM_COUNT	"INSPECTION_CAM_COUNT"
	#define  Key_INSPECTION_SCAN_WIDTH	"INSPECTION_SCAN_WIDTH"
	#define  Key_INSPECTION_SCAN_HEIGHT	"INSPECTION_SCAN_HEIGHT"

	#define  Key_AOI_BLOCK_SCAN_START_X	 "BLOCK_SCAN_START_X"
	#define  Key_AOI_BLOCK_SCAN_START_Y "BLOCK_SCAN_START_Y"

	#define  Key_AOI_CAM_DISTANCE_X_LENS_TYPE1				"CAM_DISTANCE_X"
	#define  Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1				"CAM_DISTANCE_Y"
	#define  Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1		"SCAN_START_OFFSET_ODD"
	#define  Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1		"SCAN_START_OFFSET_EVEN"

	#define  Key_AOI_CAM_DISTANCE_X_LENS_TYPE2				"CAM_DISTANCE_X2"
	#define  Key_AOI_CAM_DISTANCE_Y_LENS_TYPE2				"CAM_DISTANCE_Y2"
	#define  Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE2		"SCAN_START_OFFSET_ODD2"
	#define  Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE2		"SCAN_START_OFFSET_EVEN2"

	#define  Key_OnlyGrabImageSave	"OnlyGrabImageSave"
	#define  Key_SaveDefectOrgImage	"SaveDefectOrgImage"
	#define  Key_DefectSaveFileType	"DefectSaveFileType"

	#define  Key_PadInspectAreaType	"PadInspectAreaType"

	#define  Key_SERIAL_PORT_CAM_DERCTION	"SERIAL_PORT_CAM_DERCTION"


	#define  Key_IMAGE_SAVE_PATH	"IMAGE_SAVE_PATH"
	#define  Key_RESULT_SAVE_PATH	"RESULT_SAVE_PATH"


	#define  Key_IMAGE_UPDATE_PATH	"IMAGE_UPDATE_PATH"
	#define  Key_RESULT_UPDATE_PATH	"RESULT_UPDATE_PATH"

	#define  Key_AXIS_Y_OVERLAP	"AXIS_Y_OVERLAP"

	#define  Key_LENS_CURVATURE_TYPE1	"LENS_CURVATURE_2d5"
	#define  Key_LENS_CURVATURE_TYPE2	"LENS_CURVATURE_4d0"

	#define  Key_DALSA_CCF_PATH		"DALSA_CCF_PATH"



#define  Section_LENS_OFFSET		"LENS_OFFSET"
	#define  Key_LensZone_X	"LensZone"
	#define  Key_LensOffset_X	"LensOffset"


#define  Section_Data_Options		"Data_Options"
	#define  Key_SavePadCutDrawImg	"SavePadCutDrawImg"
	#define  Key_SaveInspectImage		"SaveInspectImage"
	#define  Key_SaveSmallImage			"SaveSmallImage"
	#define  Key_DrawDefectArea		"DrawDefectArea"
	#define  Key_SaveActiveAreaHistogram	"SaveActiveAreaHistogram"
	#define  Key_SavePadOrgImgALL				"SavePadOrgImgALL"
	#define  Key_SavePadOrgImgSaveBmp	    "SavePadOrgImgSaveBmp"
	#define  Key_SavePadOrgImgBlurStep	    "SavePadOrgImgBlurStep"

	#define  Key_SmallDefectSizeX			"SmallDefectSizeX"
	#define  Key_SmallDefectSizeY				"SmallDefectSizeY"
	#define  Key_AOI_LightingHistMaxPresent				"LightingHistMaxPresent"

#define  Section_UpDn_PAD_AlignOptions		"UpDn_PAD_AlignOptions"
#define  Key_UsePadAlign				"UsePadAlign"

#define  Key_AlignWidth				"AlignWidth"
#define  Key_AlignWidthOverX		"AlignWidthOverX"
#define  Key_AlignOKValue				"AlignOKValue"

#define  Key_AlignUseMaxHigh			"AlignUseMaxHigh"
#define  Key_AlignImgNotChange			"AlignImgNotChange"
#define  Key_AlignImgUseCanny			"AlignImgUseCanny"

#define  Key_AlignImgUseCannyTh1			"AlignImgUseCannyTh1"
#define  Key_AlignImgUseCannyTh2			"AlignImgUseCannyTh2"