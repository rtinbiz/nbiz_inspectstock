#pragma once


#define  ALARM_CODE_None								0
#define  ALARM_CODE_HWParam							1
#define  ALARM_CODE_ActivParam						2
#define  ALARM_CODE_PadParam						3
#define  ALARM_CODE_GLassParamSize				4
#define  ALARM_CODE_GLassParamCal					5
#define  ALARM_CODE_HWParamPath					6
#define  ALARM_CODE_GrabStart						7
//////////////////////////////////////////////////////////////////////////
#define  ALARM_CODE_GrabBuffer						8
#define  ALARM_CODE_DiskBuffer						9

static char *G_strAlarmDefe[] = 
{
	"None Alarm",
	"H/W Parameter Loading Error",
	"Active Param Input Error",
	"Pad Param Input Error",
	"Glass Param Set Error(Param Size)",
	"Glass Param Coordinate Error",
	"H/W Param Result / Org Image Save Path Create Error",
	"Grabber Grab Start Error",
	//////////////////////////////////////////////////////////////////////////
	"Grab Buffer Over Flow",
	//////////////////////////////////////////////////////////////////////////
	"Disk Buffer Over Flow"
	
};