
// VS11AOII.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.

//#include "ProcState.h"
#include "AOI_ImageCtrlQ.h"
#include "PadAreaCtrlQ.h"

#include "GrabberCtrl.h"
#include "LocalImgGrab.h"
#include "PadCutterProc.h"
#include "PADInspectProc.h"
#include "ResultProcess.h"
#include "DefectFileFormat.h"
#include "SortResultProcess.h"
#include "WriteFileProc.h"
#include "ProcMsgQ.h"
#include "..\..\..\CommonHeader\Inspection\CoordinatesProc.h"
bool G_AOI_fnLoadHWParam(CoordinatesProc *pCoordProcObj, int LensType = 1);//Lens Index
bool G_AOI_fnReloadHWParamByLens(int TaskNumber, CoordinatesProc *pCoordProcObj, int LensTyep);
//bool G_AOI_fnLoadLightingHist(int TaskNumber, double *pSettingValue);

bool G_AOI_fnLoadInspectParam(CoordinatesProc *pCoordProcObj, int TaskNum);
// CVS11AOIIApp:
// 이 클래스의 구현에 대해서는 VS11AOII.cpp을 참조하십시오.
//

class CVS11AOIIApp : public CWinApp
{
public:
	CVS11AOIIApp();

// 재정의입니다.
public:
	virtual BOOL InitInstance();

// 구현입니다.

	DECLARE_MESSAGE_MAP()
};

extern CVS11AOIIApp theApp;