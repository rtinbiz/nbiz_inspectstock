#pragma once
#include "AOI_ImageCtrlQ.h"
#include "AOIGrabCutter.h"
class LocalImgGrab
{
public:
	LocalImgGrab(void);
	~LocalImgGrab(void);
private:
	AOI_ImageCtrlQ	*	m_pIDTParam;

public:
	bool initImgGrab(AOI_ImageCtrlQ * pIDTInterface);
	bool exitImgGrab(void);
	bool GrabStart(int ScanLineNum, int LineScnaWidth, int LoadImgCount = 1, int ImageWidth = 8192);
	bool GrabEnd(void);

public:
	PROC_STATE	*	m_pUpdateState;
	AOIGrabCutterQ	*m_pGrabCutter;
};
void PutTextInImageCreateDefect(IplImage *pMainMap, CvPoint *pNamePos,char*text);
UINT fn_GrabFromFile(LPVOID pParam);
//UINT fn_GrabFromFilePerLoading(LPVOID pParam);