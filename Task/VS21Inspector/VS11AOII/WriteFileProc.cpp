#include "StdAfx.h"
#include "WriteFileProc.h"
#include "resource.h"

#include "AOIGrabCutter.h"

CvFont		G_DefectFont;
void PutTextInImageDefect(IplImage *pMainMap, CvPoint *pNamePos,char*text)
{
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_DefectFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= pNamePos->x-5;
	StartTxt.y	= pNamePos->y+baseline;
	EndTxt.x	= pNamePos->x+text_size.width+5;
	EndTxt.y	= pNamePos->y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, CV_RGB(0, 0, 0));

	cvPutText(pMainMap, text, *pNamePos, &G_DefectFont, CV_RGB(255,255,255));

	//cvRectangle(pMainMap, StartTxt, EndTxt, CV_RGB(255, 0, 0));
}

UINT fn_WriteFileProcess(LPVOID pParam)
{
	PROC_RETWRITE	*		pProcParam		= (PROC_RETWRITE *) pParam;

	PROC_STATE	*			pUpdateState	= pProcParam->m_pUpdateState;

	CDefectFileFormat*		pWriteFileObj	= pProcParam->m_pWriteFileObj;
	CInterServerInterface *	pSInterface		= pProcParam->m_pServerInterface;
	DefectDataQ	*			pResultDefectQ	= pProcParam->m_pResultDefectQ;

	CoordinatesProc*		pCoordObj		= pProcParam->m_pCoordObj;

	ImageSaveQ	*		    pHistogramImgQ	= pProcParam->m_pHistogramImgQ;
	ImageSaveQ	*		    pInspectSaveImgQ = pProcParam->m_pInspectSaveImgQ;
	IMGNodeObj	*			pSaveNode		= nullptr;
	//////////////////////////////////////////////////////////////////////////
	IplImage	*				pRevewImg	= cvCreateImage( cvSize(pProcParam->m_ReviewImgW, pProcParam->m_ReviewImgW), IPL_DEPTH_8U, 1);
	IplImage	*				pSaveSmallImg	= cvCreateImage( cvSize(pCoordObj->m_HWParam.m_DefectReviewSizeW, pCoordObj->m_HWParam.m_DefectReviewSizeH), IPL_DEPTH_8U, 3);
	//cvZero(pRevewImg);
	memset(pRevewImg->imageData, 0xFF, pRevewImg->imageSize);
	DEFECT_DATA				DefectData;
	COORD_DINFO				CoordData;
	int						ReviewImgSize = 0;
	BYTE	 *				pReviewImgData =(BYTE *) pRevewImg->imageData;
	//////////////////////////////////////////////////////////////////////////
	extern int				G_MasterTaskIndex;
	//////////////////////////////////////////////////////////////////////////
	//Defect Info Size
	CString ImgText;
	CvPoint TextPos,  TextPos2, TextPos3, TextPos4, TextPos5;
	if(pCoordObj->m_HWParam.m_DefectReviewSizeH>400)
	{
		TextPos = cvPoint(20,30);
		TextPos2 = cvPoint(20,60);
		TextPos3 = cvPoint(20,90);
		TextPos4 = cvPoint(20,120);

		TextPos5 = cvPoint(20,150);
		cvInitFont (&G_DefectFont, CV_FONT_HERSHEY_TRIPLEX , 0.6f, 0.7f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	}
	else
	{
		TextPos = cvPoint(5,10);
		TextPos2 = cvPoint(5,25);
		TextPos3 = cvPoint(5,40);
		TextPos4 = cvPoint(5,55);
		TextPos5 = cvPoint(5,70);
		cvInitFont (&G_DefectFont, CV_FONT_HERSHEY_SIMPLEX , 0.2f, 0.5f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	}

	char SaveImageName[256];
	int DefectType = 0;
	int ScanLineNum = 0;
	int ScanImgCount = 0;

	int PadIndex = 0;
	int PadFlagMentCount = 0;
	int DefectIndex = 0;

	int ReviewRectOffset = 5;
	CvvImage ViewImage;
	ViewImage.Create(pRevewImg->width, pRevewImg->height, ((IPL_DEPTH_8U & 255)*3) );
	
	if(pProcParam->m_ProcessNum == 1)
	{//Defect Write Proc Thread Run
		
		while(pProcParam->m_bProcessRun == true)
		{
			if(pUpdateState->m_InspectionStop == INSPECTION_STOP)
			{
				if(pResultDefectQ->QGetCnt()>0)
					pResultDefectQ->QClean();

				if(pWriteFileObj->m_fnIsOpened() == true)
					pWriteFileObj->m_fnCloseFileObj();


				if(pHistogramImgQ->QGetCnt()>0)
					pHistogramImgQ->QClean();

				Sleep(10);
			}
			else	if(pResultDefectQ->QGetNode(&DefectType, &ScanLineNum, &ScanImgCount, &PadIndex, &PadFlagMentCount, &DefectIndex, &DefectData, &CoordData, pReviewImgData,  &ReviewImgSize) == true)
			{

				pWriteFileObj->m_fnWriteImageDataOnNode(pReviewImgData);

				cvCvtColor( pRevewImg, pSaveSmallImg, CV_GRAY2RGB );


				if(pUpdateState->m_pHDcView != nullptr  )
				{
					if(pUpdateState->bTEST_DEFECT_IMG_SAVE_JPG)
					//if(pCoordObj->m_HWParam.m_SaveDefectOrgImage > 0)
					{
						static char PadPos[10];
						switch (DefectType)
						{
						case 1:
							//sprintf_s(SaveImageName, "%s\\DAct_%02d_%03d_%02d.jpg", pCoordObj->m_HWParam.strResultPath, ScanLineNum, ScanImgCount, DefectIndex);
							//if(pUpdateState->bTEST_DEFECT_IMG_BMPorJPG == true)
							//	sprintf_s(SaveImageName, "%s\\DAct_%02d_%03d_%02d.bmp", pCoordObj->m_HWParam.strOrgImgPath, ScanLineNum, ScanImgCount, DefectIndex);
							//else
							//	sprintf_s(SaveImageName, "%s\\DAct_%02d_%03d_%02d.jpg", pCoordObj->m_HWParam.strOrgImgPath, ScanLineNum, ScanImgCount, DefectIndex);
							sprintf_s(SaveImageName, "%s\\%s\\DAct_%02d_%03d_%02d.bmp", pCoordObj->m_HWParam.strOrgImgPath, SAVE_SMALL_IMG_FOLDER, ScanLineNum, ScanImgCount, DefectIndex);
							break;
						case 2:
							{
								switch(ScanImgCount)
								{
								case 0:	sprintf_s(PadPos, "Up");	break;
								case 1:	sprintf_s(PadPos, "Dn");	break;
								case 2:	sprintf_s(PadPos, "Lf");	break;
								case 3:	sprintf_s(PadPos, "Rt");	break;
								default:
									sprintf_s(PadPos, "Un");	break;

								}
								//////////////////////////////////////////////////////////////////////////
								// 1: Scan Line Num, 2:Pad부(상하좌우), 3:Pad위치, 4:단편화 Index, 5:Defect Index
								/*sprintf_s(SaveImageName, "%s\\DPad_%02d_%s_%02d_%02d_%02d.jpg", pCoordObj->m_HWParam.strResultPath, 
																								ScanLineNum, PadPos, PadIndex,  PadFlagMentCount, DefectIndex);*/

								sprintf_s(SaveImageName, "%s\\%s\\DPad_%02d_%s_%02d_%02d_%02d.bmp", pCoordObj->m_HWParam.strOrgImgPath, SAVE_SMALL_IMG_FOLDER, 
									ScanLineNum, PadPos, PadIndex,  PadFlagMentCount, DefectIndex);	
								//if(pUpdateState->bTEST_DEFECT_IMG_BMPorJPG == true)
								//	sprintf_s(SaveImageName, "%s\\DPad_%02d_%s_%02d_%02d_%02d.bmp", pCoordObj->m_HWParam.strOrgImgPath, 
								//	ScanLineNum, PadPos, PadIndex,  PadFlagMentCount, DefectIndex);
								//else
								//	sprintf_s(SaveImageName, "%s\\DPad_%02d_%s_%02d_%02d_%02d.jpg", pCoordObj->m_HWParam.strOrgImgPath, 
								//	ScanLineNum, PadPos, PadIndex,  PadFlagMentCount, DefectIndex);
							}break;
						default:
							sprintf_s(SaveImageName, "%s\\%s\\DUn_%02d_%03d_%02d.bmp", pCoordObj->m_HWParam.strOrgImgPath, SAVE_SMALL_IMG_FOLDER,  ScanLineNum, ScanImgCount, DefectIndex);
							//sprintf_s(SaveImageName, "%s\\DUn_%02d_%03d_%02d.jpg", pCoordObj->m_HWParam.strResultPath, ScanLineNum, ScanImgCount, DefectIndex);
							//if(pUpdateState->bTEST_DEFECT_IMG_BMPorJPG == true)
							//	sprintf_s(SaveImageName, "%s\\DUn_%02d_%03d_%02d.jpg", pCoordObj->m_HWParam.strOrgImgPath, ScanLineNum, ScanImgCount, DefectIndex);
							//else
							//	sprintf_s(SaveImageName, "%s\\DUn_%02d_%03d_%02d.bmp", pCoordObj->m_HWParam.strOrgImgPath, ScanLineNum, ScanImgCount, DefectIndex);
							//break;
						}
					}
					
					ImgText.Format("C X:%8d, Y:%8d", CoordData.CoodStartX, CoordData.CoodStartY);
					PutTextInImageDefect(pSaveSmallImg, &TextPos, ImgText.GetBuffer(ImgText.GetLength()));
					ImgText.Format("M X:%8d, Y:%8d", (pCoordObj->m_GlassParam.CenterPointX + CoordData.CoodStartX)+(CoordData.CoodWidth/2),
													 (pCoordObj->m_GlassParam.CenterPointY+CoordData.CoodStartY*(-1))+(CoordData.CoodHeight/2));
					PutTextInImageDefect(pSaveSmallImg, &TextPos2, ImgText.GetBuffer(ImgText.GetLength()));

					ImgText.Format("D X:%8d, Y:%8d",  (CoordData.CoodWidth), (CoordData.CoodHeight));
					PutTextInImageDefect(pSaveSmallImg, &TextPos3, ImgText.GetBuffer(ImgText.GetLength()));

					ImgText.Format("P X:%8d, Y:%8d",	DefectData.StartX+ abs(DefectData.StartX- DefectData.EndX)/2, 
														DefectData.StartY+ abs(DefectData.StartY- DefectData.EndY)/2);
					PutTextInImageDefect(pSaveSmallImg, &TextPos4, ImgText.GetBuffer(ImgText.GetLength()));

#ifdef TEST_LOCAL_FILE
					ImgText.Format("DEFECT TYPE : %d",	DefectData.DefectType);
					CvPoint posDefectType = cvPoint(TextPos4.x, TextPos4.y + (TextPos4.y - TextPos3.y));
					PutTextInImageDefect(pSaveSmallImg, &posDefectType, ImgText.GetBuffer(ImgText.GetLength()));

					char szText[256];
					sprintf_s(szText,"[INS] (%d) Defect Type : %d", DefectIndex, DefectData.DefectType);
					char szPath[MAX_PATH];
					strcpy(szPath, pCoordObj->m_HWParam.strOrgImgPath);
					char *pFileName = strrchr(szPath, '\\');
					if(pFileName != 0x00) {
						*pFileName = 0x00;
						pFileName = strrchr(szPath, '\\');
						sprintf_s(szText,"[INS]     > %s (%d) Defect Type : %d", pFileName + 1, DefectIndex, DefectData.DefectType);
					}
					OutputDebugStringA(szText);
#endif

					TextPos5.y = pSaveSmallImg->height-10;
					ImgText.Format("Min:%3d, Max:%3d, Avg:%3d, Cnt:%4d",	CoordData.nDValueMin, CoordData.nDValueMax, CoordData.nDValueAvg, CoordData.nDefectCount);
					PutTextInImageDefect(pSaveSmallImg, &TextPos5, ImgText.GetBuffer(ImgText.GetLength()));

					CvRect DrawDefectRect;
					DrawDefectRect.x = pCoordObj->m_HWParam.m_DefectReviewSizeW/2;
					DrawDefectRect.y = pCoordObj->m_HWParam.m_DefectReviewSizeH/2;

					DrawDefectRect.width	= abs(DefectData.StartX- DefectData.EndX);
					DrawDefectRect.height	= abs(DefectData.StartY- DefectData.EndY);
					DrawDefectRect.x			-= DrawDefectRect.width/2;
					DrawDefectRect.y		-= DrawDefectRect.height/2;

					DrawDefectRect.x			-= ReviewRectOffset;
					DrawDefectRect.y		-= ReviewRectOffset;
					DrawDefectRect.width	+= ReviewRectOffset*2;
					DrawDefectRect.height	+= ReviewRectOffset*2;

					if(DrawDefectRect.x<0)
						DrawDefectRect.x = 2;
					if(DrawDefectRect.y<0)
						DrawDefectRect.y = 2;

					CvPoint StarPos, EndPos;
					if(pUpdateState->nSmallDefectSizeX == 0 && pUpdateState->nSmallDefectSizeY == 0)
					{
						StarPos.x = DrawDefectRect.x;
						StarPos.y = DrawDefectRect.y;

						EndPos.x = DrawDefectRect.x+DrawDefectRect.width;
						EndPos.y =  DrawDefectRect.y+DrawDefectRect.height;


						if(EndPos.x > pCoordObj->m_HWParam.m_DefectReviewSizeW)
							EndPos.x = pCoordObj->m_HWParam.m_DefectReviewSizeW;
						if(EndPos.y > pCoordObj->m_HWParam.m_DefectReviewSizeH)
							EndPos.y = pCoordObj->m_HWParam.m_DefectReviewSizeH;

						cvDrawRect(pSaveSmallImg, StarPos, EndPos,CV_RGB(0,255,0), 1);
					}
					else
					{
						int WSmallSize = pUpdateState->nSmallDefectSizeX;
						int HSmallSize = pUpdateState->nSmallDefectSizeY;
						if(WSmallSize>pCoordObj->m_HWParam.m_DefectReviewSizeW)
							WSmallSize = pCoordObj->m_HWParam.m_DefectReviewSizeW;
						if(HSmallSize>pCoordObj->m_HWParam.m_DefectReviewSizeH)
							HSmallSize = pCoordObj->m_HWParam.m_DefectReviewSizeH;

						StarPos.x = (pCoordObj->m_HWParam.m_DefectReviewSizeW/2) - (WSmallSize/2) ;
						StarPos.y = (pCoordObj->m_HWParam.m_DefectReviewSizeH/2) - (HSmallSize/2) ;

						EndPos.x = StarPos.x+ WSmallSize;
						EndPos.y = StarPos.y+ HSmallSize;

						cvDrawRect(pSaveSmallImg, StarPos, EndPos,CV_RGB(255,0,0), 1);
					}				
					//ImgText.Format("I N:%8d, C:%8d", ScanLineNum, ScanLineCount);
					//PutTextInImageDefect(pRevewImg, &TextPos3, ImgText.GetBuffer(ImgText.GetLength()));
					if(pUpdateState->bTEST_DEFECT_IMG_SAVE_JPG)
					//if(pCoordObj->m_HWParam.m_SaveDefectOrgImage > 0)
					{
						if(!cvSaveImage(SaveImageName, pSaveSmallImg))
						{
							pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"cvSaveImage Error(%s)", SaveImageName);
						}
					}

					ViewImage.CopyOf(pSaveSmallImg);
					ViewImage.DrawToHDC(*pUpdateState->m_pHDcView, pUpdateState->m_ViewRect);
				}
			}
			else if(pUpdateState->m_bBlockScanEnd == true && pResultDefectQ->QGetCnt() == 0 )
			{//Block Scan이 완료 되고, Write할 Result File이 없다
				
				DWORD nProcFileUpdateTime =  GetTickCount();

				pUpdateState->m_bBlockScanEnd = false; 
				
				CString CopyTargetFile;
				CString UpdateFilePath;
				//Block End 후 처리를 진행 한다.
				if(pWriteFileObj->m_fnIsOpened() == true)
				{
					CopyTargetFile.Format("%s", pWriteFileObj->m_ImageFilePath);
					UpdateFilePath.Format("%s", pWriteFileObj->m_ImageFileUpdatePath);
					pWriteFileObj->m_fnCloseFileObj();
				}

				//Defect Img File Write Start를 알린다.
				pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_FileAccess, nBiz_Recv_DefectWriteStart, 0);
				//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
				//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM, nBiz_AOI_FileAccess, nBiz_Recv_DefectWriteStart, 0);
		
				pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Defect Write Start");
				if(CopyTargetFile.GetLength()>3)
				{
					pUpdateState->m_bDefectFileCopy = true;
					if(::CopyFile(CopyTargetFile, UpdateFilePath,  FALSE) == TRUE)
						::DeleteFile(CopyTargetFile);
					pUpdateState->m_bDefectFileCopy = false;
				}
				//Defect Img File Write End를 알린다.
				pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_FileAccess, nBiz_Recv_DefectWriteEnd, 0);
				//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
				//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM, nBiz_AOI_FileAccess, nBiz_Recv_DefectWriteEnd, 0);

				pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Defect Write End");

				//File Backup Start.
				pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_FileAccess, nBiz_Recv_BackupStart, 0);
				//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
				//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM, nBiz_AOI_FileAccess, nBiz_Recv_BackupStart, 0);

				pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Backup Start");
				{
					while(pInspectSaveImgQ->QGetCnt() > 0)
						Sleep( 10 );
					Sleep( 100);
					pUpdateState->m_bOrgImageBackup = true;
					FolderCopyFromAToB(pCoordObj->m_HWParam.strOrgImgPath,  pCoordObj->m_ImageUpdatePath);
					pUpdateState->m_bOrgImageBackup = false;
				}


				pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_FileAccess, nBiz_Recv_BackupEnd, 0);
				//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
				//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM, nBiz_AOI_FileAccess, nBiz_Recv_BackupEnd, 0);
		
				pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Backup End");
				::SendMessage(pUpdateState->m_UpdateUIHandle, WM_USER_STATE_UPDATE, nBiz_AOI_STATE_IDLE, 0);

				pUpdateState->m_nProcFileUpdateTime =  GetTickCount()-nProcFileUpdateTime;

				if(pUpdateState->m_nProcFileUpdateMaxTime<pUpdateState->m_nProcFileUpdateTime)
					pUpdateState->m_nProcFileUpdateMaxTime = pUpdateState->m_nProcFileUpdateTime;
			}
			else
				Sleep(10);

			if(pHistogramImgQ!= nullptr)
			{
				if(pHistogramImgQ->QGetCnt()>0)
				{
					pSaveNode = pHistogramImgQ->QGetNode();
					if(pSaveNode != nullptr)
					{
						ImgText.Format("Histogram_%02d_%03d", pSaveNode->ScanLine, pSaveNode->ScanCount);
						PutTextInImageDefect(pSaveNode->pSaveImg, &TextPos, ImgText.GetBuffer(ImgText.GetLength()));

						if(pUpdateState->bRealTimeLightValueSend == true &&	pUpdateState->bRealTimePixelValueSend == false)
							sprintf_s(SaveImageName, "%s\\%s\\Hist_Light_%02d_%03d.jpg",pCoordObj->m_HWParam.strOrgImgPath, SAVE_HISTO_IMG_FOLDER, pSaveNode->ScanLine, pSaveNode->ScanCount);
						else
							sprintf_s(SaveImageName, "%s\\%s\\Hist_Pixel_%02d_%03d.jpg",pCoordObj->m_HWParam.strOrgImgPath, SAVE_HISTO_IMG_FOLDER, pSaveNode->ScanLine, pSaveNode->ScanCount);

						if(!cvSaveImage(SaveImageName, pSaveNode->pSaveImg))
							pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"cvSaveImage Error(%s)", SaveImageName);

						//////////////////////////////////////////////////////////////////////////
						//저장 Image 정리.
						if(pSaveNode->pSaveImg != nullptr)
							cvReleaseImage(&pSaveNode->pSaveImg);
						pSaveNode->pSaveImg = nullptr;

						delete pSaveNode;
						pSaveNode = nullptr;
					}
				}
			}
		}
		//////////////////////////////////////////////////////////////////////////
	}
	else		
	{//Image Write Proce Thread Run
		int DataOrgStepIndex =0;
		char FileType[10];
		while(pProcParam->m_bProcessRun == true)
		{
			if(pUpdateState->m_InspectionStop == INSPECTION_STOP)
			{
				if(pInspectSaveImgQ->QGetCnt()>0)
				{
					pInspectSaveImgQ->QClean();
				}
				Sleep(10);
			}

			pSaveNode = pInspectSaveImgQ->QGetNode();
			if(pSaveNode != nullptr)
			{
				DataOrgStepIndex++;

				ImgText.Format("M X:%8d, Y:%8d, W:%d, H:%d",pSaveNode->ScanImagePos.left, pSaveNode->ScanImagePos.top, pSaveNode->ScanImagePos.Width(), pSaveNode->ScanImagePos.Height() );
				PutTextInImageDefect(pSaveNode->pSaveImg, &TextPos, ImgText.GetBuffer(ImgText.GetLength()));

				switch(pCoordObj->m_HWParam.DefectSaveFileType)
				{
				//case 0:	sprintf_s(FileType, "jpg");	break;
				case 1:		sprintf_s(FileType, "bmp");	break;
				case 2:		sprintf_s(FileType, "png");	break;
				default:	sprintf_s(FileType, "jpg");	break;
				}
				
				if(pSaveNode->ImgType == 1)//Active Image
					sprintf_s(SaveImageName, "%s\\%s\\Act_%02d_%03d.%s",pCoordObj->m_HWParam.strOrgImgPath, SAVE_ORG_IMG_FOLDER, pSaveNode->ScanLine, pSaveNode->ScanCount, FileType);
				else//Pad Image
					sprintf_s(SaveImageName, "%s\\%s\\Pad_%02d_%03d_%02d.%s",pCoordObj->m_HWParam.strOrgImgPath, SAVE_ORG_IMG_FOLDER, pSaveNode->ScanLine, pSaveNode->ScanCount,DataOrgStepIndex++, FileType);

				if(!cvSaveImage(SaveImageName, pSaveNode->pSaveImg))
				{
					pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"cvSaveImage Error(%s)", SaveImageName);
				}
				if(DataOrgStepIndex>100)
					DataOrgStepIndex = 0;
				//////////////////////////////////////////////////////////////////////////
				//저장 Image 정리.
				if(pSaveNode->pSaveImg != nullptr)
					cvReleaseImage(&pSaveNode->pSaveImg);
				pSaveNode->pSaveImg = nullptr;

				delete pSaveNode;
				pSaveNode = nullptr;
			}
			else
				Sleep(10);
		}
	}

	ViewImage.Destroy();
	if(pRevewImg = nullptr)
		cvReleaseImage(&pRevewImg);

	if(pSaveSmallImg = nullptr)
		cvReleaseImage(&pSaveSmallImg);
	
	pProcParam->m_pResultThread = nullptr;
	return 0;
}