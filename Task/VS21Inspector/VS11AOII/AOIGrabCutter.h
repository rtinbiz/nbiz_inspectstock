#pragma once
#include "AOI_ImageCtrlQ.h"


typedef struct ImgNodeObj_AGT
{
	ImgNodeObj_AGT *	m_pFrontNode;
	ImgNodeObj_AGT *	m_pNextNode;

	IplImage			*pImgObj;


	ImgNodeObj_AGT()
	{
		m_pFrontNode	= nullptr;
		m_pNextNode		= nullptr;
		pImgObj			= nullptr;
	}

}ImgNodeObj;
//////////////////////////////////////////////////////////////////////////
//Sen Command Queue./ Receive Data Queue.
class AOIGrabCutterQ  
{
public:
	AOIGrabCutterQ(	AOI_ImageCtrlQ*	pIDTCtrlPtr, int CountNode);
	~AOIGrabCutterQ();
public:
	DWORD m_tickTickN;
	AOI_ImageCtrlQ*	m_pIDTCtrlPtr;
private:
	//Debug 용이다.Image 중간에서 지정된 Size만큼의 폭을 가지고 전체 Scan Line Image를 저장 한다.
	int m_LineScanImgWidth;
	IplImage *m_pLineScanImg;

	InspectDataQ *	m_pGrabIMG;
public:
	ImgNodeObj *m_pPutNode; 
private:
	CRITICAL_SECTION m_CrtSection;
	int		m_CntNode;

	//////////////////////////////////////////////////////////////////////////
	int m_ImgSize;
	int m_CopySize;
	int m_UpCpyIndex;
	int m_DnCpyIndex;
	//////////////////////////////////////////////////////////////////////////
public:
	int m_GrabCountImg;
	int m_GrabTotoalCnt;

	void	QSendTarget(ImgNodeObj *pTargetNode);
public:
	void SaveLineScanImg();

	void	QStartGrab(int GrabTotalCnt, int LineScanImgWidth);

	int		QGetCnt(){return m_CntNode;};
	void	QProcImgData();
	BYTE*	QGetImgPtr();
	void	QDataReset();
	void	QClean(void);
};


#include "MacroInspection.h"

/*
*	Module Name		:	AOIBigImg
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2015.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
class AOIBigImg  
{
public:
	AOIBigImg();
	~AOIBigImg();
	IplImage *m_pOrgImgBuf;
	IplImage *m_pReSizeBuf;
	IplImage *m_pBigImg;
	CMacroInspect InpectBigImg;
	int m_NowScanLine;
	int m_NowImgIndex;

	int m_nTotalLine;
	int m_nTotalScanImg;
	int m_nWidth;
	int m_nHeight;
	bool m_bBigDataGrab;

	int m_ShiftOverlapOffsetX;
	int m_OddScanOffset;
	int m_EvenScanOffset;

	int m_StartMPosX;
	int m_StartMPosY;

	int m_CenterPosX;
	int m_CenterPosY;

	double m_PixelResolution;
public:
	void SetOffset(int ShiftOverlapOffsetX_um, int OddScanOffset, int EvenScanOffset, double PixelResolution);

	void SetCoordPos(int StartMPosX, int StartMPosY, int CenterPosX, int CenterPosY, double PixelResolution);

	void SetImageSize(int nWidth, int nHeight, int nTotalLine, int nTotalScanImg);
	void SetBigImage(char *pGrabImg);//, int nScanImg);
	int  InspectionBigImage(SAOI_IMAGE_INFO stImageInfo, SAOI_COMMON_PARA stCommonPara, COORD_DINFO *pCoordinateRetList);
	void SaveBigImge(CString strFullPath);

	CvFont		G_DefectFont;
	UINT fn_WriteFileProcess_BigImg(COORD_DINFO *pCoordinateRetList1, DEFECT_DATA *pDefectDataBuf,int WriteStep);
	inline void PutTextInImageDefect_BigImg(IplImage *pMainMap, CvPoint *pNamePos,char*text);
};