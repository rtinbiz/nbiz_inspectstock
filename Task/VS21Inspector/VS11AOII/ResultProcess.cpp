#include "StdAfx.h"
#include "ResultProcess.h"

CvFont G_Histfont;
void CalcAFValueSo( BYTE *a, int sx, int sy, BYTE *out) 
{ 
	int i, j, ctr = 0; 
	BYTE *p1, *p2, *p3; 

	p1 = a; p2 = p1+sx; p3 = p2+sx; 

	for( j = 1; j < sy-1; ++j ) 
	{ 
		for( i = 1; i < sx-1; ++i ) 
		{ 
			out[ctr++] = ( abs((p1[0] + 2*p1[1] + p1[2]) - (p3[0] + 2*p3[1]+ p3[2])) + 
				abs((p1[2] + 2*p2[2] + p3[2]) - (p1[0] + 2*p2[0] + p3[0])) ) / 6; 			
			++p1; ++p2; ++p3; 
		} 
	} 
} 

extern int		G_MasterTaskIndex;
//CRITICAL_SECTION G_ViewCrtSection;
UINT fn_ResultProcess(LPVOID pParam)
{
	PROC_RESULT	*			pProcParam			= (PROC_RESULT *) pParam;

	AOI_ImageCtrlQ *		pIDTCtrlQ			= pProcParam->m_pIDTCtrlQ;
	PAD_ImageCtrlQ *		pPADCtrlQ			= pProcParam->m_pPADCtrlQ;
	CoordinatesProc*		pCoordObj			= pIDTCtrlQ->m_pCoordObj;

	PROC_PAD	*			pPadProcObj			= pProcParam->m_pPadProcObj;

	PROC_STATE	*			pUpdateState		= pIDTCtrlQ->m_pUpdateState;
	InspectDataQ*			pTargetNode			= nullptr;

	ImageSaveQ	*			pInspectSaveImgQ	= pProcParam->m_pInspectSaveImgQ;

	ImageSaveQ	*			pHistogramImgQ		= pProcParam->m_pHistogramImgQ;
	
	//////////////////////////////////////////////////////////////////////////
	SCANIMGINFO	*			pCoodArea = nullptr;
	//////////////////////////////////////////////////////////////////////////
	//Defect Position Buffer.
	CRect DefectAreaInImage;
	IplImage * ImageSavebuf = cvCreateImage( cvSize(	pCoordObj->m_HWParam.m_DefectReviewSizeW, 
																			pCoordObj->m_HWParam.m_DefectReviewSizeH), 
																			IPL_DEPTH_8U, 1);
	//cvZero(ImageSavebuf);
	memset(ImageSavebuf->imageData, 0xFF, ImageSavebuf->imageSize);
	//Grab Image Area.
	CRect GrabImageArea;
	GrabImageArea.left	= 0;
	GrabImageArea.right = GrabImageArea.left + pCoordObj->m_HWParam.m_ProcImageW;
	GrabImageArea.top	= 0;
	GrabImageArea.bottom= GrabImageArea.top + pCoordObj->m_HWParam.m_ProcImageH+(pCoordObj->m_AXIS_Y_OVER_STEP*2);
	//////////////////////////////////////////////////////////////////////////
	DWORD tickTickN = 0;
	bool bSizeError = false;
#ifndef TEST_DEFECT_IMG_IN_ORG
	int PasteStartX = 0;
	int PasteStartY = 0;
#endif

	cvInitFont (&G_Histfont, CV_FONT_HERSHEY_SIMPLEX , 0.2f, 0.5f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	//cvInitFont (&G_Histfont, CV_FONT_HERSHEY_TRIPLEX , 0.6f, 0.7f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX
	//InitializeCriticalSection(&G_ViewCrtSection);
	IplImage	* pHistogramImg = cvCreateImage( cvSize(300, 170), IPL_DEPTH_8U, 3);
	CvvImage ViewImage;
	ViewImage.Create(pHistogramImg->width, pHistogramImg->height, ((IPL_DEPTH_8U & 255)*3) );

	int HistogramW = 640;
	int HistogramH = 640;
	CvRect HistImgArea;
	HistImgArea.x			= (pCoordObj->m_HWParam.m_ProcImageW/2)- (HistogramW/2);
	HistImgArea.y			= (pCoordObj->m_HWParam.m_ProcImageH/2)- (HistogramH/2);
	HistImgArea.width		= HistogramW;
	HistImgArea.height	    = HistogramH;

	CRect HistogramCheckArea;
	CRect HistogramInsertRect;
	CRect CalcHistonsertRect;
	//영상의 좌우 25%위치는 사용 하지 않는다.
	HistogramCheckArea.top		= 0;
	HistogramCheckArea.left		= (int)(pCoordObj->m_HWParam.m_ProcImageW*0.28);
	HistogramCheckArea.right		= (pCoordObj->m_HWParam.m_ProcImageW-1) - HistogramCheckArea.left;
	HistogramCheckArea.bottom	= pCoordObj->m_HWParam.m_ProcImageH-1+(pCoordObj->m_AXIS_Y_OVER_STEP*2);
		

	IplImage*pdestimage	= cvCreateImage(cvSize(HistogramW, HistogramH),IPL_DEPTH_8U, 1);
	IplImage*pAFORGImg	= cvCreateImage(cvSize(HistogramW, HistogramH),IPL_DEPTH_8U, 1);
	IplImage*pHistImgOrg   = cvCreateImage( cvSize(HistogramW, HistogramH), IPL_DEPTH_8U, 1);
	cvZero(pdestimage);
	cvZero(pAFORGImg);
	cvZero(pHistImgOrg);

	int DivAFArea = HistogramW*HistogramH;
	//CvScalar sumBuff;
	CvPoint StartP, EndP;//, XSP, XEP;	
	while(pProcParam->m_bProcessRun == true)
	{
		pTargetNode = pIDTCtrlQ->m_fnQGetInspectPTR_ResultProc();
		//if(pUpdateState->m_InspectionStop  == INSPECTION_STOP)
		//{
		//	if(pTargetNode != nullptr)
		//		pTargetNode->m_fnQSetProcessEnd(QDATA_PROC_END_SORT);
		//}
		//else
		if(pTargetNode != nullptr)
		{
			pCoodArea = (SCANIMGINFO	*)pTargetNode->m_pCoordAraeInImg;
			
			tickTickN = ::GetTickCount();
			//if(!(pTargetNode->m_DefectListCnt == 1 && pTargetNode->m_pSearchRetList[0].DefectType == 1))
			{

				if(pTargetNode->m_DefectListCnt > 0)
				{	

					for(int WriteStep = 0; WriteStep<pTargetNode->m_DefectListCnt; WriteStep++)
					{
						//Image Defect Coordinate좌표계로 변환.
						G_CalcImgPosToCood(	pCoordObj, pCoodArea,	&pTargetNode->m_pSearchRetList[WriteStep], &pTargetNode->m_pCoordinateRetList[WriteStep]);

						//if(pUpdateState->bTEST_INSPECT_IMG_DRAW_DEFECT == true)
						//{
						//	StartP	= cvPoint(pTargetNode->m_pSearchRetList[WriteStep].StartX,	pTargetNode->m_pSearchRetList[WriteStep].StartY);
						//	EndP		= cvPoint(pTargetNode->m_pSearchRetList[WriteStep].EndX,		pTargetNode->m_pSearchRetList[WriteStep].EndY);
						//	XSP		= cvPoint(pTargetNode->m_pSearchRetList[WriteStep].EndX,		pTargetNode->m_pSearchRetList[WriteStep].StartY);
						//	XEP		= cvPoint(pTargetNode->m_pSearchRetList[WriteStep].StartX,	pTargetNode->m_pSearchRetList[WriteStep].EndY);
						//	IplImage *pDrawTarget = nullptr;
						//	if(pUpdateState->bTEST_INSPECT_IMG == true)
						//		pDrawTarget = pTargetNode->m_pGrabImage;
						//	else
						//		pDrawTarget = pTargetNode->m_pGrabOrgImage;

						//	cvDrawRect(pDrawTarget, StartP, EndP,CV_RGB(128,128,128));
						//	cvLine(pDrawTarget, StartP, EndP,CV_RGB(128,128,128));
						//	cvLine(pDrawTarget, XSP, XEP,CV_RGB(128,128,128));
						//}


						//////////////////////////////////////////////////////////////////////////
						//Defect Image Resize.(Image영역에 관하여.)								    //8)Defect Image Save.
						memcpy(DefectAreaInImage, &pTargetNode->m_pSearchRetList[WriteStep].StartX,  sizeof(RECT));

						//원본 Defect 좌표 이상유무 판독
						if( DefectAreaInImage.left == DefectAreaInImage.right || DefectAreaInImage.top == DefectAreaInImage.bottom)
							bSizeError = true;
						else
							bSizeError = false;
	#ifdef TEST_DEFECT_IMG_IN_ORG
						//Defect Image의 지정 Size로 변환가능 한지 검사 하고 Size조정을 한다.(Over Size Check.)
						if(G_CalcInImageDefPosAndSize(&GrabImageArea, &DefectAreaInImage, &pCoordObj->m_HWParam)/* == TRUE*/)
						{
							G_CalcDefImgAndMask_OnOrgImg(pTargetNode->m_pGrabOrgImage,
								ImageSavebuf,  
								&DefectAreaInImage, 
								pTargetNode->m_ppDefectImgList[WriteStep],
								pTargetNode->m_ScanLine,
								&pIDTCtrlQ->m_pCoordObj->m_HWParam, bSizeError);
						}
	#else
						if(G_CalcInImageDefPosAndSize(&PasteStartX, &PasteStartY, &GrabImageArea, &DefectAreaInImage, &pCoordObj->m_HWParam)/* == TRUE*/)
						{
							G_CalcDefImgAndMask_OnOrgImg(&PasteStartX, &PasteStartY, pTargetNode->m_pGrabOrgImage,
								ImageSavebuf,  
								&DefectAreaInImage, 
								pTargetNode->m_ppDefectImgList[WriteStep],
								pTargetNode->m_ScanLine,
								bSizeError);
						}
	#endif
						else
						{
							//strMessage.Format("좌표 없음 %d, %d, %d, %d", pDefecPos->left, pDefecPos->right, pDefecPos->top, pDefecPos->bottom);
							memset(pTargetNode->m_ppDefectImgList[WriteStep], 0xFF, ImageSavebuf->imageSize);
						}
					}
					if(pCoordObj->m_HWParam.m_SaveDefectOrgImage > 0 && pTargetNode->m_DefectListCnt>0)
					{//Defect가 있는 Image를 저장 할경우.
						//Type : 1->Active
						if(pUpdateState->bTEST_INSPECT_IMG == true)
							pInspectSaveImgQ->QPutNode(1, pTargetNode->m_ScanLine, pTargetNode->m_ScanImageIndex, pTargetNode->m_pGrabImage, &pCoodArea->m_ScanImgPos);
						else
							pInspectSaveImgQ->QPutNode(1, pTargetNode->m_ScanLine, pTargetNode->m_ScanImageIndex, pTargetNode->m_pGrabOrgImage, &pCoodArea->m_ScanImgPos);
					}
				}
			}
			

			//////////////////////////////////////////////////////////////////////////
			//정속 구간(앞뒤 3장의 Image는 버리고 Histogram값을 구한다.
			//if(pTargetNode->m_DefectListCnt == 0 )//Defect가 없을때만. ^^;;
			{
				GetImgHistogram(nullptr, pHistogramImg, 0.0);//Data None Proc Histogram
				if( pTargetNode->m_ScanImageIndex>=pUpdateState->nActiveHistCalcSkipIndex && 
					pTargetNode->m_ScanImageIndex<=(pCoordObj->m_OneLineScnaImgCnt-pUpdateState->nActiveHistCalcSkipIndex) && 
					pCoodArea != nullptr)
				{
					//////////////////////////////////////////////////////////////////////////
					//1) Active Cutting(원판 수정.)
					for(int ActiveStep =0; ActiveStep<pCoodArea->m_findSubRecCnt[0]; ActiveStep++)
					{
						if(HistogramInsertRect.IntersectRect(&HistogramCheckArea,  &pCoodArea->m_InImgPositionRect[0][ActiveStep]) == TRUE)
						{

						if(/*pCoodArea->m_InImgPositionRect[0][ActiveStep].Width() >= HistImgArea.width &&
							pCoodArea->m_InImgPositionRect[0][ActiveStep].Height()>= HistImgArea.height &&*/
							CalcHistonsertRect.IntersectRect( &HistogramInsertRect, &pCoodArea->m_InImgPositionRect[0][ActiveStep]) == TRUE)
							{
								if(CalcHistonsertRect.Width()>(HistImgArea.width+200) && 
									CalcHistonsertRect.Height()>(HistImgArea.height+200))
								{
									//설정영역의 중앙에서 잡는다.
									//HistImgArea.x	= pCoodArea->m_InImgPositionRect[0][ActiveStep].left + ((pCoodArea->m_InImgPositionRect[0][ActiveStep].Width()/2) - (HistImgArea.width/2));
									//HistImgArea.y	= pCoodArea->m_InImgPositionRect[0][ActiveStep].top  + ((pCoodArea->m_InImgPositionRect[0][ActiveStep].Height()/2) - (HistImgArea.height/2));

								HistImgArea.x	= CalcHistonsertRect.left + ((CalcHistonsertRect.Width()/2) - (HistImgArea.width/2));
								HistImgArea.y	= CalcHistonsertRect.top + ((CalcHistonsertRect.Height()/2) - (HistImgArea.height/2));
								
								//////////////////////////////////////////////////////////////////////////
								cvSetImageROI(pTargetNode->m_pGrabOrgImage, HistImgArea);
								cvCopyImage(pTargetNode->m_pGrabOrgImage, pAFORGImg);
								if(pUpdateState->bRealTimePixelValueSend == true)
									cvZero(pHistImgOrg);
								else
									cvCopyImage(pTargetNode->m_pGrabOrgImage, pHistImgOrg);
								cvResetImageROI(pTargetNode->m_pGrabOrgImage);
								//////////////////////////////////////////////////////////////////////////
								if(pUpdateState->bRealTimeLightValueSend == true && pUpdateState->bRealTimePixelValueSend == true)
								{
									GetImgPixelDeviation(pAFORGImg, pHistImgOrg, pCoordObj->m_InspectActiveParam.nScaleX1, pCoordObj->m_InspectActiveParam.nScaleY1 );
								}
								GetImgHistogram(pHistImgOrg, pHistogramImg,  
												(pUpdateState->dbLightingHistMaxPresent/100.0), 
												&pUpdateState->m_HistScanMaxValue, 
												&pUpdateState->m_Histogaram_Data[0],
												pUpdateState->bRealTimeLightValueSend,
												pUpdateState->bRealTimePixelValueSend);

								if(pUpdateState->bRealTimeLightValueSend == true)
								{
									pProcParam->m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 
																	nBiz_AOI_Inspection, 
																	nBiz_Recv_IllValueRealTime,
																	(USHORT)pUpdateState->m_HistScanMaxValue,
																	sizeof(float)*256,
																	(UCHAR*)&pUpdateState->m_Histogaram_Data[0]); 
								}

									if(pUpdateState->bTEST_SAVE_HISTO_ORG_IMG == true)
									{//Histogram Image Save Area 확인용.
										StartP	= cvPoint(HistImgArea.x,	HistImgArea.y);
										EndP		= cvPoint(HistImgArea.x+HistImgArea.width,	HistImgArea.y+HistImgArea.height);

										cvDrawRect(pTargetNode->m_pGrabOrgImage, StartP, EndP,CV_RGB(255,255,255),1);


										StartP	= cvPoint(CalcHistonsertRect.left,	CalcHistonsertRect.top);
										EndP		= cvPoint(CalcHistonsertRect.right,	CalcHistonsertRect.bottom);

										cvDrawRect(pTargetNode->m_pGrabOrgImage, StartP, EndP,CV_RGB(255,255,255),1);

										char NewName[256];
										sprintf_s(NewName, "D:\\Histo_OrgImg\\Org_%d_%d.jpg", pTargetNode->m_ScanLine, pTargetNode->m_ScanImageIndex);
										cvSaveImage(NewName,pTargetNode->m_pGrabOrgImage);
									}

									if(pUpdateState->bTEST_ACTIVE_IMG_HISTOGRAM_IMG_SAVE == true)
									{
										pHistogramImgQ->QPutNode(0, pTargetNode->m_ScanLine,pTargetNode->m_ScanImageIndex, pHistogramImg, nullptr);
										pHistogramImgQ->QPutNode(0, pTargetNode->m_ScanLine,pTargetNode->m_ScanImageIndex+1000, pHistImgOrg, nullptr);
									}

									//////////////////////////////////////////////////////////////////////////
									//AF Value 계산.
									//CalcAFValueSo((BYTE*)pAFORGImg->imageData, pAFORGImg->width, pAFORGImg->height, (BYTE*)pdestimage->imageData);
									//sumBuff =  cvSum(pdestimage);
									//pUpdateState->m_ActiveAFValue = sumBuff.val[0]/DivAFArea;
									break;
								}
							}
						}
					}
				}
				//EnterCriticalSection(&G_ViewCrtSection);
				ViewImage.CopyOf(pHistogramImg);
				ViewImage.DrawToHDC(*pUpdateState->m_HistpHDcView, &pUpdateState->m_HistViewRect);
				//LeaveCriticalSection(&G_ViewCrtSection);
			}	

			if(pUpdateState != nullptr)
			{//첫장은 오류, 1번에서 2번까지의 시간을 잰다.
				pUpdateState->m_RetScanLineNum[pProcParam->m_ProcIndex]		= pTargetNode->m_ScanLine;			
				pUpdateState->m_RetScanLineIndex[pProcParam->m_ProcIndex]	= pTargetNode->m_ScanImageIndex;		
				pUpdateState->m_RetDefectCnt[pProcParam->m_ProcIndex]		= pTargetNode->m_DefectListCnt;
				pUpdateState->m_nResultProcTick[pProcParam->m_ProcIndex]		= ::GetTickCount() - tickTickN;
			}
			//Result Process가 해당 Node에 대한 작업을 완료 하였다.
			pTargetNode->m_fnQSetProcessEnd(QDATA_PROC_END_RESULT);	

			if(pCoodArea != nullptr)
			{//Line Scan이 완료 되면 Pad부 Inspection 진입을 한다.
				if(pTargetNode->m_ScanImageIndex == pCoordObj->m_OneLineScnaImgCnt)
				{
					pPadProcObj->m_ScanLineNum		= pTargetNode->m_ScanLine;
					pPadProcObj->m_StatePADInspect  = PAD_INSPECTION_RUN;		
				}
			}
		}
		else
			Sleep(1);
	}

	
	cvReleaseImage(&pHistImgOrg);

	cvReleaseImage(&pAFORGImg);
	cvReleaseImage(&pdestimage);

	cvReleaseImage(&pHistogramImg);
	cvReleaseImage(&ImageSavebuf);
	ViewImage.Destroy();
	//DeleteCriticalSection(&G_ViewCrtSection);
	pProcParam->m_pResultThread = nullptr;
	return 0;
}
//////////////////////////////////////////////////////////////////////////
void G_CalcImgPosToCood(CoordinatesProc*pCoordObj, SCANIMGINFO* pCoodArea, DEFECT_DATA *m_pSearchRetList, COORD_DINFO *pCoordinateRetList)
{
	pCoordinateRetList->DefectType = m_pSearchRetList->DefectType;
	
	//Lens곡률에 따른 변화 적용.
	//Image Axis X의 중심을 기준으로 멀어질수록 곡률적용 한다.
	float LensCurvature = 0.0;
	//if(pCoordObj->m_GlassParam.m_ScanImgResolution_W < 3.0f)
	if(pCoordObj->m_ResolutionType ==  RESOLUTION_TYPE_1)
		LensCurvature = pCoordObj->m_HWParam.m_LensCurvature_Type1;
	else
		LensCurvature = pCoordObj->m_HWParam.m_LensCurvature_Type2;

	int ImgCenter = pCoordObj->m_HWParam.m_ProcImageW/2;

	float addDefectDisX = ((m_pSearchRetList->StartX+(abs(m_pSearchRetList->StartX-m_pSearchRetList->EndX)/2)) - ImgCenter)*LensCurvature;

	//if(ImgCenter>m_pSearchRetList->StartX)
	//	addDefectDisX *=(-1);

	//Macine 좌표계
	pCoordinateRetList->CoodStartX = pCoodArea->m_ScanImgPos.left + (int)((float)m_pSearchRetList->StartX*pCoordObj->m_GlassParam.m_ScanImgResolution_W+addDefectDisX);
	pCoordinateRetList->CoodStartY = pCoodArea->m_ScanImgPos.top + (int)((float)m_pSearchRetList->StartY*pCoordObj->m_GlassParam.m_ScanImgResolution_H);

	//원점 좌표계
	pCoordinateRetList->CoodStartX = pCoordinateRetList->CoodStartX - pCoordObj->m_GlassParam.CenterPointX;
	//pCoordinateRetList->CoodStartY = (pCoordinateRetList->CoodStartY - pCoordObj->m_GlassParam.CenterPointY)*(-1);
	pCoordinateRetList->CoodStartY = pCoordObj->m_GlassParam.CenterPointY - pCoordinateRetList->CoodStartY;

	//pCoordinateRetList->CoodWidth  =  (int)((float)abs(m_pSearchRetList->EndX - m_pSearchRetList->StartX)*pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	//pCoordinateRetList->CoodHeight =  (int)((float)abs(m_pSearchRetList->EndY - m_pSearchRetList->StartY)*pCoordObj->m_GlassParam.m_ScanImgResolution_H);
	pCoordinateRetList->CoodWidth  =  (int)((float)(abs(m_pSearchRetList->EndX - m_pSearchRetList->StartX)+1)*pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	pCoordinateRetList->CoodHeight =  (int)((float)(abs(m_pSearchRetList->EndY - m_pSearchRetList->StartY)+1)*pCoordObj->m_GlassParam.m_ScanImgResolution_H);
	

	pCoordinateRetList->nDefectCount	= m_pSearchRetList->nDefectCount;
	pCoordinateRetList->nDValueMin		= m_pSearchRetList->nDValueMin;
	pCoordinateRetList->nDValueMax		= m_pSearchRetList->nDValueMax;
	pCoordinateRetList->nDValueAvg		= m_pSearchRetList->nDValueAvg;
}
//////////////////////////////////////////////////////////////////////////
//Defect Image가 들어오면 Size 계산및 Image사의 좌표 계산 까지 한다.특정 Size에 맞게.
#ifdef TEST_DEFECT_IMG_IN_ORG
bool G_CalcInImageDefPosAndSize(CRect *pImagePos, CRect *pDefecPos, HW_PARAM *pHWParam)
{
	CRect CalcIn_InsertRect;
	CRect CalcIn_RetPosRect;

	if(pDefecPos->left == pDefecPos->right )
	{
		if(pDefecPos->left<2)
			pDefecPos->right = pDefecPos->left+2;
		else
			pDefecPos->left -=2;
	}
	if(pDefecPos->top == pDefecPos->bottom )
	{
		if(pDefecPos->top<2)
			pDefecPos->bottom = pDefecPos->top+2;
		else
			pDefecPos->top -=2;
	}

	if(CalcIn_InsertRect.IntersectRect(pImagePos, pDefecPos)==TRUE)
	{
		//Width 방향으로.
		if(CalcIn_InsertRect.Width()<pHWParam->m_DefectReviewSizeW)
		{
			//중간에서 작을때 포함.
			CalcIn_RetPosRect.left = (CalcIn_InsertRect.left+(CalcIn_InsertRect.Width()>>1/*/2*/)) - (pHWParam->m_DefectReviewSizeW/2);
			CalcIn_RetPosRect.right = CalcIn_RetPosRect.left+pHWParam->m_DefectReviewSizeW;

			if(CalcIn_RetPosRect.left <= pImagePos->left)//좌측.
			{
				CalcIn_RetPosRect.left	  = pImagePos->left;
				CalcIn_RetPosRect.right  = pHWParam->m_DefectReviewSizeW;
			}
			else if(CalcIn_RetPosRect.right >= pImagePos->right)//우측.
			{
				CalcIn_RetPosRect.left  = pImagePos->right - pHWParam->m_DefectReviewSizeW;
				CalcIn_RetPosRect.right = pImagePos->right;
			}

		}
		else//Union이 클때.
		{
			CalcIn_RetPosRect.left = (CalcIn_InsertRect.left+(CalcIn_InsertRect.Width()>>1/*/2*/)) - (pHWParam->m_DefectReviewSizeW/2);
			CalcIn_RetPosRect.right = CalcIn_RetPosRect.left+pHWParam->m_DefectReviewSizeW;
		}
		//Height 방향으로.
		if(CalcIn_InsertRect.Height()<pHWParam->m_DefectReviewSizeH)
		{
			//중간에서 작을때 포함.
			CalcIn_RetPosRect.top = (CalcIn_InsertRect.top+(CalcIn_InsertRect.Height()>>1/*/2*/)) - (pHWParam->m_DefectReviewSizeH/2);
			CalcIn_RetPosRect.bottom = CalcIn_RetPosRect.top+pHWParam->m_DefectReviewSizeH;

			if(CalcIn_RetPosRect.top <= pImagePos->top)//상단.
			{
				CalcIn_RetPosRect.top	  = pImagePos->top;
				CalcIn_RetPosRect.bottom  = pHWParam->m_DefectReviewSizeH;
			}
			else if(CalcIn_RetPosRect.bottom >= pImagePos->bottom)//하단.
			{
				CalcIn_RetPosRect.top  = pImagePos->bottom - pHWParam->m_DefectReviewSizeH;
				CalcIn_RetPosRect.bottom = pImagePos->bottom;
			}

		}
		else//Union이 클때.
		{
			CalcIn_RetPosRect.top = (CalcIn_InsertRect.top+(CalcIn_InsertRect.Height()>>1/*/2*/))  - (pHWParam->m_DefectReviewSizeH/2);
			CalcIn_RetPosRect.bottom = CalcIn_RetPosRect.top+pHWParam->m_DefectReviewSizeH;
		}
	}
	else
	{	
		//CString strMessage;

		//strMessage.Format("좌표 없음 %d, %d, %d, %d", pDefecPos->left, pDefecPos->right, pDefecPos->top, pDefecPos->bottom);
		//AfxMessageBox(strMessage);

		//심각한 오류.(이미지위에 좌표 없음.)
		memset(&CalcIn_RetPosRect, 0x00, sizeof(CRect));
		return false;
	}
	if(CalcIn_RetPosRect.Width()!= pHWParam->m_DefectReviewSizeW ||CalcIn_RetPosRect.Height()!= pHWParam->m_DefectReviewSizeH)
	{
		//CString strMessage;

		//strMessage.Format("Size가 크다 %d, %d, %d, %d", CalcIn_RetPosRect.Width(),pHWParam->m_DefectReviewSizeW, CalcIn_RetPosRect.Height(),  pHWParam->m_DefectReviewSizeH);
		//AfxMessageBox(strMessage);
		return false;
	}

	memcpy(pDefecPos, CalcIn_RetPosRect, sizeof(CRect));
	return true;
}
void G_CalcDefImgAndMask_OnOrgImg(IplImage *pImageBuffer, IplImage *pSaveImageBuffer,CRect* pDefecArea, BYTE *pRetImgBuf, int ScanLineNum, HW_PARAM *pHWParam, bool bSizeError)
{
	CvRect Calc_CutImgArea;

	Calc_CutImgArea.x			= pDefecArea->left;
	Calc_CutImgArea.y			= pDefecArea->top;
	Calc_CutImgArea.width		= pDefecArea->Width();
	Calc_CutImgArea.height	    = pDefecArea->Height();

	cvSetImageROI(pImageBuffer, Calc_CutImgArea);
	cvCopy(pImageBuffer, pSaveImageBuffer);

	//if(bSizeError == true)//Size Zero가 들어 왔던 Defect에 대해서는 Result를 임시로 반전 시키다.(Debugging용이다.)
	//	cvNot(pSaveImageBuffer,pSaveImageBuffer);
	memcpy(pRetImgBuf, pSaveImageBuffer->imageData, pHWParam->m_DefectReviewSizeW*pHWParam->m_DefectReviewSizeH);
	cvResetImageROI(pImageBuffer);
}
#else
bool G_CalcInImageDefPosAndSize(int *pPasteStartX, int *pPasteStartY,CRect *pImagePos, CRect *pDefecPos, HW_PARAM *pHWParam)
{
	CRect CalcIn_InsertRect;
	CRect CalcIn_RetPosRect;

	if(pDefecPos->left == pDefecPos->right )
	{
		if(pDefecPos->left<2)
			pDefecPos->right = pDefecPos->left+2;
		else
			pDefecPos->left -=2;
	}
	if(pDefecPos->top == pDefecPos->bottom )
	{
		if(pDefecPos->top<2)
			pDefecPos->bottom = pDefecPos->top+2;
		else
			pDefecPos->top -=2;
	}

	if(CalcIn_InsertRect.IntersectRect(pImagePos, pDefecPos)==TRUE)
	{

		int DefectCX = pDefecPos->left + pDefecPos->Width()/2;
		int DefectCY = pDefecPos->top + pDefecPos->Height()/2;
		CRect OrgCpyArea;
		OrgCpyArea.left		= DefectCX-(pHWParam->m_DefectReviewSizeW/2);
		OrgCpyArea.top		= DefectCY-(pHWParam->m_DefectReviewSizeH/2);
		OrgCpyArea.right		= OrgCpyArea.left +pHWParam->m_DefectReviewSizeW;
		OrgCpyArea.bottom	= OrgCpyArea.top +pHWParam->m_DefectReviewSizeH;

		CalcIn_RetPosRect.IntersectRect(pImagePos, &OrgCpyArea);//원판에서 자를 영역.

		*pPasteStartX = abs(OrgCpyArea.Width() - CalcIn_RetPosRect.Width());
		*pPasteStartY = abs(OrgCpyArea.Height() - CalcIn_RetPosRect.Height());
		if(pImagePos->right<OrgCpyArea.right)
			*pPasteStartX = 0;
		if(pImagePos->bottom<OrgCpyArea.bottom)
			*pPasteStartY = 0;
	}
	else
	{	
		//심각한 오류.(이미지위에 좌표 없음.)
		memset(&CalcIn_RetPosRect, 0x00, sizeof(CRect));
		return false;
	}

	memcpy(pDefecPos, CalcIn_RetPosRect, sizeof(CRect));
	return true;
}

//////////////////////////////////////////////////////////////////////////
//G_CalcDefImgAndMask_OnOrgImg함수 내의 재정의를 제거 하기위해.(Speed Up)



void G_CalcDefImgAndMask_OnOrgImg(int *pPasteStartX, int *pPasteStartY, IplImage *pImageBuffer, IplImage *pSaveImageBuffer, CRect* pDefecArea, BYTE *pRetImgBuf, int ScanLineNum, bool bSizeError)
{
	CvRect Calc_OrgArea;
	CvRect Calc_CutImgArea;
	Calc_CutImgArea.x			= pDefecArea->left;
	Calc_CutImgArea.y			= pDefecArea->top;
	Calc_CutImgArea.width		= pDefecArea->Width();
	Calc_CutImgArea.height	    = pDefecArea->Height();

	Calc_OrgArea = Calc_CutImgArea;
	Calc_OrgArea.x = *pPasteStartX;
	Calc_OrgArea.y = *pPasteStartY;

	cvSetImageROI(pImageBuffer, Calc_CutImgArea);
	cvSetImageROI(pSaveImageBuffer, Calc_OrgArea);
	cvCopy(pImageBuffer, pSaveImageBuffer);
	cvResetImageROI(pImageBuffer);
	cvResetImageROI(pSaveImageBuffer);

	//if(bSizeError == true)//Size Zero가 들어 왔던 Defect에 대해서는 Result를 임시로 반전 시키다.(Debugging용이다.)
	//	cvNot(pSaveImageBuffer,pSaveImageBuffer);

	memcpy(pRetImgBuf, pSaveImageBuffer->imageData, pSaveImageBuffer->imageSize);

	memset(pSaveImageBuffer->imageData, 0xAF, pSaveImageBuffer->imageSize);
}
#endif


int IndexValue = 0;
void GetImgPixelDeviation(IplImage* simg, IplImage* Bufimg, int RepeatCycleX, int RepeatCycleY)
{
	BYTE *pSrcData = (BYTE *)simg->imageData;
	BYTE *pBufData = (BYTE *)Bufimg->imageData;

	BYTE srcValue =0, targeValue[4] ={0,0,0,0};

	BYTE MaxValue = 0;
	for(int HStep =0; HStep<simg->height; HStep++)
	{
		for(int WStep =0; WStep<simg->width; WStep++)
		{
			srcValue = *(pSrcData+WStep+(HStep*simg->width));//Org Value

			int TValuePos = (HStep+RepeatCycleY);
			if(TValuePos>simg->height-1)//아래쪽에 없으면 위쪽으로.
				TValuePos = (HStep-RepeatCycleY);

			targeValue[0] = *(pSrcData+WStep+(TValuePos*simg->width));//Y하단.


			//TValuePos = (HStep-RepeatCycleY);
			//if(TValuePos<0)
			//	TValuePos = (HStep+RepeatCycleY*2);
			//targeValue[1] = *(pSrcData+WStep+(TValuePos*simg->width));//Y상단.


			//TValuePos = (WStep-RepeatCycleX);
			//if(TValuePos<0)
			//	TValuePos = (WStep+RepeatCycleX*2);
			//targeValue[2] = *(pSrcData+TValuePos+(HStep*simg->width));//X좌측.


			//TValuePos = (WStep+RepeatCycleX);
			//if(TValuePos>simg->width-1)
			//	TValuePos = (WStep-RepeatCycleX);
			//targeValue[3] = *(pSrcData+TValuePos+(HStep*simg->width));//X우측.

			//for(int MaxStep = 0; MaxStep<4; MaxStep++)
			//{
			//	targeValue[MaxStep] = abs(srcValue - targeValue[MaxStep]);
			//}

			//for(int MaxStep = 1; MaxStep<4; MaxStep++)
			//{
			//	if(targeValue[0]<targeValue[MaxStep])
			//		targeValue[0]= targeValue[MaxStep];
			//}

			*(pBufData+WStep+(HStep*simg->width)) = abs(srcValue - targeValue[0]);//targeValue[0]+ targeValue[1]+ targeValue[2]+ targeValue[3];

			if(MaxValue<*(pBufData+WStep+(HStep*simg->width)))
				MaxValue = *(pBufData+WStep+(HStep*simg->width));
		}
	}
	////////////////////////////////////////////////////////////////////////
	//char SavePath[256];
	//sprintf_s(SavePath, "D:\\PD_%Index_%05d_Max%03d.bmp", IndexValue++, MaxValue);
	//cvSaveImage(SavePath, Bufimg);
	//sprintf_s(SavePath, "D:\\PD_%Index_%05d_org.bmp", IndexValue++);
	//cvSaveImage(SavePath, simg);
}

void GetImgHistogram(IplImage* simg, IplImage* hist_img,  double HistValue,  int *pScanMaxValue, float *pLowHistData, bool bSendRealTime, bool bPixelDeviation)
{
	char s_text[50];
	cvZero(hist_img);
	if(simg == nullptr || pScanMaxValue == nullptr)
	{
		sprintf_s( s_text, "Active Image None !!!");
		cvPutText( hist_img, s_text, cvPoint(hist_img->width/2-70, hist_img->height/2), &G_Histfont, CV_RGB(0,255,255));
		return;
	}
	CvHistogram* hist;
	//float max_value = 0;
	float hist_val = 0;
	float intensity = 0.0;

	int axis_point = 150;
	int hist_size[] = {255};

	float ranges[] = { 0, 255 }; 
	float *hist_range[] = {ranges};
	hist =cvCreateHist( 1, hist_size, CV_HIST_UNIFORM);//, 0, 1);
	//hist = cvCreateHist(1, hist_size, CV_HIST_ARRAY, hist_range, 1); 
	//hist = cvCreateHist(1, hist_size, CV_HIST_SPARSE, hist_range, 1); 
	
	cvCalcHist( &simg, hist, 0, 0);

	float max_bin_value = 0;
	float min_bin_value = 0;
	int max_level = 0;
	int min_level = 0;
	cvGetMinMaxHistValue( hist, &min_bin_value, &max_bin_value, &min_level, &max_level);

	int MaxIntensity = -1;
	int MinValue = -1;
	int MaxValue = -1;
	float Histogaram_Data[256];
	for ( int i = 0; i < 255; i++)
	{
		hist_val  = cvQueryHistValue_1D(hist,i);
		intensity = /*cvRound*/( hist_val*255/max_bin_value);
		cvLine(hist_img, cvPoint(i+20, axis_point), cvPoint(i+20, abs(axis_point - cvRound(intensity)) ), CV_RGB(255,255,0), 1);
		//if(intensity>0 && HistValue < (hist_val/max_bin_value)*100)
		//if(intensity>0.0&& (HistValue < (hist_val/max_bin_value)*100 || 	bPixelDeviation == true))
		//{
		//	MaxValue = i;
		//	MaxIntensity =  cvRound(intensity);
		//}
		if(intensity>0 && MinValue == -1)
			MinValue = i;

		Histogaram_Data[i] = hist_val;
	}

	////////////////////////////////////////////////////////////////////////////
	//int nLevelCount[256];
	//for(int nStepY = 0; nStepY < hist_img->height; nStepY++)          
	//{
	//	for(int nStepX = 0; nStepX < hist_img->width; nStepX++)
	//	{                               
	//		nLevelCount[(BYTE)hist_img->imageData[(hist_img->width) * nStepY + (nStepX)] ]++;
	//	}
	//}

	static int nMaxCount = simg->width * simg->height;
	unsigned int nSumCount = 0;
	unsigned int nSumValue = 0;
	unsigned int nPerMax = (int)((double)nMaxCount * HistValue);
	for(int nStep = 254; nStep >= 0; nStep--)
	{
		nSumCount += (unsigned int)Histogaram_Data[nStep];
		nSumValue += ((unsigned int)Histogaram_Data[nStep] * nStep);

		if( nSumCount >= nPerMax)
			break;
	}
	MaxValue =(int)((double)nSumValue / (double)nSumCount);
	//////////////////////////////////////////////////////////////////////////

	cvLine(hist_img, cvPoint(20, (axis_point - MaxIntensity)), cvPoint(275, (axis_point - MaxIntensity)), CV_RGB(255,0,255), 1);
	cvLine(hist_img, cvPoint(20+MaxValue, 30), cvPoint(20+MaxValue, axis_point), CV_RGB(255,0,255), 1);

	cvLine(hist_img, cvPoint(20, axis_point+3), cvPoint(275, axis_point+3), CV_RGB(0,255,0), 2);

	cvLine(hist_img, cvPoint(20, 0), cvPoint(20, axis_point), CV_RGB(255,0,0), 1);
	cvLine(hist_img, cvPoint(275, 0), cvPoint(275, axis_point), CV_RGB(255,0,0), 1);

	sprintf_s( s_text, "%d", MaxValue);
	cvPutText( hist_img, s_text, cvPoint(MaxValue+15, axis_point+15), &G_Histfont, CV_RGB(255,0,255));

	sprintf_s( s_text, "%d", MinValue);
	cvPutText( hist_img, s_text, cvPoint(MinValue+15, axis_point+15), &G_Histfont, CV_RGB(255,0,255));

	if(pScanMaxValue!=nullptr)
	{
		if(bSendRealTime == true)
		{
			*pScanMaxValue = MaxValue;
			memcpy(pLowHistData, Histogaram_Data, sizeof(float)*256);
		}
		else
		{
			if(*pScanMaxValue<MaxValue && pLowHistData != nullptr)
			{
				*pScanMaxValue = MaxValue;
				memcpy(pLowHistData, Histogaram_Data, sizeof(float)*256);
			}
		}
	}

	if(bPixelDeviation== true)
		sprintf_s( s_text, "Pixel Deviation Histogram");
	else
		sprintf_s( s_text, "Active Image Histogram");
	cvPutText( hist_img, s_text, cvPoint(70, 10), &G_Histfont, CV_RGB(0,255,255));

	sprintf_s( s_text, "Max Value:%3d", *pScanMaxValue);
	cvPutText( hist_img, s_text, cvPoint(180, 30), &G_Histfont, CV_RGB(255,0,0));

	cvReleaseHist(&hist);
}