
// VS11AOII.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "VS11AOII.h"
#include "VS11AOIIDlg.h"
#include "HWParamInterface.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CVS11AOIIApp

BEGIN_MESSAGE_MAP(CVS11AOIIApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CVS11AOIIApp 생성

CVS11AOIIApp::CVS11AOIIApp()
{
	// 다시 시작 관리자 지원
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}
// 유일한 CVS11AOIIApp 개체입니다.

CVS11AOIIApp theApp;

CInterServerInterface	G_ServerInterface;
//VSB Controller
AOI_ImageCtrlQ			G_InspectionDataQ;
PAD_ImageCtrlQ			G_PadInspectDataQ[4];//상, 하, 좌,우

//Graber Controller
CGraberCtrl				G_ImgGrabberCtrl;
LocalImgGrab			G_FileGrabObj;
AOIBigImg				G_GrabBigger;

//Etc Controller
PROC_STATE				G_ProcessState;
PROC_PADCUT				G_CutProcHendle[PADCUT_PROC_CNT];
PROC_PAD				G_PADProcHendle;

DefectDataQ				G_InspectDefectQ;// Defect 검출 Data가 적제 된다.
ImageSaveQ				G_InspectSaveImgQ(SAVE_ORG_IMG_MAX);// Defect 검출 Data가 적제 된다.

PROC_RESULT				G_RetProcHendle[RET_PROC_CNT];
PROC_RETSORT			G_SortProcHendle;

CDefectFileFormat		G_WriteRetObj;

CoordinatesProc			G_CoordProcObj;


PROC_RETWRITE			G_WriteProcHendle[WRE_PROC_CNT];


ImageSaveQ				G_AlignImgQ(SAVE_ORG_IMG_MAX);// Defect 검출 Data가 적제 된다.

ImageSaveQ				G_HistogramImgQ(SAVE_ORG_IMG_MAX);
//PROC_RETWRITE			G_WriteFileProcHendle;
//PROC_RETWRITE			G_WriteImgProcHendle;


//PROC_PAD_ALIGN			G_ProcPadAlign;
// CVS11AOIIApp 초기화

BOOL CVS11AOIIApp::InitInstance()
{
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();


	AfxEnableControlContainer();

	// 대화 상자에 셸 트리 뷰 또는
	// 셸 목록 뷰 컨트롤이 포함되어 있는 경우 셸 관리자를 만듭니다.
	CShellManager *pShellManager = new CShellManager;

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("로컬 응용 프로그램 마법사에서 생성된 응용 프로그램"));
	//////////////////////////////////////////////////////////////////////////
	//0) HW Parameter Load
	bool RetParam = true;
	if(G_AOI_fnLoadHWParam(&G_CoordProcObj) == false)
	{
		AfxMessageBox("HW Parameter Load Error");
		RetParam= false;
	}
	////////////////////////////////////////////////////////////////////////////
	////1) Cycle Inspect Queue Init
	G_InspectionDataQ.m_pUpdateState	= &G_ProcessState;
	G_InspectionDataQ.m_pCoordObj			= &G_CoordProcObj;
	G_InspectionDataQ.m_fnQCreateCycleQ(G_CoordProcObj.m_HWParam.m_VSB_Size);

	////////////////////////////////////////////////////////////////////////////
	////2) Grabber Process Init
#ifndef TEST_INSPECTION_LOAD_FILE
	G_ImgGrabberCtrl.m_pUpdateState		= &G_ProcessState;
#ifdef DALSA_LINE_SCAN_GRABBER

	{
		if(1 < __argc)
		{
			int nInputTaskNum = atoi(__argv[1]);
			char *pLinkPath = NULL;

			char path_buffer[_MAX_PATH];
			char chFilePath[_MAX_PATH] = {0,};
			char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
			//실행 파일 이름을 포함한 Full path 가 얻어진다.
			::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
			//디렉토리만 구해낸다.
			_splitpath_s(path_buffer, drive, dir, fname, ext);
			strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
			strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

			switch(nInputTaskNum)
			{
			case 21: 	pLinkPath = HW_PARAM_PATH_TN21; break;
			//case 12: 	pLinkPath = HW_PARAM_PATH_TN12; break;
			//case 13: 	pLinkPath = HW_PARAM_PATH_TN13; break;
			//case 14: 	pLinkPath = HW_PARAM_PATH_TN14; break;
			default:
				return false;
			}
			strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);
			char ReadPath[_MAX_PATH];
			memset(ReadPath, 0x00, _MAX_PATH);
			GetPrivateProfileString(Section_HWINFO, Key_DALSA_CCF_PATH, "T_HS-S0-12K40-00-R_Line_Master.ccf",  ReadPath, _MAX_PATH, chFilePath);
			char CCFFileFullPath[_MAX_PATH];
			memset(CCFFileFullPath,0x00, _MAX_PATH);
			sprintf_s(CCFFileFullPath, "%s%s", DALAS_CAM_CCF_PATH,ReadPath);
			G_ImgGrabberCtrl.initGrabber(CCFFileFullPath,&G_InspectionDataQ );
		}
		else
		{
			AfxMessageBox("Line Scan Camera Info File Load Error.... Check Plz...");
			return FALSE;
		}
	}
#else
	G_ImgGrabberCtrl.initGrabber(&G_InspectionDataQ );
#endif
#else
	G_FileGrabObj.m_pUpdateState			= &G_ProcessState;
	G_FileGrabObj.initImgGrab(&G_InspectionDataQ);
#endif
	//Pad Area Index Setting
	G_PadInspectDataQ[0].m_PadIndex	= 0;
	G_PadInspectDataQ[1].m_PadIndex	= 1;
	G_PadInspectDataQ[2].m_PadIndex	= 2;
	G_PadInspectDataQ[3].m_PadIndex	= 3;
	//////////////////////////////////////////////////////////////////////////
	//3) Cutter Process Run
	for(int CutProStep = 0; CutProStep<PADCUT_PROC_CNT; CutProStep++)
	{
		G_CutProcHendle[CutProStep].m_bProcessRun		= true;
		G_CutProcHendle[CutProStep].m_ProcIndex			= CutProStep;
		G_CutProcHendle[CutProStep].m_pUpdateState		= &G_ProcessState;
		G_CutProcHendle[CutProStep].m_pIDTCtrlQ			= &G_InspectionDataQ;
		G_CutProcHendle[CutProStep].m_pPADCtrlQ			= &G_PadInspectDataQ[0];
		G_CutProcHendle[CutProStep].m_pCoordProcObj		= &G_CoordProcObj;
		G_CutProcHendle[CutProStep].m_pResultThread		= ::AfxBeginThread(fn_OrgImgCutterProcess, &G_CutProcHendle[CutProStep], THREAD_PRIORITY_NORMAL);
	}
	G_PADProcHendle.m_bProcessRun		= true;
	G_PADProcHendle.m_pUpdateState		= &G_ProcessState;
	G_PADProcHendle.m_pPADCtrlQ			= &G_PadInspectDataQ[0];
	G_PADProcHendle.m_pCoordProcObj		= &G_CoordProcObj;
	G_PADProcHendle.m_pServerInterface	= &G_ServerInterface;
	G_PADProcHendle.m_pResultDefectQ	= &G_InspectDefectQ;
	G_PADProcHendle.m_pInspectSaveImgQ	= &G_InspectSaveImgQ;
	G_PADProcHendle.m_pResultThread		= ::AfxBeginThread(fn_PadInspectProcess, &G_PADProcHendle, THREAD_PRIORITY_NORMAL);//THREAD_PRIORITY_HIGHEST);
	
	//4) Result Process Run
	for(int RetProStep = 0; RetProStep<RET_PROC_CNT; RetProStep++)
	{
		G_RetProcHendle[RetProStep].m_bProcessRun		= true;
		G_RetProcHendle[RetProStep].m_ProcIndex			= RetProStep;
		G_RetProcHendle[RetProStep].m_pUpdateState		= &G_ProcessState;
		G_RetProcHendle[RetProStep].m_pIDTCtrlQ			= &G_InspectionDataQ;
		G_RetProcHendle[RetProStep].m_pPADCtrlQ			= &G_PadInspectDataQ[0];
		G_RetProcHendle[RetProStep].m_pCoordProcObj		= &G_CoordProcObj;
		G_RetProcHendle[RetProStep].m_pServerInterface	= &G_ServerInterface;
		G_RetProcHendle[RetProStep].m_pPadProcObj		= &G_PADProcHendle;

		G_RetProcHendle[RetProStep].m_pHistogramImgQ	= &G_HistogramImgQ;
		G_RetProcHendle[RetProStep].m_pInspectSaveImgQ	= &G_InspectSaveImgQ;

		G_RetProcHendle[RetProStep].m_pResultThread		= ::AfxBeginThread(fn_ResultProcess, &G_RetProcHendle[RetProStep], THREAD_PRIORITY_NORMAL);//THREAD_PRIORITY_HIGHEST);
	}
	//////////////////////////////////////////////////////////////////////////
	//4) Write Result Process Run
	G_SortProcHendle.m_bProcessRun					= true;
	G_SortProcHendle.m_pUpdateState					= &G_ProcessState;
	G_SortProcHendle.m_pIDTCtrlQ						= &G_InspectionDataQ;
	G_SortProcHendle.m_pGrabberCtrl					= &G_ImgGrabberCtrl;
	G_SortProcHendle.m_pServerInterface				= &G_ServerInterface;
	G_SortProcHendle.m_pPadProcObj					= &G_PADProcHendle;
	G_SortProcHendle.m_pResultDefectQ				= &G_InspectDefectQ;
	
	G_SortProcHendle.m_pResultThread				= ::AfxBeginThread(fn_SortResultProcess, &G_SortProcHendle, THREAD_PRIORITY_NORMAL);//THREAD_PRIORITY_HIGHEST);
	//////////////////////////////////////////////////////////////////////////


	for(int WPStep = 0; WPStep<WRE_PROC_CNT; WPStep++)
	{
		G_WriteProcHendle[WPStep].m_bProcessRun				= true;
		G_WriteProcHendle[WPStep].m_ProcessNum				= WPStep%2 == 0 ? 2 : 1;//1 Defect Write Thread 2: Defect ORG Thread
		G_WriteProcHendle[WPStep].m_ReviewImgW				= G_CoordProcObj.m_HWParam.m_DefectReviewSizeW ;
		G_WriteProcHendle[WPStep].m_ReviewImgH				= G_CoordProcObj.m_HWParam.m_DefectReviewSizeH ;
		G_WriteProcHendle[WPStep].m_pWriteFileObj			= &G_WriteRetObj;
		G_WriteProcHendle[WPStep].m_pResultDefectQ			= &G_InspectDefectQ;
		G_WriteProcHendle[WPStep].m_pHistogramImgQ			= &G_HistogramImgQ;
		G_WriteProcHendle[WPStep].m_pInspectSaveImgQ		= &G_InspectSaveImgQ;
		G_WriteProcHendle[WPStep].m_pServerInterface		= &G_ServerInterface;
		G_WriteProcHendle[WPStep].m_pUpdateState			= &G_ProcessState;
		G_WriteProcHendle[WPStep].m_pCoordObj				= &G_CoordProcObj;
		G_WriteProcHendle[WPStep].m_pResultThread			= ::AfxBeginThread(fn_WriteFileProcess, &G_WriteProcHendle[WPStep], THREAD_PRIORITY_NORMAL);//THREAD_PRIORITY_HIGHEST);//THREAD_PRIORITY_NORMAL);
	}

	//////////////////////////////////////////////////////////////////////////
	_AOI_Act_Defect_Inspection_Start( AOI_DLL_PROCESS_CNT, &G_ProcessState.m_ProcStateInspector[0]);
	_AOI_Pad_Defect_Inspection_Start(AOI_DLL_PROCESS_CNT, &G_ProcessState.m_ProcStateInspector[5]);
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	CVS11AOIIDlg dlg;
	m_pMainWnd = &dlg;
	if(RetParam == true)
	{

		INT_PTR nResponse = dlg.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
			//  코드를 배치합니다.
		}
		else if (nResponse == IDCANCEL)
		{
			// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
			//  코드를 배치합니다.
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	Sleep(100);
	_AOI_Act_Defect_Inspection_Stop();
	Sleep(100);
	_AOI_Pad_Defect_Inspection_Stop();
	Sleep(100);
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//_AOI_Act_Defect_Inspection_Start();
	//Sleep(500);
	//////////////////////////////////////////////////////////////////////////
	//4) Kill Update Timer
	KillTimer(dlg.m_hWnd, UI_UPDATE_PROC_STATE);
	KillTimer(dlg.m_hWnd, UI_UPDATE_STATE_AOI_TASK);
	
	for(int CutProStep = 0; CutProStep<PADCUT_PROC_CNT; CutProStep++)
	{
		G_CutProcHendle[CutProStep].m_bProcessRun		= false;
	}

	//////////////////////////////////////////////////////////////////////////
	//4) Result Process 정리.
	for(int RetProStep = 0; RetProStep<RET_PROC_CNT; RetProStep++)
	{
		G_RetProcHendle[RetProStep].m_bProcessRun		=  false;
	}

	
	//////////////////////////////////////////////////////////////////////////
	//5) Write Result Process 정리.
	G_PADProcHendle.m_bProcessRun						= false;
	G_SortProcHendle.m_bProcessRun						= false;

	for(int WProStep = 0; WProStep<WRE_PROC_CNT; WProStep++)
	{
		G_WriteProcHendle[WProStep].m_bProcessRun		=  false;
	}
	Sleep( 500);
	//////////////////////////////////////////////////////////////////////////
	//5) Memory 정리 작업
	G_ImgGrabberCtrl.exitGrabber();
	G_FileGrabObj.exitImgGrab();
	G_InspectionDataQ.m_fnQDeleteAll();
//	cvDestroyAllWindows();
	// 위에서 만든 셸 관리자를 삭제합니다.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

/*
*	Module Name		:	AOI_fnLoadHWParam
*	Parameter		:	 없음
*	Return			:	없음
*	Function		:	HW관련 Parameter를 Load한다.
*	Create			:	2012.02.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool G_AOI_fnLoadHWParam(CoordinatesProc *pCoordProcObj, int LensTyep)
{
	int nInputTaskNum = atoi(__argv[1]);
	char *pLinkPath = NULL;

	char path_buffer[_MAX_PATH];
	char chFilePath[_MAX_PATH] = {0,};
	char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
	//실행 파일 이름을 포함한 Full path 가 얻어진다.
	::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
	//디렉토리만 구해낸다.
	_splitpath_s(path_buffer, drive, dir, fname, ext);
	strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
	strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

	switch(nInputTaskNum)
	{
	case 21: 	pLinkPath = HW_PARAM_PATH_TN21; break;
	//case 11: 	pLinkPath = HW_PARAM_PATH_TN11; break;
	//case 12: 	pLinkPath = HW_PARAM_PATH_TN12; break;
	//case 13: 	pLinkPath = HW_PARAM_PATH_TN13; break;
	//case 14: 	pLinkPath = HW_PARAM_PATH_TN14; break;
	default:
		return false;
	}

	strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

	pCoordProcObj->m_HWParam.m_VSB_Size					= GetPrivateProfileInt(Section_HWINFO,Key_VSB_Size, 0, chFilePath);

	pCoordProcObj->m_HWParam.m_ProcImageW				= GetPrivateProfileInt(Section_HWINFO,Key_GrabImageW, 0, chFilePath);
	pCoordProcObj->m_HWParam.m_ProcImageH				= GetPrivateProfileInt(Section_HWINFO,Key_GrabImageH, 0, chFilePath);

	pCoordProcObj->m_HWParam.m_DefectMaxCntInImg		= GetPrivateProfileInt(Section_HWINFO,Key_DefectMaxCntInImg, 0, chFilePath);
	pCoordProcObj->m_HWParam.m_DefectReviewSizeW		= GetPrivateProfileInt(Section_HWINFO,Key_DefectReviewSizeW, 0, chFilePath);
	pCoordProcObj->m_HWParam.m_DefectReviewSizeH		= GetPrivateProfileInt(Section_HWINFO,Key_DefectReviewSizeH, 0, chFilePath);


	pCoordProcObj->m_HWParam.m_INSPECTION_CAM_COUNT		= GetPrivateProfileInt(Section_HWINFO,Key_INSPECTION_CAM_COUNT, 0, chFilePath);
	pCoordProcObj->m_HWParam.m_INSPECTION_SCAN_WIDTH		= GetPrivateProfileInt(Section_HWINFO,Key_INSPECTION_SCAN_WIDTH, 0, chFilePath);
	pCoordProcObj->m_HWParam.m_INSPECTION_SCAN_HEIGHT		= GetPrivateProfileInt(Section_HWINFO,Key_INSPECTION_SCAN_HEIGHT, 0, chFilePath);

	pCoordProcObj->m_HWParam.m_AOI_BLOCK_SCAN_START_X		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_BLOCK_SCAN_START_X, 0, chFilePath);
	pCoordProcObj->m_HWParam.m_AOI_BLOCK_SCAN_START_Y		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_BLOCK_SCAN_START_Y, 0, chFilePath);

	//pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X, 0, chFilePath);
	//pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y, 0, chFilePath);

	//pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD, 0, chFilePath);
	//pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN, 0, chFilePath);
	//Lens Type 추가.(20120726) Start
	switch(LensTyep)
	{
	case 1:
		{
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE1, 0, chFilePath);
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1, 0, chFilePath);
			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1, 0, chFilePath);
		}break;
	case 2:
		{
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
			if(pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X == 0xFFFFFFFF)
				pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
			if(pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y == 0xFFFFFFFF)
				pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
			if(pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD	 == 0xFFFFFFFF)
				pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
			if(pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN == 0xFFFFFFFF)
				pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN	= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1, 0, chFilePath);
		}break;
	default:
		{
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE1, 0, chFilePath);
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1, 0, chFilePath);
			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1, 0, chFilePath);
		}break;
	}
	//Lens Type 추가.(20120726) End

	pCoordProcObj->m_HWParam.m_OnlyGrabImageSave 		= GetPrivateProfileInt(Section_HWINFO,Key_OnlyGrabImageSave, 0, chFilePath);
	pCoordProcObj->m_HWParam.m_SaveDefectOrgImage		= GetPrivateProfileInt(Section_HWINFO,Key_SaveDefectOrgImage, 0, chFilePath);
	pCoordProcObj->m_HWParam.DefectSaveFileType			= GetPrivateProfileInt(Section_HWINFO,Key_DefectSaveFileType, 0, chFilePath);

	pCoordProcObj->m_HWParam.PadInspectAreaType			= GetPrivateProfileInt(Section_HWINFO,Key_PadInspectAreaType, 0, chFilePath);

	pCoordProcObj->m_HWParam.CamScanDirectionPort			= GetPrivateProfileInt(Section_HWINFO,Key_SERIAL_PORT_CAM_DERCTION, 1, chFilePath);


	char ReadData[512];
	memset(ReadData, 0x00, 512);
	GetPrivateProfileString(Section_HWINFO, Key_LENS_CURVATURE_TYPE1, "0.0",  ReadData, 511, chFilePath);
	pCoordProcObj->m_HWParam.m_LensCurvature_Type1 = (float)atof(ReadData);
	memset(ReadData, 0x00, 512);
	GetPrivateProfileString(Section_HWINFO, Key_LENS_CURVATURE_TYPE2, "0.0",  ReadData, 511, chFilePath);
	pCoordProcObj->m_HWParam.m_LensCurvature_Type2 = (float)atof(ReadData);


	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_RESULT_SAVE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	sprintf_s(pCoordProcObj->m_HWParam.strResultPath,"%s",  ReadData);
	

	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_IMAGE_SAVE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	sprintf_s(pCoordProcObj->m_HWParam.strOrgImgPath,"%s",  ReadData);


	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_IMAGE_UPDATE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;

	if(pCoordProcObj->m_ImageUpdatePath.GetLength()<= strlen(ReadData))
		pCoordProcObj->m_ImageUpdatePath.Format("%s",  ReadData);

	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_RESULT_UPDATE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	pCoordProcObj->m_ResultUpdatePath.Format("%s",  ReadData);


	pCoordProcObj->m_AXIS_Y_OVER_STEP			= GetPrivateProfileInt(Section_HWINFO, Key_AXIS_Y_OVERLAP, 300, chFilePath);

	G_AOI_fnLoadInspectParam(pCoordProcObj, nInputTaskNum);
	//////////////////////////////////////////////////////////////////////////
	




	return true;
}

/*
*	Module Name		:	G_AOI_fnReloadHWParamByLens
*	Parameter		:	 없음
*	Return			:	없음
*	Function		:	H/W Param에서 Lens에 관련된 Option Data를 Update한다.
*	Create			:	2012.07.26
*	Author			:	최지형
*	Version			:	1.0
*/
//Lens Type 추가.(20120726) Start
bool G_AOI_fnReloadHWParamByLens(int TaskNumber, CoordinatesProc *pCoordProcObj, int LensTyep)
{
	char *pLinkPath = NULL;

	char path_buffer[_MAX_PATH];
	char chFilePath[_MAX_PATH] = {0,};
	char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
	//실행 파일 이름을 포함한 Full path 가 얻어진다.
	::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
	//디렉토리만 구해낸다.
	_splitpath_s(path_buffer, drive, dir, fname, ext);
	strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
	strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

	switch(TaskNumber)
	{
	case 21: 	pLinkPath = HW_PARAM_PATH_TN21; break;
	//case 11: 	pLinkPath = HW_PARAM_PATH_TN11; break;
	//case 12: 	pLinkPath = HW_PARAM_PATH_TN12; break;
	//case 13: 	pLinkPath = HW_PARAM_PATH_TN13; break;
	//case 14: 	pLinkPath = HW_PARAM_PATH_TN14; break;
	default:
		return false;
	}

	strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

	switch(LensTyep)
	{
	case 1:
		{
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE1, 0, chFilePath);
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1, 0, chFilePath);
			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1, 0, chFilePath);
		}break;
	case 2:
		{
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
			if(pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X == 0xFFFFFFFF)
				pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
			if(pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y == 0xFFFFFFFF)
				pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
			if(pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD	 == 0xFFFFFFFF)
				pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
			if(pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN == 0xFFFFFFFF)
				pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN	= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1, 0, chFilePath);
		}break;
	default:
		{
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_X				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE1, 0, chFilePath);
			pCoordProcObj->m_HWParam.m_AOI_CAM_DISTANCE_Y				= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1, 0, chFilePath);

			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_ODD			= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1, 0, chFilePath);
			pCoordProcObj->m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1, 0, chFilePath);
		}break;
	}
	char ReadData[512];
	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_IMAGE_UPDATE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	pCoordProcObj->m_ImageUpdatePath.Format("%s",  ReadData);

	return true;
}
//bool G_AOI_fnLoadLightingHist(int TaskNumber, double *pSettingValue)
//{
//	char *pLinkPath = NULL;
//
//	char path_buffer[_MAX_PATH];
//	char chFilePath[_MAX_PATH] = {0,};
//	char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
//	//실행 파일 이름을 포함한 Full path 가 얻어진다.
//	::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
//	//디렉토리만 구해낸다.
//	_splitpath_s(path_buffer, drive, dir, fname, ext);
//	strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
//	strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));
//
//	switch(TaskNumber)
//	{
//	case 11: 	pLinkPath = HW_PARAM_PATH_TN11; break;
//	case 12: 	pLinkPath = HW_PARAM_PATH_TN12; break;
//	case 13: 	pLinkPath = HW_PARAM_PATH_TN13; break;
//	case 14: 	pLinkPath = HW_PARAM_PATH_TN14; break;
//	default:
//		return false;
//	}
//
//	strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);
//	if(pSettingValue != nullptr)
//	{
//		char ReadBuf[256];
//		memset(ReadBuf, 0x00, 256);
//		GetPrivateProfileString(Section_HWINFO, Key_AOI_LightingHistMaxPresent, "3.0", ReadBuf, 255, chFilePath);
//		*pSettingValue = atof(ReadBuf);
//		WritePrivateProfileString(Section_HWINFO, Key_AOI_LightingHistMaxPresent, ReadBuf, chFilePath);
//	}
//	else
//		return false;
//	return true;
//}
//Lens Type 추가.(20120726) End
bool G_AOI_fnLoadInspectParam(CoordinatesProc *pCoordProcObj, int TaskNum)
{

	int nInputTaskNum = TaskNum;
	char *pLinkPath = NULL;

	char path_buffer[_MAX_PATH];
	char chFilePath[_MAX_PATH] = {0,};
	char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
	//실행 파일 이름을 포함한 Full path 가 얻어진다.
	::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
	//디렉토리만 구해낸다.
	_splitpath_s(path_buffer, drive, dir, fname, ext);
	strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
	strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

	switch(nInputTaskNum)
	{
	case 21: 	pLinkPath = HW_PARAM_PATH_TN21; break;
	//case 11: 	pLinkPath = HW_PARAM_PATH_TN11; break;
	//case 12: 	pLinkPath = HW_PARAM_PATH_TN12; break;
	//case 13: 	pLinkPath = HW_PARAM_PATH_TN13; break;
	//case 14: 	pLinkPath = HW_PARAM_PATH_TN14; break;
	default:
		return false;
	}

	strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

	CString MakeKey;
	for(int DataStep = 0; DataStep<5; DataStep++)
	{
		MakeKey.Format("%s%d",Key_LensZone_X, DataStep+1);
		pCoordProcObj->m_InspectParam.nZoneStep[DataStep]	= GetPrivateProfileInt(Section_LENS_OFFSET, MakeKey, 8192, chFilePath);

		MakeKey.Format("%s%d",Key_LensOffset_X, DataStep+1);
		pCoordProcObj->m_InspectParam.nZoneOffset[DataStep]	= GetPrivateProfileInt(Section_LENS_OFFSET, MakeKey, 0, chFilePath);
	}
	return true;
}