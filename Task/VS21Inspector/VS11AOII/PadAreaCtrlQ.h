#pragma once

#include "..\..\..\CommonHeader\Inspection\InspectDataQ.h"
#include "..\..\..\CommonHeader\Inspection\InspectPadDataQ.h"
#include "..\..\..\CommonHeader\Inspection\AnaimAOI_Interface.h"
#include "..\..\..\CommonHeader\Inspection\CalculateCoordinateDefine.h"
#include "..\..\..\CommonHeader\Inspection\CoordinatesProc.h"
//#include "ProcState.h"

typedef void (*QInputPadInspector)(InspectPadDataQ *pInspectQBuffer);//Input DLL

class PAD_ImageCtrlQ
{
public:
	PAD_ImageCtrlQ(void);
	~PAD_ImageCtrlQ(void);
private: 
	CRITICAL_SECTION m_CrtSection;
	
	IplImage *m_BufferSwapImg;

	InspectPadDataQ m_HeadNode;
	InspectPadDataQ m_TailNode; 

	InspectPadDataQ *m_pWorkNode; //<<< Image Input 대상 Node를 가르킨다.
	InspectPadDataQ * m_pCopyNode;
	int						m_NodeCount;
	SAOI_IMAGE_INFO			*m_pPADMaskImageInfo;
	SAOI_COMMON_PARA		*m_pPADCommonPara;	

public:
	PAlignCoordInfo			m_PadAlignCoordData;
	IplImage						*m_PadAlignImage;
public:
	int						m_PadIndex;	//PadPosition: 0상, 1하, 2좌, 3우
	int						m_PadDeadZone;
	int						m_PadDeadZonePixel;
	int						m_PadDNDeadZoneW;
	int						m_PadDNDeadZoneH;

	int						m_ScanLineNum;	//현재 Pad부의 Scna Line위치.
	//////////////////////////////////////////////////////////////////////////
	int						m_ImgFragmentatCnt;
	int						m_ImgFragmentatStep;

	CoordinatesProc*	m_pCoordObj;
public:
	int						m_PadPosition;
	//1) Inspector에 Queue전달(Data)
	QInputPadInspector	fn_QPutPADInspector;
private:
	void QCreateNode(InspectPadDataQ * pNewNode);//PadPosition: 0상, 1하, 2좌, 3우
public:
	bool m_fnCreatePadAreaMem(int PadPosition, CoordinatesProc*	pCoordObj, SAOI_IMAGE_INFO*pInspectImgInfo, SAOI_COMMON_PARA *pInspectComParam);//PadPosition: 0상, 1하, 2좌, 3우, 연산된 좌표계.
	bool m_fnSetPadMPosition(int ScanLineNum, CoordinatesProc*	pCoordObj);//Scan Line이 변경될때 Clean Data와 함께 PAD Memory 좌표 Data를 설정한다.
	void QCleanAllData(void);
	void QDeleteAll(void);
	int   QGetNodeCnt(){return m_NodeCount;}

	bool m_fnPADImageAlign_I(int ScanLineNum);
	bool m_fnPADImageAlign_II(int ScanLineNum);
	bool m_fnFindPAD_And_ImageCopy(float Resolution_W,float Resolution_H, IplImage*pOrgImg, CRect* pPadInImgArea, CRect* pPadArea, CRect* pPadORGArea);
	
	InspectPadDataQ * m_fnGetWorkNode()
	{
		return m_pWorkNode;
	}
	bool m_fnSetFirstWorkNode()
	{
		if(m_NodeCount == 0)
		{
			m_pWorkNode = nullptr;
			return false;
		}
		if(m_HeadNode.m_pNextNode != &m_TailNode)
			m_pWorkNode = m_HeadNode.m_pNextNode;
		return true;
	}

	bool m_fnMoveNextWorkNode()
	{
		if(m_NodeCount == 0)
		{
			m_pWorkNode = nullptr;
			return false;
		}
		m_pWorkNode = m_pWorkNode->m_pNextNode; 
		if(m_pWorkNode==&m_TailNode)
			m_pWorkNode = m_HeadNode.m_pNextNode;
		return true;
	}

	void m_fnPadImageBlurAction(int StepCnt)
	{
		InspectPadDataQ *pBlurNode = m_HeadNode.m_pNextNode;
		for(int saveStep =0; saveStep<m_NodeCount; saveStep++)
		{
			pBlurNode->m_pPADAreaImg;
			cvSmooth(pBlurNode->m_pPADAreaImg, pBlurNode->m_pPADAreaImg, CV_BLUR, StepCnt, StepCnt, 0, 0);
			pBlurNode = pBlurNode->m_pNextNode;
		}
	}
//#ifdef TEST_SAVE_PAD_AREA_SAVE
	bool m_fnSavePadInspectImage(char *strPadOrgSavePath, bool SaveBmp = false)
	{
#ifdef TEST_SAVE_PAD_FIRST_LAST_SAVE
		InspectPadDataQ *pFirstNode = m_HeadNode.m_pNextNode;
		InspectPadDataQ *pLastNode = m_TailNode.m_pFrontNode;

		if(pFirstNode != &m_TailNode && pLastNode != &m_HeadNode)
		{
			char TestSaveImage[256];
			char PadPos[10];
			switch(m_PadIndex)
			{
			case 0:	sprintf_s(PadPos, "Up");	break;
			case 1:	sprintf_s(PadPos, "Dn");	break;
			case 2:	sprintf_s(PadPos, "Lf");	break;
			case 3:	sprintf_s(PadPos, "Rt");	break;
			default:
				sprintf_s(PadPos, "Un");	break;
			}
			sprintf_s(TestSaveImage, "%sPad_%d_%d%s_First_%d.jpg", PAD_ORG_IMG_SAVE_PATH, m_ScanLineNum, m_PadIndex, PadPos, 1);
			cvSaveImage(TestSaveImage, pFirstNode->m_pPADAreaImg);
			sprintf_s(TestSaveImage, "%sPad_%d_%d%s_Last_%d.jpg", PAD_ORG_IMG_SAVE_PATH, m_ScanLineNum, m_PadIndex, PadPos, m_NodeCount);
			cvSaveImage(TestSaveImage, pLastNode->m_pPADAreaImg);
		}
#else
		InspectPadDataQ *pSaveNode = m_HeadNode.m_pNextNode;
		for(int saveStep =0; saveStep<m_NodeCount; saveStep++)
		{
			char TestSaveImage[256];
			char PadPos[10];
			switch(m_PadIndex)
			{
			case 0:	sprintf_s(PadPos, "Up");	break;
			case 1:	sprintf_s(PadPos, "Dn");	break;
			case 2:	sprintf_s(PadPos, "Lf");	break;
			case 3:	sprintf_s(PadPos, "Rt");	break;
			default:
				sprintf_s(PadPos, "Un");	break;
			}

			sprintf_s(TestSaveImage, "%s\\%s\\Pad_%d_%d%s_%d.%s", 
								strPadOrgSavePath/*PAD_ORG_IMG_SAVE_PATH*/, 
								SAVE_PADORG_IMG_FOLDER, m_ScanLineNum, 
								m_PadIndex, 
								PadPos, 
								(saveStep+1), 
								(SaveBmp== true)?"bmp":"jpg");
			cvSaveImage(TestSaveImage, pSaveNode->m_pPADAreaImg);
			pSaveNode = pSaveNode->m_pNextNode;
		}
#endif
		return true;
	}
//#endif
	
	
};

