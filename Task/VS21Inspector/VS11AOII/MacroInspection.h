#pragma once
#include "stdafx.h"

using namespace std;
//
//
///**
//@struct	SMASK_AOI_IMAGEINFO
//@brief	마스크 데이터
//@remark	
//@author	고정진
//@date	2010/4/28  16:47
//*/
//struct SAOI_IMAGE_INFO
//{	
//	int		nScaleX1;						///<마스크 패턴의 단위(Active)
//	int		nScaleY1;						///<마스크 패턴의 단위(Active)
//	int		nScaleX2;						///<마스크 패턴의 단위(Left Pad)
//	int		nScaleY2;						///<마스크 패턴의 단위(Left Pad)
//	int		nScaleX3;						///<마스크 패턴의 단위(Right Pad)
//	int		nScaleY3;						///<마스크 패턴의 단위(Right Pad)
//	int		nMinRangeMain;				///<검출 허용치 Default : 60(Active)
//	int		nMaxRangeMain;				///<검출 허용치 Default : 60(Active)
//	int		nMinRangeLPad;				///<검출 허용치 Default : 60(Left Pad)
//	int		nMaxRangeLPad;				///<검출 허용치 Default : 60(Left Pad)
//	int		nMinRangeRPad;				///<검출 허용치 Default : 60(Right Pad)
//	int		nMaxRangeRPad;				///<검출 허용치 Default : 60(Right Pad)
//	int		nOutLineLeft;					///<제거할 테두리 두께. Default : 1
//	int		nOutLineRight;					///<제거할 테두리 두께. Default : 1
//	int		nOutLineUp;					///<제거할 테두리 두께. Default : 1
//	int		nOutLineDown;				///<제거할 테두리 두께. Default : 1
//	int		nBlockCount;					///<_ACC Block, Default : 128
//	int		nThreadCount;					///<_ACC Thread, Default : 64
//	int		nMinimumSizeX;				///<최소 검출 단위 Default : 2 Y 조건과 and 조건이다.
//	int		nMinimumSizeY;				///<최소 검출 단위 Default : 2 X 조건과 and 조건이다.
//	int		nMaximumSizeX;				///<검출 상한선, 설정 단위 이상은 무시한다. Y 조건과 or 조건이다.
//	int		nMaximumSizeY;				///<검출 상한선, 설정 단위 이상은 무시한다. X 조건과 or 조건이다.
//	SAOI_IMAGE_INFO()
//	{
//		memset(this, 0x00, sizeof(SAOI_IMAGE_INFO));
//	}
//};
//
//struct SAOI_COMMON_PARA
//{	
//	int		nDefectDistance;		///<동일 결함으로 간주할 거리.			Default 100 Pixel	
//	int		nImageWidth;			///<이미지의 X축 길이
//	int		nImageHeight;			///<이미지의 Y축 길이
//	int		nMaskType;				///<마스크의 사이즈	1 : 1, 2 : 5, 3 : 9	
//	int		nZoneStep[5];			///렌즈 왜곡에 따른 존 설정
//	int		nZoneOffset[5];			///렌즈 왜곡에 따른 존 Offset
//	int		bImageLogUsed;			///<이미지 로그 생성 유무					true or false
//	SAOI_COMMON_PARA()
//	{
//		memset(this, 0x00, sizeof(SAOI_COMMON_PARA));
//	}
//};
//
//
///** 
//@brief		검출 Defect정보를 저장 한다.
//
//@remark
//-			 Defect 정보를 처리 하기 위한 기본 Data구조 이다.
//-			
//@author		최지형
//@date		2010.03.00
//*/ 
//typedef struct DEFECT_DATA_TAG
//{
//	int		DefectType;
//	int		StartX;
//	int		StartY;
//	int		EndX;
//	int		EndY;
//
//	int		nDefectCount;
//	int		nDValueMin;
//	int		nDValueMax;
//	int		nDValueAvg;
//
//	DEFECT_DATA_TAG()
//	{
//		memset(this,0x00, sizeof(DEFECT_DATA_TAG));
//	}
//
//}DEFECT_DATA;


static const int				MAX_DEFECT	= 200;
static const unsigned char		DEAD_PIXEL	= 0xFF;
static const unsigned char		LIVE_PIXEL	= 0x00;

class CMacroInspect
{
private: //멤버변수
//	IplImage* mp_iplResultBuff;
	IplImage* mp_iplSourceImage;
	SAOI_IMAGE_INFO m_stImageInfo;
	SAOI_COMMON_PARA m_stCommonPara;
	int		m_nHThreshold[256];
	DEFECT_DATA	m_stPreDefectData[MAX_DEFECT];
	int		m_nMacroDefectCount;
	double m_dPixelResolution1;
public:
		IplImage* mp_iplResultBuff;
	CMacroInspect(void)
	{		
		mp_iplResultBuff = nullptr;
		mp_iplSourceImage = nullptr;
		m_nMacroDefectCount = 0;
		m_dPixelResolution1=5.0;
	}
	~CMacroInspect(void)
	{
		if(mp_iplResultBuff != nullptr)
		{
			cvReleaseImage(&mp_iplResultBuff);
			mp_iplResultBuff = nullptr;
		}

		if(mp_iplSourceImage != nullptr)
		{
			cvReleaseImage(&mp_iplSourceImage);
			mp_iplSourceImage = nullptr;
		}
	}

	int m_fnSearchDefect(IplImage* iplMacroImage, SAOI_IMAGE_INFO stImageInfo, SAOI_COMMON_PARA stCommonPara, DEFECT_DATA* stDefectData,double dPixelResolution)
	{
		memset(m_stPreDefectData, 0x00, sizeof(DEFECT_DATA)*MAX_DEFECT);

		bool	bSearchResult	= false;
		bool	bFindDead		= false;
		int		nAxis_X			= 0;
		int		nAxis_Y			= 0;	
		int		nTargetStep	= 0;
		unsigned char	chCompareValue = '\0';		
		int nOffsetJump = 0;
		m_dPixelResolution1=dPixelResolution;
		m_stImageInfo = stImageInfo;
		m_stCommonPara = stCommonPara;

		if(mp_iplResultBuff != nullptr)
		{
			cvReleaseImage(&mp_iplResultBuff);
			mp_iplResultBuff = nullptr;
		}

		if(mp_iplSourceImage != nullptr)
		{
			cvReleaseImage(&mp_iplSourceImage);
			mp_iplSourceImage = nullptr;
		}

		mp_iplSourceImage = cvCloneImage(iplMacroImage);
		
		mp_iplResultBuff = cvCloneImage(iplMacroImage);
		cvZero(mp_iplResultBuff); 


		for( int nStep = 0 ; nStep <= 255; nStep++ )
		{
			m_nHThreshold[nStep] = (int) (((float)stImageInfo.nMaxRangeMain - (float)stImageInfo.nMinRangeMain) / 256.0 * nStep + stImageInfo.nMinRangeMain );
		}
		
		for( nAxis_Y = 1; nAxis_Y < mp_iplSourceImage->height - 1; nAxis_Y++ )
		{		
			nOffsetJump = mp_iplSourceImage->width * nAxis_Y;

			for(nAxis_X = 1; nAxis_X < mp_iplSourceImage->width - 1; nAxis_X++)
			{
				nTargetStep = nAxis_X + nOffsetJump;

				chCompareValue =	m_fnCompare_AxisY(	 nAxis_X, nAxis_Y);			
				if( stImageInfo.nScaleX1 != 0 && chCompareValue != 0x00 ) //반복 단위가 0 이 아니라면...
					chCompareValue =	m_fnCompare_AxisX(	 nAxis_X, nAxis_Y ); //X축 비교를 수행한다.	

				m_SetByte((unsigned char*)mp_iplResultBuff->imageData, nTargetStep, chCompareValue);			
			}
		}

		CString strSaveImgPath;
		strSaveImgPath.Format("Z:\\[04_Master]\\MacroImage_Rst.bmp");	
		if(mp_iplResultBuff != nullptr)
			cvSaveImage(strSaveImgPath.GetBuffer(strSaveImgPath.GetLength()), mp_iplResultBuff);


		m_fnResultDefectPos();
		m_fnGetMacroDefectCount();		

		memcpy(stDefectData, m_stPreDefectData, sizeof(DEFECT_DATA) * MAX_DEFECT);		

//////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
		return m_nMacroDefectCount;
	}

	void m_fnGetMacroDefectCount()
	{
		m_nMacroDefectCount = 0;

		for(int nStep = 0; nStep < MAX_DEFECT; nStep++ )
		{
			if(m_stPreDefectData[nStep].nDefectCount == 0)
				break;			
			m_nMacroDefectCount++;
			
		}		
	}


	bool m_fnReadPosition( int nXPos, int nYPos, unsigned char* chReadData)
	{
		int		nStepPosition = 0;	

		if(nXPos < 0 || nYPos < 0 )
		{
			return FALSE;
		}
		if(nXPos >= mp_iplSourceImage->width || nYPos >= mp_iplSourceImage->height )
		{
			return FALSE;
		}

		nStepPosition=((nYPos) * mp_iplSourceImage->width) + nXPos;

		memcpy(chReadData, &mp_iplSourceImage->imageData[nStepPosition], 1);

		return true;
	}

	unsigned char m_fnCompare_AxisX(	int nXPos, int nYPos )
	{	
		unsigned char chPixelData = '\0';
		unsigned char chComparePixel = '\0';
		unsigned char	chCompareValue = '\0';
		//unsigned int		nTargetStep   = 0;
		int		nComparePosX  = 0;
		int		nComparePosY  = 0;	
		int		nCompareStep	= 1;
		int		nTempPosX		= 0;

		m_fnReadPosition(nXPos, nYPos, &chPixelData);		

		//좌측면 연산.	
		if( nXPos <= mp_iplSourceImage->width / 2 )
		{
			if( nXPos - m_stImageInfo.nScaleX1 - 1 > 0 )
			{
				nCompareStep = nCompareStep * (-1);
			}

			for( int nPoint = nCompareStep; nPoint < nCompareStep + 3; nPoint++ )
			{
				if( nPoint==0 )
					continue;

				nTempPosX = m_stImageInfo.nScaleX1 * nPoint;			

				if( nTempPosX + nXPos + 1 >= mp_iplSourceImage->width )
					break;			

				nComparePosX = nXPos + nTempPosX;
				nComparePosY = nYPos;

				m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
				chCompareValue = m_fnRGB_Compare( chPixelData, chComparePixel, 0 );

				if( chCompareValue == 0x00)
				{
					return 0x00;
				}						
			}	
		}
		//우측면 연산.
		else// if(nXPos>=IMAGE_X_SCALE/2)
		{
			nCompareStep = 1;

			if( nXPos + m_stImageInfo.nScaleX1 + 1 >= mp_iplSourceImage->width )
			{
				nCompareStep = nCompareStep * (-1);
			}

			for( int nPoint = nCompareStep; nPoint > nCompareStep - 3; nPoint-- )
			{
				if( nPoint == 0 )
					continue;

				nTempPosX = m_stImageInfo.nScaleX1 * nPoint;

				if( nTempPosX + nXPos - 1 <= 0 )
				{
					break;	
				}
				nComparePosX = nXPos + nTempPosX;
				nComparePosY = nYPos;

				m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	
				chCompareValue = m_fnRGB_Compare( chPixelData, chComparePixel, 0 );

				if( chCompareValue == 0x00)
				{
					return 0x00;
				}						
			}
		}

		//결과 좌표를 White Pixel 로 변경한다.	
		//nTargetStep = nXPos + (this->m_nImageWidth * nYPos) + (m_nImageOffset * nYPos);
		//m_SetByte(m_chResultImage, nTargetStep, 0xFF);
		//m_SetByte(m_chSizeFilterBuff, nTargetStep, 0xFF);	
		return chCompareValue;
	}
	/**	
	@brief	최초 검출 함수 이다.
	@param 	연산을 수행할 좌표
	@return	비교 결과.(PIXEL_EQUAL : 정상, PIXEL_DIFF : 이상)
	@remark
	-			X축 방향 3회,  Y축 방향 3회 최대 6회 수행한다.
	@author	고정진
	@date	2010/4/28  16:30
	*/
	unsigned char m_fnCompare_AxisY(int nXPos, int nYPos)
	{
		unsigned char chPixelData	= '\0';
		unsigned char chComparePixel = '\0';
		unsigned char	chCompareValue = '\0';

		int		nComparePosX  = 0;
		int		nComparePosY  = 0;

		m_fnReadPosition(nXPos, nYPos, &chPixelData);	

		//상측면 연산.
		//각 Scale Step	당 3Point 씩 1Scale 3Point 를 차분 연산한다.	
		int nTempPosY = 0;
		int nCompareStep=1;

		if( nYPos <= mp_iplSourceImage->height / 2 )
		{
			if( nYPos - m_stImageInfo.nScaleY1 - 1 > 0 )
				nCompareStep = nCompareStep * (-1);

			//Y 축 비교는 Y 축 범위 안에서 3회 실시.
			for( int nPoint = nCompareStep; nPoint < (nCompareStep + 3); nPoint++ )
			{
				if( nPoint == 0 )
					continue;

				nTempPosY = m_stImageInfo.nScaleY1 * nPoint;

				if( nTempPosY + nYPos + 1 >= mp_iplSourceImage->height)
					return false;				

				nComparePosX = nXPos;
				nComparePosY = nYPos + nTempPosY;

				m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	

				chCompareValue = m_fnRGB_Compare( chPixelData, chComparePixel, 0 );

				if( chCompareValue == 0x00)
				{
					return 0x00;
				}				
			}
		}
		//하측면 연산.
		else// if(nYPos>=IMAGE_Y_SCALE/2)
		{
			nCompareStep = 1;
			if( nYPos + m_stImageInfo.nScaleY1 + 1 >= mp_iplSourceImage->height)
				nCompareStep = nCompareStep * (-1);
			//Y 축 비교는 Y 축 범위 안에서 3회 실시.
			for( int nPoint = nCompareStep; nPoint > (nCompareStep - 3); nPoint--)
			{
				if( nPoint == 0)
					continue;

				nTempPosY = m_stImageInfo.nScaleY1 * nPoint;

				if( nTempPosY + nYPos - 1 <= 0 )
					return false;

				nComparePosX = nXPos;
				nComparePosY = nYPos + nTempPosY;

				if( nComparePosY >=  mp_iplSourceImage->height )
					nComparePosY = 0;

				m_fnReadPosition(nComparePosX, nComparePosY, &chComparePixel);	

				chCompareValue = m_fnRGB_Compare( chPixelData, chComparePixel, 0 );

				if( chCompareValue == 0x00)
				{
					return 0x00;
				}			
			}
		}
		return chCompareValue;
	}

	/**	
	@brief	비교 연산 함수
	@param 	비교값 1,2, 오차 허용치
	@return	PIXEL_DIFF : 허용치 밖, PIXEL_EQUAL : 허용치 안
	@remark
	-		
	@author	고정진
	@date	2010/4/28  16:44
	*/
	unsigned char m_fnRGB_Compare(unsigned char chPixel_1,unsigned char chPixel_2,int n_DefectRange)
	{	
		int nErrorRange = m_nHThreshold[int(chPixel_1)];

		if( abs( (int)chPixel_1 - (int)chPixel_2 ) > nErrorRange )
		{			
			return 0xFF;			
		}
		else
		{				
			return 0x00;
		}
	}

	inline void	m_SetByte(unsigned char* chStartAdd, unsigned int nPosition,unsigned char chSetChar)
	{
		chStartAdd[nPosition] = chSetChar;	
	};


	/**	
	@brief	Defect 의 위치와 크기 검출.
	@param 	1차 필터 처리 이미지, 열 검출 데이터, 행 검출 데이터
	@return	없음
	@remark
	-		
	@author	고정진
	@date	2010/4/28  17:04
	*/
	void m_fnResultDefectPos()
	{
		unsigned char chPixelData = '\0';
		bool		bSearchResult	= false;	
		int			nTargetStep	= 0;
		int			nTempBack	= 0;	
		int         nDefectCnt =0;
				
		cvCopyImage(mp_iplResultBuff, mp_iplSourceImage);

		ZeroMemory(m_stPreDefectData,sizeof(DEFECT_DATA) * MAX_DEFECT);
		//정의천 추가

		m_stCommonPara.nDefectDistance =(int)((float) m_stCommonPara.nDefectDistance/m_dPixelResolution1);

				
		for(int nXLineCnt = 1; nXLineCnt < mp_iplSourceImage->width - 1; nXLineCnt++)
		{
			for(int nYLineCnt = 0; nYLineCnt < mp_iplSourceImage->height - 1; nYLineCnt++)
			{	
				m_fnReadPosition(nXLineCnt,nYLineCnt,&chPixelData);		

				if(	chPixelData != LIVE_PIXEL) //검출된 에러인가?
				{
					bSearchResult	= TRUE;							//검출 플래그 셑.

					for(int nIndex = 0; nIndex < MAX_DEFECT; nIndex++)
					{
						if(m_stPreDefectData[nIndex].nDefectCount == 0)			//현재 Index 의 시작 좌표가 0 인가?
						{
							m_stPreDefectData[nIndex].nDefectCount++;
							m_stPreDefectData[nIndex].StartX = nXLineCnt;	//입력된 좌표를 셑 한다.
							m_stPreDefectData[nIndex].StartY = nYLineCnt;
							m_stPreDefectData[nIndex].EndX = nXLineCnt;
							m_stPreDefectData[nIndex].EndY = nYLineCnt;
							nDefectCnt++;
							break;
						}
						else										//현재 Index 의 시작 좌표가 0 가 아닌가?
						{
							if((abs(nXLineCnt - m_stPreDefectData[nIndex].StartX) > m_stCommonPara.nDefectDistance) &&	//입력된 Y 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
								(abs(nXLineCnt -m_stPreDefectData[nIndex].EndX) > m_stCommonPara.nDefectDistance))
								continue;


							if(nYLineCnt > m_stPreDefectData[nIndex].StartY && nYLineCnt < m_stPreDefectData[nIndex].EndY )
							{	
								m_stPreDefectData[nIndex].nDefectCount++;

								if(nYLineCnt < m_stPreDefectData[nIndex].StartY)		//입력된 X 좌표가 현재 Index 의 시작 좌표보다 작은가?
								{								
									m_stPreDefectData[nIndex].StartY = nYLineCnt;	//시작 좌표를 입력된 좌표로 설정한다.
								}
								else if(nYLineCnt > m_stPreDefectData[nIndex].EndY)//입력된 X 좌표가 현재 Index 의 끝 좌표보다 큰가?
								{
									m_stPreDefectData[nIndex].EndY = nYLineCnt;	//끝 좌표를 입력된 좌표로 설정한다.
								}
								
								m_stPreDefectData[nIndex].EndX = nXLineCnt;
								nDefectCnt++;
								break;
							}

							else if((abs(nYLineCnt - m_stPreDefectData[nIndex].StartY) > m_stCommonPara.nDefectDistance) &&    //입력된 X 좌표의 거리가 시작 좌표와 설정된 오차 범위 밖 인가?
								(abs(nYLineCnt - m_stPreDefectData[nIndex].EndY) > m_stCommonPara.nDefectDistance))	//입력된 X 좌표의 거리가 끝 좌표와 설정된 오차 범위 밖 인가?
								//입력된 Y 좌표의 거리가 끝 좌표와 설정된 오차 범위 밖 인가?
							{
								continue;	//다음 포인트를 읽는다.
							}

							else			//오차 범위 내 이라면?
							{	
								m_stPreDefectData[nIndex].nDefectCount++;

								if(nYLineCnt < m_stPreDefectData[nIndex].StartY)		//입력된 X 좌표가 현재 Index 의 시작 좌표보다 작은가?
								{								
									m_stPreDefectData[nIndex].StartY = nYLineCnt;	//시작 좌표를 입력된 좌표로 설정한다.
								}
								else if(nYLineCnt > m_stPreDefectData[nIndex].EndY)//입력된 X 좌표가 현재 Index 의 끝 좌표보다 큰가?
								{
									m_stPreDefectData[nIndex].EndY = nYLineCnt;	//끝 좌표를 입력된 좌표로 설정한다.
								}
								m_stPreDefectData[nIndex].EndX = nXLineCnt;
								nDefectCnt++;								
								break;
							}
						}
					}
				}
				else//결함이 아닌 픽셀인가?
				{

				}
			}
		}	
	}

};
