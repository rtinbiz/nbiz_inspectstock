#pragma once
#include "..\..\..\CommonHeader\Inspection\DefineStaticValue.h"

#define  INSPECTION_NONE	0
#define  INSPECTION_IDLE		1
#define  INSPECTION_STOP	2

typedef struct PROCESS_STATE_AGT
{
	//상태 변경.
	HWND			m_UpdateUIHandle;
	int					m_UnitState;
	int					m_InspectionStop;

	bool				m_bBlockScanEnd;//Block Scan이 완료되면 true가 된다.
	//////////////////////////////////////////////////////////////////////////
	//IDT Memory State
	int		*			m_pNodeCount;
	int		*			m_pGrabScanLineNum;			
	int		*			m_pGrabScanImageCnt;		
	int		*			m_pIdleNodeCnt;
	bool	*			m_pOnlyGrabImageSave;
	
	//////////////////////////////////////////////////////////////////////////
	//Mil Grabber State.
	bool				m_bMilRunGrabFunction;
	bool				m_bMilGrabFunMemError;
	int					m_nNoneProcImgCount;
	DWORD 			m_nMilGrabFunProTick;//첫장은 오류, 1번에서 2번까지의 시간을 잰다.

	DWORD 			m_nFileGrabFunProTick;//첫장은 오류, 1번에서 2번까지의 시간을 잰다.

	int					m_GrabImageCnt;
	//////////////////////////////////////////////////////////////////////////
	//Inspector Proces State.
	unsigned int m_ProcStateInspector[10];
	//////////////////////////////////////////////////////////////////////////
	//Cutter Process State.
	int					m_CutScanLineNum[PADCUT_PROC_CNT];			
	int					m_CutScanLineIndex[PADCUT_PROC_CNT];			
	DWORD 			m_nCutterProcTick[PADCUT_PROC_CNT];
	//////////////////////////////////////////////////////////////////////////
	//Result Process State.
	int					m_RetScanLineNum[RET_PROC_CNT];			
	int					m_RetScanLineIndex[RET_PROC_CNT];			
	DWORD 			m_nResultProcTick[RET_PROC_CNT];
	int					m_RetDefectCnt[RET_PROC_CNT];


	//////////////////////////////////////////////////////////////////////////
	//Sorter Process State.
	int					m_SortScanLineNum;			
	int					m_SortScanLineIndex;			
	DWORD 			m_nSortProcTick;
	//////////////////////////////////////////////////////////////////////////
	//Process Time By Images
	DWORD 			m_nOneImageTick;				//<<< Image 한장을 Grab부터 Write까지의 시간.
	DWORD 			m_nOneImageMaxTick;			//<<< Image 한장을 Grab부터 Write까지의 시간.
	DWORD 			m_nOneImageMinTick;			//<<< Image 한장을 Grab부터 Write까지의 시간.

	DWORD 			m_nOneLineStartTick;			//<<< One Line을 Grab부터 Write까지의 시간.
	DWORD 			m_nOneLineNowTick;			//<<< One Line을 Grab부터 Write까지의 시간.
	int		 			m_nOneLineNowIndex;			//<<< One Line을 Grab부터 Write까지의 시간.

	DWORD 			m_nOneBlockProcTick;			//<<< One Block을 Grab부터 Write까지의 시간.
	DWORD			m_nProcSizePerSec;				//<<< 초당 처리량.

	//////////////////////////////////////////////////////////////////////////
	//Pad Inspect Process State.
	int					m_PADInspectTotalCnt;			
	int					m_PADInspectStep;			
	DWORD			m_PadProcTick[4];
	DWORD			m_PadAlignProcTick;
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	unsigned int m_ProcStateGrabber;
	unsigned int m_ProcStatePadCutter[PADCUT_PROC_CNT];
	unsigned int m_ProcStateResult[RET_PROC_CNT];
	unsigned int m_ProcStateWriteFile;

	//////////////////////////////////////////////////////////////////////////
	//Write File Process State.
	CRect				m_ViewRect;
	HDC *			m_pHDcView;

	CRect			m_HistViewRect;
	HDC *			m_HistpHDcView;
	int				m_HistScanMaxValue;
	float			m_Histogaram_Data[256];

	//조명값 히스토그램의 Max수치 %적용.
	int  			dbLightingHistMaxPresent;
	bool			bRealTimeLightValueSend;
	bool			bRealTimePixelValueSend;
	//CRect				m_ViewSaveImgRect;
	//HDC *			m_pHDcSaveImgView;
	DWORD 			m_nWriteProcTick;

	bool				m_bDefectFileCopy;
	bool				m_bOrgImageBackup;

	bool				m_OldFileClearing;
	//////////////////////////////////////////////////////////////////////////
	bool				m_bAlignScan;//Align을 위한 Grab을 실행 한다.
	//////////////////////////////////////////////////////////////////////////

	double		m_ActiveAFValue;

	//Save Org Image Draw(Act/Pad Area)
	bool bTEST_SAVE_CUT_AREA_DRAW;

	//75 3D Task Review Data Sending
	//bool bTEST_VS75_REVIEW_SEND;

	//Defect Image Save JPG
	bool bTEST_DEFECT_IMG_SAVE_JPG;
		//Defect Image Save JPG
		//bool bTEST_DEFECT_IMG_BMPorJPG;// true BMP, false JPG
	//Save Inspect Image
	bool bTEST_INSPECT_IMG;
	//Draw Inspect Image Defect
	//bool bTEST_INSPECT_IMG_DRAW_DEFECT;

	//Active Area Histogram Image Save
	bool bTEST_ACTIVE_IMG_HISTOGRAM_IMG_SAVE;
	int nActiveHistCalcSkipIndex;
	//HISTOGRAM Org Image Save 
	bool bTEST_SAVE_HISTO_ORG_IMG;

	bool bTEST_USE_PAD_ALIGN;
	//Pad Org Area Image Save
	bool bTEST_SAVE_PAD_AREA;
	bool bTEST_SAVE_PAD_AREA_BMP;
	int nTEST_SAVE_PAD_AREA_BlurStep;
	//Small Defect View Size
	int nSmallDefectSizeX;
	int nSmallDefectSizeY;


	DWORD 			m_nProcFileUpdateTime;//Buffer Image와 검사 이외의 처리 시간.
	DWORD 			m_nProcFileUpdateMaxTime;//Buffer Image와 검사 이외의 처리 시간.
	//
	PROCESS_STATE_AGT()
	{
		m_UpdateUIHandle		= 0;
		m_UnitState				= nBiz_AOI_STATE_IDLE;
		//////////////////////////////////////////////////////////////////////////
		m_InspectionStop		= INSPECTION_NONE;
		m_bBlockScanEnd			= false;
		//////////////////////////////////////////////////////////////////////////
		m_pNodeCount			= nullptr;
		m_pGrabScanLineNum		= nullptr;
		m_pGrabScanImageCnt		= nullptr;		
		m_pOnlyGrabImageSave	= nullptr;
		m_pIdleNodeCnt			= nullptr;

		m_bMilRunGrabFunction	= false;
		m_bMilGrabFunMemError	= false;
		m_nNoneProcImgCount		= 0;
		m_nMilGrabFunProTick	= 0;
		m_nFileGrabFunProTick	= 0;

		m_GrabImageCnt			= 0;

		memset(m_ProcStateInspector, 0x00, sizeof(unsigned int)*10);

		for(int CrStep =0; CrStep<PADCUT_PROC_CNT; CrStep++)
		{
			m_CutScanLineNum[CrStep]	= 0;			
			m_CutScanLineIndex[CrStep]	= 0;			
			m_nCutterProcTick[CrStep]	= 0;			
		}

		for(int CrStep =0; CrStep<RET_PROC_CNT; CrStep++)
		{
			m_RetScanLineNum[CrStep]	= 0;			
			m_RetScanLineIndex[CrStep]	= 0;			
			m_nResultProcTick[CrStep]	= 0;	
			m_RetDefectCnt[CrStep]		= 0;
		}
		m_PadProcTick[0]				= 0;
		m_PadProcTick[1]				= 0;
		m_PadProcTick[2]				= 0;
		m_PadProcTick[3]				= 0;

		m_PadAlignProcTick			= 0;

		m_SortScanLineNum			= 0;
		m_SortScanLineIndex			= 0;			
		m_nSortProcTick				= 0;

		m_nOneImageTick				= 0;
		m_nOneImageMaxTick			= 0;
		m_nOneImageMinTick			= 99999;
		
		m_nOneLineStartTick			= 0;
		m_nOneLineNowTick			= 0;
		m_nOneLineNowIndex			= 0;

		m_nOneBlockProcTick			= 0;
		m_nProcSizePerSec			= 0;

//		m_pHDcSaveImgView			= nullptr;
		m_pHDcView					= nullptr;
		m_nWriteProcTick			= 0;

		m_HistViewRect;
		m_HistpHDcView				= NULL;
		m_HistScanMaxValue			= 0;
		memset(m_Histogaram_Data, 0x00, sizeof(float)*256);
		dbLightingHistMaxPresent	= 10;
		bRealTimeLightValueSend	 = false;
		bRealTimePixelValueSend	 = false;

		m_bDefectFileCopy			= false;
		m_bOrgImageBackup			= false;
		m_OldFileClearing			= false;


		m_bAlignScan				= false;

		m_ActiveAFValue				= 0.0f;

		//Save Org Image Draw(Act/Pad Area)
		bTEST_SAVE_CUT_AREA_DRAW = false;

		////75 3D Task Review Data Sending
		//bTEST_VS75_REVIEW_SEND= false;

		//Defect Image Save JPG
		bTEST_DEFECT_IMG_SAVE_JPG= false;
			//bTEST_DEFECT_IMG_BMPorJPG = false;

		//Save Inspect Image
		bTEST_INSPECT_IMG = false;

		//Draw Inspect Image Defect
		//bTEST_INSPECT_IMG_DRAW_DEFECT = false;

		//Active Area Histogram Image Save
		bTEST_ACTIVE_IMG_HISTOGRAM_IMG_SAVE = false;
		nActiveHistCalcSkipIndex = 3;
		//HISTOGRAM Org Image Save 
		bTEST_SAVE_HISTO_ORG_IMG = false;


		bTEST_USE_PAD_ALIGN	 = true;
		//Pad Org Area Image Save
		bTEST_SAVE_PAD_AREA = false;
		bTEST_SAVE_PAD_AREA_BMP = false;
		nTEST_SAVE_PAD_AREA_BlurStep = 0;

		nSmallDefectSizeX = 100;
		nSmallDefectSizeY = 100;

		m_nProcFileUpdateTime = 0;
		m_nProcFileUpdateMaxTime = 0;


	}
	
	
	

	
	int 	AOI_StateChange(int setAOIState)
	{
		int RetValue = nBiz_ST_CH_OK;//#define nBiz_ST_CH_OK												1
		switch(m_UnitState)
		{
		case nBiz_AOI_STATE_IDLE://#define nBiz_ST_CH_ERROR_IDLE_TO_OTHER					5//		IDLE		==> READY,			ALARM
			switch(setAOIState)
			{
			case nBiz_AOI_STATE_READY:
			case nBiz_AOI_STATE_ALARM:
			case nBiz_AOI_STATE_IDLE:
				m_UnitState = setAOIState;
				break;

			case nBiz_AOI_STATE_BUSY:
				RetValue = nBiz_ST_CH_ERROR_IDLE_TO_OTHER;
				break;
			case nBiz_AOI_STATE_ERROR:
				m_UnitState = nBiz_AOI_STATE_ERROR;
				break;
			default:
				RetValue = nBiz_ST_CH_ERROR_STATE_ERROR;
				break;
			}
			break;
		case nBiz_AOI_STATE_READY:	//#define nBiz_ST_CH_ERROR_READY_TO_OTHER				4//		READY	==> BUSY, IDLE,		ALARM
			switch(setAOIState)
			{
			case nBiz_AOI_STATE_BUSY:
			case nBiz_AOI_STATE_IDLE:
			case nBiz_AOI_STATE_READY:
			case nBiz_AOI_STATE_ALARM:
				m_UnitState = setAOIState;
				break;
			case nBiz_AOI_STATE_ERROR:
				m_UnitState = nBiz_AOI_STATE_ERROR;
				break;
			default:
				RetValue = nBiz_ST_CH_ERROR_STATE_ERROR;
				break;
			}
			break;
		case nBiz_AOI_STATE_BUSY://#define nBiz_ST_CH_ERROR_BUSY_TO_OTHER				 3//		BUSY		==> IDLE,				ALARM
			switch(setAOIState)
			{
			case nBiz_AOI_STATE_READY:
			case nBiz_AOI_STATE_BUSY:
			case nBiz_AOI_STATE_IDLE:
			case nBiz_AOI_STATE_ALARM:
				m_UnitState = setAOIState;
				break;
				//RetValue = nBiz_ST_CH_ERROR_BUSY_TO_OTHER;
				break;
			case nBiz_AOI_STATE_ERROR:
				m_UnitState = nBiz_AOI_STATE_ERROR;
				break;
			default:
				RetValue = nBiz_ST_CH_ERROR_STATE_ERROR;
				break;
			}
			break;
		case nBiz_AOI_STATE_ALARM://#define nBiz_ST_CH_ERROR_ALARM_TO_OTHER				2 //		ALARM	==> IDLE Only OK
			switch(setAOIState)
			{
			case nBiz_AOI_STATE_IDLE:
			case nBiz_AOI_STATE_ALARM:
				m_UnitState = setAOIState;
				break;

			case nBiz_AOI_STATE_BUSY:
			case nBiz_AOI_STATE_READY:
				RetValue = nBiz_ST_CH_ERROR_ALARM_TO_OTHER;
				break;
			case nBiz_AOI_STATE_ERROR:
				m_UnitState = nBiz_AOI_STATE_ERROR;
				break;
			default:
				RetValue = nBiz_ST_CH_ERROR_STATE_ERROR;
				break;
			}
			break;
		case nBiz_AOI_STATE_ERROR:
			RetValue = nBiz_ST_CH_ERROR_STATE_ERROR;
			break;
		default:
			RetValue = nBiz_ST_CH_ERROR_STATE_ERROR;
			break;
		}
		
		//////////////////////////////////////////////////////////////////////////
		return RetValue;

	}
}PROC_STATE;

//Up/Dn Pad부 Align에 사용되는 변수.
typedef struct PROC_PAD_ALIGN_AGT
{
	int m_AlignWidth;
	int m_AlignOverWidth;//X축 Align위한 값.

	

	int m_AlignOKValue;//패턴 메칭 값이 설정 수치 보다 커야 한다. (작을 경우 화이트 처리)(%단위 이다.)
	bool m_AlignResultSave;

	bool m_AlignUseMaxHigh; //높이 최대화.
	bool m_AlignImgNotChange;//위치별 Align Mark 변경 유무
	bool m_AlignImgUseCanny;//위치별 Align Mark 변경 유무
	int m_AlignImgUseCanny_threshold1;
	int m_AlignImgUseCanny_threshold2;
	PROC_PAD_ALIGN_AGT()
	{
		m_AlignWidth = 100;
		m_AlignOverWidth = 4;
		m_AlignOKValue = 80;

		m_AlignResultSave = false;
		
		m_AlignUseMaxHigh = false;
		m_AlignImgNotChange = false;
		m_AlignImgUseCanny = false;
		m_AlignImgUseCanny_threshold1 = 0;
		m_AlignImgUseCanny_threshold2 = 70;
	}
	
}PROC_PAD_ALIGN;