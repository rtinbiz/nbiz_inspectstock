#include "StdAfx.h"
#include "GrabberCtrl.h"

extern AOIBigImg G_GrabBigger;
#ifdef MATROX_LINE_SCAN_GRABBER
/*
*	Module Name		:	CGraberCtrl
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	내부 Parameter 초기화.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
CGraberCtrl::CGraberCtrl(void)
{
	imgW			= 0;
	imgH			= 0;

	m_pUserFunction	= nullptr;
	m_pIDTParam		= nullptr;

	m_pImageBuffer	= nullptr;

	m_pUpdateState = nullptr;

	m_pGrabCutter = nullptr;

}
/*
*	Module Name		:	~CGraberCtrl
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	내부 Grab Buffer Clear
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
CGraberCtrl::~CGraberCtrl(void)
{
	if(m_pImageBuffer != nullptr)
	{
		delete [] m_pImageBuffer;
		m_pImageBuffer = nullptr;
	}

	if(m_pGrabCutter != nullptr)
		delete m_pGrabCutter;
	m_pGrabCutter = nullptr;
}
/*
*	Module Name		:	initGrabber
*	Parameter		:	Trigger Function, Grab Setting Parameter.
*	Return			:	없음
*	Function		:	Trigger 신호에 Grab할 필요 Data Setting.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::initGrabber(AOI_ImageCtrlQ * pIDTInterface)
{
	if(pIDTInterface== nullptr)
		return false;

	m_pIDTParam = pIDTInterface;

	m_pGrabCutter = new AOIGrabCutterQ(pIDTInterface, GRAB_CUTTER_BUF);

	if(	m_pUserFunction != nullptr && pIDTInterface != nullptr)
	{
		exitGrabber();//초기화.
	}

	MappAlloc(M_DEFAULT, &App);
#if 0	
	MsysAlloc(M_SYSTEM_HELIOS, M_DEV0, M_DEFAULT, &Sys);
	MdigAlloc(Sys, 0, "D:\\NBIZ_AOI\\DCFs\\HS-80_08k40_full_ext.dcf", M_DEFAULT, &Dig);
#else
	MsysAlloc(M_DEF_SYSTEM_TYPE, M_DEFAULT, M_DEFAULT, &Sys);
	MdigAlloc(Sys, 0, "M_DEFAULT", M_DEFAULT, &Dig);
#endif
	MappControl(M_ERROR, M_PRINT_DISABLE);

	// 보드가 있으면 DCF 파일에서 읽어온다.
	imgW = MdigInquire(Dig, M_SIZE_X, M_NULL);
	imgH = MdigInquire(Dig, M_SIZE_Y, M_NULL);
	if(imgW==0 || imgH == 0)
		return false;

	for( int n = 0; n < BUF_NUM_COUNT; n++)
	{
		MbufAlloc2d(Sys, imgW, imgH, 8+M_UNSIGNED, M_IMAGE+M_GRAB, &Img[n]);
		MbufClear(Img[n], 0);		
	}
	m_pImageBuffer			= new BYTE[imgW*imgH];
	m_pUserFunction			= fn_GrabProcess;

	MbufCreate2d(	(MIL_ID)Sys, 
						(long)imgW, 
						(long)imgH, 
						(long)8+M_UNSIGNED, 
						(BUFATTRTYPE)M_IMAGE+M_PROC, 
						(long)M_HOST_ADDRESS+M_PITCH_BYTE, 
						(long)imgW, 
						(MIL_DATA_PTR)m_pImageBuffer, 
						(MIL_ID MPTYPE *)&MilBufTrans);
	return true;
}
/*
*	Module Name		:	exitGrabber
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Grab 종료시 사용 Data를 모두 Clear 한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::exitGrabber(void)
{
	if(	m_pUserFunction == nullptr || m_pIDTParam == nullptr)
		return false;

	if(m_pImageBuffer != nullptr)
	{
		delete [] m_pImageBuffer;
		m_pImageBuffer = nullptr;
	}

	GrabEnd();

	MbufFree(MilBufTrans);

	for( int n = 0; n < BUF_NUM_COUNT; n++ )
	{
		MbufFree(Img[n]);			
	}

	MdigFree(Dig);
	MsysFree(Sys);
	MappFree(App);

	m_pUserFunction = nullptr;
	return true;
}
/*
*	Module Name		:	GrabStart
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Trigger 신호시 호출 Function Link를 연결한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::GrabStart(int ScanLineNum, int LineScnaWidth)
{
	extern int G_OnlyGrabIndex ;
	G_OnlyGrabIndex =  0;
	if(	m_pIDTParam == nullptr)
		return false;

	//////////////////////////////////////////////////////////////////////////
	m_pGrabCutter->QStartGrab(m_pIDTParam->m_pCoordObj->m_OneLineScnaImgCnt, LineScnaWidth);

	//Grab Trigger 초기화를 위해
	MdigProcess(Dig, Img, BUF_NUM_COUNT, M_STOP , M_DEFAULT, m_pUserFunction, (void*)m_pGrabCutter);//m_pIDTParam);
	
	MdigProcess(Dig, Img, BUF_NUM_COUNT, M_START, M_DEFAULT, m_pUserFunction, (void*)m_pGrabCutter);//m_pIDTParam);
	m_pIDTParam->m_fnQGrabScanLineStart(ScanLineNum);

	if(m_pUpdateState != nullptr)
	{
		m_pUpdateState->m_GrabImageCnt	 = 0;
		m_pUpdateState->m_nOneLineStartTick = ::GetTickCount();

		m_pUpdateState->m_bMilRunGrabFunction = true;
	}
	return true;
}
/*
*	Module Name		:	GrabEnd
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Trigger 신호시 호출 Function Link를 종료한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::GrabEnd(void)
{
	if(	m_pIDTParam == nullptr)
		return false;
	if(m_pIDTParam->m_GrabScanLineNum>0)
		MdigProcess(Dig, Img, BUF_NUM_COUNT, M_STOP , M_DEFAULT, m_pUserFunction, (void*)m_pGrabCutter);//m_pIDTParam);
	else
		return false;

	if(m_pUpdateState != nullptr)
		m_pUpdateState->m_bMilRunGrabFunction = false;

	return true;
}

#endif
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
#ifdef DALSA_LINE_SCAN_GRABBER
/*
*	Module Name		:	CGraberCtrl
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	내부 Parameter 초기화.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
CGraberCtrl::CGraberCtrl(void)
{
	m_pAcq			= NULL;
	m_pAcqDevice	= NULL;
	m_pBuffers		= NULL;
	m_pXfer			= NULL;
	//View			= NULL;

	//char     m_pacqServerName[CORSERVER_MAX_STRLEN], m_configFilename[MAX_PATH];
	//UINT32   acqDeviceNumber;
	m_pacqServerName	= new char[CORSERVER_MAX_STRLEN];
	m_configFilename		= new char[MAX_PATH];

	m_pGrabCutter = nullptr;

	m_ServerNumber		= 1;
	m_acqDeviceNumber = 0;
}
/*
*	Module Name		:	~CGraberCtrl
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	내부 Grab Buffer Clear
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
CGraberCtrl::~CGraberCtrl(void)
{

	if(m_pXfer != nullptr)
	{
		if(m_pXfer->IsGrabbing() == TRUE)
		{
			m_pXfer->Freeze();
			m_pXfer->Wait(5000);
		}
	}
	

	//if(m_pImageBuffer != nullptr)
	//{
	//	delete [] m_pImageBuffer;
	//	m_pImageBuffer = nullptr;
	//}
	if(m_pGrabCutter != nullptr)
		delete m_pGrabCutter;
	m_pGrabCutter = nullptr;
	// Destroy view object
	//if (View && *View && !View->Destroy()) return FALSE;

	// Destroy transfer object
	if (m_pXfer && *m_pXfer && !m_pXfer->Destroy()) return ;

	// Destroy buffer object
	if (m_pBuffers && *m_pBuffers && !m_pBuffers->Destroy()) return ;

	// Destroy acquisition object
	if (m_pAcq && *m_pAcq && !m_pAcq->Destroy()) return ;

	// Destroy acquisition object
	if (m_pAcqDevice && *m_pAcqDevice && !m_pAcqDevice->Destroy()) return ;

	// Delete all objects
	//if (View)		delete View; 
	if (m_pXfer)		delete m_pXfer; 
	if (m_pBuffers)	delete m_pBuffers; 
	if (m_pAcq)		delete m_pAcq; 
	if (m_pAcqDevice)	delete m_pAcqDevice; 
}
/*
*	Module Name		:	initGrabber
*	Parameter		:	Trigger Function, Grab Setting Parameter.
*	Return			:	없음
*	Function		:	Trigger 신호에 Grab할 필요 Data Setting.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::initGrabber(char *pCamInfoFilePath, AOI_ImageCtrlQ * pIDTInterface)
{
	if(pIDTInterface== nullptr)
		return false;

	m_pIDTParam = pIDTInterface;

	m_pGrabCutter = new AOIGrabCutterQ(pIDTInterface, GRAB_CUTTER_BUF);

	if(	/*m_pUserFunction != nullptr &&*/ pIDTInterface != nullptr)
	{
		exitGrabber();//초기화.
	}
	//"D:\\nBiz_InspectStock\\DCFs\\T_HS-S0-12K40-00-R_Default_Default.ccf"
	//if (!GetOptions(1, "D:\\nBiz_InspectStock\\DCFs\\T_HS-S0-12K40-00-R_Default_Default.ccf", m_pacqServerName, &m_acqDeviceNumber, m_configFilename))
	//{
	//	return false;
	//}
	//int serverCount = SapManager::GetServerCount()-1; 
	m_ServerNumber		= 1;
	m_acqDeviceNumber = 0;
	sprintf_s(m_pacqServerName,CORSERVER_MAX_STRLEN, "Xcelera-HS_PX8_1" );
	sprintf_s(m_configFilename,MAX_PATH, pCamInfoFilePath);//"D:\\nBiz_InspectStock\\DCFs\\T_HS-S0-12K40-00-R_Line_Master.ccf" );
	//SapManager::GetServerName(m_ServerNumber, m_pacqServerName, CORSERVER_MAX_STRLEN);
	//int deviceCount = SapManager::GetResourceCount(m_pacqServerName, SapManager::ResourceAcq);
	//int cameraCount = SapManager::GetResourceCount(m_pacqServerName, SapManager::ResourceAcqDevice);
	//char deviceName[CORPRM_GETSIZE(CORACQ_PRM_LABEL)];
	//SapManager::GetResourceName(m_pacqServerName, SapManager::ResourceAcq, m_acqDeviceNumber, deviceName, sizeof(deviceName));
	if (m_pXfer != nullptr)		
		delete m_pXfer; 
	m_pXfer = nullptr; 
	if (m_pBuffers != nullptr)	
		delete m_pBuffers; 
	m_pBuffers = nullptr; 
	if (m_pAcq != nullptr)		
		delete m_pAcq;
	m_pAcq = nullptr; 
	if (m_pAcqDevice != nullptr)	
		delete m_pAcqDevice; 
	m_pAcqDevice = nullptr; 


	SapLocation loc(m_pacqServerName, m_acqDeviceNumber);
	if (SapManager::GetResourceCount(m_pacqServerName, SapManager::ResourceAcq) > 0)
	{
		m_pAcq			= new SapAcquisition(loc, m_configFilename);
		m_pBuffers		= new SapBuffer(BUF_NUM_COUNT, m_pAcq);
		//m_pView		= new SapView(m_pBuffers, SapHwndAutomatic);
		m_pXfer		= new SapAcqToBuf(m_pAcq, m_pBuffers,fn_GrabProcess, this);

		// Create acquisition object
		if (m_pAcq && !*m_pAcq && !m_pAcq->Create())
			return false;

	}

//	if (SapManager::GetResourceCount(m_pacqServerName, SapManager::ResourceAcqDevice) > 0)
//	{
//		m_pAcqDevice	= new SapAcqDevice(loc, FALSE);
//		m_pBuffers		= new SapBufferWithTrash(2, m_pAcqDevice);
////		m_pView		= new SapView(m_pBuffers, SapHwndAutomatic);
//		m_pXfer		= new SapAcqDeviceToBuf(m_pAcqDevice, m_pBuffers,fn_GrabProcess, this);
//
//		// Create acquisition object
//		if (m_pAcqDevice && !*m_pAcqDevice && !m_pAcqDevice->Create())
//			return false;
//
//	}
	// Create buffer object
	if (m_pBuffers && !*m_pBuffers && !m_pBuffers->Create())
		return false;

	// Create transfer object
	if (m_pXfer && !*m_pXfer && !m_pXfer->Create())
		return false;
	// Create view object
	//if (m_pView && !*m_pView && !m_pView->Create())
	//	return false;

	//char SerialPortName[256];
	//m_pAcq->GetSerialPortName(SerialPortName);
	return true;
}
/*
*	Module Name		:	exitGrabber
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Grab 종료시 사용 Data를 모두 Clear 한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::exitGrabber(void)
{
	if(	/*m_pUserFunction == nullptr || */m_pIDTParam == nullptr)
		return false;

	//if(m_pImageBuffer != nullptr)
	//{
	//	delete [] m_pImageBuffer;
	//	m_pImageBuffer = nullptr;
	//}

	GrabEnd();

	return true;
}
/*
*	Module Name		:	GrabStart
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Trigger 신호시 호출 Function Link를 연결한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::GrabStart(int ScanLineNum, int LineScnaWidth)
{
	extern int G_OnlyGrabIndex ;
	G_OnlyGrabIndex =  0;
	if(	m_pIDTParam == nullptr)
		return false;

	//////////////////////////////////////////////////////////////////////////
	m_pGrabCutter->QStartGrab(m_pIDTParam->m_pCoordObj->m_OneLineScnaImgCnt, LineScnaWidth);

	if(m_pXfer->IsGrabbing() == TRUE)
	{
		m_pXfer->Abort();
	}

	m_pXfer->Init(TRUE);
	  // Start continous grab
	if(m_pXfer->Grab() == FALSE)
		return false;
	//if(m_pXfer->Snap(m_pIDTParam->m_pCoordObj->m_OneLineScnaImgCnt) == FALSE)
	//	return false;

	m_pIDTParam->m_fnQGrabScanLineStart(ScanLineNum);

	if(m_pUpdateState != nullptr)
	{

		m_pUpdateState->m_GrabImageCnt	 = 0;
		m_pUpdateState->m_nOneLineStartTick = ::GetTickCount();

		m_pUpdateState->m_bMilRunGrabFunction = true;
	}
	G_GrabBigger.m_NowScanLine = ScanLineNum;
	G_GrabBigger.m_NowImgIndex = 0;
	return true;
}
/*
*	Module Name		:	GrabEnd
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Trigger 신호시 호출 Function Link를 종료한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::GrabEnd(void)
{
	if(	m_pIDTParam == nullptr)
		return false;
	if(m_pXfer != NULL)
	{
		if(m_pXfer->IsGrabbing() == TRUE)//m_pIDTParam->m_GrabScanLineNum>0)
		{
			m_pXfer->Abort();
		}
		else
			return false;
	}
	

	if(m_pUpdateState != nullptr)
		m_pUpdateState->m_bMilRunGrabFunction = false;

	return true;
}


#endif

#if !defined(DALSA_LINE_SCAN_GRABBER) && !defined(MATROX_LINE_SCAN_GRABBER)

/*
*	Module Name		:	CGraberCtrl
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	내부 Parameter 초기화.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
CGraberCtrl::CGraberCtrl(void)
{

	m_pIDTParam		= nullptr;

	m_pImageBuffer	= nullptr;

	m_pUpdateState = nullptr;

	m_pGrabCutter = nullptr;
}
/*
*	Module Name		:	~CGraberCtrl
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	내부 Grab Buffer Clear
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
CGraberCtrl::~CGraberCtrl(void)
{
	if(m_pImageBuffer != nullptr)
	{
		delete [] m_pImageBuffer;
		m_pImageBuffer = nullptr;
	}
	if(m_pGrabCutter != nullptr)
		delete m_pGrabCutter;
	m_pGrabCutter = nullptr;
}
/*
*	Module Name		:	initGrabber
*	Parameter		:	Trigger Function, Grab Setting Parameter.
*	Return			:	없음
*	Function		:	Trigger 신호에 Grab할 필요 Data Setting.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::initGrabber(AOI_ImageCtrlQ * pIDTInterface)
{
	if(pIDTInterface== nullptr)
		return false;

	m_pIDTParam = pIDTInterface;
	m_pGrabCutter = new AOIGrabCutterQ(pIDTInterface, GRAB_CUTTER_BUF);
	if(pIDTInterface != nullptr)
	{
		exitGrabber();//초기화.
	}
	return true;
}
/*
*	Module Name		:	exitGrabber
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Grab 종료시 사용 Data를 모두 Clear 한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::exitGrabber(void)
{
	if(	m_pIDTParam == nullptr)
		return false;

	if(m_pImageBuffer != nullptr)
	{
		delete [] m_pImageBuffer;
		m_pImageBuffer = nullptr;
	}

	GrabEnd();

	return true;
}
/*
*	Module Name		:	GrabStart
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Trigger 신호시 호출 Function Link를 연결한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::GrabStart(int ScanLineNum, int LineScnaWidth)
{
	//extern int G_OnlyGrabIndex ;
	//G_OnlyGrabIndex =  0;
	if(	m_pIDTParam == nullptr)
		return false;
	//////////////////////////////////////////////////////////////////////////
	m_pGrabCutter->QStartGrab(m_pIDTParam->m_pCoordObj->m_OneLineScnaImgCnt, LineScnaWidth);

	m_pIDTParam->m_fnQGrabScanLineStart(ScanLineNum);

	if(m_pUpdateState != nullptr)
	{

		m_pUpdateState->m_GrabImageCnt	 = 0;
		m_pUpdateState->m_nOneLineStartTick = ::GetTickCount();

		m_pUpdateState->m_bMilRunGrabFunction = true;
	}
	G_GrabBigger.m_NowScanLine = ScanLineNum;
	G_GrabBigger.m_NowImgIndex = 0;
	return true;
}
/*
*	Module Name		:	GrabEnd
*	Parameter		:	없음.
*	Return			:	없음
*	Function		:	Trigger 신호시 호출 Function Link를 종료한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CGraberCtrl::GrabEnd(void)
{
	if(	m_pIDTParam == nullptr)
		return false;

	if(m_pUpdateState != nullptr)
		m_pUpdateState->m_bMilRunGrabFunction = false;

	return true;
}
#endif