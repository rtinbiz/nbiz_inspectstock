#include "StdAfx.h"
#include "AOIGrabCutter.h"
#include "WriteFileProc.h"


/*
*	Module Name		:	QCMDCtrlList
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

AOIGrabCutterQ::AOIGrabCutterQ(	AOI_ImageCtrlQ*	pIDTCtrlPtr, int CountNode)
{
	m_tickTickN		= 0;
	m_GrabCountImg	= 0;
	m_GrabTotoalCnt = 0;
	//정보 전달 Queue등록
	m_pIDTCtrlPtr = pIDTCtrlPtr;
	{
		//Debug 용이다.Image 중간에서 지정된 Size만큼의 폭을 가지고 전체 Scan Line Image를 저장 한다.
		m_LineScanImgWidth= 40;
		m_pLineScanImg = nullptr;
	}
	//갯수만큼 Link 구성
	ImgNodeObj *pFirstCrNode = nullptr;
	ImgNodeObj *pLastCrNode = nullptr;
	ImgNodeObj *pCrNode = nullptr;
	for(int CreateStep =0; CreateStep<CountNode; CreateStep++)
	{
		pCrNode =  new ImgNodeObj;
		pCrNode->pImgObj = cvCreateImage(cvSize(m_pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageW, m_pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageH), IPL_DEPTH_8U, 1);

		if(pLastCrNode != nullptr)
		{
			pCrNode->m_pFrontNode = pLastCrNode;
			pCrNode->m_pNextNode = nullptr;

			pLastCrNode->m_pNextNode = pCrNode;
		}
		else
		{
			pFirstCrNode = pCrNode;
			pCrNode->m_pFrontNode = nullptr;
			pCrNode->m_pNextNode = nullptr;
		}

		pLastCrNode = pCrNode;
	}
	pFirstCrNode->m_pFrontNode = pLastCrNode;
	pLastCrNode->m_pNextNode = pFirstCrNode;

	m_pPutNode = pFirstCrNode;

	m_CntNode					= CountNode;

	m_ImgSize	= m_pPutNode->pImgObj->imageSize;
	m_CopySize = m_pIDTCtrlPtr->m_pCoordObj->m_AXIS_Y_OVER_STEP*m_pIDTCtrlPtr->m_pCoordObj->m_HWParam.m_ProcImageW;
	m_UpCpyIndex = m_pPutNode->pImgObj->imageSize  - m_CopySize;
	m_DnCpyIndex = m_CopySize+m_pPutNode->pImgObj->imageSize;
	
	InitializeCriticalSection(&m_CrtSection);
}
AOIGrabCutterQ::~AOIGrabCutterQ()
{
	EnterCriticalSection(&m_CrtSection);
	//QClean();
	{
		ImgNodeObj * pDeleteNode = nullptr;

		for(int DelStep = 0; DelStep<m_CntNode; DelStep++)
		{
			pDeleteNode = m_pPutNode;
			m_pPutNode = m_pPutNode->m_pNextNode;

			if(pDeleteNode->pImgObj != nullptr)
				cvReleaseImage(&pDeleteNode->pImgObj);

			pDeleteNode->pImgObj		= nullptr;
			pDeleteNode->m_pFrontNode	= nullptr;
			pDeleteNode->m_pNextNode	= nullptr;

			delete pDeleteNode;
			pDeleteNode = nullptr;

		}
		m_pPutNode = nullptr;
		m_CntNode = 0;
	}

	{
		//Debug 용이다.Image 중간에서 지정된 Size만큼의 폭을 가지고 전체 Scan Line Image를 저장 한다.
		m_LineScanImgWidth= 40;
		if(m_pLineScanImg != nullptr)
			cvReleaseImage(&m_pLineScanImg);
		m_pLineScanImg = nullptr;
	}
	LeaveCriticalSection(&m_CrtSection);

	DeleteCriticalSection(&m_CrtSection);
}

void AOIGrabCutterQ::QStartGrab(int GrabTotalCnt, int LineScanImgWidth)
{
	if(LineScanImgWidth>5 && LineScanImgWidth<=100)
	{
		if(m_GrabTotoalCnt != GrabTotalCnt || m_LineScanImgWidth != LineScanImgWidth)
		{
			m_LineScanImgWidth = LineScanImgWidth;
			if(m_pLineScanImg != nullptr)
				cvReleaseImage(&m_pLineScanImg);
			m_pLineScanImg = cvCreateImage(cvSize(m_LineScanImgWidth, GrabTotalCnt*m_pPutNode->pImgObj->height), IPL_DEPTH_8U, 1);
		}
		cvZero(m_pLineScanImg);
		
	}
	else
	{
		if(m_pLineScanImg != nullptr)
			cvReleaseImage(&m_pLineScanImg);
		m_pLineScanImg = nullptr;
		m_LineScanImgWidth = 0;
	}

	QDataReset();
	m_GrabCountImg = 0;
	m_GrabTotoalCnt = GrabTotalCnt;



}

BYTE*	AOIGrabCutterQ::QGetImgPtr()
{
	if(m_pPutNode == nullptr)
		return nullptr;
	if(m_pPutNode->pImgObj == nullptr)
		return nullptr;

	return (BYTE*)m_pPutNode->pImgObj->imageData;
}

BYTE* pOrgImgData	= nullptr;

BYTE* pUpImage		= nullptr;
BYTE* pTgImage		= nullptr;
BYTE* pDnImage		= nullptr;
////cvCopyImage(pTargetNode->pImgObj, m_pGrabIMG->m_pGrabOrgImage);

void AOIGrabCutterQ::QSendTarget(ImgNodeObj *pTargetNode)
{
	//EnterCriticalSection(&m_CrtSection);
	m_pGrabIMG	= m_pIDTCtrlPtr->m_fnQGetInspectPTR_Grabber();//적재할 Queue의 대상위치를 가져온다.
	if(m_pGrabIMG != nullptr )
	{
		m_pIDTCtrlPtr->m_pUpdateState->m_nNoneProcImgCount = 0;

		pOrgImgData	= (BYTE*)m_pGrabIMG->m_pGrabOrgImage->imageData;

		if(m_LineScanImgWidth>5 && m_LineScanImgWidth<=100 && m_pLineScanImg != NULL)
		{
			CvRect GrabImgRoiRect, BufImgRoiRect;
			GrabImgRoiRect.x = (pTargetNode->pImgObj->width/2)-(m_LineScanImgWidth/2);
			GrabImgRoiRect.y = 0;
			GrabImgRoiRect.width = m_LineScanImgWidth;
			GrabImgRoiRect.height = pTargetNode->pImgObj->height;

			BufImgRoiRect = GrabImgRoiRect;
			BufImgRoiRect.x = 0;
			BufImgRoiRect.y = pTargetNode->pImgObj->height*m_pIDTCtrlPtr->m_GrabScanImageCnt;

			cvSetImageROI(pTargetNode->pImgObj, GrabImgRoiRect);
			cvSetImageROI(m_pLineScanImg, BufImgRoiRect);

			cvCopyImage(pTargetNode->pImgObj,m_pLineScanImg);
			cvResetImageROI(m_pLineScanImg);
			cvResetImageROI(pTargetNode->pImgObj);			
		}
		pUpImage		= (BYTE*)(pTargetNode->m_pFrontNode->pImgObj->imageData+m_UpCpyIndex);
		pTgImage		= (BYTE*) pTargetNode->pImgObj->imageData;
		pDnImage		= (BYTE*) pTargetNode->m_pNextNode->pImgObj->imageData;

		m_pIDTCtrlPtr->m_GrabScanImageCnt +=1;
		memcpy(pOrgImgData,						pUpImage,		m_CopySize);
		memcpy(pOrgImgData+m_CopySize,			pTgImage,		m_ImgSize);
		memcpy(pOrgImgData+m_DnCpyIndex,			pDnImage,		m_CopySize);
		
		m_pGrabIMG->m_nOneTick				= ::GetTickCount();
		m_pGrabIMG->m_ScanLine				= m_pIDTCtrlPtr->m_GrabScanLineNum;
		m_pGrabIMG->m_ScanImageIndex		= m_pIDTCtrlPtr->m_GrabScanImageCnt;

		m_pGrabIMG->m_fnQSetProcessEnd(QDATA_PROC_END_GRAB);//현 Image는 Grabber에서 Grab을 종료 했다.

		//if(G_pUpdateState != nullptr)
		{//첫장은 오류, 1번에서 2번까지의 시간을 잰다.
			m_pIDTCtrlPtr->m_pUpdateState->m_GrabImageCnt++;
			m_pIDTCtrlPtr->m_pUpdateState->m_nMilGrabFunProTick = m_pGrabIMG->m_nOneTick - m_tickTickN;
			m_tickTickN = m_pGrabIMG->m_nOneTick;
		}
	}
	else
	{//심각한오류 Grab된 Data를 쓸곳이 없다.ㅡ,.ㅡ;;;

		//if(G_pUpdateState != nullptr)
		{
			m_pIDTCtrlPtr->m_pUpdateState->m_bMilGrabFunMemError = true;
			m_pIDTCtrlPtr->m_pUpdateState->m_nNoneProcImgCount++;

			::SendMessage(m_pIDTCtrlPtr->m_pUpdateState->m_UpdateUIHandle, WM_USER_STATE_UPDATE, nBiz_AOI_STATE_ALARM, ALARM_CODE_GrabBuffer);
		}			
	}
	//LeaveCriticalSection(&m_CrtSection);
}
void AOIGrabCutterQ::SaveLineScanImg()
{
	if(m_LineScanImgWidth>5 && m_LineScanImgWidth<=100 && m_pLineScanImg != NULL)
	{
		char strSaveImgBuf[MAX_PATH];
		char strSaveImgBuf2[MAX_PATH];
		sprintf_s(strSaveImgBuf,"%sFullLineImg_Line%d.bmp", LINE_SCAN_IMG_PATH, (m_pIDTCtrlPtr->m_GrabScanLineNum));
		cvSaveImage(strSaveImgBuf, m_pLineScanImg);
		int NameSize = (int)strlen(strSaveImgBuf);
		sprintf_s(strSaveImgBuf2, "%s",strSaveImgBuf);
		strSaveImgBuf2[NameSize-3] = 'c';
		strSaveImgBuf2[NameSize-2] = 'l';
		strSaveImgBuf2[NameSize-1] = 'x';

		CFile::Rename(strSaveImgBuf, strSaveImgBuf2);
	}
}
void AOIGrabCutterQ::QProcImgData()
{
	EnterCriticalSection(&m_CrtSection);

	if(m_GrabCountImg>0)//대상이 Grab되면 이전것을 Inspect에 투입.(0번째 Index는 Skip)
		QSendTarget(m_pPutNode->m_pFrontNode);
	m_GrabCountImg+=1;

	if(m_GrabTotoalCnt == m_GrabCountImg)
	{//마지막것은 바로 연산 투입
		//cvZero(m_pPutNode->m_pNextNode->pImgObj);
		memset(m_pPutNode->m_pNextNode->pImgObj->imageData, 0xFF, m_pPutNode->m_pNextNode->pImgObj->imageSize);
		QSendTarget(m_pPutNode);
	}

	m_pPutNode = m_pPutNode->m_pNextNode;
	LeaveCriticalSection(&m_CrtSection);
}

void AOIGrabCutterQ::QDataReset()
{
	EnterCriticalSection(&m_CrtSection);
	for(int RsStep = 0; RsStep<m_CntNode; RsStep++)
	{
		cvZero(m_pPutNode->pImgObj);
		m_pPutNode = m_pPutNode->m_pNextNode;
	}
	LeaveCriticalSection(&m_CrtSection);
}

void AOIGrabCutterQ::QClean(void)
{
	EnterCriticalSection(&m_CrtSection);
	{
		ImgNodeObj * pDeleteNode = nullptr;

		for(int DelStep = 0; DelStep<m_CntNode; DelStep++)
		{
			pDeleteNode = m_pPutNode;
			m_pPutNode = m_pPutNode->m_pNextNode;

			if(pDeleteNode->pImgObj != nullptr)
				cvReleaseImage(&pDeleteNode->pImgObj);

			pDeleteNode->pImgObj		= nullptr;
			pDeleteNode->m_pFrontNode	= nullptr;
			pDeleteNode->m_pNextNode	= nullptr;

			delete pDeleteNode;
			pDeleteNode = nullptr;

		}
		m_pPutNode = nullptr;
		m_CntNode = 0;
	}
	LeaveCriticalSection(&m_CrtSection);
}



/*
*	Module Name		:	AOIBigImg
*	Parameter		:	
*	Return			:	
*	Function		:	
*	Create			:	2015.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
AOIBigImg::AOIBigImg()
{
	m_pBigImg = nullptr;
	m_pOrgImgBuf = nullptr;
	m_pReSizeBuf = nullptr;

	m_bBigDataGrab = false;

	m_StartMPosX = 1000;
	m_StartMPosY = 1000;
	m_PixelResolution = 5.0;
}
AOIBigImg::~AOIBigImg()
{
	
	if(m_pReSizeBuf!= nullptr)
			cvReleaseImage(&m_pReSizeBuf);
	m_pReSizeBuf = nullptr;

	if(m_pOrgImgBuf!= nullptr)
		cvReleaseImage(&m_pOrgImgBuf);
	m_pOrgImgBuf = nullptr;

	if(m_pBigImg!= nullptr)
		cvReleaseImage(&m_pBigImg);
	m_pBigImg = nullptr;

}
void AOIBigImg::SetOffset(int ShiftOverlapOffsetX_um, int OddScanOffset_um, int EvenScanOffset_um, double PixelResolution)
{
	m_ShiftOverlapOffsetX = (int)((ShiftOverlapOffsetX_um/PixelResolution)/10.0); 
	m_OddScanOffset = (int)((OddScanOffset_um/PixelResolution)/10.0);
	m_EvenScanOffset = (int)((EvenScanOffset_um/PixelResolution)/10.0)-1;
}
void AOIBigImg::SetCoordPos(int StartMPosX, int StartMPosY, int CenterPosX, int CenterPosY, double PixelResolution)
{
	m_StartMPosX = StartMPosX;
	m_StartMPosY = StartMPosY;
	m_CenterPosX = CenterPosX;
	m_CenterPosY = CenterPosY;

	m_PixelResolution = PixelResolution;
}

void AOIBigImg::SetImageSize(int nWidth, int nHeight, int nTotalLine, int nTotalScanImg)
{
	if(m_bBigDataGrab == false)
		return;

	m_nTotalLine = nTotalLine;
	m_nTotalScanImg = nTotalScanImg;
	m_nWidth = nWidth/10;
	m_nHeight = nHeight/10;
	if(m_pBigImg!= nullptr)
		cvReleaseImage(&m_pBigImg);
	m_pBigImg = cvCreateImage(cvSize(m_nWidth*(m_nTotalLine+1), m_nHeight*(m_nTotalScanImg+2)), IPL_DEPTH_8U, 1);
	cvZero(m_pBigImg);

	if(m_pOrgImgBuf!= nullptr)
		cvReleaseImage(&m_pOrgImgBuf);
	m_pOrgImgBuf = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 1);
	cvZero(m_pOrgImgBuf);

	if(m_pReSizeBuf!= nullptr)
		cvReleaseImage(&m_pReSizeBuf);
	m_pReSizeBuf = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	cvZero(m_pReSizeBuf);
	
}

void AOIBigImg::SetBigImage(char *pGrabImg)
{
	m_NowImgIndex++;
	memcpy(m_pOrgImgBuf->imageData, pGrabImg, m_pOrgImgBuf->imageSize);
	cvResize(m_pOrgImgBuf, m_pReSizeBuf);

	int CenterOffsetX = m_pReSizeBuf->width/2;
	int CenterOffsetY = m_pReSizeBuf->height;

	int ShiftOffset = (m_NowScanLine-1)*m_ShiftOverlapOffsetX;
	
	CvRect rCPYPosArea;
	rCPYPosArea.x = (m_NowScanLine-1)*m_nWidth+CenterOffsetX - ShiftOffset;
	
	if(m_NowScanLine%2==1)//홀수
		rCPYPosArea.y = ((m_NowImgIndex-1)*m_nHeight) +CenterOffsetY+m_OddScanOffset;
	else//짝수
	{
		rCPYPosArea.y = ((m_nTotalScanImg - m_NowImgIndex)*m_nHeight) +CenterOffsetY+m_EvenScanOffset;
		cvFlip(m_pReSizeBuf,0);
	}

	rCPYPosArea.width = m_nWidth;
	rCPYPosArea.height = m_nHeight;
	
	cvSetImageROI(m_pBigImg, rCPYPosArea);
	cvCopyImage(m_pReSizeBuf, m_pBigImg);
	cvResetImageROI(m_pBigImg);
}
int AOIBigImg::InspectionBigImage(SAOI_IMAGE_INFO stImageInfo, SAOI_COMMON_PARA stCommonPara, COORD_DINFO *pCoordinateRetList)
{
	stCommonPara.nImageSizeX = m_pBigImg->width;
	stCommonPara.nImageSizeY = m_pBigImg->height;
//	CMacroInspect InpectBigImg;
	DEFECT_DATA DefectDataBuf[MAX_DEFECT];
	memset(DefectDataBuf, 0x00, sizeof(DEFECT_DATA)*MAX_DEFECT);

	int DefectCnt =  InpectBigImg.m_fnSearchDefect(m_pBigImg, stImageInfo, stCommonPara, DefectDataBuf, m_PixelResolution);

	int  BlackOffsetX = m_pReSizeBuf->width/2;
	int  BlackOffsetY = m_pReSizeBuf->height;

	for(int WriteStep = 0; WriteStep<DefectCnt; WriteStep++)
	{
		pCoordinateRetList->DefectType = DefectDataBuf[WriteStep].DefectType;
		//Macine 좌표계
		pCoordinateRetList[WriteStep].CoodStartX		= m_StartMPosX -(int)((float)(6000*m_PixelResolution))+ (int)((float)((DefectDataBuf[WriteStep].StartX-BlackOffsetX)*10)*m_PixelResolution);
		pCoordinateRetList[WriteStep].CoodStartY		= m_StartMPosY /*+m_OddScanOffset*/+ (int)((float)((DefectDataBuf[WriteStep].StartY-BlackOffsetY)*10)*m_PixelResolution);
		//원점 좌표계
//		pCoordinateRetList[WriteStep].CoodStartX		= pCoordinateRetList[WriteStep].CoodStartX - m_CenterPosX;
	//	pCoordinateRetList[WriteStep].CoodStartY		= m_CenterPosY - pCoordinateRetList[WriteStep].CoodStartY;

		pCoordinateRetList[WriteStep].CoodWidth			=  (int)((float)((abs(DefectDataBuf[WriteStep].EndX - DefectDataBuf[WriteStep].StartX)*10)+1)*m_PixelResolution);
		pCoordinateRetList[WriteStep].CoodHeight		=  (int)((float)((abs(DefectDataBuf[WriteStep].EndY - DefectDataBuf[WriteStep].StartY)*10)+1)*m_PixelResolution);

		pCoordinateRetList[WriteStep].nDefectCount		= DefectDataBuf[WriteStep].nDefectCount;
		pCoordinateRetList[WriteStep].nDValueMin		= DefectDataBuf[WriteStep].nDValueMin;
		pCoordinateRetList[WriteStep].nDValueMax		= DefectDataBuf[WriteStep].nDValueMax;
		pCoordinateRetList[WriteStep].nDValueAvg		= DefectDataBuf[WriteStep].nDValueAvg;


	}
        fn_WriteFileProcess_BigImg(pCoordinateRetList,&DefectDataBuf[0],DefectCnt);	
	return DefectCnt;
}

void AOIBigImg::SaveBigImge(CString strFullPath)
{
	//if(m_bBigDataGrab == false)
	//	return;

	if(m_pBigImg != nullptr)
	cvSaveImage(strFullPath.GetBuffer(strFullPath.GetLength()), m_pBigImg);
}

void AOIBigImg::PutTextInImageDefect_BigImg(IplImage *pMainMap, CvPoint *pNamePos,char*text)
{
	CvSize text_size; //폰트 사이즈를 저장해놓기 위한!
	int    baseline;
	cvGetTextSize( text, &G_DefectFont, &text_size, &baseline ); 
	CvPoint StartTxt, EndTxt;
	StartTxt.x	= pNamePos->x-5;
	StartTxt.y	= pNamePos->y+baseline;
	EndTxt.x	= pNamePos->x+text_size.width+5;
	EndTxt.y	= pNamePos->y-(text_size.height+baseline);
	CvRect TxtRect;
	TxtRect.x = StartTxt.x;
	TxtRect.y = StartTxt.y;
	TxtRect.width = EndTxt.x - StartTxt.x;
	TxtRect.height = EndTxt.y - StartTxt.y;

	CvPoint imgb_rectangle_pts[4] = {	cvPoint(TxtRect.x, TxtRect.y),
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y), 
		cvPoint(TxtRect.x+TxtRect.width, TxtRect.y+TxtRect.height),
		cvPoint(TxtRect.x, TxtRect.y+TxtRect.height)};
	CvPoint *PointArray[2] = { & imgb_rectangle_pts[0]  , 0 } ;
	int PolyVertexNumber[2] = { 4 , 0 } ;
	int PolyNumber = 1 ;
	cvFillPoly(pMainMap , PointArray, PolyVertexNumber, PolyNumber, CV_RGB(0, 0, 0));

	cvPutText(pMainMap, text, *pNamePos, &G_DefectFont, CV_RGB(255,255,255));

	//cvRectangle(pMainMap, StartTxt, EndTxt, CV_RGB(255, 0, 0));
}

UINT AOIBigImg::fn_WriteFileProcess_BigImg(COORD_DINFO *pCoordinateRetList1,DEFECT_DATA *pDefectDataBuf,int WriteStep)
{
	/*PROC_RETWRITE	*		pProcParam;

	PROC_STATE	*			pUpdateState;

	CDefectFileFormat*		pWriteFileObj;
	CInterServerInterface *	pSInterface	;
	DefectDataQ	*			pResultDefectQ;

	CoordinatesProc*		pCoordObj;

	ImageSaveQ	*		    pHistogramImgQ	;
	ImageSaveQ	*		    pInspectSaveImgQ;
	IMGNodeObj	*			pSaveNode		= nullptr;*/

	int DefectReviewSizeW=500;
	int DefectReviewSizeH=500;
	int ReviewRectOffset = 5;
	DEFECT_DATA DefectDataBuf[MAX_DEFECT];
	memset(DefectDataBuf, 0x00, sizeof(DEFECT_DATA)*MAX_DEFECT);
	memcpy(&DefectDataBuf[0],pDefectDataBuf,sizeof(DEFECT_DATA)*MAX_DEFECT);

	if(WriteStep >MAX_DEFECT)
		WriteStep=MAX_DEFECT;
	COORD_DINFO pCoordinateRetList[MAX_DEFECT];
	memset(pCoordinateRetList, 0x00, sizeof(DEFECT_DATA)*MAX_DEFECT);
	memcpy(&pCoordinateRetList[0],pCoordinateRetList1,sizeof(COORD_DINFO)*WriteStep);
	//////////////////////////////////////////////////////////////////////////
	IplImage	*				pRevewImg	= cvCreateImage( cvSize(DefectReviewSizeW, DefectReviewSizeH), IPL_DEPTH_8U, 1);
	IplImage	*				pRevewImgTemp1	= cvCreateImage( cvSize(m_pBigImg->width, m_pBigImg->height), IPL_DEPTH_8U, 1);
	IplImage	*				pRevewImgTemp	= cvCreateImage( cvSize(m_pBigImg->width, m_pBigImg->height), IPL_DEPTH_8U, 3);
	IplImage	*				pSaveSmallImg	= cvCreateImage( cvSize(DefectReviewSizeW, DefectReviewSizeH), IPL_DEPTH_8U, 3);
	IplImage	*				pSave2DImg	= cvCreateImage( cvSize(m_pBigImg->width, m_pBigImg->height), IPL_DEPTH_8U, 3);
	//cvZero(pRevewImg);
	cvCvtColor(m_pBigImg, pRevewImgTemp,CV_GRAY2RGB);
	cvCopyImage(m_pBigImg, pRevewImgTemp1);
	memset(pRevewImg->imageData, 0xFF, pRevewImg->imageSize);
	COORD_DINFO				CoordData;
	int						ReviewImgSize = 0;
//	BYTE	 *				pReviewImgData =(BYTE *) pRevewImg->imageData;
	//////////////////////////////////////////////////////////////////////////
	extern int				G_MasterTaskIndex;
	//////////////////////////////////////////////////////////////////////////
	//Defect Info Size
	CString ImgText;
	CvPoint TextPos,  TextPos2, TextPos3, TextPos4, TextPos5;

	TextPos = cvPoint(5,10);
	TextPos2 = cvPoint(5,25);
	TextPos3 = cvPoint(5,40);
	TextPos4 = cvPoint(5,55);
	TextPos5 = cvPoint(5,70);
	cvInitFont (&G_DefectFont, CV_FONT_HERSHEY_SIMPLEX , 0.2f, 0.5f, 0.0f, 1, CV_AA);//CV_FONT_HERSHEY_TRIPLEX

	char SaveImageName[256];
	int DefectType = 0;
	int ScanLineNum = 0;
	int ScanImgCount = 0;

	int DefectIndex = 0;

	CvRect DrawDefectRect;
	CvPoint StarPos, EndPos;

	//CvvImage ViewImage;
	//ViewImage.Create(pRevewImg->width, pRevewImg->height, ((IPL_DEPTH_8U & 255)*3) );
	//////////////////////////////////////////////////////////////////////////////////
	cvCvtColor( InpectBigImg.mp_iplResultBuff, pSave2DImg, CV_GRAY2RGB );

	for (int i=0; i<WriteStep; i++)
	{
		CvRect Calc_OrgArea;
		CvRect Calc_CutImgArea;

		Calc_CutImgArea.x			= DefectDataBuf[i].StartX-(DefectReviewSizeW/2);
		Calc_CutImgArea.y			= DefectDataBuf[i].StartY-(DefectReviewSizeH/2);
		Calc_CutImgArea.width		= DefectReviewSizeW;
		Calc_CutImgArea.height	    = DefectReviewSizeH;

		Calc_OrgArea.x			= 0;
		Calc_OrgArea.y			= 0;
		Calc_OrgArea.width		= DefectReviewSizeW;
		Calc_OrgArea.height	    = DefectReviewSizeH;

		if((DefectDataBuf[i].StartX-(DefectReviewSizeW/2))<0)
		{
			Calc_CutImgArea.x=0;
			Calc_CutImgArea.width=DefectReviewSizeW-(abs(DefectDataBuf[i].StartX-(DefectReviewSizeW/2)));

			Calc_OrgArea.x=abs(DefectDataBuf[i].StartX-(DefectReviewSizeW/2));
			Calc_OrgArea.width	= DefectReviewSizeW-(abs(DefectDataBuf[i].StartX-(DefectReviewSizeW/2)));
		}
		if((DefectDataBuf[i].StartX-(DefectReviewSizeH/2))<0)
		{
			Calc_CutImgArea.y=0;
			Calc_CutImgArea.height=DefectReviewSizeH-(abs(DefectDataBuf[i].StartY-(DefectReviewSizeH/2)));

			Calc_OrgArea.y			= abs(DefectDataBuf[i].StartY-(DefectReviewSizeH/2));
			Calc_OrgArea.height	    = DefectReviewSizeH-(abs(DefectDataBuf[i].StartY-(DefectReviewSizeH/2)));
		}


		cvSetImageROI(pRevewImgTemp1, Calc_CutImgArea);
		cvSetImageROI(pRevewImg, Calc_OrgArea);
		cvCopyImage(pRevewImgTemp1, pRevewImg);
		cvResetImageROI(pRevewImgTemp1);
		cvResetImageROI(pRevewImg);

		//if(bSizeError == true)//Size Zero가 들어 왔던 Defect에 대해서는 Result를 임시로 반전 시키다.(Debugging용이다.)
		//	cvNot(pSaveImageBuffer,pSaveImageBuffer);

		//memcpy(pRetImgBuf, pSaveImageBuffer->imageData, pSaveImageBuffer->imageSize);

		//memset(pSaveImageBuffer->imageData, 0xAF, pSaveImageBuffer->imageSize);
	
		//////////////////////////////////////////////////////////////////////////////////

	/*	sprintf_s(SaveImageName, "R:\\AOI_Result\\BigImage_Cut_%02d.jpg", WriteStep);

	
		ImgText.Format("C X:%8d, Y:%8d", DefectDataBuf[i].StartX, DefectDataBuf[i].StartY);
		PutTextInImageDefect(pSave2DImg, &TextPos, ImgText.GetBuffer(ImgText.GetLength()));
		ImgText.Format("M X:%8d, Y:%8d", (pCoordinateRetList[i].CoodStartX+pCoordinateRetList[i].CoodWidth/2),
		(pCoordinateRetList[i].CoodStartY+pCoordinateRetList[i].CoodHeight/2));
		PutTextInImageDefect(pSave2DImg, &TextPos2, ImgText.GetBuffer(ImgText.GetLength()));

		ImgText.Format("D X:%8d, Y:%8d",  (pCoordinateRetList[i].CoodWidth), (pCoordinateRetList[i].CoodHeight));
		PutTextInImageDefect(pSave2DImg, &TextPos3, ImgText.GetBuffer(ImgText.GetLength()));

		ImgText.Format("P X:%8d, Y:%8d",	DefectDataBuf[i].StartX+ abs(DefectDataBuf[i].StartX- DefectDataBuf[i].EndX)/2, 
		DefectDataBuf[i].StartY+ abs(DefectDataBuf[i].StartY- DefectDataBuf[i].EndY)/2);

		PutTextInImageDefect(pSave2DImg, &TextPos4, ImgText.GetBuffer(ImgText.GetLength()));
		*/	


		//TextPos5.y = pSave2DImg->height-10;
		//ImgText.Format("Min:%3d, Max:%3d, Avg:%3d, Cnt:%4d",	pCoordinateRetList->nDValueMin, pCoordinateRetList->nDValueMax, pCoordinateRetList->nDValueAvg, pCoordinateRetList->nDefectCount);
		//PutTextInImageDefect(pSave2DImg, &TextPos5, ImgText.GetBuffer(ImgText.GetLength()));



			cvCvtColor( pRevewImg, pSaveSmallImg, CV_GRAY2RGB );

				//sprintf_s(SaveImageName, "R:\\AOI_Rsult\\BigImage_Cut_%02d.jpg", i);
				sprintf_s(SaveImageName, "Z:\\[04_Master]\\BigImage_Cut_%02d.jpg", i);
						

				ImgText.Format("C X:%8d, Y:%8d", DefectDataBuf[i].StartX, DefectDataBuf[i].StartY);
				PutTextInImageDefect(pSaveSmallImg, &TextPos, ImgText.GetBuffer(ImgText.GetLength()));
				ImgText.Format("M X:%8d, Y:%8d", (pCoordinateRetList[i].CoodStartX/*+pCoordinateRetList[i].CoodWidth/2*/),
					(pCoordinateRetList[i].CoodStartY/*+pCoordinateRetList[i].CoodHeight/2*/));
				PutTextInImageDefect(pSaveSmallImg, &TextPos2, ImgText.GetBuffer(ImgText.GetLength()));

				ImgText.Format("D X:%8d, Y:%8d",  (pCoordinateRetList[i].CoodWidth), (pCoordinateRetList[i].CoodHeight));
				PutTextInImageDefect(pSaveSmallImg, &TextPos3, ImgText.GetBuffer(ImgText.GetLength()));

				ImgText.Format("P X:%8d, Y:%8d",	DefectDataBuf[i].StartX+ abs(DefectDataBuf[i].StartX- DefectDataBuf[i].EndX)/2, 
					DefectDataBuf[i].StartY+ abs(DefectDataBuf[i].StartY- DefectDataBuf[i].EndY)/2);

				PutTextInImageDefect(pSaveSmallImg, &TextPos4, ImgText.GetBuffer(ImgText.GetLength()));


			/*	StarPos.x =abs((DefectReviewSizeW/2-pCoordinateRetList[i].CoodWidth/2)-ReviewRectOffset);
				StarPos.y =abs((DefectReviewSizeH/2-pCoordinateRetList[i].CoodHeight/2)-ReviewRectOffset);

				EndPos.x = StarPos.x+pCoordinateRetList[i].CoodWidth+ReviewRectOffset*2;
				EndPos.y = StarPos.y+pCoordinateRetList[i].CoodHeight+ReviewRectOffset*2;*/
			
				StarPos.x =(DefectReviewSizeW/2-pCoordinateRetList[i].CoodWidth/2)-ReviewRectOffset;
				StarPos.y =(DefectReviewSizeH/2-pCoordinateRetList[i].CoodHeight/2)-ReviewRectOffset;

				EndPos.x = StarPos.x+pCoordinateRetList[i].CoodWidth+ReviewRectOffset*2;
				EndPos.y = StarPos.y+pCoordinateRetList[i].CoodHeight+ReviewRectOffset*2;

				cvDrawRect(pSaveSmallImg, StarPos, EndPos,CV_RGB(0,255,0), 2);

				cvSaveImage(SaveImageName, pSaveSmallImg);

				//TextPos5.y = pSaveSmallImg->height-10;
				//ImgText.Format("Min:%3d, Max:%3d, Avg:%3d, Cnt:%4d",	pCoordinateRetList->nDValueMin, pCoordinateRetList->nDValueMax, pCoordinateRetList->nDValueAvg, pCoordinateRetList->nDefectCount);
				//PutTextInImageDefect(pSaveSmallImg, &TextPos5, ImgText.GetBuffer(ImgText.GetLength()));


				DrawDefectRect.x = DefectDataBuf[i].StartX -( pCoordinateRetList[i].CoodWidth/2);
				DrawDefectRect.y = DefectDataBuf[i].StartY- (pCoordinateRetList[i].CoodHeight/2);

				DrawDefectRect.width	= abs(DefectDataBuf[i].StartX- DefectDataBuf[i].EndX);
				DrawDefectRect.height	= abs(DefectDataBuf[i].StartY- DefectDataBuf[i].EndY);
				DrawDefectRect.x			-= DrawDefectRect.width/2;
				DrawDefectRect.y		-= DrawDefectRect.height/2;

				DrawDefectRect.x			-= ReviewRectOffset;
				DrawDefectRect.y		-= ReviewRectOffset;
				DrawDefectRect.width	+= ReviewRectOffset*2;
				DrawDefectRect.height	+= ReviewRectOffset*2;

				if(DrawDefectRect.x<0)
					DrawDefectRect.x = 2;
				if(DrawDefectRect.y<0)
					DrawDefectRect.y = 2;

				
				if(pCoordinateRetList[i].CoodWidth == 0 && pCoordinateRetList[i].CoodHeight == 0)
				{
					StarPos.x = DrawDefectRect.x;
					StarPos.y = DrawDefectRect.y;

					EndPos.x = DrawDefectRect.x+DrawDefectRect.width;
					EndPos.y =  DrawDefectRect.y+DrawDefectRect.height;


					if(EndPos.x > DefectReviewSizeW)
						EndPos.x = DefectReviewSizeW;
					if(EndPos.y > DefectReviewSizeH)
						EndPos.y = DefectReviewSizeH;

					cvDrawRect(pSave2DImg, StarPos, EndPos,CV_RGB(0,255,0), 3);
					cvDrawRect(pRevewImgTemp, StarPos, EndPos,CV_RGB(255,0,0), 3);
					//cvDrawRect(pSaveSmallImg, StarPos, EndPos,CV_RGB(0,255,0), 1);
				}
				else
				{
					//int WSmallSize = pCoordinateRetList->CoodWidth;
					//int HSmallSize = pCoordinateRetList->CoodHeight;
					//if(WSmallSize>DefectReviewSizeW)
					//	WSmallSize = DefectReviewSizeW;
					//if(HSmallSize>DefectReviewSizeH)
					//	HSmallSize = DefectReviewSizeH;

					//StarPos.x = (DefectReviewSizeW/2) - (WSmallSize/2) ;
					//StarPos.y =  (DefectReviewSizeH/2) - (HSmallSize/2) ;

					//EndPos.x = StarPos.x+ WSmallSize;
					//EndPos.y = StarPos.y+ HSmallSize;
					DrawDefectRect.x = DefectDataBuf[i].StartX -( pCoordinateRetList[i].CoodWidth/2);
					DrawDefectRect.y = DefectDataBuf[i].StartY- (pCoordinateRetList[i].CoodHeight/2);
					//DrawDefectRect.width	= abs(DefectDataBuf[i].StartX- DefectDataBuf[i].EndX);
					//DrawDefectRect.height	= abs(DefectDataBuf[i].StartY- DefectDataBuf[i].EndY);
					DrawDefectRect.width	= (DefectDataBuf[i].EndX- DefectDataBuf[i].StartX);
					DrawDefectRect.height	= (DefectDataBuf[i].EndX- DefectDataBuf[i].StartX);

					StarPos.x = DrawDefectRect.x;
					StarPos.y = DrawDefectRect.y;

					EndPos.x = DrawDefectRect.x+DrawDefectRect.width;
					EndPos.y =  DrawDefectRect.y+DrawDefectRect.height;

					cvDrawRect(pSave2DImg, StarPos, EndPos,CV_RGB(255,0,0), 3);
					cvDrawRect(pRevewImgTemp, StarPos, EndPos,CV_RGB(255,0,0), 3);
					//cvDrawRect(pSaveSmallImg, StarPos, EndPos,CV_RGB(255,0,0), 1);
				}	
				Sleep(5);
	}
	sprintf_s(SaveImageName, "Z:\\[04_Master]\\BigImage_org2d.jpg");
	cvSaveImage(SaveImageName, pSave2DImg);
	sprintf_s(SaveImageName, "Z:\\[04_Master]\\BigImage_org.jpg");
	cvSaveImage(SaveImageName, pRevewImgTemp);
//			cvSaveImage(SaveImageName, pSaveSmallImg);

			//ViewImage.CopyOf(pSaveSmallImg);
			//ViewImage.DrawToHDC(*pUpdateState->m_pHDcView, pUpdateState->m_ViewRect);

		
/*				
		DWORD nProcFileUpdateTime =  GetTickCount();

		pUpdateState->m_bBlockScanEnd = false; 
				
		CString CopyTargetFile;
		CString UpdateFilePath;
		//Block End 후 처리를 진행 한다.
		if(pWriteFileObj->m_fnIsOpened() == true)
		{
			CopyTargetFile.Format("%s", pWriteFileObj->m_ImageFilePath);
			UpdateFilePath.Format("%s", pWriteFileObj->m_ImageFileUpdatePath);
			pWriteFileObj->m_fnCloseFileObj();
		}

		//Defect Img File Write Start를 알린다.
		pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_FileAccess, nBiz_Recv_DefectWriteStart, 0);
		//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
		//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM, nBiz_AOI_FileAccess, nBiz_Recv_DefectWriteStart, 0);
		
		pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Defect Write Start");
		if(CopyTargetFile.GetLength()>3)
		{
			pUpdateState->m_bDefectFileCopy = true;
			if(::CopyFile(CopyTargetFile, UpdateFilePath,  FALSE) == TRUE)
				::DeleteFile(CopyTargetFile);
			pUpdateState->m_bDefectFileCopy = false;
		}
		//Defect Img File Write End를 알린다.
		pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_FileAccess, nBiz_Recv_DefectWriteEnd, 0);
		//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
		//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM, nBiz_AOI_FileAccess, nBiz_Recv_DefectWriteEnd, 0);

		pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Defect Write End");

		//File Backup Start.
		pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_FileAccess, nBiz_Recv_BackupStart, 0);
		//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
		//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM, nBiz_AOI_FileAccess, nBiz_Recv_BackupStart, 0);

		pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Backup Start");
		{
			while(pInspectSaveImgQ->QGetCnt() > 0)
				Sleep( 10 );
			Sleep( 100);
			pUpdateState->m_bOrgImageBackup = true;
			FolderCopyFromAToB(pCoordObj->m_HWParam.strOrgImgPath,  pCoordObj->m_ImageUpdatePath);
			pUpdateState->m_bOrgImageBackup = false;
		}


		pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_FileAccess, nBiz_Recv_BackupEnd, 0);
		//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
		//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM, nBiz_AOI_FileAccess, nBiz_Recv_BackupEnd, 0);
		
		pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Backup End");
		::SendMessage(pUpdateState->m_UpdateUIHandle, WM_USER_STATE_UPDATE, nBiz_AOI_STATE_IDLE, 0);

		pUpdateState->m_nProcFileUpdateTime =  GetTickCount()-nProcFileUpdateTime;

		if(pUpdateState->m_nProcFileUpdateMaxTime<pUpdateState->m_nProcFileUpdateTime)
			pUpdateState->m_nProcFileUpdateMaxTime = pUpdateState->m_nProcFileUpdateTime;
		//////////////////////////////////////////////////////////////////////////
*/

	//ViewImage.Destroy();
	if(pRevewImg = nullptr)
		cvReleaseImage(&pRevewImg);

	if(pSaveSmallImg = nullptr)
		cvReleaseImage(&pSaveSmallImg);

	if(pRevewImgTemp = nullptr)
		cvReleaseImage(&pRevewImgTemp);	

	if(pRevewImgTemp1 = nullptr)
		cvReleaseImage(&pRevewImgTemp1);	

	if(pSave2DImg = nullptr)
		cvReleaseImage(&pSave2DImg);	
	
	
	return 0;
}