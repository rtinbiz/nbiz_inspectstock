#include "StdAfx.h"
#include "DefectFileFormat.h"

// C4996 warning expected
#pragma warning(disable : 4996)
#include <direct.h>
/*
*	Module Name		:	G_fnCheckDirAndCreate
*	Parameter		:	Dir Check Path.
*	Return			:	TRUE/FALSE
*	Function		:	Drive상에 경로 존재 여부와 경로생성을 하게 된다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool G_fnCheckDirAndCreate(char * pCheckPath)
{
	char CreateDir[512] = {0,};
	memcpy(CreateDir, pCheckPath, strlen(pCheckPath));
	//뒤에서부터 '\'를 체크한다.(대표이름 자르기)
	for(size_t DirPos= strlen(pCheckPath);  DirPos > 0 && CreateDir[DirPos] != '\\';DirPos--)
	{
		CreateDir[(int)DirPos] = 0;
	}

	DWORD Error = 0;
	::SetLastError(0);

	//앞에서 부터 '\'를체크하여 Directiory를 만든다.
	for(size_t i = 0; CreateDir[(int)i] != 0 ; i++)
	{
		if(CreateDir[(int)i] == '\\')
		{
			char CreateDirBuff[512] = {0,};
			memcpy(CreateDirBuff, CreateDir, i);
			::CreateDirectory(CreateDirBuff, NULL);
			Error = ::GetLastError();
			if(ERROR_INVALID_NAME == Error)//Name error
				return false;
			//ERROR_ALREADY_EXISTS no error
		}
	}
	return true;
}
/*
*	Module Name		:	G_ClearFolder
*	Parameter		:	Clear Dir Path.
*	Return			:	TRUE/FALSE
*	Function		:	지정 경로의 Data를 모두 지운다.(Folder 삭제->생성 형태)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//SHFILEOPSTRUCT G_DeleteFolder;
//bool G_ClearFolder(CString ClearFolderPath)
//{
//	//AOI_Forlder Clear
//	ClearFolderPath.Format("%s", ClearFolderPath.TrimRight("\\"));
//	int ret=0;
//	char DeleteChar[256];
//	memset(DeleteChar,0x00, 256);
//	strcpy(DeleteChar, ClearFolderPath);
//
//	::ZeroMemory ( &G_DeleteFolder, sizeof (SHFILEOPSTRUCT ) ) ;
//	G_DeleteFolder.hwnd = NULL;
//	G_DeleteFolder.wFunc = FO_DELETE;
//	G_DeleteFolder.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI;
//	G_DeleteFolder.pFrom = (LPCSTR)DeleteChar;
//	ret=::SHFileOperation(&G_DeleteFolder);	// check
//	mkdir(ClearFolderPath);
//	return true;
//}
bool G_ClearFolder(CString ClearFolderPath)
{
	CFileFind ff;
	CString path = ClearFolderPath;

	if(path.Right(1) != "\\")
		path += "\\";

	path += "*.*";

	BOOL res = ff.FindFile(path);

	while(res)
	{
		res = ff.FindNextFile();
		if (!ff.IsDots() && !ff.IsDirectory())
			DeleteFile(ff.GetFilePath());
		else if (ff.IsDirectory()&&!ff.IsDots())
		{
			path = ff.GetFilePath();
			G_ClearFolder(path);
			RemoveDirectory(path);
		}
	}
	return true;
}
/*
*	Module Name		:	FolderCopyFromAToB
*	Parameter		:	From Path, To Path
*	Return			:	TRUE/FALSE
*	Function		:	From Folder의 모든 File을 To Folder로 이동 한다.(All file Copy)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
SHFILEOPSTRUCT G_saveFO ;
BYTE FolderCopyFromAToB(CString fromPath, CString ToPath) 
{
	int ret=0;
	fromPath.Format("%s", fromPath.TrimRight("\\"));
	ToPath.Format("%s", ToPath.TrimRight("\\"));

	//AOI_Forlder Clear
	G_ClearFolder(ToPath);

	//New Data Move.(Backup)

	char strBufFrom[256];
	char strBufTo[256];
	memset(strBufFrom, 0x00, 256);
	memset(strBufTo, 0x00, 256);
	strcpy(strBufFrom, fromPath);
	strcpy(strBufTo, ToPath);

	::ZeroMemory ( &G_saveFO, sizeof (SHFILEOPSTRUCT ) ) ;
	G_saveFO.hwnd = NULL;
	G_saveFO.wFunc = FO_COPY;
	G_saveFO.fFlags =  FOF_SILENT |  FOF_NOCONFIRMATION | FOF_MULTIDESTFILES| FOF_NOERRORUI;
	G_saveFO.fAnyOperationsAborted = FALSE;
	G_saveFO.lpszProgressTitle = _T("AOI Grab File Backup!");
	G_saveFO.pFrom = (LPCTSTR)strBufFrom;
	G_saveFO.pTo   = (LPCTSTR)strBufTo;
	ret= ::SHFileOperation(&G_saveFO);

	//AOI_Forlder Clear
	G_ClearFolder(fromPath);


	//ORG_IMG_FILE_BACKUP *pNewOpData =  new ORG_IMG_FILE_BACKUP;

	//pNewOpData->FromFolderPath.Format("%s", fromPath);
	//pNewOpData->ToFolderPath.Format("%s", ToPath);
	//AfxBeginThread(FolderCopyThread, pNewOpData);
	return TRUE;
}

//BYTE FolderCopyFromAToB(CString fromPath, CString ToPath) 
//{
//	int ret=0;
//	fromPath.Format("%s", fromPath.TrimRight("\\"));
//	ToPath.Format("%s", ToPath.TrimRight("\\"));
//
//	//AOI_Forlder Clear
//	G_ClearFolder(ToPath);
//	CString src_path, dest_path;
//	src_path.Format("%s\\*.*", fromPath);
//	WIN32_FIND_DATA file_data;
//
//	// 지정된 경로에 존재하는 모든 파일에 대한 목록을 구성하고 해당 정보의 핸들값을 얻는다.
//	// 만약 해당 경로에 파일이나 자식 폴더가 존재하지 않는다면 INVALID_HANDLE_VALUE 값을
//	// 반환한다. 그리고 이 함수가 성공적으로 목록을 구성한 경우에는 file_data 변수에
//	// 구성된 목록중에서 첫번째 존재하는 파일의 정보를 넣어준다.
//	HANDLE search_handle = FindFirstFile(src_path, &file_data);
//	if(INVALID_HANDLE_VALUE != search_handle)
//	{
//		do {
//			// 검색된 정보가 파일인지를 속성을 체크하여 확인한다.
//			if(FILE_ATTRIBUTE_ARCHIVE & file_data.dwFileAttributes){
//				// 현재 정보가 파일인 경우, file_data.cFileName에 파일이름이 들어있다.
//				// 원본 파일의 경로를 구성한다.
//				src_path.Format("%s\\%s", fromPath, file_data.cFileName);
//				// 복사될 파일의 경로를 구성한다.
//				dest_path.Format("%s\\%s", ToPath, file_data.cFileName);
//				// 구성된 두 경로를 이용하여 파일을 복사한다.
//				CopyFile(src_path, dest_path, TRUE);
//			}
//
//			// 구성된 목록에서 다음 파일의 정보를 file_data에 복사하여 가져온다.
//			// 더이상의 정보가 없다면 FindNextFile 함수는 0을 반환한다.
//		} while(FindNextFile(search_handle, &file_data));
//
//		// 검색을 위해 사용했던 핸들을 닫는다.
//		FindClose(search_handle);
//	}
//		//AOI_Forlder Clear
//	G_ClearFolder(fromPath);
//
//	return TRUE;
//}


/*
*	Module Name		:	FolderCopyThread
*	Parameter		:	From Path, To Path
*	Return			:	TRUE/FALSE
*	Function		:	From Folder의 모든 File을 To Folder로 이동 한다.(All file Copy)-->> Thread Function Type
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
UINT FolderCopyThread(LPVOID pParam)
{
	int ret=0;
	ORG_IMG_FILE_BACKUP  *pNewPaths  = (ORG_IMG_FILE_BACKUP *)pParam;

	pNewPaths->FromFolderPath.Format("%s", pNewPaths->FromFolderPath.TrimRight("\\"));
	pNewPaths->ToFolderPath.Format("%s", pNewPaths->ToFolderPath.TrimRight("\\"));

	//AOI_Forlder Clear
	G_ClearFolder(pNewPaths->ToFolderPath);

	//New Data Move.(Backup)
	SHFILEOPSTRUCT saveFO ;
	char strBufFrom[256];
	char strBufTo[256];
	memset(strBufFrom, 0x00, 256);
	memset(strBufTo, 0x00, 256);
	strcpy(strBufFrom, pNewPaths->FromFolderPath);
	strcpy(strBufTo, pNewPaths->ToFolderPath);

	saveFO.hwnd = NULL;
	saveFO.wFunc = FO_COPY;
	saveFO.fFlags = FOF_SILENT |  FOF_NOCONFIRMATION |FOF_MULTIDESTFILES| FOF_NOERRORUI |FOF_SIMPLEPROGRESS;
	saveFO.fAnyOperationsAborted = FALSE;
	saveFO.lpszProgressTitle = _T("AOI Grab File Backup!");
	saveFO.pFrom = (LPCTSTR)strBufFrom;
	saveFO.pTo   = (LPCTSTR)strBufTo;
	ret= ::SHFileOperation(&saveFO);

	//AOI_Forlder Clear
	G_ClearFolder(pNewPaths->FromFolderPath);

	return ret;

}

//////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////
CDefectFileFormat::CDefectFileFormat()
{
	m_TextFileObj.m_hFile = INVALID_HANDLE_VALUE;
	m_ImageFileObj.m_hFile = INVALID_HANDLE_VALUE;
	m_TextFilePath.Format("");
	m_ImageFilePath.Format("");

	m_ImageFileUpdatePath.Format("");

	m_DefectImgSize		= 0;
	m_DefectImgCount	= 0;
	InitializeCriticalSection(&m_CrtSection);

}
CDefectFileFormat::~CDefectFileFormat()
{
	//Text File을 마지막으로 모두 닫는다.
	if(m_TextFileObj.m_hFile != INVALID_HANDLE_VALUE)
		m_TextFileObj.Close();
	//////////////////////////////////////////////////////////////////////////
	if(m_ImageFileObj.m_hFile != INVALID_HANDLE_VALUE)
		m_ImageFileObj.Close();

	m_TextFileObj.m_hFile = INVALID_HANDLE_VALUE;
	m_ImageFileObj.m_hFile = INVALID_HANDLE_VALUE;

	DeleteCriticalSection(&m_CrtSection);
}

/*
*	Module Name		:	m_fnSetDefectTextFilePath
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	Defect .dat, .img File의 임시저장 경로와 Update경로를 지정한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CDefectFileFormat::m_fnSetWriteDefectTextObj(CString DefectTextFilePath)
{
	//m_StrFullData.Format("");

	if(m_TextFileObj.m_hFile != INVALID_HANDLE_VALUE)
		m_TextFileObj.Close();

	m_TextFileObj.m_hFile = INVALID_HANDLE_VALUE;

	if( !m_TextFileObj.Open( DefectTextFilePath, CFile::modeCreate /*| CFile::modeNoTruncate */
		| CFile::shareDenyWrite | CFile::modeWrite))
	{
		m_TextFileObj.m_hFile = INVALID_HANDLE_VALUE;
		return false;
	}
	m_TextFileObj.SeekToBegin();
	m_TextFilePath.Format("%s",DefectTextFilePath );

	return true;
}

bool CDefectFileFormat::m_fnSetWriteDefectImageObj(CString DefectImageFilePath, CString ImgUpdatePath, int ImgW, int ImgH)
{
	if(m_ImageFileObj.m_hFile != INVALID_HANDLE_VALUE)
		m_ImageFileObj.Close();

	m_ImageFileObj.m_hFile = INVALID_HANDLE_VALUE;

	if( !m_ImageFileObj.Open( DefectImageFilePath, CFile::modeCreate /*| CFile::modeNoTruncate */
		| CFile::shareDenyWrite | CFile::modeWrite | CFile::typeBinary))
	{
//		m_ImageFileObj.Close();
		m_ImageFileObj.m_hFile = INVALID_HANDLE_VALUE;
		return false;
	}

	m_DefectImgSize = ImgW*ImgH;
	m_ImageFileObj.SeekToBegin();

	CArchive arImage(&m_ImageFileObj, CArchive::store);
	//추가 Image Data 쓰기.
	m_DefectImgCount = 0;
	arImage.Write((void*) &m_DefectImgCount, sizeof(int));//Defect Count를 기록 할 구간.
	arImage.Write((void*) &ImgW, sizeof(int));
	arImage.Write((void*) &ImgH, sizeof(int));
	arImage.Close();
	m_ImageFileObj.SeekToEnd();
	m_ImageFilePath.Format("%s",DefectImageFilePath );

	m_ImageFileUpdatePath.Format("%s",ImgUpdatePath );

	return true;
}
/*
*	Module Name		:	m_fnWriteNodeData
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	대상 Node의 Text, Image Data를 Write한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool CDefectFileFormat::m_fnWriteImageDataOnNode(BYTE *pRawData)
{
	if( m_ImageFileObj.m_hFile == INVALID_HANDLE_VALUE)
		return false;
	m_ImageFileObj.SeekToEnd();

	CString WritFileBuffer;
	try
	{
		CArchive arImage(&m_ImageFileObj, CArchive::store);
		//추가 Image Data 쓰기.
		arImage.Write((void*) pRawData, m_DefectImgSize);
		arImage.Close();
		m_DefectImgCount++;
	}
	catch (CException* pe)
	{
		pe = pe;
		m_TextFileObj.Close();
		m_TextFileObj.m_hFile = INVALID_HANDLE_VALUE;
		return false;
	}
	return true;
}
bool CDefectFileFormat::m_fnWriteFullDefectListData( CString *pWriteData)
{
	if(m_TextFileObj.m_hFile == INVALID_HANDLE_VALUE)
		return false;
	m_TextFileObj.SeekToEnd();

	try
	{
		CArchive arText(&m_TextFileObj, CArchive::store);
		arText.WriteString((LPCTSTR)*pWriteData);
		arText.Close();
	}
	catch (CException* pe)
	{
		pe = pe;
		m_TextFileObj.Close();
		m_TextFileObj.m_hFile = INVALID_HANDLE_VALUE;
		return false;
	}
	return true;
}
/*
*	Module Name		:	m_fnWriteImageFileData
*	Parameter		:	Defect 정보, Save Image Size.
*	Return			:	없음
*	Function		:	*.dat File에 Defect Image를 지정 Size만큼 저장한다.(Thread에서 바로 호출)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/

bool CDefectFileFormat::m_fnCloseFileObj()
{
	//if(m_StrFullData.GetLength()>0)
	//	m_fnWriteFullDefectListData(&m_StrFullData);
	//m_StrFullData.Format("");

	//Text File을 마지막으로 모두 닫는다.
	if(m_TextFileObj.m_hFile != INVALID_HANDLE_VALUE)
		m_TextFileObj.Close();
	//////////////////////////////////////////////////////////////////////////
	if(m_ImageFileObj.m_hFile != INVALID_HANDLE_VALUE)
	{
		if(m_DefectImgCount>0)
		{//Defect Count 저장.
			m_ImageFileObj.SeekToBegin();
			CArchive arImage(&m_ImageFileObj, CArchive::store);
			//추가 Image Data 쓰기.
			arImage.Write((void*) &m_DefectImgCount, sizeof(int));//Defect Count를 기록 할 구간.
			arImage.Close();
		}
		m_ImageFileObj.Close();
	}
	
	m_TextFileObj.m_hFile = INVALID_HANDLE_VALUE;
	m_ImageFileObj.m_hFile = INVALID_HANDLE_VALUE;

	m_TextFilePath.Format("");
	m_ImageFilePath.Format("" );
	return true;
}
bool CDefectFileFormat::m_fnIsOpened()
{
	bool retvalue = false;
	EnterCriticalSection(&m_CrtSection);
	if(m_TextFileObj.m_hFile != INVALID_HANDLE_VALUE || m_ImageFileObj.m_hFile != INVALID_HANDLE_VALUE)
		retvalue = true;
	LeaveCriticalSection(&m_CrtSection);
	return retvalue;

}
