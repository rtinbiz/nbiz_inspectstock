#pragma once
#include "AOI_ImageCtrlQ.h"
#include "PadAreaCtrlQ.h"
#include "..\..\..\CommonHeader\Inspection\CoordinatesProc.h"
/** 
@brief		Result Process에사 사용되는 구조체

@remark
-			Inspection 이후 Data 처리를 위한 구조체 이다.
-			
@author		최지형
@date		2010.00.00
*/ 
typedef struct PADCUT_PROCESS_TRG
{
	int							m_ProcIndex;
	bool						m_bProcessRun;
	CWinThread	*			m_pResultThread;
	AOI_ImageCtrlQ *		m_pIDTCtrlQ;
	PAD_ImageCtrlQ *		m_pPADCtrlQ;

	CoordinatesProc *	m_pCoordProcObj;
	PROC_STATE	*		m_pUpdateState;
	

	PADCUT_PROCESS_TRG()
	{
		m_ProcIndex			= -1;
		m_bProcessRun		=  false;
		m_pResultThread		= nullptr;
		m_pIDTCtrlQ			= nullptr;
		m_pCoordProcObj		= nullptr;
		m_pUpdateState		= nullptr;
	}

} PROC_PADCUT, *LPPADCUT_PROCESS;

//PAD Cutter Process Def.
UINT fn_OrgImgCutterProcess(LPVOID pParam);

inline bool G_CuttingActiveArea(InspectDataQ* pTargetNode, CRect* pActiveArea, PAD_ImageCtrlQ * pPadObj = nullptr);
//inline bool G_CuttingPADArea(CoordinatesProc * pCoordProcObj, PAD_ImageCtrlQ *pPadObj, int PadImgCnt, InspectDataQ* pTargetNode, CRect* pPadInImgArea, CRect* pPadArea);