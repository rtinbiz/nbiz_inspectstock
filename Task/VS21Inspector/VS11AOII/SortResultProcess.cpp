#include "StdAfx.h"
#include "SortResultProcess.h"

//Defect Send 수 제한 분할 전송.
#define  DEFECT_SEPARAT_SEND_CNT	300

UINT fn_SortResultProcess(LPVOID pParam)
{
	PROC_RETSORT	*		pProcParam		= (PROC_RETSORT *) pParam;
	AOI_ImageCtrlQ *			pIDTCtrlQ		= pProcParam->m_pIDTCtrlQ;

	CGraberCtrl		*				pMilGrabberCtrl= pProcParam->m_pGrabberCtrl;

	PROC_PAD	*				pPadProcObj	= pProcParam->m_pPadProcObj;

	CoordinatesProc*			pCoordObj		= pIDTCtrlQ->m_pCoordObj;
	PROC_STATE	*			pUpdateState	= pIDTCtrlQ->m_pUpdateState;

	InspectDataQ*			pTargetNode	= nullptr;
	CInterServerInterface *	pSInterface		= pProcParam->m_pServerInterface;

	DefectDataQ	*			pResultDefectQ = pProcParam->m_pResultDefectQ;

	//////////////////////////////////////////////////////////////////////////
	SCANIMGINFO	*			pCoodArea = nullptr;
	//////////////////////////////////////////////////////////////////////////
	extern int		G_MasterTaskIndex;
	//////////////////////////////////////////////////////////////////////////
	DWORD tickTickN			= 0;
	int CountSize			= sizeof(int);
	int DefectInfoSize		=(sizeof(COORD_DINFO)*pCoordObj->m_HWParam.m_DefectMaxCntInImg);
	int ScanInfoSize		= sizeof(SCANIMGINFO);

	int SendDataSize = CountSize+DefectInfoSize+CountSize+ScanInfoSize;
	UCHAR *pSendRetData		= new UCHAR[SendDataSize];
	//////////////////////////////////////////////////////////////////////////
	DEFECT_DATA *pDefectData = nullptr;
	COORD_DINFO *pCoordData = nullptr;
	BYTE	 *pReviewImgData = nullptr;
	while(pProcParam->m_bProcessRun == true)
	{
		pTargetNode = pIDTCtrlQ->m_fnQGetInspectPTR_SortRetProc();

		//if(pUpdateState->m_InspectionStop  == INSPECTION_STOP)
		//{
		//	if(pTargetNode != nullptr)
		//		pTargetNode->m_fnQSetProcessEnd(QDATA_PROC_END_SORT);
		//}
		//else
		if(pTargetNode != nullptr)
		{
			tickTickN =   ::GetTickCount();
			//pStrDefect = (CString *)pTargetNode->m_StrDefect;
			//pWriteFileObj->m_StrFullData.AppendFormat("%s", pStrDefect->GetBuffer(pStrDefect->GetLength()));
			//지정된 File 경로에 해당 Data를 Write 한다.
			//pWriteFileObj->m_fnWriteNodeData(pTargetNode);
			pCoodArea = (SCANIMGINFO	*)pTargetNode->m_pCoordAraeInImg;

			//Send Master Process
			if(pCoodArea != nullptr)
			{

				if(pTargetNode->m_ScanImageIndex>109)
				{
					AfxMessageBox("ddd");
				}
				//Count 갯수 만큼 복사 해서 전달 한다.
				if(pTargetNode->m_DefectListCnt<=DEFECT_SEPARAT_SEND_CNT)
				{
					DefectInfoSize = sizeof(COORD_DINFO)*pTargetNode->m_DefectListCnt;

					memcpy(pSendRetData, &pTargetNode->m_DefectListCnt, CountSize );											//Defect Count 저장.
					memcpy(pSendRetData+CountSize, pTargetNode->m_pCoordinateRetList, DefectInfoSize );						//Defect List 저장.
					memcpy(pSendRetData+CountSize+DefectInfoSize, pCoodArea, ScanInfoSize );								//Scan Info 저장.

					pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex,
													nBiz_AOI_Inspection, 
													nBiz_Recv_InspectRetInfo,
													0, 
													(USHORT)(CountSize+DefectInfoSize+ScanInfoSize),
													pSendRetData);
					//cvSaveImage("D:\\xx.bmp", pTargetNode->m_pGrabOrgImage);
				}
				else
				{
					int SendedDataCnt = 0;
					for(int SendDataIndex = 0; SendDataIndex<(pTargetNode->m_DefectListCnt/DEFECT_SEPARAT_SEND_CNT); SendDataIndex++)
					{

						DefectInfoSize = sizeof(COORD_DINFO)*DEFECT_SEPARAT_SEND_CNT;

						//memcpy(pSendRetData, &pTargetNode->m_DefectListCnt, CountSize );											//Defect Count 저장.
						*((int*)pSendRetData) = 300;
						memcpy(pSendRetData+CountSize, pTargetNode->m_pCoordinateRetList+SendedDataCnt, DefectInfoSize );		//Defect List 저장.
						memcpy(pSendRetData+CountSize+DefectInfoSize, pCoodArea, ScanInfoSize );								//Scan Info 저장.

						pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex,
														nBiz_AOI_Inspection, 
														nBiz_Recv_InspectRetInfo,
														0, 
														(USHORT)(CountSize+DefectInfoSize+ScanInfoSize),
														pSendRetData);

						SendedDataCnt += DEFECT_SEPARAT_SEND_CNT;
					}
					if((pTargetNode->m_DefectListCnt%DEFECT_SEPARAT_SEND_CNT)>0)
					{
						DefectInfoSize = sizeof(COORD_DINFO)*(pTargetNode->m_DefectListCnt%DEFECT_SEPARAT_SEND_CNT);

						//memcpy(pSendRetData, &pTargetNode->m_DefectListCnt, CountSize );										//Defect Count 저장.
						*((int*)pSendRetData) = (pTargetNode->m_DefectListCnt%DEFECT_SEPARAT_SEND_CNT);
						memcpy(pSendRetData+CountSize, pTargetNode->m_pCoordinateRetList+SendedDataCnt, DefectInfoSize );		//Defect List 저장.
						memcpy(pSendRetData+CountSize+DefectInfoSize, pCoodArea, ScanInfoSize );								//Scan Info 저장.

						pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex,
														nBiz_AOI_Inspection, 
														nBiz_Recv_InspectRetInfo,
														0, 
														(USHORT)(CountSize+DefectInfoSize+ScanInfoSize),
														pSendRetData);

						SendedDataCnt += (pTargetNode->m_DefectListCnt%DEFECT_SEPARAT_SEND_CNT);
					}

				}
				


				//DefectInfoSize = sizeof(COORD_DINFO)*pTargetNode->m_DefectListCnt;

				//memcpy(pSendRetData, &pTargetNode->m_DefectListCnt, CountSize );											//Defect Count 저장.
				//memcpy(pSendRetData+CountSize, pTargetNode->m_pCoordinateRetList, DefectInfoSize );						//Defect List 저장.
				//memcpy(pSendRetData+CountSize+DefectInfoSize, pCoodArea, ScanInfoSize );								//Scan Info 저장.

				//pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex,
				//	nBiz_AOI_Inspection, 
				//	nBiz_Recv_InspectRetInfo,
				//	0, 
				//	(USHORT)(CountSize+DefectInfoSize+ScanInfoSize),
				//	pSendRetData);




			//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
			//{
			//		pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM,
			//			nBiz_AOI_Inspection, 
			//			nBiz_Recv_InspectRetInfo,
			//			0, 
			//			(USHORT)(CountSize+DefectInfoSize+ScanInfoSize),
			//			pSendRetData);
			//}
				//pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Act Defect Data Send End");

				for(int InputStep= 0; InputStep<pTargetNode->m_DefectListCnt; InputStep++)
				{
					pDefectData		= &pTargetNode->m_pSearchRetList[InputStep];
					pCoordData			= &pTargetNode->m_pCoordinateRetList[InputStep];
					pReviewImgData	= pTargetNode->m_ppDefectImgList[InputStep];
					
					pResultDefectQ->QPutNodeAct(pTargetNode->m_ScanLine, pTargetNode->m_ScanImageIndex, InputStep+1,
												pDefectData, pCoordData, pReviewImgData, (pTargetNode->m_ReviewSizeH *pTargetNode->m_ReviewSizeH));
				}
			}

			if(pUpdateState != nullptr)
			{//첫장은 오류, 1번에서 2번까지의 시간을 잰다.
				
				DWORD nowTick = ::GetTickCount();
				//Grab되는 모든 Imag에 대해서.
				pUpdateState->m_nOneLineNowTick		= nowTick;
				pUpdateState->m_nOneLineNowIndex		= pTargetNode->m_ScanImageIndex;

				//Image 1장에 대해서.
				DWORD MinMaxValue = (nowTick - pTargetNode->m_nOneTick);///nowFreQ)/1000;
				pUpdateState->m_nOneImageTick = MinMaxValue;

				if(pUpdateState->m_nOneImageMinTick  > MinMaxValue)
					pUpdateState->m_nOneImageMinTick = MinMaxValue;

				if(pUpdateState->m_nOneImageMaxTick < MinMaxValue)
					pUpdateState->m_nOneImageMaxTick = MinMaxValue;

				pUpdateState->m_SortScanLineNum		= pTargetNode->m_ScanLine;			
				pUpdateState->m_SortScanLineIndex		= pTargetNode->m_ScanImageIndex;			
				pUpdateState->m_nSortProcTick			= (nowTick-tickTickN);
			}

			//Grab Count가 완료 되면 종료 Command를 발송 한다.
			if(pCoodArea != nullptr)
			{
				if(pTargetNode->m_ScanImageIndex == pCoordObj->m_OneLineScnaImgCnt)
				{
					//////////////////////////////////////////////////////////////////////////
					//Mil Graber에 Scan 종료를 알린다.
					//pMilGrabberCtrl->GrabEnd();
					pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_Inspection, nBiz_Recv_ScanActEnd, 0); 
					pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Act End");
					if(pUpdateState->bRealTimeLightValueSend == false)
					{
						int				BufHistScanMaxValue;
						float			BufHistogaram_Data[256];

						BufHistScanMaxValue = pUpdateState->m_HistScanMaxValue;
						memcpy(BufHistogaram_Data, pUpdateState->m_Histogaram_Data,  sizeof(float)*256);

						//조도 Max값 저장 버퍼 클리어.
						pUpdateState->m_HistScanMaxValue = 0;
						memset(pUpdateState->m_Histogaram_Data, 0x00, sizeof(float)*256);

						//Send One Line Scan Histogram Max Value
						pSInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 
															nBiz_AOI_Inspection, 
															nBiz_Recv_IlluminationValue,
															(USHORT)BufHistScanMaxValue,
															sizeof(float)*256,
															(UCHAR*)&BufHistogaram_Data[0]); 
						pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Histogram Value :%d(Scan Line :%d)", pUpdateState->m_HistScanMaxValue, pTargetNode->m_ScanLine);
					}

					//Pad부 Inspection이 완료 될때까지 대기 한다.
					while(pPadProcObj->m_StatePADInspect == PAD_INSPECTION_RUN)
						Sleep(10);
					
					pPadProcObj->m_StatePADInspect = PAD_INSPECTION_IDLE;
					
					if(pTargetNode->m_ScanLine < pCoordObj->m_AOIOneCam_ScanLineCnt)
						::SendMessage(pUpdateState->m_UpdateUIHandle, WM_USER_STATE_UPDATE, nBiz_AOI_STATE_READY, 0);


					pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_Inspection, nBiz_Recv_ScanPADEnd, 0); 
					pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send PAD End");

					
				}

				if(pTargetNode->m_ScanLine ==  pCoordObj->m_AOIOneCam_ScanLineCnt &&
					pTargetNode->m_ScanImageIndex == pCoordObj->m_OneLineScnaImgCnt)
				{
					pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex, nBiz_AOI_Inspection, nBiz_Recv_BlockEnd, 0); 
					pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"Send Block End");

					
					
					pUpdateState->m_bBlockScanEnd = true;
				}
			}
			//memset(pTargetNode->m_pGrabImage->imageData, 0x00, pTargetNode->m_pGrabImage->imageSize);
			//cvZero(pTargetNode->m_pGrabOrgImage);
			memset(pTargetNode->m_pGrabImage->imageData, 0xFF, pTargetNode->m_pGrabImage->imageSize);
			//memset(pTargetNode->m_pGrabOrgImage->imageData, 0xFF, pTargetNode->m_pGrabOrgImage->imageSize);
			//File Process 까지 끝났다 Grabber에서 사용할수 있는 Node로 변한다.
			pTargetNode->m_fnQSetProcessEnd(QDATA_PROC_END_SORT);
		}
		else
			Sleep(1);
	}
	if(pSendRetData != nullptr)
		delete [] pSendRetData;
	pProcParam->m_pResultThread = nullptr;
	return 0;
}