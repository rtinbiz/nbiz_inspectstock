#pragma once

//////////////////////////////////////////////////////////////////////////
//대상 Graber에 따른 Link설정.
//MATROX Grabber사용 설비 (DCf Folder에 DCf File을 Defalt로 사용) 
//8K Camera.
//#3호기
//#define  MATROX_LINE_SCAN_GRABBER

//Dalsa Grabber사용 설비.(DCF Folder에 CCf File을  Loading하여사용).
//12K Camera.
//SMD #1호기 #2호기, #10호기.
//#define  DALSA_LINE_SCAN_GRABBER

//Grabber가 아닌 File에서 Image Loading.
//#define  TEST_INSPECTION_LOAD_FILE	

// Local File Test 를 할 경우
//#define TEST_LOCAL_FILE
#ifdef TEST_LOCAL_FILE
#define TEST_INSPECTION_LOAD_FILE
#endif

//////////////////////////////////////////////////////////////////////////

//임의 Image에서 Pad부 Image를 취득하여 Image를 삽입 한다.
//#define  TEST_PAD_UPDN_ALIGN	
//////////////////////////////////////////////////////////////////////////
//Test Define Code
//Save Org Image 
//#define  TEST_SAVE_ORG_IMG

//Save Org Image Draw(Act/Pad Area)
//#define  TEST_SAVE_CUT_AREA_DRAW

//Pad Cutting Org Image Save
//#define  TEST_SAVE_PAD_AREA_SAVE
#ifdef TEST_SAVE_PAD_AREA_SAVE
	//#define  TEST_SAVE_PAD_FIRST_LAST_SAVE
#endif

//75 3D Task Review Data Sending
//#define  TEST_VS75_REVIEW_SEND

//Defect Image Save JPG
//#define  TEST_DEFECT_IMG_SAVE_JPG

//Defect Image Align Position(원판 에서 벗어나지 않게 자른다.)
//#define  TEST_DEFECT_IMG_IN_ORG

//Active Image의 Image Cutting방식 설정.
//#define  USE_C_ROI_CUT	

//Map 생성 과정 보기.
//#define  VIEW_CREATE_MAP
//#define  VIEW_CREATE_SCAN_PAD
//#define  VIEW_CREATE_SCAN_PAD2
//#define  VIEW_CREATE_SCAN_PAD3

//Inspect Dll을 적용하지 않고, Main Process 동작 시키기.(CPU Process Time Check용)
//#define DEBUG_WITHOUT_INSPECT_DLL