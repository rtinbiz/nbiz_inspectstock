// HWParamEdit.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VS11AOII.h"
#include "HWParamEdit.h"
#include "afxdialogex.h"


COLORREF clrEdit = RGB(255,255,255);
COLORREF clrPro = RGB(255,100,200);
// CHWParamEdit 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHWParamEdit, CDialogEx)

CHWParamEdit::CHWParamEdit(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHWParamEdit::IDD, pParent)
{
	m_TaskNum = 0;
}

CHWParamEdit::~CHWParamEdit()
{
}

void CHWParamEdit::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CU_GRID_HWPARAM, m_GridEditHWParam);
	DDX_Control(pDX, IDC_BT_SAVE, m_btSaveParam);
	DDX_Control(pDX, IDC_BT_CANCEL, m_btCloseParam);
}


BEGIN_MESSAGE_MAP(CHWParamEdit, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BT_SAVE, &CHWParamEdit::OnBnClickedBtSave)
	ON_BN_CLICKED(IDC_BT_CANCEL, &CHWParamEdit::OnBnClickedBtCancel)

	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_CU_GRID_HWPARAM, OnTagGridSelEditedText)   //-> 이 줄을 추가....
END_MESSAGE_MAP()


// CHWParamEdit 메시지 처리기입니다.


void CHWParamEdit::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::PostNcDestroy();
	delete this;
}
void CHWParamEdit::OnTagGridSelEditedText(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
	*pResult = 0;
	int nRow = pItem->iRow;//   <<- 선택된 ROW
	int nCol = pItem->iColumn;//  <<- 선택된 COLUMN

	//Size Input
	if(nCol == 1)
	{
		CString NewText;
		if(nRow != 2 && nRow != 3 && nRow != 4 && nRow != 5  && nRow != 48 && nRow != 49 && nRow != 51)
		{
			
			NewText.Format("%s", m_GridEditHWParam.GetItemText(nRow, nCol));

			if(CheckNumberStr(&NewText) == FALSE)
			{
				AfxMessageBox("Only Number Input Plz..");
				m_GridEditHWParam.Refresh();
				return;
			}

			NewText.Format("%s", m_GridEditHWParam.GetItemText(nRow, nCol));

			switch(nRow)
			{
			case 1 ://"SERIAL_PORT_CAM_DERCTION");
					m_HWParam.CamScanDirectionPort = atoi(NewText);
				break;
			case 7 ://, 0, "VSB_Size");		
				m_HWParam.m_VSB_Size = atoi(NewText);
				break;
			case 8 ://, 0, "GrabImageW");	
				m_HWParam.m_ProcImageW = atoi(NewText);
				break;
			case 9 ://, 0, "GrabImageH");		
				m_HWParam.m_ProcImageH = atoi(NewText);
				break;
			case 10 ://, 0, "DefectMaxCntInImg");	
				m_HWParam.m_DefectMaxCntInImg = atoi(NewText);
				break;
			case 11 ://, 0, "DefectReviewSizeW");		
				m_HWParam.m_DefectReviewSizeW = atoi(NewText);
				break;
			case 12 ://, 0, "DefectReviewSizeH");	
				m_HWParam.m_DefectReviewSizeH = atoi(NewText);
				break;
			case 13 ://, 0, "INSPECTION_CAM_COUNT");	
				m_HWParam.m_INSPECTION_CAM_COUNT = atoi(NewText);
				break;
			case 14 ://, 0, "INSPECTION_SCAN_WIDTH");	
				m_HWParam.m_INSPECTION_SCAN_WIDTH = atoi(NewText);
				break;
			case 15 ://, 0, "INSPECTION_SCAN_HEIGHT");	
				m_HWParam.m_INSPECTION_SCAN_HEIGHT = atoi(NewText);
				break;
			case 16 ://, 0, "BLOCK_SCAN_START_X");	
				m_HWParam.m_AOI_BLOCK_SCAN_START_X = atoi(NewText);
				break;
			case 17 ://, 0, "BLOCK_SCAN_START_Y");	
				m_HWParam.m_AOI_BLOCK_SCAN_START_Y = atoi(NewText);
				break;

			case 19 ://, 0, "CAM_DISTANCE_X");		
				m_HWParam.m_AOI_CAM_DISTANCE_X = atoi(NewText);
				break;
			case 20 ://, 0, "CAM_DISTANCE_Y");		
				m_HWParam.m_AOI_CAM_DISTANCE_Y = atoi(NewText);
				break;

			case 21 ://, 0, "SCAN_START_OFFSET_ODD");	
				m_HWParam.m_AOI_SCAN_START_OFFSET_ODD = atoi(NewText);
				break;
			case 22 ://, 0, "SCAN_START_OFFSET_EVEN");		
				m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN = atoi(NewText);
				break;
				

			case 24 ://, 0, "CAM_DISTANCE_X");		
				m_AOI_CAM_DISTANCE_X_Type2 = atoi(NewText);
				break;
			case 25 ://, 0, "CAM_DISTANCE_Y");		
				m_AOI_CAM_DISTANCE_Y_Type2 = atoi(NewText);
				break;

			case 26 ://, 0, "SCAN_START_OFFSET_ODD");	
				m_AOI_SCAN_START_OFFSET_ODD_Type2 = atoi(NewText);
				break;
			case 27 ://, 0, "SCAN_START_OFFSET_EVEN");		
				m_AOI_SCAN_START_OFFSET_EVEN_Type2 = atoi(NewText);
				break;


			case 29 ://, 0, "OnlyGrabImageSave");	
				m_HWParam.m_OnlyGrabImageSave = atoi(NewText);
				break;
			case 30 ://, 0, "SaveDefectOrgImage");	
				m_HWParam.m_SaveDefectOrgImage = atoi(NewText);
				break;
			case 31 ://, 0, "DefectSaveFileType");
				m_HWParam.DefectSaveFileType = atoi(NewText);
				break;
			case 32 ://, 0, "PadInspectAreaType");	
				m_HWParam.PadInspectAreaType = atoi(NewText);
				break;

			case 34 : case 35 : case 36 : case 37 :case 38 :
				m_InspectParam.nZoneStep[nRow-34]= atoi(NewText);
				break;
			case 40 : case 41 : case 42 : case 43 :case 44 :
				m_InspectParam.nZoneOffset[nRow-40]= atoi(NewText);
				break;
			case 46:
				m_AxisOverLap = atoi(NewText);
				break;

			}

		}
		else// if(nRow != 6 && nRow != 22)
		{//Tag Name Edit
			NewText.Format("%s", m_GridEditHWParam.GetItemText(nRow, nCol));
			switch(nRow)
			{
				case 2 ://, 0, "IMAGE_UPDATE_PATH");	
					UpdatePath_ImgData.Format("%s",  NewText);
					break;
				case 3 ://, 0, "RESULT_UPDATE_PATH");	
					UpdatePath_Result.Format("%s",  NewText);
					break;
				case 4 ://, 0, "IMAGE_SAVE_PATH");
					sprintf_s(m_HWParam.strOrgImgPath,"%s",  NewText);
					break;
				case 5 ://, 0, "RESULT_SAVE_PATH ");		
					sprintf_s(m_HWParam.strResultPath,"%s",  NewText);
					break;

				case 48:
					m_HWParam.m_LensCurvature_Type1 = (float)atof(NewText); 
					break;
				case 49:
					m_HWParam.m_LensCurvature_Type2 = (float)atof(NewText); 
					break;

				case 51:
					strDalsaCamPath.Format("%s", NewText);
					break;
			}
		}
		//else
		//	return;

		CString UpdateText;
		UpdateText.Format("%s", m_GridEditHWParam.GetItemText(nRow,nCol));
		SetGridCellBackColor(&m_GridEditHWParam,nRow,nCol, clrPro);
		m_GridEditHWParam.SetItemText(nRow, nCol, UpdateText );
		m_GridEditHWParam.Refresh();
	}
}

HBRUSH CHWParamEdit::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();
	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{

			pDC->SetTextColor(RGB(0, 0, 0)); 
			pDC->SetBkColor(RGB(255, 255, 255));
			return (HBRUSH)GetStockObject(BLACK_BRUSH);
		}break;
	case CTLCOLOR_BTN:
		return hbr;

	}
	return (HBRUSH)GetStockObject(BLACK_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL CHWParamEdit::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
	{
		::TranslateMessage(pMsg);
		::DispatchMessage(pMsg);
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		::TranslateMessage(pMsg);
		::DispatchMessage(pMsg);
		NextCellEditMove(&m_GridEditHWParam);
		return TRUE;
	}
		
	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CHWParamEdit::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	m_GridFont.CreateFont(18, 0, 0, 0, FW_BOLD ,
		FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,   DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Times New Roman");


	//////////////////////////////////////////////////////////////////////////
	tButtonStyle tStyle;
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_tColorFace.m_tClicked		= RGB(0x50, 0x50, 0x50);
	//tStyle.m_tColorBorder.m_tClicked	= RGB(0xFF, 0xFF, 0xFF);
	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btSaveParam.GetTextColor(&tColor);
		m_btSaveParam.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,31,"Times New Roman");
			tFont.lfHeight =18;
		}


		m_btSaveParam.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSaveParam.SetTextColor(&tColor);
		m_btSaveParam.SetCheckButton(false);
		m_btSaveParam.SetFont(&tFont);

		m_btCloseParam.SetRoundButtonStyle(&m_tButtonStyle);
		m_btCloseParam.SetTextColor(&tColor);
		m_btCloseParam.SetCheckButton(false);
		m_btCloseParam.SetFont(&tFont);
	}
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	try {
		//////////////////////////////////////////////////////////////////////////
		m_GridEditHWParam.SetDefCellWidth(280);
		m_GridEditHWParam.SetDefCellHeight(30);
		//m_GridNetworkStaus.AutoFill();

		m_GridEditHWParam.SetRowCount(27+12+2+3+2 +4+2);
		m_GridEditHWParam.SetColumnCount(3);//10);
		m_GridEditHWParam.SetFixedRowCount(1);		
		m_GridEditHWParam.SetFixedColumnCount(1);
		m_GridEditHWParam.SetListMode(TRUE);

		m_GridEditHWParam.SetFixedTextColor(RGB(255,200,100));
		m_GridEditHWParam.SetFixedBkColor(RGB(0,0,0));

		m_GridEditHWParam.SetFont(&m_GridFont);
		m_GridEditHWParam.SetItemText(0, 0, "");	
		m_GridEditHWParam.SetItemText(0, 1, "Value");			m_GridEditHWParam.SetColumnWidth(1, 250);
		m_GridEditHWParam.SetItemText(0, 2, "Comment");	m_GridEditHWParam.SetColumnWidth(2, 600);

		int InputIndex = 1;
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "SERIAL_PORT_CAM_DERCTION");
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "IMAGE_UPDATE_PATH");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "RESULT_UPDATE_PATH");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "IMAGE_SAVE_PATH");
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "RESULT_SAVE_PATH ");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "VSB_Size");			
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "GrabImageW");			
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "GrabImageH");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "DefectMaxCntInImg");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "DefectReviewSizeW");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "DefectReviewSizeH");			
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "INSPECTION_CAM_COUNT");			
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "INSPECTION_SCAN_WIDTH");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "INSPECTION_SCAN_HEIGHT");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "BLOCK_SCAN_START_X");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "BLOCK_SCAN_START_Y");	

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Type 1 :CAM_DISTANCE_X");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Type 1 :CAM_DISTANCE_Y");		

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Type 1 :SCAN_START_OFFSET_ODD");			
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Type 1 :SCAN_START_OFFSET_EVEN");		

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");	
		//Lens Type 추가.(20120726) Start
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Type 2 :CAM_DISTANCE_X");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Type 2 :CAM_DISTANCE_Y");		

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Type 2 :SCAN_START_OFFSET_ODD");			
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Type 2 :SCAN_START_OFFSET_EVEN");		
		//Lens Type 추가.(20120726) End

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");	

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "OnlyGrabImageSave");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "SaveDefectOrgImage");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "DefectSaveFileType");		
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "PadInspectAreaType");			

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");	

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Zone 1");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Zone 2");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Zone 3");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Zone 4");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Zone 5");	
										  										  
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");	
										  
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Offset 1");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Offset 2");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Offset 3");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Offset 4");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Lens Offset 5");		

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Axis Y Overlap");	

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");	

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Axis X Lens Curvature(Type1)");	
		m_GridEditHWParam.SetItemText(InputIndex++, 0, "Axis X Lens Curvature(Type2)");	

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "");

		m_GridEditHWParam.SetItemText(InputIndex++, 0, "DALSA Scan Camera Info");
		//////////////////////////////////////////////////////////////////////////
		InputIndex = 1;
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Camera Scan Direction Select Serial Port Number");
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "검출 Defect원본 Image Backup 위치.(Local Drive.)");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "검출 결과 Data Update 위치.(LAN 공유 Drive.)");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "검출 과정에 저장되는 원본이미지 저장위치 (Ram Drive.)");
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "검출 과정에 생성되는 *.dat, *.img File 저장위치.(Ram Drive.)");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "VSB Size");			
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Grab Cam의 W Scan Pixel.");			
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Grab Cam의 H Scan Pixel.");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Grab Image당 Max Defect의 갯수.");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Defect당 Review용 이미지의 넓이");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Defect당 Review용 이미지의 높이");			
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "System에 적용될 AOI Camera Count");			
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Scan할 Total 넓이.(um단위)");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Scan할  Total 높이.(um단위)");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "기준점.기준의 Block Scan이 시작되는 위치.(um단위)");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "기준점. 기준의 Block Scan이 시작되는 위치.(um단위)");		

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "기준점. Cam Center에서 AOI Cam과의 거리.(um단위)");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "기준점. Cam Center에서 AOI Cam과의 거리.(um단위)");		

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(홀수 Scan시 적용)(um단위)");			
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(짝수 Scan시 적용)(um단위)");		

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");	
		//Lens Type 추가.(20120726) Start
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "기준점. Cam Center에서 AOI Cam과의 거리.(um단위)");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "기준점. Cam Center에서 AOI Cam과의 거리.(um단위)");		

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(홀수 Scan시 적용)(um단위)");			
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(짝수 Scan시 적용)(um단위)");		
		//Lens Type 추가.(20120726) End

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Graber에서 Image를 모두 저장한다.1:TRUE 0:FALSE(Ram Disk Over)");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Defect가 있는 원본 File을 저장 한다");	
			
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Defect발견시 Backup용 Image Type defalut Jpg, => 0:jpg, 1:BMP, 2:PNG ");		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Pad부 검사 위치 지정 = >0:None, 1: 상하, 2:좌우. 3:양쪽다.");	

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");	

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens의 영역 1");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens의 영역 2");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens의 영역 3");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens의 영역 4");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens의 영역 5");	


		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");	

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens 적용 수치 1");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens 적용 수치 2");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens 적용 수치 3");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens 적용 수치 4");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens 적용 수치 5");	
		
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");	
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Scan (Axis Y)방향으로 겹침 Size(Pixel Cnt)");	

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens Type1 검사시 Axis X의 Center기준 um오차 보상");
		m_GridEditHWParam.SetItemText(InputIndex++, 2, "Lens Type2 검사시 Axis X의 Center기준 um오차 보상");

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "");

		m_GridEditHWParam.SetItemText(InputIndex++, 2, "DALSA  Camera Sapera CCF File Name (D:\\nBiz_InspectStock\\DCFs\\)");

	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return FALSE;
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

bool CHWParamEdit::m_fnUpdateUI()
{
	int ItemCnt = m_GridEditHWParam.GetRowCount()-1;
	for(int ChRunStep= 0; ChRunStep<ItemCnt; ChRunStep++)
	{

		SetGridCellBackColor(&m_GridEditHWParam,ChRunStep+1,1, clrEdit);
	}

	CString newData;
	int InputIndex = 1;
	newData.Format("%d", m_HWParam.CamScanDirectionPort);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);// "SERIAL_PORT_CAM_DERCTION");

	newData.Format("%s",  UpdatePath_ImgData);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"IMAGE_UPDATE_PATH");	

	newData.Format("%s",  UpdatePath_Result);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"RESULT_UPDATE_PATH");

	newData.Format("%s", m_HWParam.strOrgImgPath);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"IMAGE_SAVE_PATH");
	newData.Format("%s", m_HWParam.strResultPath);
	m_GridEditHWParam.SetItemText(InputIndex++, 1,newData);// "RESULT_SAVE_PATH ");		


	m_GridEditHWParam.SetItemText(InputIndex++, 1, "");
	newData.Format("%d", m_HWParam.m_VSB_Size);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);// "VSB_Size");	
	newData.Format("%d", m_HWParam.m_ProcImageW);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);// "GrabImageW");	
	newData.Format("%d", m_HWParam.m_ProcImageH);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"GrabImageH");		
	newData.Format("%d", m_HWParam.m_DefectMaxCntInImg);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"DefectMaxCntInImg");	
	newData.Format("%d", m_HWParam.m_DefectReviewSizeW);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"DefectReviewSizeW");	
	newData.Format("%d", m_HWParam.m_DefectReviewSizeH);		
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"DefectReviewSizeH");		
	newData.Format("%d", m_HWParam.m_INSPECTION_CAM_COUNT);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"INSPECTION_CAM_COUNT");	
	newData.Format("%d", m_HWParam.m_INSPECTION_SCAN_WIDTH);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"INSPECTION_SCAN_WIDTH");	
	newData.Format("%d", m_HWParam.m_INSPECTION_SCAN_HEIGHT);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"INSPECTION_SCAN_HEIGHT");	

	newData.Format("%d", m_HWParam.m_AOI_BLOCK_SCAN_START_X);		
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"BLOCK_SCAN_START_X");	
	newData.Format("%d", m_HWParam.m_AOI_BLOCK_SCAN_START_Y);		
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"BLOCK_SCAN_START_Y");	

	InputIndex++;
	newData.Format("%d", m_HWParam.m_AOI_CAM_DISTANCE_X);		
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"CAM_DISTANCE_X");
	newData.Format("%d", m_HWParam.m_AOI_CAM_DISTANCE_Y);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"CAM_DISTANCE_Y");		

	newData.Format("%d", m_HWParam.m_AOI_SCAN_START_OFFSET_ODD);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"SCAN_START_OFFSET_ODD");	
	newData.Format("%d", m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);// "SCAN_START_OFFSET_EVEN");

	InputIndex++;
	//Lens Type 추가.(20120726) Start
	newData.Format("%d", m_AOI_CAM_DISTANCE_X_Type2);		
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"CAM_DISTANCE_X");
	newData.Format("%d", m_AOI_CAM_DISTANCE_Y_Type2);		
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"CAM_DISTANCE_Y");		

	newData.Format("%d", m_AOI_SCAN_START_OFFSET_ODD_Type2);		
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"SCAN_START_OFFSET_ODD");	
	newData.Format("%d", m_AOI_SCAN_START_OFFSET_EVEN_Type2);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);// "SCAN_START_OFFSET_EVEN");
	//Lens Type 추가.(20120726) End


		


	m_GridEditHWParam.SetItemText(InputIndex++, 1, "");	

	newData.Format("%d", m_HWParam.m_OnlyGrabImageSave);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"OnlyGrabImageSave");	
	newData.Format("%d", m_HWParam.m_SaveDefectOrgImage);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"SaveDefectOrgImage");
	newData.Format("%d", m_HWParam.DefectSaveFileType);
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"DefectSaveFileType");	
	newData.Format("%d", m_HWParam.PadInspectAreaType);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);//"PadInspectAreaType");	

	InputIndex++;//27
	for(int DataStep = 0; DataStep<5; DataStep++)
	{
		newData.Format("%d", m_InspectParam.nZoneStep[DataStep]);
		m_GridEditHWParam.SetItemText((InputIndex)+DataStep, 1, newData);
	}
	InputIndex+=6;
	for(int DataStep = 0; DataStep<5; DataStep++)
	{
		newData.Format("%d", m_InspectParam.nZoneOffset[DataStep]);
		m_GridEditHWParam.SetItemText(InputIndex+DataStep, 1, newData);
	}
	InputIndex+=6;


	newData.Format("%d", m_AxisOverLap);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);

	m_GridEditHWParam.SetItemText(InputIndex++, 1, "");	

	newData.Format("%f", m_HWParam.m_LensCurvature_Type1);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);


	newData.Format("%f", m_HWParam.m_LensCurvature_Type2);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);

	InputIndex++;
	newData.Format("%s", strDalsaCamPath);	
	m_GridEditHWParam.SetItemText(InputIndex++, 1, newData);
	
	return true;
}
void CHWParamEdit::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE)
	{
		m_fnInspectPParam(m_TaskNum);
		m_fnLoadHWParam(m_TaskNum);
		m_fnUpdateUI();
		m_GridEditHWParam.Refresh();
	}
}


void CHWParamEdit::OnBnClickedBtSave()
{
	CString newWriteData;
	newWriteData.Format("%d",m_HWParam.m_VSB_Size);
	WritePrivateProfileString(Section_HWINFO, Key_VSB_Size, newWriteData, m_IniFilePath);

	newWriteData.Format("%d",m_HWParam.m_ProcImageW);
	WritePrivateProfileString(Section_HWINFO, Key_GrabImageW, newWriteData, m_IniFilePath);

	newWriteData.Format("%d",m_HWParam.m_ProcImageH);
	WritePrivateProfileString(Section_HWINFO, Key_GrabImageH, newWriteData, m_IniFilePath);


	newWriteData.Format("%d",m_HWParam.m_DefectMaxCntInImg);
	WritePrivateProfileString(Section_HWINFO, Key_DefectMaxCntInImg, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_HWParam.m_DefectReviewSizeW);
	WritePrivateProfileString(Section_HWINFO, Key_DefectReviewSizeW, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_HWParam.m_DefectReviewSizeH);
	WritePrivateProfileString(Section_HWINFO, Key_DefectReviewSizeH, newWriteData, m_IniFilePath);


	newWriteData.Format("%d",m_HWParam.m_INSPECTION_CAM_COUNT);
	WritePrivateProfileString(Section_HWINFO, Key_INSPECTION_CAM_COUNT, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_HWParam.m_INSPECTION_SCAN_WIDTH);
	WritePrivateProfileString(Section_HWINFO, Key_INSPECTION_SCAN_WIDTH, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_HWParam.m_INSPECTION_SCAN_HEIGHT);
	WritePrivateProfileString(Section_HWINFO, Key_INSPECTION_SCAN_HEIGHT, newWriteData, m_IniFilePath);
	

	newWriteData.Format("%d",m_HWParam.m_AOI_BLOCK_SCAN_START_X);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_BLOCK_SCAN_START_X, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_HWParam.m_AOI_BLOCK_SCAN_START_Y);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_BLOCK_SCAN_START_Y, newWriteData, m_IniFilePath);
	
	newWriteData.Format("%d",m_HWParam.m_AOI_CAM_DISTANCE_X);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_CAM_DISTANCE_X_LENS_TYPE1, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_HWParam.m_AOI_CAM_DISTANCE_Y);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1, newWriteData, m_IniFilePath);


	newWriteData.Format("%d",m_HWParam.m_AOI_SCAN_START_OFFSET_ODD);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1, newWriteData, m_IniFilePath);

	newWriteData.Format("%d",m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1, newWriteData, m_IniFilePath);


	//Lens Type 추가.(20120726) Start
	newWriteData.Format("%d",m_AOI_CAM_DISTANCE_X_Type2);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_CAM_DISTANCE_X_LENS_TYPE2, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_AOI_CAM_DISTANCE_Y_Type2);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_CAM_DISTANCE_Y_LENS_TYPE2, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_AOI_SCAN_START_OFFSET_ODD_Type2);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE2, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_AOI_SCAN_START_OFFSET_EVEN_Type2);
	WritePrivateProfileString(Section_HWINFO, Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE2, newWriteData, m_IniFilePath);
	//Lens Type 추가.(20120726) End

	newWriteData.Format("%d",m_HWParam.m_OnlyGrabImageSave);
	WritePrivateProfileString(Section_HWINFO, Key_OnlyGrabImageSave, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_HWParam.m_SaveDefectOrgImage);
	WritePrivateProfileString(Section_HWINFO, Key_SaveDefectOrgImage, newWriteData, m_IniFilePath);

	newWriteData.Format("%d",m_HWParam.DefectSaveFileType);
	WritePrivateProfileString(Section_HWINFO, Key_DefectSaveFileType, newWriteData, m_IniFilePath);
	newWriteData.Format("%d",m_HWParam.PadInspectAreaType);
	WritePrivateProfileString(Section_HWINFO, Key_PadInspectAreaType, newWriteData, m_IniFilePath);

	newWriteData.Format("%d",m_HWParam.CamScanDirectionPort);
	WritePrivateProfileString(Section_HWINFO, Key_SERIAL_PORT_CAM_DERCTION, newWriteData, m_IniFilePath);


	newWriteData.Format("%s",m_HWParam.strResultPath);
	WritePrivateProfileString(Section_HWINFO, Key_RESULT_SAVE_PATH, newWriteData, m_IniFilePath);

	newWriteData.Format("%s",m_HWParam.strOrgImgPath);
	WritePrivateProfileString(Section_HWINFO, Key_IMAGE_SAVE_PATH, newWriteData, m_IniFilePath);


	newWriteData.Format("%s",UpdatePath_ImgData);
	WritePrivateProfileString(Section_HWINFO, Key_IMAGE_UPDATE_PATH, newWriteData, m_IniFilePath);

	newWriteData.Format("%s",UpdatePath_Result);
	WritePrivateProfileString(Section_HWINFO, Key_RESULT_UPDATE_PATH, newWriteData, m_IniFilePath);

	newWriteData.Format("%d",m_AxisOverLap);
	WritePrivateProfileString(Section_HWINFO, Key_AXIS_Y_OVERLAP, newWriteData, m_IniFilePath);

	newWriteData.Format("%f",m_HWParam.m_LensCurvature_Type1);
	WritePrivateProfileString(Section_HWINFO, Key_LENS_CURVATURE_TYPE1, newWriteData, m_IniFilePath);

	newWriteData.Format("%f",m_HWParam.m_LensCurvature_Type2);
	WritePrivateProfileString(Section_HWINFO, Key_LENS_CURVATURE_TYPE2, newWriteData, m_IniFilePath);


	newWriteData.Format("%s",strDalsaCamPath);
	WritePrivateProfileString(Section_HWINFO, Key_DALSA_CCF_PATH, newWriteData, m_IniFilePath);


	CString MakeKey;

	for(int DataStep = 0; DataStep<5; DataStep++)
	{
		MakeKey.Format("%s%d",Key_LensZone_X, DataStep+1);
		newWriteData.Format("%d", m_InspectParam.nZoneStep[DataStep]);
		WritePrivateProfileString(Section_LENS_OFFSET, MakeKey, newWriteData, m_IniFilePath);

		MakeKey.Format("%s%d",Key_LensOffset_X, DataStep+1);
		newWriteData.Format("%d", m_InspectParam.nZoneOffset[DataStep]);
		WritePrivateProfileString(Section_LENS_OFFSET, MakeKey, newWriteData, m_IniFilePath);
	}
	//////////////////////////////////////////////////////////////////////////
	m_fnLoadHWParam(m_TaskNum);

	m_fnInspectPParam(m_TaskNum);
	m_fnUpdateUI();
	m_GridEditHWParam.Refresh();
}


void CHWParamEdit::OnBnClickedBtCancel()
{
	this->ShowWindow(SW_HIDE);
}

bool CHWParamEdit::m_fnLoadHWParam(int TaskNum)
{
	int nInputTaskNum = TaskNum;
	char *pLinkPath = NULL;

	char path_buffer[_MAX_PATH];
	char chFilePath[_MAX_PATH] = {0,};
	char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
	//실행 파일 이름을 포함한 Full path 가 얻어진다.
	::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
	//디렉토리만 구해낸다.
	_splitpath_s(path_buffer, drive, dir, fname, ext);
	strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
	strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

	switch(nInputTaskNum)
	{
	case 21: 	pLinkPath = HW_PARAM_PATH_TN21; break;
	//case 11: 	pLinkPath = HW_PARAM_PATH_TN11; break;
	//case 12: 	pLinkPath = HW_PARAM_PATH_TN12; break;
	//case 13: 	pLinkPath = HW_PARAM_PATH_TN13; break;
	//case 14: 	pLinkPath = HW_PARAM_PATH_TN14; break;
	default:
		return false;
	}

	strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);
	m_IniFilePath.Format("%s", chFilePath);

	m_HWParam.m_VSB_Size					= GetPrivateProfileInt(Section_HWINFO,Key_VSB_Size, 0, chFilePath);

	m_HWParam.m_ProcImageW				= GetPrivateProfileInt(Section_HWINFO,Key_GrabImageW, 0, chFilePath);
	m_HWParam.m_ProcImageH				= GetPrivateProfileInt(Section_HWINFO,Key_GrabImageH, 0, chFilePath);

	m_HWParam.m_DefectMaxCntInImg		= GetPrivateProfileInt(Section_HWINFO,Key_DefectMaxCntInImg, 0, chFilePath);
	m_HWParam.m_DefectReviewSizeW		= GetPrivateProfileInt(Section_HWINFO,Key_DefectReviewSizeW, 0, chFilePath);
	m_HWParam.m_DefectReviewSizeH		= GetPrivateProfileInt(Section_HWINFO,Key_DefectReviewSizeH, 0, chFilePath);


	m_HWParam.m_INSPECTION_CAM_COUNT		= GetPrivateProfileInt(Section_HWINFO,Key_INSPECTION_CAM_COUNT, 0, chFilePath);
	m_HWParam.m_INSPECTION_SCAN_WIDTH		= GetPrivateProfileInt(Section_HWINFO,Key_INSPECTION_SCAN_WIDTH, 0, chFilePath);
	m_HWParam.m_INSPECTION_SCAN_HEIGHT		= GetPrivateProfileInt(Section_HWINFO,Key_INSPECTION_SCAN_HEIGHT, 0, chFilePath);

	m_HWParam.m_AOI_BLOCK_SCAN_START_X		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_BLOCK_SCAN_START_X, 0, chFilePath);
	m_HWParam.m_AOI_BLOCK_SCAN_START_Y		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_BLOCK_SCAN_START_Y, 0, chFilePath);


	m_HWParam.m_AOI_CAM_DISTANCE_X		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE1, 0, chFilePath);
	m_HWParam.m_AOI_CAM_DISTANCE_Y		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE1, 0, chFilePath);

	m_HWParam.m_AOI_SCAN_START_OFFSET_ODD		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE1, 0, chFilePath);
	m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE1, 0, chFilePath);


	//Lens Type 추가.(20120726) Start
	m_AOI_CAM_DISTANCE_X_Type2		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_X_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
	if(m_AOI_CAM_DISTANCE_X_Type2 == 0xFFFFFFFF)
		m_AOI_CAM_DISTANCE_X_Type2 = m_HWParam.m_AOI_CAM_DISTANCE_X;
	m_AOI_CAM_DISTANCE_Y_Type2		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_CAM_DISTANCE_Y_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
	if(m_AOI_CAM_DISTANCE_Y_Type2 == 0xFFFFFFFF)
		m_AOI_CAM_DISTANCE_Y_Type2 = m_HWParam.m_AOI_CAM_DISTANCE_Y;

	m_AOI_SCAN_START_OFFSET_ODD_Type2		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_ODD_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
	if(m_AOI_SCAN_START_OFFSET_ODD_Type2 == 0xFFFFFFFF)
		m_AOI_SCAN_START_OFFSET_ODD_Type2 = m_HWParam.m_AOI_SCAN_START_OFFSET_ODD;
	m_AOI_SCAN_START_OFFSET_EVEN_Type2		= GetPrivateProfileInt(Section_HWINFO,Key_AOI_SCAN_START_OFFSET_EVEN_LENS_TYPE2, 0xFFFFFFFF, chFilePath);
	if(m_AOI_SCAN_START_OFFSET_EVEN_Type2 == 0xFFFFFFFF)
		m_AOI_SCAN_START_OFFSET_EVEN_Type2 = m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN;
	//Lens Type 추가.(20120726) End


	m_HWParam.m_OnlyGrabImageSave 		= GetPrivateProfileInt(Section_HWINFO,Key_OnlyGrabImageSave, 0, chFilePath);
	m_HWParam.m_SaveDefectOrgImage		= GetPrivateProfileInt(Section_HWINFO,Key_SaveDefectOrgImage, 0, chFilePath);
	m_HWParam.DefectSaveFileType			= GetPrivateProfileInt(Section_HWINFO,Key_DefectSaveFileType, 0, chFilePath);

	m_HWParam.PadInspectAreaType			= GetPrivateProfileInt(Section_HWINFO,Key_PadInspectAreaType, 0, chFilePath);

	m_HWParam.CamScanDirectionPort			= GetPrivateProfileInt(Section_HWINFO,Key_SERIAL_PORT_CAM_DERCTION, 1, chFilePath);


	m_AxisOverLap			= GetPrivateProfileInt(Section_HWINFO,Key_AXIS_Y_OVERLAP, 300, chFilePath);



	char ReadData[512];
	memset(ReadData, 0x00, 512);
	GetPrivateProfileString(Section_HWINFO, Key_LENS_CURVATURE_TYPE1, "0.0",  ReadData, 511, chFilePath);
	m_HWParam.m_LensCurvature_Type1 = (float)atof(ReadData);
	memset(ReadData, 0x00, 512);
	GetPrivateProfileString(Section_HWINFO, Key_LENS_CURVATURE_TYPE2, "0.0",  ReadData, 511, chFilePath);
	m_HWParam.m_LensCurvature_Type2 = (float)atof(ReadData);



	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_RESULT_SAVE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	sprintf_s(m_HWParam.strResultPath,"%s",  ReadData);


	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_IMAGE_SAVE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	sprintf_s(m_HWParam.strOrgImgPath,"%s",  ReadData);

	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_IMAGE_UPDATE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	UpdatePath_ImgData.Format("%s",  ReadData);

	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_RESULT_UPDATE_PATH, "d:\\",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	UpdatePath_Result.Format("%s",  ReadData);


	memset(ReadData, 0x00, 512);
	if(GetPrivateProfileString(Section_HWINFO, Key_DALSA_CCF_PATH, "T_HS-S0-12K40-00-R_Line_Master.ccf",  ReadData, 511, chFilePath)==FALSE)
		return FALSE;
	strDalsaCamPath.Format("%s",  ReadData);

	return true;
}

bool CHWParamEdit::m_fnInspectPParam(int TaskNum)
{
	int nInputTaskNum = TaskNum;
	char *pLinkPath = NULL;

	char path_buffer[_MAX_PATH];
	char chFilePath[_MAX_PATH] = {0,};
	char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
	//실행 파일 이름을 포함한 Full path 가 얻어진다.
	::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
	//디렉토리만 구해낸다.
	_splitpath_s(path_buffer, drive, dir, fname, ext);
	strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
	strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

	switch(nInputTaskNum)
	{
	case 21: 	pLinkPath = HW_PARAM_PATH_TN21; break;
	//case 11: 	pLinkPath = HW_PARAM_PATH_TN11; break;
	//case 12: 	pLinkPath = HW_PARAM_PATH_TN12; break;
	//case 13: 	pLinkPath = HW_PARAM_PATH_TN13; break;
	//case 14: 	pLinkPath = HW_PARAM_PATH_TN14; break;
	default:
		return false;
	}

	strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);
	m_IniFilePath.Format("%s", chFilePath);

	CString MakeKey;
	
	for(int DataStep = 0; DataStep<5; DataStep++)
	{
		MakeKey.Format("%s%d",Key_LensZone_X, DataStep+1);
		m_InspectParam.nZoneStep[DataStep]		= GetPrivateProfileInt(Section_LENS_OFFSET, MakeKey, 8192, chFilePath);

		MakeKey.Format("%s%d",Key_LensOffset_X, DataStep+1);
		m_InspectParam.nZoneOffset[DataStep]	= GetPrivateProfileInt(Section_LENS_OFFSET, MakeKey, 0, chFilePath);
	}
	return true;
}