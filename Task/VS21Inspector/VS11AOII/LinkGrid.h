#pragma once
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#include "../Grid/GridCtrl.h"
#include "../Grid/NewCellTypes/GridURLCell.h"
#include "../Grid/NewCellTypes/GridCellCombo.h"
#include "../Grid/NewCellTypes/GridCellCheck.h"
#include "../Grid/NewCellTypes/GridCellNumeric.h"
#include "../Grid/NewCellTypes/GridCellDateTime.h"

#define		WM_USER_BTUPDN_EVN							WM_USER + 1000
#define  USER_BT_DOWN		1
#define  USER_BT_UP			2
#include "../ClassButton/RoundButton2.h"
#include "../ClassButton/RoundButtonStyle.h"


