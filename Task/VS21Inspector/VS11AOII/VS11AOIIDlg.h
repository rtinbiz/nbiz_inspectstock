
// VS11AOIIDlg.h : 헤더 파일
//

#pragma once
// C4996 warning expected
//#pragma warning(disable : 4996)
//#pragma warning(disable : 4995)
//#include "..\..\..\CommonHeader\Class\VS64Exceptions.h"
//#include "..\..\..\CommonHeader\Class\InterServerInterface.h"
//#include "ProcInitial.h"

#include "HWParamEdit.h"
#include "CommThread.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "PictureEx.h"
#include "perfmon.h"
// CVS11AOIIDlg 대화 상자

struct FDC_Data
{          
	int INS_HDD_C_CAPA;
	int INS_HDD_D_CAPA;
	int INS_CPU_SHARE;
	int INS_MEMORY;
	FDC_Data()
	{
		memset(this, 0x00, sizeof(FDC_Data));
	}
};
class CVS11AOIIDlg : public CDialogEx, public CProcInitial
{
// 생성입니다.
public:
	CVS11AOIIDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VS11AOII_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public://Log Print
	CInterServerInterface* m_pServerInterface;
	void			AOI_AppendList(char *pMsg,...);
	void			m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...);
private:
	CCommThread			m_CamDicOgj;	
private:
	
//	short	m_AOI_State;
	void			AOI_ClearIndicator(void);
	short			AOI_StateChange(int setAOIState, int AlarmCode = 0);
	LRESULT		AOI_UpdateStateMessage(WPARAM wParam, LPARAM lParam);
	//bool			AOI_StateChangeCheck(int CheckAOIState);
	void			AOI_UpdateAOIStateToServer();
private://AOI Server Interface.
	void			Sys_fnInitVS64Interface(void);
	int				Sys_fnAnalyzeMsg(CMDMSG* cmdMsg);//Message Process Function.
	LRESULT		Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam);
	//////////////////////////////////////////////////////////////////////////
	void			AOI_UpdateProcState();
private:
	SAOI_IMAGE_INFO			m_stMaskImageInfo;
	SAOI_COMMON_PARA		m_stCommonPara;	

	SAOI_IMAGE_INFO			m_stPADMaskImageInfo[4];//0:상, 1:하, 2:좌, 3:우
	SAOI_COMMON_PARA		m_stPADCommonPara[4];	
	
	GLASS_PARAM				m_GlassReicpeParam;

	CPerfMon					m_PerfMon;
	int m_nCPU;
	FDC_Data					m_FdcData;

	BOOL _ChangeTestFile(char *szTestFilePath);
	void _WaitForFinishingInspection();

public :
	char m_szTestFileName[MAX_PATH];
	static UINT _TestLocalFolderThread(LPVOID pParam);

public:
	void DebugScreenCapture(char *pSavePath);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedBtGrabFileStart();
	afx_msg void OnBnClickedBtGrabFileEnd();
	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedBtShowMap();

	CListBox m_ViewStateList;

	CProgressCtrl m_proTotalStep;
	CProgressCtrl m_proLineStep;
	CProgressCtrl m_proPADProcStep;

	double		  m_BufferDiskSize;
	CProgressCtrl m_proRamDiskSize;

	CProgressCtrl m_proAFValue;

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);


	CFont			m_InspectActFont;
	CColorStaticST	m_stGPUProc;
	CColorStaticST m_stCutterProc[4];
	CColorStaticST m_stResultProc[4];
	
	CFont				m_TaskNumFont;
	CColorStaticST m_stMainTaskNum;

	CFont				m_StateFont;
	CColorStaticST m_stTaskState;

	CFont				m_NormalFont;
	CColorStaticST m_stScanLineCnt;
	CColorStaticST m_stScanImgCnt;

	CColorStaticST m_stDLLProcState[4];

	CColorStaticST m_stMasterTask;

	CFont				m_DspNumFont;
	CColorStaticST m_stDefectWriteCnt;
	CColorStaticST m_stOrgImgWriteCnt;
	void SetUIStaticData();
	afx_msg void OnMove(int x, int y);

	CFont				m_CommandFont;

	CListBox m_listInspectParam;
	CListBox m_listHWParam;
	CListBox m_listGlassParam;
	void WriteLogInspectionParam();
	void UpdateInspectParam(SAOI_IMAGE_INFO *pNewdata1, SAOI_COMMON_PARA*pNewData2);
	void UpdateGlassParam(GLASS_PARAM *pNewData);
	void UpdateHWParam(HW_PARAM *pNewData);

	void _TestSetGlassParam(GLASS_PARAM &dGlassParam);
	void _TestSetActiveParam(SAOI_IMAGE_INFO &dImageInfo, SAOI_COMMON_PARA &dCommonParam);
	void _TestSetPadParam(SAOI_IMAGE_INFO *listImageInfo, SAOI_COMMON_PARA *listCommonParam);

	//////////////////////////////////////////////////////////////////////////
	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btEditHWParam;
	CRoundButton2 m_btShowDrawParam;
	CRoundButton2 m_btOpenFolder;
	CRoundButton2 m_btSWVersion;


	CRoundButton2 m_btOpenCamCTRL;

	CHWParamEdit *m_pDlgEditHWParam;
	afx_msg void OnBnClickedBtEditHwparam();

	//CPictureEx m_stTestGif;
	CPictureEx m_stSaveGif;
	CPictureEx m_stCopyGif;
	CPictureEx m_stBackupGif;
	afx_msg void OnBnClickedBtShowFolder();
	
	afx_msg void OnBnClickedChkSendT75();
	afx_msg void OnBnClickedChkSaveCutPadDraw();
	afx_msg void OnBnClickedChkSaveSmallImg();



	CListBox m_ViewCamCMDList;
	CEdit m_edSendCamCmd;
	afx_msg void OnBnClickedChkSaveInspectImg();
	afx_msg void OnBnClickedChkDrawDefctArea();
	
	
	CString m_IniFilePath;
	void m_fnLoadSaveParam(CString InifilePath);
	afx_msg void OnBnClickedBtCamCtrl();
	
	afx_msg void OnBnClickedChkSaveHistogram();
	
	afx_msg void OnBnClickedRdsaveBmp();
	afx_msg void OnBnClickedRdsaveJpg();
	
	afx_msg void OnBnClickedBtSwVersion();
	afx_msg void OnBnClickedChkSavePadInspectImg();
	afx_msg void OnEnChangeEdSmallSizeX();
	afx_msg void OnEnChangeEdSmallSizeY();
	afx_msg void OnBnClickedChkSaveHistoOrtCutpos();
	afx_msg void OnEnChangeEdPadalignDiswidth();
	afx_msg void OnEnChangeEdPadalignOverwidth();
	afx_msg void OnEnChangeEdPadalignProcskipvalue();
	afx_msg void OnBnClickedChkSavePadAlignImg();
	afx_msg void OnBnClickedChkSavePadImbBmp();
	afx_msg void OnEnChangeEdPadBlurStep();
	afx_msg void OnBnClickedChkUnusePadAlign();
	afx_msg void OnEnChangeEdHistoMaxPer();

	int m_LineScanWidth;
	afx_msg void OnEnChangeEdLineScanWidth();
	afx_msg void OnBnClickedChkSavePadUseMaxHigh();
	afx_msg void OnBnClickedChkSavePadMarkNotChange();
	afx_msg void OnBnClickedChkSavePadUseCanny();
	afx_msg void OnEnChangeEdPadalignCannyThreshold1();
	afx_msg void OnEnChangeEdPadalignCannyThreshold2();
	afx_msg void OnBnClickedTestLocalFile();
	afx_msg void OnBnClickedTestLocalFolder();
	CString m_strTestFolder;
};