#pragma once
//////////////////////////////////////////////////////////////////////////
/** 
@brief		Glass 내의 Block 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
typedef struct BLOCK_INFO_TAG
{
	char	 m_BlockName[NAME_BUF_CNT];
	CRect m_BlockPos;

	BLOCK_INFO_TAG()
	{
		//int SizeofTyup = sizeof(BLOCK_INFO_TAG);
		memset(this,0x00, sizeof(BLOCK_INFO_TAG));
	}
} BLOCKINFO;
//////////////////////////////////////////////////////////////////////////
/** 
@brief		Glass 내의 Cell 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
typedef struct CELLINFO_TAG
{
	char	 m_CellName[NAME_BUF_CNT];
	CRect m_CellPos;
	CELLINFO_TAG()
	{
		//int SizeofTyup = sizeof(CELLINFO_TAG);
		memset(this,0x00, sizeof(CELLINFO_TAG));
	}
} CELLINFO;
//////////////////////////////////////////////////////////////////////////
/** 
@brief		Glass 내의 Cell 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
typedef struct SCANIMGINFO_TAG
{
	int			m_ScanLineNum;
	int			m_ScanImgIndex;
	CRect		m_ScanImgPos;


	//////////////////////////////////////////////////////////////////////////
	//Active, Pad 부의 영역을 계산 하기 위해서.
	int			m_findSubRecCnt[5];					//<<< Scan Image에 대한 Cell의 Active, Pad부에 대해 몇개의 겹침영역이 생기는지 Count 한다.
	CRect		m_IspectAreaSubtractRect[5][4];	//<<< 겹침 영역이 생기면 계속 추가 한다. (Active, Top, Bottom, Left, Right에대한 최대 4개의 겹침 영역을 의미한다.


	SCANIMGINFO_TAG()
	{
		//int SizeofTyup = sizeof(SCANIMGINFO_TAG);
		memset(this,0x00, sizeof(SCANIMGINFO_TAG));
	}
} SCANIMGINFO;
//////////////////////////////////////////////////////////////////////////
/** 
@brief		Glass 내의 Cell Pad부 정보.

@remark
-			Center Point X,Y를 기준으로 한다..
-			
@author		최지형
@date		2012.00.00
*/
typedef struct PADINFO_TAG
{
	char	 m_CellName[NAME_BUF_CNT];
	CRect m_CellPos_Top;			//<<<Cell의 상단 Pad부
	CRect m_CellPos_Bottom;		//<<<Cell의 하단 Pad부
	CRect m_CellPos_Left;			//<<<Cell의 왼쪽단 Pad부
	CRect m_CellPos_Right;			//<<<Cell의 오른쪽단 Pad부
	PADINFO_TAG()
	{
		memset(this,0x00, sizeof(PADINFO_TAG));
	}
} PADINFO;