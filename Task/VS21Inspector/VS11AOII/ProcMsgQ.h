#pragma once
#include "..\..\..\CommonHeader\Inspection\InspectDataQ.h"

typedef struct DataNodeObj_AGT
{
	DataNodeObj_AGT *	m_pFrontNode;
	DataNodeObj_AGT *	m_pNextNode;

	InspectDataQ	*	pInpectObj;


	DataNodeObj_AGT()
	{
		m_pFrontNode	= NULL;
		m_pNextNode		= NULL;
	}

}DataNodeObj;
//////////////////////////////////////////////////////////////////////////
//Sen Command Queue./ Receive Data Queue.
class ProcMsgQ  
{
public:
	ProcMsgQ();
	~ProcMsgQ();
private:
	DataNodeObj m_HeadNode;
	DataNodeObj m_TailNode; 
	CRITICAL_SECTION m_CrtSection;
	int		m_CntNode;
public:

	int		QGetCnt(){return m_CntNode;};
	void	QPutNode(InspectDataQ	*pNewObj);
	InspectDataQ	*QGetNode();
	void	QClean(void);
};
//////////////////////////////////////////////////////////////////////////

typedef struct DefectNodeObj_AGT
{
	DefectNodeObj_AGT *	m_pFrontNode;
	DefectNodeObj_AGT *	m_pNextNode;

	int					m_DefectType;
	int					m_ScanLine;
	int					m_ScanCount;
	//Pad부 전용 Data
	int					m_IndexPad;
	int					m_FlagMentPAD;

	int					m_DefectIndex;

	DEFECT_DATA			m_DefImgPos;			//<<<Image상의 Defect위치
	COORD_DINFO			m_DefCoordPos;			//<<<좌표계 상의 Defect위치
	//////////////////////////////////////////////////////////////////////////
	int					m_ReviewImgSize;
	BYTE			*	m_pDefectImg;			//<<<지정 Size만큼의 Defect Image.

	DefectNodeObj_AGT()
	{
		m_pFrontNode	= nullptr;
		m_pNextNode		= nullptr;

		m_DefectType	= 0;
		m_ScanLine		= 0;
		m_ScanCount		= 0;

		memset(&m_DefImgPos, 0x00, sizeof(DEFECT_DATA));			
		memset(&m_DefCoordPos, 0x00, sizeof(COORD_DINFO));
		//////////////////////////////////////////////////////////////////////////
		m_ReviewImgSize = 0;
		m_pDefectImg	= nullptr;	

	}

}DefectNodeObj;
//////////////////////////////////////////////////////////////////////////
//Sen Command Queue./ Receive Data Queue.
class DefectDataQ  
{
public:
	DefectDataQ();
	~DefectDataQ();
private:
	DefectNodeObj m_HeadNode;
	DefectNodeObj m_TailNode; 
	CRITICAL_SECTION m_CrtSection;
	int		m_CntNode;
public:

	int		QGetCnt(){return m_CntNode;};
	void	QPutNodeAct(int ScnaLine, int ScanCnt, int DefectIndex, DEFECT_DATA*pDefImgPos, COORD_DINFO *pDefCoordPos, BYTE*	pDefectImg, int ImgSize);
	void	QPutNodePad(int ScnaLine, int PadPos, int PadIndex, int FlagMent,  int DefectIndex, DEFECT_DATA*pDefImgPos, COORD_DINFO *pDefCoordPos, BYTE* pDefectImg, int ImgSize);
	bool	QGetNode(int *pType, int *pScnaLine, int *pScanCnt,  int *pPadIndex,  int *pFlagMent,int *pDefectInde, DEFECT_DATA*pDefImgPos, COORD_DINFO *pDefCoordPos, BYTE* pDefectImg, int *pImgSize);

	void	QClean(void);
};


typedef struct IMGNodeObj_AGT
{
	IMGNodeObj_AGT *	m_pFrontNode;
	IMGNodeObj_AGT *	m_pNextNode;

	int ImgType;
	int ScanLine;
	int ScanCount;
	CRect ScanImagePos;

	IplImage *pSaveImg;

	IMGNodeObj_AGT()
	{
		ImgType		= 0;//1:Active Image, 2 :Pad Area Image
		ScanLine		= -1;
		ScanCount	= -1;

		pSaveImg	= nullptr;									
	}

}IMGNodeObj;
//////////////////////////////////////////////////////////////////////////
//Sen Command Queue./ Receive Data Queue.
class ImageSaveQ  
{
public:
	ImageSaveQ(int BufferLimitCnt);
	~ImageSaveQ();
private:
	IMGNodeObj m_HeadNode;
	IMGNodeObj m_TailNode; 
	CRITICAL_SECTION m_CrtSection;
	int		m_CntNode;
	int		m_BufferLimitCnt;
public:

	int		QGetCnt(){return m_CntNode;};
	void	QPutNode(int Type, int ScanLine, int ScanCount, IplImage *pTargetImage, CRect *pScanImgPos);
	IMGNodeObj * QGetNode();
	////bool QPopNode();
	void	QClean(void);
};