#include "StdAfx.h"
#include "GrabberCtrl.h"
#include "AOIGrabCutter.h"
#ifdef MATROX_LINE_SCAN_GRABBER
#include "Mil_FnGrab.h"


AOIGrabCutterQ*		G_pGrabCutter				= nullptr;
//Local 변수이나 Speed Up을 위해 전역으로.
AOI_ImageCtrlQ*		G_pIDTCtrlPtr				= nullptr;
InspectDataQ *		G_pGrabIMG					= nullptr;
MIL_ID				G_ModifiedBufferId			= 0;//Grab MIL ID
PROC_STATE	*		G_pUpdateState				= nullptr;
CoordinatesProc*	G_MilpCoordObj				= nullptr;

DWORD				G_tickTickN = 0;
char				G_ImageSavePath[MAX_PATH]	= {"S:\\AOI_Out\\12_1234.bmp"};

int					G_OnlyGrabIndex				= 0;

extern ImageSaveQ	G_AlignImgQ;
/*
*	Module Name		:	fn_GrabProcess
*	Parameter		:	Grab Parameter
*	Return			:	State.
*	Function		:	Local Grab Test를 위한 Thread Function.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
MIL_INT MFTYPE fn_GrabProcess(MIL_INT HookType, MIL_ID HookId, void MPTYPE *HookDataPtr)
{
	MIL_INT				returnValue = 0;
	//if(HookDataPtr == nullptr)
	//	return returnValue;
	G_pGrabCutter	= (AOIGrabCutterQ *) HookDataPtr;
	G_pIDTCtrlPtr	= G_pGrabCutter->m_pIDTCtrlPtr;
	G_MilpCoordObj	= G_pIDTCtrlPtr->m_pCoordObj;
	G_pUpdateState	= G_pIDTCtrlPtr->m_pUpdateState;

	if(G_pUpdateState->m_bAlignScan == true)
	{
		IplImage *pNewGrabImg = cvCreateImage(cvSize(G_MilpCoordObj->m_HWParam.m_ProcImageW, G_MilpCoordObj->m_HWParam.m_ProcImageH), IPL_DEPTH_8U, 1);
		MdigGetHookInfo(HookId, M_MODIFIED_BUFFER+M_BUFFER_ID, &G_ModifiedBufferId);	//Image Buffer Get.
		MbufGet(G_ModifiedBufferId,		(void*)pNewGrabImg->imageData);
		CRect ScanImgPos;
		G_AlignImgQ.QPutNode(1,1,1, pNewGrabImg,&ScanImgPos);
		cvReleaseImage(&pNewGrabImg);
	}
	else if(G_pIDTCtrlPtr->m_OnlyGrabImageSave == true)
	{
		sprintf_s(	G_ImageSavePath, MAX_PATH-1,"%s%04d.bmp",	
			G_pIDTCtrlPtr->m_OrgImgSavePath.GetBuffer(G_pIDTCtrlPtr->m_OrgImgSavePath.GetLength()),
			G_OnlyGrabIndex++);
		//////////////////////////////////////////////////////////////////////////
		//Image Export (BMP File)
		MdigGetHookInfo(HookId, M_MODIFIED_BUFFER+M_BUFFER_ID, &G_ModifiedBufferId);
		MbufExport(G_ImageSavePath, M_BMP, G_ModifiedBufferId);//8MByte Size Image(BMP)
	}
	else	if(G_MilpCoordObj->m_OneLineScnaImgCnt > G_pIDTCtrlPtr->m_GrabScanImageCnt)
	{//조건 검사불필요 해짐.(Result에서 Grab상태 Check로 이용.)
		MdigGetHookInfo(HookId, M_MODIFIED_BUFFER+M_BUFFER_ID, &G_ModifiedBufferId);	//Image Buffer Get.
		MbufGet(G_ModifiedBufferId,		(void*)G_pGrabCutter->QGetImgPtr());
		G_pGrabCutter->QProcImgData();
		
		//G_pGrabIMG	= G_pIDTCtrlPtr->m_fnQGetInspectPTR_Grabber();//적재할 Queue의 대상위치를 가져온다.
		//if(G_pGrabIMG != nullptr )
		//{
		//	G_pUpdateState->m_nNoneProcImgCount =0;
		//	MdigGetHookInfo(HookId, M_MODIFIED_BUFFER+M_BUFFER_ID, &G_ModifiedBufferId);	//Image Buffer Get.
		//	MbufGet(G_ModifiedBufferId,		(void*)G_pGrabIMG->m_pGrabOrgImage->imageData);
		//	//cvZero(G_pGrabIMG->m_pGrabImage);

		//	G_pIDTCtrlPtr->m_GrabScanImageCnt +=1;

		//	G_pGrabIMG->m_nOneTick				= ::GetTickCount();
		//	G_pGrabIMG->m_ScanLine				= G_pIDTCtrlPtr->m_GrabScanLineNum;
		//	G_pGrabIMG->m_ScanImageIndex	= G_pIDTCtrlPtr->m_GrabScanImageCnt;

		//	G_pGrabIMG->m_fnQSetProcessEnd(QDATA_PROC_END_GRAB);//현 Image는 Grabber에서 Grab을 종료 했다.

		//	//if(G_pUpdateState != nullptr)
		//	{//첫장은 오류, 1번에서 2번까지의 시간을 잰다.
		//		G_pUpdateState->m_GrabImageCnt++;
		//		G_pUpdateState->m_nMilGrabFunProTick = ::GetTickCount() -G_tickTickN;
		//		G_tickTickN = ::GetTickCount();
		//	}
		//}
		//else
		//{//심각한오류 Grab된 Data를 쓸곳이 없다.ㅡ,.ㅡ;;;
		//	//if(G_pUpdateState != nullptr)
		//	{
		//		G_pUpdateState->m_bMilGrabFunMemError = true;
		//		G_pUpdateState->m_nNoneProcImgCount++;

		//		::SendMessage(G_pUpdateState->m_UpdateUIHandle, WM_USER_STATE_UPDATE, nBiz_AOI_STATE_ALARM, ALARM_CODE_GrabBuffer);
		//	}
		//}
	}
	else if(G_MilpCoordObj->m_OneLineScnaImgCnt == G_pIDTCtrlPtr->m_GrabScanImageCnt)
	{
		//임시로.
		extern CGraberCtrl	G_ImgGrabberCtrl;
		G_ImgGrabberCtrl.GrabEnd();		

		G_pGrabCutter->SaveLineScanImg();
	}

	return returnValue;
}

#endif