
// VS11AOIIDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VS11AOII.h"
#include "VS11AOIIDlg.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#include "MasterInterfaceDefine.h"

#include <TLHELP32.H>   //  CreateToolhelp32Snapshot 함수를 사용하기 위함 
BOOL FindProcess(CString szProcessName)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 sEntry32;
	sEntry32.dwSize = sizeof(PROCESSENTRY32); // 이거 빠지면 검색 잘 안됨

	if((hSnapshot == INVALID_HANDLE_VALUE) || (hSnapshot == NULL)) return FALSE;

	BOOL bRemain = Process32First(hSnapshot, &sEntry32);
	while(bRemain)
	{
		CString szExeName;
		szExeName.Format(_T("%s"), sEntry32.szExeFile);

		if(szExeName.CompareNoCase(szProcessName) == 0) 
		{
			//CloseToolhelp32Snapshot(hSnapshot);
			CloseHandle(hSnapshot);
			return TRUE;
		}

		bRemain = Process32Next(hSnapshot, &sEntry32);
	}

	//CloseToolhelp32Snapshot(hSnapshot);
	CloseHandle(hSnapshot);

	return FALSE;
} 


extern CInterServerInterface	G_ServerInterface;
extern PROC_STATE				G_ProcessState;
extern AOI_ImageCtrlQ			G_InspectionDataQ;
extern PAD_ImageCtrlQ			G_PadInspectDataQ[];
extern CGraberCtrl				G_ImgGrabberCtrl;
extern LocalImgGrab				G_FileGrabObj;
extern CDefectFileFormat		G_WriteRetObj;
extern CoordinatesProc			G_CoordProcObj;

extern DefectDataQ				G_InspectDefectQ;// Defect 검출 Data가 적제 된다.
extern ImageSaveQ				G_InspectSaveImgQ;

extern ImageSaveQ				G_AlignImgQ;

extern ImageSaveQ				G_HistogramImgQ;


//extern PROC_PAD_ALIGN		G_ProcPadAlign;

int										G_MasterTaskIndex = 10;
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edVersionInfo;
	CString m_strVersionInfo;
	afx_msg void OnBnClickedOk();
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
	, m_strVersionInfo(_T(""))
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ED_VERSION_INFO, m_edVersionInfo);
	DDX_Text(pDX, IDC_ED_VERSION_INFO, m_strVersionInfo);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CAboutDlg::OnBnClickedOk)
END_MESSAGE_MAP()

void CAboutDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnOK();
}

// CVS11AOIIDlg 대화 상자




CVS11AOIIDlg::CVS11AOIIDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVS11AOIIDlg::IDD, pParent)
{
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pServerInterface	= &G_ServerInterface;

	G_ProcessState.m_UnitState			= nBiz_AOI_STATE_IDLE;
	m_szTestFileName[0] = 0x00;
	m_strTestFolder = _T("Z:\\DefectData(2015-07-13)\\Defect 분류2\\TEST2\\[01]Org_Img\\Act_01_000.jpg");
//	m_strTestFolder = _T("Z:\\DefectData(2015-07-13)\\Defect 분류1\\TEST1\\[01]Org_Img");
}

void CVS11AOIIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_AOI_STATE_LIST, m_ViewStateList);
	DDX_Control(pDX, IDC_PRO_TOTAL_STEP, m_proTotalStep);
	DDX_Control(pDX, IDC_PRO_LINE_STEP, m_proLineStep);
	DDX_Control(pDX, IDC_PRO_PAD_PROC, m_proPADProcStep);
	DDX_Control(pDX, IDC_ST_INSPECT_TASK_NUM, m_stMainTaskNum);
	DDX_Control(pDX, IDC_ST_TASK_STATE, m_stTaskState);
	DDX_Control(pDX, IDC_ST_MAP_INFO_IMG_CNT, m_stScanLineCnt);
	DDX_Control(pDX, IDC_ST_MAP_INFO_SCNLINE_CNT, m_stScanImgCnt);
	DDX_Control(pDX, IDC_ST_INSPECT_PRO1, m_stDLLProcState[0]);
	DDX_Control(pDX, IDC_ST_INSPECT_PRO2, m_stDLLProcState[1]);
	DDX_Control(pDX, IDC_ST_INSPECT_PRO3, m_stDLLProcState[2]);
	DDX_Control(pDX, IDC_ST_INSPECT_PRO4, m_stDLLProcState[3]);
	DDX_Control(pDX, IDC_AOI_INSPECT_PARAM_LIST, m_listInspectParam);
	DDX_Control(pDX, IDC_AOI_GLASS_PARAM_LIST, m_listGlassParam);
	DDX_Control(pDX, IDC_AOI_HW_PARAM_LIST, m_listHWParam);
	DDX_Control(pDX, IDC_ST_MASTER_TASK_NUM, m_stMasterTask);
	DDX_Control(pDX, IDC_BT_EDIT_HWPARAM, m_btEditHWParam);
	DDX_Control(pDX, IDC_BT_SHOW_MAP, m_btShowDrawParam);
	DDX_Control(pDX, IDC_ST_WRITE_DEFECT_BUF, m_stDefectWriteCnt);
	DDX_Control(pDX, IDC_ST_WRITE_ORTIMG_BUF, m_stOrgImgWriteCnt);
	DDX_Control(pDX, IDC_ST_DELET_FILE, m_stSaveGif);
	DDX_Control(pDX, IDC_ST_DELET_FILE2, m_stCopyGif);
	DDX_Control(pDX, IDC_ST_DELET_FILE3, m_stBackupGif);
	DDX_Control(pDX, IDC_BT_SHOW_FOLDER, m_btOpenFolder);
	//DDX_Control(pDX, IDC_ST_TEST_GIF, m_stTestGif);
	DDX_Control(pDX, IDC_AOI_CAM_CMD_LIST, m_ViewCamCMDList);
	DDX_Control(pDX, IDC_ED_SEMD_CMD, m_edSendCamCmd);
	DDX_Control(pDX, IDC_ST_INSPECT_PRO_DATA1, m_stGPUProc);
	DDX_Control(pDX, IDC_ST_CUTTER_PRO_DATA1, m_stCutterProc[0]);
	DDX_Control(pDX, IDC_ST_CUTTER_PRO_DATA2, m_stCutterProc[1]);
	DDX_Control(pDX, IDC_ST_CUTTER_PRO_DATA3, m_stCutterProc[2]);
	DDX_Control(pDX, IDC_ST_CUTTER_PRO_DATA4, m_stCutterProc[3]);

	DDX_Control(pDX, IDC_ST_RESULT_PRO_DATA1, m_stResultProc[0]);
	DDX_Control(pDX, IDC_ST_RESULT_PRO_DATA2, m_stResultProc[1]);
	DDX_Control(pDX, IDC_ST_RESULT_PRO_DATA3, m_stResultProc[2]);
	DDX_Control(pDX, IDC_ST_RESULT_PRO_DATA4, m_stResultProc[3]);

	DDX_Control(pDX, IDC_BT_CAM_CTRL, m_btOpenCamCTRL);
	DDX_Control(pDX, IDC_PRO_RAM_DISK_SIZE, m_proRamDiskSize);
	DDX_Control(pDX, IDC_PRO_IMG_AF_VALUE, m_proAFValue);
	DDX_Control(pDX, IDC_BT_SW_VERSION, m_btSWVersion);
	DDX_Text(pDX, IDC_ED_TEST_FOLDER, m_strTestFolder);
}

BEGIN_MESSAGE_MAP(CVS11AOIIDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BT_GRAB_FILE_START, &CVS11AOIIDlg::OnBnClickedBtGrabFileStart)
	ON_BN_CLICKED(IDC_BT_GRAB_FILE_END, &CVS11AOIIDlg::OnBnClickedBtGrabFileEnd)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BT_SHOW_MAP, &CVS11AOIIDlg::OnBnClickedBtShowMap)

	ON_MESSAGE(WM_USER_CALLBACK, &CVS11AOIIDlg::Sys_fnMessageCallback)
	ON_WM_CTLCOLOR()
	ON_WM_MOVE()
	ON_BN_CLICKED(IDC_BT_EDIT_HWPARAM, &CVS11AOIIDlg::OnBnClickedBtEditHwparam)

	ON_BN_CLICKED(IDC_BT_SHOW_FOLDER, &CVS11AOIIDlg::OnBnClickedBtShowFolder)

	ON_MESSAGE(WM_USER_STATE_UPDATE, &CVS11AOIIDlg::AOI_UpdateStateMessage)
	ON_BN_CLICKED(IDC_CHK_SEND_T75, &CVS11AOIIDlg::OnBnClickedChkSendT75)
	ON_BN_CLICKED(IDC_CHK_SAVE_CUT_PAD_DRAW, &CVS11AOIIDlg::OnBnClickedChkSaveCutPadDraw)
	ON_BN_CLICKED(IDC_CHK_SAVE_SMALL_IMG, &CVS11AOIIDlg::OnBnClickedChkSaveSmallImg)
	ON_BN_CLICKED(IDC_CHK_SAVE_INSPECT_IMG, &CVS11AOIIDlg::OnBnClickedChkSaveInspectImg)
	ON_BN_CLICKED(IDC_CHK_DRAW_DEFCT_AREA, &CVS11AOIIDlg::OnBnClickedChkDrawDefctArea)
	ON_BN_CLICKED(IDC_BT_CAM_CTRL, &CVS11AOIIDlg::OnBnClickedBtCamCtrl)
	ON_BN_CLICKED(IDC_CHK_SAVE_HISTOGRAM, &CVS11AOIIDlg::OnBnClickedChkSaveHistogram)
	ON_BN_CLICKED(IDC_RDSAVE_BMP, &CVS11AOIIDlg::OnBnClickedRdsaveBmp)
	ON_BN_CLICKED(IDC_RDSAVE_JPG, &CVS11AOIIDlg::OnBnClickedRdsaveJpg)
	ON_BN_CLICKED(IDC_BT_SW_VERSION, &CVS11AOIIDlg::OnBnClickedBtSwVersion)
	ON_BN_CLICKED(IDC_CHK_SAVE_PAD_INSPECT_IMG, &CVS11AOIIDlg::OnBnClickedChkSavePadInspectImg)
	ON_EN_CHANGE(IDC_ED_SMALL_SIZE_X, &CVS11AOIIDlg::OnEnChangeEdSmallSizeX)
	ON_EN_CHANGE(IDC_ED_SMALL_SIZE_Y, &CVS11AOIIDlg::OnEnChangeEdSmallSizeY)
	ON_BN_CLICKED(IDC_CHK_SAVE_HISTO_ORT_CUTPOS, &CVS11AOIIDlg::OnBnClickedChkSaveHistoOrtCutpos)
	ON_EN_CHANGE(IDC_ED_PADALIGN_DISWIDTH, &CVS11AOIIDlg::OnEnChangeEdPadalignDiswidth)
	ON_EN_CHANGE(IDC_ED_PADALIGN_OVERWIDTH, &CVS11AOIIDlg::OnEnChangeEdPadalignOverwidth)
	ON_EN_CHANGE(IDC_ED_PADALIGN_PROCSKIPVALUE, &CVS11AOIIDlg::OnEnChangeEdPadalignProcskipvalue)
	ON_BN_CLICKED(IDC_CHK_SAVE_PAD_ALIGN_IMG, &CVS11AOIIDlg::OnBnClickedChkSavePadAlignImg)
	ON_BN_CLICKED(IDC_CHK_SAVE_PAD_IMB_BMP, &CVS11AOIIDlg::OnBnClickedChkSavePadImbBmp)
	ON_EN_CHANGE(IDC_ED_PAD_BLUR_STEP, &CVS11AOIIDlg::OnEnChangeEdPadBlurStep)
	ON_BN_CLICKED(IDC_CHK_UNUSE_PAD_ALIGN, &CVS11AOIIDlg::OnBnClickedChkUnusePadAlign)
	ON_EN_CHANGE(IDC_ED_HISTO_MAX_PER, &CVS11AOIIDlg::OnEnChangeEdHistoMaxPer)
	ON_EN_CHANGE(IDC_ED_LINE_SCAN_WIDTH, &CVS11AOIIDlg::OnEnChangeEdLineScanWidth)
	ON_BN_CLICKED(IDC_CHK_SAVE_PAD_USE_MAX_HIGH, &CVS11AOIIDlg::OnBnClickedChkSavePadUseMaxHigh)
	ON_BN_CLICKED(IDC_CHK_SAVE_PAD_MARK_NOT_CHANGE, &CVS11AOIIDlg::OnBnClickedChkSavePadMarkNotChange)
	ON_BN_CLICKED(IDC_CHK_SAVE_PAD_USE_CANNY, &CVS11AOIIDlg::OnBnClickedChkSavePadUseCanny)
	ON_EN_CHANGE(IDC_ED_PADALIGN_CANNY_THRESHOLD1, &CVS11AOIIDlg::OnEnChangeEdPadalignCannyThreshold1)
	ON_EN_CHANGE(IDC_ED_PADALIGN_CANNY_THRESHOLD2, &CVS11AOIIDlg::OnEnChangeEdPadalignCannyThreshold2)
	ON_BN_CLICKED(IDC_TEST_LOCAL_FILE, &CVS11AOIIDlg::OnBnClickedTestLocalFile)
	ON_BN_CLICKED(IDC_TEST_LOCAL_FOLDER, &CVS11AOIIDlg::OnBnClickedTestLocalFolder)
END_MESSAGE_MAP()

#pragma comment (lib, "version.lib")
// CVS11AOIIDlg 메시지 처리기
#include <Winnetwk.h>
#include "afxwin.h"
BOOL CVS11AOIIDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	G_ProcessState.m_UpdateUIHandle = this->m_hWnd;
	G_ProcessState.m_UnitState = nBiz_AOI_STATE_IDLE;
	AOI_StateChange(nBiz_AOI_STATE_IDLE);
	
	//////////////////////////////////////////////////////////////////////////
	//1) Server Interface Connection.
	Sys_fnInitVS64Interface();
	CString NewWindowsName;
	NewWindowsName.Format("Visual Station 64 - Inspection Task(No.%02d)", m_ProcInitVal.m_nTaskNum);
#ifdef MATROX_LINE_SCAN_GRABBER
	NewWindowsName.AppendFormat(" -MATROX");
#endif
#ifdef DALSA_LINE_SCAN_GRABBER
	NewWindowsName.AppendFormat(" -DALSA ");
#endif

#ifdef TEST_INSPECTION_LOAD_FILE
	NewWindowsName.AppendFormat(" -FILE LOADING");
#endif
	SetWindowText(NewWindowsName);
	NewWindowsName.Format("-%d-", m_ProcInitVal.m_nTaskNum);
	m_stMainTaskNum.SetWindowText(NewWindowsName);
	SetUIStaticData();
	//////////////////////////////////////////////////////////////////////////
	double TotalSize = ((((double)(G_CoordProcObj.m_HWParam.m_ProcImageW*G_CoordProcObj.m_HWParam.m_ProcImageH*G_CoordProcObj.m_HWParam.m_VSB_Size)/1024.0)/1024.0));
	NewWindowsName.Format("ImageSize(%0.3fMByte)",TotalSize);
	GetDlgItem(IDC_ST_INSPECT_INFO)->SetWindowText(NewWindowsName);
	//////////////////////////////////////////////////////////////////////////
	//Defect Review HDC
	GetDlgItem(IDC_ST_DEFECT_VIEW)->GetClientRect(&G_ProcessState.m_ViewRect);
	G_ProcessState.m_pHDcView =  &GetDlgItem(IDC_ST_DEFECT_VIEW)->GetDC()->m_hAttribDC;
	 
	GetDlgItem(IDC_ST_HISTOGRAM)->GetClientRect(&G_ProcessState.m_HistViewRect);
	G_ProcessState.m_HistpHDcView =  &GetDlgItem(IDC_ST_HISTOGRAM)->GetDC()->m_hAttribDC;

	//GetDlgItem(IDC_ST_DEFECT_ORG_IMG)->GetClientRect(&G_ProcessState.m_ViewSaveImgRect);
	//G_ProcessState.m_pHDcSaveImgView =  &GetDlgItem(IDC_ST_DEFECT_ORG_IMG)->GetDC()->m_hAttribDC;

	//////////////////////////////////////////////////////////////////////////
	//4)Update UI Timer
	for(int RetStep=0; RetStep<AOI_DLL_PROCESS_CNT; RetStep++)
	{
		GetDlgItem( IDC_ST_INSPECT_PRO1+RetStep)->ShowWindow(SW_SHOW);
		GetDlgItem( IDC_ST_INSPECT_PRO_DATA1+RetStep)->ShowWindow(SW_SHOW);
	}

	for(int CutStpe=0; CutStpe<PADCUT_PROC_CNT; CutStpe++)
	{
		GetDlgItem( IDC_ST_CUTTER_PROC1+CutStpe)->ShowWindow(SW_SHOW);
		GetDlgItem( IDC_ST_CUTTER_PRO_DATA1+CutStpe)->ShowWindow(SW_SHOW);
	}
	//Result Process State.
	for(int RetStep=0; RetStep<RET_PROC_CNT; RetStep++)
	{
		GetDlgItem( IDC_ST_RESULT_PROC1+RetStep)->ShowWindow(SW_SHOW);
		GetDlgItem( IDC_ST_RESULT_PRO_DATA1+RetStep)->ShowWindow(SW_SHOW);
	}
	//UI Update Timer
	 ::SetTimer(this->m_hWnd, UI_UPDATE_PROC_STATE, UI_UPDATE_Interval, 0);
	//Task State Timer
	 ::SetTimer(this->m_hWnd, UI_UPDATE_STATE_AOI_TASK, UI_AOI_TASK_Interval, 0);
	 //FDC State Timer
	 ::SetTimer(this->m_hWnd, UI_FDC_DATA, UI_FDC_DATA_Interval, 0);

	 //////////////////////////////////////////////////////////////////////////
	 //cam 방향 전환.
	 m_CamDicOgj.m_fnInitialize(G_CoordProcObj.m_HWParam.CamScanDirectionPort);//Com Port 5 Open(index 4)
	 m_CamDicOgj.m_fnSendCameraDirection( FALSE );
	 //////////////////////////////////////////////////////////////////////////
	 this->MoveWindow(0, 0, 1235, 840);
	 //////////////////////////////////////////////////////////////////////////
	 //UI Update
	 m_proTotalStep.SetBarColor(RGB(255,0,0));
	 m_proLineStep.SetBarColor(RGB(0,255,0));
	 m_proPADProcStep.SetBarColor(RGB(0,0,255));

	 m_proTotalStep.SetBkColor(RGB(150,150,150));
	 m_proLineStep.SetBkColor(RGB(150,150,150));
	 m_proPADProcStep.SetBkColor(RGB(150,150,150));


	 m_proTotalStep.SetFont(&m_DspNumFont);
	 m_proLineStep.SetFont(&m_DspNumFont);
	 m_proPADProcStep.SetFont(&m_DspNumFont);

	ULARGE_INTEGER FreeBytesAvailableToCaller;
	ULARGE_INTEGER TotalNumberOfBytes;
	ULARGE_INTEGER TotalNumberOfFreeBytes;
	GetDiskFreeSpaceEx(BUFFER_DISK_NAME, &FreeBytesAvailableToCaller, &TotalNumberOfBytes, &TotalNumberOfFreeBytes);

	 // 전체 디스크 공간을 MB단위로 연산한다.
	 m_BufferDiskSize = (double)TotalNumberOfBytes.QuadPart;//sectors_per_cluster*bytes_per_sector*totalnumber_of_clusters;
	 m_BufferDiskSize = m_BufferDiskSize / 1024/ 1024;

	 // 남은 디스크 공간을 MB단위로 연산한다.
	 double disk_free_size =  (double)TotalNumberOfFreeBytes.QuadPart;//sectors_per_cluster*bytes_per_sector*number_of_freeclusters;
	 disk_free_size = disk_free_size /1024/ 1024;

	m_proRamDiskSize.SetBkColor(RGB(150,150,150));
	m_proRamDiskSize.SetBarColor(RGB(0,255,0));
	m_proRamDiskSize.SetRange32(0, (int)m_BufferDiskSize);
	m_proRamDiskSize.SetPos((int)(m_BufferDiskSize-disk_free_size));

	m_proAFValue.SetWindowText(_T("AF Value : "));
	m_proAFValue.SetBkColor(RGB(150,150,150));
	m_proAFValue.SetBarColor(RGB(0,255,0));
	m_proAFValue.SetRange32(0, 100);
	m_proAFValue.SetPos(0);

	
	CString strBufsize;

	strBufsize.Format("%dMB", (int)m_BufferDiskSize);
	GetDlgItem(IDC_ST_BUF_DISK_TOTAL_SIZE)->SetWindowText(strBufsize);
	strBufsize.Format("%dMB", (int)disk_free_size);
	GetDlgItem(IDC_ST_BUF_DISK_FREE_SIZE)->SetWindowText(strBufsize);

	strBufsize.Format("%dMB", (int)(m_BufferDiskSize-disk_free_size));
	GetDlgItem(IDC_ST_BUF_DISK_USE_SIZE)->SetWindowText(strBufsize);
	

	 //if(m_stTestGif.Load(MAKEINTRESOURCE(IDR_TESTGIF1), _T("TestGif")))
	 //{
		// m_stTestGif.Draw();
	 //}
	 if(m_stSaveGif.Load(MAKEINTRESOURCE(IDR_SAVEGIF1), _T("SaveGif")))
	 {
		 m_stSaveGif.Draw();
	 }
	 if(m_stCopyGif.Load(MAKEINTRESOURCE(IDR_COPYGIF1), _T("CopyGif")))
	 {
		 m_stCopyGif.Draw();
	 }
	 if(m_stBackupGif.Load(MAKEINTRESOURCE(IDR_BACKUPGIF1), _T("BackupGif")))
	 {
		 m_stBackupGif.Draw();
	 }


	 ///////////////////////////////////////////////////////////////////////////////////////////////
	 // 현재 실행된 프로그램의 경로를 저장할 변수이다.
	 char temp_path[MAX_PATH];

	 // 현재 실행된 프로그램의 경로를 얻는다.
	 GetModuleFileName(AfxGetInstanceHandle(), temp_path, sizeof(temp_path)); 
	 // 버전 정보를 얻기 위해 사용할 핸들값을 저장하는 변수이다.
	 DWORD h_version_handle;
	 // 버전정보는 항목을 사용자가 추가/삭제 할수 있기 때문에 고정된 크기가 아니다. 
	 // 따라서 현재 프로그램의 버전정보에 대한 크기를 얻어서 그 크기에 맞는 메모리를 할당하고 작업해야한다.
	 DWORD version_info_size = GetFileVersionInfoSize(temp_path, &h_version_handle);

	 // 버전정보를 저장하기 위한 시스템 메모리를 생성한다. ( 핸들 형식으로 생성 )
	 HANDLE h_memory = GlobalAlloc(GMEM_MOVEABLE, version_info_size); 
	 // 핸들 형식의 메모리를 사용하기 위해서 해당 핸들에 접근할수 있는 주소를 얻는다.
	 LPVOID p_info_memory = GlobalLock(h_memory);

	 // 현재 프로그램의 버전 정보를 가져온다.
	 GetFileVersionInfo(temp_path, h_version_handle, version_info_size, p_info_memory);
	 // 버전 정보에 포함된 각 항목별 정보 위치를 저장할 변수이다. 이 포인터에 전달된 주소는 
	 // p_info_memory 의 내부 위치이기 때문에 해제하면 안됩니다. 
	 // ( p_info_memory 를 참조하는 형식의 포인터 입니다. )
	 char *p_data = NULL;
	 // 실제로 읽은 정보의 크기를 저장할 변수이다.
	 UINT data_size = 0;
	 // 버전정보에 포함된 FileVersion 정보를 얻어서 출력한다.
	 CString VersionInfo;
	 if(VerQueryValue(p_info_memory, "\\StringFileInfo\\041204b0\\FileVersion", (void **)&p_data, &data_size)){ 
		 VersionInfo.Format("%s", p_data);
		 SetDlgItemText(IDC_SW_VERSION, VersionInfo);
		 SetDlgItemText(IDC_BT_SW_VERSION, VersionInfo);

	 } 
	 // 버전 정보를 저장하기 위해 사용했던 메모리를 해제한다.
	 GlobalUnlock(h_memory); 
	 GlobalFree(h_memory);
	 //////////////////////////////////////////////////////////////////////////
	 //HANDLE ProcProcess = GetCurrentProcess();
	 //SetPriorityClass(ProcProcess, HIGH_PRIORITY_CLASS);
	CheckRadioButton( IDC_RDSAVE_BMP, IDC_RDSAVE_JPG, IDC_RDSAVE_JPG);

	if(FindProcess(CString("VS81CSDT.EXE")) == FALSE)
	{
		char RunPath[256];
		sprintf_s(RunPath, "D:\\nBiz_InspectStock\\AOI_DebugT81.BAT %d", m_ProcInitVal.m_nTaskNum+70);
	//	system(RunPath);

		WinExec(RunPath, 1);
	}

	//FDC 관련
	if (!m_PerfMon.Initialize())
	{
		m_vtAddLog(LOG_LEVEL_1, "Could not initialize CPerfMon!");
	}
	m_nCPU  = m_PerfMon.AddCounter(CNTR_CPU);



	GetDlgItem(IDC_ED_LINE_SCAN_WIDTH)->SetWindowText("0");
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CVS11AOIIDlg::m_vtAddLog(USHORT uLogLevel, const char* pErrLog, ...)
{
	char	cLogBuffer[MAX_LOG_BUFF];
	va_list vargs;

	va_start(vargs, pErrLog);

	vsprintf_s(cLogBuffer, pErrLog, (va_list)vargs);

	va_end(vargs);
	
	m_pServerInterface->m_fnPrintLog(m_ProcInitVal.m_nTaskNum ,uLogLevel, cLogBuffer);

}
void CVS11AOIIDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVS11AOIIDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVS11AOIIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



BOOL CVS11AOIIDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  //ESC키 눌렀을때 방지..
		return TRUE;

	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) //Enter키 눌렀을때 방지..
	{
		CString StrSendCmd;
		GetDlgItem(IDC_ED_SEMD_CMD)->GetWindowText(StrSendCmd);
		if(/*StrSendCmd.GetLength()>0 &&*/ StrSendCmd.GetLength()<250)
		{
			char SendBuffer[256];
			sprintf_s(SendBuffer,"%s\r", StrSendCmd);
			m_CamDicOgj.WriteComm((BYTE*)SendBuffer, (DWORD)strlen(SendBuffer));
			m_vtAddLog(LOG_LEVEL_1, "Send Cam CMD: %s",StrSendCmd);

			if(m_ViewCamCMDList.GetCount()>LISTDATACOUNT)
			{
				m_ViewCamCMDList.DeleteString(m_ViewCamCMDList.GetCount()-LISTDATACOUNT);
			}
			CString newStr;
			newStr.Format("Ok>%s",StrSendCmd );
			m_ViewCamCMDList.AddString(newStr);
			m_ViewCamCMDList.SetScrollPos(SB_VERT , m_ViewCamCMDList.GetCount(), TRUE);
			m_ViewCamCMDList.SetTopIndex(m_ViewCamCMDList.GetCount()-1);
			GetDlgItem(IDC_ED_SEMD_CMD)->SetWindowText("");
		}
		
		return TRUE;
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_TAB) //Enter키 눌렀을때 방지..
	{
		if(GetDlgItem(IDC_ST_TEST_GIF)->IsWindowVisible() == TRUE)
			GetDlgItem(IDC_ST_TEST_GIF)->ShowWindow(SW_HIDE);
		else
			GetDlgItem(IDC_ST_TEST_GIF)->ShowWindow(SW_SHOW);
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
//////////////////////////////////////////////////////////////////////////
/*
*	Module Name		:	AOI_InitVS64Interface
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	VS64 Connection Function.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/


void CVS11AOIIDlg::Sys_fnInitVS64Interface()
{
	if(1 < __argc)
	{
		int nInputTaskNum = atoi(__argv[1]);
		char *pLinkPath = NULL;

		char *pIniLinkPath = NULL;

		char path_buffer[_MAX_PATH];
		char chFilePath[_MAX_PATH] = {0,};
		char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		//실행 파일 이름을 포함한 Full path 가 얻어진다.
		::GetModuleFileName(NULL, path_buffer, _MAX_PATH);
		//디렉토리만 구해낸다.
		_splitpath_s(path_buffer, drive, dir, fname, ext);
		strncpy_s(chFilePath,_MAX_PATH, drive,strlen(drive));
		strncpy_s(&chFilePath[strlen(drive)], _MAX_PATH - strlen(drive), dir,strlen(dir));

		switch(nInputTaskNum)
		{
		case 21: 	pIniLinkPath = HW_PARAM_PATH_TN21; break;
		//case 11: 	pIniLinkPath = HW_PARAM_PATH_TN11; break;
		//case 12: 	pIniLinkPath = HW_PARAM_PATH_TN12; break;
		//case 13: 	pIniLinkPath = HW_PARAM_PATH_TN13; break;
		//case 14: 	pIniLinkPath = HW_PARAM_PATH_TN14; break;
		default:
			return ;
		}
		m_IniFilePath.Format("%s%s", chFilePath, pIniLinkPath);
		m_fnLoadSaveParam(m_IniFilePath);


		switch(nInputTaskNum)
		{

			
		case 21: 	pLinkPath = MSG_TASK_INI_FullPath_TN21; break;
		//case 11: 	pLinkPath = MSG_WIN_TASK_INI_FILE_FULL_NAME_TN11; break;
		//case 12: 	pLinkPath = MSG_WIN_TASK_INI_FILE_FULL_NAME_TN12; break;
		//case 13: 	pLinkPath = MSG_WIN_TASK_INI_FILE_FULL_NAME_TN13; break;
		//case 14: 	pLinkPath = MSG_WIN_TASK_INI_FILE_FULL_NAME_TN14; break;
		default:
			{
				CString ErrorMessage;
				ErrorMessage.Format("Unknown TaskNum(%d)", nInputTaskNum);
				AfxMessageBox(ErrorMessage);
				SendMessage(WM_CLOSE, 0,  0);	
				m_vtAddLog(LOG_LEVEL_1, "Unknown TaskNum(%d)", nInputTaskNum);
			}return;
		}

		strcpy_s(&chFilePath[strlen(chFilePath)],_MAX_PATH - strlen(chFilePath),pLinkPath);

		m_fnReadDatFile(chFilePath);
	}
	else
	{
		AfxMessageBox("Input Run Task Number");
		m_vtAddLog(LOG_LEVEL_1, "Input Run Task Number");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}

	m_pServerInterface->m_fnSetLogTaskInfo(m_ProcInitVal.m_nLogTaskNo, m_ProcInitVal.m_nLogBlockLovel);	
	if(m_pServerInterface->m_fnRegisterTask(m_ProcInitVal.m_nTaskNum, m_ProcInitVal.m_sServerIP, m_ProcInitVal.m_nPortNo) != 0)
	{
		AfxMessageBox("Client Register Fail");
		m_vtAddLog(LOG_LEVEL_1,"Client Register Fail");
		SendMessage(WM_CLOSE, 0,  0);
		return;
	}
	else
	{
		//m_pServerInterface->m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, "Client Register OK : %d", m_ProcInitVal.m_nTaskNum);
		m_vtAddLog(LOG_LEVEL_1,"Client Register OK : %d", m_ProcInitVal.m_nTaskNum);
	}

	m_pServerInterface->m_fnSetAppWindowHandle(m_hWnd);
	m_pServerInterface->m_fnSetAppUserMessage(WM_USER_CALLBACK);
}

void CVS11AOIIDlg::m_fnLoadSaveParam(CString InifilePath)
{
	G_ProcessState.bTEST_SAVE_CUT_AREA_DRAW					= GetPrivateProfileInt(Section_Data_Options,Key_SavePadCutDrawImg, 0, InifilePath)>0?true:false;
	G_ProcessState.bTEST_DEFECT_IMG_SAVE_JPG						= GetPrivateProfileInt(Section_Data_Options,Key_SaveSmallImage, 0, InifilePath)>0?true:false;
	G_ProcessState.bTEST_INSPECT_IMG									= GetPrivateProfileInt(Section_Data_Options,Key_SaveInspectImage, 0, InifilePath)>0?true:false;
	//G_ProcessState.bTEST_INSPECT_IMG_DRAW_DEFECT				= GetPrivateProfileInt(Section_Data_Options,Key_DrawDefectArea, 0, InifilePath)>0?true:false;
	G_ProcessState.bTEST_ACTIVE_IMG_HISTOGRAM_IMG_SAVE	= GetPrivateProfileInt(Section_Data_Options,Key_SaveActiveAreaHistogram, 0, InifilePath)>0?true:false;

	G_ProcessState.bTEST_SAVE_PAD_AREA								= GetPrivateProfileInt(Section_Data_Options,Key_SavePadOrgImgALL, 0, InifilePath)>0?true:false;

	G_ProcessState.bTEST_SAVE_PAD_AREA_BMP 					= GetPrivateProfileInt(Section_Data_Options,Key_SavePadOrgImgSaveBmp, 0, InifilePath)>0?true:false;
	G_ProcessState.nTEST_SAVE_PAD_AREA_BlurStep				= GetPrivateProfileInt(Section_Data_Options,Key_SavePadOrgImgBlurStep, 0, InifilePath);

	G_ProcessState.nSmallDefectSizeX								= GetPrivateProfileInt(Section_Data_Options, Key_SmallDefectSizeX, 100, InifilePath);
	G_ProcessState.nSmallDefectSizeY								= GetPrivateProfileInt(Section_Data_Options, Key_SmallDefectSizeY, 100, InifilePath);

	G_ProcessState.dbLightingHistMaxPresent						= GetPrivateProfileInt(Section_Data_Options, Key_AOI_LightingHistMaxPresent, 10, InifilePath);
	if(G_ProcessState.bTEST_SAVE_CUT_AREA_DRAW == true)
		CheckDlgButton(IDC_CHK_SAVE_CUT_PAD_DRAW, true);
	else
		CheckDlgButton(IDC_CHK_SAVE_CUT_PAD_DRAW, false);
		
	if(G_ProcessState.bTEST_DEFECT_IMG_SAVE_JPG == true)
		CheckDlgButton(IDC_CHK_SAVE_SMALL_IMG, true);
	else
		CheckDlgButton(IDC_CHK_SAVE_SMALL_IMG, false);

	if(G_ProcessState.bTEST_INSPECT_IMG == true)
		CheckDlgButton(IDC_CHK_SAVE_INSPECT_IMG, true);
	else
		CheckDlgButton(IDC_CHK_SAVE_INSPECT_IMG, false);

	//if(G_ProcessState.bTEST_INSPECT_IMG_DRAW_DEFECT == true)
	//	CheckDlgButton(IDC_CHK_DRAW_DEFCT_AREA, true);
	//else
	//	CheckDlgButton(IDC_CHK_DRAW_DEFCT_AREA, false);

	if(G_ProcessState.bTEST_ACTIVE_IMG_HISTOGRAM_IMG_SAVE == true)
		CheckDlgButton(IDC_CHK_SAVE_HISTOGRAM, true);
	else
		CheckDlgButton(IDC_CHK_SAVE_HISTOGRAM, false);


	if(G_ProcessState.bTEST_SAVE_PAD_AREA == true)
		CheckDlgButton(IDC_CHK_SAVE_PAD_INSPECT_IMG, true);
	else
		CheckDlgButton(IDC_CHK_SAVE_PAD_INSPECT_IMG, false);


	if(G_ProcessState.bTEST_SAVE_PAD_AREA_BMP == true)
		CheckDlgButton(IDC_CHK_SAVE_PAD_IMB_BMP, true);
	else
		CheckDlgButton(IDC_CHK_SAVE_PAD_IMB_BMP, false);

	CString strNewData;
	strNewData.Format("%d", G_ProcessState.nSmallDefectSizeX	);
	GetDlgItem(IDC_ED_SMALL_SIZE_X)->SetWindowText(strNewData);
	strNewData.Format("%d", G_ProcessState.nSmallDefectSizeY	);
	GetDlgItem(IDC_ED_SMALL_SIZE_Y)->SetWindowText(strNewData);
	strNewData.Format("%d", G_ProcessState.nTEST_SAVE_PAD_AREA_BlurStep	);
	GetDlgItem(IDC_ED_PAD_BLUR_STEP)->SetWindowText(strNewData);

	strNewData.Format("%d", G_ProcessState.dbLightingHistMaxPresent	);
	GetDlgItem(IDC_ED_HISTO_MAX_PER)->SetWindowText(strNewData);
	
	G_ProcessState.bTEST_USE_PAD_ALIGN = GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_UsePadAlign, 1, InifilePath)==1?true:false;

	if(G_ProcessState.bTEST_USE_PAD_ALIGN==true)
		CheckDlgButton(IDC_CHK_UNUSE_PAD_ALIGN, BST_UNCHECKED);
	else
		CheckDlgButton(IDC_CHK_UNUSE_PAD_ALIGN, BST_CHECKED);

	G_ProcPadAlign.m_AlignWidth				= GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_AlignWidth, 100, InifilePath);
	G_ProcPadAlign.m_AlignOverWidth		= GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_AlignWidthOverX, 4, InifilePath);
	G_ProcPadAlign.m_AlignOKValue			= GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_AlignOKValue, 80, InifilePath);

	strNewData.Format("%d", G_ProcPadAlign.m_AlignWidth	);
	GetDlgItem(IDC_ED_PADALIGN_DISWIDTH)->SetWindowText(strNewData);
	strNewData.Format("%d", G_ProcPadAlign.m_AlignOverWidth	);
	GetDlgItem(IDC_ED_PADALIGN_OVERWIDTH)->SetWindowText(strNewData);
	strNewData.Format("%d", G_ProcPadAlign.m_AlignOKValue	);
	GetDlgItem(IDC_ED_PADALIGN_PROCSKIPVALUE)->SetWindowText(strNewData);

	G_ProcPadAlign.m_AlignUseMaxHigh = GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_AlignUseMaxHigh, 0, InifilePath)==1?true:false;
	if(G_ProcPadAlign.m_AlignUseMaxHigh==true)
		CheckDlgButton(IDC_CHK_SAVE_PAD_USE_MAX_HIGH, BST_CHECKED );
	else
		CheckDlgButton(IDC_CHK_SAVE_PAD_USE_MAX_HIGH, BST_UNCHECKED);

	G_ProcPadAlign.m_AlignImgNotChange = GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_AlignImgNotChange, 0, InifilePath)==1?true:false;
	if(G_ProcPadAlign.m_AlignImgNotChange==true)
		CheckDlgButton(IDC_CHK_SAVE_PAD_MARK_NOT_CHANGE, BST_CHECKED );
	else
		CheckDlgButton(IDC_CHK_SAVE_PAD_MARK_NOT_CHANGE, BST_UNCHECKED);

	G_ProcPadAlign.m_AlignImgUseCanny = GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_AlignImgUseCanny, 0, InifilePath)==1?true:false;
	if(G_ProcPadAlign.m_AlignImgUseCanny==true)
		CheckDlgButton(IDC_CHK_SAVE_PAD_USE_CANNY, BST_CHECKED );
	else
		CheckDlgButton(IDC_CHK_SAVE_PAD_USE_CANNY, BST_UNCHECKED);

	G_ProcPadAlign.m_AlignImgUseCanny_threshold1	= GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_AlignImgUseCannyTh1, 0, InifilePath);
	G_ProcPadAlign.m_AlignImgUseCanny_threshold2	= GetPrivateProfileInt(Section_UpDn_PAD_AlignOptions, Key_AlignImgUseCannyTh2, 70, InifilePath);

	strNewData.Format("%d",G_ProcPadAlign.m_AlignImgUseCanny_threshold1);
	GetDlgItem(IDC_ED_PADALIGN_CANNY_THRESHOLD1)->SetWindowText(strNewData);
	strNewData.Format("%d", G_ProcPadAlign.m_AlignImgUseCanny_threshold2	);
	GetDlgItem(IDC_ED_PADALIGN_CANNY_THRESHOLD2)->SetWindowText(strNewData);
}
/*
*	Module Name		:	AOI_MessageCallback
*	Parameter		:	Window Message Call
*	Return			:	없음
*	Function		:	Sever로 부터 저달 되는 Command를 받는다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
LRESULT CVS11AOIIDlg::Sys_fnMessageCallback(WPARAM wParam, LPARAM lParam)
{
	int	nRet;
	CMDMSG* CmdMsg = (CMDMSG*)lParam;		//SmartPointer 버전으로 개조가 필요하다... Work Item..

	try
	{
		if(CmdMsg != NULL)
		{
			nRet = Sys_fnAnalyzeMsg(CmdMsg);

			if(OKAY != nRet)
			{
				//Client 메시지 처리에대한 에러처리...
			}

			m_pServerInterface->m_fnFreeMemory(CmdMsg->cMsgBuf);
			m_pServerInterface->m_fnFreeMemory(CmdMsg);
			return OKAY;
		}
		else
		{
			throw CVs64Exception(_T("CVS19MWDlg::m_fnVS64MsgProc, Message did not become well."));
		}

	}
	catch (CVs64Exception& e)
	{
		m_pServerInterface->m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS,  e.m_fnGetErrorMsg());

		m_pServerInterface->m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_pServerInterface->m_fnFreeMemory(CmdMsg);
	}
	catch (exception& e)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, exception -> %s"), e.what());
		m_pServerInterface->m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_pServerInterface->m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_pServerInterface->m_fnFreeMemory(CmdMsg);
	}	
	catch (CException& e)
	{
		wchar_t	wszErr[255];
		CString strErrorMsg;
		e.GetErrorMessage((LPSTR)wszErr, 255 );
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, CException -> %s"), wszErr);
		m_pServerInterface->m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_pServerInterface->m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_pServerInterface->m_fnFreeMemory(CmdMsg);
	}
	catch (...)
	{
		CString strErrorMsg;
		strErrorMsg.Format(_T("CVS19MWDlg::m_fnVS64MsgProc, 예기치 않은 Error 가 발생 했습니다.")); //예정치 않은 Error 가 발생되었습니다.
		m_pServerInterface->m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS, (const wchar_t*)strErrorMsg.GetBuffer(strErrorMsg.GetLength()));

		m_pServerInterface->m_fnFreeMemory(CmdMsg->cMsgBuf);
		m_pServerInterface->m_fnFreeMemory(CmdMsg);
	}
	return OKAY;
}

/*
*	Module Name		:	Sys_fnAnalyzeMsg
*	Parameter		:	Massage Parameter Pointer
*	Return			:	없음
*	Function		:	Sever로 부터 저달 되는 Command를 내부 Function과 Link 한다.(처리)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
bool Align_IMG_Direction = true;//true: ODD, false:Even
int CVS11AOIIDlg::Sys_fnAnalyzeMsg(CMDMSG* RcvCmdMsg)
{
	try
	{
		//AOI_AppendList("RcvComMsg:T(%d), F(%d), S(%d)",RcvCmdMsg->uTask_Dest, RcvCmdMsg->uFunID_Dest, RcvCmdMsg->uSeqID_Dest) ;
		if(RcvCmdMsg->uTask_Dest == m_ProcInitVal.m_nTaskNum) 
		{	
			switch(RcvCmdMsg->uFunID_Dest)
			{
			case nBiz_Func_System:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Seq_Alive_Question:
						{
							//TASK_STATE_NONE = 0,
							//TASK_STATE_INIT = 1,
							//TASK_STATE_IDLE,
							//TASK_STATE_RUN,
							//TASK_STATE_ERROR,
							//TASK_STATE_PM
							USHORT sendState = TASK_STATE_NONE;
							switch(G_ProcessState.m_UnitState)
							{
							case nBiz_AOI_STATE_ERROR:
								sendState = TASK_STATE_ERROR;
								break;
							case nBiz_AOI_STATE_IDLE:
								sendState = TASK_STATE_IDLE;
								break;
							case nBiz_AOI_STATE_READY:
								sendState = TASK_STATE_RUN;
								break;
							case nBiz_AOI_STATE_BUSY:
								sendState = TASK_STATE_RUN;
								break;
							case nBiz_AOI_STATE_ALARM:
								sendState = TASK_STATE_NONE;
								break;
							}
							m_pServerInterface->m_fnSendCommand_Nrs(RcvCmdMsg->uTask_Src, nBiz_Func_System, nBiz_Seq_Alive_Response, sendState);
						}break;
					case nBiz_Seq_Alive_Response:
						{
							switch (RcvCmdMsg->uTask_Src)
							{
							case TASK_10_Master:break;
							case TASK_11_Indexer:break;
							//case TASK_12_Indexer:break;
							case TASK_21_Inspector:break;
							case TASK_24_Motion:break;
							case TASK_31_Mesure3D:break;
							case TASK_32_Mesure2D:break;
							case TASK_33_SerialPort:break;
							default:					
								throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
								break;					
							}//End Switch	
							break;
						}
					default:
						throw CVs64Exception(_T("CVS11IndexerDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
						break;
					}//End Switch
				}break;
			case nBiz_AOI_System:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Send_UnitInitialize:
						{

							AOI_AppendList("▶▶▶▶ Unit Init Scan Stop");
							//각 Process에서 보고 있는 Scan Stop 명령을 Clear한다.
							G_ProcessState.m_InspectionStop = INSPECTION_STOP;
							//Grab 종료.
#ifdef TEST_INSPECTION_LOAD_FILE
							G_FileGrabObj.GrabEnd();
#else
							G_ImgGrabberCtrl.GrabEnd();
#endif


							G_ProcessState.m_bAlignScan = false;
							AOI_AppendList("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
							AOI_AppendList("nBiz AOI Unit Initialize");
							
							//각 Process에서 보고 있는 Scan Stop 명령을 Clear한다.
							G_ProcessState.m_InspectionStop = INSPECTION_IDLE;

							G_InspectDefectQ.QClean();
							AOI_AppendList("Defect Buffer Clean");
							G_InspectSaveImgQ.QClean();
							AOI_AppendList("Save Image Buffer Clean");

							m_listInspectParam.ResetContent();
							m_listHWParam.ResetContent();
							m_listGlassParam.ResetContent();
							//H/W Parameter를 Reload한다.
							if(G_AOI_fnLoadHWParam(&G_CoordProcObj) == false)
							{
								//Init Error ALARM 상태로 전환.
								AOI_StateChange(nBiz_AOI_STATE_ALARM, ALARM_CODE_HWParam);
							}
							else
							{
								//Unit Init를 지시한 Task를 Master로 인식한다.
								G_MasterTaskIndex = RcvCmdMsg->uTask_Src;
								CString strMastTask;
								strMastTask.Format("(%d)", G_MasterTaskIndex);
								m_stMasterTask.SetWindowText(strMastTask);
								AOI_StateChange(nBiz_AOI_STATE_IDLE);
							}

						}break;
					case nBiz_Send_AlarmReset:
						{
							AOI_AppendList("▶▶▶▶ Alarm Reset");
							AOI_StateChange(nBiz_AOI_STATE_IDLE);
							G_ProcessState.m_InspectionStop = INSPECTION_IDLE;
						}break;
					case nBiz_Send_SetSysTime:
						{
							if(RcvCmdMsg->uMsgSize == sizeof(SYSTEMTIME))
							{
								SYSTEMTIME sysTime;
								memcpy(&sysTime, RcvCmdMsg->cMsgBuf, sizeof(SYSTEMTIME));
								if(SetLocalTime(&sysTime) == TRUE)
									AOI_AppendList("Set New Time(%4d:%02d:%02d:%02d:%02d)", sysTime.wYear,sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute );
								else
									AOI_AppendList("Set New Time(Write Error)" );
							}
						}break;
					case nBiz_Send_CamCommand:
						{
							if(RcvCmdMsg->uMsgSize  >0 )
							{
								if(RcvCmdMsg->uMsgSize<300)
								{
									CString strSendMessage;
									
									RcvCmdMsg->cMsgBuf[RcvCmdMsg->uMsgSize] = 0;

									strSendMessage.Format("%s", RcvCmdMsg->cMsgBuf);
									strSendMessage.Remove('\r');
									strSendMessage.Remove('\n');
									strSendMessage.AppendFormat("\r");

									m_CamDicOgj.WriteComm((BYTE*)strSendMessage.GetBuffer(strSendMessage.GetLength()), (DWORD)strSendMessage.GetLength());

									if(m_ViewCamCMDList.GetCount()>LISTDATACOUNT)
										m_ViewCamCMDList.DeleteString(m_ViewCamCMDList.GetCount()-LISTDATACOUNT);

									m_ViewCamCMDList.AddString(strSendMessage);
									m_ViewCamCMDList.SetScrollPos(SB_VERT , m_ViewCamCMDList.GetCount(), TRUE);
									m_ViewCamCMDList.SetTopIndex(m_ViewCamCMDList.GetCount()-1);

									AOI_AppendList("Send Cam CMD(%dByte):%s", strSendMessage.GetLength(), strSendMessage);
								}
								else
								{
									AOI_AppendList("Send Cam CMD(%dByte)---Message Size Big Not Send", RcvCmdMsg->uMsgSize, RcvCmdMsg->cMsgBuf);
								}
							}
						}break;
						
					default:
						{
							throw CVs64Exception(_T("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
							break;
						}
					}
				}break;
			case nBiz_AOI_Parameter:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Send_ActiveAreaParam:
						{
							//Real 좌표가 수신되면 해당 Type으로 전환된다.(Reset의 의미로 사용- 없으면  무조건 Real Scan을 사용 하게 된다.)
							G_CoordProcObj.m_RealScanType = -1;

							if(G_ProcessState.m_UnitState != nBiz_AOI_STATE_IDLE)//이미동작중인 상태에서 또들어오다? Error 이다.
							{
								AOI_AppendList("▶▶▶▶ Only IDLE....Error Active Area Param Command.");
								break;
							}


							if(IsDlgButtonChecked(IDC_CHK_SAVE_HISTO_ORT_CUTPOS) == TRUE)
							{
								CheckDlgButton(IDC_CHK_SAVE_HISTO_ORT_CUTPOS, BST_UNCHECKED);
								G_ProcessState.bTEST_SAVE_HISTO_ORG_IMG= false;
							}

							if(IsDlgButtonChecked(IDC_CHK_SAVE_PAD_ALIGN_IMG) == TRUE)
							{
								CheckDlgButton(IDC_CHK_SAVE_PAD_ALIGN_IMG, BST_UNCHECKED);
								G_ProcPadAlign.m_AlignResultSave = false;
							}


							//if(IsDlgButtonChecked(IDC_CHK_UNUSE_PAD_ALIGN) == TRUE)
							//{
							//	CheckDlgButton(IDC_CHK_UNUSE_PAD_ALIGN, BST_UNCHECKED);
							//	G_ProcessState.bTEST_USE_PAD_ALIGN = true;
							//}

							if(G_ProcessState.bTEST_USE_PAD_ALIGN==true)
							{
								CheckDlgButton(IDC_CHK_UNUSE_PAD_ALIGN, BST_UNCHECKED);
							}
							else
							{
								CheckDlgButton(IDC_CHK_UNUSE_PAD_ALIGN, BST_CHECKED);
							}

							AOI_AppendList("▶▶▶▶ Active Area Param");
							if(RcvCmdMsg->uMsgSize == (sizeof(SAOI_IMAGE_INFO) + sizeof(SAOI_COMMON_PARA)))
							{
								memcpy(&m_stMaskImageInfo,	RcvCmdMsg->cMsgBuf, sizeof(SAOI_IMAGE_INFO));
								memcpy(&m_stCommonPara,		(RcvCmdMsg->cMsgBuf+sizeof(SAOI_IMAGE_INFO)), sizeof(SAOI_COMMON_PARA));
								
								//Image Size H/W Param에서 결정된다.
								m_stCommonPara.nImageSizeX =  G_CoordProcObj.m_HWParam.m_ProcImageW;
								m_stCommonPara.nImageSizeY =  G_CoordProcObj.m_HWParam.m_ProcImageH+(G_CoordProcObj.m_AXIS_Y_OVER_STEP*2);

								//m_stMaskImageInfo.nOutLineUp = G_CoordProcObj.m_AXIS_Y_OVER_STEP;
								//m_stMaskImageInfo.nOutLineDown = G_CoordProcObj.m_AXIS_Y_OVER_STEP;

								memcpy(&G_CoordProcObj.m_InspectActiveParam,	&m_stMaskImageInfo, sizeof(SAOI_IMAGE_INFO));

								//Lens Offset Value를 Read한다.
								G_AOI_fnLoadInspectParam(&G_CoordProcObj, m_ProcInitVal.m_nTaskNum);
								memcpy( &m_stCommonPara.nZoneStep[0],	&G_CoordProcObj.m_InspectParam.nZoneStep[0],	sizeof(int)*5);
								memcpy( &m_stCommonPara.nZoneOffset[0], &G_CoordProcObj.m_InspectParam.nZoneOffset[0],	sizeof(int)*5);

								//UpdateInspectParam(&m_stMaskImageInfo, &m_stCommonPara);
								//Loading Inspection Active Parameter 를 설정한다.
								//_AOI_Act_SearchParameterSet(m_stMaskImageInfo,  m_stCommonPara);

								AOI_AppendList("Active Param Recv OK");	
							}
							else
							{
								AOI_AppendList("Inspect Active Image Info Param Set Size Error");
								AOI_StateChange(nBiz_AOI_STATE_ALARM, ALARM_CODE_ActivParam);
							}

						}break;
					case nBiz_Send_PadAreaParam:
						{
							if(G_ProcessState.m_UnitState != nBiz_AOI_STATE_IDLE)//이미동작중인 상태에서 또들어오다? Error 이다.
							{
								AOI_AppendList("▶▶▶▶ Only IDLE....Error Pad Area Param Command.");
								break;
							}

							AOI_AppendList("▶▶▶▶ Pad Area Param");
							if(RcvCmdMsg->uMsgSize == ((sizeof(SAOI_IMAGE_INFO)*4) + (sizeof(SAOI_COMMON_PARA)*4)))
							{
								//상 하 좌 우 순서.
								int JumpSize =  sizeof(SAOI_IMAGE_INFO) + sizeof(SAOI_COMMON_PARA);
								for(int ReadStep= 0; ReadStep<4; ReadStep++)
								{
									memcpy(&m_stPADMaskImageInfo[ReadStep],	RcvCmdMsg->cMsgBuf+(JumpSize*ReadStep), sizeof(SAOI_IMAGE_INFO));
									memcpy(&m_stPADCommonPara[ReadStep],		(RcvCmdMsg->cMsgBuf+(JumpSize*ReadStep)+sizeof(SAOI_IMAGE_INFO)), sizeof(SAOI_COMMON_PARA));
								}
								//Image Size H/W Param에서 결정된다.
								//Pad부 Size는 다시 연산되지만 기본값은 H/W Param에서 가져와 설정 한다.
								for(int SizeStep = 0; SizeStep<4; SizeStep++)
								{
									m_stPADCommonPara[SizeStep].nImageSizeX =  G_CoordProcObj.m_HWParam.m_ProcImageW;
									m_stPADCommonPara[SizeStep].nImageSizeY =  G_CoordProcObj.m_HWParam.m_ProcImageH+(G_CoordProcObj.m_AXIS_Y_OVER_STEP*2);
								}
								AOI_AppendList("Inspect PAD Image Info Param Set OK");


								m_stMaskImageInfo.nScaleX2 = 0;
								m_stMaskImageInfo.nScaleY2 = 0;

								m_stMaskImageInfo.nScaleX3 = 0;
								m_stMaskImageInfo.nScaleY3 = 0;
								////0:상, 1:하, 2:좌, 3:우 (Pad부 이지만 Active에서 연산할 경우반복 비율을 입력 한다.)
								switch(G_CoordProcObj.m_HWParam.PadInspectAreaType)
								{
								case INSPACT_PAD_SKIP:
									{

									}break;
								case INSPACT_PAD_UPDOWN://Up/Down은 Pad연산 좌우는 Active연산.
									{
										//Active 연산 상의 Parameter 전달.
										m_stMaskImageInfo.nScaleX2 = 0;//m_stPADMaskImageInfo[2].nScaleX1;
										m_stMaskImageInfo.nScaleY2 = m_stPADMaskImageInfo[2].nScaleY1;

										m_stMaskImageInfo.nScaleX3 = 0;//m_stPADMaskImageInfo[3].nScaleX1;
										m_stMaskImageInfo.nScaleY3 = m_stPADMaskImageInfo[3].nScaleY1;


										m_stMaskImageInfo.nMinRangeLPad		=  m_stPADMaskImageInfo[2].nMinRangeMain;                             ///<검출 허용치 Default : 60(Left Pad)
										m_stMaskImageInfo.nMaxRangeLPad	=  m_stPADMaskImageInfo[2].nMaxRangeMain;                           ///<검출 허용치 Default : 60(Left Pad)
										m_stMaskImageInfo.nMinRangeRPad		=  m_stPADMaskImageInfo[3].nMinRangeMain;	                           ///<검출 허용치 Default : 60(Right Pad)
										m_stMaskImageInfo.nMaxRangeRPad	=  m_stPADMaskImageInfo[3].nMaxRangeMain;	                           ///<검출 허용치 Default : 60(Right Pad)

									}break;
								case INSPACT_PAD_LEFTRIGHT://좌우는 Pad연산 상하는 Active연산.
									{
										//Active 연산 상의 Parameter 전달.
										m_stMaskImageInfo.nScaleX2 = m_stPADMaskImageInfo[0].nScaleX1;
										m_stMaskImageInfo.nScaleY2 = m_stPADMaskImageInfo[0].nScaleY1;

										m_stMaskImageInfo.nScaleX3 = m_stPADMaskImageInfo[1].nScaleX1;
										m_stMaskImageInfo.nScaleY3 = m_stPADMaskImageInfo[1].nScaleY1;

										m_stMaskImageInfo.nMinRangeLPad		=  m_stPADMaskImageInfo[2].nMinRangeMain;                             ///<검출 허용치 Default : 60(Left Pad)
										m_stMaskImageInfo.nMaxRangeLPad	=  m_stPADMaskImageInfo[2].nMaxRangeMain;                           ///<검출 허용치 Default : 60(Left Pad)
										m_stMaskImageInfo.nMinRangeRPad		=  m_stPADMaskImageInfo[3].nMinRangeMain;	                           ///<검출 허용치 Default : 60(Right Pad)
										m_stMaskImageInfo.nMaxRangeRPad	=  m_stPADMaskImageInfo[3].nMaxRangeMain;	                           ///<검출 허용치 Default : 60(Right Pad)
									}break;
								case INSPACT_PAD_BOTH:
									{
									}break;
								}

								//Pad Param Coord에 전달.
								memcpy(&G_CoordProcObj.m_InspectPADMaskImageInfo[0],	&m_stPADMaskImageInfo[0], sizeof(SAOI_IMAGE_INFO)*4);
								memcpy(&G_CoordProcObj.m_InspectPADCommonPara[0],	&m_stPADCommonPara[0], sizeof(SAOI_COMMON_PARA)*4);


								m_stPADCommonPara[0].bImageLogUsed = 1000;
								m_stPADCommonPara[1].bImageLogUsed = 1000;
								m_stPADCommonPara[2].bImageLogUsed = 1000;
								m_stPADCommonPara[3].bImageLogUsed = 1000;

								m_stCommonPara.bImageLogUsed = 1000;

								UpdateInspectParam(&m_stMaskImageInfo, &m_stCommonPara);
								//Loading Inspection Active Parameter 를 설정한다.
								_AOI_Act_SearchParameterSet(m_stMaskImageInfo,  m_stCommonPara);

								AOI_AppendList("Inspect Active Image Info Param Set OK");	
							}
							else
							{
								AOI_AppendList("Inspect PAD Image Info Param Set Error");
								AOI_StateChange(nBiz_AOI_STATE_ALARM, ALARM_CODE_PadParam);
							}

						}break;
					case nBiz_Send_RealCellPos:
						{
							if(RcvCmdMsg->uMsgSize >8)
							{
								int *pDataBuf		= (int*)RcvCmdMsg->cMsgBuf;
								int RealCellType	= *pDataBuf;
								int RealCellCntX	= *(pDataBuf+1);
								int RealCellCntY	= *(pDataBuf+2);

				

								 //Real type, X List Cnt, Y List Cnt, (X, Y DataLists)
								  int DataTotalByte = 0;
								 if(RealCellType == 0)
									DataTotalByte = 4 + 4 + 4 + (RealCellCntX*2*sizeof(int) )+ (RealCellCntY*2*sizeof(int) );
								 else if(RealCellType == 1)
									DataTotalByte = 4 + 4 + 4 + (RealCellCntX*RealCellCntY*sizeof(int)*4);

								 if(RcvCmdMsg->uMsgSize  == DataTotalByte)
								 {
									 if(RealCellType == 0)//상단수평 우측수직에대한 성분
									 {
										 int *pDisListX		= (pDataBuf+3);
										 int *pDisListY		= (pDataBuf+3+(RealCellCntX*2));

										 if(G_CoordProcObj.m_fnSetRealCellPos( RealCellCntX, RealCellCntY, pDisListX, pDisListY ) == true)
											 AOI_AppendList("Represent Line Cell Real Pos Data Set OK");	
										 else
											 AOI_AppendList("Represent Line Cell Real Pos None");	
									 }
									 else if(RealCellType ==1)//전 Cell에대한 CvRect Type
									 {
										 CvRect *pCellRect = (CvRect*)(pDataBuf+3);
										 if(G_CoordProcObj.m_fnSetRealCellPos( RealCellCntX, RealCellCntY, pCellRect ) == true)
											 AOI_AppendList("Full Cell Real Pos Data Set OK");	
										 else
											 AOI_AppendList("Full Cell Real Pos None");	
									 }
									 else
										 AOI_AppendList("Cell Real Pos None");	
									 
								 }
								 else
								 {
									 AOI_AppendList("Cell Real Pos Data Size Error(%d != %d)", RcvCmdMsg->uMsgSize, DataTotalByte);
								 }
							}
							else
							{
								AOI_AppendList("Cell Real Pos Data Size (%d)", RcvCmdMsg->uMsgSize);
							}


						}break;
					case nBiz_Send_GlassParamSet:
						{
							if(G_ProcessState.m_UnitState != nBiz_AOI_STATE_IDLE)//이미동작중인 상태에서 또들어오다? Error 이다.
							{
								AOI_AppendList("▶▶▶▶ Only IDLE ....Error Glass Param Set Command.");
								break;
							}

							//cvDestroyWindow(PARAM_DRAW_WINDOW);
							//조도 Max값 저장 버퍼 클리어.
							G_ProcessState.m_HistScanMaxValue = 0;
							memset(G_ProcessState.m_Histogaram_Data, 0x00, sizeof(float)*256);

							if(G_ProcessState.bRealTimeLightValueSend == true)
							{
								AOI_AppendList("▶Run Light Value Real Time Off");
								G_ProcessState.bRealTimeLightValueSend = false;
								AOI_AppendList("▶Run Pixel Value Real Time Off");
								G_ProcessState.bRealTimePixelValueSend = false;
							}
							//AF 연산 수치를 클리어.
							G_ProcessState.m_ActiveAFValue = 0;
							
#ifdef TEST_SAVE_ORG_IMG
							G_fnCheckDirAndCreate(TEST_SAVE_ORG_PATH);
							G_ClearFolder(CString(TEST_SAVE_ORG_PATH));
#endif
							//if(G_ProcessState.bTEST_SAVE_CUT_AREA_DRAW == true)
							//{
							//	G_fnCheckDirAndCreate(TEST_SAVE_CUT_PATH);
							//	G_ClearFolder(CString(TEST_SAVE_CUT_PATH));
							//}
							
#ifdef TEST_SAVE_PAD_AREA_SAVE
							G_fnCheckDirAndCreate(PAD_ORG_IMG_SAVE_PATH);
							G_ClearFolder(CString(PAD_ORG_IMG_SAVE_PATH));
#endif
								
							//////////////////////////////////////////////////////////////////////////
							G_fnCheckDirAndCreate(LINE_SCAN_IMG_PATH);
							G_ClearFolder(LINE_SCAN_IMG_PATH);

							if(RcvCmdMsg->uMsgSize ==  sizeof(GLASS_PARAM)+1)// || RcvCmdMsg->uMsgSize ==  ((sizeof(GLASS_PARAM)+1) - 128))
							{
								int nMacroGrab = 0;
								AOI_AppendList("▶▶▶▶ Glass Param");
								memset(&m_GlassReicpeParam, 0x00, sizeof(GLASS_PARAM));
								if(RcvCmdMsg->uMsgSize ==  (sizeof(GLASS_PARAM)+1))
								{
									memcpy(&m_GlassReicpeParam,	RcvCmdMsg->cMsgBuf, sizeof(GLASS_PARAM));

									nMacroGrab=(int)RcvCmdMsg->cMsgBuf[sizeof(GLASS_PARAM)];
								}
								//else 
								//{
								//	memcpy(&m_GlassReicpeParam,	RcvCmdMsg->cMsgBuf, ((sizeof(GLASS_PARAM)+1)-128));
								//	nMacroGrab=(int)RcvCmdMsg->cMsgBuf[sizeof(GLASS_PARAM)];
								//}

								//if(m_GlassReicpeParam.m_ScanImgResolution_H <3.0)
									G_CoordProcObj.m_ResolutionType = RESOLUTION_TYPE_1;
								//else
								//	G_CoordProcObj.m_ResolutionType = RESOLUTION_TYPE_2;

								//Lens Type 추가.(20120726) Start
								int LensType = 1;//(m_GlassReicpeParam.m_ScanImgResolution_W<3.0f)?1:2;
								G_AOI_fnReloadHWParamByLens(m_ProcInitVal.m_nTaskNum, &G_CoordProcObj,  LensType);
								//G_AOI_fnLoadLightingHist(m_ProcInitVal.m_nTaskNum, &G_ProcessState.dbLightingHistMaxPresent);
								//Lens Type 추가.(20120726) End

								if(strlen(m_GlassReicpeParam.m_GlassName)<=0)
								{
									SYSTEMTIME NowTime;
									GetLocalTime(&NowTime);
									sprintf(m_GlassReicpeParam.m_GlassName,"%04d%02d%02d%02d%02d%02d",NowTime.wYear, NowTime.wMonth, NowTime.wDay, NowTime.wHour, NowTime.wMinute, NowTime.wSecond);
								}

								UpdateGlassParam(&m_GlassReicpeParam);
								UpdateHWParam(&G_CoordProcObj.m_HWParam);

								AOI_AppendList("Glass Param Set OK");
								if(G_CoordProcObj.m_fnSetRecipeData( m_ProcInitVal.m_nTaskNum, &m_GlassReicpeParam) == false)
								{
									AOI_AppendList("Glass Param Coordinate Error");
									AOI_StateChange(nBiz_AOI_STATE_ALARM, ALARM_CODE_GLassParamCal);
								}
								else
								{
									//PadArea Memory Create
									G_ProcessState.m_PADInspectTotalCnt  = 0;
									switch(G_CoordProcObj.m_HWParam.PadInspectAreaType)
									{
									case INSPACT_PAD_SKIP:
										{
											for(int PadCrStep = 0; PadCrStep<4; PadCrStep++)
											{
												G_PadInspectDataQ[PadCrStep].QDeleteAll();
											}

										}break;
									case INSPACT_PAD_UPDOWN:
										{
											G_PadInspectDataQ[0].m_fnCreatePadAreaMem(0, &G_CoordProcObj, &m_stPADMaskImageInfo[0], 	&m_stPADCommonPara[0]);
											G_PadInspectDataQ[1].m_fnCreatePadAreaMem(1, &G_CoordProcObj, &m_stPADMaskImageInfo[1], 	&m_stPADCommonPara[1]);
											
											
											G_PadInspectDataQ[2].QDeleteAll();
											G_PadInspectDataQ[2].m_PadDeadZone		= G_CoordProcObj.m_GlassParam.PadStartDeadZoneDisX;
											G_PadInspectDataQ[2].m_PadDNDeadZoneW	= 0;
											G_PadInspectDataQ[2].m_PadDNDeadZoneH	= 0;
											
											G_PadInspectDataQ[3].QDeleteAll();
											G_PadInspectDataQ[3].m_PadDeadZone		= G_CoordProcObj.m_GlassParam.PadEndDeadZoneDisX;
											G_PadInspectDataQ[3].m_PadDNDeadZoneW	= 0;
											G_PadInspectDataQ[3].m_PadDNDeadZoneH	= 0;

											//////////////////////////////////////////////////////////////////////////
											//UI Update
											G_ProcessState.m_PADInspectTotalCnt += (G_PadInspectDataQ[0].QGetNodeCnt()*G_PadInspectDataQ[0].m_ImgFragmentatCnt);
											G_ProcessState.m_PADInspectTotalCnt += (G_PadInspectDataQ[1].QGetNodeCnt()*G_PadInspectDataQ[1].m_ImgFragmentatCnt);

										}break;
									case INSPACT_PAD_LEFTRIGHT:
										{
											G_PadInspectDataQ[0].QDeleteAll();
											G_PadInspectDataQ[0].m_PadDeadZone		= G_CoordProcObj.m_GlassParam.PadStartDeadZoneDisY;
											G_PadInspectDataQ[0].m_PadDNDeadZoneW	= 0;
											G_PadInspectDataQ[0].m_PadDNDeadZoneH	= 0;

											G_PadInspectDataQ[1].QDeleteAll();
											G_PadInspectDataQ[1].m_PadDeadZone		= G_CoordProcObj.m_GlassParam.PadEndDeadZoneDisY;
											G_PadInspectDataQ[1].m_PadDNDeadZoneW	= G_CoordProcObj.m_GlassParam.PadDnDeadZoneWidth;
											G_PadInspectDataQ[1].m_PadDNDeadZoneH	= G_CoordProcObj.m_GlassParam.PadDnDeadZoneHeight;


											G_PadInspectDataQ[2].m_fnCreatePadAreaMem(2, &G_CoordProcObj, &m_stPADMaskImageInfo[2], 	&m_stPADCommonPara[2]);
											G_PadInspectDataQ[3].m_fnCreatePadAreaMem(3, &G_CoordProcObj, &m_stPADMaskImageInfo[3], 	&m_stPADCommonPara[3]);
											//////////////////////////////////////////////////////////////////////////
											//UI Update
											G_ProcessState.m_PADInspectTotalCnt += (G_PadInspectDataQ[2].QGetNodeCnt()*G_PadInspectDataQ[2].m_ImgFragmentatCnt);
											G_ProcessState.m_PADInspectTotalCnt += (G_PadInspectDataQ[3].QGetNodeCnt()*G_PadInspectDataQ[3].m_ImgFragmentatCnt);
										}break;
									case INSPACT_PAD_BOTH:
										{
											for(int PadCrStep = 0; PadCrStep<4; PadCrStep++)
											{
												G_PadInspectDataQ[PadCrStep].m_fnCreatePadAreaMem(PadCrStep, &G_CoordProcObj,
													&m_stPADMaskImageInfo[PadCrStep],
													&m_stPADCommonPara[PadCrStep]);

												G_ProcessState.m_PADInspectTotalCnt += (G_PadInspectDataQ[PadCrStep].QGetNodeCnt()*G_PadInspectDataQ[PadCrStep].m_ImgFragmentatCnt);
											}
										}break;
									}
									int SendData[4];
									//계산된 Scan Line Count, One Line Image Count를 반환 한다.
									SendData[0] = G_CoordProcObj.m_AOIOneCam_ScanLineCnt;
									SendData[1] = G_CoordProcObj.m_OneLineScnaImgCnt;
									m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 
																			nBiz_AOI_Parameter, nBiz_Recv_CalculateInfo, 0, (USHORT)sizeof(int)*2, (UCHAR*)&SendData[0]);
									m_pServerInterface->m_fnPrintLog(m_pServerInterface->m_uTaskNo, 4,"1) Send CalculateInfo End");

									extern AOIBigImg G_GrabBigger;
									if(nMacroGrab>0)
										G_GrabBigger.m_bBigDataGrab = true;
									else
										G_GrabBigger.m_bBigDataGrab = false;

									int OverLapSize		=(int) ((G_CoordProcObj.m_GlassParam.m_ScanImgResolution_W * (float)G_CoordProcObj.m_HWParam.m_ProcImageW) - (float)G_CoordProcObj.m_GlassParam.m_ScanLineShiftDistance);
									G_GrabBigger.SetOffset(OverLapSize, 
																     G_CoordProcObj.m_HWParam.m_AOI_SCAN_START_OFFSET_ODD,
																	 G_CoordProcObj.m_HWParam.m_AOI_SCAN_START_OFFSET_EVEN,
																	 G_CoordProcObj.m_GlassParam.m_ScanImgResolution_W);
									G_GrabBigger.SetCoordPos(	G_CoordProcObj.m_HWParam.m_AOI_BLOCK_SCAN_START_X, 
																			G_CoordProcObj.m_HWParam.m_AOI_BLOCK_SCAN_START_Y, 
																			G_CoordProcObj.m_GlassParam.CenterPointX, 
																			G_CoordProcObj.m_GlassParam.CenterPointY, 
																			G_CoordProcObj.m_GlassParam.m_ScanImgResolution_W);
									G_GrabBigger.SetImageSize(G_CoordProcObj.m_HWParam.m_ProcImageW,G_CoordProcObj.m_HWParam.m_ProcImageH,G_CoordProcObj.m_AOIOneCam_ScanLineCnt, G_CoordProcObj.m_OneLineScnaImgCnt);

									m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 
																			nBiz_AOI_Parameter, nBiz_Recv_BlockDrawInfo, G_CoordProcObj.m_nBlockCnt, 
																			(USHORT)sizeof(BLOCKINFO)*G_CoordProcObj.m_nBlockCnt, 
																			(UCHAR*)G_CoordProcObj.m_pBlockList);
									m_pServerInterface->m_fnPrintLog(m_pServerInterface->m_uTaskNo, 4,"2) Send BlockDrawInfo End");

									m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 
																			nBiz_AOI_Parameter, nBiz_Recv_CellDrawInfo, G_CoordProcObj.m_nCellCnt, 
																			(USHORT)sizeof(CELLINFO)*G_CoordProcObj.m_nCellCnt, 
																			(UCHAR*)G_CoordProcObj.m_pCellList);
									m_pServerInterface->m_fnPrintLog(m_pServerInterface->m_uTaskNo, 4,"3) Send CellDrawInfo End");

									m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 
																			nBiz_AOI_Parameter, nBiz_Recv_PADDrawInfo, G_CoordProcObj.m_nPadCellCnt, 
																			(USHORT)sizeof(PADINFO)*G_CoordProcObj.m_nPadCellCnt, 
																			(UCHAR*)G_CoordProcObj.m_pPadCellList);
									m_pServerInterface->m_fnPrintLog(m_pServerInterface->m_uTaskNo, 4,"4) Send PADDrawInfo End");

									m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 
																			nBiz_AOI_Parameter, nBiz_Recv_HWParam, 0, 
																			(USHORT)sizeof(HW_PARAM), 
																			(UCHAR*)&G_CoordProcObj.m_HWParam);
									m_pServerInterface->m_fnPrintLog(m_pServerInterface->m_uTaskNo, 4,"5) Send HWParam End");
									
									m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 
																			nBiz_AOI_Parameter, nBiz_Recv_ScanImgDrawInfo, G_CoordProcObj.m_nScanImgCnt);

									m_pServerInterface->m_fnPrintLog(m_pServerInterface->m_uTaskNo, 4,"6) Send ScanImgDrawInfo End");

									AOI_AppendList("Send Calculation Data OK(Set Sever Task:%d)", G_MasterTaskIndex);
									//////////////////////////////////////////////////////////////////////////
									//75Test
									//if(G_ProcessState.bTEST_VS75_REVIEW_SEND == true)
									//{
									//	m_pServerInterface->m_fnSendCommand_Nrs(	REVIEW_TASK_NUM, 
									//		nBiz_AOI_Parameter, nBiz_Recv_CalculateInfo, 0, (USHORT)sizeof(int)*2, (UCHAR*)&SendData[0]);

									//	m_pServerInterface->m_fnSendCommand_Nrs(	REVIEW_TASK_NUM, 
									//		nBiz_AOI_Parameter, nBiz_Recv_BlockDrawInfo, G_CoordProcObj.m_nBlockCnt, 
									//		(USHORT)sizeof(BLOCKINFO)*G_CoordProcObj.m_nBlockCnt, 
									//		(UCHAR*)G_CoordProcObj.m_pBlockList);

									//	m_pServerInterface->m_fnSendCommand_Nrs(	REVIEW_TASK_NUM, 
									//		nBiz_AOI_Parameter, nBiz_Recv_CellDrawInfo, G_CoordProcObj.m_nCellCnt, 
									//		(USHORT)sizeof(CELLINFO)*G_CoordProcObj.m_nCellCnt, 
									//		(UCHAR*)G_CoordProcObj.m_pCellList);

									//	m_pServerInterface->m_fnSendCommand_Nrs(	REVIEW_TASK_NUM, 
									//		nBiz_AOI_Parameter, nBiz_Recv_PADDrawInfo, G_CoordProcObj.m_nPadCellCnt, 
									//		(USHORT)sizeof(PADINFO)*G_CoordProcObj.m_nPadCellCnt, 
									//		(UCHAR*)G_CoordProcObj.m_pPadCellList);

									//	m_pServerInterface->m_fnSendCommand_Nrs(	REVIEW_TASK_NUM, 
									//		nBiz_AOI_Parameter, nBiz_Recv_HWParam, 0, 
									//		(USHORT)sizeof(HW_PARAM), 
									//		(UCHAR*)&G_CoordProcObj.m_HWParam);

									//	m_pServerInterface->m_fnSendCommand_Nrs(	REVIEW_TASK_NUM, 
									//		nBiz_AOI_Parameter, nBiz_Recv_ScanImgDrawInfo, G_CoordProcObj.m_nScanImgCnt);
									//}
									//////////////////////////////////////////////////////////////////////////
									//UI Update
									m_proTotalStep.SetRange32(0, (G_CoordProcObj.m_AOIOneCam_ScanLineCnt * G_CoordProcObj.m_OneLineScnaImgCnt));
									m_proLineStep.SetRange32(0, G_CoordProcObj.m_OneLineScnaImgCnt);
									m_proPADProcStep.SetRange32(0, G_ProcessState.m_PADInspectTotalCnt );

								}
								WriteLogInspectionParam();
								//Result Dir Check And Create.
								AOI_AppendList("Result Dir Check And Create");

#ifdef TEST_LOCAL_FILE
								if(m_szTestFileName[0] != 0x00) {
									strcat(G_CoordProcObj.m_HWParam.strOrgImgPath, m_szTestFileName);
									strcat(G_CoordProcObj.m_HWParam.strOrgImgPath, "\\");
								}
#endif

								//Result/Org Image Save Path Check.
								if(G_fnCheckDirAndCreate(G_CoordProcObj.m_HWParam.strResultPath) == true && 
									G_fnCheckDirAndCreate(G_CoordProcObj.m_HWParam.strOrgImgPath) == true)
								{

									G_fnCheckDirAndCreate(G_CoordProcObj.m_ResultUpdatePath.GetBuffer(G_CoordProcObj.m_ResultUpdatePath.GetLength()));
									
									//Last Image Save임으로 Folder내용을 지운다.
									//G_fnCheckDirAndCreate(G_CoordProcObj.m_ImageUpdatePath.GetBuffer(G_CoordProcObj.m_ImageUpdatePath.GetLength()));
									
									G_CoordProcObj.m_ImageUpdatePath.AppendFormat("%s\\", G_CoordProcObj.m_GlassParam.m_GlassName);
									G_fnCheckDirAndCreate(G_CoordProcObj.m_ImageUpdatePath.GetBuffer(G_CoordProcObj.m_ImageUpdatePath.GetLength()));
									
									//Folder Copy시에 수행함으로 제거.
									G_ClearFolder(G_CoordProcObj.m_ImageUpdatePath);

									//CString strTextFilePath;
									CString strImageFilePath;
									CString strImageUpdatePath;
									//strTextFilePath.Format("%sD_Insp_T%2d.txt", G_CoordProcObj.m_HWParam.strResultPath, m_ProcInitVal.m_nTaskNum);
									strImageFilePath.Format("%sD_Insp_T%02d.img", G_CoordProcObj.m_HWParam.strResultPath, m_ProcInitVal.m_nTaskNum);
									strImageUpdatePath.Format("%sD_Insp_T%02d.img", G_CoordProcObj.m_ResultUpdatePath, m_ProcInitVal.m_nTaskNum);

									G_WriteRetObj.m_fnCloseFileObj();
									
									G_ClearFolder(CString(G_CoordProcObj.m_HWParam.strResultPath));
									G_ClearFolder(CString(G_CoordProcObj.m_HWParam.strOrgImgPath));


									//////////////////////////////////////////////////////////////////////////
									CString strCreateFolderORG;
									strCreateFolderORG.Format("%s%s\\", G_CoordProcObj.m_HWParam.strOrgImgPath,   SAVE_ORG_IMG_FOLDER );
									G_fnCheckDirAndCreate(strCreateFolderORG.GetBuffer(strCreateFolderORG.GetLength()));
									G_ClearFolder(strCreateFolderORG);

									//if(G_ProcessState.bTEST_DEFECT_IMG_SAVE_JPG == true) 
									{
										CString strCreateFolderSMALL;
										strCreateFolderSMALL.Format("%s%s\\", G_CoordProcObj.m_HWParam.strOrgImgPath, SAVE_SMALL_IMG_FOLDER);
										G_fnCheckDirAndCreate(strCreateFolderSMALL.GetBuffer(strCreateFolderSMALL.GetLength()));
										G_ClearFolder(strCreateFolderSMALL);
									}

									//if(G_ProcessState.bTEST_SAVE_PAD_AREA == true)
									{
										CString strCreateFoldePADOrg;
										strCreateFoldePADOrg.Format("%s%s\\", G_CoordProcObj.m_HWParam.strOrgImgPath, SAVE_PADORG_IMG_FOLDER);
										G_fnCheckDirAndCreate(strCreateFoldePADOrg.GetBuffer(strCreateFoldePADOrg.GetLength()));
										G_ClearFolder(strCreateFoldePADOrg);
									}
									
									{
										CString strCreateFoldeHistoOrg;
										strCreateFoldeHistoOrg.Format("%s%s\\", G_CoordProcObj.m_HWParam.strOrgImgPath, SAVE_HISTO_IMG_FOLDER);
										G_fnCheckDirAndCreate(strCreateFoldeHistoOrg.GetBuffer(strCreateFoldeHistoOrg.GetLength()));
										G_ClearFolder(strCreateFoldeHistoOrg);
									}
									
									//////////////////////////////////////////////////////////////////////////


									//G_WriteRetObj.m_fnSetWriteDefectTextObj(strTextFilePath);
									G_WriteRetObj.m_fnSetWriteDefectImageObj(strImageFilePath,strImageUpdatePath,
																								G_CoordProcObj.m_HWParam.m_DefectReviewSizeW, 
																								G_CoordProcObj.m_HWParam.m_DefectReviewSizeH);

									//AOI_AppendList("Text File Path Set :%s", strTextFilePath);
									AOI_AppendList("Image File Path Set :%s", strImageFilePath);

									G_ProcessState.nActiveHistCalcSkipIndex = 3;
									AOI_AppendList("Active Hist Calc Index Reset(%d)", G_ProcessState.nActiveHistCalcSkipIndex);
									//Scna 결과 적제를 위해 Clear를 한다.
									G_InspectDefectQ.QClean();
									
									
									AOI_StateChange(nBiz_AOI_STATE_READY);
								}
								else
								{
									AOI_AppendList("H/W Param Result / Org Image Save Path Create Error");
									AOI_StateChange(nBiz_AOI_STATE_ALARM, ALARM_CODE_HWParamPath);
								}
							}
							else
							{
								AOI_AppendList("Glass Param Set Error(Param Size:%d==>%d)", sizeof(GLASS_PARAM), RcvCmdMsg->uMsgSize);
								AOI_StateChange(nBiz_AOI_STATE_ALARM, ALARM_CODE_GLassParamSize);
							}
						}break;
					default:
						{
							throw CVs64Exception(_T("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
							break;
						}
					}
				}break;
			case nBiz_AOI_Inspection:
				{
					switch(RcvCmdMsg->uSeqID_Dest)
					{
					case nBiz_Send_ScanStart:
						{			
							if(G_ProcessState.m_UnitState != nBiz_AOI_STATE_READY)//이미동작중인 상태에서 또들어오다? Error 이다.
							{
								AOI_AppendList("▶▶▶▶ Only Ready....Error Start Command.");
								break;
							}
							G_ProcessState.m_bAlignScan = false;
							//////////////////////////////////////////////////////////////////////////
							//Process등 각위치별 Data View를 위해 초기화
							for(int CutStpe=0; CutStpe<PADCUT_PROC_CNT; CutStpe++)
								G_ProcessState.m_CutScanLineIndex[CutStpe] = 0;
							for(int RetStep=0; RetStep<RET_PROC_CNT; RetStep++)
								G_ProcessState.m_RetScanLineIndex[RetStep]= 0;

							////////////////////////////////////////////////////////////////////////////
							int NewScanLineNum = (int)RcvCmdMsg->uUnitID_Dest;
							AOI_AppendList("▶▶▶▶ Scan Start(Scan Number : %2d)", NewScanLineNum);
							
							G_CoordProcObj.m_stPadArea.stLeft.nStartY	= 0; 
							G_CoordProcObj.m_stPadArea.stLeft.nEndY		= 0;
							G_CoordProcObj.m_stPadArea.stRight.nStartY	= 0;
							G_CoordProcObj.m_stPadArea.stRight.nEndY	= 0;

							switch(G_CoordProcObj.m_HWParam.PadInspectAreaType)
							{
							case INSPACT_PAD_SKIP:
								{
									G_CoordProcObj.m_stPadArea.stLeft.nStartY	= 0; 
									G_CoordProcObj.m_stPadArea.stLeft.nEndY		= 0;
									G_CoordProcObj.m_stPadArea.stRight.nStartY	= 0;
									G_CoordProcObj.m_stPadArea.stRight.nEndY	= 0;

									G_CoordProcObj.m_fnActivePadAreaLRFind(	NewScanLineNum, 
										&G_CoordProcObj.m_stPadArea.stLeft.nStartX, 
										&G_CoordProcObj.m_stPadArea.stLeft.nEndX, 
										&G_CoordProcObj.m_stPadArea.stRight.nStartX, 
										&G_CoordProcObj.m_stPadArea.stRight.nEndX);
									AOI_AppendList("Pad Inspect Area Type 현재 정의 되지않은 Type입니다.");
								}break;
							case INSPACT_PAD_UPDOWN:
								{
									G_CoordProcObj.m_stPadArea.stLeft.nStartY	= 0; 
									G_CoordProcObj.m_stPadArea.stLeft.nEndY		= G_CoordProcObj.m_HWParam.m_ProcImageH;
									G_CoordProcObj.m_stPadArea.stRight.nStartY	= 0;
									G_CoordProcObj.m_stPadArea.stRight.nEndY	= G_CoordProcObj.m_HWParam.m_ProcImageH;

									G_CoordProcObj.m_fnActivePadAreaLRFind(	NewScanLineNum, 
										&G_CoordProcObj.m_stPadArea.stLeft.nStartX, 
										&G_CoordProcObj.m_stPadArea.stLeft.nEndX, 
										&G_CoordProcObj.m_stPadArea.stRight.nStartX, 
										&G_CoordProcObj.m_stPadArea.stRight.nEndX);

									G_CoordProcObj.m_fnUpDnPadAlignCoordCalc(NewScanLineNum, 1, &G_PadInspectDataQ[0].m_PadAlignCoordData);
									AOI_AppendList("▶▶ Up PA Area:B(S:%d, E:%d)",//, S(S:%d, E:%d)", 
															G_PadInspectDataQ[0].m_PadAlignCoordData.m_BigAreaStartX, 
															G_PadInspectDataQ[0].m_PadAlignCoordData.m_BigAreaEndX);
															//G_PadInspectDataQ[0].m_PadAlignCoordData.m_SmallAreaStartX, 
															//G_PadInspectDataQ[0].m_PadAlignCoordData.m_SmallAreaEndX);

									G_CoordProcObj.m_fnUpDnPadAlignCoordCalc(NewScanLineNum, 2, &G_PadInspectDataQ[1].m_PadAlignCoordData);
									AOI_AppendList("▶▶ Dn PA Area:B(S:%d, E:%d)",//, S(S:%d, E:%d)", 
										G_PadInspectDataQ[1].m_PadAlignCoordData.m_BigAreaStartX, 
										G_PadInspectDataQ[1].m_PadAlignCoordData.m_BigAreaEndX);
										//G_PadInspectDataQ[1].m_PadAlignCoordData.m_SmallAreaStartX, 
										//G_PadInspectDataQ[1].m_PadAlignCoordData.m_SmallAreaEndX);
								}break;
							case INSPACT_PAD_LEFTRIGHT:
								{
									//G_CoordProcObj.m_stPadArea.stLeft.nStartY	= 0; 
									//G_CoordProcObj.m_stPadArea.stLeft.nEndY		= G_CoordProcObj.m_HWParam.m_ProcImageH;
									//G_CoordProcObj.m_stPadArea.stRight.nStartY	= 0;
									//G_CoordProcObj.m_stPadArea.stRight.nEndY	= G_CoordProcObj.m_HWParam.m_ProcImageH;

									//G_CoordProcObj.m_fnActivePadAreaLRFind(	NewScanLineNum, 
									//	&G_CoordProcObj.m_stPadArea.stLeft.nStartX, 
									//	&G_CoordProcObj.m_stPadArea.stLeft.nEndX, 
									//	&G_CoordProcObj.m_stPadArea.stRight.nStartX, 
									//	&G_CoordProcObj.m_stPadArea.stRight.nEndX);
									AOI_AppendList("Pad Inspect Area Type 현재 정의 되지않은 Type입니다.");
								}break;
							case INSPACT_PAD_BOTH:
								{
									//G_CoordProcObj.m_stPadArea.stLeft.nStartY	= 0; 
									//G_CoordProcObj.m_stPadArea.stLeft.nEndY		= G_CoordProcObj.m_HWParam.m_ProcImageH;
									//G_CoordProcObj.m_stPadArea.stRight.nStartY	= 0;
									//G_CoordProcObj.m_stPadArea.stRight.nEndY	= G_CoordProcObj.m_HWParam.m_ProcImageH;

									//G_CoordProcObj.m_fnActivePadAreaLRFind(	NewScanLineNum, 
									//	&G_CoordProcObj.m_stPadArea.stLeft.nStartX, 
									//	&G_CoordProcObj.m_stPadArea.stLeft.nEndX, 
									//	&G_CoordProcObj.m_stPadArea.stRight.nStartX, 
									//	&G_CoordProcObj.m_stPadArea.stRight.nEndX);
									AOI_AppendList("Pad Inspect Area Type 현재 정의 되지않은 Type입니다.");
								}break;
								
							}
							AOI_AppendList("▶▶ AIPArea:Left X(S:%d, E:%d), Right X(S:%d, E:%d)", 
								G_CoordProcObj.m_stPadArea.stLeft.nStartX, G_CoordProcObj.m_stPadArea.stLeft.nEndX, 
								G_CoordProcObj.m_stPadArea.stRight.nStartX, G_CoordProcObj.m_stPadArea.stRight.nEndX);

							if(G_CoordProcObj.m_stPadArea.stLeft.nEndX>=G_CoordProcObj.m_HWParam.m_ProcImageW)
								G_CoordProcObj.m_stPadArea.stLeft.nEndX = G_CoordProcObj.m_HWParam.m_ProcImageW-1;

							if(G_CoordProcObj.m_stPadArea.stRight.nEndX>=G_CoordProcObj.m_HWParam.m_ProcImageW)
								G_CoordProcObj.m_stPadArea.stRight.nEndX = G_CoordProcObj.m_HWParam.m_ProcImageW-1;

							if(_AOI_Act_SearchScaleSet( G_CoordProcObj.m_stPadArea ) != 1)
								AOI_AppendList("AIPArea Size Error"); 
							//if(AOI_StateChangeCheck(nBiz_AOI_STATE_BUSY) == TRUE)
							if(G_ProcessState.m_UnitState == nBiz_AOI_STATE_READY)
							{
								//AOI_AppendList("▶▶▶▶ Scan Start(Scan Number : %2d)", NewScanLineNum);
								//if(NewScanLineNum % 2)//1, 3, 5, 7,.....
								//{
								//	//처음 Scan 방향은 SCD 1 0 or 1
								//	m_CamDicOgj.m_fnSendCameraDirection( TRUE );//다음 스켄임으로 반대로.
								//	AOI_AppendList("Scan Direction SCD 1");
								//}
								//else //0, 2, 4, 6,.....
								//{
								//	//처음 Scan 방향은 SCD 1 0 or 1
								//	m_CamDicOgj.m_fnSendCameraDirection( FALSE );//다음 스켄임으로 반대로.
								//	AOI_AppendList("Scan Direction SCD 0");
								//}

								//Image Save용인지를 전달한다.
								G_InspectionDataQ.m_OnlyGrabImageSave = G_CoordProcObj.m_HWParam.m_OnlyGrabImageSave>0?true:false;
								G_InspectionDataQ.m_OrgImgSavePath.Format("%s",G_CoordProcObj.m_HWParam.strOrgImgPath);

								//Grabber Start전에 Pad부 Memory Clear및 위치 좌표 설정을 해야 한다.
								switch(G_CoordProcObj.m_HWParam.PadInspectAreaType)
								{
								case INSPACT_PAD_SKIP:
									{
										for(int PadCrStep = 0; PadCrStep<4; PadCrStep++)
										{
											G_PadInspectDataQ[PadCrStep].m_ScanLineNum = NewScanLineNum;
											G_PadInspectDataQ[PadCrStep].QDeleteAll();
										}
										AOI_AppendList("Pad Mem Delete All");
									}break;
								case INSPACT_PAD_UPDOWN:
									{
										G_PadInspectDataQ[0].m_ScanLineNum = NewScanLineNum;
										G_PadInspectDataQ[0].m_fnSetPadMPosition((int)RcvCmdMsg->uUnitID_Dest, &G_CoordProcObj);
										G_PadInspectDataQ[1].m_ScanLineNum = NewScanLineNum;
										G_PadInspectDataQ[1].m_fnSetPadMPosition((int)RcvCmdMsg->uUnitID_Dest, &G_CoordProcObj);
										AOI_AppendList("Pad UP DOWN Reset");
									}break;
								case INSPACT_PAD_LEFTRIGHT:
									{
										G_PadInspectDataQ[2].m_ScanLineNum = NewScanLineNum;
										G_PadInspectDataQ[2].m_fnSetPadMPosition((int)RcvCmdMsg->uUnitID_Dest, &G_CoordProcObj);
										G_PadInspectDataQ[3].m_ScanLineNum = NewScanLineNum;
										G_PadInspectDataQ[3].m_fnSetPadMPosition((int)RcvCmdMsg->uUnitID_Dest, &G_CoordProcObj);
										AOI_AppendList("Pad LEFT RIGHT Reset");
									}break;
								case INSPACT_PAD_BOTH:
									{
										for(int PadCrStep = 0; PadCrStep<4; PadCrStep++)
										{
											G_PadInspectDataQ[PadCrStep].m_ScanLineNum = NewScanLineNum;
											G_PadInspectDataQ[PadCrStep].m_fnSetPadMPosition((int)RcvCmdMsg->uUnitID_Dest, &G_CoordProcObj);
										}
										AOI_AppendList("Pad Mem Reset All");
									}break;
								}
								//Sleep(100);
								if(AOI_StateChange(nBiz_AOI_STATE_BUSY) == nBiz_ST_CH_OK)
								{	
#ifdef TEST_INSPECTION_LOAD_FILE
									if(G_FileGrabObj.GrabStart(NewScanLineNum, m_LineScanWidth, G_CoordProcObj.m_OneLineScnaImgCnt, G_CoordProcObj.m_HWParam.m_ProcImageW) == false)
#else
									if(G_ImgGrabberCtrl.GrabStart(NewScanLineNum, m_LineScanWidth) == false)
#endif
									{
										AOI_StateChange(nBiz_AOI_STATE_ALARM, ALARM_CODE_GrabStart);
									}
									else
									{
										//AOI_StateChange(nBiz_AOI_STATE_BUSY);
										AOI_AppendList("▶▶▶▶Grabber Scan Ready(Scan Line Num: %d)", NewScanLineNum);
										//HW가 무빙해도 된다.
										m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 	nBiz_AOI_Inspection, nBiz_Recv_ScanReady, 0);
									}
								}
							}
							else
							{
								AOI_AppendList("Now Inspection Running");
							}
						}break;
					case nBiz_Send_ScanStop:
						{
							//if(G_ProcessState.m_UnitState != nBiz_AOI_STATE_BUSY)//이미동작중인 상태에서 또들어오다? Error 이다.
							//{
							//	AOI_AppendList("▶▶▶▶ Only Busy....Error Stop Command.");
							//	break;
							//}

							AOI_AppendList("▶▶▶▶ Scan Stop");
							//각 Process에서 보고 있는 Scan Stop 명령을 Clear한다.
							G_ProcessState.m_InspectionStop = INSPECTION_STOP;
							
							
							//Grab 종료.
#ifdef TEST_INSPECTION_LOAD_FILE
							G_FileGrabObj.GrabEnd();
#else
							G_ImgGrabberCtrl.GrabEnd();
#endif
							

							//Sleep(100);
							//G_InspectionDataQ.m_fnQAll_DataReset();

							AOI_AppendList("Grab Stop");
							AOI_StateChange(nBiz_AOI_STATE_IDLE);
						}break;

					case nBiz_Send_IllumValueReset:
						{
							AOI_AppendList("▶▶▶▶ IllumValueReset");

							//조도 Max값 저장 버퍼 클리어.
							G_ProcessState.m_HistScanMaxValue = 0;
							memset(G_ProcessState.m_Histogaram_Data, 0x00, sizeof(float)*256);

						}break;
					case nBiz_Send_IllumRunRealTime:
						{//설정후 Param Setting시 Reset된다.
							switch(RcvCmdMsg->uUnitID_Dest)
							{
								case 1:
									{
										AOI_AppendList("▶▶▶▶ Light Run Real Time(Now %s -> TRUE", (G_ProcessState.bRealTimeLightValueSend?"TRUE":"FALSE"));
										G_ProcessState.bRealTimeLightValueSend = true;
										G_ProcessState.bRealTimePixelValueSend = false;
									}break;
								case 2://2변은 편차.
									{
										AOI_AppendList("▶▶▶▶ Pixel Run Real Time(Now %s -> TRUE", (G_ProcessState.bRealTimeLightValueSend?"TRUE":"FALSE"));
										G_ProcessState.bRealTimeLightValueSend = true;
										G_ProcessState.bRealTimePixelValueSend = true;
									}break;
								default:
									{
										AOI_AppendList("▶▶▶▶ Light Run Real Time(Now %s -> TRUE", (G_ProcessState.bRealTimeLightValueSend?"TRUE":"FALSE"));
										G_ProcessState.bRealTimeLightValueSend = true;
										G_ProcessState.bRealTimePixelValueSend = false;
									}break;
							}
						}break;
					case nBiz_Send_IllValueStartEndIndex:
						{

							if(G_ProcessState.m_UnitState != nBiz_AOI_STATE_READY)//Ready 상태에서만 설정 한다.
							{
								G_ProcessState.nActiveHistCalcSkipIndex = 3;
								AOI_AppendList("▶▶▶▶ Only READY .... IllValueStartEndIndex.  Now Setting(%d)", G_ProcessState.nActiveHistCalcSkipIndex);
								break;
							}

							if(RcvCmdMsg->uUnitID_Dest <=3 || G_ProcessState.nActiveHistCalcSkipIndex>(G_CoordProcObj.m_OneLineScnaImgCnt/2))
								G_ProcessState.nActiveHistCalcSkipIndex = 3;
							else
								G_ProcessState.nActiveHistCalcSkipIndex = (int)RcvCmdMsg->uUnitID_Dest;

							AOI_AppendList("One Line Histogram Index (Start :%d , End :%d)", G_ProcessState.nActiveHistCalcSkipIndex, G_CoordProcObj.m_OneLineScnaImgCnt - G_ProcessState.nActiveHistCalcSkipIndex );
						}break;

					case nBiz_Send_BigInspect:
						{
							extern AOIBigImg G_GrabBigger;
							AOI_AppendList("nBiz_Send_BigInspect Start");

							COORD_DINFO InspectResult[MAX_DEFECT];
							memset(InspectResult, 0x00, sizeof(COORD_DINFO)*MAX_DEFECT);
							int DefectCnt = 0;
							DefectCnt =  G_GrabBigger.InspectionBigImage( m_stMaskImageInfo, m_stCommonPara, &InspectResult[0]);
							{
								//m_pServerInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex,
								//nBiz_AOI_Inspection, nBiz_Recv_BigInspectResult,	0, 
								//(USHORT)(sizeof(DEFECT_DATA)*DefectCnt),
								//(UCHAR*)pInspectResult);
								int CountSize			= sizeof(int);
								int DefectInfoSize		=(sizeof(COORD_DINFO)*MAX_DEFECT);

								int SendDataSize = CountSize+DefectInfoSize;
								UCHAR *pSendRetData = new UCHAR[SendDataSize];
								memcpy(&pSendRetData[0], &DefectCnt, CountSize );											//Defect Count 저장.
								memcpy(&pSendRetData[4], InspectResult, DefectInfoSize);					//Defect List 저장.

								m_pServerInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex,
									nBiz_AOI_Inspection, 
									nBiz_Recv_BigInspectResult, 0, 
									(USHORT)(CountSize+DefectInfoSize),
									(UCHAR *)pSendRetData);

								delete [] pSendRetData;

								m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 	nBiz_Send_BigInspect, nBiz_Send_BigSaveImg, 0);
							}
							AOI_AppendList("nBiz_Send_BigInspect End");
						}break;
					case nBiz_Send_BigSaveImg:
						{
							AOI_AppendList("nBiz_Send_BigSaveImg Start");
							extern AOIBigImg G_GrabBigger;
							AOI_AppendList("nBiz_Send_BigSaveImg");
							CString strSaveImgPath;
							
							strSaveImgPath.Format("Z:\\[04_Master]\\MacroImage.bmp");
							G_GrabBigger.SaveBigImge(strSaveImgPath.GetBuffer(strSaveImgPath.GetLength()));
							m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 	nBiz_AOI_Inspection, nBiz_Send_BigSaveImg, 0);
							AOI_AppendList("nBiz_Send_BigSaveImg End");
						}break;
					default:
						{
							throw CVs64Exception(_T("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
							AOI_AppendList("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다.");
							break;
						}
					}
				}break;
				case nBiz_AOI_AlignCam:
					{
						switch(RcvCmdMsg->uSeqID_Dest)
						{
						case nBiz_Send_AlignGrabStart:
							{
								AOI_AppendList("Align Scan Start!!");
								G_AlignImgQ.QClean();

								if(RcvCmdMsg->uUnitID_Dest % 2)//1, 3, 5, 7,.....
								{
									Align_IMG_Direction = true;
									//처음 Scan 방향은 SCD 1 0 or 1
									m_CamDicOgj.m_fnSendCameraDirection( TRUE );//다음 스켄임으로 반대로.
									AOI_AppendList("Scan Direction SCD 1");
								}
								else //0, 2, 4, 6,.....
								{
									Align_IMG_Direction = false;
									//처음 Scan 방향은 SCD 1 0 or 1
									m_CamDicOgj.m_fnSendCameraDirection( FALSE );//다음 스켄임으로 반대로.
									AOI_AppendList("Scan Direction SCD 0");
								}
								G_ProcessState.m_bAlignScan = true;
#ifdef TEST_INSPECTION_LOAD_FILE
								G_FileGrabObj.GrabStart(1, m_LineScanWidth, 5, G_CoordProcObj.m_HWParam.m_ProcImageW);
#else
								G_ImgGrabberCtrl.GrabStart(1, m_LineScanWidth);
#endif

							}break;
						case nBiz_Send_AlignGrabAbort:
							{	
								AOI_AppendList("Align Scan Abort!!");
								G_ProcessState.m_bAlignScan = false;
								G_AlignImgQ.QClean();
							}break;

						case nBiz_Send_AlignImgSave:
							{	
								AOI_AppendList("Align Img Save!!");
								G_ProcessState.m_bAlignScan = false;
								int ImgCnt = G_AlignImgQ.QGetCnt();
								if(ImgCnt>0)
								{
									CvRect Calc_CutImgArea;
									Calc_CutImgArea.x = 0;
									Calc_CutImgArea.y = 0;
									Calc_CutImgArea.width = G_CoordProcObj.m_HWParam.m_ProcImageW;
									Calc_CutImgArea.height = G_CoordProcObj.m_HWParam.m_ProcImageH;

									char TestSaveImage[MAX_PATH];
									sprintf_s(TestSaveImage, "%sAlignImg_T%d.bmp", G_CoordProcObj.m_ResultUpdatePath, m_ProcInitVal.m_nTaskNum);
									IplImage *pSaveImage =	cvCreateImage(cvSize(Calc_CutImgArea.width,Calc_CutImgArea.height*ImgCnt ), IPL_DEPTH_8U, 1);
									IMGNodeObj * pTargetNode = nullptr;
									
									if(Align_IMG_Direction == true)
										Calc_CutImgArea.y = 0;
									else
										Calc_CutImgArea.y = pSaveImage->height - Calc_CutImgArea.height;

									for(int SStep =0; SStep<ImgCnt; SStep++)
									{
										pTargetNode = G_AlignImgQ.QGetNode();
										if(Align_IMG_Direction == true)
										{
											if(pTargetNode != nullptr)
											{
												cvSetImageROI(pSaveImage, Calc_CutImgArea);

												cvCopy(pTargetNode->pSaveImg, pSaveImage);
												cvResetImageROI(pSaveImage);
												Calc_CutImgArea.y += Calc_CutImgArea.height;
												//////////////////////////////////////////////////////////////////////////
												//정리작업
												if(pTargetNode->pSaveImg != nullptr)
													cvReleaseImage(&pTargetNode->pSaveImg);
												pTargetNode->pSaveImg = nullptr;
												delete pTargetNode;
												pTargetNode = nullptr;
											}
											else
												break;
										}
										else
										{
											cvFlip(pTargetNode->pSaveImg,0);//짝수 방향일경우 Image가 Y축이 바뀐 상태임으로 전환.
											
											//밑에서 위로 붙인다.
											if(pTargetNode != nullptr)
											{
												cvSetImageROI(pSaveImage, Calc_CutImgArea);

												cvCopy(pTargetNode->pSaveImg, pSaveImage);
												cvResetImageROI(pSaveImage);
												Calc_CutImgArea.y -= Calc_CutImgArea.height;

												//////////////////////////////////////////////////////////////////////////
												//정리작업
												if(pTargetNode->pSaveImg != nullptr)
													cvReleaseImage(&pTargetNode->pSaveImg);
												pTargetNode->pSaveImg = nullptr;
												delete pTargetNode;
												pTargetNode = nullptr;
											}
											else
												break;
										}
									}
									cvSaveImage(TestSaveImage, pSaveImage);
									cvReleaseImage(&pSaveImage);
									m_pServerInterface->m_fnSendCommand_Nrs(ALIGN_TASK_AOI76, nBiz_AOI_AlignCam, nBiz_Recv_AlignImgEnd, Align_IMG_Direction==true?1:2, (USHORT)strlen(TestSaveImage), (UCHAR*)&TestSaveImage[0]);
								}
								G_AlignImgQ.QClean();
							}break;
						default:
							{
								throw CVs64Exception(_T("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다."));
								AOI_AppendList("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Sequence 입니다.");
								break;
							}
						}
					}break;

			default:
				{
					throw CVs64Exception(_T("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
					AOI_AppendList("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다.");
					break;
				}
			}


		}
		else
		{
			throw CVs64Exception(_T("CNBiz_AOI_InspectorDlg::Sys_fnAnalyzeMsg, 정의 되지 않은 Function 입니다."));
		}
	}
	catch (...)
	{
		throw; 
	}


	return OKAY;
}
/*
*	Module Name		:	AOI_AppendList
*	Parameter		:	Message String
*	Return			:	없음
*	Function		:	정보 List 및 Log file저장.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
void CVS11AOIIDlg::AOI_AppendList(char *pMsg,...)
{
	//m_ViewStateList.
	char	outstr[302];
	char	buf[260];
	va_list argptr;

	va_start(argptr,pMsg);
	vsprintf(buf,pMsg,argptr);
	memset(&outstr[0],0,302);
	sprintf(&outstr[0], "%s",buf);
	va_end(argptr);

	char LogTime[30/*STRING_LENGTHE_LOG_TIME*/+1] = {0,};
	SYSTEMTIME SystemTime;
	GetLocalTime(&SystemTime); 
	//Format ex)"2006/10/20-16/48/52-102"
	sprintf_s(LogTime, "%02d:%02d:%02d:%03d", 
		SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, SystemTime.wMilliseconds);
	
	if(m_ViewStateList.GetCount()>LISTDATACOUNT)
	{
		m_ViewStateList.DeleteString(m_ViewStateList.GetCount()-LISTDATACOUNT);
	}
	CString WriteData;
	WriteData.Format("[%s] %s", LogTime, outstr);
	m_ViewStateList.AddString(WriteData);
	m_ViewStateList.SetScrollPos(SB_VERT , m_ViewStateList.GetCount(), TRUE);
	m_ViewStateList.SetTopIndex(m_ViewStateList.GetCount()-1);

	m_pServerInterface->m_fnPrintLog(m_ProcInitVal.m_nLogFileID, LOG_LEVEL_ALWAYS,  outstr);
}

LRESULT		CVS11AOIIDlg::AOI_UpdateStateMessage(WPARAM wParam, LPARAM lParam)
{
	int NewState = (int)wParam;
	int AlarmCode = (int)lParam;
	//각 Thread로 부터 상태 갱신 요청이 들어 온다.
	AOI_StateChange(NewState, AlarmCode);
	return 0;
}

/*
*	Module Name		:	AOI_StateChange
*	Parameter		:	변경 상태 값.
*	Return			:	현재 상태.
*	Function		:	상태 변경(IDLE/BUSY/READY/ALARM)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
short CVS11AOIIDlg::AOI_StateChange(int setAOIState, int AlarmCode)
{
	int OldState = G_ProcessState.m_UnitState;
	int RetStateChValue = 	G_ProcessState.AOI_StateChange(setAOIState);
	if(setAOIState == nBiz_AOI_STATE_ALARM)
	{
		extern char *G_strAlarmDefe[];
		AOI_AppendList("Alarm Code :%s", G_strAlarmDefe[AlarmCode]);
		m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, 	nBiz_AOI_System, nBiz_Recv_Alarm, AlarmCode);

		G_ProcessState.m_InspectionStop = INSPECTION_STOP;
#ifdef TEST_INSPECTION_LOAD_FILE
		G_FileGrabObj.GrabEnd();
#else
		G_ImgGrabberCtrl.GrabEnd();
#endif
		AOI_AppendList("**>>>Grab Stop By Alarm");
	}

	if(RetStateChValue == nBiz_ST_CH_OK)
	{
		CString NowState;
		//////////////////////////////////////////////////////////////////////////
		switch(OldState)
		{
		case nBiz_AOI_STATE_IDLE:
			NowState.Format("Now IDLE");
			break;
		case nBiz_AOI_STATE_READY:
			NowState.Format("Now READY");
			break;
		case nBiz_AOI_STATE_BUSY:
			NowState.Format("Now BUSY");
			break;
		case nBiz_AOI_STATE_ALARM:
			NowState.Format("Now ALARM");
			break;
		case nBiz_AOI_STATE_ERROR:
			NowState.Format("Now ERROR");
			break;
		}
		//////////////////////////////////////////////////////////////////////////
		switch(G_ProcessState.m_UnitState)
		{
		case nBiz_AOI_STATE_IDLE:	//0x00 : 0x01
			m_stTaskState.SetWindowText("IDLE");
			AOI_AppendList("Change State %s -> IDLE", NowState.GetBuffer(NowState.GetLength()));
			break;
		case nBiz_AOI_STATE_READY:	//0x00 : 0x02
			m_stTaskState.SetWindowText("READY");
			AOI_AppendList("Change State %s -> READY", NowState.GetBuffer(NowState.GetLength()));
			break;
		case nBiz_AOI_STATE_BUSY:	//0x00 : 0x04
			m_stTaskState.SetWindowText("BUSY");
			AOI_AppendList("Change State %s -> BUSY", NowState.GetBuffer(NowState.GetLength()));
			break;
		case nBiz_AOI_STATE_ALARM:
			m_stTaskState.SetWindowText("ALARM");
			AOI_AppendList("Change State %s -> ALARM", NowState.GetBuffer(NowState.GetLength()));
			break;
		}
		//////////////////////////////////////////////////////////////////////////
		AOI_ClearIndicator();
		switch(G_ProcessState.m_UnitState)
		{
		case nBiz_AOI_STATE_IDLE:
		case nBiz_AOI_STATE_READY:
			{
				// Set text blinking colors
				m_stTaskState.SetBlinkTextColors(RGB(0,0,255), RGB(0,0,0));
				// Set background blinking colors
				m_stTaskState.SetBlinkBkColors(RGB(0, 255, 0), RGB(0,255,0));
			}
			break;
		case nBiz_AOI_STATE_BUSY:
			{
				// Set text blinking colors
				m_stTaskState.SetBlinkTextColors(RGB(255,0,0), RGB(0,0,255));
				// Set background blinking colors
				m_stTaskState.SetBlinkBkColors(RGB(0, 255, 0), RGB(0,255,0));
			}
			break;
		case nBiz_AOI_STATE_ALARM:
			// Set text blinking colors
			m_stTaskState.SetBlinkTextColors(RGB(255,0,0), RGB(0,0,255));
			// Set background blinking colors
			m_stTaskState.SetBlinkBkColors(RGB(0, 0, 255), RGB(255,0,0));
			break;
		}
		// Start background blinking
		m_stTaskState.StartBkBlink(TRUE, CColorStaticST::ST_FLS_FAST);	
		// Start text blinking
		m_stTaskState.StartTextBlink(TRUE, CColorStaticST::ST_FLS_FAST);
		// We want be notified when the control blinks
		m_stTaskState.EnableNotify(this, WM_USER + 10);
		//////////////////////////////////////////////////////////////////////////
		//상태 Update.
		AOI_UpdateAOIStateToServer();
	}
	else
	{
		CString NowState;
		//////////////////////////////////////////////////////////////////////////
		switch(G_ProcessState.m_UnitState)
		{
		case nBiz_AOI_STATE_IDLE:
			NowState.Format("Now IDLE");
			break;
		case nBiz_AOI_STATE_READY:
			NowState.Format("Now READY");
			break;
		case nBiz_AOI_STATE_BUSY:
			NowState.Format("Now BUSY");
			break;
		case nBiz_AOI_STATE_ALARM:
			NowState.Format("Now ALARM");
			break;
		case nBiz_AOI_STATE_ERROR:
			NowState.Format("Now ERROR");
			break;
		}
		CString NewMsg;
		switch(setAOIState)
		{
		case nBiz_AOI_STATE_IDLE:	//0x00 : 0x01
			NewMsg.Format("Change State Error %s -> IDLE", NowState.GetBuffer(NowState.GetLength()));
			break;
		case nBiz_AOI_STATE_READY:	//0x00 : 0x02
			NewMsg.Format("Change State Error %s -> READY", NowState.GetBuffer(NowState.GetLength()));
			break;
		case nBiz_AOI_STATE_BUSY:	//0x00 : 0x04
			NewMsg.Format("Change State Error %s -> BUSY", NowState.GetBuffer(NowState.GetLength()));
			break;
		case nBiz_AOI_STATE_ALARM:
			NewMsg.Format("Change State Error %s -> ALARM", NowState.GetBuffer(NowState.GetLength()));
			break;
		}
		AOI_AppendList("%s", NewMsg.GetBuffer(NewMsg.GetLength()));
		switch(RetStateChValue)
		{
		case nBiz_ST_CH_ERROR_ALARM_TO_OTHER://		ALARM	==> IDLE Only OK
			{
				AOI_AppendList(">>> ALARM to Only IDLE");
			}break;
		case nBiz_ST_CH_ERROR_BUSY_TO_OTHER://		BUSY		==> IDLE,				ALARM
			{
				AOI_AppendList(">>> BUSY to (IDLE or ALARM)");
			}break;
		case nBiz_ST_CH_ERROR_READY_TO_OTHER://		READY	==> BUSY, IDLE,		ALARM
			{
				AOI_AppendList(">>> READY to (BUSY or IDLE or ALARM)");
			}break;
		case nBiz_ST_CH_ERROR_IDLE_TO_OTHER://		IDLE		==> READY,			ALARM
			{
				AOI_AppendList(">>> IDLE to (READY or ALARM)");
			}break;
		case nBiz_ST_CH_ERROR_STATE_ERROR:
			{

			}break;
		}
	}
	
	return RetStateChValue;
}
/*
*	Module Name		:	AOI_UpdateAOIStateToServer
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	Mask Server로 Inspect State를 전달한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
void    CVS11AOIIDlg::AOI_UpdateAOIStateToServer()
{

	//m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex,							///< 목적지 Task 번호
	//															nBiz_AOI_System,				///< 호출 할 목적지의 Funtion ID
	//															nBiz_Recv_Status,					///< 호출 할 목적지의 Sequence ID
	//															(USHORT)G_ProcessState.m_UnitState);				///< 현재 State.


	m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex,							///< 목적지 Task 번호
																	nBiz_AOI_System,				///< 호출 할 목적지의 Funtion ID
																	nBiz_Recv_Status,					///< 호출 할 목적지의 Sequence ID
																	(USHORT)G_ProcessState.m_UnitState,				///< 현재 State.
																	(USHORT)(sizeof(int)*4),
																	(UCHAR*)&m_FdcData);


	//m_pServerInterface->m_fnSendCommand_Nrs(	m_ProcInitVal.m_nTaskNum+70,	///< 목적지 Task 번호
	//																nBiz_AOI_System,				///< 호출 할 목적지의 Funtion ID
	//																nBiz_Recv_Status,					///< 호출 할 목적지의 Sequence ID
	//																(USHORT)G_ProcessState.m_UnitState);				///< 현재 State.
	//if(G_ProcessState.bTEST_VS75_REVIEW_SEND == true)
	//{
	//	m_pServerInterface->m_fnSendCommand_Nrs(	REVIEW_TASK_NUM,							///< 목적지 Task 번호
	//		nBiz_AOI_System,				///< 호출 할 목적지의 Funtion ID
	//		nBiz_Recv_Status,					///< 호출 할 목적지의 Sequence ID
	//		(USHORT)G_ProcessState.m_UnitState);				///< 현재 State.
	//}
}

void CVS11AOIIDlg::DebugScreenCapture(char *pSavePath)
{
	int nScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int nScreenHeight = GetSystemMetrics(SM_CYSCREEN);
	HWND hDesktopWnd = ::GetDesktopWindow();
	HDC hDesktopDC = ::GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC, nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap); 

	BitBlt(hCaptureDC, 0, 0, nScreenWidth, nScreenHeight, hDesktopDC, 0,0, SRCCOPY|CAPTUREBLT); 

	BITMAPINFO bmi = {0}; 
	bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader); 
	bmi.bmiHeader.biWidth = nScreenWidth; 
	bmi.bmiHeader.biHeight = nScreenHeight; 
	bmi.bmiHeader.biPlanes = 1; 
	bmi.bmiHeader.biBitCount = 32; 
	bmi.bmiHeader.biCompression = BI_RGB; 


	IplImage	*pViewImage = cvCreateImage( cvSize(nScreenWidth,nScreenHeight), IPL_DEPTH_8U, 4);
	IplImage	*pViewImage2 = cvCreateImage( cvSize(nScreenWidth,nScreenHeight), IPL_DEPTH_8U, 3);

	//RGBQUAD *pPixels = new RGBQUAD[nScreenWidth * nScreenHeight]; 

	GetDIBits(hCaptureDC, 	hCaptureBitmap, 	0,  nScreenHeight,  pViewImage->imageData, &bmi,  DIB_RGB_COLORS);  
	cvCvtColor(pViewImage, pViewImage2, CV_BGRA2BGR);// 4 Channel 3 Channel로 변환.
	cvFlip(pViewImage2, pViewImage2, 0);
	cvSaveImage(pSavePath,  pViewImage2);
	cvReleaseImage(&pViewImage);
	cvReleaseImage(&pViewImage2);

	::ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);
}

/*
*	Module Name		:	AOI_StateChangeCheck
*	Parameter		:	변경 상태 값.
*	Return			:	TRUE/FALSE
*	Function		:	변경 하려고 시도하는 상태가 가능한지 판다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
//bool CVS11AOIIDlg::AOI_StateChangeCheck(int CheckAOIState)
//{
//	bool  ChangeStateOk = TRUE;
//	short ChangeState = nBiz_AOI_STATE_ERROR;
//	//////////////////////////////////////////////////////////////////////////
//	//상태 변경이 가능한지 를 판별 한다.
//	switch(G_ProcessState.m_UnitState)
//	{
//	case nBiz_AOI_STATE_BUSY:
//		{
//			switch(CheckAOIState)
//			{
//			case nBiz_AOI_STATE_IDLE:// State Change Error.
//				AOI_AppendList("State Change Error ( BUSY -> IDLE )");
//				break;
//			case nBiz_AOI_STATE_READY:
//				ChangeState = CheckAOIState;
//				break;
//			default:
//				{
//					if((G_ProcessState.m_UnitState & nBiz_AOI_STATE_ALARM) > 0 )
//						ChangeState = CheckAOIState;
//					else
//					{
//						AOI_AppendList("Unknown State(%d)", CheckAOIState);
//						ChangeState = nBiz_AOI_STATE_ERROR;
//					}
//				}break;
//			}
//		}break;
//	case nBiz_AOI_STATE_IDLE:
//		{
//			switch(CheckAOIState)
//			{
//			case nBiz_AOI_STATE_BUSY:// State Change Error.
//				AOI_AppendList("State Change Error ( IDLE -> BUSY )");
//				break;
//			case nBiz_AOI_STATE_READY:
//				ChangeState = CheckAOIState;
//				break;
//			default:
//				{
//					if((G_ProcessState.m_UnitState & nBiz_AOI_STATE_ALARM) > 0 )
//						ChangeState = CheckAOIState;
//					else
//					{
//						AOI_AppendList("Unknown State(%d)", CheckAOIState);
//						ChangeState = nBiz_AOI_STATE_ERROR;
//					}
//				}break;
//			}
//		}break;
//	case nBiz_AOI_STATE_READY:
//		{
//			switch(CheckAOIState)
//			{
//			case nBiz_AOI_STATE_IDLE:
//				ChangeState = CheckAOIState;
//				break;
//			case nBiz_AOI_STATE_BUSY:
//				ChangeState = CheckAOIState;
//				break;
//			default:
//				{
//					if((G_ProcessState.m_UnitState & nBiz_AOI_STATE_ALARM) > 0 )
//						ChangeState = CheckAOIState;
//					else
//					{
//						AOI_AppendList("Unknown State(%d)", CheckAOIState);
//						ChangeState = nBiz_AOI_STATE_ERROR;
//					}
//				}break;
//			}
//		}break;
//	default:
//		if((G_ProcessState.m_UnitState & nBiz_AOI_STATE_ALARM) > 0 )
//		{
//			{
//				switch(CheckAOIState)
//				{
//				case nBiz_AOI_STATE_IDLE:
//					ChangeState = CheckAOIState;
//					break;
//				case nBiz_AOI_STATE_READY:
//					ChangeState = CheckAOIState;
//					break;
//				default:
//					{
//						if((G_ProcessState.m_UnitState & nBiz_AOI_STATE_ALARM) > 0 )
//							ChangeState = CheckAOIState;
//						else
//						{
//							AOI_AppendList("Unknown State(%d)", CheckAOIState);
//							ChangeState = nBiz_AOI_STATE_ERROR;
//						}
//					}break;
//				}
//			}
//		}
//		else
//		{
//			ChangeState = nBiz_AOI_STATE_ERROR;
//		}
//		break;
//	}
//
//
//	return ChangeStateOk;
//}
void CVS11AOIIDlg::OnBnClickedBtGrabFileStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	CString strTextFilePath;
	//CString strImageFilePath;
	strTextFilePath.Format("R:\\Defect_Inspect_T%2d.txt", m_ProcInitVal.m_nTaskNum);
	//strImageFilePath.Format("R:\\Defect_Inspect_T%2d.dat", m_ProcInitVal.m_nTaskNum);
	G_WriteRetObj.m_fnSetWriteDefectTextObj(strTextFilePath);//, strImageFilePath);

	_AOI_Act_SearchParameterSet(m_stMaskImageInfo,  m_stCommonPara);
	G_FileGrabObj.GrabStart(1, G_CoordProcObj.m_OneLineScnaImgCnt);
	

}


void CVS11AOIIDlg::OnBnClickedBtGrabFileEnd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	G_FileGrabObj.GrabEnd();
	G_WriteRetObj.m_fnCloseFileObj();
}


/*
*	Module Name		:	AOI_UpdateProcState
*	Parameter		:	없음
*	Return			:	없음
*	Function		:	Thread 상태 Update.(Inspect Thread/Result Thread/ Image Queue ETC)
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
int nUpdateData = -1;
void    CVS11AOIIDlg::AOI_UpdateProcState()
{

	//////////////////////////////////////////////////////////////////////////
	//UI Update
	int TotalIndex  = ((G_ProcessState.m_SortScanLineNum-1) * G_CoordProcObj.m_OneLineScnaImgCnt)+G_ProcessState.m_SortScanLineIndex;
	m_proTotalStep.SetPos(TotalIndex);
	m_proLineStep.SetPos(G_ProcessState.m_SortScanLineIndex);
	m_proPADProcStep.SetPos(G_ProcessState.m_PADInspectStep);
	//////////////////////////////////////////////////////////////////////////

	CString strNewState;
	//////////////////////////////////////////////////////////////////////////
	//IDT Memory State
	strNewState.Format("%d", *G_ProcessState.m_pNodeCount);
	GetDlgItem( IDC_ST_IDT_NODE_COUNT)->SetWindowText(strNewState);
	strNewState.Format("%d", *G_ProcessState.m_pIdleNodeCnt);
	GetDlgItem( IDC_ST_IDT_IDLE_NODES)->SetWindowText(strNewState);

	strNewState.Format("%d", *G_ProcessState.m_pGrabScanLineNum);
	GetDlgItem( IDC_ST_IDT_SCAN_NUM )->SetWindowText(strNewState);
	strNewState.Format("%d", *G_ProcessState.m_pGrabScanImageCnt);
	GetDlgItem( IDC_ST_IDT_SCAN_IMAGE_INDEX)->SetWindowText(strNewState);
	strNewState.Format("%s", *G_ProcessState.m_pOnlyGrabImageSave?"TRUE":"FALSE");
	GetDlgItem( IDC_ST_IDT_SAVE_ONLY)->SetWindowText(strNewState);
	//////////////////////////////////////////////////////////////////////////
	//Mil Grabber State.
	strNewState.Format("%s", G_ProcessState.m_bMilRunGrabFunction?"TRUE":"FALSE");
	GetDlgItem( IDC_ST_MIL_GRAB_FUN_RUN)->SetWindowText(strNewState);
	strNewState.Format("%s", G_ProcessState.m_bMilGrabFunMemError?"TRUE":"FALSE");
	GetDlgItem( IDC_ST_MIL_GRAB_FUN_MEM_ERROR)->SetWindowText(strNewState);
	strNewState.Format("%d/%d", G_ProcessState.m_nMilGrabFunProTick, G_ProcessState.m_nNoneProcImgCount);
	GetDlgItem( IDC_ST_MIL_GRAB_ONE_IMG_TIME)->SetWindowText(strNewState);
	strNewState.Format("%d", G_ProcessState.m_nFileGrabFunProTick);
	GetDlgItem( IDC_ST_FILE_GRAB_ONE_IMG_TIME)->SetWindowText(strNewState);
	//////////////////////////////////////////////////////////////////////////
	//Inspector Proces State.
	for(int i=0; i<AOI_DLL_PROCESS_CNT; i++)
	{
		switch((G_ProcessState.m_ProcStateInspector[i]>>24))
		{
		case AOI_DLLPROCESS_STATE_NONE:
			m_stDLLProcState[i].SetTextColor(RGB(100,100,100));
			m_stDLLProcState[i].SetWindowText("NONE");
			break;
		case AOI_DLLPROCESS_STATE_RUN:
			m_stDLLProcState[i].SetTextColor(RGB(255,0,0));
			m_stDLLProcState[i].SetWindowText("RUN");
			break;
		case AOI_DLLPROCESS_STATE_IDLE:
			m_stDLLProcState[i].SetTextColor(RGB(0,0,255));
			m_stDLLProcState[i].SetWindowText("IDLE");
			break;
		}

		if((G_ProcessState.m_ProcStateInspector[0] & 0x00FFFF) == G_CoordProcObj.m_OneLineScnaImgCnt)
		{
			m_stGPUProc.SetTextColor(RGB(255,255,0));
			m_stGPUProc.SetBkColor(RGB(0,0,255));
		}
		else if(m_stGPUProc.GetBkColor() != RGB(0,0,0))
		{
			m_stGPUProc.SetTextColor(RGB(0,255,0));
			m_stGPUProc.SetBkColor(RGB(0,0,0));
		}


		strNewState.Format("%02d_%04d", (G_ProcessState.m_ProcStateInspector[i]>>16)&0x00FF,  (G_ProcessState.m_ProcStateInspector[i])&0x00FFFF);
		GetDlgItem( IDC_ST_INSPECT_PRO_DATA1+i)->SetWindowText(strNewState);
	}
	//////////////////////////////////////////////////////////////////////////
	//Cutter Process State.
	for(int CutStpe=0; CutStpe<PADCUT_PROC_CNT; CutStpe++)
	{
		if(G_ProcessState.m_CutScanLineIndex[CutStpe] == G_CoordProcObj.m_OneLineScnaImgCnt)
		{
			m_stCutterProc[CutStpe].SetTextColor(RGB(255,255,0));
			m_stCutterProc[CutStpe].SetBkColor(RGB(0,0,255));
		}
		else if(m_stCutterProc[CutStpe].GetBkColor() != RGB(0,0,0))
		{
			m_stCutterProc[CutStpe].SetTextColor(RGB(0,255,0));
			m_stCutterProc[CutStpe].SetBkColor(RGB(0,0,0));
		}

		strNewState.Format("%d", G_ProcessState.m_nCutterProcTick[CutStpe]);
		GetDlgItem( IDC_ST_CUTTER_PROC1+CutStpe)->SetWindowText(strNewState);

		strNewState.Format("%02d_%04d",G_ProcessState.m_CutScanLineNum[CutStpe],  G_ProcessState.m_CutScanLineIndex[CutStpe]);
		GetDlgItem( IDC_ST_CUTTER_PRO_DATA1+CutStpe)->SetWindowText(strNewState);
	}
	//////////////////////////////////////////////////////////////////////////
	//Result Process State.
	for(int RetStep=0; RetStep<RET_PROC_CNT; RetStep++)
	{

		if(G_ProcessState.m_RetScanLineIndex[RetStep] == G_CoordProcObj.m_OneLineScnaImgCnt)
		{
			m_stResultProc[RetStep].SetTextColor(RGB(255,255,0));
			m_stResultProc[RetStep].SetBkColor(RGB(0,0,255));
		}
		else if(m_stResultProc[RetStep].GetBkColor() != RGB(0,0,0))
		{
			m_stResultProc[RetStep].SetTextColor(RGB(0,255,0));
			m_stResultProc[RetStep].SetBkColor(RGB(0,0,0));
		}

		//strNewState.Format("%d(%d)", G_ProcessState.m_nResultProcTick[RetStep], G_ProcessState.m_RetDefectCnt[RetStep]);
		strNewState.Format("%d", G_ProcessState.m_nResultProcTick[RetStep]);
		GetDlgItem( IDC_ST_RESULT_PROC1+RetStep)->SetWindowText(strNewState);
		strNewState.Format("%02d_%04d",G_ProcessState.m_RetScanLineNum[RetStep],  G_ProcessState.m_RetScanLineIndex[RetStep]);
		GetDlgItem( IDC_ST_RESULT_PRO_DATA1+RetStep)->SetWindowText(strNewState);
	}
	strNewState.Format("%d/%d",G_ProcessState.m_PadProcTick[0], G_ProcessState.m_PadAlignProcTick);
	GetDlgItem( IDC_ST_PAD_PROC)->SetWindowText(strNewState);


	strNewState.Format("%0.1f/%0.1f",(float)G_ProcessState.m_nProcFileUpdateTime/1000.0f, (float)G_ProcessState.m_nProcFileUpdateMaxTime/1000.0f);
	GetDlgItem( IDC_ST_FILE_BACKUP_TIME)->SetWindowText(strNewState);

	//////////////////////////////////////////////////////////////////////////
	//Sort Process State.
	strNewState.Format("%d", G_ProcessState.m_nSortProcTick);
	GetDlgItem( IDC_ST_SORT_PROC )->SetWindowText(strNewState);

	strNewState.Format("%02d_%04d",G_ProcessState.m_SortScanLineNum,  G_ProcessState.m_SortScanLineIndex);
	GetDlgItem( IDC_ST_SORT_PRO_DATA)->SetWindowText(strNewState);

	//////////////////////////////////////////////////////////////////////////
	//Process Time By Images
	//strNewState.Format("Img Min : %d", G_ProcessState.m_nOneImageMinTick);
	//GetDlgItem( IDC_ST_ONE_IMG_PROC_TICK )->SetWindowText(strNewState);

	//strNewState.Format("Img Max : %d", G_ProcessState.m_nOneImageMaxTick);
	//GetDlgItem( IDC_ST_ONE_LINE_PROC_TICK )->SetWindowText(strNewState);

	//strNewState.Format("Img Now : %d", G_ProcessState.m_nOneImageTick);
	//GetDlgItem( IDC_ST_ONE_BLOCK_PROC_TICK )->SetWindowText(strNewState);

	if(G_ProcessState.m_nOneLineNowIndex>0)
	{
		double TotalSize = ((G_CoordProcObj.m_HWParam.m_ProcImageW*G_CoordProcObj.m_HWParam.m_ProcImageH)/(1024.0*1024.0))*G_ProcessState.m_nOneLineNowIndex;
		double ProcTime = ((double)G_ProcessState.m_nOneLineNowTick-(double)G_ProcessState.m_nOneLineStartTick);
		double ProcPerSec = TotalSize /(ProcTime/1000.0);
		strNewState.Format("%0.3f MB/Sec",ProcPerSec);
		GetDlgItem( IDC_ST_PROC_SIZE_PER_SEC )->SetWindowText(strNewState);
	}

	strNewState.Format("Line Cnt: %d",G_CoordProcObj.m_AOIOneCam_ScanLineCnt);
	GetDlgItem( IDC_ST_MAP_INFO_IMG_CNT )->SetWindowText(strNewState);
	strNewState.Format("Img Cnt: %d",G_CoordProcObj.m_OneLineScnaImgCnt);
	GetDlgItem( IDC_ST_MAP_INFO_SCNLINE_CNT )->SetWindowText(strNewState);

	strNewState.Format("W Buf Cnt: %d",G_InspectDefectQ.QGetCnt());
	GetDlgItem( IDC_ST_INSPECT_DEFECT_CNT )->SetWindowText(strNewState);

	//////////////////////////////////////////////////////////////////////////
	//File Writer State
	int ColorNum = G_InspectDefectQ.QGetCnt()/3;
	strNewState.Format("%d",G_InspectDefectQ.QGetCnt());
	ColorNum = ColorNum>255?255:ColorNum;
	m_stDefectWriteCnt.SetTextColor(RGB(ColorNum, 255-ColorNum,100));
	m_stDefectWriteCnt.SetWindowText(strNewState);

	
	ColorNum = G_InspectSaveImgQ.QGetCnt()*(255/SAVE_ORG_IMG_MAX);
	strNewState.Format("%d/%d",G_InspectSaveImgQ.QGetCnt(), SAVE_ORG_IMG_MAX);

	ColorNum = ColorNum>255?255:ColorNum;
	m_stOrgImgWriteCnt.SetTextColor(RGB(ColorNum, 255-ColorNum,100));
	m_stOrgImgWriteCnt.SetWindowText(strNewState);

	if(G_InspectDefectQ.QGetCnt() > 0 ||G_InspectSaveImgQ.QGetCnt()>0 )
	{
		if(GetDlgItem(IDC_ST_DELET_FILE)->IsWindowVisible() == FALSE)
		{
			GetDlgItem(IDC_ST_DELET_FILE2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_ST_DELET_FILE3)->ShowWindow(SW_HIDE);

			GetDlgItem(IDC_ST_DELET_FILE)->ShowWindow(SW_SHOW);
		}
	}
	else
	{
		if(GetDlgItem(IDC_ST_DELET_FILE)->IsWindowVisible() == TRUE)
		{
			GetDlgItem(IDC_ST_DELET_FILE)->ShowWindow(SW_HIDE);
		}
	}

	if(G_ProcessState.m_bDefectFileCopy == true && GetDlgItem(IDC_ST_DELET_FILE)->IsWindowVisible() == FALSE)
	{
		if(GetDlgItem(IDC_ST_DELET_FILE2)->IsWindowVisible() == FALSE)
		{
			GetDlgItem(IDC_ST_DELET_FILE3)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_ST_DELET_FILE2)->ShowWindow(SW_SHOW);
		}
	}
	else if(G_ProcessState.m_bOrgImageBackup == true && GetDlgItem(IDC_ST_DELET_FILE)->IsWindowVisible() == FALSE)
	{
		if(GetDlgItem(IDC_ST_DELET_FILE3)->IsWindowVisible() == FALSE)
		{
			GetDlgItem(IDC_ST_DELET_FILE2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_ST_DELET_FILE3)->ShowWindow(SW_SHOW);
		}
	}
	else
	{
		if(GetDlgItem(IDC_ST_DELET_FILE3)->IsWindowVisible() == TRUE)
			GetDlgItem(IDC_ST_DELET_FILE3)->ShowWindow(SW_HIDE);
		if(GetDlgItem(IDC_ST_DELET_FILE2)->IsWindowVisible() == TRUE)
			GetDlgItem(IDC_ST_DELET_FILE2)->ShowWindow(SW_HIDE);
	}
	//////////////////////////////////////////////////////////////////////////
	DWORD sectors_per_cluster = 0, bytes_per_sector = 0;
	DWORD number_of_freeclusters = 0, totalnumber_of_clusters = 0;
	// C 디스크 드라이브의 공간 정보를 얻는다.
	//GetDiskFreeSpace(BUFFER_DISK_NAME, &sectors_per_cluster, &bytes_per_sector, 
	//	&number_of_freeclusters, &totalnumber_of_clusters);


	ULARGE_INTEGER FreeBytesAvailableToCaller;
	ULARGE_INTEGER TotalNumberOfBytes;
	ULARGE_INTEGER TotalNumberOfFreeBytes;
	GetDiskFreeSpaceEx(BUFFER_DISK_NAME, &FreeBytesAvailableToCaller, &TotalNumberOfBytes, &TotalNumberOfFreeBytes);

	// 전체 디스크 공간을 MB단위로 연산한다.
	double disk_size =  (double)TotalNumberOfBytes.QuadPart;
	disk_size = disk_size/1024/1024;

	// 남은 디스크 공간을 GB단위로 연산한다.
	double disk_free_size =  (double)TotalNumberOfFreeBytes.QuadPart;;
	disk_free_size = disk_free_size/1024/1024;

	m_proRamDiskSize.SetPos((int)(m_BufferDiskSize-disk_free_size));
	

	int ColorValue = (int) (((m_BufferDiskSize-disk_free_size)*256)/m_BufferDiskSize);
	//int ColorValue = (int)(m_BufferDiskSize-disk_free_size)/(512);
	m_proRamDiskSize.SetBarColor(RGB(ColorValue,255-ColorValue,0));

	strNewState.Format("%dMB", (int)disk_free_size);
	GetDlgItem(IDC_ST_BUF_DISK_FREE_SIZE)->SetWindowText(strNewState);

	strNewState.Format("%dMB", (int)(m_BufferDiskSize-disk_free_size));
	GetDlgItem(IDC_ST_BUF_DISK_USE_SIZE)->SetWindowText(strNewState);
	//////////////////////////////////////////////////////////////////////////

	m_proAFValue.SetPos((int)G_ProcessState.m_ActiveAFValue);
	//////////////////////////////////////////////////////////////////////////
	if(disk_free_size<150.0f && G_ProcessState.m_UnitState != nBiz_AOI_STATE_ALARM)
	{
		AOI_AppendList("Ram Disk Buffer Over Flow");
		AOI_StateChange(nBiz_AOI_STATE_ALARM, ALARM_CODE_DiskBuffer);
		
		G_InspectSaveImgQ.QClean();
		AOI_AppendList("Clear Inspect Image Save Q");

		//RamDisk Buffer를 쓰는 폴더를 지운다.
		AOI_AppendList("Clear Folder By Alarm:%s",G_CoordProcObj.m_HWParam.strResultPath);
		G_ClearFolder(CString(G_CoordProcObj.m_HWParam.strResultPath));
		AOI_AppendList("Clear Folder By Alarm:%s",G_CoordProcObj.m_HWParam.strOrgImgPath);
		G_ClearFolder(CString(G_CoordProcObj.m_HWParam.strOrgImgPath));
	}
	


}
int InputCount =0;
char SerialDataBuf[512]={0,};
void CVS11AOIIDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == UI_UPDATE_PROC_STATE)
	{
		AOI_UpdateProcState();
	}
	if(nIDEvent == UI_UPDATE_STATE_AOI_TASK)
	{
		AOI_UpdateAOIStateToServer();
		//DebugScreenCapture();
	}
	if(UI_FDC_DATA)
	{
		//////////////////////////////////////////////////////////////////////////

		ULARGE_INTEGER HDDFreeBytes;
		ULARGE_INTEGER HDDTotalNumberOfBytes;
		ULARGE_INTEGER HDDTotalNumberOfFreeBytes;
		GetDiskFreeSpaceEx("C:\\", &HDDFreeBytes, &HDDTotalNumberOfBytes, &HDDTotalNumberOfFreeBytes);
		// 전체 디스크 공간을 MB단위로 연산한다.
		double FDCdisk_free_size =  ((double)HDDTotalNumberOfFreeBytes.QuadPart)/1024.0/1024.0/1024.0;
		m_FdcData.INS_HDD_C_CAPA = (int)((FDCdisk_free_size)*1000.0);

		GetDiskFreeSpaceEx("D:\\", &HDDFreeBytes, &HDDTotalNumberOfBytes, &HDDTotalNumberOfFreeBytes);
		FDCdisk_free_size =  ((double)HDDTotalNumberOfFreeBytes.QuadPart)/1024.0/1024.0/1024.0;
		m_FdcData.INS_HDD_D_CAPA =  (int)((FDCdisk_free_size)*1000.0);


		MEMORYSTATUSEX statex;
		statex.dwLength = sizeof (statex);
		GlobalMemoryStatusEx (&statex);
		m_FdcData.INS_MEMORY = (int)((statex.dwMemoryLoad)*1000);// Memory Use Percent 
		//double MemTotal = (statex.ullTotalPhys/1024.0)/1024.0;// Memory Total MB
		//double MemFree = (statex.ullAvailPhys/1024.0)/1024.0;// Memory Free MB
		//m_FdcData.INS_MEMORY = (int)(((MemFree/MemTotal)*100.0)*1000.0);


		if (!m_PerfMon.CollectQueryData())
			m_vtAddLog(LOG_LEVEL_1, "FDC Failed Query!");
		else
		{
			long lcpu = m_PerfMon.GetCounterValue(m_nCPU);
			if (lcpu != -999)
				m_FdcData.INS_CPU_SHARE = lcpu*1000;
		}


		CString strFDCInfo;
		strFDCInfo.Format("%0.3f%%", m_FdcData.INS_CPU_SHARE/1000.0);
		GetDlgItem(IDC_ST_CPU)->SetWindowText(strFDCInfo);
		strFDCInfo.Format("%0.3f%%", m_FdcData.INS_MEMORY/1000.0);
		GetDlgItem(IDC_ST_MEM)->SetWindowText(strFDCInfo);
		strFDCInfo.Format("%0.3fGB", m_FdcData.INS_HDD_C_CAPA/1000.0);
		GetDlgItem(IDC_ST_HDD_C)->SetWindowText(strFDCInfo);
		strFDCInfo.Format("%0.3fGB", m_FdcData.INS_HDD_D_CAPA/1000.0);
		GetDlgItem(IDC_ST_HDD_D)->SetWindowText(strFDCInfo);


		//printf ("There is  %*ld percent of memory in use.",          WIDTH, statex.dwMemoryLoad);
		//printf ("There are %*I64d total Kbytes of physical memory.", WIDTH, statex.ullTotalPhys/DIV);
		//printf ("There are %*I64d free Kbytes of physical memory.",  WIDTH, statex.ullAvailPhys/DIV);
		//printf ("There are %*I64d total Kbytes of paging file.",     WIDTH, statex.ullTotalPageFile/DIV);
		//printf ("There are %*I64d free Kbytes of paging file.",      WIDTH, statex.ullAvailPageFile/DIV);
		//printf ("There are %*I64d total Kbytes of virtual memory.",  WIDTH, statex.ullTotalVirtual/DIV);
		//printf ("There are %*I64d free Kbytes of virtual memory.",   WIDTH, statex.ullAvailVirtual/DIV);
		//// Show the amount of extended memory available.
		//printf ("There are %*I64d free Kbytes of extended memory.",  WIDTH, statex.ullAvailExtendedVirtual/DIV);
	}

	{
		BYTE ReadByte;
		for(int RStep = 0; RStep<m_CamDicOgj.m_QueueRead.GetSize(); RStep++)
		{
			if(m_CamDicOgj.m_QueueRead.GetByte(&ReadByte) == TRUE)
			{
				SerialDataBuf[InputCount] = ReadByte;
				InputCount++;
				if(InputCount>=300 || (char)ReadByte == '\n' || (char)ReadByte == '\r')
				{
					CString recvMsg;
					
					SerialDataBuf[InputCount] = 0;
					recvMsg.Format("%s", SerialDataBuf);
					recvMsg.Remove('\r');
					recvMsg.Remove('\n');
					if(recvMsg.GetLength()>0)
					{
						if(m_ViewCamCMDList.GetCount()>LISTDATACOUNT)
							m_ViewCamCMDList.DeleteString(m_ViewCamCMDList.GetCount()-LISTDATACOUNT);

						m_ViewCamCMDList.AddString(recvMsg);
						m_ViewCamCMDList.SetScrollPos(SB_VERT , m_ViewCamCMDList.GetCount(), TRUE);
						m_ViewCamCMDList.SetTopIndex(m_ViewCamCMDList.GetCount()-1);

						m_pServerInterface->m_fnSendCommand_Nrs(	G_MasterTaskIndex, nBiz_AOI_System, nBiz_Recv_CamCommand, 0, 
							(USHORT)recvMsg.GetLength(), (UCHAR*)SerialDataBuf);
					}
					memset(SerialDataBuf, 0x00, 512);
					InputCount = 0;
				}

			}
		}
		
	}
	
	__super::OnTimer(nIDEvent);
}


void CVS11AOIIDlg::OnBnClickedBtShowMap()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	G_CoordProcObj.m_fnDrawGlassInspectInfo();
	cvNamedWindow(PARAM_DRAW_WINDOW);
	cvShowImage(PARAM_DRAW_WINDOW, G_CoordProcObj.m_pMainMpaDrawObj);
	
}


HBRUSH CVS11AOIIDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int DLG_ID_Number  = pWnd->GetDlgCtrlID();

	switch( nCtlColor )
	{
	case CTLCOLOR_DLG:
	case CTLCOLOR_STATIC:
		{
			if(DLG_ID_Number == IDC_ST_INSPECT_TASK_NUM||
				DLG_ID_Number == IDC_ST_TASK_STATE||
				DLG_ID_Number == IDC_ST_INSPECT_PRO1||
				DLG_ID_Number == IDC_ST_INSPECT_PRO2||
				DLG_ID_Number == IDC_ST_INSPECT_PRO3||
				DLG_ID_Number == IDC_ST_INSPECT_PRO4||
				DLG_ID_Number == IDC_ST_MASTER_TASK_NUM||
				DLG_ID_Number == IDC_ST_WRITE_DEFECT_BUF||
				DLG_ID_Number == IDC_ST_WRITE_ORTIMG_BUF||
				DLG_ID_Number == IDC_ST_INSPECT_PRO_DATA1||
				DLG_ID_Number == IDC_ST_CUTTER_PRO_DATA1||
				DLG_ID_Number == IDC_ST_CUTTER_PRO_DATA2||
				DLG_ID_Number == IDC_ST_CUTTER_PRO_DATA3||
				DLG_ID_Number == IDC_ST_CUTTER_PRO_DATA4||
				DLG_ID_Number == IDC_ST_RESULT_PRO_DATA1||
				DLG_ID_Number == IDC_ST_RESULT_PRO_DATA2||
				DLG_ID_Number == IDC_ST_RESULT_PRO_DATA3||
				DLG_ID_Number == IDC_ST_RESULT_PRO_DATA4)
			return hbr;
		}break;
	case CTLCOLOR_BTN:
		break;

	case CTLCOLOR_LISTBOX://List Color
		{
			if( DLG_ID_Number == IDC_AOI_STATE_LIST)
			{
				pDC->SetTextColor(RGB(100, 200, 255)); 
				pDC->SetBkColor(RGB(0, 0, 0));
				return (HBRUSH)GetStockObject(BLACK_BRUSH);
			}

			if( DLG_ID_Number == IDC_AOI_INSPECT_PARAM_LIST)
			{
				pDC->SetTextColor(RGB(255, 200, 100)); 
				pDC->SetBkColor(RGB(0, 0, 0));
				return (HBRUSH)GetStockObject(BLACK_BRUSH);
			}

			if( DLG_ID_Number == IDC_AOI_GLASS_PARAM_LIST)
			{
				pDC->SetTextColor(RGB(100, 255, 200)); 
				pDC->SetBkColor(RGB(0, 0, 0));
				return (HBRUSH)GetStockObject(BLACK_BRUSH);
			}

			if( DLG_ID_Number == IDC_AOI_HW_PARAM_LIST)
			{
				pDC->SetTextColor(RGB(200, 100, 255)); 
				pDC->SetBkColor(RGB(0, 0, 0));
				return (HBRUSH)GetStockObject(BLACK_BRUSH);
			}
			if( DLG_ID_Number == IDC_AOI_CAM_CMD_LIST)
			{
				pDC->SetTextColor(RGB(255, 255, 255)); 
				pDC->SetBkColor(RGB(0, 0, 0));
				return (HBRUSH)GetStockObject(BLACK_BRUSH);
			}
		}break;
	}
	pDC->SetTextColor(RGB(0, 255, 0)); 
	pDC->SetBkColor(RGB(0, 0, 0));
	return (HBRUSH)GetStockObject(BLACK_BRUSH);
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CVS11AOIIDlg::SetUIStaticData()
{
	m_InspectActFont.CreatePointFont(700, "Stencil");
	m_stGPUProc.SetTextColor(RGB(0,255,0));
	m_stGPUProc.SetBkColor(RGB(0,0,0));

	for(int i=0; i<4; i++)
	{
		m_stCutterProc[i].SetTextColor(RGB(0,255,0));
		m_stCutterProc[i].SetBkColor(RGB(0,0,0));

		m_stResultProc[i].SetTextColor(RGB(0,255,0));
		m_stResultProc[i].SetBkColor(RGB(0,0,0));
	}

	m_TaskNumFont.CreatePointFont(700, "Stencil");
	m_stMainTaskNum.SetTextColor(RGB(255,255,0));
	m_stMainTaskNum.SetBkColor(RGB(0,0,0));
	m_stMainTaskNum.SetFont(&m_TaskNumFont);

	m_StateFont.CreatePointFont(380, "Stencil");
	m_stTaskState.SetTextColor(RGB(255,255,0));
	m_stTaskState.SetBkColor(RGB(0,0,0));
	m_stTaskState.SetFont(&m_StateFont);
	m_stTaskState.EnableNotify(	this, WM_USER + 10);

	m_NormalFont.CreatePointFont(110, "Stencil");
	for(int PrStep = 0; PrStep<4; PrStep++)
	{
		m_stDLLProcState[PrStep].SetTextColor(RGB(0,0,255));
		m_stDLLProcState[PrStep].SetBkColor(RGB(200,200,200));
		m_stDLLProcState[PrStep].SetFont(&m_NormalFont);
	}

	m_stMasterTask.SetTextColor(RGB(255,0,0));
	m_stMasterTask.SetBkColor(RGB(0,0,0));
	m_stMasterTask.SetFont(&m_NormalFont);

	m_DspNumFont.CreatePointFont(180, "Stencil");
	
	m_stDefectWriteCnt.SetTextColor(RGB(255,255,0));
	m_stDefectWriteCnt.SetBkColor(RGB(0,0,0));
	m_stDefectWriteCnt.SetFont(&m_DspNumFont);
	//m_stDefectWriteCnt.SetBlinkBkColors(RGB(0,0,255), RGB(255,255,0));
	//m_stDefectWriteCnt.EnableNotify(	this, WM_USER + 10);

	m_stOrgImgWriteCnt.SetTextColor(RGB(255,255,0));
	m_stOrgImgWriteCnt.SetBkColor(RGB(0,0,0));
	m_stOrgImgWriteCnt.SetFont(&m_DspNumFont);

	//////////////////////////////////////////////////////////////////////////
	tButtonStyle tStyle;
	m_tButtonStyle.GetButtonStyle(&tStyle);
	//tStyle.m_tColorFace.m_tClicked		= RGB(0x50, 0x50, 0x50);
	//tStyle.m_tColorBorder.m_tClicked	= RGB(0xFF, 0xFF, 0xFF);
	m_tButtonStyle.SetButtonStyle(&tStyle);

	tColorScheme tColor;
	LOGFONT tFont;
	{
		m_btEditHWParam.GetTextColor(&tColor);
		m_btEditHWParam.GetFont(&tFont);
		{
			// Change Color
			// Button is disabled
			//tColor.m_tDisabled  = RGB(0xAF, 0x00, 0x00);
			// Button is enabled but not clicked
			tColor.m_tEnabled	= RGB(0xFF, 0xFF, 0xFF);
			// Button is clicked, meaning checked as check box or selected as radio button
			tColor.m_tClicked	= RGB(0x00, 0xAF, 0x00);
			// Button is pressed, Mouse is on button and left mouse button pressed
			tColor.m_tPressed	= RGB(0x00, 0x00, 0xAF);

			strcpy_s(tFont.lfFaceName,31,"Times New Roman");
			tFont.lfHeight =18;
		}

		m_btEditHWParam.SetRoundButtonStyle(&m_tButtonStyle);
		m_btEditHWParam.SetTextColor(&tColor);
		m_btEditHWParam.SetCheckButton(false);
		m_btEditHWParam.SetFont(&tFont);

		m_btShowDrawParam.SetRoundButtonStyle(&m_tButtonStyle);
		m_btShowDrawParam.SetTextColor(&tColor);
		m_btShowDrawParam.SetCheckButton(false);
		m_btShowDrawParam.SetFont(&tFont);

		m_btOpenFolder.SetRoundButtonStyle(&m_tButtonStyle);
		m_btOpenFolder.SetTextColor(&tColor);
		m_btOpenFolder.SetCheckButton(false);
		m_btOpenFolder.SetFont(&tFont);
		
		m_btOpenCamCTRL.SetRoundButtonStyle(&m_tButtonStyle);
		m_btOpenCamCTRL.SetTextColor(&tColor);
		m_btOpenCamCTRL.SetCheckButton(false);
		m_btOpenCamCTRL.SetFont(&tFont);

		m_btSWVersion.SetRoundButtonStyle(&m_tButtonStyle);
		m_btSWVersion.SetTextColor(&tColor);
		m_btSWVersion.SetCheckButton(false);
		m_btSWVersion.SetFont(&tFont);
	}
	//HW Param Edit Window
	m_pDlgEditHWParam = new CHWParamEdit(this);
	m_pDlgEditHWParam->m_TaskNum = m_ProcInitVal.m_nTaskNum;
	m_pDlgEditHWParam->Create(IDD_DLG_EDIT_HW_PARAM);
	m_pDlgEditHWParam->ShowWindow(SW_HIDE);//SW_SHOW

	m_CommandFont.CreatePointFont(90, "Consolas");
	GetDlgItem(IDC_AOI_CAM_CMD_LIST)->SetFont(&m_CommandFont);
	GetDlgItem(IDC_ED_SEMD_CMD)->SetFont(&m_CommandFont);

	GetDlgItem( IDC_ST_IDT_NODE_COUNT)->SetFont(&m_DspNumFont);
	GetDlgItem( IDC_ST_IDT_IDLE_NODES)->SetFont(&m_DspNumFont);

	GetDlgItem( IDC_ST_IDT_SCAN_NUM )->SetFont(&m_DspNumFont);
	GetDlgItem( IDC_ST_IDT_SCAN_IMAGE_INDEX)->SetFont(&m_DspNumFont);
	GetDlgItem( IDC_ST_IDT_SAVE_ONLY)->SetFont(&m_DspNumFont);

	GetDlgItem(IDC_ST_CUTTER_PROC1)->SetFont(&m_DspNumFont);
	GetDlgItem(IDC_ST_CUTTER_PROC2)->SetFont(&m_DspNumFont);
	GetDlgItem(IDC_ST_CUTTER_PROC3)->SetFont(&m_DspNumFont);
	GetDlgItem(IDC_ST_CUTTER_PROC4)->SetFont(&m_DspNumFont);

	GetDlgItem(IDC_ST_RESULT_PROC1)->SetFont(&m_DspNumFont);
	GetDlgItem(IDC_ST_RESULT_PROC2)->SetFont(&m_DspNumFont);
	GetDlgItem(IDC_ST_RESULT_PROC3)->SetFont(&m_DspNumFont);
	GetDlgItem(IDC_ST_RESULT_PROC4)->SetFont(&m_DspNumFont);

	GetDlgItem(IDC_ST_BUF_DISK_TOTAL_SIZE)->SetFont(&m_DspNumFont);
	GetDlgItem(IDC_ST_BUF_DISK_FREE_SIZE)->SetFont(&m_DspNumFont);
	GetDlgItem(IDC_ST_BUF_DISK_USE_SIZE)->SetFont(&m_DspNumFont);
}
void CVS11AOIIDlg::AOI_ClearIndicator(void)
{
	DWORD ClearColor = ::GetSysColor(COLOR_BTNTEXT);
	m_stTaskState.StartTextBlink(FALSE, CColorStaticST::ST_FLS_FAST);
	m_stTaskState.SetBlinkTextColors(ClearColor,ClearColor);
	ClearColor = ::GetSysColor(COLOR_BTNFACE);
	m_stTaskState.SetBlinkBkColors(ClearColor,ClearColor);
}

//CRect G_MainWinRect;
//CRect G_ClentWinRect;
//int G_SizeWindEdge = GetSystemMetrics(SM_CXDLGFRAME)*2+(GetSystemMetrics(SM_CXEDGE))+(GetSystemMetrics(SM_CXBORDER));
void CVS11AOIIDlg::OnMove(int x, int y)
{
	__super::OnMove(x, y);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}
void CVS11AOIIDlg::WriteLogInspectionParam()
{

	CString strNewData;
	strNewData.Format(" Active m_stMaskImageInfo");
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX1: %d",m_stMaskImageInfo.nScaleX1);    
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleY1: %d",m_stMaskImageInfo.nScaleY1);   
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX2: %d",m_stMaskImageInfo.nScaleX2);       
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleY2: %d",m_stMaskImageInfo.nScaleY2);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX3: %d",m_stMaskImageInfo.nScaleX3);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleY3: %d",m_stMaskImageInfo.nScaleY3);    
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeMain: %d",m_stMaskImageInfo.nMinRangeMain);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeMain: %d",m_stMaskImageInfo.nMaxRangeMain);  
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeLPad: %d",m_stMaskImageInfo.nMinRangeLPad);  
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeLPad: %d",m_stMaskImageInfo.nMaxRangeLPad);   
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeRPad: %d",m_stMaskImageInfo.nMinRangeRPad);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeRPad: %d",m_stMaskImageInfo.nMaxRangeRPad);  
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineLeft: %d",m_stMaskImageInfo.nOutLineLeft);        
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineRight: %d",m_stMaskImageInfo.nOutLineRight);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineUp: %d",m_stMaskImageInfo.nOutLineUp);          
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineDown: %d",m_stMaskImageInfo.nOutLineDown);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nBlockCount: %d",m_stMaskImageInfo.nBlockCount);         
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nThreadCount: %d",m_stMaskImageInfo.nThreadCount);       
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeX: %d",m_stMaskImageInfo.nMinimumSizeX);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeY: %d",m_stMaskImageInfo.nMinimumSizeY);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeX: %d",m_stMaskImageInfo.nMaximumSizeX);   
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeY: %d",m_stMaskImageInfo.nMaximumSizeY);  
	m_listInspectParam.AddString(strNewData);

	strNewData.Format(" Active m_stCommonPara");
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nDefectDistance: %d",m_stCommonPara.nDefectDistance);		
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeX: %d",m_stCommonPara.nImageSizeX);			
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeY: %d",m_stCommonPara.nImageSizeY);			
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaskType: %d",m_stCommonPara.nMaskType);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[0]: %d",m_stCommonPara.nZoneStep[0]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[1]: %d",m_stCommonPara.nZoneStep[1]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[2]: %d",m_stCommonPara.nZoneStep[2]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[3]: %d",m_stCommonPara.nZoneStep[3]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[4]: %d",m_stCommonPara.nZoneStep[4]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[0]: %d",m_stCommonPara.nZoneOffset[0]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[1]: %d",m_stCommonPara.nZoneOffset[1]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[2]: %d",m_stCommonPara.nZoneOffset[2]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[3]: %d",m_stCommonPara.nZoneOffset[3]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[4]: %d",m_stCommonPara.nZoneOffset[4]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     bImageLogUsed: %d",m_stCommonPara.bImageLogUsed);
	m_listInspectParam.AddString(strNewData);

	strNewData.Format(" Pad Up m_stMaskImageInfo");
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX1: %d",m_stPADMaskImageInfo[0].nScaleX1);   
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleY1: %d",m_stPADMaskImageInfo[0].nScaleY1);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX2: %d",m_stPADMaskImageInfo[0].nScaleX2);    
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleY2: %d",m_stPADMaskImageInfo[0].nScaleY2);   
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX3: %d",m_stPADMaskImageInfo[0].nScaleX3);         
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleY3: %d",m_stPADMaskImageInfo[0].nScaleY3);    
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeMain: %d",m_stPADMaskImageInfo[0].nMinRangeMain);   
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeMain: %d",m_stPADMaskImageInfo[0].nMaxRangeMain);    
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeLPad: %d",m_stPADMaskImageInfo[0].nMinRangeLPad); 
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeLPad: %d",m_stPADMaskImageInfo[0].nMaxRangeLPad);  
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeRPad: %d",m_stPADMaskImageInfo[0].nMinRangeRPad);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeRPad: %d",m_stPADMaskImageInfo[0].nMaxRangeRPad);    
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineLeft: %d",m_stPADMaskImageInfo[0].nOutLineLeft);        
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineRight: %d",m_stPADMaskImageInfo[0].nOutLineRight);       
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineUp: %d",m_stPADMaskImageInfo[0].nOutLineUp);          
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineDown: %d",m_stPADMaskImageInfo[0].nOutLineDown);      
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nBlockCount: %d",m_stPADMaskImageInfo[0].nBlockCount);        
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nThreadCount: %d",m_stPADMaskImageInfo[0].nThreadCount);   
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeX: %d",m_stPADMaskImageInfo[0].nMinimumSizeX);   
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeY: %d",m_stPADMaskImageInfo[0].nMinimumSizeY);  
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeX: %d",m_stPADMaskImageInfo[0].nMaximumSizeX);     
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeY: %d",m_stPADMaskImageInfo[0].nMaximumSizeY);     
	m_listInspectParam.AddString(strNewData);

	strNewData.Format(" Pad Up m_stCommonPara");
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nDefectDistance: %d", m_stPADCommonPara[0].nDefectDistance);	
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeX: %d",m_stPADCommonPara[0].nImageSizeX);		
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeY: %d",m_stPADCommonPara[0].nImageSizeY);			
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaskType: %d",m_stPADCommonPara[0].nMaskType);			
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[0]: %d",m_stPADCommonPara[0].nZoneStep[0]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[1]: %d",m_stPADCommonPara[0].nZoneStep[1]);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[2]: %d",m_stPADCommonPara[0].nZoneStep[2]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[3]: %d",m_stPADCommonPara[0].nZoneStep[3]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[4]: %d",m_stPADCommonPara[0].nZoneStep[4]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[0]: %d",m_stPADCommonPara[0].nZoneOffset[0]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[1]: %d",m_stPADCommonPara[0].nZoneOffset[1]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[2]: %d",m_stPADCommonPara[0].nZoneOffset[2]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[3]: %d",m_stPADCommonPara[0].nZoneOffset[3]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[4]: %d",m_stPADCommonPara[0].nZoneOffset[4]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     bImageLogUsed: %d",m_stPADCommonPara[0].bImageLogUsed);	m_listInspectParam.AddString(strNewData);	

	strNewData.Format(" Pad Down m_stMaskImageInfo");m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX1: %d",m_stPADMaskImageInfo[1].nScaleX1);      m_listInspectParam.AddString(strNewData);      
	strNewData.Format("     nScaleY1: %d",m_stPADMaskImageInfo[1].nScaleY1);     m_listInspectParam.AddString(strNewData);        
	strNewData.Format("     nScaleX2: %d",m_stPADMaskImageInfo[1].nScaleX2);      m_listInspectParam.AddString(strNewData);       
	strNewData.Format("     nScaleY2: %d",m_stPADMaskImageInfo[1].nScaleY2);       m_listInspectParam.AddString(strNewData);      
	strNewData.Format("     nScaleX3: %d",m_stPADMaskImageInfo[1].nScaleX3);       m_listInspectParam.AddString(strNewData);      
	strNewData.Format("     nScaleY3: %d",m_stPADMaskImageInfo[1].nScaleY3);          m_listInspectParam.AddString(strNewData);   
	strNewData.Format("     nMinRangeMain: %d",m_stPADMaskImageInfo[1].nMinRangeMain);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeMain: %d",m_stPADMaskImageInfo[1].nMaxRangeMain);    m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeLPad: %d",m_stPADMaskImageInfo[1].nMinRangeLPad);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeLPad: %d",m_stPADMaskImageInfo[1].nMaxRangeLPad);    m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeRPad: %d",m_stPADMaskImageInfo[1].nMinRangeRPad);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeRPad: %d",m_stPADMaskImageInfo[1].nMaxRangeRPad);    m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineLeft: %d",m_stPADMaskImageInfo[1].nOutLineLeft);        m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineRight: %d",m_stPADMaskImageInfo[1].nOutLineRight);       m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineUp: %d",m_stPADMaskImageInfo[1].nOutLineUp);          m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineDown: %d",m_stPADMaskImageInfo[1].nOutLineDown);      m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nBlockCount: %d",m_stPADMaskImageInfo[1].nBlockCount);         m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nThreadCount: %d",m_stPADMaskImageInfo[1].nThreadCount);       m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeX: %d",m_stPADMaskImageInfo[1].nMinimumSizeX);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeY: %d",m_stPADMaskImageInfo[1].nMinimumSizeY);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeX: %d",m_stPADMaskImageInfo[1].nMaximumSizeX);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeY: %d",m_stPADMaskImageInfo[1].nMaximumSizeY);     m_listInspectParam.AddString(strNewData);

	strNewData.Format(" Pad Down m_stCommonPara");m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nDefectDistance: %d",m_stPADCommonPara[1].nDefectDistance);		m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeX: %d",m_stPADCommonPara[1].nImageSizeX);		m_listInspectParam.AddString(strNewData);	
	strNewData.Format("     nImageSizeY: %d",m_stPADCommonPara[1].nImageSizeY);		m_listInspectParam.AddString(strNewData);	
	strNewData.Format("     nMaskType: %d",m_stPADCommonPara[1].nMaskType);			m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[0]: %d",m_stPADCommonPara[1].nZoneStep[0]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[1]: %d",m_stPADCommonPara[1].nZoneStep[1]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[2]: %d",m_stPADCommonPara[1].nZoneStep[2]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[3]: %d",m_stPADCommonPara[1].nZoneStep[3]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[4]: %d",m_stPADCommonPara[1].nZoneStep[4]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[0]: %d",m_stPADCommonPara[1].nZoneOffset[0]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[1]: %d",m_stPADCommonPara[1].nZoneOffset[1]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[2]: %d",m_stPADCommonPara[1].nZoneOffset[2]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[3]: %d",m_stPADCommonPara[1].nZoneOffset[3]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[4]: %d",m_stPADCommonPara[1].nZoneOffset[4]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     bImageLogUsed: %d",m_stPADCommonPara[1].bImageLogUsed);	m_listInspectParam.AddString(strNewData);	

	strNewData.Format("Pad Left m_stMaskImageInfo");m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX1: %d",m_stPADMaskImageInfo[2].nScaleX1);   m_listInspectParam.AddString(strNewData);         
	strNewData.Format("     nScaleY1: %d",m_stPADMaskImageInfo[2].nScaleY1);     m_listInspectParam.AddString(strNewData);        
	strNewData.Format("     nScaleX2: %d",m_stPADMaskImageInfo[2].nScaleX2);  m_listInspectParam.AddString(strNewData);           
	strNewData.Format("     nScaleY2: %d",m_stPADMaskImageInfo[2].nScaleY2);    m_listInspectParam.AddString(strNewData);         
	strNewData.Format("     nScaleX3: %d",m_stPADMaskImageInfo[2].nScaleX3);   m_listInspectParam.AddString(strNewData);          
	strNewData.Format("     nScaleY3: %d",m_stPADMaskImageInfo[2].nScaleY3);  m_listInspectParam.AddString(strNewData);           
	strNewData.Format("     nMinRangeMain: %d",m_stPADMaskImageInfo[2].nMinRangeMain);m_listInspectParam.AddString(strNewData);     
	strNewData.Format("     nMaxRangeMain: %d",m_stPADMaskImageInfo[2].nMaxRangeMain);   m_listInspectParam.AddString(strNewData); 
	strNewData.Format("     nMinRangeLPad: %d",m_stPADMaskImageInfo[2].nMinRangeLPad);    m_listInspectParam.AddString(strNewData); 
	strNewData.Format("     nMaxRangeLPad: %d",m_stPADMaskImageInfo[2].nMaxRangeLPad);   m_listInspectParam.AddString(strNewData); 
	strNewData.Format("     nMinRangeRPad: %d",m_stPADMaskImageInfo[2].nMinRangeRPad);    m_listInspectParam.AddString(strNewData); 
	strNewData.Format("     nMaxRangeRPad: %d",m_stPADMaskImageInfo[2].nMaxRangeRPad);    m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineLeft: %d",m_stPADMaskImageInfo[2].nOutLineLeft);      m_listInspectParam.AddString(strNewData);  
	strNewData.Format("     nOutLineRight: %d",m_stPADMaskImageInfo[2].nOutLineRight);      m_listInspectParam.AddString(strNewData); 
	strNewData.Format("     nOutLineUp: %d",m_stPADMaskImageInfo[2].nOutLineUp);          m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineDown: %d",m_stPADMaskImageInfo[2].nOutLineDown);      m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nBlockCount: %d",m_stPADMaskImageInfo[2].nBlockCount);         m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nThreadCount: %d",m_stPADMaskImageInfo[2].nThreadCount);     m_listInspectParam.AddString(strNewData);  
	strNewData.Format("     nMinimumSizeX: %d",m_stPADMaskImageInfo[2].nMinimumSizeX);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeY: %d",m_stPADMaskImageInfo[2].nMinimumSizeY);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeX: %d",m_stPADMaskImageInfo[2].nMaximumSizeX);   m_listInspectParam.AddString(strNewData);  
	strNewData.Format("     nMaximumSizeY: %d",m_stPADMaskImageInfo[2].nMaximumSizeY);    m_listInspectParam.AddString(strNewData); 

	strNewData.Format(" Pad Left m_stCommonPara");m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nDefectDistance: %d",m_stPADCommonPara[2].nDefectDistance);		m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeX: %d",m_stPADCommonPara[2].nImageSizeX);			m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeY: %d",m_stPADCommonPara[2].nImageSizeY);		m_listInspectParam.AddString(strNewData);	
	strNewData.Format("     nMaskType: %d",m_stPADCommonPara[2].nMaskType);			m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[0]: %d",m_stPADCommonPara[2].nZoneStep[0]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[1]: %d",m_stPADCommonPara[2].nZoneStep[1]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[2]: %d",m_stPADCommonPara[2].nZoneStep[2]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[3]: %d",m_stPADCommonPara[2].nZoneStep[3]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[4]: %d",m_stPADCommonPara[2].nZoneStep[4]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[0]: %d",m_stPADCommonPara[2].nZoneOffset[0]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[1]: %d",m_stPADCommonPara[2].nZoneOffset[1]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[2]: %d",m_stPADCommonPara[2].nZoneOffset[2]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[3]: %d",m_stPADCommonPara[2].nZoneOffset[3]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[4]: %d",m_stPADCommonPara[2].nZoneOffset[4]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     bImageLogUsed: %d",m_stPADCommonPara[2].bImageLogUsed);	m_listInspectParam.AddString(strNewData);	

	strNewData.Format(" Pad Right m_stMaskImageInfo");m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nScaleX1: %d",m_stPADMaskImageInfo[3].nScaleX1);     m_listInspectParam.AddString(strNewData);       
	strNewData.Format("     nScaleY1: %d",m_stPADMaskImageInfo[3].nScaleY1);       m_listInspectParam.AddString(strNewData);      
	strNewData.Format("     nScaleX2: %d",m_stPADMaskImageInfo[3].nScaleX2);     m_listInspectParam.AddString(strNewData);        
	strNewData.Format("     nScaleY2: %d",m_stPADMaskImageInfo[3].nScaleY2);        m_listInspectParam.AddString(strNewData);     
	strNewData.Format("     nScaleX3: %d",m_stPADMaskImageInfo[3].nScaleX3);      m_listInspectParam.AddString(strNewData);       
	strNewData.Format("     nScaleY3: %d",m_stPADMaskImageInfo[3].nScaleY3);      m_listInspectParam.AddString(strNewData);       
	strNewData.Format("     nMinRangeMain: %d",m_stPADMaskImageInfo[3].nMinRangeMain);   m_listInspectParam.AddString(strNewData);  
	strNewData.Format("     nMaxRangeMain: %d",m_stPADMaskImageInfo[3].nMaxRangeMain);    m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeLPad: %d",m_stPADMaskImageInfo[3].nMinRangeLPad);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeLPad: %d",m_stPADMaskImageInfo[3].nMaxRangeLPad);    m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinRangeRPad: %d",m_stPADMaskImageInfo[3].nMinRangeRPad);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaxRangeRPad: %d",m_stPADMaskImageInfo[3].nMaxRangeRPad);    m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineLeft: %d",m_stPADMaskImageInfo[3].nOutLineLeft);        m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineRight: %d",m_stPADMaskImageInfo[3].nOutLineRight);       m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineUp: %d",m_stPADMaskImageInfo[3].nOutLineUp);          m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nOutLineDown: %d",m_stPADMaskImageInfo[3].nOutLineDown);      m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nBlockCount: %d",m_stPADMaskImageInfo[3].nBlockCount);         m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nThreadCount: %d",m_stPADMaskImageInfo[3].nThreadCount);       m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeX: %d",m_stPADMaskImageInfo[3].nMinimumSizeX);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMinimumSizeY: %d",m_stPADMaskImageInfo[3].nMinimumSizeY);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeX: %d",m_stPADMaskImageInfo[3].nMaximumSizeX);     m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaximumSizeY: %d",m_stPADMaskImageInfo[3].nMaximumSizeY);     m_listInspectParam.AddString(strNewData);

	strNewData.Format("Pad Right m_stCommonPara");m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nDefectDistance: %d",m_stPADCommonPara[3].nDefectDistance);		m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeX: %d",m_stPADCommonPara[3].nImageSizeX);			m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nImageSizeY: %d",m_stPADCommonPara[3].nImageSizeY);			m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nMaskType: %d",m_stPADCommonPara[3].nMaskType);			m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[0]: %d",m_stPADCommonPara[3].nZoneStep[0]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[1]: %d",m_stPADCommonPara[3].nZoneStep[1]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[2]: %d",m_stPADCommonPara[3].nZoneStep[2]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[3]: %d",m_stPADCommonPara[3].nZoneStep[3]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneStep[4]: %d",m_stPADCommonPara[3].nZoneStep[4]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[0]: %d",m_stPADCommonPara[3].nZoneOffset[0]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[1]: %d",m_stPADCommonPara[3].nZoneOffset[1]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[2]: %d",m_stPADCommonPara[3].nZoneOffset[2]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[3]: %d",m_stPADCommonPara[3].nZoneOffset[3]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     nZoneOffset[4]: %d",m_stPADCommonPara[3].nZoneOffset[4]);m_listInspectParam.AddString(strNewData);
	strNewData.Format("     bImageLogUsed: %d",m_stPADCommonPara[3].bImageLogUsed);		m_listInspectParam.AddString(strNewData);

}

void CVS11AOIIDlg::UpdateInspectParam(SAOI_IMAGE_INFO *pNewdata1, SAOI_COMMON_PARA*pNewData2)
{
	CString strNewData;
	m_listInspectParam.ResetContent();

	strNewData.Format("Scale X : %d", pNewdata1->nScaleX1);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Scale Y : %d", pNewdata1->nScaleY1);
	m_listInspectParam.AddString(strNewData);


	//strNewData.Format("Range(Min) : %d", pNewdata1->nMinRange);
	//m_listInspectParam.AddString(strNewData);
	//strNewData.Format("Range(Max) : %d", pNewdata1->nMaxRange);
	//m_listInspectParam.AddString(strNewData);

	strNewData.Format("Range(Min) : %d", pNewdata1->nMinRangeMain);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Range(Max) : %d", pNewdata1->nMaxRangeMain);
	m_listInspectParam.AddString(strNewData);

	//strNewData.Format("Range Step1 : %d", pNewdata1->nRangeStep1);
	//m_listInspectParam.AddString(strNewData);
	//strNewData.Format("Range Step2 : %d", pNewdata1->nRangeStep2);
	//m_listInspectParam.AddString(strNewData);
	//strNewData.Format("Range Step3 : %d", pNewdata1->nRangeStep3);
	//m_listInspectParam.AddString(strNewData);
	//strNewData.Format("Range Zone1 : %d", pNewdata1->nRangeZone1);
	//m_listInspectParam.AddString(strNewData);
	//strNewData.Format("Range Zone2 : %d", pNewdata1->nRangeZone2);
	//m_listInspectParam.AddString(strNewData);

	strNewData.Format("Out Line Left : %d", pNewdata1->nOutLineLeft);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Out Line Right : %d", pNewdata1->nOutLineRight);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Out Line Up : %d", pNewdata1->nOutLineUp);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Out Line Down : %d", pNewdata1->nOutLineDown);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Minimum Size X : %d", pNewdata1->nMinimumSizeX);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Minimum Size Y : %d", pNewdata1->nMinimumSizeY);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Maximum Size X : %d", pNewdata1->nMaximumSizeX);
	m_listInspectParam.AddString(strNewData);
	strNewData.Format("Maximum Size Y : %d", pNewdata1->nMaximumSizeY);
	m_listInspectParam.AddString(strNewData);

	strNewData.Format("Mask Type : %d", pNewData2->nMaskType);
	m_listInspectParam.AddString(strNewData);


	for(int ZoneStep = 0; ZoneStep<5; ZoneStep++)
	{
		strNewData.Format("Lens Zone %d: %d",ZoneStep+1, pNewData2->nZoneStep[ZoneStep]);
		m_listInspectParam.AddString(strNewData);
	}
	for(int ZoneStep = 0; ZoneStep<5; ZoneStep++)
	{
		strNewData.Format("Zone Offset %d: %d",ZoneStep+1, pNewData2->nZoneOffset[ZoneStep]);
		m_listInspectParam.AddString(strNewData);
	}

	switch(G_CoordProcObj.m_HWParam.PadInspectAreaType)
	{
	case INSPACT_PAD_SKIP:
		{

		}break;
	
	case INSPACT_PAD_UPDOWN://Up/Down은 Pad연산 좌우는 Active연산.
	case INSPACT_PAD_LEFTRIGHT:
		{
			strNewData.Format("Active Pad Param");
			m_listInspectParam.AddString(strNewData);

			strNewData.Format("  Left X Scale: %d",m_stMaskImageInfo.nScaleX2);
			m_listInspectParam.AddString(strNewData);
			strNewData.Format("  Left Y Scale: %d",m_stMaskImageInfo.nScaleY2);
			m_listInspectParam.AddString(strNewData);

			strNewData.Format("  Left Range(Min) : %d", m_stMaskImageInfo.nMinRangeLPad);
			m_listInspectParam.AddString(strNewData);
			strNewData.Format("  Left Range(Max) : %d", m_stMaskImageInfo.nMaxRangeLPad);
			m_listInspectParam.AddString(strNewData);

			strNewData.Format("");
			m_listInspectParam.AddString(strNewData);

			strNewData.Format("  Right X Scale: %d",m_stMaskImageInfo.nScaleX3);
			m_listInspectParam.AddString(strNewData);
			strNewData.Format("  Right Y Scale: %d",m_stMaskImageInfo.nScaleY3);
			m_listInspectParam.AddString(strNewData);

			strNewData.Format("  Right Range(Min) : %d", m_stMaskImageInfo.nMinRangeRPad	);
			m_listInspectParam.AddString(strNewData);
			strNewData.Format("  Right Range(Max) : %d", m_stMaskImageInfo.nMaxRangeRPad);
			m_listInspectParam.AddString(strNewData);

			strNewData.Format(" Up Range(Min) : %d", m_stPADMaskImageInfo[0].nMinRangeMain);
			m_listInspectParam.AddString(strNewData);
			strNewData.Format(" Up Range(Max) : %d", m_stPADMaskImageInfo[0].nMaxRangeMain);
			m_listInspectParam.AddString(strNewData);

			strNewData.Format(" Dn Range(Min) : %d", m_stPADMaskImageInfo[1].nMinRangeMain);
			m_listInspectParam.AddString(strNewData);
			strNewData.Format(" Dn Range(Max) : %d", m_stPADMaskImageInfo[1].nMaxRangeMain);
			m_listInspectParam.AddString(strNewData);

		}break;
	case INSPACT_PAD_BOTH:
		{
		}break;
	}

	
}
void CVS11AOIIDlg::UpdateGlassParam(GLASS_PARAM *pNewData)
{
	CString strNewData;
	m_listGlassParam.ResetContent();

	strNewData.Format("Glass Name : %s", pNewData->m_GlassName);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("");
	m_listGlassParam.AddString(strNewData);
	//////////////////////////////////////////////////////////////////////////
	strNewData.Format("Scan Line Shift Distance : %0.3f mm", pNewData->m_ScanLineShiftDistance/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("Scan Img Resolution_W : %0.3f um", pNewData->m_ScanImgResolution_W);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("Scan Img Resolution_H : %0.3f um", pNewData->m_ScanImgResolution_H);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("");
	m_listGlassParam.AddString(strNewData);
	//////////////////////////////////////////////////////////////////////////
	//Glass Center Position으로 Machine좌표를 입력 한다.
	strNewData.Format("Center Point X : %0.3f mm", pNewData->CenterPointX/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("Center Point Y : %0.3f mm", pNewData->CenterPointY/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("PAD Area");
	m_listGlassParam.AddString(strNewData);
	//////////////////////////////////////////////////////////////////////////
	strNewData.Format("  1)Start Distance X : %0.3f mm", pNewData->PadStartDistanceX/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("  2)Start Distance Y : %0.3f mm", pNewData->PadStartDistanceY/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("  3)End Distance X : %0.3f mm", pNewData->PadEndDistanceX/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("  4)End Distance Y : %0.3f mm", pNewData->PadEndDistanceY/1000.0f);
	m_listGlassParam.AddString(strNewData);

	strNewData.Format("Active Dead Zone");
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("    Size(U,D,L,F) : %0.3f mm", pNewData->ActiveDeadZoneSize/1000.0f);
	m_listGlassParam.AddString(strNewData);

	strNewData.Format("PAD Dead Zone");
	m_listGlassParam.AddString(strNewData);
	//////////////////////////////////////////////////////////////////////////
	strNewData.Format("  1)Pad Start X : %0.3f mm", pNewData->PadStartDeadZoneDisX/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("  2)Pad Start Y : %0.3f mm", pNewData->PadStartDeadZoneDisY/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("  3)Pad End X : %0.3f mm", pNewData->PadEndDeadZoneDisX/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("  4)Pad End Y : %0.3f mm", pNewData->PadEndDeadZoneDisY/1000.0f);
	m_listGlassParam.AddString(strNewData);


	strNewData.Format("  5)Pad Down Height : %0.3f mm", pNewData->PadDnDeadZoneHeight/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("  6)Pad Down Width : %0.3f mm", pNewData->PadDnDeadZoneWidth/1000.0f);
	m_listGlassParam.AddString(strNewData);

	strNewData.Format("  7)Active Pad Left/Right Height : %0.3f mm", pNewData->ActivePadDeadZone/1000.0f);
	m_listGlassParam.AddString(strNewData);

	//////////////////////////////////////////////////////////////////////////
	strNewData.Format("Glass Info");
	m_listGlassParam.AddString(strNewData);
	
	//Center Point (0,0)을 기준으로 거리 Offset이다.(설계치는 mm단위이고 입력은 um로 한다.)
	strNewData.Format("CELL_EDGE_X : %0.3f mm", pNewData->CELL_EDGE_X/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("CELL_EDGE_Y : %0.3f mm", pNewData->CELL_EDGE_Y/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("CELL_DISTANCE_X : %0.3f mm", pNewData->CELL_DISTANCE_X/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("CELL_DISTANCE_Y : %0.3f mm", pNewData->CELL_DISTANCE_Y/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("CELL_SIZE_X : %0.3f mm", pNewData->CELL_SIZE_X/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("CELL_SIZE_Y : %0.3f mm", pNewData->CELL_SIZE_Y/1000.0f);
	m_listGlassParam.AddString(strNewData);

	//축 기준으로 몇개의 Cell이 있는지 나타 낸다.
	strNewData.Format("GLASS_CELL_COUNT_X : %d", pNewData->GLASS_CELL_COUNT_X);
	m_listGlassParam.AddString(strNewData);		
	strNewData.Format("GLASS_CELL_COUNT_Y : %d", pNewData->GLASS_CELL_COUNT_Y);
	m_listGlassParam.AddString(strNewData);		

	//Block 갯수를 나타낸다.
	strNewData.Format("BLOCK_DISTANCE_X : %0.3f mm", pNewData->BLOCK_DISTANCE_X/1000.0f);
	m_listGlassParam.AddString(strNewData);
	strNewData.Format("BLOCK_DISTANCE_Y : %0.3f mm", pNewData->BLOCK_DISTANCE_Y/1000.0f);
	m_listGlassParam.AddString(strNewData);

	//축 기준으로 몇개의 Block이 있는지 나타 낸다.
	strNewData.Format("GLASS_BLOCK_COUNT_X : %d ", pNewData->GLASS_BLOCK_COUNT_X);
	m_listGlassParam.AddString(strNewData);		
	strNewData.Format("GLASS_BLOCK_COUNT_Y : %d ", pNewData->GLASS_BLOCK_COUNT_Y);
	m_listGlassParam.AddString(strNewData);	
}
void CVS11AOIIDlg::UpdateHWParam(HW_PARAM *pNewData)
{
	CString strNewData;
	m_listHWParam.ResetContent();


	strNewData.Format("VSB Size : %d ",pNewData->m_VSB_Size);
	m_listHWParam.AddString(strNewData);

	strNewData.Format("Grab Image W : %d Pix",pNewData->m_ProcImageW);
	m_listHWParam.AddString(strNewData);
	strNewData.Format("Grab Image H : %d Pix",pNewData->m_ProcImageH);
	m_listHWParam.AddString(strNewData);

	strNewData.Format("Defect Max Cnt In Img : %d ",pNewData->m_DefectMaxCntInImg);
	m_listHWParam.AddString(strNewData);							//<<< Process Image당 Max Defect의 갯수.
	strNewData.Format("Defect Review Size W : %d Pix",pNewData->m_DefectReviewSizeW);
	m_listHWParam.AddString(strNewData);						//<<< Defect당 Review용 이미지의 넓이
	strNewData.Format("Defect Review Size H : %d Pix",pNewData->m_DefectReviewSizeH);
	m_listHWParam.AddString(strNewData);						//<<< Defect당 Review용 이미지의 높이

	strNewData.Format("INSPECTION_CAM_COUNT : %d ",pNewData->m_INSPECTION_CAM_COUNT);
	m_listHWParam.AddString(strNewData);					//<<< System에 적용될 AOI Camera Count

	strNewData.Format("INSPECTION_SCAN_WIDTH : %0.3f mm",pNewData->m_INSPECTION_SCAN_WIDTH/1000.0f);
	m_listHWParam.AddString(strNewData);				//<<< Scan할 Total 넓이.(um단위)
	strNewData.Format("INSPECTION_SCAN_HEIGHT : %0.3f mm",pNewData->m_INSPECTION_SCAN_HEIGHT/1000.0f);
	m_listHWParam.AddString(strNewData);				//<<< Scan할  Total 높이.(um단위)

	strNewData.Format("AOI_BLOCK_SCAN_START_X : %0.3f mm",pNewData->m_AOI_BLOCK_SCAN_START_X/1000.0f);
	m_listHWParam.AddString(strNewData);			//<<<기준점.기준의 Block Scan이 시작되는 위치.(um단위)
	strNewData.Format("AOI_BLOCK_SCAN_START_Y : %0.3f mm",pNewData->m_AOI_BLOCK_SCAN_START_Y/1000.0f);
	m_listHWParam.AddString(strNewData);			//<<<기준점. Cam기준의 Block Scan이 시작되는 위치.(um단위)

	//////////////////////////////////////////////////////////////////////////
	//Inspect Camera 별.
	strNewData.Format("AOI_CAM_DISTANCE_X : %0.3f mm",pNewData->m_AOI_CAM_DISTANCE_X/1000.0f);
	m_listHWParam.AddString(strNewData);					//<<< 기준점. Cam Center에서 AOI Cam과의 거리.(um단위)
	strNewData.Format("AOI_CAM_DISTANCE_Y : %0.3f mm",pNewData->m_AOI_CAM_DISTANCE_Y/1000.0f);
	m_listHWParam.AddString(strNewData);				//<<< 기준점. Cam Center에서 AOI Cam과의 거리.(um단위)

	strNewData.Format("AOI_SCAN_START_OFFSET_ODD : %0.3f mm",pNewData->m_AOI_SCAN_START_OFFSET_ODD/1000.0f);
	m_listHWParam.AddString(strNewData);	//<<Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(홀수 Scan시 적용)(um단위)
	strNewData.Format("AOI_SCAN_START_OFFSET_EVEN : %0.3f mm",pNewData->m_AOI_SCAN_START_OFFSET_EVEN/1000.0f);
	m_listHWParam.AddString(strNewData);	//<<Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(짝수 Scan시 적용)(um단위)

	//////////////////////////////////////////////////////////////////////////
	strNewData.Format("Only Grab Image Save : %d",pNewData->m_OnlyGrabImageSave);
	m_listHWParam.AddString(strNewData);
	//strNewData.Format("Save Defect Org Image : %d",pNewData->m_SaveDefectOrgImage);
	//m_listHWParam.AddString(strNewData);
	strNewData.Format("Defect Save File Type  : %d",pNewData->DefectSaveFileType);
	m_listHWParam.AddString(strNewData);

	strNewData.Format("Pad Inspect Area Type  : %d",pNewData->PadInspectAreaType);
	m_listHWParam.AddString(strNewData);						//<<1: 상하, 2:좌우. 3:양쪽다.

	strNewData.Format("Cam Scan Direction Port  : %d",pNewData->CamScanDirectionPort);
	m_listHWParam.AddString(strNewData);						//Camera Scan Direction을 바꾸기위한 Serial Port
	//////////////////////////////////////////////////////////////////////////
	strNewData.Format("str Result Path :%s ",pNewData->strResultPath);
	m_listHWParam.AddString(strNewData);
	strNewData.Format("str Org Img Path :%s",pNewData->strOrgImgPath);
	m_listHWParam.AddString(strNewData);
	


}

void CVS11AOIIDlg::OnBnClickedBtEditHwparam()
{
	m_pDlgEditHWParam->ShowWindow(SW_SHOW);
}




void CVS11AOIIDlg::OnBnClickedBtShowFolder()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString AOIDataPath;
	UpdateData(TRUE);
	AOIDataPath.Format("explorer %s", G_CoordProcObj.m_HWParam.strResultPath);
	::WinExec(AOIDataPath, 1);
}


void CVS11AOIIDlg::OnBnClickedChkSendT75()
{
	//if(IsDlgButtonChecked(IDC_CHK_SEND_T75) == TRUE)
	//	G_ProcessState.bTEST_VS75_REVIEW_SEND = true;
	//else
	//	G_ProcessState.bTEST_VS75_REVIEW_SEND= false;
}

//#define  Section_Data_Options		"Data_Options"
//#define  Key_SavePadCutDrawImg	"SavePadCutDrawImg"
//#define  Key_SaveInspectImage		"SaveInspectImage"
//#define  Key_SaveSmallImage			"SaveSmallImage"
//#define  Key_DrawDefectArea		"DrawDefectArea"
//#define  Key_SaveActiveAreaHistogram	"SaveActiveAreaHistogram"


void CVS11AOIIDlg::OnBnClickedChkSaveCutPadDraw()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_CUT_PAD_DRAW) == TRUE)
		G_ProcessState.bTEST_SAVE_CUT_AREA_DRAW = true;
	else
		G_ProcessState.bTEST_SAVE_CUT_AREA_DRAW= false;


	CString newWriteData;
	newWriteData.Format("%s",G_ProcessState.bTEST_SAVE_CUT_AREA_DRAW==true?"1":"0");
	WritePrivateProfileString(Section_Data_Options, Key_SavePadCutDrawImg, newWriteData, m_IniFilePath);
}


void CVS11AOIIDlg::OnBnClickedChkSaveSmallImg()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_SMALL_IMG) == TRUE)
	{
		G_ProcessState.bTEST_DEFECT_IMG_SAVE_JPG = true;
		GetDlgItem(IDC_RDSAVE_BMP)->EnableWindow(true);
		GetDlgItem(IDC_RDSAVE_JPG)->EnableWindow(true);
	}
	else
	{
		G_ProcessState.bTEST_DEFECT_IMG_SAVE_JPG= false;
		GetDlgItem(IDC_RDSAVE_BMP)->EnableWindow(false);
		GetDlgItem(IDC_RDSAVE_JPG)->EnableWindow(false);
	}

	CString newWriteData;
	newWriteData.Format("%s",G_ProcessState.bTEST_DEFECT_IMG_SAVE_JPG==true?"1":"0");
	WritePrivateProfileString(Section_Data_Options, Key_SaveSmallImage, newWriteData, m_IniFilePath);
}
void CVS11AOIIDlg::OnBnClickedRdsaveBmp()
{
	
	//if(IsDlgButtonChecked(IDC_RDSAVE_BMP) == TRUE)
		//G_ProcessState.bTEST_DEFECT_IMG_BMPorJPG = true;//BMP format
}


void CVS11AOIIDlg::OnBnClickedRdsaveJpg()
{
	//if(IsDlgButtonChecked(IDC_RDSAVE_JPG) == TRUE)
		//G_ProcessState.bTEST_DEFECT_IMG_BMPorJPG = false;//JPG format

}


void CVS11AOIIDlg::OnBnClickedChkSaveInspectImg()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_INSPECT_IMG) == TRUE)
		G_ProcessState.bTEST_INSPECT_IMG = true;
	else
		G_ProcessState.bTEST_INSPECT_IMG= false;

	CString newWriteData;
	newWriteData.Format("%s",G_ProcessState.bTEST_INSPECT_IMG==true?"1":"0");
	WritePrivateProfileString(Section_Data_Options, Key_SaveInspectImage, newWriteData, m_IniFilePath);
}


void CVS11AOIIDlg::OnBnClickedChkDrawDefctArea()
{

	//if(IsDlgButtonChecked(IDC_CHK_DRAW_DEFCT_AREA) == TRUE)
	//	G_ProcessState.bTEST_INSPECT_IMG_DRAW_DEFECT = true;
	//else
	//	G_ProcessState.bTEST_INSPECT_IMG_DRAW_DEFECT= false;

	//CString newWriteData;
	//newWriteData.Format("%s",G_ProcessState.bTEST_INSPECT_IMG_DRAW_DEFECT==true?"1":"0");
	//WritePrivateProfileString(Section_Data_Options, Key_DrawDefectArea, newWriteData, m_IniFilePath);
}

void CVS11AOIIDlg::OnBnClickedChkSaveHistogram()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_HISTOGRAM) == TRUE)
		G_ProcessState.bTEST_ACTIVE_IMG_HISTOGRAM_IMG_SAVE = true;
	else
		G_ProcessState.bTEST_ACTIVE_IMG_HISTOGRAM_IMG_SAVE= false;

	CString newWriteData;
	newWriteData.Format("%s",G_ProcessState.bTEST_ACTIVE_IMG_HISTOGRAM_IMG_SAVE==true?"1":"0");
	WritePrivateProfileString(Section_Data_Options, Key_SaveActiveAreaHistogram, newWriteData, m_IniFilePath);
}
void CVS11AOIIDlg::OnBnClickedChkSaveHistoOrtCutpos()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_HISTO_ORT_CUTPOS) == TRUE)
	{
		G_fnCheckDirAndCreate("D:\\Histo_OrgImg\\");
		G_ClearFolder("D:\\Histo_OrgImg\\");
		//CheckDlgButton(IDC_CHK_SAVE_CUT_PAD_DRAW, BST_CHECKED);
		G_ProcessState.bTEST_SAVE_HISTO_ORG_IMG = true;
	}
	else
		G_ProcessState.bTEST_SAVE_HISTO_ORG_IMG= false;
	
}

void CVS11AOIIDlg::OnBnClickedChkSavePadInspectImg()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_PAD_INSPECT_IMG) == TRUE)
		G_ProcessState.bTEST_SAVE_PAD_AREA = true;
	else
		G_ProcessState.bTEST_SAVE_PAD_AREA= false;

	CString newWriteData;
	newWriteData.Format("%s",G_ProcessState.bTEST_SAVE_PAD_AREA==true?"1":"0");
	WritePrivateProfileString(Section_Data_Options, Key_SavePadOrgImgALL, newWriteData, m_IniFilePath);
}

void CVS11AOIIDlg::OnEnChangeEdSmallSizeX()
{
	CString strNewData;
	GetDlgItem(IDC_ED_SMALL_SIZE_X)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcessState.nSmallDefectSizeX != NewValue)
	{

		//if(NewValue>299 || NewValue<3)
		//{
		//	NewValue = 100;
		//	strNewData.Format("%d", NewValue);
		//}

		G_ProcessState.nSmallDefectSizeX = NewValue;
		WritePrivateProfileString(Section_Data_Options, Key_SmallDefectSizeX, strNewData, m_IniFilePath);

		//GetDlgItem(IDC_ED_SMALL_SIZE_X)->SetWindowText(strNewData);
	}
}


void CVS11AOIIDlg::OnEnChangeEdSmallSizeY()
{
	CString strNewData;
	GetDlgItem(IDC_ED_SMALL_SIZE_Y)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcessState.nSmallDefectSizeY != NewValue)
	{
		//if(NewValue>299 || NewValue<3)
		//{
		//	NewValue = 100;
		//	strNewData.Format("%d", NewValue);
		//}

		G_ProcessState.nSmallDefectSizeY = NewValue;
		WritePrivateProfileString(Section_Data_Options, Key_SmallDefectSizeY, strNewData, m_IniFilePath);

		//GetDlgItem(IDC_ED_SMALL_SIZE_Y)->SetWindowText(strNewData);
	}
}


void CVS11AOIIDlg::OnBnClickedBtCamCtrl()
{
	CString strDirData;
	GetDlgItem(IDC_BT_CAM_CTRL)->GetWindowText(strDirData);

	if(strDirData.CompareNoCase(">>") == 0)
	{
		strDirData.Format("<<");
		GetDlgItem(IDC_BT_CAM_CTRL)->SetWindowText(strDirData);

		this->MoveWindow(0, 0, 1835, 840);
	}
	else
	{
		strDirData.Format(">>");
		GetDlgItem(IDC_BT_CAM_CTRL)->SetWindowText(strDirData);
		 this->MoveWindow(0, 0, 1235, 840);
	}

	this->CenterWindow();
}




extern char *G_strVersionInfo[];
#include "AOI_Version_INFO.h"
void CVS11AOIIDlg::OnBnClickedBtSwVersion()
{
	CAboutDlg dlg;

	//CString strVersionInfo;

	dlg.m_strVersionInfo.Format("Version 정보 표시\r\n");
	int writeVersionIdex  =0;
	while(G_strVersionInfo[writeVersionIdex] != nullptr)
	{
		dlg.m_strVersionInfo.AppendFormat("%s",G_strVersionInfo[writeVersionIdex++] );
	}
		
	//dlg.m_edVersionInfo.SetWindowText(strVersionInfo);
	dlg.CenterWindow();
	dlg.DoModal();
}






void CVS11AOIIDlg::OnEnChangeEdPadalignDiswidth()
{
	CString strNewData;
	GetDlgItem(IDC_ED_PADALIGN_DISWIDTH)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcPadAlign.m_AlignWidth != NewValue)
	{
		G_ProcPadAlign.m_AlignWidth  = NewValue;
		WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_AlignWidth, strNewData, m_IniFilePath);

	}
}


void CVS11AOIIDlg::OnEnChangeEdPadalignOverwidth()
{
	CString strNewData;
	GetDlgItem(IDC_ED_PADALIGN_OVERWIDTH)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcPadAlign.m_AlignOverWidth != NewValue)
	{

		G_ProcPadAlign.m_AlignOverWidth = NewValue;
		WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_AlignWidthOverX, strNewData, m_IniFilePath);

	}
}


void CVS11AOIIDlg::OnEnChangeEdPadalignProcskipvalue()
{
	CString strNewData;
	GetDlgItem(IDC_ED_PADALIGN_PROCSKIPVALUE)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcPadAlign.m_AlignOKValue != NewValue)
	{

		G_ProcPadAlign.m_AlignOKValue = NewValue;
		WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_AlignOKValue, strNewData, m_IniFilePath);

	}
}


void CVS11AOIIDlg::OnBnClickedChkSavePadAlignImg()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_PAD_ALIGN_IMG) == TRUE)
		G_ProcPadAlign.m_AlignResultSave = true;
	else
		G_ProcPadAlign.m_AlignResultSave = false;

	if(G_ProcPadAlign.m_AlignResultSave == true)
	{
		//Pad Align Image Folder 관리.
		G_fnCheckDirAndCreate(TEST_PAD_ALIGN_IMG_SAVE_PATH);
		G_ClearFolder(CString(TEST_PAD_ALIGN_IMG_SAVE_PATH));
	}
}


void CVS11AOIIDlg::OnBnClickedChkSavePadImbBmp()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_PAD_IMB_BMP) == TRUE)
		G_ProcessState.bTEST_SAVE_PAD_AREA_BMP = true;
	else
		G_ProcessState.bTEST_SAVE_PAD_AREA_BMP = false;


	CString newWriteData;
	newWriteData.Format("%s",G_ProcessState.bTEST_SAVE_PAD_AREA_BMP==true?"1":"0");
	WritePrivateProfileString(Section_Data_Options, Key_SavePadOrgImgSaveBmp, newWriteData, m_IniFilePath);
}


void CVS11AOIIDlg::OnEnChangeEdPadBlurStep()
{
	CString strNewData;
	GetDlgItem(IDC_ED_PAD_BLUR_STEP)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcessState.nTEST_SAVE_PAD_AREA_BlurStep != NewValue)
	{

		G_ProcessState.nTEST_SAVE_PAD_AREA_BlurStep	 = NewValue;
		WritePrivateProfileString(Section_Data_Options, Key_SavePadOrgImgBlurStep, strNewData, m_IniFilePath);

	}
	
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CVS11AOIIDlg::OnBnClickedChkUnusePadAlign()
{
	if(IsDlgButtonChecked(IDC_CHK_UNUSE_PAD_ALIGN) == TRUE)
		G_ProcessState.bTEST_USE_PAD_ALIGN = false;
	else
		G_ProcessState.bTEST_USE_PAD_ALIGN = true;


	WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_UsePadAlign, G_ProcessState.bTEST_USE_PAD_ALIGN?"1":"0", m_IniFilePath);
}


void CVS11AOIIDlg::OnEnChangeEdHistoMaxPer()
{
	CString strNewData;
	GetDlgItem(IDC_ED_HISTO_MAX_PER)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcessState.dbLightingHistMaxPresent != NewValue)
	{

		G_ProcessState.dbLightingHistMaxPresent	 = NewValue;
		WritePrivateProfileString(Section_Data_Options, Key_AOI_LightingHistMaxPresent, strNewData, m_IniFilePath);
	}
}


void CVS11AOIIDlg::OnEnChangeEdLineScanWidth()
{
	CString strNewData;
	GetDlgItem(IDC_ED_LINE_SCAN_WIDTH)->GetWindowText(strNewData);
	m_LineScanWidth = atoi(strNewData);

}


void CVS11AOIIDlg::OnBnClickedChkSavePadUseMaxHigh()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_PAD_USE_MAX_HIGH) == TRUE)
		G_ProcPadAlign.m_AlignUseMaxHigh = true;
	else
		G_ProcPadAlign.m_AlignUseMaxHigh = false;

	WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_AlignUseMaxHigh, G_ProcPadAlign.m_AlignUseMaxHigh?"1":"0", m_IniFilePath);
}


void CVS11AOIIDlg::OnBnClickedChkSavePadMarkNotChange()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_PAD_MARK_NOT_CHANGE) == TRUE)
		G_ProcPadAlign.m_AlignImgNotChange = true;
	else
		G_ProcPadAlign.m_AlignImgNotChange = false;

	WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_AlignImgNotChange, G_ProcPadAlign.m_AlignImgNotChange?"1":"0", m_IniFilePath);
}


void CVS11AOIIDlg::OnBnClickedChkSavePadUseCanny()
{
	if(IsDlgButtonChecked(IDC_CHK_SAVE_PAD_MARK_NOT_CHANGE) == TRUE)
		G_ProcPadAlign.m_AlignImgUseCanny = true;
	else
		G_ProcPadAlign.m_AlignImgUseCanny = false;

	WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_AlignImgUseCanny, G_ProcPadAlign.m_AlignImgUseCanny?"1":"0", m_IniFilePath);
}


void CVS11AOIIDlg::OnEnChangeEdPadalignCannyThreshold1()
{
	CString strNewData;
	GetDlgItem(IDC_ED_PADALIGN_CANNY_THRESHOLD1)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcPadAlign.m_AlignImgUseCanny_threshold1 != NewValue)
	{
		G_ProcPadAlign.m_AlignImgUseCanny_threshold1 = NewValue;
		WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_AlignImgUseCannyTh1, strNewData, m_IniFilePath);
	}
}


void CVS11AOIIDlg::OnEnChangeEdPadalignCannyThreshold2()
{
	CString strNewData;
	GetDlgItem(IDC_ED_PADALIGN_CANNY_THRESHOLD2)->GetWindowText(strNewData);

	int NewValue = atoi(strNewData);

	if(G_ProcPadAlign.m_AlignImgUseCanny_threshold2 != NewValue)
	{
		G_ProcPadAlign.m_AlignImgUseCanny_threshold2 = NewValue;
		WritePrivateProfileString(Section_UpDn_PAD_AlignOptions, Key_AlignImgUseCannyTh2, strNewData, m_IniFilePath);
	}
}


void CVS11AOIIDlg::OnBnClickedTestLocalFile()
{
	// TODO: Add your control notification handler code here
#ifndef TEST_LOCAL_FILE
	AfxMessageBox("본 기능은 로컬 파일 테스트 모드에서만 실행됩니다.");
	return;
#endif
	{
		CMDMSG dCmdMsg;
		dCmdMsg.uTask_Dest = TASK_21_Inspector;
		dCmdMsg.uFunID_Dest = nBiz_AOI_System;
		dCmdMsg.uSeqID_Dest = nBiz_Send_UnitInitialize;
		dCmdMsg.uUnitID_Dest = 0;

		Sys_fnAnalyzeMsg(&dCmdMsg);
	}

	{
		CMDMSG dCmdMsg;
		dCmdMsg.uTask_Dest = TASK_21_Inspector;
		dCmdMsg.uFunID_Dest = nBiz_AOI_System;
		dCmdMsg.uSeqID_Dest = nBiz_Send_AlarmReset;
		dCmdMsg.uUnitID_Dest = 0;

		Sys_fnAnalyzeMsg(&dCmdMsg);
	}

	{
		//////////////////////////////////////////////////////////////////////////
		//Active Param Send
		SAOI_IMAGE_INFO dImageInfo;
		SAOI_COMMON_PARA dCommonParam;
		_TestSetActiveParam(dImageInfo, dCommonParam);

		int SendDataSize = sizeof(SAOI_IMAGE_INFO)+sizeof(SAOI_COMMON_PARA);
		char *pSendActiveParam = new char[SendDataSize];
		memcpy(&pSendActiveParam[0], &dImageInfo, sizeof(SAOI_IMAGE_INFO));
		memcpy(&pSendActiveParam[sizeof(SAOI_IMAGE_INFO)], &dCommonParam, sizeof(SAOI_COMMON_PARA));

		CMDMSG dCmdMsg;
		dCmdMsg.uTask_Dest = TASK_21_Inspector;
		dCmdMsg.uFunID_Dest = nBiz_AOI_Parameter;
		dCmdMsg.uSeqID_Dest = nBiz_Send_ActiveAreaParam;
		dCmdMsg.uUnitID_Dest = 0;
		dCmdMsg.cMsgBuf = (UCHAR *)pSendActiveParam;
		dCmdMsg.uMsgSize = SendDataSize;

		Sys_fnAnalyzeMsg(&dCmdMsg);

		delete [] pSendActiveParam;
	}
	{
		//////////////////////////////////////////////////////////////////////////
		//Pad Param Send
		SAOI_IMAGE_INFO listImageInfo[4];
		SAOI_COMMON_PARA listCommonParam[4];
		_TestSetPadParam(listImageInfo, listCommonParam);

		int SendDataSize = (sizeof(SAOI_IMAGE_INFO)*4)+(sizeof(SAOI_COMMON_PARA)*4);
		char *pSendPadParam = new char[SendDataSize];
		int JumpSize =  sizeof(SAOI_IMAGE_INFO) + sizeof(SAOI_COMMON_PARA);

		for(int SetStep =0; SetStep<4; SetStep++)
		{
			memcpy(&pSendPadParam[(JumpSize*SetStep)], listImageInfo, sizeof(SAOI_IMAGE_INFO));
			memcpy(&pSendPadParam[(JumpSize*SetStep) + sizeof(SAOI_IMAGE_INFO)], listCommonParam, sizeof(SAOI_COMMON_PARA));
		}

		CMDMSG dCmdMsg;
		dCmdMsg.uTask_Dest = TASK_21_Inspector;
		dCmdMsg.uFunID_Dest = nBiz_AOI_Parameter;
		dCmdMsg.uSeqID_Dest = nBiz_Send_PadAreaParam;
		dCmdMsg.uUnitID_Dest = 0;
		dCmdMsg.cMsgBuf = (UCHAR *)pSendPadParam;
		dCmdMsg.uMsgSize = SendDataSize;

		Sys_fnAnalyzeMsg(&dCmdMsg);

		delete [] pSendPadParam;
	}
	{
		//////////////////////////////////////////////////////////////////////////
		//Glass(Stick)정보를 전달 하자.
		GLASS_PARAM dGlassParam;
		_TestSetGlassParam(dGlassParam);

		int SendDataSize = sizeof(GLASS_PARAM)+1;
		char *pSendHWGlassParam = new char[SendDataSize];
		memcpy(&pSendHWGlassParam[0], &dGlassParam, sizeof(GLASS_PARAM));
		pSendHWGlassParam[SendDataSize-1] = 1;//Macro

		CMDMSG dCmdMsg;
		dCmdMsg.uTask_Dest = TASK_21_Inspector;
		dCmdMsg.uFunID_Dest = nBiz_AOI_Parameter;
		dCmdMsg.uSeqID_Dest = nBiz_Send_GlassParamSet;
		dCmdMsg.uUnitID_Dest = 0;
		dCmdMsg.cMsgBuf = (UCHAR *)pSendHWGlassParam;
		dCmdMsg.uMsgSize = SendDataSize;

		Sys_fnAnalyzeMsg(&dCmdMsg);

		delete [] pSendHWGlassParam;
	}
	
	// ScanStart 
	{
		G_CoordProcObj.m_OneLineScnaImgCnt = 1;
		int nScanIndex = 1;
		CMDMSG dCmdMsg;
		dCmdMsg.uTask_Dest = TASK_21_Inspector;
		dCmdMsg.uFunID_Dest = nBiz_AOI_Inspection;
		dCmdMsg.uSeqID_Dest = nBiz_Send_ScanStart;
		dCmdMsg.uUnitID_Dest = nScanIndex;

		Sys_fnAnalyzeMsg(&dCmdMsg);
	}
}

void CVS11AOIIDlg::_TestSetGlassParam(GLASS_PARAM &dGlassParam)
{
	dGlassParam.m_ScanLineShiftDistance = 60000;
	dGlassParam.m_ScanImgResolution_W = 5.f;
	dGlassParam.m_ScanImgResolution_H = 5.f;
	dGlassParam.CenterPointX=347234;
	dGlassParam.CenterPointY=430787;
	dGlassParam.PadStartDistanceX=0;
	dGlassParam.PadStartDistanceY=0;
	dGlassParam.PadEndDistanceX=0;
	dGlassParam.PadEndDistanceY=0;
	dGlassParam.ActiveDeadZoneSize=100;
	dGlassParam.PadStartDeadZoneDisX=0;
	dGlassParam.PadStartDeadZoneDisY=0;
	dGlassParam.PadEndDeadZoneDisX=0;
	dGlassParam.PadEndDeadZoneDisY=0;
	dGlassParam.PadDnDeadZoneWidth=0;
	dGlassParam.PadDnDeadZoneHeight=0;
	dGlassParam.ActivePadDeadZone=0;
	dGlassParam.STICK_WIDTH=70000;
	dGlassParam.STICK_EDGE_DIS_WIDTH=2925;
	dGlassParam.STICK_EDGE_DIS_HEIGHT=174600;
	dGlassParam.CELL_EDGE_X=-31720;
	dGlassParam.CELL_EDGE_Y=427596;
	dGlassParam.CELL_DISTANCE_X=0;
	dGlassParam.CELL_DISTANCE_Y=123731;
	dGlassParam.CELL_SIZE_X=63441;
	dGlassParam.CELL_SIZE_Y=112807;
	dGlassParam.GLASS_CELL_COUNT_X=1;
	dGlassParam.GLASS_CELL_COUNT_Y=7;
	dGlassParam.BLOCK_DISTANCE_X=0;
	dGlassParam.BLOCK_DISTANCE_Y=0;
	dGlassParam.GLASS_BLOCK_COUNT_Y=1;
	dGlassParam.GLASS_BLOCK_COUNT_X=1;
	strcpy(dGlassParam.m_GlassName, "I_20150602\\DEG1L25RED_TEST");
}

void CVS11AOIIDlg::_TestSetActiveParam(SAOI_IMAGE_INFO &dImageInfo, SAOI_COMMON_PARA &dCommonParam)
{
	dImageInfo.nScaleX1    =440;
	dImageInfo.nScaleY1    =440;
	dImageInfo.nScaleX2    =0;
	dImageInfo.nScaleY2    =0;
	dImageInfo.nScaleX3    =0;
	dImageInfo.nScaleY3    =0;
	dImageInfo.nMinRangeMain =30;
	dImageInfo.nMaxRangeMain =40;
	dImageInfo.nMinRangeLPad =0;
	dImageInfo.nMaxRangeLPad =0;
	dImageInfo.nMinRangeRPad =0;
	dImageInfo.nMaxRangeRPad =0;
	dImageInfo.nOutLineLeft  =5; 
	dImageInfo.nOutLineRight =5;
	dImageInfo.nOutLineUp    =5;
	dImageInfo.nOutLineDown  =5;
	dImageInfo.nBlockCount   =64;
	dImageInfo.nThreadCount  =192;
//	dImageInfo.nBlockCount   = 120;
//	dImageInfo.nThreadCount  = 50;
	dImageInfo.nMinimumSizeX =5;
	dImageInfo.nMinimumSizeY =5;
	dImageInfo.nMaximumSizeX =50;
	dImageInfo.nMaximumSizeY =50;

	dCommonParam.nDefectDistance	=300;
	dCommonParam.nImageSizeX	=12000;	
	dCommonParam.nImageSizeY	=6000;	
	dCommonParam.nMaskType	=100;
	dCommonParam.nZoneStep[0]	=12000;
	dCommonParam.nZoneStep[1]	=1700;
	dCommonParam.nZoneStep[2]	=6250;
	dCommonParam.nZoneStep[3]	=7350;
	dCommonParam.nZoneStep[4]	=8192;
	dCommonParam.nZoneOffset[0]	=0;
	dCommonParam.nZoneOffset[1]	=0;
	dCommonParam.nZoneOffset[2]	=0;
	dCommonParam.nZoneOffset[3]	=0;
	dCommonParam.nZoneOffset[4]	=0;
	dCommonParam.bImageLogUsed	=1000;
}

void CVS11AOIIDlg::_TestSetPadParam(SAOI_IMAGE_INFO *listImageInfo, SAOI_COMMON_PARA *listCommonParam)
{
	for(int i = 0; i<4; i++) {
		SAOI_IMAGE_INFO &dImageInfo = listImageInfo[i];
		SAOI_COMMON_PARA &dCommonParam = listCommonParam[i];

		dImageInfo.nScaleX1	  =440;
		dImageInfo.nScaleY1	  =440;
		dImageInfo.nScaleX2	  =0;
		dImageInfo.nScaleY2	  =0;
		dImageInfo.nScaleX3	  =0;
		dImageInfo.nScaleY3	  =0;
		dImageInfo.nMinRangeMain =30;
		dImageInfo.nMaxRangeMain =40;
		dImageInfo.nMinRangeLPad =0;
		dImageInfo.nMaxRangeLPad =0;
		dImageInfo.nMinRangeRPad =0;
		dImageInfo.nMaxRangeRPad =0;
		dImageInfo.nOutLineLeft  =5;   
		dImageInfo.nOutLineRight =5;    
		dImageInfo.nOutLineUp    =5; 
		dImageInfo.nOutLineDown  =5;   
		dImageInfo.nBlockCount   =64;
		dImageInfo.nThreadCount  =192;
//		dImageInfo.nBlockCount   =1;
//		dImageInfo.nThreadCount  =12000;
		dImageInfo.nMinimumSizeX =5;
		dImageInfo.nMinimumSizeY =5;
		dImageInfo.nMaximumSizeX =50;
		dImageInfo.nMaximumSizeY =50;

		dCommonParam.nDefectDistance	=300;
		dCommonParam.nImageSizeX	=12000;	
		dCommonParam.nImageSizeY	=6000;	
		dCommonParam.nMaskType	=100;
		dCommonParam.nZoneStep[0]	=0;
		dCommonParam.nZoneStep[1]	=0;
		dCommonParam.nZoneStep[2]	=0;
		dCommonParam.nZoneStep[3]	=0;
		dCommonParam.nZoneStep[4]	=0;
		dCommonParam.nZoneOffset[0]	=0;
		dCommonParam.nZoneOffset[1]	=0;
		dCommonParam.nZoneOffset[2]	=0;
		dCommonParam.nZoneOffset[3]	=0;
		dCommonParam.nZoneOffset[4]	=0;
		dCommonParam.bImageLogUsed =1000;
	}
}

void CVS11AOIIDlg::OnBnClickedTestLocalFolder()
{
#ifndef TEST_LOCAL_FILE
	AfxMessageBox("본 기능은 로컬 파일 테스트 모드에서만 실행됩니다.");
	return;
#endif

	UpdateData(TRUE);

	AfxBeginThread(_TestLocalFolderThread, this);
}

UINT CVS11AOIIDlg::_TestLocalFolderThread(LPVOID pParam)
{
	CVS11AOIIDlg *pThis = (CVS11AOIIDlg *)pParam;

	char szTestFolderPath[MAX_PATH];
	strcpy(szTestFolderPath, LPCTSTR(pThis->m_strTestFolder));

	char szTestFileName[MAX_PATH] = {0x00 ,};
	char *pCut = strrchr(szTestFolderPath, '.');		// 폴더 경로에 "테스트 파일명"까지 있는 경우
	if(pCut) {
		pCut = strrchr(szTestFolderPath, '\\');
		*pCut = 0x00;
		strcpy(szTestFileName, pCut+1);
	}

	WIN32_FIND_DATA dFind;
	memset(&dFind, 0x00, sizeof(WIN32_FIND_DATA));

	char szFilter[MAX_PATH];
	sprintf_s(szFilter,"%s\\*.jpg",szTestFolderPath);

	char szText[256];
	sprintf_s(szText,"[INS] Start to inspect all files in folder : %s\n", szTestFolderPath);
	OutputDebugString(szText);

	int nCount = 0;
	HANDLE hFind = FindFirstFile(szFilter, &dFind);
	if(hFind != INVALID_HANDLE_VALUE) {
		do {
			if(dFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)  {
				continue;
			}

			if(strcmp(dFind.cFileName, ".")==0 || strcmp(dFind.cFileName, "..") == 0) {
				continue;
			}

			if(szTestFileName[0] != 0x00) {
				if(stricmp(szTestFileName, dFind.cFileName) == 0) {
					nCount++;
				}
			}
			else {
				nCount++;
			}
		} while(FindNextFile(hFind, &dFind));
		FindClose(hFind);
	}

	if(nCount == 0) {
		sprintf_s(szText,"[INS] > No file to be inspected.\n");
		OutputDebugString(szText);
		return 0;
	}

	sprintf_s(szText,"[INS] > File count to be inspected : %d\n", nCount);
	OutputDebugString(szText);

	int nIndex = 0;
	char szTestFilePath[MAX_PATH];
	hFind = FindFirstFile(szFilter, &dFind);
	do {
		if(dFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)  {
			continue;
		}

		if(strcmp(dFind.cFileName, ".")==0 || strcmp(dFind.cFileName, "..") == 0) {
			continue;
		}

		if(szTestFileName[0] != 0x00 && stricmp(szTestFileName, dFind.cFileName) != 0) {
			continue;
		}

		nIndex++;
		strcpy(pThis->m_szTestFileName, dFind.cFileName);

		sprintf_s(szText,"[INS] (%d/%d .. %d%%) Start to test : %s\n", nIndex, nCount, (nIndex * 100) / nCount, pThis->m_szTestFileName);
		OutputDebugString(szText);

		sprintf_s(szTestFilePath, "%s\\%s",szTestFolderPath,pThis->m_szTestFileName);
		if(pThis->_ChangeTestFile(szTestFilePath)) {
			sprintf_s(szText,"[INS]  > Changed Test File.\n");
			OutputDebugString(szText);

			sprintf_s(szText,"[INS]  > Start to Inspect.\n");
			pThis->OnBnClickedTestLocalFile();
			pThis->_WaitForFinishingInspection();

			sprintf_s(szText,"[INS]  > Done.\n");
			OutputDebugString(szText);
		}
		else {
			sprintf_s(szText,"[INS]  > Fail to Changed Test File.\n");
			OutputDebugString(szText);
		}
	} while(FindNextFile(hFind, &dFind));
	FindClose(hFind);

	pThis->m_szTestFileName[0] = 0x00;

	sprintf_s(szText,"[INS] Finished.\n");
	OutputDebugString(szText);

	return 0;
}

BOOL CVS11AOIIDlg::_ChangeTestFile(char *szTestFilePath)
{
	if(szTestFilePath == 0x00) {
		return FALSE;
	}

	char szTempFileName[MAX_PATH] = "D:\\nbiz_inspectstock\\Data\\TestImage_12k_Temp.bmp";
	char szTestDataFileName[MAX_PATH] = "D:\\nbiz_inspectstock\\Data\\TestImage_12k.bmp";
	int nTestDataSizeX = 12000;
	int nTestDataSizeY = 6000;

	IplImage *pImage = cvLoadImage(szTestFilePath, CV_LOAD_IMAGE_COLOR);
	if(pImage == 0x00) {
		return FALSE;
	}

	int nImgSizeX = pImage->width;
	int nImgSizeY = pImage->height;

	BOOL bResult = FALSE;
	if(nImgSizeX == nTestDataSizeX || nImgSizeY >= nTestDataSizeY) {
		if(nImgSizeY > nTestDataSizeY) {
			CvRect rcROI = cvRect(0, 0, nTestDataSizeX, nTestDataSizeY);
			cvSetImageROI(pImage, rcROI);
		}

		DeleteFile(szTestDataFileName);
		bResult = cvSaveImage(szTempFileName, pImage);
		MoveFileEx(szTempFileName, szTestDataFileName, MOVEFILE_WRITE_THROUGH);
	}
	cvReleaseImage(&pImage);

	return bResult;
}

void CVS11AOIIDlg::_WaitForFinishingInspection()
{
	BOOL bFinished = FALSE;
	do {
		Sleep(100);

		if(G_ProcessState.m_UnitState == nBiz_AOI_STATE_READY) {
			bFinished = TRUE;
		}
	} while(!bFinished);

	// Wait more seconds to write result files.
	Sleep(1000);
}
