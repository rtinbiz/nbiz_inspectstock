#pragma once
#include "..\..\..\CommonHeader\Class\InterServerInterface.h"
#include "..\..\..\CommonHeader\Inspection\CoordinatesProc.h"
#include "AOI_ImageCtrlQ.h"
#include "PadAreaCtrlQ.h"
#include "PADInspectProc.h"

/** 
@brief		Result Process에사 사용되는 구조체

@remark
-			Inspection 이후 Data 처리를 위한 구조체 이다.
-			
@author		최지형
@date		2010.00.00
*/ 
typedef struct RESULT_PROCESS_TRG
{
	int							m_ProcIndex;
	bool						m_bProcessRun;
	CWinThread	*				m_pResultThread;
	AOI_ImageCtrlQ *			m_pIDTCtrlQ;
	PAD_ImageCtrlQ *			m_pPADCtrlQ;

	CoordinatesProc *			m_pCoordProcObj;

	CInterServerInterface *		m_pServerInterface;
	
	PROC_STATE	*				m_pUpdateState;

	PROC_PAD	*				m_pPadProcObj;

	ImageSaveQ	*				m_pInspectSaveImgQ;
	ImageSaveQ	*				m_pHistogramImgQ;

	RESULT_PROCESS_TRG()
	{
		m_ProcIndex			= -1;
		m_bProcessRun		=  false;
		m_pResultThread		= nullptr;
		m_pIDTCtrlQ			= nullptr;
		m_pPADCtrlQ			= nullptr;
		m_pServerInterface	= nullptr;
		m_pCoordProcObj		= nullptr;
		m_pUpdateState		= nullptr;

		m_pHistogramImgQ	= nullptr;
		m_pInspectSaveImgQ	= nullptr;
	}

} PROC_RESULT, *LPRESULT_PROCESS;

//Result Process Def.
UINT fn_ResultProcess(LPVOID pParam);

inline void GetImgPixelDeviation(IplImage* simg, IplImage* Bufimg, int RepeatCycleX, int RepeatCycleY);

inline void GetImgHistogram(IplImage* simg, IplImage* hist_img, double HistValue,  int *pScanMaxValue = nullptr, float *pLowHistData = nullptr, bool bSendRealTime = false, bool bPixelDeviation= false);
//Defect Cutting Image Calculate.(위치 보정)
inline void G_CalcImgPosToCood(CoordinatesProc*pCoordObj, SCANIMGINFO* pCoodArea, DEFECT_DATA *m_pSearchRetList, COORD_DINFO *pCoordinateRetList);

inline void CalcAFValueSo( BYTE *a, int sx, int sy, BYTE *out);

#ifdef TEST_DEFECT_IMG_IN_ORG
inline bool G_CalcInImageDefPosAndSize(CRect *pImagePos, CRect *pDefecPos, HW_PARAM *pHWParam);
inline void G_CalcDefImgAndMask_OnOrgImg(IplImage *pImageBuffer, IplImage *pSaveImageBuffer,CRect* pDefecArea, 
															BYTE *pRetImgBuf, int ScanLineNum, HW_PARAM *pHWParam, bool bSizeError=false);

#else
//모두 중앙으로 보내기.
inline bool G_CalcInImageDefPosAndSize(int *pPasteStartX, int *pPasteStartY,CRect *pImagePos, CRect *pDefecPos, HW_PARAM *pHWParam);
inline void G_CalcDefImgAndMask_OnOrgImg(int *pPasteStartX, int *pPasteStartY, IplImage *pImageBuffer, IplImage *pSaveImageBuffer, CRect* pDefecArea, BYTE *pRetImgBuf, int ScanLineNum, bool bSizeError);
#endif