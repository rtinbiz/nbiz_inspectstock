#pragma once
#include "HWParamInterface.h"
#include "..\..\..\CommonHeader\Inspection\CalculateCoordinateDefine.h"

//#include "d:\nbiz_aoi_smd_m01\task\vs11aoii_x64\grid\gridctrl.h"
//#pragma once#include "afxwin.h"#include "afxwin.h"



#define  getbit(x,y) ((x)>>(y))&(0x01)
#define  setbit(x,y) (x)|=((0x01)<<(y))
#define  clrbit(x,y) (x)&=~((0x01)<<(y))
//////////////////////////////////////////////////////////////////////////

inline void DelAllItemGridRow(CGridCtrl *pTargetGrid)
{
	int DelItemCnt = pTargetGrid->GetRowCount();
	if(DelItemCnt>1)
	{
		for(int DelIndex = DelItemCnt-1; DelIndex > 0; DelIndex--)
		{
			pTargetGrid->DeleteRow(DelIndex);
		}
	}
	pTargetGrid->Refresh();
}
inline void NextCellEditMove(CGridCtrl *pTargetGrid)
{
	CCellRange GSelectItem = pTargetGrid->GetSelectedCellRange();
	int RowMin = GSelectItem.GetMinRow();
	int ColPos = -1;

	if(RowMin>0 && RowMin<pTargetGrid->GetRowCount())
	{
		for(int ColStep =0;ColStep<pTargetGrid->GetColumnCount();ColStep++)
		{
			if(pTargetGrid->IsItemEditing(RowMin, ColStep) == TRUE)
			{
				ColPos = ColStep;
				break;
			}
		}
		if(ColPos == -1)
			return;
		RowMin +=1;
		if(RowMin<pTargetGrid->GetRowCount())
		{
			pTargetGrid->SetSelectedRange(RowMin,ColPos,RowMin,ColPos,FALSE,TRUE);
			pTargetGrid->SetFocusCell(RowMin,ColPos);
		}

		pTargetGrid->Refresh();
	}
}

inline void DelSelectedItemGridRow(CGridCtrl *pTargetGrid)
{
	if(pTargetGrid!=NULL)
	{
		int TotalCnt = pTargetGrid->GetRowCount();
		if(TotalCnt>pTargetGrid->GetFixedColumnCount())
		{

			int *pDelItems = new int[TotalCnt];
			memset(pDelItems, 0x00, sizeof(int)*TotalCnt);

			CCellRange SelectItem = pTargetGrid->GetSelectedCellRange();
			int DelMin = SelectItem.GetMinRow();
			int DelMax = SelectItem.GetMaxRow();
			int DelCnt = 0;
			//Select Cell을 찾는다.(큰 Index부터 찾는다.)
			for(int DelIndex = DelMax;DelIndex>=DelMin; DelIndex--)
			{
				if(((pTargetGrid->GetItemState(DelIndex, 0)) & GVIS_SELECTED) == GVIS_SELECTED)
				{
					pDelItems[DelCnt++] = DelIndex;
				}
			}
			//찾은 Cell을 일괄 삭제 한다.
			for(int DelIndex = 0; DelIndex<DelCnt; DelIndex++)
			{
				pTargetGrid->DeleteRow(pDelItems[DelIndex]);
			}

			if(pDelItems != NULL)
				delete [] pDelItems;
			pTargetGrid->Refresh();
		}
	}
}
inline BOOL CheckNumberStr(CString *pTarget)
{
	CString temp;
	temp.Format("%d", _ttoi(*pTarget));
	if(temp.GetLength() != pTarget->GetLength())
		return FALSE;
	return TRUE;
}
inline void SetGridCellBackColor(CGridCtrl *pTargetGrid, int Row, int Col, COLORREF clr)
{
	GV_ITEM Item;

	//Item.mask = GVIF_TEXT;
	Item.row = Row;
	Item.col = Col;
	Item.crBkClr = clr;             // or - m_Grid.SetItemBkColour(row, col, clr);
	Item.crFgClr = RGB(255,0,0);    // or - m_Grid.SetItemFgColour(row, col, RGB(255,0,0));				    
	Item.mask    = (GVIF_BKCLR);

	pTargetGrid->SetItem(&Item);
}

inline void memToHexString(int ViewLine, BYTE *pDataAdd, int TotalSize, CString *pNewStr)
{

	if(ViewLine>0)
	{

		ViewLine-=1;
		int ViewSize = TotalSize - (ViewLine*10);
		int InputStep = ViewLine+1;

		if(ViewSize>0)
		{
			BYTE *pViewAdd =  pDataAdd	+ (ViewLine*10);
			pNewStr->Format("%03d : ", InputStep++);
			for(int ViewStep = 0; ViewStep < 20*10; ViewStep++)
			{

				if(ViewStep<ViewSize)
				{
					pNewStr->AppendFormat("%02X ", ((*(pViewAdd+ViewStep))&0x00FF));
				}
				else
				{
					pNewStr->AppendFormat("-- ");
				}
				if(ViewStep%10 == 9)
				{
					pNewStr->AppendFormat("\r\n");
					pNewStr->AppendFormat("%03d : ", InputStep++);
				}
			}
		}
	}
}

inline void memToASCIIString(int ViewLine, BYTE *pDataAdd, int TotalSize, CString *pNewStr)
{

	if(ViewLine>0)
	{

		ViewLine-=1;
		int ViewSize = TotalSize - (ViewLine*10);
		int InputStep = ViewLine+1;

		if(ViewSize>0)
		{
			BYTE *pViewAdd =  pDataAdd	+ (ViewLine*10);
			pNewStr->Format("%03d : ", InputStep++);
			for(int ViewStep = 0; ViewStep < 20*10; ViewStep++)
			{

				if(ViewStep<ViewSize)
				{
					char ViewChar = ((*(pViewAdd+ViewStep))&0x00FF);

					ViewChar = ViewChar<=0x1F?'.':ViewChar;
					pNewStr->AppendFormat("%c ", ViewChar);
				}
				else
				{
					pNewStr->AppendFormat("-");
				}
				if(ViewStep%10 == 9)
				{
					pNewStr->AppendFormat("\r\n");
					pNewStr->AppendFormat("%03d : ", InputStep++);
				}
			}
		}
	}
}
inline void memToBINString(int ViewLine, BYTE *pDataAdd, int TotalSize, CString *pNewStr)
{

	if(ViewLine>0)
	{

		ViewLine-=1;
		int ViewSize = TotalSize - (ViewLine*3);
		int InputStep = ViewLine+1;

		if(ViewSize>0)
		{
			BYTE *pViewAdd =  pDataAdd	+ (ViewLine*3);
			pNewStr->Format("%03d : ", InputStep++);
			for(int ViewStep = 0; ViewStep < 20*3; ViewStep++)
			{

				if(ViewStep<ViewSize)
				{
					char ViewChar = ((*(pViewAdd+ViewStep))&0x00FF);

					CString ViewBit;
					ViewBit.Format("");
					for(int BitStep =0; BitStep<8; BitStep++)
					{
						ViewBit.AppendFormat("%d", getbit(ViewChar, BitStep));
					}
					pNewStr->AppendFormat("%s ", ViewBit);
				}
				else
				{
					pNewStr->AppendFormat("-");
				}
				if(ViewStep%3 == 2)
				{
					pNewStr->AppendFormat("\r\n");
					pNewStr->AppendFormat("%03d : ", InputStep++);
				}
			}
		}
	}
}

inline void memToDECString(int ViewLine, BYTE *pDataAdd, int TotalSize, CString *pNewStr)
{

	if(ViewLine>0)
	{

		ViewLine-=1;
		int ViewSize = (TotalSize/4) - (ViewLine*3);
		int InputStep = ViewLine+1;

		if(ViewSize>0)
		{
			int *pViewAdd =( ((int *)pDataAdd)	+ (ViewLine*3));
			pNewStr->Format("%03d : ", InputStep++);
			for(int ViewStep = 0; ViewStep < 20*3; ViewStep++)
			{

				if(ViewStep<ViewSize)
				{
					int ViewChar = ((*(pViewAdd+ViewStep))&0xFFFFFFFF);

					pNewStr->AppendFormat("%10d ", ViewChar);
				}
				else
				{
					pNewStr->AppendFormat("-");
				}
				if(ViewStep%3 == 2)
				{
					pNewStr->AppendFormat("\r\n");
					pNewStr->AppendFormat("%03d : ", InputStep++);
				}
			}
		}
	}
}

// CHWParamEdit 대화 상자입니다.

class CHWParamEdit : public CDialogEx
{
	DECLARE_DYNAMIC(CHWParamEdit)

public:
	CHWParamEdit(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHWParamEdit();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_EDIT_HW_PARAM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	int m_TaskNum;
	CFont m_GridFont;
	CGridCtrl m_GridEditHWParam;
	virtual void PostNcDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedBtSave();
	afx_msg void OnBnClickedBtCancel();


	CRoundButtonStyle m_tButtonStyle;
	CRoundButton2 m_btSaveParam;
	CRoundButton2 m_btCloseParam;

	CString m_IniFilePath;

	HW_PARAM m_HWParam;	
	int		m_AOI_CAM_DISTANCE_X_Type2;						//<<< 기준점. Cam Center에서 AOI Cam과의 거리.(um단위)
	int		m_AOI_CAM_DISTANCE_Y_Type2;						//<<< 기준점. Cam Center에서 AOI Cam과의 거리.(um단위)

	int		m_AOI_SCAN_START_OFFSET_ODD_Type2;				//<<Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(홀수 Scan시 적용)(um단위)
	int		m_AOI_SCAN_START_OFFSET_EVEN_Type2;				//<<Line Scan을 시작 할때 X Axis의 Scan 오차를 보정 한다.(짝수 Scan시 적용)(um단위)

	int m_AxisOverLap;	

	CString UpdatePath_ImgData;
	CString UpdatePath_Result;

	CString strDalsaCamPath;

	void OnTagGridSelEditedText(NMHDR* pNMHDR, LRESULT* pResult);
	bool m_fnLoadHWParam(int TaskNum);
	bool m_fnUpdateUI();



	bool m_fnInspectPParam(int TaskNum);
	SAOI_COMMON_PARA m_InspectParam;//==>>> Inspection Dll에 직접 넘겨 줘야 하는 값으로 따로 관리 한다.

};
