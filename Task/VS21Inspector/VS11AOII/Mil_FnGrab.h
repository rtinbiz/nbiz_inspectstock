#pragma once
#ifdef MATROX_LINE_SCAN_GRABBER
#include "AOI_ImageCtrlQ.h"
#include <mil.h>


MIL_INT MFTYPE fn_GrabProcess(MIL_INT HookType, MIL_ID HookId, void MPTYPE *HookDataPtr);
#endif