#pragma once
// Task Number
#define		TN_MASTERPC		10



//입력되는 Task Number에 따라 Server Interface File이 바뀐다.
#define  MSG_WIN_TASK_INI_FILE_FULL_NAME_TN11 "\\Config\\AOI_APP_T11.DAT"
#define  MSG_WIN_TASK_INI_FILE_FULL_NAME_TN12 "\\Config\\AOI_APP_T12.DAT"
#define  MSG_WIN_TASK_INI_FILE_FULL_NAME_TN13 "\\Config\\AOI_APP_T13.DAT"
#define  MSG_WIN_TASK_INI_FILE_FULL_NAME_TN14 "\\Config\\AOI_APP_T14.DAT"


//Client DLL 에서 Callback 되어질 때 사용되는 이벤트
#define  WM_USER_CALLBACK				 (WM_USER + 1000)

