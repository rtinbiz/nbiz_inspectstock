#pragma once
#include "AOI_ImageCtrlQ.h"
/*
*	Module Name		:	QstrDefList
*	Parameter		:	
*	Return			:	
*	Function		:	DATA Queue를 이용 검출 Data의 Defect File쓰기 대기 상태로 갖고 있는다.
고정 Data/Defect Data/Cell Data,등으로 기록된다.
Result Process Speed를 위해 정보를 File이 아닌 Queue에 모았다가. 기록 한다.
*	Create			:	2010.04.00
*	Author			:	최지형
*	Version			:	1.0
*/
typedef struct WriteFileHeader_TAG
{
	//
	int ImgCount;
	int ImgW;
	int ImgH;
	
	char ImgList[1];
}WIMG_File;
typedef struct ORG_IMG_FILE_BACKUP_TRG
{
	CString	FromFolderPath;
	CString	ToFolderPath;


	ORG_IMG_FILE_BACKUP_TRG()
	{
		FromFolderPath.Format("");
		ToFolderPath.Format("");
	}
} ORG_IMG_FILE_BACKUP;

class CDefectFileFormat
{
public:
	CDefectFileFormat();
	~CDefectFileFormat();
private: 
	CRITICAL_SECTION m_CrtSection;

public:
	int			m_DefectImgSize;
	int			m_DefectImgCount;
	//CString m_StrFullData;
	CFile m_TextFileObj;
	CFile m_ImageFileObj;
	CString m_TextFilePath;

	CString m_ImageFilePath;
	CString m_ImageFileUpdatePath;

	bool m_fnSetWriteDefectTextObj(CString DefectTextFilePath);
	bool m_fnSetWriteDefectImageObj(CString DefectImageFilePath, CString ImgUpdatePath, int ImgW, int ImgH);

	bool m_fnWriteImageDataOnNode(BYTE *pRawData);

	bool m_fnCloseFileObj();
	bool m_fnIsOpened();
private:
	bool m_fnWriteFullDefectListData( CString *pWriteData);

};




UINT FolderCopyThread(LPVOID pParam);
BYTE FolderCopyFromAToB(CString fromPath, CString ToPath);
bool G_fnCheckDirAndCreate(char * pCheckPath);
bool G_ClearFolder(CString ClearFolderPath);