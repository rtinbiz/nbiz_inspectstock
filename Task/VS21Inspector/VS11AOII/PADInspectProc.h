#pragma once
#include "..\..\..\CommonHeader\Class\InterServerInterface.h"
#include "..\..\..\CommonHeader\Inspection\CoordinatesProc.h"
#include "PadAreaCtrlQ.h"
#include "ProcMsgQ.h"

#define PAD_INSPECTION_IDLE	1
#define PAD_INSPECTION_RUN	2
#define PAD_INSPECTION_END	3

/** 
@brief		Result Process에사 사용되는 구조체

@remark
-			Inspection 이후 Data 처리를 위한 구조체 이다.
-			
@author		최지형
@date		2010.00.00
*/ 
typedef struct PAD_PROCESS_TRG
{
	int							m_ProcIndex;
	bool						m_bProcessRun;
	CWinThread	*			m_pResultThread;
	PAD_ImageCtrlQ *	m_pPADCtrlQ;
	CoordinatesProc *	m_pCoordProcObj;

	CInterServerInterface *	m_pServerInterface;

	PROC_STATE	*		m_pUpdateState;

	int							m_StatePADInspect;

	DefectDataQ	*		m_pResultDefectQ;
	ImageSaveQ	*		m_pInspectSaveImgQ;

	int							m_ScanLineNum;
	PAD_PROCESS_TRG()
	{
		m_ProcIndex			= -1;
		m_bProcessRun		=  false;
		m_pResultThread		= nullptr;
		m_pPADCtrlQ			= nullptr;
		m_pServerInterface	= nullptr;
		m_pCoordProcObj		= nullptr;
		m_pUpdateState		= nullptr;

		m_pResultDefectQ		= nullptr;
		m_pInspectSaveImgQ	= nullptr;

		m_StatePADInspect	= PAD_INSPECTION_IDLE;
	}

} PROC_PAD, *LPPAD_PROCESS;

//Result Process Def.
UINT fn_PadInspectProcess(LPVOID pParam);


inline void G_CalcImgPADPosToCood(int *pFragmentStep, int *pFragmentSizeH, CoordinatesProc*pCoordObj, ComRect* pCoodArea, DEFECT_DATA *m_pSearchRetList, COORD_DINFO *pCoordinateRetList);

#ifdef TEST_DEFECT_IMG_IN_ORG
inline bool G_CalcInImageDefPosAndSize_PAD(CRect *pImagePos, CRect *pDefecPos, HW_PARAM *pHWParam);
inline void G_CalcDefImgAndMask_OnOrgImg_PAD(IplImage *pImageBuffer, IplImage *pSaveImageBuffer,CRect* pDefecArea, int ScanLineNum, bool bSizeError);

#else
//모두 중앙으로 보내기.
inline bool G_CalcInImageDefPosAndSize_PAD(int *pPasteStartX, int *pPasteStartY,CRect *pImagePos, CRect *pDefecPos, HW_PARAM *pHWParam);
inline void G_CalcDefImgAndMask_OnOrgImg_PAD(int *pPasteStartX, int *pPasteStartY, IplImage *pImageBuffer, IplImage *pSaveImageBuffer, CRect* pDefecArea, int ScanLineNum, bool bSizeError);
#endif