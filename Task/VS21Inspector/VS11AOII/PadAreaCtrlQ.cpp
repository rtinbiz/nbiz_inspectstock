#include "StdAfx.h"
#include "PadAreaCtrlQ.h"

#ifdef VIEW_CREATE_SCAN_PAD2
IplImage *pViewMap = nullptr;
#endif

#ifdef TEST_PAD_UPDN_ALIGN	
int saveINdex = 0;
IplImage *pPadTestImgeUp = nullptr;
IplImage *pPadTestImgeDn = nullptr;

#endif
char AlignResultPath[512];

PAD_ImageCtrlQ::PAD_ImageCtrlQ(void)
{
	InitializeCriticalSection(&m_CrtSection);
	m_HeadNode.m_pFrontNode	= nullptr;
	m_HeadNode.m_pNextNode	= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_pWorkNode						= nullptr;
	m_pCopyNode						= nullptr;
	m_NodeCount					= 0;

	m_ImgFragmentatCnt		= 0;

	m_PadIndex						= -1;
	m_ScanLineNum				= -1;

	m_PadAlignImage				= nullptr;

	m_pCoordObj					= nullptr;

	m_BufferSwapImg				= nullptr;
}


PAD_ImageCtrlQ::~PAD_ImageCtrlQ(void)
{
	EnterCriticalSection(&m_CrtSection);
	{
		InspectPadDataQ * pDeleteNode = NULL;

		if(m_HeadNode.m_pNextNode != &m_TailNode)
		{
			pDeleteNode =  m_HeadNode.m_pNextNode;

			InspectPadDataQ * pNextNodeBuf = NULL;
			while (pDeleteNode != &m_TailNode)
			{

				pNextNodeBuf = pDeleteNode->m_pNextNode;

				delete pDeleteNode;

				pDeleteNode = pNextNodeBuf;
				pNextNodeBuf = NULL;
			}
			m_HeadNode.m_pFrontNode		= NULL;
			m_HeadNode.m_pNextNode		= &m_TailNode;

			m_TailNode.m_pFrontNode		= &m_HeadNode;
			m_TailNode.m_pNextNode		= NULL;

			m_NodeCount			= 0;
		}
	}
	if(m_PadAlignImage != nullptr)
		cvReleaseImage(&m_PadAlignImage);
	m_PadAlignImage = nullptr;

	if(m_BufferSwapImg != nullptr)
		cvReleaseImage(&m_BufferSwapImg);
	m_BufferSwapImg = nullptr;
	LeaveCriticalSection(&m_CrtSection);
	DeleteCriticalSection(&m_CrtSection);
}
bool PAD_ImageCtrlQ::m_fnCreatePadAreaMem(int PadPosition, CoordinatesProc*	pCoordObj, SAOI_IMAGE_INFO*pInspectImgInfo, SAOI_COMMON_PARA *pInspectComParam)
{
	QDeleteAll();
	EnterCriticalSection(&m_CrtSection);
	bool retValue = true;

	if(pCoordObj == nullptr)
		return false;
	m_PadPosition = PadPosition;
	
	m_pPADMaskImageInfo	= pInspectImgInfo;
	m_pPADCommonPara		= pInspectComParam;
	
	m_pCoordObj =  pCoordObj;

	CRect PadArea;
	//switch(PadPosition)
	//{
	//case 0: //상
	//	m_PadDeadZone		= (int)(pCoordObj->m_GlassParam.PadStartDeadZoneDisY/pCoordObj->m_GlassParam.m_ScanImgResolution_H);
	//	m_PadDNDeadZoneW	= 0;
	//	m_PadDNDeadZoneH	= 0;
	//	PadArea = pCoordObj->m_pPadCellList[0].m_CellPos_Top;
	//	break;
	//case 1: //하
	//	m_PadDeadZone		= (int)(pCoordObj->m_GlassParam.PadEndDeadZoneDisY/pCoordObj->m_GlassParam.m_ScanImgResolution_H);
	//	m_PadDNDeadZoneW	= (int)(pCoordObj->m_GlassParam.PadDnDeadZoneWidth/pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	//	m_PadDNDeadZoneH	= (int)(pCoordObj->m_GlassParam.PadDnDeadZoneHeight/pCoordObj->m_GlassParam.m_ScanImgResolution_H);
	//	PadArea = pCoordObj->m_pPadCellList[0].m_CellPos_Bottom;
	//	break;
	//case 2: //좌
	//	m_PadDeadZone		= (int)(pCoordObj->m_GlassParam.PadStartDeadZoneDisX/pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	//	m_PadDNDeadZoneW	= 0;
	//	m_PadDNDeadZoneH	= 0;
	//	PadArea = pCoordObj->m_pPadCellList[0].m_CellPos_Left;			
	//	break;
	//case 3: //우
	//	m_PadDeadZone		= (int)(pCoordObj->m_GlassParam.PadEndDeadZoneDisX/pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	//	m_PadDNDeadZoneW	= 0;
	//	m_PadDNDeadZoneH	= 0;
	//	PadArea = pCoordObj->m_pPadCellList[0].m_CellPos_Right;		
	//	break;
	//}

	switch(PadPosition)
	{
	case 0: //상
		m_PadDeadZone		= pCoordObj->m_GlassParam.PadStartDeadZoneDisY;
		m_PadDeadZonePixel	= (int)(pCoordObj->m_GlassParam.PadStartDeadZoneDisY/pCoordObj->m_GlassParam.m_ScanImgResolution_H);
		m_PadDNDeadZoneW	= 0;
		m_PadDNDeadZoneH	= 0;
		PadArea = pCoordObj->m_pPadCellList[0].m_CellPos_Top;
		break;
	case 1: //하
		m_PadDeadZone		= pCoordObj->m_GlassParam.PadEndDeadZoneDisY;
		m_PadDeadZonePixel	= (int)(pCoordObj->m_GlassParam.PadEndDeadZoneDisY/pCoordObj->m_GlassParam.m_ScanImgResolution_H);
		m_PadDNDeadZoneW	= pCoordObj->m_GlassParam.PadDnDeadZoneWidth;
		m_PadDNDeadZoneH	= pCoordObj->m_GlassParam.PadDnDeadZoneHeight;
		PadArea = pCoordObj->m_pPadCellList[0].m_CellPos_Bottom;
		break;
	case 2: //좌
		m_PadDeadZone		= pCoordObj->m_GlassParam.PadStartDeadZoneDisX;
		m_PadDeadZonePixel	= (int)(pCoordObj->m_GlassParam.PadStartDeadZoneDisX/pCoordObj->m_GlassParam.m_ScanImgResolution_W);
		m_PadDNDeadZoneW	= 0;
		m_PadDNDeadZoneH	= 0;
		PadArea = pCoordObj->m_pPadCellList[0].m_CellPos_Left;			
		break;
	case 3: //우
		m_PadDeadZone		= pCoordObj->m_GlassParam.PadEndDeadZoneDisX;
		m_PadDeadZonePixel	= (int)(pCoordObj->m_GlassParam.PadEndDeadZoneDisX/pCoordObj->m_GlassParam.m_ScanImgResolution_W);
		m_PadDNDeadZoneW	= 0;
		m_PadDNDeadZoneH	= 0;
		PadArea = pCoordObj->m_pPadCellList[0].m_CellPos_Right;		
		break;
	}

	InspectPadDataQ *pNewNode = nullptr;
	int PadImgHeight = (int)((float)PadArea.Height() / pCoordObj->m_GlassParam.m_ScanImgResolution_H);

	//Pad부 Search를 할때 몇개로 나눌지 결정.
	m_ImgFragmentatCnt	= (PadImgHeight/(pCoordObj->m_HWParam.m_ProcImageH+(pCoordObj->m_AXIS_Y_OVER_STEP*2))) + (((PadImgHeight%(pCoordObj->m_HWParam.m_ProcImageH+(pCoordObj->m_AXIS_Y_OVER_STEP*2)))>0)?1:0);

	int  ImgFragmentatH = PadImgHeight;//pCoordObj->m_HWParam.m_ProcImageH;
	if(m_ImgFragmentatCnt > 1)
	{
		PadImgHeight		= pCoordObj->m_HWParam.m_ProcImageH*m_ImgFragmentatCnt;
		ImgFragmentatH	= pCoordObj->m_HWParam.m_ProcImageH+(pCoordObj->m_AXIS_Y_OVER_STEP*2);
	}
	//else
	//{

	//}

	if(m_BufferSwapImg!= nullptr)
		cvReleaseImage(&m_BufferSwapImg);
	m_BufferSwapImg =  cvCreateImage(cvSize(pCoordObj->m_HWParam.m_ProcImageW, ImgFragmentatH), IPL_DEPTH_8U , 1);//cvCloneImage(pNextNode->m_pPADAreaImg);
	//int  ImgFragmentatH = pCoordObj->m_HWParam.m_ProcImageH;
	//Scan 방향 Y축에 대해 Cell 겟수가 Pad부의 겟수가 된다.
	for(int PadInStep = pCoordObj->m_GlassParam.GLASS_CELL_COUNT_Y; PadInStep>0; PadInStep--)
	{
		//Memory 영역 설정.
		pNewNode = new InspectPadDataQ(PadInStep, 
														pCoordObj->m_HWParam.m_ProcImageW, 
														PadImgHeight,
														ImgFragmentatH,//입력 Image Size만큼만 단편화 한다.
														pCoordObj->m_HWParam.m_DefectMaxCntInImg);// 
														//pCoordObj->m_HWParam.m_DefectReviewSizeW, 
														//pCoordObj->m_HWParam.m_DefectReviewSizeH);
		//단편화 조각 Send Step을 Main에서 지정 한다.
		pNewNode->m_pImgFragmentatStep = &m_ImgFragmentatStep;

		QCreateNode(pNewNode);
	}


	//AOI Inspector Interface Link.
	fn_QPutPADInspector = _AOI_Pad_PushItem;

	m_pPADCommonPara->nImageSizeX = pCoordObj->m_HWParam.m_ProcImageW;
	m_pPADCommonPara->nImageSizeY = ImgFragmentatH;

	_AOI_Pad_SearchParameterSet(*m_pPADMaskImageInfo, *m_pPADCommonPara, m_PadPosition);

	LeaveCriticalSection(&m_CrtSection);

	return retValue;
}

#ifdef VIEW_CREATE_SCAN_PAD2
IplImage *pViewMap3 = nullptr;
#endif
bool PAD_ImageCtrlQ::m_fnSetPadMPosition(int ScanLineNum, CoordinatesProc*	pCoordObj)
{
	QCleanAllData();
	EnterCriticalSection(&m_CrtSection);
	

	bool retval = true;
	if(ScanLineNum<=0)
		return false;

	m_ImgFragmentatStep = 0;

	InspectPadDataQ * pSetNode = m_HeadNode.m_pNextNode;
	
	m_pCopyNode = m_HeadNode.m_pNextNode;

	int ScanImgIndex = pCoordObj->m_OneLineScnaImgCnt*(ScanLineNum-1);//대상 Scan Line의 첫번째 Grab Image 위치 이다.

	SCANIMGINFO *pFirstScanImg = &pCoordObj->m_pScanImgList[ScanImgIndex];
	CRect PadArea;
#ifdef VIEW_CREATE_SCAN_PAD3
	if(pViewMap3 == nullptr)
	{
		pViewMap3 = cvCreateImage(cvSize(850, 1050), 8, 3);
		cvNamedWindow("ScnaMapViewer3");
	}
	//for(int PadInStepY = 0; PadInStepY<pCoordObj->m_GlassParam.GLASS_CELL_COUNT_Y; PadInStepY++)
	//{
	//	for(int PadInStepX = 0; PadInStepX<pCoordObj->m_GlassParam.GLASS_CELL_COUNT_X; PadInStepX++)
	//	{
	//		PadArea = pCoordObj->m_pPadCellList[PadInStepX+pCoordObj->m_GlassParam.GLASS_CELL_COUNT_X*PadInStepY].m_CellPos_Top;

	//		cvRectangle(pViewMap3, cvPoint(PadArea.left/1000, PadArea.top/1000),
	//								cvPoint(PadArea.right/1000, PadArea.bottom/1000), CV_RGB(255, 0, 0));
	//	}
	//}
#endif
	ComRect PadAreaInScan;
	int CellCntInBlockX = pCoordObj->m_GlassParam.GLASS_CELL_COUNT_X/pCoordObj->m_GlassParam.GLASS_BLOCK_COUNT_X;
	int CellCntInBlockY = pCoordObj->m_GlassParam.GLASS_CELL_COUNT_Y/pCoordObj->m_GlassParam.GLASS_BLOCK_COUNT_Y;
	CRect CheckPosFirst;
	switch(m_PadPosition)
	{
	case 0: //상
		CheckPosFirst = pCoordObj->m_pPadCellList[0].m_CellPos_Top;
		break;
	case 1: //하
		CheckPosFirst = pCoordObj->m_pPadCellList[0].m_CellPos_Bottom;	
		break;
	case 2: //좌
		CheckPosFirst = pCoordObj->m_pPadCellList[0].m_CellPos_Left;
		break;
	case 3://우
		CheckPosFirst = pCoordObj->m_pPadCellList[0].m_CellPos_Right;
		break;
	default:
		return false;
	}
	for(int PadInStep = 0; PadInStep<pCoordObj->m_GlassParam.GLASS_CELL_COUNT_Y*pCoordObj->m_GlassParam.GLASS_CELL_COUNT_X; PadInStep++)
	{

		switch(m_PadPosition)
		{
		case 0: //상
			PadArea = pCoordObj->m_pPadCellList[PadInStep].m_CellPos_Top;
			break;
		case 1: //하
			PadArea = pCoordObj->m_pPadCellList[PadInStep].m_CellPos_Bottom;	
			break;
		case 2: //좌
			PadArea = pCoordObj->m_pPadCellList[PadInStep].m_CellPos_Left;
			break;
		case 3://우
			PadArea = pCoordObj->m_pPadCellList[PadInStep].m_CellPos_Right;
			break;
		default:
			return false;
		}
		if(pCoordObj->m_RealScanType == -1 || pCoordObj->m_RealScanType == 0)//Calc Or Half Scan
		{
			if(CheckPosFirst.left != PadArea.left)
				continue;
		}
		else if(pCoordObj->m_RealScanType == 1)//Full Scan
		{
			//full real Scan일때는 Check하지 않는다.
			if(abs(CheckPosFirst.left - PadArea.left)>30000)
				continue;
		}
		

#ifdef VIEW_CREATE_SCAN_PAD3

		PadAreaInScan.left			= pFirstScanImg->m_ScanImgPos.left;
		PadAreaInScan.top			= PadArea.top;
		PadAreaInScan.right			= PadAreaInScan.left + (int)((float) pCoordObj->m_HWParam.m_ProcImageW*pCoordObj->m_GlassParam.m_ScanImgResolution_W);
		PadAreaInScan.bottom		= PadAreaInScan.top + (int)((float) pSetNode->m_pPADAreaImg->height*pCoordObj->m_GlassParam.m_ScanImgResolution_H);

		if(m_PadPosition == 0)
		{
			cvRectangle(pViewMap3, cvPoint(PadArea.left/1000, PadArea.top/1000),
				cvPoint(PadArea.right/1000, PadArea.bottom/1000), CV_RGB(255, 0, 0));

			cvRectangle(pViewMap3, cvPoint(PadAreaInScan.left/1000, PadAreaInScan.top/1000),
				cvPoint(PadAreaInScan.right/1000, PadAreaInScan.bottom/1000), CV_RGB(255, 255, 255));//,CV_FILLED);


			TRACE("\r\n ====>>>>%02d_Step 0 Pos: (SX:%6d, SY:%6d) (EX:%6d, EX:%6d)",PadInStep, PadAreaInScan.left,PadAreaInScan.top, PadAreaInScan.right, PadAreaInScan.bottom);
		}
		cvShowImage("ScnaMapViewer3", pViewMap3);
		cvWaitKey(10);
#endif
		if(pSetNode != &m_TailNode)
		{
			//Scan Line상의 Pad부의 크기를 Machine좌표계로 환산 저장 한다.
			PadAreaInScan.left			= pFirstScanImg->m_ScanImgPos.left;
			PadAreaInScan.top			= PadArea.top;
			PadAreaInScan.right			= PadAreaInScan.left + (int)((float) pCoordObj->m_HWParam.m_ProcImageW*pCoordObj->m_GlassParam.m_ScanImgResolution_W);
			PadAreaInScan.bottom		= PadAreaInScan.top + (int)((float) pSetNode->m_pPADAreaImg->height*pCoordObj->m_GlassParam.m_ScanImgResolution_H);

			pSetNode->m_fnQSetPadMPosition(&PadAreaInScan);
			pSetNode = pSetNode->m_pNextNode;
		}
		else
			retval = false;				
	}
	if(retval == true)
		m_pWorkNode = m_HeadNode.m_pNextNode;//최초 Pad부 Grab등록.
	//////////////////////////////////////////////////////////////////////////
	//Scan 영역 안에 Pad부가 들어 오는지 검사 안들어오면 제거.
	//pSetNode = m_HeadNode.m_pNextNode;
	//if(pSetNode != &m_TailNode)
	//{
	//	CRect ScanLineArea;
	//
	//	ScanLineArea.left				= pSetNode->m_PadInScanPos.left;
	//	ScanLineArea.right			= pSetNode->m_PadInScanPos.right;
	//	ScanLineArea.top				= ;
	//	ScanLineArea.bottom		= ;

	//	for(int PadInStep = 0; PadInStep<pCoordObj->m_GlassParam.GLASS_CELL_COUNT_Y; PadInStep++)
	//	{
	//		if(pSetNode != &m_TailNode)
	//		{
	//			pSetNode->m_PadInScanPos;
	//			pSetNode = pSetNode->m_pNextNode;
	//		}
	//		else
	//			retval = false;				
	//	}
	//}

	LeaveCriticalSection(&m_CrtSection);
	return retval;
}
void PAD_ImageCtrlQ::QCreateNode(InspectPadDataQ * pNewNode)
{
	EnterCriticalSection(&m_CrtSection);

	//Link 구성.
	pNewNode->m_pFrontNode						= &m_HeadNode;
	pNewNode->m_pNextNode							= m_HeadNode.m_pNextNode;

	m_HeadNode.m_pNextNode						= pNewNode;
	(pNewNode->m_pNextNode)->m_pFrontNode	= pNewNode;

	m_NodeCount++;
	LeaveCriticalSection(&m_CrtSection);
}
void PAD_ImageCtrlQ::QCleanAllData(void)
{

#ifdef VIEW_CREATE_SCAN_PAD2
	if(pViewMap != nullptr)
		cvZero(pViewMap);
	cvNamedWindow("ScnaMapViewer2");
#endif
	EnterCriticalSection(&m_CrtSection);
	InspectPadDataQ * pCleanNode = nullptr;

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pCleanNode =  m_HeadNode.m_pNextNode;

	InspectPadDataQ * pNextNodeBuf = nullptr;
	while (pCleanNode != &m_TailNode)
	{
		pNextNodeBuf = pCleanNode->m_pNextNode;
		
		pCleanNode->m_fnQCleanData();

		pCleanNode = pNextNodeBuf;
		pNextNodeBuf = nullptr;
	}
	LeaveCriticalSection(&m_CrtSection);
}
void PAD_ImageCtrlQ::QDeleteAll(void)
{
	EnterCriticalSection(&m_CrtSection);
	InspectPadDataQ * pDeleteNode = nullptr;

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return;
	}
	pDeleteNode =  m_HeadNode.m_pNextNode;

	InspectPadDataQ * pNextNodeBuf = nullptr;
	while (pDeleteNode != &m_TailNode)
	{
		pNextNodeBuf = pDeleteNode->m_pNextNode;
		delete pDeleteNode;

		pDeleteNode = pNextNodeBuf;
		pNextNodeBuf = nullptr;
	}

	m_HeadNode.m_pFrontNode		= nullptr;
	m_HeadNode.m_pNextNode		= &m_TailNode;

	m_TailNode.m_pFrontNode		= &m_HeadNode;
	m_TailNode.m_pNextNode		= nullptr;

	m_NodeCount = 0;
	LeaveCriticalSection(&m_CrtSection);
}
//extern PROC_PAD_ALIGN			G_ProcPadAlign;
bool PAD_ImageCtrlQ::m_fnPADImageAlign_I(int ScanLineNum)
{

	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		return false;
	}
	InspectPadDataQ	*	pAlignMarkNode = m_HeadNode.m_pNextNode;
	//Find 1번Cell의 위치.
	//H/W좌표 계산.
	CvScalar DeadZoneColor;
	DeadZoneColor = CV_RGB(255,255,255);
	CvPoint StartPt, EndPt;
	CRect PAD_DN_List[4];
	memset(&PAD_DN_List, 0x00, sizeof(CRect)*4);
	CRect FindPadList[4];
	memset(&FindPadList, 0x00, sizeof(CRect)*4);
	int FindPadCnt = 0;
	int ActiveDeadZonePixelSize = (int)(m_pCoordObj->m_GlassParam.ActiveDeadZoneSize/m_pCoordObj->m_GlassParam.m_ScanImgResolution_H);

	CRect		*pFirstImgPos  = 		(CRect		*) &pAlignMarkNode->m_PadInScanPos;
	if(m_PadIndex == 1)//PAD가 Down일때만.
	{

		m_pCoordObj->m_fnFindRectCellPadInfo(m_PadIndex, pFirstImgPos, &FindPadCnt,   &FindPadList[0]);
		if(m_PadDNDeadZoneH>0 && m_PadDNDeadZoneW>0 && FindPadCnt>0)
		{
			CRect CheckRect;
			memset(&CheckRect, 0x00, sizeof(CRect));
			//하부 추가 Dead Zone(H/W좌표 계산)
			for(int InPadStep = 0; InPadStep<FindPadCnt; InPadStep++)
			{

				//Image 좌표로 환산.
				if(CheckRect.IntersectRect(pFirstImgPos, FindPadList[InPadStep]) == TRUE)
				{
					if(CheckRect.left == pFirstImgPos->left)
					{//오른쪽에 나올것
						CheckRect.left  = CheckRect.right - m_PadDNDeadZoneW;
						CheckRect.top  = CheckRect.bottom - (m_PadDNDeadZoneH + m_pCoordObj->m_GlassParam.ActiveDeadZoneSize);
					}
					else
					{//왼쪽에 그릴것
						CheckRect.right = CheckRect.left + m_PadDNDeadZoneW;
						CheckRect.top  = CheckRect.bottom - (m_PadDNDeadZoneH + m_pCoordObj->m_GlassParam.ActiveDeadZoneSize);
					}

					CheckRect.left = CheckRect.left-pFirstImgPos->left;
					CheckRect.top = CheckRect.top-pFirstImgPos->top;
					CheckRect.right = CheckRect.right-pFirstImgPos->left;
					CheckRect.bottom = CheckRect.bottom-pFirstImgPos->top;

					CheckRect.left		= (int)(CheckRect.left / m_pCoordObj->m_GlassParam.m_ScanImgResolution_W);
					CheckRect.top		= (int)(CheckRect.top / m_pCoordObj->m_GlassParam.m_ScanImgResolution_H);
					CheckRect.right		= (int)(CheckRect.right / m_pCoordObj->m_GlassParam.m_ScanImgResolution_W);
					CheckRect.bottom	= (int)(CheckRect.bottom / m_pCoordObj->m_GlassParam.m_ScanImgResolution_H);

					//Resolution 오차 극복.(연산 주체가 달라서 1pixel정도의 오차발생)
					CheckRect.left -=3;
					CheckRect.right +=3;
					CheckRect.top -=3;
					CheckRect.bottom +=3;
					PAD_DN_List[InPadStep] = CheckRect;
				}
				else
					memset(&CheckRect, 0x00, sizeof(CRect));

			}
		}
	}
	
	IplImage *pImgPadImg = pAlignMarkNode->m_pPADAreaImg;	
	//////////////////////////////////////////////////////////////////////////

	if(m_PadIndex == 0 || m_PadIndex == 1)//상, 하
	{
		if((pFirstImgPos->Height() - m_pCoordObj->m_GlassParam.ActiveDeadZoneSize*3)<=0)
		{//Pad부가 없다는것.
			return false;
		}

		//int AlignImg_H = ((int)(m_pCoordObj->m_GlassParam.ActiveDeadZoneSize/m_pCoordObj->m_GlassParam.m_ScanImgResolution_H));
		if(pImgPadImg->width ==0 || (m_PadAlignCoordData.m_BigAreaEndX<0 || m_PadAlignCoordData.m_BigAreaStartX<0))
			return false;

		//1) Align Mark 위치 선정.
		CvRect AlignImgRect;
		AlignImgRect.height = ActiveDeadZonePixelSize;
		AlignImgRect.width = G_ProcPadAlign.m_AlignWidth;
		AlignImgRect.x = m_PadAlignCoordData.m_BigAreaStartX+(abs(m_PadAlignCoordData.m_BigAreaEndX - m_PadAlignCoordData.m_BigAreaStartX)/2) - (G_ProcPadAlign.m_AlignWidth/2);

		if(m_PadIndex == 0 )//상부
			AlignImgRect.y = pImgPadImg->height - ((ActiveDeadZonePixelSize*2) + (ActiveDeadZonePixelSize/2));
		else//하부
			AlignImgRect.y = ActiveDeadZonePixelSize +  (ActiveDeadZonePixelSize/2);
		CvPoint AlignImgOrgPos;//원판 Center좌표설정.
		AlignImgOrgPos.x = AlignImgRect.x + (AlignImgRect.width/2);
		AlignImgOrgPos.y = AlignImgRect.y + (AlignImgRect.height/2);

		//2) Align Mark 취득.
		if(m_PadAlignImage != nullptr)
			cvReleaseImage(&m_PadAlignImage);
		m_PadAlignImage = cvCreateImage(cvSize(AlignImgRect.width, AlignImgRect.height), IPL_DEPTH_8U, 1);

		cvSetImageROI(pImgPadImg, AlignImgRect);
		cvCopyImage(pImgPadImg, m_PadAlignImage);
		cvResetImageROI(pImgPadImg);

//#ifdef  TEST_PAD_UPDN_ALIGN

		if(G_ProcPadAlign.m_AlignResultSave == true)
		{
			sprintf_s(AlignResultPath, "%sD%d_AlignPad_%s.jpg",TEST_PAD_ALIGN_IMG_SAVE_PATH, ScanLineNum, (m_PadIndex==0?"Up":"Dn"));
			cvSaveImage(AlignResultPath, m_PadAlignImage);
		}
//#endif

		//3) 검사 영역 설정.
		int AlignHight = ActiveDeadZonePixelSize*4;
		AlignHight =  (pImgPadImg->height<AlignHight)?pImgPadImg->height:AlignHight;
		CvRect FindROIRect;
		FindROIRect = AlignImgRect;
		FindROIRect.y = 0;
		FindROIRect.x -= G_ProcPadAlign.m_AlignOverWidth;
		FindROIRect.width += G_ProcPadAlign.m_AlignOverWidth*2;
		FindROIRect.height = AlignHight;// (pImgPadImg->height<AlignHight)?pImgPadImg->height:AlignHight;
		if(m_PadIndex == 0)//상부이면 아래쪽에서 Align 위치( Filnd Area를 찾는다.)
			FindROIRect.y = pImgPadImg->height - AlignHight;

		if(FindROIRect.y<=0)
			FindROIRect.y = 0;
			
		IplImage* imgBuffer = cvCreateImage( cvSize( FindROIRect.width - m_PadAlignImage->width+1, FindROIRect.height - m_PadAlignImage->height+1 ), IPL_DEPTH_32F, 1 ); // 상관계수를 구할 이미지(C)

		InspectPadDataQ	*	pNextNode = pAlignMarkNode->m_pNextNode;

		int CntValue = 1;
		while(pNextNode != &m_TailNode)
		{
			double min, max;
			CvPoint FindAlignPosCenter; 
			//위치 찾기
			//cvZero(imgBuffer);
			cvSetImageROI(pNextNode->m_pPADAreaImg, FindROIRect);
			cvMatchTemplate(pNextNode->m_pPADAreaImg, m_PadAlignImage, imgBuffer, CV_TM_CCOEFF_NORMED); // 상관계수를 구하여 C 에 그린다.
			cvMinMaxLoc(imgBuffer, &min, &max, NULL, &FindAlignPosCenter); // 상관계수가 최대값을 값는 위치 찾기 


//#ifdef  TEST_PAD_UPDN_ALIGN
			if(G_ProcPadAlign.m_AlignResultSave == true)
			{
#ifndef  TEST_PAD_UPDN_ALIGN
				IplImage *pSaveAlignImage = cvCloneImage(pNextNode->m_pPADAreaImg);
#else
				IplImage *pSaveAlignImage = pNextNode->m_pPADAreaImg;
#endif				
				cvDrawRect(pSaveAlignImage, FindAlignPosCenter, cvPoint(FindAlignPosCenter.x +AlignImgRect.width, FindAlignPosCenter.y +AlignImgRect.height), CV_RGB(255,255,255), 3);
				cvDrawRect(pSaveAlignImage, FindAlignPosCenter, cvPoint(FindAlignPosCenter.x +AlignImgRect.width, FindAlignPosCenter.y +AlignImgRect.height), CV_RGB(0,0,0), 1);

				sprintf_s(AlignResultPath, "%0.2f", max);
				CvPoint PrintTxtPos = cvPoint(10,10);
				PutTextInImageDefect(pSaveAlignImage, &PrintTxtPos, AlignResultPath);

				PrintTxtPos.y+=15;
				sprintf_s(AlignResultPath, "X:%4d",FindAlignPosCenter.x+ (pSaveAlignImage->width/2));
				PutTextInImageDefect(pSaveAlignImage, &PrintTxtPos, AlignResultPath);

				PrintTxtPos.y+=15;
				sprintf_s(AlignResultPath, "Y:%4d",FindAlignPosCenter.y+ (pSaveAlignImage->height/2));
				PutTextInImageDefect(pSaveAlignImage, &PrintTxtPos, AlignResultPath);

				sprintf_s(AlignResultPath, "%sD%d_AlignPad_%s_%d.jpg",TEST_PAD_ALIGN_IMG_SAVE_PATH, ScanLineNum, (m_PadIndex==0?"Up":"Dn"), ++CntValue);
				cvSaveImage(AlignResultPath, pSaveAlignImage);
#ifndef  TEST_PAD_UPDN_ALIGN
				cvReleaseImage(&pSaveAlignImage);
#endif	
			}
//#endif
			cvResetImageROI(pNextNode->m_pPADAreaImg);
#ifdef  TEST_PAD_UPDN_ALIGN
			sprintf_s(AlignResultPath, "%sD%d_AlignPad_%s_%d_ORG.jpg",TEST_PAD_ALIGN_IMG_SAVE_PATH, ScanLineNum, (m_PadIndex==0?"Up":"Dn"), CntValue);
			cvSaveImage(AlignResultPath, pNextNode->m_pPADAreaImg);
#endif	
			//지정 값미만이면 화이트 처리 한다.
			if((double)(G_ProcPadAlign.m_AlignOKValue/100.0)>max)
			{
#ifdef  TEST_PAD_UPDN_ALIGN
				sprintf_s(AlignResultPath, "%sD%d_AlignPad_%s_%d_Error.jpg",TEST_PAD_ALIGN_IMG_SAVE_PATH, ScanLineNum, (m_PadIndex==0?"Up":"Dn"), CntValue);
				cvSaveImage(AlignResultPath, pNextNode->m_pPADAreaImg);
#endif	
				memset(pNextNode->m_pPADAreaImg->imageData, 0xFF, pNextNode->m_pPADAreaImg->imageSize);
			}
			else
			{
				FindAlignPosCenter.x+= FindROIRect.x + (m_PadAlignImage->width/2);
				FindAlignPosCenter.y+= FindROIRect.y + (m_PadAlignImage->height/2);
				//Image Shift (up/down)
				int ShifXDistance = FindAlignPosCenter.x - AlignImgOrgPos.x;
				int ShifYDistance = FindAlignPosCenter.y - AlignImgOrgPos.y;
				if(ShifYDistance != 0 || ShifXDistance != 0)
				{
					//Image상의 Defect위치를 계산하기전 Align 된정보로 좌표를 통체로 수정 하자.(원래 여기 나와야 한다는 의미이다.)
					//Pad부 Image좌표는 1번위치를 제외 하고는 오차 많큼 변경 될것이다.
					pNextNode->m_PadInScanPos.left	+=(int)(ShifXDistance*m_pCoordObj->m_GlassParam.m_ScanImgResolution_W)*(-1);
					pNextNode->m_PadInScanPos.right	+=(int)(ShifXDistance*m_pCoordObj->m_GlassParam.m_ScanImgResolution_W)*(-1);

					pNextNode->m_PadInScanPos.top		+=(int)(ShifYDistance*m_pCoordObj->m_GlassParam.m_ScanImgResolution_H)*(-1);
					pNextNode->m_PadInScanPos.bottom	+=(int)(ShifYDistance*m_pCoordObj->m_GlassParam.m_ScanImgResolution_H)*(-1);
					//IplImage *pShiftImage = cvCreateImage(cvSize(pNextNode->m_pPADAreaImg->width, pNextNode->m_pPADAreaImg->height), IPL_DEPTH_8U , 1);//cvCloneImage(pNextNode->m_pPADAreaImg);
					cvCopyImage(pNextNode->m_pPADAreaImg, m_BufferSwapImg);
					memset(pNextNode->m_pPADAreaImg->imageData, 0xFF, pNextNode->m_pPADAreaImg->imageSize);

					CvRect MoveArea, ShiftPosArea;
					MoveArea.x = ShifXDistance>0? ShifXDistance:0;
					MoveArea.y = ShifYDistance>0? ShifYDistance:0;
					MoveArea.width = pNextNode->m_pPADAreaImg->width -abs(ShifXDistance);
					MoveArea.height= pNextNode->m_pPADAreaImg->height -abs(ShifYDistance);

					ShiftPosArea	= MoveArea;
					ShiftPosArea.x	= ShifXDistance>0? 0:ShifXDistance*(-1);
					ShiftPosArea.y	= ShifYDistance>0? 0:ShifYDistance*(-1);

					cvSetImageROI(m_BufferSwapImg, MoveArea);
					cvSetImageROI(pNextNode->m_pPADAreaImg, ShiftPosArea);
					cvCopyImage(m_BufferSwapImg, pNextNode->m_pPADAreaImg);
					cvResetImageROI(m_BufferSwapImg);
					cvResetImageROI(pNextNode->m_pPADAreaImg);

					//cvReleaseImage(&pShiftImage);
				}
			}
			pNextNode = pNextNode->m_pNextNode;
			
		}
		//////////////////////////////////////////////////////////////////////////
		pNextNode = m_HeadNode.m_pNextNode;
		//Dead Zone 그리기.
		for(int NodeStep = 0; NodeStep < m_NodeCount && pNextNode != &m_TailNode; NodeStep++)
		{
			//Dead Zone 칠하기.
			if(m_PadIndex == 0 )//상부
			{
				//Pad부의 Dead Zone을 칠한다.
				//위쪽
				StartPt.x	= 0;
				StartPt.y	= 0;
				EndPt.x		= pNextNode->m_pPADAreaImg->width; 
				EndPt.y		= ActiveDeadZonePixelSize;
				cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
				//아래쪽
				StartPt.x	= 0;
				StartPt.y	= pNextNode->m_pPADAreaImg->height - (m_PadDeadZonePixel+ActiveDeadZonePixelSize*2);
				EndPt.x		= pNextNode->m_pPADAreaImg->width; 
				EndPt.y		= pNextNode->m_pPADAreaImg->height;
				cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
			}
			else//하부
			{
				//Pad부의 Dead Zone을 칠한다.
				//위쪽
				StartPt.x	= 0;
				StartPt.y	= 0;
				EndPt.x		= pNextNode->m_pPADAreaImg->width; 
				EndPt.y		= m_PadDeadZonePixel + ActiveDeadZonePixelSize*2;
				cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
				//아래쪽
				StartPt.x	= 0;
				StartPt.y	= pNextNode->m_pPADAreaImg->height - ActiveDeadZonePixelSize;
				EndPt.x		= pNextNode->m_pPADAreaImg->width; 
				EndPt.y		= pNextNode->m_pPADAreaImg->height;
				cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
				//////////////////////////////////////////////////////////////////////////
				for(int DrawStep = 0; DrawStep<FindPadCnt; DrawStep++)
				{
					if(PAD_DN_List[DrawStep].Width()>0 && PAD_DN_List[DrawStep].Height()>0)

						cvRectangle(pNextNode->m_pPADAreaImg, cvPoint(PAD_DN_List[DrawStep].left, PAD_DN_List[DrawStep].top), 
						cvPoint(PAD_DN_List[DrawStep].right, PAD_DN_List[DrawStep].bottom), DeadZoneColor, CV_FILLED );
				}
			}
			pNextNode = pNextNode->m_pNextNode;
		}
		cvReleaseImage(&imgBuffer);
	}
	else//좌, 우.
	{
			return false;
	}
	return true;
}
bool PAD_ImageCtrlQ::m_fnPADImageAlign_II(int ScanLineNum)
{
#ifdef TEST_PAD_UPDN_ALIGN	
	char LoadImageName[256];

	if(ScanLineNum>1)
	{
		InspectPadDataQ	*	pInputNode = m_HeadNode.m_pNextNode;
		for(int InputStep = 0; InputStep<m_NodeCount; InputStep++)
		{
			if(m_PadIndex == 0)
			{
				sprintf_s(LoadImageName,"D:\\nBiz_InspectStock\\Data\\IMG_1173_Up%d.bmp",  InputStep);
				if(pPadTestImgeUp != nullptr)
					cvReleaseImage(&pPadTestImgeUp);

				pPadTestImgeUp = cvLoadImage(LoadImageName, CV_LOAD_IMAGE_GRAYSCALE);
				cvCopyImage(pPadTestImgeUp, pInputNode->m_pPADAreaImg);

			}
			else
			{
				sprintf_s(LoadImageName,"D:\\nBiz_InspectStock\\Data\\IMG_1173_Dn%d.bmp",  InputStep);
				if(pPadTestImgeDn != nullptr)
					cvReleaseImage(&pPadTestImgeDn);
				pPadTestImgeDn = cvLoadImage(LoadImageName, CV_LOAD_IMAGE_GRAYSCALE);
				cvCopyImage(pPadTestImgeDn, pInputNode->m_pPADAreaImg);
			}
			pInputNode = pInputNode->m_pNextNode;
		}
	}
#endif
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		return false;
	}
	InspectPadDataQ	*	pAlignMarkNode = m_HeadNode.m_pNextNode;
	//Find 1번Cell의 위치.
	//H/W좌표 계산.
	CvScalar DeadZoneColor;
	DeadZoneColor = CV_RGB(0,0,0);
	CvPoint StartPt, EndPt;
	CRect PAD_DN_List[4];
	memset(&PAD_DN_List, 0x00, sizeof(CRect)*4);
	CRect FindPadList[4];
	memset(&FindPadList, 0x00, sizeof(CRect)*4);
	int FindPadCnt = 0;
	int ActiveDeadZonePixelSize = (int)(m_pCoordObj->m_GlassParam.ActiveDeadZoneSize/m_pCoordObj->m_GlassParam.m_ScanImgResolution_H);


	int SmallRight =  (int)(pAlignMarkNode->m_pPADAreaImg->width*m_pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	int SmallLeft  = 0;

	CRect		*pFirstImgPos  = 		(CRect		*) &pAlignMarkNode->m_PadInScanPos;
	m_pCoordObj->m_fnFindRectCellPadInfo(m_PadIndex, pFirstImgPos, &FindPadCnt,   &FindPadList[0]);

	for(int InPadStep = 0; InPadStep<FindPadCnt; InPadStep++)
	{
		CRect CheckRect;
		if(CheckRect.IntersectRect(pFirstImgPos, FindPadList[InPadStep]) == TRUE)
		{
			if(CheckRect.left == pFirstImgPos->left)
			{//
				SmallLeft = CheckRect.right-pFirstImgPos->left ;
			}
			else
			{//왼쪽에 그릴것
				SmallRight = CheckRect.left-pFirstImgPos->left ;
			}
		}
	}
	SmallLeft	= (int)(SmallLeft/m_pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	SmallRight	= (int)(SmallRight/m_pCoordObj->m_GlassParam.m_ScanImgResolution_W);


	if(m_PadIndex == 1)//PAD가 Down일때만.
	{
		if(m_PadDNDeadZoneH>0 && m_PadDNDeadZoneW>0 && FindPadCnt>0)
		{
			CRect CheckRect;
			memset(&CheckRect, 0x00, sizeof(CRect));
			//하부 추가 Dead Zone(H/W좌표 계산)
			for(int InPadStep = 0; InPadStep<FindPadCnt; InPadStep++)
			{

				//Image 좌표로 환산.
				if(CheckRect.IntersectRect(pFirstImgPos, FindPadList[InPadStep]) == TRUE)
				{
					if(CheckRect.left == pFirstImgPos->left)
					{//오른쪽에 나올것
						CheckRect.left  = CheckRect.right - m_PadDNDeadZoneW;
						CheckRect.top  = CheckRect.bottom - (m_PadDNDeadZoneH + m_pCoordObj->m_GlassParam.ActiveDeadZoneSize);
					}
					else
					{//왼쪽에 그릴것
						CheckRect.right = CheckRect.left + m_PadDNDeadZoneW;
						CheckRect.top  = CheckRect.bottom - (m_PadDNDeadZoneH + m_pCoordObj->m_GlassParam.ActiveDeadZoneSize);
					}

					CheckRect.left = CheckRect.left-pFirstImgPos->left;
					CheckRect.top = CheckRect.top-pFirstImgPos->top;
					CheckRect.right = CheckRect.right-pFirstImgPos->left;
					CheckRect.bottom = CheckRect.bottom-pFirstImgPos->top;

					CheckRect.left		= (int)(CheckRect.left / m_pCoordObj->m_GlassParam.m_ScanImgResolution_W);
					CheckRect.top		= (int)(CheckRect.top / m_pCoordObj->m_GlassParam.m_ScanImgResolution_H);
					CheckRect.right		= (int)(CheckRect.right / m_pCoordObj->m_GlassParam.m_ScanImgResolution_W);
					CheckRect.bottom	= (int)(CheckRect.bottom / m_pCoordObj->m_GlassParam.m_ScanImgResolution_H);

					//Resolution 오차 극복.(연산 주체가 달라서 1pixel정도의 오차발생)
					CheckRect.left -=3;
					CheckRect.right +=3;
					CheckRect.top -=3;
					CheckRect.bottom +=3;
					PAD_DN_List[InPadStep] = CheckRect;
				}
				else
					memset(&CheckRect, 0x00, sizeof(CRect));
			}
		}
	}

	int DeadZoneOverStep =0;
	IplImage *pImgPadImg = pAlignMarkNode->m_pPADAreaImg;	
	//////////////////////////////////////////////////////////////////////////

	if(m_PadIndex == 0 || m_PadIndex == 1)//상, 하
	{
		if((pFirstImgPos->Height() - m_pCoordObj->m_GlassParam.ActiveDeadZoneSize*3)<=0)
		{//Pad부가 없다는것.
			return false;
		}

		//int AlignImg_H = ((int)(m_pCoordObj->m_GlassParam.ActiveDeadZoneSize/m_pCoordObj->m_GlassParam.m_ScanImgResolution_H));
		if(pImgPadImg->width ==0 || (m_PadAlignCoordData.m_BigAreaEndX<0 || m_PadAlignCoordData.m_BigAreaStartX<0))
			return false;

		//1) Align Mark 위치 선정.
		CvRect AlignImgRect;
		if(G_ProcPadAlign.m_AlignUseMaxHigh == true)
		{
			//원래 영역을 모두 차지 하고, Dead Zosn영역을 Buff영역으로 삼는다.
			AlignImgRect.height = pImgPadImg->height -(ActiveDeadZonePixelSize*2);
		}
		else
			AlignImgRect.height = ActiveDeadZonePixelSize;

		AlignImgRect.width = G_ProcPadAlign.m_AlignWidth;
		AlignImgRect.x = m_PadAlignCoordData.m_BigAreaStartX+(abs(m_PadAlignCoordData.m_BigAreaEndX - m_PadAlignCoordData.m_BigAreaStartX)/2) - (G_ProcPadAlign.m_AlignWidth/2);

		if(G_ProcPadAlign.m_AlignUseMaxHigh == true)
		{
			AlignImgRect.y = ActiveDeadZonePixelSize;
		}
		else
		{
			if(m_PadIndex == 0 )//상부
				AlignImgRect.y = pImgPadImg->height - ((ActiveDeadZonePixelSize*2) + (ActiveDeadZonePixelSize/2));
			else//하부
				AlignImgRect.y = ActiveDeadZonePixelSize +  (ActiveDeadZonePixelSize/2);
		}


		CvPoint AlignImgOrgPos;//원판 Center좌표설정.
		AlignImgOrgPos.x = AlignImgRect.x + (AlignImgRect.width/2);
		AlignImgOrgPos.y = AlignImgRect.y + (AlignImgRect.height/2);

		//2) Align Mark 취득.
		if(m_PadAlignImage != nullptr)
			cvReleaseImage(&m_PadAlignImage);
		m_PadAlignImage = cvCreateImage(cvSize(AlignImgRect.width, AlignImgRect.height), IPL_DEPTH_8U, 1);

		cvSetImageROI(pImgPadImg, AlignImgRect);
		cvCopyImage(pImgPadImg, m_PadAlignImage);
		cvResetImageROI(pImgPadImg);

		if(G_ProcPadAlign.m_AlignImgUseCanny == true)
		{
			cvCanny(m_PadAlignImage, m_PadAlignImage, G_ProcPadAlign.m_AlignImgUseCanny_threshold1, G_ProcPadAlign.m_AlignImgUseCanny_threshold2);
		}
		//#ifdef  TEST_PAD_UPDN_ALIGN


		//#endif

		//3) 검사 영역 설정.
		int AlignHight = ActiveDeadZonePixelSize*4;
		if(G_ProcPadAlign.m_AlignUseMaxHigh == true)
			AlignHight =  pImgPadImg->height;
		else
			AlignHight =  (pImgPadImg->height<AlignHight)?pImgPadImg->height:AlignHight;

		CvRect FindROIRect;
		FindROIRect = AlignImgRect;
		FindROIRect.y = 0;
		FindROIRect.x -= G_ProcPadAlign.m_AlignOverWidth;
		FindROIRect.width += G_ProcPadAlign.m_AlignOverWidth*2;
		FindROIRect.height = AlignHight;// (pImgPadImg->height<AlignHight)?pImgPadImg->height:AlignHight;
		if(m_PadIndex == 0)//상부이면 아래쪽에서 Align 위치( Filnd Area를 찾는다.)
			FindROIRect.y = pImgPadImg->height - AlignHight;

		if(FindROIRect.y<=0)
			FindROIRect.y = 0;

		IplImage* imgBuffer = cvCreateImage( cvSize( FindROIRect.width - m_PadAlignImage->width+1, FindROIRect.height - m_PadAlignImage->height+1 ), IPL_DEPTH_32F, 1 ); // 상관계수를 구할 이미지(C)

		InspectPadDataQ	*	pNextNode = pAlignMarkNode->m_pNextNode;

		int CntValue = 1;

		int nStep = 0;
		double min;
		double *pFindmax= new double[m_NodeCount]; 
		CvPoint *pFindAlignPosCenter = new CvPoint[m_NodeCount]; 

		//첫 Image는 기준점임으로 100, 0, 0로 기입.
		pFindmax[nStep] = 1.0;
		pFindAlignPosCenter[nStep].x = 0;
		pFindAlignPosCenter[nStep].y = 0;
		nStep++;
		while(pNextNode != &m_TailNode)
		{
			//위치 찾기
			cvSetImageROI(pNextNode->m_pPADAreaImg, FindROIRect);

			if(G_ProcPadAlign.m_AlignImgUseCanny == true)
			{
				IplImage *pTargetImg = cvCloneImage(pNextNode->m_pPADAreaImg);
				cvCanny(pTargetImg, pTargetImg, G_ProcPadAlign.m_AlignImgUseCanny_threshold1, G_ProcPadAlign.m_AlignImgUseCanny_threshold2);
				cvMatchTemplate(pTargetImg, m_PadAlignImage, imgBuffer, CV_TM_CCOEFF_NORMED); // 상관계수를 구하여 C 에 그린다.
				cvMinMaxLoc(imgBuffer, &min, &pFindmax[nStep], NULL, &pFindAlignPosCenter[nStep]); // 상관계수가 최대값을 값는 위치 찾기 
				cvReleaseImage(&pTargetImg);
			}
			else
			{
				cvMatchTemplate(pNextNode->m_pPADAreaImg, m_PadAlignImage, imgBuffer, CV_TM_CCOEFF_NORMED); // 상관계수를 구하여 C 에 그린다.
				cvMinMaxLoc(imgBuffer, &min, &pFindmax[nStep], NULL, &pFindAlignPosCenter[nStep]); // 상관계수가 최대값을 값는 위치 찾기 
			}
			

			if(G_ProcPadAlign.m_AlignResultSave == true)
			{
//#ifndef  TEST_PAD_UPDN_ALIGN
				IplImage *pSaveAlignImage = cvCloneImage(pNextNode->m_pPADAreaImg);
//#else
//				IplImage *pSaveAlignImage = pNextNode->m_pPADAreaImg;
//#endif	
				cvDrawRect(pSaveAlignImage, pFindAlignPosCenter[nStep], cvPoint(pFindAlignPosCenter[nStep].x +AlignImgRect.width, pFindAlignPosCenter[nStep].y +AlignImgRect.height), CV_RGB(255,255,255), 3);
				cvDrawRect(pSaveAlignImage, pFindAlignPosCenter[nStep], cvPoint(pFindAlignPosCenter[nStep].x +AlignImgRect.width, pFindAlignPosCenter[nStep].y +AlignImgRect.height), CV_RGB(0,0,0), 1);

				sprintf_s(AlignResultPath, "%0.2f", pFindmax[nStep]);
				CvPoint PrintTxtPos = cvPoint(10,10);
				PutTextInImageDefect(pSaveAlignImage, &PrintTxtPos, AlignResultPath);

				PrintTxtPos.y+=15;
				sprintf_s(AlignResultPath, "X:%4d",pFindAlignPosCenter[nStep].x+ (pSaveAlignImage->width/2));
				PutTextInImageDefect(pSaveAlignImage, &PrintTxtPos, AlignResultPath);

				PrintTxtPos.y+=15;
				sprintf_s(AlignResultPath, "Y:%4d",pFindAlignPosCenter[nStep].y+ (pSaveAlignImage->height/2));
				PutTextInImageDefect(pSaveAlignImage, &PrintTxtPos, AlignResultPath);

				//if(G_ProcPadAlign.m_AlignImgNotChange == false)
				{
					sprintf_s(AlignResultPath, "%sD%d_AlignPad_%s_%d_Align.jpg",TEST_PAD_ALIGN_IMG_SAVE_PATH, ScanLineNum, (m_PadIndex==0?"Up":"Dn"), CntValue);
					cvSaveImage(AlignResultPath, m_PadAlignImage);
				}

				sprintf_s(AlignResultPath, "%sD%d_AlignPad_%s_%d.jpg",TEST_PAD_ALIGN_IMG_SAVE_PATH, ScanLineNum, (m_PadIndex==0?"Up":"Dn"), ++CntValue);
				cvSaveImage(AlignResultPath, pSaveAlignImage);


//#ifndef  TEST_PAD_UPDN_ALIGN
				cvReleaseImage(&pSaveAlignImage);
//#endif	
			}
			cvResetImageROI(pNextNode->m_pPADAreaImg);
			
			//기준위치의 Center 점 찾기.
			pFindAlignPosCenter[nStep].x += FindROIRect.x + (m_PadAlignImage->width/2);
			pFindAlignPosCenter[nStep].y += FindROIRect.y + (m_PadAlignImage->height/2);
			//기준 위치에서 얼마나 떨어져 있는가?
			pFindAlignPosCenter[nStep].x = pFindAlignPosCenter[nStep].x - AlignImgOrgPos.x;
			pFindAlignPosCenter[nStep].y = pFindAlignPosCenter[nStep].y - AlignImgOrgPos.y;


			if(G_ProcPadAlign.m_AlignImgNotChange == false)
			{
				//새로운 Align Image 취득.
				cvSetImageROI(pNextNode->m_pPADAreaImg, AlignImgRect);
				cvCopyImage(pNextNode->m_pPADAreaImg, m_PadAlignImage);
				cvResetImageROI(pNextNode->m_pPADAreaImg);

				if(G_ProcPadAlign.m_AlignImgUseCanny == true)
				{
					cvCanny(m_PadAlignImage, m_PadAlignImage, G_ProcPadAlign.m_AlignImgUseCanny_threshold1, G_ProcPadAlign.m_AlignImgUseCanny_threshold2);
				}
			}
			

			pNextNode = pNextNode->m_pNextNode;
			nStep++;

		}
		//////////////////////////////////////////////////////////////////////////
		//위치 보정.
		nStep = 1;
		pNextNode = pAlignMarkNode->m_pNextNode;

		//첫 위치에서의 누적 Distance를 기록한다.
		int ShifXDistance = 0;
		int ShifYDistance = 0;
		while(pNextNode != &m_TailNode)
		{
			//지정 값미만이면 화이트 처리 한다.
			if((double)(G_ProcPadAlign.m_AlignOKValue/100.0)>pFindmax[nStep])
			{
				cvZero(pNextNode->m_pPADAreaImg);
				//memset(pNextNode->m_pPADAreaImg->imageData, 0xFF, pNextNode->m_pPADAreaImg->imageSize);
			}
			else
			{
				//Image Shift (up/down)
				ShifXDistance += pFindAlignPosCenter[nStep].x;

				if(DeadZoneOverStep<abs(ShifXDistance))
					DeadZoneOverStep = abs(ShifXDistance);

				ShifYDistance += pFindAlignPosCenter[nStep].y;
				if(ShifYDistance != 0 || ShifXDistance != 0)
				{
					//Image상의 Defect위치를 계산하기전 Align 된정보로 좌표를 통체로 수정 하자.(원래 여기 나와야 한다는 의미이다.)
					//Pad부 Image좌표는 1번위치를 제외 하고는 오차 많큼 변경 될것이다.
					pNextNode->m_PadInScanPos.left	+=(int)(ShifXDistance*m_pCoordObj->m_GlassParam.m_ScanImgResolution_W)*(-1);
					pNextNode->m_PadInScanPos.right	+=(int)(ShifXDistance*m_pCoordObj->m_GlassParam.m_ScanImgResolution_W)*(-1);

					pNextNode->m_PadInScanPos.top		+=(int)(ShifYDistance*m_pCoordObj->m_GlassParam.m_ScanImgResolution_H)*(-1);
					pNextNode->m_PadInScanPos.bottom	+=(int)(ShifYDistance*m_pCoordObj->m_GlassParam.m_ScanImgResolution_H)*(-1);
					//IplImage *pShiftImage = cvCreateImage(cvSize(pNextNode->m_pPADAreaImg->width, pNextNode->m_pPADAreaImg->height), IPL_DEPTH_8U , 1);//cvCloneImage(pNextNode->m_pPADAreaImg);
					cvCopyImage(pNextNode->m_pPADAreaImg, m_BufferSwapImg);
					//if(pNextNode->m_pFrontNode != &m_HeadNode)
					//	cvCopyImage(pNextNode->m_pFrontNode->m_pPADAreaImg, pNextNode->m_pPADAreaImg);
					cvZero(pNextNode->m_pPADAreaImg);
					//memset(pNextNode->m_pPADAreaImg->imageData, 0xFF, pNextNode->m_pPADAreaImg->imageSize);

					CvRect MoveArea, ShiftPosArea;
					MoveArea.x = ShifXDistance>0? ShifXDistance:0;
					MoveArea.y = ShifYDistance>0? ShifYDistance:0;
					MoveArea.width = pNextNode->m_pPADAreaImg->width -abs(ShifXDistance);
					MoveArea.height= pNextNode->m_pPADAreaImg->height -abs(ShifYDistance);

					ShiftPosArea	= MoveArea;
					ShiftPosArea.x	= ShifXDistance>0? 0:ShifXDistance*(-1);
					ShiftPosArea.y	= ShifYDistance>0? 0:ShifYDistance*(-1);

					if( !(MoveArea.height < 0 || ShiftPosArea.height<0 || MoveArea.width<0 ||ShiftPosArea.width<0))
					{//Shift 영역이 넘어가면 그냥 화이트가 될것이다.
						cvSetImageROI(m_BufferSwapImg, MoveArea);
						cvSetImageROI(pNextNode->m_pPADAreaImg, ShiftPosArea);
						cvCopyImage(m_BufferSwapImg, pNextNode->m_pPADAreaImg);
						cvResetImageROI(m_BufferSwapImg);
						cvResetImageROI(pNextNode->m_pPADAreaImg);
					}
					//cvReleaseImage(&pShiftImage);
				}
			}
			pNextNode = pNextNode->m_pNextNode;
			nStep++;
		}
		//////////////////////////////////////////////////////////////////////////
		//Memory 정리.
		if(pFindmax != nullptr)
			delete [] pFindmax;
		pFindmax = nullptr;

		if(pFindAlignPosCenter != nullptr)
			delete [] pFindAlignPosCenter;
		pFindAlignPosCenter = nullptr;

		//보정 완료후(Dead Zone 처리에 들어간다.)
		pNextNode = m_HeadNode.m_pNextNode;
		//Dead Zone 그리기.
		for(int NodeStep = 0; NodeStep < m_NodeCount && pNextNode != &m_TailNode; NodeStep++)
		{
			//Dead Zone 칠하기.
			if(m_PadIndex == 0 )//상부
			{
				//Pad부의 Dead Zone을 칠한다.
				//위쪽
				StartPt.x	= 0;
				StartPt.y	= 0;
				EndPt.x		= pNextNode->m_pPADAreaImg->width; 
				EndPt.y		= ActiveDeadZonePixelSize + DeadZoneOverStep;
				cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
				//아래쪽
				StartPt.x	= 0;
				StartPt.y	= pNextNode->m_pPADAreaImg->height - (m_PadDeadZonePixel+ActiveDeadZonePixelSize*2 + DeadZoneOverStep);
				EndPt.x		= pNextNode->m_pPADAreaImg->width; 
				EndPt.y		= pNextNode->m_pPADAreaImg->height;
				cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
			}
			else//하부
			{
				//Pad부의 Dead Zone을 칠한다.
				//위쪽
				StartPt.x	= 0;
				StartPt.y	= 0;
				EndPt.x		= pNextNode->m_pPADAreaImg->width; 
				EndPt.y		= m_PadDeadZonePixel + ActiveDeadZonePixelSize*2 + DeadZoneOverStep;
				cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
				//아래쪽
				StartPt.x	= 0;
				StartPt.y	= pNextNode->m_pPADAreaImg->height - (ActiveDeadZonePixelSize + DeadZoneOverStep);
				EndPt.x		= pNextNode->m_pPADAreaImg->width; 
				EndPt.y		= pNextNode->m_pPADAreaImg->height;
				cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
				//////////////////////////////////////////////////////////////////////////
				for(int DrawStep = 0; DrawStep<FindPadCnt; DrawStep++)
				{
					if(PAD_DN_List[DrawStep].Width()>0 && PAD_DN_List[DrawStep].Height()>0)

						cvRectangle(pNextNode->m_pPADAreaImg, cvPoint(PAD_DN_List[DrawStep].left, PAD_DN_List[DrawStep].top), 
						cvPoint(PAD_DN_List[DrawStep].right, PAD_DN_List[DrawStep].bottom), DeadZoneColor, CV_FILLED );
				}
			}


			//전면 Dead Zosne이다.
			//왼쪽
			StartPt.x	= 0;
			StartPt.y	= 0;
			EndPt.x		= DeadZoneOverStep; 
			EndPt.y		= pNextNode->m_pPADAreaImg->height;
			cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
			//오른쪽
			StartPt.x	= pNextNode->m_pPADAreaImg->width - DeadZoneOverStep;
			StartPt.y	= 0;
			EndPt.x		= pNextNode->m_pPADAreaImg->width; 
			EndPt.y		= pNextNode->m_pPADAreaImg->height;
			cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );

			StartPt.x	= SmallLeft-DeadZoneOverStep;
			StartPt.y	= 0;
			EndPt.x		= SmallRight+DeadZoneOverStep; 
			EndPt.y		= pNextNode->m_pPADAreaImg->height;
			cvRectangle(pNextNode->m_pPADAreaImg, StartPt, EndPt, DeadZoneColor, CV_FILLED );
			pNextNode = pNextNode->m_pNextNode;
		}
		cvReleaseImage(&imgBuffer);
	}
	else//좌, 우.
	{
		return false;
	}
	return true;
}

bool PAD_ImageCtrlQ::m_fnFindPAD_And_ImageCopy(float Resolution_W,float Resolution_H, IplImage*pOrgImg, CRect* pPadInImgArea, CRect* pPadArea, CRect* pPadORGArea)
{
#ifdef VIEW_CREATE_SCAN_PAD2
	if(pViewMap == nullptr)
		pViewMap = cvCreateImage(cvSize(850, 1050), 8, 3);
	cvNamedWindow("ScnaMapViewer2");
#endif
	EnterCriticalSection(&m_CrtSection);
	if(m_HeadNode.m_pNextNode == &m_TailNode)
	{
		LeaveCriticalSection(&m_CrtSection);
		return false;
	}

	CvRect Org_CutImgArea, Target_CutImgArea;
	Org_CutImgArea.x			= pPadInImgArea->left;
	Org_CutImgArea.y			= pPadInImgArea->top;
	Org_CutImgArea.width		= pPadInImgArea->Width();
	Org_CutImgArea.height	    = pPadInImgArea->Height();

	CRect CheckBuf, PadWorkRect;

	InspectPadDataQ * pNextNodeBuf = nullptr;
	for(int CpyNodeStep = 0; CpyNodeStep<m_NodeCount; CpyNodeStep++)
	{
		pNextNodeBuf = m_pCopyNode->m_pNextNode;

		memcpy(&PadWorkRect, &m_pCopyNode->m_PadInScanPos, sizeof(CRect));



#ifdef VIEW_CREATE_SCAN_PAD2
		if(m_PadIndex == 0)
		{
			cvRectangle(pViewMap, cvPoint(PadWorkRect.left/1000, PadWorkRect.top/1000),
				cvPoint(PadWorkRect.right/1000, PadWorkRect.bottom/1000), CV_RGB(255, 255, 0));//,CV_FILLED);
			//cvRectangle(pViewMap, cvPoint(pPadArea->left/1000, pPadArea->top/1000),
			//	cvPoint(pPadArea->right/1000, pPadArea->bottom/1000), CV_RGB(255, 0, 0));
		}
		
		//if(m_PadIndex == 1)
		//cvRectangle(pViewMap, cvPoint(pPadArea->left/1000, pPadArea->top/1000),
		//	cvPoint(pPadArea->right/1000, pPadArea->bottom/1000), CV_RGB(255, 0, 0));
#endif
		if(PadWorkRect.Width() <=0 || PadWorkRect.Height() - (m_pCoordObj->m_GlassParam.ActiveDeadZoneSize*3) <=0)
		//if(PadWorkRect.Width() < RET_PAD_ALINE_WIDTH || PadWorkRect.Height() < (RET_PAD_ALINE_SPACE_HEIGHT*2))
		{
			m_pCopyNode->m_fnQSetProcessEnd(QDATA_PROC_END_PADINPUT);
			break;
		}
		else if(CheckBuf.IntersectRect(&PadWorkRect, pPadArea) == TRUE)
		{

#ifdef VIEW_CREATE_SCAN_PAD2
			if(m_PadIndex == 0)
			cvRectangle(pViewMap, cvPoint(CheckBuf.left/1000, CheckBuf.top/1000),
				cvPoint(CheckBuf.right/1000, CheckBuf.bottom/1000), CV_RGB(255, 0, 0));
			if(m_PadIndex == 1)
				cvRectangle(pViewMap, cvPoint(CheckBuf.left/1000, CheckBuf.top/1000),
				cvPoint(CheckBuf.right/1000, CheckBuf.bottom/1000), CV_RGB(255, 255, 0));
#endif

			//////////////////////////////////////////////////////////////////////////
			//좌표계 변환.(Machine -> Image Coordinate)
			CheckBuf.left		= CheckBuf.left - PadWorkRect.left;
			CheckBuf.top		= CheckBuf.top - PadWorkRect.top;
			CheckBuf.right		= CheckBuf.right - PadWorkRect.left;
			CheckBuf.bottom		= CheckBuf.bottom -PadWorkRect.top;

			CheckBuf.left = (int)((float)CheckBuf.left/Resolution_W);
			CheckBuf.top = (int)((float)CheckBuf.top/Resolution_H);

			CheckBuf.right = (int)((float)CheckBuf.right/Resolution_W);
			CheckBuf.bottom = (int)((float)CheckBuf.bottom/Resolution_H);



			Target_CutImgArea.x			= CheckBuf.left;
			Target_CutImgArea.y			= CheckBuf.top;
			Target_CutImgArea.width		= CheckBuf.Width();
			Target_CutImgArea.height	    = CheckBuf.Height();
			//if(Target_CutImgArea.width <= 0 && Target_CutImgArea.height <= 0)
			//	return false;
			if(Org_CutImgArea.height != Target_CutImgArea.height)
			{
				if(Org_CutImgArea.height > Target_CutImgArea.height)
					Org_CutImgArea.height = Target_CutImgArea.height;
				else
					Target_CutImgArea.height = Org_CutImgArea.height;
			}

			if(Org_CutImgArea.width != Target_CutImgArea.width)//|| Org_CutImgArea.height != Target_CutImgArea.height)
			{
				TRACE("\r\n Leave XX m_fnFindPAD_And_ImageCopy(Error  %d != %d or %d != %d)", Org_CutImgArea.width, Target_CutImgArea.width, Org_CutImgArea.height, Target_CutImgArea.height);
				LeaveCriticalSection(&m_CrtSection);
				return false;
			}

			m_pCopyNode->QDataCpy(pOrgImg, &Org_CutImgArea, &Target_CutImgArea);
			//////////////////////////////////////////////////////////////////////////
			/////Up/DnPad 부 Dead Zone 설정은 Align을 완료 한후.
			//CRect PadDeadZone;
			//int SizePadDeadZone = m_PadDeadZone;
			////Pad부의 Dead Zone을 칠한다.
			//CvScalar DeadZoneColor;
			//DeadZoneColor = CV_RGB(255,255,255);
			//switch (m_PadIndex)
			//{
			//case 0://Up
			//	PadDeadZone.left	= pPadORGArea->left;
			//	PadDeadZone.top		= pPadORGArea->bottom - SizePadDeadZone;
			//	PadDeadZone.right	= pPadORGArea->right; 
			//	PadDeadZone.bottom	= pPadORGArea->bottom;
			//	//DeadZoneColor = CV_RGB(100,100,100);
			//	break;
			//case 1://DN
			//	PadDeadZone.left	= pPadORGArea->left;
			//	PadDeadZone.top		= pPadORGArea->top;
			//	PadDeadZone.right	= pPadORGArea->right; 
			//	PadDeadZone.bottom	= pPadORGArea->top + SizePadDeadZone;
			//	//DeadZoneColor = CV_RGB(150,150,150);
			//	break;
			//case 2://LF
			//	PadDeadZone.left	= pPadORGArea->right - SizePadDeadZone;
			//	PadDeadZone.top		= pPadORGArea->top;
			//	PadDeadZone.right	= pPadORGArea->right; 
			//	PadDeadZone.bottom	= pPadORGArea->bottom;
			//	//DeadZoneColor = CV_RGB(200,200,200);
			//	break;

			//case 3://RT
			//	PadDeadZone.left	= pPadORGArea->left;
			//	PadDeadZone.top		= pPadORGArea->top;
			//	PadDeadZone.right	= pPadORGArea->left + SizePadDeadZone; 
			//	PadDeadZone.bottom	= pPadORGArea->bottom;
			//	//DeadZoneColor = CV_RGB(220,220,220);
			//	break;
			//}
			//if(CheckBuf.IntersectRect(&PadDeadZone, PadWorkRect ) == TRUE && SizePadDeadZone > 0)
			//{
			//	CheckBuf.left		= CheckBuf.left		- PadWorkRect.left;
			//	CheckBuf.top		= CheckBuf.top		- PadWorkRect.top;
			//	CheckBuf.right		= CheckBuf.right		- PadWorkRect.left;
			//	CheckBuf.bottom	= CheckBuf.bottom	- PadWorkRect.top;

			//	CheckBuf.left = (int)((float)CheckBuf.left/Resolution_W);
			//	CheckBuf.top = (int)((float)CheckBuf.top/Resolution_H);

			//	CheckBuf.right = (int)((float)CheckBuf.right/Resolution_W);
			//	CheckBuf.bottom = (int)((float)CheckBuf.bottom/Resolution_H);

			//	cvRectangle(m_pCopyNode->m_pPADAreaImg, cvPoint(CheckBuf.left, CheckBuf.top), cvPoint(CheckBuf.right, CheckBuf.bottom), DeadZoneColor, CV_FILLED );
			//}
			////else
			////{
			////	TRACE("\r\n Leave XX m_fnFindPAD_And_ImageCopy Paint Dead Zone(Error  %d != %d or %d != %d)", Org_CutImgArea.width, Target_CutImgArea.width, Org_CutImgArea.height, Target_CutImgArea.height);
			////}

			//if(m_PadIndex == 1)//DN
			//{
			//	if(m_PadDNDeadZoneW != 0 && m_PadDNDeadZoneH != 0)
			//	{
			//		//Dead Zone 좌측
			//		PadDeadZone.left	= pPadORGArea->left;
			//		PadDeadZone.top		= pPadORGArea->bottom	- m_PadDNDeadZoneH;
			//		PadDeadZone.right	= pPadORGArea->left		+ m_PadDNDeadZoneW; 
			//		PadDeadZone.bottom	= pPadORGArea->bottom;

			//		if(CheckBuf.IntersectRect(&PadDeadZone, PadWorkRect ) == TRUE)
			//		{
			//			CheckBuf.left		= CheckBuf.left		- PadWorkRect.left;
			//			CheckBuf.top		= CheckBuf.top		- PadWorkRect.top;
			//			CheckBuf.right		= CheckBuf.right	- PadWorkRect.left;
			//			CheckBuf.bottom		= CheckBuf.bottom	- PadWorkRect.top;

			//			CheckBuf.left = (int)((float)CheckBuf.left/Resolution_W);
			//			CheckBuf.top = (int)((float)CheckBuf.top/Resolution_H);

			//			CheckBuf.right = (int)((float)CheckBuf.right/Resolution_W);
			//			CheckBuf.bottom = (int)((float)CheckBuf.bottom/Resolution_H);

			//			cvRectangle(m_pCopyNode->m_pPADAreaImg, cvPoint(CheckBuf.left, CheckBuf.top), cvPoint(CheckBuf.right, CheckBuf.bottom), DeadZoneColor, CV_FILLED );
			//		}
			//		//Dead Zone 우측
			//		PadDeadZone.left	= pPadORGArea->right - m_PadDNDeadZoneW;
			//		PadDeadZone.top		= pPadORGArea->bottom- m_PadDNDeadZoneH;
			//		PadDeadZone.right	= pPadORGArea->right; 
			//		PadDeadZone.bottom	= pPadORGArea->bottom;

			//		if(CheckBuf.IntersectRect(&PadDeadZone, PadWorkRect ) == TRUE)
			//		{
			//			CheckBuf.left		= CheckBuf.left		- PadWorkRect.left;
			//			CheckBuf.top		= CheckBuf.top		- PadWorkRect.top;
			//			CheckBuf.right		= CheckBuf.right		- PadWorkRect.left;
			//			CheckBuf.bottom	= CheckBuf.bottom	- PadWorkRect.top;

			//			CheckBuf.left = (int)((float)CheckBuf.left/Resolution_W);
			//			CheckBuf.top = (int)((float)CheckBuf.top/Resolution_H);

			//			CheckBuf.right = (int)((float)CheckBuf.right/Resolution_W);
			//			CheckBuf.bottom = (int)((float)CheckBuf.bottom/Resolution_H);

			//			cvRectangle(m_pCopyNode->m_pPADAreaImg, cvPoint(CheckBuf.left, CheckBuf.top), cvPoint(CheckBuf.right, CheckBuf.bottom), DeadZoneColor, CV_FILLED );
			//		}
			//	}
			//}

			m_pCopyNode->m_fnQSetProcessEnd(QDATA_PROC_END_PADINPUT);


			break;
		}


		if(pNextNodeBuf == &m_TailNode)
			pNextNodeBuf = m_HeadNode.m_pNextNode;

		m_pCopyNode = pNextNodeBuf;
		pNextNodeBuf = nullptr;
	}

	LeaveCriticalSection(&m_CrtSection);
#ifdef VIEW_CREATE_SCAN_PAD2
	cvShowImage("ScnaMapViewer2", pViewMap);
	cvWaitKey(10);
#endif
	return true;
}