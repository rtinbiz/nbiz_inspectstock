#include "StdAfx.h"
#include "PADInspectProc.h"

UINT fn_PadInspectProcess(LPVOID pParam)
{
	PROC_PAD	*		pProcParam		= (PROC_PAD *) pParam;

	PAD_ImageCtrlQ *		pPADCtrlQ				= pProcParam->m_pPADCtrlQ;
	DefectDataQ	*			pResultDefectQ		= pProcParam->m_pResultDefectQ;
	ImageSaveQ	*		    pInspectSaveImgQ	= pProcParam->m_pInspectSaveImgQ;
	CoordinatesProc *		pCoordObj				= pProcParam->m_pCoordProcObj;
	PROC_STATE	*			pUpdateState			= pProcParam->m_pUpdateState;
	CInterServerInterface *	pSInterface				= pProcParam->m_pServerInterface;
	//////////////////////////////////////////////////////////////////////////
	int *pPadCutType									= &pCoordObj->m_HWParam.PadInspectAreaType;
	int PadProcStart									= 0;
	int PadProcEnd										= 0;
	//////////////////////////////////////////////////////////////////////////
	CString			*			pStrDefect			= nullptr;
	//////////////////////////////////////////////////////////////////////////
	SCANIMGINFO	*			pCoodArea = nullptr;
	//////////////////////////////////////////////////////////////////////////
	extern int		G_MasterTaskIndex;
	//////////////////////////////////////////////////////////////////////////
	IplImage * ImageRevebuf = cvCreateImage( cvSize(	pCoordObj->m_HWParam.m_DefectReviewSizeW, 
																			pCoordObj->m_HWParam.m_DefectReviewSizeH), 
																			IPL_DEPTH_8U, 1);
	//cvZero(ImageRevebuf);
	memset(ImageRevebuf->imageData, 0xFF, ImageRevebuf->imageSize);
	//////////////////////////////////////////////////////////////////////////
	DWORD tickTickN = 0;

	InspectPadDataQ * pPutPadNode = nullptr;
	//////////////////////////////////////////////////////////////////////////
	//Grab Image Area.
	CRect PadInspectImgArea;
	CRect DefectAreaInImage;

	int DefectInfoSize =(sizeof(COORD_DINFO)*pCoordObj->m_HWParam.m_DefectMaxCntInImg);
	UCHAR *pSendRetData = new UCHAR[sizeof(int)+DefectInfoSize];

	bool bSizeError = false;
#ifndef TEST_DEFECT_IMG_IN_ORG
	int PasteStartX = 0;
	int PasteStartY = 0;
#endif
	DWORD nTimeOutCheck = 0;
	DWORD nTimePadAlign = 0;
	//cvNamedWindow("PadDefectView");
	//Pad Area Inspection
	while(pProcParam->m_bProcessRun == true)
	{

		if(pUpdateState->m_InspectionStop  == INSPECTION_STOP)
		{
			pProcParam->m_StatePADInspect  = PAD_INSPECTION_END;
		}
		else 	if(pProcParam->m_StatePADInspect  == PAD_INSPECTION_RUN)
		{
			
			tickTickN = GetTickCount();

			switch(*pPadCutType	)
			{
			case INSPACT_PAD_SKIP:
				{
					PadProcStart	= 0;
					PadProcEnd		= 0;
					pProcParam->m_StatePADInspect  = PAD_INSPECTION_END;
					pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"T%d : PAD_Inspect Skip", pSInterface->m_uTaskNo);
					continue;
				}break;
			case INSPACT_PAD_UPDOWN:
				{
					PadProcStart	= 0;
					PadProcEnd		= 2;
				}break;
			case INSPACT_PAD_LEFTRIGHT:
				{
					PadProcStart	= 2;
					PadProcEnd		= 4;
				}break;
			case INSPACT_PAD_BOTH:
				{
					PadProcStart	= 0;
					PadProcEnd		= 4;
				}break;
			}
			//////////////////////////////////////////////////////////////////////////
			pUpdateState->m_PADInspectStep =  0;
			int PadInspectNodeCnt =0;

			pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"T%d : PAD_Inspect Start", pSInterface->m_uTaskNo);
			for(int PadSep = PadProcStart; PadSep < PadProcEnd; PadSep++)
			{
				pPADCtrlQ		= &pProcParam->m_pPADCtrlQ[PadSep];
				PadInspectNodeCnt = pPADCtrlQ->QGetNodeCnt();

				//영상에 Blur를 준다.
				if(pUpdateState->nTEST_SAVE_PAD_AREA_BlurStep > 0)
				{
					pPADCtrlQ->m_fnPadImageBlurAction(pUpdateState->nTEST_SAVE_PAD_AREA_BlurStep);
				}
				//Pad Align 실행.
				if(pUpdateState->bTEST_USE_PAD_ALIGN  == true)
				{
					nTimePadAlign = GetTickCount();
					//pPADCtrlQ->m_fnPADImageAlign_I(pPADCtrlQ->m_ScanLineNum);
					pPADCtrlQ->m_fnPADImageAlign_II(pPADCtrlQ->m_ScanLineNum);
					pUpdateState->m_PadAlignProcTick = GetTickCount() - nTimePadAlign;
				}
				else
				{
					pUpdateState->m_PadAlignProcTick  = 0;
				}
//#ifdef TEST_SAVE_PAD_AREA_SAVE

				if(pUpdateState->bTEST_SAVE_PAD_AREA == true)
				{
					pPADCtrlQ->m_fnSavePadInspectImage(pCoordObj->m_HWParam.strOrgImgPath, pUpdateState->bTEST_SAVE_PAD_AREA_BMP);
				}
//#endif
				if(PadInspectNodeCnt<3)
				{
					//TRACE("PadInspectNodeCnt(%d) Error!!!!\r\n", PadInspectNodeCnt);
					pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"T%d : PAD_Inspect NodeCnt Error(Cnt : %d>3)", pSInterface->m_uTaskNo, PadInspectNodeCnt);
					break;
				}
				for(int FragSep= 0; FragSep<pPADCtrlQ->m_ImgFragmentatCnt; FragSep++)
				{
					pPADCtrlQ->m_ImgFragmentatStep = FragSep;

					if(pPADCtrlQ->m_fnSetFirstWorkNode() == true && PadInspectNodeCnt>0)
					{
						for(int InspectStep = 0; InspectStep< PadInspectNodeCnt; InspectStep++)
						{
							pPutPadNode = pPADCtrlQ->m_fnGetWorkNode();
							pPADCtrlQ->fn_QPutPADInspector(pPutPadNode);
							if(pPADCtrlQ->m_fnMoveNextWorkNode() == false)
							{
								//TRACE("m_fnMoveNextWorkNode1 Error!!!!\r\n");
								pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"T%d : PAD_Inspect Node Scan Error(Step :%d)", pSInterface->m_uTaskNo, InspectStep);
								break;
							}
						}
						//연산 Start
						nTimeOutCheck = GetTickCount();//Inspect Time Out Check.
						_AOI_Pad_Process_Start(PadSep, PadInspectNodeCnt);
						//연산 결과 받기.
						if(pPADCtrlQ->m_fnSetFirstWorkNode() == true)
						{
							int InspectStep = 0;
							while(InspectStep<PadInspectNodeCnt)
							{
								pPutPadNode = pPADCtrlQ->m_fnGetWorkNode();
								if(pPutPadNode->m_NodeProcState == QDATA_PROC_END_PADINSPECT)
								{
									pUpdateState->m_PADInspectStep +=1;//상태 갱신.

									if(pCoordObj->m_HWParam.m_SaveDefectOrgImage > 0 && pPutPadNode->m_DefectListCnt>0)
									{//Defect가 있는 Image를 저장 할경우.
										CRect newPos;
										float FlagmentMachineH = ((float)(pPADCtrlQ->m_ImgFragmentatStep)*(float)(pPutPadNode->m_ImgFragmentatSizeH)*pCoordObj->m_GlassParam.m_ScanImgResolution_H);
										newPos.left =  pPutPadNode->m_PadInScanPos.left;
										newPos.right =  pPutPadNode->m_PadInScanPos.right;
										newPos.top = pPutPadNode->m_PadInScanPos.top +(int) ((FlagmentMachineH*(float)pPADCtrlQ->m_ImgFragmentatStep));
										newPos.left = newPos.top+(int)FlagmentMachineH;
										pInspectSaveImgQ->QPutNode(2, pPADCtrlQ->m_ScanLineNum, pPADCtrlQ->m_PadIndex, pPutPadNode->m_pImgFragmentat, &newPos);
									}

									PadInspectImgArea.left	= 0;
									PadInspectImgArea.right = pCoordObj->m_HWParam.m_ProcImageW;
									PadInspectImgArea.top	= 0;
									PadInspectImgArea.bottom= pPutPadNode->m_ImgFragmentatSizeH;

									for(int WriteStep = 0; WriteStep < pPutPadNode->m_DefectListCnt; WriteStep++)
									{
										//Image Defect Coordinate좌표계로 변환.
										G_CalcImgPADPosToCood(	&pPADCtrlQ->m_ImgFragmentatStep, 
																			&pPutPadNode->m_ImgFragmentatSizeH,
																			pCoordObj,
																			&pPutPadNode->m_PadInScanPos,
																			&pPutPadNode->m_pSearchRetList[WriteStep],
																			&pPutPadNode->m_pCoordinateRetList[WriteStep]);

										memcpy(DefectAreaInImage, &pPutPadNode->m_pSearchRetList[WriteStep].StartX,  sizeof(RECT));

										if( DefectAreaInImage.left == DefectAreaInImage.right || DefectAreaInImage.top == DefectAreaInImage.bottom)
											bSizeError = true;
										else
											bSizeError = false;
#ifdef TEST_DEFECT_IMG_IN_ORG
										if(G_CalcInImageDefPosAndSize_PAD(&PadInspectImgArea, &DefectAreaInImage, &pCoordObj->m_HWParam)/* == TRUE*/)
										{
											G_CalcDefImgAndMask_OnOrgImg_PAD(	pPutPadNode->m_pImgFragmentat,
																									ImageRevebuf,  
																									&DefectAreaInImage, 
																									pProcParam->m_ScanLineNum, bSizeError);
#else
										if(G_CalcInImageDefPosAndSize_PAD(&PasteStartX, &PasteStartY,&PadInspectImgArea, &DefectAreaInImage, &pCoordObj->m_HWParam)/* == TRUE*/)
										{
											G_CalcDefImgAndMask_OnOrgImg_PAD(&PasteStartX, &PasteStartY,	pPutPadNode->m_pImgFragmentat,
																									ImageRevebuf,  
																									&DefectAreaInImage, 
																									pProcParam->m_ScanLineNum, bSizeError);
#endif
											pResultDefectQ->QPutNodePad(pPADCtrlQ->m_ScanLineNum, pPADCtrlQ->m_PadIndex, pPutPadNode->m_PadPosIndex,
																					*pPutPadNode->m_pImgFragmentatStep, WriteStep+1,

																					&pPutPadNode->m_pSearchRetList[WriteStep],
																					&pPutPadNode->m_pCoordinateRetList[WriteStep],
																					(BYTE*)ImageRevebuf->imageData, 
																					ImageRevebuf->imageSize);


											memset(ImageRevebuf->imageData, 0xAF, ImageRevebuf->imageSize);
											//cvShowImage("PadDefectView", ImageRevebuf);
											//cvWaitKey(100);
										}
									}
									//Data 저장.
									*((int*)pSendRetData)  = pPutPadNode->m_DefectListCnt;
									memcpy(pSendRetData+sizeof(int), (char*)pPutPadNode->m_pCoordinateRetList,  (sizeof(COORD_DINFO)*pPutPadNode->m_DefectListCnt));
									//Pad부 Defect 정보 알리기
									pSInterface->m_fnSendCommand_Nrs(G_MasterTaskIndex,
																					nBiz_AOI_Inspection, 
																					nBiz_Recv_InspectPADRetInfo,
																					0, 
																					(USHORT)(sizeof(int)+(sizeof(COORD_DINFO)*pPutPadNode->m_DefectListCnt)),
																					pSendRetData);
									//if(pUpdateState->bTEST_VS75_REVIEW_SEND == true)
									//{
									//	pSInterface->m_fnSendCommand_Nrs(REVIEW_TASK_NUM,
									//		nBiz_AOI_Inspection, 
									//		nBiz_Recv_InspectPADRetInfo,
									//		0, 
									//		(USHORT)(sizeof(int)+(sizeof(COORD_DINFO)*pPutPadNode->m_DefectListCnt)),
									//		pSendRetData);
									//}
									//pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"PAD Defect Data Send End");

									pPutPadNode->m_fnQSetProcessEnd(QDATA_PROC_END_PADINPUT);
									if(pPADCtrlQ->m_fnMoveNextWorkNode() == false)
									{
										//TRACE("m_fnMoveNextWorkNode2 Error!!!!\r\n");
										pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"m_fnMoveNextWorkNode2 PAD ScnaLineNum :%d, Pad Index", 
											pPADCtrlQ->m_ScanLineNum,pPADCtrlQ->m_PadIndex);
										break;
									}
									InspectStep++;
								}
								else 
								{
									//TimeOUt Check.
									if((GetTickCount()-nTimeOutCheck) > 100000)//10Sec Time Out.
									{
										pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"T%d : %d - 10Sec Time Out.", 	pSInterface->m_uTaskNo);
										break;
									}
									Sleep(0);
								}
							}
						}
					}
				}
			}
			if(pUpdateState != nullptr)
			{//첫장은 오류, 1번에서 2번까지의 시간을 잰다.
				pUpdateState->m_PadProcTick[0]		= ::GetTickCount() - tickTickN;
			}
			pProcParam->m_StatePADInspect  = PAD_INSPECTION_END;
			pSInterface->m_fnPrintLog(pSInterface->m_uTaskNo, 4,"T%d : PAD_Inspect End", pSInterface->m_uTaskNo);
		}
		else
			Sleep(1);
	}
	if(pSendRetData != nullptr)
		delete []pSendRetData;

	cvReleaseImage(&ImageRevebuf);
	pProcParam->m_pResultThread = nullptr;
	return 0;
}

//////////////////////////////////////////////////////////////////////////
void G_CalcImgPADPosToCood(int *pFragmentStep, int *pFragmentSizeH, CoordinatesProc*pCoordObj, ComRect* pCoodArea, DEFECT_DATA *m_pSearchRetList, COORD_DINFO *pCoordinateRetList)
{
	pCoordinateRetList->DefectType = m_pSearchRetList->DefectType;
	float FlagmentMachineH = ((float)(*pFragmentStep)*(float)(*pFragmentSizeH)*pCoordObj->m_GlassParam.m_ScanImgResolution_H);


	//Lens곡률에 따른 변화 적용.
	//Image Axis X의 중심을 기준으로 멀어질수록 곡률적용 한다.
	float LensCurvature = 0.0;

	if(pCoordObj->m_GlassParam.m_ScanImgResolution_W < 3.0f)
		LensCurvature = pCoordObj->m_HWParam.m_LensCurvature_Type1;
	else
		LensCurvature = pCoordObj->m_HWParam.m_LensCurvature_Type2;
	int ImgCenter = pCoordObj->m_HWParam.m_ProcImageW/2;

	float addDefectDisX = ((m_pSearchRetList->StartX+(abs(m_pSearchRetList->StartX-m_pSearchRetList->EndX)/2)) - ImgCenter)*LensCurvature;
	//float addDefectDisX = abs(ImgCenter - m_pSearchRetList->StartX+abs(m_pSearchRetList->StartX-m_pSearchRetList->EndX))*LensCurvature;

	//if(ImgCenter>m_pSearchRetList->StartX)
	//	addDefectDisX *=(-1);
	//Macine 좌표계
	pCoordinateRetList->CoodStartX = pCoodArea->left + (int)((float)m_pSearchRetList->StartX*pCoordObj->m_GlassParam.m_ScanImgResolution_W+addDefectDisX);
	pCoordinateRetList->CoodStartY = pCoodArea->top + (int)(((float)m_pSearchRetList->StartY*pCoordObj->m_GlassParam.m_ScanImgResolution_H)+FlagmentMachineH );

	//원점 좌표계
	pCoordinateRetList->CoodStartX = pCoordinateRetList->CoodStartX - pCoordObj->m_GlassParam.CenterPointX;
	//pCoordinateRetList->CoodStartY = (pCoordinateRetList->CoodStartY - pCoordObj->m_GlassParam.CenterPointY)*(-1);
	pCoordinateRetList->CoodStartY = pCoordObj->m_GlassParam.CenterPointY - pCoordinateRetList->CoodStartY;

	//pCoordinateRetList->CoodWidth  =  (int)((float)abs(m_pSearchRetList->EndX - m_pSearchRetList->StartX)*pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	//pCoordinateRetList->CoodHeight =  (int)((float)abs(m_pSearchRetList->EndY - m_pSearchRetList->StartY)*pCoordObj->m_GlassParam.m_ScanImgResolution_H);
	pCoordinateRetList->CoodWidth  =  (int)((float)(abs(m_pSearchRetList->EndX - m_pSearchRetList->StartX)+1)*pCoordObj->m_GlassParam.m_ScanImgResolution_W);
	pCoordinateRetList->CoodHeight =  (int)((float)(abs(m_pSearchRetList->EndY - m_pSearchRetList->StartY)+1)*pCoordObj->m_GlassParam.m_ScanImgResolution_H);

	pCoordinateRetList->nDefectCount	= m_pSearchRetList->nDefectCount;
	pCoordinateRetList->nDValueMin		= m_pSearchRetList->nDValueMin;
	pCoordinateRetList->nDValueMax		= m_pSearchRetList->nDValueMax;
	pCoordinateRetList->nDValueAvg		= m_pSearchRetList->nDValueAvg;
}
//////////////////////////////////////////////////////////////////////////
//Defect Image가 들어오면 Size 계산및 Image사의 좌표 계산 까지 한다.특정 Size에 맞게.
#ifdef TEST_DEFECT_IMG_IN_ORG
bool G_CalcInImageDefPosAndSize_PAD(CRect *pImagePos, CRect *pDefecPos, HW_PARAM *pHWParam)
{
	CRect CalcIn_InsertRect;
	CRect CalcIn_RetPosRect;

	if(pDefecPos->left == pDefecPos->right )
	{
		if(pDefecPos->left<2)
			pDefecPos->right = pDefecPos->left+2;
		else
			pDefecPos->left -=2;
	}
	if(pDefecPos->top == pDefecPos->bottom )
	{
		if(pDefecPos->top<2)
			pDefecPos->bottom = pDefecPos->top+2;
		else
			pDefecPos->top -=2;
	}

	if(CalcIn_InsertRect.IntersectRect(pImagePos, pDefecPos)==TRUE)
	{
		//Width 방향으로.
		if(CalcIn_InsertRect.Width()<pHWParam->m_DefectReviewSizeW)
		{
			//중간에서 작을때 포함.
			CalcIn_RetPosRect.left = (CalcIn_InsertRect.left+(CalcIn_InsertRect.Width()>>1/*/2*/)) - (pHWParam->m_DefectReviewSizeW/2);
			CalcIn_RetPosRect.right = CalcIn_RetPosRect.left+pHWParam->m_DefectReviewSizeW;

			if(CalcIn_RetPosRect.left <= pImagePos->left)//좌측.
			{
				CalcIn_RetPosRect.left	  = pImagePos->left;
				CalcIn_RetPosRect.right  = pHWParam->m_DefectReviewSizeW;
			}
			else if(CalcIn_RetPosRect.right >= pImagePos->right)//우측.
			{
				CalcIn_RetPosRect.left  = pImagePos->right - pHWParam->m_DefectReviewSizeW;
				pImagePos->right = pImagePos->right;
			}
		}
		else//Union이 클때.
		{
			CalcIn_RetPosRect.left = (CalcIn_InsertRect.left+(CalcIn_InsertRect.Width()>>1/*/2*/)) - (pHWParam->m_DefectReviewSizeW/2);
			CalcIn_RetPosRect.right = CalcIn_RetPosRect.left+pHWParam->m_DefectReviewSizeW;
		}
		//Height 방향으로.
		if(CalcIn_InsertRect.Height()<pHWParam->m_DefectReviewSizeH)
		{
			//중간에서 작을때 포함.
			CalcIn_RetPosRect.top = (CalcIn_InsertRect.top+(CalcIn_InsertRect.Height()>>1/*/2*/)) - (pHWParam->m_DefectReviewSizeH/2);
			CalcIn_RetPosRect.bottom = CalcIn_RetPosRect.top+pHWParam->m_DefectReviewSizeH;

			if(CalcIn_RetPosRect.top <= pImagePos->top)//상단.
			{
				CalcIn_RetPosRect.top	  = pImagePos->top;
				CalcIn_RetPosRect.bottom  = pHWParam->m_DefectReviewSizeH;
			}
			else if(CalcIn_RetPosRect.bottom >= pImagePos->bottom)//하단.
			{
				CalcIn_RetPosRect.top  = pImagePos->bottom - pHWParam->m_DefectReviewSizeH;
				pImagePos->bottom = pImagePos->bottom;
			}
		}
		else//Union이 클때.
		{
			CalcIn_RetPosRect.top = (CalcIn_InsertRect.top+(CalcIn_InsertRect.Height()>>1/*/2*/))  - (pHWParam->m_DefectReviewSizeH/2);
			CalcIn_RetPosRect.bottom = CalcIn_RetPosRect.top+pHWParam->m_DefectReviewSizeH;
		}
	}
	else
	{
		//심각한 오류.(이미지위에 좌표 없음.)
		memset(&CalcIn_RetPosRect, 0x00, sizeof(CRect));
		return false;
	}
	memcpy(pDefecPos, CalcIn_RetPosRect, sizeof(CRect));
	return true;
}

void G_CalcDefImgAndMask_OnOrgImg_PAD(IplImage *pImageBuffer, IplImage *pSaveImageBuffer,CRect* pDefecArea, int ScanLineNum, bool bSizeError)
{
	CvRect Calc_CutImgArea;

	Calc_CutImgArea.x			= pDefecArea->left;
	Calc_CutImgArea.y			= pDefecArea->top;
	Calc_CutImgArea.width		= pDefecArea->Width();
	Calc_CutImgArea.height	    = pDefecArea->Height();

	cvSetImageROI(pImageBuffer, Calc_CutImgArea);
	cvCopy(pImageBuffer, pSaveImageBuffer);

	//if(bSizeError == true)//Size Zero가 들어 왔던 Defect에 대해서는 Result를 임시로 반전 시키다.(Debugging용이다.)
	//	cvNot(pSaveImageBuffer,pSaveImageBuffer);

	//if(ScanLineNum%2 == 0)
	//	cvFlip(pSaveImageBuffer,0);//짝수 방향일경우 Image가 Y축이 바뀐 상태임으로 전환.
	cvResetImageROI(pImageBuffer);
}

#else
bool G_CalcInImageDefPosAndSize_PAD(int *pPasteStartX, int *pPasteStartY,CRect *pImagePos, CRect *pDefecPos, HW_PARAM *pHWParam)
{
	CRect CalcIn_InsertRect;
	CRect CalcIn_RetPosRect;

	if(pDefecPos->left == pDefecPos->right )
	{
		if(pDefecPos->left<2)
			pDefecPos->right = pDefecPos->left+2;
		else
			pDefecPos->left -=2;
	}
	if(pDefecPos->top == pDefecPos->bottom )
	{
		if(pDefecPos->top<2)
			pDefecPos->bottom = pDefecPos->top+2;
		else
			pDefecPos->top -=2;
	}

	if(CalcIn_InsertRect.IntersectRect(pImagePos, pDefecPos)==TRUE)
	{
		int DefectCX = pDefecPos->left + pDefecPos->Width()/2;
		int DefectCY = pDefecPos->top + pDefecPos->Height()/2;
		CRect OrgCpyArea;
		OrgCpyArea.left		= DefectCX-(pHWParam->m_DefectReviewSizeW/2);
		OrgCpyArea.top		= DefectCY-(pHWParam->m_DefectReviewSizeH/2);
		OrgCpyArea.right		= OrgCpyArea.left +pHWParam->m_DefectReviewSizeW;
		OrgCpyArea.bottom	= OrgCpyArea.top +pHWParam->m_DefectReviewSizeH;

		CalcIn_RetPosRect.IntersectRect(pImagePos, &OrgCpyArea);//원판에서 자를 영역.

		*pPasteStartX = abs(OrgCpyArea.Width() - CalcIn_RetPosRect.Width());
		*pPasteStartY = abs(OrgCpyArea.Height() - CalcIn_RetPosRect.Height());
		if(pImagePos->right<OrgCpyArea.right)
			*pPasteStartX = 0;
		if(pImagePos->bottom<OrgCpyArea.bottom)
			*pPasteStartY = 0;
	}
	else
	{	
		//심각한 오류.(이미지위에 좌표 없음.)
		memset(&CalcIn_RetPosRect, 0x00, sizeof(CRect));
		return false;
	}

	memcpy(pDefecPos, CalcIn_RetPosRect, sizeof(CRect));
	return true;
}


void G_CalcDefImgAndMask_OnOrgImg_PAD(int *pPasteStartX, int *pPasteStartY, IplImage *pImageBuffer, IplImage *pSaveImageBuffer, CRect* pDefecArea, int ScanLineNum, bool bSizeError)
{
	CvRect Calc_OrgArea;
	CvRect Calc_CutImgArea;
	Calc_CutImgArea.x			= pDefecArea->left;
	Calc_CutImgArea.y			= pDefecArea->top;
	Calc_CutImgArea.width		= pDefecArea->Width();
	Calc_CutImgArea.height	    = pDefecArea->Height();

	Calc_OrgArea = Calc_CutImgArea;
	Calc_OrgArea.x = *pPasteStartX;
	Calc_OrgArea.y = *pPasteStartY;

	cvSetImageROI(pImageBuffer, Calc_CutImgArea);
	cvSetImageROI(pSaveImageBuffer, Calc_OrgArea);
	cvCopy(pImageBuffer, pSaveImageBuffer);
	cvResetImageROI(pImageBuffer);
	cvResetImageROI(pSaveImageBuffer);

	//if(bSizeError == true)//Size Zero가 들어 왔던 Defect에 대해서는 Result를 임시로 반전 시키다.(Debugging용이다.)
	//	cvNot(pSaveImageBuffer,pSaveImageBuffer);

	//if(ScanLineNum%2 == 0)
	//	cvFlip(pSaveImageBuffer,0);//짝수 방향일경우 Image가 Y축이 바뀐 상태임으로 전환.
}
#endif