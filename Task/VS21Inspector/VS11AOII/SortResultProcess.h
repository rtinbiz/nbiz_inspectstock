#pragma once
//#include "..\..\..\CommonHeader\Inspection\Class\InterServerInterface.h"
#include "..\..\..\CommonHeader\Inspection\CoordinatesProc.h"
#include "AOI_ImageCtrlQ.h"
#include "PadAreaCtrlQ.h"
#include "PADInspectProc.h"
#include "DefectFileFormat.h"
#include "GrabberCtrl.h"
/** 
@brief		Write Result Process에사 사용되는 구조체

@remark
-			Result Process  이후 Data 처리를 위한 구조체 이다.
-			
@author		최지형
@date		2010.00.00
*/ 
typedef struct SORT_RESULT_PROCESS_TRG
{
	bool				m_bProcessRun;
	CWinThread		*	m_pResultThread;
	AOI_ImageCtrlQ	*	m_pIDTCtrlQ;

	CGraberCtrl		*	m_pGrabberCtrl;

	CInterServerInterface *		m_pServerInterface;
	PROC_STATE		*	m_pUpdateState;

	//Pad부 처리 Object
	PROC_PAD		*	m_pPadProcObj;
	DefectDataQ		*	m_pResultDefectQ;
	

	SORT_RESULT_PROCESS_TRG()
	{
		m_bProcessRun		=  false;
		m_pResultThread		= nullptr;
		m_pIDTCtrlQ			= nullptr;

		m_pGrabberCtrl		= nullptr;

		m_pServerInterface	= nullptr;
		m_pUpdateState		= nullptr;

		m_pPadProcObj		= nullptr;
		m_pResultDefectQ	= nullptr;

	}

} PROC_RETSORT, *LPPROC_RETSORT;

//Result Process Def.
UINT fn_SortResultProcess(LPVOID pParam);